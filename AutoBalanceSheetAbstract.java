import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class AutoBalanceSheetAbstract
{

      Common common = new Common();

      Vector VMillCode,VItemTable;
      Vector VDeptCode,VDeptName,VOpening,VReceipt,VIssue,VClosing;

      String SPrevDate="";
      String SMaxDate="";
      String SProcDate="";

      Vector V1,V2,V3,V4,V5,V6,V7,V8,V9,V10,V11,V12;
      Vector V13,V14,V15;
 
      double dDOTotal=0,dDRTotal=0,dDITotal=0,dDCTotal=0;
      double dNOTotal=0,dNRTotal=0,dNITotal=0,dNCTotal=0;

      String SStDate,SEnDate;

      int iMillCode=0;
      String SItemTable;

      Connection theConnection=null;

      boolean        bComflag  = true;

      public AutoBalanceSheetAbstract()
      {
           try
           {
                ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                                theConnection =  oraConnection.getConnection();
  
                SMaxDate = common.pureDate(common.getDate(common.parseDate(common.getServerPureDate()),1,-1));

                setMillData();
                for(int i=0;i<VMillCode.size();i++)
                {
                     iMillCode  = common.toInt((String)VMillCode.elementAt(i));
                     SItemTable = (String)VItemTable.elementAt(i); 
                     getPrevData();

                     if(!SPrevDate.equals(SMaxDate))
                          setData();
                }
           }
           catch(Exception ex)
           {
                System.out.println(ex);
                ex.printStackTrace();
           }

      }

      public void setData()
      {
           int iDiff=0;

           iDiff = common.toInt(common.getDateDiff(common.parseDate(SMaxDate),common.parseDate(SProcDate)));

           if(iDiff>=0)
           {
                setVector();
                setDataIntoVector();
                insertIntoTable();
                getACommit();

                SProcDate = common.pureDate(common.getDate(common.parseDate(SProcDate),1,1));
                setData();
           }
      }

      public void setVector()
      {
           V1  = new Vector();
           V2  = new Vector();
           V3  = new Vector();
           V4  = new Vector();
           V5  = new Vector();
           V6  = new Vector();
           V7  = new Vector();
           V8  = new Vector();
           V9  = new Vector();
           V10 = new Vector();
           V11 = new Vector();
           V12 = new Vector();
           V13 = new Vector();
           V14 = new Vector();
           V15 = new Vector();

           SStDate = SProcDate;
           SEnDate = SProcDate;

           String SGrnFilter="",SIssueFilter="";

           if(iMillCode==0)
           {
                SGrnFilter     = " And (Grn.MillCode is Null OR GRN.MillCode = "+iMillCode+") ";
                SIssueFilter   = " And (Issue.MillCode is Null OR Issue.MillCode = "+iMillCode+")";

           }
           else
           {
                SGrnFilter     = " And  GRN.MillCode = "+iMillCode;
                SIssueFilter   = " And  Issue.MillCode = "+iMillCode;
           }

           String QS2  = " Create Table c2 as select Code,sum(OpgGrnQty) as OpgGrnQty,sum(OpgGrnVal) as OpgGrnVal from "+
                         " (SELECT Grn.Code, Sum(Grn.GrnQty) as OpgGrnQty, Sum(GRN.GrnValue) as OpgGrnVal "+
                         " FROM GRN "+
                         " WHERE Grn.RejFlag=0 and (Grn.GrnBlock<2 Or Grn.GrnType=2)  And GRN.GrnDate <'"+SStDate+"' "+SGrnFilter+" "+
                         " GROUP BY Grn.Code "+
                         " Union All "+
                         " SELECT Grn.Code, Sum(Grn.GrnQty) as OpgGrnQty, Sum(GRN.GrnValue) as OpgGrnVal "+
                         " FROM GRN "+
                         " WHERE Grn.RejFlag=1 and (Grn.GrnBlock<2 Or Grn.GrnType=2)  And GRN.RejDate <'"+SStDate+"' "+SGrnFilter+" "+
                         " GROUP BY Grn.Code) "+
                         " Group by Code";


           String QS3  = " Create Table c3 as SELECT Issue.Code, Sum(Issue.Qty) as OpgIssQty, Sum(Issue.IssueValue) as OpgIssVal "+
                         " FROM Issue "+
                         " WHERE Issue.IssueDate <'"+SStDate+"' "+SIssueFilter+" "+
                         " GROUP BY Issue.Code";

           String QS4  = " Create Table c4 as SELECT Code, Sum(GrnQty) as GrnQty, Sum(GrnVal) as GrnVal from "+
                         " (SELECT Grn.Code, Sum(Grn.GrnQty) as GrnQty, Sum(GRN.GrnValue) as GrnVal "+
                         " FROM GRN "+
                         " WHERE Grn.RejFlag=0 and GRN.GrnDate >='"+SStDate+"' And Grn.GrnDate <='"+SEnDate+"' "+SGrnFilter+" "+
                         " GROUP BY Grn.Code "+
                         " Union All "+
                         " SELECT Grn.Code, Sum(Grn.GrnQty) as GrnQty, Sum(GRN.GrnValue) as GrnVal "+
                         " FROM GRN "+
                         " WHERE Grn.RejFlag=1 and GRN.RejDate >='"+SStDate+"' And Grn.RejDate <='"+SEnDate+"' "+SGrnFilter+" "+
                         " GROUP BY Grn.Code) "+
                         " Group by Code ";


           String QS5  = " Create Table c5 as SELECT Code, Sum(IssQty) as IssQty, Sum(IssVal) as IssVal from "+
                         " (SELECT Grn.Code, Sum(Grn.GrnQty) as IssQty, Sum(GRN.GrnValue) as IssVal "+
                         " FROM GRN "+
                         " WHERE Grn.RejFlag=0 and GRN.grnBlock>1 and Grn.GrnType<>2 And GRN.GrnDate >='"+SStDate+"' And Grn.GrnDate <='"+SEnDate+"' "+SGrnFilter+" "+
                         " GROUP BY Grn.Code "+
                         " Union All "+
                         " SELECT Grn.Code, Sum(Grn.GrnQty) as IssQty, Sum(GRN.GrnValue) as IssVal "+
                         " FROM GRN "+
                         " WHERE Grn.RejFlag=1 and GRN.grnBlock>1 and Grn.GrnType<>2 And GRN.RejDate >='"+SStDate+"' And Grn.RejDate <='"+SEnDate+"' "+SGrnFilter+" "+
                         " GROUP BY Grn.Code) "+
                         " Group by Code ";


           String QS6  = " Create Table c6 as SELECT Issue.Code, Sum(Issue.Qty) as IssQty, Sum(Issue.IssueValue) as IssVal "+
                         " FROM Issue "+
                         " WHERE Issue.IssueDate >='"+SStDate+"' And Issue.IssueDate <='"+SEnDate+"' "+SIssueFilter+" "+
                         " GROUP BY Issue.Code";


           String QS    = " Select ";

                       if(iMillCode==0)
                       {
                             QS = QS + " InvItems.StkGroupCode, " ;
                             QS = QS + " StockGroup.GroupName, "+
                                       " InvItems.Item_Code,InvItems.Item_Name, "+
                                       " Uom.UomName,InvItems.LocName, ";
                       }
                       else
                       {
                             QS = QS + " "+SItemTable+".StkGroupCode, " ;
                             QS = QS + " StockGroup.GroupName, "+
                                       " InvItems.Item_Code,InvItems.Item_Name, "+
                                       " Uom.UomName,"+SItemTable+".LocName, ";
                       }

                       if(iMillCode!=0)
                             QS = QS + " nvl("+SItemTable+".OpgQty,0), " ;
                       else
                             QS = QS + " nvl(InvItems.OpgQty,0), " ;

           
                             QS = QS +  " c2.OpgGRNQty,c3.OpgIssQty, "+
                                        " c4.GrnQty,c5.IssQty,c6.IssQty, ";


                       if(iMillCode!=0)
                             QS = QS + " nvl("+SItemTable+".OpgVal,0), ";
                       else
                             QS = QS + " nvl(InvItems.OpgVal,0), ";


                             QS = QS +  " c2.OpgGRNVal,c3.OpgIssVal, "+
                                        " c4.GrnVal,c5.IssVal,c6.IssVal ";


                       if(iMillCode==0)
                       {
                            QS = QS+ " From ((((((InvItems "+ 
                            " inner Join StockGroup On StockGroup.GroupCode = InvItems.StkGroupCode) "+
                            " Inner Join Uom on InvItems.UomCode=Uom.UomCode) "+
                            " left Join c2 On c2.Code = InvItems.Item_Code) "+
                            " Left Join c3 On c3.Code = InvItems.Item_Code) "+
                            " Left Join c4 On c4.Code = InvItems.Item_Code) "+
                            " Left Join c5 On c5.Code = InvItems.Item_Code) "+
                            " Left Join c6 On c6.Code = InvItems.Item_Code "+
                            " Order By 2 ";
                       }
                       else
                       {
                            QS = QS + " From ((((((("+SItemTable+" "+
                            " Inner Join StockGroup On StockGroup.GroupCode = "+SItemTable+".StkGroupCode) "+
                            " inner join InvItems on "+SItemTable+".Item_Code = InvItems.Item_code )"+
                            " Inner Join Uom on InvItems.UomCode=Uom.UomCode) "+
                            " Left Join c2 On c2.Code = InvItems.Item_Code) "+
                            " Left Join c3 On c3.Code = InvItems.Item_Code) "+
                            " Left Join c4 On c4.Code = InvItems.Item_Code) "+
                            " Left Join c5 On c5.Code = InvItems.Item_Code) "+
                            " Left Join c6 On c6.Code = InvItems.Item_Code  "+
                            " Order By 2 ";
                       }
                               
                       try
                       {
                             FileWriter fw  = new FileWriter(common.getPrintPath()+"stocksql.sql");
                             fw.write(QS);
                             fw.close();
                       }
                       catch(Exception e)
                       {}


           try
           {
                Statement stat           = theConnection.createStatement();

                try{stat.execute("Drop Table c2");}catch(Exception ex){}
                try{stat.execute("Drop Table c3");}catch(Exception ex){}
                try{stat.execute("Drop Table c4");}catch(Exception ex){}
                try{stat.execute("Drop Table c5");}catch(Exception ex){}
                try{stat.execute("Drop Table c6");}catch(Exception ex){}

                stat.execute(QS2);
                stat.execute(QS3);
                stat.execute(QS4);
                stat.execute(QS5);
                stat.execute(QS6);

                ResultSet res = stat.executeQuery(QS);

                while(res.next())
                {
                     double dPOpg   = common.toDouble(res.getString(7));
                     double dPRec   = common.toDouble(res.getString(8));
                     double dPIss   = common.toDouble(res.getString(9));

                     double dPOVal  = common.toDouble(res.getString(13));
                     double dPRVal  = common.toDouble(res.getString(14));
                     double dPIVal  = common.toDouble(res.getString(15));

                     double dOpg    = dPOpg+dPRec-dPIss;
                     double dOVal   = dPOVal+dPRVal-dPIVal;

                     double dRec    = common.toDouble(res.getString(10));
                     double dIss    = common.toDouble(res.getString(11))+common.toDouble(res.getString(12));

                     double dRVal   = common.toDouble(res.getString(16));
                     double dIVal   = common.toDouble(res.getString(17))+common.toDouble(res.getString(18));

                     double dStock = dOpg+dRec-dIss;
                     double dValue = dOVal+dRVal-dIVal;
                     double dRate  = 0;

                     if(dOpg==0 && dRec==0 && dIss==0 && dOVal==0 && dRVal==0 && dIVal==0)
                        continue;

                     if(dStock != 0)
                         dRate  = dValue / dStock;

                     V1.addElement(res.getString(1));
                     V2.addElement(res.getString(2));
                     V3.addElement(res.getString(3));
                     V4.addElement(res.getString(4));
                     V5.addElement(res.getString(5));
                     V6.addElement(res.getString(6));

                     V7.addElement(common.getRound(dOpg,3));
                     V10.addElement(common.getRound(dOVal,2));
                     V8.addElement(common.getRound(dRec,3));
                     V11.addElement(common.getRound(dRVal,2));
                     V9.addElement(common.getRound(dIss,3));
                     V12.addElement(common.getRound(dIVal,2));
                     V13.addElement(common.getRound(dStock,3));
                     V14.addElement(common.getRound(dValue,2));
                     V15.addElement(common.getRound(dRate,3));
                }
                res.close();
                stat.close();
           }
           catch(Exception ex)
           {
                 System.out.println(ex);
                 ex.printStackTrace();
           }
      }

      private void setDataIntoVector()
      {
           VDeptCode = new Vector();
           VDeptName = new Vector();
           VOpening  = new Vector();
           VReceipt  = new Vector();
           VIssue    = new Vector();
           VClosing  = new Vector();
 
           String SPDept="";
           int    iSig=0;
 
           for(int i=0;i<V1.size();i++)
           {
                  String SDept = (String)V1.elementAt(i);
                  if(!SDept.equals(SPDept))
                  {
                       iSig = 1;
                       SPDept = SDept;
                       if(i>0)
                           getDeptTotal();
 
                       dDOTotal = 0;
                       dDRTotal = 0;
                       dDITotal = 0;
                       dDCTotal = 0;
                  }
                  else
                       iSig = 0;
 
                  if(iSig==1)
                  {
                      VDeptCode.addElement((String)V1.elementAt(i));
                      VDeptName.addElement((String)V2.elementAt(i));
                  }
 
                  dDOTotal = dDOTotal + common.toDouble((String)V10.elementAt(i));
                  dDRTotal = dDRTotal + common.toDouble((String)V11.elementAt(i));
                  dDITotal = dDITotal + common.toDouble((String)V12.elementAt(i));
                  dDCTotal = dDCTotal + common.toDouble((String)V14.elementAt(i));
 
                  dNOTotal = dNOTotal + common.toDouble((String)V10.elementAt(i));
                  dNRTotal = dNRTotal + common.toDouble((String)V11.elementAt(i));
                  dNITotal = dNITotal + common.toDouble((String)V12.elementAt(i));
                  dNCTotal = dNCTotal + common.toDouble((String)V14.elementAt(i));
           }
           getDeptTotal();
      }

      public void getDeptTotal()
      {
           VOpening.addElement(common.getRound(dDOTotal,2));
           VReceipt.addElement(common.getRound(dDRTotal,2));
           VIssue.addElement(common.getRound(dDITotal,2));
           VClosing.addElement(common.getRound(dDCTotal,2));
      }

      private void insertIntoTable()
      {
           try
           {
                if(theConnection  . getAutoCommit())
                     theConnection  . setAutoCommit(false);
                
                String SInsQS = " Insert into BSAbstract(Id,BSDate,GroupCode,Opening,Receipt,Issue,Closing,MillCode)"+
                                " Values(BSAbstract_seq.nextval,?,?,?,?,?,?,?)";
 
 
                for(int i=0; i<VDeptCode.size(); i++)
                {
                     PreparedStatement thePrepare = theConnection.prepareStatement(SInsQS);
 
                     thePrepare.setString(1,SProcDate);
                     thePrepare.setString(2,(String)VDeptCode.elementAt(i));
                     thePrepare.setString(3,(String)VOpening.elementAt(i));
                     thePrepare.setString(4,(String)VReceipt.elementAt(i));
                     thePrepare.setString(5,(String)VIssue.elementAt(i));
                     thePrepare.setString(6,(String)VClosing.elementAt(i));
                     thePrepare.setInt(7,iMillCode);
 
                     thePrepare.executeUpdate();
                     thePrepare.close();
                }
           }
           catch(Exception ex)
           {
                System.out.println(ex);
                bComflag = false;
           }
      }

      private void getACommit()
      {
           try
           {
                if(bComflag)
                {
                     theConnection  . commit();
                     System         . out.println("Commit");
                }
                else
                {
                     theConnection  . rollback();
                     System         . out.println("RollBack");
                }
                theConnection   . setAutoCommit(true);
           }catch(Exception ex)
           {
                ex.printStackTrace();
           }
      }

      private void setMillData()
      {
           VMillCode  = new Vector();
           VItemTable = new Vector();

           String QS = " Select MillCode,Item_Table from Mill order by MillCode";

           try
           {
                 Statement stat = theConnection.createStatement();
                 ResultSet theResult = stat.executeQuery(QS);
                 while(theResult.next())
                 {
                       VMillCode.addElement(common.parseNull(theResult.getString(1)));
                       VItemTable.addElement(common.parseNull(theResult.getString(2)));
                 }
                 theResult.close();
                 stat.close();
           }
           catch(Exception ex)
           {
                 System.out.println(ex);
           }
      }

      private void getPrevData()
      {

           SPrevDate="";
           SProcDate="";

           String QS = " Select Max(BSDate) from BSAbstract Where MillCode="+iMillCode;

           try
           {
                 Statement stat = theConnection.createStatement();
                 ResultSet theResult = stat.executeQuery(QS);
                 while(theResult.next())
                 {
                       SPrevDate = common.parseNull(theResult.getString(1));
                 }
                 theResult.close();
                 stat.close();

                 if(common.toInt(SPrevDate)>0)
                 {
                      int iDiff = common.toInt(common.getDateDiff(common.parseDate(SMaxDate),common.parseDate(SPrevDate)));

                      if(iDiff>0)
                      {
                           SProcDate = common.pureDate(common.getDate(common.parseDate(SPrevDate),1,1));
                      }
                      else
                      {
                           SProcDate = SMaxDate;
                      }
                 }
                 else
                 {
                      SProcDate = SMaxDate;
                 }
           }
           catch(Exception ex)
           {
                 System.out.println(ex);
           }
      }

      public static void main(String arg[])
      {
           new AutoBalanceSheetAbstract();
      }



}
