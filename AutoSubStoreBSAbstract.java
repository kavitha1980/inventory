import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class AutoSubStoreBSAbstract
{

      Common common = new Common();

      Vector VDeptCode,VOpgVal,VRecVal,VIssVal,VRetVal,VAdjVal,VClsVal;

      String SPrevDate="";
      String SMaxDate="";
      String SProcDate="";

      String SStDate,SEnDate;

      Connection theConnection  = null;
      Connection theDConnection = null;

      boolean        bComflag  = true;

      public AutoSubStoreBSAbstract()
      {
           try
           {
                ORAConnection oraConnection   = ORAConnection.getORAConnection();
                              theConnection   = oraConnection.getConnection();
 
 
                DORAConnection jdbc           = DORAConnection.getORAConnection();
                               theDConnection = jdbc.getConnection();


                SMaxDate = common.pureDate(common.getDate(common.parseDate(common.getServerPureDate()),1,-1));

                getPrevData();

                if(!SPrevDate.equals(SMaxDate))
                     setData();

           }
           catch(Exception ex)
           {
                System.out.println(ex);
                ex.printStackTrace();
           }

      }

      public void setData()
      {
           int iDiff=0;

           iDiff = common.toInt(common.getDateDiff(common.parseDate(SMaxDate),common.parseDate(SProcDate)));

           if(iDiff>=0)
           {
                setVector();
                insertIntoTable();
                getACommit();

                SProcDate = common.pureDate(common.getDate(common.parseDate(SProcDate),1,1));
                setData();
           }
      }

      public void setVector()
      {
           VDeptCode      = new Vector();
           VOpgVal        = new Vector();
           VRecVal        = new Vector();
           VRetVal        = new Vector(); 
           VIssVal        = new Vector();
           VAdjVal        = new Vector();
           VClsVal        = new Vector();

           SStDate = SProcDate;
           SEnDate = SProcDate;

           int iStDate = common.toInt(SStDate);
           int iEnDate = common.toInt(SEnDate);


           try
           {
                Statement stat   = theDConnection.createStatement();

                ResultSet result = stat.executeQuery(getQString(iStDate,iEnDate));

                while(result.next())
                {
                     VDeptCode.addElement(result.getString(1));

                     double dOpgVal    = result.getDouble(2);
                     double dGrnOpgVal = result.getDouble(3);
                     double dRetOpgVal = result.getDouble(4);
                     double dIssOpgVal = result.getDouble(5);
                     double dAdjOpgVal = result.getDouble(6);
     
                     double dMOpgVal   = dOpgVal+dGrnOpgVal+dRetOpgVal+dAdjOpgVal-dIssOpgVal;

                     double dRecVal    = result.getDouble(7);
                     double dRetVal    = result.getDouble(8);
                     double dIssVal    = result.getDouble(9);
                     double dAdjVal    = result.getDouble(10);

                     double dClsVal    = (dMOpgVal+dRecVal+dRetVal+dAdjVal)-dIssVal;
     
                     VOpgVal.addElement(common.getRound(dMOpgVal,2));
                     VRecVal.addElement(common.getRound(dRecVal,2));
                     VRetVal.addElement(common.getRound(dRetVal,2));
                     VIssVal.addElement(common.getRound(dIssVal,2));
                     VAdjVal.addElement(common.getRound(dAdjVal,2));
                     VClsVal.addElement(common.getRound(dClsVal,2));
                }
                result.close();
                stat.close();
           }
           catch(Exception ex)
           {
                 System.out.println(ex);
                 ex.printStackTrace();
           }
      }

      private void insertIntoTable()
      {
           try
           {
                if(theConnection  . getAutoCommit())
                     theConnection  . setAutoCommit(false);
                
                String SInsQS = " Insert into SubStoreBSAbstract(Id,BSDate,GroupCode,Opening,Receipt,Issue,IssueReturn,Adjustment,Closing)"+
                                " Values(SubStoreBSAbstract_seq.nextval,?,?,?,?,?,?,?,?)";
 
 
                for(int i=0; i<VDeptCode.size(); i++)
                {
                     PreparedStatement thePrepare = theConnection.prepareStatement(SInsQS);
 
                     thePrepare.setString(1,SProcDate);
                     thePrepare.setString(2,(String)VDeptCode.elementAt(i));
                     thePrepare.setString(3,(String)VOpgVal.elementAt(i));
                     thePrepare.setString(4,(String)VRecVal.elementAt(i));
                     thePrepare.setString(5,(String)VIssVal.elementAt(i));
                     thePrepare.setString(6,(String)VRetVal.elementAt(i));
                     thePrepare.setString(7,(String)VAdjVal.elementAt(i));
                     thePrepare.setString(8,(String)VClsVal.elementAt(i));
 
                     thePrepare.executeUpdate();
                     thePrepare.close();
                }
           }
           catch(Exception ex)
           {
                System.out.println(ex);
                bComflag = false;
           }
      }

      private void getACommit()
      {
           try
           {
                if(bComflag)
                {
                     theConnection  . commit();
                     System         . out.println("Commit");
                }
                else
                {
                     theConnection  . rollback();
                     System         . out.println("RollBack");
                }
                theConnection   . setAutoCommit(true);
           }catch(Exception ex)
           {
                ex.printStackTrace();
           }
      }

      private void getPrevData()
      {

           SPrevDate="";
           SProcDate="";

           String QS = " Select Max(BSDate) from SubStoreBSAbstract ";

           try
           {
                 Statement stat = theConnection.createStatement();
                 ResultSet theResult = stat.executeQuery(QS);
                 while(theResult.next())
                 {
                       SPrevDate = common.parseNull(theResult.getString(1));
                 }
                 theResult.close();
                 stat.close();

                 if(common.toInt(SPrevDate)>0)
                 {
                      int iDiff = common.toInt(common.getDateDiff(common.parseDate(SMaxDate),common.parseDate(SPrevDate)));

                      if(iDiff>0)
                      {
                           SProcDate = common.pureDate(common.getDate(common.parseDate(SPrevDate),1,1));
                      }
                      else
                      {
                           SProcDate = SMaxDate;
                      }
                 }
                 else
                 {
                      SProcDate = SMaxDate;
                 }
           }
           catch(Exception ex)
           {
                 System.out.println(ex);
           }
      }

      public static void main(String arg[])
      {
           new AutoSubStoreBSAbstract();
      }

      private String getQString(int iStDate,int iEnDate)
      {
           String QS = "";
 
           QS = " select stkgroupcode, "+
                " sum(opgval) as oval, "+
                " sum(opggrnval) as ogval, "+
                " sum(opgretval) as orval, "+
                " sum(opgissval) as oival, "+
                " sum(opgadjval) as oaval, "+
                " sum(grnval) as gval, "+
                " sum(retval) as rval, "+
                " sum(issval) as ival, "+
                " sum(adjval) as aval from "+
                " (select invitems.item_code as rawcode,invitems.stkgroupcode, "+
                " sum(invitems.opg_val) as opgval, "+
                " 0 as opggrnval, "+
                " 0 as opgretval, "+
                " 0 as opgissval, "+
                " 0 as opgadjval, "+
                " 0 as grnval, "+
                " 0 as retval, "+
                " 0 as issval, "+
                " 0 as adjval  "+
                " from invitems "+
                " group by invitems.item_code,invitems.stkgroupcode "+
                " union all "+
                " select SubstoreReceipt.Rawcode as rawcode,invitems.stkgroupcode, "+
                " 0 as opgval, "+
                " sum(SubstoreReceipt.GrnQty*SubstoreReceipt.GrnRate) as opggrnval, "+
                " 0 as opgretval, "+
                " 0 as opgissval, "+
                " 0 as opgadjval, "+
                " 0 as grnval,  "+
                " 0 as retval, "+
                " 0 as issval, "+
                " 0 as adjval  "+
                " from SubstoreReceipt  "+
                " inner join invitems on SubstoreReceipt.RawCode=InvItems.Item_Code  "+
                " Where SubstoreReceipt.GrnDate<"+iStDate+
                " and SubStoreReceipt.RecType=0 "+
                " group by SubstoreReceipt.rawcode,invitems.stkgroupcode "+
                " union all "+
                " select SubstoreReceipt.Rawcode as rawcode,invitems.stkgroupcode, "+
                " 0 as opgval, "+
                " 0 as opggrnval, "+
                " sum(SubstoreReceipt.GrnQty*SubstoreReceipt.GrnRate) as opgretval, "+
                " 0 as opgissval, "+
                " 0 as opgadjval, "+
                " 0 as grnval,  "+
                " 0 as retval, "+
                " 0 as issval, "+
                " 0 as adjval  "+
                " from SubstoreReceipt  "+
                " inner join invitems on SubstoreReceipt.RawCode=InvItems.Item_Code  "+
                " Where SubstoreReceipt.GrnDate<"+iStDate+
                " and SubStoreReceipt.RecType=1 "+
                " group by SubstoreReceipt.rawcode,invitems.stkgroupcode "+
                " union all "+
                " select SubstoreIssue.Rawcode as rawcode,invitems.stkgroupcode, "+
                " 0 as opgval, "+
                " 0 as opggrnval, "+
                " 0 as opgretval, "+
                " sum(SubstoreIssue.IssueWeight*SubstoreIssue.IssRate) as opgissval, "+
                " 0 as opgadjval, "+
                " 0 as grnval,  "+
                " 0 as retval, "+
                " 0 as issval, "+
                " 0 as adjval  "+
                " from SubstoreIssue  "+
                " inner join invitems on SubstoreIssue.RawCode=InvItems.Item_Code  "+
                " Where SubStoreIssue.IndentNo>0 and SubstoreIssue.ConDate<"+iStDate+
                " group by SubstoreIssue.rawcode,invitems.stkgroupcode "+
                " union all "+
                " select SubstoreReceipt.Rawcode as rawcode,invitems.stkgroupcode, "+
                " 0 as opgval, "+
                " 0 as opggrnval, "+
                " 0 as opgretval, "+
                " 0 as opgissval, "+
                " sum(SubstoreReceipt.GrnQty*SubstoreReceipt.GrnRate) as opgadjval, "+
                " 0 as grnval,  "+
                " 0 as retval, "+
                " 0 as issval, "+
                " 0 as adjval  "+
                " from SubstoreReceipt  "+
                " inner join invitems on SubstoreReceipt.RawCode=InvItems.Item_Code  "+
                " Where SubstoreReceipt.GrnDate<"+iStDate+
                " and SubStoreReceipt.RecType=2 "+
                " group by SubstoreReceipt.rawcode,invitems.stkgroupcode "+
                " union all "+
                " select SubstoreIssue.Rawcode as rawcode,invitems.stkgroupcode, "+
                " 0 as opgval, "+
                " 0 as opggrnval, "+
                " 0 as opgretval, "+
                " 0 as opgissval, "+
                " sum(SubstoreIssue.IssueWeight*SubstoreIssue.IssRate*-1) as opgadjval, "+
                " 0 as grnval,  "+
                " 0 as retval, "+
                " 0 as issval, "+
                " 0 as adjval "+
                " from SubstoreIssue  "+
                " inner join invitems on SubstoreIssue.RawCode=InvItems.Item_Code  "+
                " Where SubStoreIssue.IndentNo=0 and SubstoreIssue.ConDate<"+iStDate+
                " group by SubstoreIssue.rawcode,invitems.stkgroupcode "+
                " union all "+
                " select SubstoreReceipt.Rawcode as rawcode,invitems.stkgroupcode, "+
                " 0 as opgval, "+
                " 0 as opggrnval, "+
                " 0 as opgretval, "+
                " 0 as opgissval, "+
                " 0 as opgadjval, "+
                " sum(SubstoreReceipt.GrnQty*SubstoreReceipt.GrnRate) as grnval, "+
                " 0 as retval, "+
                " 0 as issval, "+
                " 0 as adjval  "+
                " from SubstoreReceipt  "+
                " inner join invitems on SubstoreReceipt.RawCode=InvItems.Item_Code  "+
                " Where SubstoreReceipt.GrnDate>="+iStDate+" and SubstoreReceipt.GrnDate<="+iEnDate+
                " and SubStoreReceipt.RecType=0 "+
                " group by SubstoreReceipt.rawcode,invitems.stkgroupcode "+
                " union all "+
                " select SubstoreReceipt.Rawcode as rawcode,invitems.stkgroupcode, "+
                " 0 as opgval, "+
                " 0 as opggrnval, "+
                " 0 as opgretval, "+
                " 0 as opgissval, "+
                " 0 as opgadjval, "+
                " 0 as grnval, "+
                " sum(SubstoreReceipt.GrnQty*SubstoreReceipt.GrnRate) as retval, "+
                " 0 as issval, "+
                " 0 as adjval  "+
                " from SubstoreReceipt  "+
                " inner join invitems on SubstoreReceipt.RawCode=InvItems.Item_Code  "+
                " Where SubstoreReceipt.GrnDate>="+iStDate+" and SubstoreReceipt.GrnDate<="+iEnDate+
                " and SubStoreReceipt.RecType=1 "+
                " group by SubstoreReceipt.rawcode,invitems.stkgroupcode "+
                " union all "+
                " select SubstoreIssue.Rawcode as rawcode,invitems.stkgroupcode, "+
                " 0 as opgval, "+
                " 0 as opggrnval, "+
                " 0 as opgretval, "+
                " 0 as opgissval, "+
                " 0 as opgadjval, "+
                " 0 as grnval,  "+
                " 0 as retval, "+
                " sum(SubstoreIssue.IssueWeight*SubstoreIssue.IssRate) as issval, "+
                " 0 as adjval  "+
                " from SubstoreIssue  "+
                " inner join invitems on SubstoreIssue.RawCode=InvItems.Item_Code  "+
                " Where SubStoreIssue.IndentNo>0 and SubstoreIssue.ConDate>="+iStDate+" and SubstoreIssue.ConDate<="+iEnDate+
                " group by SubstoreIssue.rawcode,invitems.stkgroupcode "+
                " union all "+
                " select SubstoreReceipt.Rawcode as rawcode,invitems.stkgroupcode, "+
                " 0 as opgval, "+
                " 0 as opggrnval, "+
                " 0 as opgretval, "+
                " 0 as opgissval, "+
                " 0 as opgadjval, "+
                " 0 as grnval, "+
                " 0 as retval, "+
                " 0 as issval, "+
                " sum(SubstoreReceipt.GrnQty*SubstoreReceipt.GrnRate) as adjval "+
                " from SubstoreReceipt  "+
                " inner join invitems on SubstoreReceipt.RawCode=InvItems.Item_Code  "+
                " Where SubstoreReceipt.GrnDate>="+iStDate+" and SubstoreReceipt.GrnDate<="+iEnDate+
                " and SubStoreReceipt.RecType=2 "+
                " group by SubstoreReceipt.rawcode,invitems.stkgroupcode "+
                " union all "+
                " select SubstoreIssue.Rawcode as rawcode,invitems.stkgroupcode, "+
                " 0 as opgval, "+
                " 0 as opggrnval, "+
                " 0 as opgretval, "+
                " 0 as opgissval, "+
                " 0 as opgadjval, "+
                " 0 as grnval,  "+
                " 0 as retval, "+
                " 0 as issval, "+
                " sum(SubstoreIssue.IssueWeight*SubstoreIssue.IssRate*-1) as adjval "+
                " from SubstoreIssue  "+
                " inner join invitems on SubstoreIssue.RawCode=InvItems.Item_Code  "+
                " Where SubStoreIssue.IndentNo=0 and SubstoreIssue.ConDate>="+iStDate+" and SubstoreIssue.ConDate<="+iEnDate+
                " group by SubstoreIssue.rawcode,invitems.stkgroupcode) "+
                " group by stkgroupcode  "+
                " order by stkgroupcode ";
 
           return QS;
      }




}
