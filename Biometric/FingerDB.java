package Biometric;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.io.*;
import javax.sql.rowset.serial.SerialBlob;


public class FingerDB
{

     private static final String tableName="ONETOUCHEMPLOYEE";
     private static final String userColumn="EmpCode";
     private static final String print1Column="FingerPrint";
     private static final String print2Column="print2";
	
	private String URL="jdbc:mysql://localhost:3306/";
	private String host;
	private String database;
	private String userName;
	private String pwd;
	private java.sql.Connection connection=null;
	private String preppedStmtInsert=null;
	private String preppedStmtUpdate=null;
     ArrayList theList;


	public class Record 
	{
		String userID;
		byte[] fmdBinary;
		
	   Record(String ID,byte[] fmd)
	   {
		   userID=ID;
		   fmdBinary=fmd;
	   }
	}
	
	public FingerDB(String _host, String db,String user, String password)
	{
		database=db;
		userName=user;
		pwd=password;
		host=_host;
		
		URL = "jdbc:mysql://" + host + ":3306/";		
		preppedStmtInsert="INSERT INTO " + tableName + "(" + userColumn + "," + print1Column + ") VALUES(?,?)";
	}
	
/*   public void finalize()
	{
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     } */
	
	
	public boolean UserExists(String userID) throws SQLException
	{
		String sqlStmt="Select " + userColumn + " from " + tableName + " WHERE " + userColumn + "='" + userID + "'";
		Statement st=connection.createStatement();
		ResultSet rs=st.executeQuery(sqlStmt);
		return rs.next();
	}
	
	
	public List<Record> GetAllFPData() throws SQLException
	{
		List<Record> listUsers=new ArrayList<Record>();
		String sqlStmt="Select * from "+ tableName;
		Statement st=connection.createStatement();
		ResultSet rs = st.executeQuery(sqlStmt);
		while(rs.next())
		{
			if(rs.getBytes(print1Column)!=null)
               listUsers.add(new Record(rs.getString(userColumn), getOracleBlob(rs,"FINGERPRINT")));
		}	
		return listUsers;
	}
     public List<Record> GetAllFPData(int iUserCode) throws SQLException
	{
		List<Record> listUsers=new ArrayList<Record>();
          String sqlStmt="Select * from "+ tableName+" Where EmpCode=(Select EmpCode from RawUser where UserCode="+iUserCode+")";
		Statement st=connection.createStatement();
		ResultSet rs = st.executeQuery(sqlStmt);
		while(rs.next())
		{
			if(rs.getBytes(print1Column)!=null)
               listUsers.add(new Record(rs.getString(userColumn), getOracleBlob(rs,"FINGERPRINT")));
		}	
		return listUsers;
	}
	
	public String GetConnectionString()
	{
	return URL + " User: " + this.userName;
	}
	
	public String GetExpectedTableSchema()
	{
	 return "Table: " + tableName + " PK(VARCHAR(32)): " + userColumn + "VARBINARY(4000): " + print1Column;	
	}
	public void Insert(String userID,byte[] print1) throws SQLException
	{
		java.sql.PreparedStatement pst= connection.prepareStatement(preppedStmtInsert);
		pst.setString(1, userID);
		pst.setBytes(2, print1);
		pst.execute();
          pst.close();
/*        java.sql.CallableStatement call1= connection.prepareCall(" call UpdateName()");
          call1.execute();
          call1.close();*/

	}
	public void Open() throws SQLException
	{
               try
		{     
			Class.forName("oracle.jdbc.OracleDriver");
               connection = 
                   DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","essl","essl");   
               }catch(Exception ex)
		{
			System.out.println(ex);
			ex.printStackTrace();
		}

	}
	public void Close() throws SQLException
	{
          connection.close(); 
	}
    private byte[] getOracleBlob(ResultSet result, String columnName) throws SQLException
    {
    
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        byte[] bytes = null;
        
        try {    
            oracle.sql.BLOB blob = ((oracle.jdbc.OracleResultSet)result).getBLOB(columnName);        
            inputStream = blob.getBinaryStream();        
            int bytesRead = 0;        
            
            while((bytesRead = inputStream.read()) != -1) {        
                outputStream.write(bytesRead);    
            }
            
            bytes = outputStream.toByteArray();
            
        } catch(IOException e) {
            throw new SQLException(e.getMessage());
        } finally 
        {
        }
    
        return bytes;
    }

}

