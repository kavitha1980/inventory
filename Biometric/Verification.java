package Biometric;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.digitalpersona.uareu.*;
import com.digitalpersona.uareu.Engine.Candidate;
import com.digitalpersona.uareu.Fmd.Format;

public class Verification 
	extends JPanel
	implements ActionListener
{
	private static final long serialVersionUID = 6;
	private static final String ACT_BACK = "back";
	private static final String ACT_LOAD = "load";
	private static final String ACT_LOAD_FROM_DB="load_from_db";

	private CaptureThread m_capture;
	private Reader  m_reader;
	private Fmd[]   m_fmds;
	private JDialog m_dlgParent;
	public FingerDB db=new FingerDB("localhost","uareu","root","password");
	public List<FingerDB.Record> m_listOfRecords=new ArrayList<FingerDB.Record>();
	public List<Fmd> m_fmdList=new ArrayList<Fmd>();
	public Fmd[] m_fmdArray=null;  //Will hold final array of FMDs to identify against
	private JTextArea m_text;
	public Fmd m_enrollmentFmd;
	private ImagePanel m_imagePanel;
        int iVerifyOK=0;

	private final String m_strPrompt1 = "Verification started\n    put any finger on the reader\n\n";
	private final String m_strPrompt2 = "    put the same or any other finger on the reader\n\n";

        int iEmpCode=0;
	private ReaderCollection m_collection;

     public Verification(int iEmpCode)
     {
          this.iEmpCode= iEmpCode;

          System.out.println(" Before Loading EmpCode");     
          loadData(iEmpCode);

           
      try {
               this.m_collection = UareUGlobal.GetReaderCollection();
               m_collection.GetReaders();
          } catch (UareUException e1) {
               // TODO Auto-generated catch block
               JOptionPane.showMessageDialog(null, "Error getting collection");
               e1.printStackTrace();
               return;
          }
      
          if(m_collection.size()==0)
               {MessageBox.Warning("Reader is not selected"); return;}
      
          m_reader=m_collection.get(0);
      
          if(null == m_reader)
          {
               MessageBox.Warning("Reader is not selected");
          }

		m_fmds = new Fmd[2]; //two FMDs to perform comparison

		final int vgap = 5;
		final int width = 380;
		
		BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
		setLayout(layout);
		
		m_imagePanel=new ImagePanel();
          m_imagePanel.setPreferredSize(new Dimension(100,200));
		add(m_imagePanel);
		
		m_text = new JTextArea(22, 1);
		m_text.setEditable(false);
          JScrollPane paneReader = new JScrollPane(m_text);
          add(paneReader);
		Dimension dm = paneReader.getPreferredSize();
		dm.width = width;
          paneReader.setPreferredSize(dm);
		
		add(Box.createVerticalStrut(vgap));
		
		
		JButton btnBack = new JButton("Back");
		btnBack.setActionCommand(ACT_BACK);
		btnBack.addActionListener(this);
		add(btnBack);
		add(Box.createVerticalStrut(vgap));

          JDialog dlg = new JDialog((JDialog)null, "Verification", true);
          doModal(dlg);
		setOpaque(true);
	}

	public void actionPerformed(ActionEvent e)
     {
		if(e.getActionCommand().equals(ACT_BACK))
          {
			//cancel capture
			StopCaptureThread();
			m_dlgParent.setVisible(false);
		}
		
		else if(e.getActionCommand().equals(CaptureThread.ACT_CAPTURE)){
			//process result
			CaptureThread.CaptureEvent evt = (CaptureThread.CaptureEvent)e;
			if(evt.capture_result.image!=null)
			if(ProcessCaptureResult(evt)){
				//restart capture thread
				WaitForCaptureThread();
				StartCaptureThread();
			}
			else{
				//destroy dialog
				m_dlgParent.setVisible(false);
			}
            else
            {
				WaitForCaptureThread();
				StartCaptureThread();

            }
        }

	}
	
	private void StartCaptureThread(){
		m_capture = new CaptureThread(m_reader, false, Fid.Format.ANSI_381_2004, Reader.ImageProcessing.IMG_PROC_DEFAULT);
		m_capture.start(this);
	}
	
	private void StopCaptureThread(){
		if(null != m_capture) m_capture.cancel();
	}
	
	private void WaitForCaptureThread()
     {
     	if(null != m_capture)
               m_capture.join(1000);

	}
	
	private boolean ProcessCaptureResult(CaptureThread.CaptureEvent evt)
     {
		boolean bCanceled = false;
		
		if(this.m_enrollmentFmd==null && this.m_listOfRecords.size()==0)
		{
               MessageBox.Warning("You cannot verify until you register or load a template."+this.m_listOfRecords.size());
			return !bCanceled;	
		}

		if(null != evt.capture_result)
          {
			if(null != evt.capture_result.image && Reader.CaptureQuality.GOOD == evt.capture_result.quality)
               {
				//extract features
				Engine engine = UareUGlobal.GetEngine();
					
				try{
					m_imagePanel.showImage(evt.capture_result.image);
					Fmd fmd = engine.CreateFmd(evt.capture_result.image, Fmd.Format.DP_VER_FEATURES);
					m_fmds[0] = fmd;
					
                              System.out.println(" Comming here--------");

                              int target_falsematch_rate = Engine.PROBABILITY_ONE / 100000; //target rate is 0.00001
						Candidate [] matches=engine.Identify(m_fmds[0],0,m_fmdArray,target_falsematch_rate,1);

                              if(matches.length==1)
                              {
                                     JOptionPane.showMessageDialog(null, "Match found:" );     //+ this.m_listOfRecords.get(matches[0].fmd_index).userID
                                     iVerifyOK =1;
                                     StopCaptureThread();
                                     m_dlgParent.setVisible(false);
                              }
                              else
                              {
                                     JOptionPane.showMessageDialog(null, "Not Identified!!!");
                                     iVerifyOK =0;

                              }
				}
                         catch(UareUException e)
                         {
                              MessageBox.DpError("Engine.CreateFmd()", e);
                         }
				}
			
				else{
					//the loop continues
					m_text.append(m_strPrompt2);
				}
			}
	
			else if(Reader.CaptureQuality.CANCELED == evt.capture_result.quality)
               {
				//capture or streaming was canceled, just quit

				bCanceled = true;
			}
		
			else if(null != evt.exception){
				//exception during capture
				MessageBox.DpError("Capture", evt.exception);
				bCanceled = true;
			}
			else if(null != evt.reader_status){
				//reader failure
				MessageBox.BadStatus(evt.reader_status);
				bCanceled = true;
			}
		
			else
               {
				//bad quality
				MessageBox.BadQuality(evt.capture_result.quality);
               }     
	
		return !bCanceled;
	}

	private void doModal(JDialog dlgParent){
		//open reader
		try{
			m_reader.Open(Reader.Priority.COOPERATIVE);
		}
		catch(UareUException e){ MessageBox.DpError("Reader.Open()", e); }
		
		//start capture thread
		StartCaptureThread();

		//put initial prompt on the screen
		m_text.append(m_strPrompt1);
		
		//bring up modal dialog
		m_dlgParent = dlgParent;
		m_dlgParent.setContentPane(this);
		m_dlgParent.pack();
		m_dlgParent.setLocationRelativeTo(null);
		m_dlgParent.toFront();
		m_dlgParent.setVisible(true);
		m_dlgParent.dispose();
		
		//cancel capture
		StopCaptureThread();
		
		//wait for capture thread to finish
		WaitForCaptureThread();
		
		//close reader
		try{
			m_reader.Close();
		}
		catch(UareUException e){ MessageBox.DpError("Reader.Close()", e); }
	}
	
	private byte[] loadDataFile() {
		// TODO Auto-generated method stub
		JFileChooser fc=new JFileChooser(new File("test"));
		
		fc.showOpenDialog(this);
		if(fc.getSelectedFile()!=null)
		{
		InputStream input=null;
		try {
			input=new BufferedInputStream(new FileInputStream(fc.getSelectedFile()));
			byte[] data=new byte[input.available()];
			input.read(data,0,input.available());
			input.close();
			return data;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Error saving file.");
		}
		}
		return null;
	}
	
     private void loadData(int iEmpCode)
     {

			try {
				db.Open();
                    System.out.println(" Comming Here---");

                    this.m_listOfRecords=db.GetAllFPData(iEmpCode);

				for(FingerDB.Record record:this.m_listOfRecords)
				{
					Fmd fmd = UareUGlobal.GetImporter().ImportFmd(record.fmdBinary,com.digitalpersona.uareu.Fmd.Format.DP_REG_FEATURES,com.digitalpersona.uareu.Fmd.Format.DP_REG_FEATURES);
					this.m_fmdList.add(fmd);
				}
				m_fmdArray=new Fmd[this.m_fmdList.size()];
				this.m_fmdList.toArray(m_fmdArray);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				MessageBox.DpError("Failed to load FMDs from database.  Please check connection string in code.",null);			
				return;
			} catch (UareUException e1) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null, "Error importing fmd data.");
				return;
			}
			
			this.m_enrollmentFmd=null;

     }
     public int getVerified()
     {
          return iVerifyOK;
     }
}
