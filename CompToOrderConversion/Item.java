package CompToOrderConversion;

import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class Item

{

     String SItemCode;
     HashMap theMap;
     public Item(String SItemCode)
     {

          this.SItemCode = SItemCode;

          theMap = new HashMap();

     }
     public void setOrderInfoDetails(String SSupCode,

                                      String SPayTerms,String SToCode,String SThroCode,

                                      String SPortCode,String SState,String SPayDays,

                                      String STaxCliamable,String SReference,String SCatl,String SDraw,String SPrevOrderNo,String SPrevOrderBlock)

     {

          theMap.put("SupCode"     ,SSupCode);

          theMap.put("PayTerms"    ,SPayTerms);

          theMap.put("ToCode"      ,SToCode);

          theMap.put("ThroCode"    ,SThroCode);

          theMap.put("PortCode"    ,SPortCode);

          theMap.put("State"       ,SState);

          theMap.put("PayDays"     ,SPayDays);

          theMap.put("TaxCliamable",STaxCliamable);

          theMap.put("Reference"   ,SReference);

          theMap.put("Catl"        ,SCatl);

          theMap.put("Draw"        ,SDraw);

          theMap.put("PrevOrderNo" ,SPrevOrderNo);

          theMap.put("PrevOrderBlock",SPrevOrderBlock);

     }
     public void setOrderInfoDetails(String SSupCode,

                                      String SPayTerms,String SToCode,String SThroCode,

                                      String SPortCode,String SState,String SPayDays,

                                      String STaxCliamable,String SReference,String SCatl,String SDraw,String SPrevOrderNo,String SPrevOrderBlock,String SMake)

     {

          theMap.put("SupCode"     ,SSupCode);

          theMap.put("PayTerms"    ,SPayTerms);

          theMap.put("ToCode"      ,SToCode);

          theMap.put("ThroCode"    ,SThroCode);

          theMap.put("PortCode"    ,SPortCode);

          theMap.put("State"       ,SState);

          theMap.put("PayDays"     ,SPayDays);

          theMap.put("TaxCliamable",STaxCliamable);

          theMap.put("Reference"   ,SReference);

          theMap.put("Catl"        ,SCatl);

          theMap.put("Draw"        ,SDraw);

          theMap.put("PrevOrderNo" ,SPrevOrderNo);

          theMap.put("PrevOrderBlock",SPrevOrderBlock);
	  
          theMap.put("Make",SMake);
     }
     public void setOrderInfoDetails(String SSupCode,

                                      String SPayTerms,String SToCode,String SThroCode,

                                      String SPortCode,String SState,String SPayDays,

                                      String STaxCliamable,String SReference,String SCatl,String SDraw,String SPrevOrderNo,String SPrevOrderBlock,String SMake,String SDocId)

     {

          theMap.put("SupCode"     ,SSupCode);

          theMap.put("PayTerms"    ,SPayTerms);

          theMap.put("ToCode"      ,SToCode);

          theMap.put("ThroCode"    ,SThroCode);

          theMap.put("PortCode"    ,SPortCode);

          theMap.put("State"       ,SState);

          theMap.put("PayDays"     ,SPayDays);

          theMap.put("TaxCliamable",STaxCliamable);

          theMap.put("Reference"   ,SReference);

          theMap.put("Catl"        ,SCatl);

          theMap.put("Draw"        ,SDraw);

          theMap.put("PrevOrderNo" ,SPrevOrderNo);

          theMap.put("PrevOrderBlock",SPrevOrderBlock);
	  
          theMap.put("Make",SMake);
	
	  theMap.put("DocId",SDocId);     }
     public void setOrderValueDetails(double dRate,double dDiscPer,double dCenvatPer,

                                      double dTaxPer,double dSurPer,double dPlus,

                                      double dLess,double dMisc)

     {

          theMap.put("Rate"        ,String.valueOf(dRate));

          theMap.put("DiscPer"     ,String.valueOf(dDiscPer));

          theMap.put("CenvatPer"   ,String.valueOf(dCenvatPer));

          theMap.put("TaxPer"      ,String.valueOf(dTaxPer));

          theMap.put("SurPer"      ,String.valueOf(dSurPer));

          theMap.put("Plus"        ,String.valueOf(dPlus));

          theMap.put("Less"        ,String.valueOf(dLess));

          theMap.put("Misc"        ,String.valueOf(dMisc));

     }
     public void setOrderValueDetails(double dRate,double dDiscPer,double dCenvatPer,
                                      double dTaxPer,double dSurPer,double dPlus,
                                      double dLess,double dMisc,double dCGstPer,double dCGstVal,
		
		          double dSGstPer,double dSGstVal,double dIGstPer,double dIGstVal,
		          double dCessPer,double dCessVal)

     {

          theMap.put("Rate"        ,String.valueOf(dRate));

          theMap.put("DiscPer"     ,String.valueOf(dDiscPer));

          theMap.put("CenvatPer"   ,String.valueOf(dCenvatPer));

          theMap.put("TaxPer"      ,String.valueOf(dTaxPer));

          theMap.put("SurPer"      ,String.valueOf(dSurPer));

          theMap.put("Plus"        ,String.valueOf(dPlus));

          theMap.put("Less"        ,String.valueOf(dLess));

          theMap.put("Misc"        ,String.valueOf(dMisc));

          theMap.put("CGstPer"  ,String.valueOf(dCGstPer));		
          theMap.put("CGstVal"  ,String.valueOf(dCGstVal));

          theMap.put("SGstPer"  ,String.valueOf(dSGstPer));		
          theMap.put("SGstVal"  ,String.valueOf(dSGstVal));

          theMap.put("IGstPer"  ,String.valueOf(dIGstPer));		
          theMap.put("IGstVal"  ,String.valueOf(dIGstVal));

          theMap.put("CessPer"  ,String.valueOf(dCessPer));		
          theMap.put("CessVal"  ,String.valueOf(dCessVal));

     }

     public void setMrsDetails(String SMrsNo,String SMrsDate,String SUnitCode,

                              String SDeptCode,String SGroupCode,String SMrsSlNo,String SQty,String SMrsAuthUserCode,String SRemarks)

     {

          theMap.put("MrsNo"       ,  SMrsNo);

          theMap.put("MrsDate"     ,  SMrsDate);

          theMap.put("UnitCode"    ,  SUnitCode);

          theMap.put("DeptCode"    ,  SDeptCode);

          theMap.put("GroupCode"   ,  SGroupCode);

          theMap.put("MrsSlNo"     ,  SMrsSlNo);

          theMap.put("Qty"         ,  SQty);

          theMap.put("MrsAuthUserCode",SMrsAuthUserCode);

          theMap.put("Remarks"     ,SRemarks);
     }
     public void setMrsDetails(String SMrsNo,String SMrsDate,String SUnitCode,

                              String SDeptCode,String SGroupCode,String SMrsSlNo,String SQty,String SMrsAuthUserCode,String SRemarks,int iJmdApproveStatus,int iApprovalUserCode)

     {

          theMap.put("MrsNo"       ,  SMrsNo);

          theMap.put("MrsDate"     ,  SMrsDate);

          theMap.put("UnitCode"    ,  SUnitCode);

          theMap.put("DeptCode"    ,  SDeptCode);

          theMap.put("GroupCode"   ,  SGroupCode);

          theMap.put("MrsSlNo"     ,  SMrsSlNo);

          theMap.put("Qty"         ,  SQty);

          theMap.put("MrsAuthUserCode",SMrsAuthUserCode);

          theMap.put("Remarks"     ,SRemarks);

          theMap.put("JmdApproveStatus",String.valueOf(iJmdApproveStatus));
	

	  theMap.put("ApprovalUserCode",String.valueOf(iApprovalUserCode));

     }

     public void setMrsDetails(String SMrsNo,String SMrsDate,String SUnitCode,

                              String SDeptCode,String SGroupCode,String SMrsSlNo,String SQty,String SMrsAuthUserCode,String SRemarks,int iJmdApproveStatus,int iApprovalUserCode,String SPOGroupNo)

     {

          theMap.put("MrsNo"       ,  SMrsNo);

          theMap.put("MrsDate"     ,  SMrsDate);

          theMap.put("UnitCode"    ,  SUnitCode);

          theMap.put("DeptCode"    ,  SDeptCode);

          theMap.put("GroupCode"   ,  SGroupCode);

          theMap.put("MrsSlNo"     ,  SMrsSlNo);

          theMap.put("Qty"         ,  SQty);

          theMap.put("MrsAuthUserCode",SMrsAuthUserCode);

          theMap.put("Remarks"     ,SRemarks);

          theMap.put("JmdApproveStatus",String.valueOf(iJmdApproveStatus));
	

	  theMap.put("ApprovalUserCode",String.valueOf(iApprovalUserCode));
           
          theMap.put("POGroupNo",SPOGroupNo);   

     }

     public String getMrsNo()

     {

          return (String)theMap.get("MrsNo");

     }

     public String getMrsRemarks()

     {

          return (String)theMap.get("Remarks");

     }

     public String getDocumentId()
     {
	  return (String)theMap.get("DocId");		
     }

     public String getMrsSlNo()

     {

          return (String)theMap.get("MrsSlNo");

     }

     public String getUnitCode()

     {

          return (String)theMap.get("UnitCode");

     }

     public String getDeptCode()

     {

          return (String)theMap.get("DeptCode");

     }

     public String getGroupCode()

     {

          return (String)theMap.get("GroupCode");

     }
     public String getMrsDate()

     {

          return (String)theMap.get("MrsDate");

     }

     public String getMrsAuthUserCode()

     {

          return (String)theMap.get("MrsAuthUserCode");

     }

     public String getPrevOrderNo()

     {

          return (String)theMap.get("PrevOrderNo");

     }

     public String getPrevOrderBlock()

     {

          return (String)theMap.get("PrevOrderBlock");

     }

     public String getSupCode()

     {

          return (String)theMap.get("SupCode");

     }
     public String getPayTerms()

     {

          return (String)theMap.get("PayTerms");

     }

     public String getPayDays()

     {

          return (String)theMap.get("PayDays");

     }

     public String getToCode()

     {

          return (String)theMap.get("ToCode");

     }

     public String getThroCode()

     {

          return (String)theMap.get("ThroCode");

     }

     public String getPortCode()

     {

          return (String)theMap.get("PortCode");

     }

     public String getState()

     {

          return (String)theMap.get("State");

     }

     public String getTaxClaimable()

     {

          return (String)theMap.get("TaxClaimable");

     }

     public String getQty()

     {

          return (String)theMap.get("Qty");

     }

     public String getRate()

     {

          return (String)theMap.get("Rate");

     }

     public String getDiscPer()

     {

          return (String)theMap.get("DiscPer");

     }

                      

     public String  getDiscount()

     {

          double dDiscount =0;
          dDiscount = getGross()*(Double.parseDouble((String)theMap.get("DiscPer"))/100);
          return getRound(dDiscount,2);

     }
     private double getGross()

     {

          return Double.parseDouble((String)theMap.get("Qty"))*Double.parseDouble((String)theMap.get("Rate"));

     }

     public String getCenvatPer()

     {

          return (String)theMap.get("CenvatPer");

     }

     public String getCenvat()
     {
          double dCenvat =0;
          dCenvat = (getGross()-Double.parseDouble(getDiscount()))*(Double.parseDouble((String)theMap.get("CenvatPer"))/100);
          return getRound(dCenvat,2);

     }

     public String getTaxPer()

     {

          return (String)theMap.get("TaxPer");

     }
     public String getCGst()

     {

          double dTax =0;
          dTax = (getGross()-Double.parseDouble(getDiscount())+Double.parseDouble(getCenvat()))*(Double.parseDouble((String)theMap.get("CGstPer"))/100);

          return getRound(dTax,2);
     }
     public String getSGst()
     {

          double dTax =0;
          dTax = (getGross()-Double.parseDouble(getDiscount())+Double.parseDouble(getCenvat()))*(Double.parseDouble((String)theMap.get("SGstPer"))/100);

          return getRound(dTax,2);
     }
     public String getIGst()
     {

          double dTax =0;
          dTax = (getGross()-Double.parseDouble(getDiscount())+Double.parseDouble(getCenvat()))*(Double.parseDouble((String)theMap.get("IGstPer"))/100);

          return getRound(dTax,2);
     }

     public String  getCess()
     {

          double dTax =0;
          dTax = (getGross()-Double.parseDouble(getDiscount())+Double.parseDouble(getCenvat()))*(Double.parseDouble((String)theMap.get("CessPer"))/100);

          return getRound(dTax,2);
     }

     public String getTax()

     {

          double dTax =0;
//          dTax = (getGross()-Double.parseDouble(getDiscount())+Double.parseDouble(getCenvat()))*(Double.parseDouble((String)theMap.get("TaxPer"))/100);

          dTax =  Double.parseDouble(getCGst())+Double.parseDouble(getSGst())+Double.parseDouble(getIGst())+Double.parseDouble(getCess());

          return getRound(dTax,2);
     }

     public String getSurPer()

     {

          return (String)theMap.get("SurPer");

     }
     public String getSur()

     {

          double dSur =0;
          dSur = Double.parseDouble(getTax())*(Double.parseDouble((String)theMap.get("SurPer"))/100);
          return getRound(dSur,2);
     }

     public String getNet()

     {

          double dNet=0;
          dNet = getGross()-Double.parseDouble(getDiscount())+Double.parseDouble(getCenvat())+Double.parseDouble(getTax())+Double.parseDouble(getSur());
          return getRound(dNet,2);

     }

     public String getPlus()

     {

          return    (String)theMap.get("Plus");

     }

     public String getLess()

     {

          return    (String)theMap.get("Less");

     }

     public String getMisc()

     {

          return    (String)theMap.get("Misc");

     }

     public String getReference()

     {

          return (String)theMap.get("Reference");

     }

     public String getCatl()

     {

          return (String)theMap.get("Catl");

     }
 
     public String getJmdApproveStatus()
     {
	return (String)theMap.get("JmdApproveStatus");
     }
     public String getPOGroupNo()
     {
	return (String)theMap.get("POGroupNo");
     }
 
     public String getApprovalUserCode()
     {
     	return (String)theMap.get("ApprovalUserCode");
     }
     public String getMake()

     {

          return (String)theMap.get("Make");

     }
     public String getDraw()

     {

          return (String)theMap.get("Draw");

     }

     public String getRound(double dAmount,int iScale)

     {

          String str="0";

          try

          {

               java.math.BigDecimal bd1 = new java.math.BigDecimal(String.valueOf(dAmount));

               java.math.BigDecimal bd2 = bd1.setScale(iScale,4);

               str=bd2.toString();

          }

          catch(Exception ex)

          {

          }

          return str;

      }
}
