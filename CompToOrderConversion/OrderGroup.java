package CompToOrderConversion;

import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class OrderGroup

{

     String SSupCode,SSupName,SMailId;

     ArrayList itemList;

     public OrderGroup(String SSupCode)

     {

          this.SSupCode = SSupCode;

          itemList      = new ArrayList();

     }
     public OrderGroup(String SSupCode,String SSupName,String SMailId)
     {

          this.SSupCode = SSupCode;
          this.SSupName = SSupName;
          this.SMailId  = SMailId;


          itemList      = new ArrayList();
     }


     public void appendItem(Item item)

     {

          itemList.add(item);

     }

     public ArrayList getItems()

     {

          return itemList;

     }



}
