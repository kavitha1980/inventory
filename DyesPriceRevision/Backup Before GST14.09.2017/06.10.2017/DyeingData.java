
package DyesPriceRevision;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import java.util.ArrayList;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.HashMap;
import java.sql.*;
import java.io.*;


public class DyeingData {
        Connection              theConnection       = null;    
        Connection              theDyeConnection    = null;    
        Common                  common              = new Common();    
        Vector                  VDyesCode,VDyesName,VDyesConsumption,VSupCode,VSupName,VStatus,VStatusCode,
                                VBaseCode,VBaseName,VExDyesCode,VExDyesName,VDyeSupCode,VDyeSupName,VShadeBaseCode,
                                VShadeBaseName,  VShadewiseDyesCode,VShadewiseDyesName;
        
        ArrayList               ADyesDetailList,ADyesCostList;

public DyeingData(){
  //   setDyes();
    setDyesSupplier();
   setDyesSupplierDyeing();
   // setLabStatus();
    setFibreBase();
    setFibreShade();
 //  setDyesName();
}
 public void setDyes(){
    try{
        VDyesCode           = new Vector();
        VDyesName           = new Vector();
        VDyesConsumption    = new Vector();
        String sServerDate  = common.getServerDate();
        String sStDay       ="01";
        String sStMonth     ="04";
        String sCurYear     = sServerDate.substring(0, 4);
        int iStYear         = common.toInt(sCurYear)-1;
        String sStDate      = String.valueOf(iStYear)+sStMonth+sStDay;
        //System.out.println("Server Date========>"+sServerDate+"---->"+"---->"+sStDate);
        
        
        StringBuffer sb     = new StringBuffer(); 
        // 09.06.2017
 /*       sb.append("  Select Item_Code, Item_Name from InvItems ");
        sb.append("  Inner Join STOCKGROUP on StockGroup.GroupCode = InvItems.StkGroupCode ");
        sb.append("  Inner Join ISSUE@amarml  on Issue.Code = InvItems.Item_Code ");
        sb.append("  Where StockGroup.GroupCode='C01'");
     //   sb.append("  Where StockGroup.GroupCode='C01' and IssueDate>="+sStDate+" and IssueDate<="+sServerDate);
        sb.append("  Group by Item_Code, Item_Name ");
        sb.append("  Union All ");
        sb.append("  Select Dyes_InvItems.Dyes_Item_Code, Dyes_InvItems.Dyes_Item_Name from Dyes_InvItems ");
        sb.append("  Order by 2");
        
        System.out.println("Dyes Qry===> "+sb.toString());
   */
        
        
        sb.append("  Select DyeingInvItems.Item_Code, Item_Name from DyeingInvItems ");
        sb.append("  Inner Join InvItems on InvItems.Item_Code = DyeingInvItems.Item_Code ");
        sb.append("  Union All ");
        sb.append("  Select Dyes_InvItems.Dyes_Item_Code, Dyes_InvItems.Dyes_Item_Name from Dyes_InvItems@amardye2 ");
        sb.append("  Order by 2");
        

        if (theConnection==null){
                  JDBCConnection jdbc    = JDBCConnection.getJDBCConnection();
                  theConnection          = jdbc.getConnection();
              }
        PreparedStatement ps             = theConnection.prepareStatement(sb.toString());
   //     System.out.println("Set Dyes==>"+sb.toString());
       /* if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        */
        
        ResultSet rst                 = ps.executeQuery();
           // VDyesName.add("");
       while (rst.next()){
              VDyesCode              . add(rst.getString(1));
              VDyesName              . add(rst.getString(2).trim());
            //  VDyesConsumption       . add(rst.getString(3));
         }
            rst.close();
            ps.close();
            ps=null;
         //   theDyeConnection.close();
            
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("Dyes:"+e);
    }
}
public String getDyesCode(String SDyesName){
          int iIndex = VDyesName.indexOf(SDyesName.trim());
          System.out.println("InsideMethod==>"+SDyesName+"--"+VDyesCode.size()+"<---->"+iIndex);
          return (String)VDyesCode.elementAt(iIndex);
 }

public String getDyesName(String SDyesCode){
         int index      = VDyesCode.indexOf(SDyesCode);
         return String.valueOf(VDyesName.elementAt(index));
 }

public void setShadewiseDyes(String sShadeCode){
    try{
        VShadewiseDyesCode  = new Vector();
        VShadewiseDyesName  = new Vector();
        
        
        StringBuffer sb     = new StringBuffer(); 
        
        sb.append("  Select Dyes_Code as DyesCode,Item_Name as ItemName from ShadewiseDyes ");
        sb.append("  Inner Join InvItems on ShadewiseDyes.Dyes_Code = InvItems.Item_Code ");
        sb.append("  Where Shade_Code='"+sShadeCode+"'");
        sb.append("  Union All ");
        sb.append("  Select Dyes_Code as DyesCode ,Dyes_Item_Name as ItemName from ShadewiseDyes ");
        sb.append("  Inner Join Dyes_InvItems on ShadewiseDyes.Dyes_Code = Dyes_InvItems.Dyes_Item_Code ");
        sb.append("  Where Shade_Code='"+sShadeCode+"'");
        sb.append("  Order by 2");
        
  //      System.out.println("Dyes Qry===> "+sb.toString());
        

        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
           // VDyesName.add("");
       while (rst.next()){
              VShadewiseDyesCode     . add(rst.getString(1));
              VShadewiseDyesName     . add(rst.getString(2));
         }
            rst.close();
            ps.close();
            ps=null;
         //   theDyeConnection.close();
            
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("Dyes:"+e);
    }
}
public String getShadewiseDyesCode(String SDyesName){
          int iIndex = VShadewiseDyesName.indexOf(SDyesName);
          return (String)VShadewiseDyesCode.elementAt(iIndex);
 }

public String getShadewiseDyesName(String SDyesCode){
         int index      = VShadewiseDyesCode.indexOf(SDyesCode);
         return String.valueOf(VShadewiseDyesName.elementAt(index));
 }

public void setLabStatus(){
    try{
        VStatusCode           = new Vector();
        VStatus               = new Vector();
        
        
        StringBuffer sb     = new StringBuffer(); 
        
        sb.append("  Select StatusCode,Status from DyesLabStatus ");
        sb.append("  Order by 1");
     //   System.out.println("Qry===> "+sb.toString());
        

        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
        
       while (rst.next()){
              VStatusCode            . add(rst.getString(1));
              VStatus                . add(rst.getString(2));
       }
            rst.close();
            ps.close();
            ps=null;
          //  theDyeConnection.close();
            
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("Dyes:"+e);
    }
}
public String getStatusCode(String SStatus){
          int iIndex = VStatus.indexOf(SStatus);
          return (String)VStatusCode.elementAt(iIndex);
 }

public String getStatus(String SStatusCode){
         int index      = VStatusCode.indexOf(SStatusCode);
         return String.valueOf(VStatus.elementAt(index));
 }



public String getAnnualConsumption(String sDyesCode,int iZeroStatus){
    String sDyesConsumption="";
  //  System.out.println("ZeroStatus inside Qry===>"+iZeroStatus);
    try{
        
        String sServerDate  = common.getServerDate();
        String sCurDate     = sServerDate.substring(6,8);
        String sCurMonth    = sServerDate.substring(4,6);
        String sCurYear     = sServerDate.substring(0, 4);

        int iStYear         = common.toInt(sCurYear)-1;
        String sStDate      = String.valueOf(iStYear)+sCurMonth+sCurDate;
        
        
        StringBuffer sb     = new StringBuffer(); 
        
        sb.append("  Select nvl(sum(Qty),0) from InvItems ");
        sb.append("  Inner Join STOCKGROUP on StockGroup.GroupCode = InvItems.StkGroupCode ");
        sb.append("  Inner Join ISSUE@amarml  on Issue.Code = InvItems.Item_Code ");
        sb.append("  Where StockGroup.GroupCode='C01' ");
        if(iZeroStatus==0)
        sb.append("  and IssueDate>="+sStDate+" and IssueDate<="+sServerDate);
        sb.append("  and Item_Code='"+sDyesCode+"'");
        
        //sb.append("  Group by Item_Code, Item_Name ");
        
   //     System.out.println("get Annual Consumption Qry===> "+sb.toString());
        if (theDyeConnection==null){
                  JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();

       while (rst.next()){
              sDyesConsumption        = rst.getString(1);
         }
            rst.close();
            ps.close();
            ps=null;
           // theDyeConnection.close();
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("Dyes:"+e);
    }
 //   System.out.println("Dyes Consumption in Qry===>"+sDyesConsumption);
    return sDyesConsumption;
}

/*public String getAnnualConsumption (String sDyesCode){
        int index       = VDyesCode.indexOf(sDyesCode);
        return  String.valueOf(VDyesConsumption.elementAt(index));
}*/

 public void setDyesSupplier(){
    try{
        VSupCode            = new Vector();
        VSupName            = new Vector();
        
        StringBuffer sb     = new StringBuffer(); 
        
        sb.append("  Select t.Sup_Code,Supplier.Name ");
        sb.append("  from ");
        sb.append("  (select  distinct PurchaseOrder.Sup_Code from PurchaseOrder @amarml");
        sb.append("  Inner join InvItems on InvItems.Item_Code=PurchaseOrder.Item_Code ");
        sb.append("  and StkGroupCode='C01' ) t ");
        sb.append("  Inner join supplier @amarml on Supplier.Ac_Code=t.Sup_Code ");
        sb.append("  Union All ");
        sb.append("  Select Dyes_Party_Code,Dyes_Party_Name from  Dyes_Party ");
        sb.append("  order by 2 ");

        
    //    System.out.println("Supplier Qry===> "+sb.toString());
        

        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
    
       while (rst.next()){
              VSupCode               . add(rst.getString(1));
              VSupName               . add(rst.getString(2));
         }
            rst.close();
            ps.close();
            ps=null;
         //   theDyeConnection.close();
            
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("Supplier"+e);
    }
}
 
 public void setDyesSupplierDyeing(){
    try{
        VDyeSupCode         = new Vector();
        VDyeSupName         = new Vector();
        
        StringBuffer sb     = new StringBuffer(); 
        
        sb.append("  Select t.Sup_Code,Supplier.Name ");
        sb.append("  from ");
        sb.append("  (select  distinct PurchaseOrder.Sup_Code from PurchaseOrder@amarml ");
        sb.append("  Inner join InvItems on InvItems.Item_Code=PurchaseOrder.Item_Code ");
        sb.append("  and StkGroupCode='C01' ) t ");
        sb.append("  Inner join supplier@amarml on Supplier.Ac_Code=t.Sup_Code ");
        sb.append("  Union All ");
        sb.append("  Select Dyes_Party_Code,Dyes_Party_Name from  Dyes_Party ");
        sb.append("  order by 2 ");

        
  //     System.out.println("Supplier Qry===> "+sb.toString());
        

        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
    
       while (rst.next()){
              VDyeSupCode             . add(rst.getString(1));
              VDyeSupName             . add(rst.getString(2));
         }
            rst.close();
            ps.close();
            ps=null;
         //   theDyeConnection.close();
            
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("Supplier"+e);
    }
}
 
public int getSupplierCode(String SSupplierName){
          int iIndex = VSupName.indexOf(SSupplierName);
          return common.toInt((String)VSupCode.elementAt(iIndex));
 }
public int getDyeSupplierCode(String SSupplierName){
          int iIndex = VDyeSupName.indexOf(SSupplierName);
          return common.toInt((String)VDyeSupCode.elementAt(iIndex));
 }

 
//public int getInsertData (String sDyesCode,String sAnnualConsumption,String sRateperKg,String sDiscount,String sTax,String sDiscountper,String sTaxper,String sNetValue,String sTotalValue,int iSupplierCode,String sSysName,String sLabStatusCode){
public int getInsertData (String sDyesCode,String sAnnualConsumption,String sRateperKg,String sDiscount,String sTax,String sDiscountper,String sTaxper,String sNetValue,String sTotalValue,int iSupplierCode,String sSysName,String sPriceRevisionDate,String sCGSTPer,String sCGST,String sSGSTPer,String sSGST,String sIGSTPer,String sIGST){
     int isave=0;
     
    if(isave!=2){
     try{
    
         StringBuffer sb             = new StringBuffer(); 
         
         
         sb.append(" insert into DyesPriceRevision ( ID ,Dyes_Code ,AnnualConsumption ,RateperKg ,Discper,Taxper ,Discount,Tax,NetValue,TotalValue,");
         sb.append(" WithEffectFrom ,WithEffectTo ,EntrySysName ,EntryDateTime,Supp_Code,CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,  ");
         sb.append(" IGSTPer,IGSTAmount )");
         sb.append(" Values ( DyesPrice_ID_Seq.nextVal,'"+sDyesCode+"','"+sAnnualConsumption+"',"+sRateperKg+","+sDiscountper+","+sTaxper);
         sb.append(" ,"+sDiscount+","+sTax+","+sNetValue+","+sTotalValue+", "+sPriceRevisionDate+",99999999,'"+sSysName+"',to_Char(sysdate,'YYYYMMDD HH24:Mi:ss'),"+iSupplierCode+",");
         sb.append(" "+sCGSTPer+","+sCGST+","+sSGSTPer+","+sSGST+","+sIGSTPer+","+sIGST+" ) ");
         
         //sb.append(" ,"+sDiscount+","+sTax+","+sNetValue+","+sTotalValue+", to_Char(sysdate,'YYYYMMDD'),99999999,'"+sSysName+"',to_Char(sysdate,'YYYYMMDD HH24:Mi:ss'),"+iSupplierCode+")");
         
         
         
    //   System.out.println("Insert Qry:"+sb.toString());
         
         if (theDyeConnection==null){
                  DyeJDBCConnection jdbc= DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection=jdbc.getConnection();
              }
        PreparedStatement ps=theDyeConnection.prepareStatement(sb.toString());

            ps . executeUpdate();
            ps.close();
            ps=null;
      }  
   
      catch(Exception e)
      {
          isave=1;
          e.printStackTrace();
          System.out.println("InsertMethod"+e);
       }
     }
   
 return isave;
}

public void setDyesDetails(String sSupplierName){
    try{
        ADyesDetailList         = new ArrayList();
        StringBuffer sb         = new StringBuffer(); 
        
        sb.append("   Select Supplier.Name,Item_Code,AnnualConsumption,RatePerKg,Discount,Tax,NetValue,TotalValue, ");
        sb.append("   WithEffectFrom, WithEffectTo,item_Name as ItemName,Discper,Taxper,DyesLabStatus.Status ,");
        sb.append("   CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,IGSTAmount from DyesPriceRevision ");
        sb.append("   Inner Join InvItems on InvItems.Item_Code = DyesPriceRevision.Dyes_Code ");
        sb.append("   Inner Join Supplier@amarml on Supplier.AC_Code= DyesPriceRevision.Supp_Code ");
        sb.append("   Left Join DyesLabStatus on DyesLabStatus.StatusCode= DyesPriceRevision.LabStatus ");
        sb.append("   Where  Supplier.Name='"+sSupplierName+"' and (PriceRevisionStatus=0  OR PriceRevisionStatus is null)");
        
            //and to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') <=20170502 and 
      //to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') >=20170501
        
        sb.append("   Union All ");
        
        sb.append("   Select Supplier.Name,Dyes_Item_Code,AnnualConsumption,RatePerKg,Discount,Tax,NetValue,TotalValue, ");
        sb.append("   WithEffectFrom, WithEffectTo,Dyes_item_Name as ItemName,Discper,Taxper,DyesLabStatus.Status, ");
        sb.append("   CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,IGSTAmount from DyesPriceRevision ");
        sb.append("   Inner Join Dyes_InvItems on Dyes_InvItems.Dyes_Item_Code = DyesPriceRevision.Dyes_Code");
        sb.append("   Inner Join Supplier@amarml on Supplier.AC_Code= DyesPriceRevision.Supp_Code ");
        sb.append("   Left Join DyesLabStatus on DyesLabStatus.StatusCode= DyesPriceRevision.LabStatus ");
        sb.append("   Where  Supplier.Name='"+sSupplierName+"' and (PriceRevisionStatus=0  OR PriceRevisionStatus is null)");
        
        
    //    System.out.println("DyesDetails--->"+sb.toString());
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection= jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
        ResultSetMetaData rsmd        = rst.getMetaData();
        
       while (rst.next()){
              HashMap row = new HashMap();
            for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    row.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
            
                ADyesDetailList.add(row);
          //      System.out.println("Inside Qry===>"+ADyesDetailList.size());
         }
            rst.close();
            ps.close();
            ps=null;
            
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("OrderType:"+e);
    }
}

public int updatePriceRevisionStatus (int iPartyCode, String sDyesCode,int iPriceRevisionSatus){
    int iSaveValue=-1;
  try{
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" Update DyesPriceRevision  set PriceRevisionStatus=1,WithEffectTo=to_Char(sysdate,'YYYYMMDD')");
         sb.append(" Where Supp_Code='"+iPartyCode+"' and Dyes_Code='"+sDyesCode+"'");
         sb.append(" and ID=(Select Max(ID) from DyesPriceRevision Where Supp_Code='"+iPartyCode+"' and Dyes_Code='"+sDyesCode+"')");
         //sb.append(" and PriceRevisionStatus="+iPriceRevisionSatus);
        
    //     System.out.println("Update Qry:"+sb.toString());
         
           if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
        
       ps . executeUpdate();
       iSaveValue=0;
       }  

      catch(Exception e)
      {
         try{
          e.printStackTrace();
          iSaveValue=1;
  //        theProcessConnection.rollback();
          System.out.println("InsertMethod"+e);
          }
          catch(Exception ex){
              ex.printStackTrace();
          }
       }
 //   System.out.println("update Rt Value--->"+iSaveValue);
  return iSaveValue;
  
}


public int getMaxPriceRevisionStatus(int iSupplierCode,String sDyesCode){
    int         iStatus=-1;
    try{
        StringBuffer sb         = new StringBuffer(); 
        
        sb.append("   Select Max(ID) from DyesPriceRevision  ");
        sb.append("   Where Supp_Code="+iSupplierCode+" and Dyes_Code='"+sDyesCode+"'");
        
      //  System.out.println("MaxPriceStatus--->"+sb.toString());
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
 
        ResultSetMetaData rsmd        = rst.getMetaData();
        
       while (rst.next()){
              iStatus                 = rst.getInt(1);
         }
            rst.close();
            ps.close();
            ps=null;
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("OrderType:"+e);
    }
    return iStatus;
 }

public int getDyesCount (int iSupplierCode,String sDyesCode){
    int         iCount=-1;
    try{
        StringBuffer sb         = new StringBuffer(); 
        
        sb.append("   Select count(1) from DyesPriceRevision  ");
        sb.append("   Where Supp_Code="+iSupplierCode+" and Dyes_Code='"+sDyesCode+"'");
        
       // System.out.println("MaxPriceStatus--->"+sb.toString());
        
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
        ResultSetMetaData rsmd        = rst.getMetaData();
        
       while (rst.next()){
              iCount                 = rst.getInt(1);
         }
            rst.close();
            ps.close();
            ps=null;
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("OrderType:"+e);
    }
    return iCount;
 }



//public int updateDyesData (int iSupplierCode, String sDyesCode,String sRateperKg,String sDisper,String sTaxper,String sDiscount,String sTax,String sNetValue,String sTotalValue,String sStatus){
public int updateDyesData (int iSupplierCode, String sDyesCode,String sRateperKg,String sDisper,String sTaxper,String sDiscount,String sTax,String sNetValue,String sTotalValue,String sCGSTPer,String sCGST,String sSGSTPer,String sSGST,String sIGSTPer,String sIGST){
    int iSaveValue=-1;
  try{
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" Update DyesPriceRevision  set RateperKg="+sRateperKg+", Discper="+sDisper+",Taxper="+sTaxper+",");
         sb.append(" Discount="+sDiscount+",Tax="+sTax+",NetValue="+sNetValue+",TotalValue ="+sTotalValue);
         sb.append(" ,CGSTPer="+sCGSTPer+",CGSTAmount="+sCGST+",SGSTPer="+sSGSTPer+",SGSTAmount="+sSGST+",IGSTPer="+sIGSTPer);
         sb.append(" ,IGST="+sIGST );
         //sb.append(" , LabStatus="+sStatus);
         sb.append(" Where Supp_Code="+iSupplierCode+" and Dyes_Code='"+sDyesCode+"'");
         sb.append(" and PriceRevisionStatus=0");
        
         System.out.println("Update Qry:"+sb.toString());
         
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        
       ps . executeUpdate();
       iSaveValue=0;
       }  

      catch(Exception e)
      {
         try{
          e.printStackTrace();
          iSaveValue=1;
  //        theProcessConnection.rollback();
       //   System.out.println("InsertMethod"+e);
          }
          catch(Exception ex){
              ex.printStackTrace();
          }
       }
 //   System.out.println("update Rt Value--->"+iSaveValue);
  return iSaveValue;
  
}

public String getDyesSequenceValue (){
    String sSequence="";
    try{
        StringBuffer sb         = new StringBuffer(); 
        
        sb.append("   Select Dyes_InvItemCode_Seq.nextVal from dual");
        
    //    System.out.println(" Sequence-->"+sb.toString());
        
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
        
        
        ResultSetMetaData rsmd        = rst.getMetaData();
        
       while (rst.next()){
              sSequence               = rst.getString(1);
         }
            rst.close();
            ps.close();
            ps=null;
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("Sequence:"+e);
    }
    return sSequence;
 }



public int getInsertDyesInvItems (String sDyesName, String sInvItemSeq){
     int isave=0; 
     boolean bs  = isNewDyesExist(sDyesName);
     if (bs){
          isave=2;
      }
    if(isave!=2){
     try{
         
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" insert into Dyes_InvItems ( ID ,Dyes_Item_Code ,Dyes_Item_Name )");
         sb.append(" Values ( Dyes_InvID_Seq.nextVal,'"+sInvItemSeq+"','"+sDyesName+"' )");
         
   //    System.out.println("Insert Dyes:"+sb.toString());
         
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();


            ps . executeUpdate();
            ps.close();
            ps=null;
      }  
   
      catch(Exception e)
      {
          isave=1;
          e.printStackTrace();
          System.out.println("InsertMethod"+e);
       }
     }
   
 return isave;
}

public boolean isNewDyesExist(String sDyesName){

     boolean result=false;
  try{
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" Select Count(1) from Dyes_InvItems  Where Dyes_Item_Name='"+sDyesName+"'");
         
   //      System.out.println("Exist Qry"+sb.toString());
         
         if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        
        ResultSet rst=ps.executeQuery();
         while(rst.next()) {
            int iCount          = rst.getInt(1);
              if(iCount>0){      
                 result=true;
                 break;
             }   
           else{
                 result=false;
             }
      }
            rst.close();
            ps.close();
            ps=null;
            rst=null;
           // theConnection=null;
     }    
      catch(Exception e){
          System.out.println("Exist method");
          e.printStackTrace();
      }
 //   System.out.println("REsult in Exist==>"+result);
  return result;
 }

public void setFibreBase(){
    try{
        VBaseCode           = new Vector();
        VBaseName           = new Vector();
        
        StringBuffer sb     = new StringBuffer(); 
        
        sb.append("  Select BaseCode,BaseName From FibreBase ");
        sb.append("  Order by 1");
        
    //    System.out.println("FibreBase Qry===> "+sb.toString());
        
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();

        while (rst.next()){
              VBaseCode              . add(rst.getString(1));
              VBaseName              . add(rst.getString(2));
         }
      //  System.out.println("BaseCode Size==>"+VBaseCode.size());
            rst.close();
            ps.close();
            ps=null;
          //  theConnection.close();
            
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("Dyes:"+e);
    }
}
public String getBaseCode(String SBaseName){
          int iIndex = VBaseName.indexOf(SBaseName);
          return (String)VBaseCode.elementAt(iIndex);
 }

public String getBaseName(String SBaseCode){
         int index      = VBaseCode.indexOf(SBaseCode);
         return String.valueOf(VBaseName.elementAt(index));
 }


public void setFibreShade(){
    try{
        VShadeBaseCode           = new Vector();
        VShadeBaseName           = new Vector();
        
        StringBuffer sb     = new StringBuffer(); 
        
        sb.append("  Select Distinct Shade_Code,BaseName from ShadewiseDyes ");
        sb.append("  Inner Join FibreBase on ShadeWiseDyes.Shade_Code = FibreBase.BaseCode ");
        sb.append("  Order by 1");
        
    //    System.out.println("ShadeBase Qry===> "+sb.toString());
        
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();

        while (rst.next()){
              VShadeBaseCode          . add(rst.getString(1));
              VShadeBaseName          . add(rst.getString(2));
         }
      //  System.out.println("BaseCode Size==>"+VBaseCode.size());
            rst.close();
            ps.close();
            ps=null;
          //  theConnection.close();
            
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("Dyes:"+e);
    }
}
public String getFibreBaseCode(String SBaseName){
          int iIndex = VShadeBaseName.indexOf(SBaseName);
          return (String)VShadeBaseCode.elementAt(iIndex);
 }

public String getFibreBaseName(String SBaseCode){
         int index      = VShadeBaseCode.indexOf(SBaseCode);
         return String.valueOf(VShadeBaseName.elementAt(index));
 }


public void setDyesName(int iPartyCode){
    try{
        VExDyesCode         = new Vector();
        VExDyesName         = new Vector();
        
        StringBuffer sb     = new StringBuffer(); 
        
        sb.append("  Select Dyes_Code,Dyes_Item_Name from DyesPriceRevision   ");
        sb.append("  Inner Join Dyes_InvItems on Dyes_InvItems.DYES_ITEM_CODE = DyesPriceRevision.Dyes_Code ");
        sb.append("  Where Supp_Code = "+iPartyCode);
        sb.append("  and PriceRevisionStatus=0 ");
        sb.append("  Union all ");
        sb.append("  Select Dyes_Code,Item_Name from DyesPriceRevision   ");
        sb.append("  Inner Join InvItems on InvItems.ITEM_CODE = DyesPriceRevision.Dyes_Code ");
        sb.append("  Where Supp_Code = "+iPartyCode);
        sb.append("  and PriceRevisionStatus=0 ");
        
   //     System.out.println("set DyesName Qry===> "+sb.toString());
        
       if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();

        while (rst.next()){
              VExDyesCode            . add(rst.getString(1));
              VExDyesName            . add(rst.getString(2));
         }
        System.out.println("Name Size===> "+VExDyesName.size());
            rst.close();
            ps.close();
            ps=null;
          //  theConnection.close();
            
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("Dyes:"+e);
    }
}
public String getExDyesCode(String SExDyesName){
          int iIndex = VExDyesName.indexOf(SExDyesName);
          return (String)VExDyesCode.elementAt(iIndex);
 }

public String getExDyesName(String SExDyesCode){
         int index      = VExDyesCode.indexOf(SExDyesCode);
         return String.valueOf(VExDyesName.elementAt(index));
 }


public int getInsertShadewiseDyes (String sShadeCode,String sDyesCode,String sDyesPer,String sDyesRate,String sDyesCost,String sSysName,int iPartyCode,String sPriceRevisionStatus){
   int isave=0; 
/*     boolean bs  = isNewDyesExist(sDyesName);
     if (bs){
          isave=2;
      }*/
    if(isave!=2){
     try{
         
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" insert into ShadeWiseDyes ( ID ,Shade_Code ,Dyes_Code ,Dyes_Per , Dyes_Rate , Dyes_Cost, EntrySysName ,");
         sb.append(" EntryDateTime,Party_Code,  ");
         sb.append(" WithEffectFrom , WithEffectTo,PriceRevisionStatus  ) ");
         sb.append(" Values ( ShadewiseDyes_seq.nextVal,'"+sShadeCode+"','"+sDyesCode+"',"+sDyesPer+","+sDyesRate+",");
         sb.append(" "+sDyesCost+",'"+sSysName+"',to_Char(Sysdate,'YYYYMMDD HH24:MI:SS'),"+iPartyCode );
         if(sPriceRevisionStatus.trim().equals("1")){
         sb.append(" , to_Char(sysdate,'YYYYMMDD'), to_Char(sysdate,'YYYYMMDD'),"+sPriceRevisionStatus+" )" );    
         }
         else{
         sb.append(" , to_Char(sysdate,'YYYYMMDD'), 99999999,"+sPriceRevisionStatus+" )" );
         }
  //     System.out.println("Insert Shades:"+sb.toString());
         
         if (theDyeConnection==null){
                  DyeJDBCConnection jdbc= DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection=jdbc.getConnection();
              }
        PreparedStatement ps=theDyeConnection.prepareStatement(sb.toString());

            ps . executeUpdate();
            ps.close();
            ps=null;
      }  
      catch(Exception e){
          isave=1;
          e.printStackTrace();
          System.out.println("InsertMethod"+e);
       }
     }
   
 return isave;
}

public String getDyesRate(String sDyesCode){
    String sRate="";
    try{
        StringBuffer sb         = new StringBuffer(); 
        
        sb.append("   Select NetValue from DyesPriceRevision ");
        sb.append("   Where WithEffectto=99999999 and Dyes_Code='"+sDyesCode+"' and PriceRevisionStatus=0");
        
       // System.out.println(" DyesRate-->"+sb.toString());
        
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection= jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
        //ResultSetMetaData rsmd        = rst.getMetaData();
        
       while (rst.next()){
              sRate                   = rst.getString(1);
         }
            rst.close();
            ps.close();
            ps=null;
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("Dyes Rate===>:"+e);
    }
    return sRate;
 }

public String getDyesPartySequenceValue (){
    String sSequence="";
    try{
        StringBuffer sb         = new StringBuffer(); 
        
        sb.append("   Select Dyes_PartyCode_Seq.nextVal from dual");
        
   //     System.out.println(" Sequence-->"+sb.toString());
        
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
        
        ResultSetMetaData rsmd        = rst.getMetaData();
        
       while (rst.next()){
              sSequence               = rst.getString(1);
         }
            rst.close();
            ps.close();
            ps=null;
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("Sequence:"+e);
    }
    return sSequence;
 }

public int getInsertDyesPartName (String sPartyName){
     int isave=0; 
     boolean bs  = isNewDyesPartyExist(sPartyName);
     if (bs){
          isave=2;
      }
    if(isave!=2){
     try{
         
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" insert into Dyes_Party ( ID ,Dyes_Party_Code ,Dyes_Party_Name )");
         sb.append(" Values ( Dyes_PartyID_Seq.nextVal,Dyes_PartyCode_Seq.nextVal,'"+sPartyName+"' )");
         
     //  System.out.println("Insert PartyName:"+sb.toString());
         
         if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection          = jdbc.getConnection();
              }
        PreparedStatement ps=theDyeConnection.prepareStatement(sb.toString());
            ps . executeUpdate();
            ps.close();
            ps=null;
      }  
      catch(Exception e){
          isave=1;
          e.printStackTrace();
          System.out.println("InsertMethod"+e);
       }
     }
 return isave;
}

public boolean isNewDyesPartyExist(String sDyesPartyName){
  boolean result=false;
  try{
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" Select Count(1) from Dyes_Party  Where Dyes_Party_Name='"+sDyesPartyName+"'");
         
      //   System.out.println("DyesParty Exist Qry"+sb.toString());
         
         if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        

        ResultSet rst=ps.executeQuery();
         while(rst.next()) {
            int iCount          = rst.getInt(1);
              if(iCount>0){      
                 result=true;
                 break;
             }   
           else{
                 result=false;
             }
      }
            rst.close();
            ps.close();
            ps=null;
            rst=null;
          //  theConnection=null;
     }    
      catch(Exception e){
          System.out.println("Exist method");
          e.printStackTrace();
      }
  return result;
 }

public void setDyesCostDetails(int iPartyCode){
    try{
        ADyesCostList       = new ArrayList();    
        StringBuffer sb     = new StringBuffer(); 
        
        sb.append("  Select ShadeWiseDyes.Dyes_Code,Dyes_InvItems.Dyes_Item_Name,Dyes_Per,Dyes_Rate,Dyes_Cost, ");
        sb.append("  BaseCode,BaseName,decode(RecipeStatus,0,'NEW',1,'OLD') as RecipeStatus from ShadeWiseDyes  ");
        sb.append("  Inner Join FibreBase on  FibreBase.BaseCode = ShadeWiseDyes.Shade_Code ");
        sb.append("  Inner Join Dyes_InvItems  on Dyes_InvItems.DYES_ITEM_CODE = ShadeWiseDyes.Dyes_Code ");
        sb.append("  Where Party_Code = "+iPartyCode);
     //   sb.append("  and PRICEREVISIONSTATUS=0 ");
        sb.append("  Union all ");
        
        sb.append("  SElect ShadeWiseDyes.Dyes_Code,InvItems.Item_Name,Dyes_Per,Dyes_Rate,Dyes_Cost, ");
        sb.append("  BaseCode,BaseName,decode(RecipeStatus,0,'NEW',1,'OLD') as RecipeStatus from ShadeWiseDyes ");
        sb.append("  Inner Join FibreBase on  FibreBase.BaseCode = ShadeWiseDyes.Shade_Code ");
        sb.append("  Inner Join InvItems on InvItems.ITEM_CODE = ShadeWiseDyes.Dyes_Code ");
        sb.append("  Where Party_Code = "+iPartyCode);
      //  sb.append("  and PRICEREVISIONSTATUS=0 ");
        
        System.out.println("set DyesCost Qry===> "+sb.toString());
        
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
        ResultSetMetaData rsmd        = rst.getMetaData();

       while (rst.next()){
              HashMap row = new HashMap();
            for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    row.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
            
             ADyesCostList.add(row);
           }
            System.out.println("Inside Qry===>"+ADyesCostList.size());
            rst.close();
            ps.close();
            ps=null;
          //  theConnection.close();
            
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("Dyes:"+e);
    }
  }

public int updateDyesCostData (int iSupplierCode, String sDyesCode,String sRate,String sPercentage,String sCost){
    int iSaveValue=-1;
  try{
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" Update ShadeWiseDyes  set Dyes_Rate="+sRate+",Dyes_Code="+sDyesCode+",Dyes_Cost="+sCost);
         sb.append(" Where Supp_Code="+iSupplierCode+" and Dyes_Code='"+sDyesCode+"'");
         sb.append(" and PriceRevisionStatus=0");
        
      //   System.out.println("Update Qry:"+sb.toString());
         
           if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
           
                  
//        theProcessConnection.setAutoCommit(false); 
      // PreparedStatement ps=theConnection.prepareStatement(sb.toString());

       ps . executeUpdate();
       iSaveValue=0;
       }  

      catch(Exception e)
      {
         try{
          e.printStackTrace();
          iSaveValue=1;
  //        theProcessConnection.rollback();
       //   System.out.println("InsertMethod"+e);
          }
          catch(Exception ex){
              ex.printStackTrace();
          }
       }
 //   System.out.println("update Rt Value--->"+iSaveValue);
  return iSaveValue;
  
}

public int getNewDyesCount (String sDyesName) {
    int         iCount=-1;
    try{
        StringBuffer sb         = new StringBuffer(); 
        
        sb.append("   Select Count(1) from Dyes_InvItems ");
        sb.append("   Where Dyes_Item_Name like trim('"+sDyesName+"')");

    //    System.out.println("New Dyes Count--->"+sb.toString());
        
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
        
        ResultSetMetaData rsmd        = rst.getMetaData();
        
       while (rst.next()){
              iCount                 = rst.getInt(1);
         }
            rst.close();
            ps.close();
            ps=null;
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("New Dyes Count:"+e);
    }
    return iCount;
 }

 
public int getNewPartyCount (String sPartyName) {
    int         iCount=-1;
    try{
        StringBuffer sb         = new StringBuffer(); 
        
        sb.append("   Select Count(1) from Dyes_Party ");
        sb.append("   Where Dyes_Party_Name like trim('"+sPartyName+"')");

  //      System.out.println("New Party Count--->"+sb.toString());
        
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
        ResultSetMetaData rsmd        = rst.getMetaData();
        
       while (rst.next()){
              iCount                 = rst.getInt(1);
         }
            rst.close();
            ps.close();
            ps=null;
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("New Party Count:"+e);
    }
    return iCount;
 }

public int updateShadePriceRevisionStatus (int iPartyCode, String sDyesCode,String sShadeCode){
    int iSaveValue=-1;
  try{
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" Update SHADEWISEDYES  set PriceRevisionStatus=1,WithEffectTo=to_Char(sysdate,'YYYYMMDD')");
         sb.append(" Where Party_Code='"+iPartyCode+"' and Dyes_Code='"+sDyesCode+"' and Shade_Code='"+sShadeCode+"'");
         sb.append(" and ID=( Select Max(ID) from SHADEWISEDYES Where Party_Code='"+iPartyCode+"' and Dyes_Code='"+sDyesCode+"'");
         sb.append(" and Shade_Code='"+sShadeCode+"')");

    //     System.out.println("Update Qry:"+sb.toString());
         
           if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
        
       ps . executeUpdate();
       iSaveValue=0;
       }  

      catch(Exception e)
      {
         try{
          e.printStackTrace();
          iSaveValue=1;
  //        theProcessConnection.rollback();
          System.out.println("InsertMethod"+e);
          }
          catch(Exception ex){
              ex.printStackTrace();
          }
       }
 //   System.out.println("update Rt Value--->"+iSaveValue);
  return iSaveValue;
  
}

public int getShadewiseDyesCount (int iSupplierCode,String sDyesCode,String sShadeCode){
    int         iCount=-1;
    try{
        StringBuffer sb         = new StringBuffer(); 
        
        sb.append("   Select count(1) from ShadeWiseDyes  ");
        sb.append("   Where Party_Code="+iSupplierCode+" and Dyes_Code='"+sDyesCode+"'");
        sb.append("   and Shade_Code='"+sShadeCode+"'");
        
  //      System.out.println("shd wise count--->"+sb.toString());
        
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
        ResultSetMetaData rsmd        = rst.getMetaData();
        
       while (rst.next()){
              iCount                 = rst.getInt(1);
         }
            rst.close();
            ps.close();
            ps=null;
    }
    catch(Exception e){
            e.printStackTrace();
    }
    return iCount;
 }

public int getInsertData_Old (String sDyesCode,String sAnnualConsumption,String sRateperKg,String sDiscount,String sTax,String sDiscountper,String sTaxper,String sNetValue,String sTotalValue,int iSupplierCode,String sSysName,String sPriceRevisionDate){
     int isave=0;
     
    if(isave!=2){
     try{
    
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" insert into DyesPriceRevision ( ID ,Dyes_Code ,AnnualConsumption ,RateperKg ,Discper,Taxper ,Discount,Tax,NetValue,TotalValue,");
         sb.append(" WithEffectFrom ,WithEffectTo ,EntrySysName ,EntryDateTime,Supp_Code ) ");
         sb.append(" Values ( DyesPrice_ID_Seq.nextVal,'"+sDyesCode+"','"+sAnnualConsumption+"',"+sRateperKg+","+sDiscountper+","+sTaxper);
         sb.append(" ,"+sDiscount+","+sTax+","+sNetValue+","+sTotalValue+", "+sPriceRevisionDate+",99999999,'"+sSysName+"',to_Char(sysdate,'YYYYMMDD HH24:Mi:ss'),"+iSupplierCode+")");
         
         //sb.append(" ,"+sDiscount+","+sTax+","+sNetValue+","+sTotalValue+", to_Char(sysdate,'YYYYMMDD'),99999999,'"+sSysName+"',to_Char(sysdate,'YYYYMMDD HH24:Mi:ss'),"+iSupplierCode+")");
         
    //   System.out.println("Insert Qry:"+sb.toString());
         
         if (theDyeConnection==null){
                  DyeJDBCConnection jdbc= DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection=jdbc.getConnection();
              }
        PreparedStatement ps=theDyeConnection.prepareStatement(sb.toString());

            ps . executeUpdate();
            ps.close();
            ps=null;
      }  
   
      catch(Exception e)
      {
          isave=1;
          e.printStackTrace();
          System.out.println("InsertMethod"+e);
       }
     }
   
 return isave;
}

public int updateDyesData_Old (int iSupplierCode, String sDyesCode,String sRateperKg,String sDisper,String sTaxper,String sDiscount,String sTax,String sNetValue,String sTotalValue){
    int iSaveValue=-1;
  try{
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" Update DyesPriceRevision  set RateperKg="+sRateperKg+", Discper="+sDisper+",Taxper="+sTaxper+",");
         sb.append(" Discount="+sDiscount+",Tax="+sTax+",NetValue="+sNetValue+",TotalValue ="+sTotalValue);
         //sb.append(" , LabStatus="+sStatus);
         sb.append(" Where Supp_Code="+iSupplierCode+" and Dyes_Code='"+sDyesCode+"'");
         sb.append(" and PriceRevisionStatus=0");
        
         System.out.println("Update Qry:"+sb.toString());
         
        if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
              }
        PreparedStatement ps          = theDyeConnection.prepareStatement(sb.toString());
        
       ps . executeUpdate();
       iSaveValue=0;
       }  

      catch(Exception e)
      {
         try{
          e.printStackTrace();
          iSaveValue=1;
  //        theProcessConnection.rollback();
       //   System.out.println("InsertMethod"+e);
          }
          catch(Exception ex){
              ex.printStackTrace();
          }
       }
 //   System.out.println("update Rt Value--->"+iSaveValue);
  return iSaveValue;
  
}

}
