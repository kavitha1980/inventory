
package DyesPriceRevision;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

public class PriceRevisionModel extends DefaultTableModel {
    String  ColumnName[];
    String  ColumnType[];
    int     iColumnWidth[] ;
    
 public PriceRevisionModel(){
       setDataVector(getRowData(),ColumnName);
 }   
 
 public PriceRevisionModel(String[] ColumName,String[] ColumnType,int[] iColumnWidth )
    {
        super();
        this.ColumnName=ColumName;
        this.ColumnType=ColumnType;
        this.iColumnWidth = iColumnWidth;
        setDataVector(getRowData(),ColumnName);
    }
 
/*public Class getColumnClass(int iCol)
{
        return getValueAt(0,iCol).getClass();
}*/

public boolean isCellEditable(int iRow,int iCol)
{
        if (ColumnType[iCol]=="E"||ColumnType[iCol]=="B")
            
            return true;
            return false;
}
    
private Object[][]getRowData()
{
        Object RowData[][]=new Object[1][ColumnName.length];
        
        for(int i=0;i<ColumnName.length;i++)
            RowData[0][i]="";
        return RowData;
}
    
public void appendRow(Vector theVect)
{
        insertRow(getRows(),theVect);
}
public int getRows()
{
        return super.dataVector.size();
}
}
