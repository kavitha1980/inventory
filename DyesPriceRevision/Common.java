
package DyesPriceRevision;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.*;
import java.net.*;

public class Common {
public   String   bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
            HashMap  theMap;

            String   S0To9[]   = {"Zero ","One ","Two ","Three ","Four ","Five ","Six ","Seven ","Eight ","Nine "};
            String   S10To19[] = {"Ten ","Eleven ","Twelve ","Thirteen ","Fourteen ","Fifteen ","Sixteen ","Seventeen ","Eightteen ","Nineteen "};
            String   TensArr[] = {"Twenty ","Thirty ","Forty ","Fifty ","Sixty ","Seventy ","Eighty ","Ninety "};

    public Common(){
      bgColor = "#FEFCD8";
      bgHead  = "#DFD9F2";     // "#000080";
      bgUom   = "#FFB0B0";     // "#CCCCFF";
      bgBody  = "#9DECFF";
      fgHead  = "#000080";     // "#00FFFF";
      fgUom   = "#000080";
      fgBody  = "#000080";
      setMonthNames();
 }
    public String getRound(double dAmount,int iScale)
   {
      try
      {
         java.math.BigDecimal bd1 = new java.math.BigDecimal(String.valueOf(dAmount));
         return (bd1.setScale(iScale,4)).toString();
      }
      catch(Exception ex){}
      return "0";
   }

   public String getUpperRound(double dAmount,int iScale)
   {
      try
      {
         java.math.BigDecimal bd1 = new java.math.BigDecimal(String.valueOf(dAmount));
         return (bd1.setScale(iScale,3)).toString();
      }
      catch(Exception ex){}
      return "0";
   }

   public String getRound(String SAmount,int iScale)
   {
      try
      {
         SAmount = SAmount.trim();
         java.math.BigDecimal bd1 = new java.math.BigDecimal(SAmount);
         return (bd1.setScale(iScale,4)).toString();
      }
      catch(Exception ex){}
      return "0";
   }
        public  double getRound5(double dValue)
     {
          double dRoundValue=0;
                   
          if(Math.IEEEremainder(dValue,10)>=0 && Math.IEEEremainder(dValue,10)!=-5)
          {
               dRoundValue    =    (Math.IEEEremainder(dValue,10)<=2?dValue-Math.IEEEremainder(dValue,10):(dValue-Math.IEEEremainder(dValue,10)+5));
          }
          else
          {
               dRoundValue    =    (Math.IEEEremainder(dValue,10)<=-3?(dValue-(Math.IEEEremainder(dValue,10)+5)):(dValue-Math.IEEEremainder(dValue,10)));
          }
          return dRoundValue;
     }

     public  double getRound10(double dValue)
     {
          double dRoundValue=0;

          if(Math.IEEEremainder(dValue,10)>=0 && Math.IEEEremainder(dValue,10)!=-5)
          {
               dRoundValue    =    (Math.IEEEremainder(dValue,10)<=2?dValue-Math.IEEEremainder(dValue,10):(dValue-Math.IEEEremainder(dValue,10)));
          }
          else
          {
               dRoundValue    =    (Math.IEEEremainder(dValue,10)<=-3?(dValue-(Math.IEEEremainder(dValue,10))):(dValue-Math.IEEEremainder(dValue,10)));
          }
          return dRoundValue;
     }
   
   public String Pad(String Str,int Len)
   {
      Str = ""+Str;
      if(Str.startsWith("null"))
         return Space(Len);

      if(Str.length() > Len)
         return Str.substring(0,Len);
      else
         return Str+Space(Len-Str.length());
   }

   public String Rad(String Str,int Len)
   {

      Str = ""+Str;
      if(Str.startsWith("null"))
         return Space(Len);

      if(Str.length() > Len)
         return Str.substring(0,Len);
      else
         return Space(Len-Str.length())+Str;
   }

   public String Space(int Len)
   {
      String RetStr="";
      for(int index=0;index<Len;index++)
         RetStr=RetStr+" ";
      return RetStr;
   }

   public String Replicate(String Str,int Len)
   {
      String RetStr="";
      for(int index=0;index<Len;index++)
         RetStr = RetStr+Str;
      return RetStr;
   }

   public String parseNull(String str)
   {
      String RetStr=str;
      str = ""+str;
      if(str.startsWith("null"))
         return "";
      return RetStr;
   }

   public String Cad(String str,int Len)
   {
      if(Len < str.length())
         return Pad(str,Len);

      String RetStr="";
      int FirstLen = (Len-str.length())/2;
      int SecondLen = Len-FirstLen;
      return Pad(Space(FirstLen)+str,Len);
   }

   public String parseDate(String str)
   {
      str=parseNull(str);
      if(str.length()!=8)
         return str;
      try
      {
         return str.substring(6,8)+"."+str.substring(4,6)+"."+str.substring(0,4);
      }
      catch(Exception ex){}
      return "";
   }

   public int getNextMonth(int iMonth)
   {
      iMonth = toInt(String.valueOf(iMonth).substring(0,6));
      String SMonth = String.valueOf(iMonth);
      String STmp = "";
      int iYear = toInt(SMonth.substring(0,4));
      int iNextMonth = iMonth+1;
      if(String.valueOf(iNextMonth).substring(4,6).equals("13"))
         STmp=String.valueOf(iYear+1)+"01";
      else
         STmp=String.valueOf(iNextMonth);
      return toInt(STmp);
   }

   public String pureDate(String str)
   {
      str=parseNull(str);
      if(str.length()!=10)
         return "0";
      try
      {
         return str.substring(6,10)+str.substring(3,5)+str.substring(0,2);
      }
      catch(Exception ex){}
      return "";
   }

   public double toDouble(String str)
   {
      str = parseNull(str);
      try
      {
         return Double.parseDouble(str.trim());       
      }
      catch(Exception ex)
      {
         return 0;
      }
   }

   public int toInt(String str)
   {
       str = parseNull(str);
      try
      {
         return Integer.parseInt(str);       
      }
      catch(Exception ex)
      {
          return 0;
      }
   }

   public float toFloat(String str)
   {
      str = parseNull(str);
      try
      {
         return Float.parseFloat(str);       
      }
      catch(Exception ex)
      {
         return 0;
      }
   }

   public String getDateDiff(String str1,String str2)
   {
      int iYear1,iMonth1,iDay1;
      int iYear2,iMonth2,iDay2;

      try
      {
         iYear1  = toInt(str1.substring(6,10))-1900;
         iMonth1 = toInt(str1.substring(3,5))-1;
         iDay1   = toInt(str1.substring(0,2));
      }
      catch(Exception ex)
      {
         return "";
      }
      try
      {
         iYear2  = toInt(str2.substring(6,10))-1900;
         iMonth2 = toInt(str2.substring(3,5))-1;
         iDay2   = toInt(str2.substring(0,2));
      }
      catch(Exception ex)
      {
         return "";
      }
      java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);
      java.util.Date dt2 = new java.util.Date(iYear2,iMonth2,iDay2);
      return String.valueOf((dt1.getTime()-dt2.getTime())/(24*60*60*1000));
   }

   public int getDateDiff(int int1,int int2)
   {
      String str1 = "";
      String str2 = "";
      
      str1 = parseDate(String.valueOf(int1));
      str2 = parseDate(String.valueOf(int2));

      int iYear1,iMonth1,iDay1;
      int iYear2,iMonth2,iDay2;

      try
      {
         iYear1  = toInt(str1.substring(6,10))-1900;
         iMonth1 = toInt(str1.substring(3,5))-1;
         iDay1   = toInt(str1.substring(0,2));
      }
      catch(Exception ex)
      {
         return 0;
      }
      try
      {
         iYear2  = toInt(str2.substring(6,10))-1900;
         iMonth2 = toInt(str2.substring(3,5))-1;
         iDay2   = toInt(str2.substring(0,2));
      }
      catch(Exception ex)
      {
         return 0;
      }

      java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);
      java.util.Date dt2 = new java.util.Date(iYear2,iMonth2,iDay2);
      return toInt(String.valueOf((dt1.getTime()-dt2.getTime())/(24*60*60*1000)));
   }


   public String getDate(String str1,long days,int Sig)
   {
      String SDay="",SMonth="",SYear="";
      int iYear1=0,iMonth1=0,iDay1=0;
      try
      {
         iYear1  = toInt(str1.substring(6,10))-1900;
         iMonth1 = toInt(str1.substring(3,5))-1;
         iDay1   = toInt(str1.substring(0,2));
      }
      catch(Exception ex)
      {
         return "0";
      }
      java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);
      
      long num1 = dt1.getTime();
      long num2 = (days*24*60*60*1000);
      if (Sig == -1)
         dt1.setTime(num1-num2);
      else
         dt1.setTime(num1+num2);
      
      int iDay   = dt1.getDate();
      int iMonth = dt1.getMonth()+1;
      int iYear  = dt1.getYear()+1900;
      
      SDay   = (iDay<10 ?"0"+iDay:""+iDay);
      SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
      SYear  = ""+iYear;
      return SDay+"."+SMonth+"."+SYear;
   }

   public int getNextDate(int int1)
   {
      String str1 = parseDate(String.valueOf(int1));
      String SDay="",SMonth="",SYear="";
      int iYear1=0,iMonth1=0,iDay1=0;
      try
      {
         iYear1  = toInt(str1.substring(6,10))-1900;
         iMonth1 = toInt(str1.substring(3,5))-1;
         iDay1   = toInt(str1.substring(0,2));
      }
      catch(Exception ex)
      {
         return 0;
      }
      java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);

      long num1 = dt1.getTime();
      long num2 = (24*60*60*1000);
      dt1.setTime(num1+num2);
      
      int iDay   = dt1.getDate();
      int iMonth = dt1.getMonth()+1;
      int iYear  = dt1.getYear()+1900;
      
      SDay   = (iDay<10 ?"0"+iDay:""+iDay);
      SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
      SYear  = ""+iYear;
      return toInt(SYear+SMonth+SDay);
   }

   public String getCurrentDate()
   {
      String SDay,SMonth,SYear;
      
      java.util.Date dt    = new java.util.Date();
      int iDay   = dt.getDate();
      int iMonth = dt.getMonth()+1;
      int iYear  = dt.getYear()+1900;
      
      if(iDay < 10)
         SDay = "0"+iDay;
      else
         SDay = String.valueOf(iDay);

      if(iMonth < 10)
         SMonth = "0"+iMonth;
      else
         SMonth = String.valueOf(iMonth);

      SYear = String.valueOf(iYear);

      return(SYear+SMonth+SDay);
   }

   public int indexOf(Vector vect,String str)
   {
      int iindex=-1;
      str = str.trim();
      for(int i=0;i<vect.size();i++)
      {
         String str1 = (String)vect.elementAt(i);
         str1 = str1.trim();
         if(str1.startsWith(str))
            return i;
      }
      return iindex;
   }
   
   // SOp - operation : "S" - SUM; "A" - Average
   public String getSum(Vector Vx,String SOp)
   {

      double dAmount=0.0;   
      for(int i=0;i<Vx.size();i++)
      {
         dAmount = dAmount+toDouble((String)Vx.elementAt(i));
      }
      if(SOp.equals("A"))
      {
         dAmount = dAmount/Vx.size();
      }
      return getRound(dAmount,3);
   }

      public String getSum(Vector Vx,String SOp,int iScale)
      {
            double dAmount=0.0;   
            for(int i=0;i<Vx.size();i++)
            {
                dAmount = dAmount+toDouble((String)Vx.elementAt(i));
            }
            if(SOp.equals("A"))
            {
              dAmount = dAmount/Vx.size();
            }
            return getRound(dAmount,iScale);
      }

   public String getDate()
   {
      java.util.Date date = new java.util.Date();

      int iDay   = date.getDate();
      int iMonth = date.getMonth()+1;
      String sYear  = String.valueOf(date.getYear()+1900);
      
      String sDay    = (iDay>=10?""+iDay:"0"+iDay);
      String sMonth  = (iMonth>=10?""+iMonth:"0"+iMonth);
      
      String sDate  = sYear+sMonth+sDay;
      
      return sDate;
   }

   public String getTime()
   {
      java.util.Date dt        = new java.util.Date();

      int iHrs       = dt.getHours();
      int iMin       = dt.getMinutes();
      
      String sHrs    =(iHrs<10?"0"+iHrs:""+iHrs);
      String sMin    =(iMin<10?"0"+iMin:""+iMin);
      
      return sHrs+":"+sMin;
   }

   public String getUser()
   {
      return "";
   }

   public String getFinancePeriod()
   {
      return "";
   }

   public String stuff(String str,int iLen)
   {

      for(;str.length()<iLen;)
      {
         str="0"+str;
      }
      return str;
   }

   public String getMonthName(int iMonth)
   {
      String SArr[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
      String SMonth = null;
      try
      {
         SMonth = String.valueOf(iMonth);
         iMonth = toInt(SMonth.substring(4,6))-1;
         int iYear  = toInt(SMonth.substring(0,4));  
         SMonth = SArr[iMonth]+" "+iYear;
      }
      catch(Exception ex)
      {
         return "";
      }
      return SMonth;
   }

   private void setMonthNames()
   {
      theMap = new HashMap();
      
      theMap.put("01","JAN");
      theMap.put("02","FEB");
      theMap.put("03","MAR");
      theMap.put("04","APR");
      theMap.put("05","MAY");
      theMap.put("06","JUN");
      theMap.put("07","JUL");
      theMap.put("08","AUG");
      theMap.put("09","SEP");
      theMap.put("10","OCT");
      theMap.put("11","NOV");
      theMap.put("12","DEC");
   }

   public HashMap getMonthNames()
   {
      return theMap;
   }
   
   public String getMonthYear(int iYearMonth)
   {
      String str = "";
      String SYearMonth=String.valueOf(iYearMonth);
      str = theMap.get(SYearMonth.substring(4,6))+"_"+SYearMonth.substring(0,4);
      return str;
   }

   public int getNextMonthYear(int iYearMonth)
   {
      int nextMonth = 0;
      String SYearMonth=String.valueOf(iYearMonth);
      String SYear  = SYearMonth.substring(0,4);
      String SMonth = SYearMonth.substring(4,6);
      
      if(toInt(SMonth)>0 && toInt(SMonth)<9)
         nextMonth=toInt( SYear +"0"+String.valueOf( toInt(SMonth)+1 ) );
      else if(toInt(SMonth)>8 && toInt(SMonth)<12)
         nextMonth=toInt( SYear + String.valueOf( toInt(SMonth)+1 ) );
      else if(toInt(SMonth)==12)
         nextMonth=toInt( String.valueOf(toInt(SYear)+1) + "01");
      
      return nextMonth;
   }

   public String getRupee(String str)
   {
      str = getRound(str,2);
      int  lRupee=0;
      int  iPaise=0;
      java.util.StringTokenizer ST = new java.util.StringTokenizer(str,".");
      while(ST.hasMoreTokens())
      {
         lRupee=toInt(ST.nextToken());
         iPaise=toInt(ST.nextToken());
      }
      String SRs = (lRupee > 0 ? getCrores(lRupee):"Zero");
      String SPs = (iPaise > 0 ? " and paise "+getTens(iPaise):"");
      return "Rupees "+SRs+SPs+"Only";
   }
   
   private String getCrores(int i)
   {
      if(i<10000000)
         return getLacs(i);
      String str="";
      int j=0,k=0;
      if(i<=99999999)
      {
         str   = String.valueOf(i);
         j        = toInt(str.substring(0,1));
         k        = toInt(str.substring(1,8));
         return getSingle(j)+"Crore "+getLacs(k);
      }
      str   = String.valueOf(i);
      j        = toInt(str.substring(0,2));
      k        = toInt(str.substring(2,9));
      return getTens(j)+"Crore "+getLacs(k);
   }

   private String getLacs(int i)
   {
      if(i<100000)
         return getThousands(i);
      
      String str="";
      int j=0,k=0;
      if(i<=999999)
      {
         str   = String.valueOf(i);
         j        = toInt(str.substring(0,1));
         k        = toInt(str.substring(1,6));
         return getSingle(j)+"Lac "+getThousands(k);
      }
      str   = String.valueOf(i);
      j        = toInt(str.substring(0,2));
      k        = toInt(str.substring(2,7));
      return getTens(j)+"Lac "+getThousands(k);
   }

   private String getThousands(int i)
   {
      if(i<1000)
         return getHundreds(i);
      
      String str="";
      int j=0,k=0;
      if(i<=9999)
      {
         str   = String.valueOf(i);
         j        = toInt(str.substring(0,1));
         k        = toInt(str.substring(1,4));
         return getSingle(j)+"Thousand "+getHundreds(k);
      }
      str   = String.valueOf(i);
      j        = toInt(str.substring(0,2));
      k        = toInt(str.substring(2,5));
      return getTens(j)+"Thousand "+getHundreds(k);
   }
   
   private String getHundreds(int i)
   {
      if(i<100)
         return getTens(i);

      String str="";
      int j=0,k=0;
      str   = String.valueOf(i);
      j        = toInt(str.substring(0,1));
      k        = toInt(str.substring(1,3));
      return getSingle(j)+"Hundred "+getTens(k);
   }

   private String getTens(int i)
   {
      if(i==0)
         return "";
      if(i >= 0 && i <= 9)
         return getSingle(i);
      if(i >= 10 && i <= 19)
         return S10To19[i-10];

      String str   = String.valueOf(i);
      int j        = toInt(str.substring(0,1))-2;
      int k        = toInt(str.substring(1,2));
      return TensArr[j]+(k>0?getSingle(k):"");
   }

   private String getSingle(int i)
   {
      return S0To9[i];          
   }

   public static void main(String[] args)
   {
      Common common = new Common();
      //System.out.println(common.parseDate(common.getCurrentDate()));
      try
      {

         String Sname = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
         String myString = Sname.substring(11,14);
         System.out.println(myString);
      
      }
      catch(Exception ex)
      {
         ex.printStackTrace();
      }
   }

   public String getServerDate() throws Exception
   {

      Connection theConnection = null;
      String SServerDate       = "";

      if(theConnection == null)
      {
         JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
         theConnection        = jdbc.getConnection();
      }
      
      Statement stat   = theConnection.createStatement();
      ResultSet result = stat.executeQuery(getDateQS());
      if(result.next())
         SServerDate = result.getString(1);
      result.close();
      stat.close();
      return SServerDate;
   }

   public String getServerTime() throws Exception
   {

      Connection theConnection = null;
      String SServerTime       = "";

      if(theConnection == null)
      {
         JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
         theConnection        = jdbc.getConnection();
      }

      Statement stat   = theConnection.createStatement();
      ResultSet result = stat.executeQuery(getTimeQS());
      if(result.next())
         SServerTime = result.getString(1);
      result.close();

      stat.close();
      return SServerTime;
   }


   public String getSerDateTimeQS()
   {
      Connection theConnection = null;

      String SDate = "";
      try
      {
         String QS = "Select to_Char(Sysdate,'DD.MM.YYYY HH24:MI:SS') From Dual";
         if(theConnection == null)
         {
            JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
            theConnection        = jdbc.getConnection();
         }
         Statement stat   = theConnection.createStatement();

         ResultSet res    = stat.executeQuery(QS);
         while(res.next())
         {
            SDate = res.getString(1);
         }
         res.close();
         stat.close();
      }
      catch(Exception e)
      {
         e.printStackTrace();
      }
      return SDate;
   }

   private String getTimeQS()
   {
      String QS = "Select to_Char(sysdate,'hh24:mi:ss') from Dual";
      return QS;
   }
   
   private String getDateQS()
   {
      String QS = "Select to_Char(sysdate,'yyyymmdd') from Dual";
      return QS;
   }
   
   public String getPreviousDate(String SDate)
   {
      return pureDate(getDate(parseDate(SDate),1,-1));
   }
   
   public String setDueDays(String SDate)
   {
      java.util.Date dt    = new java.util.Date();
      
      int iDay   = dt.getDate()+toInt(SDate);
      int iMonth = dt.getMonth()+1;
      int iYear  = dt.getYear()+1900;
      
      String SDay,SMonth,SYear;
      
      if(iDay < 10)
         SDay = "0"+iDay;
      else
         SDay = String.valueOf(iDay);
      
      if(iMonth < 10)
         SMonth = "0"+iMonth;
      else
         SMonth = String.valueOf(iMonth);
      
      SYear = String.valueOf(iYear);

      java.util.Date dt1 = new java.util.Date(toInt(SYear)-1900,toInt(SMonth)-1,toInt(SDay));

      int iDay1  = dt1.getDate();
      int iMonth1= dt1.getMonth()+1;
      int iYear1 = dt1.getYear()+1900;

      String SDay1,SMonth1,SYear1;

      SDay1  = (iDay1<10 ?"0"+iDay1:""+iDay1);
      SMonth1= (iMonth1<10 ?"0"+iMonth1:""+iMonth1);
      SYear1 = ""+iYear1;

      String SDate1 = SYear1+SMonth1+SDay1;

      return SDate1;
   }

   public String getDate1(String str1,long days,int Sig)
   {
      String SDay="",SMonth="",SYear="";
      int iYear1=0,iMonth1=0,iDay1=0;
      try
      {
         iYear1  = toInt(str1.substring(0,4))-1900;
         iMonth1 = toInt(str1.substring(4,6))-1;
         iDay1   = toInt(str1.substring(6,8));
      }
      catch(Exception ex)
      {
         return "0";
      }

      java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);

      long num1 = dt1.getTime();
      long num2 = (days*24*60*60*1000);
      if (Sig == -1)
         dt1.setTime(num1-num2);
      else
         dt1.setTime(num1+num2);

      int iDay   = dt1.getDate();
      int iMonth = dt1.getMonth()+1;
      int iYear  = dt1.getYear()+1900;
      
      SDay   = (iDay<10 ?"0"+iDay:""+iDay);
      SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
      SYear  = ""+iYear;
      return SDay+"."+SMonth+"."+SYear;
   }

   public int getPreviousMonth(int iMonth)
   {
      String   SMonth   = null;
               SMonth   = String.valueOf(iMonth);
      int      iYear    = toInt(SMonth.substring(0,4));
      String   SPreMonth= String.valueOf(iMonth-1);

      if(SPreMonth.substring(4,6).equals("00"))
         SPreMonth=String.valueOf(iYear-1)+"12";
      return toInt(SPreMonth);
   }

   public String getPrevDate(String SDate)
   {
      return pureDate(getDate(parseDate(SDate),1,-1));
   }

   public int getIntCurrentDate()
   {
      return(toInt(getCurrentDate()));
   }
   
   public boolean isLeap(int iYear)
   {
      double dYear = iYear;
      if(Math.IEEEremainder(dYear,100.0)==0)
         return true;
      else if(Math.IEEEremainder(dYear,4.0)==0)
         return true;
      else
         return false;
   }

   public int getPreviousMonth(int iMonth,int iMany)
   {
      int iTemp = iMonth;
      for(int i = 0; i < iMany; i++)
      {
         iTemp = getPreviousMonth(iTemp);
      }
      return iTemp;
   }

   public int[] getPreviousMonths(int iMonth,int iMany)  
   {
      int iMonthArr[] = new int[iMany];
      for(int i=0;i<iMonthArr.length;i++)
         iMonthArr[i]=0;
         for(int i=0;i<iMonthArr.length;i++)
         {
            iMonthArr[i]=getPreviousMonth(iMonth);
            iMonth=iMonthArr[i];
         }
      return iMonthArr;
   }

   public int getMonths(int iStartMonth,int iEndMonth)
   {
      int iCntr=0;

      while(iStartMonth<=iEndMonth)
      {
         iCntr++;
         int iMonth = getNextMonth(iStartMonth);
         iStartMonth=iMonth;
      }
      return iCntr;
   }

   public String getLocalHostName()
   {
      String         SComName="";
      try
      {
         InetAddress    host      =    InetAddress    . getLocalHost();
         SComName                 =    host           . getHostName();
      }catch(Exception ex)
      {
         System.out.println(ex);
      }
      return SComName;
   }

   public boolean CheckDateWithPieceWageCalAuth(String SAsOnDate,String SStDate,String SEnDate,String SAsOnMonth,int iCatCode)
   {
      try
      {
         JDBCConnection theConnect     = JDBCConnection.getJDBCConnection();
         Connection connect            = theConnect.getConnection();
         Statement theStatement        = connect.createStatement();

         String SQuery                 = "";
         int iCount                    = 0;

         if(iCatCode == 1)
         {
            SQuery                     = "Select Count(*) from Piece_Wage_Cal_Auth where AsOnDate = "+SAsOnDate;
         }
         if(iCatCode == 2)
         {
            SQuery                     = "Select Count(*) from Piece_Wage_Cal_Auth where EnDate between "+SStDate+" and "+SEnDate;
         }
         if(iCatCode == 3)
         {
            SQuery                     = "Select Count(*) from Piece_Wage_Cal_Auth where AsOnMonth = "+SAsOnMonth;
         }

         ResultSet rs                  = theStatement.executeQuery(SQuery);

         if(rs.next())
         {
            iCount                     = rs.getInt(1);
         }

         rs                            . close();
         theStatement                  . close();

         if(iCount == 0)
         {
            return true;
         }
      }
      catch(Exception ex)
      {
         ex.printStackTrace();
      }
      return false;
   }

     public String getPureRound(double dAmount,int iScale)
     {
            String str="0";
            try
            {
                  java.math.BigDecimal bd1 = new java.math.BigDecimal(String.valueOf(dAmount));
                  java.math.BigDecimal bd2 = bd1.setScale(iScale,4);
                  str=bd2.toString();
            }
            catch(Exception ex)
            {
            }

            if(toDouble(str) != 0)
            {
               return str;
            }
            else
            {
               return "";
            }   
      }
     

     public String getPureRound(String SAmount,int iScale)
     {
            String str="0";
            try
            {
                  java.math.BigDecimal bd1 = new java.math.BigDecimal(SAmount);
                  java.math.BigDecimal bd2 = bd1.setScale(iScale,4);
                  str=bd2.toString();
            }
            catch(Exception ex)
            {
            }

            if(toDouble(str) != 0)
            {
               return str;
            }
            else
            {
               return "";
            }   
      }

     public String getTimeDiffNew(String SStartDate, String SStartTime, String SEndDate, String SEndTime)
     {
          double dTimeDiff                   = 0;
          String SH                          = "",SM = "";

          StringBuffer SB                    = null;
          SB                                 = new StringBuffer();

          SB.append(" Select (to_Date('"+SEndDate+" "+SEndTime+"','YYYYMMDD HH24:MI') - ");
          SB.append(" to_Date('"+SStartDate+" "+SStartTime+"','YYYYMMDD HH24:MI')) * 24 * 60 from Dual ");

          try
          {
               JDBCConnection theConnect     = JDBCConnection.getJDBCConnection();

               Connection theConnection      = theConnect.getConnection();
               Statement theStatement        = theConnection.createStatement();
               ResultSet theResult           = theStatement.executeQuery(SB.toString());

               if(theResult.next())
               {
                     dTimeDiff               = toDouble(theResult.getString(1));
               }

               theResult                     . close();
               theStatement                  . close();
          }
          catch(Exception ex)
          {
               return "";
          }

          double dHours  = dTimeDiff / 60.0;
          double dMins   = dTimeDiff % 60.0;

          double dActHrs = Math.floor(dHours);

          if(dHours <= 9.0)
          {
               SH        = "0"+getRound(dActHrs,0);
          }
          else
          {
               SH        = getRound(dActHrs,0);
          }

          if(dMins <= 9.0)
          {
               SM        = "0"+getRound(dMins,0);
          }
          else
          {
               SM        = getRound(dMins,0);
          }

          return  SH+":"+SM;
     }

     //date in pure form like (20030401)

     public String getMonthEndDate(String SDate)
     {
            String retStr = "";
            int    iMonth = toInt(SDate.substring(4,6));
            int    iYear  = toInt(SDate.substring(0,4));
            String SDay;
            switch (iMonth)
            {
                  case 1:
                        SDay="31";
                        break;   
                  case 2:
                        if(isLeap(iYear))
                           SDay="29";
                        else
                           SDay="28";
                        break;   
                  case 3:
                        SDay="31";
                        break;   
                  case 4:
                        SDay="30";
                        break;   
                  case 5:
                        SDay="31";
                        break;   
                  case 6:
                        SDay="30";
                        break;   
                  case 7:
                        SDay="31";
                        break;   
                  case 8:
                        SDay="31";
                        break;   
                  case 9:
                        SDay="30";
                        break;   
                  case 10:
                        SDay="31";
                        break;   
                  case 11:
                        SDay="30";
                        break;   
                  case 12:
                        SDay="31";
                        break;
                  default:
                        SDay="30";
                        break;
            }
            return (SDate.substring(0,6)+SDay);
     }

     public String getNextDateThroughNo(String SDate, int iDays)
     {
          SDate               = parseDate(SDate);

          Calendar theCal     = Calendar.getInstance();

          int iYear           = toInt(SDate.substring(6,10)) - 1900;
          int iMonth          = toInt(SDate.substring(3,5)) - 1;
          int iDay            = toInt(SDate.substring(0,2));

          java.util.Date dt   = new java.util.Date(iYear,iMonth,iDay);

          theCal              . setTime(dt);

          theCal              . add(Calendar.DAY_OF_YEAR, iDays);

          int iDate1          = theCal.get(Calendar.DATE);
          int iMonth1         = theCal.get(Calendar.MONTH) + 1;
          int iYear1          = theCal.get(Calendar.YEAR);

          String SCurDate     = iDate1 <= 9 ? "0"+iDate1 : String.valueOf(iDate1);
          String SCurMonth    = iMonth1 <= 9 ? "0"+iMonth1 : String.valueOf(iMonth1);

          return SCurDate+"."+SCurMonth+"."+iYear1;
     }

     public String getMonthDays(String SYearMonth)
     {
            String retStr = "";

            int    iMonth = toInt(SYearMonth.substring(4,6));
            int    iYear  = toInt(SYearMonth.substring(0,4));

            String SDay;

            switch (iMonth)
            {
                  case 1:
                        SDay="31";
                        break;   
                  case 2:
                        if(isLeap(iYear))
                           SDay="29";
                        else
                           SDay="28";
                        break;   
                  case 3:
                        SDay="31";
                        break;   
                  case 4:
                        SDay="30";
                        break;   
                  case 5:
                        SDay="31";
                        break;   
                  case 6:
                        SDay="30";
                        break;   
                  case 7:
                        SDay="31";
                        break;   
                  case 8:
                        SDay="31";
                        break;   
                  case 9:
                        SDay="30";
                        break;   
                  case 10:
                        SDay="31";
                        break;   
                  case 11:
                        SDay="30";
                        break;   
                  case 12:
                        SDay="31";
                        break;
                  default:
                        SDay="30";
                        break;
            }

            return SDay;
     }
     
/*     	public String getPrintPath()
	{
		String SFilePath="";
		String workingdir = System.getProperty("user.dir");
		String sfile = "One.txt";
		File fileName = new File(workingdir,sfile);
		File[] root = fileName.listRoots();
                
		String OSName = System.getProperty("os.name");
		String SActFile="";
		for (int i=0;i<root.length ;i++ )
		{
			String SCurFile = root[i].getAbsolutePath();
			if(OSName.startsWith("Lin"))
			{
				//SActFile = SCurFile;
				SActFile = SCurFile+"soft29"+System.getProperty("file.separator");
				break;
			}
			else
			{
				if(SCurFile.startsWith("D"))
				{
					SActFile = SCurFile;
					break;
				}
			}
		}

          SFilePath = SActFile+"HRDReports"+System.getProperty("file.separator")+"HRDPrn"+System.getProperty("file.separator");
          

		return SFilePath;
	}*/

public String getPrintPath()
	{
		String SFilePath="";
		String workingdir = System.getProperty("user.dir");
		String sfile = "One.txt";
		File fileName = new File(workingdir,sfile);
		File[] root = fileName.listRoots();
                
		String OSName = System.getProperty("os.name");
		String SActFile="";
		for (int i=0;i<root.length ;i++ )
		{
			String SCurFile = root[i].getAbsolutePath();
			if(OSName.startsWith("Lin"))
			{
				//SActFile = SCurFile;
				//SActFile = SCurFile+"soft29"+System.getProperty("file.separator");
//                                SActFile = SCurFile+"soft48"+System.getProperty("file.separator");
                                SActFile = SCurFile+"software"+System.getProperty("file.separator");
				break;
			}
			else
			{
				if(SCurFile.startsWith("D"))
				{
					SActFile = SCurFile;
					break;
				}
			}
		}

                
            SFilePath = SActFile+System.getProperty("file.separator");
                System.out.println("FilePath inside common===>"+SFilePath);
          //SFilePath = SActFile+"Reports"+System.getProperty("file.separator")+"HRDPrn"+System.getProperty("file.separator");
          

		return SFilePath;
	}

     public String getServerPureDate()
     {
	  String SDate = "";
          String SQry="select to_Char(sysdate,'yyyymmdd') from dual";
          try
          {
			 JDBCConnection connect= JDBCConnection.getJDBCConnection();
			Connection theConnection = connect.getConnection();
			Statement theStatement = theConnection.createStatement();
			ResultSet theResult = theStatement.executeQuery(SQry);
                        if(theResult.next())
                               SDate = theResult.getString(1);

               System.out.println(" ServerDate--->"+SDate);
               theResult.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               return "";
          }
          return SDate;
     }

     public String getServerDateandTime()
     {
          String SDate="";

          Connection theConnection = null;
          
          String SQry="select to_Char(sysdate,'DD.MM.YYYY hh24:mi:ss') from dual";
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnect  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","hrdnew","hrdnew");
               Statement stat   = theConnect.createStatement();
               ResultSet theResult = stat.executeQuery(SQry);
               if(theResult.next())
                    SDate= theResult.getString(1);

               theResult.close();
               stat.close();
          }
          catch(Exception ex)
          {
               return "";
          }
          return SDate;
     }

     	public String getPDFPath()
	{
		String SFilePath="";
		String workingdir = System.getProperty("user.dir");
		String sfile = "One.txt";
		File fileName = new File(workingdir,sfile);
		File[] root = fileName.listRoots();
                
		String OSName = System.getProperty("os.name");
		String SActFile="";
		for (int i=0;i<root.length ;i++ )
		{
			String SCurFile = root[i].getAbsolutePath();
			if(OSName.startsWith("Lin"))
			{
				//SActFile = SCurFile;
				SActFile = SCurFile+"soft29"+System.getProperty("file.separator");
				break;
			}
			else
			{
				if(SCurFile.startsWith("D"))
				{
					SActFile = SCurFile;
					break;
				}
			}
		}

          SFilePath = SActFile+"HRDReports"+System.getProperty("file.separator")+"HRDPrn"+System.getProperty("file.separator");
          

		return SFilePath;
	}


    
}
