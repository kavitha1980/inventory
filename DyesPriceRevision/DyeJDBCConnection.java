
package DyesPriceRevision;
import java.sql.*;

public class DyeJDBCConnection {
     
     static DyeJDBCConnection connect = null;
     Connection theConnection = null;
     
     String SDriver   = "oracle.jdbc.OracleDriver";
     String SDSN      = "jdbc:oracle:thin:@192.168.252.39:1521:amardye2";
     String SUser     = "dyeraw";
     String SPassword = "dyeraw";

private DyeJDBCConnection(){
        super();
		try{
               Class          . forName(SDriver);
               theConnection  = DriverManager.getConnection(SDSN,SUser,SPassword);
		}
		catch(Exception e)
		{
               System.out.println("JDBCConnection : "+e);
		}
	}

public static DyeJDBCConnection getJDBCConnection(){
        if (connect == null){
               connect = new DyeJDBCConnection();
	    }
		return connect;
	}

public Connection getConnection(){
		return theConnection;
	}

public void finalize(){
           if (theConnection != null){
        	try{
				theConnection.close();
    		   }
		catch (Exception e){
				e.printStackTrace();
			}
		}
	}
 
    

}
