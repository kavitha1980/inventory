
package DyesPriceRevision;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.border.TitledBorder;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Vector;
import java.util.HashMap;


public class DyeList extends JDialog {
    
 JPanel                         pnlMain,pnlReason,pnlButton,pnlList;
 JTable                         reasonTable;
 JButton                        btnOK;
  int                           iRow;
  String                        sDyesName,sDyesCode;
  JList                         theList;
  DefaultListModel              theListModel;
  DyeingData                    data     ;
  
public DyeList(int iRow, DyeingData data) {
 this.iRow               = iRow;
 this.data               = data;
 createComponent();
 setLayout();
 addComboBoxItem();
 addComponent();
 addListener();
 setData();
}

private void createComponent(){
 
 pnlMain                        = new JPanel();
 pnlButton                      = new JPanel();
 pnlReason                      = new JPanel();
 pnlList                        = new JPanel();
 btnOK                          = new JButton("Ok");

 theListModel                   = new DefaultListModel();
 theList                        = new JList(theListModel);
 
 
}
private void addComboBoxItem(){
}
private void setLayout(){
    
    pnlMain                    . setLayout(new BorderLayout());
    pnlList                    . setBorder(new TitledBorder("DyesName"));
    pnlList                    . setLayout(new BorderLayout());

    this                        . setModal(true);
    this                        . setSize(250,300);
    this                        . setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    this                        . setTitle("Dyes Name");
    this                        . setResizable(false);
    
}
private void addComponent(){
    
  pnlList                       . add(new JScrollPane(theList));
  this                          . add(pnlList);
          
}
private void addListener(){
   theList                     .addKeyListener(new KeyList());
}
private class KeyList extends KeyAdapter{
   public KeyList(){
        
    }
public void keyReleased(KeyEvent ke){
 if(ke.getKeyCode()==KeyEvent.VK_ENTER){
   try{
        sDyesName             = (String)theList.getSelectedValue();
        sDyesCode             = data.getDyesCode(sDyesName);
        System.out.println("Dyes Name==>"+sDyesName+"<--->"+sDyesCode);   
        dispose();
     }
   catch(Exception e){
          e.printStackTrace();
          System.out.println("Inside KeyListener:"+e);
      }
    }
  }
}

private void setData(){
    //data            = new DyeingData();
   // data.setDyesName();
    System.out.println("Data Size inside Dyes=====>"+data.VExDyesName.size());
    for(int i=0;i<data.VExDyesName.size();i++){
        String sDyesName       = (String)data.VExDyesName.elementAt(i);
        String sDyesCode       = (String)data.VExDyesCode.elementAt(i);
        
        theListModel.addElement(sDyesName);
   }   
}


}
