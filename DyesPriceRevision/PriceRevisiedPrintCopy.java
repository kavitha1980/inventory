
package DyesPriceRevision;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.GridLayout;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;
import java.awt.Component;
import java.awt.event.*;
import java.io.*;

public class PriceRevisiedPrintCopy extends JPanel
{
    JPanel                  TopPanel,MiddlePanel,pnlMain,ItemPanel,pnlZero;
    JComboBox               JCSupplier;
    JEditorPane             pnlEditor;
    JButton                 BExit,BPrint;
    JRadioButton            rbtnWithZero, rbtnWithoutZero;
    Connection              theConnection;
    Connection              theDyeConnection;
    Vector                  VSupplierCode,VSupplierName;
    ArrayList               ADataList;
    Common                  common = new Common();
    JList                   theItemList;
    DefaultListModel        theItemListModel;
    Vector                  VItemCode,VItemName;
    ArrayList               AItemList  = new ArrayList();
    ArrayList               ADyesCodeList,ADyesNameList ;
    ArrayList               ARateDateList   = new ArrayList();
    StringBuffer            SItemCode,SItemName;
    String                  SFileName, SFileOpenPath, SFileWritePath;
    double                  dPrevRateperKg=0.00, dPrevTotalValue=0.00, dPrevNetValue=0.00;
    double                  dGrTotalValue=0.00, dGrNetValue=0.00;
    double                  dDiffTotalValue=0.00, dDiffNetValue=0.00, dDiffRateperKg=0.00;
    double                  dNewRateperKg=0.00, dNewTotalValue=0.00, dNewNetValue=0.00;
    double                  dPurNetValue=0.00, dPurRatperKg=0.00, dPurTotalValue=0.00;
    
    Document                document;
    PdfPTable               table;
   // int[]                   iColWidth = {10,45,25,15,15,15,15,15,25,25,1,15,15,15,15,15,25,25,1,15,25,15,15 };
     int[]                   iColWidth ;
    PdfPCell                c1;
    private static Font     bigbold = FontFactory.getFont("TIMES_ROMAN", 15, Font.BOLD);
    private static Font     mediumbold = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
    private static Font     mediumbold1 = FontFactory.getFont("TIMES_ROMAN", 6, Font.BOLD);
    private static Font     smallnormal = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
    private static Font     smallbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font     smallbold1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    double[]                dGNetTotal,dGTotalValue;
    double[]                dOldGNetTotal,dOldGTotalValue;
    double[]                dDiffGNetTotal,dDiffGTotalValue;
    //double[]                dDiffGNetTotal,dDiffGTotalValue;
    int                     iGOldCount=0;
    int                     iGNewCount=0;
    int                     iDiffCount=0;
    
    
    public PriceRevisiedPrintCopy()
    {
        SetSupplier();
        CreateComponent();
        addListeners();
        SetLayout();
        // SetData();
         //SetReport();
    }
    public void SetSupplier()
    {
        VSupplierCode = new Vector();
        VSupplierName = new Vector();
        
         try 
         {
             theDyeConnection = null;
             
             StringBuffer sb1 = new StringBuffer();
                
            sb1.append("  Select t.Sup_Code,Supplier.Name ");
            sb1.append("  from ");
            sb1.append("  (select  distinct PurchaseOrder.Sup_Code from PurchaseOrder @amarml");
            sb1.append("  Inner join InvItems on InvItems.Item_Code=PurchaseOrder.Item_Code ");
            sb1.append("  and StkGroupCode='C01' ) t ");
            sb1.append("  Inner join supplier @amarml on Supplier.Ac_Code=t.Sup_Code ");
            sb1.append("  order by 2 ");

             if(theDyeConnection == null) 
             {
                DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                theDyeConnection = jdbc.getConnection();
             }
             PreparedStatement thePrepare = theDyeConnection.prepareStatement(sb1.toString());
             ResultSet theResult = thePrepare.executeQuery();
             while (theResult.next()) 
             {
                 VSupplierCode.add(theResult.getString(1));
                 VSupplierName.add(theResult.getString(2));
             }
            theResult.close();
            thePrepare.close();       
         }
         catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex);
         }
    }
    
    /*public void SetData(){
        try{
             StringBuffer sb = new StringBuffer();
             ADataList = new ArrayList();
             
            String SPartyCode = getSupplierCode((String)JCSupplier.getSelectedItem());

            sb.append(" Select ItemCode,ItemName,sum(AnnualConsumptionNewRate) as AnnualConNew,sum(RatePerKgNewRate) as RatePerKgNew,  ");
            sb.append(" sum(DiscPerNewRate) as DiscPerNew,sum(DiscNewRate) as DiscNew,sum(TaxPerNewRate) as TaxPerNew,sum(TaxNewRate) as TaxNew,sum(NetValueNewRate) as NetValueNew, ");
            sb.append(" sum(TotalValueNewRate) as TotalValueNew,sum(AnnualConsumptionOldRate) as AnnualConOld,sum(RatePerKgOldRate) as RatePerKgOld ");
            sb.append(" ,sum(DiscPerOldRate) as DiscPerOld,sum(DiscOldRate) as DiscOld,sum(TaxPerOldRate) as TaxPerOld,sum(TaxOldRate) as TaxOld,sum(NetValueOldRate) as NetValueOld,  ");
            sb.append(" sum(TotalValueOldRate) as TotalValueOld ,sum(LabStatusNew) as LabStatusNew,sum(LabStatusOld) as LabStatusOld ");
            sb.append(" from ( ");
            sb.append(" select InvItems.Item_Code as ItemCode ,InvItems.ITEM_Name as ItemName,");
            sb.append(" DyesPriceRevision.AnnualConsumption as AnnualConsumptionNewRate,  DyesPriceRevision.RatePerKg as RatePerKgNewRate,");
            sb.append(" DyesPriceRevision.DiscPer as DiscPerNewRate,DyesPriceRevision.Discount as DiscNewRate ");
            sb.append(" ,DyesPriceRevision.TaxPer as TaxPerNewRate,  DyesPriceRevision.TAX as TaxNewRate, ");
            sb.append(" DyesPriceRevision.NetValue as NetValueNewRate,");
            sb.append(" DyesPriceRevision.TotalValue as TotalValueNewRate  ,0 as AnnualConsumptionOldRate,0 as RatePerKgOldRate, 0 as DiscPerOldRate,0 as DiscOldRate,");
            sb.append(" 0 as TaxPerOldRate,0 as TaxOldRate,0 as NetValueOldRate, 0 as TotalValueOldRate  ,DyesPriceRevision.LabStatus as LabStatusNew,'0' as LabStatusOld ");
            sb.append(" from DyesPriceRevision   ");
            sb.append(" inner join  invitems on invitems.ITEM_CODE =dyespricerevision.DYES_CODE   ");
            sb.append(" inner join  stockgroup on groupcode = invitems.STKGROUPCODE   and  groupcode='C01' ");
            sb.append(" where SUPP_CODE='"+SPartyCode+"' and PRICEREVISIONSTATUS = 0 ");
            sb.append(" Union All ");
            sb.append(" select Dyes_InvItems.Dyes_Item_Code as ItemCode ,Dyes_InvItems.Dyes_ITEM_Name as ItemName,");
            sb.append(" DyesPriceRevision.AnnualConsumption as AnnualConsumptionNewRate,  DyesPriceRevision.RatePerKg as RatePerKgNewRate,");
            sb.append(" DyesPriceRevision.DiscPer as DiscPerNewRate,DyesPriceRevision.Discount as DiscNewRate");
            sb.append(" ,DyesPriceRevision.TaxPer as TaxPerNewRate,  DyesPriceRevision.TAX as TaxNewRate,");
            sb.append(" DyesPriceRevision.NetValue as NetValueNewRate,");
            sb.append(" DyesPriceRevision.TotalValue as TotalValueNewRate  ,0 as AnnualConsumptionOldRate,0 as RatePerKgOldRate, 0 as DiscPerOldRate,0 as DiscOldRate,");
            sb.append(" 0 as TaxPerOldRate,0 as TaxOldRate,0 as NetValueOldRate, 0 as TotalValueOldRate  ,DyesPriceRevision.LabStatus as LabStatusNew,'0' as LabStatusOld ");
            sb.append(" from DyesPriceRevision   ");
            sb.append(" inner join  Dyes_InvItems on Dyes_InvItems.Dyes_ITEM_CODE =Dyespricerevision.DYES_CODE   ");
            sb.append(" where SUPP_CODE='"+SPartyCode+"' and PRICEREVISIONSTATUS = 0 ");
            sb.append(" Union All ");
            sb.append(" select InvItems.Item_Code as ItemCode,InvItems.ITEM_Name as ItemName,0 as AnnualConsumptionNewRate,0 as RatePerKgNewRate, ");
            sb.append(" 0 as DiscPerNewRate, 0 as DiscNewRate,0 as TaxPerNewRate,0 as TaxNewRate,0 as NetValueNewRate, 0 as TotalValueNewRate  ,DyesPriceRevision.AnnualConsumption as AnnualConsumptionOldRate,  ");
            sb.append(" DyesPriceRevision.RatePerKg as RatePerKgOldRate,DyesPriceRevision.DiscPer as DiscPerOldRate, DyesPriceRevision.Discount as DiscOldRate, ");
            sb.append(" DyesPriceRevision.TaxPer as TaxPerOld,DyesPriceRevision.TAX as TaxOldRate,");
            sb.append(" DyesPriceRevision.NetValue as NetValueOld,DyesPriceRevision.TotalValue as TotalValueOld , '0' as LabStatusNew");
            sb.append(" ,DyesPriceRevision.LabStatus as LabStatusOld ");
            sb.append(" from dyespricerevision   ");
            sb.append(" inner join  invitems on invitems.ITEM_CODE =dyespricerevision.DYES_CODE   ");
            sb.append(" inner join  stockgroup on groupcode = invitems.STKGROUPCODE   and  groupcode='C01' ");
            sb.append(" where SUPP_CODE='"+SPartyCode+"' and  PRICEREVISIONSTATUS =1 ");
            sb.append(" and DyesPriceRevision.ID in  ( Select Max(ID) from DyesPriceRevision  where SUPP_CODE='"+SPartyCode+"' and PriceRevisionStatus =1 group by DYES_CODE	)  ");
            sb.append(" Union All");
            sb.append(" select Dyes_InvItems.Dyes_Item_Code as ItemCode,Dyes_InvItems.Dyes_ITEM_Name as ItemName,0 as AnnualConsumptionNewRate,0 as RatePerKgNewRate, ");
            sb.append(" 0 as DiscPerNewRate, 0 as DiscNewRate,0 as TaxPerNewRate,0 as TaxNewRate,0 as NetValueNewRate, 0 as TotalValueNewRate  ,DyesPriceRevision.AnnualConsumption as AnnualConsumptionOldRate,  ");
            sb.append(" DyesPriceRevision.RatePerKg as RatePerKgOldRate,DyesPriceRevision.DiscPer as DiscPerOldRate, DyesPriceRevision.Discount as DiscOldRate,");
            sb.append(" DyesPriceRevision.TaxPer as TaxPerOld,DyesPriceRevision.TAX as TaxOldRate,");
            sb.append(" DyesPriceRevision.NetValue as NetValueOld,DyesPriceRevision.TotalValue as TotalValueOld , '0' as LabStatusNew ");
            sb.append(" ,DyesPriceRevision.LabStatus as LabStatusOld ");
            sb.append(" from dyespricerevision   ");
            sb.append(" inner join  Dyes_InvItems on Dyes_InvItems.Dyes_ITEM_CODE =dyespricerevision.DYES_CODE   ");
            sb.append(" where SUPP_CODE='"+SPartyCode+"' and  PRICEREVISIONSTATUS =1 ");
            sb.append(" and DyesPriceRevision.ID in  ( Select Max(ID) from DyesPriceRevision  where SUPP_CODE='"+SPartyCode+"' and PriceRevisionStatus =1 group by DYES_CODE	)  ");
            sb.append(" )group by ItemCode,ItemName ");
        
            theConnection = null;
            
             if(theConnection == null) 
             {
                JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                theConnection = jdbc.getConnection();
             }
             PreparedStatement thePrepare = theConnection.prepareStatement(sb.toString());
             ResultSet theResult = thePrepare.executeQuery();
             ResultSetMetaData rsmd = theResult.getMetaData();
             while (theResult.next()) 
             {
                 HashMap themap = new HashMap();
                 for (int i = 0; i < rsmd.getColumnCount(); i++) 
                 {
                    themap.put(rsmd.getColumnName(i + 1), theResult.getString(i + 1));
                 }
                ADataList.add(themap);
            }
            theResult.close();
            thePrepare.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e);
        }
    }*/
    
    public void SetData(){
        try{
             StringBuffer sb            = new StringBuffer();
             ADataList                  = new ArrayList();
             ADyesNameList              = new ArrayList();
             ADyesCodeList              = new ArrayList();
             String sDyesName           = "", sDyesCode="";
             String SPartyCode          = getSupplierCode((String)JCSupplier.getSelectedItem());
             
             String sServerDate         = common.getServerDate();
             String sCurDate            = sServerDate.substring(6,8);
             String sCurMonth           = sServerDate.substring(4,6);
             String sCurYear            = sServerDate.substring(0, 4);

             int iStYear                = common.toInt(sCurYear)-1;
             String sStDate             = String.valueOf(iStYear)+sCurMonth+sCurDate;
        
             for(int i=0;i<AItemList.size();i++){
               String sItemName         = (String)AItemList.get(i);
               StringTokenizer     st   = new StringTokenizer(sItemName,"#") ;
               while (st.hasMoreTokens()){
                  sDyesName             = st.nextToken();
                  sDyesCode             = st.nextToken();
                  ADyesCodeList         . add(sDyesCode);
                  ADyesNameList         . add(sDyesName);
                }
              }
             
          //  System.out.println("Item Code List===>"+ADyesCodeList);

            sb.append(" Select sum(ID) as ID ,sum(Rate) as Rate,sum(Disper) as DisPer,sum(Taxper) as TaxPer,sum(IssueQty) as Consumption,ItemName,ItemCode,");
            sb.append(" PurStatus,PriceRevisionStatus,NewRateDate, WithEffectTo,RateRank ,Labstatus  from (");
            sb.append(" Select 0 as ID, 0 as Rate,0 as DisPer,0 as TaxPer,sum(Issue.Qty) as IssueQty,Item_Name as ItemName,Issue.Code as ItemCode,1 as PurStatus, 2 as PriceRevisionStatus,'' as NewRateDate,");
            sb.append(" 0 as WithEffectTo,0 as RateRank, ''  as Labstatus from Issue @amarml ");
            sb.append(" Inner Join InvItems  on Issue.Code = InvItems.Item_Code ");
         //   sb.append(" Inner Join PurchaseOrder on InvItems.Item_Code = PurchaseOrder.Item_Code ");
            //sb.append(" Inner Join ISSUE  on Issue.Code = InvItems.Item_Code ");
            sb.append(" Where  Issue.Code in( '");
            //sb.append(" Where PurchaseOrder.Sup_Code='"+SPartyCode+"'  and Issue.Code in( '");
            for(int a=0;a<ADyesCodeList.size();a++){
                String sLstDyesCode    = (String)ADyesCodeList.get(a);
                if(a==0){
                   sb.append(""+sLstDyesCode+"' ");
                   }else{
                       sb.append(",'"+sLstDyesCode+"'");
                   }
            }
            sb.append(" )");
            if (rbtnWithZero.isSelected()) {
                 sb.append("  and IssueDate>="+sStDate+" and IssueDate<="+sServerDate);
             }
            
            sb.append(" Group by Item_Name,Issue.Code ");

            sb.append(" Union All ");
            
            sb.append(" Select PurchaseOrder.ID,PurchaseOrder.Rate,PurchaseOrder.Discper,PurchaseOrder.TaxPer,0 as IssueQty, Item_Name, ");
            sb.append(" PurchaseOrder.Item_Code as ITemCode,1 as PurStatus ,2 as  PriceRevisionStatus ,  '' as NewRateDate,");
            sb.append(" 0 as WithEffectTo, 0 as RateRank , ''  as Labstatus from  PurchaseOrder @amarml ");
            sb.append(" Inner Join InvItems on InvItems.Item_Code = PurchaseOrder.Item_Code ");
            
            sb.append(" Where PurchaseOrder.Sup_Code='"+SPartyCode+"'");
            sb.append(" and PurchaseOrder.ID in (Select Max(ID) from PurchaseOrder @amarml  where SUP_CODE='"+SPartyCode+"' and Item_code in ('");
            for(int a=0;a<ADyesCodeList.size();a++){
                String sLstDyesCode    = (String)ADyesCodeList.get(a);
                if(a==0){
                   sb.append(""+sLstDyesCode+"' ");
                   }else{
                       sb.append(",'"+sLstDyesCode+"'");
                   }
            }
            sb.append(" ) Group by Item_Code ");
            sb.append(" ) ");
            sb.append(" ) ");
            sb.append(" Group by ItemName,ItemCode,PurStatus,PriceRevisionStatus,NewRateDate,WithEffectTo,RateRank,Labstatus   ");

            sb.append(" Union All ");
            
            sb.append(" Select ID,RatePerKg,DiscPer,TaxPer,AnnualConsumption,ItemName,ItemCode, PurStatus,PriceRevisionStatus,NewRateDate,WithEffectTo ,RateRank , LabStatus from ( ");
            sb.append(" Select DyesPriceRevision.ID,RatePerKg,DiscPer,TaxPer,0 as AnnualConsumption, ");
            sb.append(" decode (Dyes_Item_Name,'',InvItems.ITEM_NAME,Dyes_InvItems.Dyes_Item_Name) as ItemName,DyesPriceRevision.Dyes_Code as ItemCode,0 as PurStatus, ");
            sb.append(" DyesPriceRevision.PriceRevisionStatus,to_Char(DyesPriceRevision.WithEffectFrom) as NewRateDate, ");
             sb.append(" DyesPriceRevision.WithEffectTo ,rank() over(partition by DyesPriceRevision.Dyes_Code order by DyesPriceRevision.WithEffectFrom desc) as RateRank,");
             
            //sb.append(" DyesPriceRevision.PriceRevisionStatus,to_Char(to_Date(DyesPriceRevision.EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') as NewRateDate, ");
           // sb.append(" DyesPriceRevision.WithEffectTo ,rank() over(partition by DyesPriceRevision.Dyes_Code order by to_Char(to_Date(DyesPriceRevision.EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') desc) as RateRank,");
            sb.append(" Case When Shadewisedyes.Labstatus=1 then 'OK' ");
            sb.append(" When Shadewisedyes.Labstatus=2 then 'NotOK' ");
	    sb.append(" else ");
            sb.append(" 'Not Made'");
            sb.append(" End  as LabStatus ");
                                 
            sb.append(" from DyesPriceRevision ");
            sb.append(" Left Join Dyes_InvItems on Dyes_InvItems.Dyes_Item_Code = DyesPriceRevision.Dyes_Code ");
            sb.append(" Left join Shadewisedyes on shadewisedyes.DYES_CODE=dyespricerevision.DYES_CODE and ShadewiseDyes.PriceRevisionStatus=0" );
            sb.append(" Left Join InvItems on InvItems.Item_Code = DyesPriceRevision.Dyes_Code ");
          
            sb.append(" Where DyesPriceRevision.Supp_Code = '"+SPartyCode+"'  and DyesPriceRevision.Dyes_Code in ('");
            
            for(int a=0;a<ADyesCodeList.size();a++){
                String sLstDyesCode    = (String)ADyesCodeList.get(a);
                if(a==0){
                   sb.append(""+sLstDyesCode+"' ");
                   }else{
                       sb.append(",'"+sLstDyesCode+"'");
                   }
            }
            sb.append(" )");
            
            sb.append(" Order by DyesPriceRevision.ID asc ");
            sb.append(" ) ");
            sb.append(" Where RateRank<=3 ");
            sb.append(" Order by 9 desc");
            //sb.append(" Order by 10 asc");
            //sb.append(" Order by PurStatus desc");

         //   System.out.println("Price REv Qry=>"+sb.toString());
            theDyeConnection = null;
            
             if(theDyeConnection == null) {
                DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                theDyeConnection = jdbc.getConnection();
             }
             PreparedStatement thePrepare = theDyeConnection.prepareStatement(sb.toString());
             ResultSet theResult = thePrepare.executeQuery();
             ResultSetMetaData rsmd = theResult.getMetaData();
             
             while (theResult.next()) {
                 HashMap themap = new HashMap();
                 for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), theResult.getString(i + 1));
                 }
                ADataList.add(themap);
            }
            theResult.close();
            thePrepare.close();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e);
        }
    }
    
    
    private void CreateComponent() {
        TopPanel            = new JPanel();
        MiddlePanel         = new JPanel();
        pnlMain             = new JPanel();
        ItemPanel           = new JPanel();
        pnlZero             = new JPanel();
        JCSupplier          = new JComboBox(VSupplierName);
        pnlEditor           = new JEditorPane();
        
        BExit               = new JButton("Exit");
        BPrint              = new JButton("Print PDF");
        
        rbtnWithZero        = new JRadioButton("WithZero", true);
        rbtnWithoutZero     = new JRadioButton("WithoutZero", true);
    
        theItemListModel    = new DefaultListModel();
        theItemList         = new JList(theItemListModel);
        theItemList         .setVisibleRowCount(5);
    }
    private void SetLayout() 
    {
       // setTitle("Price Revisied Print");
        setVisible(true);
      //  setResizable(true);
        setBounds(0, 0, 1300, 500);
        
        ItemPanel          .    setBorder(new TitledBorder("Select Dyes Name"));
        ItemPanel          .    setLayout(new BorderLayout());
        ItemPanel          .    add(new JScrollPane(theItemList));
        
        TopPanel           .    setBorder(new TitledBorder("Filter"));
        TopPanel           .    setLayout(new GridLayout(1,4));
        //TopPanel.setLayout(new FlowLayout());
        
        ButtonGroup group1 = new ButtonGroup();
        group1          .       add(rbtnWithZero);
        pnlZero         .       add(rbtnWithZero);
        group1          .       add(rbtnWithoutZero);
        pnlZero         .       add(rbtnWithoutZero);
        
        TopPanel        .       add(new JLabel("Supplier Name"));
        TopPanel        .       add(JCSupplier);
        TopPanel        .       add(ItemPanel);
        TopPanel        .       add(pnlZero);
        TopPanel        .       add(BPrint);
        TopPanel        .       setLayout(new FlowLayout());
        
        MiddlePanel     .       setBorder(new TitledBorder("Report Details"));
        MiddlePanel     .       setLayout(new BorderLayout());
        MiddlePanel     .       add(pnlEditor);
        pnlEditor       .       setEditable(false);
        pnlMain         .       setLayout(new BorderLayout());
        
        pnlMain         .       add("North",TopPanel);
        pnlMain         .       add("Center",MiddlePanel);
    }
    
    private void addListeners() {
        BExit           .       addActionListener(new ActList());
        BPrint          .       addActionListener(new ActList());
        JCSupplier      .       addItemListener(new ItemList());
    }
    private class ActList implements ActionListener 
    {
        public void actionPerformed(ActionEvent ae) 
        {
           
            if (ae.getSource() == BExit)
            {
               // dispose();
            }
            if (ae.getSource() == BPrint)
            {
                SetData();
                SetReport();
                CreatePDFFile();
            }
        }
     }
    
 public class ItemList implements ItemListener {
      public void itemStateChanged(ItemEvent ie){
           try{
                if (ie.getSource() == JCSupplier && ie.getStateChange()==ie.SELECTED) {
                        SetItem();
                        SetItemList();
                      //  SetData();
                       // SetReport();
                     }
               }
               catch(Exception ex){
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null,ex,"Price Revisied Frame",JOptionPane.ERROR_MESSAGE);
               }
          }
     }
     
private void SetItemList(){
  theItemListModel.removeAllElements();
   for (int i = 0; i <VItemCode.size(); i++) {
            String SItemName = (String)VItemName.elementAt(i);
            String SItemCode = (String)VItemCode.elementAt(i);
            
            theItemListModel.addElement(new CheckBoxItem(SItemName+"#"+SItemCode));
        }
        theItemList.setCellRenderer(new CheckListRendererItm());
        theItemList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        theItemList.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
                
                JList jlist = (JList) event.getSource();
                int index = jlist.locationToIndex(event.getPoint());
                CheckBoxItem Citem = (CheckBoxItem) jlist.getModel().getElementAt(index);
                Citem.setSelected(!Citem.isSelected());
                jlist.repaint(jlist.getCellBounds(index, index));
                String sDyesName = String.valueOf(Citem);
                
                if (Citem.isSelected() == true) {
                    AItemList.add(sDyesName);
                   // System.out.println("Add Party List ---->" + AItemList);
                    for (int j = 0; j < AItemList.size(); j++) {
                        //lblSelectedCount.setText(String.valueOf(APartyList));
                    }
                }
                
                if (Citem.isSelected() == false){
                    String SItemName = sDyesName;
                    AItemList.remove(SItemName);
                    System.out.println("Party List --Remove-->" + AItemList);
                    //lblSelectedCount.setText(String.valueOf(AOrderTypeList));
                }
            }
        });
    }
    
    public class CheckBoxItem {
        private String label;
        boolean isSelected = false;

        public CheckBoxItem(String label) {
            this.label = label;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean isSelected) {
            this.isSelected = isSelected;
        }

        public String toString() {
            return label;
        }
    }

    class CheckListRendererItm extends JCheckBox implements ListCellRenderer {
        public Component getListCellRendererComponent(JList list, Object value, int index,boolean isSelected, boolean hasFocus) {
            setEnabled(list.isEnabled());
            setSelected(((CheckBoxItem) value).isSelected());
            setFont(list.getFont());
            setBackground(list.getBackground());
            setForeground(list.getForeground());
            setText(value.toString());
            return this;
        }
    }

    public void SetReport() {
        try {
            String sReport = "";
            sReport = getReport();
            pnlEditor.setContentType("text/html");
            pnlEditor.setText(sReport);
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getSupplierCode(String SPartyName)
    {
        int iIndex = VSupplierName.indexOf(SPartyName);
	return (String)VSupplierCode.elementAt(iIndex);
    }
    
    
    public void SetItem(){
   
        VItemCode = new Vector();
        VItemName = new Vector();
        String sSupplierName    = (String)JCSupplier.getSelectedItem();
        String sSupplierCode    = getSupplierCode((String)JCSupplier.getSelectedItem());
   //     System.out.println("SupplierName--------->"+sSupplierName+"<--->"+sSupplierCode);
        try {
            theConnection = null;

            StringBuffer sb1 = new StringBuffer();

           /* sb1.append("  Select distinct InvItems.Item_Code,Item_Name from InvItems  ");
            sb1.append("  inner join  PurchaseOrder @amarml on invitems.ITEM_CODE =PurchaseOrder.Item_Code  ");   
            sb1.append("  Where StkGroupCode='C01'  and PurchaseOrder.SUP_CODE='"+sSupplierCode+"'");
            sb1.append("  Union All ");
            sb1.append("  Select distinct Dyes_Item_Code,Dyes_Item_Name from DYES_INVITEMS");
            sb1.append("  inner join  Dyespricerevision on Dyes_InvItems.Dyes_ITEM_CODE =Dyespricerevision.DYES_CODE ");
            sb1.append("  Where SUPP_CODE='"+sSupplierCode+"'");
            sb1.append("  order by 2 ");
            */
            
            sb1.append("  Select distinct DYEINGINVITEMS.Item_Code,Item_Name from DYEINGINVITEMS");
            sb1.append("  Inner Join InvItems on InvItems.Item_Code = DyeingInvItems.Item_Code ");
            sb1.append("  inner join  Dyespricerevision@amardye2 on InvItems.ITEM_CODE =Dyespricerevision.DYES_CODE ");
            sb1.append("  Where SUPP_CODE='"+sSupplierCode+"'");
            sb1.append("  order by 2 ");
            
          //  System.out.println("Itmes--->"+sb1.toString());

            if (theConnection == null) {
                JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                theConnection = jdbc.getConnection();
            }
            PreparedStatement thePrepare = theConnection.prepareStatement(sb1.toString());
            ResultSet theResult = thePrepare.executeQuery();
            while (theResult.next()) {
                VItemCode.add(theResult.getString(1));
                VItemName.add(theResult.getString(2));
            }
            theResult.close();
            thePrepare.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex);
        }
    }
  /*  
    private String getReport(){
        String SPartyName = (String)JCSupplier.getSelectedItem();
        String sStr = "";
                             
        double dPurchasedRateperKg=0.00, dPurchasedTotalValue=0.00, dOldConsumption=0.00, dOldNetValue=0.00;
        double dNewRateperKg = 0.00, dNewTotalValue=0.00, dNewNetValue=0.00,dDiffPercentage=0.00;
        double dRateDiff=0.00, dTotalValueDiff=0.00;
       
        String sHeading = "REVISED PRICE IN "+SPartyName+" - "+common.parseDate(common.getCurrentDate())+" ";
        
        try 
        {
            sStr = "<html><head>";
            sStr = sStr + "Revisied Price List";
            sStr = sStr + "</head>";
            sStr = sStr + "<body >";
            sStr = sStr + "<h2 align ='center'> <b>" + sHeading + "</b></h2>";
            sStr = sStr + "<table border='1' width='70%' bgcolor='lightGrey'>";
            sStr = sStr + "<tr>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>SL NO </b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>DYES NAME</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>ONE YEAR CONSUMPTION QTY</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' colspan='7'><font color='#000099' ><b>PURCHASED RATE</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>&nbsp;</b></font></td>";
           
            //int iPurStatusCount=getPurStatusCount();
            
            int iPriceRevisionCount =  setMaxCount();
            for (int i = 0; i < ADataList.size(); i++) {
                HashMap themap      = (HashMap) ADataList.get(i);
                String sRevStatus   = (String)themap.get("PRICEREVISIONSTATUS");
                int iRevStatus      = common.toInt(sRevStatus);
                if(iRevStatus==0){
            
           for(int j=0;j<iPriceRevisionCount;j++) { 
               String sDate    = common.parseDate((String)themap.get("NEWRATEDATE"));
               System.out.println("New Rate Date====>"+sDate+"---"+iRevStatus);
            sStr = sStr + "<td align='center' color='#000099' colspan='7'><font color='#000099' ><b>NEW RATE("+sDate+")</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>&nbsp;</b></font></td>";
            break;
                }
              }
            }
            sStr = sStr + "<td align='center' color='#000099' colspan='3'><font color='#000099' ><b>DIFFERENCE</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>LAB STATUS</b></font></td>";
            sStr = sStr + "</tr>";
            
            sStr = sStr + "<tr>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2'><font color='#000099' ><b>Discount  </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2'><font color='#000099' ><b>Tax  </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Net value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Total</b></font></td>";
            for(int i=0;i<iPriceRevisionCount;i++){
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2'><font color='#000099' ><b>Discount  </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2'><font color='#000099' ><b>Tax  </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Net Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Total</b></font></td>";
            }
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Total Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>%</b></font></td>";
            sStr = sStr + "</tr>";
            
            sStr = sStr + "<tr>";
            
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
            for(int i=0;i<iPriceRevisionCount;i++){
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
            }
            sStr = sStr + "</tr>";
          /*  
            for (int i = 0; i < ADataList.size(); i++) {
                HashMap themap      = (HashMap) ADataList.get(i);
                String sPurStatus   = (String)themap.get("PURSTATUS");
                int iPurStatus      = common.toInt(sPurStatus);
                if(iPurStatus==1){
                String SConsumption = (String) themap.get("CONSUMPTION");
                double dRatePerKg   = common.toDouble((String) themap.get("RATE"));
                double dDiscPer     = common.toDouble((String) themap.get("DISPER"));
                double dTaxPer      = common.toDouble((String) themap.get("TAXPER"));
                double dDisValue    = (dRatePerKg*dDiscPer)/100;
                double dTaxValue    = (dRatePerKg*dTaxPer)/100;
                double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
                double dTotalValue  = common.toDouble(SConsumption)*dNetValue;
                dOldConsumption     = common.toDouble(SConsumption);
                }
            }
            
            double[] dTotal = new double[20];
            ArrayList     APriceList    = new ArrayList();
            //System.out.println("DyesCode List===>"+ADyesCodeList);
            for (int i = 0; i < ADyesCodeList.size(); i++) {
                String sDyesCode       = (String)ADyesCodeList.get(i);
                String sDyesName       = (String)ADyesNameList.get(i);
                int iDyesRevisionCount = getDyeswisePriceRevisionCount(sDyesCode);
                APriceList             = getPriceList(sDyesCode);
                int iSlNo = (i+1);
                sStr = sStr + "<tr>";
                sStr = sStr + "<td   align='center' ><font color='#CD5C5C' ><b>"+iSlNo+"</b></font></td>";
                sStr = sStr + "<td   align='left'><font color= '#CD5C5C' ><b>" + sDyesName + "</b></font></td>";
             //   System.out.println("PriceListSize==>"+sDyesName+"---"+APriceList.size()); 
              for(int j=0;j<APriceList.size();j++){
                  HashMap   theMap      = (HashMap)APriceList.get(j);
                  String sConsumption   = (String)theMap.get("CONSUMPTION");
                  String sPurStatus     = (String)theMap.get("PURSTATUS");
                  String sItemCode      = (String)theMap.get("ITEMCODE");
                  
                  System.out.println("Consumption===>"+sConsumption);
                  int iPurStatus        = common.toInt(sPurStatus);
                //  System.out.println("PurStatus------->"+iPurStatus);
                if(iPurStatus==1){
                    
                  //String sConsumption   = (String)theMap.get("CONSUMPTION")    ;
                  String sRateperKg     = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  double dNetValue      = common.toDouble(sRateperKg)-dDiscount+dTax;
                  double dTotalValue    = common.toDouble(sConsumption)*dNetValue;
              //      System.out.println("PurStatus==1>"+sRateperKg);
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sConsumption + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sRateperKg + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sDiscPer + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + String.valueOf(dDiscount) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sTaxPer + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + String.valueOf(dTax) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + String.valueOf(dNetValue) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + String.valueOf(dTotalValue) + "</b></font></td>";
                  
                  dPurchasedRateperKg       =   common.toDouble(sRateperKg);
                  dPurchasedTotalValue      =   dTotalValue;
                  dOldNetValue              =   dNetValue;
                  }
                if(sItemCode.startsWith("CD") && iPurStatus==0){
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                  }
                 for(int a=0;a<iPriceRevisionCount;a++){
                      
                    String sRateperKg      = "";
                    double dTotalValue     = 0.00, dNetValue=0.00;
                    String sRate       = (String)theMap.get("RATE")    ;

                  if(iPurStatus==0){
                //  String sConsumption   = (String)theMap.get("CONSUMPTION")    ;
                  sRateperKg            = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  dNetValue             = common.toDouble(sRateperKg)-dDiscount+dTax;
                  dTotalValue           = dOldConsumption*dNetValue;
                  if(iPriceRevisionCount>iDyesRevisionCount){
                      System.out.println("Dyes RevissionCount--->"+iDyesRevisionCount);
                      sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                      sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>"; 
                      sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>"; 
                      sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>"; 
                      sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>"; 
                      sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>"; 
                      sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>"; 
                      sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>"; 
                      iDyesRevisionCount ++;
                   }
                  // if(iDyesRevisionCount<a+1){
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sRateperKg + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sDiscPer + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + String.valueOf(dDiscount) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sTaxPer + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + String.valueOf(dTax) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + String.valueOf(dNetValue) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + String.valueOf(dTotalValue) + "</b></font></td>";
                  dNewRateperKg        = common.toDouble(sRate);
                  dNewTotalValue       = dTotalValue;
                  dNewNetValue         = dNetValue;
                  break; 
                  }
                 System.out.println("Size===>"+iPriceRevisionCount+"<--->"+iDyesRevisionCount);
                  //  dRateDiff          = dNewRateperKg - dPurchasedRateperKg;
                    //dTotalValueDiff    = dNewTotalValue- dPurchasedTotalValue;
                    //dDiffPercentage    = ((dNewNetValue - dOldNetValue) / dOldNetValue) * 100;
                  }  
              }
              
              for(int j=0;j<APriceList.size();j++){
                  HashMap   theMap      = (HashMap)APriceList.get(j);
                  String sConsumption   = (String)theMap.get("CONSUMPTION");
                  String sPurStatus     = (String)theMap.get("PURSTATUS");
                  String sItemCode      = (String)theMap.get("ITEMCODE");
                  int iPurStatus        = common.toInt(sPurStatus);
                if(iPurStatus==1){
                    
                  String sRateperKg     = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  double dNetValue      = common.toDouble(sRateperKg)-dDiscount+dTax;
                  double dTotalValue    = common.toDouble(sConsumption)*dNetValue;

                  dPurchasedRateperKg   =   common.toDouble(sRateperKg);
                  dPurchasedTotalValue  =   dTotalValue;
                  dOldNetValue          =   dNetValue;
                  
                 
                }
                
                for(int a=0;a<iPriceRevisionCount;a++){
                      
                    String sRateperKg   = "";
                    double dTotalValue  = 0.00, dNetValue=0.00;
                    String sRate        = (String)theMap.get("RATE")    ;

                  if(iPurStatus==0){
                //  String sConsumption   = (String)theMap.get("CONSUMPTION")    ;
                  sRateperKg            = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  dNetValue             = common.toDouble(sRateperKg)-dDiscount+dTax;
                  dTotalValue           = dOldConsumption*dNetValue;
                  
                  dNewRateperKg        = common.toDouble(sRate);
                  dNewTotalValue       = dTotalValue;
                  dNewNetValue         = dNetValue;
                  System.out.println("Rate--->"+sRate);
                  //break; 
                  }
                    System.out.println("Rate--After--->"+sRate);
                    dRateDiff          = dNewRateperKg - dPurchasedRateperKg;
                    dTotalValueDiff    = dNewTotalValue- dPurchasedTotalValue;
                    dDiffPercentage    = ((dNewNetValue - dOldNetValue) / dOldNetValue) * 100;
                  }  
                  System.out.println("RateDiff--->"+dRateDiff);
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dRateDiff),2) + "</b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dTotalValueDiff),2) + "</b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dDiffPercentage),2) + "</b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>";
               break;  
              }
                 sStr = sStr + "</tr>";    
            }   */
                
       /*     sStr = sStr + "</table>";
            sStr = sStr + "</body>";
            sStr = sStr + "</html>";
         
        }
        catch (Exception ex) 
        {
                  
            System.out.println("set Report--->" + ex);
            ex.printStackTrace();
        }
            
        return sStr;
    }*/
    

    /*
    private String getReport(){
        String SPartyName = (String)JCSupplier.getSelectedItem();
        String sStr = "";
                             
        double dPurchasedRateperKg=0.00, dPurchasedTotalValue=0.00, dOldConsumption=0.00, dOldNetValue=0.00;
     
        double dDiffPercentage=0.00;
        double dRateDiff=0.00, dTotalValueDiff=0.00;
        //double dGNetValue=0.00, dGTotalValue=0.00;
        double dPurchasedNetValue = 0.00;
            dPrevNetValue         = 0.00;
            dPrevRateperKg        = 0.00;
            dPrevTotalValue       = 0.00;
            dNewRateperKg         = 0.00;
            dNewTotalValue        = 0.00;
            dNewNetValue          = 0.00;
            dDiffTotalValue       = 0.00;
            dDiffRateperKg        = 0.00;
            dPurNetValue          = 0.00;
            dPurTotalValue        = 0.00;
       
        String sHeading = "REVISED PRICE IN "+SPartyName+" - "+common.parseDate(common.getCurrentDate())+" ";
        
        try 
        {
            sStr = "<html><head>";
            sStr = sStr + "Revisied Price List";
            sStr = sStr + "</head>";
            sStr = sStr + "<body >";
            sStr = sStr + "<h2 align ='center'> <b>" + sHeading + "</b></h2>";
            sStr = sStr + "<table border='1' width='70%' bgcolor='lightGrey'>";
            sStr = sStr + "<tr>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>SL NO </b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'  width='125' ><font color='#000099' ><b>             DYES NAME    </b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>ONE YEAR CONSUMPTION QTY</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' colspan='7'><font color='#000099' ><b>LAST PURCHASED RATE</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>&nbsp;</b></font></td>";
           
            //getRateDate();
            int iPriceRevisionCount  = setMaxCount();
            int iColSize             = iPriceRevisionCount*7;
            int iTotalColumns        = iColSize+14;
            dGNetTotal               = new double[iColSize];
            dGTotalValue             = new double[iColSize];
            
            dOldGNetTotal            = new double[1];
            dOldGTotalValue          = new double[1];
            
            dDiffGNetTotal           = new double[1];
            dDiffGTotalValue         = new double[1];
            int iNewRateCount        = 0;      
            //System.out.println("PriceRevCount===>"+iPriceRevisionCount+"--"+iColSize);
            String sTempItemCode ="", sTempDate="";
            //System.out.println("PriceRev Count===>"+iPriceRevisionCount);
            for (int i = 0; i < ADataList.size(); i++) {
                HashMap themap      = (HashMap) ADataList.get(i);
                String sPurStatus   = (String)themap.get("PURSTATUS");
                String SItemCode    = (String)themap.get("ITEMCODE");
                int iPurStatus      = common.toInt(sPurStatus);
                iNewRateCount       = getPriceRevCount(SItemCode);
              //  System.out.println("New RateCount===>"+iCount);
            for(int j=0;j<iPriceRevisionCount;j++) { 
             if(iPurStatus==0){   
               String sDate         = common.parseDate((String)themap.get("NEWRATEDATE"));
               String sItemCode     = (String)themap.get("ITEMCODE");
               String sPurDate      = (String)themap.get("NEWRATEDATE");
               String sPriceRevSt   = (String)themap.get("PRICEREVISIONSTATUS");    
               int iPriceRevSt      = common.toInt((String)themap.get("PRICEREVISIONSTATUS")); 
                 
            if(!sPurDate.equals(sTempDate) && iPriceRevSt==1)   {     
           // if(iPriceRevSt==1)   {        
            sStr = sStr + "<td align='center' color='#000099' colspan='7'><font color='#000099' ><b> OLD RATE("+sDate+")</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>&nbsp;</b></font></td>";
            }
            //if(!sItemCode.trim().equals(sTempItemCode))   {
            if(!sPurDate.equals(sTempDate) && iPriceRevSt==0)   {     
            sStr = sStr + "<td align='center' color='#000099' colspan='7'><font color='#000099' ><b> NEW RATE("+sDate+")</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>&nbsp;</b></font></td>";
            //sTempItemCode = sItemCode;
            //sTempDate     = sPurDate;
            //break;
            }
            sTempItemCode = sItemCode;
            sTempDate     = sPurDate;
            break;
              
                }
              }
            }

            sStr = sStr + "<td align='center' color='#000099' colspan='3' width='125'><font color='#000099' ><b>DIFFERENCE</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>LAB STATUS</b></font></td>";
            sStr = sStr + "</tr>";
            
            sStr = sStr + "<tr>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2' width='100'><font color='#000099' ><b>Discount</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2' width='100' ><font color='#000099' ><b>   Tax  </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Net value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Total</b></font></td>";
            
            for (int i = 0; i < iNewRateCount; i++) {
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2' width='100' ><font color='#000099' ><b> Discount  </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2' width='100' ><font color='#000099' ><b>  Tax  </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Net Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Total</b></font></td>";
                
            }
            
        
            
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Total Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>%</b></font></td>";
            sStr = sStr + "</tr>";
            
            sStr = sStr + "<tr>";
            
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
           
            for (int i = 0; i < iNewRateCount; i++) {
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
           } 


            sStr = sStr + "</tr>";
            
            for (int i = 0; i < ADataList.size(); i++) {
                HashMap themap      = (HashMap) ADataList.get(i);
                String sPurStatus   = (String)themap.get("PURSTATUS");
                int iPurStatus      = common.toInt(sPurStatus);
                if(iPurStatus==1){
                String SConsumption = (String) themap.get("CONSUMPTION");
                double dRatePerKg   = common.toDouble((String) themap.get("RATE"));
                double dDiscPer     = common.toDouble((String) themap.get("DISPER"));
                double dTaxPer      = common.toDouble((String) themap.get("TAXPER"));
                double dDisValue    = (dRatePerKg*dDiscPer)/100;
                double dTaxValue    = (dRatePerKg*dTaxPer)/100;
                double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
                double dTotalValue  = common.toDouble(SConsumption)*dNetValue;
                dOldConsumption     = common.toDouble(SConsumption);
                }
            }
            
            ArrayList     APriceList    = new ArrayList();
             for (int i = 0; i < ADyesCodeList.size(); i++) {
                String sDyesCode       = (String)ADyesCodeList.get(i);
                String sDyesName       = (String)ADyesNameList.get(i);
                int iDyesRevisionCount = getDyeswisePriceRevisionCount(sDyesCode);
                APriceList             = getPriceList(sDyesCode);
                int iSlNo = (i+1);
                sStr = sStr + "<tr>";
                sStr = sStr + "<td   align='center' ><font color='#CD5C5C' ><b>"+iSlNo+"</b></font></td>";
                sStr = sStr + "<td   align='left'><font color= '#CD5C5C' ><b>" + sDyesName + "</b></font></td>";
 
              for(int j=0;j<APriceList.size();j++){
                  HashMap   theMap      = (HashMap)APriceList.get(j);
                  String sConsumption   = (String)theMap.get("CONSUMPTION");
                  String sPurStatus     = (String)theMap.get("PURSTATUS");
                  String sItemCode      = (String)theMap.get("ITEMCODE");
                  int iPurStatus        = common.toInt(sPurStatus);
                  System.out.println("PurStatus=======>"+sItemCode+"---"+iPurStatus);
                if(iPurStatus==1){
                    
                  //String sConsumption   = (String)theMap.get("CONSUMPTION")    ;
                  String sRateperKg     = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  double dNetValue      = common.toDouble(sRateperKg)-dDiscount+dTax;
                  double dTotalValue    = common.toDouble(sConsumption)*dNetValue;
              //      System.out.println("PurStatus==1>"+sRateperKg);
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sConsumption + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sRateperKg + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sDiscPer + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + String.valueOf(dDiscount) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sTaxPer + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + String.valueOf(dTax) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dNetValue),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dTotalValue),2) + "</b></font></td>";
                  
            
                  dPurchasedRateperKg       =   common.toDouble(sRateperKg);
                  dPurchasedTotalValue      =   dTotalValue;
                  dOldNetValue              =   dNetValue;
                  dPurNetValue              =   dPurNetValue+dNetValue;
                  dPurTotalValue            =   dPurTotalValue+dTotalValue;
                  }
                
                else if(iPurStatus!=0){
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                }
                
                if(sItemCode.startsWith("CD") && iPurStatus==0 && j==0){
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                      
                  }
                
                for(int a=0;a<iPriceRevisionCount;a++){
                      
                    String sRateperKg      = "", sLabStatus="";
                    double dTotalValue     = 0.00, dNetValue=0.00;
                    String sRate       = (String)theMap.get("RATE")    ;

                if(iPurStatus==0){
                //  String sConsumption   = (String)theMap.get("CONSUMPTION")    ;
                  sRateperKg            = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  //String sItemCode      = (String)theMap.get("ITEMCODE")    ;
                  String sDate          = (String)theMap.get("NEWRATEDATE")    ;
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  dNetValue             = common.toDouble(sRateperKg)-dDiscount+dTax;
                  dTotalValue           = dOldConsumption*dNetValue;
                  
                  dPrevNetValue         = getPrevNetValue(sDate,sItemCode);
                  dPrevRateperKg        = getPrevRateperKg(sDate,sItemCode);
                  dPrevTotalValue       = getPrevTotalValue(sDate,sItemCode);
                  dNewRateperKg         = getNewRateperKg(sItemCode);
                  dNewTotalValue        = getNewTotalValue(sItemCode);
                  dNewNetValue          = getNewNetValue(sItemCode);
                  
                  dGrTotalValue         = dGrTotalValue+dPrevNetValue;
                  dGrNetValue           = dGrNetValue+dPrevTotalValue;
                  
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sRateperKg + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sDiscPer + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + String.valueOf(dDiscount) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sTaxPer + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + String.valueOf(dTax) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dNetValue),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dTotalValue),2) + "</b></font></td>";
                  
                  break; 
                   }
                 }  
                   
              }
              
              for(int j=0;j<APriceList.size();j++){
                  String sLabStatus="";
                  HashMap   theMap      = (HashMap)APriceList.get(j);
                  String sConsumption   = (String)theMap.get("CONSUMPTION");
                  String sPurStatus     = (String)theMap.get("PURSTATUS");
                  String sItemCode      = (String)theMap.get("ITEMCODE");
                  String sPriceRevisionSt  = (String)theMap.get("PRICEREVISIONSTATUS");
                  System.out.println("PriceREvSt==>"+sPriceRevisionSt);
                  sLabStatus            = getLabStatus(sItemCode);
                  
                  int iPurStatus        = common.toInt(sPurStatus);
                  
                if(iPurStatus==1){
                    
                  String sRateperKg     = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  
                  
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  double dNetValue      = common.toDouble(sRateperKg)-dDiscount+dTax;
                  double dTotalValue    = common.toDouble(sConsumption)*dNetValue;

                  dPurchasedRateperKg   =   common.toDouble(sRateperKg);
                  dPurchasedTotalValue  =   dTotalValue;
                  dPurchasedNetValue    =   dNetValue;
                  dOldNetValue          =   dNetValue;
                }
                
         
                for(int a=0;a<iPriceRevisionCount;a++){
                      
                    String sRateperKg   = "";
                    double dTotalValue  = 0.00, dNetValue=0.00;
                    String sRate        = (String)theMap.get("RATE")    ;
                    
                    dRateDiff             = dNewRateperKg - dPrevRateperKg;
                    dTotalValueDiff       = dNewTotalValue- dPrevTotalValue;
                    dDiffPercentage       = ((dNewNetValue - dPrevNetValue) / dPrevNetValue) * 100;
                   // System.out.println("Diff %===>"+dNewNetValue+"---"+dPrevNetValue);
                  }  
                
                //dDiffPercentage       = ((dNewNetValue - dPrevNetValue) / dPrevNetValue) * 100;
                    dDiffTotalValue       =  dDiffTotalValue+dTotalValueDiff;
                    dDiffRateperKg        =  dDiffRateperKg+dRateDiff;
                 // System.out.println("DiffTotal= Report==>"+dTotalValueDiff+dDiffTotalValue);
                 
                 
                 
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dRateDiff),2) + "</b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dTotalValueDiff),2) + "</b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dDiffPercentage),2) + "</b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>"+sLabStatus+"</b></font></td>";
               break;  
              }
                 sStr = sStr + "</tr>";    
            }
             
             sStr = sStr + "<tr>";
             
             sStr = sStr + "<td   align='center' colspan='3'><font color='#CD5C5C' ><b>Grand Total</b></font></td>";
             
             System.out.println("PriceList Size==>"+APriceList.size());
             for(int j=0;j<APriceList.size();j++){     
                  HashMap   theMap      = (HashMap)APriceList.get(j);
                  String sConsumption   = (String)theMap.get("CONSUMPTION");
                  String sPurStatus     = (String)theMap.get("PURSTATUS");
                  String sItemCode      = (String)theMap.get("ITEMCODE");
                  int iPurStatus        = common.toInt(sPurStatus);
                  System.out.println("GTotal==>"+sItemCode+"--"+iPurStatus);
               //  if(iPurStatus==1){
                     
                  String sDate          = (String)theMap.get("NEWRATEDATE")    ;
                     
                 // dPurNetValue          = dPurNetValue+getOldNetValue(sItemCode);
                  dPurRatperKg          = getOldRateperKg(sItemCode); 
                  //dPurTotalValue        = dPurTotalValue+getOldTotalValue(sItemCode);  
                  
                 //}
             }
                  //sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dPurNetValue),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dPurTotalValue),2) + "</b></font></td>";
              
             
            
              // New Rate  
              double dDatewiseNetValue=0.00 , dDatewiseTotalValue=0.00; 
              double dDatewisePrevNetValue=0.00 , dDatewisePrevTotalValue=0.00; 
              for(int j=0;j<APriceList.size();j++){     
                  HashMap   theMap      = (HashMap)APriceList.get(j);
                  String sConsumption   = (String)theMap.get("CONSUMPTION");
                  String sPurStatus     = (String)theMap.get("PURSTATUS");
                  String sItemCode      = (String)theMap.get("ITEMCODE");
                  int iPurStatus        = common.toInt(sPurStatus);
                 if(iPurStatus==0){
                  String sDate          = (String)theMap.get("NEWRATEDATE")    ;
                     
                  dDatewiseNetValue     =  getDatewiseNetValue(sDate);
                  dDatewiseTotalValue   =  getDatewiseTotalValue(sDate);
                  
                  
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dDatewiseNetValue),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dDatewiseTotalValue),2) + "</b></font></td>";
                  
                  dDatewiseNetValue   = 0.00;
                  dDatewiseTotalValue = 0.00;
                  
                   }
                }
                
             sStr = sStr + "<td   align='center' colspan='1'><font color='#CD5C5C' ><b></b></font></td>";                    
             sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>"+common.getRound(dDiffRateperKg,2)+"</b></font></td>"; 
             sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>"+common.getRound(dDiffTotalValue,2)+"</b></font></td>"; 
             sStr = sStr + "<td   align='center' colspan='1'><font color='#CD5C5C' ><b></b></font></td>";
             sStr = sStr + "<td   align='center' colspan='1'><font color='#CD5C5C' ><b></b></font></td>";
             
            sStr = sStr + "</tr>";
            sStr = sStr + "</table>";
            sStr = sStr + "</body>";
            sStr = sStr + "</html>";
         
        }
        catch (Exception ex) 
        {
                  
            System.out.println("set Report--->" + ex);
            ex.printStackTrace();
        }
            
        return sStr;
    } */
    
//11.05.2017
        private String getReport(){
        String SPartyName = (String)JCSupplier.getSelectedItem();
        String sStr = "";
        double dGrandTotal   = 0.00;
                             
        double dPurchasedRateperKg=0.00, dPurchasedTotalValue=0.00, dOldConsumption=0.00, dOldNetValue=0.00;
     
        double dDiffPercentage=0.00;
        double dRateDiff=0.00, dTotalValueDiff=0.00,dNetValueDiff=0.00;
        //double dGNetValue=0.00, dGTotalValue=0.00;
        double dPurchasedNetValue=0.00;
	    dPrevNetValue         = 0.00;
            dPrevRateperKg        = 0.00;
            dPrevTotalValue       = 0.00;
            dNewRateperKg         = 0.00;
            dNewTotalValue        = 0.00;
            dNewNetValue          = 0.00;
            dDiffTotalValue       = 0.00;
            dDiffRateperKg        = 0.00;
            dPurNetValue          = 0.00;
            dPurTotalValue        = 0.00;

String sTempItemCode1="",sTempDate1="";
            String SPriceRevDate="";
            int iNewRateCount=0;
            for (int i = 0; i < ADataList.size(); i++) {
                HashMap themap      = (HashMap) ADataList.get(i);
                String sPurStatus   = (String)themap.get("PURSTATUS");
                String SItemCode    = (String)themap.get("ITEMCODE");
                iNewRateCount       = getPriceRevCount(SItemCode);
                int iPurStatus      = common.toInt(sPurStatus);
                int iPriceRevisionCount =  setMaxCount();
                
                
             for(int j=0;j<iPriceRevisionCount;j++) { 
             if(iPurStatus==0){   
               String sDate         = common.parseDate((String)themap.get("NEWRATEDATE"));
               String sItemCode     = (String)themap.get("ITEMCODE");
               String sPurDate      = (String)themap.get("NEWRATEDATE");
               String sPriceRevSt   = (String)themap.get("PRICEREVISIONSTATUS");    
               int iPriceRevSt      = common.toInt((String)themap.get("PRICEREVISIONSTATUS")); 
               
               if(!sPurDate.equals(sTempDate1) && iPriceRevSt==0)   {     
                   SPriceRevDate    = sDate;
               }
                 sTempItemCode1 = sItemCode;
                 sTempDate1     = sPurDate;
                 break;
                }
              }
            }
       
        String sHeading = "REVISED PRICE IN "+SPartyName+" - "+common.parseDate(SPriceRevDate)+" ";
        
       
//        String sHeading = "REVISED PRICE IN "+SPartyName+" - "+common.parseDate(common.getCurrentDate())+" ";
        
        try 
        {
            sStr = "<html><head>";
            sStr = sStr + "Revisied Price List";
            sStr = sStr + "</head>";
            sStr = sStr + "<body >";
            sStr = sStr + "<h2 align ='center'> <b>" + sHeading + "</b></h2>";
            sStr = sStr + "<table border='1' width='70%' bgcolor='lightGrey'>";
            sStr = sStr + "<tr>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>SL NO </b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'  width='125' ><font color='#000099' ><b>             DYES NAME    </b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>ONE YEAR CONSUMPTION QTY</b></font></td>";
          //  sStr = sStr + "<td align='center' color='#000099' colspan='7'><font color='#000099' ><b>LAST PURCHASED RATE</b></font></td>";
          //  sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>&nbsp;</b></font></td>";
           
            //getRateDate();
            int iPriceRevisionCount  = setMaxCount();
            int iColSize             = iPriceRevisionCount*7;
            int iTotalColumns        = iColSize+14;
            dGNetTotal               = new double[iColSize];
            dGTotalValue             = new double[iColSize];
            
            dOldGNetTotal            = new double[1];
            dOldGTotalValue          = new double[1];
            
            dDiffGNetTotal           = new double[1];
            dDiffGTotalValue         = new double[1];
	    iNewRateCount            = 0;      
            
          //  System.out.println("PriceRevCount===>"+iPriceRevisionCount+"--"+iColSize);
            String sTempItemCode ="", sTempDate="";
            //System.out.println("PriceRev Count===>"+iPriceRevisionCount);
            for (int i = 0; i < ADataList.size(); i++) {
                HashMap themap      = (HashMap) ADataList.get(i);
                String sPurStatus   = (String)themap.get("PURSTATUS");
		String SItemCode    = (String)themap.get("ITEMCODE");
                int iPurStatus      = common.toInt(sPurStatus);
		iNewRateCount       = getPriceRevCount(SItemCode);
                
            for(int j=0;j<iPriceRevisionCount;j++) { 
             if(iPurStatus==0){   
               String sDate         = common.parseDate((String)themap.get("NEWRATEDATE"));
               String sItemCode     = (String)themap.get("ITEMCODE");
               String sPurDate      = (String)themap.get("NEWRATEDATE");
               String sPriceRevSt   = (String)themap.get("PRICEREVISIONSTATUS");    
               int iPriceRevSt      = common.toInt((String)themap.get("PRICEREVISIONSTATUS")); 
                 
            if(!sPurDate.equals(sTempDate) && iPriceRevSt==1)   {     
           // if(iPriceRevSt==1)   {        
            sStr = sStr + "<td align='center' color='#000099' colspan='7'><font color='#000099' ><b> OLD RATE("+sDate+")</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>&nbsp;</b></font></td>";
            }
            //if(!sItemCode.trim().equals(sTempItemCode))   {
            if(!sPurDate.equals(sTempDate) && iPriceRevSt==0)   {     
            sStr = sStr + "<td align='center' color='#000099' colspan='7'><font color='#000099' ><b> NEW RATE("+sDate+")</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>&nbsp;</b></font></td>";
            //sTempItemCode = sItemCode;
            //sTempDate     = sPurDate;
            //break;
            }
            sTempItemCode = sItemCode;
            sTempDate     = sPurDate;
            break;
                }
              }
            }

            sStr = sStr + "<td align='center' color='#000099' colspan='3' width='125'><font color='#000099' ><b>DIFFERENCE</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>LAB STATUS</b></font></td>";
            sStr = sStr + "</tr>";
            
            sStr = sStr + "<tr>";
          /*  sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2' width='100'><font color='#000099' ><b>Discount</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2' width='100' ><font color='#000099' ><b>   Tax  </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Net value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Total</b></font></td>";
         */
            
	for (int i = 0; i < iNewRateCount; i++) {
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2' width='100' ><font color='#000099' ><b> Discount  </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2' width='100' ><font color='#000099' ><b>  Tax  </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Net Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Total</b></font></td>";
                
            }

            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Net Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Total Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>%</b></font></td>";
            sStr = sStr + "</tr>";
            
            sStr = sStr + "<tr>";
            
         /*  sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
           */ 

         for (int i = 0; i < iNewRateCount; i++) {
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
           } 

        
            sStr = sStr + "</tr>"; 
            for (int i = 0; i < ADataList.size(); i++) {
                HashMap themap      = (HashMap) ADataList.get(i);
                String sPurStatus   = (String)themap.get("PURSTATUS");
                int iPurStatus      = common.toInt(sPurStatus);
                if(iPurStatus==1){
                String SConsumption = (String) themap.get("CONSUMPTION");
                double dRatePerKg   = common.toDouble((String) themap.get("RATE"));
                double dDiscPer     = common.toDouble((String) themap.get("DISPER"));
                double dTaxPer      = common.toDouble((String) themap.get("TAXPER"));
                double dDisValue    = (dRatePerKg*dDiscPer)/100;
                double dTaxValue    = (dRatePerKg*dTaxPer)/100;
                double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
                double dTotalValue  = common.toDouble(SConsumption)*dNetValue;
//                dOldConsumption     = common.toDouble(SConsumption);
		//dOldConsumption	   = getConsumptionQty()

                }
            }
            
            ArrayList     APriceList    = new ArrayList();
             for (int i = 0; i < ADyesCodeList.size(); i++) {
                String sDyesCode       = (String)ADyesCodeList.get(i);
                String sDyesName       = (String)ADyesNameList.get(i);
                int iDyesRevisionCount = getDyeswisePriceRevisionCount(sDyesCode);
                APriceList             = getPriceList(sDyesCode);
                int iSlNo = (i+1);
                sStr = sStr + "<tr>";
                sStr = sStr + "<td   align='center' ><font color='#CD5C5C' ><b>"+iSlNo+"</b></font></td>";
                sStr = sStr + "<td   align='left'><font color= '#CD5C5C' ><b>" + sDyesName + "</b></font></td>";
 
              for(int j=0;j<APriceList.size();j++){
                  HashMap   theMap      = (HashMap)APriceList.get(j);
                  String sConsumption   = (String)theMap.get("CONSUMPTION");
                  String sPurStatus     = (String)theMap.get("PURSTATUS");
                  String sItemCode      = (String)theMap.get("ITEMCODE");
                  String sPriceRevisionSt    = (String)theMap.get("PRICEREVISIONSTATUS")    ;
                  int iPriceRevStatus   = common.toInt(sPriceRevisionSt);
                  
                  int iPurStatus        = common.toInt(sPurStatus);
                if(iPurStatus==1){
                    
                  //String sConsumption   = (String)theMap.get("CONSUMPTION")    ;
                  String sRateperKg     = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  double dNetValue      = common.toDouble(sRateperKg)-dDiscount+dTax;
                  double dTotalValue    = common.toDouble(sConsumption)*dNetValue;
                  
                  
              //      System.out.println("PurStatus==1>"+sRateperKg);
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sConsumption + "</b></font></td>";
              /*    sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sRateperKg + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sDiscPer + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dDiscount),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(sTaxPer,2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dTax),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dNetValue),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dTotalValue),2) + "</b></font></td>";
                  
            */
                  dPurchasedRateperKg       =   common.toDouble(sRateperKg);
                  dPurchasedTotalValue      =   dTotalValue;
                  dOldNetValue              =   dNetValue;
                  dPurNetValue              =   dPurNetValue+dNetValue;
                  dPurTotalValue            =   dPurTotalValue+dTotalValue;
                  }
                
                if(sItemCode.startsWith("CD") && iPurStatus==0 && j==0){
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                       sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>0</b></font></td>";
                  }
                
                 for(int a=0;a<iPriceRevisionCount;a++){
                      
                    String sRateperKg      = "";
                    double dTotalValue     = 0.00, dNetValue=0.00;
                    String sRate       = (String)theMap.get("RATE")    ;

                if(iPurStatus==0){
                //  String sConsumption   = (String)theMap.get("CONSUMPTION")    ;
                  sRateperKg            = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  //String sItemCode      = (String)theMap.get("ITEMCODE")    ;
                  String sDate          = (String)theMap.get("NEWRATEDATE")    ;
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  dNetValue             = common.toDouble(sRateperKg)-dDiscount+dTax;
		  dOldConsumption	= common.toDouble(getConsumptionQty(sItemCode));
		  dTotalValue           = dOldConsumption*dNetValue;
                  dGrandTotal           = dGrandTotal+dTotalValue;
                  //dTotalValue           = dOldConsumption*dNetValue;

		  //System.out.println("Consumption==>"+dOldConsumption+"<-->"+sRateperKg+"<-->"+dDiscount+"<--->"+dTax+"<-->"+dTotalValue+"<-->"+dGrandTotal);
                  
                  dPrevNetValue         = getPrevNetValue(sDate,sItemCode);
                  dPrevRateperKg        = getPrevRateperKg(sDate,sItemCode);
                  //dPrevTotalValue       = getPrevTotalValue(sDate,sItemCode);
                  dNewRateperKg         = getNewRateperKg(sItemCode);
                  dNewTotalValue        = getNewTotalValue(sItemCode);
                  dNewNetValue          = getNewNetValue(sItemCode);
                  dPrevTotalValue       = getPrevTotalValue(sDate,sItemCode,dOldConsumption);
                  
                  dGrTotalValue         = dGrTotalValue+dPrevNetValue;
                  dGrNetValue           = dGrNetValue+dPrevTotalValue;


                  
//                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sRateperKg + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sDiscPer + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dDiscount),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sTaxPer + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dTax),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dNetValue),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dTotalValue),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  
                  break; 
                   }
                 }  
              }
                  String sLabStatus="";              
              for(int j=0;j<APriceList.size();j++){
//                  String sLabStatus="";
                  HashMap   theMap      = (HashMap)APriceList.get(j);
                  String sConsumption   = (String)theMap.get("CONSUMPTION");
                  String sPurStatus     = (String)theMap.get("PURSTATUS");
                  String sItemCode      = (String)theMap.get("ITEMCODE");
                  int iPurStatus        = common.toInt(sPurStatus);
                  String sPriceRevisionSt  = (String)theMap.get("PRICEREVISIONSTATUS");
                  sLabStatus            = getLabStatus(sItemCode);
                  String sPriceRevStatus = (String)theMap.get("PRICEREVISIONSTATUS");
                  int iPriceRevStatus   = common.toInt(sPriceRevStatus);
              //    System.out.println("PriceRevCount inside==>"+iPurStatus+"<-->"+iPriceRevStatus);
                  
                if(iPurStatus==1){
                    
                  String sRateperKg     = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  double dNetValue      = common.toDouble(sRateperKg)-dDiscount+dTax;
                  double dTotalValue    = common.toDouble(sConsumption)*dNetValue;
                  //double dTotalValue    = common.toDouble(sConsumption)*dNetValue;

                  dPurchasedRateperKg   =   common.toDouble(sRateperKg);
                  dPurchasedTotalValue  =   dTotalValue;
                  dPurchasedNetValue    =   dNetValue;
                  dOldNetValue          =   dNetValue;
                }
         
                for(int a=0;a<iPriceRevisionCount;a++){
                      
                    String sRateperKg   = "";
                    double dTotalValue  = 0.00, dNetValue=0.00;
                    String sRate        = (String)theMap.get("RATE")    ;
                  if(iPurStatus==0){
                    
                  sRateperKg            = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  dNetValue             = common.toDouble(sRateperKg)-dDiscount+dTax;
                  dTotalValue           = common.toDouble(sConsumption)*dNetValue;

                  dPurchasedRateperKg   =   common.toDouble(sRateperKg);
                  dPurchasedTotalValue  =   dTotalValue;
                  dPurchasedNetValue    =   dNetValue;
                  dOldNetValue          =   dNetValue;
                }
         
                    //System.out.println("PriceRevCount inside==>"+iPurStatus+"<-->"+iPriceRevStatus);
                  if(iPurStatus==0 && iPriceRevStatus==1){ 
                  String sDate          = (String)theMap.get("NEWRATEDATE")    ;
                 //  System.out.println("inside PrevDate=TD===>"+sDate);
                  dPrevNetValue         = getPrevNetValue(sDate,sItemCode);
                  dPrevRateperKg        = getPrevRateperKg(sDate,sItemCode);
              //        System.out.println("OldConsumption==>"+dOldConsumption);
                  dPrevTotalValue       = getPrevTotalValue(sDate,sItemCode,dOldConsumption);
                  }
                  if(iPurStatus==0 && iPriceRevStatus==0){ 
                  String sDate          = (String)theMap.get("NEWRATEDATE")    ;
                  dNewRateperKg         = getNewRateperKg(sItemCode);
                  dNewTotalValue        = getNewTotalValue(sItemCode,dOldConsumption);
                  dNewNetValue          = getNewNetValue(sItemCode);
                   }
                 // }  
                   dRateDiff             = dNewRateperKg - dPrevRateperKg;
                   
                  dTotalValueDiff       = dNewTotalValue- dPrevTotalValue;
                  dNetValueDiff         = dNewNetValue  - dPrevNetValue;
                //    System.out.println("Values in Td==>"+dNewTotalValue+"<--->"+dPrevTotalValue);
                  dDiffPercentage       = ((dNewNetValue - dPrevNetValue) / dPrevNetValue) * 100;
               
                  }
                }
              
              //   System.out.println("Total Values ==>"+dTotalValueDiff);
                    dDiffTotalValue       =  dDiffTotalValue+dTotalValueDiff;
                    dDiffNetValue         =  dDiffNetValue+dNetValueDiff;

                 //sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dNetValueDiff),2) + "</b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dTotalValueDiff),2) + "</b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dDiffPercentage),2) + "</b></font></td>";
		// sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>"+sLabStatus+"</b></font></td>";
              // break;
              
              //}
                 sStr = sStr + "</tr>";    
            }
             
             sStr = sStr + "<tr>";
             
             sStr = sStr + "<td   align='center' colspan='3'><font color='#CD5C5C' ><b>Grand Total</b></font></td>";
             
        //     System.out.println("PriceList Size==>"+APriceList.size());
             
          /*   for(int j=0;j<APriceList.size();j++){     
                  HashMap   theMap      = (HashMap)APriceList.get(j);
                  String sConsumption   = (String)theMap.get("CONSUMPTION");
                  String sPurStatus     = (String)theMap.get("PURSTATUS");
                  String sItemCode      = (String)theMap.get("ITEMCODE");
                  int iPurStatus        = common.toInt(sPurStatus);
                     
                  String sDate          = (String)theMap.get("NEWRATEDATE")    ;
                     
                  dPurRatperKg          = getOldRateperKg(sItemCode); 
                  
             }*/
              
              double dDatewiseNetValue=0.00 , dDatewiseTotalValue=0.00; 
              double dDatewisePrevNetValue=0.00 , dDatewisePrevTotalValue=0.00; 
            //    System.out.println("PriceListSize==>"+APriceList.size()+"--"+APriceList);
              for(int j=0;j<APriceList.size();j++){     
                  HashMap   theMap      = (HashMap)APriceList.get(j);
                  String sConsumption   = (String)theMap.get("CONSUMPTION");
                  String sPurStatus     = (String)theMap.get("PURSTATUS");
                  String sItemCode      = (String)theMap.get("ITEMCODE");
              //    System.out.println("ItemCode========>"+sItemCode);
                  int iPurStatus        = common.toInt(sPurStatus);
                  String sPriceRevisionSt  = (String)theMap.get("PRICEREVISIONSTATUS");
                  
                 if(iPurStatus==0 ){
                  String sDate          = (String)theMap.get("NEWRATEDATE")    ;
                     
                  dDatewiseNetValue     =  getDatewiseNetValue(sDate);
                  double doldConsumption= common.toDouble(getConsumptionQty(sItemCode));
                  dDatewiseTotalValue   =  getDatewiseTotalValue(sDate,sItemCode);
                  
                  
                  //sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dDatewiseNetValue),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dDatewiseTotalValue),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  
                  dDatewiseNetValue   = 0.00;
                  dDatewiseTotalValue = 0.00;
                  
                   }
                }
                
            // sStr = sStr + "<td   align='center' colspan='1'><font color='#CD5C5C' ><b></b></font></td>";                    
             sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>"+common.getRound(dDiffNetValue,2)+"</b></font></td>"; 
             sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>"+common.getRound(dDiffTotalValue,2)+"</b></font></td>"; 
             sStr = sStr + "<td   align='center' colspan='1'><font color='#CD5C5C' ><b></b></font></td>";
             sStr = sStr + "<td   align='center' colspan='1'><font color='#CD5C5C' ><b></b></font></td>";
             
            sStr = sStr + "</tr>";
            sStr = sStr + "</table>";
            sStr = sStr + "</body>";
            sStr = sStr + "</html>";
         
        }
        catch (Exception ex) 
        {
                  
            System.out.println("set Report--->" + ex);
            ex.printStackTrace();
        }
            
        return sStr;
    }
            
    private void CreatePDFFile() 
    {
        try 
        {
            String SPartyName       = (String)JCSupplier.getSelectedItem();
            double dPurchasedRateperKg=0.00, dPurchasedTotalValue=0.00, dOldConsumption=0.00, dOldNetValue=0.00;
            //double dNewRateperKg = 0.00, dNewTotalValue=0.00, dNewNetValue=0.00,dDiffPercentage=0.00;
            double dDiffPercentage=0.00;
            double dRateDiff=0.00, dTotalValueDiff=0.00,dNetValueDiff=0.00;
            double dPurchasedNetValue=0.00;
            dPrevNetValue         = 0.00;
            dPrevRateperKg        = 0.00;
            dPrevTotalValue       = 0.00;
            dNewRateperKg         = 0.00;
            dNewTotalValue        = 0.00;
            dNewNetValue          = 0.00;
            dDiffTotalValue       = 0.00;
            dDiffRateperKg        = 0.00;
            dPurNetValue          = 0.00;
            dPurTotalValue        = 0.00;
                  
            String PDFFile          = "/root/"+SPartyName+".pdf";
           // String sFileName        = SPartyName+".pdf";
            //String PDFFile          = common.getPrintPath()+sFileName;
            
            document                = new Document();
            PdfWriter               . getInstance(document, new FileOutputStream(PDFFile));
            document                . setPageSize(PageSize.A4.rotate());
            document                . open();
            int iPriceRevisionCount =  setMaxCount();
         //   int iColSize            = iPriceRevisionCount*7;
           // int iTotalColumns       = iColSize+14;
            
            int iColSize            = iPriceRevisionCount*7;
            int iTotalColumns       = iColSize+7;
            int iNewRateCount       = 0;
          //  System.out.println("TotalColumn==>"+iTotalColumns+"--"+iColSize);
            
             iColWidth = new int[iTotalColumns];
             iColWidth[0] = 8;
             iColWidth[1] = 35;
             iColWidth[2] = 20;
           /*  
             iColWidth[3] = 10;
             iColWidth[4] = 7;
             iColWidth[5] = 7;
             iColWidth[6] = 7;
             iColWidth[7] = 7;
             iColWidth[8] = 12;
             iColWidth[9] = 12;
             */
             for (int j = 0; j < iColSize; j++) {
               iColWidth[j+3] = 20;
             }
             int iNewRateColSize =3+iColSize;

             iColWidth[iNewRateColSize+0] = 20;
             iColWidth[iNewRateColSize+1] = 20;
             iColWidth[iNewRateColSize+2] = 12;
             iColWidth[iNewRateColSize+3] = 15;
             
            table                   =  new PdfPTable(iTotalColumns);
            table                   . setWidths(iColWidth);
            table                   . setWidthPercentage(100f);
         //   table                   . setHeaderRows(5);
            
             String sTempItemCode1="",sTempDate1="";
             String SPriceRevDate="";
            for (int i = 0; i < ADataList.size(); i++) {
                HashMap themap      = (HashMap) ADataList.get(i);
                String sPurStatus   = (String)themap.get("PURSTATUS");
                String SItemCode    = (String)themap.get("ITEMCODE");
                iNewRateCount       = getPriceRevCount(SItemCode);
                int iPurStatus      = common.toInt(sPurStatus);
                
                
             for(int j=0;j<iPriceRevisionCount;j++) { 
             if(iPurStatus==0){   
               String sDate         = common.parseDate((String)themap.get("NEWRATEDATE"));
               String sItemCode     = (String)themap.get("ITEMCODE");
               String sPurDate      = (String)themap.get("NEWRATEDATE");
               String sPriceRevSt   = (String)themap.get("PRICEREVISIONSTATUS");    
               int iPriceRevSt      = common.toInt((String)themap.get("PRICEREVISIONSTATUS")); 
               
               if(!sPurDate.equals(sTempDate1) && iPriceRevSt==0)   {     
                   SPriceRevDate    = sDate;
               }
                 sTempItemCode1 = sItemCode;
                 sTempDate1     = sPurDate;
                 break;
                }
              }
            }

            AddCellIntoTable("REVISED PRICE IN "+SPartyName+" - "+common.parseDate(SPriceRevDate), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns , 0, 0, 0, 0, bigbold,30f);
            AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns , 0, 0, 0, 0, bigbold,30f);
            
            AddCellIntoTable("Sl.No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,3);
            AddCellIntoTable("Dyes Name", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,3);
            AddCellIntoTable("One Year Consumption Qty", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,3);
         //   AddCellIntoTable("Last Purchased Rate", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 7 , 4, 1, 8, 2, smallbold1);
           
            
            String sTempItemCode="",sTempDate="";
                       
             for (int i = 0; i < ADataList.size(); i++) {
                HashMap themap      = (HashMap) ADataList.get(i);
                String sPurStatus   = (String)themap.get("PURSTATUS");
                String SItemCode    = (String)themap.get("ITEMCODE");
                iNewRateCount       = getPriceRevCount(SItemCode);
                int iPurStatus      = common.toInt(sPurStatus);
                
                
             for(int j=0;j<iPriceRevisionCount;j++) { 
             if(iPurStatus==0){   
               String sDate         = common.parseDate((String)themap.get("NEWRATEDATE"));
               String sItemCode     = (String)themap.get("ITEMCODE");
               String sPurDate      = (String)themap.get("NEWRATEDATE");
               String sPriceRevSt   = (String)themap.get("PRICEREVISIONSTATUS");    
               int iPriceRevSt      = common.toInt((String)themap.get("PRICEREVISIONSTATUS")); 
               
               if(!sPurDate.equals(sTempDate) && iPriceRevSt==1)   {     
                    AddCellIntoTable("Old Rate ("+sDate+")", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 7 , 4, 1, 8, 2,smallbold1,30f);
                      //AddCellIntoTable("Old Rate ("+sDate+")", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2,smallbold1,30f);
               }
               if(!sPurDate.equals(sTempDate) && iPriceRevSt==0)   {     
                    AddCellIntoTable("New Rate ("+sDate+")", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 7 , 4, 1, 8, 2, smallbold1,30f);
                    //break;
               }
                 sTempItemCode = sItemCode;
                 sTempDate     = sPurDate;
                 break;
                }
              }
            }

            AddCellIntoTable("Difference", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 3 , 4, 1, 8, 2, smallbold1,30f);
            AddCellIntoTable("Lab Status", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2,smallbold,3);
      /*     
            AddCellIntoTable("Rate/Kg1", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
            AddCellIntoTable("Discount1", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2 , 4, 1, 8, 2, smallbold);
            AddCellIntoTable("Tax1 ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2 , 4, 1, 8, 2, smallbold);
            AddCellIntoTable("Net Value1", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
            AddCellIntoTable("Total1", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
        */    
            for (int i = 0; i < iNewRateCount; i++) {
                AddCellIntoTable("Rate/Kg", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
                AddCellIntoTable("Discount", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2 , 4, 1, 8, 2, smallbold,30f);
                AddCellIntoTable("Tax ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2 , 4, 1, 8, 2, smallbold,30f);
                AddCellIntoTable("Net Value", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
                AddCellIntoTable("Total", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
            }
            
           // AddCellIntoTable("Rate/Kg", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
             AddCellIntoTable("Net Value", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
            AddCellIntoTable("Total Value", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
            AddCellIntoTable("%", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
        /*    
            AddCellIntoTable("%4", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold);
            AddCellIntoTable("Value4", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold);
            AddCellIntoTable("%4", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold);
            AddCellIntoTable("Value4", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold);
          */  
           for (int i = 0; i < iNewRateCount; i++) {
            AddCellIntoTable("%", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2,smallbold,30f);
            AddCellIntoTable("Value", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2,smallbold,30f);
            AddCellIntoTable("%", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,30f);
            AddCellIntoTable("Value", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,30f);
           }
                        
            for (int i = 0; i < ADataList.size(); i++) {
              HashMap themap      = (HashMap) ADataList.get(i);
              String sPurStatus   = (String)themap.get("PURSTATUS");
              int iPurStatus      = common.toInt(sPurStatus);
              if(iPurStatus==1){
              String SConsumption = (String) themap.get("CONSUMPTION");
              double dRatePerKg   = common.toDouble((String) themap.get("RATE"));
              double dDiscPer     = common.toDouble((String) themap.get("DISPER"));
              double dTaxPer      = common.toDouble((String) themap.get("TAXPER"));
              double dDisValue    = (dRatePerKg*dDiscPer)/100;
              double dTaxValue    = (dRatePerKg*dTaxPer)/100;
              double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
              double dTotalValue  = common.toDouble(SConsumption)*dNetValue;
              dOldConsumption     = common.toDouble(SConsumption);
              }
            }
            
          //  double[] dTotal = new double[20];
            ArrayList     APriceList    = new ArrayList();
            for (int i = 0; i < ADyesCodeList.size(); i++) {
                String sDyesCode       = (String)ADyesCodeList.get(i);
                String sDyesName       = (String)ADyesNameList.get(i);
                int iDyesRevisionCount = getDyeswisePriceRevisionCount(sDyesCode);
                APriceList             = getPriceList(sDyesCode);
                int iSlNo = (i+1);
                 AddCellIntoTable(String.valueOf(iSlNo), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                 AddCellIntoTable(sDyesName, table, Element.ALIGN_LEFT , Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                
              for(int j=0;j<APriceList.size();j++){
                  HashMap   theMap      = (HashMap)APriceList.get(j);
                  String sConsumption   = (String)theMap.get("CONSUMPTION");
                  String sPurStatus     = (String)theMap.get("PURSTATUS");
                  String sItemCode      = (String)theMap.get("ITEMCODE");
                  String sPriceRevStatus = (String)theMap.get("PRICEREVISIONSTATUS");
                  int iPurStatus        = common.toInt(sPurStatus);
                  int iPriceRevStatus   = common.toInt(sPriceRevStatus);

                if(iPurStatus==1){
                    
                  String sRateperKg     = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  double dNetValue      = common.toDouble(sRateperKg)-dDiscount+dTax;
                  double dTotalValue    = common.toDouble(sConsumption)*dNetValue;
              //      System.out.println("PurStatus==1>"+sRateperKg);
                  
                  AddCellIntoTable(sConsumption, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
            /*      AddCellIntoTable(sRateperKg, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
                  AddCellIntoTable(sDiscPer, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
                  AddCellIntoTable(String.valueOf(dDiscount), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
                  AddCellIntoTable(sTaxPer, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
                  AddCellIntoTable(String.valueOf(dTax), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
                  AddCellIntoTable(common.getRound(String.valueOf(dNetValue),2), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
                  AddCellIntoTable(common.getRound(String.valueOf(dTotalValue),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
              */    
                  dPurchasedRateperKg       =   common.toDouble(sRateperKg);
                  dPurchasedTotalValue      =   dTotalValue;
                  dOldNetValue              =   dNetValue;
                  
                  dPurNetValue              =   dPurNetValue+dNetValue;
                  dPurTotalValue            =   dPurTotalValue+dTotalValue;
                }
                
                if(sItemCode.startsWith("CD") && iPurStatus==0 && j==0){
                   AddCellIntoTable("0", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                   AddCellIntoTable("0", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                   AddCellIntoTable("0", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                   AddCellIntoTable("0", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                   AddCellIntoTable("0", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                   AddCellIntoTable("0", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                   AddCellIntoTable("0", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                   AddCellIntoTable("0", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                    }
                
                 for(int a=0;a<iPriceRevisionCount;a++){
                      
                    String sRateperKg      = "";
                    double dTotalValue     = 0.00, dNetValue=0.00;
                    String sRate       = (String)theMap.get("RATE")    ;

                  if(iPurStatus==0){
                //  String sConsumption   = (String)theMap.get("CONSUMPTION")    ;
                  sRateperKg            = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  //String sDate          = (String)theMap.get("NEWRATEDATE")    ;
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  dNetValue             = common.toDouble(sRateperKg)-dDiscount+dTax;
                  dOldConsumption	= common.toDouble(getConsumptionQty(sItemCode));
		  dTotalValue           = dOldConsumption*dNetValue;
                  
                  if(iPurStatus==0 && iPriceRevStatus==1){ 
                  String sDate          = (String)theMap.get("NEWRATEDATE")    ;
                //      System.out.println("inside PrevDate==>"+sDate);
                  dPrevNetValue         = getPrevNetValue(sDate,sItemCode);
                  dPrevRateperKg        = getPrevRateperKg(sDate,sItemCode);
                  dPrevTotalValue       = getPrevTotalValue(sDate,sItemCode,dOldConsumption);
                  //dPrevTotalValue       = getPrevTotalValue(sDate,sItemCode);
                  }
                  if(iPurStatus==0 && iPriceRevStatus==0){ 
                  String sDate          = (String)theMap.get("NEWRATEDATE")    ;
                  dNewRateperKg         = getNewRateperKg(sItemCode);
                  dNewTotalValue        = getNewTotalValue(sItemCode);
                  dNewNetValue          = getNewNetValue(sItemCode);
                  }
                  
                 // AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
                  AddCellIntoTable(sRateperKg, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);     
                  AddCellIntoTable(sDiscPer, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                  AddCellIntoTable(String.valueOf(dDiscount), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                  AddCellIntoTable(sTaxPer, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                  AddCellIntoTable(String.valueOf(dTax), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                  AddCellIntoTable(common.getRound(String.valueOf(dNetValue),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                  AddCellIntoTable(common.getRound(String.valueOf(dTotalValue),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);

                  dNewRateperKg        = common.toDouble(sRate);
                  dNewTotalValue       = dTotalValue;
                  dNewNetValue         = dNetValue;
                  break; 
                  }
                }  
              }
              
              String sLabStatus="";
              for(int j=0;j<APriceList.size();j++){
                  
                  HashMap   theMap      = (HashMap)APriceList.get(j);
                  String sConsumption   = (String)theMap.get("CONSUMPTION");
                  String sPurStatus     = (String)theMap.get("PURSTATUS");
                  String sItemCode      = (String)theMap.get("ITEMCODE");
                  sLabStatus            = getLabStatus(sItemCode);
                  int iPurStatus        = common.toInt(sPurStatus);
                if(iPurStatus==1){
                    
                  String sRateperKg     = (String)theMap.get("RATE")    ;
                  String sDiscPer       = (String)theMap.get("DISPER")    ;
                  String sTaxPer        = (String)theMap.get("TAXPER")    ;
                  double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
                  double dTax           = (common.toDouble(sRateperKg)*common.toDouble(sTaxPer))/100;
                  double dNetValue      = common.toDouble(sRateperKg)-dDiscount+dTax;
                  double dTotalValue    = common.toDouble(sConsumption)*dNetValue;

                  dPurchasedRateperKg   =   common.toDouble(sRateperKg);
                  dPurchasedTotalValue  =   dTotalValue;
                  dPurchasedNetValue    =   dNetValue;
                  dOldNetValue          =   dNetValue;
                }
                
                for(int a=0;a<iPriceRevisionCount;a++){
                      
                    String sRateperKg   = "";
                    double dTotalValue  = 0.00, dNetValue=0.00;
                    String sRate        = (String)theMap.get("RATE")    ;

                    dRateDiff             = dNewRateperKg - dPrevRateperKg;
                    dNetValueDiff         = dNewNetValue - dPrevNetValue;
                    dDiffPercentage       = ((dNewNetValue - dPrevNetValue) / dPrevNetValue) * 100;
                    dTotalValueDiff       = dNewTotalValue- dPrevTotalValue;
                    
                   // dDiffPercentage       = ((dNewNetValue - dPrevNetValue) / dPrevNetValue) * 100;
                   }
                  }
                  
                    dDiffTotalValue       =  dDiffTotalValue+dTotalValueDiff;
                    dDiffNetValue         =  dDiffNetValue+dNetValueDiff;
                    dDiffRateperKg        =  dDiffRateperKg+dRateDiff;
               
                 AddCellIntoTable(common.getRound(String.valueOf(dNetValueDiff),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                 AddCellIntoTable(common.getRound(String.valueOf(dTotalValueDiff),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                 AddCellIntoTable(common.getRound(String.valueOf(dDiffPercentage),2), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                 AddCellIntoTable(sLabStatus, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
              //break;  
                //}
               }
               AddCellIntoTable("Grand Total", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 3 , 4, 1, 8, 2, mediumbold1,30f);
                
              double dDatewiseNetValue=0.00 , dDatewiseTotalValue=0.00; 
              double dDatewisePrevNetValue=0.00 , dDatewisePrevTotalValue=0.00; 
              
              for(int j=0;j<APriceList.size();j++){     
                  HashMap   theMap      = (HashMap)APriceList.get(j);
                  String sConsumption   = (String)theMap.get("CONSUMPTION");
                  String sPurStatus     = (String)theMap.get("PURSTATUS");
                  String sItemCode      = (String)theMap.get("ITEMCODE");
                  int iPurStatus        = common.toInt(sPurStatus);
                 if(iPurStatus==0){
                  String sDate          = (String)theMap.get("NEWRATEDATE")    ;
                     
                  dDatewiseNetValue     =  getDatewiseNetValue(sDate);
                  dDatewiseTotalValue   =  getDatewiseTotalValue(sDate,sItemCode);
                  

                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 5 , 4, 1, 8, 2, mediumbold1,30f);                  
                AddCellIntoTable(common.getRound(String.valueOf(dDatewiseNetValue),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);                  
                AddCellIntoTable(common.getRound(String.valueOf(dDatewiseTotalValue),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);                  
                  
                  dDatewiseNetValue   = 0.00;
                  dDatewiseTotalValue = 0.00;
               }
             }
           //  AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);                     
             AddCellIntoTable(common.getRound(String.valueOf(dDiffNetValue/2),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);                  
             AddCellIntoTable(common.getRound(String.valueOf(dDiffTotalValue),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);                  
             AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);                     
             AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);                     
        //   }
                document.add(table);
                document.close();
            }
            catch(Exception e){
                e.printStackTrace();
            }
       

    }
    private int getPurStatusCount(){
        int iCount=0;
        for(int i=0;i<ADataList.size();i++){
            HashMap  theMap     = (HashMap)ADataList.get(i);
            String sIName       = (String)theMap.get("ITEMNAME");
            String sPurStatus   = (String)theMap.get("PURSTATUS");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            if(iPurStatus==0){
                iCount          = iCount+1;
            }
        }
        return iCount;
    }
    
    private int getDyeswisePriceRevisionCount(String sDyesCode){
        int iCount=0;
        for(int i=0;i<ADataList.size();i++){
            HashMap  theMap     = (HashMap)ADataList.get(i);
            String sICode       = (String)theMap.get("ITEMCODE");
            String sPStatus     = (String)theMap.get("PRICEREVISIONSTATUS");
            int iPStatus        = common.toInt((String)theMap.get("PRICEREVISIONSTATUS"));
         //   System.out.println("DyesName-->"+sDyesCode+"---"+sICode+"----"+sPStatus);
            if(sDyesCode.trim().equals(sICode) && iPStatus!=2){
                iCount          = iCount+1;
            }
        }
      //  System.out.println("DyesCount===>"+iCount);
        return iCount;
    }
    
    private ArrayList getPriceList(String sDyesCode){
        ArrayList APriceList= new ArrayList();
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            String sItemCode    = (String)theMap.get("ITEMCODE");
            
            if(sDyesCode.trim().equals(sItemCode)){
            String sPurStatus   = (String)theMap.get("PURSTATUS");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            if(iPurStatus==1){
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sID          = (String)theMap.get("ID");
            String sItemName    = (String)theMap.get("ITEMNAME");
            String sConsumption = (String)theMap.get("CONSUMPTION");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");  
            String SLabStatus   = (String)theMap.get("LABSTATUS");  
        //    System.out.println("ItemCode inside get PriceList===>"+sItemCode);
            APriceList          . add(theMap);
             }
            
            else if(iPurStatus==0){
            String sID          = (String)theMap.get("ID");
            String sItemName    = (String)theMap.get("ITEMNAME");
            String sConsumption = (String)theMap.get("CONSUMPTION");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            String SLabStatus   = (String)theMap.get("LABSTATUS");  
            APriceList          . add(theMap);
             }
            }
        }
        return APriceList;
    }
    
    private double getDatewiseNetValue(String sDate){
        double dValue=0.00;
        //System.out.println("Data List Size===>"+ADataList.size());
        
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            String sRateDate    = (String)theMap.get("NEWRATEDATE");
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sItemName    = (String)theMap.get("ITEMNAME");
            String sWithEffectTo = (String)theMap.get("WITHEFFECTTO");
            String sPurStatus   = (String)theMap.get("PURSTATUS");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            if(iPurStatus==0){
                //System.out.println("Date====>"+sDate+"--"+sWithEffectTo+"---"+sItemCode+"---"+SItemCode);
            if(sDate.trim().equals(sRateDate))    {
        
            String sID          = (String)theMap.get("ID");
            String sConsumption = (String)theMap.get("CONSUMPTION");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            double dDiscPer     = common.toDouble(SDiscper);
            double dTaxPer      = common.toDouble(STaxper);
            double dDisValue    = (dRatePerKg*dDiscPer)/100;
            double dTaxValue    = (dRatePerKg*dTaxPer)/100;
            double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
                
            dValue              = dValue+dNetValue;
             }
           }
        }
        return dValue;
    }
    
    
    private double getDatewiseTotalValue(String sDate){
        double dValue=0.00;
        String SConsumption="";
        //System.out.println("Data List Size===>"+ADataList.size());
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            int iPurStatus      = common.toInt(String.valueOf(theMap.get("PURSTATUS")));
            if(iPurStatus==1){
                 SConsumption   = (String) theMap.get("CONSUMPTION");
            }   
            String sRateDate    = (String)theMap.get("NEWRATEDATE");
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sWithEffectTo = (String)theMap.get("WITHEFFECTTO");
            
            if(iPurStatus==0){
                //System.out.println("Date====>"+sDate+"--"+sWithEffectTo+"---"+sItemCode+"---"+SItemCode);
            if(sDate.trim().equals(sRateDate))    {
            String sID          = (String)theMap.get("ID");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            double dDiscPer     = common.toDouble(SDiscper);
            double dTaxPer      = common.toDouble(STaxper);
            double dDisValue    = (dRatePerKg*dDiscPer)/100;
            double dTaxValue    = (dRatePerKg*dTaxPer)/100;
            double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
            double dTotalValue  = common.toDouble(SConsumption)*dNetValue;   
            //    System.out.println("DateWise TotalValue===============>"+SConsumption+"--"+dNetValue+"--"+dTotalValue);
            dValue              = dValue+dTotalValue;
             }
           }
        }
      //  System.out.println("Datewise Total Value===>"+sDate+"---"+dValue);
        return dValue;
    }
    
    private double getDatewiseTotalValue(String sDate,String sItemCode){
        double dValue=0.00, dRtTotalValue=0.00;
        String SConsumption="";
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            int iPurStatus      = common.toInt(String.valueOf(theMap.get("PURSTATUS")));
            int iPriceRevStatus = common.toInt(String.valueOf(theMap.get("PRICEREVISIONSTATUS")));
            String SItemCode    = (String)theMap.get("ITEMCODE");
            
            if(iPurStatus==1){
                String SItmCode  = (String)theMap.get("ITEMCODE");
                 SConsumption   = (String) theMap.get("CONSUMPTION");
               //  System.out.println("Inside==>"+SItmCode+"---"+SConsumption);
            }   
            String sRateDate    = (String)theMap.get("NEWRATEDATE");
            SItemCode           = (String)theMap.get("ITEMCODE");
            String sWithEffectTo = (String)theMap.get("WITHEFFECTTO");
            
            if(iPurStatus==0 && iPriceRevStatus==1){
             if(sDate.trim().equals(sRateDate)){
                
                
            String SItmCode  = (String)theMap.get("ITEMCODE");    
            double dOldConsumption = getItemWiseConsumptionValue(SItemCode);
          //  System.out.println("L1------>"+sItemCode+"---"+SItmCode);
            String sID          = (String)theMap.get("ID");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            double dDiscPer     = common.toDouble(SDiscper);
            double dTaxPer      = common.toDouble(STaxper);
            double dDisValue    = (dRatePerKg*dDiscPer)/100;
            double dTaxValue    = (dRatePerKg*dTaxPer)/100;
            double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
            double dTotalValue  = dOldConsumption*dNetValue;   
               // System.out.println("DateWise TotalValue= Prev1==============>"+dOldConsumption+"--"+dNetValue+"--"+dTotalValue);
               dValue              = dValue+dTotalValue;
               
             }
           }
            
            if(iPurStatus==0 && iPriceRevStatus==0){
            if(sDate.trim().equals(sRateDate))    {
             String SItmCode  = (String)theMap.get("ITEMCODE");    
            double dOldConsumption = getItemWiseConsumptionValue(SItemCode);
         //   System.out.println("L1------>"+sItemCode+"---"+SItmCode);   
            String sID          = (String)theMap.get("ID");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            double dDiscPer     = common.toDouble(SDiscper);
            double dTaxPer      = common.toDouble(STaxper);
            double dDisValue    = (dRatePerKg*dDiscPer)/100;
            double dTaxValue    = (dRatePerKg*dTaxPer)/100;
            double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
            double dTotalValue  = dOldConsumption*dNetValue;   
                //System.out.println("DateWise TotalValue====Prev0===========>"+SConsumption+"--"+dNetValue+"--"+dTotalValue);
               dValue           = dValue+dTotalValue;
             }
           }
            
        }
      //  System.out.println("Datewise Total Value===>"+sDate+"---"+dValue);
        return dValue;
    }
    
   private double getItemWiseConsumptionValue(String sItemCode){
       double dValue=0.00;
       for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            int iPurStatus      = common.toInt(String.valueOf(theMap.get("PURSTATUS")));
            int iPriceRevStatus = common.toInt(String.valueOf(theMap.get("PRICEREVISIONSTATUS")));
            String SItemCode    = (String)theMap.get("ITEMCODE");
            if(iPurStatus==1 && sItemCode.trim().equals(SItemCode)){
                String SItmCode  = (String)theMap.get("ITEMCODE");
                dValue           = common.toDouble((String) theMap.get("CONSUMPTION"));
            }   
        }
       return dValue;
     
   }
    
    
    private double getDatewiseTotalValue(String sDate,double dOldConsumption){
        double dValue=0.00;
        String SConsumption="";
        //System.out.println("Data List Size===>"+ADataList.size());
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            int iPurStatus      = common.toInt(String.valueOf(theMap.get("PURSTATUS")));
            if(iPurStatus==1){
                 SConsumption   = (String) theMap.get("CONSUMPTION");
            }   
            String sRateDate    = (String)theMap.get("NEWRATEDATE");
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sWithEffectTo = (String)theMap.get("WITHEFFECTTO");
            
            if(iPurStatus==0){
                //System.out.println("Date====>"+sDate+"--"+sWithEffectTo+"---"+sItemCode+"---"+SItemCode);
            if(sDate.trim().equals(sRateDate))    {
            String sID          = (String)theMap.get("ID");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            double dDiscPer     = common.toDouble(SDiscper);
            double dTaxPer      = common.toDouble(STaxper);
            double dDisValue    = (dRatePerKg*dDiscPer)/100;
            double dTaxValue    = (dRatePerKg*dTaxPer)/100;
            double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
            double dTotalValue  = dOldConsumption*dNetValue;    
            System.out.println("Datewise Total Value===>"+sDate+"---"+dOldConsumption+"---"+dNetValue+"---"+dTotalValue+"--"+SConsumption);
            dValue              = dValue+dTotalValue;
             }
           }
        }
      System.out.println("Datewise RT Value===>"+sDate+"---"+dValue);
        return dValue;
    }
    
    private double getNewRateperKg(String sItemCode){
        double dValue=0.00;
        //System.out.println("Data List Size===>"+ADataList.size());
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            String sRateDate    = (String)theMap.get("NEWRATEDATE");
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sItemName    = (String)theMap.get("ITEMNAME");
            String sWithEffectTo = (String)theMap.get("WITHEFFECTTO");
           
            String sPurStatus   = (String)theMap.get("PURSTATUS");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            if(iPurStatus==0){
                //System.out.println("Date====>"+sDate+"--"+sWithEffectTo+"---"+sItemCode+"---"+SItemCode);
            if(sItemCode.trim().equals(SItemCode)&& sWithEffectTo.equals("99999999"))    {
            String sID          = (String)theMap.get("ID");
            String sConsumption = (String)theMap.get("CONSUMPTION");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            dValue              = dRatePerKg;
             }
           }
        }
        return dValue;
    }
    
    private double getNewTotalValue(String sItemCode,double dOldConsumption){
        double dValue=0.00;
        String SConsumption="";
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            String sRateDate    = (String)theMap.get("NEWRATEDATE");
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sItemName    = (String)theMap.get("ITEMNAME");
            String sWithEffectTo = (String)theMap.get("WITHEFFECTTO");
           
            String sPurStatus   = (String)theMap.get("PURSTATUS");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            if(iPurStatus==1){
                 SConsumption   = (String) theMap.get("CONSUMPTION");
            }   
            if(iPurStatus==0){
                //System.out.println("Date====>"+sDate+"--"+sWithEffectTo+"---"+sItemCode+"---"+SItemCode);
            if(sItemCode.trim().equals(SItemCode)&& sWithEffectTo.equals("99999999"))    {
           String sID          = (String)theMap.get("ID");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            double dDiscPer     = common.toDouble(SDiscper);
            double dTaxPer      = common.toDouble(STaxper);
            double dDisValue    = (dRatePerKg*dDiscPer)/100;
            double dTaxValue    = (dRatePerKg*dTaxPer)/100;
            double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
            double dTotalValue  = dOldConsumption*dNetValue;    
            dValue              = dValue+dTotalValue;
             }
           }
        }
        return dValue;
    }
    
    private double getNewTotalValue(String sItemCode){
        double dValue=0.00;
        String SConsumption="";
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            String sRateDate    = (String)theMap.get("NEWRATEDATE");
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sItemName    = (String)theMap.get("ITEMNAME");
            String sWithEffectTo = (String)theMap.get("WITHEFFECTTO");
           
            String sPurStatus   = (String)theMap.get("PURSTATUS");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            if(iPurStatus==1){
                 SConsumption   = (String) theMap.get("CONSUMPTION");
            }   
            if(iPurStatus==0){
                //System.out.println("Date====>"+sDate+"--"+sWithEffectTo+"---"+sItemCode+"---"+SItemCode);
            if(sItemCode.trim().equals(SItemCode)&& sWithEffectTo.equals("99999999"))    {
           String sID          = (String)theMap.get("ID");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            double dDiscPer     = common.toDouble(SDiscper);
            double dTaxPer      = common.toDouble(STaxper);
            double dDisValue    = (dRatePerKg*dDiscPer)/100;
            double dTaxValue    = (dRatePerKg*dTaxPer)/100;
            double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
            double dTotalValue  = common.toDouble(SConsumption)*dNetValue;    
            dValue              = dValue+dTotalValue;
             }
           }
        }
        return dValue;
    }
    
    private double getNewNetValue(String sItemCode){
        double dValue=0.00;
        String SConsumption="";
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            String sRateDate    = (String)theMap.get("NEWRATEDATE");
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sItemName    = (String)theMap.get("ITEMNAME");
            String sWithEffectTo = (String)theMap.get("WITHEFFECTTO");
           
            String sPurStatus   = (String)theMap.get("PURSTATUS");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            if(iPurStatus==1){
                 SConsumption   = (String) theMap.get("CONSUMPTION");
            }   
            if(iPurStatus==0){
                //System.out.println("Date====>"+sDate+"--"+sWithEffectTo+"---"+sItemCode+"---"+SItemCode);
            if(sItemCode.trim().equals(SItemCode)&& sWithEffectTo.equals("99999999"))    {
           String sID          = (String)theMap.get("ID");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            double dDiscPer     = common.toDouble(SDiscper);
            double dTaxPer      = common.toDouble(STaxper);
            double dDisValue    = (dRatePerKg*dDiscPer)/100;
            double dTaxValue    = (dRatePerKg*dTaxPer)/100;
            double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
           // double dTotalValue  = common.toDouble(SConsumption)*dNetValue;    
            dValue              = dValue+dNetValue;
             }
           }
        }
        return dValue;
    }
    
    private double getOldNetValue(String sItemCode){
        double dValue=0.00;
        String SConsumption="";
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            String sRateDate    = (String)theMap.get("NEWRATEDATE");
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sItemName    = (String)theMap.get("ITEMNAME");
            String sWithEffectTo = (String)theMap.get("WITHEFFECTTO");
           
            String sPurStatus   = (String)theMap.get("PURSTATUS");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            if(iPurStatus==1){
                 SConsumption   = (String) theMap.get("CONSUMPTION");
            
           if(sItemCode.trim().equals(SItemCode))    {
            String sID          = (String)theMap.get("ID");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            double dDiscPer     = common.toDouble(SDiscper);
            double dTaxPer      = common.toDouble(STaxper);
            double dDisValue    = (dRatePerKg*dDiscPer)/100;
            double dTaxValue    = (dRatePerKg*dTaxPer)/100;
            double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
           // double dTotalValue  = common.toDouble(SConsumption)*dNetValue;    
            dValue              = dValue+dNetValue;
           }
          }
        }
        System.out.println("Inside Method==>"+dValue);
        return dValue;
    }
    
    private double getOldRateperKg(String sItemCode){
        double dValue=0.00;
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            String SItemCode    = (String)theMap.get("ITEMCODE");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            if(iPurStatus==1){
            if(sItemCode.trim().equals(SItemCode))    {
            String sID          = (String)theMap.get("ID");
            String sConsumption = (String)theMap.get("CONSUMPTION");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            dValue              = dRatePerKg;
             }
           }
        }
        return dValue;
    }
    private double getOldTotalValue(String sItemCode){
        double dValue=0.00;
        String SConsumption="";
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            String sRateDate    = (String)theMap.get("NEWRATEDATE");
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sItemName    = (String)theMap.get("ITEMNAME");
            String sWithEffectTo = (String)theMap.get("WITHEFFECTTO");
           
            String sPurStatus   = (String)theMap.get("PURSTATUS");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            if(iPurStatus==1){
              SConsumption   = (String) theMap.get("CONSUMPTION");
            
           if(sItemCode.trim().equals(SItemCode))    {
            String sID          = (String)theMap.get("ID");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            double dDiscPer     = common.toDouble(SDiscper);
            double dTaxPer      = common.toDouble(STaxper);
            double dDisValue    = (dRatePerKg*dDiscPer)/100;
            double dTaxValue    = (dRatePerKg*dTaxPer)/100;
            double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
            double dTotalValue  = common.toDouble(SConsumption)*dNetValue;    
            dValue              = dValue+dTotalValue;
           }
          }
        }
        return dValue;
    }
    
    private double getPrevNetValue(String sDate,String sItemCode){
        double dValue=0.00;
        //System.out.println("Data List Size===>"+ADataList.size());
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sItemName    = (String)theMap.get("ITEMNAME");
            String sWithEffectTo = (String)theMap.get("NEWRATEDATE");
         //   System.out.println("ItemName===>"+sItemName);
           
            String sPurStatus   = (String)theMap.get("PURSTATUS");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            if(iPurStatus==0){
            //    System.out.println("Date====>"+sDate+"--"+sWithEffectTo+"---"+sItemCode+"---"+SItemCode);
            if(sDate.trim().equals(sWithEffectTo) && sItemCode.trim().equals(SItemCode))    {
            String sID          = (String)theMap.get("ID");
            String sConsumption = (String)theMap.get("CONSUMPTION");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            double dDiscPer     = common.toDouble(SDiscper);
            double dTaxPer      = common.toDouble(STaxper);
            double dDisValue    = (dRatePerKg*dDiscPer)/100;
            double dTaxValue    = (dRatePerKg*dTaxPer)/100;
            double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
                
            dValue              = dNetValue;
            break;
             }
           }
        }
        return dValue;
    }
    
    private double getPrevRateperKg(String sDate,String sItemCode){
        double dValue=0.00;
        //System.out.println("Data List Size===>"+ADataList.size());
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sItemName    = (String)theMap.get("ITEMNAME");
            String sWithEffectTo = (String)theMap.get("WITHEFFECTTO");
         //   System.out.println("ItemName===>"+sItemName);
           
            String sPurStatus   = (String)theMap.get("PURSTATUS");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            if(iPurStatus==0){
                //System.out.println("Date====>"+sDate+"--"+sWithEffectTo+"---"+sItemCode+"---"+SItemCode);
            if(sDate.trim().equals(sWithEffectTo) && sItemCode.trim().equals(SItemCode))    {
            String sID          = (String)theMap.get("ID");
            String sConsumption = (String)theMap.get("CONSUMPTION");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            dValue              = dRatePerKg;
             }
           }
        }
        return dValue;
    }
    
    private double getPrevTotalValue(String sDate,String sItemCode,double dOldConsumption){
        double dValue=0.00;
        String SConsumption="";
        //System.out.println("Data List Size===>"+ADataList.size());
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            int iPurStatus      = common.toInt(String.valueOf(theMap.get("PURSTATUS")));
            if(iPurStatus==1){
                 SConsumption   = (String) theMap.get("CONSUMPTION");
            }   
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sWithEffectTo = (String)theMap.get("NEWRATEDATE");
            
            if(iPurStatus==0){
                //System.out.println("Date====>"+sDate+"--"+sWithEffectTo+"---"+sItemCode+"---"+SItemCode);
            if(sDate.trim().equals(sWithEffectTo) && sItemCode.trim().equals(SItemCode))    {
            String sID          = (String)theMap.get("ID");
            //String sConsumption = (String)theMap.get("CONSUMPTION");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            double dDiscPer     = common.toDouble(SDiscper);
            double dTaxPer      = common.toDouble(STaxper);
            double dDisValue    = (dRatePerKg*dDiscPer)/100;
            double dTaxValue    = (dRatePerKg*dTaxPer)/100;
            double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
            double dTotalValue  = dOldConsumption*dNetValue;    
            dValue              = dTotalValue;
             }
           }
        }
        return dValue;
    }
    
    private double getPrevTotalValue(String sDate,String sItemCode){
        double dValue=0.00;
        String SConsumption="";
        //System.out.println("Data List Size===>"+ADataList.size());
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            int iPurStatus      = common.toInt(String.valueOf(theMap.get("PURSTATUS")));
            if(iPurStatus==1){
                 SConsumption   = (String) theMap.get("CONSUMPTION");
            }   
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sWithEffectTo = (String)theMap.get("NEWRATEDATE");
            
            if(iPurStatus==0){
                //System.out.println("Date====>"+sDate+"--"+sWithEffectTo+"---"+sItemCode+"---"+SItemCode);
            if(sDate.trim().equals(sWithEffectTo) && sItemCode.trim().equals(SItemCode))    {
            String sID          = (String)theMap.get("ID");
            //String sConsumption = (String)theMap.get("CONSUMPTION");
            String SRateperKg   = (String)theMap.get("RATE");    
            String SDiscper     = (String)theMap.get("DISPER");    
            String STaxper      = (String)theMap.get("TAXPER");    
            double dRatePerKg   = common.toDouble(SRateperKg);
            double dDiscPer     = common.toDouble(SDiscper);
            double dTaxPer      = common.toDouble(STaxper);
            double dDisValue    = (dRatePerKg*dDiscPer)/100;
            double dTaxValue    = (dRatePerKg*dTaxPer)/100;
            double dNetValue    = dRatePerKg-dDisValue+dTaxValue;
            double dTotalValue  = common.toDouble(SConsumption)*dNetValue;    
            dValue              = dTotalValue;
             }
           }
        }
        return dValue;
    }
    
    
    private int getPriceRevCount(String sItemCode){
        int iCount=0;
        for(int i=0;i<ADataList.size();i++) {
            HashMap  theMap     = (HashMap)ADataList.get(i);
            
            String SItemCode    = (String)theMap.get("ITEMCODE");
            String sItemName    = (String)theMap.get("ITEMNAME");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
         if(sItemCode.trim().equals(SItemCode)){   
            if(iPurStatus==0){
                iCount          = iCount+1;
             } 
         }
        }
      //  System.out.println("OCount==>"+iCount);
        return iCount;
    }
    
    
    public int setMaxCount() {
  
        PreparedStatement pst       = null;
        ResultSet rst               = null;
        StringBuffer sb             = new StringBuffer();
        String SPartyCode           = getSupplierCode((String)JCSupplier.getSelectedItem());
        int icount                  = 0;
        try {
                sb.append("    Select Max(Cnt) from ( ");
                sb.append("    Select Dyes_Code,Count(1) as Cnt from ( ");
                sb.append("    Select  Dyes_Code,rank() over(partition by DyesPriceRevision.Dyes_Code ");
                sb.append("    order by to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') desc) as RateRank ");
                sb.append("    from DyesPriceRevision ");
                sb.append("    Where DyesPriceRevision.Supp_Code='"+SPartyCode+"'  and Dyes_Code in( '");
                
                    for(int a=0;a<ADyesCodeList.size();a++){
                        String sLstDyesCode    = (String)ADyesCodeList.get(a);
                         if(a==0){
                            sb.append(""+sLstDyesCode+"' ");
                          }else{
                            sb.append(",'"+sLstDyesCode+"'");
                       }
                    }
                sb.append(" ) )");
                sb.append("    Where RateRank<=3  ");
                sb.append("    Group by Dyes_Code ");
                sb.append("    ) ");
               
              
        //      System.out.println("countQry=>"+sb.toString());
             if (theDyeConnection == null) {
                DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                theDyeConnection = jdbc.getConnection();
            }
            pst = theDyeConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd = rst.getMetaData();
            while (rst.next()) {
                
               icount=rst.getInt(1);

            }
            rst.close();
            pst.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        
      //   System.out.println("count"+icount);
         return icount;
    }
    
    
    
    public ArrayList getRateDate() {
        ArrayList   ARateDateList   = new ArrayList();
        PreparedStatement pst       = null;
        ResultSet rst               = null;
        StringBuffer sb             = new StringBuffer();
        String SPartyCode           = getSupplierCode((String)JCSupplier.getSelectedItem());
        int icount                  = 0;
        try {
                sb.append("    Select distinct NewRateDate from (     ");
                sb.append("    Select  to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') as NewRateDate ");
                sb.append("    from DyesPriceRevision ");
                sb.append("    Where DyesPriceRevision.Supp_Code='"+SPartyCode+"'  and Dyes_Code in( '");
                    for(int a=0;a<ADyesCodeList.size();a++){
                        String sLstDyesCode    = (String)ADyesCodeList.get(a);
                         if(a==0){
                            sb.append(""+sLstDyesCode+"' ");
                          }else{
                            sb.append(",'"+sLstDyesCode+"'");
                       }
                    }
                sb.append(" )");
                sb.append(" Order by DyesPriceRevision.ID desc  ");
                sb.append(" )");
                sb.append("  Where RowNum<=3 ");
                sb.append(" Order by 1 asc ");
               
              
          //    System.out.println("countQry=>"+sb.toString());
             if (theDyeConnection == null) {
                DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                theDyeConnection = jdbc.getConnection();
            }
            pst = theDyeConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd = rst.getMetaData();
            while (rst.next()) {
               ARateDateList    . add(rst.getString(1)) ;
            }
            rst.close();
            pst.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        
       // System.out.println("Rate List"+ARateDateList);
         return ARateDateList;
    }
    
    private String getLabStatus(String SItemCode){
            String sLabStatus="";
        for(int i=0;i<ADataList.size();i++){
            HashMap  theMap     = (HashMap)ADataList.get(i);
            String sIName       = (String)theMap.get("ITEMNAME");
            String sItemCode    = (String)theMap.get("ITEMCODE");
            //String sIName       = (String)theMap.get("ITEMNAME");
            String sPriceRevStatus   = (String)theMap.get("PRICEREVISIONSTATUS");
            int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            if(iPurStatus==0){
               if(sPriceRevStatus.trim().equals("0")) {
                  if(SItemCode.trim().equals(sItemCode)) {
                    sLabStatus   = (String)theMap.get("LABSTATUS");  
                  }
               }
            }
        }
        return sLabStatus;
    }
    

private String getConsumptionQty(String SItemCode){
            String sConsumption="";
        for(int i=0;i<ADataList.size();i++){
            HashMap  theMap     = (HashMap)ADataList.get(i);
            String sItemCode    = (String)theMap.get("ITEMCODE");
            int iPrevStatus     = common.toInt((String)theMap.get("PRICEREVISIONSTATUS"));
         //   System.out.println("ItemCode int Method==>"+SItemCode+"--"+sItemCode);
            if(SItemCode.trim().equals(sItemCode) && iPrevStatus==2){
                sConsumption    = (String)theMap.get("CONSUMPTION");
            }
         }
        return sConsumption;
    }
    


   /* public static void main(String[] args) 
    {
        PriceRevisiedPrint theFrame = new PriceRevisiedPrint();
    }*/
    

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont,float fHeight) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }


    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont,float fHeight,float fBorderRight) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorderWidthRight(fBorderRight);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }
}
