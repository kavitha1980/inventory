
package DyesPriceRevision;

import java.util.Vector;
import javax.swing.table.DefaultTableModel;

public class PriceRevisionCostingModel extends DefaultTableModel {
  
	String ColumnName[]  = {"SNO", "PARTY NAME", "DYES NAME", "ANNUAL CONSUMPTION", "RATE/KG", "DIS %", "DISCOUNT", "%", "VALUE", "%", "VALUE", "%", "VALUE", "%", "VALUE", "NET VALUE", "TOTAL VALUE", "LABSTATUS"};
	String  ColumnType[] = { "S" , "S"         , "S"        , "S"                 , "S"      , "S"    , "S"       , "S" , "S"   , "S", "S"    , "S", "S"    , "S", "S", "S" , "S", "S" };
	int iColumnWidth[]   = { 8 , 50 , 25, 40, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15 };  

	public PriceRevisionCostingModel() {
		setDataVector(getRowData(),ColumnName);
	}   
	
	public Class getColumnClass(int iCol) {
    	
    	return getValueAt(0,iCol).getClass();
	}

	public boolean isCellEditable(int iRow,int iCol)
	{
		if (ColumnType[iCol]=="E"||ColumnType[iCol]=="B")
		    return true;
		    
	    	return false;
	}
	
	private Object[][]getRowData()
	{
		Object RowData[][]=new Object[1][ColumnName.length];
		
		for(int i=0;i<ColumnName.length;i++)
		    RowData[0][i]="";
		    
		return RowData;
	}
	
	public void appendRow(Vector theVect)
	{
		insertRow(getRows(),theVect);
	}
	
	public int getRows()
	{
		return super.dataVector.size();
	}

}
