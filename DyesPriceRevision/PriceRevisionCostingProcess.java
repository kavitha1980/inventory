package DyesPriceRevision;

import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.InetAddress;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import java.text.*;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;


public class PriceRevisionCostingProcess extends JInternalFrame {
    
    JLayeredPane				Layer;
    
    JPanel                      pnlMain,pnlData,pnlControl;
    
    JButton                     btnProcess;
    
    PriceRevisionCostingModel   theModel;
    JTable                      theTable;
    
    String                      sAuthSysName;
    
    JRadioButton                rbtnInsert, rbtnUpdate;
    
    Common                      common  = new Common();
    
    DyeingData                  data    = new DyeingData();
    
    int  iSNO = 0, iPARTYNAME = 1, iDYESNAME = 2, iANNUALCONSUMPTION = 3, iRATEPerKG = 4, iDISPer = 5, iDISCOUNT = 6, iCGSTPer = 7, iCGST = 8, iSGSTPer = 9, iSGST = 10, iIGSTPer = 11, iIGST = 12, iTAXPer = 13, iTAX = 14, iNETVALUE = 15, iTOTALVALUE = 16, iLABSTATUS = 17, iSelect = 18;
    
    
	public PriceRevisionCostingProcess(JLayeredPane Layer){

    	this.Layer			= Layer;
    	
		createComponent();
		addLayout();
		addListener();
		addComponent();
		setTableData();
	}   

	private void createComponent(){
    
		pnlMain             =       new JPanel();
		pnlData             =       new JPanel();
		pnlControl          =       new JPanel();
		
		rbtnInsert          =       new JRadioButton("Insert", true);
		rbtnUpdate          =       new JRadioButton("Update", true);
		
		btnProcess          =       new JButton("Costing Process");
		
		theModel            =       new PriceRevisionCostingModel();
		theTable            =       new JTable();
		
	    theTable             .       setColumnModel(new GroupableTableColumnModel());
	    theTable             .       setTableHeader(new GroupableTableHeader((GroupableTableColumnModel) theTable.getColumnModel()));
	    theTable             .       setModel(theModel);
		    
	    GroupableTableColumnModel cm = (GroupableTableColumnModel) theTable.getColumnModel();
	    ColumnGroup g_CGST   =       new ColumnGroup("CGST");
	    g_CGST               .       add(cm.getColumn(iCGSTPer));
	    g_CGST               .       add(cm.getColumn(iCGST));
	    cm                   .       addColumnGroup(g_CGST);
	   
	    ColumnGroup g_SGST   = new ColumnGroup("SGST");
	    g_SGST               .       add(cm.getColumn(iSGSTPer));
	    g_SGST               .       add(cm.getColumn(iSGST));
	    cm                   .       addColumnGroup(g_SGST);
	   
	    ColumnGroup g_IGST   = new ColumnGroup("IGST");
	    g_IGST               .       add(cm.getColumn(iIGSTPer));
	    g_IGST               .       add(cm.getColumn(iIGST));
	    cm                   .       addColumnGroup(g_IGST);
	   
	    ColumnGroup g_Tax    = new ColumnGroup("TAX");
	    g_Tax                .       add(cm.getColumn(iTAXPer));
	    g_Tax                .       add(cm.getColumn(iTAX));
	    cm                   .       addColumnGroup(g_Tax);
	   
	}

	private void addLayout(){
    
        this            . setSize(1150, 600);
        this            . setTitle("DYES COSTING PROCESS FRAME");
        this			. setMaximizable(true);
        this			. setClosable(true);
        this            . setResizable(true);
    
        pnlData         . setBorder(new TitledBorder("Data"));
        pnlData         . setLayout(new BorderLayout());
        
        pnlControl      . setBorder(new TitledBorder("Selected OrderType"));
        pnlControl      . setLayout(new FlowLayout(FlowLayout.CENTER));
        
        pnlMain         . setLayout(new BorderLayout());
        
	}

	private void addListener() {
    	 //theTable               . addKeyListener(new KeyList());
    	 btnProcess             . addActionListener(new ActList());
	}

	private void addComponent() {
    	
    	pnlData                 .   add(new JScrollPane(theTable), BorderLayout.CENTER);
    	pnlControl              .   add(btnProcess);
    	
    	//pnlMain                 .   add(pnlData,BorderLayout.NORTH);
    	//pnlMain                 .   add(pnlControl,BorderLayout.SOUTH);
    	
    	getContentPane().add("Center",pnlData);
    	getContentPane().add("South",pnlControl);
    	
    	//this                    .   add(pnlMain);
	}

	private class ActList implements ActionListener {
	   public void actionPerformed(ActionEvent ae) {
		    if (ae.getSource() == btnProcess) {
	            setDyesCostingProcess();
		    }
	   }
	}

	private void setTableData() {
    	theModel.setNumRows(0);
    	
		int iSlNo = 0;
		
    	data.setNewDyesDetails();
    	
		for(int i=0;i<data.ADyesDetailList.size();i++){
		    HashMap      theMap      = (HashMap)data.ADyesDetailList.get(i);
		    
		    String sDyes_Name        = (String)theMap.get("ITEMNAME");
		    String sConsumption      = (String)theMap.get("ANNUALCONSUMPTION");
		    String sWithEffectFrom   = (String)theMap.get("WITHEFFECTFROM");
		    String sWithEffectTo     = (String)theMap.get("WITHEFFECTTO");
		    String sRateperKg        = (String)theMap.get("RATEPERKG");
		    String sDiscount         = (String)theMap.get("DISCOUNT");
		    String sTax              = (String)theMap.get("TAX");
		    String sNetValue         = (String)theMap.get("NETVALUE");
		    String sTotalValue       = (String)theMap.get("TOTALVALUE");
		    String sDiscpercentage   = (String)theMap.get("DISCPER");
		    String sTaxpercentage    = (String)theMap.get("TAXPER");
		    String sPartyName        = (String)theMap.get("NAME");
		    String sLabStatus        = (String)theMap.get("STATUS");
		    
		    String sCGSTPercentage   = (String)theMap.get("CGSTPER");
		    String sSGSTPercentage   = (String)theMap.get("SGSTPER");
		    String sIGSTPercentage   = (String)theMap.get("IGSTPER");
		    
		    String sCGST             = (String)theMap.get("CGSTAMOUNT");
		    String sSGST             = (String)theMap.get("SGSTAMOUNT");
		    String sIGST             = (String)theMap.get("IGSTAMOUNT");
		       
	        Vector  theVect     =  new Vector();
	                iSlNo       = i+1;
	                theVect     .  add(String.valueOf(iSlNo));
	                theVect     .  add(common.parseNull(sPartyName));
	                theVect     .  add(common.parseNull(sDyes_Name));
	                theVect     .  add(common.parseNull(sConsumption));
	                theVect     .  add(common.parseNull(sRateperKg));
	                theVect     .  add(common.parseNull(sDiscpercentage));
	                theVect     .  add(common.parseNull(sDiscount));
	                theVect     .  add(common.parseNull(sCGSTPercentage));
	                theVect     .  add(common.parseNull(sCGST));
	                theVect     .  add(common.parseNull(sSGSTPercentage));
	                theVect     .  add(common.parseNull(sSGST));
	                theVect     .  add(common.parseNull(sIGSTPercentage));
	                theVect     .  add(common.parseNull(sIGST));
	                theVect     .  add(common.parseNull(sTaxpercentage));
	                theVect     .  add(common.parseNull(sTax));
	                theVect     .  add(common.getRound(common.parseNull(sNetValue),2));
	                theVect     .  add(common.getRound(common.parseNull(sTotalValue),2));
	                theVect     .  add(common.parseNull(sLabStatus));
	                //theVect     .  add(new Boolean(true));
		                 
             theModel    .  appendRow(theVect);
	    }
    }


	private void setLabStatusTableData() {
    	theModel.setNumRows(0);
    	
    	int iSlNo = 0;
    	
    	data.setLabStatusNewDyesDetails();
    	
		for(int i=0;i<data.ADyesDetailList.size();i++){
		    HashMap      theMap      = (HashMap)data.ADyesDetailList.get(i);
        
		    String sDyes_Name        = (String)theMap.get("ITEMNAME");
		    String sConsumption      = (String)theMap.get("ANNUALCONSUMPTION");
		    String sWithEffectFrom   = (String)theMap.get("WITHEFFECTFROM");
		    String sWithEffectTo     = (String)theMap.get("WITHEFFECTTO");
		    String sRateperKg        = (String)theMap.get("RATEPERKG");
		    String sDiscount         = (String)theMap.get("DISCOUNT");
		    String sTax              = (String)theMap.get("TAX");
		    String sNetValue         = (String)theMap.get("NETVALUE");
		    String sTotalValue       = (String)theMap.get("TOTALVALUE");
		    String sDiscpercentage   = (String)theMap.get("DISCPER");
		    String sTaxpercentage    = (String)theMap.get("TAXPER");
		    String sPartyName        = (String)theMap.get("NAME");
		    String sLabStatus        = (String)theMap.get("STATUS");
		    
		    String sCGSTPercentage   = (String)theMap.get("CGSTPER");
		    String sSGSTPercentage   = (String)theMap.get("SGSTPER");
		    String sIGSTPercentage   = (String)theMap.get("IGSTPER");
		    
		    String sCGST             = (String)theMap.get("CGSTAMOUNT");
		    String sSGST             = (String)theMap.get("SGSTAMOUNT");
		    String sIGST             = (String)theMap.get("IGSTAMOUNT");
           
             Vector  theVect     =  new Vector();
                     iSlNo       = i+1;
                     theVect     .  add(String.valueOf(iSlNo));
                     theVect     .  add(common.parseNull(sPartyName));
                     theVect     .  add(common.parseNull(sDyes_Name));
                     theVect     .  add(common.parseNull(sConsumption));
                     theVect     .  add(common.parseNull(sRateperKg));
                     theVect     .  add(common.parseNull(sDiscpercentage));
                     theVect     .  add(common.parseNull(sDiscount));
                     theVect     .  add(common.parseNull(sCGSTPercentage));
                     theVect     .  add(common.parseNull(sCGST));
                     theVect     .  add(common.parseNull(sSGSTPercentage));
                     theVect     .  add(common.parseNull(sSGST));
                     theVect     .  add(common.parseNull(sIGSTPercentage));
                     theVect     .  add(common.parseNull(sIGST));
                     theVect     .  add(common.parseNull(sTaxpercentage));
                     theVect     .  add(common.parseNull(sTax));
                     theVect     .  add(common.getRound(common.parseNull(sNetValue),2));
                     theVect     .  add(common.getRound(common.parseNull(sTotalValue),2));
                     theVect     .  add(common.parseNull(sLabStatus));
                     //theVect     .  add(new Boolean(true));
                     
                     theModel    .  appendRow(theVect);
        }
    }


	private void setDyesCostingProcess(){
    	data.setDyesPerDetails();
    	
		ArrayList       Dyes_PerList        = new ArrayList();
		ArrayList       Dyes_BaseCodeList   = new ArrayList();
		ArrayList       Dyes_PartyList      = new ArrayList();
		ArrayList       Dyes_LabStatusList  = new ArrayList();
		ArrayList       Dyes_RecipeStatusList  = new ArrayList();
		
		int     iSaveValue  = 0;
    
		for(int i=0;i<data.ADyesDetailList.size();i++){
		    HashMap      theMap             = (HashMap)data.ADyesDetailList.get(i);
        
		    String sDyes_Code               = (String)theMap.get("ITEM_CODE");
		    String sNetValue                = (String)theMap.get("NETVALUE");
		    Dyes_PerList                    = getDyesPercentage(sDyes_Code);
		    Dyes_BaseCodeList               = getBaseCode(sDyes_Code);
		    Dyes_PartyList                  = getDyesPartyCode(sDyes_Code);
		    Dyes_LabStatusList              = getDyesLabStatus(sDyes_Code);
		    Dyes_RecipeStatusList           = getDyesRecipeStatus(sDyes_Code);
        
        	for(int j=0;j<Dyes_PartyList.size();j++){
        
				double dDyes_Per            = common.toDouble(String.valueOf(Dyes_PerList.get(j)));
				int iDyes_Party             = common.toInt(String.valueOf(Dyes_PartyList.get(j)));
				String sPartyCode           = String.valueOf(Dyes_PartyList.get(j));
				String sBaseCode            = String.valueOf(Dyes_BaseCodeList.get(j));
				int iCount                  = getShadewiseDyesCount(sPartyCode, sDyes_Code, sBaseCode);
				String sPercentage          = (String.valueOf(Dyes_PerList.get(j))); 
				String sLabStatus           = String.valueOf(Dyes_LabStatusList.get(j));
				String sRecipeStatus        = String.valueOf(Dyes_RecipeStatusList.get(j));
				double dDyes_Cost           = (common.toDouble(sNetValue) * dDyes_Per)/100;
				String sCost                = common.getRound(String.valueOf(dDyes_Cost),3);
				String sPriceRevisionStatus = "-1";
        
            	System.out.println("Count-->"+iCount);
            	
             	if (iCount > 0) {
                 sPriceRevisionStatus   = "1";
                 System.out.println("update");
              		//int iUpdateStatus     = data.updateShadePriceRevisionStatus(iDyes_Party, sDyes_Code, sBaseCode,sPriceRevisionStatus,sRecipeStatus,dDyes_Per);
              	}  
              	
        		sPriceRevisionStatus    = "0";
            	System.out.println("insert");
            	
        			//iSaveValue            = data.getInsertShadewiseDyes(sBaseCode, sDyes_Code, sPercentage, sNetValue, sCost, sAuthSysName, iDyes_Party,sPriceRevisionStatus,sRecipeStatus,sLabStatus);
        			
          	}
        }
        
   		if(iSaveValue==0){
       		System.out.println("DyesBasCode-->"+Dyes_BaseCodeList);
       		
		    for(int j=0;j<Dyes_BaseCodeList.size();j++){
		        String sShade_Code        = (String)Dyes_BaseCodeList.get(j);
		        System.out.println("Shade_Code-->"+sShade_Code);
		        setDyesLabStatus (sShade_Code);
		    }
		    
            JOptionPane.showMessageDialog(null, "Data Saved", "Information", JOptionPane.INFORMATION_MESSAGE);
         	//int iUpdateStatus         = data.updateCostingStatus();
            setLabStatusTableData ();
            btnProcess.setEnabled(false);
        }
    }


	private void setCostingTableData() {
    	theModel.setNumRows(0);
    	
		for(int i=0;i<data.ADyesDetailList.size();i++){
		  HashMap      theMap             = (HashMap)data.ADyesDetailList.get(i);
		    String sDyes_Code             = (String)theMap.get("ITEM_CODE");
		    
		}
	}


	/*
	private double getDyesPercentage(String sDyesCode){
	    double   dPercentage=0.00;
	    for(int i=0; i<data.ADyesPerList.size();i++){
	       HashMap          theMap      = (HashMap) data.ADyesPerList.get(i);
	       String   sDyes_Code          = (String)theMap.get("DYES_CODE");
	       if(sDyesCode.trim().equals(sDyes_Code)){
	           String sPer              = (String)theMap.get("DYES_PER");
	           dPercentage              = common.toDouble(sPer);
	       }
	    }
	    
	    return   dPercentage;
	}
	*/

	private ArrayList getDyesRecipeStatus(String sDyesCode){
		ArrayList       AStatusList = new ArrayList();
		
		for(int i=0; i<data.ADyesPerList.size();i++){
		   HashMap          theMap      = (HashMap) data.ADyesPerList.get(i);
		   String   sDyes_Code          = (String)theMap.get("DYES_CODE");
		   if(sDyesCode.trim().equals(sDyes_Code)){
		       String sPer              = (String)theMap.get("RECIPESTATUS");
		       AStatusList              . add(sPer);
		   }
		}
		
		return   AStatusList;
	}

	private ArrayList getDyesLabStatus(String sDyesCode){
		ArrayList       AStatusList = new ArrayList();
		
		for(int i=0; i<data.ADyesPerList.size();i++){
		   HashMap          theMap      = (HashMap) data.ADyesPerList.get(i);
		   String   sDyes_Code          = (String)theMap.get("DYES_CODE");
		   if(sDyesCode.trim().equals(sDyes_Code)){
		       String sPer              = (String)theMap.get("LABSTATUS");
		       AStatusList              . add(sPer);
		   }
		}
		
		return   AStatusList;
	}


	private ArrayList getDyesPercentage(String sDyesCode){
		ArrayList       APerList = new ArrayList();
		
		for(int i=0; i<data.ADyesPerList.size();i++){
		   HashMap          theMap      = (HashMap) data.ADyesPerList.get(i);
		   String   sDyes_Code          = (String)theMap.get("DYES_CODE");
		   if(sDyesCode.trim().equals(sDyes_Code)){
		       String sPer              = (String)theMap.get("DYES_PER");
		       APerList                 . add(sPer);
		   }
		}
		
		return   APerList;
	}

	private ArrayList getBaseCode(String sDyesCode) {
		ArrayList       ABase_CodeList = new ArrayList();
		
		for(int i=0; i<data.ADyesPerList.size();i++) {
		   HashMap          theMap      = (HashMap) data.ADyesPerList.get(i);
		   
		   String   sDyes_Code          = (String)theMap.get("DYES_CODE");
		   
		   if(sDyesCode.trim().equals(sDyes_Code)) {
		      String sBaseCode         = (String)theMap.get("BASECODE");
		      
		       if(!ABase_CodeList.contains(sBaseCode)) {
		         ABase_CodeList           . add(sBaseCode);
		      }
		   }
		   
		}
		
		return   ABase_CodeList;
	}

	private ArrayList getDyesPartyCode(String sDyesCode){
		ArrayList       APartyCodeList = new ArrayList();
		
		for(int i=0; i<data.ADyesPerList.size();i++){
		   HashMap          theMap      = (HashMap) data.ADyesPerList.get(i);
		   
		   String   sDyes_Code          = (String)theMap.get("DYES_CODE");
		   
		   if(sDyesCode.trim().equals(sDyes_Code)) {
		       String sPartyCode        = (String)theMap.get("PARTY_CODE");
		       
		       APartyCodeList           . add(sPartyCode);
		   }
		   
		}
		
		return   APartyCodeList;
	}
	
	private int  getShadewiseDyesCount(String sDyes_Party, String sDyes_Code, String sBase_Code) {
	   int iCount = 0;
	   
		for(int i=0; i<data.ADyesPerList.size();i++){
		   HashMap          theMap      = (HashMap) data.ADyesPerList.get(i);
		   
		   String   sDyesCode           = (String)theMap.get("DYES_CODE");
		   String   sPartyCode          = (String)theMap.get("PARTY_CODE");
		   String   sBaseCode           = (String)theMap.get("BASECODE");
		   
		   if(sDyesCode.trim().equals(sDyes_Code) && sPartyCode.trim().equals(sDyes_Party) && sBase_Code.trim().equals(sBaseCode)) {
		        iCount                  = iCount+1;
		   }
	   }
	   
	   return iCount;
	}

	private void setDyesLabStatus(String sShadeCode){
		data.setShadewiseNewDyesData();
		
		double      dOldRecipeCost      = 0.00, dNewRecipeCost=0.00, dCostDiff=0.00;
		int         iLabStatus          = 0;
		String      sLabStatus          = "";
		
		for(int i=0;i<data.AShadewiseDyesDetailList.size();i++){
		    HashMap         theMap      = (HashMap)data.AShadewiseDyesDetailList.get(i);
		    
		    String      sShCode         = (String)theMap.get("SHADE_CODE");
		    String      sDyesCode       = (String)theMap.get("DYES_CODE");
		    String      sCost           = (String)theMap.get("COST");
		    String      sRecipeStatus   = (String)theMap.get("RECIPESTATUS");
		    
		    if(sShadeCode.trim().equals(sShCode)) {
				if(sRecipeStatus.trim().equals("1"))
				    dOldRecipeCost          = common.toDouble(sCost);
				else if(sRecipeStatus.trim().equals("0"))
				    dNewRecipeCost          = common.toDouble(sCost);
				  //  dCostDiff               = dNewRecipeCost  - dOldRecipeCost;
				   dCostDiff               =  dOldRecipeCost-dNewRecipeCost ;
				    
				if(dCostDiff< 0){
				      sLabStatus            = "NOTOK";  
				      iLabStatus            = 2;
				}else{
				      sLabStatus            = "OK";  
				      iLabStatus            = 1;
				}
				
		
			    try{
			   		data.updateLabStatus(sShadeCode,iLabStatus);
			    }catch(Exception ex){
			   		ex.printStackTrace();
			   		System.out.println("Update LabStatus-->"+ex);
			    }
		    }
		}
	} 

}
