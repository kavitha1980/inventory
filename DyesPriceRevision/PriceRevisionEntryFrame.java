package DyesPriceRevision;

import java.awt.*;
import javax.swing.*;
import java.util.*;
import javax.swing.border.*;
import java.awt.event.*;
import javax.swing.table.TableRowSorter;
import javax.swing.table.*;
import java.io.IOException;
import java.net.*;
import guiutil.*;
import java.rmi.RemoteException;
import javax.swing.event.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.JEditorPane;

public class PriceRevisionEntryFrame extends JInternalFrame {
    
    JPanel                  pnlMain,pnlEntry,pnlData,pnlControl,pnlInsert,pnlLeft,pnlTop,pnlZero;
    JRadioButton            rbtnInsert, rbtnUpdate;
    JRadioButton            rbtnWithZero, rbtnWithoutZero;
    JComboBox               cmbDyesName, cmbSupplierName,cmbLabStatus;
    JTextField              txtRateperKg,txtDiscount,txtTax,txtNetValue,txtTotalValue,txtConsumption,
                            txtTaxpercentage,txtDiscountpercentage,txtNewDyes,txtPriceRevisionDate; 
    
    JTextField              txtCGSTPercentage,txtCGST,txtSGSTPercentage,txtSGST,txtIGSTPercentage,txtIGST;
    
    JButton                 btnSave,btnExit,btnDyesSave,btnPDFCreation,btnSelectDyes;
    String[]                ColumnName, ColumnType, ColumnName1, ColumnType1, ColumnName2, ColumnType2;
    int[]                   iColumnWidth, iColumnWidth1, iColumnWidth2;
    JCheckBox               chkNewDyes;
    String                  SItemTable="DyeingInvItems";
    
    String[]                sBody;
    JSplitPane              spltPane;
    PriceRevisionModel      theModel;
    PriceRevisionEntryModel theModel1;
    JTable                  theTable, theTable1;
    TableRowSorter<TableModel> sorter;
    AddressField            addField    =  new AddressField();
    Common                  common      =  new Common();
    DyeingData              data        =  new DyeingData();
    PriceRevisiedPrint      print       =  new PriceRevisiedPrint();
  //  JMDApprovalStatus       appstatus   =  new JMDApprovalStatus();
    ItemSearchList          itemsearchlist ;
    String                  sAuthSysName="";
    ArrayList               ADyesNameList;
    JTabbedPane             tabbedPan;
    JEditorPane             pnlEditor;
    JLayeredPane            Layer;
    JTextField              TMatCode;
    
    int  iSNO = 0,iPARTYNAME =1 ,iDYESNAME =2,iANNUALCONSUMPTION=3,iRATEPerKG=4,iDISPer=5,iDISCOUNT=6,iCGSTPer=7,iCGST=8,iSGSTPer=9,iSGST=10,iIGSTPer=11,iIGST=12,
         iTAXPer=13,iTAX=14,iNETVALUE=15,iTOTALVALUE=16,iLABSTATUS=17;
    
    
public PriceRevisionEntryFrame(JLayeredPane Layer){  
    this.Layer	= Layer;	
    setColumn();
    createComponent();
    addLayout();
    addListener();
    addComponent();
    setTableData();
    
     try{
          sAuthSysName=InetAddress.getLocalHost().getHostName();
       }
     catch(Exception e){
          e.printStackTrace();
       }
}    

private void createComponent(){
    
    pnlMain             =       new JPanel();
    pnlData             =       new JPanel();
    pnlEntry            =       new JPanel();
    pnlInsert           =       new JPanel();
    pnlControl          =       new JPanel();
    pnlLeft             =       new JPanel();
    pnlTop              =       new JPanel();
    pnlZero             =       new JPanel();
    
    rbtnInsert          =       new JRadioButton("Insert", true);
    rbtnUpdate          =       new JRadioButton("Update", true);
    rbtnWithZero        =       new JRadioButton("WithZero", true);
    rbtnWithoutZero     =       new JRadioButton("WithoutZero", true);
    TMatCode            =       new JTextField();
    
//    cmbDyesName         =       new JComboBox(data.VDyesName);
    
    cmbSupplierName     =       new JComboBox(data.VDyeSupName);
    cmbLabStatus        =       new JComboBox();
//    cmbLabStatus        =       new JComboBox(data.VStatus);
    
    txtRateperKg        =       new JTextField(10);
    txtDiscount         =       new JTextField(10);
    txtTax              =       new JTextField(10);
    txtDiscountpercentage =     new JTextField(10);
    txtTaxpercentage    =       new JTextField(10);
    txtNetValue         =       new JTextField(10);
    txtTotalValue       =       new JTextField(10);
    txtConsumption      =       new JTextField(10);
    txtNewDyes          =       new JTextField(10);
    txtPriceRevisionDate =      new JTextField(10);
    txtCGSTPercentage   =       new JTextField(10);
    txtCGST             =       new JTextField(10);
    txtSGSTPercentage   =       new JTextField(10);
    txtSGST             =       new JTextField(10);
    txtIGSTPercentage   =       new JTextField(10);
    txtIGST             =       new JTextField(10);
    
    chkNewDyes          =       new JCheckBox(" New Dyes", false);
    
    btnSave             =       new JButton("Save");
    btnDyesSave         =       new JButton("New Dyes Save");
    btnExit             =       new JButton("Exit");
    btnPDFCreation      =       new JButton("Create PDF");
    btnSelectDyes       =       new JButton("Select DyesName");
    
    theModel1           =       new PriceRevisionEntryModel();
    theTable1           =       new JTable();
    
   theTable1             .       setColumnModel(new GroupableTableColumnModel());
   theTable1             .       setTableHeader(new GroupableTableHeader((GroupableTableColumnModel) theTable1.getColumnModel()));
   theTable1             .       setModel(theModel1);
        
   GroupableTableColumnModel cm = (GroupableTableColumnModel) theTable1.getColumnModel();
   ColumnGroup g_CGST   = new ColumnGroup("CGST");
   g_CGST               .       add(cm.getColumn(iCGSTPer));
   g_CGST               .       add(cm.getColumn(iCGST));
   cm                   .       addColumnGroup(g_CGST);
   
   
   //GroupableTableColumnModel cm = (GroupableTableColumnModel) theTable1.getColumnModel();
   ColumnGroup g_SGST   = new ColumnGroup("SGST");
   g_SGST               .       add(cm.getColumn(iSGSTPer));
   g_SGST               .       add(cm.getColumn(iSGST));
   cm                   .       addColumnGroup(g_SGST);
   
   
   //GroupableTableColumnModel cm = (GroupableTableColumnModel) theTable1.getColumnModel();
   ColumnGroup g_IGST   = new ColumnGroup("IGST");
   g_IGST               .       add(cm.getColumn(iIGSTPer));
   g_IGST               .       add(cm.getColumn(iIGST));
   cm                   .       addColumnGroup(g_IGST);
   
   ColumnGroup g_Tax    = new ColumnGroup("TAX");
   g_Tax                .       add(cm.getColumn(iTAXPer));
   g_Tax                .       add(cm.getColumn(iTAX));
   cm                   .       addColumnGroup(g_Tax);
   
    
    spltPane            =       new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    
    tabbedPan           =       new JTabbedPane();
    pnlEditor           =       new JEditorPane();
        
}
//22195
private void addLayout(){
    
        pnlEntry        .       setBorder(new TitledBorder("Price Entry"));
        pnlEntry        .       setLayout(new GridBagLayout());
        pnlData         .       setBorder(new TitledBorder("Data"));
        pnlData         .       setLayout(new BorderLayout());
        
        pnlControl      .       setBorder(new TitledBorder("Selected OrderType"));
        pnlControl      .       setLayout(new FlowLayout(FlowLayout.CENTER));
        pnlTop          .       setLayout(new GridLayout(2, 1));
        pnlLeft         .       setLayout(new BorderLayout());
        
        spltPane        .       setOneTouchExpandable(true);
        spltPane        .       setDividerLocation(350);
        spltPane        .       setDividerSize(0);
        
        pnlMain         .       setLayout(new GridLayout(1, 2));
        
        this            .       setSize(1150, 600);
       // this            .       setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this            .       setTitle("DYES REVISED PRICE ENTRY FRAME");
        this		.	setMaximizable(true);
        this		.	setClosable(true);
        this            .       setResizable(true);
    
}

private void addListener(){
//     cmbDyesName            . addItemListener (new ItemListen());
     cmbSupplierName        . addItemListener (new ItemListen());
     txtTaxpercentage       . addFocusListener(new FocusList());
     txtDiscountpercentage  . addFocusListener(new FocusList());
     //txtTax                 . addFocusListener(new FocusList());
     txtIGSTPercentage      . addFocusListener(new FocusList());
     txtSGSTPercentage      . addFocusListener(new FocusList());
     txtCGSTPercentage      . addFocusListener(new FocusList());
     txtRateperKg           . addFocusListener(new FocusList());
     
     theTable1              . addKeyListener(new KeyList());
     btnSave                . addActionListener(new ActList());
     btnDyesSave            . addActionListener(new ActList());
     btnExit                . addActionListener(new ActList());
     rbtnInsert             . addActionListener(new ActList());
     rbtnUpdate             . addActionListener(new ActList());
     chkNewDyes             . addActionListener(new ActList());
     btnPDFCreation         . addActionListener(new ActList());
     btnSelectDyes          . addActionListener(new ActList());
}

private class ItemListen implements ItemListener {
 public void itemStateChanged(ItemEvent e) {
    if (e.getSource() == cmbDyesName) {
            setRefresh();
            setAunualConsumption();
            setRefresh();
     }
    if (e.getSource() == cmbSupplierName) {
            setRefresh();
            setTableData();
     }
   }
}

private class FocusList extends FocusAdapter {
    public void focusLost(FocusEvent fe) {
                    setValues();
      }
    public void focusGained(FocusEvent fe) {
                    setFocusValues();
      }
 }

private class  ActList implements ActionListener{
  public void actionPerformed (ActionEvent ae){
      if(ae.getSource()== btnSave){
        int iSaveValue= -1;
        String sDyesName            = "";
        String sNewDyesName         = addField.getText();
        
       if(chkNewDyes.isSelected()){
         if(sNewDyesName.length()>0){
           sDyesName               =  sNewDyesName;
         }
        }
       else{
          // sDyesName               = (String)cmbDyesName.getSelectedItem();
            sDyesName               = btnSelectDyes.getText();
       }
      if(chkNewDyes.isSelected()){
       if(isNewDyesExist()){
           //System.out.println("New Dyes Selected");
           
        String sSupplierName       = (String)cmbSupplierName.getSelectedItem();
        //String sDyesName           = (String)cmbDyesName.getSelectedItem();
        int iSupplierCode          = data.getDyeSupplierCode(sSupplierName);
        String sDyesCode           = data.getDyesCode(sDyesName);
        
        if (rbtnInsert.isSelected()) {
        	int iCount                  = data.getDyesCount(iSupplierCode, sDyesCode);
        	
        	if(iCount>0){
           		int iMaxPriceRevisionID = data.getMaxPriceRevisionStatus(iSupplierCode,sDyesCode);
            	int iUpdateStatus       = data.updatePriceRevisionStatus(iSupplierCode,sDyesCode,iMaxPriceRevisionID);
           }
        }
         
         iSaveValue                 = getSave();
        
          if (iSaveValue == 0) {
            JOptionPane.showMessageDialog(null, "Data Saved", "Information", JOptionPane.INFORMATION_MESSAGE);
             setTableData();
             setRefresh();
           } 
            else if (iSaveValue == 2) {
              JOptionPane.showMessageDialog(null, "Data Already Exist", "Information", JOptionPane.INFORMATION_MESSAGE);
           }
          }    
      else{
       JOptionPane.showMessageDialog(null, "Please Save New Dyes Name", "Information", JOptionPane.INFORMATION_MESSAGE);    
       }
     }
      else{
        String sSupplierName       = (String)cmbSupplierName.getSelectedItem();
       //   System.out.println("dyesName==>"+sDyesName);
        //String sDyesName           = (String)cmbDyesName.getSelectedItem();
        
        int iSupplierCode          = data.getDyeSupplierCode(sSupplierName);
        String sDyesCode           = data.getDyesCode(sDyesName);
        
        if (rbtnInsert.isSelected()) {
        int iCount                  = data.getDyesCount(iSupplierCode, sDyesCode);
        if(iCount>0){
            int iMaxPriceRevisionID = data.getMaxPriceRevisionStatus(iSupplierCode,sDyesCode);
            int iUpdateStatus       = data.updatePriceRevisionStatus(iSupplierCode,sDyesCode,iMaxPriceRevisionID);
           }
         }
         iSaveValue                 = getSave();
        
          if (iSaveValue == 0) {
            JOptionPane.showMessageDialog(null, "Data Saved", "Information", JOptionPane.INFORMATION_MESSAGE);
             setTableData();
             setRefresh();
           } 
            else if (iSaveValue == 2) {
              JOptionPane.showMessageDialog(null, "Data Already Exist", "Information", JOptionPane.INFORMATION_MESSAGE);
           }
       }
    }
    if(ae.getSource()==btnExit){
          dispose();
      }
      if(ae.getSource()== btnDyesSave){
          setSaveNewDyes();
          data.setDyes();
       }
       if(ae.getSource()== chkNewDyes){
          if (chkNewDyes.isSelected()) {
            addField.setEditable(true);
          }
          else{
            addField.setEditable(false);
          }
      }
      if(ae.getSource()== btnPDFCreation){
          
      }
       if(ae.getSource()== btnSelectDyes){
           data             .setDyes();
          itemsearchlist    = new ItemSearchList(TMatCode,btnSelectDyes,1,SItemTable);
          setRefresh();
          setAunualConsumption();
          setRefresh();
      }
    }
}

private class KeyList extends KeyAdapter {
   public void keyPressed(KeyEvent ke) {
      if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
          
           int iRow = theTable1.getSelectedRow();
           System.out.println("Row===>"+iRow);
            try {
                    rbtnUpdate.setSelected(true);
                    String sPartyName       = (String) theModel1.getValueAt(iRow, iPARTYNAME);
                    String sDyesName        = (String) theModel1.getValueAt(iRow, iDYESNAME);
                    String sConsumption     = (String) theModel1.getValueAt(iRow, iANNUALCONSUMPTION);
                    String sRateperKg       = (String) theModel1.getValueAt(iRow, iRATEPerKG);
                    String sDiscper         = (String) theModel1.getValueAt(iRow, iDISPer);
                    String sDiscount        = (String) theModel1.getValueAt(iRow, iDISCOUNT);
                    String sCGSTPer         = (String) theModel1.getValueAt(iRow, iCGSTPer);
                    String sCGST            = (String) theModel1.getValueAt(iRow, iCGST);
                    String sSGSTPer         = (String) theModel1.getValueAt(iRow, iSGSTPer);
                    String sSGST            = (String) theModel1.getValueAt(iRow, iSGST);
                    String sIGSTPer         = (String) theModel1.getValueAt(iRow, iIGSTPer);
                    String sIGST            = (String) theModel1.getValueAt(iRow, iIGST);
                    String sNetValue        = (String) theModel1.getValueAt(iRow, iNETVALUE);
                    String sTotalValue      = (String) theModel1.getValueAt(iRow, iTOTALVALUE);
                    
                    cmbSupplierName         . setSelectedItem(sPartyName);
                 //   cmbDyesName             . setSelectedItem(sDyesName);
                    btnSelectDyes           . setText(sDyesName);
                    txtConsumption          . setText(sConsumption);
                    txtRateperKg            . setText(sRateperKg);
                    txtDiscountpercentage   . setText(sDiscper);
                    txtDiscount             . setText(sDiscount);
                    txtCGSTPercentage       . setText(common.getRound(sCGSTPer,2));
                    txtSGSTPercentage       . setText(common.getRound(sSGSTPer,2));
                    txtIGSTPercentage       . setText(common.getRound(sIGSTPer,2));
                    txtCGST                 . setText(common.getRound(sCGST,2));
                    txtSGST                 . setText(common.getRound(sSGST,2));
                    txtIGST                 . setText(common.getRound(sIGST,2));
                    txtNetValue             . setText(common.getRound(sNetValue,2));
                    txtTotalValue           . setText(common.getRound(sTotalValue,2));
    
                 
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Enter-->:" + e);
                }
            }
        }
    }

private void addComponent(){
    Font font            =       new Font("Courier", Font.BOLD, 12);
    GridBagConstraints c =       new GridBagConstraints();
        c.fill           =       GridBagConstraints.HORIZONTAL;

        c.gridx          =       0;
        c.gridy          =       0;
        c.weightx        =       2;
        c.weighty        =       2;
        c.ipady          =       15;

        pnlEntry        .        add(new JLabel(""), c);

        ButtonGroup group = new ButtonGroup();
        group           .       add(rbtnInsert);
        pnlInsert       .       add(rbtnInsert);
        group           .       add(rbtnUpdate);
        pnlInsert       .       add(rbtnUpdate);

        c.gridx         =       1;
        c.gridy         =       0;
        pnlEntry        .       add(pnlInsert, c);
        
        c.gridx         =       0;
        c.gridy         =       1;
        JLabel lblPDate =       new JLabel("Price Revision Date");
        lblPDate        .      setForeground(Color.RED);
        pnlEntry        .      add(lblPDate, c);

        c.gridx         =       1;
        c.gridy         =       1;
        cmbSupplierName .       setFont(font);
        pnlEntry        .       add(txtPriceRevisionDate, c);
        
        
        c.gridx         =       0;
        c.gridy         =       2;
        JLabel lblPName =       new JLabel("Party Name");
        lblPName        .      setForeground(Color.RED);
        pnlEntry        .      add(lblPName, c);

        c.gridx         =       1;
        c.gridy         =       2;
        cmbSupplierName .       setFont(font);
        pnlEntry        .       add(cmbSupplierName, c);

        
        ButtonGroup group1 = new ButtonGroup();
        group1          .       add(rbtnWithZero);
        pnlZero         .       add(rbtnWithZero);
        group1          .       add(rbtnWithoutZero);
        pnlZero         .       add(rbtnWithoutZero);
        
        
        c.gridx         =       1;
        c.gridy         =       3;
        pnlEntry        .       add(pnlZero, c);
        
        
        c.gridx         =       0;
        c.gridy         =       4;
        JLabel lblDyesName =   new JLabel("Dyes Name");
        lblDyesName     .      setForeground(Color.RED);
        pnlEntry        .      add(lblDyesName, c);

        c.gridx         =       1;
        c.gridy         =       4;
      //  cmbDyesName     .       setFont(font);
      //  pnlEntry        .       add(cmbDyesName, c);
         pnlEntry        .       add(btnSelectDyes, c);
        
        
        /*c.gridx         =       0;
        c.gridy         =       3;
        JLabel lblDyesName =   new JLabel("Dyes Name");
        lblDyesName     .      setForeground(Color.RED);
        pnlEntry        .      add(lblDyesName, c);*/

        c.gridx         =       1;
        c.gridy         =       5;
        chkNewDyes      .       setFont(font);
        pnlEntry        .       add(chkNewDyes, c);
        
        
        c.gridx         =       0;
        c.gridy         =       6;
        btnDyesSave     .       setFont(font);
        pnlEntry        .       add(btnDyesSave, c);
        
        c.gridx         =       1;
        c.gridy         =       6;
        addField        .       setEditable(false);
        pnlEntry        .       add(addField, c);
        
        
        c.gridx         =       0;
        c.gridy         =       7;
        JLabel lblQty   =      new JLabel("Yearly Consumption");
        lblQty          .      setForeground(Color.RED);
        pnlEntry        .      add(lblQty, c);

        c.gridx         =       1;
        c.gridy         =       7;
        txtConsumption  .       setFont(font);
        pnlEntry        .       add(txtConsumption, c);
        txtConsumption  .       setText("0");
        
        c.gridx         =       0;
        c.gridy         =       8;
        JLabel lblRate  =       new JLabel("Rate / Kg");
        lblRate         .       setForeground(Color.RED);
        pnlEntry        .       add(lblRate, c);

        c.gridx         =       1;
        c.gridy         =       8;
        pnlEntry        .       add(txtRateperKg, c);
        txtRateperKg    .       setText("0");

        c.gridx         =        0;
        c.gridy         =        9;
        JLabel lblDiscper=       new JLabel("Discount %");
        lblDiscper      .       setForeground(Color.RED);
        pnlEntry        .       add(lblDiscper, c);

        c.gridx         =       1;
        c.gridy         =       9;
        pnlEntry        .       add(txtDiscountpercentage, c);
        txtDiscountpercentage  .       setText("0");
        
        
        /*c.gridx         =       0;
        c.gridy         =       10;
        JLabel lblTaxper=       new JLabel("Tax %");
        lblTaxper       .       setForeground(Color.RED);
        pnlEntry        .       add(lblTaxper, c);
        
        c.gridx         =       1;
        c.gridy         =       10;
        pnlEntry        .       add(txtTaxpercentage, c);
        txtTaxpercentage .       setText("0");
*/
        
        c.gridx         =       0;
        c.gridy         =       10;
        JLabel lblCGSTPer=       new JLabel("CGST %");
        lblCGSTPer      .       setForeground(Color.RED);
        pnlEntry        .       add(lblCGSTPer, c);
        
        c.gridx         =       1;
        c.gridy         =       10;
        pnlEntry        .       add(txtCGSTPercentage, c);
        txtCGSTPercentage .       setText("0");
        
        
        c.gridx         =       0;
        c.gridy         =       11;
        JLabel lblSGSTPer=       new JLabel("SGST %");
        lblSGSTPer      .       setForeground(Color.RED);
        pnlEntry        .       add(lblSGSTPer, c);
        
        c.gridx         =       1;
        c.gridy         =       11;
        pnlEntry        .       add(txtSGSTPercentage, c);
        txtSGSTPercentage .       setText("0");
        
        
        c.gridx         =       0;
        c.gridy         =       12;
        JLabel lblIGSTPer=       new JLabel("IGST %");
        lblIGSTPer      .       setForeground(Color.RED);
        pnlEntry        .       add(lblIGSTPer, c);
        
        c.gridx         =       1;
        c.gridy         =       12;
        
        pnlEntry        .       add(txtIGSTPercentage, c);
        txtIGSTPercentage .       setText("0");
        
      /*  c.gridx         =        0;
        c.gridy         =        13;
        JLabel lblDisc  =       new JLabel("Discount");
        lblDisc         .       setForeground(Color.RED);
        pnlEntry        .       add(lblDisc, c);

        
        c.gridx         =       1;
        c.gridy         =       13;
        txtDiscount     .       setEditable(false);
        pnlEntry        .       add(txtDiscount, c);
        
        
        c.gridx         =       0;
        c.gridy         =       14;
        JLabel lblCGST   =       new JLabel("CGST");
        lblCGST         .       setForeground(Color.RED);
        pnlEntry        .       add(lblCGST, c);
        
        c.gridx         =       1;
        c.gridy         =       14;
        txtCGST         .       setEditable(false);
        pnlEntry        .       add(txtCGST, c);
        
        
        c.gridx         =       0;
        c.gridy         =       15;
        JLabel lblSGST  =       new JLabel("SGST");
        lblSGST         .       setForeground(Color.RED);
        pnlEntry        .       add(lblSGST, c);
        
        c.gridx         =       1;
        c.gridy         =       15;
        txtSGST         .       setEditable(false);
        pnlEntry        .       add(txtSGST, c);

        
        c.gridx         =       0;
        c.gridy         =       16;
        JLabel lblIGST  =       new JLabel("IGST");
        lblIGST         .       setForeground(Color.RED);
        pnlEntry        .       add(lblIGST, c);
        
        c.gridx         =       1;
        c.gridy         =       16;
        txtIGST         .       setEditable(false);
        pnlEntry        .       add(txtIGST, c);
        
        c.gridx         =       0;
        c.gridy         =       17;
        JLabel lblNet   =       new JLabel("Net Value");
        lblNet          .       setForeground(Color.RED);
        pnlEntry        .       add(lblNet, c);
        
        c.gridx         =       1;
        c.gridy         =       17;
        txtNetValue     .       setEditable(false);
        pnlEntry        .       add(txtNetValue, c);


        c.gridx         =       0;
        c.gridy         =       18;
        JLabel lblTot   =       new JLabel("Total Value");
        lblTot          .       setForeground(Color.RED);
        pnlEntry        .       add(lblTot, c);
        
        c.gridx         =       1;
        c.gridy         =       18;
        txtTotalValue   .       setEditable(false);
        pnlEntry        .       add(txtTotalValue, c);
*/
        
     /*    c.gridy         =       13;
        JLabel lblLabSt =       new JLabel("Lab Status");
        lblLabSt        .       setForeground(Color.RED);
        pnlEntry        .       add(lblLabSt, c);
        
        c.gridx         =       1;
        c.gridy         =       13;
        pnlEntry        .       add(cmbLabStatus, c);

       */ 

        c.gridx         =       0;
        c.gridy         =       13;
        pnlEntry        .       add(btnSave, c);

        c.gridx         =       1;
        c.gridy         =       13;
        pnlEntry        .       add(btnExit, c);
        
     /*   c.gridx         =       2;
        c.gridy         =       14;
        pnlEntry        .       add(btnPDFCreation, c);*/
        
      //  pnlData         .       add(new JScrollPane(theTable), BorderLayout.CENTER);
          pnlData         .       add(new JScrollPane(theTable1), BorderLayout.CENTER);
        
//        pnlTop            .       add(pnlEntry, BorderLayout.NORTH);
        pnlLeft           .       add(pnlEntry, BorderLayout.NORTH);
        pnlLeft           .       add(new JScrollPane(pnlEntry));
        
        tabbedPan         .       addTab("DYES ENTRY", pnlData);
        tabbedPan         .       addTab("PRINT PREVIEW", print.pnlMain);
       // tabbedPan         .       addTab("JMD APPROVAL STATUS",appstatus.pnlMain);

        spltPane.add(pnlLeft);
        spltPane.add(new JScrollPane(tabbedPan));
        
        /*spltPane        .       add(pnlLeft);
        spltPane        .       add(pnlData);*/
        this            .       add(spltPane);
 }

private void setColumn() {
        int iColumnCount1       =   17;
        ColumnName              =   new String[iColumnCount1];
        ColumnType              =   new String[iColumnCount1];
        iColumnWidth            =   new int[iColumnCount1];
        int iCount1             =   0;

        ColumnName[iCount1]     =   "SLNO";
        ColumnType[iCount1]     =   "S";
        iColumnWidth[iCount1]   =   50;
        iCount1                 = iCount1 + 1;

        ColumnName[iCount1]     =   "DYES NAME";
        ColumnType[iCount1]     =   "S";
        iColumnWidth[iCount1]   =    50;
        iCount1                 =   iCount1 + 1;

        ColumnName[iCount1]     = "ONE YEAR CONSUMPTION QTY";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 50;
        iCount1                 = iCount1 + 1;

        ColumnName[iCount1]     = "RATE / KG";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 100;
        iCount1                 = iCount1 + 1;

        ColumnName[iCount1]     = "DISCOUNT 6%";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 30;
        iCount1                 = iCount1 + 1;

        ColumnName[iCount1]     = "TAX 5%";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 30;
        iCount1                 = iCount1 + 1;

        ColumnName[iCount1]     = "NET VALUE";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 30;
        iCount1                 = iCount1 + 1;
        
        ColumnName[iCount1]     = "TOTAL VALUE";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 30;
        iCount1                 = iCount1 + 1;

        ColumnName[iCount1]     = "RATE / KG";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 100;
        iCount1                 = iCount1 + 1;

        ColumnName[iCount1]     = "DISCOUNT 6%";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 30;
        iCount1                 = iCount1 + 1;

        ColumnName[iCount1]     = "TAX 5%";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 30;
        iCount1                 = iCount1 + 1;

        ColumnName[iCount1]     = "NET VALUE";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 30;
        iCount1                 = iCount1 + 1;
        
        ColumnName[iCount1]     = "TOTAL VALUE";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 30;
        iCount1                 = iCount1 + 1;
        
        
        ColumnName[iCount1]     = "RATE / KG";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 100;
        iCount1                 = iCount1 + 1;

        ColumnName[iCount1]     = "TOTAL VALUE";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 30;
        iCount1                 = iCount1 + 1;

        ColumnName[iCount1]     = "%";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 30;
        iCount1                 = iCount1 + 1;
        
        ColumnName[iCount1]     = "";
        ColumnType[iCount1]     = "S";
        iColumnWidth[iCount1]   = 30;
        iCount1                 = iCount1 + 1;


        theModel = new PriceRevisionModel(ColumnName, ColumnType, iColumnWidth);
        theTable = new JTable(theModel) {

        protected javax.swing.table.JTableHeader createDefaultTableHeader() {
                return new guiutil.GroupableTableHeader(columnModel);
            }
        };
        sorter = new TableRowSorter<TableModel>(theModel);
        theTable.setRowSorter(sorter);
        // Column Group:

        int iCount2 = 2;
        iCount2 = iCount2 + 1;
        
        javax.swing.table.TableColumnModel CM = theTable.getColumnModel();
        int N = theModel.ColumnName.length;


        guiutil.ColumnGroup g_OldRate = new guiutil.ColumnGroup("OLD RATE ");
        g_OldRate.add(CM.getColumn(iCount2));
        g_OldRate.add(CM.getColumn(iCount2));
        g_OldRate.add(CM.getColumn(iCount2));
        g_OldRate.add(CM.getColumn(iCount2));
        g_OldRate.add(CM.getColumn(iCount2));

        guiutil.GroupableTableHeader header = (guiutil.GroupableTableHeader) theTable.getTableHeader();
        header.addColumnGroup(g_OldRate);

        int iCount3 = 7;
        iCount3 = iCount3 + 1;

        guiutil.ColumnGroup g_NewRate = new guiutil.ColumnGroup("NEW RATE");
        g_NewRate.add(CM.getColumn(iCount3));
        g_NewRate.add(CM.getColumn(iCount3));
        g_NewRate.add(CM.getColumn(iCount3));
        g_NewRate.add(CM.getColumn(iCount3));
        g_NewRate.add(CM.getColumn(iCount3));
        
        guiutil.GroupableTableHeader header1 = (guiutil.GroupableTableHeader) theTable.getTableHeader();
        header1.addColumnGroup(g_NewRate);
        
        int iCount4 = 12;
        iCount4 = iCount4 + 1;

        guiutil.ColumnGroup g_DiffRate = new guiutil.ColumnGroup("DIFFERENCE RATE");
        g_DiffRate.add(CM.getColumn(iCount4));
        g_DiffRate.add(CM.getColumn(iCount4));
        g_DiffRate.add(CM.getColumn(iCount4));
        
        
        guiutil.GroupableTableHeader header2 = (guiutil.GroupableTableHeader) theTable.getTableHeader();
        header2.addColumnGroup(g_DiffRate);

        
        int iCount5 = 16;
        //iCount5 = iCount5 + 1;

        guiutil.ColumnGroup g_Status = new guiutil.ColumnGroup("LAB STATUS");
        g_Status.add(CM.getColumn(iCount5));
        
        
        guiutil.GroupableTableHeader header3 = (guiutil.GroupableTableHeader) theTable.getTableHeader();
        header3.addColumnGroup(g_Status);

    }

private void setAunualConsumption(){
    System.out.println("Set Annual Consumption");
    String sAnnualConsumption="";
    
    //String sDyesName            = (String)cmbDyesName.getSelectedItem();
    String sDyesName            = btnSelectDyes.getText();
    String sDyesCode            = data.getDyesCode(sDyesName.trim());
    String sZeroStatus          = "";
    if (rbtnWithZero.isSelected()) {
       sAnnualConsumption        = data.getAnnualConsumption(sDyesCode,0);
    }
    else{
        sAnnualConsumption        = data.getAnnualConsumption(sDyesCode,1);
    }
    
    txtConsumption              . setText(common.parseNull(sAnnualConsumption));
    txtDiscount                 . setText("");
    txtTax                      . setText("");
    txtDiscountpercentage       . setText("");
    txtTaxpercentage            . setText("");
    txtNetValue                 . setText("");
    txtTotalValue               . setText("");
    
}

private void setValues(){
    System.out.println("SetValues");
    double  dCGST   = 0.00, dSGST=0.00, dIGST=0.00;
    String sRate                = txtRateperKg.getText();
    double dDiscountpercentage  = common.toDouble(txtDiscountpercentage.getText());
    double dCGSTPercentage      = common.toDouble(txtCGSTPercentage.getText());
    double dSGSTPercentage      = common.toDouble(txtSGSTPercentage.getText());
    double dIGSTPercentage      = common.toDouble(txtIGSTPercentage.getText());
    double dDiscount            = common.toDouble(sRate)*dDiscountpercentage/100;
    if(dCGSTPercentage>0){
        dCGST                   = (common.toDouble(sRate)-dDiscount)*dCGSTPercentage/100;
        System.out.println("CGSTValue==>"+dCGST);
    }
    else{
        dCGST                   =  0; 
    }
    if(dSGSTPercentage>0) {
        dSGST                   = (common.toDouble(sRate)-dDiscount)*dSGSTPercentage/100;
    }
    else{
       dSGST                    =  0; 
    }
    if(dIGSTPercentage>0){
        dIGST                   = (common.toDouble(sRate)-dDiscount)*dIGSTPercentage/100;
    }
    else {
        dIGST                   =  0; 
    }
    System.out.println("Values===>"+sRate+"---"+dDiscount+"--"+dCGST+"--"+dSGST+"--"+dIGST);
    double dNetValue            = (common.toDouble(sRate)-dDiscount)+dCGST+dSGST+dIGST;
    
    
    System.out.println("Net Value in SetValues--->"+dNetValue);
    // double dNetValue            = (common.toDouble(sRate)+dTax)-dDiscount;
    double dAnnualConsumption   = common.toDouble(txtConsumption.getText());
    double dTotalValue          = dAnnualConsumption * dNetValue;
    
    txtDiscount                 . setText(String.valueOf(dDiscount));
    //txtTax                      . setText(String.valueOf(dTax));
    txtCGST                     . setText(common.getRound(String.valueOf(dCGST),2));
    txtSGST                     . setText(common.getRound(String.valueOf(dSGST),2));
    txtIGST                     . setText(common.getRound(String.valueOf(dIGST),2));
    
    txtNetValue                 . setText(common.getRound(String.valueOf(dNetValue),2));
    txtTotalValue               . setText(common.getRound(String.valueOf(dTotalValue),2));
}
private void setFocusValues(){
    System.out.println("setFocusValues");
    double  dCGST   = 0.00, dSGST= 0.00, dIGST = 0.00;
    String sRate                = txtRateperKg.getText();
    double dDiscountpercentage  = common.toDouble(txtDiscountpercentage.getText());
    double dCGSTPercentage      = common.toDouble(txtCGSTPercentage.getText());
    double dSGSTPercentage      = common.toDouble(txtSGSTPercentage.getText());
    double dIGSTPercentage      = common.toDouble(txtIGSTPercentage.getText());
    double dDiscount            = common.toDouble(sRate)*dDiscountpercentage/100;
    
    if(dCGSTPercentage>0)
        dCGST                   = (common.toDouble(sRate)-dDiscount)*dCGSTPercentage/100;
    else
        dCGST                   =  0; 
    if(dSGSTPercentage>0) 
        dSGST                   = (common.toDouble(sRate)-dDiscount)*dSGSTPercentage/100;
    else
       dSGST                    =  0; 
    if(dIGSTPercentage>0)
        dIGST                   = (common.toDouble(sRate)-dDiscount)*dIGSTPercentage/100;
    else
        dIGST                   =  0; 
    
    double dNetValue            = (common.toDouble(sRate)-dDiscount)+dCGST+dSGST+dIGST;
    System.out.println("NetValue in Focus--->"+dNetValue);
    //double dNetValue            = (common.toDouble(sRate)+dTax)-dDiscount;
    double dAnnualConsumption   = common.toDouble(txtConsumption.getText());
    double dTotalValue          = dAnnualConsumption * dNetValue;
    txtDiscount                 . selectAll();
    txtDiscount                 . setText(String.valueOf(dDiscount));
    txtNetValue                 . setText(common.getRound(String.valueOf(dNetValue),2));
    txtTotalValue               . setText(common.getRound(String.valueOf(dTotalValue),2));
}

private void setSaveNewDyes(){
    int iSaveValue=-1;
    String sNewDyesName  = "";
    sNewDyesName         = addField.getText().replaceAll("\\s+", " ");    
    //String sTrimText      = addField.getText().replaceAll("\\s+", " ");
    //System.out.println("TrimText===>"+sTrimText);

    String sItemSeq      = data.getDyesSequenceValue();
    String sInv_ItemSeq  = "CD01"+sItemSeq;
    iSaveValue           = data.getInsertDyesInvItems(sNewDyesName,sInv_ItemSeq);
    System.out.println("SaveValue==>"+iSaveValue);
    if (iSaveValue == 0) {
      JOptionPane.showMessageDialog(null, "New Dyes Name Saved", "Information", JOptionPane.INFORMATION_MESSAGE);
    } 
    else if (iSaveValue == 1) {
      JOptionPane.showMessageDialog(null, "Dyes Name Already Saved", "Information", JOptionPane.INFORMATION_MESSAGE);
    } 
    else if (iSaveValue == 2) {
     JOptionPane.showMessageDialog(null, "New Dyes Name Not Saved", "Information", JOptionPane.INFORMATION_MESSAGE);
    }
}

private int getSave(){
    int iSaveValue=-1;
    String sDyesName            = "";
    String sNewDyesName         = addField.getText();
    if(chkNewDyes.isSelected()){
     if(sNewDyesName.length()>0){
        sDyesName               =  sNewDyesName;
      }
    }
    else{
      //  sDyesName               = (String)cmbDyesName.getSelectedItem();
          sDyesName               = btnSelectDyes.getText();
    }
    String sPriceRevisionDate   = common.parseNull(txtPriceRevisionDate.getText());
    String sSupplierName        = (String)cmbSupplierName.getSelectedItem();
    int iSupplierCode           = data.getDyeSupplierCode(sSupplierName);
    String sDyesCode            = data.getDyesCode(sDyesName);
    String sAnnualConsumption   = common.parseNull(txtConsumption.getText());
    String sRateperKg           = common.parseNull(txtRateperKg.getText());
    String sDiscount            = common.parseNull(txtDiscount.getText());
    String sTax                 = common.parseNull(txtTax.getText());
    String sDiscountper         = common.parseNull(txtDiscountpercentage.getText());
    String sTaxper              = common.parseNull(txtTaxpercentage.getText());
    String sNetValue            = common.parseNull(txtNetValue.getText());
    String sTotalValue          = common.parseNull(txtTotalValue.getText());
    
    String sCGSTPer             = common.parseNull(txtCGSTPercentage.getText());
    String sSGSTPer             = common.parseNull(txtSGSTPercentage.getText());
    String sIGSTPer             = common.parseNull(txtIGSTPercentage.getText());

    String sCGST                = common.parseNull(txtCGST.getText());
    String sSGST                = common.parseNull(txtSGST.getText());
    String sIGST                = common.parseNull(txtIGST.getText());

    /*String sLabStatus           = (String)cmbLabStatus.getSelectedItem();
    String sLabCode             = data.getStatusCode(sLabStatus);*/
    
    
   if (rbtnInsert.isSelected()) {
      //if(isValidate())  {
        //iSaveValue              = data.getInsertData(sDyesCode,sAnnualConsumption,sRateperKg,sDiscount,sTax,sDiscountper,sTaxper,sNetValue,sTotalValue,iSupplierCode,sAuthSysName,sLabCode);
          iSaveValue              = data.getInsertData(sDyesCode,sAnnualConsumption,sRateperKg,sDiscount,sTax,sDiscountper,sTaxper,sNetValue,sTotalValue,iSupplierCode,sAuthSysName,common.pureDate(sPriceRevisionDate),sCGSTPer,sCGST,sSGSTPer,sSGST,sIGSTPer,sIGST);
     /*}
      else{
          JOptionPane.showMessageDialog(null, "Data Not Saved Please fill Rate,Discount,Tax", "Information", JOptionPane.INFORMATION_MESSAGE);
      }*/
    }
    else{
  //   iSaveValue                 = data.updateDyesData (iSupplierCode, sDyesCode,sRateperKg,sDiscountper,sTaxper,sDiscount,sTax,sNetValue,sTotalValue,sLabCode);  
          iSaveValue                 = data.updateDyesData (iSupplierCode, sDyesCode,sRateperKg,sDiscountper,sTaxper,sDiscount,sTax,sNetValue,sTotalValue,sCGSTPer,sCGST,sSGSTPer,sSGST,sIGSTPer,sIGST);  
      }
    return iSaveValue;
}

private boolean isNewDyesExist(){
    
    boolean bool = false;
    String sDyesName            = "";
    String sNewDyesName         = addField.getText();
    if(chkNewDyes.isSelected()){
        int iCount              = data.getNewDyesCount(sNewDyesName);
        if(iCount>0){
         bool                   = true;
        }
        else{
            bool                = false;
        }
    }
    //System.out.println("Bool=>"+bool);
    return bool;
}


private boolean isValidate(){
    boolean bool = false;
    String sRateperKg           = common.parseNull(txtRateperKg.getText());
    String sDiscount            = common.parseNull(txtDiscount.getText());
    String sTax                 = common.parseNull(txtTax.getText());
    
   //System.out.println("Data=>"+sRateperKg.length()+"--"+sDiscount.length()+"--"+sTax.length());
    
    if(!sRateperKg.equals("0") || !sDiscount.equals("0") || !sTax.equals("0")){
         bool                  = false;
    }
    else{
        bool                    = true;
    }
    //System.out.println("Bool=>"+bool);
    return bool;
}


private void setTableData(){
 
    ADyesNameList = new ArrayList();
    theModel1.setNumRows(0);
    int iSlNo=0;
    String sSupplierName         = (String)cmbSupplierName.getSelectedItem();
    int iSupCode                 = data.getDyeSupplierCode(sSupplierName);
    data.setDyesDetails(sSupplierName);
    for(int i=0;i<data.ADyesDetailList.size();i++){
        HashMap      theMap      = (HashMap)data.ADyesDetailList.get(i);
        String sDyes_Name        = (String)theMap.get("ITEMNAME");
        String sConsumption      = (String)theMap.get("ANNUALCONSUMPTION");
        String sWithEffectFrom   = (String)theMap.get("WITHEFFECTFROM");
        String sWithEffectTo     = (String)theMap.get("WITHEFFECTTO");
        String sRateperKg        = (String)theMap.get("RATEPERKG");
        String sDiscount         = (String)theMap.get("DISCOUNT");
        String sTax              = (String)theMap.get("TAX");
        String sNetValue         = (String)theMap.get("NETVALUE");
        String sTotalValue       = (String)theMap.get("TOTALVALUE");
        String sDiscpercentage   = (String)theMap.get("DISCPER");
        String sTaxpercentage    = (String)theMap.get("TAXPER");
        String sPartyName        = (String)theMap.get("NAME");
        String sLabStatus        = (String)theMap.get("STATUS");
        
        String sCGSTPercentage   = (String)theMap.get("CGSTPER");
        String sSGSTPercentage   = (String)theMap.get("SGSTPER");
        String sIGSTPercentage   = (String)theMap.get("IGSTPER");
        
        String sCGST             = (String)theMap.get("CGSTAMOUNT");
        String sSGST             = (String)theMap.get("SGSTAMOUNT");
        String sIGST             = (String)theMap.get("IGSTAMOUNT");
           
             Vector  theVect     =  new Vector();
                     iSlNo       = i+1;
                     theVect     .  add(String.valueOf(iSlNo));
                     theVect     .  add(common.parseNull(sPartyName));
                     theVect     .  add(common.parseNull(sDyes_Name));
                     theVect     .  add(common.parseNull(sConsumption));
                     theVect     .  add(common.parseNull(sRateperKg));
                     theVect     .  add(common.parseNull(sDiscpercentage));
                     theVect     .  add(common.parseNull(sDiscount));
                     theVect     .  add(common.parseNull(sCGSTPercentage));
                     theVect     .  add(common.parseNull(sCGST));
                     theVect     .  add(common.parseNull(sSGSTPercentage));
                     theVect     .  add(common.parseNull(sSGST));
                     theVect     .  add(common.parseNull(sIGSTPercentage));
                     theVect     .  add(common.parseNull(sIGST));
                     theVect     .  add(common.parseNull(sTaxpercentage));
                     theVect     .  add(common.parseNull(sTax));
                     theVect     .  add(common.getRound(common.parseNull(sNetValue),2));
                     theVect     .  add(common.getRound(common.parseNull(sTotalValue),2));
                     theVect     .  add(common.parseNull(sLabStatus));
                     
                     theModel1   .  appendRow(theVect);
       }
}

private void setRefresh(){
    
        //txtConsumption          . setText("");
        txtDiscount             . setText("0");
        txtTax                  . setText("0");
        txtCGST                 . setText("0");
        txtSGST                 . setText("0");
        txtIGST                 . setText("0");
        txtDiscountpercentage   . setText("0");
        txtTaxpercentage        . setText("0");
        txtCGSTPercentage       . setText("0");
        txtSGSTPercentage       . setText("0");
        txtIGSTPercentage       . setText("0");
        txtNetValue             . setText("0");
        txtTotalValue           . setText("0");
        txtRateperKg            . setText("0");
        chkNewDyes              . setSelected(false);
        addField                . setEditable(false);
        addField                . setText("");
        
}


/*public static void main (String args[])    {
 PriceRevisionEntryFrame         frame       = new PriceRevisionEntryFrame();
                                 frame       . setVisible(true);
 }
*/

}
