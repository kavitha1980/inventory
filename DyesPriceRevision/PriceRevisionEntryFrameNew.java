package DyesPriceRevision;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumnModel; 
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.net.InetAddress;  

import java.awt.Insets;


import guiutil.*;


public class PriceRevisionEntryFrameNew extends JInternalFrame {
    
    private int           iUserCode;
    
    private JLayeredPane  Layer;
    
    private JPanel        leftpanel;
    private JPanel        ltoppanel, lmidpanel, lbotentrypanel;
    
    private JPanel        rentrypanel, rentrytoppanel, rentrybotpanel;
    
    private JTabbedPane   righttabPane, lbottabPane;
    private JSplitPane    splitPane;
    private JScrollPane   entryscrollpanetop;
    
    public  JComboBox     JSupplierName, JDyesName;
    
    public  JRadioButton  rbtnWithZero, rbtnWithoutZero;
    
    private JButton       bsave, bupdate, bexit, bprint, bSaveNewDye;
    
    private DateField     pricerevdate;
    private JTextField	  TNewDyes   = null;
    private JCheckBox     selectchk;
    
    PriceRevisionEntryModelNew      thePriceModel;
    JTable                          thePriceTable;
    
    DyesNamesModel					theModel;
    JTable                          theTable;
    
    PriceRevisionReportPanelNew     pricerevisionreportPanel;
    ItemSearchListPanel				itemSearchListPanel;
    
	private java.util.Vector        VSupplierCode, VSupplierName, VDyesCode, VDyesName, VDyesSupCode, VDyesPriceEntryDate;
    
    java.util.List ADyesDetailList = new java.util.ArrayList();
    
    java.sql.Connection   theInvConnection = null;
    java.sql.Connection   theDyeConnection = null;

    TableModelListener      		   tableModelListener = null;
    
    private static final Insets insets = new Insets(0, 0, 0, 0);
    private static final Insets insts  = new Insets(2, 2, 2, 2);
    
    private JLabel[][]   			  LDyesprice = null;
    private FractionNumberField[][]   FDyesprice = null;
    private JLabel[] 			      LDyesName  = null;
    private JTextField[] 			  TStatus    = null;
    private JLabel[] 			      LConsump   = null;
    
    Common common = new Common();
    
	public PriceRevisionEntryFrameNew(JLayeredPane Layer, int iUserCode) {
		
		try{
			this.Layer     = Layer;
			this.iUserCode = iUserCode;
		
		
			System.out.println("1");
			setData();
			System.out.println("2");
			createComponents();
			System.out.println("3");
			setLayouts();
			System.out.println("4");
			addComponents();
			System.out.println("5");
			addListeneres();
			System.out.println("6");
			setTableData();
			System.out.println("7");
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}    

	private void createComponents(){
		
		righttabPane = new JTabbedPane();
		lbottabPane  = new JTabbedPane();
		
		splitPane   = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		splitPane   . setContinuousLayout(true);
    	splitPane   . setOneTouchExpandable(true);
		splitPane   . setDividerLocation(350);
    	
		leftpanel   = new JPanel();
		ltoppanel   = new JPanel();
		lmidpanel   = new JPanel();
		lbotentrypanel = new JPanel();
		
		rentrypanel    = new JPanel();
		rentrytoppanel = new JPanel();
		rentrybotpanel = new JPanel();
		
		entryscrollpanetop = new JScrollPane();
		
		pricerevdate  = new DateField();
		pricerevdate  . setTodayDate();
		
		JSupplierName = new JComboBox(VSupplierName);
		JDyesName     = new JComboBox(VDyesName);
		
		TNewDyes      = new JTextField("");
		
		selectchk   = new JCheckBox("Select All Dyes",false);
		
        rbtnWithZero        = new JRadioButton("Entire consumption", false);
        rbtnWithoutZero     = new JRadioButton("Last one year consumption", true);

        	//with zero    - entire consumption
        	//without zero - last one year consumption

        //rbtnWithZero        = new JRadioButton("WithZero", true);
        //rbtnWithoutZero     = new JRadioButton("WithoutZero", false);
		
        ButtonGroup group1  = new ButtonGroup();
        group1              . add(rbtnWithZero);
        group1              . add(rbtnWithoutZero);
		
		bsave       = new JButton("Revision");
		bupdate     = new JButton("Update");
		//bsave  	    . setEnabled(false);
		
		bSaveNewDye = new JButton("Save");
		
		bexit  = new JButton("Exit");
		bprint = new JButton("Print");
		
    	thePriceModel = new PriceRevisionEntryModelNew();      ;
    	thePriceTable = new JTable();
		
   		thePriceTable . setColumnModel(new GroupableTableColumnModel());
   		thePriceTable . setTableHeader(new GroupableTableHeader((GroupableTableColumnModel) thePriceTable.getColumnModel()));
   		thePriceTable . setModel(thePriceModel);
        
        thePriceTable . setDefaultRenderer(Object.class, new MyRenderer());
        
        TableColumnModel TCM  = thePriceTable.getColumnModel();

        for(int j=0;j<thePriceModel.ColumnName.length;j++) {
            TCM .getColumn(j).setPreferredWidth(thePriceModel.iColumnWidth[j]);
        }
		
        thePriceTable . getTableHeader().setReorderingAllowed(false);
        
        thePriceTable . setRowHeight(25);
        
	    GroupableTableColumnModel cm = (GroupableTableColumnModel) thePriceTable.getColumnModel();
	    ColumnGroup groupRate  = new ColumnGroup("RATE/KG");
	    groupRate              . add(cm.getColumn(3));
	    groupRate              . add(cm.getColumn(4));
	    cm                     . addColumnGroup(groupRate);
	    
	    ColumnGroup groupDisc  = new ColumnGroup("DISCOUNT");
	    groupDisc              . add(cm.getColumn(5));
	    groupDisc              . add(cm.getColumn(6));
	    cm                     . addColumnGroup(groupDisc);
	    
	    ColumnGroup groupCGST  = new ColumnGroup("CGST");
	    groupCGST              . add(cm.getColumn(7));
	    groupCGST              . add(cm.getColumn(8));
	    cm                     . addColumnGroup(groupCGST);

	    ColumnGroup groupSGST  = new ColumnGroup("SGST");
	    groupSGST              . add(cm.getColumn(9));
	    groupSGST              . add(cm.getColumn(10));
	    cm                     . addColumnGroup(groupSGST);

	    ColumnGroup groupIGST  = new ColumnGroup("IGST");
	    groupIGST              . add(cm.getColumn(11));
	    groupIGST              . add(cm.getColumn(12));
	    cm                     . addColumnGroup(groupIGST);

	    ColumnGroup groupLabsts= new ColumnGroup("LAB STATUS");
	    groupLabsts            . add(cm.getColumn(13));
	    groupLabsts            . add(cm.getColumn(14));
	    cm                     . addColumnGroup(groupLabsts);
		
		//
    	theModel =  new DyesNamesModel();
        theTable = new JTable(theModel);
        
        TableColumnModel TC  = theTable.getColumnModel();

        for(int j=0;j<theModel.ColumnName.length;j++) {
            TC .getColumn(j).setPreferredWidth(theModel.iColumnWidth[j]);
        }
		
        theTable . getTableHeader().setReorderingAllowed(false);
		
		pricerevisionreportPanel = new PriceRevisionReportPanelNew(this);
		
		itemSearchListPanel      = new ItemSearchListPanel(this,1,"DyeingInvItems");
		
	}
	
	private void setLayouts(){
		
        setTitle("Price Revision Entry Frame");
        setClosable(true);
        setMaximizable(true);
        setIconifiable(true);
        setResizable(true);
        setBounds(10,10,1200,600);
		
      	SCLayout  leftlayout         =  new SCLayout(3,SCLayout.FILL,SCLayout.FILL,5);
                  leftlayout         .  setScale(0,0.20);
                  leftlayout         .  setScale(1,0.40);
                  leftlayout         .  setScale(2,0.40);

        leftpanel.setLayout(leftlayout);
		
        ltoppanel     . setLayout(new GridBagLayout());
        lmidpanel     . setLayout(new BorderLayout());
        lbotentrypanel . setLayout(new GridBagLayout());
		
			
      	SCLayout  rentrypanellayout  =  new SCLayout(2,SCLayout.FILL,SCLayout.FILL,2);
                  rentrypanellayout  .  setScale(0,0.90);
                  rentrypanellayout  .  setScale(1,0.10);

        rentrypanel.setLayout(rentrypanellayout);
        
        rentrytoppanel .setLayout(new BorderLayout());
        rentrybotpanel .setLayout(new GridBagLayout());
		
		ltoppanel.setBorder(new TitledBorder(""));
		lmidpanel.setBorder(new TitledBorder("Dyes Names"));
		
		rentrytoppanel.setBorder(new TitledBorder("Price Entry"));
		rentrybotpanel.setBorder(new TitledBorder("Controls"));
		
	}
	
	private void addComponents() {
		
//Left Panel		
		//addComponent(ltoppanel, new JLabel("Date"), 0, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		addComponent(ltoppanel, pricerevdate, 1, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		
		//addComponent(ltoppanel, new JLabel("Supplier"), 0, 2, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		addComponent(ltoppanel, JSupplierName, 1, 2, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		
		//addComponent(ltoppanel, new JLabel("Consumption"), 0, 3, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		addComponent(ltoppanel, rbtnWithZero, 1, 3, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		addComponent(ltoppanel, rbtnWithoutZero, 1, 4, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		
		//addComponent(ltoppanel, new JLabel("Select"), 0, 5, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		addComponent(ltoppanel, selectchk, 1, 5, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
          
          
		lmidpanel.add(new JScrollPane(theTable));
		
		addComponent(lbotentrypanel, new JLabel("Dyes Name"), 0, 2, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		addComponent(lbotentrypanel, TNewDyes, 1, 2, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		
		addComponent(lbotentrypanel, bSaveNewDye, 1, 3, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		
		
		lbottabPane.add(itemSearchListPanel,"Dye Selection");
		lbottabPane.add(lbotentrypanel,"New Dye Entry");
		
		leftpanel.add(ltoppanel);
		leftpanel.add(lmidpanel);
		leftpanel.add(lbottabPane);
		
//Right Entry Panel
		rentrytoppanel.add(new JScrollPane(thePriceTable));
		
		addComponent(rentrybotpanel, bsave, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH);
		addComponent(rentrybotpanel, bupdate, 3, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH);
		addComponent(rentrybotpanel, bexit, 5, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH);
		
		rentrypanel . add(rentrytoppanel);
		rentrypanel . add(rentrybotpanel);
		
		righttabPane.add(rentrypanel,"Price Revision Entry");
		righttabPane.add(pricerevisionreportPanel,"Print View and Comparison");
		
		splitPane.setLeftComponent(leftpanel);
    	splitPane.setRightComponent(righttabPane);
		
        getContentPane().add("Center",splitPane);
        
	}
	

	private void setPriceEntryTableData() {
		
		thePriceModel.setNumRows(0);

		int iSino = 0;
		
		int iNoofRows = theModel.getRowCount();
		
		
		java.util.Vector V = null;
		
		int isize  = ADyesDetailList.size();
		
        for(int i=0;i<iNoofRows;i++) {
        	
        	if(((Boolean)theModel.getValueAt(i, 2))) {
        		
        		String SItemcode = getItemCode(((String)theModel.getValueAt(i, 1)));
        		
        		for(int n=0;n<isize;n++){
        			java.util.HashMap theMap = (java.util.HashMap)ADyesDetailList.get(n);
        			
        			if(SItemcode.equals(((String)theMap.get("ITEM_CODE")))) {
						
						V = new java.util.Vector();
						
						V.add(String.valueOf(++iSino));
						V.add(common.parseNull((String)theModel.getValueAt(i, 1)));
						V.add(common.parseNull((String)theMap.get("ANNUALCONSUMPTION")));
						V.add(common.parseNull((String)theMap.get("RATEPERKG")));
						V.add("");
						V.add(common.parseNull((String)theMap.get("DISCPER")));
						V.add("");
						V.add(common.parseNull((String)theMap.get("CGSTPER")));
						V.add("");
						V.add(common.parseNull((String)theMap.get("SGSTPER")));
						V.add("");
						V.add(common.parseNull((String)theMap.get("IGSTPER")));
						V.add("");
						V.add(common.parseNull((String)theMap.get("STATUS")));
						V.add("");
						
						thePriceModel.appendRow(V);
       				}
       			}
       		}
        }
		
	}

	
	private static void addComponent(Container container, Component component, int gridx, int gridy, int gridwidth, int gridheight, int anchor, int fill) {
    	
    	GridBagConstraints gbc = new GridBagConstraints(gridx, gridy, gridwidth, gridheight, 1.0, 1.0, anchor, fill, insets, 0, 0);
    	container.add(component, gbc);
    	
  	}	

	private static void addComponent(Container container, Component component, int gridx, int gridy, int anchor, int fill) {
    	
    	GridBagConstraints gbc = new GridBagConstraints(gridx, gridy, 1, 1, 0.5, 0.5, anchor, fill, insts, 0, 0);
    	container.add(component, gbc);
    	
  	}


	private static void addPriceEntryComponent(Container container, Component component, int gridx, int gridy, int anchor, int fill) {
    	
    	GridBagConstraints gbc = new GridBagConstraints(gridx, gridy, 1, 1, 0.5, 0.5, anchor, fill, insts, 0, 0);
    	container.add(component, gbc);
    	
  	}
	
	private void addListeneres(){
		
		JSupplierName . addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent ie){
				setTableData();
				setDyesDetails(getSupplierCode());
				
				thePriceModel.setNumRows(0);
				
				selectchk.setSelected(false);
				
				pricerevisionreportPanel.setItemNames(getSupplierCode());
				
			}
		});
		
        selectchk.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) { 
               
               if(e.getStateChange()==ItemEvent.SELECTED){
                   
                   for(int i=0;i<theModel.getRowCount();i++){
                   		theModel.setValueAt(new Boolean(true),i,2);
                   }
                   
                   setPriceEntryTableData();
               }
               
               if(e.getStateChange()==ItemEvent.DESELECTED){
                   
                   for(int i=0;i<theModel.getRowCount();i++){
                   		theModel.setValueAt(new Boolean(false),i,2);
                   }
                   
                   setPriceEntryTableData();
               }
               
            }
        });
		
		theTable.getModel().addTableModelListener(tableModelListener = new TableModelListener(){
			
			public void tableChanged(TableModelEvent e) {
            	
      			int iRow = theTable.getSelectedRow();
      			
      			System.out.println("theModel Listener iRow==>"+iRow);
      			
      			if(iRow!=-1){
      				
		        	if(((Boolean)theModel.getValueAt(iRow, 2))) {
						 setPriceEntryTableData();
					}
		        	if(!((Boolean)theModel.getValueAt(iRow, 2))) {
						 setPriceEntryTableData();
					}
      				
            	}
			}
		});
		
        bsave.addActionListener(new ActList());
        bupdate.addActionListener(new ActList());
        bSaveNewDye.addActionListener(new ActList());
        bexit.addActionListener(new ActList());
        bprint.addActionListener(new ActList());
		
	}
	
    private class ActList implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if(e.getSource()==bsave){
                System.out.println("dyes price Save");
                
                //if(isValidRevisionDate(pricerevdate.toNormal())){			//Validate - valid date, greater than previous revision date
                
                	String SRevDate = "<html><font color='red'>Revision Date : "+pricerevdate+"</font></html>";
                	if(javax.swing.JOptionPane.showConfirmDialog(null,"Do You Want to Save Data?\n"+SRevDate, "Confirm", javax.swing.JOptionPane.YES_NO_OPTION)==0 ) {
                		saveData();
            		}
            	/*	
            	}else{
            		javax.swing.JOptionPane.showMessageDialog(null,"Please check Revision Date, You are entering less than previous revision date","Info",javax.swing.JOptionPane.INFORMATION_MESSAGE,null);
            	}
            	*/
            }
            
            if(e.getSource()==bupdate){
                System.out.println("dyes price update");
                if(javax.swing.JOptionPane.showConfirmDialog(null,"Do You Want to Update Data?","Confirm",javax.swing.JOptionPane.YES_NO_OPTION)==0 ) {
                	updateData();
            	}
            }
            
            
            if(e.getSource()==bSaveNewDye){
             	System.out.println("New dyes Save");
             	
         	 if(javax.swing.JOptionPane.showConfirmDialog(null,"Do You Want to Save Data?","Confirm",javax.swing.JOptionPane.YES_NO_OPTION)==0 ) {
         		String SDyesName = ((String)TNewDyes.getText()).toUpperCase();
             	
             	if(SDyesName.trim().length()>4 && isDyesNameExist(SDyesName)==0) {
             		int isaved = insertDyesNames(SDyesName);
             		if(isaved==1){
             			TNewDyes.setText("");
             			if(javax.swing.JOptionPane.showConfirmDialog(null,"Data saved...\n Do You Want enter price details for this item?","Confirm",javax.swing.JOptionPane.YES_NO_OPTION)==0 ) {
             			    appendNewDyes(SDyesName);
             			}
             			
             		}else{
             			javax.swing.JOptionPane.showMessageDialog(null,"Data not saved...","Info",javax.swing.JOptionPane.INFORMATION_MESSAGE,null);
             		}
             	}else{
             		if(SDyesName.trim().length()<=4){
             			javax.swing.JOptionPane.showMessageDialog(null,"Please enter Dyes Name..","Info",javax.swing.JOptionPane.INFORMATION_MESSAGE,null);
             		}else{
             			javax.swing.JOptionPane.showMessageDialog(null,"New dyes name already exist...","Info",javax.swing.JOptionPane.INFORMATION_MESSAGE,null);
             		}
             	}
              }	
            }
            
            if(e.getSource()==bexit){
                removeHelpFrame();
            }
        }
         
    }
    
    private void appendNewDyes(String SItemName){
		int iSino = 0;
		/*
			iSino = theModel.getRowCount();
    
	  java.util.Vector  V = new java.util.Vector();
						V . add(String.valueOf(++iSino));
						V . add(common.parseNull(SItemName));
						V . add(new Boolean(true));
						
      theModel.appendRow(V);
      */
      
		    iSino = 0;
			iSino = thePriceModel.getRowCount();

	  java.util.Vector  V = new java.util.Vector();
						V.add(String.valueOf(++iSino));
						V.add(common.parseNull(SItemName));
						V.add("");
						V.add("");
						V.add("");
						V.add("");
						V.add("");
						V.add("");
						V.add("");
						V.add("");
						V.add("");
						V.add("");
						V.add("");
						V.add("");
						V.add("");
						
		thePriceModel.appendRow(V);
		
    }
    
	
    private void removeHelpFrame()   {

        try{
            Layer . remove(this);
            Layer . updateUI();

        }catch(Exception ex)  {
            ex.printStackTrace();
        }
   }
	
	
	private void setData(){
		
		setSupplierNames();
		setDyesName();
		
	}
	
	
	private void setTableData() {
		theTable.getModel().removeTableModelListener(tableModelListener);
		theModel.setNumRows(0);
		thePriceModel.setNumRows(0);
		
		String SupCode = common.parseNull((String)VSupplierCode.elementAt(VSupplierName.indexOf(JSupplierName.getSelectedItem())));
		
		java.util.Vector V = new java.util.Vector();
		
		int isino = 0;
		
	   for (int i = 0; i <VDyesCode.size(); i++) {
            String SItemName = (String)VDyesName.elementAt(i);
            String SSupCode  = common.parseNull((String)VDyesSupCode.elementAt(i));
            
            if(SSupCode.equals(SupCode)){
            	V = new java.util.Vector();
            	V.add(String.valueOf(++isino));
            	V.add(common.parseNull(SItemName));
            	V.add(new Boolean(false));
            	
            	theModel.appendRow(V);
            }
        }
        
		theTable.getModel().addTableModelListener(tableModelListener);
	}
	
	public void setSearchItemNames(String SItemCode, String SItemName) {
		String SupCode = "";
			
		java.util.Vector V = new java.util.Vector();
		
		int isino = 0;
		
	    for (int i = 0; i <VDyesCode.size(); i++) {
            String SCode = (String)VDyesCode.elementAt(i);
            
            if(SCode.equals(SItemCode)) {
            	SupCode = common.parseNull((String)VDyesSupCode.elementAt(i));
            	break;
            }
        }
        
        
        int ind  = VSupplierCode.indexOf(SupCode);
        
        if(ind!=-1){
			theModel.setNumRows(0);
			thePriceModel.setNumRows(0);
        
			JSupplierName.setSelectedItem((String)VSupplierName.elementAt(ind));
			
			setTableData();
			
			int iNoofRows = theModel.getRowCount();
			
			for(int n=0;n<iNoofRows;n++){
				
				if(common.parseNull((String)theModel.getValueAt(n,1)).trim().equals(SItemName.trim())){
					theModel.setValueAt(new Boolean(true),n,2);
				}
			}
			
			theModel.fireTableDataChanged();
			
		}else{
			appendNewDyes(SItemName);
		}
		
		System.out.println("setSearchItemNames Data set done....");
	}
	
	private void clearValues(){
		
		setDyesName();
		setDyesDetails(getSupplierCode());
		setPriceEntryTableData();
		//selectchk.setSelected(false);
		
	}
	
	
	private void saveData() {
		int iUpdate = 0;
		int iInsert = 0;
	
		String SRevisionDate = pricerevdate.toNormal();
        String SupplierCode  = getSupplierCode();
        
        
        String SItemCode     = "";
		
		try{
            if(theDyeConnection==null){
               DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
               theDyeConnection       = jdbc.getConnection();
            }
		    theDyeConnection.setAutoCommit(false);
			
			int iNoofRows = thePriceModel.getRowCount();
			
		    for(int s=0;s<iNoofRows;s++){
			
				SItemCode = getItemCode((String)thePriceModel.getValueAt(s,1));
			
				iUpdate = updatePriceRevisionStatus(SupplierCode, SItemCode, SRevisionDate);
			
				if(iUpdate==2){
					break;
				}
				
				double dCons  = common.toDouble((String)thePriceModel.getValueAt(s,2));	  //Consumption	
				double dRate  = common.toDouble((String)thePriceModel.getValueAt(s,4));	  //Rate
				double dDisc  = common.toDouble((String)thePriceModel.getValueAt(s,6));	  //Discount
				double dCgst  = common.toDouble((String)thePriceModel.getValueAt(s,8));	  //Cgst
				double dSgst  = common.toDouble((String)thePriceModel.getValueAt(s,10));  //Sgst
				double dIgst  = common.toDouble((String)thePriceModel.getValueAt(s,12));  //Igst
				
				iInsert = insertPriceDetails(SupplierCode, SItemCode, dCons, dRate, dDisc, dSgst, dCgst, dIgst, SRevisionDate);
				
				if(iInsert==2){
					break;
				}
			}
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			try{
				if(iUpdate==1 && iInsert==1) {
					theDyeConnection.commit();
					System.out.println("commit");
					
					javax.swing.JOptionPane.showMessageDialog(null,"Data saved...","Info",javax.swing.JOptionPane.INFORMATION_MESSAGE,null);
					
					clearValues();
					
					
				}else{
					theDyeConnection.rollback();
					System.out.println("rollback");
					javax.swing.JOptionPane.showMessageDialog(null,"Data not saved...","Info",javax.swing.JOptionPane.INFORMATION_MESSAGE,null);
				}
				theDyeConnection.setAutoCommit(true);
			
			}catch(Exception e){
				e.printStackTrace();
			}
		
		}
	
	}
	
	
	private void updateData() {
		int iUpdate = 0;
		
		String SMsg          = "";
		String SRevisionDate = pricerevdate.toNormal();
        String SupplierCode  = getSupplierCode();
        
        String SItemCode     = "";
		
		try{
            if(theDyeConnection==null){
               DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
               theDyeConnection       = jdbc.getConnection();
            }
		    theDyeConnection.setAutoCommit(false);
		
			String ServerDate   = common.getServerDate();
		
			int iNoofRows = thePriceModel.getRowCount();
			
		    for(int s=0;s<iNoofRows;s++){
			
				SItemCode = getItemCode((String)thePriceModel.getValueAt(s,1));
			
				//if(isReceipeExist(SItemCode)==0) {
				
					double dCons  = common.toDouble((String)thePriceModel.getValueAt(s,2));	  //Consumption	
					double dRate  = common.toDouble((String)thePriceModel.getValueAt(s,4));	  //Rate
					double dDisc  = common.toDouble((String)thePriceModel.getValueAt(s,6));	  //Discount
					double dCgst  = common.toDouble((String)thePriceModel.getValueAt(s,8));	  //Cgst
					double dSgst  = common.toDouble((String)thePriceModel.getValueAt(s,10));  //Sgst
					double dIgst  = common.toDouble((String)thePriceModel.getValueAt(s,12));  //Igst
				
					iUpdate = updateDyesPrice(SupplierCode, SItemCode, dCons, dRate, dDisc, dSgst, dCgst, dIgst, SRevisionDate);
					
					if(iUpdate==2) {
						break;
					}
					
				/*	
				}else{
					SMsg += "Receipe exist for this item \n"+(String)thePriceModel.getValueAt(s,1);
				}
				*/
			}
		
			
			if(SMsg.length()>0){
			javax.swing.JOptionPane.showMessageDialog(null,"Dyes price not updated for this Items : "+SMsg, "Info", javax.swing.JOptionPane.INFORMATION_MESSAGE, null);
			}		
	
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			try{
				if(iUpdate==1) {
					theDyeConnection.commit();
					System.out.println("commit");
					javax.swing.JOptionPane.showMessageDialog(null,"Data saved...","Info",javax.swing.JOptionPane.INFORMATION_MESSAGE,null);
					
					selectchk.setSelected(false);
					
					clearValues();
					
				}else{
					theDyeConnection.rollback();
					System.out.println("rollback");
					javax.swing.JOptionPane.showMessageDialog(null,"Data not saved...","Info",javax.swing.JOptionPane.INFORMATION_MESSAGE,null);
				}
				theDyeConnection.setAutoCommit(true);
			
			}catch(Exception e){
				e.printStackTrace();
			}
		
		}
	
	}
	
	
	private boolean isValidRevisionDate(String SRevisionDate) {
		
		int isize  = ADyesDetailList.size();
        		
		int iNoofRows = thePriceModel.getRowCount();
		
		String SItemCode = "";
		
	    for(int s=0;s<iNoofRows;s++) {
			SItemCode = getItemCode((String)thePriceModel.getValueAt(s,1));
        		
			for(int n=0;n<isize;n++) {
				java.util.HashMap theMap = (java.util.HashMap)ADyesDetailList.get(n);
			
				if(SItemCode.equals(common.parseNull((String)theMap.get("ITEM_CODE")))) {
			
					if(common.toInt(common.parseNull((String)theMap.get("WITHEFFECTFROM")))>common.toInt(SRevisionDate)){
						return false;
					}
				}
		    }
	    }
        
		return true;
	}
	
	
	public int isReceipeExist(String SItemCode){
         StringBuffer sb = new StringBuffer(); 
         			  sb . append(" Select Count(*) from ShadeWiseDyes  Where Dyes_Code = '"+SItemCode.trim()+"' and new_receipe = 1 ");
         
	 	try{         
         	if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
            }
        	java.sql.PreparedStatement ps = theDyeConnection.prepareStatement(sb.toString());
        	java.sql.ResultSet rst =   ps . executeQuery();
        	
         	if(rst.next()) {
              	return rst.getInt(1);
      		}
            rst.close();
            rst=null;
            ps.close();
            ps=null;
           
     	}catch(Exception e){
          	e.printStackTrace();
          	return 1;
      	}
 
  		return 0;
 	}
	
	
	public int updatePriceRevisionStatus (String SupplierCode, String sDyesCode, String SRevisionDate) {
        StringBuffer sb = new StringBuffer(); 
				     sb . append(" Update DyesPriceRevision  set PriceRevisionStatus=1, WithEffectTo = '"+SRevisionDate+"' ");
					 sb . append(" Where Supp_Code='"+SupplierCode+"' and Dyes_Code='"+sDyesCode+"' ");
					 sb . append(" and ID = (Select Max(ID) from DyesPriceRevision Where Supp_Code='"+SupplierCode+"' and Dyes_Code='"+sDyesCode+"' ) ");
         			 //sb.append(" and PriceRevisionStatus="+iPriceRevisionSatus);
         
        try{
           java.sql.PreparedStatement ps = theDyeConnection.prepareStatement(sb.toString());
       								  ps . executeUpdate();
       	
        }catch(Exception e) {
       		e.printStackTrace();
       		return 2;
        }
 
  		return 1;
	}
	
	
	private int insertPriceDetails(String SupplierCode, String SItemCode, double dCons, double dRate, double dDisc, double dSgst, double dCgst, double dIgst, String SRevisionDate) {
	
         StringBuffer sb = new StringBuffer(); 
         sb.append(" insert into DyesPriceRevision ( ID , Dyes_Code, AnnualConsumption, RateperKg, Discper, Taxper, Discount, Tax, NetValue, TotalValue, ");
         sb.append(" WithEffectFrom, WithEffectTo, EntrySysName, EntryDateTime, Supp_Code, CGSTPer, CGSTAmount, SGSTPer, SGSTAmount, ");
         sb.append(" IGSTPer, IGSTAmount, REVISION_DATE, REVISION_UCODE ) ");
         sb.append(" Values ( DyesPrice_ID_Seq.nextVal, ?, ?, ?, ?, 0, ?, 0, ?, ?, ");
         sb.append(" ?, 99999999, ?, to_Char(sysdate,'YYYYMMDD HH24:Mi:ss'), ?, ?, ?, ?, ?, ?, ?, SYSDATE, ? ) ");
	
	
		String sDiscount	= common.getRound((dRate*(dDisc/100)),2);
		
		double dNetValue    = (dRate-common.toDouble(sDiscount));
		//double dNetValue    = (dRate-common.toDouble(sDiscount))+common.toDouble(sCGST)+common.toDouble(sSGST)+common.toDouble(sIGST);
		String sCGST		= common.getRound((dNetValue*(dCgst/100)),2);
		String sSGST		= common.getRound((dNetValue*(dSgst/100)),2);
		String sIGST		= common.getRound((dNetValue*(dIgst/100)),2);
		double dTotalValue  = dCons*dNetValue;
         
        try{
        	java.sql.PreparedStatement ps = theDyeConnection.prepareStatement(sb.toString());
        							   ps . setString(1, SItemCode);
        							   ps . setString(2, String.valueOf(dCons));
        							   ps . setString(3, String.valueOf(dRate));
        							   ps . setString(4, String.valueOf(dDisc));
        							   ps . setString(5, sDiscount);
        							   ps . setString(6, common.getRound(dNetValue,2));
        							   ps . setString(7, common.getRound(dTotalValue,2));
        							   ps . setString(8, SRevisionDate);
        							   ps . setString(9, InetAddress.getLocalHost().getHostName());
        							   ps . setString(10, SupplierCode);
        							   ps . setString(11, String.valueOf(dCgst));
        							   ps . setString(12, sCGST);
        							   ps . setString(13, String.valueOf(dSgst));
        							   ps . setString(14, sSGST);
        							   ps . setString(15, String.valueOf(dIgst));
        							   ps . setString(16, sIGST);
        							   ps . setString(17, String.valueOf(iUserCode));
            						   ps . executeUpdate();
            						   ps . close();
            						   ps = null;
            						   
            						   
      	}catch(Exception e) {
           e.printStackTrace();
           return 2;
        }
   
	 	return 1;
	}
	
	
	private int updateDyesPrice(String SupplierCode, String SItemCode, double dCons, double dRate, double dDisc, double dSgst, double dCgst, double dIgst, String SRevisionDate) {
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" Update DyesPriceRevision  set updated_date = sysdate, RateperKg= ?, Discper = ?, Discount = ?, NetValue = ?, TotalValue = ?, ");
         sb.append(" CGSTPer = ?, CGSTAmount= ?, SGSTPer = ?, SGSTAmount = ?, IGSTPer = ?, IGSTAmount = ? ");
         sb.append(" Where Supp_Code = ? and Dyes_Code= ? and PriceRevisionStatus = 0 ");
		
		
		String sDiscount	= common.getRound((dRate*(dDisc/100)),2);
		double dNetValue    = (dRate-common.toDouble(sDiscount));
		//double dNetValue    = (dRate-common.toDouble(sDiscount))+common.toDouble(sCGST)+common.toDouble(sSGST)+common.toDouble(sIGST);
		String sCGST		= common.getRound((dNetValue*(dCgst/100)),2);
		String sSGST		= common.getRound((dNetValue*(dSgst/100)),2);
		String sIGST		= common.getRound((dNetValue*(dIgst/100)),2);
		double dTotalValue  = dCons*dNetValue;
		
        try{
        	java.sql.PreparedStatement ps = theDyeConnection.prepareStatement(sb.toString());
        							   ps . setString(1, String.valueOf(dRate));
        							   ps . setString(2, String.valueOf(dDisc));
        							   ps . setString(3, sDiscount);
        							   ps . setString(4, common.getRound(dNetValue,2));
        							   ps . setString(5, common.getRound(dTotalValue,2));
        							   ps . setString(6, String.valueOf(dCgst));
        							   ps . setString(7, sCGST);
        							   ps . setString(8, String.valueOf(dSgst));
        							   ps . setString(9, sSGST);
        							   ps . setString(10, String.valueOf(dIgst));
        							   ps . setString(11, sIGST);
        							   ps . setString(12, SupplierCode);
        							   ps . setString(13, SItemCode);
            						   ps . executeUpdate();
            						   ps . close();
            						   ps = null;
            						   
            						   
      	}catch(Exception e) {
           e.printStackTrace();
           return 2;
        }
   
	 	return 1;
	}
	
	
	private int insertDyesNames(String SDyesName){
         StringBuffer sb = new StringBuffer(); 
         			  sb . append(" insert into Dyes_InvItems ( ID , Dyes_Item_Code ,Dyes_Item_Name )");
         			  sb . append(" Values ( Dyes_InvID_Seq.nextVal,'CD01'||Dyes_InvItemCode_Seq.nextVal,'"+SDyesName+"' ) ");
         
		try{
        	if(theDyeConnection==null){
           		DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
           		theDyeConnection       = jdbc.getConnection();
        	}
        	java.sql.PreparedStatement ps = theDyeConnection.prepareStatement(sb.toString());
            				  		   ps . executeUpdate();
            				  		   ps . close();
                              		   ps = null;
	
	
      	}catch(Exception e)  {
          	e.printStackTrace();
          	return 2;
      	}
		return 1;
	}
	
	public int isDyesNameExist(String SDyesName){
         StringBuffer sb = new StringBuffer(); 
         			  sb . append(" Select Count(*) from Dyes_InvItems  Where Dyes_Item_Name = '"+SDyesName.trim()+"' ");
         
	 	try{         
         	if (theDyeConnection==null){
                  DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                  theDyeConnection       = jdbc.getConnection();
            }
        	java.sql.PreparedStatement ps = theDyeConnection.prepareStatement(sb.toString());
        	java.sql.ResultSet rst =   ps . executeQuery();
        	
         	if(rst.next()) {
              	return rst.getInt(1);
      		}
            rst.close();
            rst=null;
            ps.close();
            ps=null;
           
     	}catch(Exception e){
          	e.printStackTrace();
          	return 1;
      	}
 
  		return 0;
 	}
	

	 public void setSupplierNames(){
		VSupplierCode	= new java.util.Vector();
		VSupplierName	= new java.util.Vector();
        StringBuilder sb = new StringBuilder(); 
					  sb . append("  Select t.Sup_Code,Supplier.Name ");
					  sb . append("  from ");
					  sb . append("  (select  distinct PurchaseOrder.Sup_Code from PurchaseOrder@amarml ");
					  sb . append("  Inner join InvItems on InvItems.Item_Code=PurchaseOrder.Item_Code ");
					  sb . append("  and StkGroupCode='C01' ) t ");
					  sb . append("  Inner join supplier@amarml on Supplier.Ac_Code=t.Sup_Code ");
					  sb . append("  Union All ");
					  sb . append("  Select Dyes_Party_Code,Dyes_Party_Name from  Dyes_Party ");
					  sb . append("  order by 2 ");

        
		try{
        	if (theDyeConnection==null){
                DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                theDyeConnection       = jdbc.getConnection();
            }
        	java.sql.Statement stat    = theDyeConnection.createStatement();
        	java.sql.ResultSet  rst    = stat.executeQuery(sb.toString());
    
          while (rst.next()){
              VSupplierCode  . add(common.parseNull(rst.getString(1)));
              VSupplierName  . add(common.parseNull(rst.getString(2)));
         	}
            rst.close();
            stat.close();
            
    	}catch(Exception e){
            e.printStackTrace();
	    }
	    
	}

	 public void setDyesName(){
		VDyesCode   	= new java.util.Vector();
		VDyesName	    = new java.util.Vector();
		VDyesSupCode    = new java.util.Vector();
		VDyesPriceEntryDate  = new java.util.Vector();
        StringBuilder sb = new StringBuilder(); 
        			  sb . append("  Select distinct Item_Code, Item_Name, supp_code, max(DyesPriceEntryDate) from ( ");
					  sb . append("  Select distinct DyeingInvItems.Item_Code, Item_Name, supp_code, to_char(to_date(Dyespricerevision.entrydatetime,'yyyymmdd hh24:mi:ss'),'yyyymmdd') as DyesPriceEntryDate from DyeingInvItems ");
					  sb . append("  Inner Join InvItems on InvItems.Item_Code = DyeingInvItems.Item_Code and DyeingInvItems.STKGROUPCODE in ('C01') ");
					  sb . append("  Left join  Dyespricerevision@amardye2 on InvItems.ITEM_CODE = Dyespricerevision.DYES_CODE ");
					  sb . append("  Union All ");
					  sb . append("  Select Dyes_InvItems.Dyes_Item_Code, Dyes_InvItems.Dyes_Item_Name, supp_code, to_char(to_date(Dyespricerevision.entrydatetime,'yyyymmdd hh24:mi:ss'),'yyyymmdd') as DyesPriceEntryDate from Dyes_InvItems@amardye2 ");
					  sb . append("  inner join  Dyespricerevision@amardye2 on Dyes_InvItems.Dyes_Item_Code = Dyespricerevision.DYES_CODE ");
					  sb . append(" ) Group by Item_Code, Item_Name, supp_code ");
					  sb . append(" Order by 2 ");
        
		try{
             if(theInvConnection==null) {
                JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                theInvConnection     = jdbc.getConnection();
             }
        	java.sql.Statement stat    = theInvConnection.createStatement();
        	java.sql.ResultSet  rst    = stat.executeQuery(sb.toString());
    
          while (rst.next()){
              VDyesCode    . add(common.parseNull(rst.getString(1)));
              VDyesName    . add(common.parseNull(rst.getString(2)));
              VDyesSupCode . add(common.parseNull(rst.getString(3)));
              VDyesPriceEntryDate . add(common.parseNull(rst.getString(4)));
         	}
            rst.close();
            stat.close();
            
    	}catch(Exception e){
            e.printStackTrace();
	    }
	    
	}
	
	public void setDyesDetails(String SupCode) {
        ADyesDetailList         = new java.util.ArrayList();
        StringBuffer sb         = new StringBuffer(); 
        sb.append(" Select Name, Item_Code, AnnualConsumption, RatePerKg, Discount, Tax, NetValue, TotalValue,  ");
        sb.append(" WithEffectFrom, WithEffectTo, ItemName, Discper, Taxper, Status , ");
        sb.append(" CGSTPer, CGSTAmount, SGSTPer, SGSTAmount, IGSTPer, IGSTAmount from ( ");
        sb.append(" Select Supplier.Name,Item_Code,AnnualConsumption,RatePerKg,Discount,Tax,NetValue,TotalValue, ");
        sb.append(" WithEffectFrom, WithEffectTo,item_Name as ItemName,Discper,Taxper,DyesLabStatus.Status ,");
        sb.append(" CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,IGSTAmount from DyesPriceRevision@amardye2 ");
        sb.append(" Inner Join InvItems 	  on InvItems.Item_Code 	= DyesPriceRevision.Dyes_Code ");
        sb.append(" Inner Join Supplier 	  on Supplier.AC_Code	    = DyesPriceRevision.Supp_Code ");
        sb.append(" Left Join DyesLabStatus on DyesLabStatus.StatusCode = DyesPriceRevision.LabStatus ");
        sb.append(" Where  Supplier.Ac_code = '"+SupCode+"'  and (PriceRevisionStatus=0  OR PriceRevisionStatus is null) ");
        sb.append(" Union All ");
        sb.append(" Select Supplier.Name,Dyes_Item_Code,AnnualConsumption,RatePerKg,Discount,Tax,NetValue,TotalValue, ");
        sb.append(" WithEffectFrom, WithEffectTo,Dyes_item_Name as ItemName,Discper,Taxper,DyesLabStatus.Status, ");
        sb.append(" CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,IGSTAmount from DyesPriceRevision@amardye2 ");
        sb.append(" Inner Join Dyes_InvItems on Dyes_InvItems.Dyes_Item_Code = DyesPriceRevision.Dyes_Code");
        sb.append(" Inner Join Supplier 	   on Supplier.AC_Code			 = DyesPriceRevision.Supp_Code ");
        sb.append(" Left Join DyesLabStatus  on DyesLabStatus.StatusCode     = DyesPriceRevision.LabStatus ");
        sb.append(" Where  Supplier.Ac_code = '"+SupCode+"'  and (PriceRevisionStatus=0  OR PriceRevisionStatus is null) ");
        sb.append(" ) Order by 1 ");
        
        try{
        	if(theInvConnection==null) {
                JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                theInvConnection     = jdbc.getConnection();
            }
        	java.sql.PreparedStatement ps     = theInvConnection.prepareStatement(sb.toString());
        	java.sql.ResultSet rst            = ps.executeQuery();
        	
        	java.sql.ResultSetMetaData rsmd   = rst.getMetaData();
        
       		while (rst.next()){
              	java.util.HashMap row = new java.util.HashMap();
              	
            	for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    row.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
            
                ADyesDetailList.add(row);
         	}
            rst.close();
            ps.close();
            ps=null;
            
            
    	}catch(Exception e){
    	    e.printStackTrace();
    	}
    
	}

	
	public String getSupplierCode(){
		int ind = VSupplierName.indexOf(JSupplierName.getSelectedItem());
		
		if(ind!=-1){
			return (String)VSupplierCode.elementAt(ind);
		}
		
		return null;
	}
	
	public String getItemCode(String SItemName){
	
		int ind = VDyesName.indexOf(SItemName);
		
		if(ind!=-1){
			return (String)VDyesCode.elementAt(ind);
		}
		
		return null;
	}
	
	public String getDyesPriceEntryDate(String SItemName){
	
		int ind = VDyesName.indexOf(SItemName);
		
		if(ind!=-1){
			return (String)VDyesPriceEntryDate.elementAt(ind);
		}
		
		return null;
	}


    class MyRenderer  extends javax.swing.table.DefaultTableCellRenderer {
    	@Override
  		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    		
			java.awt.Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
 
		    if (column == 0) {
		        this.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
	        }else if(column>=2 && column<=4){
	        	this.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        	}else if(column>=5 && column<=12){
        		this.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		    }else {
		        this.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
			}
			
        	return component;    		
  		}
	}

}
