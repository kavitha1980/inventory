package DyesPriceRevision;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.Insets;

import com.itextpdf.text.Document;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import java.io.FileOutputStream;



import guiutil.*;

import java.util.Vector;

public class PriceRevisionReportPanelNew extends JPanel {
    
    private JPanel        toppanel, bottompanel;
    
    private static final Insets insets = new Insets(0, 0, 0, 0);
    
    private JComboBox       JOldrate;
    
    JList                   theItemList;
    DefaultListModel        theItemListModel;
    
    private JEditorPane	  editorPane;
    
    private JButton       bprint, bexit;
    
	private Vector  	  VDyesCode, VDyesName, VDyesSupCode;
	
	Common  common;
	
	java.util.List<java.util.HashMap>   theDataList = null;
	
	java.sql.Connection theInvConnection = null;
	java.sql.Connection theDyeConnection = null;


    Document                document;
    PdfPTable               table;
    int[]                   iColWidth ;
    PdfPCell                c1;
    
    private static Font     bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font     mediumbold = FontFactory.getFont("TIMES_ROMAN", 6, Font.NORMAL);
    private static Font     mediumbold1 = FontFactory.getFont("TIMES_ROMAN", 6, Font.BOLD);
    private static Font     smallnormal = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
    private static Font     smallbold = FontFactory.getFont("TIMES_ROMAN", 7, Font.BOLD);
    private static Font     smallbold1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
	
	PriceRevisionEntryFrameNew  priceRevisionEntryFrameNew = null;
	
	public PriceRevisionReportPanelNew(PriceRevisionEntryFrameNew priceRevisionEntryFrameNew) {
    
		this.priceRevisionEntryFrameNew = priceRevisionEntryFrameNew;
    
    	initValues();
    	createComponents();
    	setLayouts();
    	addComponents();
    	addListeneres();
	}    
	
	private void initValues(){
		common = new Common();
		
		theDataList = new java.util.ArrayList<java.util.HashMap>();
		setDyesName();
	}
	
	private void createComponents(){
		
		toppanel    = new JPanel();
		bottompanel = new JPanel();
		
		JOldrate      = new JComboBox();
		
		JOldrate .addItem("1");
		JOldrate .addItem("2");
		JOldrate .addItem("3");
		JOldrate .addItem("4");
		JOldrate .addItem("5");
		JOldrate .addItem("ALL");
        
        theItemListModel = new DefaultListModel();
        theItemList      = new JList(theItemListModel);
        theItemList      . setVisibleRowCount(5);
        
        theItemList.setCellRenderer(new CheckListRendererItm());
		theItemList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		editorPane  = new JEditorPane();
		editorPane  . setEditable(false);
		
		bexit  = new JButton("Exit");
		bprint = new JButton("Print");
		
	}
	
	private void setLayouts(){
		
      	SCLayout  baselayout         =  new SCLayout(2,SCLayout.FILL,SCLayout.FILL,0);
                  baselayout         .  setScale(0,0.25);
                  baselayout         .  setScale(1,0.75);

        setLayout(baselayout);
		
        toppanel     . setLayout(new GridBagLayout());
        bottompanel  . setLayout(new BorderLayout());
        
        toppanel     . setBorder(new TitledBorder("Top"));
        bottompanel  . setBorder(new TitledBorder("Price Details"));
		
	}
	
	private void addComponents(){

		JScrollPane DyesScrollpane = new JScrollPane(theItemList);
					DyesScrollpane . setBorder(BorderFactory.createTitledBorder("Dyes "));

		addComponent(toppanel, DyesScrollpane, 0, 1, 5, 3, GridBagConstraints.CENTER, GridBagConstraints.BOTH);
		
		addComponent(toppanel, new JLabel("No of Old Rate"), 6, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		addComponent(toppanel, JOldrate, 7, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		
		addComponent(toppanel, bprint, 7, 2, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		
		bottompanel.add(new JScrollPane(editorPane));
		
        this.add("Center",toppanel);
        this.add("South",bottompanel);
	}
	
	private static void addComponent(Container container, Component component, int gridx, int gridy, int gridwidth, int gridheight, int anchor, int fill) {
    	
    	GridBagConstraints gbc = new GridBagConstraints(gridx, gridy, gridwidth, gridheight, 1.0, 1.0, anchor, fill, insets, 0, 0);
    	container.add(component, gbc);
    	
  	}	
  	
	private static void addComponent(Container container, Component component, int gridx, int gridy, int anchor, int fill) {
    	
    	GridBagConstraints gbc = new GridBagConstraints(gridx, gridy, 1, 1, 0.5, 0.5, anchor, fill, insets, 0, 0);
    	container.add(component, gbc);
    	
  	}	
	
	private void addListeneres(){
		
        bprint.addActionListener(new ActionListener(){
        
		    public void actionPerformed(ActionEvent e) {
		        
		        String SName = (String)priceRevisionEntryFrameNew.JSupplierName.getSelectedItem();
		        String SCode = priceRevisionEntryFrameNew.getSupplierCode();

		        setData(SCode);
		        setReport(SName);
		    }
        });
        
        theItemList.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
                
                JList jlist = (JList) event.getSource();
                
                int index = jlist.locationToIndex(event.getPoint());
                
                CheckBoxItem Citem = (CheckBoxItem) jlist.getModel().getElementAt(index);
                
                Citem.setSelected(!Citem.isSelected());
                
                jlist.repaint(jlist.getCellBounds(index, index));
            }
        });
        
		
	}
	
    public void setReport(String SName) {
        
        try {
            String sReport = "";
            sReport = getReport(SName);
            editorPane.setContentType("text/html");
            editorPane.setText(sReport);
            
        }catch (Exception e) {
           e.printStackTrace();
        }
    }
	
	
	
	public void setItemNames(String SupCode){
		theItemListModel = new DefaultListModel();
		theItemListModel . removeAllElements();
		
		System.out.println("SupCode==>"+SupCode);
		
	   for (int i = 0; i <VDyesCode.size(); i++) {
            String SItemName = (String)VDyesName.elementAt(i);
            String SSupCode  = (String)VDyesSupCode.elementAt(i);
            
            if(SSupCode.equals(SupCode)){
            	theItemListModel.addElement(new CheckBoxItem(SItemName));
            }
        }
		
		theItemList.setModel( theItemListModel );
		
		updateUI();
		
	}

	
	private String getItemCode(String SItemName){
		
		int ind = VDyesName.indexOf(SItemName);
		
		if(ind!=-1){
			return common.parseNull((String)VDyesCode.elementAt(ind));
		}
		
		return null;
	}

	 public void setDyesName(){
		VDyesCode   	= new java.util.Vector();
		VDyesName	    = new java.util.Vector();
		VDyesSupCode    = new java.util.Vector();
        StringBuilder sb = new StringBuilder(); 
					  sb . append("  Select distinct DYEINGINVITEMS.Item_Code, Item_Name, SUPP_CODE  from DYEINGINVITEMS");
					  sb . append("  Inner Join InvItems on InvItems.Item_Code = DyeingInvItems.Item_Code ");
					  sb . append("  inner join  Dyespricerevision@amardye2 on InvItems.ITEM_CODE = Dyespricerevision.DYES_CODE ");
					  //sb . append("  Where SUPP_CODE = '"+sSupplierCode+"'");
					  sb . append("  order by 2 ");
					  
        
		try{
        	if(theInvConnection==null) {
                JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                theInvConnection     = jdbc.getConnection();
            }
        	java.sql.Statement stat    = theInvConnection.createStatement();
        	java.sql.ResultSet  rst    = stat.executeQuery(sb.toString());
    
       		while (rst.next()){
              VDyesCode    . add(rst.getString(1));
              VDyesName    . add(rst.getString(2));
              VDyesSupCode . add(rst.getString(3));
         	}
            rst.close();
            stat.close();
            
    	}catch(Exception e){
            e.printStackTrace();
	    }
	    
	}
	
	private String getReport(String SPartyName){
		String sStr = "";
        
        try{
        	String SPriceRevDate = "";
        	
        	String PDFFile          = "";
        	
            String Sos = System.getProperty("os.name").toLowerCase();
            if(Sos.trim().startsWith("wind")){
            	PDFFile          = "D:/"+SPartyName+".pdf";
            }else{
            	PDFFile          = "/"+SPartyName+".pdf";
            }
        	
            document    = new Document();
            PdfWriter writer = PdfWriter   . getInstance(document, new FileOutputStream(PDFFile));
            document    . setPageSize(PageSize.A4.rotate());
            document    . open();
        	
        	int  iMaxRateRank = getMaximumRateRank();
        	
        	int iTotalColumns = (12+(5*iMaxRateRank));
        	
        	float[] columnWidths = new float[iTotalColumns];
			
        	if(iMaxRateRank==1) {
        		columnWidths = new float[]{15f,80f,30f  ,25f,12f,30f,30f,40f ,25f,12f,30f,30f,40f  ,30f,40f,30f,30f };
        	}else{
        		columnWidths[0] = 15f;
        		columnWidths[1] = 80f;
        		columnWidths[2] = 30f;
        		
        		int n = iTotalColumns-4;	//22, 27,32,37, 42
        		
        		columnWidths[n]   = 30f;
        		columnWidths[n+1] = 50f;
        		columnWidths[n+2] = 30f;
        		columnWidths[n+3] = 30f;
        		
        		int icnt = 0;
        		
				for (int c=3; c<n; c++) {
				    icnt++;
				    if(icnt==1){
				    	columnWidths[c] = 25f;
				    }else if(icnt==2){
				    	columnWidths[c] = 12f;
				    }else if(icnt==5){
				    	columnWidths[c] = 50f;
				    	icnt= 0 ;
				    }else{
				    	columnWidths[c] = 30f;
				    }
				}
		            	
        	}
        	
        	
             table      =  new PdfPTable(iTotalColumns);
             table      . setWidths(columnWidths);
             table      . setWidthPercentage(100f);
             //table	. setHeaderRows(1);   //Header for each rows
             
             //writer . setPageEvent(new HeaderFooter());
        	
        	
            sStr = "<html> <head>";
            sStr = sStr + "Revisied Price List";
            sStr = sStr + "</head>";
            sStr = sStr + "<body >";
            //sStr = sStr + "<h2 align ='center'> <b>REVISED PRICE IN "+SPartyName+" - "+common.parseDate(SPriceRevDate)+" </b></h2>";
            sStr = sStr + "<h2 align ='center'> <b>REVISED PRICE IN "+SPartyName+" - "+common.parseDate(getMaximumDate())+" </b></h2>";
            
            
            AddCellIntoTable("REVISED PRICE IN "+SPartyName+" - "+common.parseDate(getMaximumDate()), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns , 0, 0, 0, 0, bigbold,25f);
            
            AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns , 0, 0, 0, 0, bigbold,10f);
            
            
            sStr = sStr + "<table border='1' width='70%' bgcolor='lightGrey'>";
            
            sStr = sStr + "<tr>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>SL NO </b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>DYES NAME</b></font></td>";
            sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>ONE YEAR CONSUMPTION QTY</b></font></td>";
            
            for(int n=0;n<iMaxRateRank;n++) {
        	sStr = sStr + "<td align='center' color='#000099' colspan='5'><font color='#000099' ><b> OLD RATE("+common.parseDate(getMinimumDate((iMaxRateRank-n)))+")</b></font></td>";
        	sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>&nbsp;</b></font></td>";
        	}
        	
        	sStr = sStr + "<td align='center' color='#000099' colspan='5'><font color='#000099' ><b> NEW RATE("+common.parseDate(getMaximumDate())+")</b></font></td>";
        	sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>&nbsp;</b></font></td>";
	        sStr = sStr + "<td align='center' color='#000099' colspan='3' width='125'><font color='#000099' ><b>DIFFERENCE</b></font></td>";
	        sStr = sStr + "<td align='center' color='#000099' rowspan='3'><font color='#000099' ><b>LAB STATUS</b></font></td>";
			sStr = sStr + "</tr>";
			
			sStr = sStr + "<tr>";
			for(int n=0;n<iMaxRateRank;n++){
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2' width='100' ><font color='#000099' ><b> Discount  </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Net Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Total</b></font></td>";
			}
			
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' colspan='2' width='100' ><font color='#000099' ><b> Discount  </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Net Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Total</b></font></td>";
			
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Net Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>Total Value</b></font></td>";
            sStr = sStr + "<td align='center' color='#00099' rowspan='2'><font color='#000099' ><b>%</b></font></td>";
            sStr = sStr + "</tr>";
            
            sStr = sStr + "<tr>";
            for(int n=0;n<iMaxRateRank;n++){
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
			}
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b> % </b></font></td>";
            sStr = sStr + "<td align='center' color='#00099'><font color='#000099' ><b>Value</b></font></td>";
            sStr = sStr + "</tr>";
			
			
			
            AddCellIntoTable("Sl.No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,3);
            AddCellIntoTable("Dyes Name", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,3);
            AddCellIntoTable("One Year Consumption Qty", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,3);
            for(int n=0;n<iMaxRateRank;n++){
            AddCellIntoTable("Old Rate ("+common.parseDate(getMinimumDate((iMaxRateRank-n)))+")", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 5 , 4, 1, 8, 2,smallbold1,30f);
            }
            AddCellIntoTable("New Rate ("+common.parseDate(getMaximumDate())+")", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 5 , 4, 1, 8, 2, smallbold1,30f);
            AddCellIntoTable("Difference", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 3 , 4, 1, 8, 2, smallbold1,30f);
            AddCellIntoTable("Lab Status", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2,smallbold,3);
			
			for(int n=0;n<iMaxRateRank;n++){
            AddCellIntoTable("Rate/Kg", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
            AddCellIntoTable("Discount", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2 , 4, 1, 8, 2, smallbold,30f);
            AddCellIntoTable("Net Value", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
            AddCellIntoTable("Total", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
			}
			
            AddCellIntoTable("Rate/Kg", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
            AddCellIntoTable("Discount", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2 , 4, 1, 8, 2, smallbold,30f);
            AddCellIntoTable("Net Value", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
            AddCellIntoTable("Total", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);

            AddCellIntoTable("Net Value", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
            AddCellIntoTable("Total Value", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);
            AddCellIntoTable("%", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold,2);

			//
			for(int n=0;n<iMaxRateRank;n++){
            AddCellIntoTable("%", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2,smallbold,30f);
            AddCellIntoTable("Value", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2,smallbold,30f);
			}
            AddCellIntoTable("%", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2,smallbold,30f);
            AddCellIntoTable("Value", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2,smallbold,30f);
			
			
			table		. setHeaderRows(5);   //Header for each rows
			
			int iSino = 0;
			
			//System.out.println("iMaxRateRank==>"+iMaxRateRank);

			int iSize = theDataList.size();
			
			String SItemName = "";
			String SItemCode = "";
			String SConsumption = "";
			
			double[] dGPrevNetValue   = new double[iMaxRateRank];
			double[] dGPrevTotalValue = new double[iMaxRateRank];
			
            double dGNewNetValue= 0;
            double dGNewTotalValue= 0;
			
			java.util.List thePriceList = new java.util.ArrayList();
			
			java.util.List theSelectedDyesList = getSelectedDyes();
			
			for(int n=0;n<theSelectedDyesList.size();n++){
                SItemName = (theSelectedDyesList.get(n).toString());
                SItemCode = getItemCode(SItemName);
                SConsumption = getConsumption(SItemCode);
                
                iSino++;
                
                sStr = sStr + "<tr>";
                sStr = sStr + "<td   align='center' ><font color='#CD5C5C' ><b>"+(iSino)+"</b></font></td>";
                sStr = sStr + "<td   align='left'><font color= '#CD5C5C' ><b>" + SItemName + "</b></font></td>";
                sStr = sStr + "<td   align='left'><font color= '#CD5C5C' ><b>"+ SConsumption +"</b></font></td>";
                
                 AddCellIntoTable(String.valueOf(iSino), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                 AddCellIntoTable(SItemName, table, Element.ALIGN_LEFT , Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                 AddCellIntoTable(SConsumption, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                
                thePriceList = getPriceList(SItemCode, iMaxRateRank);
                
                double dNewNetValue  = 0, dPrevNetValue = 0;
                double dNewTotalValue= 0, dPrevTotalValue = 0;
                
                
                String SLabStatus = "";
                
				for(int i=0;i<thePriceList.size();i++) {
				    java.util.HashMap  theMap = (java.util.HashMap)thePriceList.get(i);
                		
        			int iPurStatus        = common.toInt((String)theMap.get("PURSTATUS"));
              		int iPriceRevStatus   = common.toInt((String)theMap.get("PRICEREVISIONSTATUS"));
            		
		            String sRateperKg     = common.parseNull((String)theMap.get("RATE"));
		            String sDiscPer       = common.parseNull((String)theMap.get("DISPER"));
		            double dDiscount      = (common.toDouble(sRateperKg)*common.toDouble(sDiscPer))/100;
		            
		            double dNetValue      = (common.toDouble(sRateperKg)-dDiscount);
		            double dTotalValue    =  common.toDouble(SConsumption)*dNetValue;
            		
            		//System.out.println("iPriceRevStatus==>"+iPriceRevStatus+"=RateRank="+common.parseNull((String)theMap.get("RATERANK"))+"=dGPrevNetValue="+dGPrevNetValue.length);
            		
            		if(iPriceRevStatus==1) {
            			dGPrevNetValue[i]     += dNetValue;
            			dGPrevTotalValue[i]   += dTotalValue;
            		}
            		
            		if(iPriceRevStatus==1 && common.toInt((String)theMap.get("RATERANK"))==2){
            			dPrevNetValue   = dNetValue;
            			dPrevTotalValue = dTotalValue;
            		}
            		if(iPriceRevStatus==0 && common.toInt((String)theMap.get("RATERANK"))==1){
            			dNewNetValue   = dNetValue;
            			dNewTotalValue = dTotalValue;
            			
            			SLabStatus     = common.parseNull((String)theMap.get("LABSTATUS"));  
            		}
            		
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sRateperKg + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + sDiscPer + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dDiscount),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dNetValue),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dTotalValue),2) + "</b></font></td>";
                  sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
                  
                  
                  AddCellIntoTable(sRateperKg, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);     
                  AddCellIntoTable(sDiscPer, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                  AddCellIntoTable(common.getRound(String.valueOf(dDiscount),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                  AddCellIntoTable(common.getRound(String.valueOf(dNetValue),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                  AddCellIntoTable(common.getRound(String.valueOf(dTotalValue),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
		            
                }
		          double dNetValueDiff    = dNewNetValue  - dPrevNetValue;
		          double dTotalValueDiff  = dNewTotalValue- dPrevTotalValue;
		          double dDiffPercentage  = ((dNewNetValue - dPrevNetValue) / dPrevNetValue) * 100;
		          
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dNetValueDiff),2) + "</b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dTotalValueDiff),2) + "</b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dDiffPercentage),2) + "</b></font></td>";
                 sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + SLabStatus + "</b></font></td>";
                 sStr = sStr + "</tr>";
                 
		        
                 AddCellIntoTable(common.getRound(String.valueOf(dNetValueDiff),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                 AddCellIntoTable(common.getRound(String.valueOf(dTotalValueDiff),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                 AddCellIntoTable(common.getRound(String.valueOf(dDiffPercentage),2), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);
                 AddCellIntoTable(SLabStatus, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,30f);

		        
		        dGNewNetValue    += dNewNetValue;
		        //dGPrevNetValue   += dPrevNetValue;
		        
		        dGNewTotalValue  += dNewTotalValue;
		        //dGPrevTotalValue += dPrevTotalValue;
		        
                dNewNetValue  = 0;
                dPrevNetValue = 0;
                dNewTotalValue = 0;
                dPrevTotalValue = 0;
                
            }
			
              sStr = sStr + "<tr>";
              sStr = sStr + "<td   align='center' colspan='3'><font color='#CD5C5C' ><b>Grand Total</b></font></td>";
			  for(int n=0;n<iMaxRateRank;n++){
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dGPrevNetValue[n]),2) + "</b></font></td>";
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dGPrevTotalValue[n]),2) + "</b></font></td>";
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
			  }
			  
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dGNewNetValue),2) + "</b></font></td>";
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf(dGNewTotalValue),2) + "</b></font></td>";
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>";
              
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf((dGNewNetValue-dGPrevNetValue[iMaxRateRank-1])),2) + "</b></font></td>";
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b>" + common.getRound(String.valueOf((dGNewTotalValue-dGPrevTotalValue[iMaxRateRank-1])),2) + "</b></font></td>";
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>";
              sStr = sStr + "<td   align='center'><font color='#CD5C5C' ><b></b></font></td>"; 
               
              sStr = sStr + "</tr>";
              sStr = sStr + "</table>";
              sStr = sStr + "</body>";
              sStr = sStr + "</html>";
              
              /*
              System.out.println("iMaxRateRank==>"+iMaxRateRank);
              for(int n=0;n<iMaxRateRank;n++) {
              	System.out.println("dGPrevNetValue==>"+dGPrevNetValue[n]+"==dGPrevTotalValue="+dGPrevTotalValue[n]);
              }
              System.out.println("dGNewNetValue==>"+dGNewNetValue+"=dGNewTotalValue="+dGNewTotalValue);
              System.out.println("Differ net val=>"+String.valueOf((dGNewNetValue-dGPrevNetValue[iMaxRateRank-1])));
              System.out.println("Differ total val==>"+String.valueOf((dGNewTotalValue-dGPrevTotalValue[iMaxRateRank-1])));
              */
              
//Pdf Table Grand Total
				//System.out.println("Grant Total start");
				
				AddCellIntoTable("Grand Total", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 3 , 4, 1, 8, 2, mediumbold1,30f);
                
                for(int n=0;n<iMaxRateRank;n++){                  
                	AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 3 , 4, 1, 8, 2, mediumbold1,30f);
                	AddCellIntoTable(common.getRound(String.valueOf((dGPrevNetValue[n])),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);                  
                	AddCellIntoTable(common.getRound(String.valueOf((dGPrevTotalValue[n])),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);			
				}
				
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 3 , 4, 1, 8, 2, mediumbold1,30f);                  
                AddCellIntoTable(common.getRound(String.valueOf(dGNewNetValue),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);                  
                AddCellIntoTable(common.getRound(String.valueOf(dGNewTotalValue),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);		
                
                
             	AddCellIntoTable(common.getRound(String.valueOf((dGNewNetValue-(dGPrevNetValue[iMaxRateRank-1]))),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);                  
             	AddCellIntoTable(common.getRound(String.valueOf((dGNewTotalValue-(dGPrevTotalValue[iMaxRateRank-1]))),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);                  
             	AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);                     
             	AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold1,30f);                     
             
             //System.out.println("Grant Total before add table");
             
             
             AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns , 0, 0, 0, 0, bigbold,30f);
             
             AddCellIntoTable("GST % will be extra, and GST claimed at our side", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns , 0, 0, 0, 0, mediumbold1, 30f);
             
             document.add(table);
             document.close();
             
             //System.out.println("Grant Total end");
			
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return sStr;
	}
	
	private int getMaximumRateRank() {
		int max = 0;
		
		for(java.util.HashMap theMap : theDataList) {
			
			if(max==0) {
				max = common.toInt(common.parseNull((String)theMap.get("RATERANK")));
			}
			
			if(common.toInt(common.parseNull((String)theMap.get("RATERANK")))!=0 && common.toInt(common.parseNull((String)theMap.get("RATERANK")))>max) {
				max = common.toInt(common.parseNull((String)theMap.get("RATERANK")));
			}
			
		}
		
		if(max>0) {
			return (max-1);
		}else{
			return 1;
		}
		
	}
	
	
	private String getMinimumDate(int iRateRank) {
		int min = 0;
		
		iRateRank += 1;
		
		for(java.util.HashMap theMap : theDataList) {
			
			if(common.toInt(common.parseNull((String)theMap.get("PRICEREVISIONSTATUS")))==1 && common.toInt(common.parseNull((String)theMap.get("RATERANK")))==iRateRank){
			
				if(min==0){
					min = common.toInt(common.parseNull((String)theMap.get("NEWRATEDATE")));
				}
			
				if(common.toInt(common.parseNull((String)theMap.get("NEWRATEDATE")))!=0 && common.toInt(common.parseNull((String)theMap.get("NEWRATEDATE")))>min) {
				//if(common.toInt(common.parseNull((String)theMap.get("NEWRATEDATE")))!=0 && common.toInt(common.parseNull((String)theMap.get("NEWRATEDATE")))<min) {
					min = common.toInt(common.parseNull((String)theMap.get("NEWRATEDATE")));
				}	
			
			}
		}	
		
		return String.valueOf(min);
	}

	private String getMaximumDate() {
		int max = 0;
		
		for(java.util.HashMap theMap : theDataList) {
			
			if(common.toInt(common.parseNull((String)theMap.get("PRICEREVISIONSTATUS")))==0) {
			
				if(max==0) {
					max = common.toInt(common.parseNull((String)theMap.get("NEWRATEDATE")));
				}
			
				if(common.toInt(common.parseNull((String)theMap.get("NEWRATEDATE")))!=0 && common.toInt(common.parseNull((String)theMap.get("NEWRATEDATE")))>max) {
					max = common.toInt(common.parseNull((String)theMap.get("NEWRATEDATE")));
				}
			}
		}
		
		return String.valueOf(max);
	}

	
	private java.util.List getPriceList(String SItemCode, int iMaxRateRank) {
		java.util.List thePriceList = new java.util.ArrayList();
		
		int iSize = theDataList.size();
		
		boolean boldrate = false, bnewrate = false;
		
		int iRateRank = iMaxRateRank+1;		//Old Rank + New Rank
		
		for(int i=0;i<iSize;i++){
			java.util.HashMap theMap = (java.util.HashMap)theDataList.get(i);
			
			if(SItemCode.equals(common.parseNull((String)theMap.get("ITEMCODE"))) && common.toInt((String)theMap.get("PURSTATUS"))==0) {
				
				if(common.toInt((String)theMap.get("RATERANK"))<=iRateRank) {	//common.toInt((String)theMap.get("PRICEREVISIONSTATUS"))  = 0 or 1
					thePriceList.add(theMap);
				}
			}
		}
		
		//System.out.println("Actual thePriceList size==>"+thePriceList.size());
		
		if(thePriceList.size()<iRateRank) {
			
			java.util.List theTempList = new java.util.ArrayList();
			
			for(int t=0;t<thePriceList.size();t++){
				java.util.HashMap theMap = (java.util.HashMap)thePriceList.get(t);
				
				theTempList.add(theMap);
			}
			
			thePriceList.clear();
			thePriceList = new java.util.ArrayList();
			
			int iTemp = iRateRank-theTempList.size();
			
			for(int j=0;j<iTemp;j++){
				java.util.HashMap theMap = new java.util.HashMap();
				
				theMap.put("RATERANK", String.valueOf((iRateRank-j)));
				
				if(j==(iRateRank-1) && theTempList.size()==0){
					theMap.put("PRICEREVISIONSTATUS", "0");
				}else{
					theMap.put("PRICEREVISIONSTATUS", "1");
				}
				
				thePriceList.add(theMap);
			}
			
			for(int x=0;x<theTempList.size();x++) {
				java.util.HashMap theMap = (java.util.HashMap)theTempList.get(x);
				
				thePriceList.add(theMap);
			}
			
			//System.out.println("Modified thePriceList size==>"+thePriceList.size());
		}
		
		return thePriceList;
	}	
	

  private String getConsumption(String sDyesCode){
        String SConsumption = "";
        
        for(int i=0;i<theDataList.size();i++) {
            java.util.HashMap  theMap     = (java.util.HashMap)theDataList.get(i);
            
            String sItemCode    = (String)theMap.get("ITEMCODE");
            
            if(common.parseNull((String)theMap.get("ITEMCODE")).equals(sDyesCode.trim())) {
            	int iPurStatus      = common.toInt((String)theMap.get("PURSTATUS"));
            	
            	if(iPurStatus==1 && common.toDouble((String)theMap.get("CONSUMPTION"))>0){
            		return  common.parseNull((String)theMap.get("CONSUMPTION"));
             	} else if(iPurStatus==0 && common.toDouble((String)theMap.get("CONSUMPTION"))>0){
            		return  common.parseNull((String)theMap.get("CONSUMPTION"));
              	}
            }
        }
        
        return SConsumption;
    }

	public java.util.List getSelectedDyes() {
    	java.util.List list = new java.util.ArrayList();
    	
    	DefaultListModel dlm = (DefaultListModel) theItemList.getModel();
    	int iSize = dlm.size();
    	
    	for (int i = 0; i < iSize; i++) {
      		Object obj = dlm.getElementAt(i);
      	
    		CheckBoxItem chkitem = (CheckBoxItem) obj;
        		
    		if (chkitem.isSelected()) {
      			list.add(obj.toString());
    		}
    	}  
    	
    	return list;  
	}


    public void setData(String SupCode){
         theDataList = new java.util.ArrayList<java.util.HashMap>();
         
     try{    
         String sServerDate         = common.getServerDate();
         String sCurDate            = sServerDate.substring(6,8);
         String sCurMonth           = sServerDate.substring(4,6);
         String sCurYear            = sServerDate.substring(0, 4);

         int    iStYear             = common.toInt(sCurYear)-1;
         String sStDate             = String.valueOf(iStYear)+sCurMonth+sCurDate;
        
         String SNoofOldRate        = ((String)JOldrate.getSelectedItem());
         
      	 int iWithZero = -1;
          
	         if(priceRevisionEntryFrameNew.rbtnWithoutZero.isSelected()){
      		 	iWithZero = 1;
      		 }
             
            String SDyesCodes = "("   ;
            
            java.util.List theList = getSelectedDyes();
            
            for(int n=0;n<theList.size();n++){
                
                if(n==0){
                   SDyesCodes += "'"+getItemCode(theList.get(n).toString())+"' ";
                }else{
                   SDyesCodes += ",'"+getItemCode(theList.get(n).toString())+"' ";
                }
            }
            
            SDyesCodes += ")";
             
         StringBuffer sb   = new StringBuffer();
         sb.append(" Select sum(ID) as ID ,sum(Rate) as Rate,sum(Disper) as DisPer,sum(Taxper) as TaxPer,sum(IssueQty) as Consumption, ");
         sb.append(" ItemName,ItemCode,  PurStatus,PriceRevisionStatus,NewRateDate, WithEffectTo,RateRank ,Labstatus,sum(CGSTPer) as CGSTPer, ");
         sb.append(" sum(CGSTAmount) as CGSTAmount, sum(SGSTPer) as SGSTPer,sum(SGSTAmount) as SGSTAmount, sum(IGSTPer) as IGSTPer,sum(IGSTAmount) as IGSTAmount ");
         sb.append(" from ( ");
         
         
         sb.append(" Select 0 as ID, 0 as Rate,0 as DisPer,0 as TaxPer,sum(Issue.Qty) as IssueQty,Item_Name as ItemName,Issue.Code as ItemCode,1 as PurStatus,");
         sb.append(" 2 as PriceRevisionStatus,'' as NewRateDate, 0 as WithEffectTo,0 as RateRank, ''  as Labstatus ,0 as CGSTPer,0 as CGSTAmount,0 as SGSTPer,");
         sb.append(" 0 as SGSTAmount, 0 as IGSTPer, 0 as IGSTAmount   from Issue@amarml "); 
         sb.append(" Inner Join InvItems  on Issue.Code = InvItems.Item_Code ");
         sb.append(" Where  Issue.Code in "+SDyesCodes+" ");
            
            if (iWithZero==1) {
                 sb.append("  and IssueDate>="+sStDate+" and IssueDate<="+sServerDate);
            }
            
         sb.append(" Group by Item_Name,Issue.Code ");

         sb.append(" Union All  ");
  
         sb.append(" Select PurchaseOrder.ID,PurchaseOrder.Rate, PurchaseOrder.Discper, PurchaseOrder.TaxPer, 0 as IssueQty, Item_Name,  PurchaseOrder.Item_Code as ITemCode,");
         sb.append(" 1 as PurStatus ,2 as  PriceRevisionStatus ,  '' as NewRateDate, 0 as WithEffectTo, 0 as RateRank , ''  as Labstatus, 0 as CGSTPer,0 as CGSTAmount,0 as SGSTPer,");
         sb.append(" 0 as SGSTAmount, 0 as IGSTPer, 0 as IGSTAmount ");
         sb.append(" from  PurchaseOrder @amarml ");
         sb.append(" Inner Join InvItems on InvItems.Item_Code = PurchaseOrder.Item_Code  ");
         sb.append(" Where PurchaseOrder.Sup_Code='"+SupCode+"'");
         sb.append(" and PurchaseOrder.ID in (Select Max(ID) from PurchaseOrder @amarml  where SUP_CODE='"+SupCode+"' ");
         sb.append(" and Item_code in "+SDyesCodes+" ");
         sb.append(" Group by Item_Code ");
         sb.append(" ) ");
            
         sb.append(" ) ");
         sb.append(" Group by ItemName,ItemCode,PurStatus,PriceRevisionStatus,NewRateDate,WithEffectTo,RateRank,Labstatus   ");

            
         sb.append(" Union All  ");

         sb.append(" Select ID, RatePerKg, DiscPer, TaxPer, AnnualConsumption, ItemName, ItemCode, PurStatus, PriceRevisionStatus, NewRateDate, WithEffectTo, ");
         sb.append(" RateRank , LabStatus, CGSTPer, CGSTAmount, SGSTPer, SGSTAmount, IGSTPer, IGSTAmount from ( ");
         sb.append(" Select DyesPriceRevision.ID, RatePerKg, DiscPer, TaxPer, 0 as AnnualConsumption,  ");
         sb.append(" decode (Dyes_Item_Name,'',InvItems.ITEM_NAME,Dyes_InvItems.Dyes_Item_Name) as ItemName,DyesPriceRevision.Dyes_Code as ItemCode, ");
         sb.append(" 0 as PurStatus,  DyesPriceRevision.PriceRevisionStatus,to_Char(DyesPriceRevision.WithEffectFrom) as NewRateDate, ");
         sb.append(" DyesPriceRevision.WithEffectTo ,rank() over(partition by DyesPriceRevision.Dyes_Code order by DyesPriceRevision.WithEffectFrom desc) as RateRank, ");
         sb.append(" '' as LabStatus , ");
         sb.append(" DyesPriceRevision.CGSTPer, DyesPriceRevision.CGSTAmount, DyesPriceRevision.SGSTPer, DyesPriceRevision.SGSTAmount, DyesPriceRevision.IGSTPer, ");
         sb.append(" DyesPriceRevision.IGSTAmount  ");
         sb.append(" from DyesPriceRevision  ");
         sb.append(" Left Join Dyes_InvItems on Dyes_InvItems.Dyes_Item_Code = DyesPriceRevision.Dyes_Code  ");
         sb.append(" Left Join InvItems on InvItems.Item_Code = DyesPriceRevision.Dyes_Code  ");
         sb.append(" Where DyesPriceRevision.Supp_Code = '"+SupCode+"'  and DyesPriceRevision.Dyes_Code in "+SDyesCodes+" ");
         sb.append(" Order by DyesPriceRevision.ID asc ");
         sb.append(" ) ");
        
         if(!SNoofOldRate.equals("ALL")){
           sb.append(" Where RateRank<= "+(common.toInt(SNoofOldRate)+1)+" ");
         }
         
         sb.append(" Order by 9 desc, 12 desc, itemname ");
            
       
         //try{
             if(theDyeConnection == null) {
                DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                theDyeConnection = jdbc.getConnection();
             }
             java.sql.Statement 	stat = theDyeConnection.createStatement();
             java.sql.ResultSet 	rst  = stat.executeQuery(sb.toString());
             
             java.sql.ResultSetMetaData rsmd  = rst.getMetaData();
             
             while (rst.next()) {
                 java.util.HashMap theMap = new java.util.HashMap();
                 for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    theMap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                 }
                 
                theDataList.add(theMap);
            }
            rst.close();
            stat.close();
            
        }catch(Exception e){
           e.printStackTrace();
           System.out.println(e);
        }
        
    }
    
    
    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont,float fHeight) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont,float fHeight,float fBorderRight) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorderWidthRight(fBorderRight);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }



//
    public class CheckBoxItem {
        private String label;
        boolean isSelected = false;

        public CheckBoxItem(String label) {
            this.label = label;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean isSelected) {
            this.isSelected = isSelected;
        }

        public String toString() {
            return label;
        }
    }
    
    class CheckListRendererItm extends JCheckBox implements ListCellRenderer {
        public Component getListCellRendererComponent(JList list, Object value, int index,boolean isSelected, boolean hasFocus) {
            setEnabled(list.isEnabled());
            setSelected(((CheckBoxItem) value).isSelected());
            setFont(list.getFont());
            setBackground(list.getBackground());
            setForeground(list.getForeground());
            setText(value.toString());
            return this;
        }
    }
    

}
