
package DyesPriceRevision;

import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
import java.net.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableModel;
import javax.swing.JEditorPane;
import java.text.*;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Font;

import java.io.FileOutputStream;

import guiutil.*;
import jdbc.ORAConnection3;

public class ShadeReceipeCostingReport extends JInternalFrame {
    
    JLayeredPane	        Layer;

    JPanel                  basepanel, toppanel, botpanel;
    
    JButton                 bPrint, bExit;
    
    JEditorPane             pnlEditor;
    
    JList                   theShadeList;
    DefaultListModel        theShadeListModel;
    
	DateField				tFromDate, tToDate;
	    
    String                  sAuthSysName = "";
    
    Vector 				    VShadeBaseCode, VShadeBaseName;
    Vector 				    VShadewiseDyesCode, VShadewiseDyesName, VShadeCode;
    
    java.sql.Connection     theDyeConnection = null;
    java.sql.Connection     theScmConnection = null;
    
    //ArrayList               ADataList       = new ArrayList();
    ArrayList                   AShadeList      = new ArrayList();
    ArrayList                   ACostList		= new ArrayList();
    ArrayList                   AConsumptionList= new ArrayList();
    
    java.util.List              ADataList       = new java.util.ArrayList();
    
    //Pdf
    Document                    document;
    PdfPTable                   table, table1;
    int                         iTotalColumns = 15;
    int                         iColWidth[] ;
    String                      sparty,sfibre, sfindStr="";
    
    PdfPCell c1;
    private static Font bigbold     = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
    private static Font mediumbold  = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
    private static Font smallbold   = FontFactory.getFont("TIMES_ROMAN", 7, Font.BOLD);
    private static Font smallbold1  = FontFactory.getFont("TIMES_ROMAN", 7, Font.BOLD);
    
    Common                     common      = new Common();
    
	public ShadeReceipeCostingReport(JLayeredPane Layer) {
		this.Layer		= Layer;
		
		initValues();
        createComponent();
        setLayout();
        addComponent();
        addListener();

        try {
            sAuthSysName       = InetAddress.getLocalHost().getHostName();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void initValues(){
    
    	setFibreShade();
    	setShadewiseDyes();	
    	
    }
    
	private void createComponent() {
		basepanel= new JPanel();
		toppanel = new JPanel();
		botpanel = new JPanel();
		
        theShadeList            = new JList(theShadeListModel);
        theShadeList            . setVisibleRowCount(4);
        theShadeList        	. setCellRenderer(new CheckListRendererItm());
		theShadeList	        . setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		tFromDate = new DateField();
		tToDate   = new DateField();
		
        pnlEditor = new JEditorPane();
        pnlEditor . setEditable(false);
        
		bPrint    = new JButton("Print"); 
		bExit     = new JButton("Exit"); 
		
	}
	
	private void setLayout() {
        setTitle("SHADE WISE DYES COSTING REPORT");
        setSize(1000, 600);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setMaximizable(true);
        setClosable(true);
        setVisible(true);
	
      	SCLayout  basepanellayout  = new SCLayout(2,SCLayout.FILL,SCLayout.FILL,2);
                  basepanellayout  . setScale(0,0.25);
                  basepanellayout  . setScale(1,0.75);

    	basepanel.setLayout(basepanellayout);
		
		toppanel.setLayout(new GridBagLayout());
		botpanel.setLayout(new BorderLayout());
		
		toppanel.setBorder(new TitledBorder(""));
		botpanel.setBorder(new TitledBorder("Report"));
	}
	
	private void addComponent() {
	
        GridBagConstraints c    = new GridBagConstraints();
        c.fill                  = GridBagConstraints.HORIZONTAL;

        c.gridx                 = 0;
        c.gridy                 = 0;
        c.weightx               = 2;
        c.weighty               = 2;

		JScrollPane shadeScrollpane = new JScrollPane(theShadeList);
					shadeScrollpane . setBorder(BorderFactory.createTitledBorder("Shade"));

        c.gridx                 = 1;
        c.gridy                 = 0;
        c.gridheight            = 3;
        toppanel                .add(shadeScrollpane, c);
		
        c.gridx                 = 3;
        c.gridy                 = 1;
        c.gridheight            = 1;
		toppanel                .add(new JLabel("From Date"), c);

        c.gridx                 = 3;
        c.gridy                 = 2;
		toppanel                .add(new JLabel("To Date"), c);
		

        c.gridx                 = 4;
        c.gridy                 = 1;
		toppanel                .add(tFromDate, c);

        c.gridx                 = 4;
        c.gridy                 = 2;
		toppanel                .add(tToDate, c);
		
        c.gridx                 = 5;
        c.gridy                 = 2;
		toppanel                .add(bPrint, c);
		
		botpanel.add(new JScrollPane(pnlEditor));
		
		basepanel.add(toppanel);
		basepanel.add(botpanel);
		
		getContentPane().add("Center",basepanel);
	}
	
	private void addListener() {
	
        bPrint . addActionListener(new ActList());
        bExit  . addActionListener(new ActList());
	
        theShadeList.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
                
                JList jlist = (JList) event.getSource();
                
                int index = jlist.locationToIndex(event.getPoint());
                
                CheckBoxItem Citem = (CheckBoxItem) jlist.getModel().getElementAt(index);
                
                Citem.setSelected(!Citem.isSelected());
                
                jlist.repaint(jlist.getCellBounds(index, index));
            }
        });
	
	
	}
    

    private class ActList implements ActionListener   {
        public void actionPerformed(ActionEvent ae)  {
            
            if (ae.getSource()==bPrint) {
                SetDataIntoVector();
                SetConsumption();
                SetReport();
                //CreatePDFFile();
			}
            
            if (ae.getSource()==bExit) {
			
			}
			
		}
	}
	
  	public void SetReport(){
      	try{
            String sReport = "";
            sReport = getReport();
            pnlEditor.setContentType("text/html");
            pnlEditor.setText(sReport);
            
        }catch(Exception e){
           e.printStackTrace();
        }
 	}
	
  	private String getReport(){
            String sStr = "";
            String sHeading ="Shade Cost Report";
    	
    	try{
    		int NoofReceipe = getMaxReceipeNo();
    	
    	//Pdf
            String SPDFFile = "";
           
            String Sos = System.getProperty("os.name").toLowerCase();
            if(Sos.trim().startsWith("wind")){
            	SPDFFile = "D:/ShadeCost.pdf";
            }else{
            	SPDFFile = "/ShadeCost.pdf";
            }
           	
           	DecimalFormat   df  = new DecimalFormat("0.000");
           	
            document        = new Document();
            PdfWriter       .getInstance(document, new FileOutputStream(SPDFFile));
            document        .setPageSize(PageSize.LEGAL.rotate());
            //document        .setPageSize(PageSize.A4.rotate());
            document        .open();
            
            int iColSize	= 4*NoofReceipe;
          
            iTotalColumns  = iColSize+8;
             table         = new PdfPTable(iTotalColumns);
             iColWidth     = new int[iTotalColumns];
             
             iColWidth[0] = 20;
             iColWidth[1] = 40;
             iColWidth[2] = 40;
             iColWidth[3] = 40;
             iColWidth[4] = 40;
             iColWidth[5] = 40;
             
             for (int n = 0; n<NoofReceipe; n++) {
             	  iColWidth[(6+(n*4))] = 100;
             	  
             	  iColWidth[(7+(n*4))] = 40;
             	  iColWidth[(8+(n*4))] = 40;
             	  iColWidth[(9+(n*4))] = 40;
             }
             
             iColWidth[iTotalColumns-2] = 30;
             iColWidth[iTotalColumns-1] = 30;
             
             /*
            int iDiffColSize = 6+iColSize;
            iColWidth[iDiffColSize+0] = 17;
            iColWidth[iDiffColSize+1] = 15;
            */
            table.setWidths(iColWidth);
            table.setWidthPercentage(100f);
            table.setHeaderRows(3);
            
           String sDate= common.getServerDate();
    	
            sStr = "<html><head>";
            sStr =sStr + "  </head>";
            sStr =sStr + "  <body>";
            sStr =sStr + "  <table border='1' width='100%' >";
            sStr =sStr + "  <tr  bgcolor='lightgreen'> ";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>SL NO </b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Shade Code</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Consumption</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Appd Rate</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Lab Dye Cost</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Lab Chem Cost</b></font></td>";
            //sStr =sStr + "  <td align='center' color='#FF00FF' colspan='4'><font color='#000099' ><b>Old Recipe</b></font></td>";
            for(int n=0;n<NoofReceipe;n++) {
            sStr =sStr + "  <td align='center' color='#FF00FF' colspan='4'><font color='#000099' ><b>Recipe No : "+(n+1)+"</b></font></td>";
            }
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Difference</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Lab Status </b></font></td>";
            sStr =sStr +"  </tr> ";
            
            sStr =sStr +"  <tr> ";
            for(int n=0;n<NoofReceipe;n++) {
		        sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Dyes Name</b></font></td>"; 
		        sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>%</b></font></td>"; 
		        sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Rate / kg</b></font></td>";
		        sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Cost</b></font></td>"; 
            }
            sStr =sStr +"  </tr> ";
        
//Pdf Head
            AddCellIntoTable("Shade Cost Report "+common.parseDate(sDate)+"", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns , 0, 0, 0, 0, bigbold);
            
            AddCellIntoTable("Sl.No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Shade Code", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Consumption", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Appd Rate", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Lab Dye Cost", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Lab Chem Cost", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            for(int n=0;n<NoofReceipe;n++) {
            AddCellIntoTable(" Recipe No : "+(n+1), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 4 , 4, 1, 8, 2, mediumbold,1);
            }
            AddCellIntoTable("Difference", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Lab Status", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            
            
            for(int n=0;n<NoofReceipe;n++) {
		        AddCellIntoTable("Dyes Name", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
		        AddCellIntoTable("%", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
		        AddCellIntoTable("Rate", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
		        AddCellIntoTable("Cost", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
        	}
        
        
             
		 // Body of the String
		    String sLabStatus="";
		    
		    java.util.List[] theReceipeList = null;
				  		     theReceipeList  = new java.util.ArrayList[NoofReceipe];
			
			double[]  dTotalCost = null; 
				  	  dTotalCost = new double[NoofReceipe];
			
			String[]  SLabStatus = null;
				  	  SLabStatus = new String[NoofReceipe];
							  	  
		 	for(int i=0;i<AShadeList.size();i++){
		      	String sShadeCode=String.valueOf(AShadeList.get(i));
		      
		      	SetCost(sShadeCode);
		      
		      	int iCostCount   = ACostList.size();
		      	sLabStatus       = getLabStatus(sShadeCode);
		       
				theReceipeList   = new java.util.ArrayList[NoofReceipe];
				int iRowSpan     = 0;
				
				for(int n=0;n<NoofReceipe;n++) {
					theReceipeList[n] = getDyesNewRateList(sShadeCode, (n+1));
					
					dTotalCost[n] = 0;
					SLabStatus[n] = "";
					
					if(theReceipeList[n].size()>iRowSpan) {
						iRowSpan = theReceipeList[n].size();
					}
				}
		       
		     	sStr =sStr +"    <tr   bgcolor='pink'> ";   
		     	sStr =sStr +"    <td   align='center' rowspan='"+iRowSpan+"' ><font color='#000099' ><b>"+(i+1)+"</b></font></td>";
		     	sStr =sStr +"    <td   align='left'   rowspan='"+iRowSpan+"' ><font color='#000099' ><b>"+sShadeCode +"</b></font></td>";
		     	sStr =sStr +"    <td   align='left'   rowspan='"+iRowSpan+"' ><font color='#000099' ><b>"+getConsumption(sShadeCode) +"</b></font></td>";
       
//Pdf Body       
		       AddCellIntoTable(String.valueOf(i+1), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iRowSpan);
		       AddCellIntoTable(sShadeCode, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iRowSpan);
		       AddCellIntoTable(String.valueOf(getConsumption(sShadeCode)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iRowSpan);
       
			   if(iCostCount>0) {
				   
				 double dAppRate        = getAppRate(sShadeCode);
				 double dLabDyeCost     = getLabDyeCost(sShadeCode);
				 double dLabChemCost    = getLabChemCost(sShadeCode);
				 
				 sStr = sStr +" <td align='center' rowspan='"+iRowSpan+"'><font color='#000099' ><b>"+dAppRate+"</b></font></td>";
				 sStr = sStr +" <td align='center' rowspan='"+iRowSpan+"'><font color='#000099' ><b>"+dLabDyeCost+"</b></font></td>";
				 sStr = sStr +" <td align='center' rowspan='"+iRowSpan+"'><font color='#000099' ><b>"+dLabChemCost+"</b></font></td>";
				 
	       AddCellIntoTable(String.valueOf(String.valueOf(dAppRate)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iRowSpan);
	       AddCellIntoTable(String.valueOf(String.valueOf(dLabDyeCost)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iRowSpan);
	       AddCellIntoTable(String.valueOf(String.valueOf(dLabChemCost)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iRowSpan);
				 
			   }else {
				 sStr = sStr +" <td align='center' rowspan='"+iRowSpan+"'><font color='#000099' ><b></b></font></td>";
				 sStr = sStr +" <td align='center' rowspan='"+iRowSpan+"'><font color='#000099' ><b></b></font></td>";
				 sStr = sStr +" <td align='center' rowspan='"+iRowSpan+"'><font color='#000099' ><b></b></font></td>";
				 
				 AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iRowSpan);
				 AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iRowSpan);
				 AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iRowSpan);
				 
			   	 //break;
			   }
				
				
				for(int m=0;m<iRowSpan;m++) {  //Rows
					
					for(int n=0;n<NoofReceipe;n++) {	//Cols
						
						 if(m<theReceipeList[n].size()) {
						 	java.util.HashMap theMap = (java.util.HashMap) theReceipeList[n].get(m);
						 	
							 sStr =sStr +"    <td   align='left'><font color='#000099'   ><b>"+common.parseNull((String)theMap.get("ITEM_NAME"))+"</b></font></td>";
							 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.parseNull((String)theMap.get("DYES_PER"))+"</b></font></td>";
							 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.parseNull((String)theMap.get("DYES_RATE"))+"</b></font></td>";
							 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.parseNull((String)theMap.get("DYES_COST"))+"</b></font></td>";
							 
		AddCellIntoTable(common.parseNull((String)theMap.get("ITEM_NAME")), table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(df.format(Double.parseDouble(common.parseNull((String)theMap.get("DYES_PER")))), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(df.format(Double.parseDouble(common.parseNull((String)theMap.get("DYES_RATE")))), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(df.format(Double.parseDouble(common.parseNull((String)theMap.get("DYES_COST")))), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
							 
							 dTotalCost[n] += common.toDouble(common.parseNull((String)theMap.get("DYES_COST")));
							 SLabStatus[n]  = common.parseNull((String)theMap.get("LABSTATUS"));
						 }else{
							 sStr =sStr +"    <td   align='left'><font color='#000099'   ><b>&nbsp</b></font></td>";
							 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
							 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
							 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
							 
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
							 
						 }
					}
					
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
				
					sStr = sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp;</b></font></td>";
					sStr = sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp;</b></font></td>";
					sStr = sStr +"  </tr> "; 	
				}
				
				 
				 
				 sStr =sStr +"  <tr> "; 
				 sStr =sStr +"    <td   align='center' colspan='6'><font color='#000099' ><b>SHADE WISE TOTAL</b></font></td>";
				 
                AddCellIntoTable("SHADE WISE TOTAL", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 6 , 4, 1, 8, 2, mediumbold,1);
				 
				 double dMaxCost = 0;
				 double dMinCost = 0;
				 
				 for(int n=0;n<NoofReceipe;n++) {
					 
					 if(common.parseNull(SLabStatus[n]).equals("OK")){
					 sStr =sStr +"    <td   align='center'><font color='green'><b>"+common.parseNull(SLabStatus[n])+"</b></font></td>";
					 sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
					 sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
					 sStr =sStr +"    <td   align='center'><font color='green' ><b>"+common.getRound(dTotalCost[n],2)+"</b></font></td>";
					 }else{
					 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.parseNull(SLabStatus[n])+"</b></font></td>";
					 sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
					 sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
					 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.getRound(dTotalCost[n],2)+"</b></font></td>";
					 }
					 if(n==0){
					 	dMinCost = common.toDouble(common.getRound(dTotalCost[n],2));
					 	dMaxCost = common.toDouble(common.getRound(dTotalCost[n],2));
					 }
					 
					 if(common.toDouble(common.getRound(dTotalCost[n],2))>0 && common.toDouble(common.getRound(dTotalCost[n],2))<dMinCost){
					 	dMinCost = common.toDouble(common.getRound(dTotalCost[n],2));
					 }
					 
					 if(common.toDouble(common.getRound(dTotalCost[n],2))>0 && dMaxCost>common.toDouble(common.getRound(dTotalCost[n],2))) {
					 	dMaxCost = common.toDouble(common.getRound(dTotalCost[n],2));
					 }
					 
                AddCellIntoTable(common.parseNull(SLabStatus[n]), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(common.getRound(String.valueOf(dTotalCost[n]),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
					 
				 }
				 
				  double[] dArr = dTotalCost;
				  java.util.Arrays.sort(dArr);
				  
				  for (double number : dArr) {
				  	
			         if(number>dMinCost){
			         	dMaxCost = number;
			         	break;
			         }
			      }

				 //sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.getRound(dTotalCost-dNewTotalCost,2)+"</b></font></td>";
				 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp;"+common.getRound((dMaxCost-dMinCost),2)+"</b></font></td>";
				 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp;</b></font></td>";
				 sStr =sStr +"    </tr> "; 
				 
				AddCellIntoTable(common.getRound((dMaxCost-dMinCost),2), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
				AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);				 
				 
			 }
        
       		try{
			 	document.add(table);
			 	document.close();
			 }catch(Exception e){
			 }
        
    	}catch (Exception ex) {
         	ex.printStackTrace();
     	}
     
 		return sStr;
    }
	
	
	public int  getMaxReceipeNo() {
      	int iMax = 1;
      	
        for (int i = 0; i < ADataList.size(); i++) {
            HashMap themap   = (HashMap) ADataList.get(i);
           
           if(i==0) {
           	  iMax = common.toInt((String) themap.get("OLDRECIPENO"));
           }
           
           if(common.toInt((String) themap.get("OLDRECIPENO"))>iMax) {
           	  iMax = common.toInt((String) themap.get("OLDRECIPENO"));
           }
		}
		
        return iMax;
	}	
	
	

	public ArrayList  getDyesNewRateList( String sShadeCode, int iReceipeNo){
    
        ArrayList theList   = null;
                  theList   = new ArrayList();
                  theList   .clear();
       
        for (int i = 0; i < ADataList.size(); i++) {
            HashMap themap   = (HashMap) ADataList.get(i);

           String sItemName  = ((String) themap.get("ITEM_NAME"));
           String sDyesPer   = ((String) themap.get("DYES_PER"));
           String sShade     = ((String) themap.get("SHADE_CODE"));
           String sDyesRate  = ((String) themap.get("DYES_RATE"));
           String sDyesCost  = ((String) themap.get("DYES_COST"));
           String sDyesCode  = ((String) themap.get("DYES_CODE"));
           String sPriceRevisionStatus  = ((String) themap.get("RECIPESTATUS"));
          // String sPriceRevisionStatus  = ((String) themap.get("PRICEREVISIONSTATUS"));
          
            if(sShadeCode.equals(sShade) && sPriceRevisionStatus.trim().equals("0") && iReceipeNo==common.toInt((String) themap.get("OLDRECIPENO"))) {
           		theList.add(themap);
            }
        }
        
        return theList;
	}
	
	public String getLabStatus( String sShadeCode){
       String sLabStatus="";  
        
        for (int i = 0; i < ADataList.size(); i++) {
          HashMap themap         = (HashMap) ADataList.get(i);
            String sshade        = ((String) themap.get("SHADE_CODE"));
          
          //  String sPriceRevisionStatus  = ((String) themap.get("PRICEREVISIONSTATUS"));
            String sPriceRevisionStatus  = ((String) themap.get("RECIPESTATUS"));
            
            if (sShadeCode.equals(sshade) && sPriceRevisionStatus.equals("0"))  {
                    sLabStatus   = ((String) themap.get("LABSTATUS"));
          	}
        }
        
        return sLabStatus;
	}
		
	
    private double getConsumption(String sShadeCode ){
     	double dConsumption = 0.0;
     	
        for (int i = 0; i < AConsumptionList.size(); i++) {
            HashMap themap = (HashMap) AConsumptionList.get(i);

           String sshade = ((String) themap.get("BASECODE"));
           
           if (sShadeCode.equals(sshade))  {
             	dConsumption = (common.toDouble(common.parseNull((String)themap.get("CONSUMPTION"))));
         		break;
           }
        }
    
     	return dConsumption;
 	}     
	
   public void SetConsumption() 
    {
        
        AConsumptionList = new ArrayList();
        AConsumptionList.clear();
        
    try{
        String sServerDate         = common.getServerDate();
        String sCurDate            = sServerDate.substring(6,8);
        String sCurMonth           = sServerDate.substring(4,6);
        String sCurYear            = sServerDate.substring(0, 4);
        
         int    iStYear            = common.toInt(sCurYear)-1;
         String sStDate            = String.valueOf(iStYear)+sCurMonth+sCurDate;
        
        StringBuffer sb = new StringBuffer();
        sb.append(" select sum(netweight) as Consumption,fibrebase.basecode,basename from receipt ");
        sb.append(" inner join fibre on fibre.fibrecode = receipt.fibrecode ");
        sb.append(" inner join fibrebase on fibrebase.basecode = fibre.basecode ");
        sb.append(" where godowndate>="+sStDate+" and godowndate<="+sServerDate+" and receipttype=8 and Accode='100059' ");
        sb.append(" group by fibrebase.basecode,basename Order by 2 ");
        
         
        
            if (theScmConnection == null) {
                ORAConnection3 jdbc = ORAConnection3.getORAConnection();
				theScmConnection       = jdbc.getConnection();
            }
        	java.sql.PreparedStatement pst = theScmConnection.prepareStatement(sb.toString());
        	java.sql.ResultSet   rst = pst . executeQuery();
            
            java.sql.ResultSetMetaData rsmd = rst.getMetaData();
            
            while (rst.next()) {
                HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
                
                AConsumptionList.add(themap);
            }
            rst.close();
            pst.close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
	
    private double getCost(String SParty,String sShadeCode ){
     	double dcost=0.0;
     	
        for (int i = 0; i < ADataList.size(); i++) {
            HashMap themap = (HashMap) ADataList.get(i);

           String sPartyName = ((String) themap.get("NAME"));
           String sshade = ((String) themap.get("SHADE_CODE"));
           
           if ((SParty.equals(sPartyName)) && sShadeCode.equals(sshade))  {
             	dcost+=(common.toDouble(common.parseNull((String)themap.get("DYES_COST"))));
           }
        }
    
     	return dcost;
 	}     
      
 
   public void SetCost(String sShadeCode) 
    {
        StringBuffer sb = new StringBuffer();
        
        ACostList = new ArrayList();
        ACostList.clear();
       
        sb.append(" Select count(1), OrderRate,LabDyesCost,LabChemCost,FibreCode from FibreLotCost ");
        sb.append(" inner join dyeingorder on dyeingorder.id = fibrelotcost.orderid   ");
        sb.append(" Where FibreLotCost.FibreCode = '"+sShadeCode+"'" );
        sb.append(" and OrderID = (Select Max(OrderID) from FibreLotCost Where FibreCode = '"+sShadeCode+"' )");
        sb.append(" Group by OrderRate,LabDyesCost,LabChemCost,FibreCode ");
        
         
        try{
            if (theDyeConnection == null) {
                DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                theDyeConnection       = jdbc.getConnection();
            }
        	java.sql.PreparedStatement pst = theDyeConnection.prepareStatement(sb.toString());
        	java.sql.ResultSet   rst = pst . executeQuery();
            
            java.sql.ResultSetMetaData rsmd = rst.getMetaData();
            
            while (rst.next()) {
                HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
                ACostList.add(themap);
            }
            rst.close();
            pst.close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
   
 	private double getAppRate(String sShadeCode){
     	double dValue = 0.00;
     	
        for(int i=0;i<ACostList.size();i++){
         	java.util.HashMap theMap = (java.util.HashMap) ACostList.get(i);
         	
         	String sShCode = (String)theMap.get("FIBRECODE");
         	
         	if(sShadeCode.trim().equals(sShCode)){
                dValue   = common.toDouble(String.valueOf(theMap.get("ORDERRATE")));
         	}
        }
        
        return dValue;
    }

  private double getLabDyeCost(String sShadeCode){
     double dValue = 0.00;

       for(int i=0;i<ACostList.size();i++){
         java.util.HashMap theMap = (java.util.HashMap) ACostList.get(i);
         String sShCode           = (String)theMap.get("FIBRECODE");
         if(sShadeCode.trim().equals(sShCode)){
                        dValue    = common.toDouble(String.valueOf(theMap.get("LABDYESCOST")));
         }
       }
       return dValue;
   }
  
  private double getLabChemCost(String sShadeCode){
     double dValue = 0.00;
       for(int i=0;i<ACostList.size();i++){
         java.util.HashMap theMap = (java.util.HashMap) ACostList.get(i);
         String sShCode           = (String)theMap.get("FIBRECODE");
         if(sShadeCode.trim().equals(sShCode)){
                        dValue    = common.toDouble(String.valueOf(theMap.get("LABCHEMCOST")));
         }
       }
       return dValue;
   }
	
	
  public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
// heading
    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }
    
    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont,int iRowspan,float fHeight) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowspan);
        //c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
	
	
    public class CheckBoxItem {

        private String label;
        boolean isSelected = false;

        public CheckBoxItem(String label) {
            this.label = label;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean isSelected) {
            this.isSelected = isSelected;
        }

        public String toString() {
            return label;
        }
    }
	
	
    class CheckListRendererItm extends JCheckBox implements ListCellRenderer {

        public Component getListCellRendererComponent(JList list, Object value, int index,boolean isSelected, boolean hasFocus) {
            
            setEnabled(list.isEnabled());
            setSelected(((CheckBoxItem) value).isSelected());
            setFont(list.getFont());
            setBackground(list.getBackground());
            setForeground(list.getForeground());
            setText(value.toString());
            
            return this;
        }
    }
	
	
	
	public void setFibreShade(){
	    theShadeListModel       = new DefaultListModel();
        VShadeBaseCode          = new Vector();
        VShadeBaseName          = new Vector();
        
        StringBuffer sb = new StringBuffer(); 
					 sb . append("  Select Distinct Shade_Code, BaseName from ShadewiseDyes ");
					 sb . append("  Inner Join FibreBase on ShadeWiseDyes.Shade_Code = FibreBase.BaseCode ");
					 sb . append("  Order by 1 ");
        
        try{
			if (theDyeConnection==null) {
		      	DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
			    theDyeConnection       = jdbc.getConnection();
			}
			java.sql.PreparedStatement ps = theDyeConnection.prepareStatement(sb.toString());
			java.sql.ResultSet rst        = ps.executeQuery();
			
			while (rst.next()){
			  	VShadeBaseCode    . add(rst.getString(1));
				VShadeBaseName    . add(rst.getString(2));
				
				theShadeListModel . addElement(new CheckBoxItem(rst.getString(1)));
			 }
			 rst.close();
			 ps.close();
			 ps = null;
         
		}catch(Exception e){
		    e.printStackTrace();
		}
	}
	
	public void setShadewiseDyes() {
        VShadewiseDyesCode  = new Vector();
        VShadewiseDyesName  = new Vector();
        VShadeCode          = new Vector();
        
        StringBuffer sb = new StringBuffer(); 
					 sb . append("  Select Dyes_Code as DyesCode,Item_Name as ItemName, Shade_Code  from ShadewiseDyes ");
					 sb . append("  Inner Join InvItems on ShadewiseDyes.Dyes_Code = InvItems.Item_Code ");
					 sb . append("  Where PriceRevisionStatus = 0 and RecipeStatus = 1 ");
					 sb . append("  Union All ");
					 sb . append("  Select Dyes_Code as DyesCode ,Dyes_Item_Name as ItemName, Shade_Code from ShadewiseDyes ");
					 sb . append("  Inner Join Dyes_InvItems on ShadewiseDyes.Dyes_Code = Dyes_InvItems.Dyes_Item_Code ");
					 sb . append("  Where PriceRevisionStatus = 0 and RecipeStatus = 1 ");
					 sb . append("  Order by 2");
        
        
		try{
		    if(theDyeConnection==null) {
				DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
				theDyeConnection       = jdbc.getConnection();
		    }
		    java.sql.PreparedStatement ps = theDyeConnection.prepareStatement(sb.toString());
		    java.sql.ResultSet rst        = ps.executeQuery();
		       
		    while (rst.next()){
		          VShadewiseDyesCode     . add(rst.getString(1));
		          VShadewiseDyesName     . add(rst.getString(2));
		          VShadeCode             . add(rst.getString(3));
		    }
		    rst.close();
		    ps.close();
		    ps=null;
            
		}catch(Exception e){
            e.printStackTrace();
	    }
	    
	}
	
	
	public java.util.ArrayList getSelectedShades() {
    	java.util.ArrayList list = new java.util.ArrayList();
    	
    	DefaultListModel dlm = (DefaultListModel) theShadeList.getModel();
    	
    	int iSize = dlm.size();
    	
    	for (int i = 0; i < iSize; i++) {
      		Object obj = dlm.getElementAt(i);
      	
    		CheckBoxItem chkitem = (CheckBoxItem) obj;
        		
    		if(chkitem.isSelected()) {
      			list.add(obj.toString());
    		}
    	}
    	/*
    	if(list.size()==0){
    		
    		for(int j=0;j<ADataList.size();j++){
    			HashMap theMap = (HashMap)ADataList.get(j);
    			
    			if(!list.contains(common.parseNull((String)theMap.get("SHADE_CODE")))) {
    				list.add(common.parseNull((String)theMap.get("SHADE_CODE")));
    			}
    		}
    		
    	}
    	*/
    	return list;  
    }
	
    public void SetDataIntoVector() {
        StringBuffer sb            = new StringBuffer();
        
        ADataList                  = new ArrayList();
        ADataList                  . clear();
        
        String sFromDate           = tFromDate.toNormal();
        String sToDate             = tToDate.toNormal();
		
		AShadeList         = new ArrayList();
		AShadeList	   = getSelectedShades();
		
		String SQs = "";
		
            for(int i=0;i<AShadeList.size();i++){
                if(i==0){
               	    SQs += " and shade_code in ('"+(String)AShadeList.get(i)+"'";
                }else{
               	    SQs += ",'"+(String)AShadeList.get(i)+"'";
                }
            }
            
            if(SQs.length()>0){
		SQs += ")";
	    }
			
            System.out.println(AShadeList.size()+"<==>"+SQs);


            sb.append(" Select shade_code,item_name,Dyes_per,Dyes_Rate,Dyes_cost,name,PriceRevisionStatus,");
            sb.append(" decode(LabStatus,1,'OK',2,'NOTOK',0,'NOTMADE') as LabStatus , RecipeStatus, OLDRECIPENO from SHADEWISEDYES ");
            sb.append(" inner join  invitems on invitems.ITEM_CODE =SHADEWISEDYES.DYES_CODE  and SHADEWISEDYES.NEW_RECEIPE = 1 ");
            sb.append(" inner join  stockgroup on groupcode = invitems.STKGROUPCODE   ");
            sb.append(" inner join SUPPLIER@amarml on AC_CODE= SHADEWISEDYES.PARTY_CODE");
            sb.append(" Inner Join FibreBase on  FibreBase.BaseCode = ShadeWiseDyes.Shade_Code  ");  //J0074','M4384')");
            sb.append(SQs);
            sb.append(" and to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') >="+sFromDate);
            sb.append(" and to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') <="+sToDate);
            sb.append(" and RecipeStatus=0  and WithEffectTo=99999999 ");
                   
            sb.append(" union all");
           
            sb.append(" Select shade_code,item_name,Dyes_per,Dyes_Rate,Dyes_cost,name,PriceRevisionStatus,");
            sb.append(" decode(LabStatus,1,'OK',2,'NOTOK',0,'NOTMADE') as LabStatus , RecipeStatus, OLDRECIPENO from SHADEWISEDYES ");
            sb.append(" inner join  invitems on invitems.ITEM_CODE =SHADEWISEDYES.DYES_CODE  and SHADEWISEDYES.NEW_RECEIPE = 1  ");
            sb.append(" inner join  stockgroup on groupcode = invitems.STKGROUPCODE   ");
            sb.append(" inner join SUPPLIER@amarml on AC_CODE= SHADEWISEDYES.PARTY_CODE");
            sb.append(" Inner Join FibreBase on  FibreBase.BaseCode = ShadeWiseDyes.Shade_Code ");  //J0074','M4384')");
            sb.append(SQs);
            sb.append( " and RecipeStatus=1 and PriceREvisionStatus=0 " );
           
            sb.append(" union all");
           
            sb.append(" Select shade_code,decode (Dyes_Item_Name,'',InvItems.ITEM_NAME,Dyes_InvItems.Dyes_Item_Name) as ItemName,");
            sb.append(" Dyes_per,Dyes_Rate,Dyes_cost,name, PriceRevisionStatus,");
            sb.append(" decode(LabStatus,0,'OK','NOTOK') as LabStatus,RecipeStatus, OLDRECIPENO from SHADEWISEDYES ");
            sb.append(" inner join  Dyes_invItems on Dyes_invItems.Dyes_ITEM_CODE =SHADEWISEDYES.DYES_CODE and SHADEWISEDYES.NEW_RECEIPE = 1  ");
            sb.append(" inner join SUPPLIER@amarml on AC_CODE= SHADEWISEDYES.PARTY_CODE");
            sb.append(" Left Join  InvItems on InvItems.Item_Code = Dyes_InvItems.Dyes_Item_Code ");
            sb.append(" Inner Join FibreBase on  FibreBase.BaseCode = ShadeWiseDyes.Shade_Code "); //J0074','M4384')");
            sb.append(SQs);
            sb.append(" and to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') >="+sFromDate);
            sb.append(" and to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') <="+sToDate);
            sb.append(" Order by shade_code, OLDRECIPENO, Item_Name ");

       
        try{
            if(theDyeConnection == null) {
                DyeJDBCConnection jdbc =  DyeJDBCConnection.getJDBCConnection();
                theDyeConnection          =  jdbc.getConnection();
            }
	    java.sql.PreparedStatement pst = theDyeConnection.prepareStatement(sb.toString());
	    java.sql.ResultSet   rst = pst . executeQuery();
            
            java.sql.ResultSetMetaData rsmd = rst.getMetaData();
            
            while (rst.next()) {
                HashMap themap = new HashMap();
                
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
                  
                ADataList.add(themap);
            }
            rst.close();
            pst.close();
            

    	if(AShadeList.size()==0){
    		
    		for(int j=0;j<ADataList.size();j++){
    			HashMap theMap = (HashMap)ADataList.get(j);
    			
    			if(!AShadeList.contains(common.parseNull((String)theMap.get("SHADE_CODE")))) {
    			    AShadeList.add(common.parseNull((String)theMap.get("SHADE_CODE")));
    			}
    		}
    		
    	}


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
	

}
