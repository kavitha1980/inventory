
package DyesPriceRevision;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

public class ShadeWiseDyeCostModel extends DefaultTableModel {
    String ColumnName[]={"SNO","SHADE NAME","DYES NAME","%","RATE","COST"};
  String  ColumnType[]={"S","S","S","S","S","S"};
  int iColumnWidth[] ={10,20,25,40,20,20};  
  
public ShadeWiseDyeCostModel(){
    setDataVector(getRowData(),ColumnName);
}   
public Class getColumnClass(int iCol)
{
        return getValueAt(0,iCol).getClass();
}

public boolean isCellEditable(int iRow,int iCol)
{
        if (ColumnType[iCol]=="E"||ColumnType[iCol]=="B")
            
            return true;
            return false;
}
    
private Object[][]getRowData()
{
        Object RowData[][]=new Object[1][ColumnName.length];
        
        for(int i=0;i<ColumnName.length;i++)
            RowData[0][i]="";
        return RowData;
}
    
public void appendRow(Vector theVect)
{
        insertRow(getRows(),theVect);
}
public int getRows()
{
        return super.dataVector.size();
}

}
