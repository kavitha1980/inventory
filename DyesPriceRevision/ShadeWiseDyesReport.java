package DyesPriceRevision;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;

import com.itextpdf.text.Document;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import java.io.FileOutputStream;
import java.text.DecimalFormat;


public class ShadeWiseDyesReport extends JInternalFrame {
	
	JPanel                      toppanel, botpanel;
                                
    JEditorPane                 editorPane;
    JButton                     bPrint, bExit;
    
    Connection                  theConnection;
    Connection                  theDyeConnection;
    
    JTextField                  TfromDate, TtoDate;
    
    Common                      common      = new Common();
    
    JList                       theShadeList;
    DefaultListModel            theShadeListModel;
    
    JList                       theDyesNameList;
    DefaultListModel            theDyesNameListModel;
    
    
    int                         iTotalColumns = 15;
    int                         iColWidth[] ;
    
    String                      sparty, sfibre, sfindStr = "";
    
    Vector                      VShadeCodeList;
    Vector                      VDyesList = new Vector();
    
    Vector 						VShadeBaseCode, VShadeBaseName;
    Vector 						VDyesCode, VDyesName, VShadeCode;
    
    JLayeredPane		Layer;
    
    java.util.List      ADataList = null;
    ArrayList           ACostList       = new ArrayList();
    java.util.List      theDyesList     = new java.util.ArrayList();
    java.util.List      theDyesNewPriceList = new java.util.ArrayList();
    
    
    private static final Insets insets = new Insets(2, 2, 2, 2);
    
        
    Document                    document;
    PdfPTable                   table, table1;
    
    PdfPCell c1;
    
    private static Font bigbold     = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
    private static Font mediumbold  = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
    private static Font smallbold   = FontFactory.getFont("TIMES_ROMAN", 7, Font.BOLD);
    private static Font smallbold1  = FontFactory.getFont("TIMES_ROMAN", 7, Font.BOLD);
    
    public ShadeWiseDyesReport(JLayeredPane Layer){
		this.Layer		= Layer;
		
		setData();
        createComponent();
        setLayout();
        addComponents();
        addListeners();
    }
   
    private void createComponent(){
        
        toppanel             = new JPanel();
        botpanel             = new JPanel();
        
        editorPane           = new JEditorPane();
        
        TfromDate            = new JTextField(10);
        TtoDate              = new JTextField(10);
        
        bPrint               = new JButton("Print");
        bExit                = new JButton("Exit");
        
        theShadeList         = new JList(theShadeListModel);
        theShadeList         . setVisibleRowCount(5);
        theShadeList		 . setCellRenderer(new CheckboxListCellRenderer());
        theShadeList		 . setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        theDyesNameListModel = new DefaultListModel();
        theDyesNameList      = new JList(theDyesNameListModel);
        theDyesNameList      . setVisibleRowCount(5);
        
    }
    
    private void setLayout(){
        setVisible(true);
        setSize(800,500);
      	setResizable(true);
		setMaximizable(true);
        setClosable(true);

        toppanel  . setLayout(new GridBagLayout());
        toppanel  . setBorder(new TitledBorder(""));
        
        botpanel  . setLayout(new BorderLayout()); 
        botpanel  . setBorder(new TitledBorder("Report"));
    }
    
    private void addComponents() {
        
		JScrollPane ShadeScrollpane = new JScrollPane(theShadeList);
					ShadeScrollpane . setBorder(BorderFactory.createTitledBorder("Shade "));

		JScrollPane DyesScrollpane = new JScrollPane(theDyesNameList);
					DyesScrollpane . setBorder(BorderFactory.createTitledBorder("Dyes"));
		
		addComponent(toppanel, ShadeScrollpane, 0, 1, 3, 3, GridBagConstraints.CENTER, GridBagConstraints.BOTH);
		addComponent(toppanel, DyesScrollpane, 4, 1, 3, 3, GridBagConstraints.CENTER, GridBagConstraints.BOTH);
		
		addComponent(toppanel, new JLabel("From Date"), 7, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		addComponent(toppanel, TfromDate, 8, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		
		addComponent(toppanel, new JLabel("To Date"), 7, 2, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		addComponent(toppanel, TtoDate, 8, 2, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		
		addComponent(toppanel, bPrint, 9, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
		addComponent(toppanel, bExit, 9, 2, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL);
        

		botpanel  . add(new JScrollPane(editorPane));
		
        getContentPane().add("North",toppanel);
        getContentPane().add("Center",botpanel);
		
    }
    
	private static void addComponent(Container container, Component component, int gridx, int gridy, int gridwidth, int gridheight, int anchor, int fill) {
    	
    	GridBagConstraints gbc = new GridBagConstraints(gridx, gridy, gridwidth, gridheight, 1.0, 1.0, anchor, fill, insets, 0, 0);
    	container.add(component, gbc);
    	
  	}	
    
    private void addListeners() {
        
        //theShadeList . addKeyListener(new KList());
        bPrint       . addActionListener(new ActList());
        bExit        . addActionListener(new ActList());
        
		theShadeList.addMouseListener(new MouseAdapter() {
         	public void mouseClicked(MouseEvent event) {
         		JList list = (JList) event.getSource();
            	//JList<CheckBoxItem> list = (JList<CheckBoxItem>) event.getSource();
 
            // Get index of item clicked
            int index = list.locationToIndex(event.getPoint());
            
            CheckBoxItem item = (CheckBoxItem) list.getModel().getElementAt(index);
 
            // Toggle selected state
            item.setSelected(!item.isSelected());
 			
            // Repaint cell
            list.repaint(list.getCellBounds(index, index));
            
            setShadewiseDyes();
         }
      });        
        
        
    }
    
    private class ActList implements ActionListener {
    
       public void actionPerformed(ActionEvent ae) {
            
            if (ae.getSource() == bExit){
                removeHelpFrame();
            }
            
            if (ae.getSource() == bPrint){
                setDataIntoVector();
                setReport();
                CreatePDFFile();
                
            }
        }
    }
    
    private void removeHelpFrame()   {

        try{
            Layer . remove(this);
            Layer . updateUI();

        }catch(Exception ex)  {
            ex.printStackTrace();
        }
   }
    
    
  	public void setReport(){
      	try{
            String sReport = "";
            sReport = getReport();
            editorPane.setContentType("text/html");
            editorPane.setText(sReport);
            
        }catch(Exception e){
            e.printStackTrace();
        }
 	}
    
    
	public java.util.List getSelectedShades() {
    	java.util.List list = new java.util.ArrayList();
    	
    	DefaultListModel dlm = (DefaultListModel) theShadeList.getModel();
    	int iSize = dlm.size();
    	
    	for (int i = 0; i < iSize; i++) {
      		Object obj = dlm.getElementAt(i);
      	
    		CheckBoxItem chkitem = (CheckBoxItem) obj;
        		
    		if (chkitem.isSelected()) {
      			list.add(obj.toString());
    		}
    	}  
    	
    	return list;  
	}
    
  	private String getReport(){
            String sStr = "";
            
    	try {
            sStr = "<html><head>";
            sStr =sStr + "  </head>";
            sStr =sStr + "  <body>";
            sStr =sStr + "  <h2 align ='center'> <b>Shade Cost Report</b></h2>";
            sStr =sStr + "  <table border='1' width='100%' >";
            sStr =sStr + "  <tr  bgcolor='lightgreen'> ";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>SL NO </b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Shade Code</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Appd Rate</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Lab Dye Cost</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Lab Chem Cost</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' colspan='4'><font color='#000099' ><b>Old Recipe</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' colspan='4'><font color='#000099' ><b>New Recipe</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Difference</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Lab Status </b></font></td>";
            sStr =sStr +"  </tr> ";
            
            sStr =sStr +"<tr> ";
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Dyes Name</b></font></td>"; 
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>%</b></font></td>"; 
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Cost</b></font></td>"; 
            
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Dyes Name</b></font></td>"; 
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>%</b></font></td>"; 
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Cost</b></font></td>"; 
                       
            sStr =sStr +"</tr> ";
        
            System.out.println("MODEL SIZE:" + ((DefaultListModel)theShadeList.getModel()).size());
             
             java.util.List theShadeCodeList = getSelectedShades();
             
		 // Body of the String
		    String sItemname="";
		    String sDyesPer="";
		    String sDyesRate="";
		    String sDyeCost="";
		    String sPriceRevisionStatus="";
		    String sLabStatus="";
		    double dTotalCost=0.0, dNewTotalCost=0.00;
        
     		for(int i=0;i<theShadeCodeList.size();i++) {
          
          		String sShadeCode = String.valueOf(theShadeCodeList.get(i));
          
          		SetCost(sShadeCode);
          		
			  	int iNoofDyes             = getNoofDyes(sShadeCode);
          
          		int iCostCount            = ACostList.size();
          		theDyesList               = getDyesList(sShadeCode);
          		theDyesNewPriceList       = getDyesNewRateList(sShadeCode);
          		sLabStatus                = getLabStatus(sShadeCode);
           
         		sStr = sStr +" <tr   bgcolor='pink'> ";   
         		sStr = sStr +"   <td   align='center' rowspan='"+iNoofDyes+"' ><font color='#000099' ><b>"+(i+1)+"</b></font></td>";
         		sStr = sStr +"   <td   align='left'   rowspan='"+iNoofDyes+"' ><font color='#000099' ><b>"+sShadeCode +"</b></font></td>";
       
       			if(iCostCount>0) {
           
				 double dAppRate        = getAppRate(sShadeCode);
				 double dLabDyeCost     = getLabDyeCost(sShadeCode);
				 double dLabChemCost    = getLabChemCost(sShadeCode);
				 
				 sStr =sStr +" <td align='center' rowspan='"+iNoofDyes+"'><font color='#000099' ><b>"+dAppRate+"</b></font></td>";
				 sStr =sStr +" <td align='center' rowspan='"+iNoofDyes+"'><font color='#000099' ><b>"+dLabDyeCost+"</b></font></td>";
				 sStr =sStr +" <td align='center' rowspan='"+iNoofDyes+"'><font color='#000099' ><b>"+dLabChemCost+"</b></font></td>";
			   }else {
				 sStr =sStr +" <td align='center' rowspan='"+iNoofDyes+"'><font color='#000099' ><b></b></font></td>";
				 sStr =sStr +" <td align='center' rowspan='"+iNoofDyes+"'><font color='#000099' ><b></b></font></td>";
				 sStr =sStr +" <td align='center' rowspan='"+iNoofDyes+"'><font color='#000099' ><b></b></font></td>";
			   }

			  if(theDyesList.size()>0) {
      

      			for(int a=0;a<iNoofDyes;a++){
       				
       				if(a<theDyesList.size()) {
					   java.util.HashMap theMap1 = (java.util.HashMap) theDyesList.get(a);
					   
					   sItemname            = (String)theMap1.get("ITEM_NAME");
					   sDyesPer             = (String)theMap1.get("DYES_PER");
					   sDyesRate            = (String)theMap1.get("DYES_RATE");
					   sDyeCost             = (String)theMap1.get("DYES_COST");
					   sPriceRevisionStatus = (String)theMap1.get("PRICEREVISIONSTATUS");
					  // sLabStatus         = (String)theMap1.get("LABSTATUS");
					   
					   DecimalFormat   df  = new DecimalFormat("0.000");
					   String sPer         = df.format(Double.parseDouble(sDyesPer));
					   String sCost        = df.format(Double.parseDouble(sDyeCost));        
					   
					   sStr =sStr +" <td   align='left'><font color='#000099'   ><b>"+sItemname+"</b></font></td>";
					   sStr =sStr +" <td   align='center'><font color='#000099' ><b>"+sPer+"</b></font></td>";
					   sStr =sStr +" <td   align='center'><font color='#000099' ><b>"+sDyesRate+"</b></font></td>";
					   sStr =sStr +" <td   align='center'><font color='#000099' ><b>"+sCost+"</b></font></td>";
				 
					   dTotalCost         = dTotalCost+common.toDouble(sDyeCost);
				    }else{
					   sStr = sStr +" <td   align='left'><font color='#000099'   ><b>&nbsp</b></font></td>";
					   sStr = sStr +" <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
					   sStr = sStr +" <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
					   sStr = sStr +" <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
				    }
				   
				    //System.out.println(sShadeCode+"Dyes New Price List Size==>"+theDyesNewPriceList.size());
         
    			    if(theDyesNewPriceList.size()>0){
    			    
      					for(int b=a;b<theDyesNewPriceList.size();b++){
      					
       						if(a<theDyesNewPriceList.size()) {   
       							   java.util.HashMap theMap2 = (java.util.HashMap) theDyesNewPriceList.get(b);
       
								   sItemname            = (String)theMap2.get("ITEM_NAME");
								   sDyesPer             = (String)theMap2.get("DYES_PER");
								   sDyesRate            = (String)theMap2.get("DYES_RATE");
								   sDyeCost             = (String)theMap2.get("DYES_COST");
								   sPriceRevisionStatus = (String)theMap2.get("PRICEREVISIONSTATUS");
								   
								   DecimalFormat   df1  = new DecimalFormat("0.000");
								   String sPer1         = df1.format(Double.parseDouble(sDyesPer));
								   String sCost1        = df1.format(Double.parseDouble(sDyeCost));        
								   
								  sStr =sStr +"    <td   align='left'><font color='#000099'   ><b>"+sItemname+"</b></font></td>";
								  sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sPer1+"</b></font></td>";
								  sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyesRate+"</b></font></td>";
								  sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sCost1+"</b></font></td>";
								 
								  dNewTotalCost      = dNewTotalCost+common.toDouble(sDyeCost);
								  
         					} else{
								 sStr =sStr +"    <td   align='left'><font color='#000099'   ><b>&nbsp</b></font></td>";
								 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
								 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
								 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
       						}
       						
       						break;
       					}
       				
			        }else if(theDyesNewPriceList.size()== 0){
						sStr =sStr +"    <td   align='left'><font color='#000099'   ><b></b></font></td>";
					 	sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
					 	sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
					 	sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
				    }
      
        			sStr =sStr +"  </tr> "; 
       			}
    		} else if(theDyesList.size()==0) {
    		
     			if(theDyesNewPriceList.size()>0){
         
         			System.out.println("New Price Size==>"+theDyesNewPriceList.size());
         			
      				for(int b=0;b<theDyesNewPriceList.size();b++){   
       					java.util.HashMap theMap2 = (java.util.HashMap) theDyesNewPriceList.get(b);
      				
						 sStr =sStr +"    <td   align='left'><font color='#000099'   ><b></b></font></td>";
						 sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
						 sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
						 sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
       
					     sItemname            = (String)theMap2.get("ITEM_NAME");
					     sDyesPer             = (String)theMap2.get("DYES_PER");
					     sDyesRate            = (String)theMap2.get("DYES_RATE");
					     sDyeCost             = (String)theMap2.get("DYES_COST");
					     sPriceRevisionStatus = (String)theMap2.get("PRICEREVISIONSTATUS");
					   
					     DecimalFormat   df  = new DecimalFormat("0.000");
					     String sPer         = df.format(Double.parseDouble(sDyesPer));
					     String sCost        = df.format(Double.parseDouble(sDyeCost));        
					   
						 sStr =sStr +"    <td   align='left'><font color='#000099'   ><b>"+sItemname+"</b></font></td>";
						 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sPer+"</b></font></td>";
						 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyesRate+"</b></font></td>";
						 sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sCost+"</b></font></td>";
					 
						 dNewTotalCost      = dNewTotalCost+common.toDouble(sDyeCost);
						 sStr =sStr +"  </tr> "; 
       				}
    			} else if(theDyesNewPriceList.size()== 0) {
    			
					 sStr =sStr +"    <td   align='left'><font color='#000099'   ><b></b></font></td>";
					 sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
					 sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
					 sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
					 
					 sStr =sStr +"    <td   align='left'><font color='#000099'   ><b></b></font></td>";
					 sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
					 sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
					 sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
					 sStr =sStr +"  </tr> "; 
       			}
    		}
    		
         
		     sStr =sStr +"  <tr> "; 
		     sStr =sStr +"    <td   align='center' colspan='5'><font color='#000099' ><b>SHADE WISE TOTAL</b></font></td>";
		     sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
		     sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
		     sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
		     sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.getRound(dTotalCost,2)+"</b></font></td>";
		     sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
		     sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
		     sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
		     sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.getRound(dNewTotalCost,2)+"</b></font></td>";
		     sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.getRound(dTotalCost-dNewTotalCost,2)+"</b></font></td>";
		     sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sLabStatus+"</b></font></td>";
		     sStr =sStr +"    </tr> "; 
		     
		     dTotalCost = 0.00;
		     dNewTotalCost = 0.00;
     	}
        theDyesList.clear();
        theDyesNewPriceList.clear();
        
       
    	}catch (Exception ex) {
          ex.printStackTrace();
     	}
     
 		return sStr;
    }    
    
    
	private void CreatePDFFile(){
      
      	try{
            String SPDFFile = "";
        	
            String Sos = System.getProperty("os.name").toLowerCase();
            if(Sos.trim().startsWith("wind")){
            	SPDFFile = "D:/ShadeCost.pdf";
            }else{
            	SPDFFile = "/ShadeCost.pdf";
            }
           
            document        = new Document();
            PdfWriter       .getInstance(document, new FileOutputStream(SPDFFile));
            document        .setPageSize(PageSize.A4.rotate());
            document        .open();
            int iColSize=4*2;
          
            iTotalColumns  = iColSize+7;
             table         = new PdfPTable(iTotalColumns);
             iColWidth     = new int[iTotalColumns];
             
             iColWidth[0] = 6;
             iColWidth[1] = 25;
             iColWidth[2] = 10;
             iColWidth[3] = 10;
             iColWidth[4] = 10;
             
             for (int j = 0; j < iColSize; j++) {
               iColWidth[j+5] = 25;
            }
            int iDiffColSize = 5+iColSize;
            iColWidth[iDiffColSize+0] = 17;
            iColWidth[iDiffColSize+1] = 15;
            
            table.setWidths(iColWidth);
            table.setWidthPercentage(100f);
            table.setHeaderRows(3);
            
           String sDate= common.getServerDate();

            AddCellIntoTable("Shade Cost Report "+common.parseDate(sDate)+"", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns , 0, 0, 0, 0, bigbold);
           //  AddCellIntoTable("DATE", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,2,20f);
            AddCellIntoTable("Sl.No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Shade Code", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Appd Rate", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Lab Dye Cost", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Lab Chem Cost", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Old Recipe", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 4 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("New Recipe", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 4 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("Difference", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Lab Status", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            
            
            AddCellIntoTable("Dyes Name", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("%", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("Rate", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("Cost", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            
            AddCellIntoTable("Dyes Name", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("%", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("Rate", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("Cost", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            
           PDFBody();
      }catch(Exception ex){
       ex.printStackTrace();   
      }
  }

  private void PDFBody(){
        
        int icountDiff = 0;
        String sItemname = "";
        String sDyesPer  = "";
        String sDyesRate = "";
        String sDyeCost  = "";
        String sPriceRevisionStatus = "", sLabStatus = "";
        double dTotalCost = 0.00, dNewTotalCost = 0.00;
        
        java.util.List theShadeCodeList = getSelectedShades();
        
       for(int i=0;i<theShadeCodeList.size();i++){
          
          String sShadeCode=String.valueOf(theShadeCodeList.get(i));
          
          SetCost(sShadeCode);
          
          int iMaxCount             = getNoofDyes(sShadeCode);
          
          if(iMaxCount==0){
          	 iMaxCount = 1;
          }
          
          int iCostCount            = ACostList.size();
          theDyesList               = getDyesList(sShadeCode);
          theDyesNewPriceList       = getDyesNewRateList(sShadeCode);
          int iDyesRowSize          = iMaxCount;
          sLabStatus                = getLabStatus(sShadeCode);
          
            
           AddCellIntoTable(String.valueOf(i+1), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
           AddCellIntoTable(String.valueOf(theShadeCodeList.get(i)), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
           
         if(iCostCount>0){
           double dAppRate        = getAppRate(sShadeCode);
           double dLabDyeCost     = getLabDyeCost(sShadeCode);
           double dLabChemCost    = getLabChemCost(sShadeCode);
                    
	        AddCellIntoTable(String.valueOf(String.valueOf(dAppRate)), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
	        AddCellIntoTable(String.valueOf(String.valueOf(dLabDyeCost)), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
	        AddCellIntoTable(String.valueOf(String.valueOf(dLabChemCost)), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
          }else{
		    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
		    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
		    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
          }
          
    	   //System.out.println(sShadeCode+"<==DyesRowSize-->"+iDyesRowSize+"--old--"+theDyesList.size()+"---New--"+theDyesNewPriceList.size());
           
           if(theDyesList.size()>0) {
           
			   for(int a=0;a<iDyesRowSize;a++) {
				 
				 if(a<theDyesList.size()) {
				     DecimalFormat   df  = new DecimalFormat("0.000");
				           
				    java.util.HashMap theMap1 = (java.util.HashMap) theDyesList.get(a);
				         sItemname            = (String)theMap1.get("ITEM_NAME");
				         sDyesPer             = (String)theMap1.get("DYES_PER");
				         sDyesRate            = (String)theMap1.get("DYES_RATE");
				         sDyeCost             = (String)theMap1.get("DYES_COST");
				         sPriceRevisionStatus = (String)theMap1.get("PRICEREVISIONSTATUS");
				         
				        AddCellIntoTable(sItemname, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
				        AddCellIntoTable(df.format(Double.parseDouble(sDyesPer)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
				        AddCellIntoTable(df.format(Double.parseDouble(sDyesRate)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
				        AddCellIntoTable(df.format(Double.parseDouble(sDyeCost)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
				        
				        dTotalCost         = dTotalCost+common.toDouble(sDyeCost);
				  } else{
				        AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
				        AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
				        AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
				        AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
				      //AddCellIntoTable("5", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
				  } 
         		
         		//Dyes New Price List Start
         		  if(theDyesNewPriceList.size()>0){	
         				
     				if(a<theDyesNewPriceList.size()) {  
     				
						for(int b=a;b<theDyesNewPriceList.size();b++){
						
						  if(b<theDyesNewPriceList.size()) {  
						  	
						  	java.util.HashMap theMap2 = (java.util.HashMap) theDyesNewPriceList.get(b);
						    DecimalFormat   df  = new DecimalFormat("0.000");
						    sItemname            = (String)theMap2.get("ITEM_NAME");
						    sDyesPer             = (String)theMap2.get("DYES_PER");
						    sDyesRate            = (String)theMap2.get("DYES_RATE");
						    sDyeCost             = (String)theMap2.get("DYES_COST");
						    sPriceRevisionStatus = (String)theMap2.get("PRICEREVISIONSTATUS");
				   
						    int iNewSize         = theDyesNewPriceList.size();
						//     System.out.println("NewSize==>"+iNewSize+"---"+a+1);
						     
						    AddCellIntoTable(sItemname, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable(df.format(Double.parseDouble(sDyesPer)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable(df.format(Double.parseDouble(sDyesRate)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable(df.format(Double.parseDouble(sDyeCost)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    
						    dNewTotalCost      = dNewTotalCost+common.toDouble(sDyeCost);
						     
						     break;
						  }else{
						       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						       
						       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						  } 
						}
						
					  }else{
					       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					       
					       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					  }	
						
						
			        }else if(theDyesNewPriceList.size()== 0){
					       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					       AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					       
						   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					}
				//Dyes New Price List End		
				
				
				}	//for loop end
				
			   } else if(theDyesList.size()==0) {
			   
			   
         		//Dyes New Price List Start
         		  if(theDyesNewPriceList.size()>0){	
         
						for(int b=0;b<theDyesNewPriceList.size();b++){
						  	java.util.HashMap theMap2 = (java.util.HashMap) theDyesNewPriceList.get(b);
						    DecimalFormat   df  = new DecimalFormat("0.000");
						    
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
								
						    sItemname            = (String)theMap2.get("ITEM_NAME");
						    sDyesPer             = (String)theMap2.get("DYES_PER");
						    sDyesRate            = (String)theMap2.get("DYES_RATE");
						    sDyeCost             = (String)theMap2.get("DYES_COST");
						    sPriceRevisionStatus = (String)theMap2.get("PRICEREVISIONSTATUS");
				   
						    int iNewSize         = theDyesNewPriceList.size();
						//     System.out.println("NewSize==>"+iNewSize+"---"+a+1);
						     
						    AddCellIntoTable(sItemname, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable(df.format(Double.parseDouble(sDyesPer)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable(df.format(Double.parseDouble(sDyesRate)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable(df.format(Double.parseDouble(sDyeCost)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    
						    dNewTotalCost      = dNewTotalCost+common.toDouble(sDyeCost);
						}
						
			        }else if(theDyesNewPriceList.size()== 0){
			        
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
								
			        
					        AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					        AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					        AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					        AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					        
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
						    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
					        
					}
				//Dyes New Price List End		
				
               } //end if 
               
              
               /*
                if(theDyesList.size()>theDyesNewPriceList.size()) {
                   int iOldSize     = theDyesList.size();
                   int iNewSize     = theDyesNewPriceList.size();
                   int iRowSize     = iOldSize-iNewSize;
                   
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                }
                */
       
                AddCellIntoTable("SHADE WISE TOTAL", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 5 , 4, 1, 8, 2, mediumbold,1);
 
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(common.getRound(String.valueOf(dTotalCost),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
                
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(common.getRound(String.valueOf(dNewTotalCost),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
                AddCellIntoTable(common.getRound(String.valueOf(dTotalCost-dNewTotalCost),2), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
                AddCellIntoTable(sLabStatus, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                
                dTotalCost = 0.00;
                dNewTotalCost = 0.00;
            }
               theDyesList.clear();
               theDyesNewPriceList.clear();
               
               
       try{
       		document.add(table);
       		document.close();
       }catch(Exception ex){
           
       }
      
    }
  
    
    
    public void setDataIntoVector() {
        ADataList  = new ArrayList();
        ADataList  . clear();
        
        String sFromDate = (String)TfromDate.getText();
        String sToDate   = (String)TtoDate.getText();
        
        StringBuilder shadecodes = new StringBuilder();
        
        java.util.List AShadeList = getSelectedShades();
        
           for(int i=0;i<AShadeList.size();i++){
               String sfibre = (String)AShadeList.get(i);
               
               	if(i==0){
                   	shadecodes.append(" ('"+sfibre+"'");
                }else{
                    shadecodes.append(",'"+sfibre+"'");
                }
           }
        	shadecodes.append(" ) ");
        
        System.out.println("shadecodes==>"+shadecodes.toString());
        
        
        StringBuffer sb            = new StringBuffer();
        
            sb.append(" Select shade_code,item_name,Dyes_per,Dyes_Rate,Dyes_cost,name,PriceRevisionStatus, ");
            sb.append(" decode(LabStatus,1,'OK',2,'NOTOK',0,'NOTMADE') as LabStatus , RecipeStatus from SHADEWISEDYES ");
            sb.append(" inner join  invitems on invitems.ITEM_CODE =SHADEWISEDYES.DYES_CODE  ");
            sb.append(" inner join  stockgroup on groupcode = invitems.STKGROUPCODE   ");
            sb.append(" inner join SUPPLIER@amarml on AC_CODE= SHADEWISEDYES.PARTY_CODE ");
            sb.append(" Inner Join FibreBase on  FibreBase.BaseCode = ShadeWiseDyes.Shade_Code and shade_code in ");
            sb.append(shadecodes.toString());
             
            sb.append(" and to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') <="+common.pureDate(sToDate)+" and  ");
            sb.append(" to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') >="+common.pureDate(sFromDate));
            sb.append(" and RecipeStatus=0  and WithEffectTo=99999999 " );
                   
            sb.append(" union all");
           
            sb.append(" Select shade_code,item_name,Dyes_per,Dyes_Rate,Dyes_cost,name,PriceRevisionStatus,");
            sb.append(" decode(LabStatus,1,'OK',2,'NOTOK',0,'NOTMADE') as LabStatus , RecipeStatus from SHADEWISEDYES ");
            sb.append(" inner join  invitems on invitems.ITEM_CODE =SHADEWISEDYES.DYES_CODE  ");
            sb.append(" inner join  stockgroup on groupcode = invitems.STKGROUPCODE   ");
            sb.append(" inner join SUPPLIER@amarml on AC_CODE= SHADEWISEDYES.PARTY_CODE");
            sb.append(" Inner Join FibreBase on  FibreBase.BaseCode = ShadeWiseDyes.Shade_Code and shade_code in "); 
            sb.append(shadecodes.toString());
            sb.append(" and RecipeStatus=1 and PriceRevisionStatus=0 " );
            sb.append(" union all ");
            sb.append(" Select shade_code,decode (Dyes_Item_Name,'',InvItems.ITEM_NAME,Dyes_InvItems.Dyes_Item_Name) as ItemName, ");
            sb.append(" Dyes_per,Dyes_Rate,Dyes_cost,name, PriceRevisionStatus, ");
            sb.append(" decode(LabStatus,0,'OK','NOTOK') as LabStatus,RecipeStatus from SHADEWISEDYES ");
            sb.append(" inner join  Dyes_invItems on Dyes_invItems.Dyes_ITEM_CODE =SHADEWISEDYES.DYES_CODE  ");
            sb.append(" inner join SUPPLIER@amarml on AC_CODE= SHADEWISEDYES.PARTY_CODE ");
            sb.append(" Left Join InvItems on InvItems.Item_Code = Dyes_InvItems.Dyes_Item_Code ");
            sb.append(" Inner Join FibreBase on  FibreBase.BaseCode = ShadeWiseDyes.Shade_Code and shade_code in "); 
            sb.append(shadecodes.toString());
            sb.append(" and to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') <="+common.pureDate(sToDate)+" and  ");
            sb.append(" to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') >="+common.pureDate(sFromDate));
            sb.append("  Order by Item_Name Desc ");
             
       		System.out.println("sb==>"+sb.toString());
       		
       		
        try{
            if(theDyeConnection == null) {
                DyeJDBCConnection jdbc =  DyeJDBCConnection.getJDBCConnection();
                theDyeConnection       =  jdbc.getConnection();
            }
        	PreparedStatement  pst = theDyeConnection.prepareStatement(sb.toString());
        	ResultSet    rst = pst . executeQuery();
            
            ResultSetMetaData rsmd = rst.getMetaData();
            
            java.util.HashMap theMap = null;
            
            while (rst.next()) {
                theMap = new java.util.HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    theMap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
                  
                ADataList.add(theMap);
            }
            rst.close();
            pst.close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    
    private void setData(){
    	
		setShadeNames();
    	setDyesName();
    }
    
    
	public void setShadeNames(){
        theShadeListModel = new DefaultListModel();
        VShadeBaseCode    = new Vector();
        VShadeBaseName    =  new Vector();
        
        StringBuffer sb = new StringBuffer(); 
					 sb . append("  Select Distinct Shade_Code, BaseName from ShadewiseDyes ");
					 sb . append("  Inner Join FibreBase on ShadeWiseDyes.Shade_Code = FibreBase.BaseCode ");
					 sb . append("  Order by 1 ");

    	try{
		    if (theDyeConnection==null){
		         DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
		         theDyeConnection       = jdbc.getConnection();
		    }
		    PreparedStatement ps        = theDyeConnection.prepareStatement(sb.toString());
		    ResultSet rst               = ps.executeQuery();

		    while (rst.next()){
		        VShadeBaseCode    . add(rst.getString(1));
		        VShadeBaseName    . add(rst.getString(2));
		        
		        theShadeListModel . addElement(new CheckBoxItem(rst.getString(1)));
		    }
		    rst.close();
		    ps.close();
		    ps=null;
         
            
    	}catch(Exception e){
       		e.printStackTrace();
    	}
    	
	}
    
    public void getDyesName(String sShadeCode){
    	
    	
    
    }
    
	public void setDyesName() {
        VDyesCode  = new Vector();
        VDyesName  = new Vector();
        VShadeCode = new Vector();
        
        StringBuffer sb  = new StringBuffer(); 
					 sb . append(" Select Distinct Dyes_Code as DyesCode,Item_Name as ItemName, Shade_Code from ShadewiseDyes ");
					 sb . append(" Inner Join InvItems on ShadewiseDyes.Dyes_Code = InvItems.Item_Code ");
					 sb . append(" Where  PriceRevisionStatus = 0 ");
					 //sb . append(" Where Shade_Code='"+sShadeCode+"' and PriceRevisionStatus = 0 ");
					 sb . append(" Union All ");
					 sb . append(" Select Dyes_Code as DyesCode ,Dyes_Item_Name as ItemName, Shade_Code from ShadewiseDyes ");
					 sb . append(" Inner Join Dyes_InvItems on ShadewiseDyes.Dyes_Code = Dyes_InvItems.Dyes_Item_Code ");
					 sb . append(" Where PriceRevisionStatus = 0 ");
					 //sb . append(" Where Shade_Code ='"+sShadeCode+"' and PriceRevisionStatus = 0 ");
					 sb . append(" Order by 2");
        
		try{
		    if (theDyeConnection==null) {
		        DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
		      	theDyeConnection       = jdbc.getConnection();
		    }
		    PreparedStatement ps       = theDyeConnection.prepareStatement(sb.toString());
		    ResultSet rst              = ps.executeQuery();
		       
		   	while (rst.next()){
		        VDyesCode . add(rst.getString(1));
		        VDyesName . add(rst.getString(2));
		        VShadeCode. add(rst.getString(3));
		    }
		    rst. close();
		    rst= null;
		    ps . close();
		    ps = null;
		     
		        
		}catch(Exception e){
		    e.printStackTrace();
		}
	}
    
	public void setShadewiseDyes(){
        theDyesNameListModel . removeAllElements();
        java.util.List theShadeCodeList = getSelectedShades();
        
        for (int i = 0; i < VDyesCode.size(); i++) {
            String sDyesName = (String)VDyesName.get(i);
            String sShadeCode = (String)VShadeCode.get(i);
            
            if(theShadeCodeList.contains(sShadeCode)) {
            	theDyesNameListModel.addElement(new CheckBoxItem(sDyesName));
            }
        }
	}
    
    
    
  public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
// heading
    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }
    
    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont,int iRowspan,float fHeight) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowspan);
        //c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
    
	public int  getNoofDyes( String sShadeCode){
        int iOldRecNoofDyes = 0;
        int iNewRecNoofDyes = 0;
                  
        for (int i = 0; i < ADataList.size(); i++) {
            HashMap themap   = (HashMap) ADataList.get(i);

           String sShade     = ((String) themap.get("SHADE_CODE"));
           String sPriceRevisionStatus  = ((String) themap.get("RECIPESTATUS"));
           
	       if(sShadeCode.equals(sShade)){
	       	   if(sPriceRevisionStatus.trim().equals("1")){
               		iOldRecNoofDyes++;
               }
	       	   if(sPriceRevisionStatus.trim().equals("0")){
               		iNewRecNoofDyes++;
               }
           }
            
        }
        
       if(iNewRecNoofDyes>iOldRecNoofDyes){
           return iNewRecNoofDyes;
       }else{
       		return iOldRecNoofDyes;
       }
      
	}
    
	public ArrayList  getDyesList( String sShadeCode){
        ArrayList theList     = new ArrayList();
        ArrayList theListNew  = new ArrayList();
  
        for (int i = 0; i < ADataList.size(); i++) {
            HashMap themap   = (HashMap) ADataList.get(i);
           
           String sShade     = ((String) themap.get("SHADE_CODE"));
           String sPriceRevisionStatus  = ((String) themap.get("RECIPESTATUS"));
           
           if(sPriceRevisionStatus.trim().equals("1") && sShadeCode.equals(sShade)) {
           		theList.add(themap);
           }
           if(sPriceRevisionStatus.trim().equals("0") && sShadeCode.equals(sShade)){
               theListNew.add(themap);
           }
         
      	}
      	
      	return theList;
	}
    
	public ArrayList  getDyesNewRateList( String sShadeCode){
        ArrayList theList   = new ArrayList();
       
        for (int i = 0; i < ADataList.size(); i++) {
           HashMap themap   = (HashMap) ADataList.get(i);
           
           String sShade     = ((String) themap.get("SHADE_CODE"));
           String sPriceRevisionStatus  = ((String) themap.get("RECIPESTATUS"));
          
            if(sPriceRevisionStatus.trim().equals("0") && sShadeCode.equals(sShade)) {
               theList.add(themap);
            }
        }
        
        return theList;
	}
	
	public String getLabStatus( String sShadeCode){
       String sLabStatus = "";  
       
        for (int i = 0; i < ADataList.size(); i++) {
          HashMap themap  = (HashMap) ADataList.get(i);
          
          String sshade  = ((String) themap.get("SHADE_CODE"));
          String sPriceRevisionStatus  = ((String) themap.get("RECIPESTATUS"));
          
          if (sShadeCode.equals(sshade) && sPriceRevisionStatus.equals("0")) {
            	sLabStatus   = ((String) themap.get("LABSTATUS"));
            	break;
          }
        }
        
        return sLabStatus;
	}
	
	
   public void SetCost(String sShadeCode) {
        ACostList = new ArrayList();
        ACostList.clear();
    
        StringBuffer sb = new StringBuffer();
					 sb . append(" Select count(1), OrderRate,LabDyesCost,LabChemCost,FibreCode from FibreLotCost ");
					 sb . append(" inner join dyeingorder on dyeingorder.id = fibrelotcost.orderid   ");
					 sb . append(" Where FibreLotCost.FibreCode = '"+sShadeCode+"' " );
					 sb . append(" and OrderID = (Select Max(OrderID) from FibreLotCost Where FibreCode = '"+sShadeCode+"' )");
					 sb . append(" Group by OrderRate,LabDyesCost,LabChemCost,FibreCode ");
         
        try{
            if (theDyeConnection == null) {
                DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                theDyeConnection       = jdbc.getConnection();
            }
        	PreparedStatement pst = theDyeConnection.prepareStatement(sb.toString());
        	ResultSet rst  =  pst . executeQuery();
            
            ResultSetMetaData rsmd = rst.getMetaData();
            
            while (rst.next()) {
                HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
                
                ACostList.add(themap);
            }
            rst.close();
            pst.close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
    }
   
 private double getAppRate(String sShadeCode){
     double dValue = 0.00;
     
       for(int i=0;i<ACostList.size();i++){
         java.util.HashMap theMap = (java.util.HashMap) ACostList.get(i);
         
         String sShCode           = (String)theMap.get("FIBRECODE");
         
         if(sShadeCode.trim().equals(sShCode)){
              dValue    = common.toDouble(String.valueOf(theMap.get("ORDERRATE")));
         }
         
       }
       
       return dValue;
   }

  private double getLabDyeCost(String sShadeCode){
     double dValue = 0.00;

       for(int i=0;i<ACostList.size();i++){
         java.util.HashMap theMap = (java.util.HashMap) ACostList.get(i);
         
         String sShCode           = (String)theMap.get("FIBRECODE");
         
         if(sShadeCode.trim().equals(sShCode)){
              dValue    = common.toDouble(String.valueOf(theMap.get("LABDYESCOST")));
         }
         
       }
       
       return dValue;
   }
  
  private double getLabChemCost(String sShadeCode){
     double dValue = 0.00;
     
       for(int i=0;i<ACostList.size();i++){
         java.util.HashMap theMap = (java.util.HashMap) ACostList.get(i);
         
         String sShCode           = (String)theMap.get("FIBRECODE");
         
         if(sShadeCode.trim().equals(sShCode)){
              dValue    = common.toDouble(String.valueOf(theMap.get("LABCHEMCOST")));
         }
         
       }
       
       return dValue;
   }
	
    
    

    public class CheckBoxItem {
        private String label;
        boolean isSelected = false;

        public CheckBoxItem(String label) {
            this.label = label;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean isSelected) {
            this.isSelected = isSelected;
        }

        public String toString() {
            return label;
        }
    }
    
    
	public class CheckboxListCellRenderer extends JCheckBox implements ListCellRenderer {

		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

		    setComponentOrientation(list.getComponentOrientation());
		    setFont(list.getFont());
		    setBackground(list.getBackground());
		    setForeground(list.getForeground());
		    //setSelected(isSelected);
		    setSelected(((CheckBoxItem) value).isSelected());
		    setEnabled(list.isEnabled());
		    setText(value.toString());  

		    return this;
		}
	}    
	

}
