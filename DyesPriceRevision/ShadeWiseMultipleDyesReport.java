package DyesPriceRevision;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import java.text.*;
import javax.swing.border.TitledBorder;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import guiutil.AddressField;
import java.awt.*;
import java.awt.event.*;
import java.io.FileOutputStream;

public class ShadeWiseMultipleDyesReport extends JInternalFrame {
    JPanel                      TopPanel,MiddlePanel,pnlMain,pnlParty, pnlShade,pnlButton, BottomPanel,
                                pnlDate, pnlNotes,pnlFilter,pnlTopSearch,pnlShadeSearch,pnlDyesSearch;
    JComboBox                   JCSupplier,JCok;
    JEditorPane                 pnlEditor;
    JButton                     BExit,BPrint;
    Connection                  theConnection;
    Connection                  theDyeConnection;
    Vector                      VSupplierCode,VSupplierName;
    JTextField                  txtFromDate,txtToDate,txtDyesCode;
    JButton                     BSave; 
    //ArrayList                   ADataList;
    ArrayList                   APartyList      = new ArrayList();
    ArrayList                   AShadeList      = new ArrayList();
    ArrayList                   ACostList       = new ArrayList();
    ArrayList                   ADyesNameList   = new ArrayList();
    ArrayList                   ADyesCodeList   = new ArrayList();
    ArrayList                   ADyesList       = new ArrayList();
    java.util.List              AMaxList        = new java.util.ArrayList();
    java.util.List              ADataList       = new java.util.ArrayList();
    java.util.List              AFibreCodeList  = new java.util.ArrayList();
    java.util.List              theDyesList     = new java.util.ArrayList();
    java.util.List              theDyesNewPriceList = new java.util.ArrayList();
    
    Common                      common      = new Common();
    DyeingData                  data        = new DyeingData();
    //JList                       theDyesNameList;
    //DefaultListModel            theDyesNameModel;
    
    JList                       theShadeList;
    DefaultListModel            theShadeListModel;
    
    JList                       theDyesNameList;
    DefaultListModel            theDyesNameListModel;
    
    Document                    document;
    PdfPTable                   table, table1;
    int                         iTotalColumns = 15;
    int                         iColWidth[] ;
    String                      sparty,sfibre, sfindStr="";
    Vector                      VShadeCodeList;
    Vector                      VDyesList = new Vector();
    AddressField                txtShadeCode = new AddressField();
    JCheckBox                   chkAll;
    JLayeredPane		Layer;
    
    //PdfPTable                   table;
    //int[]                       iColWidth = {10,45,25,15,15,15,15,15,25,25,1,15,15,15,15,15,25,25,1,15,25,15,15 };
        
    PdfPCell c1;
    private static Font bigbold     = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
    private static Font mediumbold  = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
    private static Font smallbold   = FontFactory.getFont("TIMES_ROMAN", 7, Font.BOLD);
    private static Font smallbold1  = FontFactory.getFont("TIMES_ROMAN", 7, Font.BOLD);
    
   public ShadeWiseMultipleDyesReport(JLayeredPane Layer){
	this.Layer		= Layer;
        setCreateComponent();
        setLayout();
        addComponent();
        setShadeList();
        addListeners();
   } 
   
     private void setCreateComponent(){
        TopPanel                    =   new JPanel();
        MiddlePanel                 =   new JPanel();
        pnlMain                     =   new JPanel();
        pnlParty                    =   new JPanel();
        pnlShade                    =   new JPanel();
        pnlButton                   =   new JPanel();
        pnlDate                     =   new JPanel();
        BottomPanel                 =   new JPanel();
        pnlNotes                    =   new JPanel();
        pnlFilter                   =   new JPanel();
        pnlTopSearch                =   new JPanel();
        pnlShadeSearch              =   new JPanel();
        pnlDyesSearch               =   new JPanel();
        JCSupplier                  =   new JComboBox(data.VDyeSupName);
        pnlEditor                   =   new JEditorPane();
        txtFromDate                 =   new JTextField(10);
        txtToDate                   =   new JTextField(10);
        //txtShadeCode                =   new JTextField(25);
        txtDyesCode                 =   new JTextField(25);
        JCok                        =   new JComboBox(); 
        JCok                        .   addItem("OK");
        JCok                        .   addItem("NOT OK");
        BSave                       =   new JButton("SAVE");
        BExit                       =   new JButton("Exit");
        BPrint                      =   new JButton("Print Report / Print PDF");
        chkAll                      =   new JCheckBox(" All", false);
        
        theShadeListModel           =   new DefaultListModel();
        theShadeList                =   new JList(theShadeListModel);
        theShadeList                .   setVisibleRowCount(5);
        
        theDyesNameListModel        =   new DefaultListModel();
        theDyesNameList             =   new JList(theDyesNameListModel);
        theDyesNameList             .   setVisibleRowCount(5);
        
    }
    
    private void setLayout(){
            setVisible(true);
            setSize(800,500);
      	    setResizable(true);
        //setBounds(0, 0, 1300, 500);
        
        pnlParty                    .       setBorder(new TitledBorder("Select DyesName"));
        pnlParty                    .       setLayout(new BorderLayout());
        
        pnlShade                    .       setBorder(new TitledBorder("Select Shade"));
        pnlShade                    .       setLayout(new BorderLayout());
        
        pnlDate                     .       setBorder(new TitledBorder("Date"));
        pnlDate                     .       setLayout(new GridLayout(2,2));
        
        pnlButton                   .       setBorder(new TitledBorder(""));
        pnlButton                   .       setLayout(new BorderLayout());
        
        pnlButton                   .       add(BPrint);
        
        pnlFilter                   .       setBorder(new TitledBorder("Filter"));
        pnlFilter                   .       setLayout(new GridLayout(1,6));
        
//        TopPanel                    .       setBorder(new TitledBorder("Filter"));
        pnlShadeSearch              .       setBorder(new TitledBorder("Search Shade"));
        pnlShadeSearch              .       setLayout(new BorderLayout());
        pnlDyesSearch               .       setBorder(new TitledBorder("Search Dyes"));
        pnlDyesSearch               .       setLayout(new BorderLayout());

        TopPanel                    .       setLayout(new GridLayout(1,4));


        
        pnlShade                    .       add("Center",new JScrollPane(theShadeList));
        pnlParty                    .       add("Center",new JScrollPane(theDyesNameList));
        
        TopPanel                    .       add(pnlShade);
        TopPanel                    .       add(pnlParty);
        TopPanel                    .       add(pnlDate);
        TopPanel                    .       add(pnlButton);
        
        pnlDate                     .       add(new JLabel("From Date"));
        pnlDate                     .       add(txtFromDate);
        pnlDate                     .       add(new JLabel("To Date"));
        pnlDate                     .       add(txtToDate);
    
        BottomPanel                 .       add(new JLabel("Lab Status")); 
      //  BottomPanel                 .       add(JCok);
      //  BottomPanel                 .       add(BSave);
        BottomPanel                 .       setBorder(new TitledBorder("Controls"));
        BottomPanel                 .       setLayout(new FlowLayout()); 
                
        MiddlePanel                 .       setBorder(new TitledBorder("Report Details"));
        MiddlePanel                 .       setLayout(new BorderLayout());
        MiddlePanel                 .       add(new JScrollPane(pnlEditor));
        pnlEditor                   .       setEditable(false);
        pnlMain                     .       setLayout(new BorderLayout());
        
        pnlMain                     .       add("North",TopPanel);
        //pnlMain                     .       add("North",pnlFilter);
        pnlMain                     .       add("Center",MiddlePanel);
      //  pnlMain                     .       add("South",BottomPanel);
        this                        .       add(pnlMain);
	this			    .	    setMaximizable(true);
        this			    .	    setClosable(true);
        this            	    .  	    setResizable(true);
    
    }
    
    private void addComponent(){
        
    }
    private void addListeners() {
        BExit                       .       addActionListener(new ActList());
        BPrint                      .       addActionListener(new ActList());
        BSave                       .       addActionListener(new ActList());    
        BPrint                      .       addActionListener(new ActList());
        txtShadeCode                .       addKeyListener (new ShadeKeyList());
        theShadeList                .       addKeyListener(new KList());   
        chkAll                      .       addActionListener(new ActList());
    }
    private class ActList implements ActionListener {
       public void actionPerformed(ActionEvent ae) {
            if (ae.getSource() == BExit){
               // dispose();
            }
            if (ae.getSource() == BPrint){
                SetDataIntoVector();
                CreatePDFFile();
                SetReport();
            }
             if(ae.getSource()==BSave){
            //    System.out.println("btnSave");
                updatedetails();
            }
             if(ae.getSource()==chkAll){
                 System.out.println("Select All");
                setShadeList();
            }
        }
    }
    
public class ItemList implements ItemListener {
  public void itemStateChanged(ItemEvent ie){
   try{
          if (ie.getSource() == JCSupplier && ie.getStateChange()==ie.SELECTED) {
                     }
               }
          catch(Exception ex){
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null,ex,"ShadewiseDyes",JOptionPane.ERROR_MESSAGE);
               }
       }
   }
    
private class ShadeKeyList extends KeyAdapter{
  public ShadeKeyList(){
        
    }
public void keyReleased(KeyEvent ke){
  char lastchar=ke.getKeyChar();
  lastchar=Character.toUpperCase(lastchar);
 // System.out.println("Inside Listener==>"+lastchar);
  try{
  if(ke.getKeyCode()==8){
    sfindStr=sfindStr.substring(0,(sfindStr.length()-1));
    setShadeCodeViewDetails(sfindStr);
  }
   else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9')){
    sfindStr=sfindStr+lastchar;
    setShadeCodeViewDetails(sfindStr);
    setDyesNameList(sfindStr);
  }
  if(ke.getKeyCode()== KeyEvent.VK_DELETE){
        txtShadeCode         . setText("");
        sfindStr             = "";
        VShadeCodeList       . removeAllElements();
        theShadeList         .setListData( VShadeCodeList);
   }
  
  if(txtShadeCode.getText().length()==0){
      theDyesNameListModel  .clear();
      theShadeList       .setListData(VShadeCodeList);
    }
  }
catch(Exception ex){
      ex.printStackTrace();
      System.out.println("TxtKeyListener"+ex);
  }
 }
}

private class KList extends KeyAdapter{
   public KList(){
        
    }
public void keyReleased(KeyEvent ke){
if(ke.getKeyCode()==KeyEvent.VK_ENTER){
   try{
        int iSelectedIndex=0;   
             iSelectedIndex              = theShadeList.getSelectedIndex();
             for(int j=0; j<=AShadeList.size(); j++){
              String sShade         = (String)AShadeList.get(j)  ;
               setDyesNameList(sShade);
            }
     }
   catch(Exception e){
          e.printStackTrace();
          System.out.println("Inside KeyListener:"+e);
      }
    }
  }
}


    
private class TableKeyList extends KeyAdapter {
    public void keyReleased(KeyEvent ke) {
       try {
          theDyesNameList.clearSelection();
            for (int a = 0; a < data.VDyeSupName.size(); a++) {
                    CheckBoxItem Citem = (CheckBoxItem) theDyesNameList.getModel().getElementAt(a);
                    if (Citem.isSelected()) {
                        Citem.setSelected(false);
                        theDyesNameList.repaint();
                    }
                 }
              } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Enter-->:" + e);
            }
        }

        public void keyPressed(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
            }
        }
    }

    public class CheckBoxItem {
        private String label;
        boolean isSelected = false;

        public CheckBoxItem(String label) {
            this.label = label;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean isSelected) {
            this.isSelected = isSelected;
        }

        public String toString() {
            return label;
        }
    }

    class CheckListRendererItm extends JCheckBox implements ListCellRenderer {

        public Component getListCellRendererComponent(
            JList list, Object value, int index,
            boolean isSelected, boolean hasFocus) {
            setEnabled(list.isEnabled());
            setSelected(((CheckBoxItem) value).isSelected());
            setFont(list.getFont());
            setBackground(list.getBackground());
            setForeground(list.getForeground());
            setText(value.toString());
            return this;
        }
    }
    
private void setShadeCodeViewDetails(String str){
try{
    String sShadeCode="";
    Vector VSelectedShadeCode    =   new Vector();
 if(!str.equals("")){
       for(int i=0;i<VShadeCodeList.size();i++){
           sShadeCode                = (String)VShadeCodeList.get(i);
           if(common.parseNull(sShadeCode).startsWith(str)){
               VSelectedShadeCode   .addElement(common.parseNull(sShadeCode));
           }
       }
   }
      theShadeList.setListData(VSelectedShadeCode);
      theShadeList.setSelectedIndex(0);
}

catch(Exception e){
    e.printStackTrace();
    System.out.println("View Shade Code ----->"+e);
  }
}

	private void setDyesNameList(String sShadeCode) {
       String      sFromDate       = txtFromDate.getText();
       String      sToDate         = txtToDate.getText();
       
       data.setShadewiseDyes(sShadeCode);
        for (int i = 0; i < data.VShadewiseDyesCode.size(); i++) {
            String sDyesName = (String) data.VShadewiseDyesName.get(i);
            String sDyesCode = (String) data.VShadewiseDyesCode.get(i);
            theDyesNameListModel.addElement(new CheckBoxItem(sDyesName+"#"+sDyesCode));
            VDyesList           .addElement(sDyesName+"#"+sDyesCode);
            ADyesNameList       .add(sDyesName+"#"+sDyesCode);
        }
    }
  
    private void setShadeList() {
        VShadeCodeList         = new Vector();
        AShadeList             = new ArrayList();
        for (int i = 0; i < data.VShadeBaseCode.size(); i++) {
            String sBaseCode   = (String) data.VShadeBaseCode.get(i);
            
             theShadeListModel . addElement(new CheckBoxItem(sBaseCode));
             VShadeCodeList    . addElement(sBaseCode);
        }
        theShadeList.setCellRenderer(new CheckListRendererItm());
        theShadeList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        theShadeList.addMouseListener(new MouseAdapter() {

            public void mouseClicked(MouseEvent event) {
                int iSelectedIndex = -1;
                JList jlist = (JList) event.getSource();
                int index = jlist.locationToIndex(event.getPoint());
                CheckBoxItem Citem = (CheckBoxItem) jlist.getModel().getElementAt(index);
                Citem.setSelected(!Citem.isSelected());
                jlist.repaint(jlist.getCellBounds(index, index));
                String sOrderType = String.valueOf(Citem);
                
                if(chkAll.isSelected()){
                    System.out.println("L0");
                    Citem.setSelected(true);
                }else{ 
                if (Citem.isSelected() == true) {
                    AShadeList.add(sOrderType);
                 //   System.out.println("Party List ---->"+AShadeList);
                    for (int j = 0; j < AShadeList.size(); j++) {
                        String sShadeCode       = (String)AShadeList.get(j);
                        setDyesNameList(sShadeCode);
                    }
                  }
                }
                if (Citem.isSelected() == false) {
                    String sPartyName = sOrderType;
                    theDyesNameListModel   .removeAllElements();
                    AShadeList.remove(sPartyName);
                    System.out.println("Party List --Remove-->"+AShadeList);
                    //lblSelectedCount.setText(String.valueOf(AOrderTypeList));
                }
            }
        });
     }   
    
  public void SetReport(){
      try{
            String sReport = "";
            sReport = getReport();
            pnlEditor.setContentType("text/html");
            pnlEditor.setText(sReport);
        }
        catch(Exception e){
            e.printStackTrace();
        }
 }
  
  
/*
 private String getReport(){
             String sStr = "";
            String sHeading ="Shade Cost Report";
    try {
            sStr = "<html><head>";
            sStr =sStr + "  </head>";
            sStr =sStr + "  <body>";
            sStr =sStr + "  <table border='1' width='70%' >";
            sStr =sStr + "  <tr  bgcolor='lightgreen'> ";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>SL NO </b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Shade Code</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Appd Rate</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Lab Dye Cost</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Lab Chem Cost</b></font></td>";
            
            for(int i=0;i<APartyList.size();i++){
                sparty = (String) APartyList.get(i);
                sStr =sStr +"  <td align='center' color='#FF00FF' colspan='4'><font color='#000099' ><b>"+sparty+"</b></font></td>";
           }
             sStr =sStr +"  </tr> ";
             sStr =sStr +"<tr  bgcolor='darkgreen'>";
           for(int i=0;i<APartyList.size();i++){

             sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Dyes</b></font></td>"; 
             sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>%</b></font></td>"; 
             sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Rate / kg</b></font></td>";
             sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>cost</b></font></td>"; 
            }
             sStr =sStr +"  </tr> ";
        
             
     // Body of the String
  
        int icountDiff=0;
        String sItemname="";
        String sDyesPer="";
        String sDyesRate="";
        String sDyeCost="";
        double dTotalCost=0.0;
        
       
     for(int i=0;i<AShadeList.size();i++){
          String sShadeCode=String.valueOf(AShadeList.get(i));
          SetCost(sShadeCode);
          int iMaxCount= setMaxCount(sShadeCode);
          java.util.HashMap theMap = (java.util.HashMap) ACostList.get(i);
          // System.out.println("Max Count=>"+iMaxCount);
       for(int m=0;m<iMaxCount;m++) {
         sStr =sStr +"  <tr   bgcolor='pink'> ";   
        if(m==0){
            
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"' ><font color='#000099' ><b>"+(i+1)+"</b></font></td>";
         sStr =sStr +"    <td   align='left'   rowspan='"+iMaxCount+"' ><font color='#000099' ><b>"+sShadeCode +"</b></font></td>";
       // sStr =sStr +"    <td   align='left'   rowspan='"+iMaxCount+"' ><font color='#000099' ><b>"+vShade.get(i) +"</b></font></td>";
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b>"+(String)theMap.get("ORDERRATE")+"</b></font></td>";
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b>"+(String)theMap.get("LABDYESCOST")+"</b></font></td>";
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b>"+(String)theMap.get("LABCHEMCOST")+"</b></font></td>";
        }
          for(int j=0;j<APartyList.size();j++){
                sparty = (String) APartyList.get(j);
                 System.out.println("sShadeCode==>"+sShadeCode+"sparty====>"+sparty);
             
                theDyesList=  getDyesList(sparty,sShadeCode);
                int iPartyCount = getDyesPartyList(sparty,sShadeCode);
                System.out.println("m====>"+m+" theDyesList "+theDyesList.size()+" And PACount "+iPartyCount+" MaxCount "+iMaxCount+"sparty==>"+sparty);

                 if(iPartyCount<(m+1)) 
                 {
                     sStr =sStr +"    <td   align='left'  ><font color='#000099' ><b>&nbsp;</b></font></td>";
                     sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp;</b></font></td>";
                     sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp;</b></font></td>";
                     sStr =sStr +"    <td   align='center' ><font color='#000099' ><b>&nbsp;</b></font></td>";

                 }else{
                 java.util.HashMap theMap1 = (java.util.HashMap) theDyesList.get(m);
                 sItemname=common.parseNull((String) theMap1.get("ITEM_NAME"));
                 sDyesPer=common.parseNull((String) theMap1.get("DYES_PER"));
                 sDyesRate=common.parseNull((String) theMap1.get("DYES_RATE"));
                 sDyeCost=common.parseNull((String) theMap1.get("DYES_COST"));
                
                 
           //        System.out.println("sItemname=================================>"+sItemname);
                   sStr =sStr +"    <td   align='left'><font color='#000099'   ><b>"+sItemname+"</b></font></td>";
                   sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyesPer+"</b></font></td>";
                   sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyesRate+"</b></font></td>";
                   sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyeCost+"</b></font></td>";
                 }
                    theDyesList.clear();
       
               }
                    sStr =sStr +"  </tr> "; 
            }
                   sStr =sStr +"     <tr > ";
                   sStr =sStr +"     <td   align='left' ><font color='#000099'  ><b>Total</b></font></td>";
                   sStr =sStr +"     <td   align='center' ><font color='#000099' ><b>&nbsp;</b></font></td>";
                   sStr =sStr +"     <td   align='center'><font color='#000099' ><b>&nbsp;</b></font></td>";
                   sStr =sStr +"     <td   align='center' ><font color='#000099' ><b>&nbsp;</b></font></td>";
                   sStr =sStr +"     <td   align='left' ><font color='#000099'  ><b>&nbsp;</b></font></td>";
                   
                   for(int k=0;k<APartyList.size();k++){
                        String  spartyNew   =   (String)APartyList.get(k);
                        double dCost        =   getCost(spartyNew,sShadeCode);
                        dTotalCost          =   dCost;

                   sStr =sStr +"     <td   align='center' ><font color='#000099' ><b>&nbsp;</b></font></td>";
                   sStr =sStr +"     <td   align='center'><font color='#000099' ><b>&nbsp;</b></font></td>";
                   sStr =sStr +"     <td   align='left' ><font color='#000099'  ><b>&nbsp;</b></font></td>";
                   sStr =sStr +"     <td   align='center' ><font color='#000099' ><b>"+dTotalCost+"</b></font></td>";
                }
                   sStr =sStr +"   </tr > "; 
          }
      } catch (Exception ex) {
            ex.printStackTrace();
     }
 return sStr;
    }
 */
  
  //05.05.2017
 /* 
  private String getReport(){
            String sStr = "";
            String sHeading ="Shade Cost Report";
    try {
            sStr = "<html><head>";
            sStr =sStr + "  </head>";
            sStr =sStr + "  <body>";
            sStr =sStr + "  <table border='1' width='70%' >";
            sStr =sStr + "  <tr  bgcolor='lightgreen'> ";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>SL NO </b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Shade Code</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Appd Rate</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Lab Dye Cost</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Lab Chem Cost</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' colspan='4'><font color='#000099' ><b>Old Rate</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' colspan='4'><font color='#000099' ><b>New Rate</b></font></td>";
            sStr =sStr +"  </tr> ";
            sStr =sStr +"  <tr> ";
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Dyes Name</b></font></td>"; 
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>%</b></font></td>"; 
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Cost</b></font></td>"; 
            
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Dyes Name</b></font></td>"; 
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>%</b></font></td>"; 
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Cost</b></font></td>"; 
           
            sStr =sStr +"  </tr> ";
        
             
     // Body of the String
        String sItemname="";
        String sDyesPer="";
        String sDyesRate="";
        String sDyeCost="";
        String sPriceRevisionStatus="";
        double dTotalCost=0.0, dNewTotalCost=0.00;
       
     for(int i=0;i<AShadeList.size();i++){
          String sShadeCode=String.valueOf(AShadeList.get(i));
          SetCost(sShadeCode);
          int iMaxCount             = setMaxCount(sShadeCode);
          int iCostCount            = ACostList.size();
          theDyesList               = getDyesList(sShadeCode);
          theDyesNewPriceList       = getDyesNewRateList(sShadeCode);
          
         System.out.println("Cost Count==>"+iCostCount);
         
         sStr =sStr +"    <tr   bgcolor='pink'> ";   
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"' ><font color='#000099' ><b>"+(i+1)+"</b></font></td>";
         sStr =sStr +"    <td   align='left'   rowspan='"+iMaxCount+"' ><font color='#000099' ><b>"+sShadeCode +"</b></font></td>";
         
       if(iCostCount>0){
            java.util.HashMap theMap = (java.util.HashMap) ACostList.get(i);
         
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b>"+(String)theMap.get("ORDERRATE")+"</b></font></td>";
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b>"+(String)theMap.get("LABDYESCOST")+"</b></font></td>";
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b>"+(String)theMap.get("LABCHEMCOST")+"</b></font></td>";
       }
       else {
           
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b></b></font></td>";
         }
       
     
    for(int a=0;a<theDyesList.size();a++){
     java.util.HashMap theMap1 = (java.util.HashMap) theDyesList.get(a);
       sItemname            = (String)theMap1.get("ITEM_NAME");
       sDyesPer             = (String)theMap1.get("DYES_PER");
       sDyesRate            = (String)theMap1.get("DYES_RATE");
       sDyeCost             = (String)theMap1.get("DYES_COST");
       sPriceRevisionStatus = (String)theMap1.get("PRICEREVISIONSTATUS");
       //if(sPriceRevisionStatus.trim().equals("1")){
         sStr =sStr +"    <td   align='left'><font color='#000099'   ><b>"+sItemname+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyesPer+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyesRate+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyeCost+"</b></font></td>";
     
         dTotalCost         = dTotalCost+common.toDouble(sDyeCost);
      // }
      for(int b=a;b<theDyesNewPriceList.size();b++){
       java.util.HashMap theMap2 = (java.util.HashMap) theDyesNewPriceList.get(b);
       
       sItemname            = (String)theMap2.get("ITEM_NAME");
       sDyesPer             = (String)theMap2.get("DYES_PER");
       sDyesRate            = (String)theMap2.get("DYES_RATE");
       sDyeCost             = (String)theMap2.get("DYES_COST");
       sPriceRevisionStatus = (String)theMap2.get("PRICEREVISIONSTATUS");
       
           
         sStr =sStr +"    <td   align='left'><font color='#000099'   ><b>"+sItemname+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyesPer+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyesRate+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyeCost+"</b></font></td>";
     
         dNewTotalCost      = dNewTotalCost+common.toDouble(sDyeCost);
       break;
       }
        sStr =sStr +"  </tr> "; 
       }
         //sStr =sStr +"  </tr> "; 
         sStr =sStr +"  <tr> "; 
         sStr =sStr +"    <td   align='center' colspan='5'><font color='#000099' ><b>SHADE WISE TOTAL</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.getRound(dTotalCost,2)+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.getRound(dNewTotalCost,2)+"</b></font></td>";
         sStr =sStr +"  </tr> "; 
         dTotalCost = 0.00;
         dNewTotalCost = 0.00;
     }
        theDyesList.clear();
        theDyesNewPriceList.clear();
        
       
    }
         catch (Exception ex) {
         ex.printStackTrace();
     }
 return sStr;
    }
*/
  
  
  private String getReport(){
            String sStr = "";
            String sHeading ="Shade Cost Report";
    try {
            sStr = "<html><head>";
            sStr =sStr + "  </head>";
            sStr =sStr + "  <body>";
            sStr =sStr + "  <table border='1' width='100%' >";
            sStr =sStr + "  <tr  bgcolor='lightgreen'> ";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>SL NO </b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Shade Code</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Appd Rate</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Lab Dye Cost</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Lab Chem Cost</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' colspan='4'><font color='#000099' ><b>Old Recipe</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' colspan='4'><font color='#000099' ><b>New Recipe</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Difference</b></font></td>";
            sStr =sStr + "  <td align='center' color='#FF00FF' rowspan='2'><font color='#000099' ><b>Lab Status </b></font></td>";
            sStr =sStr +"  </tr> ";
            
            sStr =sStr +"  <tr> ";
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Dyes Name</b></font></td>"; 
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>%</b></font></td>"; 
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Cost</b></font></td>"; 
            
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Dyes Name</b></font></td>"; 
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>%</b></font></td>"; 
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Rate / kg</b></font></td>";
            sStr =sStr +"<td align='center' color='#FF00FF'><font color='#000099' ><b>Cost</b></font></td>"; 
                       
            sStr =sStr +"  </tr> ";
        
             
     // Body of the String
        String sItemname="";
        String sDyesPer="";
        String sDyesRate="";
        String sDyeCost="";
        String sPriceRevisionStatus="";
        String sLabStatus="";
        double dTotalCost=0.0, dNewTotalCost=0.00;
        //System.out.println("ShadeList Size==>"+AShadeList.size());
     for(int i=0;i<AShadeList.size();i++){
          String sShadeCode=String.valueOf(AShadeList.get(i));
          SetCost(sShadeCode);
          int iMaxCount1            = setMaxCount(sShadeCode);
          int iMaxCount             = getReportDyesList(sShadeCode);
          //System.out.println("MaxCount--->"+iMaxCount+"---"+sShadeCode);
          int iCostCount            = ACostList.size();
          theDyesList               = getDyesList(sShadeCode);
          theDyesNewPriceList       = getDyesNewRateList(sShadeCode);
          int iDyesRowSize          = getReportDyesList(sShadeCode);
          sLabStatus                = getLabStatus(sShadeCode);
           //System.out.println("Rowspan==>"+iMaxCount);
         sStr =sStr +"    <tr   bgcolor='pink'> ";   
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"' ><font color='#000099' ><b>"+(i+1)+"</b></font></td>";
         sStr =sStr +"    <td   align='left'   rowspan='"+iMaxCount+"' ><font color='#000099' ><b>"+sShadeCode +"</b></font></td>";
       
       if(iCostCount>0) {
           
         double dAppRate        = getAppRate(sShadeCode);
         double dLabDyeCost     = getLabDyeCost(sShadeCode);
         double dLabChemCost    = getLabChemCost(sShadeCode);
         
         
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b>"+dAppRate+"</b></font></td>";
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b>"+dLabDyeCost+"</b></font></td>";
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b>"+dLabChemCost+"</b></font></td>";
       }
       else {
           
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center' rowspan='"+iMaxCount+"'><font color='#000099' ><b></b></font></td>";
       //  break;
         }

  if(theDyesList.size()>0) {
      
    //for(int a=0;a<theDyesList.size();a++){
      for(int a=0;a<iDyesRowSize;a++){
       if(a<theDyesList.size()) {
      java.util.HashMap theMap1 = (java.util.HashMap) theDyesList.get(a);
       sItemname            = (String)theMap1.get("ITEM_NAME");
       sDyesPer             = (String)theMap1.get("DYES_PER");
       sDyesRate            = (String)theMap1.get("DYES_RATE");
       sDyeCost             = (String)theMap1.get("DYES_COST");
       sPriceRevisionStatus = (String)theMap1.get("PRICEREVISIONSTATUS");
      // sLabStatus           = (String)theMap1.get("LABSTATUS");
       
       DecimalFormat   df  = new DecimalFormat("0.000");
       String sPer         = df.format(Double.parseDouble(sDyesPer));
       String sCost        = df.format(Double.parseDouble(sDyeCost));        
       
           
       //if(sPriceRevisionStatus.trim().equals("1")){
         sStr =sStr +"    <td   align='left'><font color='#000099'   ><b>"+sItemname+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sPer+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyesRate+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sCost+"</b></font></td>";
     
         dTotalCost         = dTotalCost+common.toDouble(sDyeCost);
       }
       else{
         sStr =sStr +"    <td   align='left'><font color='#000099'   ><b>&nbsp</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
       }
         //System.out.println("Dyes New Price List Size==>"+theDyesNewPriceList.size());
    if(theDyesNewPriceList.size()>0){
      for(int b=a;b<theDyesNewPriceList.size();b++){
       if(a<theDyesNewPriceList.size()) {   
       java.util.HashMap theMap2 = (java.util.HashMap) theDyesNewPriceList.get(b);
       
       sItemname            = (String)theMap2.get("ITEM_NAME");
       sDyesPer             = (String)theMap2.get("DYES_PER");
       sDyesRate            = (String)theMap2.get("DYES_RATE");
       sDyeCost             = (String)theMap2.get("DYES_COST");
       sPriceRevisionStatus = (String)theMap2.get("PRICEREVISIONSTATUS");
       
       DecimalFormat   df1  = new DecimalFormat("0.000");
       String sPer1         = df1.format(Double.parseDouble(sDyesPer));
       String sCost1        = df1.format(Double.parseDouble(sDyeCost));        
       
         // System.out.println("Rate in Prceision=======>"+sDyesPer+"<-->"+sPer);
       
           
         sStr =sStr +"    <td   align='left'><font color='#000099'   ><b>"+sItemname+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sPer1+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyesRate+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sCost1+"</b></font></td>";
     
         dNewTotalCost      = dNewTotalCost+common.toDouble(sDyeCost);
         }
       else{
         sStr =sStr +"    <td   align='left'><font color='#000099'   ><b>&nbsp</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>&nbsp</b></font></td>";
       }
       break;
       }
    }
    else if(theDyesNewPriceList.size()== 0){
         sStr =sStr +"    <td   align='left'><font color='#000099'   ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
    }
      
        sStr =sStr +"  </tr> "; 
       }
    }
  
  
    else if(theDyesList.size()==0){
     if(theDyesNewPriceList.size()>0){
         
         System.out.println("New Price Size==>"+theDyesNewPriceList.size());
      for(int b=0;b<theDyesNewPriceList.size();b++){   
         sStr =sStr +"    <td   align='left'><font color='#000099'   ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         
      //for(int b=a;b<theDyesNewPriceList.size();b++){
       java.util.HashMap theMap2 = (java.util.HashMap) theDyesNewPriceList.get(b);
       
       sItemname            = (String)theMap2.get("ITEM_NAME");
       sDyesPer             = (String)theMap2.get("DYES_PER");
       sDyesRate            = (String)theMap2.get("DYES_RATE");
       sDyeCost             = (String)theMap2.get("DYES_COST");
       sPriceRevisionStatus = (String)theMap2.get("PRICEREVISIONSTATUS");
       
               
       DecimalFormat   df  = new DecimalFormat("0.000");
       String sPer         = df.format(Double.parseDouble(sDyesPer));
       String sCost        = df.format(Double.parseDouble(sDyeCost));        
       
       
         sStr =sStr +"    <td   align='left'><font color='#000099'   ><b>"+sItemname+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sPer+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sDyesRate+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sCost+"</b></font></td>";
     
         dNewTotalCost      = dNewTotalCost+common.toDouble(sDyeCost);
         sStr =sStr +"  </tr> "; 
       }
    }
    else if(theDyesNewPriceList.size()== 0){
         sStr =sStr +"    <td   align='left'><font color='#000099'   ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         
         sStr =sStr +"    <td   align='left'><font color='#000099'   ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"  </tr> "; 
       }
    }
         //sStr =sStr +"  </tr> "; 
         sStr =sStr +"  <tr> "; 
         sStr =sStr +"    <td   align='center' colspan='5'><font color='#000099' ><b>SHADE WISE TOTAL</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.getRound(dTotalCost,2)+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b></b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.getRound(dNewTotalCost,2)+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+common.getRound(dTotalCost-dNewTotalCost,2)+"</b></font></td>";
         sStr =sStr +"    <td   align='center'><font color='#000099' ><b>"+sLabStatus+"</b></font></td>";
         sStr =sStr +"    </tr> "; 
         
         dTotalCost = 0.00;
         dNewTotalCost = 0.00;
     }
        theDyesList.clear();
        theDyesNewPriceList.clear();
        
       
    }
         catch (Exception ex) {
         ex.printStackTrace();
     }
 return sStr;
   }
// Original
  
    public void SetDataIntoVector() {
        
        ADyesCodeList              . clear();
        ADyesList                  . clear();
        PreparedStatement pst      = null;
        ResultSet rst              = null;
        StringBuffer sb            = new StringBuffer();
        
        ADataList                  = new ArrayList();
      //  AShadeList                 = new ArrayList();
        ADataList                  . clear();
      //  AShadeList                 . clear();
        String sFromDate            = txtFromDate.getText();
        String sToDate              = txtToDate.getText();
        String sDyesName            = "";
        String sDyesCode            = "";
        
        //String sShadeCode           = txtShadeCode.getText();
        //AShadeList                  . add(sShadeCode);
       // System.out.println("Shade List in Datainto  Vector-->"+ADyesNameList.size());
        
        for(int i=0;i<ADyesNameList.size();i++){
           String sItemName         = (String)ADyesNameList.get(i);
           StringTokenizer     st   = new StringTokenizer(sItemName,"#") ;
           while (st.hasMoreTokens()){
              sDyesName             = st.nextToken();
              sDyesCode             = st.nextToken();
              ADyesCodeList         . add(sDyesCode);
              ADyesList             . add(sDyesName);
                }
              }
            sb.append(" Select shade_code,item_name,Dyes_per,Dyes_Rate,Dyes_cost,name,PriceRevisionStatus,");
            sb.append(" decode(LabStatus,1,'OK',2,'NOTOK',0,'NOTMADE') as LabStatus , RecipeStatus from SHADEWISEDYES ");
            sb.append(" inner join  invitems on invitems.ITEM_CODE =SHADEWISEDYES.DYES_CODE  ");
            sb.append(" inner join  stockgroup on groupcode = invitems.STKGROUPCODE   ");
            sb.append(" inner join SUPPLIER@amarml on AC_CODE= SHADEWISEDYES.PARTY_CODE");
            sb.append(" Inner Join FibreBase on  FibreBase.BaseCode = ShadeWiseDyes.Shade_Code and shade_code in ('");//J0074','M4384')");
               
           for(int i=0;i<AShadeList.size();i++){
               sfibre = (String)AShadeList.get(i);
                   if(i==0){
                   sb.append(""+sfibre+"' ");
                   }else{
                       sb.append(",'"+sfibre+"'");
                  }
               }
             sb.append(" )");
             /*
             sb.append(" and Dyes_Code in ( '");
             for(int i=0;i<ADyesCodeList.size();i++){
               String SDyesCode = (String)ADyesCodeList.get(i);
                   if(i==0){
                   sb.append(""+SDyesCode.trim()+"' ");
                   }else{
                       sb.append(",'"+SDyesCode.trim()+"'");
                  }
               }
           sb.append(" )");
             */
           sb.append( " and to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') <="+common.pureDate(sToDate)+" and  ");
           sb.append( " to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') >="+common.pureDate(sFromDate));
           sb.append( " and RecipeStatus=0  and WithEffectTo=99999999 " );
                   
           sb.append(" union all");
           
            sb.append(" Select shade_code,item_name,Dyes_per,Dyes_Rate,Dyes_cost,name,PriceRevisionStatus,");
            sb.append(" decode(LabStatus,1,'OK',2,'NOTOK',0,'NOTMADE') as LabStatus , RecipeStatus from SHADEWISEDYES ");
            sb.append(" inner join  invitems on invitems.ITEM_CODE =SHADEWISEDYES.DYES_CODE  ");
            sb.append(" inner join  stockgroup on groupcode = invitems.STKGROUPCODE   ");
            sb.append(" inner join SUPPLIER@amarml on AC_CODE= SHADEWISEDYES.PARTY_CODE");
            sb.append(" Inner Join FibreBase on  FibreBase.BaseCode = ShadeWiseDyes.Shade_Code and shade_code in ('");//J0074','M4384')");
               
           for(int i=0;i<AShadeList.size();i++){
               sfibre = (String)AShadeList.get(i);
                   if(i==0){
                   sb.append(""+sfibre+"' ");
                   }else{
                       sb.append(",'"+sfibre+"'");
                  }
               }
             sb.append(" )");
             /*
             sb.append(" and Dyes_Code in ( '");
             for(int i=0;i<ADyesCodeList.size();i++){
               String SDyesCode = (String)ADyesCodeList.get(i);
                   if(i==0){
                   sb.append(""+SDyesCode.trim()+"' ");
                   }else{
                       sb.append(",'"+SDyesCode.trim()+"'");
                  }
               }
           sb.append(" )");
             */
           //sb.append( " and to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') <="+common.pureDate(sToDate)+" and  ");
           //sb.append( " to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') >="+common.pureDate(sFromDate));
           sb.append( " and RecipeStatus=1 and PriceREvisionStatus=0 " );
           
           sb.append(" union all");
           
           sb.append(" Select shade_code,decode (Dyes_Item_Name,'',InvItems.ITEM_NAME,Dyes_InvItems.Dyes_Item_Name) as ItemName,");
           sb.append(" Dyes_per,Dyes_Rate,Dyes_cost,name, PriceRevisionStatus,");
           sb.append(" decode(LabStatus,0,'OK','NOTOK') as LabStatus,RecipeStatus from SHADEWISEDYES ");
           sb.append(" inner join  Dyes_invItems on Dyes_invItems.Dyes_ITEM_CODE =SHADEWISEDYES.DYES_CODE  ");
           sb.append(" inner join SUPPLIER@amarml on AC_CODE= SHADEWISEDYES.PARTY_CODE");
           sb.append(" Left Join InvItems on InvItems.Item_Code = Dyes_InvItems.Dyes_Item_Code ");
           sb.append(" Inner Join FibreBase on  FibreBase.BaseCode = ShadeWiseDyes.Shade_Code and shade_code in ('");//J0074','M4384')");
           
             for(int i=0;i<AShadeList.size();i++){
               sfibre = (String)AShadeList.get(i);
                   if(i==0)
                   {  
                   sb.append(""+sfibre+"' ");
                   }else{
                       sb.append(",'"+sfibre+"'");
                   }
                  }
              sb.append(" )");
              /*
              sb.append(" and Dyes_Code in ( '");
             for(int i=0;i<ADyesCodeList.size();i++){
               String SDyesCode = (String)ADyesCodeList.get(i);
                   if(i==0){
                   sb.append(""+SDyesCode.trim()+"' ");
                   }else{
                       sb.append(",'"+SDyesCode.trim()+"'");
                  }
               }
              sb.append(" )");
           */
              sb.append( " and to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') <="+common.pureDate(sToDate)+" and  ");
              sb.append( " to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') >="+common.pureDate(sFromDate));
              sb.append( "  Order by Item_Name Desc ");
             // sb.append( "  Order by PriceRevisionStatus Desc ");
              
  //     System.out.println("QS:" + sb.toString());
       
        try {

           if (theDyeConnection == null) {
                DyeJDBCConnection jdbc =  DyeJDBCConnection.getJDBCConnection();
                theDyeConnection          =  jdbc.getConnection();
            }
       
            pst = theDyeConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd = rst.getMetaData();
            while (rst.next()) {
                HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
                  
                ADataList.add(themap);
            }
          //  System.out.println("DatList Size-->"+ADataList.size());
            rst.close();
             
            pst.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
/*
  public void SetDataIntoVector() {
        
        ADyesCodeList              . clear();
        ADyesList                  . clear();
        PreparedStatement pst      = null;
        ResultSet rst              = null;
        StringBuffer sb            = new StringBuffer();
        
        ADataList                  = new ArrayList();
      //  AShadeList                 = new ArrayList();
        ADataList                  . clear();
      //  AShadeList                 . clear();
        String sFromDate            = txtFromDate.getText();
        String sToDate              = txtToDate.getText();
        String sDyesName            = "";
        String sDyesCode            = "";
        
        //String sShadeCode           = txtShadeCode.getText();
        //AShadeList                  . add(sShadeCode);
       // System.out.println("Shade List in Datainto  Vector-->"+ADyesNameList.size());
        
        for(int i=0;i<ADyesNameList.size();i++){
           String sItemName         = (String)ADyesNameList.get(i);
           StringTokenizer     st   = new StringTokenizer(sItemName,"#") ;
           while (st.hasMoreTokens()){
              sDyesName             = st.nextToken();
              sDyesCode             = st.nextToken();
              ADyesCodeList         . add(sDyesCode);
              ADyesList             . add(sDyesName);
                }
              }
            sb.append(" Select shade_code,item_name,Dyes_per,Dyes_Rate,Dyes_cost,name,PriceRevisionStatus,");
            sb.append(" decode(LabStatus,1,'OK',2,'NOTOK',0,'NOTMADE') as LabStatus , RecipeStatus from SHADEWISEDYES20171209 ");
            sb.append(" inner join  invitems on invitems.ITEM_CODE =SHADEWISEDYES20171209.DYES_CODE  ");
            sb.append(" inner join  stockgroup on groupcode = invitems.STKGROUPCODE   ");
            sb.append(" inner join SUPPLIER@amarml on AC_CODE= SHADEWISEDYES20171209.PARTY_CODE");
            sb.append(" Inner Join FibreBase on  FibreBase.BaseCode = SHADEWISEDYES20171209.Shade_Code and shade_code in ('");//J0074','M4384')");
               
           for(int i=0;i<AShadeList.size();i++){
               sfibre = (String)AShadeList.get(i);
                   if(i==0){
                   sb.append(""+sfibre+"' ");
                   }else{
                       sb.append(",'"+sfibre+"'");
                  }
               }
             sb.append(" )");
             sb.append(" and Dyes_Code in ( '");
             for(int i=0;i<ADyesCodeList.size();i++){
               String SDyesCode = (String)ADyesCodeList.get(i);
                   if(i==0){
                   sb.append(""+SDyesCode.trim()+"' ");
                   }else{
                       sb.append(",'"+SDyesCode.trim()+"'");
                  }
               }
           sb.append(" )");
             
    //       sb.append( " and to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') <="+common.pureDate(sToDate)+" and  ");
   //        sb.append( " to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') >="+common.pureDate(sFromDate));
           sb.append( " and RecipeStatus=0" );
                   
            sb.append(" union all");
           
            sb.append(" Select shade_code,item_name,Dyes_per,Dyes_Rate,Dyes_cost,name,PriceRevisionStatus,");
            sb.append(" decode(LabStatus,1,'OK',2,'NOTOK',0,'NOTMADE') as LabStatus , RecipeStatus from SHADEWISEDYES20171209 ");
            sb.append(" inner join  invitems on invitems.ITEM_CODE =SHADEWISEDYES20171209.DYES_CODE  ");
            sb.append(" inner join  stockgroup on groupcode = invitems.STKGROUPCODE   ");
            sb.append(" inner join SUPPLIER@amarml on AC_CODE= SHADEWISEDYES20171209.PARTY_CODE");
            sb.append(" Inner Join FibreBase on  FibreBase.BaseCode = SHADEWISEDYES20171209.Shade_Code and shade_code in ('");//J0074','M4384')");
               
           for(int i=0;i<AShadeList.size();i++){
               sfibre = (String)AShadeList.get(i);
                   if(i==0){
                   sb.append(""+sfibre+"' ");
                   }else{
                       sb.append(",'"+sfibre+"'");
                  }
               }
             sb.append(" )");
             sb.append(" and Dyes_Code in ( '");
             for(int i=0;i<ADyesCodeList.size();i++){
               String SDyesCode = (String)ADyesCodeList.get(i);
                   if(i==0){
                   sb.append(""+SDyesCode.trim()+"' ");
                   }else{
                       sb.append(",'"+SDyesCode.trim()+"'");
                  }
               }
           sb.append(" )");
           sb.append( " and RecipeStatus=1 and PriceREvisionStatus=0 " );
           sb.append( "  Order by Item_Name Desc ");
             // sb.append( "  Order by PriceRevisionStatus Desc ");
              
       System.out.println("QS:" + sb.toString());
       
        try {

           if (theDyeConnection == null) {
                DyeJDBCConnection jdbc =  DyeJDBCConnection.getJDBCConnection();
                theDyeConnection          =  jdbc.getConnection();
            }
       
            pst = theDyeConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd = rst.getMetaData();
            while (rst.next()) {
                HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
                  
                ADataList.add(themap);
            }
          //  System.out.println("DatList Size-->"+ADataList.size());
            rst.close();
             
            pst.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
      */
public int  getDyesPartyList( String SParty,String sShadeCode)
{
      ArrayList theList = null;
        theList = new ArrayList();
        theList.clear();
        int iCount=0;
       // System.out.println("Inside GetParty=>"+SParty+"----"+sShadeCode);
        for (int i = 0; i < ADataList.size(); i++) {
            HashMap themap = (HashMap) ADataList.get(i);

           String sPartyName = ((String) themap.get("NAME"));
             String sshade = ((String) themap.get("SHADE_CODE"));
             
           if ((SParty.equals(sPartyName)) && sShadeCode.equals(sshade))  {
               iCount       = iCount+1;
             //theList.add(themap);
            }
        }
        return iCount;
     
}


 public int  setMaxCount(String sShadeCode) {
        ADyesCodeList              = new ArrayList();   
        ADyesCodeList              . clear();
        ADyesList                  . clear();
              
        PreparedStatement pst      = null;
        ResultSet rst              = null;
        StringBuffer sb            = new StringBuffer();
        AMaxList                   = new ArrayList();
        AMaxList                    .clear();
        String      sFromDate       = txtFromDate.getText();
        String      sToDate         = txtToDate.getText();
     
     int icount=0;
     for(int i=0;i<ADyesNameList.size();i++){
           String sItemName         = (String)ADyesNameList.get(i);
           StringTokenizer     st   = new StringTokenizer(sItemName,"#") ;
           while (st.hasMoreTokens()){
              String sDyesName      = st.nextToken();
              String sDyesCode      = st.nextToken();
              ADyesCodeList         . add(sDyesCode);
              ADyesList             . add(sDyesName);
         //      System.out.println("DyesCode==>"+ADyesCodeList);
                }
              }
  //   System.out.println("DyesCode Size===>"+ADyesCodeList);
        try {

           sb.append("  Select Sum(Cnt) as MaxCount from (   select count(*) as Cnt,Name from  ShadewiseDyes ");
           sb.append( " inner join SUPPLIER@amarml on AC_CODE= SHADEWISEDYES.PARTY_CODE  where Shade_code ='"+sShadeCode+"'");
           
           sb.append( " and to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') <="+common.pureDate(sToDate)+" and  ");
           sb.append( " to_Char(to_Date(EntryDateTime,'YYYYMMDD HH24:Mi:SS'),'YYYYMMDD') >="+common.pureDate(sFromDate));
           sb.append( " and  RecipeStatus=1");// or PriceRevisionStatus=0 ) ");
         //    sb.append( " and  PriceRevisionStatus=1");// or PriceRevisionStatus=0 ) "); 04.10.2017
         //  sb.append( " and PriceRevisionStatus=0 ");
           sb.append("  and Dyes_Code in ( '");
             for(int i=0;i<ADyesCodeList.size();i++){
               String SDyesCode = (String)ADyesCodeList.get(i);
                   if(i==0){
                   sb.append(""+SDyesCode.trim()+"' ");
                   }else{
                       sb.append(",'"+SDyesCode.trim()+"'");
                  }
               }
            sb.append(" )");
            
            sb.append(" Group by Name ) ");
        //    System.out.println("countQry=>"+sb.toString());
              
             if (theDyeConnection == null) {
                DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                theDyeConnection = jdbc.getConnection();
            }
            pst = theDyeConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd = rst.getMetaData();
            while (rst.next()) {
               icount=rst.getInt(1);
            }
            rst.close();
            pst.close();
        } catch (Exception e) {
            System.out.println(e);
        }
         return icount;
    }
  public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
// heading
    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }
    
    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont,int iRowspan,float fHeight) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowspan);
        //c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
    
    
public int  getReportDyesList( String sShadeCode){
        int iSize=0;
        ArrayList theList   = null;
                  theList   = new ArrayList();
                  theList   . clear();

        ArrayList theListNew  = null;
                  theListNew  = new ArrayList();
                  theListNew  . clear();
                  
      //  System.out.println("DataList inside method==>"+ADataList.size()+"----"+sShadeCode);
        for (int i = 0; i < ADataList.size(); i++) {
            HashMap themap   = (HashMap) ADataList.get(i);

           String sItemName  = ((String) themap.get("ITEM_NAME"));
           String sDyesPer   = ((String) themap.get("DYES_PER"));
           String sShade     = ((String) themap.get("SHADE_CODE"));
           String sDyesRate  = ((String) themap.get("DYES_RATE"));
           String sDyesCost  = ((String) themap.get("DYES_COST"));
           String sDyesCode  = ((String) themap.get("DYES_CODE"));
           //String sPriceRevisionStatus  = ((String) themap.get("PRICEREVISIONSTATUS"));
           String sPriceRevisionStatus  = ((String) themap.get("RECIPESTATUS"));
           // System.out.println("ShadeCode-->"+sShade+"--"+sShadeCode);
             if(sPriceRevisionStatus.trim().equals("1")){
             if (sShadeCode.equals(sShade))  {
               theList.add(themap);
              }
            }
           if(sPriceRevisionStatus.trim().equals("0")){
             if (sShadeCode.equals(sShade))  {
               theListNew.add(themap);
              }
            }
           //System.out.println("List Size-inside method->"+theList.size()+"--"+theListNew.size());
           if(theList.size()>theListNew.size())
               iSize        = theList.size();
           else
               iSize        = theListNew.size();
            
        }
      //  System.out.println("Report Size==>"+iSize);
      return iSize;
}
    
    
public ArrayList  getDyesList( String sShadeCode){
    
        ArrayList theList   = null;
                  theList   = new ArrayList();
                  theList   . clear();

        ArrayList theListNew  = null;
                  theListNew  = new ArrayList();
                  theListNew  . clear();
                  
   //     System.out.println("DataList==>"+ADataList.size()+"----"+sShadeCode);
        for (int i = 0; i < ADataList.size(); i++) {
            HashMap themap   = (HashMap) ADataList.get(i);

           String sItemName  = ((String) themap.get("ITEM_NAME"));
           String sDyesPer   = ((String) themap.get("DYES_PER"));
           String sShade     = ((String) themap.get("SHADE_CODE"));
           String sDyesRate  = ((String) themap.get("DYES_RATE"));
           String sDyesCost  = ((String) themap.get("DYES_COST"));
           String sDyesCode  = ((String) themap.get("DYES_CODE"));
           String sPriceRevisionStatus  = ((String) themap.get("RECIPESTATUS"));
           //String sPriceRevisionStatus  = ((String) themap.get("PRICEREVISIONSTATUS"));
            if(sPriceRevisionStatus.trim().equals("1")){
             if (sShadeCode.equals(sShade))  {
               theList.add(themap);
              }
            }
           if(sPriceRevisionStatus.trim().equals("0")){
             if (sShadeCode.equals(sShade))  {
               theListNew.add(themap);
              }
            }
         //   System.out.println("List Size-->"+theList.size()+"--"+theListNew.size());
        }
      return theList;
}

public ArrayList  getDyesNewRateList( String sShadeCode){
    
        ArrayList theList   = null;
                  theList   = new ArrayList();
                  theList   .clear();
       // System.out.println("DataList==>"+ADataList.size());
        for (int i = 0; i < ADataList.size(); i++) {
            HashMap themap   = (HashMap) ADataList.get(i);

           String sItemName  = ((String) themap.get("ITEM_NAME"));
           String sDyesPer   = ((String) themap.get("DYES_PER"));
           String sShade     = ((String) themap.get("SHADE_CODE"));
           String sDyesRate  = ((String) themap.get("DYES_RATE"));
           String sDyesCost  = ((String) themap.get("DYES_COST"));
           String sDyesCode  = ((String) themap.get("DYES_CODE"));
           String sPriceRevisionStatus  = ((String) themap.get("RECIPESTATUS"));
          // String sPriceRevisionStatus  = ((String) themap.get("PRICEREVISIONSTATUS"));
            if(sPriceRevisionStatus.trim().equals("0")){
             if (sShadeCode.equals(sShade))  {
               theList.add(themap);
              }
            }
        }
        return theList;
     
}

public ArrayList  getDyesList( String SParty,String sShadeCode){
    //System.out.println("Get DyesList");
       ArrayList theList = null;
        theList = new ArrayList();
        theList.clear();
      //  System.out.println("DataList==>"+ADataList.size());
        for (int i = 0; i < ADataList.size(); i++) {
            HashMap themap   = (HashMap) ADataList.get(i);

           String sPartyName = ((String) themap.get("NAME"));
            String sshade    = ((String) themap.get("SHADE_CODE"));
            
              if ((SParty.equals(sPartyName) && sShadeCode.equals(sshade)) ) {
             //System.out.println("SParty==>"+SParty+":"+sPartyName+"===>"+sShadeCode+":"+sshade);
        
             theList.add(themap);
            }
        }
        return theList;
     
}

public String getLabStatus( String sShadeCode){
       String sLabStatus="";  
        for (int i = 0; i < ADataList.size(); i++) {
          HashMap themap         = (HashMap) ADataList.get(i);
            String sshade        = ((String) themap.get("SHADE_CODE"));
          //  String sPriceRevisionStatus  = ((String) themap.get("PRICEREVISIONSTATUS"));
            String sPriceRevisionStatus  = ((String) themap.get("RECIPESTATUS"));
             if (sShadeCode.equals(sshade) && sPriceRevisionStatus.equals("0"))  {
                    sLabStatus   = ((String) themap.get("LABSTATUS"));
              }
        }
        return sLabStatus;
}

private void CreatePDFFile(){
      try{
         //   String SPDFFile = "/root/ShadeCost.pdf";
            String SPDFFile = "D:/ShadeCost.pdf";
           // String SPDFFile = common.getPrintPath()+"ShadeCostComparision.pdf";      
            document        = new Document();
            PdfWriter       .getInstance(document, new FileOutputStream(SPDFFile));
            document        .setPageSize(PageSize.A4.rotate());
            document        .open();
            int iColSize=4*2;
          
            iTotalColumns  = iColSize+7;
             table         = new PdfPTable(iTotalColumns);
             iColWidth     = new int[iTotalColumns];
             
             iColWidth[0] = 6;
             iColWidth[1] = 25;
             iColWidth[2] = 10;
             iColWidth[3] = 10;
             iColWidth[4] = 10;
             
             for (int j = 0; j < iColSize; j++) {
               iColWidth[j+5] = 25;
            }
            int iDiffColSize = 5+iColSize;
            iColWidth[iDiffColSize+0] = 17;
            iColWidth[iDiffColSize+1] = 15;
            
            table.setWidths(iColWidth);
            table.setWidthPercentage(100f);
            table.setHeaderRows(3);
            
           String sDate= common.getServerDate();

            AddCellIntoTable("Shade Cost Report "+common.parseDate(sDate)+"", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns , 0, 0, 0, 0, bigbold);
           //  AddCellIntoTable("DATE", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,2,20f);
            AddCellIntoTable("Sl.No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Shade Code", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Appd Rate", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Lab Dye Cost", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Lab Chem Cost", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Old Recipe", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 4 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("New Recipe", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 4 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("Difference", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("Lab Status", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,2);
            
            
            AddCellIntoTable("Dyes Name", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("%", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("Rate", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("Cost", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            
            AddCellIntoTable("Dyes Name", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("%", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("Rate", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            AddCellIntoTable("Cost", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold,1);
            
           PDFBody();
      }catch(Exception ex){
       ex.printStackTrace();   
      }
  }

  private void PDFBody(){
        int icountDiff=0;
        String sItemname="";
        String sDyesPer="";
        String sDyesRate="";
        String sDyeCost="";
        String sPriceRevisionStatus="", sLabStatus="";
        double dTotalCost=0.00, dNewTotalCost=0.00;
        
       for(int i=0;i<AShadeList.size();i++){
          String sShadeCode=String.valueOf(AShadeList.get(i));
          SetCost(sShadeCode);
          //int iMaxCount             = setMaxCount(sShadeCode);
          
          int iMaxCount             = getReportDyesList(sShadeCode);
          int iCostCount            = ACostList.size();
          theDyesList               = getDyesList(sShadeCode);
          theDyesNewPriceList       = getDyesNewRateList(sShadeCode);
          int iDyesRowSize          = getReportDyesList(sShadeCode);
          sLabStatus                = getLabStatus(sShadeCode);
          
            
           AddCellIntoTable(String.valueOf(i+1), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
           AddCellIntoTable(String.valueOf(AShadeList.get(i)), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
           
         if(iCostCount>0){
           double dAppRate        = getAppRate(sShadeCode);
           double dLabDyeCost     = getLabDyeCost(sShadeCode);
           double dLabChemCost    = getLabChemCost(sShadeCode);
                    
           AddCellIntoTable(String.valueOf(String.valueOf(dAppRate)), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
           AddCellIntoTable(String.valueOf(String.valueOf(dLabDyeCost)), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
           AddCellIntoTable(String.valueOf(String.valueOf(dLabChemCost)), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
          }
         else{
          AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
          AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
          AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,iMaxCount);
          }
    //       System.out.println("DyesRowSize-->"+iDyesRowSize+"--old--"+theDyesList.size()+"---New--"+theDyesNewPriceList.size());
           
       for(int a=0;a<iDyesRowSize;a++) {
         if(a<theDyesList.size()) {
             DecimalFormat   df  = new DecimalFormat("0.000");
                   
            java.util.HashMap theMap1 = (java.util.HashMap) theDyesList.get(a);
                 sItemname            = (String)theMap1.get("ITEM_NAME");
                 sDyesPer             = (String)theMap1.get("DYES_PER");
                 sDyesRate            = (String)theMap1.get("DYES_RATE");
                 sDyeCost             = (String)theMap1.get("DYES_COST");
                 sPriceRevisionStatus = (String)theMap1.get("PRICEREVISIONSTATUS");
                 
                 
                AddCellIntoTable(sItemname, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(df.format(Double.parseDouble(sDyesPer)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(df.format(Double.parseDouble(sDyesRate)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(df.format(Double.parseDouble(sDyeCost)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                
                dTotalCost         = dTotalCost+common.toDouble(sDyeCost);
                }
             else{
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                //   AddCellIntoTable("5", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                } 
         
            for(int b=a;b<theDyesNewPriceList.size();b++){
              if(b<theDyesNewPriceList.size()) {  
              java.util.HashMap theMap2 = (java.util.HashMap) theDyesNewPriceList.get(b);
                DecimalFormat   df  = new DecimalFormat("0.000");
                sItemname            = (String)theMap2.get("ITEM_NAME");
                sDyesPer             = (String)theMap2.get("DYES_PER");
                sDyesRate            = (String)theMap2.get("DYES_RATE");
                sDyeCost             = (String)theMap2.get("DYES_COST");
                sPriceRevisionStatus = (String)theMap2.get("PRICEREVISIONSTATUS");
       
                int iNewSize         = theDyesNewPriceList.size();
            //     System.out.println("NewSize==>"+iNewSize+"---"+a+1);
                /* if(iNewSize<(a+1)) 
                 {
                   System.out.println("L0");
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                 }*/
                 
                AddCellIntoTable(sItemname, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(df.format(Double.parseDouble(sDyesPer)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(df.format(Double.parseDouble(sDyesRate)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(df.format(Double.parseDouble(sDyeCost)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                dNewTotalCost      = dNewTotalCost+common.toDouble(sDyeCost);
                break;
                 }
              else{
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                  } 
                 }
               }
              
               
                if(theDyesList.size()>theDyesNewPriceList.size()){
                   int iOldSize     = theDyesList.size();
                   int iNewSize     = theDyesNewPriceList.size();
                   int iRowSize     = iOldSize-iNewSize;
                   
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                }
              /*  else if(theDyesList.size()<theDyesNewPriceList.size()){
                    System.out.println("Inside else");
                   int iOldSize     = theDyesList.size();
                   int iNewSize     = theDyesNewPriceList.size();
                   int iRowSize     = iOldSize-iNewSize;
                   
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                   AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                }*/
       
                AddCellIntoTable("SHADE WISE TOTAL", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 5 , 4, 1, 8, 2, mediumbold,1);
 
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(common.getRound(String.valueOf(dTotalCost),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
                
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(common.getRound(String.valueOf(dNewTotalCost),2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
                AddCellIntoTable(common.getRound(String.valueOf(dTotalCost-dNewTotalCost),2), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, mediumbold);
                AddCellIntoTable(sLabStatus, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal);
                
                dTotalCost = 0.00;
                dNewTotalCost = 0.00;
            }
               theDyesList.clear();
               theDyesNewPriceList.clear();
       try{
       document.add(table);
       document.close();
       }catch(Exception ex){
           
       }
      
    }
  
  
  
    private double getCost(String SParty,String sShadeCode ){
     double dcost=0.0;
       for (int i = 0; i < ADataList.size(); i++) {
            HashMap themap = (HashMap) ADataList.get(i);

           String sPartyName = ((String) themap.get("NAME"));
             String sshade = ((String) themap.get("SHADE_CODE"));
           if ((SParty.equals(sPartyName)) && sShadeCode.equals(sshade))  {
               //System.out.println("SParty"+SParty+"<><><>"+sShadeCode+"=="+sshade);
             dcost+=(common.toDouble(common.parseNull((String)themap.get("DYES_COST"))));
               
           }
       }
    
     return dcost;
 }     
      
 
   public void SetCost(String sShadeCode) 
    {
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuffer sb = new StringBuffer();
        ACostList = new ArrayList();
        ACostList.clear();
        
        String      sFromDate       = txtFromDate.getText();
        String      sToDate         = txtToDate.getText();
        //System.out.println("Date===>"+sFromDate+"--"+sToDate);
       
        sb.append("  Select count(1), OrderRate,LabDyesCost,LabChemCost,FibreCode from FibreLotCost ");
        sb.append("  inner join dyeingorder on dyeingorder.id = fibrelotcost.orderid   ");
        sb.append("  Where FibreLotCost.FibreCode = '"+sShadeCode+"'" );
        sb.append("  and OrderID = (Select Max(OrderID) from FibreLotCost Where FibreCode = '"+sShadeCode+"' )");
        sb.append(" Group by OrderRate,LabDyesCost,LabChemCost,FibreCode ");
                
        //sb.append("  inner join dyeingorderdetails on dyeingorder.orderno=dyeingorderdetails.dyeingorderno ");
        /* sb.append("  Where FibreLotCost.FibreCode in ('");
        for(int i=0;i<AShadeList.size();i++){
               sfibre = (String)AShadeList.get(i);
                   if(i==0){
                   sb.append(""+sfibre+"' ");
                   }else{
                       sb.append(",'"+sfibre+"'");
                   }
                  }
        sb.append(" ) and UnLoadDate>=20120101");*/
        
        
        
       /*
        sb.append("  Select distinct dyeingorderdetails.fibrecode as fibrecode ,fibrelotcost.LABDYESCOST as LABDYESCOST,fibrelotcost.LABCHEMCOST as LABCHEMCOST,");
        sb.append("  fibrelotcost.ORDERRATE as ORDERRATE from fibrelotcost  ");
        sb.append("  inner join partymaster on partymaster.partycode = fibrelotcost.partycode ");
        sb.append("  inner join dyeingorder on dyeingorder.id = fibrelotcost.orderid ");
        sb.append("  inner join dyeingorderdetails on dyeingorder.orderno=dyeingorderdetails.dyeingorderno ");
        sb.append("  where dyeingorder.orderno in(select distinct orderno from (select orderno,unloaddate from fibrelotcost ");
        sb.append("  where unloaddate>="+common.pureDate(sFromDate)+" and unloaddate<="+common.pureDate(sToDate));
        sb.append("  )) and unloaddate<="+common.pureDate(sToDate)+"  and dyeingorderdetails.fibrecode in ('");
        for(int i=0;i<AShadeList.size();i++){
               sfibre = (String)AShadeList.get(i);
                   if(i==0){
                   sb.append(""+sfibre+"' ");
                   }else{
                       sb.append(",'"+sfibre+"'");
                   }
                  }
        sb.append(" )");
       */    
       //  System.out.println("cost QS=====>:" + sb.toString());
         
        try {

            if (theDyeConnection == null) {
                DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                theDyeConnection       = jdbc.getConnection();
            }
            pst = theDyeConnection.prepareStatement(sb.toString());
            
            rst = pst.executeQuery();
            ResultSetMetaData rsmd = rst.getMetaData();
            while (rst.next()) {
                HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
                ACostList.add(themap);
            }
            rst.close();
            pst.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
   
 private double getAppRate(String sShadeCode){
     double dValue = 0.00;
       for(int i=0;i<ACostList.size();i++){
         java.util.HashMap theMap = (java.util.HashMap) ACostList.get(i);
         String sShCode           = (String)theMap.get("FIBRECODE");
         if(sShadeCode.trim().equals(sShCode)){
                        dValue    = common.toDouble(String.valueOf(theMap.get("ORDERRATE")));
         }
       }
       return dValue;
   }

  private double getLabDyeCost(String sShadeCode){
     double dValue = 0.00;

       for(int i=0;i<ACostList.size();i++){
         java.util.HashMap theMap = (java.util.HashMap) ACostList.get(i);
         String sShCode           = (String)theMap.get("FIBRECODE");
         if(sShadeCode.trim().equals(sShCode)){
                        dValue    = common.toDouble(String.valueOf(theMap.get("LABDYESCOST")));
         }
       }
       return dValue;
   }
  
  private double getLabChemCost(String sShadeCode){
     double dValue = 0.00;
       for(int i=0;i<ACostList.size();i++){
         java.util.HashMap theMap = (java.util.HashMap) ACostList.get(i);
         String sShCode           = (String)theMap.get("FIBRECODE");
         if(sShadeCode.trim().equals(sShCode)){
                        dValue    = common.toDouble(String.valueOf(theMap.get("LABCHEMCOST")));
         }
       }
       return dValue;
   }
    private void updatedetails(){
    
         StringBuffer sb = new StringBuffer();
       // System.out.println("Enter into update QS");
          try{
              theDyeConnection             . setAutoCommit(false);
              String sLabSt                 = "0";
              String sLabStatus             = (String)JCok.getSelectedItem();
              if(sLabStatus.trim().equals("OK"))
                  sLabSt                    = "1";
              else
                  sLabSt                    = "2";
              
        
            sb.append(" Update ShadeWiseDyes set labstatus='"+sLabSt+"'  where  PRICEREVISIONSTATUS = 0");
            sb.append(" and Dyes_code in ('");
             
            for(int i=0;i<ADyesCodeList.size();i++){
               String SDyesCode = (String)ADyesCodeList.get(i);
                   if(i==0){
                  sb.append(""+SDyesCode.trim()+"' ");
                   }else{
                     sb.append(",'"+SDyesCode.trim()+"'");
                  }
               }
            sb.append(" )");
            sb.append(" and Shade_Code in ('");
            
              for(int i=0;i<AShadeList.size();i++){
               sfibre = (String)AShadeList.get(i);
                   if(i==0){
                  sb.append(""+sfibre+"'");
                   }else{
                     sb.append(",'"+sfibre+"'");
                  }
               }
           sb.append(" )");

    //     System.out.println("updateqs===>"+sb.toString());
           
            if (theDyeConnection == null) {
                DyeJDBCConnection jdbc = DyeJDBCConnection.getJDBCConnection();
                theDyeConnection       = jdbc.getConnection();
            }
            PreparedStatement ps      = theDyeConnection.prepareStatement(sb.toString());
              ps                        . executeQuery();
              ps                        . close();
              ps                        = null;
              
              System.out.println("connected");
              theDyeConnection            . commit();
              theDyeConnection            . setAutoCommit(true);
              
              JOptionPane.showMessageDialog(null,"Data has been updated Successfully ","Dear User",JOptionPane.INFORMATION_MESSAGE);

              
              }
              catch(Exception e)
                 { e.printStackTrace();}
  }
    
/*public static void main (String args[])    {
    ShadeWiseMultipleDyesReport     report      = new ShadeWiseMultipleDyesReport();
    report.setVisible(true);
}*/

}
