
package DyesPriceRevision;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import javax.swing.border.*;
import java.awt.event.*;
import javax.swing.table.TableRowSorter;
import javax.swing.table.*;
import java.io.IOException;
import java.net.*;
import guiutil.*;
import java.rmi.RemoteException;
import javax.swing.event.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.JEditorPane;


public class ShadewiseDyesEntryFrame extends JFrame {
    JPanel                  pnlMain,pnlEntry,pnlData,pnlControl,pnlInsert,pnlLeft,pnlTop;
    JRadioButton            rbtnInsert, rbtnUpdate;
    JComboBox               cmbBaseName, cmbSupplierName;
    JTextField              txtNewPartyName,txtDyesPercentage,txtDyesRate,txtDyesCost,txtDyesName;
    JButton                 btnSave,btnExit,btnPartySave,btnPDFCreation;
    AddressField            addField  =  new AddressField();
    String[]                ColumnName, ColumnType, ColumnName1, ColumnType1, ColumnName2, ColumnType2;
    int[]                   iColumnWidth, iColumnWidth1, iColumnWidth2;
    JCheckBox               chkNewParty;
    String[]                sBody;
    JSplitPane              spltPane;
    ShadeWiseDyeCostModel   theModel;
    
    JTable                  theTable;
    TableRowSorter<TableModel> sorter;
    Common                  common  =  new Common();
    DyeingData              data    =  new DyeingData();
    String                  sAuthSysName="";
    ArrayList               ADyesNameList,ADyesCostList;
    JTabbedPane             tabbedPan;
    JEditorPane             pnlEditor;
    JList                   theDyesList;
    DefaultListModel        theDyesListModel;
    DyeList                 dyelist;
    
public ShadewiseDyesEntryFrame() {
    createComponent();
    addLayout();
    addListener();
    addComponent();
    setTableData();
    try{
          sAuthSysName=InetAddress.getLocalHost().getHostName();
       }
     catch(Exception e){
          e.printStackTrace();
       }
}

private void createComponent(){
    pnlMain             =       new JPanel();
    pnlData             =       new JPanel();
    pnlEntry            =       new JPanel();
    pnlInsert           =       new JPanel();
    pnlControl          =       new JPanel();
    pnlLeft             =       new JPanel();
    pnlTop              =       new JPanel();
    
    rbtnInsert          =       new JRadioButton("Insert", true);
    rbtnUpdate          =       new JRadioButton("Update", true);
    cmbBaseName         =       new JComboBox(data.VBaseName);
    
    cmbSupplierName     =       new JComboBox(data.VSupName);
    chkNewParty         =       new JCheckBox(" New Party", false);
    
    btnSave             =       new JButton("Save");
    btnPartySave        =       new JButton("New Party Save");
    btnExit             =       new JButton("Exit");
    btnPDFCreation      =       new JButton("Create PDF");
    txtDyesPercentage   =       new JTextField(10);
    txtDyesRate         =       new JTextField(10);
    txtDyesCost         =       new JTextField(10);
    txtDyesName         =       new JTextField(10);
    
    theModel            =       new ShadeWiseDyeCostModel();
    theTable            =       new JTable(theModel);
    
    theDyesListModel    =       new DefaultListModel();
    theDyesList         =       new JList(theDyesListModel);
    theDyesList         .       setVisibleRowCount(4);
    
    spltPane            =       new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    
    //tabbedPan           =       new JTabbedPane();
    //pnlEditor           =       new JEditorPane();
    
}
private void addListener(){
          theTable        .  addKeyListener(new KyList());
          theDyesList     .  addKeyListener(new KeyList());
          txtDyesName     .  addKeyListener(new KeyList());  
          txtDyesPercentage .  addFocusListener(new FocusList());
          txtDyesRate     .  addFocusListener(new FocusList());
          txtDyesCost     .  addFocusListener(new FocusList());
          chkNewParty     . addActionListener(new ActList());
          btnPartySave    . addActionListener(new ActList());
          btnSave         . addActionListener(new ActList());
          cmbSupplierName . addItemListener (new ItemListen());
}
private void addLayout(){
        pnlEntry        .       setBorder(new TitledBorder("Price Entry"));
        pnlEntry        .       setLayout(new GridBagLayout());
        pnlData         .       setBorder(new TitledBorder("Data"));
        pnlData         .       setLayout(new BorderLayout());
        
        pnlControl      .       setBorder(new TitledBorder("Selected OrderType"));
        pnlControl      .       setLayout(new FlowLayout(FlowLayout.CENTER));
        pnlTop          .       setLayout(new GridLayout(2, 1));
        pnlLeft         .       setLayout(new BorderLayout());
        
        spltPane        .       setOneTouchExpandable(true);
        spltPane        .       setDividerLocation(450);
        spltPane        .       setDividerSize(0);
        
        pnlMain         .       setLayout(new GridLayout(1, 2));
        
        this            .       setSize(1050, 600);
        this            .       setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this            .       setTitle("SHADE WISE DYES ENTRY FRAME");
}

private void addComponent(){
    Font font            =       new Font("Courier", Font.BOLD, 12);
    GridBagConstraints c =       new GridBagConstraints();
        c.fill           =       GridBagConstraints.HORIZONTAL;

        c.gridx          =       0;
        c.gridy          =       0;
        c.weightx        =       2;
        c.weighty        =       2;
        c.ipady          =       15;

        pnlEntry        .        add(new JLabel(""), c);

        ButtonGroup group = new ButtonGroup();
        group           .       add(rbtnInsert);
        pnlInsert       .       add(rbtnInsert);
       /* group           .       add(rbtnUpdate);
        pnlInsert       .       add(rbtnUpdate);*/

        c.gridx         =       1;
        c.gridy         =       0;
        pnlEntry        .       add(pnlInsert, c);
        
        c.gridx         =       0;
        c.gridy         =       1;
        JLabel lblPName =       new JLabel("Party Name");
        lblPName        .      setForeground(Color.RED);
        pnlEntry        .      add(lblPName, c);

        c.gridx         =       1;
        c.gridy         =       1;
        cmbSupplierName .       setFont(font);
        pnlEntry        .       add(cmbSupplierName, c);

        c.gridx         =       0;
        c.gridy         =       2;
        JLabel lblBaseName =   new JLabel("Base Name");
        lblBaseName     .      setForeground(Color.RED);
        pnlEntry        .      add(lblBaseName, c);

        c.gridx         =       1;
        c.gridy         =       2;
        cmbBaseName     .       setFont(font);
        pnlEntry        .       add(cmbBaseName, c);
        
        c.gridx         =       1;
        c.gridy         =       3;
        chkNewParty     .       setFont(font);
        pnlEntry        .       add(chkNewParty, c);
    
        c.gridx         =       0;
        c.gridy         =       4;
        btnPartySave    .       setFont(font);
        pnlEntry        .       add(btnPartySave, c);
        
        c.gridx         =       1;
        c.gridy         =       4;
        addField        .       setEditable(false);
        pnlEntry        .       add(addField, c);

        c.gridx         =       0;
        c.gridy         =       5;
        JLabel lblDyesList =   new JLabel("Dyes Name");
        lblDyesList     .      setForeground(Color.RED);
        pnlEntry        .      add(lblDyesList, c);

        c.gridx         =       1;
        c.gridy         =       5;
        txtDyesName     .       setFont(font);
        pnlEntry        .       add(txtDyesName, c);
    //    theDyesList     .       setFont(font);
      //  pnlEntry        .       add(theDyesList, c);
    
        c.gridx         =       0;
        c.gridy         =       6;
        JLabel lblDyePer=      new JLabel("%");
        lblDyePer       .      setForeground(Color.RED);
        pnlEntry        .      add(lblDyePer, c);

        c.gridx         =       1;
        c.gridy         =       6;
        txtDyesPercentage .     setFont(font);
        pnlEntry        .       add(txtDyesPercentage, c);
    
        c.gridx         =       0;
        c.gridy         =       7;
        JLabel lblRate  =     new JLabel("Rate");
        lblRate         .      setForeground(Color.RED);
        pnlEntry        .      add(lblRate, c);

        c.gridx         =       1;
        c.gridy         =       7;
        txtDyesRate     .       setFont(font);
        pnlEntry        .       add(txtDyesRate, c);
    
        c.gridx         =       0;
        c.gridy         =       8;
        JLabel lblCost  =      new JLabel("Cost");
        lblCost         .      setForeground(Color.RED);
        pnlEntry        .      add(lblCost, c);

        c.gridx         =       1;
        c.gridy         =       8;
        txtDyesCost     .       setFont(font);
        pnlEntry        .       add(txtDyesCost, c);
        
        
        c.gridx         =       0;
        c.gridy         =       14;
        pnlEntry        .       add(btnSave, c);

        c.gridx         =       1;
        c.gridy         =       14;
        pnlEntry        .       add(btnExit, c);
         
        pnlData         .       add(new JScrollPane(theTable), BorderLayout.CENTER);
        
        pnlTop          .       add(pnlEntry, BorderLayout.NORTH);
        pnlLeft         .       add(pnlTop, BorderLayout.NORTH);
        pnlLeft         .       add(new JScrollPane(pnlTop));
        
 /*     tabbedPan         .       addTab("DYES ENTRY", pnlData);
        tabbedPan         .       addTab("PRINT PREVIEW", pnlEditor);
        spltPane.add(pnlLeft);
        spltPane.add(new JScrollPane(tabbedPan));*/
        
        spltPane        .       add(pnlLeft);
        spltPane        .       add(pnlData);
        this            .       add(spltPane);
}

private class  ActList implements ActionListener{
  public void actionPerformed (ActionEvent ae){
      if(ae.getSource()== btnSave){
        int iSaveValue= -1;
        String sDyesPartyName       = "";
        String sNewDyesParty        = addField.getText();
        
       if(chkNewParty.isSelected()){
         if(sNewDyesParty.length()>0){
           sDyesPartyName           =  sNewDyesParty;
         }
        }
       else{
           sDyesPartyName           = (String)cmbSupplierName.getSelectedItem();
       }
        
        //String sSupplierName       = (String)cmbSupplierName.getSelectedItem();
        //String sDyesName           = (String)cmbDyesName.getSelectedItem();
        int iSupplierCode            = data.getSupplierCode(sDyesPartyName);
        //String sDyesCode           = data.getDyesCode(sDyesPartyName);
        
        /*if (rbtnInsert.isSelected()) {
        int iCount                  = data.getDyesCount(iSupplierCode, sDyesCode);
        if(iCount>0){
            int iMaxPriceRevisionID = data.getMaxPriceRevisionStatus(iSupplierCode,sDyesCode);
            int iUpdateStatus       = data.updatePriceRevisionStatus(iSupplierCode,sDyesCode,iMaxPriceRevisionID);
           }
         }*/
         iSaveValue                 = getSave();
        
          if (iSaveValue == 0) {
            JOptionPane.showMessageDialog(null, "Data Saved", "Information", JOptionPane.INFORMATION_MESSAGE);
             
             setTableData();
             //setRefresh();
           } 
            else if (iSaveValue == 2) {
              JOptionPane.showMessageDialog(null, "Data Already Exist", "Information", JOptionPane.INFORMATION_MESSAGE);
           }
    }
    if(ae.getSource()==btnExit){
          dispose();
      }
      if(ae.getSource()== btnPartySave){
          setSaveNewParty ();
          data.setDyes();
      }
      if(ae.getSource()== chkNewParty) {
          if (chkNewParty.isSelected()){
            addField.setEditable(true);
          }
          else{
            addField.setEditable(false);
          }
      }
      if(ae.getSource()== btnPDFCreation){
          
      }
    }
}

private class ItemListen implements ItemListener {
 public void itemStateChanged(ItemEvent e) {
 
    if (e.getSource() == cmbSupplierName) {
 //           setRefresh();
            setTableData();
     }
   }
}

private class KyList extends KeyAdapter{
  public void keyReleased(KeyEvent ke)  {
    if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
        
        int iSelectedRow=0, iRow=0, iEndRow=0;
        //   iRow = theTable.getSelectedRow();
        iEndRow      = theModel.getRowCount()-1;
        iSelectedRow = theTable.getSelectedRow();
        if(iSelectedRow==1)
            iRow = 0;
        else if(iSelectedRow>1)
            iRow = theTable.getSelectedRow()-1;
        else
            iRow = iEndRow;

            try {
                    rbtnUpdate.setSelected(true);
                    String sShadeName       = (String) theModel.getValueAt(iRow, 1);
                    String sDyesName        = (String) theModel.getValueAt(iRow, 2);
                    String sPercentage      = (String) theModel.getValueAt(iRow, 3);
                    String sRate            = (String) theModel.getValueAt(iRow, 4);
                    String sCost            = (String) theModel.getValueAt(iRow, 5);
                    txtDyesPercentage       . setText(sPercentage);
                    txtDyesRate             . setText(sRate);
                    txtDyesCost             . setText(sCost);
                    txtDyesName             . setText(sDyesName);
                 
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Enter-->:" + e);
                }
           }
      }
}

private class KeyList extends KeyAdapter{
   public KeyList(){
        
    }
   
public void keyReleased(KeyEvent ke)  {
 if(ke.getKeyCode()==KeyEvent.VK_F2) {
    try{
           int iRow=0;
           String sPartyName    =  (String)cmbSupplierName.getSelectedItem();
           int iPartyCode       =  data.getSupplierCode(sPartyName);
            data                .  setDyesName(iPartyCode);
           dyelist              =  new DyeList(iRow,data);
           dyelist              .  setVisible(true);
           setListData();
     }
   catch(Exception e){
          e.printStackTrace();
       //   System.out.println("Inside KeyListener Shadewise DyesEntry:"+e);
      }
    }
 
  }
}

private class FocusList extends FocusAdapter {
    public void focusLost(FocusEvent fe) {
                    setValues();
      }
 }

public void setListData(){
          try{
              String sDyesName       =  dyelist.sDyesName;
              txtDyesName            .  setText(sDyesName);
          }
          catch(Exception ex){
               ex.printStackTrace();
               JOptionPane.showMessageDialog(null,ex,"Dyes List",JOptionPane.ERROR_MESSAGE);
          }
     }

private void setValues(){
    double dPercentage                = common.toDouble(txtDyesPercentage.getText());
    String sRate                      = data.getDyesRate(dyelist.sDyesCode);
    txtDyesRate                       . setText(sRate);
    double dRate                      = common.toDouble(sRate);   
    double dCost                      = (dPercentage*dRate)/100;
    txtDyesCost                       . setText(common.getRound(String.valueOf(dCost),2));
}

private void setSaveNewParty(){
    int iSaveValue=-1;
    String sNewDyesPartyName  = "";
    sNewDyesPartyName         = addField.getText().replaceAll("\\s+", " ");    
    //String sTrimText      = addField.getText().replaceAll("\\s+", " ");

    /*String sPartyCodeSeq = data.getDyesPartySequenceValue();
    String sInv_ItemSeq  = "CD01"+sPartyCodeSeq;*/
    iSaveValue           = data.getInsertDyesPartName(sNewDyesPartyName);
    if (iSaveValue == 0) {
      JOptionPane.showMessageDialog(null, "New Dyes Party Name Saved", "Information", JOptionPane.INFORMATION_MESSAGE);
    } 
    else if (iSaveValue == 2) {
      JOptionPane.showMessageDialog(null, "Party Name Already Saved", "Information", JOptionPane.INFORMATION_MESSAGE);
    } 
    else if (iSaveValue == 2) {
     JOptionPane.showMessageDialog(null, "New Dyes Party Name Not Saved", "Information", JOptionPane.INFORMATION_MESSAGE);
    }
}


private int getSave(){
    System.out.println("gETsAVE");
    int iSaveValue=-1;
    String sDyesPartyName       = "";
    String sNewDyesPartyName    = addField.getText();
    if(chkNewParty.isSelected()){
     if(sNewDyesPartyName.length()>0){
        sDyesPartyName          =  sNewDyesPartyName;
      }
    }
    else{
        sDyesPartyName          = (String)cmbSupplierName.getSelectedItem();
    }
    int iSupplierCode           = data.getSupplierCode(sDyesPartyName);
    String  sFibreName          = (String)cmbBaseName.getSelectedItem();
    String  sFibreCode          = (String)data.getBaseCode(sFibreName);
    String sDyesName            = txtDyesName.getText();
    String sDyesCode            = data.getExDyesCode(sDyesName);
    String sPercentage          = txtDyesPercentage.getText();
    String sRate                = txtDyesRate.getText();
    String sCost                = txtDyesCost.getText();
    
    System.out.println("Percentage======>"+sPercentage);
    if (rbtnInsert.isSelected()) {
      iSaveValue                = data.getInsertShadewiseDyes(sFibreCode,sDyesCode,sPercentage,sRate,sCost,sAuthSysName,iSupplierCode);
    }
/*    else{
        System.out.println("Update"); 
      iSaveValue                 = data.updateDyesCostData (iSupplierCode, sDyesCode,sRate,sPercentage,sCost);  
     }*/
    return iSaveValue;
}

private void setTableData(){
 
    ADyesCostList =  new ArrayList();
    theModel      . setNumRows(0);
    int iSlNo=0;
    String sSupplierName         = (String)cmbSupplierName.getSelectedItem();
    int iSupCode                 = data.getSupplierCode(sSupplierName);
    data                         . setDyesCostDetails(iSupCode);
    //data.setDyesDetails(sSupplierName);
    for(int i=0;i<data.ADyesCostList.size();i++){
        HashMap      theMap      = (HashMap)data.ADyesCostList.get(i);
        String sDyes_Code        = (String)theMap.get("DYES_CODE");
        String sDyes_Name        = (String)theMap.get("DYES_ITEM_NAME");
        String sDyes_Per         = (String)theMap.get("DYES_PER");
        String sDyes_Rate        = (String)theMap.get("DYES_RATE");
        String sDyes_Cost        = (String)theMap.get("DYES_COST");
        String sBaseCode         = (String)theMap.get("BASECODE");
        String sBaseName         = (String)theMap.get("BASENAME");
            
             Vector  theVect     =  new Vector();
                      iSlNo         = i+1;
                     theVect     .  add(String.valueOf(iSlNo));
                     theVect     .  add(common.parseNull(sBaseCode+"-"+sBaseName));
                     theVect     .  add(common.parseNull(sDyes_Name));
                     theVect     .  add(common.parseNull(sDyes_Per));
                     theVect     .  add(common.parseNull(sDyes_Rate));
                     theVect     .  add(common.parseNull(sDyes_Cost));
                     
                     theModel    .  appendRow(theVect);
        }
}


public static void main(String args[]){
    ShadewiseDyesEntryFrame            frame       = new ShadewiseDyesEntryFrame();
                                       frame       . setVisible(true);
 }

}
