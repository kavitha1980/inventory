package guiutil;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import javax.swing.border.*;
import java.io.*;
import java.util.*;
import java.awt.*;

public class CommonTableModel extends DefaultTableModel
{
     private   String[]  ColumnName;
     private   String[]  ColumnType;
     private   int[]     iColumnWidth;

     public CommonTableModel(String[] ColumnName, String[] ColumnType, int[] iColumnWidth)
     {
          this.ColumnName     = ColumnName;
          this.ColumnType     = ColumnType;
          this.iColumnWidth   = iColumnWidth;

          setDataVector(getRowData(),ColumnName);
     }

     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";

          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }

     public void addRow()
     {
          ArrayList theList   = null;
          theList             = new ArrayList();
          theList             . clear();

          for(int i=0; i<ColumnName.length; i++)
          {
               theList        . add("");
          }

          appendRow(new Vector(theList));
     }

     public void setTableSerialNo()
     {
          for(int i=0; i<getRows(); i++)
          {
               setValueAt(String.valueOf(i+1), i, 0);
          }
     }
}
