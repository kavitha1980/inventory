package guiutil;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Date;
import util.Common;
import rndi.*;

public class DateField extends MyPanel implements CodedNames
{

    protected  int            iStDate,iEnDate;
    protected  boolean        bFlag=false;

    public     JTextField     theDate;
               Common         common = new Common();

    public DateField()
    {
        iStDate = 0;
        iEnDate = 0;
        bFlag = false;

        theDate = new JTextField(8);
        setLayout(new FlowLayout(0,0,0));
        add(theDate);
        add(new JLabel(new ImageIcon("//"+SCRIPTHOST+"/e/rawmaterial/src/guiutil/blue-ball.gif")));

        theDate.setBackground(new Color(255,222,206));    

        theDate.addFocusListener(new FocusList());
    }
     
    public DateField(boolean isCurrent)
    {
        iStDate = 0;
        iEnDate = 0;
        bFlag = false;

        theDate = new JTextField(8);
        setLayout(new FlowLayout(0,0,0));
        add(theDate);
        add(new JLabel(new ImageIcon("//"+SCRIPTHOST+"/e/rawmaterial/src/guiutil/blue-ball.gif")));

        theDate.setBackground(new Color(255,222,206));    

        theDate.addFocusListener(new FocusList());
        setTodayDate();
        theDate.setEnabled(false);
    }

    public DateField(int iStDate,int iEnDate)
    {
        this.iStDate = iStDate;
        this.iEnDate = iEnDate;
        bFlag = true;

        theDate = new JTextField(8);
        setLayout(new FlowLayout(0,0,0));
        add(theDate);
        add(new JLabel(new ImageIcon("//"+SCRIPTHOST+"/e/rawmaterial/src/guiutil/blue-ball.gif")));

        theDate.setBackground(new Color(255,222,206));

        theDate.addFocusListener(new FocusList());
    }

    private class FocusList extends FocusAdapter
    {

        public void focusLost(FocusEvent fe)
        {
            String str = theDate.getText();
            str = common.pureDate(common.getDate(str,0,1));

            if(common.toInt(str)==0)
                setTodayDate();
            else
                theDate.setText(common.parseDate(str));

            if(bFlag)
            {
                if(isBeyond())
                {
                    JOptionPane.showMessageDialog(null,"Date Falls Beyond the Finance Year","Error Message",JOptionPane.INFORMATION_MESSAGE);
                    setTodayDate();
                }
            }
        }
    }

    private boolean isBeyond()
    {

        int iDate = common.toInt(toNormal());

        if(iDate >= iStDate && iDate <= iEnDate)
            return false;

        return true;
    }

    //-- Data Accessor methods---//

    public void setEditable(boolean bFlag)
    {
        theDate.setEditable(bFlag);
    }

    public void setEnabled(boolean bFlag)
    {
        theDate.setEnabled(bFlag);
    }

    public void setTodayDate() 
    {
        String SDay,SMonth,SYear;
        try
        {
            String SDate = common.getServerDate();

    /*      Date dt = new Date();
            int iDay   = dt.getDate();
            int iMonth = dt.getMonth()+1;
            int iYear  = dt.getYear()+1900;
    
    
            if(iDay < 10)
               SDay = "0"+iDay;
            else
               SDay = String.valueOf(iDay);
    
            if(iMonth < 10)
               SMonth = "0"+iMonth;
            else
               SMonth = String.valueOf(iMonth);
    
            SYear = String.valueOf(iYear);*/
    
    
            SYear    = SDate.substring(0,4);
            SMonth   = SDate.substring(4,6);
            SDay     = SDate.substring(6,8);
    
    
            theDate.setText(SDay+"."+SMonth+"."+SYear);


        }
        catch(Exception ex)
        {
        }
    }

    public String toNormal()
    {
        return common.pureDate(theDate.getText());
    }
    public String toString()
    {
        return theDate.getText();
    }
    public void fromString(String str)
    {
        theDate.setText(common.parseDate(str));
    }

    public void requestFocus()
    {
        theDate.requestFocus();
    }
}

