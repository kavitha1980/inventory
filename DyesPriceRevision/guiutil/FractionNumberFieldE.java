package guiutil;


import javax.swing.*;
import javax.swing.text.*; 
import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.*;
import java.awt.event.*;
public class FractionNumberFieldE extends JTextField {

     int     iColumns        = 13;
     int     iFractionDigits = 0; 
     int     dotCount        = 0;
     double  myDouble = 0.0;
     int     myLength,indexPlace;
     String  myValue,myString;
     private Toolkit toolkit;

     public FractionNumberFieldE()
	{
          super();
          setColumns(iColumns);
          setHorizontalAlignment(SwingConstants.RIGHT);
          toolkit = Toolkit.getDefaultToolkit();
          setBackground(new Color(0,160,160));
          setFont(new Font("",Font.BOLD,10));
          setText("0.0");
          addFocusListener(new FocusList());
     }

     public FractionNumberFieldE(String value, int columns) {
          super(columns);
          setColumns(columns);
		iColumns = columns;
          setText(value);
          setHorizontalAlignment(SwingConstants.RIGHT);
          toolkit = Toolkit.getDefaultToolkit();
          setBackground(new Color(0,160,160));
          setFont(new Font("",Font.BOLD,10));   

          addKeyListener(new KeyList());
     }

     public FractionNumberFieldE(int columns)
     {
          super(columns);
          setColumns(columns);
		iColumns = columns;
          setHorizontalAlignment(SwingConstants.RIGHT);
          toolkit = Toolkit.getDefaultToolkit();
          setBackground(new Color(0,160,160));
          setFont(new Font("",Font.BOLD,10));   

          addKeyListener(new KeyList());
     }

     public class FocusList extends FocusAdapter
     {
          public void focusGained(FocusEvent fe)
          {
            setCaretPosition(0);
            moveCaretPosition(getText().length());
          }

     }

     public class KeyList implements KeyListener
     {
          int i=0;
          public void keyPressed(KeyEvent ke){}
          public void keyTyped(KeyEvent ke){}


          public void keyReleased(KeyEvent ke)
          {
               myLength = getText().trim().length();
               myString = getText().trim();
               if (myLength==0)
               {
                    setText("");
                    return;
               }
               if (myLength>0)
               {

                    indexPlace=getText().trim().indexOf(".");
                    if (indexPlace>0 && myLength-indexPlace+1>2)
                    {
                        if (myLength>=indexPlace+3+iFractionDigits)
                        {
                             myValue = myString.substring(0,indexPlace+3+iFractionDigits);
                             setText(myValue);
                        }
                    }
                    else
                    {
                        myValue = myString;
                    }
                    return;
               }
          }
     }

}
    
