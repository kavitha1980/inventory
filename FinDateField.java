package Finance;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Date;
import util.*;
public class FinDateField extends JPanel
{
    protected int        iStDate,iEnDate;
    protected boolean    bFlag=false;

    JTextField theDate;
    Common     common = new Common();

    public FinDateField()
    {
        iStDate     = 0;
        iEnDate     = 0;
        bFlag       = false;

        theDate     = new JTextField(10);
        setLayout(new FlowLayout(0,0,0));
        add(theDate);
        theDate     . setBackground(new Color(255,222,206));
        setTodayDate();
    }

    public FinDateField(boolean isCurrent)
    {
        iStDate     = 0;
        iEnDate     = 0;
        bFlag       = false;

        theDate     = new JTextField(7);
        setLayout(new FlowLayout(0,0,0));
        add(theDate);

        theDate     . setBackground(new Color(255,222,206));    

        setTodayDate();
        theDate     . setEnabled(false);
    }

    public FinDateField(int iStDate,int iEnDate)
    {
        this . iStDate   = iStDate;
        this . iEnDate   = iEnDate;
               bFlag     = true;

        theDate = new JTextField(8);
        setLayout(new FlowLayout(0,0,0));
        add(theDate);

        theDate     . setBackground(new Color(255,222,206));
    }

    private boolean isBeyond()
    {
        int iDate = common.toInt(toNormal());

        if(iDate >= iStDate && iDate <= iEnDate)
            return false;

        return true;
    }

    //-- Data Accessor methods---//

    public void setEditable(boolean bFlag)
    {
        theDate     . setEditable(bFlag);
    }

    public void setEnabled(boolean bFlag)
    {
        theDate     . setEnabled(bFlag);
    }

    public void setTodayDate() 
    {
        String SDay,SMonth,SYear;
        try
        {
            String SDate = common.getServerDate();
    /*      Date dt = new Date();
            int iDay   = dt.getDate();
            int iMonth = dt.getMonth()+1;
            int iYear  = dt.getYear()+1900;
    
    
            if(iDay < 10)
               SDay = "0"+iDay;
            else
               SDay = String.valueOf(iDay);
    
            if(iMonth < 10)
               SMonth = "0"+iMonth;
            else
               SMonth = String.valueOf(iMonth);
    
            SYear = String.valueOf(iYear);*/
    
    
            SYear    = SDate.substring(0,4);
            SMonth   = SDate.substring(4,6);
            SDay     = SDate.substring(6,8);
    
    
            theDate.setText(SDay+"."+SMonth+"."+SYear);
        }
        catch(Exception ex)
        {
        }
    }

    public String toNormal()
    {
        return common.pureDate(theDate.getText());
    }
    public String toString()
    {
        return theDate.getText();
    }
    public void fromString(String str)
    {
        theDate.setText(common.parseDate(str));
    }

    public void requestFocus()
    {
        theDate.requestFocus();
    }

    public boolean isValidate()
    {
          String    SMsg      = "";
          boolean   bVlflag   = true;
          String    SDate     = theDate.getText();

          String    SYear     = "";
          String    SMonth    = "";
          String    SDay      = "";

          int       Num       = 0;
          boolean   bNumCheck = false;
               
          if(SDate.length() != 10)
          {
               bVlflag   = false;
               if(SMsg.length()==0)
                    SMsg      = "Enter the Date Format dd.mm.yyyy";
          }

          if((common.pureDate(SDate).trim()).length() != 8)
          {
               bVlflag   = false;
               if(SMsg.length()==0)
                    SMsg      = "Enter the Date Format dd.mm.yyyy";
          }

          if(SDate.length() == 10)
          {
               String    SDotOne   = SDate. substring(2,3);
               String    SDotTwo   = SDate. substring(5,6);

               int       iSDate    = common.toInt(SDate. substring(0,2));
               int       iSMonth   = common.toInt(SDate. substring(3,5));
               int       iSYear    = common.toInt(SDate. substring(6,10));

                         SYear     = SDate. substring(0,2);
                         SMonth    = SDate. substring(3,5);
                         SDay      = SDate. substring(6,10);

               if(!(SDotOne.equals(".") && SDotTwo.equals(".")))
               {
                    bVlflag   = false;
                    if(SMsg.length()==0)
                         SMsg      = "Use . to separte Date,month,year";
                    else
                         SMsg     += "\nUse . to separte Date,month,year";
               }

               if(iSMonth>12)
               {
                    bVlflag   = false;
                    if(SMsg.length()==0)
                         SMsg      = "Enter the Month<13";
                    else
                         SMsg     += "\n Enter the Month<13";
               }

               if(iSMonth==4||iSMonth==6||iSMonth==9||iSMonth==11)
               {
                    String SMn ="";

                    if(iSMonth ==4)
                         SMn =" Apr ";

                    if(iSMonth ==6)
                         SMn =" Jun ";

                    if(iSMonth ==9)
                         SMn =" Sep ";

                    if(iSMonth ==11)
                         SMn = " Nov ";

                    if(iSDate>30)
                    {
                         bVlflag   = false;
                         if(SMsg.length()==0)
                              SMsg      = "Enter the Date "+SMn+" <31 ";
                         else
                              SMsg     += "\n Enter the Date Feb <31 ";
                    }
               }

               if(iSMonth==1||iSMonth==3||iSMonth==5||iSMonth==7||iSMonth==8||iSMonth==10||iSMonth==12)
               {                              
                    if(iSDate>31)
                    {
                         String SMn ="";
     
                         if(iSMonth ==1)
                              SMn =" Jan ";
     
                         if(iSMonth ==3)
                              SMn =" Mar ";
     
                         if(iSMonth ==5)
                              SMn =" May ";
     
                         if(iSMonth ==7)
                              SMn = " July ";
     
                         if(iSMonth ==8)
                              SMn = " Aug ";
     
                         if(iSMonth ==10)
                              SMn = " Oct ";

                         if(iSMonth ==12)
                              SMn = " Dec ";
     
                         bVlflag   = false;

                         if(SMsg.length()==0)
                              SMsg      = "Enter the Date "+SMn+" <32 ";
                         else
                              SMsg     += "\n Enter the Date"+SMn+" <32 ";
                    }
               }

               if(iSMonth==2)
               {                              
                    if(common.isLeap(iSYear))
                    {
                         if(iSDate>29)
                         {
                              bVlflag   = false;
                              if(SMsg.length()==0)
                                   SMsg      = "Enter the Date Feb <30 (its leap year)";
                              else
                                   SMsg     += "\n Enter the Date Feb <30 (its leap year)";
                         }
                    }
                    else
                    {
                         if(iSDate>28)
                         {
                              bVlflag   = false;
                              if(SMsg.length()==0)
                                   SMsg      = "Enter the Date Feb <29 (its not leap year)";
                              else
                                   SMsg     += "\n Enter the Date Feb <29 (its not leap year)";
                         }
                    }
               }

               try
               {
                   Num = Integer.parseInt(SYear);
               }
               catch(NumberFormatException NFE)
               {
                    theDate    . setText("");
                    bVlflag   = false;

                    if(SMsg.length()==0)
                         SMsg      = "Don't use alpabets and other symbols";
                    else
                         SMsg      = "Don't use alpabets and other symbols";

                    bNumCheck = true;
               }

               try
               {
                   Num = Integer.parseInt(SMonth);
                    
               }
               catch(NumberFormatException NFE)
               {
                    theDate    . setText("");

                    if(!bNumCheck == true)
                    {
                         if(SMsg.length()==0)
                              SMsg      = "Don't use alpabets and other symbols";
                         else
                              SMsg      = "Don't use alpabets and other symbols";
                    }
                    bNumCheck = true;
                    bVlflag   = false;
               }
               try
               {
                   Num = Integer.parseInt(SDay);
               }
               catch(NumberFormatException NFE)
               {
                    theDate    . setText("");
                    if(!bNumCheck == true)
                    {
                         if(SMsg.length()==0)
                              SMsg      = "Don't use alpabets and other symbols";
                         else
                              SMsg      = "Don't use alpabets and other symbols";
                    }

                    bNumCheck = true;
                    bVlflag   = false;
               }
          }
          if(!bVlflag)
          {
               theDate        . setText("");
               // theDate.requestFocus();

               JOptionPane    . showMessageDialog(null,SMsg,"Information",JOptionPane.INFORMATION_MESSAGE);
          }
          return bVlflag;
     }
}

