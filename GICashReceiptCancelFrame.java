
import java.awt.*;
import java.awt.event.*;
import java.rmi.RemoteException;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumnModel; 

import jdbc.*;
import util.*;
import guiutil.*;

public class GICashReceiptCancelFrame extends JInternalFrame  {

     private  int              		iUnitCode;
	// private  String 				susercode   = "0";
	 private  int 				iUserCode   = 0;
     private  String           		SDateTime   = "";
     private  JLayeredPane     		Layer;
        
     private JPanel            		toppanel,bottompanel, middlepanel;
     
     private  JTable                      theTable;
     private  GICashReceiptCancelModel    theModel;

     private JButton          		bapply, bsave, bexit;
    
     private JComboBox			    JCSelectType;
	 private JTextField  			TGINo; 
	 private DateField 				TGIDate;
	 
		
     Common    		               common; 
     
	private String 				   STodayDate = "";

	java.util.List 				   theGIList 		 = null;
	
	java.sql.Connection            theAlpaConnection = null;
	java.sql.Connection            theInvConnection = null;

    public GICashReceiptCancelFrame(JLayeredPane Layer,int iUserCode) throws RemoteException {

		try{
            this.Layer     =  Layer;
            this.iUserCode = iUserCode;
			
			initValues();
            createComponents();
            setLayouts();
            addComponents();
            addListeners();

        }catch(Exception ex){
			ex.printStackTrace();
		}
     }
	 
	 private void initValues(){
		
		common         = new Common();
		theModel       = new GICashReceiptCancelModel();
		
	 }

     private void createComponents(){
        toppanel       =   new JPanel();
        middlepanel    =   new JPanel();
        bottompanel    =   new JPanel();

		TGINo			= new JTextField();
		TGINo			. setEnabled(true);
		
		TGIDate			= new DateField();
		TGIDate			. setTodayDate();
		TGIDate 		. setEnabled(false);
		STodayDate		= TGIDate.toString();
	
		JCSelectType   = new JComboBox();
	    JCSelectType   . addItem("GI No");
	    JCSelectType   . addItem("GI Date");
		
        theTable       = new JTable(theModel);

        TableColumnModel TC  = theTable.getColumnModel();

        for(int j=0;j<theModel.ColumnName.length;j++)   {
            TC .getColumn(j).setPreferredWidth(theModel.iColumnWidth[j]);
        }
		theTable.getTableHeader().setReorderingAllowed(false); 
		theModel.setNumRows(0);
		
		bapply	       = new JButton("Apply");
		bsave 	       = new JButton("Save");
		bexit	       = new JButton("Exit");

     }
	 
    private void setLayouts(){

		setTitle("GI Cash Receipt Cancel Frame");
		setClosable(true);
		setMaximizable(true);
		setIconifiable(true);
		setResizable(true);
		setBounds(10,10,700, 550);
                        
		toppanel      . setLayout(new GridLayout(3,4));
		middlepanel   . setLayout(new BorderLayout());
		bottompanel   . setLayout(new GridLayout(1,5));

		toppanel      . setBorder(new TitledBorder("GI Selection"));
		middlepanel   . setBorder(new TitledBorder("GI Details"));
		bottompanel   . setBorder(new TitledBorder("Controls"));

    }
	 

     private void addComponents(){

		toppanel .add(new JLabel("Select Type"));
		toppanel .add(JCSelectType);
		toppanel .add(new JLabel(""));
		toppanel .add(new JLabel(""));

		toppanel .add(new JLabel("GI No"));
		toppanel .add(TGINo);
		toppanel .add(new JLabel("GI Date"));
		toppanel .add(TGIDate);

		toppanel .add(new JLabel(""));
		toppanel .add(new JLabel(""));
		toppanel .add(new JLabel(""));
		toppanel .add(bapply);

        middlepanel.add(new JScrollPane(theTable));

		bottompanel.add(new JLabel(""));
        bottompanel.add(bsave);
        bottompanel.add(new JLabel(""));
        bottompanel.add(bexit);
		bottompanel.add(new JLabel(""));
        
        getContentPane().add("North",toppanel);
        getContentPane().add("Center",middlepanel);
        getContentPane().add("South",bottompanel);

     }


    private void addListeners(){

        JCSelectType.addItemListener(new ItemListener(){
			
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange()==e.SELECTED){
					int ind = JCSelectType.getSelectedIndex();
					
					TGINo	.setText("");
					//TGIDate .setText(STodayDate);

					if(ind==0){
						TGINo	.setEnabled(true);
						TGIDate .setEnabled(false);
					}else{
						TGINo	.setEnabled(false);
						TGIDate .setEnabled(true);
					}
				}
			}			
			
		});

		bapply.addActionListener(new ActList());
        bsave.addActionListener(new ActList());
        bexit.addActionListener(new ActList());
    }

    private class ActList implements ActionListener  {
		
		public void actionPerformed(ActionEvent ae) {
	  
			if(ae.getSource()==bapply){
				setTabelData();
			}
	  
			if(ae.getSource()==bsave){
				saveData();
			}
			
			if(ae.getSource()==bexit){
				removeHelpFrame();
			}
		}
    }

	private void saveData(){
		java.util.List theList = new java.util.ArrayList();
		try{
			int iRows = theModel.getRowCount();
			
			java.util.HashMap theMap = new java.util.HashMap();
			
			int iCashRecExist = -1;
			
			for(int i=0;i<iRows;i++){
				java.lang.Boolean  bselect  = (java.lang.Boolean)theModel.getValueAt(i,8);
				
				if(bselect){
					int  iMillCode = getMillCode(common.parseNull((String)theModel.getValueAt(i,1)));
					int  iDivCode  = -1;

					if(iMillCode==0){
						iDivCode = 1;
					}else if(iMillCode==1){
						iDivCode = 2;
					}else if(iMillCode==2){
						iDivCode = 3;
					}else{
						iDivCode = -1;
					}
					
					theMap = new java.util.HashMap();
					theMap . put("GI_NO",    common.parseNull((String)theModel.getValueAt(i,1)));
					theMap . put("GI_DATE",  common.pureDate(common.parseNull((String)theModel.getValueAt(i,2))));
					theMap . put("MILLCODE", String.valueOf(iMillCode));
					theMap . put("DIVCODE",  String.valueOf(iDivCode));
					
					iCashRecExist = 0;
					iCashRecExist = isRCMEntryExist(common.parseNull((String)theModel.getValueAt(i,1)), common.pureDate(common.parseNull((String)theModel.getValueAt(i,2))), iDivCode);
					
					if(iCashRecExist==0){
						theList = new java.util.ArrayList();
						javax.swing.JOptionPane. showMessageDialog(null," Cash Receipt already Made... GINo : "+common.parseNull((String)theModel.getValueAt(i,1)),"Info",javax.swing.JOptionPane.INFORMATION_MESSAGE);
						break;
					}
					if(iCashRecExist>0){
						theList.add(theMap);
					}
				}
			}
			
			
			
			if(iCashRecExist==-1 || theList.size()==0){
				javax.swing.JOptionPane. showMessageDialog(null," Please select GI No... ","Info",javax.swing.JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			
			if(iCashRecExist==0){
				return;
			}
			
			if(iCashRecExist>0 && theList.size()>0)
			if(JOptionPane.showConfirmDialog(null,"Do You Want to Save?","Confirm",JOptionPane.YES_NO_OPTION)==0){

				int saved = 0;
					saved = updateCashReceipt(theList);
				String SMsg = "";
				if(saved==1) {
				   SMsg = "Data Saved Successfully";
				}else {
				   SMsg = "Data Not Saved.";
				}
		           
			    javax.swing.JOptionPane. showMessageDialog(null,SMsg,"Info",javax.swing.JOptionPane.INFORMATION_MESSAGE);

                theModel.setNumRows(0);
                updateUI();
			}else{
				javax.swing.JOptionPane . showMessageDialog(null,"Data Not Saved .","Info",javax.swing.JOptionPane.INFORMATION_MESSAGE);          
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
	
	
	
    private int updateCashReceipt(java.util.List theList) {
		int saved = 1;
		String SQs = "";
			   SQs = " Update GateInWard set CashReceiptStatus = 0 "+
			         " Where CashReceiptStatus = 1 and GiNo= ? And GiDate = ? And Millcode = ? ";


		try{
			if(theInvConnection==null){
				JDBCInvConnection  jdbc = JDBCInvConnection.getJDBCConnection();
				theInvConnection = jdbc . getConnection();
			}
			theInvConnection.setAutoCommit(false);
			java.sql.PreparedStatement pst = theInvConnection.prepareStatement(SQs);
									for(int i=0;i<theList.size();i++){
										java.util.HashMap theMap = (java.util.HashMap)theList.get(i);
										
										pst.setString(1,common.parseNull((String)theMap . get("GI_NO")));
										pst.setString(2,common.parseNull((String)theMap . get("GI_DATE")));
										pst.setString(3,common.parseNull((String)theMap . get("MILLCODE")));
									    pst.executeUpdate();
									}
										pst.close();

				SQs = "";
			    SQs = " Delete From  RegisteredParty_Invoice Where GINo = ? And Divisioncode=? And to_char(EntryDate,'yyyymmdd')>= ? and ReceiptDate is null ";
			   
			if(theAlpaConnection==null){
				JDBCAlpaConnection  jdbc = JDBCAlpaConnection.getJDBCConnection();
				theAlpaConnection = jdbc . getConnection();
			}
				theAlpaConnection.setAutoCommit(false);
			java.sql.PreparedStatement ps = theAlpaConnection.prepareStatement(SQs);
									for(int i=0;i<theList.size();i++){
										java.util.HashMap theMap = (java.util.HashMap)theList.get(i);
										ps . setString(1,common.parseNull((String)theMap . get("GI_NO")));
										ps . setString(2,common.parseNull((String)theMap . get("DIVCODE")));
										ps . setString(3,common.parseNull((String)theMap . get("GI_DATE")));
									    ps . executeUpdate();
									}
										ps . close();
			

			    SQs = "";
			    SQs = " Delete From  ReverseCharge_Invoice Where GINo = ? And Divisioncode=?  And to_char(EntryDate,'yyyymmdd')>= ? and ReceiptDate is null  ";

			                            ps = theAlpaConnection.prepareStatement(SQs);
									for(int i=0;i<theList.size();i++){
										java.util.HashMap theMap = (java.util.HashMap)theList.get(i);
										ps . setString(1,common.parseNull((String)theMap . get("GI_NO")));
										ps . setString(2,common.parseNull((String)theMap . get("DIVCODE")));
										ps . setString(3,common.parseNull((String)theMap . get("GI_DATE")));
									    ps . executeUpdate();
									}
										ps . close();


		}catch(Exception ex){
		  ex.printStackTrace();
		  saved = 2;
		}
		finally{
			try{
				if(theInvConnection!=null){
					if(saved==1){
						theInvConnection.commit();
					}else{
						theInvConnection.rollback();
					}
					theInvConnection.setAutoCommit(true);
				}
				
				if(theAlpaConnection!=null){
					if(saved==1){
						theAlpaConnection.commit();
					}else{
						theAlpaConnection.rollback();
					}
					theAlpaConnection.setAutoCommit(true);
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}	
		}
		
		return saved;
    }


    private int isRCMEntryExist(String SGiNo, String SGiDate, int iDivCode) {
		String SQs = "";
			   SQs = " Select sum(exist) From ( "+
			         " Select count(1) as exist from RegisteredParty_Invoice where GINo = '"+SGiNo+"' And Divisioncode='"+iDivCode+"' And to_char(EntryDate,'yyyymmdd')>='"+SGiDate+"' and ReceiptDate is null "+
			         " union "+
			         " Select count(1) as exist from ReverseCharge_Invoice where GINo = '"+SGiNo+"'  And Divisioncode='"+iDivCode+"'  And to_char(EntryDate,'yyyymmdd')>='"+SGiDate+"' and ReceiptDate is null "+
					 " ) ";
					 
			         //" Select count(1) as exist from RegisteredParty_Invoice where GINo = '"+SGiNo+"' And Divisioncode='"+iDivCode+"' And YearCode = 13 and ReceiptDate is null "+

			   
		try{
			if(theAlpaConnection==null){
				JDBCAlpaConnection  jdbc = JDBCAlpaConnection.getJDBCConnection();
				theAlpaConnection = jdbc . getConnection();
			}
			java.sql.Statement stat = theAlpaConnection.createStatement();
			java.sql.ResultSet rst  = stat.executeQuery(SQs);
			
			if(rst.next()){
				return rst.getInt(1);
			}
			rst.close();
			stat.close();

		}catch(Exception ex){
			ex.printStackTrace();
			return 0;
		}
		
		return 0;
    }

	
	private int getMillCode(String SGiNo){
		int iMillCode = -1;
		
		for(int i =0;i<theGIList.size();i++)   {
		   java.util.HashMap theMap = (java.util.HashMap)theGIList.get(i);
			
			if(SGiNo.trim().equals(common.parseNull((String)theMap.get("GINO")).trim()) ){
				iMillCode =common.toInt(common.parseNull((String)theMap.get("MILLCODE")));
				break;
			}
		}
		
		return iMillCode;
		
	}
	
    private java.util.List SetGiDetails(int iSelType, String SGiNo){
		java.util.List theList = new java.util.ArrayList();
		String SQs = "";
			   SQs = " Select GiNo, GiDate, supplier.Name, InvNo, InvDate, Item_name, GateQty, CashReceiptStatus, GateInWard.MillCode From GateInWard "+
			         " Inner join Supplier on Supplier.ac_code = GateInWard.sup_code "+
			         " Where CashReceiptStatus = 1 "+
					 " And round(Sysdate-to_date(Gidate,'yyyymmdd'))<=3";//3
					 
			 if(iSelType==0){
			   SQs += " and GiNo= '"+SGiNo+"' ";
			 }else{
			   SQs += " and GiDate= '"+SGiNo+"' ";
			 }
			   SQs += " Order by 2, 1, 3 ";
			   
			   
		try{
			if(theInvConnection==null){
				JDBCInvConnection  jdbc = JDBCInvConnection.getJDBCConnection();
				theInvConnection = jdbc . getConnection();
			}
			java.sql.Statement stat = theInvConnection.createStatement();
			java.sql.ResultSet rst  = stat.executeQuery(SQs);
			
			java.util.HashMap theMap = new java.util.HashMap();
			
			while(rst.next()){
				theMap = new java.util.HashMap();
				theMap.put("GINO",				rst.getString(1));
				theMap.put("GIDATE",			rst.getString(2));
				theMap.put("SUPPLIER",			rst.getString(3));
				theMap.put("INVNO",				rst.getString(4));
				theMap.put("INVDATE",			rst.getString(5));
				theMap.put("ITEM_NAME",			rst.getString(6));
				theMap.put("GATEQTY",		    rst.getString(7));
				theMap.put("CASHRECEIPTSTATUS", rst.getString(8));
				theMap.put("MILLCODE", 			rst.getString(9));
				
				theList.add(theMap);
			}
			rst.close();
			stat.close();

		}catch(Exception ex){
		  ex.printStackTrace();
		}
		
		return theList;
    }


    public void setTabelData() {
        theModel.setNumRows(0);
        theGIList = null;
		theGIList = new java.util.ArrayList();
        
		try{
			int ind = JCSelectType.getSelectedIndex();
			
			if(ind==0){
				if(common.parseNull((String)TGINo.getText()).length()<3){
					return;
				}
				
				theGIList = SetGiDetails(0, (String)TGINo.getText());	
			}else{
				theGIList = SetGiDetails(1, (String)TGIDate.toNormal());	
			}

			for(int i =0;i<theGIList.size();i++)   {
			   java.util.HashMap theMap = (java.util.HashMap)theGIList.get(i);

			   java.util.Vector  V  = new java.util.Vector();
								 V  . addElement(String.valueOf(i+1));
								 V  . addElement(common.parseNull((String)theMap.get("GINO")));
								 V  . addElement(common.parseDate(common.parseNull((String)theMap.get("GIDATE"))));
								 V  . addElement(common.parseNull((String)theMap.get("SUPPLIER")));
								 V  . addElement(common.parseNull((String)theMap.get("INVNO")));
								 V  . addElement(common.parseDate(common.parseNull((String)theMap.get("INVDATE"))));
								 V  . addElement(common.parseNull((String)theMap.get("ITEM_NAME")));
								 V  . addElement(common.parseNull((String)theMap.get("GATEQTY")));
								 V  . addElement(new Boolean(false));
								 
			   theModel . appendRow(V);
			}
	   
		}catch(Exception ex){
			ex.printStackTrace();
		}
    }


    private void removeHelpFrame(){

		try{
			Layer . remove(this);
			Layer . updateUI();

		}catch(Exception ex) {
			ex.printStackTrace();
		}
    }


}
