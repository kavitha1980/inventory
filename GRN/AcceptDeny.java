package GRN;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class AcceptDeny extends JWindow
{
     JButton BAccept,BDeny;
     JLabel  theLabel;
     JTextArea TA;
     JPanel  MiddlePanel;
     AcceptDeny(String str)
     {
          MiddlePanel = new JPanel(true);

          BAccept  = new JButton("Accept");
          BDeny    = new JButton("Deny");
          theLabel = new JLabel("Warning",JLabel.CENTER);
          TA       = new JTextArea(3,5);
          TA.setText(str);
          MiddlePanel.setLayout(new GridLayout(1,2));
          getContentPane().setLayout(new BorderLayout());

          MiddlePanel.add(BAccept);
          MiddlePanel.add(BDeny);

          TA.setEditable(false);

          theLabel.setBackground(Color.cyan);
          theLabel.setForeground(Color.red);
          BAccept.setBackground(Color.cyan);
          BAccept.setForeground(Color.blue);
          BDeny.setBackground(Color.red);
          BDeny.setForeground(Color.white);
          TA.setFont(new Font("monospaced",Font.PLAIN,12));
          TA.setBackground(new Color(230,250,190));
          TA.setForeground(new Color(130,30,60));

          getContentPane().add("North",theLabel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",new JScrollPane(TA));

          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          double x             = screenSize.getWidth();
          double y             = screenSize.getHeight();
          x=x/2;
          y=y/2;
          setSize(300,150);
          setLocation(new Double(x-150).intValue(),new Double(y-75).intValue());
     }
//     public static void main(String arg[])
//     {
//          AcceptDeny AD = new AcceptDeny("Warning");
//          AD.show();
//     }
}

