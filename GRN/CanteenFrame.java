package GRN;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class CanteenFrame extends JInternalFrame
{
     
     
     JLayeredPane   Layer;
     Vector         VInwNo,VInwName;
     TabReport      tabreportx;

     Vector         VInwNox,VDesc16,VQty16,VSupName16;

     JButton        BOk;
     JTextField     TSupplier,TMaterial;
     JDialog        theDialog;
     NextField      TQty;

     JPanel         TopPanel,MiddlePanel,BottomPanel,CashPanel;
     JComboBox      JCType;

     int            index = 0;
     int            iMillCode;

     Common common = new Common();

     CanteenFrame(JLayeredPane Layer,Vector VInwNo,Vector VInwName,TabReport tabreportx,Vector VInwNox,Vector VDesc16,Vector VQty16,Vector VSupName16,JDialog theDialog,int iMillCode)
     {
          
          this.Layer      = Layer;
          this.VInwNo     = VInwNo;
          this.VInwName   = VInwName;
          this.tabreportx = tabreportx;
          this.VInwNox    = VInwNox;
          this.VDesc16    = VDesc16;
          this.VQty16     = VQty16;
          this.VSupName16 = VSupName16;
          this.theDialog  = theDialog;
          this.iMillCode  = iMillCode;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          BOk            = new JButton("Okay");
          JCType         = new JComboBox(VInwName);
          TQty           = new NextField();
          TMaterial      = new JTextField();
          TSupplier      = new JTextField();
          TopPanel       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BottomPanel    = new JPanel(true);
          CashPanel      = new JPanel(true);
     }

     private void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(4,2));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,350,300);
     }

     private void addComponents()
     {
          TopPanel       .add(new JLabel("Supplier"));
          TopPanel       .add(TSupplier);
          TopPanel       .add(new JLabel("Inward Type"));
          TopPanel       .add(JCType);
          TopPanel       .add(new JLabel("Description @Gate"));
          TopPanel       .add(TMaterial);
          TopPanel       .add(new JLabel("Qty"));
          TopPanel       .add(TQty);

          BottomPanel    .add(BOk);

          getContentPane().add("North",TopPanel);
          getContentPane().add("South",BOk);
		 
          CashPanel      .setLayout(new BorderLayout()); 
          CashPanel      .add("North",TopPanel);
          CashPanel      .add("Center",MiddlePanel);
          CashPanel      .add("South",BOk);

          setPresets();
     }

     private void setPresets()
     {
          index       = tabreportx.ReportTable.getSelectedRow(); 
          int iType   = common.toInt((String)VInwNox.elementAt(index));

          JCType    .setSelectedIndex(iType);

          TMaterial .setText((String)VDesc16.elementAt(index)); 
          TQty      .setText((String)VQty16.elementAt(index));
          TSupplier .setText((String)VSupName16.elementAt(index));

          TMaterial .setEditable(false);
          TSupplier .setEditable(false);
          JCType.setEnabled(false);
          TQty.setEditable(false);
     }

     private void addListeners()
     {
         BOk        .addActionListener(new ActList());
     }
               
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(false);

               tabreportx.ReportTable.setValueAt(new Boolean(true),index,9);

               removeHelpFrame();
               tabreportx.ReportTable.requestFocus();
          }
     }

     private void removeHelpFrame()
     {
          try{theDialog.setVisible(false);}catch(Exception e){}
          
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }


}
