package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class CashGrnMrsLinkFrame extends JInternalFrame
{
     
     
     JLayeredPane   Layer;
     Vector         VInwNo,VInwName;
     TabReport      tabreportx;
     TabReport      tabreport;

     Vector         VCode,VName,VNameCode,VInwNox;
     Vector         VDesc4,VCode4,VQty4,VSup_Name4,VMrsId4,VIssStatus4;

     JButton        BMaterial,BOk;
     JTextField     TMatCode,TSupplier,TMaterial;
     JDialog        theDialog;
     NextField      TRate,TQty,TNet;

     JPanel         TopPanel,MiddlePanel,BottomPanel,CashPanel;
     JComboBox      JCType;

     int            index = 0;
     int            iMillCode;
     String         SItemTable;

     Common common = new Common();

     Object RowData[][];
     String ColumnData[] = {"MRS No","Material Code","Material Name","MRS Qty","Click for Select"};
     String ColumnType[] = {"N"     ,"S"            ,"S"            ,"N"      ,"B"};

     Vector VMrsNo,VMrsCode,VMrsName,VMrsQty,VId;

     CashGrnMrsLinkFrame(JLayeredPane Layer,Vector VInwNo,Vector VInwName,TabReport tabreportx,Vector VCode,Vector VName,Vector VNameCode,Vector VInwNox,Vector VDesc4,Vector VCode4,Vector VQty4,Vector VSup_Name4,Vector VMrsId4,Vector VIssStatus4,JDialog theDialog,int iMillCode,String SItemTable)
     {
          this.Layer      = Layer;
          this.VInwNo     = VInwNo;
          this.VInwName   = VInwName;
          this.tabreportx = tabreportx;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.VInwNox    = VInwNox;
          this.VDesc4     = VDesc4;
          this.VCode4     = VCode4;
          this.VQty4      = VQty4;
          this.VSup_Name4 = VSup_Name4;
          this.VMrsId4    = VMrsId4;
          this.VIssStatus4= VIssStatus4;
          this.theDialog  = theDialog;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          BMaterial      = new JButton("Material");
          BOk            = new JButton("Okay");
          JCType         = new JComboBox(VInwName);
          TQty           = new NextField();
          TMatCode       = new JTextField();
          TMaterial      = new JTextField();
          TSupplier      = new JTextField();
          TopPanel       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BottomPanel    = new JPanel(true);
          CashPanel      = new JPanel(true);
     }

     private void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(5,2));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,350,300);
     }

     private void addComponents()
     {
          TopPanel       .add(new JLabel("Select Material"));
          TopPanel       .add(BMaterial);
          TopPanel       .add(new JLabel("Supplier"));
          TopPanel       .add(TSupplier);
          TopPanel       .add(new JLabel("Inward Type"));
          TopPanel       .add(JCType);
          TopPanel       .add(new JLabel("Description @Gate"));
          TopPanel       .add(TMaterial);
          TopPanel       .add(new JLabel("Qty"));
          TopPanel       .add(TQty);

          BottomPanel    .add(BOk);

          getContentPane().add("North",TopPanel);
          getContentPane().add("South",BOk);
		 
          CashPanel      .setLayout(new BorderLayout()); 
          CashPanel      .add("North",TopPanel);
          CashPanel      .add("Center",MiddlePanel);
          CashPanel      .add("South",BOk);

          setPresets();
     }

     private void setPresets()
     {
          index       = tabreportx.ReportTable.getSelectedRow(); 
          int iType   = common.toInt((String)VInwNox.elementAt(index));
          int iIssStatus = common.toInt((String)VIssStatus4.elementAt(index));

          JCType    .setSelectedIndex(iType);

          TMaterial .setText((String)VDesc4.elementAt(index)); 
          TQty      .setText((String)VQty4.elementAt(index));
          TMatCode  .setText((String)VCode4.elementAt(index));
          TSupplier .setText((String)VSup_Name4.elementAt(index));

          TMaterial .setEditable(false);
          TSupplier .setEditable(false);
          JCType.setEnabled(false);

          if(iIssStatus==2)
          {
               BMaterial.setEnabled(false);
               TQty.setEditable(false);
          }

     }

     private void addListeners()
     {
         BMaterial  .addActionListener(new MaterialList(this));
         BOk        .addActionListener(new ActList());
     }
               
     private class MaterialList implements ActionListener
     {

          CashGrnMrsLinkFrame cashgrnmrslinkframe;

          public MaterialList(CashGrnMrsLinkFrame cashgrnmrslinkframe)
          {
               this.cashgrnmrslinkframe = cashgrnmrslinkframe;
          }
          public void actionPerformed(ActionEvent ae)
          {
               
               Frame dummy = new Frame();
               JDialog theDialog = new JDialog(dummy,"Description Dialog",true);

               MaterialSearch2 MS = new MaterialSearch2(Layer,TMatCode,BMaterial,VCode,VName,VNameCode,theDialog,cashgrnmrslinkframe,iMillCode,SItemTable);
               theDialog.getContentPane().add(MS.MaterialPanel);
               theDialog.setBounds(80,100,450,350);
               theDialog.setVisible(true);
          }
     }
     public void setTabReport(String SMatCode)
     {
          setDataIntoVector(SMatCode);
          setRowData();

          try
          {
             MiddlePanel.remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             MiddlePanel.add(tabreport,BorderLayout.CENTER);
             tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
             setSelected(true);
             MiddlePanel.repaint();
             MiddlePanel.updateUI();
             Layer.repaint();
             Layer.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }
     }


     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(false);

               if(common.toDouble(TQty.getText())<=0)
               {
                    JOptionPane.showMessageDialog(null,"Qty Field is Empty","Attention",JOptionPane.INFORMATION_MESSAGE);
                    TQty.requestFocus();
                    BOk.setEnabled(true);
                    return;
               }
               Vector VMrsId = new Vector();

               try
               {
                    if(RowData.length>0)
                    {
                         for(int i=0;i<RowData.length;i++)
                         {
                              Boolean bValue = (Boolean)RowData[i][4];
               
                              if(!bValue.booleanValue())
                                   continue;
     
                              String SMrsId   = (String)VId.elementAt(i);
                              VMrsId.addElement(SMrsId);
                         }
                         if(VMrsId.size()==0)
                         {
                              VMrsId.addElement("0");
                         }
                    }
                    else
                    {
                         VMrsId.addElement("0");
                    }
               }
               catch(Exception ex)
               {
                    VMrsId.addElement("0");
               }

               String SMatName = BMaterial.getText();
               String SMatCode = TMatCode.getText();
               String SSupplier= TSupplier.getText();

               tabreportx.ReportTable.setValueAt(new Boolean(true),index,8);

               if(SMatName.equals("Material"))
               {
                    SMatName = TMaterial.getText();
               }
               tabreportx.ReportTable.setValueAt(SMatName,index,5);
               tabreportx.ReportTable.setValueAt(TQty.getText(),index,6);

               VCode4    .setElementAt(SMatCode,index);
               VQty4     .setElementAt(TQty.getText(),index);
               VSup_Name4.setElementAt(SSupplier,index);
               VMrsId4   .setElementAt(VMrsId,index);
               removeHelpFrame();
               tabreportx.ReportTable.requestFocus();
          }
     }

     private void removeHelpFrame()
     {
          try{theDialog.setVisible(false);}catch(Exception e){}
          
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }

     public void setDataIntoVector(String SMatCode)
     {
           VMrsNo    = new Vector();
           VMrsCode  = new Vector();
           VMrsName  = new Vector();
           VMrsQty   = new Vector();
           VId       = new Vector();



           String QS = " SELECT MRS.MrsNo, MRS.Item_Code, InvItems.Item_Name, MRS.Qty, MRS.Id "+
                       " FROM MRS INNER JOIN InvItems on MRS.Item_Code=InvItems.Item_Code "+
                       " WHERE MRS.Qty>0 And MRS.OrderNo=0 and MRS.Item_Code='"+SMatCode+"'"+
                       " and MRS.MillCode="+iMillCode;

           try
           {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
                 
                 ResultSet res = stat.executeQuery(QS);

                 while (res.next())
                 {
                    VMrsNo    .addElement(res.getString(1));
                    VMrsCode  .addElement(res.getString(2));
                    VMrsName  .addElement(res.getString(3));
                    VMrsQty   .addElement(res.getString(4));
                    VId       .addElement(res.getString(5));
                 }
                 res.close();
                 stat.close();
           }
           catch(Exception ex){System.out.println(ex);}
     }
     public void setRowData()
     {
         RowData     = new Object[VMrsNo.size()][ColumnData.length];
         for(int i=0;i<VMrsNo.size();i++)
         {
               RowData[i][0]  = (String)VMrsNo    .elementAt(i);
               RowData[i][1]  = (String)VMrsCode  .elementAt(i);
               RowData[i][2]  = (String)VMrsName  .elementAt(i);
               RowData[i][3]  = (String)VMrsQty   .elementAt(i);
               RowData[i][4]  = new Boolean(false);
        }  
     }

}
