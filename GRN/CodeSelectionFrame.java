package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class CodeSelectionFrame extends JInternalFrame
{
     
     
     JLayeredPane   Layer;
     Vector         VInwNo,VInwName;
     TabReport      tabreportx;

     Vector         VCode,VName,VNameCode,VInwNox;
     Vector         VDescx,VCodex,VQtyx,VSup_Namex,VIdx,VIssStatusx;

     JButton        BMaterial,BOk;
     JTextField     TMatCode,TSupplier,TMaterial;
     JDialog        theDialog;
     NextField      TQty;

     JPanel         TopPanel,MiddlePanel,BottomPanel,CashPanel;
     JComboBox      JCType;

     int            index = 0;
     int            iMillCode;
     String         SItemTable;

     Common common = new Common();


     CodeSelectionFrame(JLayeredPane Layer,Vector VInwNo,Vector VInwName,TabReport tabreportx,Vector VCode,Vector VName,Vector VNameCode,Vector VInwNox,Vector VDescx,Vector VCodex,Vector VQtyx,Vector VSup_Namex,Vector VIdx,Vector VIssStatusx,JDialog theDialog,int iMillCode,String SItemTable)
     {
          this.Layer      = Layer;
          this.VInwNo     = VInwNo;
          this.VInwName   = VInwName;
          this.tabreportx = tabreportx;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.VInwNox    = VInwNox;
          this.VDescx     = VDescx;
          this.VCodex     = VCodex;
          this.VQtyx      = VQtyx;
          this.VSup_Namex = VSup_Namex;
          this.VIdx       = VIdx;
          this.VIssStatusx= VIssStatusx;
          this.theDialog  = theDialog;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          BMaterial      = new JButton("Material");
          BOk            = new JButton("Update Item");
          JCType         = new JComboBox(VInwName);
          TQty           = new NextField();
          TMatCode       = new JTextField();
          TMaterial      = new JTextField();
          TSupplier      = new JTextField();
          TopPanel       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BottomPanel    = new JPanel(true);
          CashPanel      = new JPanel(true);
     }

     private void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(5,2));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,350,300);
     }

     private void addComponents()
     {
          TopPanel       .add(new JLabel("Select Material"));
          TopPanel       .add(BMaterial);
          TopPanel       .add(new JLabel("Supplier"));
          TopPanel       .add(TSupplier);
          TopPanel       .add(new JLabel("Inward Type"));
          TopPanel       .add(JCType);
          TopPanel       .add(new JLabel("Description @Gate"));
          TopPanel       .add(TMaterial);
          TopPanel       .add(new JLabel("Qty"));
          TopPanel       .add(TQty);

          BottomPanel    .add(BOk);

          getContentPane().add("North",TopPanel);
          getContentPane().add("South",BOk);
		 
          CashPanel      .setLayout(new BorderLayout()); 
          CashPanel      .add("North",TopPanel);
          CashPanel      .add("Center",MiddlePanel);
          CashPanel      .add("South",BOk);

          setPresets();
     }

     private void setPresets()
     {
          index       = tabreportx.ReportTable.getSelectedRow(); 
          int iType   = common.toInt((String)VInwNox.elementAt(index));
          int iIssStatus = common.toInt((String)VIssStatusx.elementAt(index));

          JCType    .setSelectedIndex(iType);

          TMaterial .setText((String)VDescx.elementAt(index)); 
          TQty      .setText((String)VQtyx.elementAt(index));
          TMatCode  .setText((String)VCodex.elementAt(index));
          TSupplier .setText((String)VSup_Namex.elementAt(index));

          TMaterial .setEditable(false);
          TSupplier .setEditable(false);
          TQty      .setEditable(false);
          JCType.setEnabled(false);

          if(iIssStatus==2)
          {
               BMaterial.setEnabled(false);
               BOk.setEnabled(false);
          }
     }

     private void addListeners()
     {
         BMaterial  .addActionListener(new MaterialList(this));
         BOk        .addActionListener(new ActList());
     }
               
     private class MaterialList implements ActionListener
     {

          CodeSelectionFrame codeselectionframe;

          public MaterialList(CodeSelectionFrame codeselectionframe)
          {
               this.codeselectionframe = codeselectionframe;
          }
          public void actionPerformed(ActionEvent ae)
          {
               
               Frame dummy = new Frame();
               JDialog theDialog = new JDialog(dummy,"Description Dialog",true);

               DyesChemicalsSearch MS = new DyesChemicalsSearch(Layer,TMatCode,BMaterial,VCode,VName,VNameCode,theDialog,SItemTable);
               theDialog.getContentPane().add(MS.MaterialPanel);
               theDialog.setBounds(80,100,450,350);
               theDialog.setVisible(true);
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(false);

               String SMatName = BMaterial.getText();
               String SMatCode = TMatCode.getText();

               if(SMatName.equals("Material"))
               {
                    JOptionPane.showMessageDialog(null,"Item Not Selected","Attention",JOptionPane.INFORMATION_MESSAGE);
                    BOk.setEnabled(false);
                    BMaterial.requestFocus();
               }
               else
               {
                    try
                    {
                         ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                         Connection      theConnection =  oraConnection.getConnection();
                         Statement       stat =  theConnection.createStatement();
          
                        int iGId = common.toInt((String)VIdx.elementAt(index));

                        String QS = " Update GateInward set "+
                                    " IssueStatus=1,Item_Name='',"+
                                    " Item_Code='"+SMatCode+"'"+
                                    " Where MillCode=1 and Id="+iGId;

                        stat.execute(QS);
                        stat.close();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }

                    tabreportx.ReportTable.setValueAt(SMatName,index,5);
                    VCodex    .setElementAt(SMatCode,index);
                    VIssStatusx.setElementAt("1",index);
                    removeHelpFrame();
                    tabreportx.ReportTable.requestFocus();
               }
          }
     }

     private void removeHelpFrame()
     {
          try{theDialog.setVisible(false);}catch(Exception e){}
          
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }

}
