package GRN;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class ControlFigures
{
     FileWriter     FW;
     int            iMillCode;
     int            iSize;
     
     String         str1 = "|------------------------------------------------------------------------------------------------------------------------------------------------|";
     String         str2 = "|              |                   O R D E R E D                                |                            G R N                               |"; 
     String         str3 = "|              |---------------------------------------------------------------------------------------------------------------------------------|";
     String         str4 = "| Transactions |        SST |        SSP |        NST |        NSP |        NPR |        SST |        SSP |        NST |        NSP |        NPR |";
     String         str5 = "|              |---------------------------------------------------------------------------------------------------------------------------------|";
     String         str6 = "-------------------------------------------------------------------------------------------------------------------------------------------------|";
     
     Vector         VMaxOrdNo,VMaxGRNNo;
     Vector         VMaxOrdDate,VMaxGRNDate;
     Vector         VOrdPendings,VGRNPendings;
     
     Common         common = new Common();
     
     ControlFigures(FileWriter FW,int iMillCode) throws Exception
     {
          this.FW        = FW;
          this.iMillCode = iMillCode;

          setVectors();

          String strl1 = "| "+common.Pad("Last No",12)+" | "+
                              common.Rad((String)VMaxOrdNo.elementAt(0),10)+" | "+
                              common.Rad((String)VMaxOrdNo.elementAt(1),10)+" | "+
                              common.Rad((String)VMaxOrdNo.elementAt(2),10)+" | "+
                              common.Rad((String)VMaxOrdNo.elementAt(3),10)+" | "+
                              common.Rad((String)VMaxOrdNo.elementAt(4),10)+" | "+
                              common.Rad((String)VMaxGRNNo.elementAt(0),10)+" | "+
                              common.Rad((String)VMaxGRNNo.elementAt(1),10)+" | "+
                              common.Rad((String)VMaxGRNNo.elementAt(2),10)+" | "+
                              common.Rad((String)VMaxGRNNo.elementAt(3),10)+" | "+
                              common.Rad((String)VMaxGRNNo.elementAt(4),10)+" |";

          String strl2 = "| "+common.Pad("Last Date",12)+" | "+
                              common.Rad((String)VMaxOrdDate.elementAt(0),10)+" | "+
                              common.Rad((String)VMaxOrdDate.elementAt(1),10)+" | "+
                              common.Rad((String)VMaxOrdDate.elementAt(2),10)+" | "+
                              common.Rad((String)VMaxOrdDate.elementAt(3),10)+" | "+
                              common.Rad((String)VMaxOrdDate.elementAt(4),10)+" | "+
                              common.Rad((String)VMaxGRNDate.elementAt(0),10)+" | "+
                              common.Rad((String)VMaxGRNDate.elementAt(1),10)+" | "+
                              common.Rad((String)VMaxGRNDate.elementAt(2),10)+" | "+
                              common.Rad((String)VMaxGRNDate.elementAt(3),10)+" | "+
                              common.Rad((String)VMaxGRNDate.elementAt(4),10)+" |";

          String strl3 = "| "+common.Pad("Pendings",12)+" | "+
                              common.Rad((String)VOrdPendings.elementAt(0),10)+" | "+
                              common.Rad((String)VOrdPendings.elementAt(1),10)+" | "+
                              common.Rad((String)VOrdPendings.elementAt(2),10)+" | "+
                              common.Rad((String)VOrdPendings.elementAt(3),10)+" | "+
                              common.Rad((String)VOrdPendings.elementAt(4),10)+" | "+
                              common.Rad((String)VGRNPendings.elementAt(0),10)+" | "+
                              common.Rad((String)VGRNPendings.elementAt(1),10)+" | "+
                              common.Rad((String)VGRNPendings.elementAt(2),10)+" | "+
                              common.Rad((String)VGRNPendings.elementAt(3),10)+" | "+
                              common.Rad((String)VGRNPendings.elementAt(4),10)+" |";

          FW.write(str1+"\n");
          FW.write(str2+"\n");
          FW.write(str3+"\n");
          FW.write(str4+"\n");
          FW.write(str5+"\n");
          FW.write(strl1+"\n");
          FW.write(strl2+"\n");
          FW.write(strl3+"\n");
          FW.write(str6+"\n");
     }

     private void setVectors()
     {
          VMaxOrdNo     = new Vector();
          VMaxGRNNo     = new Vector(); 
          VMaxOrdDate   = new Vector();
          VMaxGRNDate   = new Vector();
          VOrdPendings  = new Vector();
          VGRNPendings  = new Vector();
          for(int i=0;i<5;i++)
          {
               VOrdPendings.addElement("0");
               VGRNPendings.addElement("0");
          }
          String QS1 = "";
          String QS2 = "";
          String QS3 = "";
          String QS4 = "";
          String QS5 = "";
          String QS6 = "";
          
          QS1 =     " Select Max(OrderNo) From PurchaseOrder Where OrderDate > '20050331' And MillCode="+iMillCode+" And OrderBlock = ";
          QS2 =     " Select Max(GrnNo) From GRN Where GRNDate > '20050331' And MillCode="+iMillCode+" And GRNBlock = ";
          QS3 =     " Select Max(OrderDate) From PurchaseOrder Where OrderDate > '20050331' And MillCode="+iMillCode+" And OrderBlock = ";
          QS4 =     " Select Max(GrnDate) From GRN Where GRNDate > '20050331' And MillCode="+iMillCode+" And GRNBlock = ";
          QS5 =     " SELECT OrderBlock,count(OrderNo) "+
                    " FROM (SELECT OrderNo, OrderBlock "+
                    " FROM PurchaseOrder WHERE InvQty<Qty And MillCode="+iMillCode+
                    " GROUP BY OrderNo, OrderBlock) "+
                    " GROUP BY OrderBlock Order By 1,2 ";
          QS6 =     " SELECT GrnBlock,count(GrnNo) "+
                    " FROM (SELECT GrnNo, GrnBlock "+
                    " FROM GRN WHERE SPJNo = 0 And Qty > 0 And MillCode="+iMillCode+
                    " GROUP BY GrnNo, GrnBlock) "+
                    " GROUP BY GrnBlock Order By 1,2 ";

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
               for(int i=0;i<5;i++)
               {
                    ResultSet theResult = stat.executeQuery(QS1+i);
                    while(theResult.next())
                         VMaxOrdNo.addElement(theResult.getString(1));

                    theResult.close();
               }
               for(int i=0;i<5;i++)
               {
                    ResultSet theResult = stat.executeQuery(QS2+i);
                    while(theResult.next())
                         VMaxGRNNo.addElement(theResult.getString(1));

                    theResult.close();
               }
               for(int i=0;i<5;i++)
               {
                    ResultSet theResult = stat.executeQuery(QS3+i);
                    while(theResult.next())
                         VMaxOrdDate.addElement(common.parseDate(theResult.getString(1)));

                    theResult.close();
               }  
               for(int i=0;i<5;i++)
               {
                    ResultSet theResult = stat.executeQuery(QS4+i);
                    while(theResult.next())
                         VMaxGRNDate.addElement(common.parseDate(theResult.getString(1)));

                    theResult.close();
               }

               ResultSet theResult1 = stat.executeQuery(QS5);
               while(theResult1.next())
                    VOrdPendings.setElementAt(theResult1.getString(2),theResult1.getInt(1));

               theResult1.close();

               ResultSet theResult2 = stat.executeQuery(QS6);
               while(theResult2.next())
                    VGRNPendings.setElementAt(theResult2.getString(2),theResult2.getInt(1));

               theResult2.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}
