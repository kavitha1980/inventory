package GRN;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGRNCollectionFrame extends JInternalFrame
{

     JTextField     TGINo,TGIDate,TInvDate,TDCDate;
     DateField      TDate;
     JTextField     TInvNo,TDCNo,TSupName;
     WholeNumberField TInvoice;
     DirectGRNMiddlePanel MiddlePanel;
     JPanel         TopPanel,BottomPanel;
     JButton        BOk,BCancel;

     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel  SPanel;
     String SIndex;
     int iIndex=0;
     JTable ReportTable;
     String SSupCode;
     int iMillCode;

     JLabel LGrnNo ;

     Common common   = new Common();

     Connection theConnection=null;
     
     DirectGRNCollectionFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,String SIndex,JTable ReportTable,String SSupCode,int iMillCode)
     {
          this.DeskTop            = DeskTop;
          this.VCode              = VCode;
          this.VName              = VName;
          this.SPanel             = SPanel;
          this.SIndex             = SIndex;
          this.ReportTable        = ReportTable;
          this.SSupCode           = SSupCode;
          this.iMillCode          = iMillCode;

          iIndex = common.toInt(SIndex);
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }
     public void createComponents()
     {
          BOk        = new JButton("Okay");
          BCancel    = new JButton("Abort");

          TDate       = new DateField();
          TGINo       = new JTextField();
          LGrnNo      = new JLabel();
          
          TGIDate     = new JTextField();
          TInvDate    = new JTextField();
          TDCDate     = new JTextField();
          TDCDate     = new JTextField();
          TInvNo      = new JTextField();
          TDCNo       = new JTextField();
          TSupName    = new JTextField();
          TInvoice    = new WholeNumberField(2);

          MiddlePanel = new DirectGRNMiddlePanel(DeskTop,SSupCode,iMillCode,LGrnNo);

          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          TDate.setTodayDate();

          TGINo.setEditable(false);
          TGIDate.setEditable(false);
          TSupName.setEditable(false);
     }
     public void setLayouts()
     {
          setTitle("Invoice Valuation of Materials recd without orders");

          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);

          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(5,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());
     }
     public void addComponents()
     {
          try
          {
               TopPanel.add(new JLabel("Gate Inward No"));
               TopPanel.add(TGINo);
     
               TopPanel.add(new JLabel("Gate Inward Date"));
               TopPanel.add(TGIDate);
     
               TopPanel.add(new JLabel("GRN No"));
               TopPanel.add(LGrnNo);
     
               TopPanel.add(new JLabel("GRN Date"));
               TopPanel.add(TDate);
     
               TopPanel.add(new JLabel("Supplier"));
               TopPanel.add(TSupName);
     
               TopPanel.add(new JLabel("No. of Bills"));
               TopPanel.add(TInvoice);
     
               TopPanel.add(new JLabel("Invoice No"));
               TopPanel.add(TInvNo);
     
               TopPanel.add(new JLabel("Invoice Date"));
               TopPanel.add(TInvDate);
     
               TopPanel.add(new JLabel("DC No"));
               TopPanel.add(TDCNo);
     
               TopPanel.add(new JLabel("DC Date"));
               TopPanel.add(TDCDate);
     
               BottomPanel.add(BOk);
               BottomPanel.add(BCancel);
               getContentPane().add(TopPanel,BorderLayout.NORTH);
               getContentPane().add(MiddlePanel,BorderLayout.CENTER);
               getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     
               TGINo.setText((String)ReportTable.getModel().getValueAt(iIndex,0));
               TGIDate.setText((String)ReportTable.getModel().getValueAt(iIndex,1));
               TSupName.setText((String)ReportTable.getModel().getValueAt(iIndex,2));
               TInvNo.setText((String)ReportTable.getModel().getValueAt(iIndex,3));
               TInvDate.setText((String)ReportTable.getModel().getValueAt(iIndex,4));
               TDCNo.setText((String)ReportTable.getModel().getValueAt(iIndex,5));
               TDCDate.setText((String)ReportTable.getModel().getValueAt(iIndex,6));
               MiddlePanel.createComponents(TGINo.getText());

          }
          catch(Exception e)
          {
              System.out.println(e);
          }
     }
     public void addListeners()
     {
        BOk.addActionListener(new ActList());
     }
     public class ActList implements ActionListener
     {
            public void actionPerformed(ActionEvent ae)
            {
                    if(validateGRN())
                         insertGRNCollectedData();
            }
     }
     private void insertGRNCollectedData()
     {
          BOk.setEnabled(false);
          MiddlePanel.setGRNNo();
          insertGRNDetails();
          setGILink();
          setOrderLink();
          if(iMillCode==1)
          {
               updateSubStoreMasterData();
          }
          setMRSLink();
          removeHelpFrame();
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
     public void setGILink()
     {
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();

               for(int i=0;i<MiddlePanel.RD.length;i++)
               {
                    Boolean BValue = (Boolean)MiddlePanel.RD[i][3];
                    if(BValue.booleanValue())
                    {
                         String QS = "Update GateInward Set ";
                         QS = QS+" GrnNo=1";
                         QS = QS+" Where Id = "+(String)MiddlePanel.VGId.elementAt(i); 
                         stat.execute(QS);
                    }
               }
               stat.close();
          }
          catch(Exception ex)
          {
                System.out.println("E1"+ex);
          }
     }
     public void insertGRNDetails()
     {
          String QString = "Insert Into GRN (OrderNo,MRSNo,GrnNo,GrnDate,GrnBlock,Sup_Code,GateInNo,GateInDate,InvNo,InvDate,DcNo,DcDate,Code,InvQty,MillQty,Pending,OrderQty,Qty,InvRate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,InvAmount,InvNet,plus,Less,Misc,Dept_Code,Group_Code,Unit_Code,InvSlNo,ActualModVat,NoOfBills,MillCode,id,slno,mrsslno,orderslno,taxclaimable) Values (";
          String QSL     = "Select Max(InvSlNo) From GRN";
          String QS="";
          int iInvSlNo=0,iSlNo=0;
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
               ResultSet res        = stat.executeQuery(QSL);
               while(res.next())
               {
                    iInvSlNo = res.getInt(1);
               }
               res.close();
               iInvSlNo++;

               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();                 
               String SAdd       = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess      = MiddlePanel.MiddlePanel.TLess.getText();

               double dpm        = common.toDouble(SAdd)-common.toDouble(SLess);
               double dActVat    = MiddlePanel.MiddlePanel.getModVat();
               double dVatBasic  = MiddlePanel.MiddlePanel.getVatBasic();
               double dBasic     = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio     = dpm/dBasic;

               double dVatRatio  = dActVat/dVatBasic;
               String SGrnNo     = LGrnNo.getText();

               for(int i=0;i<RowData.length;i++)
               {
                    double dDcQty     = common.toDouble((String)RowData[i][6]);
                    if(dDcQty == 0)
                         continue;

                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SBlockCode = MiddlePanel.getBlockCode(i);
                    String SOrderNo   = MiddlePanel.getOrderNo(i);
                    String SMRSNo     = MiddlePanel.getMRSNo(i);
                    String SOrdQty    = MiddlePanel.getOrdQty(i);

                    iSlNo++;
                    String SMrsSLNo   = MiddlePanel.getMrsSLNo(i);
                    String SOrderSLNo = MiddlePanel.getOrderSLNo(i);
                    String STaxC      = MiddlePanel.getTaxC(i);

                    dBasic            = common.toDouble((String)RowData[i][13]);
                    double dCenVatPer = common.toDouble((String)RowData[i][10]);
                    String SCenVat    = "0";

                    if(dCenVatPer > 0)
                         SCenVat = common.getRound(dBasic*dVatRatio,3);

                    String SMisc      = common.getRound(dBasic*dRatio,3);

                    String  SItemCode = (String)RowData[i][0];

                    String  SInvAmount =(String)RowData[i][18];

                    String  SMillQty   =(String)RowData[i][7];
                                             
                    String QS1 = QString;
                    QS1 = QS1+"0"+SOrderNo+",";
                    QS1 = QS1+"0"+SMRSNo+",";
                    QS1 = QS1+"0"+SGrnNo+",";
                    QS1 = QS1+"'"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                    QS1 = QS1+"0"+SBlockCode+",";
                    QS1 = QS1+"'"+SSupCode+"',";
                    QS1 = QS1+"0"+TGINo.getText()+",";
                    QS1 = QS1+"'"+common.pureDate(TGIDate.getText())+"',";
                    QS1 = QS1+"'"+TInvNo.getText()+"',";
                    QS1 = QS1+"'"+common.pureDate(TInvDate.getText())+"',";
                    QS1 = QS1+"'"+TDCNo.getText()+"',";
                    QS1 = QS1+"'"+common.pureDate(TDCDate.getText())+"',";
                    QS1 = QS1+"'"+SItemCode+"',";
                    QS1 = QS1+"0"+(String)RowData[i][6]+",";
                    QS1 = QS1+"0"+SMillQty+",";
                    QS1 = QS1+"0"+(String)RowData[i][5]+",";
                    QS1 = QS1+"0"+SOrdQty+",";
                    QS1 = QS1+"0"+(String)RowData[i][6]+",";
                    QS1 = QS1+"0"+(String)RowData[i][8]+",";
                    QS1 = QS1+"0"+(String)RowData[i][9]+",";
                    QS1 = QS1+"0"+(String)RowData[i][14]+",";
                    QS1 = QS1+"0"+(String)RowData[i][10]+",";
                    QS1 = QS1+"0"+(String)RowData[i][15]+",";
                    QS1 = QS1+"0"+(String)RowData[i][11]+",";
                    QS1 = QS1+"0"+(String)RowData[i][16]+",";
                    QS1 = QS1+"0"+(String)RowData[i][12]+",";
                    QS1 = QS1+"0"+(String)RowData[i][17]+",";
                    QS1 = QS1+"0"+SInvAmount+",";
                    QS1 = QS1+"0"+SInvAmount+",";
                    QS1 = QS1+"0"+SAdd+",";
                    QS1 = QS1+"0"+SLess+",";
                    QS1 = QS1+"0"+SMisc+",";
                    QS1 = QS1+"0"+SDeptCode+",";
                    QS1 = QS1+"0"+SGroupCode+",";
                    QS1 = QS1+"0"+SUnitCode+",";
                    QS1 = QS1+"0"+iInvSlNo+",";
                    QS1 = QS1+"0"+SCenVat+",";
                    QS1 = QS1+"0"+TInvoice.getText()+",";
                    QS1 = QS1+"0"+iMillCode+",grn_seq.nextval,"+iSlNo +","+SMrsSLNo+","+SOrderSLNo+","+STaxC +")";

                    stat.executeUpdate(QS1);

                    updateItemMaster(dDcQty,SItemCode,STaxC,SBlockCode,SInvAmount,SMillQty,SMisc,SCenVat);
               }
               if(iMillCode==0)
               {
                    QS = "Update Config set MaxNo="+SGrnNo+" Where Id=3";
               }
               else
               {
                    QS = "Update Config set MaxNo="+SGrnNo+" Where Id=4";
               }
               stat.executeUpdate(QS);

               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
          }
     }
     public void setOrderLink()
     {
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();                 
               for(int i=0;i<RowData.length;i++)
               {
                    double dDcQty     = common.toDouble((String)RowData[i][6]);
                    if(dDcQty == 0)
                         continue;

                    String QS = "Update PurchaseOrder Set ";
                    QS = QS+" InvQty=InvQty+"+(String)RowData[i][7];
                    QS = QS+" Where Id = "+(String)MiddlePanel.VId.elementAt(i);
                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
                System.out.println("E3"+ex);
          }
     }
     public void setMRSLink()
     {
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();                 
               for(int i=0;i<RowData.length;i++)
               {
                    double dDcQty     = common.toDouble((String)RowData[i][6]);
                    if(dDcQty == 0)
                         continue;

                    String SMrsSLNo   = MiddlePanel.getMrsSLNo(i);

                    String QS = "Update MRS Set ";
                    QS = QS+" GrnNo="+MiddlePanel.getGRNNo(i);
                    QS = QS+" Where MRSNo = "+(String)MiddlePanel.VGMRSNo.elementAt(i)+" and Item_Code = '"+(String)MiddlePanel.RowData[i][0]+"' And SlNo="+SMrsSLNo;

                    if(iMillCode == 0)
                         QS = QS+" And (MRS.MillCode Is Null Or Mrs.MillCode = 0) ";
                    else
                         QS = QS+" And Mrs.MillCode = 1 ";

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
                System.out.println("E4"+ex);
          }
     }
     private boolean validateGRN()
     {
               BOk.setEnabled(false);
               boolean bflag=false;
               String str="";

               int iInvoice = common.toInt(TInvoice.getText());
               if(iInvoice==0)
               {
                    str="No of Bills Field is Empty";
                    bflag=true;
               }
               if(isQtyDiff())
               {
                    str="Quantity Mismatch";
                    bflag=true;
               }
               if(isQtyNotTouched())
               {
                    str=str+"\n"+"Invalid GRN - All Zero";
                    bflag=true;
               }

               if(isGateNotTicked())
               {
                    str = str+"\n"+"Some Gate Entries are Not Closed";
                    bflag=true;
               }
               if(!bflag)
                    return true;

               AcceptDeny AD = new AcceptDeny(str);
               AD.show();
               //AD.BAccept.addActionListener(new ADList(AD));
               AD.BDeny.addActionListener(new ADList(AD));
               return false;                              
     }
     private boolean isQtyDiff()
     {
          Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<RowData.length;i++)
          {
               double dDcQty   = common.toDouble((String)RowData[i][6]);
               double dInvQty  = common.toDouble((String)RowData[i][7]);
               if(dDcQty != dInvQty)
                    return true;
          }
          return false;
     }
     private boolean isQtyNotTouched()
     {
          double dTDCQty=0;
          Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<RowData.length;i++)
          {
               dTDCQty   = dTDCQty+common.toDouble((String)RowData[i][6]);
          }
          if(dTDCQty == 0)
               return true;
          return false;
     }
     private boolean isGateNotTicked()
     {
          for(int i=0;i<MiddlePanel.RD.length;i++)
          {
               Boolean BValue = (Boolean)MiddlePanel.RD[i][3];
               if(!BValue.booleanValue())
                    return true;
          }
          return false;
     }
     private class ADList implements ActionListener
     {
               AcceptDeny AD;
               ADList(AcceptDeny AD)
               {
                    this.AD = AD;
               }
               public void actionPerformed(ActionEvent ae)
               {
                    if(ae.getSource()==AD.BDeny)
                    {
                         BOk.setEnabled(true);
                         AD.setVisible(false);
                    }
               }
     }

     private void updateSubStoreMasterData()
     {
          try
          {
               if(theConnection==null)
               {
                    ORAConnection1 jdbc  =    ORAConnection1.getORAConnection();
                    theConnection        =    jdbc.getConnection();
               }
               Statement stat   =    theConnection.createStatement();

               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();                 
               for(int i=0;i<RowData.length;i++)
               {
                    double dDcQty     = common.toDouble((String)RowData[i][6]);
                    if(dDcQty == 0)
                         continue;

                    String SItemCode  = (String)RowData[i][0];
                    int iCount=0;

                    ResultSet res = stat.executeQuery("Select count(*) from InvItems Where Item_Code='"+SItemCode+"'");
                    while(res.next())
                    {
                         iCount   =    res.getInt(1);
                    }
                    res.close();

                    if(iCount==0)
                         continue;

                    int iBlockCode = common.toInt(MiddlePanel.getBlockCode(i));

                    String QS = "";

                    if(iBlockCode>1)
                    {
                         QS = "Update InvItems Set ";
                         QS = QS+" MSRecQty=MSRecQty+"+(String)RowData[i][7]+",";
                         QS = QS+" MSIssQty=MSIssQty+"+(String)RowData[i][7]+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    else
                    {
                         QS = "Update InvItems Set ";
                         QS = QS+" MSRecQty=MSRecQty+"+(String)RowData[i][7]+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("E6"+e);
          }

     }
     private void updateItemMaster(double dDcQty,String SItemCode,String TaxC,String SBlockCode,String SInvAmount,String SMillQty,String SMisc,String SCenVat)
     {
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement stat   =    theConnection.createStatement();

               int iBlockCode = common.toInt(SBlockCode);

               String QS = "";

               double dValue =0;

               if(TaxC.equals("1"))
               dValue=common.toDouble(SInvAmount)+common.toDouble(SMisc)-common.toDouble(SCenVat);
               else
               dValue=common.toDouble(SInvAmount)+common.toDouble(SMisc);

               if(iMillCode==0)
               {
                    if(iBlockCode>1)
                    {
                         QS = "Update InvItems Set  RecVal =RecVal+"+dValue;
                         QS = QS+", RecQty=RecQty+"+SMillQty+",";
                         QS = QS+" IssQty=IssQty+"+SMillQty+",";
                         QS = QS+" IssVal=IssVal+"+dValue+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    else
                    {
                         QS = "Update InvItems Set RecVal =RecVal+"+dValue;
                         QS = QS+", RecQty=RecQty+"+SMillQty+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    stat.execute(QS);
               }
               else
               {

                    if(iBlockCode>1)
                    {
                         QS = "Update DyeingInvItems Set RecVal =RecVal+"+dValue;
                         QS = QS+" ,RecQty=RecQty+"+SMillQty+",";
                         QS = QS+" IssQty=IssQty+"+SMillQty+",";
                         QS = QS+" IssVal=IssVal+"+dValue+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    else
                    {
                         QS = "Update DyeingInvItems Set RecVal =RecVal+"+dValue;
                         QS = QS+", RecQty=RecQty+"+SMillQty+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("E6"+e);
          }

     }


}
