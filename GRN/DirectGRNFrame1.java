package GRN;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGRNFrame1 extends JInternalFrame
{
     DateField                TDate;
     JTextField               TSupCode;
     WholeNumberField         TInvoice;
     DateField                TInvDate,TDCDate;
     MyTextField              TInvNo,TDCNo;
     InwardModeModel          InwMode;

     DirectGRNMiddlePanel1    MiddlePanel;
     JPanel                   TopPanel,BottomPanel;
     JButton                  BOk,BApply,BSupplier;
     
     JLayeredPane             DeskTop;
     Vector                   VCode,VName,VNameCode;
     StatusPanel              SPanel;
     int                      iMillCode,iUserCode,iAuthCode;
     String                   SYearCode;
     String                   SItemTable,SSupTable;

     JLabel                   LGrnNo;
     JComboBox                JCInwType,JCSort;

     Vector                   VInwName,VInwNo;

     Connection               theMConnection = null;
     Connection               theDConnection = null;

     boolean                  bComflag       = true;
     Common                   common         = new Common();
     JComboBox       	     cmbInvoiceType ;
     int iInvCheckCount		= -1, iTrialPendingCount=-1;

     int iEntryCount=0;

     String 				SGrnNo="",SGateNo="";
     Vector   				VInvTypeCode , VInvTypeName ;

     public DirectGRNFrame1(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iMillCode,int iAuthCode,int iUserCode,String SYearCode,String SItemTable,String SSupTable)
     {
          this.DeskTop            = DeskTop;
          this.VCode              = VCode;
          this.VName              = VName;
          this.VNameCode          = VNameCode;
          this.SPanel             = SPanel;
          this.iMillCode          = iMillCode;
          this.iAuthCode          = iAuthCode;
          this.iUserCode          = iUserCode;
          this.SYearCode          = SYearCode;
          this.SItemTable         = SItemTable;
          this.SSupTable          = SSupTable;

          if(iMillCode==1)
          {
               DORAConnection jdbc           = DORAConnection.getORAConnection();
                              theDConnection = jdbc.getConnection();
          }

          ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                         theMConnection = oraConnection.getConnection();
          

	   iInvCheckCount   	=       setPendingInvCheck();
	   iTrialPendingCount	= 	  getTrialPendingCount();
	if(iTrialPendingCount==0){	   
	  if(iInvCheckCount!=0){
          setInwardData();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
	 }
    else{
	  return;
      }
   }
else{
	JOptionPane.showMessageDialog(null," Please Select Users for Corresponding TrialOrders","Information",JOptionPane.INFORMATION_MESSAGE);
	return;
}


	    
     }

     public void createComponents()
     {
          BOk        = new JButton("Okay");
          BApply     = new JButton("Apply");
          BSupplier  = new JButton("Supplier");
          
          TDate       = new DateField();
          LGrnNo      = new JLabel();
          
          TSupCode    = new JTextField();
          TInvoice    = new WholeNumberField(2);

          TInvDate    = new DateField();
          TDCDate     = new DateField();
          TInvNo      = new MyTextField(15);
          TDCNo       = new MyTextField(15);

          JCInwType   = new JComboBox(VInwName);
          InwMode     = new InwardModeModel(iUserCode,iAuthCode,iMillCode);
          JCSort      = new JComboBox();
          
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();
		setInvoiceTypeVector();
  	     cmbInvoiceType   = new JComboBox(VInvTypeName);
		cmbInvoiceType.setSelectedItem(VInvTypeName.get(0));

          
          TDate.setTodayDate();
          
          TDate.setEditable(false);
          TSupCode.setEditable(false);

          JCInwType.setSelectedIndex(0);
          JCInwType.setEnabled(false);

          LGrnNo.setText("To be determined");

          InwMode.TCode.setText("11");
          InwMode.setText("RENGAVILAS");
     }

     public void setLayouts()
     {
          setTitle("Invoice Valuation of Materials recd against order 1");
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,1000,650);
          
          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(6,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());

          BSupplier.setBorder(new javax.swing.border.BevelBorder(0));
          BSupplier.setBackground(new Color(128,128,255));
          BSupplier.setForeground(Color.RED);

          TopPanel.setBorder(new TitledBorder(""));
     }

     public void addComponents()
     {
          try
          {
               JCSort.addItem("OrderNo");
               JCSort.addItem("Name");
               JCSort.addItem("Code");

               TopPanel.add(new JLabel("GRN No"));
               TopPanel.add(LGrnNo);
               
               TopPanel.add(new JLabel("GRN Date"));
               TopPanel.add(TDate);
               
               TopPanel.add(new JLabel("Supplier"));
               TopPanel.add(BSupplier);
               
               TopPanel.add(new JLabel("No. of Bills"));
               TopPanel.add(TInvoice);

               TopPanel.add(new JLabel("Invoice No"));
               TopPanel.add(TInvNo);
     
               TopPanel.add(new JLabel("Invoice Date"));
               TopPanel.add(TInvDate);
     
               TopPanel.add(new JLabel("DC No"));
               TopPanel.add(TDCNo);
     
               TopPanel.add(new JLabel("DC Date"));
               TopPanel.add(TDCDate);

               TopPanel.add(new JLabel("Select Inward Type"));
               TopPanel.add(JCInwType);

               TopPanel.add(new JLabel("Sorting Pending Orders By"));
               TopPanel.add(JCSort);

               TopPanel.add(new JLabel("Select Inward Mode"));
               TopPanel.add(InwMode);


			TopPanel.add(cmbInvoiceType);
           	

               TopPanel.add(BApply);

               
               BottomPanel.add(BOk);
               getContentPane().add(TopPanel,BorderLayout.NORTH);
  	        
          }
          catch(Exception e)
          {
               System.out.println(e);
          }
     }

     public void addListeners()
     {
          BApply.addActionListener(new ActList());
          BOk.addActionListener(new ActList());
          BSupplier.addActionListener(new SupplierSearch(DeskTop,TSupCode,SSupTable));


     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
                    setMiddlePanel();

               }

               if(ae.getSource()==BOk)
               {
                    BOk.setEnabled(false);

                    if(validateGRN())
                    {
			          TDate.setTodayDate();
                            insertGRNCollectedData();
			          getACommit();
                    }
                    else
                    {
                         BOk.setEnabled(true);
                    }
               }
          }
     }

     private void setMiddlePanel()
     {

          String SSupCode = TSupCode.getText();
          String SSupName = BSupplier.getText();
          int iSortIndex  = JCSort.getSelectedIndex();

          if(SSupCode.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Please Select the Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
          }
          else
          {
               int iCount = checkPendingOrder(SSupCode);

               if(iCount<=0)
               {
                    JOptionPane.showMessageDialog(null,"There is no Pending Orders for this Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
               }

               BSupplier.setEnabled(false);
               BApply.setEnabled(false);
               JCInwType.setEnabled(false);
               JCSort.setEnabled(false);

               MiddlePanel = new DirectGRNMiddlePanel1(DeskTop,iMillCode,SSupCode,SSupName,iSortIndex);

               getContentPane().add(MiddlePanel,BorderLayout.CENTER);
               getContentPane().add(BottomPanel,BorderLayout.SOUTH);

               DeskTop.repaint();
               DeskTop.updateUI();
          }
     }

     private int checkPendingOrder(String SSupCode)
     {
          int iCount=0;

          try
          {
               String QS = " Select Count(*) from PurchaseOrder "+
                           " Where Sup_Code = '"+SSupCode+"' "+
                           " And InvQty < Qty And Authentication=1 "+
                           " And MillCode="+iMillCode;

               Statement       stat =  theMConnection.createStatement();
               
               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    iCount = res.getInt(1);    
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E1"+ex );
          }
          return iCount;
     }

     private boolean validateGRN()
     {
          iEntryCount=0;
          
          String SInvNo       = common.parseNull(TInvNo.getText().trim());
          String SInvDate     = TInvDate.toNormal();
          String SDCNo        = common.parseNull(TDCNo.getText().trim());
          String SDCDate      = TDCDate.toNormal();
          String SInwMode     = common.parseNull(InwMode.getText().trim());

          if(!SDCNo.equals(""))
          {
               if(common.toInt(SDCDate) ==0)
               {
                    JOptionPane.showMessageDialog(null,"Enter Dc Date ","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(common.toInt(SDCDate)>0)
          {
               if(SDCNo.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Enter Dc No ","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(!SInvNo.equals(""))
          {
               if(common.toInt(SInvDate) ==0)
               {
                    JOptionPane.showMessageDialog(null,"Enter Invoice Date ","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(common.toInt(SInvDate)>0)
          {
               if(SInvNo.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Enter Invoice No ","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(SDCNo.equals("") && SInvNo.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Select DC No Or Invoice No","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
               return false;

          }
          if(SInwMode.equals("Mode"))
          {
               JOptionPane.showMessageDialog(null,"Mode Is Not Select","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          int iInvoice = common.toInt(TInvoice.getText());
          if(iInvoice==0)
          {
               JOptionPane.showMessageDialog(null,"No of Bills Field is Empty","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          if(isGateNotFilled())
          {
               JOptionPane.showMessageDialog(null,"Some Gate Entries are Not Filled","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          if(isGRNNotTicked())
          {
               JOptionPane.showMessageDialog(null,"Some GRN Entries are Not filled","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          if(iEntryCount<=0)
          {
               JOptionPane.showMessageDialog(null,"No Entries Made","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          
          return true;
     }
     
     private boolean isGateNotFilled()
     {GateModel.getRows()>0)
          {
          if(MiddlePanel.GatePanel.
               for(int i=0;i<MiddlePanel.GatePanel.GateModel.getRows();i++)
               {
                    String SDesc = (String)MiddlePanel.GatePanel.GateModel.getValueAt(i,0);
                    String SQty  = (String)MiddlePanel.GatePanel.GateModel.getValueAt(i,1);

                    if(SDesc.equals("") || SQty.equals("") || common.toDouble(SQty)<=0)
                         return true;

                    iEntryCount++;

               }
          }
          return false;
     }
     
     private boolean isGRNNotTicked()
     {
          if(MiddlePanel.GrnPanel.GrnModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.GrnPanel.GrnModel.getRows();i++)
               {
                    String SQty    = (String)MiddlePanel.GrnPanel.GrnModel.getValueAt(i,8);
                    Boolean bValue = (Boolean)MiddlePanel.GrnPanel.GrnModel.getValueAt(i,10);
                    
                    if(common.toDouble(SQty)>0 && !bValue.booleanValue())
                         return true;


                    if(common.toDouble(SQty)>0 && bValue.booleanValue())
                         iEntryCount++;
               }
          }
          return false;
     }

     private void insertGRNCollectedData()
     {
		try
		{
	          if(MiddlePanel.MiddlePanel.dataModel.getRows()>0 || MiddlePanel.GatePanel.GateModel.getRows()>0)
	          {
	          	setGateNo();
			}

	          if(MiddlePanel.MiddlePanel.dataModel.getRows()>0)
	          {
		          setGRNNo();
	               insertGRNDetails();
	               //insertGateDetails();
//	               setOrderLink();
//	               setMRSLink();

	               if(iMillCode==1)
	               {
	                    updateSubStoreMasterData();
	               }
	          }

	          if(MiddlePanel.GatePanel.GateModel.getRows()>0)
	          {
	               insertDirectGateDetails();
	          }
		}
		catch(Exception ex)
          {
               System.out.println("I1 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

     private void setGRNNo()
     {
          SGrnNo="";

          String QS ="";
          try
          {
               QS = "  Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 2 for update of MaxNo noWait";

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               Statement       stat =  theMConnection.createStatement();
               
               PreparedStatement thePrepare = theMConnection.prepareStatement("Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 2"); 

               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    SGrnNo = res.getString(1);    
               }
               res.close();

               thePrepare.setInt(1,common.toInt(SGrnNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println("E2"+ex );
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    setGRNNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
     }

     private void setGateNo()
     {
          SGateNo="";

          String QS ="";
          try
          {
               QS = " Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 3 for update of MaxNo noWait";

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               Statement       stat =  theMConnection.createStatement();

               PreparedStatement thePrepare = theMConnection.prepareStatement(" Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 3"); 
               
              ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    SGateNo = res.getString(1);    
               }
               res.close();

               thePrepare.setInt(1,common.toInt(SGateNo));
            //   thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println("E3"+ex );
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    setGateNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
     }

     public void insertGRNDetails()
     {
          String QString = "Insert Into GRN (OrderNo,MRSNo,GrnNo,GrnDate,GrnBlock,Sup_Code,GateInNo,GateInDate,InvNo,InvDate,DcNo,DcDate,Code,InvQty,MillQty,Pending,OrderQty,Qty,InvRate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,InvAmount,InvNet,plus,Less,Misc,Dept_Code,Group_Code,Unit_Code,InvSlNo,ActualModVat,NoOfBills,MillCode,id,slno,mrsslno,mrsauthusercode,orderslno,orderapprovalstatus,taxclaimable,usercode,creationdate,entrystatus,grnqty,grnvalue,InvoiceType) Values (";
          String QSL     = "Select Max(InvSlNo) From GRN";
          String QS="";
          int iInvSlNo=0,iSlNo=0;
	    int iInvoiceTypeCode =-1;
          try
          {

               Statement       stat =  theMConnection.createStatement();
               ResultSet res        =  stat.executeQuery(QSL);
               while(res.next())
               {
                    iInvSlNo = res.getInt(1);
               }
               res.close();
               iInvSlNo++;
               
               Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();
               Object GrnData[][] = MiddlePanel.GrnPanel.getFromVector();
               
               String SAdd        = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess       = MiddlePanel.MiddlePanel.TLess.getText();
               
               double dpm         = common.toDouble(SAdd)-common.toDouble(SLess);
               double dBasic      = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio      = dpm/dBasic;
               
               String SInvNo       = common.parseNull(TInvNo.getText().trim());
               String SInvDate     = TInvDate.toNormal();
               String SDCNo        = common.parseNull(TDCNo.getText().trim());
               String SDCDate      = TDCDate.toNormal();

               String SSupCode = TSupCode.getText();
               String SDate    = TDate.toNormal();

               String SDateTime = common.getServerDateTime();

               String SEntryStatus = "1";

               for(int i=0;i<RowData.length;i++)
               {
                    String SSeleId = (String)MiddlePanel.VSeleId.elementAt(i);

                    int iMidRow       = getRowIndex(SSeleId,GrnData);

                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SBlockCode = MiddlePanel.getBlockCode(iMidRow);
                    String SOrdQty    = MiddlePanel.getOrdQty(iMidRow);


				String sInvTypeName		= (String)cmbInvoiceType.getSelectedItem();
				    iInvoiceTypeCode          = getInvTypeCode(sInvTypeName);
				System.out.println("Invoice Type:"+iInvoiceTypeCode);
                    
                    iSlNo++;
                    String SMrsSLNo       = MiddlePanel.getMrsSLNo(iMidRow);
                    String SMrsUserCode   = MiddlePanel.getMrsUserCode(iMidRow);
                    String SOrderSLNo     = MiddlePanel.getOrderSLNo(iMidRow);
                    String STaxC          = MiddlePanel.getTaxC(iMidRow);
                    String SOrderApproval = MiddlePanel.getOrderApproval(iMidRow);
                    
                    String SOrderNo     = (String)RowData[i][4];
                    String SMRSNo       = (String)RowData[i][3];
                    
                    String  SItemCode  = (String)RowData[i][0];
                    
                    String  SBasic     = (String)RowData[i][14];
                    String  SInvAmount = (String)RowData[i][19];
                    String  SVat       = (String)RowData[i][17];
                    
                    String SMisc       = common.getRound(common.toDouble(SBasic)*dRatio,3);
                    
                    String  SMillQty   = (String)RowData[i][8];
                    
                    String SGrnQty   = SMillQty;
                    String SGrnValue = "";
                    
                    SGrnValue = common.getRound(common.toDouble(SInvAmount) + common.toDouble(SMisc),2);

                    
                    String    QS1 = QString;
                              QS1 = QS1+"0"+SOrderNo+",";
                              QS1 = QS1+"0"+SMRSNo+",";
                              QS1 = QS1+"0"+SGrnNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"0"+SBlockCode+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"0"+SGateNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"'"+SInvNo+"',";
                              QS1 = QS1+"'"+SInvDate+"',";
                              QS1 = QS1+"'"+SDCNo+"',";
                              QS1 = QS1+"'"+SDCDate+"',";
                              QS1 = QS1+"'"+SItemCode+"',";
                              QS1 = QS1+"0"+(String)RowData[i][7]+",";
                              QS1 = QS1+"0"+SMillQty+",";
                              QS1 = QS1+"0"+(String)RowData[i][6]+",";
                              QS1 = QS1+"0"+SOrdQty+",";
                              QS1 = QS1+"0"+SMillQty+",";
                              QS1 = QS1+"0"+(String)RowData[i][9]+",";
                              QS1 = QS1+"0"+(String)RowData[i][10]+",";
                              QS1 = QS1+"0"+(String)RowData[i][15]+",";
                              QS1 = QS1+"0"+(String)RowData[i][11]+",";
                              QS1 = QS1+"0"+(String)RowData[i][16]+",";
                              QS1 = QS1+"0"+(String)RowData[i][12]+",";
                              QS1 = QS1+"0"+(String)RowData[i][17]+",";
                              QS1 = QS1+"0"+(String)RowData[i][13]+",";
                              QS1 = QS1+"0"+(String)RowData[i][18]+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SAdd+",";
                              QS1 = QS1+"0"+SLess+",";
                              QS1 = QS1+"0"+SMisc+",";
                              QS1 = QS1+"0"+SDeptCode+",";
                              QS1 = QS1+"0"+SGroupCode+",";
                              QS1 = QS1+"0"+SUnitCode+",";
                              QS1 = QS1+"0"+iInvSlNo+",";
                              QS1 = QS1+"0"+0+",";
                              QS1 = QS1+"0"+TInvoice.getText()+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"grn_seq.nextval"+",";
                              QS1 = QS1+"0"+iSlNo+",";
                              QS1 = QS1+"0"+SMrsSLNo+",";
                              QS1 = QS1+"0"+SMrsUserCode+",";
                              QS1 = QS1+"0"+SOrderSLNo+",";
                              QS1 = QS1+"0"+SOrderApproval+",";
                              QS1 = QS1+"0"+STaxC+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+SDateTime+"',";
                              QS1 = QS1+"0"+SEntryStatus+",";
                              QS1 = QS1+"0"+SGrnQty+",";
                              QS1 = QS1+"0"+SGrnValue+",";
						QS1 = QS1+"0"+iInvoiceTypeCode+ ")";


                  //  if(theMConnection  . getAutoCommit())
                   //      theMConnection  . setAutoCommit(false);

                   // stat.executeUpdate(QS1);
                    
//                    updateItemMaster(stat,SItemCode,SBlockCode,SGrnQty,SGrnValue);

                    String SMsg = " Ur Goods Received GrnNo - "+SGrnNo+" , ItemName - "+(String)RowData[i][1]+" ";

//                    insertSMS(SMsg,SMrsUserCode);

                    if(common.toInt(SBlockCode)<=1)
                    {
                      //   updateUserItemStock(stat,SItemCode,SGrnQty,SGrnValue,SMrsUserCode);
                    }
               }
		    stat.close();
	if(iInvoiceTypeCode==2){
			setSupDetails();
  	 
		} 
            
          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

private void setSupDetails(){

		   String sSupCode	= TSupCode.getText();
		   StringBuffer      sb = new StringBuffer();
      
               sb.append("  Select Name,Email from Supplier Where AC_Code='"+sSupCode+"'");
        
               System.out.println("InvNotSupplier Qry:"+sb.toString());
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       theStatement  =  theConnection.createStatement();

                    ResultSet res = theStatement.executeQuery(sb.toString());
                    while(res.next())
                    {
                         String sSupName        = res.getString(1);
                         String sEMail          = res.getString(2);
				 int    iSendStatus	= 0;
                    	 insertInvRemainderMail(sSupCode,sSupName,sEMail,iSendStatus,iMillCode); 
                    }
                    
                    res            . close();
                    theStatement   . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }

}

  private void insertInvRemainderMail(String sSupCode,String sSupName,String sMailID,int iSendStatus,int iMillCode){
	System.out.println("RemainderMail");
 try{
	
        StringBuffer sb = new StringBuffer(); 

          sb.append(" Insert Into InvoiceRemainderMail (SupCode,SendStatus,MillCode,MailID,SupName,CreatedDateTime,EntryDate)");
	    sb.append(" Values (?,?,?,?,?,to_Char(sysdate,'DD-MON-YYYY:hh:mi:ss'),to_Char(sysdate,'YYYYMMDD'))");

	    System.out.println("Qry:"+sb.toString());
         
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();

        PreparedStatement ps=theConnection.prepareStatement(sb.toString());

              ps                    . setString(1,sSupCode);
              ps                    . setInt(2,iSendStatus);
              ps                    . setInt(3,iMillCode);
              ps                    . setString(4,sMailID);
              ps                    . setString(5,sSupName);

              ps . executeUpdate();
            //rst.close();
            ps.close();
            ps=null;
      }  
   
      catch(Exception e)
      {
          e.printStackTrace();
          System.out.println("InsertMethod"+e);
       }


  }

     private int getRowIndex(String SSeleId,Object GrnData[][])
     {
          int iIndex=-1;
          
          for(int i=0;i<GrnData.length;i++)
          {
               String SId = (String)MiddlePanel.VId.elementAt(i);
               
               if(!SSeleId.equals(SId))
                    continue;
               
               iIndex=i;
               return iIndex;
          }
          return iIndex;
     }

     public void insertGateDetails()
     {
          String QString = " Insert Into GateInward (GINo,GIDate,InwNo,Sup_Code,InvNo,InvDate,DcNo,DcDate,ModeCode,Item_Name,SupQty,GateQty,UnmeasuredQty,UserCode,UserTime,BaseCode,MillCode,id,GrnNo,GrnNumber,EntryStatus,Authentication) Values(";
          try
          {
               Statement       stat =  theMConnection.createStatement();

               Object GrnData[][] = MiddlePanel.GrnPanel.getFromVector();
               
               Vector VGIName = new Vector();
               Vector VGICode = new Vector();
               Vector VGIQty  = new Vector();

               String SDateTime = common.getServerDateTime();

               String SEntryStatus = "1";

               for(int i=0;i<GrnData.length;i++)
               {
                    Boolean bValue = (Boolean)GrnData[i][10];
     
                    if(!bValue.booleanValue())
                         continue;

                    String  SItemCode  = (String)GrnData[i][0];
                    String  SItemName  = (String)GrnData[i][1];
                    String  SQty       = (String)GrnData[i][8];

                    int iIndex = common.indexOf(VGICode,SItemCode);

                    if(iIndex>=0)
                    {
                         double dRecQty     = common.toDouble(SQty);
                         double dPrevRecQty = common.toDouble((String)VGIQty.elementAt(iIndex));
                         double dTotRecQty  = dRecQty + dPrevRecQty;
                         VGIQty.setElementAt(String.valueOf(dTotRecQty),iIndex);
                    }
                    else
                    {
                         VGIName.addElement(SItemName);
                         VGICode.addElement(SItemCode);
                         VGIQty.addElement(SQty);
                    }
               }

               String SInvNo       = common.parseNull(TInvNo.getText().trim());
               String SInvDate     = TInvDate.toNormal();
               String SDCNo        = common.parseNull(TDCNo.getText().trim());
               String SDCDate      = TDCDate.toNormal();

               String SSupCode = TSupCode.getText();
               String SDate    = TDate.toNormal();

               String SInwNo = (String)VInwNo.elementAt(JCInwType.getSelectedIndex());

               String SModeCode = InwMode.TCode.getText();

               for(int i=0;i<VGICode.size();i++)
               {
                    String  SItemName  = (String)VGIName.elementAt(i);
                    String  SQty       = (String)VGIQty.elementAt(i);
                    
                    String    QS1 = QString;
                              QS1 = QS1+"0"+SGateNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"0"+SInwNo+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"'"+SInvNo+"',";
                              QS1 = QS1+"'"+SInvDate+"',";
                              QS1 = QS1+"'"+SDCNo+"',";
                              QS1 = QS1+"'"+SDCDate+"',";
                              QS1 = QS1+"0"+SModeCode+",";
                              QS1 = QS1+"'"+SItemName+"',";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+"0"+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+SDateTime+"',";
                              QS1 = QS1+"0"+iAuthCode+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"gateinward_seq.nextval"+",";
                              QS1 = QS1+"0"+"1"+",";
                              QS1 = QS1+"0"+SGrnNo+",";
                              QS1 = QS1+"0"+SEntryStatus+",";
                              QS1 = QS1+"0"+"1"+")";

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                  //  stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

     public void setOrderLink()
     {
          try
          {

               Statement       stat =  theMConnection.createStatement();
               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();                 
               for(int i=0;i<RowData.length;i++)
               {
                    String    QS = "Update PurchaseOrder Set ";
                              QS = QS+" InvQty=InvQty+"+(String)RowData[i][8];
                              QS = QS+" Where Id = "+(String)MiddlePanel.VSeleId.elementAt(i);

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                   // stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E3"+ex);
               bComflag  = false;
          }
     }
     
     public void setMRSLink()
     {
          try
          {
               Statement       stat =  theMConnection.createStatement();

               Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();
               Object GrnData[][] = MiddlePanel.GrnPanel.getFromVector();

               for(int i=0;i<RowData.length;i++)
               {
                    String SMrsNo     = (String)RowData[i][3];
                    
                    if(common.toInt(SMrsNo)==0)
                         continue;
                    
                    String SSeleId = (String)MiddlePanel.VSeleId.elementAt(i);

                    int iMidRow       = getRowIndex(SSeleId,GrnData);

                    String SMrsSLNo   = MiddlePanel.getMrsSLNo(iMidRow);
                    
                    String    QS = "Update MRS Set ";
                              QS = QS+" GrnNo="+SGrnNo;
                              QS = QS+" Where MRSNo ="+SMrsNo+" and Item_Code = '"+(String)RowData[i][0]+"' And SlNo="+SMrsSLNo;
                              QS = QS+" And Mrs.MillCode = "+iMillCode;

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                 //   stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E4"+ex);
               bComflag  = false;
          }
     }

     private void updateSubStoreMasterData()
     {
          try
          {
               Statement stat   =    theDConnection.createStatement();
               
               Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();
               Object GrnData[][] = MiddlePanel.GrnPanel.getFromVector();

               for(int i=0;i<RowData.length;i++)
               {
                    String SItemCode  = (String)RowData[i][0];
                    int iCount=0;
                    
                    ResultSet res = stat.executeQuery("Select count(*) from InvItems Where Item_Code='"+SItemCode+"'");
                    while(res.next())
                    {
                         iCount   =    res.getInt(1);
                    }
                    res.close();
                    
                    if(iCount==0)
                         continue;
                         

                    String SSeleId = (String)MiddlePanel.VSeleId.elementAt(i);

                    int iMidRow       = getRowIndex(SSeleId,GrnData);

                    int iBlockCode = common.toInt(MiddlePanel.getBlockCode(iMidRow));
                    
                    String QS = "";
                    
                    if(iBlockCode>1)
                    {
                         QS = "Update InvItems Set ";
                         QS = QS+" MSRecQty=nvl(MSRecQty,0)+"+(String)RowData[i][8]+",";
                         QS = QS+" MSIssQty=nvl(MSIssQty,0)+"+(String)RowData[i][8]+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    else
                    {
                         QS = "Update InvItems Set ";
                         QS = QS+" MSRecQty=nvl(MSRecQty,0)+"+(String)RowData[i][8]+",";
                         QS = QS+" MSStock=nvl(MSStock,0)+"+(String)RowData[i][8]+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    if(theDConnection  . getAutoCommit())
                         theDConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("E6"+e);
               bComflag  = false;
          }
     }

     private void updateItemMaster(Statement stat,String SItemCode,String SBlockCode,String SGrnQty,String SGrnValue)
     {
          try
          {
               int iBlockCode = common.toInt(SBlockCode);
               
               String QS = "";
               
               if(iBlockCode>1)
               {
                    QS = "Update "+SItemTable+" Set  RecVal =nvl(RecVal,0)+"+SGrnValue+",";
                    QS = QS+" RecQty=nvl(RecQty,0)+"+SGrnQty+",";
                    QS = QS+" IssQty=nvl(IssQty,0)+"+SGrnQty+",";
                    QS = QS+" IssVal=nvl(IssVal,0)+"+SGrnValue+" ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";
               }
               else
               {
                    QS = "Update "+SItemTable+" Set RecVal =nvl(RecVal,0)+"+SGrnValue+",";
                    QS = QS+" RecQty=nvl(RecQty,0)+"+SGrnQty+" ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";
               }

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               stat.execute(QS);
          }
          catch(Exception e)
          {
               System.out.println("E6"+e);
               bComflag  = false;
          }
     }

     private void updateUserItemStock(Statement stat,String SItemCode,String SGrnQty,String SGrnValue,String SMrsUserCode)
     {
          try
          {
               Item IC = new Item(SItemCode,iMillCode,SItemTable,SSupTable);
     
               double dAllStock = common.toDouble(IC.getClStock());
               double dAllValue = common.toDouble(IC.getClValue());
     
               double dRate   = 0;
               try
               {
                    dRate = dAllValue/dAllStock;
               }
               catch(Exception ex)
               {
                    dRate=0;
               }
     
               if(dAllStock==0)
               {
                    dRate = common.toDouble(IC.SRate);
               }

               int iCount=0;

               String QS = " Select count(*) from ItemStock Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+iMillCode;
               
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    iCount   =    res.getInt(1);
               }
               res.close();

               String QS1 = "";

               if(iCount>0)
               {
                    QS1 = "Update ItemStock Set Stock=nvl(Stock,0)+"+SGrnQty+" ";
                    QS1 = QS1+" Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+iMillCode;
               }
               else
               {
                    QS1 = " Insert into ItemStock (MillCode,HodCode,ItemCode,Stock,StockValue) Values (";
                    QS1 = QS1+"0"+iMillCode+",";
                    QS1 = QS1+"0"+SMrsUserCode+",";
                    QS1 = QS1+"'"+SItemCode+"',";
                    QS1 = QS1+"0"+SGrnQty+",";
                    QS1 = QS1+"0"+common.getRound(dRate,4)+")";
               }

               String QS2 = " Update ItemStock set StockValue="+common.getRound(dRate,4)+
                            " Where ItemCode='"+SItemCode+"' and MillCode="+iMillCode;


               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               stat.execute(QS1);
               stat.execute(QS2);
          }
          catch(Exception e)
          {
               System.out.println("E7"+e);
               bComflag  = false;
          }
     }

     public void insertDirectGateDetails()
     {
          String QString = " Insert Into GateInward (GINo,GIDate,InwNo,Sup_Code,InvNo,InvDate,DcNo,DcDate,ModeCode,Item_Name,SupQty,GateQty,UnmeasuredQty,UserCode,UserTime,BaseCode,MillCode,id,EntryStatus,Authentication) Values(";
          try
          {
               Statement       stat =  theMConnection.createStatement();

               Object GateData[][] = MiddlePanel.GatePanel.getFromVector();
               
               String SInvNo       = common.parseNull(TInvNo.getText().trim());
               String SInvDate     = TInvDate.toNormal();
               String SDCNo        = common.parseNull(TDCNo.getText().trim());
               String SDCDate      = TDCDate.toNormal();

               String SSupCode = TSupCode.getText();
               String SDate    = TDate.toNormal();

               String SInwNo = (String)VInwNo.elementAt(JCInwType.getSelectedIndex());

               String SModeCode = InwMode.TCode.getText();

               String SDateTime = common.getServerDateTime();

               String SEntryStatus = "1";

               for(int i=0;i<GateData.length;i++)
               {
                    String  SItemName  = ((String)GateData[i][0]).toUpperCase();
                    String  SQty       = (String)GateData[i][1];
                    
                    String    QS1 = QString;
                              QS1 = QS1+"0"+SGateNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"0"+SInwNo+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"'"+SInvNo+"',";
                              QS1 = QS1+"'"+SInvDate+"',";
                              QS1 = QS1+"'"+SDCNo+"',";
                              QS1 = QS1+"'"+SDCDate+"',";
                              QS1 = QS1+"0"+SModeCode+",";
                              QS1 = QS1+"'"+SItemName+"',";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+"0"+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+SDateTime+"',";
                              QS1 = QS1+"0"+iAuthCode+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"gateinward_seq.nextval"+",";
                              QS1 = QS1+"0"+SEntryStatus+",";
                              QS1 = QS1+"0"+"1"+")";

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

     public void insertSMS(String Msg,String SCode)
     {
          String QS1 = " INSERT INTO TBL_ALERT_COLLECTION (COLLECTIONID,STATUSID,ALERTMESSAGE,CUSTOMERCODE,STATUS,SMSTYPE) VALUES (scm.alertCollection_seq.nextval,'STATUS56','"+Msg+"',"+SCode+",0,0) ";

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();

               stat      . executeUpdate(QS1);
               stat      . close();

          }catch(Exception Ex)
          {
               System.out.println("UpdateGINo method"+Ex);
          }
     } 

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theMConnection . commit();

                    if(iMillCode==1)
                         theDConnection . commit();

                    JOptionPane    . showMessageDialog(null,"The Entered Data is Saved with GRN No - "+SGrnNo+" and GI No - "+SGateNo,"Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("Commit");
               }
               else
               {
                    theMConnection . rollback();

                    if(iMillCode==1)
                         theDConnection . rollback();

                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theMConnection   . setAutoCommit(true);

               if(iMillCode==1)
                    theDConnection   . setAutoCommit(true);


               if(bComflag)
               {
                    preset();
               }
               else
               {
                    BOk.setEnabled(true);
               }

          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void preset()
     {
          MiddlePanel.removeAll();
          getContentPane().remove(MiddlePanel);
          getContentPane().remove(BottomPanel);

          LGrnNo.setText("To be determined");
          BSupplier.setText("Supplier");
          BSupplier.setEnabled(true);

          TInvoice.setText("");

          TInvNo.setText("");
          TDCNo.setText("");

          TInvDate.TDay.setText("");
          TInvDate.TMonth.setText("");
          TInvDate.TYear.setText("");

          TDCDate.TDay.setText("");
          TDCDate.TMonth.setText("");
          TDCDate.TYear.setText("");

          JCSort.setSelectedIndex(0);
          JCSort.setEnabled(true);

          InwMode.TCode.setText("11");
          InwMode.setText("RENGAVILAS");

          BApply.setEnabled(true);
          BOk.setEnabled(true);

          DeskTop.repaint();
          DeskTop.updateUI();
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     private void setInwardData()
     {
          VInwName   = new Vector();
          VInwNo     = new Vector();

          String QS1 = "Select InwName,InwNo From InwType Where InwNo Not in (12) Order By 2";

          try
          {
              ORAConnection   oraConnection =  ORAConnection.getORAConnection();
              Connection      theConnection =  oraConnection.getConnection();
              Statement       stat =  theConnection.createStatement();

              ResultSet result1 = stat.executeQuery(QS1);
              while(result1.next())
              {
                    VInwName.addElement(result1.getString(1));
                    VInwNo.addElement(result1.getString(2));
              }
              result1.close();
              stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
  public void setInvoiceTypeVector()
      {
          VInvTypeCode      = new Vector();
          VInvTypeName      = new Vector();
              
               String QString = "Select TypeName,TypeCode From InvoiceType Order By TypeCode";
               System.out.println("Qry:"+QString);
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       stat =  theConnection.createStatement();
                    
                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         VInvTypeName.addElement(res.getString(1));
                         VInvTypeCode.addElement(res.getString(2));
                    }
                    res            . close();
                    stat           . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
      }


public int getInvTypeCode(String SName)
	{
          int iIndex = VInvTypeName.indexOf(SName);
          return common.toInt((String)VInvTypeCode.elementAt(iIndex));
	}


/* public String getInvTypeCode(int i)                       // 20
     {
//         Vector VCurVector = GrnModel.getCurVector(i);
         String str = VInvTypeName.getSelectedItem();
         int iid = VInvTypeName.indexOf(str);
         return (iid==-1 ?"0":(String)VInvTypeCode.elementAt(iid));
     }*/


private int setPendingInvCheck(){
int		iPendingCount	= -1, iCount=-1;;
String 	sCheckDate		= "", sGRNDate="";
int		iCheckDate=-1, iGRNDate=-1;

iPendingCount			= getPendingInvCount();
if(iPendingCount>0){
	sCheckDate			= getInvCount();
	sGRNDate			= getGRNDate();
	iCheckDate			= common.toInt(sCheckDate);
	iGRNDate			= common.toInt(sGRNDate);

System.out.println("CheckDate:"+sCheckDate+"-"+sGRNDate+"MaxCheckDate--"+iCheckDate+"Mini GRNDate"+iGRNDate);

if(iCheckDate>iGRNDate){
System.out.println("Line 1");

if(!sCheckDate.equals("")){
System.out.println("Line 2");

	JOptionPane.showMessageDialog(null,"Is there any Pending Invoice Received From Party? If Received, Please Close the Status.","Information",JOptionPane.INFORMATION_MESSAGE);
	iCount=0;
   }
  }
  
}
 return iCount;
}

public int getPendingInvCount(){
		int iCount=-1;

               String QString = "Select Count(1) From GRN Where InvoiceType=2";
//               System.out.println("Qry:"+QString);
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       stat =  theConnection.createStatement();
                    
                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         iCount	= res.getInt(1);
                    }
                    res            . close();
                    stat           . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }

	return iCount;
}


public String getInvCount(){
		int iCount=-1;
		String sCheckDate="";

               String QString = "Select ChkDate from ( Select Max(CheckDate) as ChkDate from PendingInvoiceCheck ) Where ChkDate<=to_Char(SysDate-5,'YYYYMMDD')";
               System.out.println("Qry:"+QString);
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       stat =  theConnection.createStatement();
                    
                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         sCheckDate	= res.getString(1);
                    }
                    res            . close();
                    stat           . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }

	return sCheckDate;
}


public String getGRNDate(){
		int iCount=-1;
		String sGRNDate="";

               String QString = "Select Min(GRNDate) from GRN Where InvoiceType=2 ";
               System.out.println("Qry:"+QString);
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       stat =  theConnection.createStatement();
                    
                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         sGRNDate	= res.getString(1);
                    }
                    res            . close();
                    stat           . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }

	return sGRNDate;
}

public int getTrialPendingCount(){
int		iCount	=-1;
 try{
        StringBuffer sb     =new StringBuffer(); 

        sb.append("  Select Sum(Cnt) from (");
        sb.append("  Select Count(*) as Cnt from GateInward");
	  sb.append("  Left Join RawUser on GateInward.MRSAuthUserCode = RawUser.UserCode  ");
	  sb.append("   Where GateInward.InwNo= 1 and GateInward.IssueStatus = 0  And GateInward.GrnNo = 0"); 
	  sb.append("   And (trim(GateInward.Item_Code) is Null)  And GateInward.MillCode = 0 And GateInward.ModiStatus = 0");
	  sb.append("   And GateInward.Authentication = 1  and GateInward.TrialOrderAuthStatus=0  and UserName is null");
  
	  sb.append("    Union All  ");
  
	  sb.append("   Select Count(*) as Cnt from GateInward");
	  sb.append("   Left Join RawUser on GateInward.MRSAuthUserCode = RawUser.UserCode  ");
	  sb.append("   Where GateInward.InwNo = 1 and GateInward.IssueStatus = 0  And GateInward.GrnNo = 0 ");
	  sb.append("   And (trim(GateInward.Item_Name) is Null)  And GateInward.MillCode = 0 And GateInward.ModiStatus = 0 ");
	  sb.append("   And GateInward.Authentication = 1 and GateInward.TrialOrderAuthStatus=0 and UserName is null");
        sb.append(")");
         

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
                 
        PreparedStatement ps          = theConnection.prepareStatement(sb.toString());
	//System.out.println("Get Trial Count:"+sb.toString());

        ResultSet rst                 = ps.executeQuery();
       while (rst.next())
         {
		iCount			  = rst.getInt(1);
         }
       
       
            rst.close();
            ps.close();
            ps=null;
            
    }
    catch(Exception e){
        e.printStackTrace();
        System.out.println(e);
    }
System.out.println("Inside get TrialCount Method :"+iCount);
return iCount;
}


}

