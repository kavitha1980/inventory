package GRN;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class DirectGRNMiddlePanel extends JTabbedPane 
{
         DirectGRNInvMiddlePanel MiddlePanel;
         DirectGRNPanel GrnPanel;
         DirectGatePanel GatePanel;
         Object RowData[][];
         Object RData[][];
         Object GData[][];

         String ColumnData[] = {"Code","Name","Block","MRS No","Order No","Order Qty","Pending Qty","Inv/DC Qty","Recd Qty","Rate","Disc (%)","Cenvat (%)","Tax (%)","Surcharge (%)","Basic","Disc (Rs)","Vat (Rs)","Tax (Rs)","Surcharge (Rs)","Net (Rs)","Department","Group","Unit","VAT"};
         String ColumnType[] = {"S"   ,"S"   ,"S"    ,"N"     ,"N"       ,"N"        ,"N"          ,"N"         ,"N"       ,"N"   ,"N"       ,"N"         ,"N"      ,"N"            ,"N"    ,"N"        ,"N"       ,"N"       ,"N"             ,"N"       ,"B"         ,"B"    ,"B"   ,"S"};

         String CData[] = {"Code","Name","Order No","Order Date","Block","Mrs No","Order Qty","Pending Qty","Recd Qty","BinNo","Click for Select"};
         String CType[] = {"S"   ,"S"   ,"N"       ,"S"         ,"S"    ,"N"     ,"N"        ,"N"          ,"E"       ,"E"     ,"E"               };


         String CGData[] = {"Description","Inv/DC Qty"};
         String CGType[] = {"E"          ,"E"         };

         JLayeredPane DeskTop;
         String SSupCode,SSupName;
		 Vector VBinNo;
		

         Vector VQtyStatus;
         Vector VGCode,VGName,VGBlock,VGOrderNo,VGMRSNo;
         Vector VGPendQty,VGGrnQty,VGRate,VGDiscPer,VGVatPer,VGSTPer,VGSurPer;
         Vector VGDept,VGDeptCode,VGGroup,VGGroupCode,VGUnit,VGUnitCode;
         Vector VGBlockCode,VId,VGOrdQty,VGOrdDate;
         Vector VMrsSlNo,VOrderSlNo,VTaxC,VPQtyAllow,VMrsUserCode,VApproval;

         Vector VSeleId;

         Common common   = new Common();
         int iMillCode;
         int iSortIndex;

         Control control = new Control();

         public DirectGRNMiddlePanel(JLayeredPane DeskTop,int iMillCode,String SSupCode,String SSupName,int iSortIndex)
         {
              this.DeskTop    = DeskTop;
              this.iMillCode  = iMillCode;
              this.SSupCode   = SSupCode;
              this.SSupName   = SSupName;
              this.iSortIndex = iSortIndex;

              VSeleId      = new Vector();
              createComponents();
         }

         public void createComponents()
         {
	
              setVectorData();
              setRowData();
              setRData();
              setGData();
              try
              {
                    GatePanel   = new DirectGatePanel(DeskTop,GData,CGData,CGType);
			  GrnPanel    = new DirectGRNPanel(DeskTop,RData,CData,CType,iMillCode,SSupCode,SSupName,this,VPQtyAllow,VQtyStatus,VBinNo);
			  MiddlePanel = new DirectGRNInvMiddlePanel(DeskTop,RowData,ColumnData,ColumnType,iMillCode,SSupCode);
                    addTab("Gate Inward Without Order",GatePanel);
                    addTab("Gate Inward against Order",GrnPanel);
                    addTab("GRN Against Order",MiddlePanel);
                    GatePanel.setBorder(new TitledBorder(""));
                    GrnPanel.setBorder(new TitledBorder(""));
                    MiddlePanel.setBorder(new TitledBorder("Pending Purchase Orders"));
                    setSelectedIndex(1);
              }
              catch(Exception ex)
              {
                    System.out.println("GRNInvMiddlePanel: "+ex);
              }
         }
		

        public void setVectorData()
        {
               
            VQtyStatus   = new Vector();
            VGCode       = new Vector();
            VGName       = new Vector(); 
            VGBlock      = new Vector();
            VGOrderNo    = new Vector();
            VGMRSNo      = new Vector();
            VGPendQty    = new Vector();
            VGGrnQty     = new Vector();
            VGRate       = new Vector();
            VGDiscPer    = new Vector();
            VGVatPer     = new Vector();
            VGSTPer      = new Vector();
            VGSurPer     = new Vector();
            VGDept       = new Vector();
            VGDeptCode   = new Vector(); 
            VGGroup      = new Vector();
            VGGroupCode  = new Vector(); 
            VGUnit       = new Vector();
            VGUnitCode   = new Vector();
            VId          = new Vector();
            VGBlockCode  = new Vector();
            VGOrdQty     = new Vector();
            VGOrdDate    = new Vector();
            VMrsSlNo     = new Vector();
            VOrderSlNo   = new Vector();
            VTaxC        = new Vector();
            VPQtyAllow   = new Vector();
            VMrsUserCode = new Vector();
            VApproval    = new Vector();
		VBinNo       = new Vector();

            String QS1= "";

            QS1 =   " SELECT PurchaseOrder.Item_Code, InvItems.Item_Name, OrdBlock.BlockName, "+
                    " PurchaseOrder.OrderNo, PurchaseOrder.MrsNo, "+
                    " PurchaseOrder.Qty-PurchaseOrder.InvQty AS Pending, "+
                    " PurchaseOrder.Rate, PurchaseOrder.DiscPer, PurchaseOrder.CenVatPer, "+
                    " PurchaseOrder.TaxPer, PurchaseOrder.SurPer, Dept.Dept_Name, "+
                    " PurchaseOrder.Dept_Code, Cata.Group_Name, PurchaseOrder.Group_Code, "+
                    " Unit.Unit_Name, PurchaseOrder.Unit_Code,PurchaseOrder.Id, "+
                    " PurchaseOrder.OrderBlock,PurchaseOrder.Qty,PurchaseOrder.OrderDate,"+
                    " purchaseOrder.Mrsslno,purchaseOrder.Slno,purchaseOrder.taxclaimable,"+
                    " InvItems.QtyAllowance,PurchaseOrder.MrsAuthUserCode,PurchaseOrder.JMDOrderApproval,InvItems.LocName "+
                    " FROM ((((PurchaseOrder INNER JOIN InvItems ON "+
                    " PurchaseOrder.Item_Code = InvItems.Item_Code) "+
                    " INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock = OrdBlock.Block) "+
                    " INNER JOIN Dept ON PurchaseOrder.Dept_Code = Dept.Dept_code) "+
                    " INNER JOIN Cata ON PurchaseOrder.Group_Code = Cata.Group_Code) "+
                    " INNER JOIN Unit ON PurchaseOrder.Unit_Code = Unit.Unit_Code "+
                    " Where PurchaseOrder.Sup_Code = '"+SSupCode+"' "+
                    " And PurchaseOrder.InvQty < PurchaseOrder.Qty  "+
                    " And PurchaseOrder.Authentication=1 "+
                    " And PurchaseOrder.MillCode="+iMillCode;

            if(iSortIndex==0)
            {
                 QS1 = QS1 + " Order By 4,2 ";
            }
            if(iSortIndex==1)
            {
                 QS1 = QS1 + " Order By 2,4 ";
            }
            if(iSortIndex==2)
            {
                 QS1 = QS1 + " Order By 1,4 ";
            }
System.out.println("Qs -->"+QS1);

            try
            {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       stat =  theConnection.createStatement();

                   ResultSet res1 = stat.executeQuery(QS1);

                   while(res1.next())
                   {
                        VGCode      .addElement(res1.getString(1));
                        VGName      .addElement(res1.getString(2)); 
                        VGBlock     .addElement(res1.getString(3));
                        VGOrderNo   .addElement(res1.getString(4));
                        VGMRSNo     .addElement(res1.getString(5));
                        VGPendQty   .addElement(res1.getString(6));
                        VGGrnQty    .addElement("0");
                        VGRate      .addElement(res1.getString(7));
                        VGDiscPer   .addElement(res1.getString(8));
                        VGVatPer    .addElement(res1.getString(9));
                        VGSTPer     .addElement(res1.getString(10));
                        VGSurPer    .addElement(res1.getString(11));
                        VGDept      .addElement(res1.getString(12));
                        VGDeptCode  .addElement(res1.getString(13)); 
                        VGGroup     .addElement(res1.getString(14));
                        VGGroupCode .addElement(res1.getString(15)); 
                        VGUnit      .addElement(res1.getString(16));
                        VGUnitCode  .addElement(res1.getString(17));
                        VId         .addElement(res1.getString(18));
                        VGBlockCode .addElement(res1.getString(19));
                        VGOrdQty    .addElement(res1.getString(20));
                        VGOrdDate   .addElement(res1.getString(21));
                        VMrsSlNo    .addElement(res1.getString(22));
                        VOrderSlNo  .addElement(res1.getString(23));
                        VTaxC       .addElement(res1.getString(24));
                        VPQtyAllow  .addElement(res1.getString(25));
                        VMrsUserCode.addElement(res1.getString(26));
                        VApproval   .addElement(res1.getString(27));
                        VQtyStatus  .addElement("0");
                        String SBinNo = common.parseNull(res1.getString(28));
			if(!SBinNo.equals("null"))
			      VBinNo	.addElement(SBinNo);
                        
                   }
                   res1.close();
                   stat.close();
            }
            catch(Exception ex)
            {
                System.out.println("E1"+ex );
            }
        }

     public boolean setRowData()
     {
         RowData = new Object[1][ColumnData.length];

         RowData[0][0]  = "";
         RowData[0][1]  = "";
         RowData[0][2]  = "";
         RowData[0][3]  = "";
         RowData[0][4]  = "";
         RowData[0][5]  = "";
         RowData[0][6]  = "";
         RowData[0][7]  = "";
         RowData[0][8]  = "";
         RowData[0][9]  = "";
         RowData[0][10] = "";
         RowData[0][11] = "";
         RowData[0][12] = "";
         RowData[0][13] = "";
         RowData[0][14] = "";
         RowData[0][15] = "";
         RowData[0][16] = "";
         RowData[0][17] = "";
         RowData[0][18] = "";
         RowData[0][19] = "";
         RowData[0][20] = "";
         RowData[0][21] = "";
         RowData[0][22] = "";
         RowData[0][23] = "";

         return true;
     }

     public boolean setRData()
     {
					 
         RData = new Object[VGCode.size()][CData.length];

         for(int i=0;i<VGCode.size();i++)
         {
              RData[i][0]  = (String)VGCode.elementAt(i);
              RData[i][1]  = (String)VGName.elementAt(i);
              RData[i][2]  = (String)VGOrderNo.elementAt(i);
              RData[i][3]  = common.parseDate((String)VGOrdDate.elementAt(i));
              RData[i][4]  = (String)VGBlock.elementAt(i);
              RData[i][5]  = (String)VGMRSNo.elementAt(i);
              RData[i][6]  = (String)VGOrdQty.elementAt(i);
              RData[i][7]  = (String)VGPendQty.elementAt(i);
              RData[i][8]  = "";
		  RData[i][9]  = (String)VBinNo.elementAt(i);
              RData[i][10] = new Boolean(false);
         }
         return true;
     }

     public boolean setGData()
     {
         GData = new Object[1][CGData.length];

         GData[0][0] = "";
         GData[0][1] = "";

         return true;
     }

     public String getBlockCode(int i)
     {
            return (String)VGBlockCode.elementAt(i);
     }
	
     public String getOrdQty(int i)
     {
            return (String)VGOrdQty.elementAt(i);
     }
     public String getMrsSLNo(int i)
     {
            return (String)VMrsSlNo.elementAt(i);
     }
     public String getMrsUserCode(int i)
     {
            String SMrsUserCode = common.parseNull((String)VMrsUserCode.elementAt(i));

            if(SMrsUserCode.equals(""))
                 SMrsUserCode="1";

            return SMrsUserCode;
     }
     public String getOrderSLNo(int i)
     {
            return (String)VOrderSlNo.elementAt(i);
     }
     public String getTaxC(int i)
     {
            return (String)VTaxC.elementAt(i);
     }
     public String getOrderApproval(int i)
     {
            return (String)VApproval.elementAt(i);
     }


}
