package GRN;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGRNModel extends DefaultTableModel
{
        Object    RData[][],CNames[],CType[];
        Vector    VQtyStatus;
        Common common = new Common();
        public DirectGRNModel(Object[][] RData, Object[] CNames,Object[] CType,Vector VQtyStatus)
        {
            super(RData,CNames);
            this.RData       = RData;
            this.CNames      = CNames;
            this.CType       = CType;
            this.VQtyStatus  = VQtyStatus;

            for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
            }
        }
       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }            
       public boolean isCellEditable(int row,int col)
       {
            if(col==8)
            {
                 String SQtyStatus = (String)VQtyStatus.elementAt(row);

                 if(SQtyStatus.equals("0"))
                      return true;
                 return false;
            }
            else
            {
                 if(CType[col]=="B" || CType[col]=="E")
                    return true;
                 return false;
            }
       }
       public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }
    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][CNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
             {
                if(j==10)
                {
                  FinalData[i][j] = (Boolean)curVector.elementAt(j);
                }
                else
                {
                  FinalData[i][j] = ((String)curVector.elementAt(j)).trim();
                }
             }
        }
        return FinalData;
    }
    public int getRows()
    {
         return super.dataVector.size();
    }
    public Vector getCurVector(int i)
    {
         return (Vector)super.dataVector.elementAt(i);
    }

}
