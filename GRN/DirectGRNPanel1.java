// Used By Order,GRN  and other operations
// The Operation Varies with the state of bSig.
// Initially it is set to true once called from Non-Order activities,
// the same shall be set to false.

package GRN;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;



import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGRNPanel1 extends JPanel
{
   JTable               GrnTable;
   DirectGRNModel1       GrnModel;

   JPanel         GridPanel,BottomPanel,ControlPanel;
   JPanel         GridBottom;

   JLayeredPane   DeskTop;
   Object         RData[][];
   String         CData[],CType[];
   int            iMillCode;
   String         SSupCode,SSupName;
   Vector         VSeleGINo,VPQtyAllow,VQtyStatus;
   DirectGRNMiddlePanel1  MiddlePanel;
   JComboBox          cmbInvoiceType ;
   JTextField     TFind,TFind1;
   Vector VInvTypeCode, VInvTypeName;

   Common common = new Common();

   // Constructor method referred in GRN Collection Frame Operations
    DirectGRNPanel1(JLayeredPane DeskTop,Object RData[][],String CData[],String CType[],int iMillCode,String SSupCode,String SSupName,DirectGRNMiddlePanel1 MiddlePanel,Vector VPQtyAllow,Vector VQtyStatus)
    {
         this.DeskTop      = DeskTop;
         this.RData        = RData;
         this.CData        = CData;
         this.CType        = CType;
         this.iMillCode    = iMillCode;
         this.SSupCode     = SSupCode;
         this.SSupName     = SSupName;
         this.MiddlePanel  = MiddlePanel;
         this.VPQtyAllow   = VPQtyAllow;
         this.VQtyStatus   = VQtyStatus;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
         setReportTable();
    }
    public void createComponents()
    {
         GridPanel        = new JPanel(true);
         GridBottom       = new JPanel(true);
         BottomPanel      = new JPanel();
         ControlPanel     = new JPanel();

         TFind            = new JTextField();
         TFind1           = new JTextField();
	    cmbInvoiceType   = new JComboBox();
     }
          
     public void setLayouts()
     {
         //GridPanel.setLayout(new GridLayout(1,1));
         GridPanel.setLayout(new BorderLayout());
         BottomPanel.setLayout(new BorderLayout());
         ControlPanel.setLayout(new GridLayout(1,5));
         GridBottom.setLayout(new BorderLayout());         
     }
     public void addComponents()
     {
         ControlPanel.add(new JLabel("Code Find"));
         ControlPanel.add(TFind);
         ControlPanel.add(new JLabel(""));
         ControlPanel.add(new JLabel("Name Find"));
         ControlPanel.add(TFind1);
	    setInvoiceTypeVector();
	    cmbInvoiceType         = new JComboBox(VInvTypeName);
     }
     public void addListeners()
     {
          TFind   . addKeyListener(new CodeKeyList());
          TFind1  . addKeyListener(new NameKeyList());
     }
     public class ActList implements ActionListener
     {
            public void actionPerformed(ActionEvent ae)
            {
            }
     }
     public void setReportTable()
     {
         GrnModel      = new DirectGRNModel1(RData,CData,CType,VQtyStatus);       
         GrnTable      = new JTable(GrnModel);
         //GrnTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<GrnTable.getColumnCount();col++)
         {
               if(CType[col]=="N" || CType[col]=="B")
                    GrnTable.getColumn(CData[col]).setCellRenderer(cellRenderer);
         }
         //GrnTable.setShowGrid(false);

         TableColumn InvTypeColumn  = GrnTable.getColumn("InvoiceType");

         InvTypeColumn.setCellEditor(new DefaultCellEditor(cmbInvoiceType));

         setLayout(new BorderLayout());
         GridBottom.add(GrnTable.getTableHeader(),BorderLayout.NORTH);
         GridBottom.add(new JScrollPane(GrnTable),BorderLayout.CENTER);

         GridPanel.add(ControlPanel,BorderLayout.NORTH);
         GridPanel.add(GridBottom,BorderLayout.CENTER);

         add(BottomPanel,BorderLayout.SOUTH);
         add(GridPanel,BorderLayout.CENTER);

         GrnTable.addKeyListener(new KeyList1());
         GrnTable.addMouseListener(new MouseList1());
     }

     public class CodeKeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String    SFind     = TFind.getText();
               int       i         = codeIndexOf(SFind);
               if (i>-1)
               {
                              GrnTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = GrnTable.getCellRect(i,0,true);
                              GrnTable.scrollRectToVisible(cellRect);
               }
          }
     }
     public int codeIndexOf(String SFind)
     {
          SFind = SFind.toUpperCase();

          for(int i=0;i<GrnModel.getRows();i++)
          {
               String str  = (String)GrnModel.getValueAt(i,0);
               if(str.startsWith(SFind))
                    return i;
          }
          return -1;
     }

     public class NameKeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String    SFind     = TFind1.getText();
               int       i         = nameIndexOf(SFind);
               if (i>-1)
               {
                              GrnTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = GrnTable.getCellRect(i,0,true);
                              GrnTable.scrollRectToVisible(cellRect);
               }
          }
     }
     public int nameIndexOf(String SFind)
     {
          SFind = SFind.toUpperCase();

          for(int i=0;i<GrnModel.getRows();i++)
          {
               String str  = (String)GrnModel.getValueAt(i,1);

               int iIndex = str.indexOf(SFind);

               if(iIndex>=0)
                    return i;
          }
          return -1;
     }

     private class KeyList1 extends KeyAdapter
     {
         public void keyPressed(KeyEvent ke)
         {
            if(ke.getKeyCode()==KeyEvent.VK_SPACE)
            {
                if(validSeleData())
                {
                     setKeyGRNData();
                }
            }
         }
     }

     public class MouseList1 extends MouseAdapter
     {
          public void mouseReleased(MouseEvent me)
          {
               if(validSeleData())
               {
                    setMouseGRNData();
               }
          }
     }

     private boolean validSeleData()
     {
         int iCol     = GrnTable.getSelectedColumn();

         if(iCol!=10)
             return false;
         
         int iRow     = GrnTable.getSelectedRow();

         String SQty = (String)GrnModel.getValueAt(iRow,8);

         if(common.toDouble(SQty)<=0)
         {
             GrnModel.setValueAt(new Boolean(false),iRow,10);
             JOptionPane.showMessageDialog(null,"Invalid Recd Qty","Error",JOptionPane.ERROR_MESSAGE);
             return false;
         }

         int iQtyAllowance = common.toInt((String)VPQtyAllow.elementAt(iRow));

         if(iQtyAllowance==0)
         {
              double dPendQty   = common.toDouble((String)GrnModel.getValueAt(iRow,7));
              double dCurRecQty = common.toDouble(SQty);

              if(dCurRecQty>dPendQty)
              {
                  GrnModel.setValueAt(new Boolean(false),iRow,10);
                  JOptionPane.showMessageDialog(null,"Quantity Mismatch","Error",JOptionPane.ERROR_MESSAGE);
                  return false;
              }
         }
         else
         {
              double dExcessQty=0;

              double dOrderQty  = common.toDouble((String)GrnModel.getValueAt(iRow,6));
              double dPendQty   = common.toDouble((String)GrnModel.getValueAt(iRow,7));
              double dCurRecQty = common.toDouble(SQty);

              if(dCurRecQty>dPendQty)
              {
                  dExcessQty = dCurRecQty - dPendQty;
              }

              if(dExcessQty>0)
              {
                   double dAllowedQty = (dOrderQty * 200) / 100;
                   if(dExcessQty>dAllowedQty)
                   {
                       GrnModel.setValueAt(new Boolean(false),iRow,10);
                       JOptionPane.showMessageDialog(null,"Quantity Allowance Exceeded","Error",JOptionPane.ERROR_MESSAGE);
                       return false;
                   }
              }
         }

         return true;
     }

     private void setKeyGRNData()
     {
          try
          {
               int iRow     = GrnTable.getSelectedRow();

               Boolean bValue = (Boolean)GrnModel.getValueAt(iRow,10);

               if(!bValue.booleanValue())
               {
                    addGrnRow(iRow);
               }
               else
               {
                    deleteGrnRow(iRow);
               }
          }
          catch(Exception ex){}
     }

     private void setMouseGRNData()
     {
          try
          {
               int iRow     = GrnTable.getSelectedRow();

               Boolean bValue = (Boolean)GrnModel.getValueAt(iRow,10);

               if(bValue.booleanValue())
               {
                    addGrnRow(iRow);
               }
               else
               {
                    deleteGrnRow(iRow);
               }
          }
          catch(Exception ex){}
     }


     private void addGrnRow(int iRow)
     {
          try
          {
               String SCode      = (String)GrnModel.getValueAt(iRow,0);
               String SName      = (String)GrnModel.getValueAt(iRow,1);
               String SPendQty   = (String)GrnModel.getValueAt(iRow,7);
               String SQty       = (String)GrnModel.getValueAt(iRow,8);

               double dRecQty     = common.toDouble(SQty);
               double dPendQty    = common.toDouble(SPendQty);
               double dPrevRecQty = common.toDouble((String)MiddlePanel.VGGrnQty.elementAt(iRow));
               double dTotRecQty  = dRecQty + dPrevRecQty;
               MiddlePanel.VGGrnQty.setElementAt(String.valueOf(dTotRecQty),iRow);
 
               Vector VEmpty = new Vector();
 
               VEmpty.addElement((String)MiddlePanel.VGCode.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGName.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGBlock.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGMRSNo.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGOrderNo.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGOrdQty.elementAt(iRow));
               VEmpty.addElement(""+dPendQty);
               VEmpty.addElement(SQty);
               VEmpty.addElement(""+dRecQty);
               VEmpty.addElement((String)MiddlePanel.VGRate.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGDiscPer.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGVatPer.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGSTPer.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGSurPer.elementAt(iRow));
               VEmpty.addElement("");
               VEmpty.addElement("");
               VEmpty.addElement("");
               VEmpty.addElement("");
               VEmpty.addElement("");
               VEmpty.addElement("");
               VEmpty.addElement((String)MiddlePanel.VGDept.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGGroup.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGUnit.elementAt(iRow));
               if(((String)MiddlePanel.VTaxC.elementAt(iRow)).equals("0"))
               {
                   VEmpty.addElement("Not Claimable");
               }
               else
               {
                   VEmpty.addElement("Claimable");
               }
               MiddlePanel.MiddlePanel.dataModel.addRow(VEmpty);
               MiddlePanel.VSeleId.addElement((String)MiddlePanel.VId.elementAt(iRow));
 
               GrnModel.VQtyStatus.setElementAt("1",iRow);

               int iRows = MiddlePanel.MiddlePanel.dataModel.getRows();
               for(int i=0;i<iRows;i++)
               {
                  Vector rowVector = (Vector)MiddlePanel.MiddlePanel.dataModel.getCurVector(i);
                  MiddlePanel.MiddlePanel.dataModel.setMaterialAmount(rowVector);
               }
          }
          catch(Exception ex){}
     }

     private void deleteGrnRow(int iRow)
     {
          try
          {
               String SCode      = (String)GrnModel.getValueAt(iRow,0);
               String SId        = (String)MiddlePanel.VId.elementAt(iRow);

               Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();

               int iGrnRow        = getRowIndex(SId,RowData);

               if(iGrnRow>=0)
               {
                    String  SRecQty = (String)RowData[iGrnRow][8];

                    double dRecQty     = common.toDouble(SRecQty);
                    double dPrevRecQty = common.toDouble((String)MiddlePanel.VGGrnQty.elementAt(iRow));
                    double dTotRecQty  = dPrevRecQty - dRecQty;
                    MiddlePanel.VGGrnQty.setElementAt(String.valueOf(dTotRecQty),iRow);
      
                    MiddlePanel.VSeleId.removeElementAt(iGrnRow);

                    MiddlePanel.MiddlePanel.dataModel.removeRow(iGrnRow);
               }

               GrnModel.VQtyStatus.setElementAt("0",iRow);

               int iRows = MiddlePanel.MiddlePanel.dataModel.getRows();

               if(iRows>0)
               {
                    for(int i=0;i<iRows;i++)
                    {
                       Vector rowVector = (Vector)MiddlePanel.MiddlePanel.dataModel.getCurVector(i);
                       MiddlePanel.MiddlePanel.dataModel.setMaterialAmount(rowVector);
                    }
               }
               else
               {
                    MiddlePanel.MiddlePanel.LBasic.setText("0");
                    MiddlePanel.MiddlePanel.LDiscount.setText("0");
                    MiddlePanel.MiddlePanel.LCenVat.setText("0");
                    MiddlePanel.MiddlePanel.LTax.setText("0");
                    MiddlePanel.MiddlePanel.LSur.setText("0");
                    MiddlePanel.MiddlePanel.TAdd.setText("");
                    MiddlePanel.MiddlePanel.TLess.setText("");
                    MiddlePanel.MiddlePanel.TModVat.setText("");
                    MiddlePanel.MiddlePanel.LNet.setText("0");
               }
          }
          catch(Exception ex){}
     }

     private int getRowIndex(String SId,Object RowData[][])
     {
          int iIndex=-1;
          
          for(int i=0;i<RowData.length;i++)
          {
               String SSeleId = (String)MiddlePanel.VSeleId.elementAt(i);
               
               if(!SSeleId.equals(SId))
                    continue;
               
               iIndex=i;
               return iIndex;
          }
          return iIndex;
     }


     public Object[][] getFromVector()
     {
          return GrnModel.getFromVector();     
     }

  public void setInvoiceTypeVector()
      {
          VInvTypeCode      = new Vector();
          VInvTypeName      = new Vector();
              
               String QString = "Select TypeName,TypeCode From InvoiceType Order By TypeCode";
//               System.out.println("Qry:"+QString);
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       stat =  theConnection.createStatement();
                    
                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         VInvTypeName.addElement(res.getString(1));
                         VInvTypeCode.addElement(res.getString(2));
                    }
                    res            . close();
                    stat           . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
      }

 public String getInvTypeCode(int i)                       // 20
     {
         Vector VCurVector = GrnModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(9);
         int iid = VInvTypeName.indexOf(str);
System.out.println("InvType:"+str+ "Code"+iid);
         return (iid==-1 ?"0":(String)VInvTypeCode.elementAt(iid));
     }



}

