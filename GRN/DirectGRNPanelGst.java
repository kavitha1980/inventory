// Used By Order,GRN  and other operations
// The Operation Varies with the state of bSig.
// Initially it is set to true once called from Non-Order activities,
// the same shall be set to false.

package GRN;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;



import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGRNPanelGst extends JPanel
{
   JTable               GrnTable;
   DirectGRNModelGst    GrnModel;

   JPanel         GridPanel,BottomPanel,ControlPanel;
   JPanel         GridBottom;
   JComboBox      JCBinNo;
   JLayeredPane   DeskTop;
   Object         RData[][];
   String         CData[],CType[];
   int            iMillCode;
   String         SSupCode,SSupName;
   Vector         VSeleGINo,VPQtyAllow,VQtyStatus,VBinNo;
   DirectGRNMiddlePanelGst  MiddlePanel;
   JButton        BRefresh;
   JTextField     TFind,TFind1;
   Vector VSeleCode,VRefItemCode,VRefHsnCode,VRefHsnType;
   
   Common common = new Common();
   String shsncode="",shsntype="";

   Connection       theConnection = null;

   // Constructor method referred in GRN Collection Frame Operations
    DirectGRNPanelGst(JLayeredPane DeskTop,Object RData[][],String CData[],String CType[],int iMillCode,String SSupCode,String SSupName,DirectGRNMiddlePanelGst MiddlePanel,Vector VPQtyAllow,Vector VQtyStatus,Vector VBinNo)
    {
	
         this.DeskTop      = DeskTop;
         this.RData        = RData;
         this.CData        = CData;
         this.CType        = CType;
         this.iMillCode    = iMillCode;
         this.SSupCode     = SSupCode;
         this.SSupName     = SSupName;
         this.MiddlePanel  = MiddlePanel;
         this.VPQtyAllow   = VPQtyAllow;
         this.VQtyStatus   = VQtyStatus;
         this.VBinNo       = VBinNo;
		
         createComponents();
	 setLayouts();
	 addComponents();
	 addListeners();
	 setReportTable();
    }
    public void createComponents()
    {
         GridPanel        = new JPanel(true);
         GridBottom       = new JPanel(true);
         BottomPanel      = new JPanel();
         ControlPanel     = new JPanel();
         BRefresh         = new JButton("Refresh");  
		 
         TFind            = new JTextField();
         TFind1           = new JTextField();
     }
          
     public void setLayouts()
     {
         //GridPanel.setLayout(new GridLayout(1,1));
         GridPanel.setLayout(new BorderLayout());
         BottomPanel.setLayout(new FlowLayout());
         ControlPanel.setLayout(new GridLayout(1,5));
         GridBottom.setLayout(new BorderLayout());         
     }
     public void addComponents()
     {
         ControlPanel.add(new JLabel("Code Find"));
         ControlPanel.add(TFind);
         ControlPanel.add(new JLabel(""));
         ControlPanel.add(new JLabel("Name Find"));
         ControlPanel.add(TFind1);
         BottomPanel.add(new JLabel("F2 - To Select Existing HSN Code"));
         //BottomPanel.add(BRefresh);
         BottomPanel.add(new JLabel("    F4 - To Entry New HSN Code"));
   	 JCBinNo          = new JComboBox(VBinNo);
	 JCBinNo          .setEditable(true);
     }

     public void addListeners()
     {
          TFind   . addKeyListener(new CodeKeyList());
          TFind1  . addKeyListener(new NameKeyList());
          //BRefresh   .addActionListener(new ActList());
     }
   
    //hsncode

     public class ServList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_F2) 
	       {
		    int i = GrnTable.getSelectedRow();
		    int iCol = GrnTable.getSelectedColumn();

               	    String SGrnStatus = (String)MiddlePanel.VGGrnStatus.elementAt(i);

		    if(iCol==11)
		    {
		 	 String SHCode     = (String)GrnModel.getValueAt(i,11);
	       	         String SItemCode  = (String)GrnModel.getValueAt(i,0); 
		         String SMasterHsn = getMasterHsn(SItemCode);
		         String SMultiHsn  = getMultiHsn(SItemCode);

			 if((SHCode.equals("") || SHCode.equals("0") || SMasterHsn.equals("0") || SMasterHsn.equals("")))
			 {
			      if(SMultiHsn.equals("1"))
			      {	
		                   setHsnList(SItemCode,1);
			      }
			      else
			      {	
		                   setHsnList(SItemCode,0);
			      }
			 }
			 else 
			 {
			      Boolean bValue = (Boolean)GrnModel.getValueAt(i,10);

			      if(bValue.booleanValue())
			      {
			           JOptionPane.showMessageDialog(null,"This row couldn't be changed. Already Selected for GRN","Information",JOptionPane.INFORMATION_MESSAGE);
			      }
			      else
			      {
				   if(SGrnStatus.equals("1"))
				   {
			                JOptionPane.showMessageDialog(null,"This row couldn't be changed. Partial Grn Already Made for  ItemCode-"+SItemCode,"Information",JOptionPane.INFORMATION_MESSAGE);
				   }
				   else
				   {
				        setHsnList(SItemCode,1);
				   }
			      }	
		         }
		    }
	       }

               if(ke.getKeyCode()==KeyEvent.VK_F4) 
	       {
		    int i = GrnTable.getSelectedRow();
		    int iCol = GrnTable.getSelectedColumn();

               	    String SGrnStatus = (String)MiddlePanel.VGGrnStatus.elementAt(i);

		    if(iCol==11)
		    {
		 	 String SHCode     = (String)GrnModel.getValueAt(i,11);
	       	         String SItemCode  = (String)GrnModel.getValueAt(i,0); 
		         String SMasterHsn = getMasterHsn(SItemCode);
		         String SMultiHsn  = getMultiHsn(SItemCode);

			 if((SHCode.equals("") || SHCode.equals("0") || SMasterHsn.equals("0") || SMasterHsn.equals("")))
			 {
			      new MaterialHsnCodeUpdate(DeskTop,GrnModel,GrnTable,MiddlePanel,SHCode,SItemCode);
			 }
			 else
			 {
			      Boolean bValue = (Boolean)GrnModel.getValueAt(i,10);

			      if(bValue.booleanValue())
			      {
			           JOptionPane.showMessageDialog(null,"This row couldn't be changed. Already Selected for GRN","Information",JOptionPane.INFORMATION_MESSAGE);
			      }
			      else
			      {
				   if(SGrnStatus.equals("1"))
				   {
			                JOptionPane.showMessageDialog(null,"This row couldn't be changed. Partial Grn Already Made for  ItemCode-"+SItemCode,"Information",JOptionPane.INFORMATION_MESSAGE);
				   }
				   else
				   {
				        new MaterialHsnCodeUpdate(DeskTop,GrnModel,GrnTable,MiddlePanel,SHCode,SItemCode);
				   }
			      }	
		         }
		    }
	       }
          }
     }

     public String getMasterHsn(String SItemCode)
     {
	String SMHsn = "";

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "Select HsnCode from InvItems where Item_Code='"+SItemCode+"'";

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    SMHsn = common.parseNull(result.getString(1));
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getMasterHsn :"+ex);
        }
	return SMHsn;
     }

     public String getMultiHsn(String SItemCode)
     {
	String SMulti = "";

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "Select nvl(MultiHsn,0) from InvItems where Item_Code='"+SItemCode+"'";

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    SMulti = common.parseNull(result.getString(1));
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getMultiHsn :"+ex);
        }
	return SMulti;
     }

  //Set HsnList
     private void setHsnList(String SItemCode,int iDispStatus)
     {
           HsnCodePicker HP = new HsnCodePicker(DeskTop,GrnModel,GrnTable,MiddlePanel,SItemCode,iDispStatus);
           try
           {
                DeskTop   . add(HP);
                DeskTop   . repaint();
                HP        . setSelected(true);
                DeskTop   . updateUI();
                HP        . show();
                HP        . BrowList.requestFocus();
           }
           catch(java.beans.PropertyVetoException ex){}
     }

     /*public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BRefresh)
               {
		    VRefItemCode = new Vector();
		    VRefHsnCode  = new Vector();
		    VRefHsnType  = new Vector();

		    setSelectVector();		  

                    for(int i=0;i<GrnModel.getRows();i++)
                    {
                         Boolean bValue = (Boolean)GrnModel.getValueAt(i,10);
 
			 if(bValue.booleanValue())
                   	      continue;

		         String SItemCode = (String)GrnModel.getValueAt(i,0); 

			 int iSeleIndex = common.indexOf(VSeleCode,SItemCode);

			 if(iSeleIndex>=0)
			      continue;

			 shsncode = "";
			 shsntype = "";

			 int iRefIndex = common.indexOf(VRefItemCode,SItemCode);

			 if(iRefIndex>=0)
			 {
			      shsncode = (String)VRefHsnCode.elementAt(iRefIndex);
			      shsntype = (String)VRefHsnType.elementAt(iRefIndex);
	
		              GrnModel.setValueAt(shsncode,i,11);
			      MiddlePanel.VGHsnCode.setElementAt(shsncode,i);
			      MiddlePanel.VGHsnType.setElementAt(shsntype,i);
			 }
			 else
			 {
                    	      getHsnCode(SItemCode);
					
		              GrnModel.setValueAt(shsncode,i,11);
			      MiddlePanel.VGHsnCode.setElementAt(shsncode,i);
			      MiddlePanel.VGHsnType.setElementAt(shsntype,i);
			 }
	            }
               }
          }
     }

     public void setSelectVector()
     {
	    VSeleCode    = new Vector();

            for(int i=0;i<GrnModel.getRows();i++)
            {
                 Boolean bValue = (Boolean)GrnModel.getValueAt(i,10);

		 if(!bValue.booleanValue())
           	      continue;

	         String SItemCode = (String)GrnModel.getValueAt(i,0); 

		 int iSeleIndex = common.indexOf(VSeleCode,SItemCode);

		 if(iSeleIndex<0)
		      VSeleCode.addElement(SItemCode);
            }
     }*/

     public void setReportTable()
     {
         GrnModel      = new DirectGRNModelGst(RData,CData,CType,VQtyStatus);       
         GrnTable      = new JTable(GrnModel);
         //GrnTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<GrnTable.getColumnCount();col++)
         {
               if(CType[col]=="N" || CType[col]=="B")
                    GrnTable.getColumn(CData[col]).setCellRenderer(cellRenderer);
         }
         //GrnTable.setShowGrid(false);
		 
		 
	 TableColumn BinNoColumn  = GrnTable.getColumn("BinNo");
	 BinNoColumn.setCellEditor(new DefaultCellEditor(JCBinNo));
		 		
         setLayout(new BorderLayout());
         GridBottom.add(GrnTable.getTableHeader(),BorderLayout.NORTH);
         GridBottom.add(new JScrollPane(GrnTable),BorderLayout.CENTER);

         GridPanel.add(ControlPanel,BorderLayout.NORTH);
         GridPanel.add(GridBottom,BorderLayout.CENTER);
         GridPanel.add(BottomPanel,BorderLayout.SOUTH); 

         add(BottomPanel,BorderLayout.SOUTH);
         add(GridPanel,BorderLayout.CENTER);

         GrnTable.addKeyListener(new KeyList1());
         GrnTable.addMouseListener(new MouseList1());
         GrnTable.addKeyListener(new ServList());
     }

     public class CodeKeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String    SFind     = TFind.getText();
               int       i         = codeIndexOf(SFind);
               if (i>-1)
               {
                    GrnTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = GrnTable.getCellRect(i,0,true);
                    GrnTable.scrollRectToVisible(cellRect);
               }
          }
     }

     public int codeIndexOf(String SFind)
     {
          SFind = SFind.toUpperCase();

          for(int i=0;i<GrnModel.getRows();i++)
          {
               String str  = (String)GrnModel.getValueAt(i,0);
               if(str.startsWith(SFind))
                    return i;
          }
          return -1;
     }

     public class NameKeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String    SFind     = TFind1.getText();
               int       i         = nameIndexOf(SFind);
               if (i>-1)
               {
                              GrnTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = GrnTable.getCellRect(i,0,true);
                              GrnTable.scrollRectToVisible(cellRect);
               }
          }
     }
     public int nameIndexOf(String SFind)
     {
          SFind = SFind.toUpperCase();

          for(int i=0;i<GrnModel.getRows();i++)
          {
               String str  = (String)GrnModel.getValueAt(i,1);

               int iIndex = str.indexOf(SFind);

               if(iIndex>=0)
                    return i;
          }
          return -1;
     }

     private class KeyList1 extends KeyAdapter
     {
         public void keyPressed(KeyEvent ke)
         {
            if(ke.getKeyCode()==KeyEvent.VK_SPACE)
            {
                if(validSeleData())
                {
                     setKeyGRNData();
                }
            }
			
         }
     }
	
     public class MouseList1 extends MouseAdapter
     {
          public void mouseReleased(MouseEvent me)
          {
               if(validSeleData())
               {
                    setMouseGRNData();
               }
          }
     }

     private boolean validSeleData()
     {
         int iCol     = GrnTable.getSelectedColumn();

         if(iCol!=10)
             return false;
         
         int iRow     = GrnTable.getSelectedRow();

         String SHsnCode = (String)GrnModel.getValueAt(iRow,11);
         double dGstRate = common.toDouble((String)GrnModel.getValueAt(iRow,12));
         String SQty     = (String)GrnModel.getValueAt(iRow,8);
         String SBinNo   = (String)GrnModel.getValueAt(iRow,9);
		 
         if(SHsnCode.equals("") || SHsnCode.equals("0"))
         {
             GrnModel.setValueAt(new Boolean(false),iRow,10);
             JOptionPane.showMessageDialog(null,"HsnCode Not Updated for Material @Row-"+String.valueOf(iRow+1),"Error",JOptionPane.ERROR_MESSAGE);
             return false;
         }

         if(MiddlePanel.SSeleSupType.equals("1"))
	 {
	      int iHsnRateCheck = checkHsnGstRate(SHsnCode,dGstRate);

	      if(iHsnRateCheck<=0)
	      {
	           GrnModel.setValueAt(new Boolean(false),iRow,10);
		   JOptionPane.showMessageDialog(null,"This Combination of HsnCode and GstRate Not Exists for Material @Row-"+String.valueOf(iRow+1),"Error",JOptionPane.ERROR_MESSAGE);
		   return false;
	      }
	 }

         if(common.toDouble(SQty)<=0)
         {
             GrnModel.setValueAt(new Boolean(false),iRow,10);
             JOptionPane.showMessageDialog(null,"Invalid Recd Qty","Error",JOptionPane.ERROR_MESSAGE);
             return false;
         }

         int iQtyAllowance = common.toInt((String)VPQtyAllow.elementAt(iRow));

         if(iQtyAllowance==0)
         {
              double dPendQty   = common.toDouble((String)GrnModel.getValueAt(iRow,7));
              double dCurRecQty = common.toDouble(SQty);

              if(dCurRecQty>dPendQty)
              {
                  GrnModel.setValueAt(new Boolean(false),iRow,10);
                  JOptionPane.showMessageDialog(null,"Quantity Mismatch","Error",JOptionPane.ERROR_MESSAGE);
                  return false;
              }
         }
         else
         {
              double dExcessQty=0;

              double dOrderQty  = common.toDouble((String)GrnModel.getValueAt(iRow,6));
              double dPendQty   = common.toDouble((String)GrnModel.getValueAt(iRow,7));
              double dCurRecQty = common.toDouble(SQty);

              if(dCurRecQty>dPendQty)
              {
                  dExcessQty = dCurRecQty - dPendQty;
              }

              if(dExcessQty>0)
              {
                   double dAllowedQty = (dOrderQty * 200) / 100;
                   if(dExcessQty>dAllowedQty)
                   {
                       GrnModel.setValueAt(new Boolean(false),iRow,10);
                       JOptionPane.showMessageDialog(null,"Quantity Allowance Exceeded","Error",JOptionPane.ERROR_MESSAGE);
                       return false;
                   }
              }
         }

	  if(SBinNo.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Bin No Must Be Entered","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
		 
         return true;
     }

     public int checkHsnGstRate(String SHsnCode,double dGstRate)
     {
	int iHCount=0;

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "Select Count(*) from HsnGstRate Where HsnCode='"+SHsnCode+"' and GstRate="+dGstRate;

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    iHCount = result.getInt(1);
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("checkHsnGstRate :"+ex);
        }
	return iHCount;
     }

     private void setKeyGRNData()
     {
          try
          {
               int iRow     = GrnTable.getSelectedRow();

               Boolean bValue = (Boolean)GrnModel.getValueAt(iRow,10);

               if(!bValue.booleanValue())
               {
                    addGrnRow(iRow);
               }
               else
               {
                    deleteGrnRow(iRow);
               }
          }
          catch(Exception ex){}
     }

     private void setMouseGRNData()
     {
          try
          {
               int iRow     = GrnTable.getSelectedRow();

               Boolean bValue = (Boolean)GrnModel.getValueAt(iRow,10);

               if(bValue.booleanValue())
               {
                    addGrnRow(iRow);
               }
               else
               {
                    deleteGrnRow(iRow);
               }
          }
          catch(Exception ex){}
     }

     private void addGrnRow(int iRow)
     {
          try
          {
               String SCode      = (String)GrnModel.getValueAt(iRow,0);
               String SName      = (String)GrnModel.getValueAt(iRow,1);
               String SPendQty   = (String)GrnModel.getValueAt(iRow,7);
               String SQty       = (String)GrnModel.getValueAt(iRow,8);

               double dRecQty     = common.toDouble(SQty);
               double dPendQty    = common.toDouble(SPendQty);
               double dPrevRecQty = common.toDouble((String)MiddlePanel.VGGrnQty.elementAt(iRow));
               double dTotRecQty  = dRecQty + dPrevRecQty;
               MiddlePanel.VGGrnQty.setElementAt(String.valueOf(dTotRecQty),iRow);
 
               Vector VEmpty = new Vector();
 
               VEmpty.addElement((String)MiddlePanel.VGCode.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGName.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGNewHsnCode.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGBlock.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGMRSNo.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGOrderNo.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGOrdQty.elementAt(iRow));
               VEmpty.addElement(""+dPendQty);
               VEmpty.addElement(SQty);
               VEmpty.addElement(""+dRecQty);
               VEmpty.addElement((String)MiddlePanel.VGRate.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGDiscPer.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGNewCGSTPer.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGNewSGSTPer.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGNewIGSTPer.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGNewCessPer.elementAt(iRow));
               VEmpty.addElement("");
               VEmpty.addElement("");
               VEmpty.addElement("");
               VEmpty.addElement("");
               VEmpty.addElement("");
               VEmpty.addElement("");
               VEmpty.addElement("");
               VEmpty.addElement((String)MiddlePanel.VGDept.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGGroup.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGUnit.elementAt(iRow));
               if(((String)MiddlePanel.VTaxC.elementAt(iRow)).equals("0"))
               {
                   VEmpty.addElement("Not Claimable");
               }
               else
               {
                   VEmpty.addElement("Claimable");
               }

               MiddlePanel.MiddlePanel.dataModel.addRow(VEmpty);
               MiddlePanel.MiddlePanel.VHsnType.addElement((String)MiddlePanel.VGHsnType.elementAt(iRow));
               MiddlePanel.MiddlePanel.VSeleId.addElement((String)MiddlePanel.VId.elementAt(iRow));
               MiddlePanel.MiddlePanel.VRateStatus.addElement((String)MiddlePanel.VGRateStatus.elementAt(iRow));
 
               GrnModel.VQtyStatus.setElementAt("1",iRow);

               int iRows = MiddlePanel.MiddlePanel.dataModel.getRows();

	       MiddlePanel.MiddlePanel.dataModel.ColumnData = new Object[iRows][MiddlePanel.MiddlePanel.dataModel.ColumnType.length];

               for(int i=0;i<iRows;i++)
               {
                  Vector rowVector   = (Vector)MiddlePanel.MiddlePanel.dataModel.getCurVector(i);
		  String SHsnType    = (String)MiddlePanel.MiddlePanel.VHsnType.elementAt(i);
		  String SRateStatus = (String)MiddlePanel.MiddlePanel.VRateStatus.elementAt(i);
                  MiddlePanel.MiddlePanel.dataModel.setMaterialAmount(rowVector,SHsnType,SRateStatus,i);
               }

	       checkOrderRate();
          }
          catch(Exception ex){}
     }

     private void deleteGrnRow(int iRow)
     {
          try
          {
               String SCode      = (String)GrnModel.getValueAt(iRow,0);
               String SId        = (String)MiddlePanel.VId.elementAt(iRow);

               Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();

               int iGrnRow        = getRowIndex(SId,RowData);

               if(iGrnRow>=0)
               {
                    String  SRecQty = (String)RowData[iGrnRow][8];

                    double dRecQty     = common.toDouble(SRecQty);
                    double dPrevRecQty = common.toDouble((String)MiddlePanel.VGGrnQty.elementAt(iRow));
                    double dTotRecQty  = dPrevRecQty - dRecQty;
                    MiddlePanel.VGGrnQty.setElementAt(String.valueOf(dTotRecQty),iRow);
      
                    MiddlePanel.MiddlePanel.VSeleId.removeElementAt(iGrnRow);
                    MiddlePanel.MiddlePanel.VHsnType.removeElementAt(iGrnRow);
                    MiddlePanel.MiddlePanel.VRateStatus.removeElementAt(iGrnRow);

                    MiddlePanel.MiddlePanel.dataModel.removeRow(iGrnRow);
               }

               GrnModel.VQtyStatus.setElementAt("0",iRow);

               int iRows = MiddlePanel.MiddlePanel.dataModel.getRows();

               if(iRows>0)
               {
                    for(int i=0;i<iRows;i++)
                    {
                       Vector rowVector   = (Vector)MiddlePanel.MiddlePanel.dataModel.getCurVector(i);
		       String SHsnType    = (String)MiddlePanel.MiddlePanel.VHsnType.elementAt(i);
		       String SRateStatus = (String)MiddlePanel.MiddlePanel.VRateStatus.elementAt(i);
                       MiddlePanel.MiddlePanel.dataModel.setMaterialAmount(rowVector,SHsnType,SRateStatus,i);
                    }
               }
               else
               {
                    MiddlePanel.MiddlePanel.LBasic.setText("0");
                    MiddlePanel.MiddlePanel.LDiscount.setText("0");
                    MiddlePanel.MiddlePanel.LCGST.setText("0");
                    MiddlePanel.MiddlePanel.LSGST.setText("0");
                    MiddlePanel.MiddlePanel.LIGST.setText("0");
                    MiddlePanel.MiddlePanel.LCess.setText("0");
                    MiddlePanel.MiddlePanel.TAdd.setText("");
                    MiddlePanel.MiddlePanel.TLess.setText("");
                    MiddlePanel.MiddlePanel.LNet.setText("0");
                    MiddlePanel.MiddlePanel.LOthers.setText("0");
               }
          }
          catch(Exception ex){}
     }

     private int getRowIndex(String SId,Object RowData[][])
     {
          int iIndex=-1;
          
          for(int i=0;i<RowData.length;i++)
          {
               String SSeleId = (String)MiddlePanel.MiddlePanel.VSeleId.elementAt(i);
               
               if(!SSeleId.equals(SId))
                    continue;
               
               iIndex=i;
               return iIndex;
          }
          return iIndex;
     }

     private void checkOrderRate()
     {
          try
          {
               int iRows = MiddlePanel.MiddlePanel.dataModel.getRows();
               for(int i=0;i<iRows;i++)
               {
                  Vector rowVector = (Vector)MiddlePanel.MiddlePanel.dataModel.getCurVector(i);
		  String SHsnType = (String)MiddlePanel.MiddlePanel.VHsnType.elementAt(i);
		  if(SHsnType.equals("1"))
			continue;

		  String SOrderNo  = (String)rowVector.elementAt(5);
		  String SItemCode = (String)rowVector.elementAt(0);
		  String SCGST     = common.getRound(common.toDouble((String)rowVector.elementAt(12)),2);
		  String SSGST     = common.getRound(common.toDouble((String)rowVector.elementAt(13)),2);
		  String SIGST     = common.getRound(common.toDouble((String)rowVector.elementAt(14)),2);
		  String SCess     = common.getRound(common.toDouble((String)rowVector.elementAt(15)),2);

		  String SSeleId = (String)MiddlePanel.MiddlePanel.VSeleId.elementAt(i);

		  int iOrdIndex = common.exactIndexOf(MiddlePanel.VId,SSeleId);

		  String SOrdCGST = common.getRound(common.toDouble((String)MiddlePanel.VGCGSTPer.elementAt(iOrdIndex)),2);
		  String SOrdSGST = common.getRound(common.toDouble((String)MiddlePanel.VGSGSTPer.elementAt(iOrdIndex)),2);
		  String SOrdIGST = common.getRound(common.toDouble((String)MiddlePanel.VGIGSTPer.elementAt(iOrdIndex)),2);
		  String SOrdCess = common.getRound(common.toDouble((String)MiddlePanel.VGCessPer.elementAt(iOrdIndex)),2);

		  if(!(SCGST.equals(SOrdCGST) && SSGST.equals(SOrdSGST) && SIGST.equals(SOrdIGST) && SCess.equals(SOrdCess)))
		  {
               		//JOptionPane.showMessageDialog(null,"GRN and Order Tax Rate Mismatch at Row-"+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
			JOptionPane.showMessageDialog(null,getAllFilledMessage(i,SOrderNo,SItemCode,SCGST,SSGST,SIGST,SCess,SOrdCGST,SOrdSGST,SOrdIGST,SOrdCess),"Error",JOptionPane.ERROR_MESSAGE);

             		GrnModel.setValueAt(new Boolean(false),iOrdIndex,10);
			deleteGrnRow(iOrdIndex);
		  }
               }
          }
          catch(Exception ex){}
     }

     private String getAllFilledMessage(int iRow,String SOrderNo,String SItemCode,String SCGST,String SSGST,String SIGST,String SCess,String SOrdCGST,String SOrdSGST,String SOrdIGST,String SOrdCess)
     {
          String str = "<html><body>";
          str = str + "<b>GRN and Order Tax Rate Mismatch at Row-"+String.valueOf(iRow+1)+"</b>"+"<br>";
          str = str + "<b>OrderNo	 : "+SOrderNo+"		Code		: "+SItemCode+"</b>"+"<br>";
          str = str + "Order CGST %	 : "+SOrdCGST+"		Grn CGST %	: "+SCGST+"<br>";
          str = str + "Order SGST %	 : "+SOrdSGST+"		Grn SGST %	: "+SSGST+"<br>";
          str = str + "Order IGST %	 : "+SOrdIGST+"		Grn IGST %	: "+SIGST+"<br>";
          str = str + "Order Cess %	 : "+SOrdCess+"		Grn Cess %	: "+SCess+"<br>";
          str = str + "</body></html>";
          return str;
     }


     public Object[][] getFromVector()
     {
          return GrnModel.getFromVector();     
     }

     /*private void getHsnCode(String SItemCode)
     {
          try
          {
               String QS = " select HsnCode,nvl(HsnType,0) as HsnType from invitems where item_code ='"+SItemCode+"'";

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement = theConnection.createStatement();
               ResultSet theResult    = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    shsncode = common.parseNull(theResult.getString(1));
                    shsntype = common.parseNull(theResult.getString(2));
               }
               theResult.close();
               theStatement.close();

	       VRefItemCode.addElement(SItemCode);
	       VRefHsnCode.addElement(shsncode);
	       VRefHsnType.addElement(shsntype);
          }
          catch(Exception ex)
          {
               System.out.println(ex);  
          }
     }*/


}

