package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGRNUpdateFrame extends JInternalFrame {

	 JLayeredPane             DeskTop;
	 private JDialog		  thedialog = null;
     
     int                      iMillCode, iDivisionCode, iUserCode, iAuthCode;
     String                   SYearCode;
     String                   SItemTable,SSupTable;

	 JPanel                   sp, TopPanel, MiddlePanel, MidTopPanel, MidBotPanel, BottomPanel, FigurePanel,ControlPanel;

	 JTextField               TDate;
     DateField                TInvDate, TDCDate;
     
     JTextField               TSupCode, TSupData;
     WholeNumberField         TInvoice;
     MyTextField              TInvNo, TDCNo;
     
     InwardModeModel          InwMode;
	
	 JLabel                   LGrnNo;
	 
     JComboBox                JCInwType, JCSort;
	 JComboBox       	      cmbInvoiceType ;
	 JComboBox 				  JCDept, JCGroup, JCUnit;
	 
	 JButton                  BOk, BExit, BApply, BSupplier;
	 
	 Vector                   VInwName, VInwNo, VInvTypeCode, VInvTypeName;
   	 Vector 				  VDept,VDeptCode,VGroup,VGroupCode,VUnit,VUnitCode;
	 
	 String                   SGrnNo = "", SPJNo = "", SGateNo = "";
	 
     java.sql.Connection       theMConnection = null;
     java.sql.Connection       theDConnection = null;
	 
	 DirectGRNUpdateModel      GrnModel;
	 JTable               	   GrnTable;
	 
     Vector VQtyStatus;
     Vector VGCode, VGName, VGBlock, VGOrderNo, VGMRSNo, VBinNo;
     Vector VGPendQty, VGGrnQty, VGRate, VGDiscPer, VGCGSTPer, VGSGSTPer, VGIGSTPer, VGCessPer;
     Vector VGDiscVal, VGCGSTVal, VGSGSTVal, VGIGSTVal, VGCessVal, VGInvVal, VGPlus, VGMinus;
     
     Vector VGDept, VGDeptCode, VGGroup, VGGroupCode, VGUnit, VGUnitCode;
     Vector VGBlockCode, VId, VGOrdQty, VGrnDate, VPurOrdDate;
     Vector VMrsSlNo, VOrderSlNo, VTaxC, VPQtyAllow, VMrsUserCode, VApproval;
     Vector VGHsnCode, VGHsnType, VGGrnStatus, VGRateStatus, VGGstRate, VItemCode, VItemName;
     
	 Vector VHsnType, VSeleId, VRateStatus;
	 
	 Vector VSupCode, VSupName, VInvNo, VInvDate, VDcNo, VDcDate, VNoofbills, VGstPartyTypeCode;
	 
	 //" grn.sup_code, "+SSupTable+".Name, grn.invno,  grn.invdate, grn.Dcno, grn.Dcdate, grn.noofbills, grn.gstpartytypecode   "+
	 Common common = new Common();
	    
 	 JLabel         		  LBasic, LDiscount, LCGST, LSGST, LIGST, LCess, LNet, LOthers;
     NextField      		  TAdd,TLess;
     JButton        		  BAdd,BDel;
	 
	 double dTGross = 0, dTDisc = 0, dTCGST = 0, dTSGST = 0, dTIGST = 0, dTCess = 0, dTNet = 0, dTOthers = 0;
	 
	 String 			   SSeleSupType, SSeleStateCode;
	 
	 boolean bComflag = true;
	 
    public DirectGRNUpdateFrame(JLayeredPane DeskTop,String SGrnNo,String SGateNo,String SGIDate,String SPJNo,int iMillCode,int iUserCode,String SItemTable,String SSupTable)
    {
        this.DeskTop            = DeskTop;
        this.SYearCode          = SYearCode;
        this.iDivisionCode      = (iMillCode+1);
        this.iMillCode          = iMillCode;
        this.iUserCode          = iUserCode;
        this.SItemTable         = SItemTable;
        this.SSupTable          = SSupTable;
        this.SGrnNo             = SGrnNo;
        this.SPJNo              = SPJNo;
        this.SGateNo            = SGateNo;
        
		initValues();
		createComponents();
		setLayout();
		addComponents();
		addListeners();
		setGrnDetails();
		
		thedialog   . setBounds(0,50,1000,580);
		thedialog   . setVisible(true);
		thedialog   . setFocusable(true);
	}
	
	private void initValues() {
		
		setInwardData();
		setInvoiceTypeVector();
		getDeptGroupUnit();
		
		thedialog   = new JDialog(new Frame(),"Grn Update Frame",true);
	}
	 
	 
	private void createComponents(){
	 	sp          = new JPanel();
	 	TopPanel    = new JPanel();
	 	MiddlePanel = new JPanel();
	 	MidTopPanel = new JPanel();
	 	MidBotPanel = new JPanel();
	 	BottomPanel = new JPanel();
	 	FigurePanel  = new JPanel();
	 	ControlPanel = new JPanel();
	 	
        BOk         = new JButton("Okay");
		BExit       = new JButton("Exit");
        BApply      = new JButton("Apply");
        BApply      . setEnabled(false);
        BSupplier   = new JButton("Supplier");
	 	
		TDate       = new JTextField();
		TDate       . setEditable(false);
		LGrnNo      = new JLabel("");
		LGrnNo      . setText(SGrnNo);

		TSupCode    = new JTextField();
		TSupData    = new JTextField();
		
		TInvoice    = new WholeNumberField(2);

		TInvDate    = new DateField();
		TDCDate     = new DateField();
		
		TInvNo      = new MyTextField(25);
		TDCNo       = new MyTextField(15);

		JCInwType   = new JComboBox(VInwName);
		InwMode     = new InwardModeModel(iUserCode, iAuthCode, iMillCode);
		JCSort      = new JComboBox();
		
        JCSort.addItem("OrderNo");
        JCSort.addItem("Name");
        JCSort.addItem("Code");
		
		cmbInvoiceType = new JComboBox(VInvTypeName);
		cmbInvoiceType . setSelectedItem(VInvTypeName.get(0));
	 	
        JCDept  = new JComboBox(VDept);
        JCGroup = new JComboBox(VGroup);
        JCUnit  = new JComboBox(VUnit);
	 	
	 	setVectorData();
	 	
        LBasic           = new JLabel("0");
        LDiscount        = new JLabel("0");
        LCGST            = new JLabel("0");
        LSGST            = new JLabel("0");
        LIGST            = new JLabel("0");
        LCess            = new JLabel("0");
        TAdd             = new NextField();
        TLess            = new NextField();
        LNet             = new JLabel("0");
        LOthers          = new JLabel("0");

        BAdd             = new JButton("Add New Record");
        BDel             = new JButton("Delete Record");
	 	
	 	
		GrnModel = new DirectGRNUpdateModel(LBasic, LDiscount, LCGST, LSGST, LIGST, LCess, TAdd, TLess , LNet, LOthers, SSeleSupType, SSeleStateCode, VGName.size());
		GrnTable = new JTable(GrnModel);
	 	
	 	GrnTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	 	GrnTable.setRowHeight(20);
	 	
        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setHorizontalAlignment(JLabel.RIGHT);
	 	
        for (int col=0;col<GrnTable.getColumnCount();col++)
        {
            if(GrnModel.ColumnType[col]=="N" || GrnModel.ColumnType[col]=="B")
                GrnTable.getColumn(GrnModel.ColumnData[col]).setCellRenderer(cellRenderer);
        }
	 	
        TableColumn deptColumn  = GrnTable.getColumn("Department");
        TableColumn groupColumn = GrnTable.getColumn("Group");
        TableColumn unitColumn  = GrnTable.getColumn("Unit");

        deptColumn.setCellEditor(new DefaultCellEditor(JCDept));
        groupColumn.setCellEditor(new DefaultCellEditor(JCGroup));
        unitColumn.setCellEditor(new DefaultCellEditor(JCUnit));
	 	
	 	
	 	
	}
	 
	private void setLayout(){
        /*
        setTitle("Grn Update");
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setBounds(5,5,1300,600);
        */
          
        //thedialog   . setBounds(50, 50, 1300, 600);
        //thedialog   . setAlwaysOnTop(true);
        //thedialog   . setUndecorated(true);
        //thedialog   . setLocationRelativeTo(null);
		//thedialog   . setVisible(true);
        //thedialog   . setFocusable(true);
          
        //getContentPane()    .setLayout(new BorderLayout());
          
        SCLayout  splayout = new SCLayout(3,guiutil.SCLayout.FILL,guiutil.SCLayout.FILL,3);
                  splayout . setScale(0,0.30);
                  splayout . setScale(1,0.55);
                  splayout . setScale(2,0.15);

        sp     . setLayout(new BorderLayout());
          
          
        TopPanel            . setLayout(new GridLayout(7,4,5,5));
        
        SCLayout    MiddlePanellayout  =  new SCLayout(2,SCLayout.FILL,SCLayout.FILL,3);
                    MiddlePanellayout  .  setScale(0,0.75);
                    MiddlePanellayout  .  setScale(1,0.25);

        MiddlePanel        .  setLayout(MiddlePanellayout);
        
        MidTopPanel         . setLayout(new BorderLayout());
        MidBotPanel         . setLayout(new FlowLayout());
		FigurePanel			. setLayout(new GridLayout(2,9,5,5));
		ControlPanel		. setLayout(new FlowLayout());
        
        BottomPanel         . setLayout(new FlowLayout());
		
        BSupplier.setBorder(new javax.swing.border.BevelBorder(0));
        BSupplier.setBackground(new Color(128,128,255));
        BSupplier.setForeground(Color.RED);

        TopPanel    . setBorder(new TitledBorder(""));
        MiddlePanel . setBorder(new TitledBorder(""));
        BottomPanel . setBorder(new TitledBorder(""));
	}
	 
	private void addComponents(){
	 
		TopPanel.add(new JLabel("GRN No"));
		TopPanel.add(LGrnNo);

		TopPanel.add(new JLabel("GRN Date"));
		TopPanel.add(TDate);

		TopPanel.add(new JLabel("Supplier"));
		TopPanel.add(BSupplier);

		TopPanel.add(new JLabel("No. of Bills"));
		TopPanel.add(TInvoice);

		TopPanel.add(new JLabel("Invoice No"));
		TopPanel.add(TInvNo);

		TopPanel.add(new JLabel("Invoice Date"));
		TopPanel.add(TInvDate);

		TopPanel.add(new JLabel("DC No"));
		TopPanel.add(TDCNo);

		TopPanel.add(new JLabel("DC Date"));
		TopPanel.add(TDCDate);
		
		TopPanel.add(new JLabel("Select Inward Type"));
		TopPanel.add(JCInwType);

		TopPanel.add(new JLabel("Sorting Pending Orders By"));
		TopPanel.add(JCSort);

		TopPanel.add(new JLabel("Select Inward Mode"));
		TopPanel.add(InwMode);

		TopPanel.add(new JLabel(""));
		TopPanel.add(BApply);
		
		TopPanel.add(new JLabel("Invoice Type"));
		TopPanel.add(cmbInvoiceType);
		
		MidTopPanel.add(new JScrollPane(GrnTable,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS));

        FigurePanel.add(new JLabel("Basic"));
        FigurePanel.add(new JLabel("Discount"));
        FigurePanel.add(new JLabel("CGST"));
        FigurePanel.add(new JLabel("SGST"));
        FigurePanel.add(new JLabel("IGST"));
        FigurePanel.add(new JLabel("Cess"));
        FigurePanel.add(new JLabel("Plus"));
        FigurePanel.add(new JLabel("Minus"));
        FigurePanel.add(new JLabel("Net"));

        FigurePanel.add(LBasic);
        FigurePanel.add(LDiscount);
        FigurePanel.add(LCGST);
        FigurePanel.add(LSGST);
        FigurePanel.add(LIGST);
        FigurePanel.add(LCess);
        FigurePanel.add(TAdd);
        FigurePanel.add(TLess);
        FigurePanel.add(LNet);

        ControlPanel.add(new JLabel("F2 - For add Other Charges"));
        ControlPanel.add(BAdd);
        ControlPanel.add(BDel);
		
        MidBotPanel.add(ControlPanel);
        MidBotPanel.add(FigurePanel);
		
		MiddlePanel.add(MidTopPanel);
		MiddlePanel.add(MidBotPanel);
		
        BottomPanel.add(BOk);
		BottomPanel.add(BExit);
		
		thedialog.getContentPane().add(TopPanel,"North");
        thedialog.getContentPane().add(MiddlePanel,"Center");
        thedialog.getContentPane().add(BottomPanel,"South");
	}
	 
	private void addListeners(){
          
        BAdd.addActionListener(new ActList());
        BDel.addActionListener(new ActList());
        TAdd.addKeyListener(new KeyList());
        TLess.addKeyListener(new KeyList());
          
       // BApply    . addActionListener(new ActList());
        BOk       . addActionListener(new ActList());
		BExit     . addActionListener(new ActList());
        BSupplier . addActionListener(new GrnSupplierSearch(DeskTop,TSupCode,SSupTable,TSupData));
        
	 	GrnTable.addKeyListener(new ServList());
	 	
	}
	
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
          
               if(ae.getSource()==BOk)
               {
                    BOk.setEnabled(false);
                    bComflag  = true;
                    updateGRNDetails();
			   }   
			   if(ae.getSource()==BExit){
					
			   }
			          
               if(ae.getSource()==BAdd)
               {
                    int iRows = GrnModel.getRows();

	    		  if(iRows>=1) {
		    		
		            Vector VEmpty1 = new Vector();
		       
		            for(int i=0;i<GrnModel.ColumnData.length;i++)
		            {
		                 VEmpty1   . addElement("");
		            }

					VHsnType.addElement("1");
					VSeleId.addElement("0");
					VRateStatus.addElement("0");
					
					GrnModel.VQtyStatus.addElement("0");
		            GrnModel.VHsnType.addElement("1");
		            GrnModel.VRateStatus.addElement("0");
					
		            GrnModel         . appendRow(VEmpty1);
		            GrnModel		 . fireTableDataChanged();
		            GrnTable         . updateUI();
		    	  }
               }

               if(ae.getSource()==BDel)
               {
                    int iRows = GrnModel.getRows();
		    	if(iRows>1)
		    	{
		         	int i = GrnTable.getSelectedRow();
		         	
			 		//String SHsnType = (String)VHsnType.elementAt(i);
			 		if(i!=-1){
			 		String SSeleId = (String)VSeleId.elementAt(i);
			 		
			 		//if(SHsnType.equals("1"))
			 		if(SSeleId.equals("0"))
			 		{
				 		GrnModel.removeRow(i);

				 		VHsnType.removeElementAt(i);
				 		VSeleId.removeElementAt(i);
				 		VRateStatus.removeElementAt(i);
						
						GrnModel.VQtyStatus.removeElementAt(i);
						
				 		GrnModel.setTaxData();

				 		GrnTable.updateUI();
				 		GrnModel.fireTableDataChanged();
			 		}
			 		else
			 		{
		              JOptionPane.showMessageDialog(null,"This row couldn't be Deleted","Information",JOptionPane.INFORMATION_MESSAGE);
			 		}
			 		}
		    	}
             }
          }
     }
	
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               calc();
          }
     }
     
     public void calc()
     {
               double dTNet = common.toDouble(LBasic.getText())-common.toDouble(LDiscount.getText())+common.toDouble(LCGST.getText())+common.toDouble(LSGST.getText())+common.toDouble(LIGST.getText())+common.toDouble(LCess.getText());
               
               dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
               LNet.setText(common.getRound(dTNet,2));                              
     }

	
     public class ServList extends KeyAdapter
     {
       	public void keyPressed(KeyEvent ke)
        {
            if(ke.getKeyCode()==KeyEvent.VK_F2) 
		    {
				int i = GrnTable.getSelectedRow();
			 	//String SHsnType = (String)VHsnType.elementAt(i);
			 	String SSeleId = (String)VSeleId.elementAt(i);
			 	 
			 	if(SSeleId.equals("0"))
			 	{
                   setServList();
			 	}
			 	else
			 	{
		              JOptionPane.showMessageDialog(null,"This row couldn't be changed","Information",JOptionPane.INFORMATION_MESSAGE);
			 	}
		    }
        }
     }

     private void setServList()
     {
     	 	
           ServiceNameList SP = new ServiceNameList(DeskTop,GrnTable,GrnModel,VRateStatus, SSeleSupType, SSeleStateCode);
           try
           {
                DeskTop   . add(SP);
                DeskTop   . repaint();
                SP        . setSelected(true);
                DeskTop   . updateUI();
                SP        . show();
                SP        . BrowList.requestFocus();
           }
           catch(java.beans.PropertyVetoException ex){
           }
     }
	
	
     public void updateGRNDetails()
     {
          if(common.toInt(SPJNo) > 0)
          {
               String QString = "Update GRN Set ";
               try
               {
               	if(theMConnection==null){
                    ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                                   theMConnection =  oraConnection.getConnection();
                }
                theMConnection.setAutoCommit(false);
                    Statement       stat =  theMConnection.createStatement();
                    Object RowData[][]   = GrnModel.getFromVector();
                    
                    for(int i=0;i<RowData.length;i++)
                    {
                         String SUnitCode  = getUnitCode(i);
                         String SDeptCode  = getDeptCode(i);
                         String SGroupCode = getGroupCode(i);
     
                         String QS1 = QString;
                         QS1 = QS1+" Dept_Code=0"+SDeptCode+",";
                         QS1 = QS1+" Group_Code=0"+SGroupCode+",";
                         QS1 = QS1+" Unit_Code=0"+SUnitCode;
                         QS1 = QS1+" Where Code='"+(String)RowData[i][0]+"' and GrnNo="+(String)LGrnNo.getText()+" and OrderNo="+(String)RowData[i][5]+" ";
                         QS1 = QS1+" and GrnDate="+common.pureDate((String)TDate.getText())+" and SPJNo=0 and NoofSPJ=0 ";
                         
                         System.out.println("SPJNo>0 QS1==>"+QS1);
                         stat.executeUpdate(QS1);
                     }
                     stat.close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    bComflag  = false;
               }
          }
          else
          {

               
               try
               {
               	    if(theMConnection==null){
                       ORAConnection    oraConnection = ORAConnection.getORAConnection();
                       theMConnection = oraConnection . getConnection();
                    }
                    theMConnection.setAutoCommit(false);
                    Statement       stat =  theMConnection.createStatement();

                    Object RowData[][]   = GrnModel.getFromVector();

		            String SAdd        = TAdd.getText();
		            String SLess       = TLess.getText();
		            double dOthers     = common.toDouble(LOthers.getText());
		           
		            double dpm         = common.toDouble(SAdd)-common.toDouble(SLess)+dOthers;

		            double dNet        = common.toDouble(LNet.getText())-dpm;
		            double dRatio      = dpm/dNet;
		            
					/*
					inv = 353.06
					dpm = 0-0.06+82.6 = 82.54
					net = 435.66-82.54 = 353.12
					drat = 0.2337
					*/
					
					System.out.println("dOthers="+dOthers);
					
					if(dOthers>=0){
						
						if(javax.swing.JOptionPane.showConfirmDialog(null," Other(Freight/Courier) Charges "+dOthers+" Do You Want to Continue?","Info",javax.swing.JOptionPane.YES_NO_OPTION)==0){
							
						}else{
							return;
						}
					}
					
					System.out.println("RowData.length==>"+RowData.length);
					
                    for(int i=0;i<RowData.length;i++)
                    {
                    	 String SSeleId = (String)VSeleId.elementAt(i);
                    	 
                    	 if(common.toInt(SSeleId)==0){
                    	 	 insertGRNDetails(i);
                    	 	 continue;
                    	 }
                    	 
                    
                         String SUnitCode  = getUnitCode(i);
                         String SDeptCode  = getDeptCode(i);
                         String SGroupCode = getGroupCode(i);
                              
                    	 String SGstRate   = common.getRound((common.toDouble((String)RowData[i][12])+common.toDouble((String)RowData[i][13])+common.toDouble((String)RowData[i][14])),2);

						String SMisc      = "0";

            			String SHsnType   = (String)VHsnType.elementAt(i);

						if(SHsnType.equals("0"))
						{
						 	   SMisc = common.getRound(common.toDouble((String)RowData[i][22])*dRatio,3);
						}
						String SGrnValue = "";
		                	   SGrnValue = common.getRound(common.toDouble((String)RowData[i][22]) + common.toDouble(SMisc),2);

//GRN
               String GrnQS  = " Update GRN Set ";

					if(common.toInt(SSeleStateCode)==33){
					  GrnQS += " CGST = 0"+(String)RowData[i][12]+", CGStval= 0"+(String)RowData[i][18]+", SGST = 0"+(String)RowData[i][13]+", SGSTval = 0"+(String)RowData[i][19]+", ";
					}else{
				      GrnQS += " IGST =  0"+(String)RowData[i][14]+", IGSTval= 0"+(String)RowData[i][20]+", ";
					}
					
					  GrnQS += " InvRate =0"+(String)RowData[i][10]+",  INVAMOUNT=0"+(String)RowData[i][22]+", InvNEt = 0"+(String)RowData[i][22]+", GSTRate= "+SGstRate+", grnvalue='0"+SGrnValue+"', Plus =0"+SAdd+", Less =0"+SLess+",  Misc = 0"+SMisc+", ";
					  GrnQS += " Dept_Code=0"+SDeptCode+", Group_Code=0"+SGroupCode+", Unit_Code = 0"+SUnitCode+", NoOfBills = 0"+TInvoice.getText()+" ";
					  GrnQS += " where Code='"+(String)RowData[i][0]+"'  and GrnNo="+(String)LGrnNo.getText()+" and OrderNo='"+(String)RowData[i][5]+"' ";
					  GrnQS += " and GrnDate="+common.pureDate((String)TDate.getText())+" and SPJNo=0 and NoofSPJ=0  and MillCode="+iMillCode;

					System.out.println("SPJNo==0 > QS1==>"+GrnQS);
				    stat.executeUpdate(GrnQS);
					

//Invoice GRN
               String InvoiceGrnQS  = " Update INVOICEGRN Set ";

					if(common.toInt(SSeleStateCode)==33){
					  InvoiceGrnQS += " CGST = 0"+(String)RowData[i][12]+", CGStval= 0"+(String)RowData[i][18]+", SGST = 0"+(String)RowData[i][13]+", SGSTval = 0"+(String)RowData[i][19]+", ";
					}else{
				      InvoiceGrnQS += " IGST =  0"+(String)RowData[i][14]+", IGSTval= 0"+(String)RowData[i][20]+", ";
					}
					
					  InvoiceGrnQS += " InvRate =0"+(String)RowData[i][10]+",  INVAMOUNT=0"+(String)RowData[i][22]+", InvNEt = 0"+(String)RowData[i][22]+", GSTRate= "+SGstRate+", grnvalue='0"+SGrnValue+"', Plus =0"+SAdd+", Less =0"+SLess+",  Misc = 0"+SMisc+", ";
					  InvoiceGrnQS += " Dept_Code=0"+SDeptCode+", Group_Code=0"+SGroupCode+", Unit_Code = 0"+SUnitCode+", NoOfBills = 0"+TInvoice.getText()+" ";
					  InvoiceGrnQS += " where Code='"+(String)RowData[i][0]+"'  and GrnNo="+(String)LGrnNo.getText()+" and OrderNo='"+(String)RowData[i][5]+"' ";
					  InvoiceGrnQS += " and GrnDate="+common.pureDate((String)TDate.getText())+" and SPJNo=0 and NoofSPJ=0  and MillCode="+iMillCode;

					System.out.println("SPJNo==0 > QS1==>"+InvoiceGrnQS);
				    stat.executeUpdate(InvoiceGrnQS);
					
                    	 
            	 if(common.toInt(SHsnType)==1) {
            	 	 continue;
            	 }
				

//PurchaseOrder
				
               String POQS     = " Update PurchaseOrder Set ";
				if(common.toInt(SSeleStateCode)==33){
					  POQS  += " CGST = 0"+(String)RowData[i][12]+", CGStval= 0"+(String)RowData[i][18]+", SGST = 0"+(String)RowData[i][13]+", SGSTval = 0"+(String)RowData[i][19]+", ";
				}else{
					  POQS += " IGST =  0"+(String)RowData[i][14]+", IGSTval= 0"+(String)RowData[i][20]+", ";
				}

					  POQS += " Rate =0"+(String)RowData[i][10]+",  Net = 0"+(String)RowData[i][22]+", GSTRate= "+SGstRate+" ";
					  POQS += " Where OrderNo='"+(String)RowData[i][5]+"' and Item_Code='"+(String)RowData[i][0]+"' and OrderDate= "+(String)VPurOrdDate.elementAt(i);
					  POQS += " and MillCode="+iMillCode+" ";
					  
                System.out.println("SPJNo==0 > POQS==>"+POQS);	
                stat.executeUpdate(POQS);
                
//ItemStock               
               Item IC = new Item((String)RowData[i][0], common.pureDate((String)TDate.getText()), iMillCode, SItemTable, SSupTable);
     
               double dAllStock = common.toDouble(IC.getClStock());
               double dAllValue = common.toDouble(IC.getClValue());
     
               double dRate   = 0;
               try
               {
                    dRate = dAllValue/dAllStock;
               }
               catch(Exception ex)
               {
                    dRate=0;
               }
     
               if(dAllStock==0)
               {
                    dRate = common.toDouble(IC.SRate);
               }

  
               String StockQS = " Update ItemStock set StockValue="+common.getRound(dRate,4)+" Where ItemCode='"+(String)RowData[i][0]+"' and MillCode="+iMillCode;
               
               System.out.println("SPJNo==0 > StockQS==>"+StockQS);	
               stat.executeUpdate(StockQS);
               
//Issue            
				/*   
			   double dIRate    = common.toDouble((String)RowData[i][10]);
			   double disc 	   = common.toDouble((String)RowData[i][11]);
	   		   double dActRate = dIRate-(dIRate*(disc/100));
	   		   
	   		   double cgstval=0, sgstval = 0, igstval = 0;
	   		   
	   		   if(common.toInt(SSeleStateCode)==33){
	   		   		cgstval = (dActRate*(common.toDouble((String)RowData[i][12])/100));
	   		   		sgstval = (dActRate*(common.toDouble((String)RowData[i][13])/100));
	   		   }else{
	   		   		igstval = (dActRate*(common.toDouble((String)RowData[i][14])/100));
	   		   }
			   
			   double dIssRate = dActRate+cgstval+sgstval+igstval;
			   */
			   
               String IssueQS  = " Update Issue set IssRate = 0"+common.getRound(dRate,2)+", IssueValue =nvl(qty,0)*"+common.getRound(dRate,4)+" ";
               		  IssueQS += " Where Code = '"+(String)RowData[i][0]+"'  And IssueDate>="+common.pureDate((String)TDate.getText())+" and MillCode="+iMillCode;

				System.out.println("SPJNo==0 > IssueQS==>"+IssueQS);
				stat.executeUpdate(IssueQS);

                         
                    }
                    stat.close();
                    
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    bComflag  = false;
               }
               finally{
               		try{
               			if(theMConnection!=null){
               				if(bComflag){
               					System.out.println("success- commit");
               					theMConnection.commit();
               					BOk.setEnabled(false);
               					JOptionPane.showMessageDialog(null,"Data saved successfully.","Information",JOptionPane.INFORMATION_MESSAGE);
               				}else{
               					System.out.println("failure- rollback");
               					theMConnection.rollback();
               					BOk.setEnabled(true);
               					JOptionPane.showMessageDialog(null,"Data not saved.","Information",JOptionPane.INFORMATION_MESSAGE);
               				}
               				theMConnection.setAutoCommit(true);
               			}
               		}catch(Exception e){
               		}
               }
          }
     }
	
	
	private void setGrnDetails(){
		GrnModel.setNumRows(0);
		
		//setVectorData();
		
		dTGross  = 0; 
		dTDisc   = 0; 
		dTCGST   = 0;
		dTSGST   = 0;
		dTIGST   = 0;
		dTCess   = 0;
		dTNet    = 0;
		dTOthers = 0;
		
		String SGrnDate = "";
		for(int i=0;i<VGName.size();i++) {
			
			System.out.println(i+"==>"+(String)VGName.elementAt(i));
			
			if(i==0){
				BSupplier . setText(common.parseNull((String)VSupName.elementAt(i)));
				TSupCode  . setText(common.parseNull((String)VSupCode.elementAt(i)));
				TSupData  . setText(common.parseNull((String)VSupName.elementAt(i)));
				
				System.out.println("GrnDate==>"+common.parseNull((String)VGrnDate.elementAt(i)));
				System.out.println("InvDate==>"+common.parseNull((String)VInvDate.elementAt(i)));
				
				TInvDate  . fromString1(common.parseNull((String)VInvDate.elementAt(i)));
				TDCDate   . fromString1(common.parseNull((String)VDcDate.elementAt(i)));
				
				SGrnDate = "";
				SGrnDate = common.parseNull((String)VGrnDate.elementAt(i));
				
				System.out.println("SGrnDate==>"+SGrnDate);
				
				//TDate     . fromString1(SGrnDate);
				TDate     . setText(common.parseDate(SGrnDate));
				
				TInvoice  . setText(common.parseNull((String)VNoofbills.elementAt(i)));		//No of bills
				TInvNo    . setText(common.parseNull((String)VInvNo.elementAt(i)));
				TDCNo     . setText(common.parseNull((String)VDcNo.elementAt(i)));
				
				TAdd      . setText(common.getRound(common.parseNull((String)VGPlus.elementAt(i)),2));
				TLess     . setText(common.getRound(common.parseNull((String)VGMinus.elementAt(i)),2));
			}
			
			addGrnRow(i);
		}
		
		/*
		common.parseNull((String)VSupCode		   .elementAt(i));
		common.parseNull((String)VSupName 		   .elementAt(i));
		common.parseNull((String)VInvNo   		   .elementAt(i));
		common.parseNull((String)VInvDate 		   .elementAt(i));
		common.parseNull((String)VDcNo    		   .elementAt(i));
		common.parseNull((String)VDcDate  		   .elementAt(i));
		common.parseNull((String)VNoofbills  	   .elementAt(i));
		common.parseNull((String)VGstPartyTypeCode .elementAt(i));
		*/
		
		//TDate          . fromString();
		//JCInwType      . setSelectedItem();
		//cmbInvoiceType . setSelectedItem();
		
        dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
          
        LBasic      . setText(common.getRound(dTGross,2));
        LDiscount   . setText(common.getRound(dTDisc,2));
        LCGST	    . setText(common.getRound(dTCGST,2));
        LSGST	    . setText(common.getRound(dTSGST,2));
        LIGST	    . setText(common.getRound(dTIGST,2));
        LCess	    . setText(common.getRound(dTCess,2));
        LNet	    . setText(common.getRound(dTNet,2));
        LOthers     . setText(common.getRound(dTOthers,2));
		
	}
	
     private void addGrnRow(int iRow)
     {
          try
          {
           //String SCode      = (String)GrnModel.getValueAt(iRow,0);
           //String SName      = (String)GrnModel.getValueAt(iRow,1);
           //String SPendQty   = (String)GrnModel.getValueAt(iRow,7);
           //String SQty       = (String)GrnModel.getValueAt(iRow,8);
           
           //String SCode      = (String)GrnModel.getValueAt(iRow,0);
           //String SName      = (String)GrnModel.getValueAt(iRow,1);
               
           String SPendQty   = (String)VGPendQty.elementAt(iRow);
           String SQty       = (String)VGGrnQty.elementAt(iRow);

           double dRecQty     = common.toDouble(SQty);
           double dPendQty    = common.toDouble(SPendQty);
           double dPrevRecQty = common.toDouble((String)VGGrnQty.elementAt(iRow));
           double dTotRecQty  = dRecQty+dPrevRecQty;
           
           double dBasic      = 0;
           
           if(common.toInt((String)VGHsnType.elementAt(iRow))==1){
           	 dBasic      = common.toDouble((String)VGRate.elementAt(iRow));
           }else{
           	 dBasic      = (common.toDouble((String)VGRate.elementAt(iRow))*dRecQty);
           }
              
           //VGGrnQty.setElementAt(String.valueOf(dTotRecQty),iRow);
 				
		      dTGross   = dTGross+dBasic;
		      dTDisc    = dTDisc+common.toDouble((String)VGDiscVal.elementAt(iRow));
		      dTCGST    = dTCGST+common.toDouble((String)VGCGSTVal.elementAt(iRow));
		      dTSGST    = dTSGST+common.toDouble((String)VGSGSTVal.elementAt(iRow));
		      dTIGST    = dTIGST+common.toDouble((String)VGIGSTVal.elementAt(iRow));
		      dTCess    = dTCess+common.toDouble((String)VGCessVal.elementAt(iRow));
		      dTNet     = dTNet+common.toDouble((String)VGInvVal.elementAt(iRow));
 				
 				
               Vector VEmpty = new Vector();
 
               VEmpty.addElement(common.parseNull((String)VGCode.elementAt(iRow)));
               VEmpty.addElement(common.parseNull((String)VGName.elementAt(iRow)));
               VEmpty.addElement(common.parseNull((String)VGHsnCode.elementAt(iRow)));
               VEmpty.addElement(common.parseNull((String)VGBlock.elementAt(iRow)));
               VEmpty.addElement(common.parseNull((String)VGMRSNo.elementAt(iRow)));
               VEmpty.addElement(common.parseNull((String)VGOrderNo.elementAt(iRow)));
               VEmpty.addElement(common.parseNull((String)VGOrdQty.elementAt(iRow)));
               VEmpty.addElement(""+dPendQty);
               VEmpty.addElement(SQty);
               VEmpty.addElement(""+dRecQty);
               VEmpty.addElement((String)VGRate.elementAt(iRow));
               VEmpty.addElement((String)VGDiscPer.elementAt(iRow));
               VEmpty.addElement((String)VGCGSTPer.elementAt(iRow));
               VEmpty.addElement((String)VGSGSTPer.elementAt(iRow));
               VEmpty.addElement((String)VGIGSTPer.elementAt(iRow));
               VEmpty.addElement((String)VGCessPer.elementAt(iRow));
               
               VEmpty.addElement(common.getRound(dBasic,2));			//Basic
               VEmpty.addElement((String)VGDiscVal.elementAt(iRow));	//Disc Val
               VEmpty.addElement((String)VGCGSTVal.elementAt(iRow));	//Cgst Val
               VEmpty.addElement((String)VGSGSTVal.elementAt(iRow));	//Sgst Val
               VEmpty.addElement((String)VGIGSTVal.elementAt(iRow));	//Igst Val
               VEmpty.addElement((String)VGCessVal.elementAt(iRow));	//Cess Val
               VEmpty.addElement((String)VGInvVal.elementAt(iRow));	    //Net Val
               VEmpty.addElement(common.parseNull((String)VGDept.elementAt(iRow)));
               VEmpty.addElement(common.parseNull((String)VGGroup.elementAt(iRow)));
               VEmpty.addElement(common.parseNull((String)VGUnit.elementAt(iRow)));
               
               
               if(common.parseNull(((String)VTaxC.elementAt(iRow))).equals("0"))
               {
                   VEmpty.addElement("Not Claimable");
               }
               else
               {
                   VEmpty.addElement("Claimable");
               }

               GrnModel.appendRow(VEmpty);
               
               
               VHsnType.addElement((String)VGHsnType.elementAt(iRow));
               VSeleId.addElement((String)VId.elementAt(iRow));
               VRateStatus.addElement((String)VGRateStatus.elementAt(iRow));
 
               GrnModel.VQtyStatus.addElement("1");
               GrnModel.VHsnType.addElement((String)VGHsnType.elementAt(iRow));
           	   GrnModel.VRateStatus.addElement("1");
               
               //GrnModel.VQtyStatus.setElementAt("1",iRow);
               
			   /*	
               int iRows = GrnModel.getRows();

	       	   dataModel.ColumnData = new Object[iRows][dataModel.ColumnType.length];

               for(int i=0;i<iRows;i++)
               {
                  Vector rowVector   = (Vector)dataModel.getCurVector(i);
		          String SHsnType    = (String)VHsnType.elementAt(i);
		          String SRateStatus = (String)VRateStatus.elementAt(i);
                  dataModel.setMaterialAmount(rowVector,SHsnType,SRateStatus,i);
               }

	           checkOrderRate();
	           */
	           
          }catch(Exception ex){
          	ex.printStackTrace();
          }
     }

	

    private void setInwardData()
    {
        VInwName   = new Vector();
        VInwNo     = new Vector();

        String QS1 = "Select InwName,InwNo From InwType Where InwNo Not in (12) Order By 2 ";

        try{
            ORAConnection   oraConnection =  ORAConnection.getORAConnection();
            Connection      theConnection =  oraConnection.getConnection();
            Statement       stat =  theConnection.createStatement();

          	ResultSet result1 = stat.executeQuery(QS1);
          
          	while(result1.next())
          	{
                VInwName.addElement(result1.getString(1));
                VInwNo.addElement(result1.getString(2));
          	}
          	result1.close();
          	stat.close();
          	
        }catch(Exception ex){
            System.out.println(ex);
        }
    }

    public int getInvTypeCode(String SName)
	{
          int iIndex = VInvTypeName.indexOf(SName);
          return common.toInt((String)VInvTypeCode.elementAt(iIndex));
	}

  	public void setInvoiceTypeVector()
    {
        VInvTypeCode      = new Vector();
        VInvTypeName      = new Vector();
              
        String QString = " Select TypeName,TypeCode From InvoiceType Order By TypeCode ";
        
        try{
           ORAConnection   oraConnection =  ORAConnection.getORAConnection();
           Connection      theConnection =  oraConnection.getConnection();
           Statement       stat =  theConnection.createStatement();
                    
           ResultSet res = stat.executeQuery(QString);
           
           while(res.next()) {
                VInvTypeName.addElement(res.getString(1));
                VInvTypeCode.addElement(res.getString(2));
           }
           res   . close();
           stat  . close();
           
        }catch(Exception ex) {
            System.out.println(ex);
            ex.printStackTrace();
        }
    }

     public String getDeptCode(int i)                       // 19
     {
         Vector VCurVector = GrnModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(23);
         int iid = VDept.indexOf(str);
         return (iid==-1 ?"0":(String)VDeptCode.elementAt(iid));
     }
     public String getGroupCode(int i)                     // 20
     {
         Vector VCurVector = GrnModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(24);
         int iid = VGroup.indexOf(str);
         return (iid==-1?"0":(String)VGroupCode.elementAt(iid));
     }
     public String getUnitCode(int i)                     //  21
     {
         Vector VCurVector = GrnModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(25);
         int iid = VUnit.indexOf(str);
         return (iid==-1?"0":(String)VUnitCode.elementAt(iid));
     }

    public void getDeptGroupUnit()
    {
        VDept	   = new Vector();
        VDeptCode  = new Vector();
        
        VGroup     = new Vector();
        VGroupCode = new Vector();

        VUnit      = new Vector();
        VUnitCode  = new Vector();

        try
        {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();

               String QS1 = "";
               String QS2 = "";
               String QS3 = "";

               QS1 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               QS2 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               QS3 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";

               ResultSet result1 = stat.executeQuery(QS1);
               while(result1.next())
               {
                    VDept.addElement(result1.getString(1));
                    VDeptCode.addElement(result1.getString(2));
               }
               result1.close();

               ResultSet result2 = stat.executeQuery(QS2);
               while(result2.next())
               {
                    VGroup.addElement(result2.getString(1));
                    VGroupCode.addElement(result2.getString(2));
               }
               result2.close();

               ResultSet result3 = stat.executeQuery(QS3);
               while(result3.next())
               {
                    VUnit.addElement(result3.getString(1));
                    VUnitCode.addElement(result3.getString(2));
               }
               result3.close();
               stat.close();
        }
        catch(Exception ex)
        {
            System.out.println("Dept,Group & Unit :"+ex);
        }
    }

    public void setVectorData()
    {
    	int iSortIndex  = JCSort.getSelectedIndex();
    	
        VQtyStatus   = new Vector();
        VGCode       = new Vector();
        VGName       = new Vector(); 
        VGBlock      = new Vector();
        VGOrderNo    = new Vector();
        VGMRSNo      = new Vector();
        VGPendQty    = new Vector();
        VGGrnQty     = new Vector();
        VGRate       = new Vector();
        VGDiscPer    = new Vector();
        VGCGSTPer    = new Vector();
        VGSGSTPer    = new Vector();
        VGIGSTPer    = new Vector();
        VGCessPer    = new Vector();
        VGDept       = new Vector();
        VGDeptCode   = new Vector(); 
        VGGroup      = new Vector();
        VGGroupCode  = new Vector(); 
        VGUnit       = new Vector();
        VGUnitCode   = new Vector();
        VId          = new Vector();
        VGBlockCode  = new Vector();
        VGOrdQty     = new Vector();
        VGrnDate     = new Vector();
        VPurOrdDate  = new Vector();
        VMrsSlNo     = new Vector();
        VOrderSlNo   = new Vector();
        VTaxC        = new Vector();
        VPQtyAllow   = new Vector();
        VMrsUserCode = new Vector();
        VApproval    = new Vector();
		VBinNo       = new Vector();
		VGHsnCode    = new Vector();
		VGHsnType    = new Vector();
		VGGrnStatus  = new Vector();
		VGRateStatus = new Vector();
		VGGstRate    = new Vector();

		VGDiscVal = new Vector();
		VGCGSTVal = new Vector();
		VGSGSTVal = new Vector();
		VGIGSTVal = new Vector();
		VGCessVal = new Vector();
		VGInvVal  = new Vector();
		VGPlus    =  new Vector();
		VGMinus   =  new Vector();

        VHsnType    = new Vector();
        VSeleId     = new Vector();
        VRateStatus = new Vector();
        
        VSupCode 		  = new Vector();
        VSupName 		  = new Vector();
        VInvNo   		  = new Vector();
        VInvDate 		  = new Vector();
        VDcNo    		  = new Vector();
        VDcDate  		  = new Vector();
        VNoofbills  	  = new Vector();
        VGstPartyTypeCode = new Vector();
		SSeleSupType   	  = "";
		SSeleStateCode    = "";

        String QS1= "";
            QS1 =   " SELECT GRN.Code, InvItems.Item_Name, OrdBlock.BlockName,  "+
                    " GRN.OrderNo, GRN.MrsNo,  "+
                    " GRN.Pending-GRN.InvQty AS Pending, "+ 
                    " GRN.InvRate, GRN.DiscPer, GRN.CGST, "+
                    " GRN.SGST, GRN.IGST, GRN.Cess, Dept.Dept_Name, "+
                    " GRN.Dept_Code, Cata.Group_Name, GRN.Group_Code, "+
                    " Unit.Unit_Name, GRN.Unit_Code, GRN.Id,   "+
                    " GRN.GrnBlock, GRN.InvQty, GRN.GrnDate,   "+
                    " GRN.Mrsslno, GRN.Slno, purchaseOrder.taxclaimable, "+
                    " InvItems.QtyAllowance,PurchaseOrder.MrsAuthUserCode,PurchaseOrder.JMDOrderApproval,InvItems.LocName,  "+
                    " decode(Grn.HsnCode,null,InvItems.HsnCode,Grn.HsnCode) as HsnCode, nvl(InvItems.HsnType,0) as HsnType,  "+
                    " decode(Grn.InvQty,0,0,1) as GrnStatus, GRN.Disc, GRN.CgstVal, GRN.SgstVal, GRN.IgstVal, GRN.CessVal, Grn.InvNet, "+
                    " grn.sup_code, "+SSupTable+".Name, grn.invno,  grn.invdate, grn.Dcno, grn.Dcdate, grn.noofbills, grn.gstpartytypecode, grn.gststatecode, "+
                    " PurchaseOrder.OrderDate, Grn.Plus, Grn.Less  FROM ((((GRN  "+
                    " INNER JOIN InvItems ON InvItems.Item_Code = GRN.Code ) "+ 
                    " INNER JOIN "+SSupTable+" ON GRN.Sup_Code  = "+SSupTable+".Ac_Code "+
                    " INNER JOIN OrdBlock ON OrdBlock.Block     = GRN.GrnBlock ) "+
                    " INNER JOIN Dept     ON Dept.Dept_code     = GRN.Dept_Code ) "+
                    " INNER JOIN Cata     ON Cata.Group_Code    = GRN.Group_Code ) "+
                    " INNER JOIN Unit     ON Unit.Unit_Code     = GRN.Unit_Code  "+
                    " Left JOIN PurchaseOrder  ON PurchaseOrder.OrderNo = GRN.OrderNo and PurchaseOrder.MrsNo = GRN.MrsNo  "+
                    " and PurchaseOrder.Item_Code = GRN.Code and GRN.OrderSlNo=PurchaseOrder.SlNo And PurchaseOrder.Authentication=1  "+
                    " And PurchaseOrder.MillCode="+iMillCode+" "+
                    " Where GRN.GRNNO = "+SGrnNo+" ";
                    //" Order By GrnStatus desc, Slno ";
                    
            if(iSortIndex==0)
            {
                 QS1 = QS1 + " Order By GrnStatus desc, 4,2 ";
            }
            if(iSortIndex==1)
            {
                 QS1 = QS1 + " Order By GrnStatus desc, 2,4 ";
            }
            if(iSortIndex==2)
            {
                 QS1 = QS1 + " Order By GrnStatus desc, 1,4 ";
            }
                    
				//--And PurchaseOrder.InvQty < PurchaseOrder.Qty  

            try{
                ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                Connection      theConnection =  oraConnection.getConnection();
                Statement       stat =  theConnection.createStatement();

                ResultSet res1 = stat.executeQuery(QS1);

                while(res1.next())
                {
                    String SCGSTPer = res1.getString(9);
                    String SSGSTPer = res1.getString(10);
                    String SIGSTPer = res1.getString(11);
					String SGstRate = common.getRound(common.toDouble(SCGSTPer)+common.toDouble(SSGSTPer)+common.toDouble(SIGSTPer),2);
					
                    VGCode      .addElement(res1.getString(1));
                    VGName      .addElement(res1.getString(2)); 
                    VGBlock     .addElement(res1.getString(3));
                    VGOrderNo   .addElement(res1.getString(4));
                    VGMRSNo     .addElement(res1.getString(5));
                    VGPendQty   .addElement(res1.getString(6));
                    VGGrnQty    .addElement(res1.getString(21));
                    VGRate      .addElement(res1.getString(7));
                    
                    VGDiscPer   .addElement(res1.getString(8));
                    VGCGSTPer   .addElement(res1.getString(9));
                    VGSGSTPer   .addElement(res1.getString(10));
                    VGIGSTPer   .addElement(res1.getString(11));
                    VGCessPer   .addElement(res1.getString(12));
                    
                    VGDept      .addElement(res1.getString(13));
                    VGDeptCode  .addElement(res1.getString(14)); 
                    VGGroup     .addElement(res1.getString(15));
                    VGGroupCode .addElement(res1.getString(16)); 
                    VGUnit      .addElement(res1.getString(17));
                    VGUnitCode  .addElement(res1.getString(18));
                    VId         .addElement(res1.getString(19));
                    VGBlockCode .addElement(res1.getString(20));
                    VGOrdQty    .addElement(res1.getString(21));
                    VGrnDate    .addElement(res1.getString(22));
                    VMrsSlNo    .addElement(res1.getString(23));
                    VOrderSlNo  .addElement(res1.getString(24));
                    VTaxC       .addElement(res1.getString(25));
                    VPQtyAllow  .addElement(res1.getString(26));
                    VMrsUserCode.addElement(res1.getString(27));
                    VApproval   .addElement(res1.getString(28));
                    VQtyStatus  .addElement("0");
                    VBinNo	    .addElement(common.parseNull(res1.getString(29)));
                    VGHsnCode   .addElement(common.parseNull(res1.getString(30)));
                    VGHsnType   .addElement(common.parseNull(res1.getString(31)));
                    VGGrnStatus .addElement(common.parseNull(res1.getString(32)));
                    VGRateStatus.addElement("0");
                    VGGstRate   .addElement(SGstRate);
                    
                    VGDiscVal.addElement(common.parseNull(res1.getString(33)));
                    VGCGSTVal.addElement(common.parseNull(res1.getString(34)));
                    VGSGSTVal.addElement(common.parseNull(res1.getString(35)));
                    VGIGSTVal.addElement(common.parseNull(res1.getString(36)));
                    VGCessVal.addElement(common.parseNull(res1.getString(37)));
                    VGInvVal .addElement(common.parseNull(res1.getString(38)));
                    
					VSupCode 		  .addElement(common.parseNull(res1.getString(39)));
					VSupName 		  .addElement(common.parseNull(res1.getString(40)));
					VInvNo   		  .addElement(common.parseNull(res1.getString(41)));
					VInvDate 		  .addElement(common.parseNull(res1.getString(42)));
					VDcNo    		  .addElement(common.parseNull(res1.getString(43)));
					VDcDate  		  .addElement(common.parseNull(res1.getString(44)));
					VNoofbills  	  .addElement(common.parseNull(res1.getString(45)));
					VGstPartyTypeCode .addElement(common.parseNull(res1.getString(46)));
					
					SSeleSupType   = common.parseNull(res1.getString(46));
					SSeleStateCode = common.parseNull(res1.getString(47));
					VPurOrdDate    . addElement(common.parseNull(res1.getString(48)));
					VGPlus         . addElement(common.parseNull(res1.getString(49)));
					VGMinus        . addElement(common.parseNull(res1.getString(50)));
               }
               res1.close();
               stat.close();
                   
        }catch(Exception ex){
            System.out.println("E1"+ex );
        }
    }


     public void insertGRNDetails(int i)
     {
		 					
          String QString = " Insert Into GRN (OrderNo,MRSNo,GrnNo,GrnDate, GrnBlock, Sup_Code, GateInNo, GateInDate, InvNo, InvDate, DcNo, DcDate, Code, InvQty, MillQty, Pending, OrderQty, Qty, InvRate, DiscPer, Disc, CGST, CGSTVal, SGST, SGSTVal, IGST, IGSTVal, Cess, CessVal, InvAmount, InvNet, plus, Less, Misc, Dept_Code, Group_Code, Unit_Code, InvSlNo, ActualModVat, NoOfBills, MillCode, id, slno, mrsslno, mrsauthusercode, orderslno, orderapprovalstatus, taxclaimable, usercode, creationdate, entrystatus, grnqty, grnvalue, HsnCode, GstRate, GstPartyTypeCode, GstStateCode, BinNo, InvoiceType) Values ( ";
          
          String QSL     = " Select nvl(InvSlNo,0),nvl(max(slno),0) From GRN Where GrnNo="+(String)LGrnNo.getText()+" and GrnDate="+common.pureDate((String)TDate.getText())+" and SPJNo=0 and NoofSPJ=0 group by nvl(InvSlNo,0) ";
          
          String QS="";
          int iInvSlNo=0, iSlNo =  0;
	      int iInvoiceTypeCode  = -1;
			
		  System.out.println("insertGRNDetails=="+i+"=>"+QSL);
			
          try
          {
               Statement       stat =  theMConnection.createStatement();
               ResultSet res        = stat.executeQuery(QSL);
               while(res.next())
               {
                    iInvSlNo = res.getInt(1);
                    iSlNo    = res.getInt(2);
               }
               res.close();
               //iInvSlNo++;
			   System.out.println("insertGRNDetails=="+i+"=iInvSlNo=>"+iInvSlNo);
		               
               //Object RowData[][] = GrnModel.getCurVector(i);
               Object RowData[][] = GrnModel.getFromVector();
               //Object GrnData[][] = MiddlePanel.GrnPanel.getFromVector();
               
               String SAdd        = TAdd.getText();
               String SLess       = TLess.getText();
               double dOthers     = common.toDouble(LOthers.getText());
               
               double dpm         = common.toDouble(SAdd)-common.toDouble(SLess)+dOthers;

               double dNet       = common.toDouble(LNet.getText())-dpm;
               double dRatio     = dpm/dNet;
               
               String SInvNo       = common.parseNull(TInvNo.getText().trim());
               String SInvDate     = TInvDate.toNormal();
               String SDCNo        = common.parseNull(TDCNo.getText().trim());
               String SDCDate      = TDCDate.toNormal();

               String SSupCode = TSupCode.getText();
               String SDate    = common.pureDate((String)TDate.getText()); //TDate.toNormal();

               String SDateTime = common.getServerDateTime();

               String SEntryStatus = "1";

               //for(int i=0;i<RowData.length;i++) {
               
                    String SSeleId = (String)VSeleId.elementAt(i);
	System.out.println("insertGRNDetails=="+i+"=SSeleId=>"+SSeleId);
                    int iMidRow       = i;
                    //int iMidRow       = getRowIndex(SSeleId,GrnData);

		    String SHsnType   = (String)VHsnType.elementAt(i);

                    String SBlockCode     = "";
                    String SOrdQty        = "";
                    String SMrsSLNo       = "";
                    String SMrsUserCode   = "";
                    String SOrderSLNo     = "";
                    String STaxC          = "";
                    String SOrderApproval = "";
		    String SBinNo         = "";
	System.out.println("insertGRNDetails=="+i+"=SHsnType=>"+SHsnType);
		    if(SHsnType.equals("1"))
		    {
		            SBlockCode     = "0";
		            SOrdQty        = "0";
		            SMrsSLNo       = "0";
		            SMrsUserCode   = "0";
		            SOrderSLNo     = "0";
		            STaxC          = "0";
		            SOrderApproval = "0";
			    	SBinNo         = "0";
		    }
		    else
		    {		
		    		/*
		            SBlockCode     = getBlockCode(iMidRow);
		            SOrdQty        = getOrdQty(iMidRow);
		            SMrsSLNo       = getMrsSLNo(iMidRow);
		            SMrsUserCode   = getMrsUserCode(iMidRow);
		            SOrderSLNo     = getOrderSLNo(iMidRow);
		            STaxC          = getTaxC(iMidRow);
		            SOrderApproval = getOrderApproval(iMidRow);
             	    SBinNo         = (String)GrnModel.getValueAt(iMidRow,9);
             	    */
	         }


                    String SUnitCode  = getUnitCode(i);
                    String SDeptCode  = getDeptCode(i);
                    String SGroupCode = getGroupCode(i);
                    String sInvTypeName	= (String)cmbInvoiceType.getSelectedItem();
					iInvoiceTypeCode    = getInvTypeCode(sInvTypeName);
             
                    iSlNo++;
                    
                    String SOrderNo     = (String)RowData[i][5];
                    String SMRSNo       = (String)RowData[i][4];
                    
                    String  SItemCode  = (String)RowData[i][0];
                    String  SHsnCode   = ((String)RowData[i][2]).trim();
                    
                    String  SBasic     = (String)RowData[i][16];
                    String  SInvAmount = (String)RowData[i][22];

		    String SMisc      = "0";

		    if(SHsnType.equals("0"))
		    {
             	   SMisc = common.getRound(common.toDouble(SInvAmount)*dRatio,3);
		    }

                    String  SMillQty   = (String)RowData[i][9];
                    
                    String SGrnQty   = SMillQty;
                    String SGrnValue = "";
                    
                    SGrnValue = common.getRound(common.toDouble(SInvAmount) + common.toDouble(SMisc),2);

		    String SCGSTPer   = common.getRound(common.toDouble(((String)RowData[i][12]).trim()),2);
		    String SSGSTPer   = common.getRound(common.toDouble(((String)RowData[i][13]).trim()),2);
		    String SIGSTPer   = common.getRound(common.toDouble(((String)RowData[i][14]).trim()),2);
		    String SGstRate   = common.getRound(common.toDouble(SCGSTPer)+common.toDouble(SSGSTPer)+common.toDouble(SIGSTPer),2);
                    
                    String    QS1 = QString;
                              QS1 = QS1+"0"+SOrderNo+",";
                              QS1 = QS1+"0"+SMRSNo+",";
                              QS1 = QS1+"0"+SGrnNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"0"+SBlockCode+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"0"+SGateNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"'"+SInvNo+"',";
                              QS1 = QS1+"'"+SInvDate+"',";
                              QS1 = QS1+"'"+SDCNo+"',";
                              QS1 = QS1+"'"+SDCDate+"',";
                              QS1 = QS1+"'"+SItemCode+"',";
                              QS1 = QS1+"0"+(String)RowData[i][8]+",";
                              QS1 = QS1+"0"+SMillQty+",";
                              QS1 = QS1+"0"+(String)RowData[i][7]+",";
                              QS1 = QS1+"0"+SOrdQty+",";
                              QS1 = QS1+"0"+SMillQty+",";
                              QS1 = QS1+"0"+(String)RowData[i][10]+",";
                              QS1 = QS1+"0"+(String)RowData[i][11]+",";
                              QS1 = QS1+"0"+(String)RowData[i][17]+",";
                              QS1 = QS1+"0"+SCGSTPer+",";
                              QS1 = QS1+"0"+(String)RowData[i][18]+",";
                              QS1 = QS1+"0"+SSGSTPer+",";
                              QS1 = QS1+"0"+(String)RowData[i][19]+",";
                              QS1 = QS1+"0"+SIGSTPer+",";
                              QS1 = QS1+"0"+(String)RowData[i][20]+",";
                              QS1 = QS1+"0"+(String)RowData[i][15]+",";
                              QS1 = QS1+"0"+(String)RowData[i][21]+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SAdd+",";
                              QS1 = QS1+"0"+SLess+",";
                              QS1 = QS1+"0"+SMisc+",";
                              QS1 = QS1+"0"+SDeptCode+",";
                              QS1 = QS1+"0"+SGroupCode+",";
                              QS1 = QS1+"0"+SUnitCode+",";
                              QS1 = QS1+"0"+iInvSlNo+",";
                              QS1 = QS1+"0"+0+",";
                              QS1 = QS1+"0"+TInvoice.getText()+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"grn_seq.nextval"+",";
                              QS1 = QS1+"0"+iSlNo+",";
                              QS1 = QS1+"0"+SMrsSLNo+",";
                              QS1 = QS1+"0"+SMrsUserCode+",";
                              QS1 = QS1+"0"+SOrderSLNo+",";
                              QS1 = QS1+"0"+SOrderApproval+",";
                              QS1 = QS1+"0"+STaxC+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+SDateTime+"',";
                              QS1 = QS1+"0"+SEntryStatus+",";
                              QS1 = QS1+"0"+SGrnQty+",";
                              QS1 = QS1+"0"+SGrnValue+",";
                    	      QS1 = QS1+"'"+SHsnCode+"',";
                    	      QS1 = QS1+"0"+SGstRate+",";
                    	      QS1 = QS1+"0"+SSeleSupType+",";
                    	      QS1 = QS1+"'"+SSeleStateCode+"',";
							  QS1 = QS1+"'"+SBinNo+"',";
							  QS1 = QS1+"0"+iInvoiceTypeCode+ ")";

					System.out.println("SPJNo==0 > insert GRN QS1==>"+QS1);	
					
                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.executeUpdate(QS1);

              // }
               stat.close();
		 

          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }


     private int getRowIndex(String SId,Object RowData[][])
     {
          int iIndex=-1;
          
          for(int i=0;i<RowData.length;i++)
          {
               String SSeleId = (String)VSeleId.elementAt(i);
               
               if(!SSeleId.equals(SId))
                    continue;
               
               iIndex=i;
               return iIndex;
          }
          return iIndex;
     }



}

