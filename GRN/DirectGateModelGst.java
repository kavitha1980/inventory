package GRN;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGateModelGst extends DefaultTableModel
{
        JLabel lblTotalInvoice,lblNetAmount;  
        Object RData[][],CNames[],CType[];
        Common common = new Common();
        
        Vector              VHSNCode,VGSTRate,VCGSTPer,VSGSTPer,VIGSTPer;
        int                 iGSTStateCode = 0;        
        // set DirectGate Model  Index
        int ItemName=0,ItemCode=1,HSNCode=2,Qty=3,Rate=4,Value=5,DiscPer=6,Discount=7,BillValue=8,CGRatePer=9,CGValue=10,SGRatePer=11,SGValue=12,IGRatePer=13,IGValue=14,TotalValue=15,Select=16;   
        //int ItemName=0,ItemCode=1,HSNCode=2,Qty=3,Rate=4,Value=5,CGRatePer=6,CGValue=7,SGRatePer=8,SGValue=9,IGRatePer=10,IGValue=11,TotalValue=12,Select=13;   

        public DirectGateModelGst(Object[][] RData, Object[] CNames,Object[] CType,JLabel lblTotalInvoice,JLabel lblNetAmount,int iGSTStateCode)
        {
            super(RData,CNames);
            this.RData       = RData;
            this.CNames      = CNames;
            this.CType       = CType;
            this.lblTotalInvoice    = lblTotalInvoice;
            this.lblNetAmount       = lblNetAmount;
            this.iGSTStateCode      = iGSTStateCode;
            setHSNVector();
            for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
            }
        }
        public Class getColumnClass(int col)
        { 
            return getValueAt(0,col).getClass(); 
            
        }           
       
        public boolean isCellEditable(int row,int col)
        {
                if(CType[col]=="B" || CType[col]=="E")
                    return true;
                return false;
        }

        public Object[][] getFromVector()
        {
            Object FinalData[][] = new Object[super.dataVector.size()][CNames.length];
            for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
                for(int j=0;j<curVector.size();j++)
                {
                    if(j==Select)
                    {
                      FinalData[i][j] = (Boolean)curVector.elementAt(j);
                    }
                    else
                    {
                        FinalData[i][j] = (String.valueOf(curVector.elementAt(j)).trim());
                      //FinalData[i][j] = ((String)curVector.elementAt(j)).trim();
                    }
                }
            }
            return FinalData;
        }
        public int getRows()
        {
            return super.dataVector.size();
        }
        public Vector getCurVector(int i)
        {
            return (Vector)super.dataVector.elementAt(i);
        }
        public void setValueAt(Object aValue, int row, int column)
        {
            try
            {
                Vector rowVector = (Vector)super.dataVector.elementAt(row);
                rowVector.setElementAt(aValue, column);
                fireTableChanged(new TableModelEvent(this, row, row, column,0));
                if(column   ==  Rate)
                {
                    setGSTValues(row);
                }
                if(column   ==  ItemName)
                {
                    setHSNDetails(row);
                }
                if(column   ==  DiscPer)
                {
                    setDiscGSTValues(row);
                }  
                setTotalValue();
            }

            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
                ex.printStackTrace();
            }
        }     

        public void setGSTValues(int iRow)
        {

            double dCGSTValue   =   0.0;
            double dSGSTValue   =   0.0;
            double dIGSTValue   =   0.0;
            double dTotalValue  =   0.0;

            double dRate = common.toDouble(String.valueOf(getValueAt(iRow,Rate)));
            double dQty  = common.toDouble(String.valueOf(getValueAt(iRow,Qty)));
            double dValue= dRate*dQty;
            double dCGSTPer =   common.toDouble(String.valueOf(getValueAt(iRow,CGRatePer)));
            double dSGSTPer =   common.toDouble(String.valueOf(getValueAt(iRow,SGRatePer)));
            double dIGSTPer =   common.toDouble(String.valueOf(getValueAt(iRow,IGRatePer)));
            if(iGSTStateCode==33)
            {
                dCGSTValue = dValue*dCGSTPer/100;
                dSGSTValue = dValue*dSGSTPer/100;
                dIGSTPer   = 0;
                dIGSTValue = 0;
            }
            else
            {
                dIGSTValue = dValue*dIGSTPer/100;
            }
            if(iGSTStateCode==33)
            {
                dTotalValue = dValue+dCGSTValue+dSGSTValue;
            }
            else
            {
                dTotalValue = dValue+dIGSTValue;
            }

            setValueAt(common.toDouble(common.getRound(dValue,1)),iRow,Value) ;
            setValueAt(common.toDouble(common.getRound(dCGSTValue,1)),iRow,CGValue) ;
            setValueAt(common.toDouble(common.getRound(dSGSTValue,1)),iRow,SGValue) ;
            setValueAt(common.toDouble(common.getRound(dIGSTValue,1)),iRow,IGValue) ;
            setValueAt(common.toDouble(common.getRound(dTotalValue,1)),iRow,TotalValue) ;        

        }
        public void setDiscGSTValues(int iRow)
        {
            double dCGSTValue   =   0.0;
            double dSGSTValue   =   0.0;
            double dIGSTValue   =   0.0;
            double dTotalValue  =   0.0;

            double dRate = common.toDouble(String.valueOf(getValueAt(iRow,Rate)));
            double dQty  = common.toDouble(String.valueOf(getValueAt(iRow,Qty)));
            double dValue= dRate*dQty;
            double dDiscPer     =   common.toDouble(String.valueOf(getValueAt(iRow,DiscPer)));
            double dDiscValue   =   dValue*dDiscPer/100;
            double dBillValue   =   dValue-dDiscValue;
            double dCGSTPer     =   common.toDouble(String.valueOf(getValueAt(iRow,CGRatePer)));
            double dSGSTPer     =   common.toDouble(String.valueOf(getValueAt(iRow,SGRatePer)));
            double dIGSTPer     =   common.toDouble(String.valueOf(getValueAt(iRow,IGRatePer)));
            if(iGSTStateCode==33)
            {
                dCGSTValue = dBillValue*dCGSTPer/100;
                dSGSTValue = dBillValue*dSGSTPer/100;
                dIGSTPer   = 0;
                dIGSTValue = 0;
            }
            else
            {
                dIGSTValue = dBillValue*dIGSTPer/100;
            }
            if(iGSTStateCode==33)
            {
                dTotalValue = dBillValue+dCGSTValue+dSGSTValue;
            }
            else
            {
                dTotalValue = dBillValue+dIGSTValue;
            }

            setValueAt(common.toDouble(common.getRound(dValue,1)),iRow,Value) ;
            setValueAt(common.toDouble(common.getRound(dDiscValue,1)),iRow,Discount) ;
            setValueAt(common.toDouble(common.getRound(dBillValue,1)),iRow,BillValue) ;
            setValueAt(common.toDouble(common.getRound(dCGSTValue,1)),iRow,CGValue) ;
            setValueAt(common.toDouble(common.getRound(dSGSTValue,1)),iRow,SGValue) ;
            setValueAt(common.toDouble(common.getRound(dIGSTValue,1)),iRow,IGValue) ;
            setValueAt(common.toDouble(common.getRound(dTotalValue,1)),iRow,TotalValue) ;        

        }        
        private void setHSNDetails(int iRow)
        {
            int iHSNIndex   = -1;
            String SHSNCode = String.valueOf(getValueAt(iRow,HSNCode));

            iHSNIndex       = VHSNCode.indexOf(SHSNCode);

            if(iHSNIndex==-1)
                return;

            double dValue        =   common.toDouble(String.valueOf(getValueAt(iRow,Value)));
            double dCGSTPer      =   common.toDouble(String.valueOf(VCGSTPer.elementAt(iHSNIndex)));
            double dCGSTValue    =   dValue*dCGSTPer/100;
            double dSGSTPer      =   common.toDouble(String.valueOf(VSGSTPer.elementAt(iHSNIndex)));
            double dSGSTValue    =   dValue*dSGSTPer/100;
            double dIGSTPer      =   common.toDouble(String.valueOf(VIGSTPer.elementAt(iHSNIndex)));
            double dIGSTValue    =   dValue*dIGSTPer/100;
            double dTotalValue   =   dValue+dCGSTValue+dSGSTValue+dIGSTValue;

            setValueAt(common.toDouble(common.getRound(dCGSTPer,1)),iRow,CGRatePer) ;
            setValueAt(common.toDouble(common.getRound(dCGSTValue,1)),iRow,CGValue) ;
            setValueAt(common.toDouble(common.getRound(dSGSTPer,1)),iRow,SGRatePer) ;
            setValueAt(common.toDouble(common.getRound(dSGSTValue,1)),iRow,SGValue) ;
            setValueAt(common.toDouble(common.getRound(dIGSTPer,1)),iRow,IGRatePer) ;
            setValueAt(common.toDouble(common.getRound(dIGSTValue,1)),iRow,IGValue) ;
            setValueAt(common.toDouble(common.getRound(dTotalValue,1)),iRow,TotalValue) ;

        }
        private void setTotalValue()
        {
            double dTotal = 0.0;
            for(int i=0;i<getRowCount();i++)
            {
                dTotal += common.toDouble(String.valueOf(getValueAt(i,TotalValue)));
            }
            lblTotalInvoice . setText(common.getRound(dTotal,2));
            lblNetAmount    . setText(common.getRound(dTotal,2));
        }
        private void setHSNVector()
        {
            VHSNCode =  new Vector();
            VGSTRate =  new Vector();
            VCGSTPer =  new Vector();
            VSGSTPer =  new Vector();
            VIGSTPer =  new Vector();
            try
            {
                ORAConnection connect   =   ORAConnection.getORAConnection();
                Connection theConnection=   connect.getConnection();

                PreparedStatement theStatement  =   theConnection.prepareStatement("Select HSNCode,GSTRate,CGST,SGST,IGST From HSNGSTRate");
                ResultSet           theResult   =   theStatement.executeQuery();
                while(theResult.next())
                {
                    VHSNCode    .   addElement(theResult.getString(1));
                    VGSTRate    .   addElement(theResult.getString(2));
                    VCGSTPer    .   addElement(theResult.getString(3));
                    VSGSTPer    .   addElement(theResult.getString(4));                
                    VIGSTPer    .   addElement(theResult.getString(5));                
                }
                theResult   .   close();
                theStatement.   close();
            }
            catch(Exception ex)
            {
                System.out.println(" setHSNVector "+ex);
            }
        }
}
