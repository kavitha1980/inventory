package GRN;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGatePanel extends JPanel
{
   JTable               GateTable;
   DirectGateModel      GateModel;

   JPanel         GridPanel,BottomPanel,ControlPanel;
   JPanel         GridBottom;

   JButton        BAdd,BDelete;

   JLayeredPane   DeskTop;
   Object         GData[][];
   String         CGData[],CGType[];
   int            iMillCode;
   String         SItemTable;

   Common common = new Common();

    DirectGatePanel(JLayeredPane DeskTop,Object GData[][],String CGData[],String CGType[],int iMillCode,String SItemTable)
    {
         this.DeskTop      = DeskTop;
         this.GData        = GData;
         this.CGData       = CGData;
         this.CGType       = CGType;
	 this.iMillCode    = iMillCode;
	 this.SItemTable   = SItemTable;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
         setReportTable();
    }
    public void createComponents()
    {
         BAdd    = new JButton("Add");
         BDelete = new JButton("Delete");

         GridPanel        = new JPanel(true);
         GridBottom       = new JPanel(true);
         BottomPanel      = new JPanel();
         ControlPanel     = new JPanel();
     }
          
     public void setLayouts()
     {
         GridPanel.setLayout(new GridLayout(1,1));
         BottomPanel.setLayout(new BorderLayout());
         ControlPanel   . setLayout(new FlowLayout());
         GridBottom.setLayout(new BorderLayout());         
     }
     public void addComponents()
     {
         ControlPanel.add(new JLabel("F3 - Select Item"));
         ControlPanel.add(BAdd);
         ControlPanel.add(BDelete);

         BottomPanel.add("North",ControlPanel);
     }
     public void addListeners()
     {
         BAdd. addActionListener(new ActList());
         BDelete. addActionListener(new ActList());
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BAdd)
                     doAdd();
               if(ae.getSource()==BDelete)
                     doDelete();
          }
     }

     public void doAdd()
     {
          Vector VEmptyData = new Vector();
          for(int i=0;i<CGData.length;i++)
                VEmptyData.addElement("");
          GateModel.addRow(VEmptyData);
          GateModel.setColumnData();
     }
     public void doDelete()
     {
          try
          {
              int i = GateTable.getSelectedRow();
              GateModel.removeRow(i);
	      GateModel.setColumnData();
          }
          catch(Exception ex){}
     }

     public void setReportTable()
     {
         GateModel     = new DirectGateModel(GData,CGData,CGType);       
         GateTable     = new JTable(GateModel);
         //GateTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<GateTable.getColumnCount();col++)
         {
               if(CGType[col]=="N" || CGType[col]=="B")
                    GateTable.getColumn(CGData[col]).setCellRenderer(cellRenderer);
         }
         //GateTable.setShowGrid(false);

         GateModel.removeRow(0);

         setLayout(new BorderLayout());
         GridBottom.add(GateTable.getTableHeader(),BorderLayout.NORTH);
         GridBottom.add(new JScrollPane(GateTable),BorderLayout.CENTER);

         GridPanel.add(GridBottom);

         add(BottomPanel,BorderLayout.SOUTH);
         add(GridPanel,BorderLayout.CENTER);

         GateTable.addKeyListener(new KeyList());
     }

     public class KeyList extends KeyAdapter
     {
               public void keyPressed(KeyEvent ke)
               {
                    if (ke.getKeyCode()==KeyEvent.VK_F3)
                    {
			 int iSeleRow = GateTable.getSelectedRow();
			 ItemSearchList itemsearchlist = new ItemSearchList(iSeleRow,GateModel,iMillCode,SItemTable);	
                    }
               }
     }

     public Object[][] getFromVector()
     {
          return GateModel.getFromVector();     
     }


}

