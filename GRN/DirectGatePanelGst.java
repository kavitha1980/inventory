package GRN;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGatePanelGst extends JPanel
{
   JTable               GateTable;
   DirectGateModelGst   GateModelGst;

   JPanel         GridPanel,BottomPanel,ControlPanel;
   JPanel         GridBottom;

   JButton        BAdd,BDelete;
   JTextField     txtRoundOff;
   JLabel         lblTotalInvoice,lblNetAmount;
   JLayeredPane   DeskTop;
   Object         GGstData[][];
   String         CGGstData[],CGGstType[];
   Vector         VPartyCode, VPartyName, VItemCode, VItemName,VItemHSNCode;
   Vector         VHSNCode, VGSTRate, VCGSTPer, VSGSTPer, VIGSTPer,VGSTPartyTypeCode,VGSTPartyType,VTaxCatCode;
   int            iMaxLimit,iHsnType,iGstTypeCode,iClaimStatus;
   String         SSupCode,SSeleSupType,SSeleStateCode;
   Common common = new Common();

   
     // set DirectGate Model  Index
     int ItemName=0,ItemCode=1,HSNCode=2,Qty=3,Rate=4,Value=5,DiscPer=6,Discount=7,BillValue=8,CGRatePer=9,CGValue=10,SGRatePer=11,SGValue=12,IGRatePer=13,IGValue=14,TotalValue=15,Select=16;   
     
    DirectGatePanelGst(JLayeredPane DeskTop,Object GGstData[][],String CGGstData[],String CGGstType[],String SSupCode,String SSeleSupType,String SSeleStateCode)
    {
         this.DeskTop      = DeskTop;
         this.GGstData     = GGstData;
         this.CGGstData      = CGGstData;
         this.CGGstType      = CGGstType;
         this.SSupCode       = SSupCode;
         this.SSeleSupType   = SSeleSupType;
         this.SSeleStateCode = SSeleStateCode;
         setHSNVector();
         createComponents();
         setLayouts();
         addComponents();
         setReportTable();
         addListeners();
    }
    DirectGatePanelGst(JLayeredPane DeskTop,Object GGstData[][],String CGGstData[],String CGGstType[],String SSupCode,String SSeleSupType,String SSeleStateCode,Vector VItemCode,Vector VItemName)
    {
         this.DeskTop      = DeskTop;
         this.GGstData     = GGstData;
         this.CGGstData      = CGGstData;
         this.CGGstType      = CGGstType;
         this.SSupCode       = SSupCode;
         this.SSeleSupType   = SSeleSupType;
         this.SSeleStateCode = SSeleStateCode;
         this.VItemCode      = VItemCode;
         this.VItemName      = VItemName;

         setHSNVector();
         createComponents();
         setLayouts();
         addComponents();
         setReportTable();
         addListeners();
    }

    public void createComponents()
    {
         BAdd    = new JButton("Add");
         BDelete = new JButton("Delete");
         txtRoundOff      = new JTextField(8);
         lblTotalInvoice  = new JLabel("");
         lblTotalInvoice  . setText("0.0");
         lblNetAmount     = new JLabel("");
         GridPanel        = new JPanel(true);
         GridBottom       = new JPanel(true);
         BottomPanel      = new JPanel();
         ControlPanel     = new JPanel();
     }
          
     public void setLayouts()
     {
         GridPanel.setLayout(new GridLayout(1,1));
         BottomPanel.setLayout(new BorderLayout());
         ControlPanel   . setLayout(new FlowLayout());
         GridBottom.setLayout(new BorderLayout());         
     }
     public void addComponents()
     {
         ControlPanel   . add(new JLabel("For ItemSelection-->Double Click in ItemName"));
         ControlPanel   . add(BAdd);
         ControlPanel   . add(BDelete);
         ControlPanel   . add(new JLabel(""));
         ControlPanel   . add(new JLabel(" Total  "));
         ControlPanel   . add(lblTotalInvoice);
         ControlPanel   . add(new JLabel(""));
         ControlPanel   . add(new JLabel("  RoundOff "));
         ControlPanel   . add(txtRoundOff);
         ControlPanel   . add(new JLabel("  Net "));
         ControlPanel   . add(lblNetAmount);
         BottomPanel.add("North",ControlPanel);
     }
     public void addListeners()
     {
         BAdd   . addActionListener(new ActList());
         BDelete. addActionListener(new ActList());
         GateTable    .   addMouseListener(new MouseList());
         txtRoundOff  .   addFocusListener(new FocusList());
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BAdd)
                     doAdd();
               if(ae.getSource()==BDelete)
                     doDelete();
          }
     }
    private class MouseList extends MouseAdapter
    {
       public void mouseClicked(MouseEvent me)
       {
            int iRow = GateTable.getSelectedRow();
            int iCol = GateTable.getSelectedColumn();
             if(me.getClickCount()==2)
             {
                if (iCol == ItemName) 
                {
                     InvItemSearch itemsearch = new InvItemSearch(GateModelGst,iRow,iCol,VItemCode,VItemName,iHsnType,iGstTypeCode,iClaimStatus);
                     itemsearch      .   setVisible(true);
                }
             }
       }
    }
    private class FocusList extends FocusAdapter
    {
        public void focusLost(FocusEvent fe)
        {
            if(fe.getSource() == txtRoundOff)
            {
                double dTotal      = common.toDouble(lblTotalInvoice.getText());
                double dRoundOff   = common.toDouble(txtRoundOff.getText());
                lblNetAmount       . setText(common.getRound(dTotal+(dRoundOff),2));
                
            }
        }
    }
   public void doAdd()
   {
       txtRoundOff . setText("");
    try
    {
          Vector VEmptyData = new Vector();
          
         VEmptyData.addElement("");
         VEmptyData.addElement("");
         VEmptyData.addElement("");
         VEmptyData.addElement("");
         VEmptyData.addElement("");
         VEmptyData.addElement(0.0);
         VEmptyData.addElement("");
         VEmptyData.addElement(0.0);
         VEmptyData.addElement(0.0);
         VEmptyData.addElement(0.0);  
         VEmptyData.addElement(0.0);
         VEmptyData.addElement(0.0);  
         VEmptyData.addElement(0.0);
         VEmptyData.addElement(0.0);  
         VEmptyData.addElement(0.0);  
         VEmptyData.addElement(0.0);
/*          for(int i=0;i<CGData.length-1;i++)
          {
            VEmptyData.addElement("");
          }*/
          
          VEmptyData.addElement(new Boolean(false));
          GateModelGst.addRow(VEmptyData);
   }catch(Exception ex){ex.printStackTrace();}
     }
     public void doDelete()
     {
          try
          {
              int i = GateTable.getSelectedRow();
              GateModelGst.removeRow(i);
          }
          catch(Exception ex){}
     }

     public void setReportTable()
     {
         int iGSTStateCode = common.toInt(SSeleStateCode);
         GateModelGst     = new DirectGateModelGst(GGstData,CGGstData,CGGstType,lblTotalInvoice,lblNetAmount,iGSTStateCode);       
         GateTable     = new JTable();
         //GateTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

            GateTable.setColumnModel(new GroupableTableColumnModel());
            GateTable.setTableHeader(new GroupableTableHeader((GroupableTableColumnModel) GateTable.getColumnModel()));
            GateTable.setModel(GateModelGst);
            // Setup Column Groups
            GroupableTableColumnModel cm = (GroupableTableColumnModel) GateTable.getColumnModel();
            ColumnGroup g_CGST = new ColumnGroup("CGST");
            g_CGST.add(cm.getColumn(CGRatePer));
            g_CGST.add(cm.getColumn(CGValue));
            cm.addColumnGroup(g_CGST);

            ColumnGroup g_SGST = new ColumnGroup("SGST");
            g_SGST.add(cm.getColumn(SGRatePer));
            g_SGST.add(cm.getColumn(SGValue));
            cm.addColumnGroup(g_SGST);

            ColumnGroup g_IGST = new ColumnGroup("IGST");
            g_IGST.add(cm.getColumn(IGRatePer));
            g_IGST.add(cm.getColumn(IGValue));
            cm.addColumnGroup(g_IGST);

            GateTable.setCellSelectionEnabled(true);
         
		     DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
		     cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

		     for (int col=0;col<GateTable.getColumnCount();col++)
		     {
		           if(CGGstType[col]=="N" || CGGstType[col]=="B")
		                GateTable.getColumn(CGGstData[col]).setCellRenderer(cellRenderer);
		     }
		     //GateTable.setShowGrid(false);

		     GateModelGst.removeRow(0);

		     setLayout(new BorderLayout());
		     GridBottom.add(GateTable.getTableHeader(),BorderLayout.NORTH);
		     GridBottom.add(new JScrollPane(GateTable),BorderLayout.CENTER);

		     GridPanel.add(GridBottom);

		     add(BottomPanel,BorderLayout.SOUTH);
		     add(GridPanel,BorderLayout.CENTER);
     }
     public Object[][] getFromVector()
     {
          return GateModelGst.getFromVector();     
     }

    private void setHSNVector() 
    {
        iMaxLimit= 0;
        VHSNCode = new Vector();
        VGSTRate = new Vector();
        VCGSTPer = new Vector();
        VSGSTPer = new Vector();
        VIGSTPer = new Vector();
        
      /*  VItemCode= new Vector();
        VItemName= new Vector();
        VItemHSNCode = new Vector();
        
        VPartyCode   = new Vector();
        VPartyName   = new Vector();*/

        VGSTPartyTypeCode = new Vector();
        VGSTPartyType     = new Vector();
        VTaxCatCode       = new Vector();
        try 
        {
            ORAConnection connect    = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();

            PreparedStatement theStatement = theConnection.prepareStatement("Select HSNCode,GSTRate,CGST,SGST,IGST,TaxCatCode From HSNGSTRate");
            ResultSet theResult = theStatement.executeQuery();
            while (theResult.next()) 
            {
                VHSNCode    .   addElement(theResult.getString(1));
                VGSTRate    .   addElement(theResult.getString(2));
                VCGSTPer    .   addElement(theResult.getString(3));
                VSGSTPer    .   addElement(theResult.getString(4));
                VIGSTPer    .   addElement(theResult.getString(5));
                VTaxCatCode .   addElement(theResult.getString(6));
            }
            theResult       .   close();
            theStatement    .   close();

            
/*            theStatement    = theConnection.prepareStatement("Select Item_Code,Item_Name,HSNCode From InvItems Order By Item_Name");
            theResult       = theStatement.executeQuery();
            System.out.println(" Bef TIme 2 "+common.getServerDateTime());
            while(theResult.next())
            {
                    VItemCode   .   addElement(theResult.getString(1));
                    VItemName   .   addElement(theResult.getString(2));
                    VItemHSNCode    .   addElement(common.parseNull(theResult.getString(3))); 
            }
            System.out.println(" Af TIme 2 "+common.getServerDateTime());
            theResult       .   close();            
            theStatement    .   close();
            
            theStatement    = theConnection.prepareStatement("Select PartyCode,PartyName From PartyMaster Order By PartyName");
            theResult       = theStatement.executeQuery();
            System.out.println(" Bef TIme 3 "+common.getServerDateTime());
            while(theResult.next())
            {
                    VPartyCode   .   addElement(theResult.getString(1));
                    VPartyName   .   addElement(theResult.getString(2));
            }
            System.out.println(" Af TIme 3 "+common.getServerDateTime());
            theResult       .   close();            
            theStatement    .   close();            */

            ORAConnection3 sconnect    = ORAConnection3.getORAConnection();
            Connection theConnect      = sconnect.getConnection();

            StringBuffer sb			   = new StringBuffer();
            sb.append("	Select Code,Name From GSTPartyType Order By Code");

            PreparedStatement thestate = theConnect.prepareStatement(sb.toString());
            ResultSet rst  	   		   = thestate.executeQuery();
            while(rst.next())
            {
                    VGSTPartyTypeCode   .   addElement(rst.getString(1));
                    VGSTPartyType       .   addElement(rst.getString(2));
            }
            rst       .   close();            
            thestate  .   close();   

            JDBCConnection1 alpaconnect     =   JDBCConnection1 . getJDBCConnection();
            Connection alpaConnection       =   alpaconnect     . getConnection();
         
            PreparedStatement stat = alpaConnection.prepareStatement("Select Max_Limit from ReverseCharge");
            ResultSet res  	   	   = stat.executeQuery();
            while(res.next())
            {
                iMaxLimit	=	res.getInt(1);
            }
            res	.	close();
            stat.   close();
        } 
        catch (Exception ex) 
        {
            System.out.println(" setHSNVector " + ex);
        }
    }
    private boolean isAllItemsSelected()
    {
        for(int i=0;i<GateTable.getRowCount();i++)
        {
                boolean bSelect = ((Boolean) GateModelGst.getValueAt(i, Select)).booleanValue();
                if (!bSelect) 
                {
                    JOptionPane.showMessageDialog(null,"Plz Select All Items In the List");
                    return false;
                }
        }
        return true;
    }
}

