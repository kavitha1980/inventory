package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

 

// pdf import
import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;

public class FinPendingList extends JInternalFrame
{
      JPanel        TopPanel;
      JPanel        MiddlePanel;
      JPanel        BottomPanel;
      JPanel        First,Second,Third,Fourth;
 
      String        SStDate,SEnDate;
      JButton       BApply,BPrint,BCreatePdf;
      JTextField    TFile;
      DateField     TStDate,TEnDate;
      JRadioButton  JRAsOn,JRPeriod;


      JLayeredPane  DeskTop;
      Vector        VCode,VName;
      StatusPanel   SPanel;
      int           iMillCode;
      String        SSupTable,SMillName;

      Common        common = new Common();
      TabReport     tabreport;

      JComboBox     JCDept,JCDelay;
      Vector        VDept,VDeptCode;


      Vector        VAcCode,VAcName,VGrnNo,VGrnBlock,VGrnDate,VGateNo,VGateDate,VOrderNo;
      Vector        VInvNo,VInvDate;
      Vector        VMatName,VInvQty,VRecQty,VRejQty,VAcQty;
      Vector        VInvValue,VBlock,VOrdQty,VFinDelay,VGrnDelay;
      Vector        VGrnType,VRejNo,VRejDate;

      Vector        VXOrderDate,VXOrderBlock,VXOrderNo,VXIndex,VPaymentTerms;

      String        ColumnData[] = {"Supplier","Grn Type","Grn No","Block","Grn Date","Rej No","Rej Date","Order No","Order Date","Invoice No","Invoice Date","Material Name","Qty Accepted","Invoice Value","GRN Vs Finance","GI Vs GRN"};
      String        ColumnType[] = {"S"       , "S"      ,"S"     ,"S"    ,"S"       ,"S"     ,"S"       ,"S"       ,"S"         ,"S"         ,"S"           ,"S"            ,"N"           ,"V"            ,"N"             ,"N"};
      Object        RowData[][];

      String        SHead1 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String        SHead2 = "|                           GRN                             |          Gate           |           Order           |        Invoice          |                                          |                        Q U A N T I T Y                         |      Invoice |   Payment Terms     | Delayed Days  |";
      String        SHead3 = "|-------------------------------------------------------------------------------------------------------------------------------------------|              Material Name               |----------------------------------------------------------------|        Value |                     |---------------|";
      String        SHead4 = "| GrnType |   GrnNo  | GrnDate  |Block|   RejNo  |  RejDate |         No |       Date |    No    |Block|   Date   |     No     |       Date |                                          |  As Per DC |   Received |   Rejected |   Accepted |    Ordered |           Rs |                     |  Grn | GI-GRN |";
      String        SHead5 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String        SHead6 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String        SHead7 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String        SHead8 = "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String        SHead9 = "|         |          |          |     |          |          |            |            |          |     |          |            |            |                                          |            |            |            |            |            |              |                     |      |        |";
                               
      int           Lctr = 100,Pctr     = 0;
      FileWriter    FW;
      double        dInvValue = 0;

//pdf

    Document document;
    PdfPTable table;
    int iTotalColumns = 23;
    int iWidth[] = {10, 10, 14, 10, 10, 8, 8, 14, 10, 8,14, 10, 15, 20, 10, 10, 10, 10, 10, 14,14,10,8};
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font bigNormal = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
    String SFile="";   
    String PDFFile = common.getPrintPath()+"/FinPending.pdf";

      public FinPendingList(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,String SSupTable,String SMillName)
      {
          super("Grn's Pending @ Finance As On ");
          this.DeskTop   = DeskTop;
          this.VCode     = VCode;
          this.VName     = VName;
          this.SPanel    = SPanel;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SMillName = SMillName;

          setDeptCombo();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
      }

      public void createComponents()
      {
          TopPanel       = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();

          First          = new JPanel();
          Second         = new JPanel();
          Third          = new JPanel();
          Fourth         = new JPanel();

          TStDate        = new DateField();
          TEnDate        = new DateField();

          JCDept         = new JComboBox(VDept);
  		  JCDelay        = new JComboBox();

		  JCDelay.addItem("All");
          JCDelay.addItem("Delay Days > 10 Days");

          BApply         = new JButton("Apply");
          BPrint         = new JButton("Print");
          BCreatePdf   = new JButton("Create pdf"); 
          TFile          = new JTextField(15);

          JRAsOn         = new JRadioButton("As On",true);
          JRPeriod       = new JRadioButton("Periodical",false);

          TStDate.setTodayDate();
          TEnDate.setTodayDate();
      }

      public void setLayouts()
      {
          TopPanel   .setLayout(new GridLayout(1,4));
          First      .setLayout(new GridLayout(1,1));
          Second     .setLayout(new GridLayout(2,1));
          Third      .setLayout(new GridLayout(2,2));
          Fourth     .setLayout(new GridLayout(1,1));

          MiddlePanel.setLayout(new BorderLayout());
            
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
      }
                                                                        
      public void addComponents()
      {
          First.setBorder(new TitledBorder("Dept"));
          First.add(JCDept);

          Second.setBorder(new TitledBorder("Basis"));
          Second.add(JRAsOn);
          Second.add(JRPeriod);
          
          Third.setBorder(new TitledBorder("As On"));
          Third.add(new JLabel("AsOnDate"));
          Third.add(TEnDate);
          Third.add(new JLabel("Delay"));
          Third.add(JCDelay);

          Fourth.add(BApply);

          TopPanel.add(First);
          TopPanel.add(Second);
          TopPanel.add(Third);
          TopPanel.add(Fourth);

          BottomPanel         . add(BPrint);
          BottomPanel         . add(TFile);
          BottomPanel. add(BCreatePdf);
          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(MiddlePanel,BorderLayout.CENTER);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
      }

      public void addListeners()
      {
          JRPeriod  .addActionListener(new JRList());
          JRAsOn    .addActionListener(new JRList()); 

          BApply    . addActionListener(new ApplyList());
          BPrint    . addActionListener(new PrintList());
          BCreatePdf.addActionListener(new PrintList()); 
      }

      private class JRList implements ActionListener
      {
           public void actionPerformed(ActionEvent ae)
           {
                if(ae.getSource()==JRPeriod)
                {
                     Third.setBorder(new TitledBorder("Periodical"));
                     Third.removeAll();
                     Third.add(TStDate);
                     Third.add(TEnDate);
                     Third.updateUI();
                     JRAsOn.setSelected(false);
                }
                else
                {
                     Third.setBorder(new TitledBorder("As On"));
                     Third.removeAll();
                     Third.add(TEnDate);
                     Third.add(new JLabel(""));
                     Third.updateUI();
                     JRPeriod.setSelected(false);
                }
           }
      }

      public class ApplyList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setDataIntoVector();
                  setVectorIntoRowData();
                  try
                  {
                        MiddlePanel     . remove(tabreport);
                  }
                  catch(Exception ex){}

                  tabreport   = new TabReport(RowData,ColumnData,ColumnType);                        
                  MiddlePanel . add("Center",tabreport);
                  MiddlePanel . updateUI();
            }
      }

      public class PrintList implements ActionListener
      {

              public void actionPerformed(ActionEvent ae)
            {
               if(ae.getSource() == BCreatePdf)
               {
                        createPDFFile();
                     
                      try{
                File theFile   = new File(PDFFile);
                Desktop        . getDesktop() . open(theFile);
                  

                  
               }catch(Exception ex){}

                       
               }
                  setPendingReport();
                  removeHelpFrame();
            }
            
            
      }

      public void removeHelpFrame()
      {
            try
            {
                  DeskTop     . remove(this);
                  DeskTop     . repaint();
                  DeskTop     . updateUI();
            }
            catch(Exception ex) { ex.printStackTrace();}
      }

      private void setDataIntoVector()
      {
            VAcCode           = new Vector();
            VAcName           = new Vector();
            VGrnNo            = new Vector();
            VGrnBlock         = new Vector();
            VGrnDate          = new Vector();
            VGateNo           = new Vector();
            VGateDate         = new Vector();
            VOrderNo          = new Vector();
            VInvNo            = new Vector();
            VInvDate          = new Vector();
            VMatName          = new Vector();
            VInvQty           = new Vector();
            VRecQty           = new Vector();
            VRejQty           = new Vector();
            VAcQty            = new Vector();
            VInvValue         = new Vector();
            VBlock            = new Vector();
            VOrdQty           = new Vector();
            VGrnType          = new Vector();
            VRejNo            = new Vector();
            VRejDate          = new Vector();

            VXOrderDate       = new Vector();
            VXOrderBlock      = new Vector();
            VXOrderNo         = new Vector();
            VPaymentTerms     = new Vector();

            SStDate = TStDate.toNormal();
            SEnDate = TEnDate.toNormal();


            String QS         = "";
            String QS1        = "";

            QS =   " select SupplierCode,Suppliername,GrnNum,"+
                   " OrdBlockName,grnDate,GInNum,GinDate,OrdNum,"+
                   " GrnInvNum,GrnInvDate,ItemName,InvQty,MilQty,sum(RejQty),sum(GrnQty),"+
                   " sum(GrnValue),BlockCode,PendingQty,RejecNo,RejecDate,GrnType,ReFlag,Grnslno"+
                   " from"+
                   " (select grn.sup_code as Suppliercode,"+SSupTable+".name as Suppliername,"+
                   " grn.grnno as GrnNum,ordblock.blockname as OrdBlockName,"+
                   " grn.grndate as grnDate,grn.gateinno as GInNum,"+
                   " grn.gateindate as GinDate,grn.orderno as OrdNum,"+
                   " GRN.InvNo as GrnInvNum,GRN.InvDate as GrnInvDate,"+
                   " InvItems.ITEM_NAME as ItemName,GRN.InvQty as InvQty,GRN.MillQty as MilQty,"+
                   " GRN.RejQty as RejQty,GRN.GrnQty as GrnQty,GRN.GrnValue as GrnValue,"+
                   " Grn.GrnBlock as BlockCode,Grn.Pending as PendingQty,"+
                   " 0 as RejecNo,' ' as RejecDate,'Grn' as GrnType,Grn.Rejflag as ReFlag,Grn.SlNo as Grnslno"+
                   " from grn"+
                   " inner join "+SSupTable+" on "+SSupTable+".ac_code = grn.sup_code"+
                   " inner join invitems on grn.code = invitems.item_code and invitems.hsntype=0 "+
                   " inner join ordblock on grn.grnblock = ordblock.block"+
                   " inner join (select grnno,slno,sum(grnvalue) from grn where spjno=0 "+
                   " and millcode="+iMillCode+" group by grnno,slno having sum(grnvalue)>0) t "+
                   " on (grn.grnno=t.grnno and grn.slno=t.slno) ";

            if(JRPeriod.isSelected())
            {
                   QS = QS + " Where GRN.GRNDate >='"+SStDate+"' and GRN.GRNDate <='"+SEnDate+"'";
            }
            else
            {
                   QS = QS + " Where GRN.GRNDate <='"+SEnDate+"'";
            }


               if(!(JCDept.getSelectedItem()).equals("ALL"))
                    QS = QS + " and GRN.Dept_code="+VDeptCode.elementAt(JCDept.getSelectedIndex());

            QS = QS + " and Grn.millcode="+iMillCode+" and rejflag = 0 And GRN.SPJNo=0"+
                   " union all"+
                   " select grn.sup_code as Suppliercode,"+SSupTable+".name as Suppliername,"+
                   " grn.grnno as GrnNum,ordblock.blockname as OrdBlockName,"+
                   " grn.grndate as grnDate,grn.gateinno as GInNum,"+
                   " grn.gateindate as GinDate,grn.orderno as OrdNum,"+
                   " GRN.InvNo as GrnInvNum,GRN.InvDate as GrnInvDate,"+
                   " InvItems.ITEM_NAME as ItemName,GRN.InvQty as InvQty,GRN.MillQty as MilQty,"+
                   " GRN.RejQty as RejQty,GRN.GrnQty as GrnQty,GRN.GrnValue as GrnValue,"+
                   " Grn.GrnBlock as BlockCode,Grn.Pending as PendingQty,"+
                   " Grn.RejNo as RejecNo,Grn.RejDate as RejecDate,'Rejection' as GrnType,Grn.Rejflag as ReFlag,Grn.SlNo as Grnslno"+
                   " from grn"+
                   " inner join "+SSupTable+" on "+SSupTable+".ac_code = grn.sup_code"+
                   " inner join invitems on grn.code = invitems.item_code and invitems.hsntype=0"+
                   " inner join ordblock on grn.grnblock=ordblock.block"+
                   " inner join (select grnno,slno,sum(grnvalue) from grn where spjno=0 "+
                   " and millcode="+iMillCode+" group by grnno,slno having sum(grnvalue)>0) t "+
                   " on (grn.grnno=t.grnno and grn.slno=t.slno) "+
                   " where grn.Grnno in (select grnno from grn";

            if(JRPeriod.isSelected())
            {
                   QS = QS + " Where GRN.GRNDate >='"+SStDate+"' and GRN.GRNDate <='"+SEnDate+"'";
            }
            else
            {
                   QS = QS + " Where GRN.GRNDate <='"+SEnDate+"'";
            }


               if(!(JCDept.getSelectedItem()).equals("ALL"))
                    QS = QS + " and GRN.Dept_code="+VDeptCode.elementAt(JCDept.getSelectedIndex());


            QS = QS + " and rejflag = 0 and Grn.millcode="+iMillCode+" And GRN.SPJNo=0)"+
                   " and grn.millcode="+iMillCode+" and rejflag = 1)"+
                   " group by SupplierCode,Suppliername,GrnNum,OrdBlockName,grnDate,GInNum,"+
                   " GinDate,OrdNum,GrnInvNum,GrnInvDate,ItemName,InvQty,MilQty,"+
                   " BlockCode,PendingQty,RejecNo,RejecDate,GrnType,ReFlag,Grnslno "+
                   " Order by SupplierName,"+
                   " GrnNum,Grnslno,ReFlag,OrdBlockName,OrdNum,grnDate,ItemName ";

            QS1 =  " Select PurchaseOrder.OrderDate,Grn.GrnBlock,Grn.OrderNo,PurchaseOrder.PayTerms From PurchaseOrder "+
                   " Inner Join GRN On (PurchaseOrder.OrderBlock = Grn.GrnBlock And PurchaseOrder.OrderNo = Grn.OrderNo and PurchaseOrder.SlNo = Grn.OrderSlNo)"+
                   " Where GRN.SPJNo=0 And Grn.MillCode="+iMillCode+" Order By 2,3 ";

            try
            {
                 ORAConnection       oraConnection  = ORAConnection.getORAConnection();
                 Connection          theConnection  = oraConnection.getConnection();
                 Statement           stat           = theConnection.createStatement();
                 ResultSet           theResult      = stat.executeQuery(QS);

                 while(theResult.next())
                 {
					String SGrnDate  = common.parseDate(theResult.getString(5));
					String SFinDelay = common.getDateDiff(TEnDate.toString(),SGrnDate);

					if(JCDelay.getSelectedIndex()==0)
					{

                      VAcCode         . addElement(theResult.getString(1));
                      VAcName         . addElement(theResult.getString(2));
                      VGrnNo          . addElement(theResult.getString(3));
                      VGrnBlock       . addElement(theResult.getString(4));
                      VGrnDate        . addElement(theResult.getString(5));
                      VGateNo         . addElement(theResult.getString(6));
                      VGateDate       . addElement(theResult.getString(7));
                      VOrderNo        . addElement(theResult.getString(8));
                      VInvNo          . addElement(theResult.getString(9));
                      VInvDate        . addElement(theResult.getString(10));
                      VMatName        . addElement(theResult.getString(11));
                      VInvQty         . addElement(theResult.getString(12));
                      VRecQty         . addElement(theResult.getString(13));
                      VRejQty         . addElement(theResult.getString(14));
                      VAcQty          . addElement(theResult.getString(15));
                      VInvValue       . addElement(theResult.getString(16));
                      VBlock          . addElement(theResult.getString(17));
                      VOrdQty         . addElement(common.getRound(theResult.getString(18),2));
                      VRejNo          . addElement(common.parseNull(theResult.getString(19)));
                      VRejDate        . addElement(common.parseNull(theResult.getString(20)));
                      VGrnType        . addElement(common.parseNull(theResult.getString(21)));
					}
					if(JCDelay.getSelectedIndex()==1 && common.toInt(SFinDelay)>10)
					{
						  VAcCode         . addElement(theResult.getString(1));
						  VAcName         . addElement(theResult.getString(2));
						  VGrnNo          . addElement(theResult.getString(3));
						  VGrnBlock       . addElement(theResult.getString(4));
						  VGrnDate        . addElement(theResult.getString(5));
						  VGateNo         . addElement(theResult.getString(6));
						  VGateDate       . addElement(theResult.getString(7));
						  VOrderNo        . addElement(theResult.getString(8));
						  VInvNo          . addElement(theResult.getString(9));
						  VInvDate        . addElement(theResult.getString(10));
						  VMatName        . addElement(theResult.getString(11));
						  VInvQty         . addElement(theResult.getString(12));
						  VRecQty         . addElement(theResult.getString(13));
						  VRejQty         . addElement(theResult.getString(14));
						  VAcQty          . addElement(theResult.getString(15));
						  VInvValue       . addElement(theResult.getString(16));
						  VBlock          . addElement(theResult.getString(17));
						  VOrdQty         . addElement(common.getRound(theResult.getString(18),2));
						  VRejNo          . addElement(common.parseNull(theResult.getString(19)));
						  VRejDate        . addElement(common.parseNull(theResult.getString(20)));
						  VGrnType        . addElement(common.parseNull(theResult.getString(21)));
					}
                 }
                 theResult.close();

                 ResultSet theResult1 = stat.executeQuery(QS1);
                 while(theResult1.next())
                 {
                     VXOrderDate       . addElement(theResult1.getString(1));    
                     VXOrderBlock      . addElement(theResult1.getString(2));    
                     VXOrderNo         . addElement(theResult1.getString(3));
                     VPaymentTerms     . addElement(theResult1.getString(4));
                 }
                 theResult1.close();
                 stat.close();
                 setIndexTable();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
                  ex.printStackTrace();
            }
      }

      private void setVectorIntoRowData()
      {
            RowData = new Object[VAcCode.size()][ColumnData.length];
            for(int i=0;i<VAcCode.size();i++)
            {
                  String SFinDelay      = common.getDateDiff(TEnDate.toString(),common.parseDate((String)VGrnDate.elementAt(i)));
                  String SGateDelay     = common.getDateDiff(common.parseDate((String)VGrnDate.elementAt(i)),common.parseDate((String)VGateDate.elementAt(i)));

                  RowData[i][0]         = ""+(String)VAcName     . elementAt(i);
                  RowData[i][1]         = ""+(String)VGrnType    . elementAt(i);
                  RowData[i][2]         = ""+(String)VGrnNo      . elementAt(i);
                  RowData[i][3]         = ""+(String)VGrnBlock   . elementAt(i);
                  RowData[i][4]         = ""+common.parseDate((String)VGrnDate.elementAt(i));
                  RowData[i][5]         = ""+getConStr((String)VRejNo      . elementAt(i));
                  RowData[i][6]         = ""+common.parseDate((String)VRejDate.elementAt(i));
                  RowData[i][7]         = ""+(String)VOrderNo    . elementAt(i);
                  RowData[i][8]         = ""+common.parseDate(getOrdDate(i));
                  RowData[i][9]         = ""+(String)VInvNo      . elementAt(i);
                  RowData[i][10]        = ""+ common.parseDate((String)VInvDate.elementAt(i));
                  RowData[i][11]        = ""+(String)VMatName    . elementAt(i);
                  RowData[i][12]        = ""+(String)VAcQty      . elementAt(i);
                  RowData[i][13]        = ""+(String)VInvValue   . elementAt(i);
                  RowData[i][14]        = ""+SFinDelay;
                  RowData[i][15]        = ""+SGateDelay;
            }
      }

     private void setPendingReport()
     {
          if(VAcCode.size() == 0)
               return;
          
          Lctr = 100;
          Pctr = 0;
          
          String    PAcCode   = (String)VAcCode.elementAt(0);
          String    SPGrnNo   = "";
          String    SPBlock   = "";
          String    SFile     = TFile.getText();
                    dInvValue = 0;
          
          if((SFile.trim()).length()==0)
               SFile = "1.prn";
          
          try
          {
               FW = new FileWriter(common.getPrintPath()+SFile);
			//FW = new FileWriter(common.getPrintPath()+SFile);
               for(int i=0;i<VAcCode.size();i++)
               {
                    setHead((String)VAcName.elementAt(i));

                    String    SAcCode   = (String)VAcCode   . elementAt(i);
                    String    SGrnNo    = (String)VGrnNo    . elementAt(i);
                    String    SBlock    = (String)VGrnBlock . elementAt(i);

                    if(!SAcCode.equals(PAcCode))
                    {
                         setContBreak();
                         PAcCode   = SAcCode;
                         setFirstLine(0,(String)VAcName.elementAt(i));
                         dInvValue = 0;
                    }
                    if(SPGrnNo.equals(SGrnNo))
                         setBody(i,false);
                    else
                    {
                         setBody(i,true);
                    }
                    SPGrnNo = SGrnNo;
                    SPBlock = SBlock;
               }
               setContBreak();
             
               new ControlFigures(FW,iMillCode);
              // FW   . write("< End of Report >\n\n");
               FW  . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void setHead(String SName) throws Exception
     {
          if (Lctr < 60)
               return;
               
          if (Pctr > 0)
               setPageBreak();
               
          Pctr++;

          String str1 = "gCompany  : "+SMillName;
          String str2 = "";
          if(JRPeriod.isSelected())
          {
               str2 = "Document : Report on GRN's Pending @ Finpro From "+TStDate.toString()+"  To "+TEnDate.toString()+" For Department : "+(String)JCDept.getSelectedItem();
          }
          else
          {
               str2 = "Document : Report on GRN's Pending @ Finpro as On "+TEnDate.toString()+" For Department : "+(String)JCDept.getSelectedItem();
          }

          String str3 = "Page No  : "+Pctr;
          
          FW   . write(str1+"\n");
          FW   . write(str2+"\n");
          FW   . write(str3+"\n");
          FW   . write(SHead1+"\n");
          FW   . write(SHead2+"\n");
          FW   . write(SHead3+"\n");
          FW   . write(SHead4+"\n");
          FW   . write(SHead5+"\n");
          Lctr = 8;

          if(Pctr == 1)
               setFirstLine(0,SName);
     }

     private void setFirstLine(int i,String SName) throws Exception 
     {
          String str="";

          if(i==1)
               SName = SName+"(Contd...)";               
          
          String    strx =    "| "+common.Pad(SName,114)+
                              common.Rad(" ",10)+"   "+common.Pad(" ",10)+"   "+
                              common.Pad(" ",40)+"   "+
                              common.Rad(" ",10)+"   "+common.Rad(" ",10)+"   "+
                              common.Rad(" ",10)+"   "+common.Rad(" ",10)+"   "+
                              common.Rad(" ",10)+"   "+common.Rad(" ",12)+"   "+
                              common.Pad(".",19)+" | "+
                              common.Rad(" ",4)+"   "+common.Rad(" ",6)+" |";
          
          FW   . write(SHead6+"\n");
          FW   . write(strx+"\n");
          FW   . write(SHead7+"\n");
          Lctr = Lctr+3;
     }

     private void setContBreak() throws Exception
     {
          if(Lctr == 8)
               return;
          FW.write(SHead5+"\n");
          
          String str  = "|"+common.Rad(" ",9)+"|"+common.Rad(" ",10)+"|"+common.Pad(" ",10)+"|"+common.Pad(" ",5)+"|"+
                         common.Rad(" ",10)+"|"+common.Pad(" ",10)+"| "+common.Rad(" ",10)+" | "+common.Pad(" ",10)+" |"+
                         common.Rad(" ",10)+"|"+common.Pad(" ",5)+"|"+common.Pad(" ",10)+"| "+
                         common.Rad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                         common.Pad("T O T A L",40)+" | "+
                         common.Rad(" ",10)+" | "+common.Rad(" ",10)+" | "+
                         common.Rad(" ",10)+" | "+common.Rad(" ",10)+" | "+
                         common.Rad(" ",10)+" | "+common.Rad(common.getRound(dInvValue,2),12)+" | "+
                         common.Space(19)+" | "+
                         common.Rad(" ",4)+" | "+common.Rad(" ",6)+" |";
          
          FW   . write(str+"\n");
          FW   . write(SHead5+"\n");
          
          Lctr = Lctr+3;
     }

     private void setPageBreak() throws Exception
     {
          FW.write(SHead8+"\n");
     }

     private void setBody(int i,boolean bFlag) throws Exception
     {
          if(Lctr == 8)
          {
               setFirstLine(1,(String)VAcName.elementAt(i));
               bFlag = true;
          }

          if(bFlag && Lctr > 8)
          {
               FW.write(SHead9+"\n");
               Lctr++;
          }

          String    SGrnNo    = "",SGrnDate  = "",SGINo     = "",
                    SGIDate   = "",SInvNo    = "",SInvDate  = "",
                    SGrnBlock = "",SRejDate  = "",SRejNo    = "",
                    SGrnType  = "";

          if(bFlag)
          {
                    SGrnNo         = (String)VGrnNo                        . elementAt(i);
                    SGrnDate       = common.parseDate((String)VGrnDate     . elementAt(i));
          }
                    SGINo          = (String)VGateNo                       . elementAt(i);
                    SGIDate        = common.parseDate((String)VGateDate    . elementAt(i));
                    SInvNo         = (String)VInvNo                        . elementAt(i);
                    SInvDate       = common.parseDate((String)VInvDate     . elementAt(i));

                    SRejDate       = common.parseDate((String)VRejDate     . elementAt(i));
                    SRejNo         =                   (String)VRejNo      . elementAt(i);
                    SGrnType       =                   (String)VGrnType    . elementAt(i);
                    SGrnBlock      =                   (String)VGrnBlock   . elementAt(i);

          String    SOrderNo       = (String)VOrderNo  . elementAt(i);
          String    SOrdBlock      = (String)VGrnBlock . elementAt(i);
          String    SOrderDate     = common.parseDate(getOrdDate(i));

          String    SPaymentTerms  = getPaymentTerms(i);
          String    SMatName       = (String)VMatName  . elementAt(i);
          
          String    SInvQty        = common.getRound((String)VInvQty  . elementAt(i),3);
          String    SRecQty        = common.getRound((String)VRecQty  . elementAt(i),3);        
          String    SRejQty        = common.getRound((String)VRejQty  . elementAt(i),3);        
          String    SAcQty         = common.getRound((String)VAcQty   . elementAt(i),3);         
          String    SOrdQty        = common.getRound((String)VOrdQty  . elementAt(i),3);         
          
          String    SInvValue      = common.getRound((String)VInvValue. elementAt(i),2);
          String    SFinDelay      = common.getDateDiff(TEnDate.toString(),common.parseDate((String)VGrnDate.elementAt(i)));
          String    SGateDelay     = common.getDateDiff(common.parseDate((String)VGrnDate.elementAt(i)),common.parseDate((String)VGateDate.elementAt(i)));
          
                    dInvValue      = dInvValue+common.toDouble(SInvValue);
          
          String str  = "|"+common.Pad(SGrnType,9)+"|"+common.Pad(" "+SGrnNo,10)+"|"+
                         common.Pad(SGrnDate,10)+"|"+common.Pad(" "+SGrnBlock,5)+"|"+common.Pad(" "+getConStr(SRejNo.trim()),10)+"|"+common.Pad(SRejDate,10)+"| "+
                         common.Rad(SGINo,10)+" | "+common.Pad(SGIDate,10)+" |"+
                         common.Rad(SOrderNo,10)+"|"+common.Pad(SOrdBlock,5)+"|"+common.Pad(SOrderDate,10)+"| "+
                         common.Rad(SInvNo,10)+" | "+common.Pad(SInvDate,10)+" | "+
                         common.Pad(SMatName,40)+" | "+
                         common.Rad(SInvQty,10)+" | "+common.Rad(SRecQty,10)+" | "+
                         common.Rad(SRejQty,10)+" | "+common.Rad(SAcQty,10)+" | "+
                         common.Rad(SOrdQty,10)+" | "+common.Rad(SInvValue,12)+" | "+
                         common.Pad(SPaymentTerms,19)+" | "+
                         common.Rad(SFinDelay,4)+" | "+common.Rad(SGateDelay,6)+" |";

          FW.write(str+"\n");
          Lctr++;
     }

     private void setIndexTable()
     {
          VXIndex = new Vector();

          for(int i=0;i<VXOrderBlock.size();i++)
          {
               String    str1      = (String)VXOrderBlock.elementAt(i);
               String    str2      = (String)VXOrderNo.elementAt(i);
                         VXIndex   . addElement(str1+"|"+str2);
          }
     }

     private String getOrdDate(int i)
     {
          String    SOrderDate= "";
          String    SOrderNo  = (String)VOrderNo.elementAt(i);
          int       iIndex    = 0;
          iIndex    = VXOrderNo.indexOf(SOrderNo);

          if(iIndex!=-1)
               SOrderDate     = common.parseNull((String)VXOrderDate.elementAt(iIndex));

          return SOrderDate;
     }
     
     private String getPaymentTerms(int i)
     {
          String SPayTerms    = "";
          String    SOrderNo  = (String)VOrderNo.elementAt(i);
          int       iIndex    = 0;
                    iIndex    = VXOrderNo.indexOf(SOrderNo);

          if(iIndex!=-1)
               SPayTerms = common.parseNull((String)VPaymentTerms.elementAt(iIndex));

          return SPayTerms;
     }

     private String getConStr(String SRejNo)
     {
          String SConRejNo = "";

                    if(!SRejNo.equals("0"))
                         SConRejNo = SRejNo;

          return SConRejNo;
     }

     private void setDeptCombo()
     {
          VDept          = new Vector();
          VDeptCode      = new Vector();
          
          VDept          .addElement("ALL");
          VDeptCode      .addElement("9999");
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               String QS1 = "";
               QS1 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               
               ResultSet result3 = stat.executeQuery(QS1);
               while(result3.next())
               {
                    VDept     .addElement(result3.getString(1));
                    VDeptCode .addElement(result3.getString(2));
               }
               result3.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept:"+ex);
               System.exit(0);
          }
     }

private void createPDFFile() {

if(VAcCode.size() == 0)
               return;
          
        //  Lctr = 100;
         // Pctr = 0;
          
          String    PAcCode   = (String)VAcCode.elementAt(0);
          String    SPGrnNo   = "";
          String    SPBlock   = "";
          String    SFile     = TFile.getText();
                    dInvValue = 0;
          
          if((SFile.trim()).length()==0)
               SFile = "1.pdf";
          
          try
          {

             
             document = new Document(PageSize.A4.rotate());
              PdfWriter.getInstance(document, new FileOutputStream(PDFFile));
              document.open();
              document .  newPage();  
              

                        table  = new PdfPTable(23);
			table  . setWidths(iWidth);
			table  . setWidthPercentage(100);
                        table.setHeaderRows(4);

                AddCellIntoTable("Company : "+SMillName+" ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 23, 0, 0, 0, 0, smallbold);
 if(JRPeriod.isSelected())
          {
             AddCellIntoTable( "Document : Report on GRN's Pending @ Finpro From "+TStDate.toString()+"  To "+TEnDate.toString()+" For Department : "+(String)JCDept.getSelectedItem()+"", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 23, 0, 0, 0, 0, smallbold);           
  
          }
          else
          {

           AddCellIntoTable( "Document : Report on GRN's Pending @ Finpro as On "+TEnDate.toString()+" For Department : "+(String)JCDept.getSelectedItem()+"", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 23, 0, 0, 0, 0, smallbold);           
  
          }
	


      setHeadpdf();
               for(int i=0;i<VAcCode.size();i++)
               {
                   // setHead((String)VAcName.elementAt(i));

                    String    SAcCode   = (String)VAcCode   . elementAt(i);
                    String    SGrnNo    = (String)VGrnNo    . elementAt(i);
                    String    SBlock    = (String)VGrnBlock . elementAt(i);

                    if(!SAcCode.equals(PAcCode) || i==0)
                    {
                         setContBreakpdf();
                         PAcCode   = SAcCode;
                         setFirstLinepdf((String)VAcName.elementAt(i));
                         dInvValue = 0;
                    }
                    if(SPGrnNo.equals(SGrnNo))
                         setBodypdf(i,false);
                    else
                    {
                         setBodypdf(i,true);
                    }
                    SPGrnNo = SGrnNo;
                    SPBlock = SBlock;
               }
              // setContBreak();
              
               //new ControlFigures(FW,iMillCode);
              document.add(table); 
              document   . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

}

 private void setHeadpdf() throws Exception
{

        AddCellIntoTable("GRN  ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 6, 4, 1, 2, 2, mediumbold);
	AddCellIntoTable("GATE", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("Order ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 3, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("Invoice", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 2, mediumbold);
	AddCellIntoTable("Material Name", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
        AddCellIntoTable("  QUANTITY     ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 5, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("Invoice Value ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("Payment Terms", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
	AddCellIntoTable("Delayed Days", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 2, mediumbold);

	AddCellIntoTable("GRN TYPE ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 2, 2, mediumbold);
	AddCellIntoTable("GRN No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("GRN Date ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("Block", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
	AddCellIntoTable("Rej No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("Rej Date", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("Date", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("Block", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("Date", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 2, 2, mediumbold);
	AddCellIntoTable("Date", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("Material Name ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("As Per DC", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
	AddCellIntoTable(" Received", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("Rejected", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("Accepted", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("Ordered", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("Rs.", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("grn", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("GI-GRN", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
 }
 private void setFirstLinepdf(String SName) throws Exception 
     {
          String str="";
         /* if(i==1)
               str = "Name : "+SName+" (Contd...)";               
          else*/
               str = "Name : "+SName+" ";

          AddCellIntoTable(str, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 23, 0, 0, 0, 0, smallbold); 
         
     }

 private void setBodypdf(int i,boolean bFlag) throws Exception
     {
         /* if(Lctr == 8)
          {
               setFirstLinepdf((String)VAcName.elementAt(i));
               bFlag = true;
          }

          if(bFlag && Lctr > 8)
          {
               FW.write(SHead9+"\n");
               Lctr++;
          }*/

          String    SGrnNo    = "",SGrnDate  = "",SGINo     = "",
                    SGIDate   = "",SInvNo    = "",SInvDate  = "",
                    SGrnBlock = "",SRejDate  = "",SRejNo    = "",
                    SGrnType  = "";

         
                    SGrnNo         = (String)VGrnNo                        . elementAt(i);
                    SGrnDate       = common.parseDate((String)VGrnDate     . elementAt(i));
          
                    SGINo          = (String)VGateNo                       . elementAt(i);
                    SGIDate        = common.parseDate((String)VGateDate    . elementAt(i));
                    SInvNo         = (String)VInvNo                        . elementAt(i);
                    SInvDate       = common.parseDate((String)VInvDate     . elementAt(i));

                    SRejDate       = common.parseDate((String)VRejDate     . elementAt(i));
                    SRejNo         =                   (String)VRejNo      . elementAt(i);
                    SGrnType       =                   (String)VGrnType    . elementAt(i);
                    SGrnBlock      =                   (String)VGrnBlock   . elementAt(i);

          String    SOrderNo       = (String)VOrderNo  . elementAt(i);
          String    SOrdBlock      = (String)VGrnBlock . elementAt(i);
          String    SOrderDate     = common.parseDate(getOrdDate(i));

          String    SPaymentTerms  = getPaymentTerms(i);
          String    SMatName       = (String)VMatName  . elementAt(i);
          
          String    SInvQty        = common.getRound((String)VInvQty  . elementAt(i),3);
          String    SRecQty        = common.getRound((String)VRecQty  . elementAt(i),3);        
          String    SRejQty        = common.getRound((String)VRejQty  . elementAt(i),3);        
          String    SAcQty         = common.getRound((String)VAcQty   . elementAt(i),3);         
          String    SOrdQty        = common.getRound((String)VOrdQty  . elementAt(i),3);         
          
          String    SInvValue      = common.getRound((String)VInvValue. elementAt(i),2);
          String    SFinDelay      = common.getDateDiff(TEnDate.toString(),common.parseDate((String)VGrnDate.elementAt(i)));
          String    SGateDelay     = common.getDateDiff(common.parseDate((String)VGrnDate.elementAt(i)),common.parseDate((String)VGateDate.elementAt(i)));
          
                    dInvValue      = dInvValue+common.toDouble(SInvValue);
          
          String str  = "|"+common.Pad(SGrnType,9)+"|"+common.Pad(" "+SGrnNo,10)+"|"+
                         common.Pad(SGrnDate,10)+"|"+common.Pad(" "+SGrnBlock,5)+"|"+common.Pad(" "+getConStr(SRejNo.trim()),10)+"|"+common.Pad(SRejDate,10)+"| "+
                         common.Rad(SGINo,10)+" | "+common.Pad(SGIDate,10)+" |"+
                         common.Rad(SOrderNo,10)+"|"+common.Pad(SOrdBlock,5)+"|"+common.Pad(SOrderDate,10)+"| "+
                         common.Rad(SInvNo,10)+" | "+common.Pad(SInvDate,10)+" | "+
                         common.Pad(SMatName,40)+" | "+
                         common.Rad(SInvQty,10)+" | "+common.Rad(SRecQty,10)+" | "+
                         common.Rad(SRejQty,10)+" | "+common.Rad(SAcQty,10)+" | "+
                         common.Rad(SOrdQty,10)+" | "+common.Rad(SInvValue,12)+" | "+
                         common.Pad(SInvValue,19)+" | "+
                         common.Rad(SInvValue,4)+" | "+common.Rad(SGateDelay,6)+" |";


		AddCellIntoTable(SGrnType, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 2, 2, smallnormal);
		AddCellIntoTable(SGrnNo, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
		AddCellIntoTable(SGrnDate, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SGrnBlock, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(SRejNo, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
		AddCellIntoTable(SRejDate, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SGINo, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SGIDate, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
		AddCellIntoTable(SOrderNo, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SOrdBlock, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SOrderDate, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
                AddCellIntoTable(SInvNo, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
		AddCellIntoTable(SInvDate, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);   
		AddCellIntoTable(SMatName, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 2, 2, smallnormal);
		AddCellIntoTable(SInvQty, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
		AddCellIntoTable(SRecQty, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SRejQty, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(SAcQty, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
		AddCellIntoTable(SOrdQty, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SInvValue, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SPaymentTerms, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
		AddCellIntoTable(SFinDelay, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SGateDelay, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 


                
        
     }
 private void setContBreakpdf() throws Exception
     {

 AddCellIntoTable("TOTAL", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 2, mediumbold); 
                AddCellIntoTable("", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 17, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(String.valueOf(common.getRound(dInvValue,2)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
               AddCellIntoTable("", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 3, 4, 1, 8, 2, smallnormal); 
     }

  public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }



}
