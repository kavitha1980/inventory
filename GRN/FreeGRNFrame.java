package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class FreeGRNFrame extends JInternalFrame
{
     
     JLayeredPane Layer;
     Vector VInwNo,VInwName;
     TabReport tabreportx;
     Vector VCode,VName,VInwNox;
     Vector VCode5,VQty5,VRate,VNet,VSup_Name5;

     JButton    BMaterial,BOk;
     JTextField TMatCode,TSupplier,TMaterial;
     JDialog    theDialog;
     NextField  TRate,TQty,TNet;

     JPanel TopPanel,BottomPanel,CashPanel;
     JComboBox JCType;
     JTextArea TA;
     int index=0;
     Common common = new Common();
     int iMillCode;

     FreeGRNFrame(JLayeredPane Layer,Vector VInwNo,Vector VInwName,TabReport tabreportx,Vector VCode,Vector VName,Vector VInwNox,Vector VCode5,Vector VQty5,Vector VSup_Name5,JDialog theDialog,int iMillCode)
     {
          this.Layer      = Layer;
          this.VInwNo     = VInwNo;
          this.VInwName   = VInwName;
          this.tabreportx = tabreportx;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VInwNox    = VInwNox;
          this.VCode5     = VCode5;
          this.VQty5      = VQty5;
          this.VSup_Name5 = VSup_Name5;
          this.theDialog  = theDialog;
          this.iMillCode  = iMillCode;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          BMaterial   = new JButton("Select Material");
          BOk         = new JButton("Okay");
          JCType      = new JComboBox(VInwName);
          TA          = new JTextArea(10,70);
          TQty        = new NextField();
          TMatCode    = new JTextField();
          TMaterial   = new JTextField();
          TSupplier   = new JTextField();
          TopPanel    = new JPanel(true);
          BottomPanel = new JPanel(true);
          CashPanel   = new JPanel(true);
     }

     private void setLayouts()
     {
         TopPanel.setLayout(new GridLayout(4,2));
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,350,300);
     }
     private void addComponents()
     {
         TopPanel.add(new JLabel("Material"));
         TopPanel.add(BMaterial);
         TopPanel.add(new JLabel("Supplier"));
         TopPanel.add(TSupplier);
         TopPanel.add(new JLabel("Inward Type"));
         TopPanel.add(JCType);
         TopPanel.add(new JLabel("Qty"));
         TopPanel.add(TQty);

         BottomPanel.add(BOk);

         getContentPane().add("North",TopPanel);
         getContentPane().add("Center",new JScrollPane(TA));
         getContentPane().add("South",BOk);
		 
         CashPanel.setLayout(new BorderLayout()); 
         CashPanel.add("North",TopPanel);
         CashPanel.add("Center",new JScrollPane(TA));
         CashPanel.add("South",BOk);

         setPresets();
     }

     private void setPresets()
     {
         index       = tabreportx.ReportTable.getSelectedRow(); 
         int iType   = common.toInt((String)VInwNox.elementAt(index));
         JCType.setSelectedIndex(iType);
         setDescript();

//         TMaterial.setText((String)VMatName.elementAt(index)); 
         TQty.setText((String)VQty5.elementAt(index));
         TMatCode.setText((String)VCode5.elementAt(index));
         TSupplier.setText((String)VSup_Name5.elementAt(index));

         TA.setEditable(false);
         TMaterial.setEditable(false);

     }

     private void addListeners()
     {
         BMaterial.addActionListener(new MaterialList());
         BOk.addActionListener(new ActList());
     }

     private class MaterialList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
			  Frame dummy = new Frame();
			  JDialog theDialog = new JDialog(dummy,"Description Dialog",true);
			  
                 MaterialSearch1 MS = new MaterialSearch1(Layer,TMatCode,BMaterial,VCode,VName,theDialog,iMillCode);
			  theDialog.getContentPane().add(MS.MaterialPanel);
			  theDialog.setBounds(80,100,450,350);
			  theDialog.setVisible(true);
		  }
	 }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(false);
               String SMatName = BMaterial.getText();
               String SMatCode = TMatCode.getText();


               String SSupplier= TSupplier.getText();
               String SInwNo   = (String)VInwNo.elementAt(JCType.getSelectedIndex());

//               tabreportx.ReportTable.setValueAt(new Boolean(true),index,7);
               tabreportx.ReportTable.setValueAt(new Boolean(true),index,5);

               tabreportx.ReportTable.setValueAt(SMatName,index,3);
               tabreportx.ReportTable.setValueAt(SSupplier,index,4);

               VCode5.setElementAt(SMatCode,index);
               VInwNox.setElementAt(SInwNo,index);
               VQty5.setElementAt(TQty.getText(),index);
               VSup_Name5.setElementAt(SSupplier,index);
                    
               removeHelpFrame();
               tabreportx.ReportTable.requestFocus();
          }
     }

     private void removeHelpFrame()
     {
          try{theDialog.setVisible(false);}catch(Exception e){}
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }
     public void setDescript()
     {
          String SGINo = (String)tabreportx.RowData[index][0];

          String QS  = "";
          if(iMillCode == 0)
          {
               QS   = " SELECT [GateInward].[Item_Name],[GateInward.SupQty],[GateInward].[Id],InvItems.Item_Name,GateInward.GateQty "+
                      " From GateInward Left Join InvItems On InvItems.Item_Code = GateInward.Item_Code "+
                      " WHERE [GateInward].[GINo]="+SGINo+" And GrnNo=0 "+
                      " And (GateInward.MillCode Is Null Or GateInward.MillCode = 0) "+
                      " Order By 1 ";
          }
          else
          {
               QS   = " SELECT [GateInward].[Item_Name],[GateInward.SupQty],[GateInward].[Id],InvItems.Item_Name,GateInward.GateQty "+
                      " From GateInward Left Join InvItems On InvItems.Item_Code = GateInward.Item_Code "+
                      " WHERE [GateInward].[GINo]="+SGINo+" And GrnNo=0 And GateInward.Authentication=1 "+
                      " And GateInward.MillCode = 1 "+
                      " Order By 1 ";
          }

          TA.setText("");
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    String str = common.parseNull(res.getString(1))+""+common.parseNull(res.getString(4));
                    TA.append(str+"\n");
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}
