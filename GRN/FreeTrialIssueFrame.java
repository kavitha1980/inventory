package GRN;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class FreeTrialIssueFrame extends JInternalFrame
{
     JPanel MiddlePanel,BottomPanel;
     JButton     BOk;
     JTabbedPane JTP;
     TabReport tabreport;

     Object RowData[][];
     String ColumnData[] = {"GI No","Date","Type","Supplier","Material Code","Material Name","Recd Qty","Issue Qty","Issue Rate","Indent No","Modified"};
     String ColumnType[] = {"N"    ,"S"   ,"S"   ,"S"       ,"S"            ,"S"            ,"N"       ,"N"        ,"E"         ,"E"        ,"B"       };

     Common  common  = new Common();

     JLayeredPane DeskTop;
     StatusPanel SPanel;
     Vector VCode,VName;
     int iMillCode,iAuthCode;
     String SSupTable;
     
     Vector VGINo,VGIDate,VSupName,VMName,VMCode,VRecQty,VId,VInwName,VInwCode;

     Connection theConnection=null;
     Connection theDConnection=null;

     boolean bComflag = true;

     public FreeTrialIssueFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,int iAuthCode,String SSupTable)
     {
         super("Issue of Free,Trial & Cash Materials to SubStore");
         this.DeskTop   = DeskTop;
         this.VCode     = VCode;
         this.VName     = VName;
         this.SPanel    = SPanel;
         this.iMillCode = iMillCode;
         this.iAuthCode = iAuthCode;
         this.SSupTable = SSupTable;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     public void createComponents()
     {
          MiddlePanel  = new JPanel();
          BottomPanel  = new JPanel();
          JTP          = new JTabbedPane();
          BOk          = new JButton("Issue to SubStore");
     }

     public void setLayouts()
     {
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,650,500);
         MiddlePanel.setLayout(new BorderLayout());
     }

     public void addComponents()
     {
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

          MiddlePanel.add("Center",JTP);
          if(iAuthCode>1)
          {
               BottomPanel.add(BOk);
          }

          setDataIntoVector();
          setRowData();

          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

             JTP.addTab("Trial,Free & Cash Materials",tabreport);

             setSelected(true);
             DeskTop.repaint();
             DeskTop.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println("Prob"+ex);
          }
     }
     public void addListeners()
     {
          BOk.addActionListener(new ModiList());                    
     }

     private class ModiList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(isDataValid())
               {
                    BOk.setEnabled(false);
                    updateGateInward();
                    getACommit();
               }
          }
     }

     private boolean isDataValid()
     {
          boolean bflag = true;

          for(int i=0;i<RowData.length;i++)
          {
               Boolean bValue = (Boolean)RowData[i][10];

               if(!bValue.booleanValue())
                    continue;

               double dRecQty = common.toDouble((String)RowData[i][6]);
               double dIssQty = common.toDouble((String)RowData[i][7]);

               if((dIssQty<=0) || (dIssQty>dRecQty))
               {
                    JOptionPane.showMessageDialog(null,"Invalid Issue Qty","Attention",JOptionPane.INFORMATION_MESSAGE);
                    tabreport.ReportTable.requestFocus();
                    return false;
               }

               double dIssRate = common.toDouble((String)RowData[i][8]);
               if(dIssRate<=0)
               {
                    JOptionPane.showMessageDialog(null,"Invalid Issue Rate","Attention",JOptionPane.INFORMATION_MESSAGE);
                    tabreport.ReportTable.requestFocus();
                    return false;
               }

               int iIndentNo = common.toInt((String)RowData[i][9]);
               if(iIndentNo<=0)
               {
                    JOptionPane.showMessageDialog(null,"Invalid IndentNo","Attention",JOptionPane.INFORMATION_MESSAGE);
                    tabreport.ReportTable.requestFocus();
                    return false;
               }
          }     
          return bflag;
     }

     private void updateGateInward()
     {
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
          
               for(int i=0;i<RowData.length;i++)
               {
                    Boolean bValue = (Boolean)RowData[i][10];
     
                    if(!bValue.booleanValue())
                         continue;

                    String SGINo     = (String)RowData[i][0];
                    String SDate     = common.getServerPureDate();
                    String SGIType   = (String)VInwCode.elementAt(i);
                    String SCode     = (String)RowData[i][4];
                    String SQty      = common.getRound((String)RowData[i][7],3);
                    String SRate     = common.getRound((String)RowData[i][8],4);
                    String SIndentNo = (String)RowData[i][9];
                    String SGId      = (String)VId.elementAt(i);

                    insertIntoSubStoreReceipt(SGINo,SDate,SGIType,SCode,SQty,SRate,SIndentNo);

                    String QS = "Update GateInward Set ";
                    QS = QS+" IssueStatus = 2 , ";
                    QS = QS+" IndentNo    = 0"+SIndentNo+",";
                    QS = QS+" IssueQty    = 0"+SQty+",";
                    QS = QS+" GrnType     = 0"+SGIType;
                    QS = QS+" Where Id    = "+SGId;
                    QS = QS+" and MillCode = 1 ";

                    if(theConnection  . getAutoCommit())
                         theConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag  = false;
          }
     }

     private void insertIntoSubStoreReceipt(String SGINo,String SDate,String SGIType,String SCode,String SQty,String SRate,String SIndentNo)
     {
          try
          {
               String SStkVal = common.getRound(common.toDouble(SQty) * common.toDouble(SRate),2);

               if(theDConnection==null)
               {
                    DORAConnection jdbc  = DORAConnection.getORAConnection();
                    theDConnection       = jdbc.getConnection();
               }
               PreparedStatement psmt    =  theDConnection.prepareStatement(getInsertQS());
               Statement         theStat =  theDConnection.createStatement();

               if(theDConnection  . getAutoCommit())
                    theDConnection  . setAutoCommit(false);

               psmt.setInt(1,getID());
               psmt.setInt(2,common.toInt(SGINo));
               psmt.setInt(3,common.toInt(SDate));
               psmt.setInt(4,common.toInt(SIndentNo));
               psmt.setString(5,SCode);
               psmt.setDouble(6,common.toDouble(SQty));
               psmt.setDouble(7,common.toDouble(SRate));
               psmt.setString(8,"1");
               psmt.setInt(9,common.toInt(SGIType));
               psmt.execute();
               psmt.close();

               
               String QS = "Update InvItems Set ";
               QS = QS+" CSStock=nvl(CSStock,0)+"+SQty+",";
               QS = QS+" CSValue=nvl(CSValue,0)+"+SStkVal+" ";
               QS = QS+" Where Item_Code = '"+SCode+"'";

               theStat.execute(QS);
               theStat.close();
          }
          catch(Exception e)
          {
               bComflag  = false;
               e.printStackTrace();
          }
     }
     private String getInsertQS()
     {
          String QS ="";
          QS        =    "Insert into SubStoreReceipt(ID,GrnNo,GrnDate,IndentSlipNo,RawCode,GrnQty,GrnRate,ShiftCode,GrnType) "+
                         "values(?,?,?,?,?,?,?,?,?) ";
          return QS;
     }
     private int getID()
     {
          int iId=0;
          try
          {
               if(theDConnection==null)
               {
                    DORAConnection jdbc = DORAConnection.getORAConnection();
                    theDConnection      = jdbc.getConnection();
               }
               Statement theStatement   =    theDConnection.createStatement();
               ResultSet theResult      =    theStatement.executeQuery("Select SubStoreReceipt_Seq.nextVal from Dual");
               while(theResult.next())
               {
                    iId                 =    theResult.getInt(1);
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception e)
          {
               bComflag  = false;
               e.printStackTrace();
          }
          return iId;
     }

     private void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex){}
     }

     public void setDataIntoVector()
     {
          String QString = "";

          QString  = " SELECT GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.Item_Code,InvItems.Item_Name,GateInward.Id,GateInward.GateQty,InwType.InwName,GateInward.InwNo "+
                     " From ((GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                     " Inner Join InvItems on GateInward.Item_Code = InvItems.Item_Code) "+
                     " Inner Join InwType on GateInward.InwNo=InwType.InwNo "+
                     " Where GateInward.GrnNo = 0 And GateInward.IssueStatus = 1 "+
                     " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                     " And GateInward.MillCode = 1 "+
                     " Order By 9,2,1,5";


            VGINo    = new Vector();
            VGIDate  = new Vector();
            VSupName = new Vector();
            VMName   = new Vector();
            VMCode   = new Vector();
            VRecQty  = new Vector();
            VId      = new Vector();
            VInwName = new Vector();
            VInwCode = new Vector();

            try
            {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();

              ResultSet res = stat.executeQuery(QString);
              while (res.next())
              {
                    VGINo   .addElement(""+ res.getString(1));
                    VGIDate .addElement(""+ common.parseDate(res.getString(2)));
                    VSupName.addElement(""+ res.getString(3));
                    VMCode  .addElement(""+ res.getString(4));
                    VMName  .addElement(""+ res.getString(5));
                    VId     .addElement(""+ res.getString(6));
                    VRecQty .addElement(""+ res.getString(7));
                    VInwName.addElement(""+ res.getString(8));
                    VInwCode.addElement(""+ res.getString(9));
              }
              res.close();
              stat.close();
           }
           catch(Exception ex){System.out.println("sql "+ex);}
     }
     public void setRowData()
     {
         RowData      = new Object[VGINo.size()][ColumnData.length];

         for(int i=0;i<VGINo.size();i++)
         {
               RowData[i][0]  = (String)VGINo.elementAt(i);
               RowData[i][1]  = (String)VGIDate.elementAt(i);
               RowData[i][2]  = (String)VInwName.elementAt(i);
               RowData[i][3]  = (String)VSupName.elementAt(i);
               RowData[i][4]  = (String)VMCode.elementAt(i);
               RowData[i][5]  = (String)VMName.elementAt(i);
               RowData[i][6]  = (String)VRecQty.elementAt(i);
               RowData[i][7]  = (String)VRecQty.elementAt(i);
               RowData[i][8]  = "";
               RowData[i][9]  = "";
               RowData[i][10] = new Boolean(false);
        }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnection  . commit();
                    theDConnection . commit();
                    removeHelpFrame();

                    JOptionPane    . showMessageDialog(null,"The Entered Data is Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("Commit");
               }
               else
               {
                    theConnection  . rollback();
                    theDConnection . rollback();
                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theConnection    . setAutoCommit(true);
               theDConnection   . setAutoCommit(true);

          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }


}
