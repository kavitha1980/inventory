package GRN;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GIAllocationPrintFrame extends JInternalFrame
{
      JLayeredPane Layer;
      JPanel         TopPanel,BottomPanel,MiddlePanel,TopLeft,TopRight;
      MyButton       BApply,BOk,BCancel;
      TabReport      theReport;
      JTabbedPane    thePane;
      DateField      TDate;
      Vector VGrnNo,VGrnDate,VSupName,VItemCode,VItemName,VRejQty,VRdc,VRdcDate,VDesp;
      Object RowData[][];

      Common common   = new Common();

      int iUserCode,iMillCode,iPageNo,iLineCount;
      String SSupTable;

      FileWriter FW;


      ORAConnection connect;
      Connection theconnect;
      String SNo,Qty,SuppName,Code,Supplier,GINo,GIDate,SRdc,SRdcDate,SDesp;
      public GIAllocationPrintFrame(JLayeredPane Layer,int iUserCode,int iMillCode,String SSupTable)
      {
            this.Layer     = Layer;
            this.iUserCode = iUserCode;
            this.iMillCode = iMillCode;
            this.SSupTable = SSupTable;

            createComponents();
            setLayouts();
            addComponents();
            addListeners();
      }

      public void createComponents()
      {
            try
            {
                 thePane     = new JTabbedPane();
                 TDate       = new DateField();

                 TDate.setTodayDate();
                 TDate.setEditable(false);

                 TopPanel    = new JPanel(true);
                 TopLeft     = new JPanel(true);
                 TopRight    = new JPanel(true);
                 BottomPanel = new JPanel(true);
                 MiddlePanel = new JPanel(true);

                 BApply      = new MyButton("Apply");
                 BOk         = new MyButton("Print");
                 BCancel     = new MyButton("Cancel");

            }catch(Exception ex)
            {
                ex.printStackTrace();
            }
      }
      public void setLayouts()
      {
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,500,500);
            setTitle("Direct RDC Allocation Print");

            TopPanel.setLayout(new GridLayout(1,2));
            TopLeft.setLayout(new GridLayout(1,2));
            TopLeft.setBorder(new TitledBorder("DateInfo"));
            TopRight.setLayout(new GridLayout(1,1));
            TopRight.setBorder(new TitledBorder("Apply"));
            MiddlePanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());
            TopPanel.setBorder(new TitledBorder("Info"));
      }

      public void addComponents()
      {
            TopLeft.add(new MyLabel("As On"));
            TopLeft.add(TDate);

            TopRight.add(BApply);

            TopPanel.add(TopLeft);
            TopPanel.add(TopRight);

            BottomPanel.add(BOk);
            BottomPanel.add(BCancel);

            getContentPane().add("North",TopPanel);
            getContentPane().add("Center",MiddlePanel);
            getContentPane().add("South",BottomPanel);
     }

     public void addListeners()
     {
          BOk         .addActionListener(new ActList());
          BCancel     .addActionListener(new ActList());
          BApply      .addActionListener(new ActList());
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
                    BApply.setEnabled(false);
                    String SDate  = TDate.toNormal();
                    showList(SDate);
                    setTabReport();

                    if(VGrnNo.size()>0)
                         BOk.setEnabled(true);
               }

               if(ae.getSource()==BOk)
               {
                         toHeadPrn();
                         setGIAllocationPrint();
                         BOk.setEnabled(false);
                         removeHelpFrame();
               }

               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
               }
          }
     }

     private void setTabReport()
     {
          try
          {
               MiddlePanel.removeAll();
               thePane.removeAll();

               String ColumnName[]  = {"SlNo","GINo","GI Date","Supplier","Code","Name","Qty","Rdc No","Rdc Date","Descript"};
               String ColumnType[]  = {"N"   ,"N"    ,"S"       ,"S"       ,"S"   ,"S"   ,"N"  ,"N"     ,"S"       ,"S"     };
               int    ColumnWidth[] = {50    ,50     ,75        ,100       ,75    ,100   ,75   ,60      ,75        ,100     };

               RowData = new Object[VGrnNo.size()][ColumnName.length];

               if(VGrnNo.size()>0)
               {
                    for(int i=0;i<VGrnNo.size();i++)
                    {
                         RowData[i][0]   = String.valueOf(i+1);
                         RowData[i][1]   = common.parseNull((String)VGrnNo         .elementAt(i));
                         RowData[i][2]   = common.parseDate((String)VGrnDate       .elementAt(i));
                         RowData[i][3]   = common.parseNull((String)VSupName       .elementAt(i));
                         RowData[i][4]   = common.parseNull((String)VItemCode      .elementAt(i));
                         RowData[i][5]   = common.parseNull((String)VItemName      .elementAt(i));
                         RowData[i][6]   = common.parseNull((String)VRejQty        .elementAt(i));
                         RowData[i][7]   = common.parseNull((String)VRdc           .elementAt(i));
                         RowData[i][8]   = common.parseNull((String)VRdcDate       .elementAt(i));
                         RowData[i][9]   = common.parseNull((String)VDesp          .elementAt(i));
                    }
                    theReport      = new TabReport(RowData,ColumnName,ColumnType);
                    theReport.setPrefferedColumnWidth(ColumnWidth);

                    MiddlePanel.add("Center",thePane);
                    thePane.addTab("Completed Rejection List",theReport);
               }
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }
     private void showList(String SDate)
     {
          VGrnNo         = new Vector();
          VGrnDate       = new Vector();
          VSupName       = new Vector();
          VItemCode      = new Vector();
          VItemName      = new Vector();
          VRejQty        = new Vector();

          VRdc           = new Vector();
          VRdcDate       = new Vector();
          VDesp          = new Vector();

                             
          String QS =    " Select GiNo,GIDate,Name,Item_Code,Item_Name,GateQty,RdcNo,RdcDate,Descript from "+
                         " (Select GateInward.GiNo,GateInward.GIDate,"+SSupTable+".Name,GateInward.Item_Code,InvItems.Item_Name,GateInward.GateQty, "+
                         " Rdc.RdcNo,RDC.RdcDate,Rdc.Descript from GateInward "+
                         " Inner join "+SSupTable+" on "+SSupTable+".Ac_Code=GateInward.Sup_Code "+
                         " Inner join InvItems on InvItems.Item_Code=GateInward.Item_Code "+
                         " Inner join Rdc on RDC.id=gateInward.rdcid "+
                         " Where GateInward.RDCMatchStatus=1 and Rdc.GiMatchStatus=1 and GateInward.GiDate<="+SDate+" "+
                         " and GateInward.MillCode="+iMillCode+" and GateInward.Item_Code is Not Null "+
                         " Union All "+
                         " Select GateInward.GiNo,GateInward.GIDate,"+SSupTable+".Name,GateInward.Item_Code,GateInward.Item_Name,GateInward.GateQty, "+
                         " Rdc.RdcNo,RDC.RdcDate,Rdc.Descript from GateInward "+
                         " Inner join "+SSupTable+" on "+SSupTable+".Ac_Code=GateInward.Sup_Code "+
                         " Inner join Rdc on RDC.id=gateInward.rdcid "+
                         " Where GateInward.RDCMatchStatus=1 and Rdc.GiMatchStatus=1 and GateInward.GiDate<="+SDate+" "+
                         " and GateInward.MillCode="+iMillCode+" and GateInward.Item_Code is Null) "+
                         " Order by 1 ";

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat       = theconnect.createStatement();
               ResultSet theResult  = stat.executeQuery(QS);

               while(theResult.next())
               {
                    VGrnNo      .addElement(theResult.getString(1));
                    VGrnDate    .addElement(theResult.getString(2));
                    VSupName    .addElement(theResult.getString(3));
                    VItemCode   .addElement(theResult.getString(4));
                    VItemName   .addElement(theResult.getString(5));
                    VRejQty     .addElement(theResult.getString(6));
                    VRdc        .addElement(theResult.getString(7));
                    VRdcDate    .addElement(theResult.getString(8));
                    VDesp       .addElement(theResult.getString(9));
               }
               theResult.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void setGIAllocationPrint()
     {
          int iLinecount =0;
          int iPageNo    =1;
          int iSlNo      =1;

          try
          {
               FW=new FileWriter(common.getPrintPath()+"DirectAllocationPrint.prn");
               FW.write(toHeadPrn());

               for(int i=0;i<VGrnNo.size();i++)
               {
                 int SI=(i+1);

                 SNo        =common.Pad(Integer.toString(SI),3);
                 GINo       =common.Pad((String)VGrnNo.elementAt(i),10);
                 GIDate     =common.Pad(common.parseDate(VGrnDate.elementAt(i).toString()),10);
                 Supplier   =common.Pad((String)VSupName.elementAt(i),30);
                 Code       =common.Pad((String)VItemCode.elementAt(i),10);
                 SuppName   =common.Pad((String)VItemName.elementAt(i),30);
                 Qty        =common.Pad((String)VRejQty.elementAt(i),10);
                 SRdc       =common.Pad((String)VRdc.elementAt(i),10);
                 SRdcDate   =common.Pad((String)VRdcDate.elementAt(i),10);
                 SDesp      =common.Pad((String)VDesp.elementAt(i),10);
                 

                if(iLineCount < 57)                                                                                          
                {
                        iLineCount = 0;

                        FW.write("|"+SNo+"|"+GINo+"|"+GIDate+"|"+Supplier+"|"+Code+"|"+SuppName+"|"+Qty+"|"+SRdc+"|"+SRdcDate+"|"+SDesp+"|\n");
                        FW.write("|"+common.Space(3)+"|"+common.Space(10)+"|"+common.Space(10)+"|"+common.Space(30)+"|"+common.Space(10)+"|"+common.Space(30)+"|"+common.Space(10)+"|"+common.Space(10)+"|"+common.Space(10)+"|"+common.Space(10)+"|\n");
                        iLineCount += 2;
                        
                }
                else
                {
                        FW.write("|"+SNo+"|"+GINo+"|"+GIDate+"|"+Supplier+"|"+Code+"|"+SuppName+"|"+Qty+"|"+SRdc+"|"+SRdcDate+"|"+SDesp+"|\n");
                        FW.write("|"+common.Space(3)+"|"+common.Space(10)+"|"+common.Space(10)+"|"+common.Space(30)+"|"+common.Space(10)+"|"+common.Space(30)+"|"+common.Space(10)+"|"+common.Space(10)+"|"+common.Space(10)+"|"+common.Space(10)+"|\n");
                        
                        
                }
                     
                }
                    FW.write(toFootPrn());
                    FW.close();

             }
          catch(Exception e){

               e.printStackTrace();
          }

     }
     private String toHeadPrn()
     {
          StringBuffer SB = new StringBuffer();
          SB.append("P\n");
          SB.append("EDirect RDC Allocation Print  As On:"+TDate.toString()+"F\n");
          SB.append("\n");
          SB.append("EPage No : "+(iPageNo++)+"\n");
          SB.append(" |----------------------------------------------------------------------------------------------------------------------------------------------| \n");
          SB.append(" |Sl.|   GINO   |   GIDate |            Supplier          |     Code |       Name                   |Quantity  | Rdc      | Rdc      | Desp     | \n");
          SB.append(" |No.|          |          |                              |          |                              |          |          | Date     |          |  \n");
          SB.append(" |----------------------------------------------------------------------------------------------------------------------------------------------|F\n");
          SB.append("");                                                                                                 
          
          return SB.toString();                                   
     }
     private String toFootPrn()
     {                                                                                                                                                                                                     
          StringBuffer SB = new StringBuffer();

          SB.append(" -----------------------------------------------------------------------------------------------------------------------------------------------| \n");
          SB.append("\n");

          return SB.toString();
     }



     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
}
