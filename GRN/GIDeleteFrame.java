package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GIDeleteFrame extends JInternalFrame
{
     
     JLayeredPane   Layer;
     TabReport      tabreport;

     JButton        BOk;
     JDialog        theDialog;
     int            iMillCode;
     Vector         VDeleGINo;
     String         SSupName;
     NewDirectGRNGatePanel GatePanel;

     JPanel         TopPanel,MiddlePanel,BottomPanel,DeletePanel;
     JTextField     TSupplier;

     Common common = new Common();

     Object RowData[][];
     String ColumnData[] = {"GI No","Click for Delete"};
     String ColumnType[] = {"N"    ,"B"};

     GIDeleteFrame(JLayeredPane Layer,JDialog theDialog,int iMillCode,Vector VDeleGINo,String SSupName,NewDirectGRNGatePanel GatePanel)
     {
          
          this.Layer      = Layer;
          this.theDialog  = theDialog;
          this.iMillCode  = iMillCode;
          this.VDeleGINo  = VDeleGINo;
          this.SSupName   = SSupName;
          this.GatePanel  = GatePanel;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setTabReport();
     }

     private void createComponents()
     {
          BOk            = new JButton("Okay");
          TSupplier      = new JTextField();
          TopPanel       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BottomPanel    = new JPanel(true);
          DeletePanel    = new JPanel(true);
     }

     private void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(1,2));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,350,300);
     }

     private void addComponents()
     {
          TopPanel       .add(new JLabel("Supplier"));
          TopPanel       .add(TSupplier);

          getContentPane().add("North",TopPanel);
          getContentPane().add("South",BOk);
		 
          DeletePanel      .setLayout(new BorderLayout()); 
          DeletePanel      .add("North",TopPanel);
          DeletePanel      .add("Center",MiddlePanel);
          DeletePanel      .add("South",BOk);

          setPresets();
     }

     private void setPresets()
     {
          TSupplier .setText(SSupName);

          TSupplier .setEditable(false);
     }

     private void addListeners()
     {
         BOk.addActionListener(new ActList());
     }
               
     public void setTabReport()
     {
          setRowData();
          try
          {
             MiddlePanel.remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             MiddlePanel.add(tabreport,BorderLayout.CENTER);
             tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
             setSelected(true);
             Layer.repaint();
             Layer.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }
     }


     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(false);

               Vector VDeleteGINo = new Vector();
               for(int i=0;i<RowData.length;i++)
               {
                    Boolean bValue = (Boolean)RowData[i][1];
     
                    if(!bValue.booleanValue())
                         continue;

                    String SGINo = (String)VDeleGINo.elementAt(i);
                    VDeleteGINo.addElement(SGINo);
               }
               if(VDeleteGINo.size()>0)
               {
                    GatePanel.remove(VDeleteGINo);
                    removeHelpFrame();
               }
               else
               {
                    JOptionPane.showMessageDialog(null,"No Row Selected","Error",JOptionPane.ERROR_MESSAGE);
                    BOk.setEnabled(true);
               }

          }
     }

     private void removeHelpFrame()
     {
          try{theDialog.setVisible(false);}catch(Exception e){}
          
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }

     public void setRowData()
     {
         RowData     = new Object[VDeleGINo.size()][ColumnData.length];
         for(int i=0;i<VDeleGINo.size();i++)
         {
               RowData[i][0]  = (String)VDeleGINo    .elementAt(i);
               RowData[i][1]  = new Boolean(false);
        }  
     }

}
