package GRN;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;



// pdf import
import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;

public class GIPendingList extends JInternalFrame
{
      JPanel    TopPanel;
      JPanel    BottomPanel;
 
      String     SDate;
      JButton    BApply,BPrint,BCreatePdf;
      JTextField TFile;
      JComboBox  JCFilter,JCDelay;
      MyComboBox JCParty;
     
      DateField TDate;
 
      String SInt = "  ";
      String Strline = "";
      int iLen=0;
      String SName,SOName;

      Object RowData[][];
      String ColumnData[] = {"GI Date","GI No","Code","Name","Supplier","Invoice No","Invoice Date","DC No","DC Date","Invoice Qty","Delay Days"};
      String ColumnType[] = {"S","N","S","S","S","S","S","S","S","N","N"};

      Vector VGIDate,VGINo,VPCode,VPName,VSupName,VInvNo,VInvDate,VDCNo,VDCDate,VQty,VDueDays;

      Vector VPartyCode,VPartyName;

      JLayeredPane DeskTop;
      Vector VCode,VName;
      StatusPanel SPanel;

      Common common = new Common();
      TabReport tabreport;
      int iMillCode;
      String SSupTable,SMillName;

    
   // pdf 
    
    Document document;
    PdfPTable table;
    int iTotalColumns = 10;
    int iWidth[] = {10, 10, 10, 20, 10, 8, 8, 8, 10, 8};
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font bigNormal = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
    String SFile="";   
      String PDFFile = common.getPrintPath()+"/GIPending.pdf";

      public GIPendingList(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,String SSupTable,String SMillName)
      {
            super("Materials Pending @Gate As On ");
            this.DeskTop   = DeskTop;
            this.VCode     = VCode;
            this.VName     = VName;
            this.SPanel    = SPanel;
            this.iMillCode = iMillCode;
            this.SSupTable = SSupTable;
            this.SMillName = SMillName;

            setPartyCombo();
            createComponents();
            setLayouts();
            addComponents();
            addListeners();

      }
      public void createComponents()
      {
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();
 
          TDate    = new DateField();
          BApply   = new JButton("Apply");
          BPrint   = new JButton("Print");
           BCreatePdf   = new JButton("Create pdf"); 
          TFile    = new JTextField(15);
          JCFilter = new JComboBox();
		  JCDelay  = new JComboBox();
          JCParty  = new MyComboBox(VPartyName);
 
          TDate.setTodayDate();
          TFile.setText("GIPend.prn");
      }
      public void setLayouts()
      {
          TopPanel.setLayout(new GridLayout(2,4,10,10));
 
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
      }
      public void addComponents()
      {
          JCFilter.addItem("Against P.O.");
          JCFilter.addItem("Without P.O.");
          JCFilter.addItem("All         ");

		  JCDelay.addItem("All");
          JCDelay.addItem("Delay Days > 10 Days");

          TopPanel.add(new JLabel("Select Party"));
          TopPanel.add(JCParty);
          TopPanel.add(new JLabel("List Only"));
          TopPanel.add(JCFilter);
          TopPanel.add(new JLabel("As On "));
          TopPanel.add(TDate);
          TopPanel.add(JCDelay);
          TopPanel.add(BApply);
 
          BottomPanel.add(BPrint);
          BottomPanel.add(TFile);
           BottomPanel. add(BCreatePdf);
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
      }
      public void addListeners()
      {
          BApply.addActionListener(new ApplyList());
          BPrint.addActionListener(new PrintList());
        BCreatePdf.addActionListener(new PrintList()); 
      }
      public class PrintList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  if(ae.getSource() == BCreatePdf)
               {
                        createPDFFile();
                     
                      try{
                File theFile   = new File(PDFFile);
                Desktop        . getDesktop() . open(theFile);
                  

                  
               }catch(Exception ex){}

                       
                             // saveData();
                               // printPDF();
                        
                  
               }
                  String STitle = "";
                  if(JCFilter.getSelectedIndex()==0)
                  {
                       STitle = "Materials Pending @Gate Against Purchase Order As On "+TDate.toString();
                  }
                  else if (JCFilter.getSelectedIndex()==1)
                  {
                       STitle = "Materials Pending @Gate Without Purchase Order As On "+TDate.toString();
                  }
                  else
                  {
                       STitle = "Materials Pending @Gate As On "+TDate.toString();
                  }

                  if(JCParty.getSelectedIndex()>0)
                  {
                       STitle = STitle + " For Party : "+(String)JCParty.getSelectedItem();
                  }

                  STitle = STitle + " \n";

                  Vector VHead  = getPendingHead();
                  iLen = ((String)VHead.elementAt(0)).length();
                  Strline = common.Replicate("-",iLen)+"\n";
                  Vector VBody  = getPendingBody();
                  String SFile  = TFile.getText();
                  new DocPrint(VBody,VHead,STitle,SFile,iMillCode,SMillName);
                  removeHelpFrame();
            }
      }
      public void removeHelpFrame()
      {
            try
            {
                  DeskTop.remove(this);
                  DeskTop.repaint();
                  DeskTop.updateUI();
            }
            catch(Exception ex) { }
      }

      public Vector getPendingHead()
      {
            Vector vect = new Vector();
 
            String Head1[] = {"GI Date","GI No","Mat Code","Mat Name","Inv No","Inv Date","DC No","DC Date","Inv/DC Qty","Delay Days"};
 
            String Sha1=((String)Head1[0]).trim();
            String Sha2=((String)Head1[1]).trim();
            String Sha3=((String)Head1[2]).trim();
            String Sha4=((String)Head1[3]).trim();
            String Sha5=((String)Head1[4]).trim();
            String Sha6=((String)Head1[5]).trim();
            String Sha7=((String)Head1[6]).trim();
            String Sha8=((String)Head1[7]).trim();
            String Sha9=((String)Head1[8]).trim();
            String Sha10=((String)Head1[9]).trim();
 
            Sha1  = common.Pad(Sha1,10);
            Sha2  = common.Rad(Sha2,6)+SInt;
            Sha3  = common.Pad(Sha3,9)+SInt;
            Sha4  = common.Pad(Sha4,30)+SInt;
            Sha5  = common.Pad(Sha5,10);
            Sha6  = common.Pad(Sha6,10)+SInt;
            Sha7  = common.Pad(Sha7,10);
            Sha8  = common.Pad(Sha8,10)+SInt;
            Sha9  = common.Rad(Sha9,10)+SInt;
            Sha10 = common.Rad(Sha10,10);
 
            String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+"\n";
            vect.add(Strh1);
            return vect;
      }
      public Vector getPendingBody()
      {
            Vector vect = new Vector();
            for(int i=0;i<RowData.length;i++)
            {
                  if(i>0)
                  {
                       SName     = ((String)RowData[i][4]).trim();
                       SOName = ((String)RowData[i-1][4]).trim();
                  }
 
                  String Sda1=((String)RowData[i][0]).trim();
                  String Sda2=((String)RowData[i][1]).trim();
                  String Sda3=((String)RowData[i][2]).trim();
                  String Sda4=((String)RowData[i][3]).trim();
                  String Sda5=((String)RowData[i][5]).trim();
                  String Sda6=((String)RowData[i][6]).trim();
                  String Sda7=((String)RowData[i][7]).trim();
                  String Sda8=((String)RowData[i][8]).trim();
                  String Sda9=((String)RowData[i][9]).trim();
                  String Sda10=((String)RowData[i][10]).trim();
 
                  Sda1  = common.Pad(Sda1,10);
                  Sda2  = common.Rad(Sda2,6)+SInt;
                  Sda3  = common.Pad(Sda3,9)+SInt;
                  Sda4  = common.Pad(Sda4,30)+SInt;
                  Sda5  = common.Pad(Sda5,10);
                  Sda6  = common.Pad(Sda6,10)+SInt;
                  Sda7  = common.Pad(Sda7,10);
                  Sda8  = common.Pad(Sda8,10)+SInt;
                  Sda9  = common.Rad(Sda9,10)+SInt;
                  Sda10 = common.Rad(Sda10,10);
 
                  String Sdb1 = ((String)RowData[i][4]).trim();
                  Sdb1 = common.Pad(Sdb1,25);
                  String Strd1 = common.Rad("Name : ",25)+" "+Sdb1+"\n";
 
                  String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+"\n";
                  if(i==0)
                  {
                       vect.add(Strd1);
                       vect.add(Strd);
                  }
                  else
                  {
                       if(SName.equals(SOName))
                            vect.add(Strd);
                       else
                       {
                            vect.add(Strline);
                            vect.add(Strd1);
                            vect.add(Strd);
                       }
                  }
            }
            return vect;
      }

      public class ApplyList implements ActionListener
      {
           public void actionPerformed(ActionEvent ae)
           {
                setDataIntoVector();
                setRowData();
                try
                {
                   getContentPane().remove(tabreport);
                }
                catch(Exception ex){}
 
                try
                {
                   tabreport = new TabReport(RowData,ColumnData,ColumnType);
                   getContentPane().add(tabreport,BorderLayout.CENTER);
                   setSelected(true);
                   DeskTop.repaint();
                   DeskTop.updateUI();
                }
                catch(Exception ex)
                {
                   System.out.println(ex);
                }
           }
      }

      public void setDataIntoVector()
      {
            VGIDate   = new Vector();
            VGINo     = new Vector();
            VPCode    = new Vector();
            VPName    = new Vector();
            VSupName  = new Vector();
            VInvNo    = new Vector();
            VInvDate  = new Vector();
            VDCNo     = new Vector();
            VDCDate   = new Vector();
            VQty      = new Vector();
            VDueDays  = new Vector();

            String SDate   = TDate.toNormal();
            String QS = "";

           /* if(JCFilter.getSelectedIndex()==0)
            {
                   QS = " Select GateInward.GIDate,GateInward.GINo,GateInward.Item_Code,InvItems.Item_Name,"+SSupTable+".Name,GateInward.InvNo,GateInward.InvDate,GateInward.DCNo,GateInward.DCDate,GateInward.SupQty "+
                        " From (GateInward Inner Join InvItems On InvItems.Item_Code = GateInward.Item_Code) Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = GateInward.Sup_Code "+
                        " Where GateInward.Item_Name is Null"+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                        " And InwNo = 0 and GateInward.GrnNo = 0 and GateInward.GIDate<='"+SDate+"'"+
                        " And GateInward.MillCode="+iMillCode;

               if(!(JCParty.getSelectedItem()).equals("ALL"))
                    QS = QS + " and GateInward.Sup_code='"+VPartyCode.elementAt(JCParty.getSelectedIndex())+"'";

                    QS = QS + " Order By 5,1,3 ";
            }
            else if(JCFilter.getSelectedIndex()==1)
            {
                   QS = " Select GateInward.GIDate,GateInward.GINo,GateInward.Item_Code,GateInward.Item_Name,"+SSupTable+".Name,GateInward.InvNo,GateInward.InvDate,GateInward.DCNo,GateInward.DCDate,GateInward.SupQty "+
                        " From GateInward Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = GateInward.Sup_Code "+
                        " Where GateInward.Item_Code Is Null "+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                        " And InwNo = 0 and GateInward.GrnNo = 0 and GateInward.GIDate<='"+SDate+"'"+
                        " And GateInward.MillCode="+iMillCode;

               if(!(JCParty.getSelectedItem()).equals("ALL"))
                    QS = QS + " and GateInward.Sup_code='"+VPartyCode.elementAt(JCParty.getSelectedIndex())+"'";

                    QS = QS + " Order By 5,1,3 ";
            }*/
             if(JCFilter.getSelectedIndex()==0)
             {
               QS = " Select GIDate,GINo,Item_Code,Item_Name,Name,InvNo,InvDate,DCNo,DCDate,SupQty  from ("+
                        " Select GateInward.GIDate,GateInward.GINo,'' as Item_Code,GateInward.Item_Name,"+SSupTable+".Name,GateInward.InvNo,GateInward.InvDate,GateInward.DCNo,GateInward.DCDate,GateInward.SupQty "+
                        " From GateInward Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = GateInward.Sup_Code "+
                         " where trim(GateInward.Item_Code) is Null "+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 and GateInward.entrystatus=1 "+
                        " And InwNo = 0 and GateInward.GrnNo = 0 and GateInward.GIDate<='"+SDate+"'"+
                        " And GateInward.MillCode="+iMillCode;

               if(!(JCParty.getSelectedItem()).equals("ALL"))
                    QS = QS + " and GateInward.Sup_code='"+VPartyCode.elementAt(JCParty.getSelectedIndex())+"'";

                    QS = QS + " Union all "  +
                        " Select GateInward.GIDate,GateInward.GINo,GateInward.Item_Code,InvItems.Item_Name,"+SSupTable+".Name,GateInward.InvNo,GateInward.InvDate,GateInward.DCNo,GateInward.DCDate,GateInward.SupQty "+
                        " From (GateInward Inner Join InvItems On InvItems.Item_Code = GateInward.Item_Code) Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = GateInward.Sup_Code "+
                        " where trim(GateInward.Item_Code) is Not Null "+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 and GateInward.entrystatus=1 "+
                        " And InwNo = 0 and GateInward.GrnNo = 0 and GateInward.GIDate<='"+SDate+"'"+
                        " And GateInward.MillCode="+iMillCode;

               if(!(JCParty.getSelectedItem()).equals("ALL"))
                    QS = QS + " and GateInward.Sup_code='"+VPartyCode.elementAt(JCParty.getSelectedIndex())+"'";

                    QS = QS + " ) Order By 5,1,3 ";
             }
             else if(JCFilter.getSelectedIndex()==1)
            {

               QS = " Select GIDate,GINo,Item_Code,Item_Name,Name,InvNo,InvDate,DCNo,DCDate,SupQty  from ("+
                        " Select GateInward.GIDate,GateInward.GINo,'' as Item_Code,GateInward.Item_Name,"+SSupTable+".Name,GateInward.InvNo,GateInward.InvDate,GateInward.DCNo,GateInward.DCDate,GateInward.SupQty "+
                        " From GateInward Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = GateInward.Sup_Code "+
                         " where trim(GateInward.Item_Code) is Null "+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 and GateInward.entrystatus=0 "+
                        " And InwNo = 0 and GateInward.GrnNo = 0 and GateInward.GIDate<='"+SDate+"'"+
                        " And GateInward.MillCode="+iMillCode;

               if(!(JCParty.getSelectedItem()).equals("ALL"))
                    QS = QS + " and GateInward.Sup_code='"+VPartyCode.elementAt(JCParty.getSelectedIndex())+"'";

                    QS = QS + " Union all "  +
                        " Select GateInward.GIDate,GateInward.GINo,GateInward.Item_Code,InvItems.Item_Name,"+SSupTable+".Name,GateInward.InvNo,GateInward.InvDate,GateInward.DCNo,GateInward.DCDate,GateInward.SupQty "+
                        " From (GateInward Inner Join InvItems On InvItems.Item_Code = GateInward.Item_Code) Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = GateInward.Sup_Code "+
                        " where trim(GateInward.Item_Code) is Not Null "+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 and GateInward.entrystatus=0 "+
                        " And InwNo = 0 and GateInward.GrnNo = 0 and GateInward.GIDate<='"+SDate+"'"+
                        " And GateInward.MillCode="+iMillCode;

               if(!(JCParty.getSelectedItem()).equals("ALL"))
                    QS = QS + " and GateInward.Sup_code='"+VPartyCode.elementAt(JCParty.getSelectedIndex())+"'";

                    QS = QS + " ) Order By 5,1,3 ";
            }
            else
            {      QS = " Select GIDate,GINo,Item_Code,Item_Name,Name,InvNo,InvDate,DCNo,DCDate,SupQty  from ("+
                        " Select GateInward.GIDate,GateInward.GINo,'' as Item_Code,GateInward.Item_Name,"+SSupTable+".Name,GateInward.InvNo,GateInward.InvDate,GateInward.DCNo,GateInward.DCDate,GateInward.SupQty "+
                        " From GateInward Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = GateInward.Sup_Code "+
                         " where trim(GateInward.Item_Code) is Null "+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                        " And InwNo = 0 and GateInward.GrnNo = 0 and GateInward.GIDate<='"+SDate+"'"+
                        " And GateInward.MillCode="+iMillCode;

               if(!(JCParty.getSelectedItem()).equals("ALL"))
                    QS = QS + " and GateInward.Sup_code='"+VPartyCode.elementAt(JCParty.getSelectedIndex())+"'";

                    QS = QS + " Union all "  +
                        " Select GateInward.GIDate,GateInward.GINo,GateInward.Item_Code,InvItems.Item_Name,"+SSupTable+".Name,GateInward.InvNo,GateInward.InvDate,GateInward.DCNo,GateInward.DCDate,GateInward.SupQty "+
                        " From (GateInward Inner Join InvItems On InvItems.Item_Code = GateInward.Item_Code) Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = GateInward.Sup_Code "+
                        " where trim(GateInward.Item_Code) is Not Null "+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                        " And InwNo = 0 and GateInward.GrnNo = 0 and GateInward.GIDate<='"+SDate+"'"+
                        " And GateInward.MillCode="+iMillCode;

               if(!(JCParty.getSelectedItem()).equals("ALL"))
                    QS = QS + " and GateInward.Sup_code='"+VPartyCode.elementAt(JCParty.getSelectedIndex())+"'";

                    QS = QS + " ) Order By 5,1,3 ";
            }

            try
            {

                  ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                  Connection      theConnection =  oraConnection.getConnection();
                  Statement       stat =  theConnection.createStatement();
                  ResultSet res      = stat.executeQuery(QS);
                  while(res.next())
                  {
                     String SGIDate = common.parseDate(res.getString(1));
                     String SDueDays = common.getDateDiff(TDate.toString(),SGIDate);

					if(JCDelay.getSelectedIndex()==0)
					{
		                 VGIDate  .addElement(SGIDate);
		                 VGINo    .addElement(res.getString(2));
		                 VPCode   .addElement(common.parseNull(res.getString(3)));
		                 VPName   .addElement(res.getString(4));
		                 VSupName .addElement(res.getString(5));
		                 VInvNo   .addElement(res.getString(6));
		                 VInvDate .addElement(common.parseDate(res.getString(7)));
		                 VDCNo    .addElement(res.getString(8));
		                 VDCDate  .addElement(common.parseDate(res.getString(9)));
		                 VQty     .addElement(res.getString(10));
		                 VDueDays .addElement(SDueDays);
					}
					if(JCDelay.getSelectedIndex()==1 && common.toInt(SDueDays)>10)
					{
						VGIDate  .addElement(SGIDate);
		                 VGINo    .addElement(res.getString(2));
		                 VPCode   .addElement(common.parseNull(res.getString(3)));
		                 VPName   .addElement(res.getString(4));
		                 VSupName .addElement(res.getString(5));
		                 VInvNo   .addElement(res.getString(6));
		                 VInvDate .addElement(common.parseDate(res.getString(7)));
		                 VDCNo    .addElement(res.getString(8));
		                 VDCDate  .addElement(common.parseDate(res.getString(9)));
		                 VQty     .addElement(res.getString(10));
		                 VDueDays .addElement(SDueDays);
					}
                  }
                  res.close();
                  stat.close();
            }
            catch(Exception ex)
            {
               System.out.println(ex);
            }
      }
      public void setRowData()
      {
            RowData = new Object[VGIDate.size()][ColumnData.length];
       

            for(int i=0;i<VGIDate.size();i++)


            {
                  RowData[i][0]  = common.parseNull((String)VGIDate  .elementAt(i));
                  RowData[i][1]  = common.parseNull((String)VGINo    .elementAt(i));
                  RowData[i][2]  = common.parseNull((String)VPCode   .elementAt(i));
                  RowData[i][3]  = common.parseNull((String)VPName   .elementAt(i));
                  RowData[i][4]  = common.parseNull((String)VSupName .elementAt(i));
                  RowData[i][5]  = common.parseNull((String)VInvNo   .elementAt(i));
                  RowData[i][6]  = common.parseNull((String)VInvDate .elementAt(i));
                  RowData[i][7]  = common.parseNull((String)VDCNo    .elementAt(i));
                  RowData[i][8]  = common.parseNull((String)VDCDate  .elementAt(i));
                  RowData[i][9]  = common.parseNull((String)VQty     .elementAt(i));
                  RowData[i][10] = common.parseNull((String)VDueDays .elementAt(i));
            }
      }

     private void setPartyCombo()
     {
          VPartyName     = new Vector();
          VPartyCode     = new Vector();
          
          VPartyName     .addElement("ALL");
          VPartyCode     .addElement("ALL");
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               String QS1 = "";

               QS1 = " Select Supplier.Name,GateInward.Sup_Code "+
                     " From GateInward "+
                     " Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = GateInward.Sup_Code "+
                     " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                     " And InwNo = 0 and GateInward.GrnNo = 0 "+
                     " And GateInward.MillCode="+iMillCode+
                     " Group by Supplier.Name,GateInward.Sup_Code "+
                     " Order By 1 ";

               ResultSet result3 = stat.executeQuery(QS1);
               while(result3.next())
               {
                    VPartyName.addElement(result3.getString(1));
                    VPartyCode.addElement(result3.getString(2));
               }
               result3.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("Party:"+ex);
               System.exit(0);
          }
     }

private void createPDFFile() {
        try {
           
            document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(PDFFile));
            document.setPageSize(PageSize.A4.rotate());
            
            document.open();

            table = new PdfPTable(10);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            table.setHeaderRows(1);
            


            addHead(document, table);
            addBody(document, table);

          String SDateTime = common.getServerDateTime2();
          String SEnd = "Report Taken on "+SDateTime+" ";

           Paragraph paragraph;

            String Str1 = SEnd;
            //String Str2   = "Deduction Details";

            paragraph = new Paragraph(Str1, bigbold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            paragraph.setSpacingAfter(10);
            document.add(paragraph);
            document.close();
            JOptionPane.showMessageDialog(null, "PDF File Created in " + PDFFile, "Info", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addHead(Document document, PdfPTable table) throws BadElementException {
        try {

         
           
            document.newPage();
            Paragraph paragraph;

            String Str1 = "Company : AMARJOTHI SPINNING MILLS LTD ";
            //String Str2   = "Deduction Details";

            paragraph = new Paragraph(Str1, bigbold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            paragraph.setSpacingAfter(10);
            document.add(paragraph);//Document : RDC List 
            
            String Str2 = "Materials Pending @Gate "+JCFilter.getSelectedItem()+" As On "+TDate.toString()+"";
            //String Str2   = "Deduction Details";

            paragraph = new Paragraph(Str2, bigbold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            paragraph.setSpacingAfter(10);
            document.add(paragraph);
            
           

            PdfPCell c1;

                AddCellIntoTable("GI DATE", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold,1);
                AddCellIntoTable("GI NO", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold,1);
                AddCellIntoTable("Mat code", table, Element.ALIGN_CENTER, Element.ALIGN_RIGHT, 1, 4, 1, 8, 2, mediumbold,1);
                AddCellIntoTable("Mat Name", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold,1);
               // AddCellIntoTable(String.valueOf( common.parseNull((String)VSupName .elementAt(i))), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,1); 
                AddCellIntoTable("Inv NO", table, Element.ALIGN_CENTER, Element.ALIGN_RIGHT, 1, 4, 1, 8, 2, mediumbold,1);
                AddCellIntoTable("Inv Date", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold,1);
                AddCellIntoTable("DC No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold,1);
                AddCellIntoTable("DC Date", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold,1); 
                AddCellIntoTable("Inv/DC Qty", table, Element.ALIGN_CENTER, Element.ALIGN_RIGHT, 1, 4, 1, 8, 2, mediumbold,1);
                AddCellIntoTable("Delay Days", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold,1);
             
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    private void addBody(Document document, PdfPTable table) throws BadElementException {
        try {
 
           RowData = new Object[VGIDate.size()][ColumnData.length];
         
               
            for(int i=0;i<VGIDate.size();i++)
            {

              
                  RowData[i][0]  = common.parseNull((String)VGIDate  .elementAt(i));
                  RowData[i][1]  = common.parseNull((String)VGINo    .elementAt(i));
                  RowData[i][2]  = common.parseNull((String)VPCode   .elementAt(i));
                  RowData[i][3]  = common.parseNull((String)VPName   .elementAt(i));
                  RowData[i][4]  = common.parseNull((String)VSupName .elementAt(i));
                  RowData[i][5]  = common.parseNull((String)VInvNo   .elementAt(i));
                  RowData[i][6]  = common.parseNull((String)VInvDate .elementAt(i));
                  RowData[i][7]  = common.parseNull((String)VDCNo    .elementAt(i));
                  RowData[i][8]  = common.parseNull((String)VDCDate  .elementAt(i));
                  RowData[i][9]  = common.parseNull((String)VQty     .elementAt(i));
                  RowData[i][10] = common.parseNull((String)VDueDays .elementAt(i));

              String soldname="" ,sname="";
                 sname  = common.parseNull((String)VSupName .elementAt(i));       
           if(i>0){
                  soldname  = common.parseNull((String)VSupName .elementAt(i-1));        
                   
                  }
   if(!soldname.equals(sname) || i==0){
                AddCellIntoTable( "NAME:"+sname+"", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,10, 4, 1, 8, 2, mediumbold,1);}
       
                
                AddCellIntoTable(String.valueOf(common.parseNull((String)VGIDate  .elementAt(i))), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,1);
                AddCellIntoTable(String.valueOf(common.parseNull((String)VGINo    .elementAt(i))), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,1);
                AddCellIntoTable(String.valueOf(common.parseNull((String)VPCode   .elementAt(i))), table, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT, 1, 4, 1, 8, 2, smallnormal,1);
                AddCellIntoTable(String.valueOf(common.parseNull((String)VPName   .elementAt(i))), table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,1);
               // AddCellIntoTable(String.valueOf( common.parseNull((String)VSupName .elementAt(i))), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,1); 
                AddCellIntoTable(String.valueOf(common.parseNull((String)VInvNo   .elementAt(i))), table, Element.ALIGN_LEFT, Element.ALIGN_RIGHT, 1, 4, 1, 8, 2, smallnormal,1);
                AddCellIntoTable(String.valueOf(common.parseNull((String)VInvDate .elementAt(i))), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,1);
                AddCellIntoTable(String.valueOf(common.parseNull((String)VDCNo    .elementAt(i))), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,1);
                AddCellIntoTable(String.valueOf(common.parseNull((String)VDCDate  .elementAt(i))), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,1); 
                AddCellIntoTable(String.valueOf(common.parseNull((String)VQty     .elementAt(i))), table, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT, 1, 4, 1, 8, 2, smallnormal,1);
                AddCellIntoTable(String.valueOf(common.parseNull((String)VDueDays .elementAt(i))), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,1);
             
            }


            document.add(table);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("ex" + ex);
        }
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
 public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    
    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

}
