package GRN;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class GIRDCLinkFrame extends JInternalFrame
{
     
     JLayeredPane   Layer;
     Vector         VInwNo,VInwName;
     TabReport      tabreportx;
     TabReport      tabreport;

     Vector         VCode,VName,VInwNox;
     Vector         VDesc8,VMatName8,VQty8,VSup_Name8;

     JButton        BMaterial,BOk;
     JTextField     TMatCode,TSupplier,TMaterial;
     JDialog        theDialog;
     NextField      TQty;

     JPanel         TopPanel,MiddlePanel,BottomPanel,CashPanel;
     JComboBox      JCType;

     int            index = 0;
     int            iMillCode;

     Common common = new Common();

     GIRDCLinkFrame(JLayeredPane Layer,Vector VInwNo,Vector VInwName,TabReport tabreportx,Vector VInwNox,Vector VDesc8,Vector VMatName8,Vector VQty8,Vector VSup_Name8,JDialog theDialog,int iMillCode)
     {
          
          this.Layer      = Layer;
          this.VInwNo     = VInwNo;
          this.VInwName   = VInwName;
          this.tabreportx = tabreportx;
          this.VInwNox    = VInwNox;
          this.VDesc8     = VDesc8;
          this.VMatName8  = VMatName8;
          this.VQty8      = VQty8;
          this.VSup_Name8 = VSup_Name8;
          this.theDialog  = theDialog;
          this.iMillCode  = iMillCode;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          BMaterial      = new JButton("Material");
          BOk            = new JButton("Okay");
          JCType         = new JComboBox(VInwName);
          TQty           = new NextField();
          TMaterial      = new JTextField();
          TSupplier      = new JTextField();
          TopPanel       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BottomPanel    = new JPanel(true);
          CashPanel      = new JPanel(true);
     }

     private void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(5,2));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,350,300);
     }

     private void addComponents()
     {
          TopPanel       .add(new JLabel("Select Material"));
          TopPanel       .add(BMaterial);
          TopPanel       .add(new JLabel("Supplier"));
          TopPanel       .add(TSupplier);
          TopPanel       .add(new JLabel("Inward Type"));
          TopPanel       .add(JCType);
          TopPanel       .add(new JLabel("Description @Gate"));
          TopPanel       .add(TMaterial);
          TopPanel       .add(new JLabel("Qty"));
          TopPanel       .add(TQty);

          BottomPanel    .add(BOk);

          getContentPane().add("North",TopPanel);
          getContentPane().add("South",BOk);
		 
          CashPanel      .setLayout(new BorderLayout()); 
          CashPanel      .add("North",TopPanel);
          CashPanel      .add("Center",MiddlePanel);
          CashPanel      .add("South",BOk);

          setPresets();
     }

     private void setPresets()
     {
          index       = tabreportx.ReportTable.getSelectedRow(); 
          int iType   = common.toInt((String)VInwNox.elementAt(index));

          JCType    .setSelectedIndex(iType);

          TMaterial .setText((String)VDesc8.elementAt(index)); 
          TQty      .setText((String)VQty8.elementAt(index));
          TSupplier .setText((String)VSup_Name8.elementAt(index));

          TMaterial .setEditable(false);
          TSupplier .setEditable(false);
          JCType.setEnabled(false);
     }

     private void addListeners()
     {
         //BMaterial  .addActionListener(new MaterialList(this));
         BOk        .addActionListener(new ActList());
     }
               
     /*private class MaterialList implements ActionListener
     {

          GIRDCLinkFrame girdclinkframe;

          public MaterialList(GIRDCLinkFrame girdclinkframe)
          {
               this.girdclinkframe = girdclinkframe;
          }
          public void actionPerformed(ActionEvent ae)
          {
               
               Frame dummy = new Frame();
               JDialog theDialog = new JDialog(dummy,"Description Dialog",true);

               RDCMaterialSearch MS = new RDCMaterialSearch(Layer,BMaterial,theDialog,girdclinkframe);
               theDialog.getContentPane().add(MS.MaterialPanel);
               theDialog.setBounds(80,100,450,350);
               theDialog.setVisible(true);
          }
     }*/
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(false);

               String SMatName = BMaterial.getText();
               String SSupplier= TSupplier.getText();

               tabreportx.ReportTable.setValueAt(new Boolean(true),index,6);

               if(SMatName.equals("Material"))
               {
                    SMatName = TMaterial.getText();
               }
               tabreportx.ReportTable.setValueAt(SMatName,index,3);
               tabreportx.ReportTable.setValueAt(TQty.getText(),index,4);

               VSup_Name8.setElementAt(SSupplier,index);
               removeHelpFrame();
               tabreportx.ReportTable.requestFocus();
          }
     }

     private void removeHelpFrame()
     {
          try{theDialog.setVisible(false);}catch(Exception e){}
          
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }


}
