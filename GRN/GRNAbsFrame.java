package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GRNAbsFrame extends JInternalFrame
{
     String         SStDate,SEnDate;
     JComboBox      JCOrder,JCFilter;
     JButton        BApply,BPrint,BList,BLaserPrint;
     NextField      TNo;
     JPanel         TopPanel;
     JPanel         BottomPanel;

     Vector         VSelectdGrn,VSelectdGI;
     
     TabReport      tabreport;
     DateField      TStDate;
     DateField      TEnDate;
     
     Object         RowData[][];
     String         ColumnData[]   = {"Click To Print","GRN No","Date","Supplier","GI No","GI Date","Inv No","Inv Date","DC No","DC Date","Net","PayTerm"};
     String         ColumnType[]   = {"B"             ,"S"     ,"S"   ,"S"       ,"N"    ,"S"      ,"S"     ,"S"       ,"S"    ,"S"      ,"N"  ,"S"};
     Common         common         = new Common();
     
     Vector         VGrnNo,VGRNDate,VSupName,VGateInNo,VGateInDate,VInvNo,VInvDate,VDCNo,VDCDate,VGValue;
     Vector         VSupCode,VPayTerm,VOrderNo,VEntryStatus;

     Vector         VSeleGINo,VSeleGIDate,VSeleSupName,VSeleInvNo,VSeleInvDate,VSeleDCNo,VSeleDCDate,VSeleInwType,VSeleInwMode,VSeleCategory,VSeleRemarks,VSeleTime;
     
     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     Vector         VCode,VName;
     int            iMillCode;
     String         SYearCode;
     String         SItemTable,SSupTable,SMillName;
     
     JScrollPane    TabScroll;

     PrinterComm    Printer;
     
     public GRNAbsFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,String SYearCode,String SItemTable,String SSupTable,String SMillName)
     {    
          super("Abstract on GRNs During a Period");
          this . DeskTop      = DeskTop;
          this . VCode        = VCode;
          this . VName        = VName;
          this . SPanel       = SPanel;
          this . iMillCode    = iMillCode;
          this . SYearCode    = SYearCode;
          this . SItemTable   = SItemTable;
          this . SSupTable    = SSupTable;
          this . SMillName    = SMillName;
          
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TStDate        = new DateField();
          TEnDate        = new DateField();
          BApply         = new JButton("Apply");
          BPrint         = new JButton("Print Marked GRNs");
          BList          = new JButton("Print Marked Lists");
          BLaserPrint    = new JButton("Print");

          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          JCOrder        = new JComboBox();
          JCFilter       = new JComboBox();
          TNo            = new NextField(5);
          
          TStDate        . setTodayDate();
          TEnDate        . setTodayDate();
          Printer        = new PrinterComm();
     }
     
     public void setLayouts()
     {
          TopPanel  . setLayout(new FlowLayout());

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,725,500);
     }

     public void addComponents()
     {
          JCOrder             . addItem("GRN No");
          JCOrder             . addItem("Supplierwise");
          
          JCFilter            . addItem("All");
          JCFilter            . addItem("Invoice Received");
          JCFilter            . addItem("Invoice Not Received");
          
          TopPanel            . add(TNo);
          
          TopPanel            . add(new JLabel("Sorted On"));
          TopPanel            . add(JCOrder);
          
          TopPanel            . add(new JLabel("Show Only"));
          TopPanel            . add(JCFilter);
          
          TopPanel            . add(TStDate);
          TopPanel            . add(TEnDate);
          TopPanel            . add(BApply);
          
          BottomPanel         . add(BPrint);
          BottomPanel         . add(BList);
          BottomPanel         . add(BLaserPrint);

          BPrint              . setEnabled(false);
          BList               . setEnabled(false);
          BLaserPrint         . setEnabled(false);

          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply         . addActionListener(new ApplyList());
          BPrint         . addActionListener(new PrintList());
          BList          . addActionListener(new List());
          BLaserPrint    . addActionListener(new LaserPrintList());
     }

     private class LaserPrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setPrint();

               try
               {
                    Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler "+common.getPrintPath()+"GRNGIPrint.html");
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
          }
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setPrint();
          }
     }

     private void setPrint()
     {
          try
          {
               FileWriter FWHtml             = new FileWriter(common.getPrintPath()+"GRNGIPrint.html");

               // start Html Print..

               FWHtml.write("<html> ");
               FWHtml.write("<head> ");
               FWHtml.write("<title></title> ");
               FWHtml.write("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'> ");
               
               FWHtml.write("     <style type='text/css'> ");
               FWHtml.write("          table           {border-collapse: collapse;} ");
               FWHtml.write("          P.pagebreakhere {page-break-before: always;} ");
               FWHtml.write("    </style> ");
               FWHtml.write("</head> ");
               
               FWHtml.write("<body onload='javascript:self.print()'> ");

               // set Print..

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean BValue = (Boolean)RowData[i][0];
                    if(BValue.booleanValue())
                    {
                         String SEntryStatus = (String)VEntryStatus.elementAt(i);

                         VSelectdGrn        . addElement((String)RowData[i][1]);
                         VSelectdGI         . addElement((String)RowData[i][4]);

                         if(SEntryStatus.equals("1"))
                         {
                              String SGINo = (String)RowData[i][4];

                              setGIVector(SGINo);
                         }
                    }
               }
               new GrnAbsPrint(VSelectdGrn,VSelectdGI,VSeleGINo,VSeleGIDate,VSeleSupName,VSeleInvNo,VSeleInvDate,VSeleDCNo,VSeleDCDate,VSeleInwType,VSeleInwMode,VSeleCategory,VSeleRemarks,VSeleTime,iMillCode,SSupTable,SMillName,FWHtml);

               // close Html Print..

               FWHtml.write("</body>");
               FWHtml.write("</html>");
               FWHtml.close();

               removeHelpFrame();
          }
          catch(Exception ex){};
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop   . remove(this);
               DeskTop   . repaint();
               DeskTop   . updateUI();
          }
          catch(Exception ex) { }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               VSelectdGrn   = new Vector();
               VSelectdGI    = new Vector();

               VSeleGINo     = new Vector();
               VSeleGIDate   = new Vector();
               VSeleSupName  = new Vector();
               VSeleInvNo    = new Vector();
               VSeleInvDate  = new Vector();
               VSeleDCNo     = new Vector();
               VSeleDCDate   = new Vector();
               VSeleInwType  = new Vector();
               VSeleInwMode  = new Vector();
               VSeleCategory = new Vector();
               VSeleRemarks  = new Vector();
               VSeleTime     = new Vector();

               setDataIntoVector();
               setRowData();

               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}

               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    tabreport           . ReportTable.addKeyListener(new TabKeyList());
                    getContentPane()    . add(tabreport,BorderLayout.CENTER);
                    updateUI();
                    setSelected(true);
                    DeskTop             . repaint();
                    DeskTop             . updateUI();

                    BPrint              . setEnabled(true);
                    BList              . setEnabled(true);
                    BLaserPrint         . setEnabled(true);
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public class List implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               printData();
          }

     } 
     public class TabKeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_F2)
                    setStatus(true);
               if(ke.getKeyCode()==KeyEvent.VK_F3)
                    setStatus(false);
          }
     }

     public void setStatus(boolean bFlag)
     {
          for(int i=0;i<RowData.length;i++)
               RowData[i][0] = new Boolean(bFlag);
          tabreport.ReportTable.updateUI();
     }

     public void printData()
     {
            String STitle = "GRN Abstract Report Taken On: " +common.getServerDate(); 

            String SFile  = "PrintList.prn";
            new DocPrint1(getPendingBody(),getPendingHead(),STitle,SFile,iMillCode,SMillName,2);
     }

     public Vector getPendingBody()
     {
        Vector vect = new Vector();

        int iCount=0;


          for(int i=0;i<RowData.length;i++)
          {
               Boolean BValue = (Boolean)RowData[i][0];

               if(!BValue.booleanValue())
                    continue;

              iCount++;


              String strl="";

              String SGRNNo       = (String)VGrnNo.elementAt(i);
              String SGRNDate     = common.parseDate((String)VGRNDate.elementAt(i));
              String SSupplier    = common.parseNull((String)VSupName.elementAt(i));
              String SInvNo       = (String)VInvNo.elementAt(i);
              String SInvDate     = common.parseDate((String)VInvDate.elementAt(i));
              String SDCNo        = (String)VDCNo.elementAt(i);
              String SDCDate      = common.parseDate((String)VDCDate.elementAt(i));
              String SNet         = common.getRound(common.toDouble((String)VGValue.elementAt(i)),2);

              strl = strl + common.Pad(String.valueOf(iCount),5)+common.Space(3);
              strl = strl + common.Pad(SGRNNo,9)+common.Space(3);
              strl = strl + common.Pad(SGRNDate,12)+common.Space(3);
              strl = strl + common.Pad(SSupplier,25)+common.Space(3);
              strl = strl + common.Pad(SInvNo,9)+common.Space(3);
              strl = strl + common.Pad(SInvDate,12)+common.Space(3);
              strl = strl + common.Pad(SDCNo,9)+common.Space(3);
              strl = strl + common.Pad(SDCDate,12)+common.Space(3);
              strl = strl + common.Pad(SNet,12)+common.Space(3);
              strl = strl + common.Pad("",10)+common.Space(3);
              strl = strl + common.Pad("",10)+common.Space(3);
              

              strl = strl+"\n";
              vect.addElement(strl);
        }
        return vect;
     }

     public Vector getPendingHead()
     {
           Vector vect = new Vector();

           String strl1 = "";

              strl1 = strl1 + common.Cad("S.No",5)+common.Space(3);
              strl1 = strl1 + common.Cad("GRN No",9)+common.Space(3);
              strl1 = strl1 + common.Cad("GRN Date",12)+common.Space(3);
              strl1 = strl1 + common.Cad("Supplier",25)+common.Space(3);
              strl1 = strl1 + common.Cad("Inv No",9)+common.Space(3);
              strl1 = strl1 + common.Cad("Inv Date",12)+common.Space(3);
              strl1 = strl1 + common.Cad("DC No",9)+common.Space(3);
              strl1 = strl1 + common.Cad("DC Date",12)+common.Space(3);
              strl1 = strl1 + common.Cad("Net",12)+common.Space(3);
              strl1 = strl1 + common.Cad("Given Date",10)+common.Space(3);
              strl1 = strl1 + common.Cad("Sign",10)+common.Space(3);

           strl1 = strl1+"\n";

           vect.addElement(strl1);

           return vect;
     } 


     public void setDataIntoVector()
     {
          VGrnNo         = new Vector();
          VGRNDate       = new Vector();
          VSupName       = new Vector();
          VGateInNo      = new Vector();
          VGateInDate    = new Vector();
          VInvNo         = new Vector();
          VInvDate       = new Vector();
          VDCNo          = new Vector();
          VDCDate        = new Vector();
          VSupCode       = new Vector();
          VOrderNo       = new Vector();
          VGValue        = new Vector();
          VPayTerm       = new Vector();
          VEntryStatus   = new Vector();

          String StDate = TStDate.toNormal();
          String EnDate = TEnDate.toNormal();

          try
          {
               ORAConnection   oraConnection = ORAConnection.getORAConnection();
               Connection      theConnection = oraConnection.getConnection();
               Statement       stat          = theConnection.createStatement();
               
               String    QString   = getQString(StDate,EnDate);
               ResultSet res       = stat.executeQuery(QString);
               while (res.next())
               {
                    String str = res.getString(6);

                    if(JCFilter.getSelectedIndex()==1)
                         if(((str.trim()).length())==0)
                              continue;

                    if(JCFilter.getSelectedIndex()==2)
                         if(((str.trim()).length())>0)
                              continue;

                    VGrnNo         . addElement(common.parseNull((String)res.getString(1)));
                    VGRNDate       . addElement(common.parseNull((String)res.getString(2)));
                    VSupName       . addElement(common.parseNull((String)res.getString(3)));
                    VGateInNo      . addElement(common.parseNull((String)res.getString(4)));
                    VGateInDate    . addElement(common.parseNull((String)res.getString(5)));
                    VInvNo         . addElement(common.parseNull((String)res.getString(6)));
                    VInvDate       . addElement(common.parseNull((String)res.getString(7)));
                    VDCNo          . addElement(common.parseNull((String)res.getString(8)));
                    VDCDate        . addElement(common.parseNull((String)res.getString(9)));
                    VSupCode       . addElement(common.parseNull((String)res.getString(10)));
                    VOrderNo       . addElement(common.parseNull((String)res.getString(11))); 
                    VGValue        . addElement(common.parseNull((String)res.getString(12)));
                    VEntryStatus   . addElement(common.parseNull((String)res.getString(13)));
                    VPayTerm       . addElement(" ");
               }
               res.close();

               for (int i=0;i<VGrnNo.size();i++)
               {
                    ResultSet result    = stat.executeQuery("Select PayTerms From PurchaseOrder Where MillCode="+iMillCode+" and OrderNo = "+(String)VOrderNo.elementAt(i));
                    String    SPayTerm  = "";
                    while(result.next())
                    {
                         SPayTerm  = common.parseNull((String)result.getString(1));
                         VPayTerm  . setElementAt(SPayTerm,i);
                    }
                    result.close();
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("SQL 1"+ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VGrnNo.size()][ColumnData.length];
          for(int i=0;i<VGRNDate.size();i++)
          {
               RowData[i][0]       = new Boolean(true);
               RowData[i][1]       = common.parseNull((String)VGrnNo         . elementAt(i));
               RowData[i][2]       = common.parseDate(   common.parseNull((String)VGRNDate       . elementAt(i)));
               RowData[i][3]       = common.parseNull((String)VSupName       . elementAt(i));
               RowData[i][4]       = common.parseNull((String)VGateInNo      . elementAt(i));
               RowData[i][5]       = common.parseDate(   common.parseNull((String)VGateInDate    . elementAt(i)));
               RowData[i][6]       = common.parseNull((String)VInvNo         . elementAt(i));
               RowData[i][7]       = common.parseDate(   common.parseNull((String)VInvDate       . elementAt(i)));
               RowData[i][8]       = common.parseNull((String)VDCNo          . elementAt(i));
               RowData[i][9]       = common.parseDate(   common.parseNull((String)VDCDate        . elementAt(i)));
               RowData[i][10]      = common.parseNull((String)VGValue        . elementAt(i));
               RowData[i][11]      = common.parseNull((String)VPayTerm       . elementAt(i));
          }  
     }

     public String getQString(String StDate,String EnDate)
     {
          int       iNo       =    common.toInt(TNo.getText());
          String    QString   =    "";

                    QString   =    " SELECT GrnNo,GRNDate,Name,max(GateInNo),max(GateInDate),max(InvNo),max(InvDate),max(DCNo),max(DCDate),"+
                                   " Sup_Code,Max(OrderNo),sum(GValue),Max(EntryStatus) from (  SELECT GRN.GrnNo,"+
                                   " GRN.GRNDate,"+SSupTable+".Name,GRN.GateInNo,GRN.GateInDate,GRN.InvNo,GRN.InvDate,"+
                                   " GRN.DCNo,GRN.DCDate,Grn.Sup_Code,"+
                                   " GRN.MillCode,max(Grn.OrderNo) as OrderNo,sum(Grn.InvNet+Grn.Misc) as GValue,max(Grn.EntryStatus) as EntryStatus "+
                                   " From GRN Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = GRN.Sup_Code";
                QString   = QString+" INNER JOIN InvItems ON GRN.Code = InvItems.Item_Code  and hsntype=0";
          if(iNo > 0)
               QString   = QString+" and GRN.GRNNo = "+iNo;
          else
               QString   = QString+" and GRN.GRNDate >= '"+StDate+"' and GRN.GRNDate <='"+EnDate+"' ";

               QString   = QString+" and GRN.millcode = "+iMillCode;

               QString   = QString+" Group By GRN.GrnNo,GRN.GRNDate,"+SSupTable+".Name,GRN.GateInNo,GRN.GateInDate,GRN.InvNo,GRN.InvDate,GRN.DCNo,GRN.DCDate,Grn.Sup_Code,GRN.MillCode ";

          if(JCOrder.getSelectedIndex() == 0)      
               QString = QString+" Order By GRN.GRNDate,GRN.GrnNo) group by GrnNo,GRNDate,Name,Sup_Code Order by GrnNo ";
          if(JCOrder.getSelectedIndex() == 1)      
               QString = QString+" Order By "+SSupTable+".Name,GRN.GrnDate) group by GrnNo,GRNDate,Name,Sup_Code Order by Name,GrnNo ";

          return QString;
     }

     public void setGIVector(String SGINo)
     {
           try
           {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();

              String QString = getGIString(SGINo);
              ResultSet res  = theStatement.executeQuery(QString);
              while (res.next())
              {
                 String SCategory = "Without P.O.";

                 String STime = "";
                 String str2 = common.parseNull(res.getString(11));
                 STime = str2.substring(11,19);

                 VSeleGINo    .addElement(common.parseNull(res.getString(1)));
                 VSeleGIDate  .addElement(common.parseDate(res.getString(2)));
                 VSeleSupName .addElement(common.parseNull(res.getString(3)));
                 VSeleInvNo   .addElement(common.parseNull(res.getString(4)));
                 VSeleInvDate .addElement(common.parseDate(res.getString(5)));
                 VSeleDCNo    .addElement(common.parseNull(res.getString(6)));
                 VSeleDCDate  .addElement(common.parseDate(res.getString(7)));
                 VSeleInwType .addElement(common.parseNull(res.getString(8)));
                 VSeleInwMode .addElement(common.parseNull(res.getString(9)));
                 VSeleCategory.addElement(SCategory);
                 VSeleRemarks .addElement(common.parseNull(res.getString(10)));
                 VSeleTime    .addElement(STime);
              }
              res.close();
              theStatement.close();
           }
           catch(Exception ex){System.out.println(ex);}
     }

     public String getGIString(String SGINo)
     {
          // Category -    0 - without P.O.    1 - with P.O.

          String QString = " SELECT GateInward.GINo, GateInward.GIDate, "+SSupTable+".Name,  GateInward.InvNo, GateInward.InvDate, GateInward.DcNo, GateInward.DcDate, InwType.InwName, InwardMode.Name, GateInward.Remarks, GateInward.UserTime "+
                           " FROM ((GateInward INNER JOIN "+SSupTable+" ON GateInward.Sup_Code = "+SSupTable+".Ac_Code and GateInward.GINo="+SGINo+
                           " and GateInward.MillCode="+iMillCode+" and GateInward.ModiStatus=0 "+
                           " and GateInward.Status=0 and GateInward.Authentication=1) "+
                           " INNER JOIN InwType ON GateInward.InwNo = InwType.InwNo) INNER JOIN InwardMode ON GateInward.ModeCode = InwardMode.Code "+
                           " Group by GateInward.GINo, GateInward.GIDate, "+SSupTable+".Name, GateInward.InvNo, GateInward.InvDate, GateInward.DcNo, GateInward.DcDate, InwType.InwName, InwardMode.Name, GateInward.Status, GateInward.ModiStatus,GateInward.Authentication,GateInward.Remarks,GateInward.UserTime,GateInward.MillCode ";

          return QString;
     }


}
