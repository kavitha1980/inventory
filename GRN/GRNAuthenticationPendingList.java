
package GRN;
import java.awt.BorderLayout;
import java.awt.*;
import java.awt.event.*;
import java.net.InetAddress;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.border.*;
import java.util.HashMap;
import java.util.Vector;
import java.util.*;
import guiutil.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import util.*;

public class GRNAuthenticationPendingList extends JInternalFrame{
  
        JPanel                         pnlMain,pnlFilter,pnlData,pnlGRNControl,
                                       pnlTitle, pnlTop, pnlNotes,pnlBottom,
                                       pnlGRNData,pnlData1;
        GRNAuthPendingModel            theModel;
        JTable                         theTable;
        
        JButton                        btnExit;
        JCheckBox                      chkDate;

        JLayeredPane                   Layer;
        GRNData                        grndata;
        Common                         common      = new Common();
	  int					   iMillCode;

        int             iSupName       = 1;
        int             iDesc          = 2;
        int             iUnit          = 3;
        int             iUser          = 4;
        int             iGRNNo         = 5;
        int             iInvNo         = 6;
        int             iQty           = 7;
    
 public GRNAuthenticationPendingList(JLayeredPane Layer, int iMillCode){
	
    this.Layer				= Layer;
    this.iMillCode			= iMillCode;
		
    createComponent();
    addLayout();
    addListener();
    addComponent();
    setColumn();
    setTableData();
 }  
 
 private void createComponent(){
 
    pnlTop                          = new JPanel();
    pnlMain                         = new JPanel();
    pnlTitle                        = new JPanel();
    pnlData                         = new JPanel();
    pnlGRNData                      = new JPanel();
    pnlData1                        = new JPanel();
    pnlGRNControl                   = new JPanel();
    pnlFilter                       = new JPanel();
    pnlNotes                        = new JPanel();
    pnlBottom                       = new JPanel();
    chkDate                         = new JCheckBox();
    chkDate                         . setSelected(true);
    btnExit                         = new JButton("Exit");
    theModel                        = new GRNAuthPendingModel();
    theTable                        = new JTable(theModel);
    
 }
 
 private void addLayout(){
     
    pnlMain                            . setLayout(new BorderLayout());
    pnlTitle                           . setLayout(new FlowLayout());
    pnlFilter                          . setBorder(new TitledBorder("Filtter"));
    pnlFilter                          . setLayout(new GridLayout(1,7));
    
    pnlData                            . setBorder(new TitledBorder("GRN AUTHENTICATION PENDING LIST"));
    pnlData                            . setLayout(new BorderLayout());
    
    pnlGRNControl                      . setBorder(new TitledBorder("Control"));
    pnlGRNControl                      . setLayout(new FlowLayout());
    pnlNotes                           . setLayout(new GridLayout(2,1));
    
    this.setSize(1000,550);
    this.setVisible(true);
    //this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    this.setTitle("GRN AUTHENTICATION PENDING LIST");
    this.setClosable(true);
    this.setMaximizable(true);
     
 }
 private void addListener(){
     btnExit                            . addActionListener(new ExtList());
 }   
 
 private void addComponent(){
     
     pnlGRNControl                      . add(btnExit);
//     pnlBottom                          . add(pnlGRNControl,BorderLayout.NORTH);
     
     pnlData                            . add(new JScrollPane(theTable));
    
    pnlMain                             . add(pnlData,BorderLayout.NORTH);
    pnlMain                             . add(pnlGRNControl ,BorderLayout.SOUTH);
    
    this                                . add(pnlMain);
    
}
 
 private class ActList implements ActionListener{
     public void actionPerformed(ActionEvent ae){
         
     }
 }

 private class ExtList implements ActionListener{
    public void actionPerformed(ActionEvent e){
             if(e.getSource()==btnExit){
                 dispose();
      }
    }
 }
private void setColumn(){
     
     TableColumn        column          = null;
     for(int i=0;i<theModel.getColumnCount();i++){
         column     = theTable.getColumnModel().getColumn(i);
       if(i==0)  {
         column.setPreferredWidth(50);  
       }
       if(i==1)  {
         column.setPreferredWidth(100);  
       }
       if(i==2)  {
         column.setPreferredWidth(100);  
       }
       if(i==3)  {
         column.setPreferredWidth(250);  
       }
       
       if(i==4)  {
         column.setPreferredWidth(100);  
       }
     }
 }
     
 
 private void setTableData(){
     theModel.setNumRows(0);
     int iNo=0;
     int iMillCode          = 0;
     grndata                = new GRNData();
     grndata.setGRNPendingList(iMillCode);
     for(int i=0;i<grndata.APendingList.size();i++){
       GRNClass           grnclass        = (GRNClass)grndata.APendingList.get(i);
         for(int j=0;j<grnclass.AGRNPendingList.size();j++)     {
             HashMap        theMap        = (HashMap)grnclass.AGRNPendingList.get(j);
             Vector         theVect       = new Vector();
             
             theVect                      . addElement(i+1);
             theVect                      . addElement(theMap.get("SupplierName"));
             theVect                      . addElement(theMap.get("ItemCode"));
             theVect                      . addElement(theMap.get("ItemName"));
             theVect                      . addElement(theMap.get("User"));
             theVect                      . addElement(theMap.get("Unit"));
             theVect                      . addElement(theMap.get("MRSNo"));
             theVect                      . addElement(theMap.get("GRNNo"));
             theVect                      . addElement(common.parseDate(String.valueOf(theMap.get("GRNDate"))));
             theVect                      . addElement(theMap.get("InvNo"));
             theVect                      . addElement(common.parseDate(String.valueOf(theMap.get("InvDate"))));
             theVect                      . addElement(theMap.get("Qty"));
             theVect                      . addElement(theMap.get("InvNet"));
             
             theModel.appendRow(theVect);
         }
     }
 }
 /*public static void main (String args[]){
     GRNAuthenticationPendingList       list    = new GRNAuthenticationPendingList();
 }*/
 
}
