
package GRN;
import java.util.HashMap;
import java.util.*;
import util.*;

public class GRNClass {
    
    Common      common            = new Common();
    ArrayList                     AGRNPendingList;    
    
 public GRNClass(){

     AGRNPendingList             =  new ArrayList();
 }   
 
 
public void appendGRNPendingList(String sSupName,String sItemName,String sUnit,String sGRNNo,String sGRNDate,String sInvNo,String sInvDate,String sInvQty,String sUserName,String sCode,String sInvNet,String sMRSNo){
     HashMap                theMap      = new HashMap();
     
     theMap.put("SupplierName", sSupName);
     theMap.put("ItemName", sItemName);
     theMap.put("Unit", sUnit);
     theMap.put("GRNNo", sGRNNo);
     theMap.put("GRNDate", sGRNDate);
     theMap.put("InvNo", sInvNo);
     theMap.put("InvDate", sInvDate);
     theMap.put("Qty", sInvQty);
     theMap.put("User", sUserName);
     theMap.put("ItemCode", sCode);
     theMap.put("MRSNo", sMRSNo);
     theMap.put("InvNet", sInvNet);
     
     AGRNPendingList.add(theMap); 
     
 }   
  
 
}
