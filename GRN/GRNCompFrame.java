package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GRNCompFrame extends JInternalFrame
{
     CriteriaPanel  criteriapanel;
     TabReport      tabreport;
     Object         RowData[][];
     String         ColumnData[]= {"Material Code","Material Name","April","May","June","July","August","September","October","November","December","January","Febraury","March"}; 
     String         ColumnType[]= {"S","S","N","N","N","N","N","N","N","N","N","N","N","N"};  
     Vector         VMatRec;
     Common         common = new Common();
     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     Vector         VCode,VName;
     int            iMillCode;
     String         SStDate,SEnDate;
     String         SSupTable;

     public GRNCompFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,String SStDate,String SEnDate,String SSupTable)
     {
          super("Monthly Comparison on GRN's");

          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.VCode     = VCode;
          this.VName     = VName;
          this.iMillCode = iMillCode;
          this.SStDate   = SStDate;
          this.SEnDate   = SEnDate;
          this.SSupTable = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          criteriapanel = new CriteriaPanel(DeskTop,VCode,VName,iMillCode,SSupTable);
     }

     public void setLayouts()
     {
          setClosable(true);
          setIconifiable(true);
          setResizable(true);
          setMaximizable(true);
          setBounds(0,0,650,500);
     }

     public void addComponents()
     {
          getContentPane().add(criteriapanel,BorderLayout.NORTH);
     }

     public void addListeners()
     {
          criteriapanel.BApply.addActionListener(new ApplyList());
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport); 
               }
               catch(Exception ex)
               {
               }
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    getContentPane()    . add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    DeskTop             . repaint();
                    DeskTop             . updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public void setDataIntoVector()
     {
                         VMatRec   = new Vector();
          MatMonRec      matmonrec = new MatMonRec();
          String         SMatCode  = "";

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();
               Statement      stat           = theConnection.createStatement();
               ResultSet      res            = stat.executeQuery(getQString());

               while (res.next())
               {
                    int       iMon      = res.getInt(1);  
                    String    str2      = res.getString(2);
                    String    str3      = res.getString(3);
                    String    str4      = res.getString(4);
                    String    str5      = res.getString(5);

                    if(!str2.equals(SMatCode))
                    {
                         VMatRec   . addElement(matmonrec); 
                         matmonrec = new MatMonRec();
                         matmonrec . SMatCode = str2;
                         matmonrec . SMatName = str3;
                         SMatCode  = str2;
                    }
                    matmonrec.setQty(str4,iMon);
                    matmonrec.setValue(str5,iMon);
               }
               VMatRec   . addElement(matmonrec);
               VMatRec   . removeElementAt(0);
               res.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VMatRec.size()][14];
          for(int i=0;i<VMatRec.size();i++)
          {
               MatMonRec matmonrec      = (MatMonRec)VMatRec.elementAt(i);
                         RowData[i][0]  = (String)matmonrec.SMatCode;
                         RowData[i][1]  = (String)matmonrec.SMatName;

               if(criteriapanel.JRQty.isSelected())
               {
                    for(int j=0;j<matmonrec.VQty.size();j++)
                         RowData[i][j+2] = (String)matmonrec.VQty.elementAt(j);
               }

               if(criteriapanel.JRValue.isSelected())
               {
                    for(int j=0;j<matmonrec.VValue.size();j++)
                         RowData[i][j+2] = (String)matmonrec.VValue.elementAt(j);
               }
          }  
     }

     public String getQString()
     {
          String QString = "",HString   = "",str  = "",QS   = "";
          int    iSig    = 0;

          if(criteriapanel.JRSeleDept.isSelected())
          {                         
               QS        = QString+", GRN.Dept_Code ";
               HString   = "GRN.Dept_Code="+(String)criteriapanel.VDeptCode.elementAt(criteriapanel.JCDept.getSelectedIndex());
               iSig      = 1;            
          }
          if(criteriapanel.JRSeleGroup.isSelected())
          {
               QS        = QString+", GRN.Group_Code ";
               str       = "GRN.Group_Code="+(String)criteriapanel.VGroupCode.elementAt(criteriapanel.JCGroup.getSelectedIndex());
               HString   = (iSig==1 ? " "+HString+" and "+str:str);
               iSig      = 1;
          }
          if(criteriapanel.JRSeleUnit.isSelected())
          {
               QS        = QString+", GRN.Unit_Code ";
               str       = "GRN.Unit_Code="+(String)criteriapanel.VUnitCode.elementAt(criteriapanel.JCUnit.getSelectedIndex());
               HString   = (iSig==1 ? " "+HString+" and "+str:str);
               iSig      = 1;
          }
          if(criteriapanel.JRSeleSup.isSelected())
          {
               QS        = QString+", GRN.Sup_Code ";
               str       = "GRN.Sup_Code='"+criteriapanel.VSupCode.elementAt(criteriapanel.JCSupplier.getSelectedIndex())+"'";
               HString   = (iSig==1 ? " "+HString+" and "+str:str);
               iSig      = 1;
          }

          HString   = (iSig>0 ? " and "+HString : " ");
          QS        = (iSig>0 ? QS : " ");

          QString   =    " select GrnMonth,ItemCode,ItemName,sum(GQty),sum(GValue) from "+
                         " (select substr(GRN.GrnDate,5,2) as GrnMonth,GRN.Code as ItemCode, "+
                         " InvItems.Item_Name as ItemName,sum(GRN.GrnQty) as GQty, sum(Grn.GrnValue) as GValue "+
                         " from grn "+
                         " inner join "+SSupTable+" on "+SSupTable+".ac_code = grn.sup_code "+
                         " inner join invitems on grn.code = invitems.item_code "+
                         " inner join ordblock on grn.grnblock = ordblock.block "+
                         " Where GRN.GrnDate>='"+SStDate+"'"+
                         " and GRN.GrnDate<='"+SEnDate+"' and"+
                         " Grn.millcode  = "+iMillCode+" and rejflag = 0 "+HString+
                         " group  by substr(GRN.GrnDate,5,2),Code,Item_name"+QS+
                         " union all "+
                         " select substr(GRN.GrnDate,5,2) as GrnMonth,GRN.Code as ItemCode, "+
                         " InvItems.Item_Name as ItemName,sum(GRN.GrnQty) as GQty, sum(Grn.GrnValue) as GValue "+
                         " from grn "+
                         " inner join "+SSupTable+" on "+SSupTable+".ac_code = grn.sup_code "+
                         " inner join invitems on grn.code = invitems.item_code "+
                         " inner join ordblock on grn.grnblock=ordblock.block   "+
                         " Where GRN.REJDATE>='"+SStDate+"'"+
                         " and GRN.REJDATE<='"+SEnDate+"'  and "+
                         " grn.millcode = "+iMillCode+" and rejflag = 1 "+HString+
                         " group  by substr(GRN.GrnDate,5,2),Code,Item_name"+QS+
                         " ) "+
                         " group by GrnMonth,ItemCode,ItemName "+
                         " Order by ItemCode";

          return QString;
     }
}
