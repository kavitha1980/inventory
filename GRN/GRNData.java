
package GRN;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import java.util.ArrayList;
import java.sql.DriverManager;
import java.sql.Statement;
import util.*;
import jdbc.*;

public class GRNData {
    
 Connection    theConnection              = null;    
 Common         common                    = new Common();
 ArrayList      APendingList;
 GRNClass       grnclass;

public GRNData(){

    APendingList                   = new ArrayList();
}   

public void setGRNPendingList(int iMillCode)
  {
      StringBuffer      sb = new StringBuffer();
      
               sb.append("   Select Name,Item_Name,Unit_Name,GRNNo,GRNDate,InvNo,InvDate,InvQty,UserName,GRN.Code,InvNet,MRSNo from GRN");
               sb.append("   Inner Join InvItems on InvItems.Item_Code=Code");
               sb.append("   Inner Join Supplier on GRN.Sup_Code = Supplier.AC_Code ");
               sb.append("   Inner Join Unit on GRN.UNIT_CODE = Unit.Unit_Code");
               //sb.append("   Inner Join Department on GRN.DEPT_CODE = Department.DeptCode ");
               sb.append("   Inner Join RawUser on GRN.MRSAuthUserCode = RawUser.UserCode");
               sb.append("   Where GRNAUTHSTATUS=0 and MillCode="+iMillCode);
               sb.append("   Order by UserName");
        
              // System.out.println("GRNPending Qry:"+sb.toString());
               try
               {
                 if(theConnection == null)
                    {
                         ORAConnection jdbc    = ORAConnection.getORAConnection();
                         theConnection        = jdbc.getConnection();
                    }

                    Statement theStatement    = theConnection.createStatement();

                    ResultSet res = theStatement.executeQuery(sb.toString());
                    while(res.next())
                    {
                         String sSupName        = res.getString(1);
                         String sItemName       = res.getString(2);
                         String sUnit           = res.getString(3);
                         String sGRNNo          = res.getString(4);
                         String sGRNDate        = res.getString(5);
                         String sInvNo          = res.getString(6);
                         String sInvDate        = res.getString(7);
                         String sInvQty         = res.getString(8);
                         String sUserName       = res.getString(9);
                         String sCode           = res.getString(10);
                         String sInvNet         = res.getString(11);
                         String sMRSNo          = res.getString(12);
                         
                         grnclass                = new GRNClass();
                         grnclass                . appendGRNPendingList(sSupName,sItemName,sUnit,sGRNNo,sGRNDate,sInvNo,sInvDate,sInvQty,sUserName,sCode,sInvNet,sMRSNo);
                         APendingList            . add(grnclass);
                           
                    }
                    
                    res            . close();
                    theStatement   . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
      }

}
