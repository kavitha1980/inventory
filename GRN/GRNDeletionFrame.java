package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GRNDeletionFrame extends JInternalFrame
{
     JButton             BApply;
     JPanel              TopPanel,BottomPanel,DatePanel,ApplyPanel;
     JButton             BDelete;
     JLayeredPane        DeskTop;

     StatusPanel         SPanel;
     TabReport           tabreport;
     NextField           TStNo;
     Object              RowData[][];
     Common              common = new Common();
     Connection          theConnection  = null;
     Connection          theDConnection = null;

     String              ColumnData[] = {"GRN No","Date","Block","Supplier","Order No","G.I. No","G.I. Date","DC No","DC Date","Inv No","Inv Date","Code","Name","Inv Qty","Recd Qty","Inv Rate","Amount","Accepted Qty","Dept","Group","Unit","Status"};
     String              ColumnType[] = {"N","S","S","S","N","N","S","S","S","S","S","S","S","N","N","N","N","N","S","S","S","S"};

     Vector              VGrnNo,VGrnDate,VBlock,VSupName,VOrdNo,VDcNo,VDcDate,VGno,VGDate,VINo,VIDate,VGrnCode,VGrnName,VIQty,VRQty,VIRate,VIAmt,VAccQty,VGrnDeptName,VGrnCataName,VGrnUnitName,VStatus,VSPJNo,VId,VOrdQty,VMrsNo,VBlockCode;;
     Vector              VDGINo,VDBlock,VDOrderNo,VDQty,VGOrdSlNo,VGrnUserCode;
     Vector              VCode,VName;
     Vector              VICode,VIGrnBlock,VIGrnQty,VIGrnValue,VMrsUserCode;

     int                 iMillCode      = 0;
     String              SItemTable,SSupTable;
	int				iUserCode;

     int                 iInspection    = 0,iGrnNo     = 0;
	String			SGrnDate="";

     boolean             bComFlag       = true;

     int iGateCount=0;

     Vector VSUserCode,VSItemCode,VSGrnQty;

     public GRNDeletionFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,String SItemTable,String SSupTable,int iUserCode)
     {
          super("GRN Deletion");
          this.DeskTop    = DeskTop;
          this.SPanel     = SPanel;
          this.VCode      = VCode;
          this.VName      = VName;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
		this.iUserCode  = iUserCode;

          getConn();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void getConn()
     {
          try
          {
               ORAConnection  oraConnection = ORAConnection.getORAConnection();
                              theConnection = oraConnection.getConnection();
                              theConnection . setAutoCommit(false);

	          if(iMillCode==1)
	          {
	               DORAConnection jdbc           = DORAConnection.getORAConnection();
	                              theDConnection = jdbc.getConnection();
							theDConnection . setAutoCommit(false);
	          }
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void createComponents()
     {
          TStNo          = new NextField();
          BApply         = new JButton("Apply");
     
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          DatePanel      = new JPanel();
          ApplyPanel     = new JPanel();
     
          BDelete        = new JButton("Delete GRN");
          BDelete        . setEnabled(false);
     }

     public void setLayouts()
     {
          TopPanel       . setLayout(new GridLayout(1,2));
          DatePanel      . setLayout(new GridLayout(2,1));
          ApplyPanel     . setLayout(new BorderLayout());
     
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }

     public void addComponents()
     {
          DatePanel           . add(new JLabel("Grn No"));
          DatePanel           . add(TStNo);
     
          ApplyPanel          . add("Center",BApply);
     
          TopPanel            . add(DatePanel);
          TopPanel            . add(ApplyPanel);
     
          BottomPanel         . add(BDelete);
     
          DatePanel           . setBorder(new TitledBorder("Grn No"));
          ApplyPanel          . setBorder(new TitledBorder("Control"));
     
          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply   . addActionListener(new ApplyList());
          BDelete  . addActionListener(new DeleteList());
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(common.toInt(TStNo.getText())==0)
               {
                    JOptionPane    . showMessageDialog(null,"Invalid Grn No","Error Message",JOptionPane.INFORMATION_MESSAGE);
                    TStNo          . setText("");
                    TStNo          . requestFocus();
                    return;
               }

               getInspection();

               if(iGrnNo==0)
               {
                    JOptionPane    . showMessageDialog(null,"There is No GrnNo","Error Message",JOptionPane.INFORMATION_MESSAGE);
                    TStNo          . setText("");
                    TStNo          . requestFocus();
                    return;
               }    

               String SCurDate   = common.parseDate(common.getServerPureDate());

               int iDateDiff = common.toInt(common.getDateDiff(SCurDate,SGrnDate));

               if(iUserCode!=16)
               {
                    if(iDateDiff>25)
                    {
                         JOptionPane.showMessageDialog(null,"Grn Date Limit Exceeded - Contact EDP ","Attention",JOptionPane.INFORMATION_MESSAGE);
                         return;
                    }
               }

               if(iInspection==1)
               {
                    JOptionPane    . showMessageDialog(null,"Already Inspected","Error Message",JOptionPane.INFORMATION_MESSAGE);
                    TStNo          . setText("");
                    TStNo          . requestFocus();
                    return;
               }           

               getGateCount();
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    getContentPane()    . add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    DeskTop             . repaint();
                    DeskTop             . updateUI();
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
               if(RowData.length>0)
               {
                    BDelete.setEnabled(true);
                    TStNo.setEditable(false);
                    BApply.setEnabled(false);
               }
               else
               {
                    BDelete.setEnabled(false);
                    TStNo.setEditable(true);
                    BApply.setEnabled(true);
               }
          }
     }

     public void getInspection()
     {
          ResultSet result         = null;
                    iInspection    = 0;
                    iGrnNo         = 0;
				SGrnDate		= "";

          String SMillCond =  " And Grn.MillCode = "+iMillCode;

          String QS = " select distinct inspection,GrnNo,GrnDate from grn Where GRN.GRNNo ="+TStNo.getText()+SMillCond;

          try
          {
               Statement      stat          = theConnection.createStatement();
                              result        = stat.executeQuery(QS);

               while (result.next())
               {
                    iInspection = common.toInt(common.parseNull(String.valueOf(result.getInt(1))));
                    iGrnNo      = common.toInt(common.parseNull(String.valueOf(result.getInt(2))));
				SGrnDate    = common.parseDate(result.getString(3));
               }
               result    . close();
               stat      . close();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setDataIntoVector()
     {
          ResultSet      result         = null;
          
                         VGrnNo         = new Vector();
                         VGrnDate       = new Vector();
                         VBlock         = new Vector();
                         VSupName       = new Vector();
                         VOrdNo         = new Vector();
                         VGno           = new Vector();
                         VGDate         = new Vector();
                         VDcNo          = new Vector();
                         VDcDate        = new Vector();
                         VINo           = new Vector();
                         VIDate         = new Vector();
                         VGrnCode       = new Vector();
                         VGrnName       = new Vector();
                         VIQty          = new Vector();
                         VRQty          = new Vector();
                         VIRate         = new Vector();
                         VIAmt          = new Vector();
                         VAccQty        = new Vector();
                         VGrnDeptName   = new Vector();
                         VGrnCataName   = new Vector();
                         VGrnUnitName   = new Vector();
                         VStatus        = new Vector();
                         VSPJNo         = new Vector();
                         VId            = new Vector();
                         VOrdQty        = new Vector();
                         VMrsNo         = new Vector();
                         VBlockCode     = new Vector();
                         VGOrdSlNo      = new Vector();
                         VGrnUserCode   = new Vector();

          String         QString        = getQString();
          try
          {
               Statement      stat          = theConnection.createStatement();
                              result        = stat.executeQuery(QString);

               while (result.next())
               {
                    String str1  = result.getString(1);  
                    String str2  = result.getString(2);
                    String str3  = result.getString(3);
                    String str4  = result.getString(4);
                    String str5  = result.getString(5);
                    String str6  = result.getString(6);
                    String str7  = result.getString(7);
                    String str8  = result.getString(8);
                    String str9  = result.getString(9);
                    String str10 = result.getString(10);
                    String str11 = result.getString(11);
                    String str12 = result.getString(12);
                    String str13 = result.getString(13);
                    String str14 = result.getString(14);
                    String str15 = result.getString(15);
                    String str16 = result.getString(16);
                    String str17 = result.getString(17);
                    String str18 = result.getString(18);
                    String str19 = result.getString(19);
                    String str20 = result.getString(20);
                    String str21 = result.getString(21);
                    String str22 = result.getString(22);
                    String str23 = result.getString(23);
                    String str24 = result.getString(24);
                    String str25 = result.getString(25);

                    if(common.toInt(str22)>0)
                    {
                         JOptionPane    . showMessageDialog(null,"This Grn is Accounted","Error Message",JOptionPane.INFORMATION_MESSAGE);
                         TStNo          . setText("");
                         TStNo          . requestFocus();
                         return;
                    }
                    VGrnNo         . addElement(str1);
                    VGrnDate       . addElement(common.parseDate(str2));
                    VBlock         . addElement(str3);
                    VSupName       . addElement(str4);
                    VOrdNo         . addElement(str5);
                    VGno           . addElement(str6);
                    VGDate         . addElement(common.parseDate(str7));
                    VDcNo          . addElement(common.parseNull(str8));
                    VDcDate        . addElement(common.parseDate(str9));
                    VINo           . addElement(common.parseNull(str10));
                    VIDate         . addElement(common.parseDate(str11));
                    VGrnCode       . addElement(str12);
                    VGrnName       . addElement(str13);
                    VIQty          . addElement(str14);
                    VRQty          . addElement(str15);
                    VIRate         . addElement(str16);
                    VIAmt          . addElement(str17);
                    VAccQty        . addElement(str18);
                    VGrnDeptName   . addElement(common.parseNull(str19));
                    VGrnCataName   . addElement(common.parseNull(str20));
                    VGrnUnitName   . addElement(common.parseNull(str21));
                    VSPJNo         . addElement(str22);
                    VStatus        . addElement(" ");
                    VId            . addElement(str23);
                    VOrdQty        . addElement(str24);
                    VMrsNo         . addElement(str25);
                    VBlockCode     . addElement(result.getString(26));
                    VGOrdSlNo      . addElement(result.getString(27));
                    VGrnUserCode   . addElement(result.getString(28));
               }
          }
          catch(Exception ex){System.out.println(ex);
               ex.printStackTrace();

          }
     }

     public void getGateCount()
     {
          ResultSet result         = null;

          iGateCount=0;

          String SGrnNo = TStNo.getText();

          String QS = " Select Count(*) from GateInward Where ModiStatus=0 and MillCode="+iMillCode+" and GrnNumber="+SGrnNo;

          try
          {
               Statement      stat          = theConnection.createStatement();
                              result        = stat.executeQuery(QS);

               while (result.next())
               {
                    iGateCount = common.toInt(common.parseNull(String.valueOf(result.getInt(1))));
               }
               result    . close();
               stat      . close();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }


     public void setRowData()
     {
          RowData     = new Object[VGrnNo.size()][ColumnData.length];

          for(int i=0;i<VGrnNo.size();i++)
          {
               RowData[i][0]  = (String)VGrnNo         . elementAt(i);
               RowData[i][1]  = (String)VGrnDate       . elementAt(i);
               RowData[i][2]  = (String)VBlock         . elementAt(i);
               RowData[i][3]  = (String)VSupName       . elementAt(i);
               RowData[i][4]  = (String)VOrdNo         . elementAt(i);
               RowData[i][5]  = (String)VGno           . elementAt(i);
               RowData[i][6]  = (String)VGDate         . elementAt(i);
               RowData[i][7]  = (String)VDcNo          . elementAt(i);
               RowData[i][8]  = (String)VDcDate        . elementAt(i);
               RowData[i][9]  = (String)VINo           . elementAt(i);
               RowData[i][10] = (String)VIDate         . elementAt(i);
               RowData[i][11] = (String)VGrnCode       . elementAt(i);
               RowData[i][12] = (String)VGrnName       . elementAt(i);
               RowData[i][13] = (String)VIQty          . elementAt(i);
               RowData[i][14] = (String)VRQty          . elementAt(i);
               RowData[i][15] = (String)VIRate         . elementAt(i);
               RowData[i][16] = (String)VIAmt          . elementAt(i);
               RowData[i][17] = (String)VAccQty        . elementAt(i);
               RowData[i][18] = (String)VGrnDeptName   . elementAt(i);
               RowData[i][19] = (String)VGrnCataName   . elementAt(i);
               RowData[i][20] = (String)VGrnUnitName   . elementAt(i);
               RowData[i][21] = (String)VStatus        . elementAt(i);
          }
     }

     public String getQString()
     {
          String QString  = "";

          QString  =     " SELECT GRN.GrnNo, GRN.GrnDate, OrdBlock.BlockName, "+
                         " "+SSupTable+".Name, GRN.OrderNo, GRN.GateInNo, GRN.GateInDate, "+
                         " GRN.DcNo, GRN.DcDate, GRN.InvNo, GRN.InvDate, GRN.Code, "+
                         " InvItems.Item_Name, GRN.InvQty, GRN.MillQty, GRN.InvRate, "+
                         " GRN.InvAmount, GRN.GrnQty, Dept.Dept_Name, Cata.Group_Name, "+
                         " Unit.Unit_Name, GRN.SPJNo,GRN.Id, GRN.OrderQty,GRN.MrsNo,OrdBlock.Block,"+
                         " Grn.OrderSlno,Grn.MrsAuthUserCode "+
                         " FROM (((((GRN INNER JOIN OrdBlock ON GRN.GrnBlock = OrdBlock.Block "+
                         " and GRN.GRNNo ="+TStNo.getText() +
                         " And Grn.MillCode = "+iMillCode+") "+
                         " INNER JOIN "+SSupTable+" ON GRN.Sup_Code = "+SSupTable+".Ac_Code) "+
                         " INNER JOIN InvItems ON GRN.Code = InvItems.Item_Code) "+
                         " INNER JOIN Dept ON GRN.Dept_Code = Dept.Dept_code) "+
                         " INNER JOIN Cata ON GRN.Group_Code = Cata.Group_Code) "+
                         " INNER JOIN Unit ON GRN.Unit_Code = Unit.Unit_Code ";

          return QString;
     }

     private class DeleteList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validStock())
               {
                    BDelete   . setEnabled(false);
                    deleteRecords();
		          if(iMillCode==1)
		          {
		               updateSubStoreMasterData();
		          }
                    getCommStatus();
                    if(bComFlag)
                    {
                         try
                         {
                              theConnection  . setAutoCommit(true);

			               if(iMillCode==1)
			                    theDConnection   . setAutoCommit(true);
                         }catch(Exception ex){ex.printStackTrace();}
                         removeHelpFrame();
                    }
               }
          }
     }

     private boolean validStock()
     {
          VSUserCode = new Vector();
          VSItemCode = new Vector();
          VSGrnQty   = new Vector();

          int iIndex=-1;

          try
          {
               Statement       stat          = theConnection.createStatement();

               for(int i=0;i<RowData.length;i++)
               {
                    String SBlockCode   = (String)VBlockCode  . elementAt(i);
                    String SItemCode    = (String)VGrnCode    . elementAt(i);
                    double dGrnQty      = common.toDouble((String)VAccQty     . elementAt(i));
                    String SUserCode    = (String)VGrnUserCode. elementAt(i);

                    if(common.toInt(SBlockCode)>=2)
                         continue;

                    iIndex=-1;

                    iIndex = getIndexOf(SUserCode,SItemCode);


                    if(iIndex>=0)
                    {
                         double dOldQty = common.toDouble((String)VSGrnQty.elementAt(iIndex));

                         double dNewQty = dOldQty + dGrnQty;

                         VSGrnQty.setElementAt(common.getRound(dNewQty,3),iIndex);
                    }
                    else
                    {
                         VSUserCode.addElement(SUserCode);
                         VSItemCode.addElement(SItemCode);
                         VSGrnQty.addElement(""+dGrnQty);
                    }
               }

               if(VSUserCode.size()>0)
               {
                    for(int i=0;i<VSUserCode.size();i++)
                    {
     
                         String SUserCode = (String)VSUserCode.elementAt(i);
                         String SItemCode = (String)VSItemCode.elementAt(i);
                         double dGrnQty   = common.toDouble((String)VSGrnQty.elementAt(i));

                         double dStock  = 0;
			 double dIndRes = 0;
			 double dAvail  = 0;

                         String QS = " Select Sum(Stock),Sum(IndentQty+ReservedQty) from ItemStock Where MillCode="+iMillCode+
                                     " and HodCode="+SUserCode+" and ItemCode='"+SItemCode+"'";
     
                         ResultSet result = stat.executeQuery(QS);

                         while(result.next())
                         {
                              dStock  = common.toDouble(common.parseNull(result.getString(1)));
                              dIndRes = common.toDouble(common.parseNull(result.getString(2)));
			      dAvail  = dStock - dIndRes;	
                         }
                         result.close();

                         if(dStock<dGrnQty)
                         {
                              JOptionPane.showMessageDialog(null,"Stock Not Available for Item - "+SItemCode,"Attention",JOptionPane.INFORMATION_MESSAGE);
                              return false;
                         }

                         if(dAvail<dGrnQty)
                         {
                              JOptionPane.showMessageDialog(null,"Indent/Reservation was made for Item - "+SItemCode,"Attention",JOptionPane.INFORMATION_MESSAGE);
                              return false;
                         }
                    }
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return true;
     }

     public int getIndexOf(String SUserCode,String SItemCode)
     {
          int iIndex=-1;

          for(int i=0;i<VSUserCode.size();i++)
          {
               String SOUserCode = (String)VSUserCode.elementAt(i);
               String SOItemCode = (String)VSItemCode.elementAt(i);

               if(SUserCode.equals(SOUserCode) && SItemCode.equals(SOItemCode))
               {
                    iIndex = i;
                    break;
               }
               else
               {
                    continue;
               }
          }
          return iIndex;
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop   . remove(this);
               DeskTop   . repaint();
               DeskTop   . updateUI();
          }
          catch(Exception ex) { }
     }

     private void deleteRecords()
     {
          String QS = "";
          try
          {
               Statement       stat          = theConnection.createStatement();

               String SGrnNo = TStNo.getText();

               QS = "Update GRN Set DM=9 Where GrnNo="+SGrnNo+" and MillCode="+iMillCode;
               try
               {
                    stat.execute(QS);
               }
               catch(Exception ex)
               {
                    JOptionPane.showMessageDialog(null,"Problem in Deletion - 1","Attention",JOptionPane.INFORMATION_MESSAGE);
                    bComFlag  = false;
                    return;
               }

               try
               {
                    QS   = " Insert Into DeletedGRN Select * From GRN Where GRN.DM=9 and GRN.GrnNo="+SGrnNo+" and GRN.MillCode="+iMillCode;
                    stat . execute(QS);
                    updateItemMaster(SGrnNo);
               }
               catch(Exception ex)
               {
                    JOptionPane    . showMessageDialog(null,"Problem in Deletion - 2","Attention",JOptionPane.INFORMATION_MESSAGE);
                    ex             . printStackTrace();
                    bComFlag       = false;
                    return;
               }

               try
               {
                    QS = " Delete From GRN Where DM=9 and GrnNo="+SGrnNo+" and MillCode="+iMillCode;
                    stat . execute(QS);
               }
               catch(Exception ex)
               {
                    JOptionPane.showMessageDialog(null,"Problem in Deletion - 3","Attention",JOptionPane.INFORMATION_MESSAGE);
                    ex.printStackTrace();
                    bComFlag  = false;
                    return;
               }

               for(int i=0;i<RowData.length;i++)
               {
                    String SBlockCode = (String)VBlockCode  . elementAt(i);
                    String SGINo      = (String)VGno        . elementAt(i);
                    String SOrderNo   = (String)VOrdNo      . elementAt(i);
                    String SItemCode  = (String)VGrnCode    . elementAt(i);
                    String SOrdQty    = (String)VOrdQty     . elementAt(i);
                    String SRecQty    = (String)VAccQty     . elementAt(i);
                    String SMrsNo     = (String)VMrsNo      . elementAt(i);
                    String SGOrdSlNo  = (String)VGOrdSlNo   . elementAt(i);

                    QS = " Update PurchaseOrder Set InvQty=InvQty-"+SRecQty+
                         " Where OrderNo="+SOrderNo+" And OrderBlock="+SBlockCode+
                         " And Item_Code='"+SItemCode+"' And slno="+SGOrdSlNo+
                         " And MillCode="+iMillCode;

                    try
                    {
                         stat.execute(QS);
                    }
                    catch(Exception ex)
                    {
                         JOptionPane.showMessageDialog(null,"Problem updatation in purchaseorder","Attention",JOptionPane.INFORMATION_MESSAGE);
                         bComFlag  = false;
                         return;
                    }

                    if(iGateCount>0)
                    {
                         QS = " Update GateInward Set GrnNo=0,GrnNumber=0 "+
                              " Where GINo="+SGINo+
                              " And GrnNumber="+SGrnNo+
                              " And MillCode="+iMillCode;
                    }
                    else
                    {
                         QS = " Update GateInward Set GrnNo=0 "+
                              " Where GINo="+SGINo+
                              " And MillCode="+iMillCode;
                    }

                    try
                    {
                         stat.execute(QS);
                    }
                    catch(Exception ex)
                    {
                         JOptionPane.showMessageDialog(null,"Problem updatation in Gateinward","Attention",JOptionPane.INFORMATION_MESSAGE);
                         bComFlag  = false;
                         return;
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);

               bComFlag  = false;
          }
     }

     private void updateItemMaster(String SGrnNo)
     {
          try
          {
               GrnDetails(SGrnNo);

               Statement      stat  = theConnection. createStatement();

               for(int i = 0;i<VICode.size();i++)
               {
                    String SItemCode    = (String)VICode         . elementAt(i);
                    String SBlockCode   = (String)VIGrnBlock     . elementAt(i);
                    String SGrnQty      = (String)VIGrnQty       . elementAt(i);
                    String SGrnValue    = (String)VIGrnValue     . elementAt(i);
                    String SMrsUserCode = (String)VMrsUserCode   . elementAt(i); 

                    int       iBlockCode= common.toInt(SBlockCode);
                    String    QS        = "";
                    double    dValue    = 0;
     
                    if(iBlockCode>1)
                    {
                         QS = " Update "+SItemTable+" Set  RecVal=nvl(RecVal,0)-"+SGrnValue;
                         QS = QS+", RecQty=nvl(RecQty,0)-"+SGrnQty+",";
                         QS = QS+" IssQty=nvl(IssQty,0)-"+SGrnQty+",";
                         QS = QS+" IssVal=nvl(IssVal,0)-"+SGrnValue+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    else
                    {
                         Item IC = new Item(SItemCode,iMillCode,SItemTable,SSupTable);
               
                         double dAllStock = common.toDouble(IC.getClStock());
                         double dAllValue = common.toDouble(IC.getClValue());
               
                         dAllStock = dAllStock - common.toDouble(SGrnQty);
                         dAllValue = dAllValue - common.toDouble(SGrnValue);
          
                         double dRate   = 0;
                         try
                         {
                              dRate = dAllValue/dAllStock;
                         }
                         catch(Exception ex)
                         {
                              dRate=0;
                         }
               
                         if(dAllStock==0)
                         {
                              dRate = common.toDouble(IC.SRate);
                         }


                         QS = " Update "+SItemTable+" Set RecVal=nvl(RecVal,0)-"+SGrnValue;
                         QS = QS+", RecQty=nvl(RecQty,0)-"+SGrnQty+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";

                         String QS1 = "Update ItemStock Set Stock=nvl(Stock,0)-"+SGrnQty+" ";
                         QS1 = QS1+" Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+iMillCode;

                         String QS2 = " Update ItemStock set StockValue="+common.getRound(dRate,4)+
                                      " Where ItemCode='"+SItemCode+"' and MillCode="+iMillCode;


                         stat.execute(QS1);
                         stat.execute(QS2);
                    }
                    stat.execute(QS);
               }     
          }
          catch(Exception e)
          {
               System.out.println("E6"+e);
               bComFlag  = false;
          }
     }

     private void updateSubStoreMasterData()
     {
          try
          {
               Statement stat   =    theDConnection.createStatement();
               
               for(int i = 0;i<VICode.size();i++)
               {
                    String SItemCode    = (String)VICode         . elementAt(i);
                    int iCount=0;
                    
                    ResultSet res = stat.executeQuery("Select count(*) from InvItems Where Item_Code='"+SItemCode+"'");
                    while(res.next())
                    {
                         iCount   =    res.getInt(1);
                    }
                    res.close();
                    
                    if(iCount==0)
                         continue;

                    String SBlockCode   = (String)VIGrnBlock     . elementAt(i);
                    String SGrnQty      = (String)VIGrnQty       . elementAt(i);

                    int iBlockCode = common.toInt(SBlockCode);
                    
                    String QS = "";
                    
                    if(iBlockCode>1)
                    {
                         QS = "Update InvItems Set ";
                         QS = QS+" MSRecQty=nvl(MSRecQty,0)-"+SGrnQty+",";
                         QS = QS+" MSIssQty=nvl(MSIssQty,0)-"+SGrnQty+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    else
                    {
                         QS = "Update InvItems Set ";
                         QS = QS+" MSRecQty=nvl(MSRecQty,0)-"+SGrnQty+",";
                         QS = QS+" MSStock=nvl(MSStock,0)-"+SGrnQty+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    if(theDConnection  . getAutoCommit())
                         theDConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("E7"+e);
               bComFlag  = false;
          }
     }


     public void GrnDetails(String SGrnNo)
     {
          VICode         = new Vector();
          VIGrnBlock     = new Vector();
          VIGrnQty       = new Vector();
          VIGrnValue     = new Vector();
          VMrsUserCode   = new Vector();

          String QS = " Select Code,GrnBlock,GrnQty,GrnValue,MrsAuthUserCode From Grn Where DM=9 and GrnNo="+SGrnNo+" and MillCode="+iMillCode;
          try
          {
               Statement      stat      = theConnection.createStatement();
               ResultSet      result    = stat.executeQuery(QS);

               while(result.next())
               {
                    VICode      . addElement(common.parseNull((String)result.getString(1)));
                    VIGrnBlock  . addElement(common.parseNull((String)result.getString(2)));
                    VIGrnQty    . addElement(common.parseNull((String)result.getString(3)));
                    VIGrnValue  . addElement(common.parseNull((String)result.getString(4)));
                    VMrsUserCode. addElement(common.parseNull((String)result.getString(5)));
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void getCommStatus()
     {
          try
          {
               if(bComFlag)
               {
                    theConnection  . commit();

                    if(iMillCode==1)
                         theDConnection . commit();

                    System         . out.println("Commit");
                    JOptionPane    . showMessageDialog(null,"Grn Deleted","Attention",JOptionPane.INFORMATION_MESSAGE);
               }
               else
               {
                    theConnection  . rollback();

                    if(iMillCode==1)
	                   theDConnection . rollback();

                    System         . out.println("RollBack");
                    JOptionPane    . showMessageDialog(null,"Problem in Deletion","Attention",JOptionPane.INFORMATION_MESSAGE);
               }
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
