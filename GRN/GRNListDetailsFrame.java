package GRN;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class GRNListDetailsFrame extends JInternalFrame
{
     Object RowData[][];
     String ColumnData[] = {"GRN Type","GRN No","Date","Rejection No","Rej Date","Block","Supplier","Order No","G.I.No","G.I.Date","DC No","DC Date","Inv No","InvDate","Code","Name","Inv Qty","Recd Qty","Inv Rate","Amount","Accepted Qty","Dept","Group","Unit","Status"};
     String ColumnType[] = {  "S"     ,"N"     , "S"  ,     "N"      ,   "S"    ,  "S"  ,"S"       ,  "N"     ,   "N"  ,"S"       , "N"   ,   "S"   ,"N"     ,   "S"   ,  "S" ,"S"   ,   "N"   ,   "N"    ,"N"       ,  "N"   ,   "N"        , "S"  , "S"   ,  "S" ,  "S"};

     Vector VGrnNo,VGrnDate,VBlock,VSupName,VOrdNo,VDcNo,VDcDate,VGno,VGDate,VINo,VIDate,VGrnCode,VGrnName,VIQty,VRQty,VIRate,VIAmt,VAccQty,VGrnDeptName,VGrnCataName,VGrnUnitName,VStatus,VSPJNo,VId,VInspection,VRejFlag,VRejNo,VRejDate,VSlNo,VGrnType;

     TabReport tabreport;
     GRNCritPanel TopPanel;
     JButton BPrint;
     JTextField TFileName;
     FileWriter FW;

     JPanel BottomPanel;

     Common common = new Common();

     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;

     String SDate = "";
     String EDate = "" ;
     int len=0;
     int iUserCode,iMillCode;
     String SYearCode;
     String SItemTable,SSupTable,SMillName;

     int  printLine =0;
     int  ipage=1;
     public GRNListDetailsFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable,String SMillName)
     {
         super("GRNList Details with Constraints");
         this.DeskTop    = DeskTop;
         this.VCode      = VCode;
         this.VName      = VName;
         this.SPanel     = SPanel;
         this.iUserCode  = iUserCode;
         this.iMillCode  = iMillCode;
         this.SYearCode  = SYearCode;
         this.SItemTable = SItemTable;
         this.SSupTable  = SSupTable;
         this.SMillName  = SMillName;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
      }

     public void createComponents()
     {
         TopPanel    = new GRNCritPanel(iMillCode,SItemTable,SSupTable);
         BottomPanel = new JPanel();
         BPrint      = new JButton("Print");
         TFileName   = new JTextField(25);
         TFileName.setText("GRNListDetails.prn");
     }
     public void setLayouts()
     {
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,650,500);
     }

     public void addComponents()
     {
         BPrint.setEnabled(false);
         BottomPanel.add(BPrint);
         BottomPanel.add(TFileName);
         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
         TopPanel.BApply.addActionListener(new ApplyList());
         BPrint.addActionListener(new PrintList());
     }
     private void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.updateUI();
               DeskTop.repaint();
          }
          catch(Exception ex){}
     }
     public class PrintList implements  ActionListener
     {
       public void actionPerformed(ActionEvent ae)
       {
         PrnFileWrite();
         JOptionPane.showMessageDialog(null,getInfo(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
       }
     }
     public void PrnFileWrite()
     {
           try
           {
             String SFile =TFileName.getText();

             if((SFile.trim()).length()==0)
                SFile = "1.prn";

             FW = new FileWriter(common.getPrintPath()+SFile);
             PrintMainHead();
             PrintHead();
             PrintData();
             prtStrl(common.Replicate("-",len)+"");
             FW.close();
           }
          catch(Exception e)
          {
            System.out.println(e);
          }
     }
     private void PrintMainHead()
     {
          prtStrl("g"+SMillName);
          prtStrl(getHeadTitle());
          prtStrl("Page :"+ipage);
          prtStrl("\n");
     }
     private void PrintHead()
     {
          String   S1= common.Cad("S No",5)+" ";
          String   S2= common.Cad("GRN Type",8)+" ";
          String   S3= common.Cad("GRN No",8)+" ";
          String   S4= common.Cad("Date",10)+" ";
          String   S5= common.Cad("Rej No",6)+" ";
          String   S6= common.Cad("Rej Date",10)+" ";
          String   S7= common.Cad("Block",5)+" ";
          String   S8= common.Cad("Supplier",25)+" ";
          String   S9= common.Cad("Order No",8)+" ";
          String   S10= common.Cad("G.I.No",7)+" ";
          String   S11= common.Cad("G.I.Date",10)+" ";
          String   S12= common.Cad("DC No",10)+" ";
          String   S13= common.Cad("DC Date",10)+" ";
          String   S14= common.Cad("Inv No",10)+" ";
          String   S15= common.Cad("InvDate",10)+" ";
          String   S16= common.Cad("Code",9)+" ";
          String   S17= common.Cad("Name",20)+" ";
          String   S18= common.Cad("Inv Qty",10)+" ";
          String   S19= common.Cad("Recd Qty",10)+" ";
          String   S20= common.Cad("Inv Rate",10)+" ";
          String   S21= common.Cad("Amount",12)+" ";
          String   S22= common.Cad("Accepted Qty",12)+"   ";
          String   S23= common.Cad("Department",12)+"   ";
          String   S24= common.Cad("Group",12)+"   ";
          String   S25= common.Cad("Unit",8)+" ";

          String Mar=S1+S2+S3+S4+S5+S6+S7+S8+S9+S10+S11+S12+S13+S14+S15+S16+S17+S18+S19+S20+S21+S22+S23+S24+S25;
          len=Mar.length();
          prtStrl(common.Replicate("-",len));
          prtStrl(Mar);
          prtStrl(common.Replicate("-",len));
     }
     private void PrintData()
     {
          for(int i=0; i<VGrnDate.size(); i++)
          {
               String Str0  =Integer.toString(i+1);
               String Str1  =(String)VGrnType.elementAt(i);
               String Str2  =(String)VGrnNo.elementAt(i);
               String Str3  =(String)VGrnDate.elementAt(i);
               String Str4  =(String)VRejNo.elementAt(i);
               String Str5  =(String)VRejDate.elementAt(i);
               String Str6  =(String)VBlock.elementAt(i);
               String Str7  =(String)VSupName.elementAt(i);
               String Str8  =(String)VOrdNo.elementAt(i);
               String Str9  =(String)VGno.elementAt(i);
               String Str10 =(String)VGDate.elementAt(i);
               String Str11 =(String)VDcNo.elementAt(i);
               String Str12 =(String)VDcDate.elementAt(i);
               String Str13 =(String)VINo.elementAt(i);
               String Str14 =(String)VIDate.elementAt(i);
               String Str15  =(String)VGrnCode.elementAt(i);  
               String Str16  =(String)VGrnName.elementAt(i);
               String Str17  =common.getRound((String)VIQty.elementAt(i),3);
               String Str18  =common.getRound((String)VRQty.elementAt(i),3);
               String Str19  =common.getRound((String)VIRate.elementAt(i),2);
               String Str20  =common.getRound((String)VIAmt.elementAt(i),2);
               String Str21  =common.getRound((String)VAccQty.elementAt(i),3);
               String Str22  =(String)VGrnDeptName.elementAt(i);
               String Str23  =(String)VGrnCataName.elementAt(i);
               String Str24  =(String)VGrnUnitName.elementAt(i);
               
               String   S0 = common.Cad(Str0,5)+" ";
               String   S1 = common.Pad(Str1,8)+" ";
               String   S2 = common.Rad(Str2,8)+" ";
               String   S3 = common.Cad(Str3,10)+" ";
               String   S4 = common.Rad(Str4,6)+" ";
               String   S5 = common.Cad(Str5,10)+" ";
               String   S6 = common.Cad(Str6,5)+" ";
               String   S7 = common.Pad(Str7,25)+" ";
               String   S8 = common.Rad(Str8,8)+" ";
               String   S9 = common.Rad(Str9,7)+" ";
               String   S10= common.Cad(Str10,10)+" ";
               String   S11= common.Rad(Str11,10)+" ";
               String   S12= common.Cad(Str12,10)+" ";
               String   S13= common.Rad(Str13,10)+" ";
               String   S14 = common.Cad(Str14,10)+" ";
               String   S15 = common.Pad(Str15,9)+" ";
               String   S16 = common.Pad(Str16,20)+" ";
               String   S17 = common.Rad(Str17,10)+" ";
               String   S18 = common.Rad(Str18,10)+" ";
               String   S19 = common.Rad(Str19,10)+" ";
               String   S20 = common.Rad(Str20,12)+" ";
               String   S21 = common.Rad(Str21,12)+"   ";
               String   S22 = common.Pad(Str22,12)+"   ";
               String   S23 = common.Pad(Str23,12)+"   ";
               String   S24 = common.Pad(Str24,8)+" ";

               String Mar=S0+S1+S2+S3+S4+S5+S6+S7+S8+S9+S10+S11+S12+S13+S14+S15+S16+S17+S18+S19+S20+S21+S22+S23+S24;
               len=Mar.length();
               prtStrl(Mar);

          }
     }
     private String getHeadTitle()
     {
          String STitle="GRNListDetails From   "+common.parseDate(TopPanel.TStDate.toNormal())+  " TO "  +common.parseDate(TopPanel.TEnDate.toNormal());
          if(TopPanel.JRSeleUnit.isSelected())
          {
               STitle= STitle+" and Unit:"+(String)TopPanel.JCUnit.getSelectedItem();
          }
          if(TopPanel.JRSeleDept.isSelected())
          {
               STitle= STitle+ " and  Department: "+(String)TopPanel.JCDept.getSelectedItem();
          }
          if(TopPanel.JRSeleGroup.isSelected())
          {
               STitle= STitle+ " and  Group: "+(String)TopPanel.JCGroup.getSelectedItem();
          }
          if(TopPanel.JRSeleBlock.isSelected())
          {
               STitle= STitle+ " and  Block: "+(String)TopPanel.JCBlock.getSelectedItem();
          }
          if(TopPanel.JRSeleMaterial.isSelected())
          {
               STitle= STitle+ " and  Material: "+(String)TopPanel.JCMaterial.getSelectedItem();
          }
          if(TopPanel.JRSeleSup.isSelected())
          {
               STitle= STitle+ " and  Supplier:"+(String)TopPanel.JCSup.getSelectedItem();
          }
          if(TopPanel.JRSeleList.isSelected())
          {
               STitle= STitle+ " and  List:"+(String)TopPanel.JCList.getSelectedItem();
          }
          return STitle;
     }

     private void prtStrl(String Strl)
     {
          try
          {
              printLine=printLine+1;
              if(printLine>64)
              {
                ipage=ipage +1;
                printLine =0;
                prtStrl(common.Replicate("-",len));
                FW.write(" \n");
                PrintMainHead();
                PrintHead();
              }
              FW.write(Strl+"\n");

          }
          catch(Exception ex){System.out.println("Error : "+ex);}
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               ipage=1;
               printLine=0;

               setDataIntoVector();
               setRowData();
               BPrint.setEnabled(true);
               try
               {
                  getContentPane().remove(tabreport);
               }
               catch(Exception ex){}

               try
               {
                  tabreport = new TabReport(RowData,ColumnData,ColumnType);
                  getContentPane().add(tabreport,BorderLayout.CENTER);
                  setSelected(true);
                  DeskTop.repaint();
                  DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
               }
          }
     }
     public void setDataIntoVector()
     {

           VGrnNo       = new Vector();
           VGrnDate     = new Vector();
           VBlock       = new Vector();
           VSupName     = new Vector();
           VOrdNo       = new Vector();
           VGno         = new Vector();
           VGDate       = new Vector();
           VDcNo        = new Vector();
           VDcDate      = new Vector();
           VINo         = new Vector();
           VIDate       = new Vector();
           VGrnCode     = new Vector();
           VGrnName     = new Vector();
           VIQty        = new Vector();
           VRQty        = new Vector();
           VIRate       = new Vector();
           VIAmt        = new Vector();
           VAccQty      = new Vector();
           VGrnDeptName = new Vector();
           VGrnCataName = new Vector();
           VGrnUnitName = new Vector();
           VStatus      = new Vector();
           VSPJNo       = new Vector();
           VId          = new Vector();
           VInspection  = new Vector();
           VRejFlag     = new Vector();
           VRejNo       = new Vector();
           VRejDate     = new Vector();
           VSlNo        = new Vector();
           VGrnType     = new Vector();

           String QString = getQString();

           try
           {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
               stat.execute(QString);
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {

                     String str1  = ""+res.getString(1);  
                     String str2  = ""+res.getString(2);
                     String str3  = ""+res.getString(3);
                     String str4  = ""+res.getString(4);
                     String str5  = ""+res.getString(5);
                     String str6  = ""+res.getString(6);
                     String str7  = ""+res.getString(7);
                     String str8  = ""+res.getString(8);
                     String str9  = ""+res.getString(9);
                     String str10 = ""+res.getString(10);
                     String str11 = ""+res.getString(11);
                     String str12 = ""+res.getString(12);
                     String str13 = ""+res.getString(13);
                     String str14 = ""+res.getString(14);
                     String str15 = ""+res.getString(15);
                     String str16 = ""+res.getString(16);
                     String str17 = ""+res.getString(17);
                     String str18 = ""+res.getString(18);
                     String str19 = ""+res.getString(19);
                     String str20 = ""+res.getString(20);
                     String str21 = ""+res.getString(21);
                     String str22 = ""+res.getString(22);
                     String str23 = ""+res.getString(23);
                     String str24 = ""+res.getString(24);
                     String str25 = ""+res.getString(25);
                     String str26 = ""+res.getString(26);
                     String str27 = ""+res.getString(27);
                     String str28 = ""+res.getString(28);
    
                     if(str1.equals("0"))
                     {
                         VGrnType     .addElement("Grn");
                     }
                     else
                     {
                         VGrnType     .addElement("Rejection");
                     }
                     VRejFlag     .addElement(str1);
                     VGrnNo       .addElement(str2);
                     VGrnDate     .addElement(common.parseDate(str3));
                     VRejNo       .addElement(common.parseNull(str4));
                     VRejDate     .addElement(common.parseDate(str5));
                     VBlock       .addElement(str6);
                     VSupName     .addElement(str7);
                     VOrdNo       .addElement(str8);
                     VGno         .addElement(str9);
                     VGDate       .addElement(common.parseDate(str10));
                     VDcNo        .addElement(common.parseNull(str11));
                     VDcDate      .addElement(common.parseDate(str12));
                     VINo         .addElement(common.parseNull(str13));
                     VIDate       .addElement(common.parseDate(str14));
                     VGrnCode     .addElement(str15);
                     VGrnName     .addElement(str16);
                     VIQty        .addElement(str17);
                     VRQty        .addElement(str18);
                     VIRate       .addElement(str19);
                     VIAmt        .addElement(str20);
                     VAccQty      .addElement(str21);
                     VGrnDeptName .addElement(common.parseNull(str22));
                     VGrnCataName .addElement(common.parseNull(str23));
                     VGrnUnitName .addElement(common.parseNull(str24));
                     if(str25.equals("0"))
                     {
                         VStatus      .addElement("Not Inspected");
                     }
                     else
                     {
                         VStatus      .addElement("Inspected");
                     }
                     VInspection  .addElement(str25);
                     VSPJNo       .addElement(str26);
                     VId          .addElement(str27);
                     VSlNo        .addElement(str28);
               }
               res.close();
               stat.close();
           }
           catch(Exception ex){System.out.println("hai"+ex);}
     }
     public void setRowData()
     {
         RowData     = new Object[VGrnNo.size()][ColumnData.length];
         for(int i=0;i<VGrnNo.size();i++)
         {
               RowData[i][0]  = (String)VGrnType.elementAt(i);
               RowData[i][1]  = (String)VGrnNo.elementAt(i);
               RowData[i][2]  = (String)VGrnDate.elementAt(i);
               RowData[i][3]  = (String)VRejNo.elementAt(i);
               RowData[i][4]  = (String)VRejDate.elementAt(i);
               RowData[i][5]  = (String)VBlock.elementAt(i);
               RowData[i][6]  = (String)VSupName.elementAt(i);
               RowData[i][7]  = (String)VOrdNo.elementAt(i);
               RowData[i][8]  = (String)VGno.elementAt(i);
               RowData[i][9]  = (String)VGDate.elementAt(i);
               RowData[i][10] = (String)VDcNo.elementAt(i);
               RowData[i][11] = (String)VDcDate.elementAt(i);
               RowData[i][12] = (String)VINo.elementAt(i);
               RowData[i][13] = (String)VIDate.elementAt(i);
               RowData[i][14] = (String)VGrnCode.elementAt(i);
               RowData[i][15] = (String)VGrnName.elementAt(i);
               RowData[i][16] = (String)VIQty.elementAt(i);
               RowData[i][17] = (String)VRQty.elementAt(i);
               RowData[i][18] = (String)VIRate.elementAt(i);
               RowData[i][19] = (String)VIAmt.elementAt(i);
               RowData[i][20] = (String)VAccQty.elementAt(i);
               RowData[i][21] = (String)VGrnDeptName.elementAt(i);
               RowData[i][22] = (String)VGrnCataName.elementAt(i);
               RowData[i][23] = (String)VGrnUnitName.elementAt(i);
               RowData[i][24] = (String)VStatus.elementAt(i);
        }  
     }
     public String getQString()
     {
          String SDate = TopPanel.TStDate.toNormal();
          String EDate=  TopPanel.TEnDate.toNormal();
          String QString ="";
          String str ="";

          if(TopPanel.JRDate.isSelected())
          {
               QString   = " SELECT GRN.RejFlag,GRN.GrnNo, GRN.GrnDate,Grn.RejNo,Grn.RejDate,OrdBlock.BlockName,"+
                           " "+SSupTable+".Name, GRN.OrderNo, GRN.GateInNo, GRN.GateInDate, "+
                           " GRN.DcNo, GRN.DcDate, GRN.InvNo, GRN.InvDate, GRN.Code, "+
                           " InvItems.Item_Name, GRN.InvQty, GRN.MillQty, GRN.InvRate, "+
                           " GRN.grnvalue, GRN.Qty, Dept.Dept_Name, Cata.Group_Name, "+
                           " Unit.Unit_Name, Grn.Inspection,GRN.SPJNo,GRN.Id,Grn.SlNo,invitems.hsntype "+
                           " FROM (((((GRN INNER JOIN OrdBlock ON GRN.GrnBlock = OrdBlock.Block) "+
                           " INNER JOIN "+SSupTable+" ON GRN.Sup_Code = "+SSupTable+".Ac_Code) "+
                           " INNER JOIN InvItems ON GRN.Code = InvItems.Item_Code) "+
                           " INNER JOIN Dept ON GRN.Dept_Code = Dept.Dept_code) "+
                           " INNER JOIN Cata ON GRN.Group_Code = Cata.Group_Code) "+
                           " INNER JOIN Unit ON GRN.Unit_Code = Unit.Unit_Code "+
                           " Where GRN.GrnDate >= '"+SDate+"' and GRN.GrnDate <='"+EDate+"' and Invitems.hsntype=0 and GRN.millcode="+iMillCode ;
          }
          else
          {
               QString   = " SELECT GRN.RejFlag,GRN.GrnNo, GRN.GrnDate,Grn.RejNo,Grn.RejDate,OrdBlock.BlockName,"+
                           " "+SSupTable+".Name, GRN.OrderNo, GRN.GateInNo, GRN.GateInDate, "+
                           " GRN.DcNo, GRN.DcDate, GRN.InvNo, GRN.InvDate, GRN.Code, "+
                           " InvItems.Item_Name, GRN.InvQty, GRN.MillQty, GRN.InvRate, "+
                           " GRN.grnvalue, GRN.Qty, Dept.Dept_Name, Cata.Group_Name, "+
                           " Unit.Unit_Name, Grn.Inspection,GRN.SPJNo,GRN.Id,Grn.SlNo,invitems.hsntype "+
                           " FROM (((((GRN INNER JOIN OrdBlock ON GRN.GrnBlock = OrdBlock.Block) "+
                           " INNER JOIN "+SSupTable+" ON GRN.Sup_Code = "+SSupTable+".Ac_Code) "+
                           " INNER JOIN InvItems ON GRN.Code = InvItems.Item_Code) "+
                           " INNER JOIN Dept ON GRN.Dept_Code = Dept.Dept_code) "+
                           " INNER JOIN Cata ON GRN.Group_Code = Cata.Group_Code) "+
                           " INNER JOIN Unit ON GRN.Unit_Code = Unit.Unit_Code "+
                           " Where GRN.GrnNo >="+TopPanel.TStNo.getText()+" and GRN.GrnNo <= "+TopPanel.TEnNo.getText()+"  and Invitems.hsntype=0  and GRN.millcode="+iMillCode ;
          }
          String SHave="";
          if(TopPanel.JRSeleUnit.isSelected())
          {
               str = "Grn.Unit_Code = "+(String)TopPanel.VUnitCode.elementAt(TopPanel.JCUnit.getSelectedIndex());
               SHave=SHave+" and "+str;
          }
          if(TopPanel.JRSeleDept.isSelected())
          {
               str = "Grn.Dept_Code = "+(String)TopPanel.VDeptCode.elementAt(TopPanel.JCDept.getSelectedIndex());
               SHave=SHave+" and "+str;
          }
          if(TopPanel.JRSeleGroup.isSelected())
          {
               str = "Grn.Group_Code = "+(String)TopPanel.VGroupCode.elementAt(TopPanel.JCGroup.getSelectedIndex());
               SHave=SHave+" and "+str;
          }
          if(TopPanel.JRSeleBlock.isSelected())
          {
               str = "GRN.GrnBlock = "+(String)TopPanel.VBlockCode.elementAt(TopPanel.JCBlock.getSelectedIndex());
               SHave=SHave+" and "+str;
          }

          if(TopPanel.JRSeleMaterial.isSelected())
          {
               str = "GRN.Code ='"+(String)TopPanel.VMaterialCode.elementAt(TopPanel.JCMaterial.getSelectedIndex())+"'";
               SHave=SHave+" and "+str;
          }
          if(TopPanel.JRSeleSup.isSelected())
          {
               str = "Grn.Sup_Code = '"+(String)TopPanel.VSupCode.elementAt(TopPanel.JCSup.getSelectedIndex())+"'";
               SHave=SHave+" and "+str;
          }
          QString = QString+ SHave;
                      
          if(TopPanel.JRSeleList.isSelected())
          {
               DateField df = new DateField();
               df.setTodayDate();
               String SToday = df.toNormal();
               int iSelect = TopPanel.JCList.getSelectedIndex();

               if (iSelect==0)
               {
                    QString = QString+" and GRN.Inspection=1 <= '"+SToday+"'";

               }
               if(iSelect==1)
               {
                    QString = QString+" and GRN.Inspection = 0";
               }
          }

          String SOrder=(TopPanel.TSort.getText()).trim();
          if(SOrder.length()>0)
                QString = QString+" Order By "+TopPanel.TSort.getText()+",16";
          else
                QString = QString+" Order By 2,3,28,1";
          return QString;
     }
     private String getInfo()
     {

          String str = "<html><body>"; 
          str = str+"<h4><font color='green'> PRN File Successfully Created</font></h4>";
          str = str+"</body></html>";

        return str;
     }

}
