
package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;



import javax.comm.*;

import javax.print.*;
import javax.print.attribute.*;
import javax.print.attribute.standard.*;
import javax.print.event.*;


public class GRNListFrame extends JInternalFrame
{
     String    SStDate,SEnDate;
     JComboBox JCOrder,JCFilter,JCBlock;
     JButton   BApply,BPrint;
     JPanel    TopPanel,BottomPanel;
     JButton   BRevoke;
     JPanel    DatePanel,FilterPanel,SortPanel,BasisPanel,ApplyPanel;

     TabReport tabreport;
     DateField TStDate;
     DateField TEnDate;
     GRNRecords grnrecords;

     JRadioButton JRPeriod,JRNo;
     NextField    TStNo,TEnNo;

     Object RowData[][];
     String ColumnData[] = {"GRN Type","GRN No","Date","Rejection No","Rej Date","Block","Supplier","Order No","G.I. No","G.I. Date","DC No","DC Date","Inv No","Inv Date","Code","Name","Bin","Inv Qty","Recd Qty","Inv Rate","Amount","Accepted Qty","Current Stock","Dept","Group","Unit","Status","Qty","Count","Click"};
     String ColumnType[] = {"S"       ,"N"     ,"S"   ,"N"           ,"S"       ,"S"    ,"S"       ,"N"       ,"N"      ,"S"        ,"S"    ,"S"      ,"S"     ,"S"       ,"S"   ,"S"   ,"S","N"      ,"N"       ,"N"       ,"N"     ,"N"           ,  "N"           ,"S"   ,"S"    ,"S"   ,"S"   ,"E"   ,"E"  , "B"};
     Common common = new Common();
     Vector VGrnNo,VGrnDate,VBlock,VSupName,VOrdNo,VDcNo,VDcDate,VGno,VGDate,VINo,VIDate,VGrnCode,VGrnName,VIQty,VRQty,VIRate,VIAmt,VAccQty,VGrnDeptName,VGrnCataName,VGrnUnitName,VStatus,VSPJNo,VId,VInspection,VRejFlag,VRejNo,VRejDate,VSlNo,VGrnType,VBin,VHsnType;
     JLayeredPane DeskTop;
     StatusPanel SPanel;
     Vector VCode,VName;
     int iMillCode;
     String SYearCode,SItemCoddee;
     String SItemTable,SSupTable;
     Vector VCurrentStock,VGRNItemCode;
	      String       SPort = "COM1";
		 
	     Connection     theConnection;
	 ArrayList    ACatl,ADraw,AItemCode;


     public GRNListFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,String SYearCode,String SItemTable,String SSupTable)
     {
         super("GRN List During a Period");
         this.DeskTop    = DeskTop;
         this.SPanel     = SPanel;
         this.VCode      = VCode;
         this.VName      = VName;
         this.iMillCode  = iMillCode;
         this.SYearCode  = SYearCode;
         this.SItemTable = SItemTable;
         this.SSupTable  = SSupTable;

         System.out.println("millcode-->"+iMillCode);
         System.out.println("SItemTable-->"+SItemTable);
         System.out.println("SSupTable-->"+SSupTable);

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     public void createComponents()
     {
         TStDate  = new DateField();
         TEnDate  = new DateField();
         TStNo    = new NextField();
         TEnNo    = new NextField();
         BApply   = new JButton("Apply");
         BPrint   = new JButton("Print");

         TopPanel    = new JPanel();
         BottomPanel = new JPanel();

         JCOrder  = new JComboBox();
         JCFilter = new JComboBox();
         JCBlock  = new JComboBox();

         JRPeriod = new JRadioButton("Periodical",true);
         JRNo     = new JRadioButton("GRN No");

         BRevoke  = new JButton("Revoke");

         DatePanel   = new JPanel();
         FilterPanel = new JPanel();
         SortPanel   = new JPanel();
         BasisPanel  = new JPanel();
         ApplyPanel  = new JPanel();

         TStDate.setTodayDate();
         TEnDate.setTodayDate();
     }

     public void setLayouts()
     {
         TopPanel       .setLayout(new GridLayout(1,5));

         DatePanel      .setLayout(new GridLayout(3,1));
         SortPanel      .setLayout(new BorderLayout());
         FilterPanel    .setLayout(new BorderLayout());
         BasisPanel     .setLayout(new GridLayout(2,1));
         ApplyPanel     .setLayout(new GridLayout(2,1));
       
         setClosable(true);
         setIconifiable(true);
         setMaximizable(true);
         setResizable(true);
         setBounds(0,0,650,500);
     }

     public void addComponents()
     {
         JCOrder        .addItem("GRN No");
         JCOrder        .addItem("Materialwise");
         JCOrder        .addItem("Supplierwise");
         JCOrder        .addItem("Departmentwise");
         JCOrder        .addItem("Groupwise");
         JCOrder        .addItem("Processing Unitwise");

         JCBlock        .addItem("SST");
         JCBlock        .addItem("SSP");
         JCBlock        .addItem("NST");
         JCBlock        .addItem("NSP");
         JCBlock        .addItem("NPR");

         JCFilter       .addItem("All");
         JCFilter       .addItem("Inspected");
         JCFilter       .addItem("Not Inspected");

         SortPanel      .add("Center",JCOrder);
         FilterPanel    .add("Center",JCFilter);

         DatePanel      .add(TStDate);
         DatePanel      .add(TEnDate);

         BasisPanel     .add(JRPeriod);
         BasisPanel     .add(JRNo);

         ApplyPanel     .add("Center",BApply);
         ApplyPanel     .add("Center",BPrint);

         TopPanel       .add(SortPanel);
         TopPanel       .add(FilterPanel);
         TopPanel       .add(BasisPanel);
         TopPanel       .add(DatePanel);
         TopPanel       .add(ApplyPanel);

         //BottomPanel    .add(BRevoke);

         SortPanel      .setBorder(new TitledBorder("Sorting"));
         DatePanel      .setBorder(new TitledBorder("Period"));
         BasisPanel     .setBorder(new TitledBorder("Basis"));
         ApplyPanel     .setBorder(new TitledBorder("Control"));
         FilterPanel    .setBorder(new TitledBorder("Filter"));

         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
         BApply   .addActionListener(new ApplyList());
	    BPrint.addActionListener(new ActList());

         JRPeriod .addActionListener(new JRList());
         JRNo     .addActionListener(new JRList());
     }
     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    DatePanel.setBorder(new TitledBorder("Periodical"));
                    DatePanel.removeAll();
                    DatePanel.add(TStDate);
                    DatePanel.add(TEnDate);
                    DatePanel.updateUI();
                    JRNo.setSelected(false);
               }
               else
               {
                    DatePanel.setBorder(new TitledBorder("Numbered"));
                    DatePanel.removeAll();
                    DatePanel.add(JCBlock);
                    DatePanel.add(TStNo);
                    DatePanel.add(TEnNo);
                    DatePanel.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }


     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               GetCurrentStock();
               setRowData();
               try
               {
                  getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                  tabreport = new TabReport(RowData,ColumnData,ColumnType);
                  getContentPane().add(tabreport,BorderLayout.CENTER);
                  tabreport.ReportTable.addKeyListener(new KeyList());
                  setSelected(true);
                  DeskTop.repaint();
                  DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
               }
          }
     }
     public class KeyList extends KeyAdapter
     {
           public void keyPressed(KeyEvent ke)
           {

                if (ke.getKeyCode()==KeyEvent.VK_INSERT)
                {
                     int i = tabreport.ReportTable.getSelectedRow();
                     int iInspection = common.toInt((String)VInspection.elementAt(i));

		     String SGRNNo1  = (String)RowData[i][1];
                     int iStatus = getInspectionStatus(SGRNNo1);
                     
                     if(iStatus==0)
                     {
	                 String SGRNNo  = (String)RowData[i][1];
	                 String SGateNo = (String)RowData[i][8];
	                 String SGIDate = (String)RowData[i][9];
	                 String SPJNo   = (String)VSPJNo.elementAt(i);
	                 GRNModiFrame grnmodiframe = new GRNModiFrame(DeskTop,SGRNNo,SGateNo,SGIDate,SPJNo,iMillCode,SSupTable);
	                 DeskTop.add(grnmodiframe);
	                 grnmodiframe.show();
	                 grnmodiframe.moveToFront();
	                 DeskTop.repaint();
	                 DeskTop.updateUI();
	             }
	             else
	             {
	                 JOptionPane.showMessageDialog(null,"Inspection Done for this GRN","Error",JOptionPane.ERROR_MESSAGE);
	             }
                }
           } 
     }
	
	 private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               
			
			
			if(ae.getSource()==BPrint)
			{
				if(JOptionPane.showConfirmDialog(null, "Are you sure you want to Print the Details?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
				{ 		
					updateItemStock();                        
				     printLabelNew();

				}
			}
		}
	}
	
     public void setDataIntoVector()
     {
           VGrnNo       = new Vector();
           VGrnDate     = new Vector();
           VBlock       = new Vector();
           VSupName     = new Vector();
           VOrdNo       = new Vector();
           VGno         = new Vector();
           VGDate       = new Vector();
           VDcNo        = new Vector();
           VDcDate      = new Vector();
           VINo         = new Vector();
           VIDate       = new Vector();
           VGrnCode     = new Vector();
           VGrnName     = new Vector();
           VIQty        = new Vector();
           VRQty        = new Vector();
           VIRate       = new Vector();
           VIAmt        = new Vector();
           VAccQty      = new Vector();
           VGrnDeptName = new Vector();
           VGrnCataName = new Vector();
           VGrnUnitName = new Vector();
           VStatus      = new Vector();
           VSPJNo       = new Vector();
           VId          = new Vector();
           VInspection  = new Vector();
           VRejFlag     = new Vector();
           VRejNo       = new Vector();
           VRejDate     = new Vector();
           VSlNo        = new Vector();
           VGrnType     = new Vector();
           VBin         = new Vector();
           VHsnType     = new Vector();

           SStDate       = TStDate.TDay.getText()+"."+TStDate.TMonth.getText()+"."+TStDate.TYear.getText();
           SEnDate       = TEnDate.TDay.getText()+"."+TEnDate.TMonth.getText()+"."+TEnDate.TYear.getText();
           String StDate = TStDate.TYear.getText()+TStDate.TMonth.getText()+TStDate.TDay.getText();
           String EnDate = TEnDate.TYear.getText()+TEnDate.TMonth.getText()+TEnDate.TDay.getText();

           String QString = getQString(StDate,EnDate);
System.out.println("QString-->"+QString);
           try
           {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                     String str1  = res.getString(1);  
                     String str2  = res.getString(2);
                     String str3  = res.getString(3);
                     String str4  = res.getString(4);
                     String str5  = res.getString(5);
                     String str6  = res.getString(6);
                     String str7  = res.getString(7);
                     String str8  = res.getString(8);
                     String str9  = res.getString(9);
                     String str10 = res.getString(10);
                     String str11 = res.getString(11);
                     String str12 = res.getString(12);
                     String str13 = res.getString(13);
                     String str14 = res.getString(14);
                     String str15 = res.getString(15);
                     String str16 = res.getString(16);
                     String str17 = res.getString(17);
                     String str18 = res.getString(18);
                     String str19 = res.getString(19);
                     String str20 = res.getString(20);
                     String str21 = res.getString(21);
                     String str22 = res.getString(22);
                     String str23 = res.getString(23);
                     String str24 = res.getString(24);
                     String str25 = res.getString(25);
                     String str26 = res.getString(26);
                     String str27 = res.getString(27);
                     String str28 = res.getString(28);
                     String str29 = res.getString(29);
                     String str30 = res.getString(30);
    
                     VGrnNo       .addElement(common.parseNull(str1));
                     VGrnDate     .addElement(common.parseDate(str2));
                     VBlock       .addElement(common.parseNull(str3));
                     VSupName     .addElement(common.parseNull(str4));
                     VOrdNo       .addElement(common.parseNull(str5));
                     VGno         .addElement(common.parseNull(str6));
                     VGDate       .addElement(common.parseDate(str7));
                     VDcNo        .addElement(common.parseNull(str8));
                     VDcDate      .addElement(common.parseDate(str9));
                     VINo         .addElement(common.parseNull(str10));
                     VIDate       .addElement(common.parseDate(str11));
                     VGrnCode     .addElement(common.parseNull(str12));
                     VGrnName     .addElement(common.parseNull(str13));
                     VIQty        .addElement(common.parseNull(str14));
                     VRQty        .addElement(common.parseNull(str15));
                     VIRate       .addElement(common.parseNull(str16));
                     VIAmt        .addElement(common.parseNull(str17));
                     VAccQty      .addElement(common.parseNull(str18));
                     VGrnDeptName .addElement(common.parseNull(str19));
                     VGrnCataName .addElement(common.parseNull(str20));
                     VGrnUnitName .addElement(common.parseNull(str21));
                     VSPJNo       .addElement(common.parseNull(str22));
                     if(str24.equals("0"))
                     {
                         VStatus      .addElement("Not Inspected");
                     }
                     else
                     {
                         VStatus      .addElement("Inspected");
                     }
                     VId          .addElement(common.parseNull(str23));
                     VInspection  .addElement(common.parseNull(str24));

                     if(str25.equals("0"))
                     {
                         VGrnType     .addElement("Grn");
                     }
                     else
                     {
                         VGrnType     .addElement("Rejection");
                     }
                     VRejFlag     .addElement(common.parseNull(str25));
                     VRejNo       .addElement(common.parseNull(str26));
                     VRejDate     .addElement(common.parseNull(str27));
                     VSlNo        .addElement(common.parseNull(str28));
                     VBin         .addElement(common.parseNull(str29));
                     VHsnType     .addElement(common.parseNull(str30));  
                    
               }
               res.close();
               stat.close();
           }
           catch(Exception ex){System.out.println(ex);}
     }
     public void setRowData()
     {
         System.out.println("VGrnNo.size--->"+VGrnNo.size());
         RowData     = new Object[VGrnNo.size()][ColumnData.length];

         
         for(int i=0;i<VGrnNo.size();i++)
         {

             
               RowData[i][0]  = (String)VGrnType.elementAt(i);
               RowData[i][1]  = (String)VGrnNo.elementAt(i);
               RowData[i][2]  = (String)VGrnDate.elementAt(i);
               RowData[i][3]  = (String)VRejNo.elementAt(i);
               RowData[i][4]  = (String)VRejDate.elementAt(i);
               RowData[i][5]  = (String)VBlock.elementAt(i);
               RowData[i][6]  = (String)VSupName.elementAt(i);
               RowData[i][7]  = (String)VOrdNo.elementAt(i);
               RowData[i][8]  = (String)VGno.elementAt(i);
               RowData[i][9]  = (String)VGDate.elementAt(i);
               RowData[i][10] = (String)VDcNo.elementAt(i);
               RowData[i][11] = (String)VDcDate.elementAt(i);
               RowData[i][12] = (String)VINo.elementAt(i);
               RowData[i][13] = (String)VIDate.elementAt(i);
               RowData[i][14] = (String)VGrnCode.elementAt(i);
               RowData[i][15] = (String)VGrnName.elementAt(i);
               RowData[i][16] = (String)VBin .elementAt(i);
               RowData[i][17] = (String)VIQty.elementAt(i);
               RowData[i][18] = (String)VRQty.elementAt(i);
               RowData[i][19] = (String)VIRate.elementAt(i);
               RowData[i][20] = (String)VIAmt.elementAt(i);
               RowData[i][21] = (String)VAccQty.elementAt(i);
               String SCode=(String)VGrnCode.elementAt(i);
               int iIndex = common.indexOf(VGRNItemCode,SCode);    
               if(iIndex>=0){
               RowData[i][22] = (String)VCurrentStock.elementAt(iIndex);
               }else{
                RowData[i][22] = "0";
               }
               RowData[i][23] = (String)VGrnDeptName.elementAt(i);
               RowData[i][24] = (String)VGrnCataName.elementAt(i);
               RowData[i][25] = (String)VGrnUnitName.elementAt(i);
               RowData[i][26] = (String)VStatus.elementAt(i);
               RowData[i][27] = (String)VIQty.elementAt(i);
			
			RowData[i][28] = "";
			RowData[i][29]  = new Boolean(false);                         // click box
        }

      
     }
     public String getQString(String StDate,String EnDate)
     {
          String QString  = "";

        /*  if(JRPeriod.isSelected())
          {
            
               QString  = " SELECT GRN.GrnNo, GRN.GrnDate, OrdBlock.BlockName, "+
                          " "+SSupTable+".Name, GRN.OrderNo, GRN.GateInNo, GRN.GateInDate, "+
                          " GRN.DcNo, GRN.DcDate, GRN.InvNo, GRN.InvDate, GRN.Code, "+
                          " InvItems.Item_Name, GRN.InvQty, GRN.MillQty, GRN.InvRate, "+
                          " GRN.GrnValue, GRN.GrnQty, Dept.Dept_Name, Cata.Group_Name, "+
                          " Unit.Unit_Name, GRN.SPJNo,GRN.Id,Grn.Inspection, "+
                          " Grn.RejFlag,Grn.RejNo,Grn.RejDate,Grn.SlNo "+
                          " FROM (((((GRN INNER JOIN OrdBlock ON GRN.GrnBlock = OrdBlock.Block) "+
                          " INNER JOIN "+SSupTable+" ON GRN.Sup_Code = "+SSupTable+".Ac_Code) "+
                          " INNER JOIN InvItems ON GRN.Code = InvItems.Item_Code) "+
                          " INNER JOIN Dept ON GRN.Dept_Code = Dept.Dept_code) "+
                          " INNER JOIN Cata ON GRN.Group_Code = Cata.Group_Code) "+
                          " INNER JOIN Unit ON GRN.Unit_Code = Unit.Unit_Code "+
                          " Where GRN.GrnDate >= '"+StDate+"' and GRN.GrnDate <='"+EnDate+"' "+
                          " And Grn.MillCode="+iMillCode;
          }
          else
          {
               QString  = " SELECT GRN.GrnNo, GRN.GrnDate, OrdBlock.BlockName, "+
                          " "+SSupTable+".Name, GRN.OrderNo, GRN.GateInNo, GRN.GateInDate, "+
                          " GRN.DcNo, GRN.DcDate, GRN.InvNo, GRN.InvDate, GRN.Code, "+
                          " InvItems.Item_Name, GRN.InvQty, GRN.MillQty, GRN.InvRate, "+
                          " GRN.GrnValue, GRN.GrnQty, Dept.Dept_Name, Cata.Group_Name, "+
                          " Unit.Unit_Name, GRN.SPJNo,GRN.Id,Grn.Inspection, "+
                          " Grn.RejFlag,Grn.RejNo,Grn.RejDate,Grn.SlNo "+
                          " FROM (((((GRN INNER JOIN OrdBlock ON GRN.GrnBlock = OrdBlock.Block) "+
                          " INNER JOIN "+SSupTable+" ON GRN.Sup_Code = "+SSupTable+".Ac_Code) "+
                          " INNER JOIN InvItems ON GRN.Code = InvItems.Item_Code) "+
                          " INNER JOIN Dept ON GRN.Dept_Code = Dept.Dept_code) "+
                          " INNER JOIN Cata ON GRN.Group_Code = Cata.Group_Code) "+
                          " INNER JOIN Unit ON GRN.Unit_Code = Unit.Unit_Code "+
                          " Where GRNBlock = "+JCBlock.getSelectedIndex()+" And "+
                          " GRN.GRNNo >="+TStNo.getText()+" and GRN.GrnNo <= "+TEnNo.getText() +
                          " And Grn.MillCode="+iMillCode;
          } */


  if(JRPeriod.isSelected())
          {
               if (iMillCode ==0)
               {
               QString  = " SELECT GRN.GrnNo, GRN.GrnDate, OrdBlock.BlockName, "+
                          " "+SSupTable+".Name, GRN.OrderNo, GRN.GateInNo, GRN.GateInDate, "+
                          " GRN.DcNo, GRN.DcDate, GRN.InvNo, GRN.InvDate, GRN.Code, "+
                          " InvItems.Item_Name, GRN.InvQty, GRN.MillQty, GRN.InvRate, "+
                          " GRN.GrnValue, GRN.GrnQty, Dept.Dept_Name, Cata.Group_Name, "+
                          " Unit.Unit_Name, GRN.SPJNo,GRN.Id,Grn.Inspection, "+
                          " Grn.RejFlag,Grn.RejNo,Grn.RejDate,Grn.SlNo ,InvItems.LocName,invitems.hsntype "+
                          " FROM (((((GRN INNER JOIN OrdBlock ON GRN.GrnBlock = OrdBlock.Block) "+
                          " INNER JOIN "+SSupTable+" ON GRN.Sup_Code = "+SSupTable+".Ac_Code) "+
                          " INNER JOIN InvItems ON GRN.Code = InvItems.Item_Code  ) "+
                          " INNER JOIN Dept ON GRN.Dept_Code = Dept.Dept_code) "+
                          " INNER JOIN Cata ON GRN.Group_Code = Cata.Group_Code) "+
                          " INNER JOIN Unit ON GRN.Unit_Code = Unit.Unit_Code "+
                          " Where GRN.GrnDate >= '"+StDate+"' and GRN.GrnDate <='"+EnDate+"' "+
                          " and invitems.hsntype=0 And Grn.MillCode="+iMillCode;
                }
              else if (iMillCode == 1)
                {
               QString  = " SELECT GRN.GrnNo, GRN.GrnDate, OrdBlock.BlockName, "+
                          " "+SSupTable+".Name, GRN.OrderNo, GRN.GateInNo, GRN.GateInDate, "+
                          " GRN.DcNo, GRN.DcDate, GRN.InvNo, GRN.InvDate, GRN.Code, "+
                          " InvItems.Item_Name, GRN.InvQty, GRN.MillQty, GRN.InvRate, "+
                          " GRN.GrnValue, GRN.GrnQty, Dept.Dept_Name, Cata.Group_Name, "+
                          " Unit.Unit_Name, GRN.SPJNo,GRN.Id,Grn.Inspection, "+
                          " Grn.RejFlag,Grn.RejNo,Grn.RejDate,Grn.SlNo,dyeinginvitems.LocName,invitems.hsntype "+
                          " FROM ((((((GRN INNER JOIN OrdBlock ON GRN.GrnBlock = OrdBlock.Block  ) "+
                          " INNER JOIN "+SSupTable+" ON GRN.Sup_Code = "+SSupTable+".Ac_Code) "+
                          " INNER JOIN InvItems ON GRN.Code = InvItems.Item_Code) "+
                          " INNER JOIN dyeinginvitems ON GRN.Code = dyeinginvitems.Item_Code) "+
                          " INNER JOIN Dept ON GRN.Dept_Code = Dept.Dept_code) "+
                          " INNER JOIN Cata ON GRN.Group_Code = Cata.Group_Code) "+
                          " INNER JOIN Unit ON GRN.Unit_Code = Unit.Unit_Code "+
                          " Where GRN.GrnDate >= '"+StDate+"' and GRN.GrnDate <='"+EnDate+"' "+
                          " and invitems.hsntype=0 And Grn.MillCode="+iMillCode;
                 }

          }
          else
          {
               if(iMillCode == 0)
               {
               QString  = " SELECT GRN.GrnNo, GRN.GrnDate, OrdBlock.BlockName, "+
                          " "+SSupTable+".Name, GRN.OrderNo, GRN.GateInNo, GRN.GateInDate, "+
                          " GRN.DcNo, GRN.DcDate, GRN.InvNo, GRN.InvDate, GRN.Code, "+
                          " InvItems.Item_Name, GRN.InvQty, GRN.MillQty, GRN.InvRate, "+
                          " GRN.GrnValue, GRN.GrnQty, Dept.Dept_Name, Cata.Group_Name, "+
                          " Unit.Unit_Name, GRN.SPJNo,GRN.Id,Grn.Inspection, "+
                          " Grn.RejFlag,Grn.RejNo,Grn.RejDate,Grn.SlNo,InvItems.LocName,invitems.hsntype "+
                          " FROM (((((GRN INNER JOIN OrdBlock ON GRN.GrnBlock = OrdBlock.Block  ) "+
                          " INNER JOIN "+SSupTable+" ON GRN.Sup_Code = "+SSupTable+".Ac_Code) "+
                          " INNER JOIN InvItems ON GRN.Code = InvItems.Item_Code) "+
                          " INNER JOIN Dept ON GRN.Dept_Code = Dept.Dept_code) "+
                          " INNER JOIN Cata ON GRN.Group_Code = Cata.Group_Code) "+
                          " INNER JOIN Unit ON GRN.Unit_Code = Unit.Unit_Code "+
                          " Where GRNBlock = "+JCBlock.getSelectedIndex()+" And "+
                          " GRN.GRNNo >="+TStNo.getText()+" and GRN.GrnNo <= "+TEnNo.getText() +
                          " and invitems.hsntype=0 And Grn.MillCode="+iMillCode;
                }

               else if(iMillCode == 1)
                {

               QString  = " SELECT GRN.GrnNo, GRN.GrnDate, OrdBlock.BlockName, "+
                          " "+SSupTable+".Name, GRN.OrderNo, GRN.GateInNo, GRN.GateInDate, "+
                          " GRN.DcNo, GRN.DcDate, GRN.InvNo, GRN.InvDate, GRN.Code, "+
                          " InvItems.Item_Name, GRN.InvQty, GRN.MillQty, GRN.InvRate, "+
                          " GRN.GrnValue, GRN.GrnQty, Dept.Dept_Name, Cata.Group_Name, "+
                          " Unit.Unit_Name, GRN.SPJNo,GRN.Id,Grn.Inspection, "+
                          " Grn.RejFlag,Grn.RejNo,Grn.RejDate,Grn.SlNo,dyeinginvitems.LocName ,invitems.hsntype "+
                          " FROM ((((((GRN INNER JOIN OrdBlock ON GRN.GrnBlock = OrdBlock.Block ) "+
                          " INNER JOIN "+SSupTable+" ON GRN.Sup_Code = "+SSupTable+".Ac_Code) "+
                          " INNER JOIN InvItems ON GRN.Code = InvItems.Item_Code) "+
                          " INNER JOIN dyeinginvitems ON GRN.Code = dyeinginvitems.Item_Code) "+
                          " INNER JOIN Dept ON GRN.Dept_Code = Dept.Dept_code) "+
                          " INNER JOIN Cata ON GRN.Group_Code = Cata.Group_Code) "+
                          " INNER JOIN Unit ON GRN.Unit_Code = Unit.Unit_Code "+
                          " Where GRNBlock = "+JCBlock.getSelectedIndex()+" And "+
                          " GRN.GRNNo >="+TStNo.getText()+" and GRN.GrnNo <= "+TEnNo.getText() +
                          " and invitems.hsntype=0 And Grn.MillCode="+iMillCode;
                }
          }

          if(JCFilter.getSelectedIndex()==1)
               QString = QString+" and GRN.Inspection = 1";

          if(JCFilter.getSelectedIndex()==2)
               QString = QString+" and GRN.Inspection = 0";

          if(JCOrder.getSelectedIndex() == 0)
               QString = QString+" Order By GRN.GrnNo,GRN.GrnDate,Grn.SlNo,Grn.RejFlag";
          if(JCOrder.getSelectedIndex() == 1)
               QString = QString+" Order By InvItems.Item_Name,GRN.GrnDate,Grn.GrnNo,Grn.SlNo,Grn.RejFlag";
          if(JCOrder.getSelectedIndex() == 2)
               QString = QString+" Order By "+SSupTable+".Name,GRN.GrnDate,Grn.GrnNo,Grn.SlNo,Grn.RejFlag";
          if(JCOrder.getSelectedIndex() == 3)
               QString = QString+" Order By Dept.Dept_Name,GRN.GrnDate,Grn.GrnNo,Grn.SlNo,Grn.RejFlag";
          if(JCOrder.getSelectedIndex() == 4)
               QString = QString+" Order By Cata.Group_Name,GRN.GrnDate,Grn.GrnNo,Grn.SlNo,Grn.RejFlag";
          if(JCOrder.getSelectedIndex() == 5)
               QString = QString+" Order By Unit.Unit_Name,GRN.GrnDate,Grn.GrnNo,Grn.SlNo,Grn.RejFlag";

          return QString;
     }
   public int getInspectionStatus(String SGRNNo)
   {
	int iInspectionStatus = -1 ;
	try
	{
                ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                Connection      theConnection =  oraConnection.getConnection();
                Statement       stat =  theConnection.createStatement();
                ResultSet res  = stat.executeQuery("Select count(1) from GRN where GrnNo='"+SGRNNo+"' and GRN.RejFlag=0 and GRN.Inspection=1");
		while (res.next())
               	{
        		iInspectionStatus = res.getInt(1);
			
		}
		res.close();
                stat.close();
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return iInspectionStatus;
	
   }

	private void GetCurrentStock()
	{

       VCurrentStock  = new Vector();
       VGRNItemCode   = new Vector();

       SStDate       = TStDate.TDay.getText()+"."+TStDate.TMonth.getText()+"."+TStDate.TYear.getText();
       SEnDate       = TEnDate.TDay.getText()+"."+TEnDate.TMonth.getText()+"."+TEnDate.TYear.getText();
       String StDate = TStDate.TYear.getText()+TStDate.TMonth.getText()+TStDate.TDay.getText();
       String EnDate = TEnDate.TYear.getText()+TEnDate.TMonth.getText()+TEnDate.TDay.getText();

        String QS="";

		if(JRPeriod.isSelected())
		{
		QS=" select itemcode,sum(Stock) from itemstock where millcode="+iMillCode+" "+
			  " and itemcode in (select distinct code from grn where GRN.GrnDate >= '"+StDate+"' and GRN.GrnDate <='"+EnDate+"'  and grn.millcode= "+iMillCode+"  ) "+
			  " group by itemcode ";

		}else{


		QS=" select itemcode,sum(Stock) from itemstock where millcode="+iMillCode+" "+
			  " and itemcode in (select distinct code from grn where GRN.GRNNo >="+TStNo.getText()+" and GRN.GrnNo <= "+TEnNo.getText() +"  and grn.millcode= "+iMillCode+" ) "+
			  " group by itemcode ";

		}
 

           try
           {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {

                  VGRNItemCode . addElement(common.parseNull(res.getString(1)));
                  VCurrentStock.addElement(common.parseNull(res.getString(2)));
 
                }

            
               res.close();
               stat.close();
           }
           catch(Exception ex){System.out.println(ex);}


   }

/*
  private String GetCurrentStockValue(String Scode)
  {

    String SStock="";

           for(int i=0;i<VGRNItemCode.size();i++)
          {
               
               String SGrnItemCode =(String)VGRNItemCode.get(i);

               if(Scode.equals(SGrnItemCode)) 
               {

                    SStock=(String)VCurrentStock.get(i);

              System.out.println("SStock"+SStock);

               } 

          }
           

    return SStock;

 }*/


	private void printLabelNew()
     {
          try
          {
			
			  
               SerialPort serialPort    = getSerialPort();
               serialPort               . setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
               OutputStream output      = serialPort.getOutputStream();
			  
			   
			setItemDetails();
			
			   System.out.println("commin final "+serialPort);
			
				for(int i=0;i<VGrnNo.size();i++)
				{
					Boolean bValue = (Boolean)tabreport.ReportTable.getValueAt(i, 29);
					int iCount = common.toInt((String)tabreport.ReportTable.getValueAt(i, 28));
						System.out.println("Count--->"+iCount);
					

					if(bValue.booleanValue() && iCount >0)
					{
						for(int j=0 ; j<iCount;j++){
						System.out.println("Count--->"+iCount);
						
						String SCode   = common.parseNull((String)tabreport.ReportTable.getValueAt(i, 14));				
						String SItemNamee = "ITEM: "+common.parseNull((String)tabreport.ReportTable.getValueAt(i, 15));
						String SItemCoddee   = "CODE: "+common.parseNull((String)tabreport.ReportTable.getValueAt(i, 14));	
						String SLoc = "LOC: "+common.parseNull((String)tabreport.ReportTable.getValueAt(i, 16));
						String SQty = "QTY: "+common.parseNull((String)tabreport.ReportTable.getValueAt(i, 27));
						String SDate  = "DATE: "+common.parseDate((String)tabreport.ReportTable.getValueAt(i,2));

						String SCatl = "CATL: "+Catl(SCode);
						String SDraw = "DRAW: "+Draw(SCode);
						
						String SLocDraw = SCatl  +"      "+  SDraw;
						String SLocQty = SLoc  +"             "+  SQty;
						String SItemDate = SCode+"        "+ SDate;
						
						String str = "";
						String substr = "";
						String substr2 = "";
						
						 if(SItemNamee.length()>28) {
							
							substr = SItemNamee.substring(0, 28);
							substr2 = SItemNamee.substring(28,SItemNamee.length());
						}else {
							substr = SItemNamee;	
						}	
	
				/*String Str2 = "191100301600020"+substr+"\r";     //191100501400070    191100300800070
				String Str4 = "191100301400055"+substr2+"\r";     //191100501400070    191100300800070
				String Str3 = "191100301100020"+SLocDraw+"\r";
                    String Str5 = "191100300800020"+SLocQty+"\r";
                    String Str7 = "1e6205000200070"+SCode+"\r";
				String Str8 = "191100200000075"+SItemDate+"\r";*/
				
				String Str2 = "191100301600065"+substr+"\r";     //191100501400070    191100300800070
				String Str4 = "191100301400100"+substr2+"\r";     //191100501400070    191100300800070
				String Str3 = "191100301200065"+SItemCoddee+"\r";
                    String Str5 = "191100301000065"+SCatl+"\r";
				String Str10 = "191100300800065"+SDraw+"\r";
                    String Str7 = "191100300600065"+SLoc+"\r";
				String Str11 = "191100300400065"+SQty+"\r";
				String Str8 = "1e6205000000185"+SCode+"\r";

				output.write("n".getBytes());
                    output.write("f285".getBytes());
                    output.write("L".getBytes());
                    output.write("H10".getBytes());
                    output.write("D11".getBytes());

             
				output.write(Str3.getBytes());
                    output.write(Str4.getBytes());
                    output.write(Str5.getBytes());
                    output.write(Str7.getBytes());
				output.write(Str2.getBytes()); 
			     output.write(Str8.getBytes()); 
				output.write(Str10.getBytes()); 
				output.write(Str11.getBytes());
				output.write("E\r".getBytes()); 
				
               /*     output.write(Str3.getBytes());
                    output.write(Str4.getBytes());
                    output.write(Str5.getBytes());
                    output.write(Str7.getBytes());
				output.write(Str2.getBytes());
				 output.write(Str8.getBytes());
                    
				output.write("E\r".getBytes()); */
               
			   
			   
							  
			System.out.println("commin all print final");

					}  // this is for loop
					}  // this is if 
				} // this is for loop

			serialPort.close();
 
		  }catch(Exception ex)	    {
			  ex.printStackTrace();
		  }
	 }	  

     private SerialPort getSerialPort() 
     {
          SerialPort serialPort         = null;
     
          try
          {
               Enumeration portList     = CommPortIdentifier.getPortIdentifiers();
     
               while(portList.hasMoreElements())
               {
                    CommPortIdentifier portId = (CommPortIdentifier)portList.nextElement();

                    if(portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort     = (SerialPort)portId.open("comapp", 2000);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               
          }
     
          return serialPort;
     }

  
	private String Catl(String SCode){
		return common.parseNull((String)ACatl.get(AItemCode.indexOf(SCode)));
	}
	
	private String Draw(String SCode){
		return common.parseNull((String)ADraw.get(AItemCode.indexOf(SCode)));
	}
	
	private void setItemDetails()
	{
		
		ACatl			= null;
		ADraw			= null;
		AItemCode			= null;
		
		ACatl			= new ArrayList();
		ADraw			= new ArrayList();
		AItemCode			= new ArrayList();
		
		ACatl			. clear();
		ADraw			. clear();
		AItemCode			. clear();
	
	try
		{
			if(theConnection ==null) {

				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
			Statement theStatement = theConnection.createStatement();

			String QS100 = " select Catl,Draw,Item_Code from invitems  ";
			ResultSet rs1 = theStatement.executeQuery(QS100);

			while (rs1.next())
			{
				ACatl		. add(common.parseNull(rs1.getString(1)));
				ADraw		. add(common.parseNull(rs1.getString(2)));
				AItemCode      . add(common.parseNull(rs1.getString(3)));
			}
			rs1.close();
			theStatement 		.close();
		}
		catch(Exception ex)
			{
				ex.printStackTrace();
			System.out.println("From InvItems "+ex);
			}
	
	}
	
	


private void updateItemStock(){
					
		try{
			
			if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
               }
			theConnection.setAutoCommit(false);
			
			for(int i=0;i<VGrnNo.size();i++)
				{
					
				Boolean bValue = (Boolean)tabreport.ReportTable.getValueAt(i, 28);
				if(bValue.booleanValue())			
				{
				String SCode = common.parseNull((String)tabreport.ReportTable.getValueAt(i, 14));				
				
				String QS33 = "Update invitems set BarcodePrintingStatus = 1 Where Item_Code = '"+SCode+"'  ";
			
				System.out.println(QS33);
			
			

			
			PreparedStatement ps1 = theConnection.prepareStatement(QS33);
			ps1.executeQuery();	
			ps1.close();
				
			}
			}
			theConnection			. commit();
			theConnection			. setAutoCommit(true);	
			
				
		}
		
			catch(Exception e)
			{
			try
			{
				theConnection		. rollback();
				theConnection		. setAutoCommit(true);
			}
			catch(Exception ex)
			{
			ex.printStackTrace();
			}
			}

}
	

} // end of the file
	
	
	
