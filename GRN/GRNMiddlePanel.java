package GRN;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class GRNMiddlePanel extends JPanel 
{
         GRNInvMiddlePanel MiddlePanel;
         Object RowData[][],PIdData[],IdData[],TaxCData[],PGValData[];

         Vector VTaxClaim,VPGrnValue,VBlockCode;
            
         JLayeredPane DeskTop;
         Vector VCode,VName;
         int iMillCode;

         Common common = new Common();

         public GRNMiddlePanel(JLayeredPane DeskTop,Vector VCode,Vector VName,int iMillCode)
         {
              this.DeskTop  = DeskTop;
              this.VCode    = VCode;
              this.VName    = VName;
              this.iMillCode= iMillCode;

              setLayout(new BorderLayout());
         }

        public void setData(GRNRecords grnrecords)
        {
            String CData[] = {"Code","Name","MRS No","Order No","Order Qty","Pending Qty","Inv/DC Qty","Accepted/Recd Qty","Rate","Disc (%)","Cenvat (%)","Tax (%)","Surcharge (%)","Basic","Disc (Rs)","Vat (Rs)","Tax (Rs)","Surcharge (Rs)","Net (Rs)","Department","Group","Unit"};
            String CType[] = {"S"   ,"S"   ,"N"     ,"N"       ,"N"        ,"N"          ,"N"         ,"N"                ,"N"   ,"N"       ,"N"         ,"N"      ,"N"            ,"N"    ,"N"        ,"N"       ,"N"       ,"N"             ,"N"       ,"B"         ,"B"    ,"B"};

            RowData    = new Object[grnrecords.VGItemCode.size()][CData.length];
            IdData     = new Object[grnrecords.VGItemCode.size()];
            PIdData    = new Object[grnrecords.VGItemCode.size()];

            VTaxClaim  = new Vector();
            VPGrnValue = new Vector();
            VBlockCode = new Vector(); 

            for(int i=0;i<grnrecords.VGItemCode.size();i++)
            {
                double dAccQty=common.toDouble((String)grnrecords.VGAccQty.elementAt(i));
                RowData[i][0] = (String)grnrecords.VGItemCode.elementAt(i);
                RowData[i][1] = (String)grnrecords.VGItemName.elementAt(i);
                RowData[i][2] = (String)grnrecords.VGMRS.elementAt(i);
                RowData[i][3] = (String)grnrecords.VGBlock.elementAt(i)+"-"+(String)grnrecords.VGOrdNo.elementAt(i);
                RowData[i][4] = (String)grnrecords.VGOrdQty.elementAt(i);
                RowData[i][5] = (String)grnrecords.VGPending.elementAt(i);
                RowData[i][6] = (String)grnrecords.VGInvQty.elementAt(i);
                if (dAccQty == 0)
                {
                    RowData[i][7] = (String)grnrecords.VGRecdQty.elementAt(i);
                }
                else
                {
                    RowData[i][7] = (String)grnrecords.VGAccQty.elementAt(i);
                }
                RowData[i][8] = (String)grnrecords.VGInvRate.elementAt(i);
                RowData[i][9] = (String)grnrecords.VGDiscPer.elementAt(i);
                RowData[i][10] = (String)grnrecords.VGCenVatPer.elementAt(i);
                RowData[i][11] = (String)grnrecords.VGTaxPer.elementAt(i);
                RowData[i][12] = (String)grnrecords.VGSurPer.elementAt(i);
                RowData[i][13] = " ";
                RowData[i][14] = " ";
                RowData[i][15] = " ";
                RowData[i][16] = " ";
                RowData[i][17] = " ";
                RowData[i][18] = " ";
                RowData[i][19] = (String)grnrecords.VGDeptName.elementAt(i);
                RowData[i][20] = (String)grnrecords.VGGroupName.elementAt(i);
                RowData[i][21] = (String)grnrecords.VGUnitName.elementAt(i);
                IdData[i]      = (String)grnrecords.VId.elementAt(i);
                PIdData[i]     = (String)grnrecords.VPId.elementAt(i);
                VTaxClaim.addElement((String)grnrecords.VTaxC.elementAt(i));
                VPGrnValue.addElement((String)grnrecords.VPGrnValue.elementAt(i));
                VBlockCode.addElement((String)grnrecords.VBlockCode.elementAt(i));
            }
            try
            {
                  MiddlePanel           = new GRNInvMiddlePanel(DeskTop,VCode,VName,RowData,CData,CType,iMillCode);
                  add(MiddlePanel,BorderLayout.CENTER);
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
        }
}         
