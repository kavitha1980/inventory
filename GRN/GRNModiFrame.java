package GRN;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GRNModiFrame extends JInternalFrame
{

     JTextField     TGINo,TGIDate,TInvDate,TDCDate;
     DateField      TDate;
     JTextField     TGrnNo,TInvNo,TDCNo,TSupName;
     WholeNumberField TInvoice;
     GRNMiddlePanel MiddlePanel;
     JPanel         TopPanel,BottomPanel;
     JButton        BOk,BCancel;

     JLayeredPane   DeskTop;
     Vector         VCode,VName;
     StatusPanel    SPanel;
     String         SGrnNo,SGateNo,SGIDate,SPJNo;
     GRNRecords     grnrecords;

     Common common         = new Common();
     Control control       = new Control();
     int iMillCode;
     String SSupTable;

     GRNModiFrame(JLayeredPane DeskTop,String SGrnNo,String SGateNo,String SGIDate,String SPJNo,int iMillCode,String SSupTable)
     {
          this.DeskTop            = DeskTop;
          this.SGrnNo             = SGrnNo;
          this.SGateNo            = SGateNo;
          this.SGIDate            = SGIDate;
          this.SPJNo              = SPJNo;
          this.iMillCode          = iMillCode;
          this.SSupTable          = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setData();
          show();
     }
     public void createComponents()
     {
          BOk        = new JButton("Okay");
          BCancel    = new JButton("Abort");

          TDate       = new DateField();
          TGrnNo      = new JTextField();
          TGINo       = new JTextField();
          TGIDate     = new JTextField();
          TInvDate    = new JTextField();
          TDCDate     = new JTextField();
          TDCDate     = new JTextField();
          TInvNo      = new JTextField();
          TDCNo       = new JTextField();
          TSupName    = new JTextField();
          TInvoice    = new WholeNumberField(2);

          MiddlePanel = new GRNMiddlePanel(DeskTop,VCode,VName,iMillCode);

          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          TGrnNo.setEditable(false);
          TGINo.setEditable(false);
          TGIDate.setEditable(false);
          TSupName.setEditable(false);
          TDate.setEditable(false);
          TInvNo.setEditable(false);
          TInvDate.setEditable(false);
          TDCNo.setEditable(false);
          TDCDate.setEditable(false);
     }
     public void setLayouts()
     {
          setTitle("Modification of Invoice Valuation");

          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);

          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(5,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());
     }
     public void addComponents()
     {
          TopPanel.add(new JLabel("Gate Inward No"));
          TopPanel.add(TGINo);

          TopPanel.add(new JLabel("Gate Inward Date"));
          TopPanel.add(TGIDate);

          TopPanel.add(new JLabel("GRN No"));
          TopPanel.add(TGrnNo);

          TopPanel.add(new JLabel("GRN Date"));
          TopPanel.add(TDate);

          TopPanel.add(new JLabel("Supplier"));
          TopPanel.add(TSupName);

          TopPanel.add(new JLabel("No. of Bills"));
          TopPanel.add(TInvoice);

          TopPanel.add(new JLabel("Invoice No"));
          TopPanel.add(TInvNo);

          TopPanel.add(new JLabel("Invoice Date"));
          TopPanel.add(TInvDate);

          TopPanel.add(new JLabel("DC No"));
          TopPanel.add(TDCNo);

          TopPanel.add(new JLabel("DC Date"));
          TopPanel.add(TDCDate);


          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
        BOk.addActionListener(new ActList());
     }
     public class ActList implements ActionListener
     {
            public void actionPerformed(ActionEvent ae)
            {
                    BOk.setEnabled(false);
                    updateGRNDetails();
                    removeHelpFrame();
            }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
     public void updateGRNDetails()
     {
          if(common.toInt(SPJNo) > 0)
          {
               String QString = "Update GRN Set ";
               try
               {
                    ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                    Connection      theConnection =  oraConnection.getConnection();
                    Statement       stat =  theConnection.createStatement();
                    Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
                    for(int i=0;i<RowData.length;i++)
                    {
                         String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                         String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                         String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
     
                         String QS1 = QString;
                         QS1 = QS1+"Dept_Code=0"+SDeptCode+",";
                         QS1 = QS1+"Group_Code=0"+SGroupCode+",";
                         QS1 = QS1+"Unit_Code=0"+SUnitCode;
                         QS1 = QS1+" Where Id = "+(String)MiddlePanel.IdData[i];
                         stat.executeUpdate(QS1);
                     }
                     stat.close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
          else
          {

               String QString = "Update GRN Set ";
               try
               {
                    ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                    Connection      theConnection =  oraConnection.getConnection();
                    Statement       stat =  theConnection.createStatement();

                    Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();

                    /*String SAdd       = MiddlePanel.MiddlePanel.TAdd.getText();
                    String SLess      = MiddlePanel.MiddlePanel.TLess.getText();
                    double dpm        = common.toDouble(SAdd)-common.toDouble(SLess);
                    double dBasic     = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
                    double dRatio     = dpm/dBasic;*/

                    for(int i=0;i<RowData.length;i++)
                    {
                         String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                         String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                         String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);

                         /*String STaxC      = (String)MiddlePanel.VTaxClaim.elementAt(i);
                         String SPGrnValue = (String)MiddlePanel.VPGrnValue.elementAt(i);
                         String SBlockCode = (String)MiddlePanel.VBlockCode.elementAt(i);

                         String  SItemCode  = (String)RowData[i][0];
      
                         String  SBasic     = (String)RowData[i][13];
                         String  SInvAmount = (String)RowData[i][18];
                         String  SCenvat    = (String)RowData[i][15];

                         String SMisc       = common.getRound(common.toDouble(SBasic)*dRatio,3);

                         String SGrnValue = "";

                         if(STaxC.equals("0"))
                              SGrnValue = common.getRound(common.toDouble(SInvAmount) + common.toDouble(SMisc),2);
                         else
                              SGrnValue = common.getRound(common.toDouble(SInvAmount) + common.toDouble(SMisc) - common.toDouble(SCenvat),2);*/


                         String QS1 = QString;
                         /*QS1 = QS1+"InvNo='"+TInvNo.getText()+"',";
                         QS1 = QS1+"InvDate='"+common.pureDate(TInvDate.getText())+"',";
                         QS1 = QS1+"DCNo='"+TDCNo.getText()+"',";
                         QS1 = QS1+"DCDate='"+common.pureDate(TDCDate.getText())+"',";
                         QS1 = QS1+"InvRate=0"+(String)RowData[i][8]+",";
                         QS1 = QS1+"DiscPer=0"+(String)RowData[i][9]+",";
                         QS1 = QS1+"Disc=0"+(String)RowData[i][14]+",";
                         QS1 = QS1+"CenvatPer=0"+(String)RowData[i][10]+",";
                         QS1 = QS1+"Cenvat=0"+(String)RowData[i][15]+",";
                         QS1 = QS1+"TaxPer=0"+(String)RowData[i][11]+",";
                         QS1 = QS1+"Tax=0"+(String)RowData[i][16]+",";
                         QS1 = QS1+"SurPer=0"+(String)RowData[i][12]+",";
                         QS1 = QS1+"Sur=0"+(String)RowData[i][17]+",";
                         QS1 = QS1+"InvAmount=0"+(String)RowData[i][18]+",";
                         QS1 = QS1+"InvNet=0"+(String)RowData[i][18]+",";
                         QS1 = QS1+"Plus=0"+SAdd+",";
                         QS1 = QS1+"Less=0"+SLess+",";
                         QS1 = QS1+"Misc=0"+SMisc+",";
                         QS1 = QS1+"GrnValue=0"+SGrnValue+",";*/
                         QS1 = QS1+"Dept_Code=0"+SDeptCode+",";
                         QS1 = QS1+"Group_Code=0"+SGroupCode+",";
                         QS1 = QS1+"Unit_Code=0"+SUnitCode+",";
                         QS1 = QS1+"NoOfBills=0"+TInvoice.getText()+" ";
                         QS1 = QS1+" Where Id = "+(String)MiddlePanel.IdData[i];

                         stat.executeUpdate(QS1);

                         //updateItemMaster(stat,SItemCode,SBlockCode,SPGrnValue,SGrnValue);
                    }
                    stat.close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     /*private void updateItemMaster(Statement stat,String SItemCode,String SBlockCode,String SPGrnValue,String SGrnValue)
     {
          try
          {
               int iBlockCode = common.toInt(SBlockCode);

               String SNetValue = common.getRound(common.toDouble(SGrnValue) - common.toDouble(SPGrnValue),2);
               String QS = "";

               if(iMillCode==0)
               {
                    if(iBlockCode>1)
                    {
                         QS = "Update InvItems Set  RecVal =RecVal+"+SNetValue+",";
                         QS = QS+" IssVal=IssVal+"+SNetValue+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    else
                    {
                         QS = "Update InvItems Set RecVal =RecVal+"+SNetValue+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
               }
               else
               {

                    if(iBlockCode>1)
                    {
                         QS = "Update DyeingInvItems Set  RecVal =RecVal+"+SNetValue+",";
                         QS = QS+" IssVal=IssVal+"+SNetValue+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    else
                    {
                         QS = "Update DyeingInvItems Set RecVal =RecVal+"+SNetValue+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
               }
               stat.execute(QS);
          }
          catch(Exception e)
          {
               System.out.println("E6"+e);
          }

     }*/ 

     public void setData()
     {
          grnrecords = new GRNRecords(SGrnNo,SGateNo,iMillCode,SSupTable);

          TGrnNo  .setText(SGrnNo);
          TGINo   .setText(SGateNo);
          TGIDate .setText(SGIDate);
          TSupName.setText(grnrecords.SSupName);
          TInvNo  .setText(grnrecords.SInvNo);
          TInvDate.setText(grnrecords.SInvDate);
          TDCNo   .setText(grnrecords.SDCNo);
          TDCDate .setText(grnrecords.SDCDate);
          TDate   .fromString(common.parseDate(grnrecords.SGDate));
          TInvoice.setText(grnrecords.SInvoice);

          MiddlePanel.setData(grnrecords);
          MiddlePanel.MiddlePanel.TAdd.setText(grnrecords.SAdd);
          MiddlePanel.MiddlePanel.TLess.setText(grnrecords.SLess);
          MiddlePanel.MiddlePanel.calc();
     }
}
