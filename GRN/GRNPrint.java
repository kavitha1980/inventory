package GRN;

import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GRNPrint
{
     FileWriter     FW;
     String         SGrnNo,SBlock,SBlockCode,SGrnDate,SSupCode,SSupName,SNet,SPayTerm,SOrdBlock;
     
     String         SGINo     = "",SGIDate   = "";
     String         SInvNo    = "",SInvDate  = "";
     String         SDCNo     = "",SDCDate   = "";
     
     Vector         VGCode,VGName,VGUoM,VGOrdNo,VGMRSNo,VGDCQty,VGRecQty,VGRejQty,VGAcQty,VGPendQty,VGUnit,VGAmend;
     
     Common         common    = new Common();
     int            Lctr      = 100;
     int            Pctr      = 0;
     int            iAmend    = 0;

     GRNPrint(FileWriter FW,String SGrnNo,String SBlockCode,String SGrnDate,String SSupCode,String SSupName,String SNet,String SPayTerm)
     {
          this . FW           = FW;
          this . SGrnNo       = SGrnNo;
          this . SBlock       = "";
          this . SBlockCode   = SBlockCode;
          this . SGrnDate     = SGrnDate;
          this . SSupCode     = SSupCode;
          this . SSupName     = SSupName;
          this . SPayTerm     = SPayTerm;
          this . SNet         = SNet;
          this . SOrdBlock    = "";
          
          setDataIntoVector();
          setGrnHead();
          setGrnBody();
          //setGrnFoot(0);
     }

     public void setGrnHead()
     {
          if(Lctr < 43)
               return;

          if(Pctr > 0)
               setGrnFoot(1);

          Pctr++;

          String Strl1 = "Amarjothi Spinning Mills Ltd., Nambiyur";
          String Strl1a= " ";
          String Strl1b= "EGoods Receipt NoteF";
          String Strl4 = "";
          String Strl5 = "                                           ";
          String Strl6 = "G.R.N No       : "+SGrnNo+"/"+SGrnDate; 
          String Strl7 = "";
          String Strl8 = "Supplier Name  : "+SSupName;
          String Strl9 = "";
          String Str20 = "";
          String Str21= "Payment Term   : "+SPayTerm;
          String Str22 = "";
          String Strl10 = "";
          String Strl11 = "|--------------------------------------------------------------------------------------------------------|";
          String Strl12 = "|          GATE INWARD          |            INVOICE            |                                        |";
          String Strl13 = "|---------------------------------------------------------------|            GRN VALUE                   |";
          String Strl14 = "| No            | Date          | No            | Date          |                                        |";
          String Strl15 = "|--------------------------------------------------------------------------------------------------------| ";
          String Strl16 = "| "+common.Pad(SGINo,14)+"| "+common.Pad(SGIDate,14)+"| "+common.Pad(SInvNo,14)+"| "+common.Pad(SInvDate,14)+"| "+common.Cad(common.getRound(SNet,2),14+2+21)+"  |";
          String Strl17 = "|--------------------------------------------------------------------------------------------------------|";
          String Strl18 = "|--------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl19 = "|Material |Material Description        |Order No/Date  |MRS No|  Unit   |                 Q U A N T I T Y                      |       |";
          String Strl20 = "|Code     |                            |               |      |         |------------------------------------------------------|  UOM  |";
          String Strl21 = "|         |                            |               |      |         |    DC Qty|  Received|  Rejected|  Accepted|   Ordered|       |";
          String Strl22 = "|--------------------------------------------------------------------------------------------------------------------------------------|";
          
          try
          {
               FW.write(Strl1+"\n");
               FW.write(Strl1a+"\n");
               FW.write(Strl1b+"\n");
               FW.write(Strl4+"\n");
               FW.write(Strl5+"\n");
               FW.write(Strl6+"\n");
               FW.write(Strl7+"\n");
               FW.write(Strl8+"\n");
               FW.write(Strl9+"\n");
               FW.write(Str21+"\n");
               FW.write(Str22+"\n");
               
               FW.write(Strl10+"\n");
               FW.write(Strl11+"\n");
               FW.write(Strl12+"\n");
               FW.write(Strl13+"\n");
               FW.write(Strl14+"\n");
               FW.write(Strl15+"\n");
               FW.write(Strl16+"\n");
               FW.write(Strl17+"\n");
               FW.write(Strl18+"\n");
               FW.write(Strl19+"\n");
               FW.write(Strl20+"\n");
               FW.write(Strl21+"\n");
               FW.write(Strl22+"\n");
               Lctr = 24;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setGrnBody()
     {
          double    dDCQty=0,dRecQty=0,dRejQty=0,dAcQty=0,dPendQty=0;
          String    SCode="",SPCode="";
          int       ctr=0;
          for(int i=0;i<VGCode.size();i++)
          {
               SCode     = (String)VGCode.elementAt(i);
               if(!SCode.equals(SPCode))
               {
                    if(i>0)
                         setDCFoot(0,ctr,dDCQty,dRecQty,dRejQty,dAcQty,dPendQty);                    
                    SPCode    = SCode;
                    ctr       = 0;
                    dDCQty    = 0;dRecQty=0;dRejQty=0;dAcQty=0;dPendQty=0;
               }
               setGrnHead();
               String SName   = common.Pad((String)VGName.elementAt(i),28);
               String SGUoM   = common.Pad((String)VGUoM.elementAt(i),7);
               String SGOrdNo = common.Pad((String)VGOrdNo.elementAt(i),15);
               String SGAmend = (String)VGAmend.elementAt(i);
               String SAmend  = "";
               if(SGAmend.equals("1"))
               {
                    SAmend = common.Pad("Amended",15);
               }

               String    SGMRSNo   = common.Pad((String)VGMRSNo.elementAt(i),6);
               String    SUnit     = common.Pad((String)VGUnit.elementAt(i),9);
               
               String    SGDCQty   = common.getRound((String)VGDCQty.elementAt(i),2);
               String    SGRecQty  = common.getRound((String)VGRecQty.elementAt(i),2);
               String    SGRejQty  = common.getRound((String)VGRejQty.elementAt(i),2);
               String    SGAcQty   = common.getRound((String)VGAcQty.elementAt(i),2);
               String    SGPendQty = common.getRound((String)VGPendQty.elementAt(i),2);
               
                         dDCQty    = dDCQty+common.toDouble(SGDCQty);
                         dRecQty   = dRecQty+common.toDouble(SGRecQty);
                         dRejQty   = dRejQty+common.toDouble(SGRejQty);
                         dAcQty    = dAcQty+common.toDouble(SGAcQty);
                         dPendQty  = dPendQty+common.toDouble(SGPendQty);
                    
                         SGDCQty   = common.Rad(SGDCQty,10);
                         SGRecQty  = common.Rad(SGRecQty,10);
                         SGRejQty  = common.Rad(SGRejQty,10);
                         SGAcQty   = common.Rad(SGAcQty,10);
                         SGPendQty = common.Rad(SGPendQty,10);
                    
               try
               {
                    String Strl =  "|"+(ctr==0?common.Pad(SCode,9):common.Space(9))+"|"+(ctr==0?SName:common.Space(28))+"|"+
                                   SGOrdNo+"|"+SGMRSNo+"|"+SUnit+"|"+SGDCQty+"|"+
                                   SGRecQty+"|"+SGRejQty+"|"+SGAcQty+"|"+SGPendQty+"|"+SGUoM+"|";


                    FW.write(Strl+"\n");
                    Lctr = Lctr++;
                    if(SGAmend.equals("1"))
                    {
                         String Strl1 = "|"+common.Space(9)+"|"+common.Space(28)+"|"+
                                        SAmend+"|"+common.Space(6)+"|"+common.Space(9)+"|"+common.Space(10)+"|"+
                                        common.Space(10)+"|"+common.Space(10)+"|"+common.Space(10)+"|"+common.Space(10)+"|"+common.Space(7)+"|";

                         FW.write(Strl1+"\n");
                         Lctr = Lctr++;
                    }

                    ctr++;
               }
               catch(Exception ex){}
          }
          setDCFoot(1,ctr,dDCQty,dRecQty,dRejQty,dAcQty,dPendQty);                    
     }

     public void setDCFoot(int iEnd,int ctr,double dDCQty,double dRecQty,double dRejQty,double dAcQty,double dPendQty)                    
     {
          if(ctr==1)
          {
               try
               {
                    if(iEnd==0)
                         FW.write("|--------------------------------------------------------------------------------------------------------------------------------------|\n");
                    else
                         FW.write("|--------------------------------------------------------------------------------------------------------------------------------------|\n");
                    Lctr++;
               }
               catch(Exception ex){}
               return;
          }

          String SDGDCQty    = common.Rad(common.getRound(dDCQty,2),10);
          String SDGRecQty   = common.Rad(common.getRound(dRecQty,2),10);
          String SDGRejQty   = common.Rad(common.getRound(dRejQty,2),10);
          String SDGAcQty    = common.Rad(common.getRound(dAcQty,2),10);
          String SDGPendQty  = common.Rad(common.getRound(dPendQty,2),10);

          try
          {
               FW.write("|--------------------------------------------------------------------------------------------------------------------------------------|\n");
               String    Strl =    "|"+common.Space(9)+"|"+common.Space(28)+"|"+
                                   common.Space(15)+"|"+common.Space(6)+"|"+common.Space(9)+"|"+SDGDCQty+"|"+
                                   SDGRecQty+"|"+SDGRejQty+"|"+SDGAcQty+"|"+SDGPendQty+"|"+common.Space(7)+"|";

               FW.write(Strl+"\n");
               if(iEnd==0)
                    FW.write("|--------------------------------------------------------------------------------------------------------------------------------------|\n");
               else
                    FW.write("|--------------------------------------------------------------------------------------------------------------------------------------|\n");

               Lctr=Lctr+3;
          }
          catch(Exception ex){}
     }

     public void setGrnFoot(int iSig)
     {
          String Strl1 = "|--------------------------------------------------------------------------------------------------------|";
          String Strl2 = "| Reason for Rejection                                          | Remarks:                               |";
          String Strl3 = "|                                                               |                                        |";
          String Strl4 = "|                                                               |                                        |";
          String Strl5 = "|--------------------------------------------------------------------------------------------------------|";
          String Strl6 = "|Prepared    |Binned      |Inspected   |Store-Keeper|Received   |Issued       |Passed                    |";
          String Strl7 = "|            |            |            |            |           |             |                          |";
          String Strl8 = "|            |            |            |            |           |             |                          |";
          String Strl9 = "---------------------------------------------------------------------------------------------------------|";
          
          try
          {
               FW.write(Strl1+"\n");
               FW.write(Strl2+"\n");
               FW.write(Strl3+"\n");
               FW.write(Strl4+"\n");
               FW.write(Strl5+"\n");
               FW.write(Strl6+"\n");
               FW.write(Strl7+"\n");
               FW.write(Strl8+"\n");
               FW.write(Strl9+"\n");
               if(iSig==0)
                    FW.write("< End Of Report >\n");
               else
                    FW.write("(Continued on Next Page) \n");
          }
          catch(Exception ex){}
     }

     public void setDataIntoVector()
     {
          VGCode      = new Vector();
          VGName      = new Vector();
          VGUoM       = new Vector();
          VGOrdNo     = new Vector();
          VGMRSNo     = new Vector();
          VGDCQty     = new Vector();
          VGRecQty    = new Vector();
          VGRejQty    = new Vector();
          VGAcQty     = new Vector();
          VGPendQty   = new Vector();
          VGUnit      = new Vector();
          VGAmend     = new Vector();

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();
               Statement      stat           =  theConnection.createStatement();
               String         QString        = getQString();
               ResultSet      res            = stat.executeQuery(QString);
               while (res.next())
               {
                    VGCode    . addElement(res.getString(1));
                    VGName    . addElement(res.getString(2));
                    VGOrdNo   . addElement(res.getString(3));
                    VGMRSNo   . addElement(res.getString(5));
                    VGDCQty   . addElement(res.getString(6));
                    VGRecQty  . addElement(res.getString(7));
                    VGRejQty  . addElement(res.getString(8));
                    VGAcQty   . addElement(res.getString(9));
                    VGPendQty . addElement(res.getString(10));
                    SGINo     = res.getString(11);
                    SGIDate   = common.parseDate(res.getString(12));
                    SInvNo    = common.parseDate(res.getString(13));
                    SInvDate  = common.parseDate(res.getString(14));
                    SDCNo     = common.parseDate(res.getString(15));
                    SDCDate   = common.parseDate(res.getString(16));
                    VGUoM     . addElement(res.getString(17));
                    VGUnit    . addElement(res.getString(18));
                    VGAmend   . addElement("0");
               }

               for(int i=0;i<VGOrdNo.size();i++)
               {
                    ResultSet result = stat.executeQuery("Select OrderDate,Amended From PurchaseOrder Where OrderNo = "+(String)VGOrdNo.elementAt(i));
                    String SDate = "";
                    String SPay  = "";
                    while(result.next())
                    {
                         SDate     = result.getString(1);
                         int j     = result.getInt(2);
                         if(j==1)
                         {
                              iAmend    = j;
                              VGAmend   . setElementAt(String.valueOf(j),i);
                         }
                    }
                    SDate     = (String)VGOrdNo.elementAt(i)+"/"+common.parseDate(SDate);
                    VGOrdNo   . setElementAt(SDate,i);
               }
          }
          catch(Exception ex){System.out.println(ex);}
     }
     
     public String getQString()
     {
          String QString  = "";
          
          QString = " Select GRN.Code, InvItems.Item_Name, Grn.OrderNo, ' ' as OrderDate, Grn.MRSNo, Grn.InvQty, Grn.MillQty, Grn.RejQty, Grn.Qty,Grn.Pending, "+
                    " Grn.GateInNo, Grn.GateInDate, Grn.InvNo,Grn.InvDate, Grn.DCNo, Grn.DCDate,Uom.UoMName,Unit.Unit_Name "+
                    " From (((GRN "+
                    " Inner Join InvItems On Grn.Code = InvItems.Item_Code) "+
                    " Inner Join Unit On Grn.Unit_code = Unit.Unit_Code) "+
                    " Inner Join Uom On Uom.UomCode = InvItems.UomCode)"+
                    " Where GRN.GrnNo = "+SGrnNo+
                    " Order By 1";
          
          return QString;
     }
}
