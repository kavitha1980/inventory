package GRN;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class GRNRecords
{

        String SGDate = "",SSupName = "",SGIDate = "";
        String SInvNo = "",SInvDate = "",SDCNo = "",SDCDate = "";
        String SAdd = "0",SLess = "0",SInvoice="0";
        Vector VGBlock,VGMRS,VGOrdNo,VGOrdQty,VGPending,VGItemCode,VGItemName,VGInvQty,VGRecdQty,VGAccQty;
        Vector VGInvRate,VGDiscPer,VGCenVatPer,VGTaxPer,VGSurPer;
        Vector VGDeptName,VGGroupName,VGUnitName;
        Vector VId,VPId,VTaxC,VPGrnValue,VBlockCode;

        Common common = new Common();
        String SGrnNo,SGateNo;
        int iMillCode;
        String SSupTable;

    GRNRecords(String SGrnNo,String SGateNo,int iMillCode,String SSupTable)
    {
        this.SGrnNo    = SGrnNo;
        this.SGateNo   = SGateNo;
        this.iMillCode = iMillCode;
        this.SSupTable = SSupTable;

        VGBlock     = new Vector();
        VGMRS       = new Vector();
        VGOrdNo     = new Vector();
        VGOrdQty    = new Vector();
        VGPending   = new Vector();
        VGItemCode  = new Vector();
        VGItemName  = new Vector();
        VGInvQty    = new Vector();
        VGRecdQty   = new Vector();
        VGAccQty    = new Vector();
        VGInvRate   = new Vector();
        VGDiscPer   = new Vector();
        VGCenVatPer = new Vector();
        VGTaxPer    = new Vector();
        VGSurPer    = new Vector();
        VGDeptName  = new Vector();
        VGGroupName = new Vector();
        VGUnitName  = new Vector();
        VId         = new Vector();
        VPId        = new Vector();
        VTaxC       = new Vector();
        VPGrnValue  = new Vector();
        VBlockCode  = new Vector();


        String QS = "";

        QS = " SELECT GRN.GRNDate, GRN.GateInDate, GRN.InvNo, "+
             " GRN.InvDate, GRN.DcNo, GRN.DcDate, GRN.OrderNo, "+
             " OrdBlock.BlockName, "+SSupTable+".Name, GRN.Code, "+
             " InvItems.Item_Name, GRN.InvQty, GRN.InvRate, "+
             " GRN.DiscPer, GRN.CenvatPer, GRN.TaxPer, "+
             " GRN.SurPer, Dept.Dept_Name, Cata.Group_Name, "+
             " Unit.Unit_Name, GRN.Id,GRN.plus,GRN.Less, "+
             " GRN.MRSNo,GRN.OrderQty,GRN.Pending,GRN.MillQty, "+
             " PurchaseOrder.Id,GRN.Qty,GRN.NoOfBills,Grn.TaxClaimable,Grn.GrnValue,Grn.GrnBlock FROM "+
             " ((((((GRN INNER JOIN "+SSupTable+" ON GRN.Sup_Code="+SSupTable+".Ac_Code) "+
             " INNER JOIN OrdBlock ON GRN.GrnBlock=OrdBlock.Block) "+
             " INNER JOIN Dept ON GRN.Dept_Code=Dept.Dept_code) "+
             " INNER JOIN Cata ON GRN.Group_Code=Cata.Group_Code) "+
             " INNER JOIN Unit ON GRN.Unit_Code=Unit.Unit_Code) "+
             " INNER JOIN InvItems ON GRN.Code=InvItems.Item_Code) "+
             " LEFT JOIN PurchaseOrder ON ((GRN.OrderNo=PurchaseOrder.OrderNo) "+
             " AND (GRN.Code=PurchaseOrder.Item_Code) AND "+
             " (GRN.OrderSlNo=PurchaseOrder.SlNo)) "+
             " Where GRN.GrnNo = "+SGrnNo+
             " And Grn.MillCode = "+iMillCode;

        try
        {
            ORAConnection   oraConnection =  ORAConnection.getORAConnection();
            Connection      theConnection =  oraConnection.getConnection();
            Statement       stat =  theConnection.createStatement();
            ResultSet res  = stat.executeQuery(QS);
            while (res.next())
            {
                SGDate   = common.parseDate(res.getString(1));
                SGIDate  = common.parseDate(res.getString(2));
                SInvNo   = res.getString(3);
                SInvDate = common.parseDate(res.getString(4));
                SDCNo    = res.getString(5);
                SDCDate  = common.parseDate(res.getString(6));

                VGOrdNo     .addElement(res.getString(7));
                VGBlock     .addElement(res.getString(8));
                SSupName = res.getString(9);

                VGItemCode  .addElement(res.getString(10));
                VGItemName  .addElement(res.getString(11));
                VGInvQty    .addElement(res.getString(12));
                VGInvRate   .addElement(res.getString(13));
                VGDiscPer   .addElement(res.getString(14));
                VGCenVatPer .addElement(res.getString(15));
                VGTaxPer    .addElement(res.getString(16));
                VGSurPer    .addElement(res.getString(17));
                VGDeptName  .addElement(res.getString(18));
                VGGroupName .addElement(res.getString(19));
                VGUnitName  .addElement(res.getString(20));
                VId         .addElement(res.getString(21));
                SAdd  = res.getString(22);
                SLess = res.getString(23);
                VGMRS       .addElement(res.getString(24));
                VGOrdQty    .addElement(res.getString(25));
                VGPending   .addElement(res.getString(26));
                VGRecdQty   .addElement(res.getString(27));
                VPId        .addElement(res.getString(28));
                VGAccQty    .addElement(res.getString(29));
                SInvoice = res.getString(30);
                VTaxC       .addElement(res.getString(31));
                VPGrnValue  .addElement(res.getString(32));
                VBlockCode  .addElement(res.getString(33));
            }
            
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
    }

}

