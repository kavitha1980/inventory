package GRN;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GRNTableModel extends DefaultTableModel
{
        Object    RowData[][],ColumnNames[],ColumnType[];
        JLabel    LBasic,LDiscount,LCenVat,LTax,LSur,LNet;
        NextField TAdd,TLess;
        Common common = new Common();
        public GRNTableModel(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType,JLabel LBasic,JLabel LDiscount,JLabel LCenVat,JLabel LTax,JLabel LSur,NextField TAdd,NextField TLess,JLabel LNet)
        {
            super(RowData,ColumnNames);
            this.RowData     = RowData;
            this.ColumnNames = ColumnNames;
            this.ColumnType  = ColumnType;
            this.LBasic      = LBasic;
            this.LDiscount   = LDiscount;
            this.LCenVat     = LCenVat; 
            this.LTax        = LTax;
            this.LSur        = LSur;
            this.TAdd        = TAdd;
            this.TLess       = TLess;
            this.LNet        = LNet;

            for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
                setMaterialAmount(curVector);
            }
        }
       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }            
       public boolean isCellEditable(int row,int col)
       {
               if(ColumnType[col]=="B" || ColumnType[col]=="E")
                  return true;
               return false;
       }
       public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
                   if(column>=6 && column<=12)
                      setMaterialAmount(rowVector);
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }
      public void setMaterialAmount(Vector RowVector)
      {
              double dTGross=0,dTDisc=0,dTCenVat=0,dTTax=0,dTSur=0,dTNet=0;

              double dQty        = common.toDouble((String)RowVector.elementAt(7));
              double dRate       = common.toDouble((String)RowVector.elementAt(8));
              double dDiscPer    = common.toDouble((String)RowVector.elementAt(9));
              double dCenVatPer  = common.toDouble((String)RowVector.elementAt(10));
              double dTaxPer     = common.toDouble((String)RowVector.elementAt(11));
              double dSurPer     = common.toDouble((String)RowVector.elementAt(12));

              double dGross  = dQty*dRate;
              double dDisc   = dGross*dDiscPer/100;
              double dCenVat = (dGross-dDisc)*dCenVatPer/100;
              double dTax    = (dGross-dDisc+dCenVat)*dTaxPer/100;
              double dSur    = (dTax)*dSurPer/100;
              double dNet    = dGross-dDisc+dCenVat+dTax+dSur;

              RowVector.setElementAt(common.getRound(dGross,2),13);
              RowVector.setElementAt(common.getRound(dDisc,2),14);
              RowVector.setElementAt(common.getRound(dCenVat,2),15);
              RowVector.setElementAt(common.getRound(dTax,2),16);
              RowVector.setElementAt(common.getRound(dSur,2),17);
              RowVector.setElementAt(common.getRound(dNet,2),18);
              for(int i=0;i<super.dataVector.size();i++)
              {
                  Vector curVector = (Vector)super.dataVector.elementAt(i);
                  dTGross   = dTGross+common.toDouble((String)curVector.elementAt(13));
                  dTDisc    = dTDisc+common.toDouble((String)curVector.elementAt(14));
                  dTCenVat  = dTCenVat+common.toDouble((String)curVector.elementAt(15));
                  dTTax     = dTTax+common.toDouble((String)curVector.elementAt(16));
                  dTSur     = dTSur+common.toDouble((String)curVector.elementAt(17));
                  dTNet     = dTNet+common.toDouble((String)curVector.elementAt(18));
              }
              dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
              LBasic.setText(common.getRound(dTGross,2));
              LDiscount.setText(common.getRound(dTDisc,2));
              LCenVat.setText(common.getRound(dTCenVat,2));
              LTax.setText(common.getRound(dTTax,2));
              LSur.setText(common.getRound(dTSur,2));
              LNet.setText(common.getRound(dTNet,2));
    }
    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
                FinalData[i][j] = (String)curVector.elementAt(j);
        }
        return FinalData;
    }
    public int getRows()
    {
         return super.dataVector.size();
    }
    public Vector getCurVector(int i)
    {
         return (Vector)super.dataVector.elementAt(i);
    }

}
