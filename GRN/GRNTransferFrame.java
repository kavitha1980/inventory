package GRN;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GRNTransferFrame extends JInternalFrame
{
     JTextField               TGINo,TGIDate,TInvDate,TDCDate;
     DateField                TDate;
     JTextField               TInvNo,TDCNo,TSupName;
     WholeNumberField         TInvoice;
     GRNTransferMiddlePanel   MiddlePanel;
     JPanel                   TopPanel,BottomPanel;
     JButton                  BOk,BCancel;

     Connection     theConnection = null;
     
     JLayeredPane             DeskTop;
     Vector                   VCode,VName;
     StatusPanel              SPanel;
     String                   SIndex;
     int                      iIndex=0;
     JTable                   ReportTable;
     String                   SSupCode;
     int                      iMillCode,iUserCode;
     String                   SItemTable,SSupTable,SYearCode;

     JLabel                   LGrnNo;
     
     Common                   common   = new Common();

     boolean        bComflag  = true;

     GRNTransferFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,String SIndex,JTable ReportTable,String SSupCode,int iMillCode,int iUserCode,String SItemTable,String SSupTable,String SYearCode)
     {
          this.DeskTop            = DeskTop;
          this.VCode              = VCode;
          this.VName              = VName;
          this.SPanel             = SPanel;
          this.SIndex             = SIndex;
          this.ReportTable        = ReportTable;
          this.SSupCode           = SSupCode;
          this.iMillCode          = iMillCode;
          this.iUserCode          = iUserCode;
          this.SItemTable         = SItemTable;
          this.SSupTable          = SSupTable;
          this.SYearCode          = SYearCode;
          

          ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                          theConnection =  oraConnection.getConnection();

          iIndex = common.toInt(SIndex);
          
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }

     public void createComponents()
     {
          BOk        = new JButton("Okay");
          BCancel    = new JButton("Abort");
          
          TDate       = new DateField();
          LGrnNo      = new JLabel();
          TGINo       = new JTextField();
          TGIDate     = new JTextField();
          TInvDate    = new JTextField();
          TDCDate     = new JTextField();
          TDCDate     = new JTextField();
          TInvNo      = new JTextField();
          TDCNo       = new JTextField();
          TSupName    = new JTextField();
          TInvoice    = new WholeNumberField(2);
          
          MiddlePanel = new GRNTransferMiddlePanel(DeskTop,SSupCode,iMillCode,LGrnNo,SYearCode);
          
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();
          
          TDate.setTodayDate();
          TInvoice.setText("1");
          
          TGINo.setEditable(false);
          TGIDate.setEditable(false);
          TSupName.setEditable(false);
     }

     public void setLayouts()
     {
          setTitle("Invoice Valuation of Materials recd without orders");
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          
          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(5,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("Gate Inward No"));
          TopPanel.add(TGINo);
          
          TopPanel.add(new JLabel("Gate Inward Date"));
          TopPanel.add(TGIDate);
          
          TopPanel.add(new JLabel("GRN No"));
          TopPanel.add(LGrnNo);
          
          TopPanel.add(new JLabel("GRN Date"));
          TopPanel.add(TDate);
          
          TopPanel.add(new JLabel("Supplier"));
          TopPanel.add(TSupName);
          
          TopPanel.add(new JLabel("No. of Bills"));
          TopPanel.add(TInvoice);
          
          TopPanel.add(new JLabel("Invoice No"));
          TopPanel.add(TInvNo);
          
          TopPanel.add(new JLabel("Invoice Date"));
          TopPanel.add(TInvDate);
          
          TopPanel.add(new JLabel("DC No"));
          TopPanel.add(TDCNo);
          
          TopPanel.add(new JLabel("DC Date"));
          TopPanel.add(TDCDate);
          
          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);
          
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
          
          TGINo          . setText((String)ReportTable.getModel().getValueAt(iIndex,0));
          TGIDate        . setText((String)ReportTable.getModel().getValueAt(iIndex,1));
          TSupName       . setText((String)ReportTable.getModel().getValueAt(iIndex,2));
          TInvNo         . setText(common.parseNull((String)ReportTable.getModel().getValueAt(iIndex,3)));
          TInvDate       . setText((String)ReportTable.getModel().getValueAt(iIndex,4));
          TDCNo          . setText(common.parseNull((String)ReportTable.getModel().getValueAt(iIndex,5)));
          TDCDate        . setText((String)ReportTable.getModel().getValueAt(iIndex,6));
          MiddlePanel    . createComponents(TGINo.getText());
     }

     public void addListeners()
     {
          BOk.addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validateGRN())
                    insertGRNCollectedData();
          }
     }

     private void insertGRNCollectedData()
     {
          BOk.setEnabled(false);
          insertGRNDetails();
          setGILink();
          setOrderLink();
          getACommit();
          removeHelpFrame();
     }

     private String  getInsertId()
     {
          String SId ="";
          String QS ="";
          try
          {
               QS = " Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 2 for update of MaxNo noWait";

               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);

               Statement       stat =  theConnection.createStatement();
               
               PreparedStatement thePrepare = theConnection.prepareStatement(" Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 2"); 
               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    SId = res.getString(1);    
               }
               res.close();

               thePrepare.setInt(1,common.toInt(SId));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println("E1"+ex );
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getInsertId();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
          LGrnNo.setText(SId);
          return SId;
     }


     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public void setGILink()
     {
          try
          {
               Statement       stat =  theConnection.createStatement();
               
               for(int i=0;i<MiddlePanel.RD.length;i++)
               {
                    Boolean BValue = (Boolean)MiddlePanel.RD[i][3];
                    if(BValue.booleanValue())
                    {
                         String    QS = "Update GateInward Set ";
                                   QS = QS+" GrnNo=1";
                                   QS = QS+" Where Id = "+(String)MiddlePanel.VGId.elementAt(i);

                         if(theConnection   . getAutoCommit())
                              theConnection   . setAutoCommit(false);

                         stat.execute(QS);
                    }
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag  = false;
               System.out.println(ex);
          }
     }

     public void insertGRNDetails()
     {
          String QString = "Insert Into GRN (OrderNo,MRSNo,GrnNo,GrnDate,GrnBlock,Sup_Code,GateInNo,GateInDate,InvNo,InvDate,DcNo,DcDate,Code,InvQty,MillQty,Pending,OrderQty,Qty,InvRate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,InvAmount,InvNet,Plus,Less,Misc,Dept_Code,Group_Code,Unit_Code,InvSlNo,ActualModVat,NoOfBills,SPJNo,NoofSPJ,SPJApprovalStatus,MillCode,id,slno,usercode,creationdate,spjdate,mrsslno,orderslno,taxclaimable,grnqty,grnvalue,MrsAuthUserCode) Values (";
          String QSL     = "Select Max(InvSlNo) From GRN";
          String QS="";
          int iInvSlNo=0,iSlNo=0;
          try
          {
               Statement       stat =  theConnection.createStatement();
               
               ResultSet res        = stat.executeQuery(QSL);
               while(res.next())
               {
                    iInvSlNo = res.getInt(1);
               }
               res.close();
               iInvSlNo++;
               
               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();                 
               String SAdd       = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess      = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm        = common.toDouble(SAdd)-common.toDouble(SLess);
               double dBasic     = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio     = dpm/dBasic;
               
               String SGrnNo      = getInsertId();
               
               for(int i=0;i<RowData.length;i++)
               {
                    double dDcQty     = common.toDouble((String)RowData[i][6]);
                    if(dDcQty == 0)
                         continue;
                    
                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);

                    String SMrsUserCode = MiddlePanel.MiddlePanel.getUserCode(i);
               
                    if(SUnitCode.equals("0"))
                         SUnitCode = getUnitCode(stat);

                    if(SDeptCode.equals("0"))
                         SDeptCode="9";
                    
                    if(SGroupCode.equals("0"))
                         SGroupCode="15";

                    
                    String SBlockCode = MiddlePanel.getBlockCode(i);
                    //String SOrderNo   = MiddlePanel.getOrderNo(i);
                    String SOrderNo   = "0";
                    String SMRSNo     = MiddlePanel.getMRSNo(i);
                    String SOrdQty    = MiddlePanel.getOrdQty(i);
                    
                    iSlNo++;
                         
                    String SMrsSlNo   = "0";
                    String SOrderSlNo = "0";
                    String STaxC      = "0";
                    
                    String  SItemCode  = (String)RowData[i][0];
                    
                    String  SBasic     = (String)RowData[i][13];
                    String  SInvAmount = (String)RowData[i][18];
                    String  SVat       = (String)RowData[i][16];
                    
                    String SMisc       = common.getRound(common.toDouble(SBasic)*dRatio,3);
                    
                    String  SMillQty   = (String)RowData[i][7];
                    String SGrnQty     = SMillQty;
                    
                    String SGrnValue = "";
                    
                    /*if(STaxC.equals("0"))
                         SGrnValue = common.getRound(common.toDouble(SInvAmount) + common.toDouble(SMisc),2);
                    else
                         SGrnValue = common.getRound(common.toDouble(SInvAmount) + common.toDouble(SMisc) - common.toDouble(SVat),2);*/
                    
                    
                    SGrnValue = common.getRound(common.toDouble(SInvAmount) + common.toDouble(SMisc),2);

                    String    QS1 = QString;
                              QS1 = QS1+"0"+SOrderNo+",";
                              QS1 = QS1+"0"+SMRSNo+",";
                              QS1 = QS1+"0"+SGrnNo+",";
                              QS1 = QS1+"'"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                              QS1 = QS1+"0"+SBlockCode+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"0"+TGINo.getText()+",";
                              QS1 = QS1+"'"+common.pureDate(TGIDate.getText())+"',";
                              QS1 = QS1+"'"+TInvNo.getText()+"',";
                              QS1 = QS1+"'"+common.pureDate(TInvDate.getText())+"',";
                              QS1 = QS1+"'"+TDCNo.getText()+"',";
                              QS1 = QS1+"'"+common.pureDate(TDCDate.getText())+"',";
                              QS1 = QS1+"'"+SItemCode+"',";
                              QS1 = QS1+"0"+(String)RowData[i][6]+",";
                              QS1 = QS1+"0"+SMillQty+",";
                              QS1 = QS1+"0"+(String)RowData[i][5]+",";
                              QS1 = QS1+"0"+SOrdQty+",";
                              QS1 = QS1+"0"+(String)RowData[i][6]+",";
                              QS1 = QS1+"0"+(String)RowData[i][8]+",";
                              QS1 = QS1+"0"+(String)RowData[i][9]+",";
                              QS1 = QS1+"0"+(String)RowData[i][14]+",";
                              QS1 = QS1+"0"+(String)RowData[i][10]+",";
                              QS1 = QS1+"0"+(String)RowData[i][15]+",";
                              QS1 = QS1+"0"+(String)RowData[i][11]+",";
                              QS1 = QS1+"0"+(String)RowData[i][16]+",";
                              QS1 = QS1+"0"+(String)RowData[i][12]+",";
                              QS1 = QS1+"0"+(String)RowData[i][17]+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SAdd+",";
                              QS1 = QS1+"0"+SLess+",";
                              QS1 = QS1+"0"+SMisc+",";
                              QS1 = QS1+"0"+SDeptCode+",";
                              QS1 = QS1+"0"+SGroupCode+",";
                              QS1 = QS1+"0"+SUnitCode+",";
                              QS1 = QS1+"0"+iInvSlNo+",";
                              QS1 = QS1+"0"+0+",";
                              QS1 = QS1+"0"+TInvoice.getText()+",";
                              QS1 = QS1+"99999"+",";
                              QS1 = QS1+"1"+",";
                              QS1 = QS1+"1"+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"grn_seq.nextval"+",";
                              QS1 = QS1+"0"+iSlNo+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+common.getServerDateTime()+"',";
                              QS1 = QS1+"0"+common.getServerPureDate()+",";
                              QS1 = QS1+"0"+SMrsSlNo+",";
                              QS1 = QS1+"0"+SOrderSlNo+",";
                              QS1 = QS1+"0"+STaxC+",";
                              QS1 = QS1+"0"+SGrnQty+",";
                              QS1 = QS1+"0"+SGrnValue+",";
                              QS1 = QS1+"0"+SMrsUserCode+")";
               
                    if(theConnection   . getAutoCommit())
                         theConnection   . setAutoCommit(false);

                    stat.executeUpdate(QS1);
                    
                    updateItemMaster(stat,SItemCode,SBlockCode,SGrnQty,SGrnValue);

                    if(common.toInt(SBlockCode)<=1)
                    {
                         updateUserItemStock(stat,SItemCode,SGrnQty,SGrnValue,SMrsUserCode);
                    }
               }

               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("@QS1 : "+ex);
               bComflag  = false;
          }
     }

     public void setOrderLink()
     {
          try
          {
               Statement       stat =  theConnection.createStatement();
               
               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
			   
               for(int i=0;i<RowData.length;i++)
               {
                    double dDcQty     = common.toDouble((String)RowData[i][6]);
					
                    if(dDcQty == 0)
                         continue;
                    
                    String    QS = "Update StockTransfer Set ";
                              QS = QS+" InvQty=InvQty+"+(String)RowData[i][7];
                              QS = QS+" Where Id = "+(String)MiddlePanel.VId.elementAt(i);

                    if(theConnection   . getAutoCommit())
                         theConnection   . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag  = false;
               System.out.println(ex);
          }
     }

     private boolean validateGRN()
     {
          BOk.setEnabled(false);
          boolean bflag=false;
          String str="";
          
          int iInvoice = common.toInt(TInvoice.getText());
          if(iInvoice==0)
          {
               str="No of Bills Field is Empty";
               bflag=true;
          }
          if(isQtyDiff())
          {
               str="Quantity Mismatch";
               bflag=true;
          }
          if(isQtyNotTouched())
          {
               str=str+"\n"+"Invalid GRN - All Zero";
               bflag=true;
          }
          if(isUserNotSelected())
          {
               str=str+"\n"+"User Name Not Selected";
               bflag=true;
          }
          if(isGateNotTicked())
          {
               str = str+"\n"+"Some Gate Entries are Not Closed";
               bflag=true;
          }
          if(!bflag)
               return true;
          
          AcceptDeny AD = new AcceptDeny(str);
          AD.show();
          AD.BAccept.addActionListener(new ADList(AD));
          AD.BDeny.addActionListener(new ADList(AD));

          return false;                              
     }

     private boolean isQtyDiff()
     {
          Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<RowData.length;i++)
          {
               double dDcQty   = common.toDouble((String)RowData[i][6]);
               double dInvQty  = common.toDouble((String)RowData[i][7]);
               if(dDcQty != dInvQty)
                    return true;
          }
          return false;
     }

     private boolean isQtyNotTouched()
     {
          double dTDCQty=0;
          Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<RowData.length;i++)
          {
               dTDCQty   = dTDCQty+common.toDouble((String)RowData[i][6]);
          }
          if(dTDCQty == 0)
               return true;
          return false;
     }

     private boolean isUserNotSelected()
     {
          Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<RowData.length;i++)
          {
               double dDcQty = common.toDouble((String)RowData[i][6]);

               if(dDcQty>0)
               {
                    String SUser  = ((String)RowData[i][22]).trim();
     
                    if(SUser.length() == 0)
                         return true;
               }
          }
          return false;
     }

     private boolean isGateNotTicked()
     {
          for(int i=0;i<MiddlePanel.RD.length;i++)
          {
               Boolean BValue = (Boolean)MiddlePanel.RD[i][3];
               if(!BValue.booleanValue())
                    return true;
          }
          return false;
     }
     
     private class ADList implements ActionListener
     {
          AcceptDeny AD;
          ADList(AcceptDeny AD)
          {
               this.AD = AD;
          }
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(true);
               AD.setVisible(false);
          }
     }

     private void updateItemMaster(Statement stat,String SItemCode,String SBlockCode,String SGrnQty,String SGrnValue)
     {
          try
          {
               int iBlockCode = common.toInt(SBlockCode);
               
               String QS = "";
               
               if(iBlockCode>1)
               {
                    QS = "Update "+SItemTable+" Set RecVal =RecVal+"+SGrnValue+",";
                    QS = QS+" RecQty=RecQty+"+SGrnQty+",";
                    QS = QS+" IssQty=IssQty+"+SGrnQty+",";
                    QS = QS+" IssVal=IssVal+"+SGrnValue+" ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";
               }
               else
               {
                    QS = "Update "+SItemTable+" Set RecVal =RecVal+"+SGrnValue+",";
                    QS = QS+" RecQty=RecQty+"+SGrnQty+" ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";
               }

               if(theConnection   . getAutoCommit())
                    theConnection   . setAutoCommit(false);

               stat.execute(QS);
          }
          catch(Exception e)
          {
               System.out.println("E6"+e);
               bComflag  = false;
          }
     }

     private void updateUserItemStock(Statement stat,String SItemCode,String SGrnQty,String SGrnValue,String SMrsUserCode)
     {
          try
          {
               Item IC = new Item(SItemCode,iMillCode,SItemTable,SSupTable);
     
               double dAllStock = common.toDouble(IC.getClStock());
               double dAllValue = common.toDouble(IC.getClValue());
     
               double dRate   = 0;
               try
               {
                    dRate = dAllValue/dAllStock;
               }
               catch(Exception ex)
               {
                    dRate=0;
               }
     
               if(dAllStock==0)
               {
                    dRate = common.toDouble(IC.SRate);
               }

               int iCount=0;

               String QS = " Select count(*) from ItemStock Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+iMillCode;
               
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    iCount   =    res.getInt(1);
               }
               res.close();

               String QS1 = "";

               if(iCount>0)
               {
                    QS1 = "Update ItemStock Set Stock=nvl(Stock,0)+"+SGrnQty+" ";
                    QS1 = QS1+" Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+iMillCode;
               }
               else
               {
                    QS1 = " Insert into ItemStock (MillCode,HodCode,ItemCode,Stock,StockValue) Values (";
                    QS1 = QS1+"0"+iMillCode+",";
                    QS1 = QS1+"0"+SMrsUserCode+",";
                    QS1 = QS1+"'"+SItemCode+"',";
                    QS1 = QS1+"0"+SGrnQty+",";
                    QS1 = QS1+"0"+common.getRound(dRate,4)+")";
               }

               String QS2 = " Update ItemStock set StockValue="+common.getRound(dRate,4)+
                            " Where ItemCode='"+SItemCode+"' and MillCode="+iMillCode;


               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);

               stat.execute(QS1);
               stat.execute(QS2);
          }
          catch(Exception e)
          {
               System.out.println("E7"+e);
               bComflag  = false;
          }
     }

     public String getUnitCode(Statement stat)
     {
          String SUnitCode="";

          try
          {
               int iCount=0;

               String QS = " Select Min(Unit_Code) from Unit Where MillCode="+iMillCode;
               
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    SUnitCode = res.getString(1);
               }
               res.close();
          }
          catch(Exception e)
          {
               System.out.println("E7"+e);
               bComflag  = false;
          }

          return SUnitCode;
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnection  . commit();
                    JOptionPane    . showMessageDialog(null,"The Entered Data is Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("Commit");
               }
               else
               {
                    theConnection  . rollback();
                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theConnection   . setAutoCommit(true);
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
