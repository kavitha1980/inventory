package GRN;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class GRNTransferMiddlePanel extends JTabbedPane 
{
         GRNTransferInvMiddlePanel MiddlePanel;
         Object RowData[][],RD[][];

         String ColumnData[] = {"Code","Name","GRN No","Order No","MRS No","Pending Qty","Inv/DC Qty","Recd Qty","Rate","Disc (%)","Cenvat (%)","Tax (%)","Surcharge (%)","Basic","Disc (Rs)","Vat (Rs)","Tax (Rs)","Surcharge (Rs)","Net (Rs)","Department","Group","Unit","User"};
         String ColumnType[] = {"S"   ,"S"   ,"S"     ,"S"       ,"N"     ,"N"          ,"B"         ,"B"       ,"N"   ,"N"       ,"N"         ,"N"      ,"N"            ,"N"    ,"N"        ,"N"       ,"N"       ,"N"             ,"N"       ,"B"         ,"B"    ,"B"   ,"B"   };

         String CD[] = {"Description","DC/Inv Qty","Qty Recd @ Gate","Click To Close"};
         String CT[] = {"S","N","N","B"};

         JLayeredPane DeskTop;
         String       SSupCode;
         int          iMillCode;
         JLabel       LGrnNo;
         String       SYearCode;

         Vector VIName,VDCQty,VQty,VGId;

         Vector VGCode,VGName,VGBlock,VGGrnNo,VGOrderNo,VGMRSNo;
         Vector VGPendQty,VGRate,VGDiscPer,VGVatPer,VGSTPer,VGSurPer;
         Vector VGDept,VGDeptCode,VGGroup,VGGroupCode,VGUnit,VGUnitCode;
         Vector VGBlockCode,VId,VGOrdQty,VGOrdDate;

         Common common   = new Common();

         public GRNTransferMiddlePanel(JLayeredPane DeskTop,String SSupCode,int iMillCode,JLabel LGrnNo,String SYearCode)
         {
              this.DeskTop   = DeskTop;
              this.SSupCode  = SSupCode;
              this.iMillCode = iMillCode;
              this.LGrnNo    = LGrnNo;
              this.SYearCode = SYearCode;
         }

         public void createComponents(String SGINo)
         {
            setVectorData(SGINo);
            setRowData();
            try
            {
                  MiddlePanel = new GRNTransferInvMiddlePanel(DeskTop,RowData,ColumnData,ColumnType,iMillCode);
                  TabReport tabreport = new TabReport(RD,CD,CT);
                  addTab("Gate Inward ",tabreport);
                  addTab("Materials Pending for Transfer",MiddlePanel);
                  tabreport.setBorder(new TitledBorder("Reference From Gate Entry"));
                  MiddlePanel.setBorder(new TitledBorder("Pending Purchase Orders"));
                  tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            }
            catch(Exception ex)
            {
                  System.out.println("GRNInvMiddlePanel: "+ex);
            }
         }
        public void setVectorData(String SGINo)
        {
               
          String QS = "";
          String QS1= "";

          QS  =   " SELECT GateInward.Item_Name,GateInward.SupQty,GateInward.Id,InvItems.Item_Name,GateInward.GateQty "+
                  " From GateInward Left Join InvItems On InvItems.Item_Code = GateInward.Item_Code "+
                  " WHERE GateInward.GINo="+SGINo+" And GrnNo=0 And GateInward.Authentication=1 "+
                  " And GateInward.MillCode="+iMillCode+
                  " Order By 1";

          QS1 =   " SELECT StockTransfer.Code, InvItems.Item_Name, OrdBlock.BlockName, "+
                  " StockTransfer.DCNo as OrderNo, 0 as MrsNo, "+
                  " StockTransfer.Qty-StockTransfer.InvQty AS Pending, "+
                  " StockTransfer.Rate, 0 as DiscPer, 0 as CenVatPer, "+
                  " 0 as TaxPer, 0 as SurPer, Dept.Dept_Name, "+
                  " StockTransfer.Dept_Code, Cata.Group_Name, StockTransfer.Group_Code, "+
                  " Unit.Unit_Name, StockTransfer.Unit_Code,StockTransfer.Id, "+
                  " StockTransfer.Block,StockTransfer.Qty,StockTransfer.TranDate "+
                  " FROM ((((StockTransfer INNER JOIN InvItems ON "+
                  " StockTransfer.Code = InvItems.Item_Code) "+
                  " INNER JOIN OrdBlock ON StockTransfer.Block = OrdBlock.Block) "+
                  " INNER JOIN Dept ON StockTransfer.Dept_Code = Dept.Dept_code) "+
                  " INNER JOIN Cata ON StockTransfer.Group_Code = Cata.Group_Code) "+
                  " INNER JOIN Unit ON StockTransfer.Unit_Code = Unit.Unit_Code "+
                  " Where StockTransfer.InvQty < StockTransfer.Qty  "+
                  " And StockTransfer.ToMillCode="+iMillCode+
                  " Order By 3,4";

            VIName    = new Vector();
            VDCQty    = new Vector();
            VQty      = new Vector();
            VGId      = new Vector();

            VGCode     = new Vector();
            VGName     = new Vector(); 
            VGBlock    = new Vector();
            VGGrnNo    = new Vector();
            VGOrderNo  = new Vector();
            VGMRSNo    = new Vector();
            VGPendQty  = new Vector();
            VGRate     = new Vector();
            VGDiscPer  = new Vector();
            VGVatPer   = new Vector();
            VGSTPer    = new Vector();
            VGSurPer   = new Vector();
            VGDept     = new Vector();
            VGDeptCode = new Vector(); 
            VGGroup    = new Vector();
            VGGroupCode= new Vector(); 
            VGUnit     = new Vector();
            VGUnitCode = new Vector();
            VGBlockCode= new Vector();
            VGOrdQty   = new Vector();
            VGOrdDate  = new Vector();
            VId        = new Vector();

            try
            {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       stat =  theConnection.createStatement();
                   ResultSet res  = stat.executeQuery(QS);
                   while (res.next())
                   {
                         String str = common.parseNull(res.getString(1))+""+common.parseNull(res.getString(4));
                         VIName    .addElement(str);
                         VDCQty    .addElement(res.getString(2));
                         VQty      .addElement(res.getString(5));
                         VGId      .addElement(res.getString(3));
                   }
                   res.close();

                   ResultSet res1 = stat.executeQuery(QS1);
                   while(res1.next())
                   {
                           VGCode    .addElement(res1.getString(1));
                           VGName    .addElement(res1.getString(2)); 
                           VGBlock   .addElement(res1.getString(3));
                           VGGrnNo   .addElement("0");
                           VGOrderNo .addElement(res1.getString(4));
                           VGMRSNo   .addElement(res1.getString(5));
                           VGPendQty .addElement(res1.getString(6));
                           VGRate    .addElement(res1.getString(7));
                           VGDiscPer .addElement(res1.getString(8));
                           VGVatPer  .addElement(res1.getString(9));
                           VGSTPer   .addElement(res1.getString(10));
                           VGSurPer  .addElement(res1.getString(11));
                           VGDept    .addElement(res1.getString(12));
                           VGDeptCode.addElement(res1.getString(13)); 
                           VGGroup   .addElement(res1.getString(14));
                           VGGroupCode.addElement(res1.getString(15)); 
                           VGUnit     .addElement(res1.getString(16));
                           VGUnitCode .addElement(res1.getString(17));
                           VId        .addElement(res1.getString(18));
                           VGBlockCode.addElement(res1.getString(19));
                           VGOrdQty   .addElement(res1.getString(20));
                           VGOrdDate  .addElement(res1.getString(21));
                   }
                   res1.close();
                   stat.close();
                   setGRNNo();
            }
            catch(Exception ex)
            {
                System.out.println(ex);
            }
        }
        public void setGRNNo()
        {
               String SGNo = getId();
               LGrnNo.setText(SGNo);

               for(int i=0;i<VGBlock.size();i++)
               {
                    VGGrnNo.setElementAt(SGNo,i);    
               }
        }
        public boolean setRowData()
        {
            RD      = new Object[VIName.size()][CD.length];
            RowData = new Object[VGCode.size()][ColumnData.length];

            for(int i=0;i<VIName.size();i++)
            {
                    RD[i][0] = (String)VIName.elementAt(i);
                    RD[i][1] = (String)VDCQty.elementAt(i);
                    RD[i][2] = (String)VQty.elementAt(i);
                    RD[i][3] = new Boolean(false);
            }

            for(int i=0;i<VGCode.size();i++)
            {
                      RowData[i][0]  = (String)VGCode    .elementAt(i);
                      RowData[i][1]  = (String)VGName    .elementAt(i);
                      RowData[i][2]  = (String)VGBlock.elementAt(i)+"-"+(String)VGGrnNo   .elementAt(i);
                      RowData[i][3]  = "DC-"+(String)VGOrderNo .elementAt(i)+"/"+common.parseDate((String)VGOrdDate.elementAt(i));
                      RowData[i][4]  = (String)VGMRSNo   .elementAt(i);
                      RowData[i][5]  = (String)VGPendQty .elementAt(i);
                      RowData[i][6]  = "";
                      RowData[i][7]  = "";
                      RowData[i][8]  = (String)VGRate    .elementAt(i);
                      RowData[i][9]  = (String)VGDiscPer .elementAt(i);
                      RowData[i][10] = (String)VGVatPer  .elementAt(i);
                      RowData[i][11] = (String)VGSTPer   .elementAt(i);
                      RowData[i][12] = (String)VGSurPer  .elementAt(i);
                      RowData[i][13] = "0";
                      RowData[i][14] = "0";
                      RowData[i][15] = "0";
                      RowData[i][16] = "0";
                      RowData[i][17] = "0";
                      RowData[i][18] = "0";
                      RowData[i][19] = "";
                      RowData[i][20] = "";
                      RowData[i][21] = "";
                      RowData[i][22] = "";
            }
            return true;
        }
        public String getBlockCode(int i)
        {
               return (String)VGBlockCode.elementAt(i);
        }
        public String getOrderNo(int i)
        {
               return (String)VGOrderNo.elementAt(i);
        }
        public String getMRSNo(int i)
        {
               return (String)VGMRSNo.elementAt(i);
        }
        public String getGRNNo(int i)
        {
               return (String)VGGrnNo.elementAt(i);
        }
        public String getOrdQty(int i)
        {
               return (String)VGOrdQty.elementAt(i);
        }

        private String  getId()
        {
            String SId ="";
            String QS ="";
            try
            {
                    QS = "  Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 2";

                    ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                    Connection      theConnection =  oraConnection.getConnection();

                    Statement       stat =  theConnection.createStatement();
                    
                   ResultSet res  = stat.executeQuery(QS);
                   while (res.next())
                   {
                     SId = res.getString(1);    
                   }
                   res.close();
                   stat.close();
            }
            catch(Exception ex)
            {
                System.out.println("E1"+ex );
            }

           return SId;
        }

}
