package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GrnAbsPrint
{
     FileWriter     FW, FWHtml;
     Vector         VSelectedGrnNo,VSelectedGI;

     Vector         VSeleGINo,VSeleGIDate,VSeleSupName,VSeleInvNo,VSeleInvDate,VSeleDCNo,VSeleDCDate,VSeleInwType,VSeleInwMode,VSeleCategory,VSeleRemarks,VSeleTime;

     Common         common    = new Common();
     int            iMillCode = 0;
     String         SSupTable,SMillName;

     PrinterComm    Printer;

     public GrnAbsPrint(Vector VSelectedGrnNo,Vector VSelectedGI,Vector VSeleGINo,Vector VSeleGIDate,Vector VSeleSupName,Vector VSeleInvNo,Vector VSeleInvDate,Vector VSeleDCNo,Vector VSeleDCDate,Vector VSeleInwType,Vector VSeleInwMode,Vector VSeleCategory,Vector VSeleRemarks,Vector VSeleTime,int iMillCode,String SSupTable,String SMillName)
     {
          this . VSelectedGrnNo    = VSelectedGrnNo;
          this . VSelectedGI       = VSelectedGI;
          this . VSeleGINo         = VSeleGINo;
          this . VSeleGIDate       = VSeleGIDate;
          this . VSeleSupName      = VSeleSupName;
          this . VSeleInvNo        = VSeleInvNo;
          this . VSeleInvDate      = VSeleInvDate;
          this . VSeleDCNo         = VSeleDCNo;
          this . VSeleDCDate       = VSeleDCDate;
          this . VSeleInwType      = VSeleInwType;
          this . VSeleInwMode      = VSeleInwMode;
          this . VSeleCategory     = VSeleCategory;
          this . VSeleRemarks      = VSeleRemarks;
          this . VSeleTime         = VSeleTime;
          this . iMillCode         = iMillCode;
          this . SSupTable         = SSupTable;
          this . SMillName         = SMillName;

          try
          {
               Printer       = new PrinterComm();
               File file     = new File(common.getPrintPath()+"Grns.prn");
               FW = new FileWriter(file);
               PrnWriting();
               FW   . close();
               Printer.CallPrinter(file);
          }catch(Exception ex)
          {ex.printStackTrace();}
     }

     public GrnAbsPrint(Vector VSelectedGrnNo,Vector VSelectedGI,Vector VSeleGINo,Vector VSeleGIDate,Vector VSeleSupName,Vector VSeleInvNo,Vector VSeleInvDate,Vector VSeleDCNo,Vector VSeleDCDate,Vector VSeleInwType,Vector VSeleInwMode,Vector VSeleCategory,Vector VSeleRemarks,Vector VSeleTime,int iMillCode,String SSupTable,String SMillName,FileWriter FWHtml)
     {
          this . VSelectedGrnNo    = VSelectedGrnNo;
          this . VSelectedGI       = VSelectedGI;
          this . VSeleGINo         = VSeleGINo;
          this . VSeleGIDate       = VSeleGIDate;
          this . VSeleSupName      = VSeleSupName;
          this . VSeleInvNo        = VSeleInvNo;
          this . VSeleInvDate      = VSeleInvDate;
          this . VSeleDCNo         = VSeleDCNo;
          this . VSeleDCDate       = VSeleDCDate;
          this . VSeleInwType      = VSeleInwType;
          this . VSeleInwMode      = VSeleInwMode;
          this . VSeleCategory     = VSeleCategory;
          this . VSeleRemarks      = VSeleRemarks;
          this . VSeleTime         = VSeleTime;
          this . iMillCode         = iMillCode;
          this . SSupTable         = SSupTable;
          this . SMillName         = SMillName;
          this . FWHtml            = FWHtml;

          try
          {
               Printer       = new PrinterComm();
               File file     = new File(common.getPrintPath()+"Grns.prn");
               FW = new FileWriter(file);
               PrnWriting();
               FW   . close();
               Printer.CallPrinter(file);
          }catch(Exception ex)
          {ex.printStackTrace();}
     }

     public void PrnWriting()
     {
          for(int i = 0;i<VSelectedGrnNo.size();i++)
          {
               int       iGrnRej   = 0;
               String    SGrnNo    = (String)VSelectedGrnNo.elementAt(i);
                         iGrnRej   = getRejDetail((String)VSelectedGrnNo.elementAt(i));

               new GrnFormPrint(SGrnNo,iMillCode,SSupTable,SMillName,FW,FWHtml);

               if(iGrnRej  != 0)
               {
                    Vector    VOneGrn   = new Vector();
                              VOneGrn   . addElement((String)VSelectedGrnNo.elementAt(i));
     
                    new GrnRejectionFormPrint(VOneGrn,iMillCode,SSupTable,SMillName,FW);
               }

               String SGI = (String)VSelectedGI.elementAt(i);

               int iIndex = common.indexOf(VSeleGINo,SGI);

               if(iIndex>=0)
               {
                    String SGINo     = (String)VSeleGINo.elementAt(iIndex);
                    String SGIDate   = (String)VSeleGIDate.elementAt(iIndex);
                    String SSupName  = (String)VSeleSupName.elementAt(iIndex);
                    String SInwType  = (String)VSeleInwType.elementAt(iIndex);
                    String SInvNo    = (String)VSeleInvNo.elementAt(iIndex);
                    String SInvDate  = (String)VSeleInvDate.elementAt(iIndex);
                    String SDCNo     = (String)VSeleDCNo.elementAt(iIndex);
                    String SDCDate   = (String)VSeleDCDate.elementAt(iIndex);
                    String SInwMode  = (String)VSeleInwMode.elementAt(iIndex);
                    String SRemarks  = (String)VSeleRemarks.elementAt(iIndex);
                    String SCategory = (String)VSeleCategory.elementAt(iIndex);
                    String STime     = (String)VSeleTime.elementAt(iIndex);
     
                    new GIPrint(FW,SGINo,SGIDate,SSupName,SInwType,SInvNo,SInvDate,SDCNo,SDCDate,SInwMode,SRemarks,SCategory,STime,iMillCode,SMillName,FWHtml);
               }
          }
     }

     public int getRejDetail(String SGrnNo)
     {
          int       iRejCount = 0;
          String    QS        = " Select count(*) from Grn where Grn.rejflag = 1 and MillCode="+iMillCode+" and Grn.GrnNo = "+SGrnNo;
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();
               Statement      stat           = theConnection.createStatement();
               ResultSet      result         = stat.executeQuery(QS);
               
               while (result.next())
               {
                    iRejCount     = common.toInt((String)result.getString(1));
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {System.out.println(ex);ex.printStackTrace();}
          return iRejCount;
     }
}
