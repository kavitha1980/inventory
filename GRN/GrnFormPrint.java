package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GrnFormPrint
{
     FileWriter     FW,FWHtml;
     Common         common    = new Common();

     Vector    VGNo,VGDate,VSupName,VICode,VIName,VSName,VMrsNo,VBName,VOrdNo,
               VUName,VGInwardNo,VGInwardDate,VInvoiceNo,VInvoiceDate,VDcNum,VDcDate,
               VDcQty,VReceivedQty,VOrdQty,VAcceptedQty,VRejectedQty,VGValue,VGOrdSlNo,VGMrsUser;

     Vector    VPOrdNo,VPOSlNo,VPPayTerms,VOrdDate;

     String         SGrnNo    = "";
     String    SRejectionNos  = "";

     double    dValue         = 0;
     int       iMillCode      = 0;
     String    SSupTable,SMillName;

     public GrnFormPrint(String SGrnNo,int iMillCode,String SSupTable,String SMillName,FileWriter FW)
     {
          this . SGrnNo    = SGrnNo;
          this . iMillCode = iMillCode;
          this . SSupTable = SSupTable;
          this . SMillName = SMillName;
          this . FW        = FW;

          setVectors();
          toPrnt();
     }

     public GrnFormPrint(String SGrnNo,int iMillCode,String SSupTable,String SMillName,FileWriter FW, FileWriter FWHtml)
     {
          this . SGrnNo    = SGrnNo;
          this . iMillCode = iMillCode;
          this . SSupTable = SSupTable;
          this . SMillName = SMillName;
          this . FW        = FW;
          this . FWHtml    = FWHtml;

          setVectors();
          toPrnt();
          setHtmlPrint();
     }

     public void toPrnt()
     {
          int iPage      = 0;
          int iPgCont    = 0;
          int iP         = 0;
          int iPgeDiv    = 0;
          try
          {
               for(int i = 0;i<VGNo.size();i++)
               {
                    if(iPage == 0)
                    {
                         iPgCont   = 0;
                         if(iP!=0)
                         {
                              FW.write("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                              FW.write("                                                                                                                                                                                                               ...contd.");
                         }

                         FW.write("M"+common.Space(41)+"E"+SMillName+"F\n\n");
                         //FW.write(common.Space(68)+"EPERUNDURAI - 638 052F\n\n");
                         FW.write(common.Space(62)+"EGOODS RECEIVED NOTE (G.R.N)Fg\n");


                         FW.write("|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                         FW.write("| GRN NO.                                    |"+"E"+common.Pad(" "+(String)VGNo.elementAt(i),70)+"F@g"+common.Space(117)+"| GRN DATE.                          |"+"E"+common.Pad(" "+common.parseDate((String)VGDate.elementAt(i)),25)+"F@g"+common.Space(180)+"| Page       |"+common.Cad(String.valueOf((iP+1)),21)+"|\n");
                         FW.write("|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                         FW.write("| SUPPLIER NAME                              |E"+common.Pad(" "+(String)VSupName.elementAt(i),70)+"F | PAYMENT TERMS                      |"+common.Pad(" "+getPayTerms((String)VOrdNo.elementAt(i)),60)+"|\n");
                         FW.write("|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                         FW.write("|         |                           |      |          |     |                   |         |       |                  |                     |                       EQUANTITYF                                 |        |\n");
                         FW.write("| M.CODE  |    MATERIAL DESCRIPTION   |  UOM | MRS. NO. |BLOCK|     P.O.No/Dt.    |   USER  |  UNIT |   G.I.No. / Dt.  |     Inv./DC.No/Dt.  |----------------------------------------------------------------| Recd.By|\n");
                         FW.write("|         |                           |      |          |     |                   |         |       |                  |                     | As Per DC  | RECEIVED   | REJECTED   | ACCEPTED   |  ORDERED   |        |\n");
                         FW.write("|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");


                         iP++;

                         if(((VGNo.size())-i)>25)
                         {
                             iPgeDiv    =    25;
                         }
                         else if(((VGNo.size())-i)==25)
                         {
                             iPgeDiv    =    23;
                         }
                         else if(((VGNo.size())-i)==24)
                         {
                             iPgeDiv    =    22;
                         }
                         else
                         {
                             iPgeDiv    =    20;
                         }
                    }
                    FW   .    write("|"+common.Pad((String)VICode     . elementAt(i),9)+"|"+
                              common.Pad((String)VIName         . elementAt(i),27)+"|"+
                              common.Pad((String)VSName         . elementAt(i),6)+"|"+
                              common.Pad((String)VMrsNo         . elementAt(i),10)+"|"+
                              common.Pad((String)VBName         . elementAt(i),5)+"|"+
                              common.Pad((String)VOrdNo         . elementAt(i)+"/"+getOrdDate((String)VOrdNo.elementAt(i)),19)+"|"+
                              common.Pad((String)VGMrsUser      . elementAt(i),9)+"|"+
                              common.Pad((String)VUName         . elementAt(i),7)+"|"+
                              common.Pad((String)VGInwardNo     . elementAt(i)+"/"+common.parseDate((String)VGInwardDate   . elementAt(i)),18)+"|");

                              if(((String)VInvoiceNo     . elementAt(i)).length()>0)
                                   FW   .    write(common.Pad((String)VInvoiceNo     . elementAt(i)+"/"+common.parseDate((String)VInvoiceDate   . elementAt(i)),21)+"|");
                              else
                                   FW   .    write(common.Pad((String)VDcNum         . elementAt(i)+"/"+common.parseDate((String)VDcDate        . elementAt(i)),21)+"|");

                    FW   .    write(common.Rad(common.getRound((String)VDcQty    . elementAt(i),3),12)+"|"+
                              common.Rad(common.getRound((String)VReceivedQty    . elementAt(i),3),12)+"|"+
                              common.Rad(common.getRound(String.valueOf(-1*common.toDouble((String)VRejectedQty    . elementAt(i))),3),12)+"|"+
                              common.Rad(common.getRound((String)VAcceptedQty    . elementAt(i),3),12)+"|"+
                              common.Rad(common.getRound((String)VOrdQty         . elementAt(i),3),12)+"|"+
                              common.Pad("",8)+"|\n");

                    FW   .    write("|"+common.Space(9)+"|"+
                              common.Space(27)    +"|"+
                              common.Space(6)     +"|"+
                              common.Space(10)    +"|"+
                              common.Space(5)     +"|"+
                              common.Space(19)    +"|"+
                              common.Space(9)     +"|"+
                              common.Space(7)     +"|"+
                              common.Space(18)    +"|"+
                              common.Space(21)    +"|"+
                              common.Space(12)    +"|"+
                              common.Space(12)    +"|"+
                              common.Space(12)    +"|"+
                              common.Space(12)    +"|"+
                              common.Space(12)    +"|"+
                              common.Space(8)     +"|\n");

                    dValue    +=   common.toDouble((String)VGValue   . elementAt(i));
                    iPgCont++;
                    iPage = iPgCont%iPgeDiv;
                }
     
               FW.write("|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW.write("| REASON  |                                                                           |                              |                                    | TOTAL      |                                               |\n");
               FW.write("|   FOR   |"+common.Pad(" "+SRejectionNos,75)+"|  Remarks                     |                                    | GRN        |"+"E"+common.Rad(common.getRound(String.valueOf(dValue),2),23)+"F@g"+common.Space(215)+"|\n");
               FW.write("|REJECTION|                                                                           |                              |                                    | VALUE      |                                               |\n");
               FW.write("|         |                                                                           |                              |                                    |            |                                               |\n");
               FW.write("|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW.write("|                                     |                        |                                                     |                                                              |                                  |\n");
               FW.write("|                                     |                        |                                                     |                                                              |                                  |\n");
               FW.write("|                                     |                        |                                                     |                                                              |                                  |\n");
               FW.write("|                                     |                        |                                                     |                                                              |                                  |\n");
               FW.write("|            prepared by              |       Binned           |                    Inspected by                     |                       Store Keeper                           |                Passed            |\n");
               FW.write("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
          }catch(Exception ex)
          {System.out.println(ex);ex.printStackTrace();}
     }

     public void  setVectors()
     {
          SRejectionNos  = "";

          String SMillCon  = " and Grn.Millcode = "+iMillCode;

          VGNo           = new Vector();
          VGDate         = new Vector();
          VSupName       = new Vector();
          VICode         = new Vector();
          VIName         = new Vector();
          VSName         = new Vector();
          VMrsNo         = new Vector();
          VBName         = new Vector();
          VOrdNo         = new Vector();
          VUName         = new Vector();
          VGInwardNo     = new Vector();
          VGInwardDate   = new Vector();
          VInvoiceNo     = new Vector();
          VInvoiceDate   = new Vector();
          VDcNum         = new Vector();
          VDcDate        = new Vector();
          VDcQty         = new Vector();
          VReceivedQty   = new Vector();
          VOrdQty        = new Vector();
          VAcceptedQty   = new Vector();
          VRejectedQty   = new Vector();
          VGValue        = new Vector();
          VGOrdSlNo      = new Vector();
          VGMrsUser      = new Vector();

          VPOrdNo        = new Vector();
          VPOSlNo        = new Vector();
          VPPayTerms     = new Vector();
          VOrdDate       = new Vector();

          String Qs =    " select Serialno,GNo,GDate,SupName,ICode,IName,SName,MrsNo,BName,OrdNo,UName,GInwardNo,"+
                         " GInwardDate,InvoiceNo,InvoiceDate,DcNum,DcDate,DcQty,sum(ReceivedQty),OrdQty,sum(AcceptedQty),sum(RejectedQty),sum(GInvNet),OrdSlNo,UserName "+
                         " from (select grn.slno as Serialno,grn.grnno as GNo,grn.grndate as GDate,"+SSupTable+".name as SupName,"+
                         " Grn.code as ICode,Invitems.item_name as IName,Uom.UomName as SName,"+
                         " grn.mrsno as MrsNo,ordblock.blockname as BName,grn.Orderno as OrdNo,"+
                         " unit.unit_name as UName,Grn.gateinno as GInwardNo,grn.gateindate as GInwardDate,"+
                         " grn.invno as InvoiceNo,grn.invdate as InvoiceDate,grn.dcno as DcNum,"+
                         " grn.dcdate as DcDate,grn.invqty as DcQty,grn.millqty as ReceivedQty,Grn.OrderQty as OrdQty,sum(Grn.GrnQty) as AcceptedQty, 0 as rejectedQty,sum(Grn.InvNet+Grn.Misc) as GInvNet,Grn.OrderSlNo as OrdSlNo,RawUser.UserName from grn"+
                         " inner join "+SSupTable+" on "+SSupTable+".ac_code = grn.sup_code and  grnno = "+SGrnNo+SMillCon+" and Grn.rejflag = 0"+
                         " inner join invitems on invitems.item_Code = grn.code"+
                         " inner join uom on uom.uomcode = invitems.uomcode"+
                         " inner join ordblock on ordblock.block = grn.grnblock"+
                         " inner join unit on unit.unit_code = grn.unit_code"+
                         " inner join rawuser on grn.mrsauthusercode=rawuser.usercode "+
                         " group by grn.slno,grn.grnno,grn.grndate,"+SSupTable+".name,Grn.code,Invitems.item_name,"+
                         " Uom.UomName,grn.mrsno,ordblock.blockname,grn.Orderno,unit.unit_name,"+
                         " Grn.gateinno,grn.gateindate,grn.invno,grn.invdate,grn.dcno,grn.dcdate,"+
                         " grn.invqty,grn.millqty,Grn.OrderQty,Grn.OrderSlNo,RawUser.UserName "+
                         " union all"+
                         " select grn.slno as Serialno,grn.grnno as GNo,grn.grndate as GDate,"+SSupTable+".name as SupName,"+
                         " Grn.code as ICode,Invitems.item_name as IName,Uom.UomName as SName,"+
                         " grn.mrsno as MrsNo,ordblock.blockname as BName,grn.Orderno as OrdNo,"+
                         " unit.unit_name as UName,Grn.gateinno as GInwardNo,grn.gateindate as GInwardDate,"+
                         " grn.invno as InvoiceNo,grn.invdate as InvoiceDate,grn.dcno as DcNum,"+
                         " grn.dcdate as DcDate,grn.invqty as DcQty,grn.millqty as ReceivedQty,Grn.OrderQty as OrdQty, sum(Grn.GrnQty) as AcceptedQty,sum(Grn.GrnQty) as rejectedQty,sum(Grn.InvNet+Grn.Misc) as GInvNet,Grn.OrderSlNo as OrdSlNo,RawUser.UserName from grn"+
                         " inner join "+SSupTable+" on "+SSupTable+".ac_code = grn.sup_code and  grnno = "+SGrnNo+SMillCon+" and Grn.rejflag = 1"+
                         " inner join invitems on invitems.item_Code = grn.code"+
                         " inner join uom on uom.uomcode = invitems.uomcode"+
                         " inner join ordblock on ordblock.block = grn.grnblock"+
                         " inner join unit on unit.unit_code = grn.unit_code"+
                         " inner join rawuser on grn.mrsauthusercode=rawuser.usercode "+
                         " group by grn.slno,grn.grnno,grn.grndate,"+SSupTable+".name,Grn.code,Invitems.item_name,"+
                         " Uom.UomName,grn.mrsno,ordblock.blockname,grn.Orderno,unit.unit_name,"+
                         " Grn.gateinno,grn.gateindate,grn.invno,grn.invdate,grn.dcno,grn.dcdate,"+
                         " grn.invqty,grn.millqty,Grn.OrderQty,Grn.OrderSlNo,RawUser.UserName "+
                         " )"+
                         " group by serialno,GNo,GDate,SupName,ICode,IName,SName,MrsNo,BName,OrdNo,UName,GInwardNo,"+
                         " GInwardDate,InvoiceNo,InvoiceDate,DcNum,DcDate,DcQty,OrdQty,OrdSlNo,UserName ";

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               ResultSet      result         = stat.executeQuery(Qs);

               while(result.next())
               {
                    VGNo           . addElement(common.parseNull((String)result.getString(2)));
                    VGDate         . addElement(common.parseNull((String)result.getString(3)));
                    VSupName       . addElement(common.parseNull((String)result.getString(4)));
                    VICode         . addElement(common.parseNull((String)result.getString(5)));
                    VIName         . addElement(common.parseNull((String)result.getString(6)));
                    VSName         . addElement(common.parseNull((String)result.getString(7)));
                    VMrsNo         . addElement(common.parseNull((String)result.getString(8)));
                    VBName         . addElement(common.parseNull((String)result.getString(9)));
                    VOrdNo         . addElement(common.parseNull((String)result.getString(10)));
                    VUName         . addElement(common.parseNull((String)result.getString(11)));
                    VGInwardNo     . addElement(common.parseNull((String)result.getString(12)));
                    VGInwardDate   . addElement(common.parseNull((String)result.getString(13)));
                    VInvoiceNo     . addElement(common.parseNull((String)result.getString(14)));
                    VInvoiceDate   . addElement(common.parseNull((String)result.getString(15)));
                    VDcNum         . addElement(common.parseNull((String)result.getString(16)));
                    VDcDate        . addElement(common.parseNull((String)result.getString(17)));
                    VDcQty         . addElement(common.parseNull((String)result.getString(18)));
                    VReceivedQty   . addElement(common.parseNull((String)result.getString(19)));
                    VOrdQty        . addElement(common.parseNull((String)result.getString(20)));
                    VAcceptedQty   . addElement(common.parseNull((String)result.getString(21)));
                    VRejectedQty   . addElement(common.parseNull((String)result.getString(22)));
                    VGValue        . addElement(common.parseNull((String)result.getString(23)));
                    VGOrdSlNo      . addElement(common.parseNull((String)result.getString(24)));
                    VGMrsUser      . addElement(common.parseNull((String)result.getString(25)));

               }
               result    . close();

               String QS1 =   " select PurchaseOrder.OrderNo,PurchaseOrder.slNo,PurchaseOrder.PayTerms,PurchaseOrder.OrderDate from PurchaseOrder"+
                              " where MillCode="+iMillCode+" and purchaseOrder.OrderNo in ("+getOrderNo(VOrdNo)+")";

               result    = stat.executeQuery(QS1);

               while(result.next())
               {
                    VPOrdNo        . addElement(common.parseNull((String)result.getString(1)));
                    VPOSlNo        . addElement(common.parseNull((String)result.getString(2)));
                    VPPayTerms     . addElement(common.parseNull((String)result.getString(3)));
                    VOrdDate       . addElement(common.parseNull((String)result.getString(4)));
               }
               result    . close();

               String QS2 = "select distinct rejno from grn where MillCode="+iMillCode+" and grnno = "+SGrnNo+" and (rejno is not null or rejno<>0)";

               result    = stat.executeQuery(QS2);

               while(result.next())
               {
                           SRejectionNos += common.parseNull((String)result.getString(1))+",";
               }

               if(SRejectionNos.length()>=1)
               {
                    SRejectionNos = "See Rejection No : "+SRejectionNos.substring(0,SRejectionNos.length()-1);
               }
               else
               {
                    SRejectionNos = "";
               }
               result    . close();
               stat      . close();

          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public String getOrderNo(Vector theVect)
     {
          String SOrderNo = " ";
          Vector theVe   = new Vector();
          for(int i=0;i<theVect.size();i++)
          {
               String SOrdNo = (String)theVect.elementAt(i);
               if(i == 0)
               {
                    theVe     . addElement(common.parseNull((String)theVect.elementAt(i)));
               }
               else
               {
                    if(!theVe.contains(SOrdNo))
                    {
                         theVe     . addElement(common.parseNull((String)theVect.elementAt(i)));
                    }
               }
          }

          for(int i = 0;i<theVe.size();i++)
          {
               if(i!= (theVe.size()-1))
               {
                    SOrderNo +=    (String)theVe  . elementAt(i);
                    SOrderNo +=    ",";
               }
               else
               {
                    SOrderNo +=    (String)theVe  . elementAt(i);
               }
          }
          return SOrderNo;
     }

     public String getOrdDate(String OrdNo)
     {
          String    SDate     = "";
          int       iIndex    = VPOrdNo   . indexOf(OrdNo);

                    if(VOrdDate.size()>0)
                         SDate     = (String)VOrdDate  . elementAt(iIndex);

          return common.parseDate(SDate);
     }

     public String getPayTerms(String OrdNo)
     {
          String    SPayTerms = "";
          int       iIndex    = VPOrdNo   . indexOf(OrdNo);
                    if(VPPayTerms.size()>0)
                         SPayTerms = (String)VPPayTerms     . elementAt(iIndex);

          return SPayTerms;
     }

     // set Html Print...

     public void setHtmlPrint()
     {
          try
          {
               String SGRNNo       = common.parseNull((String)VGNo.elementAt(0));
               String SGRNDate     = common.parseDate((String)VGDate.elementAt(0));
               String SSupplierName= common.parseNull((String)VSupName.elementAt(0));
               String SPayTerms    = getPayTerms((String)VOrdNo.elementAt(0));

               FWHtml.write("     <p><b> ");
               FWHtml.write("       "+SMillName+"<br> ");
               FWHtml.write("       Goods Received Note (GRN) ");
               FWHtml.write("     </b></p> ");
               
               FWHtml.write("     <table border='0' class='One'> ");
               FWHtml.write("          <tr> ");
               FWHtml.write("               <td width='150'><font size='2'>GRN No.</font></td> ");
               FWHtml.write("               <td width='20'><font size='2'>:</font></td> ");
               FWHtml.write("               <td width='500'><font size='2'><b>"+SGRNNo+"</b></font></td> ");
               FWHtml.write("               <td width='150'><font size='2'>GRN Date</font></td> ");
               FWHtml.write("               <td width='20'><font size='2'>:</font></td> ");
               FWHtml.write("               <td width='500'><font size='2'><b>"+SGRNDate+"</b></font></td> ");
               FWHtml.write("          </tr> ");
               
               FWHtml.write("          <tr> ");
               FWHtml.write("               <td width='150'><font size='2'>Supplier Name</font></td> ");
               FWHtml.write("               <td width='20'><font size='2'>:</font></td> ");
               FWHtml.write("               <td width='500'><font size='2'><b>"+SSupplierName+"</b></font></td> ");
               FWHtml.write("               <td width='150'><font size='2'>Payment Terms</font></td> ");
               FWHtml.write("               <td width='20'><font size='2'>:</font></td> ");
               FWHtml.write("               <td width='500'><font size='2'><b>"+SPayTerms+"</b></font></td> ");
               FWHtml.write("          </tr> ");
               FWHtml.write("     </table> ");
               
               FWHtml.write("     <br> ");
                     
               FWHtml.write("     <table border='1'> ");
               FWHtml.write("          <tr> ");
               FWHtml.write("               <td rowspan='2' align='Center' width='75'><font size='1'><b>M.Code</b></font></td> ");
               FWHtml.write("               <td rowspan='2' align='Center' width='150'><font size='1'><b>Material Description</b></font></td> ");
               FWHtml.write("               <td rowspan='2' align='Center' width='50'><font size='1'><b>UOM</b></font></td> ");
               FWHtml.write("               <td rowspan='2' align='Center' width='100'><font size='1'><b>MRS No.</b></font></td> ");
               FWHtml.write("               <td rowspan='2' align='Center' width='40'><font size='1'><b>Block</b></font></td> ");
               FWHtml.write("               <td rowspan='2' align='Center' width='120'><font size='1'><b>P.O.No/Dt.</b></font></td> ");
               FWHtml.write("               <td rowspan='2' align='Center' width='80'><font size='1'><b>Unit</b></font></td> ");
               FWHtml.write("               <td rowspan='2' align='Center' width='100'><font size='1'><b>GI No./Dt.</b></font></td> ");
               FWHtml.write("               <td rowspan='2' align='Center' width='100'><font size='1'><b>Inv./DC.No./Dt.</b></font></td> ");
               FWHtml.write("               <td colspan='5' align='Center' width='150'><font size='1'><b>Quantity</b></font></td> ");
               FWHtml.write("          </tr> ");
               
               FWHtml.write("          <tr> ");
               FWHtml.write("               <td align='Center' width='30'><font size='1'><b>As Per DC</b></font></td> ");
               FWHtml.write("               <td align='Center' width='30'><font size='1'><b>RECEIVED</b></font></td> ");
               FWHtml.write("               <td align='Center' width='30'><font size='1'><b>REJECTED</b></font></td> ");
               FWHtml.write("               <td align='Center' width='30'><font size='1'><b>ACCEPTED</b></font></td> ");
               FWHtml.write("               <td align='Center' width='30'><font size='1'><b>ORDERED</b></font></td> ");
               FWHtml.write("          </tr> ");
               
               for(int i=0; i<VGNo.size(); i++)
               {
                    String SCode        = common.parseNull((String)VICode.elementAt(i));
                    String SName        = common.parseNull((String)VIName.elementAt(i));
                    String SUOM         = common.parseNull((String)VSName.elementAt(i));
                    String SMrsNo       = common.parseNull((String)VMrsNo.elementAt(i));
                    String SBlock       = common.parseNull((String)VBName.elementAt(i));
                    String SOrderNo     = common.parseNull((String)VOrdNo.elementAt(i))+"/"+getOrdDate((String)VOrdNo.elementAt(i));
                    String SUnitName    = common.parseNull((String)VUName.elementAt(i));
                    String SGINo        = common.parseNull((String)VGInwardNo.elementAt(i))+"/"+common.parseDate((String)VGInwardDate.elementAt(i));

                    String SInvoice     = "";
                    if(common.parseNull((String)VInvoiceNo.elementAt(i)).length() > 0)
                         SInvoice       = common.parseNull((String)VInvoiceNo.elementAt(i))+"/"+common.parseDate((String)VInvoiceDate.elementAt(i));
                    else
                         SInvoice       = common.parseNull((String)VDcNum.elementAt(i))+"/"+common.parseDate((String)VDcDate.elementAt(i));

                    String SDCQty       = common.getRound((String)VDcQty.elementAt(i), 3);
                    String SRecdQty     = common.getRound((String)VReceivedQty.elementAt(i), 3);
                    String SRejectQty   = common.getRound(String.valueOf(-1 * common.toDouble((String)VRejectedQty    . elementAt(i))), 3);
                    String SAcceptQty   = common.getRound((String)VAcceptedQty.elementAt(i), 3);
                    String SOrderQty    = common.getRound((String)VOrdQty.elementAt(i), 3);

                    FWHtml.write("<tr> ");
                    FWHtml.write("      <td width='75'><font size='1'>"+SCode+"&nbsp;</font></td> ");
                    FWHtml.write("      <td width='150'><font size='1'>"+SName+"&nbsp;</font></td> ");
                    FWHtml.write("      <td width='50'><font size='1'>"+SUOM+"&nbsp;</font></td> ");
                    FWHtml.write("      <td width='100'><font size='1'>"+SMrsNo+"&nbsp;</font></td> ");
                    FWHtml.write("      <td width='40'><font size='1'>"+SBlock+"&nbsp;</font></td> ");
                    FWHtml.write("      <td width='120'><font size='1'>"+SOrderNo+"&nbsp;</font></td> ");
                    FWHtml.write("      <td width='80'><font size='1'>"+SUnitName+"&nbsp;</font></td> ");
                    FWHtml.write("      <td width='100'><font size='1'>"+SGINo+"&nbsp;</font></td> ");
                    FWHtml.write("      <td width='100'><font size='1'>"+SInvoice+"&nbsp;</font></td> ");
                    FWHtml.write("      <td width='30' align='Right'><font size='1'>"+SDCQty+"&nbsp;</font></td> ");
                    FWHtml.write("      <td width='30' align='Right'><font size='1'>"+SRecdQty+"&nbsp;</font></td> ");
                    FWHtml.write("      <td width='30' align='Right'><font size='1'>"+SRejectQty+"&nbsp;</font></td> ");
                    FWHtml.write("      <td width='30' align='Right'><font size='1'>"+SAcceptQty+"&nbsp;</font></td> ");
                    FWHtml.write("      <td width='30' align='Right'><font size='1'>"+SOrderQty+"&nbsp;</font></td> ");
                    FWHtml.write("</tr> ");
               }

               String STotalGRNValue    = common.getRound(String.valueOf(dValue), 2);

               FWHtml.write("<tr>");
               FWHtml.write(" <td colspan='11' align='right'><font size='2'><b>Total GRN Value:&nbsp;&nbsp;</b></font></td> ");
               FWHtml.write(" <td colspan='3' align='center'><font size='2'><b>"+STotalGRNValue+"</b></font></td> ");
               FWHtml.write("</tr>");

               FWHtml.write("<tr>");
               FWHtml.write(" <td colspan='8'><font size='2'><b>Reason For Rejection:&nbsp;"+SRejectionNos+"</b></font></td>");
               FWHtml.write(" <td colspan='6'><font size='2'><b>GRN Remarks:</b></font></td> ");
               FWHtml.write("</tr> ");

               FWHtml.write("<tr> ");
               FWHtml.write(" <td colspan='3' height='60' valign='Bottom' align='center'><font size='2'><b>Prepared By</b></font></td> ");
               FWHtml.write(" <td colspan='3' height='60' valign='Bottom' align='center'><font size='2'><b>Binned</b></font></td> ");
               FWHtml.write(" <td colspan='3' height='60' valign='Bottom' align='center'><font size='2'><b>Inspected By</b></font></td> ");
               FWHtml.write(" <td colspan='3' height='60' valign='Bottom' align='center'><font size='2'><b>Store Keeper</b></font></td> ");
               FWHtml.write(" <td colspan='2' height='60' valign='Bottom' align='center'><font size='2'><b>Passed</b></font></td> ");
               FWHtml.write("</tr> ");
               FWHtml.write("</table> ");

               FWHtml.write("<P CLASS='pagebreakhere'> ");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
