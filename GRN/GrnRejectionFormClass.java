package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GrnRejectionFormClass
{
     Vector         VSelectedGrnNo;
     int            iMillCode = 0;
     Common         common    = new Common();

     int            iRejNo         = 0,iGrnNo     = 0,iNrDcNo    = 0;
     String         SRejDate       = "",SGrnDate  = "",SNrDcDate = "";
     String         SSupplierCode  = "";

     Vector         VInvNo,VOrdNo,VGateInNo;
     Vector         VInvDate,VGateInDate; //,VOrdDate
     Vector         VGrnQty,VGrnValue,VReason,VDescription;


     public GrnRejectionFormClass(int iRejNo,int iGrnNo,String SRejDate,String SGrnDate,String SSupplierCode,int iNrDcNo,String SNrDcDate)
     {
          this . iRejNo            = iRejNo;
          this . iGrnNo            = iGrnNo;
          this . SRejDate          = SRejDate;
          this . SGrnDate          = SGrnDate;
          this . SSupplierCode     = SSupplierCode;
          this . iNrDcNo           = iNrDcNo;
          this . SNrDcDate         = SNrDcDate;

          VInvNo                   = new Vector();
          VInvDate                 = new Vector();
          VDescription             = new Vector();
          VOrdNo                   = new Vector();
          VGateInNo                = new Vector();
          VGateInDate              = new Vector();
          VGrnQty                  = new Vector();
          VGrnValue                = new Vector();
          VReason                  = new Vector();
     }

     public void setData(String SInvNo,String SInvDate,int iOrderNo,int iGateInNo,String SGateinDate,String SItemName,double dGrnQty,double dGrnValue,String SReason)
     {
          VInvNo         . addElement(SInvNo);
          VInvDate       . addElement(SInvDate);

          VDescription   . addElement(SItemName);

          VOrdNo         . addElement(String.valueOf(iOrderNo));

          VGateInNo      . addElement(String.valueOf(iGateInNo));
          VGateInDate    . addElement(SGateinDate);

          VGrnQty        . addElement(String.valueOf(dGrnQty));
          VGrnValue      . addElement(String.valueOf(dGrnValue));
          VReason        . addElement(SReason);
     }
}
