package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GrnRejectionFormFrame extends JInternalFrame
{
     Connection          theConnection  = null;
     Common              common         = new Common();
     DateField           DFromDate,DToDate;

     JPanel              TopPanel,BottomPanel,ApplyPanel,DatePanel;
     JButton             BApply,BOk;
     JLayeredPane        DeskTop;

     TabReport           tabreport;
     Object              RowData[][];

     String              ColumnData[] = {"GRN No","Grn Date","Supplier Name","Select for Rejection"};
     String              ColumnType[] = {"N"     ,"S"       ,"S"            ,"B"};

     Vector              VGrnNo,VGrnDate,VSupplierName;
     int                 iMillCode      = 0;
     String              SSupTable,SMillName;
     FileWriter          FW;

     public GrnRejectionFormFrame(JLayeredPane DeskTop,int iMillCode,String SSupTable,String SMillName)
     {
          super("GRN Rejection Form");

          this .DeskTop       = DeskTop;
          this .iMillCode     = iMillCode;
          this .SSupTable     = SSupTable;
          this .SMillName     = SMillName;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          BApply         = new JButton("Apply");
          BOk            = new JButton("OK");
          DFromDate      = new DateField();
          DToDate        = new DateField();

          TopPanel       = new JPanel();
          DatePanel      = new JPanel();
          BottomPanel    = new JPanel();
          ApplyPanel     = new JPanel();

          DFromDate      . setTodayDate();
          DToDate        . setTodayDate();
     }

     public void setLayouts()
     {
          TopPanel       . setLayout(new GridLayout(1,2));
          DatePanel      . setLayout(new GridLayout(2,2));
          ApplyPanel     . setLayout(new BorderLayout());

          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,450,475);
     }

     public void addComponents()
     {
          DatePanel           . add(new JLabel("From Date"));
          DatePanel           . add(new JLabel("To Date"));
          DatePanel           . add(DFromDate);
          DatePanel           . add(DToDate);
          ApplyPanel          . add(BApply);

          TopPanel            . add(DatePanel);
          TopPanel            . add(ApplyPanel);

          BottomPanel         . add(BOk);

          DatePanel           . setBorder(new TitledBorder("Date"));
          ApplyPanel          . setBorder(new TitledBorder("Control"));

          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply    . addActionListener(new ApplyList());
          BOk       . addActionListener(new PrintList());
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    getContentPane()    . add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    DeskTop             . repaint();
                    DeskTop             . updateUI();
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
               if(RowData.length>0)
               {
                    BOk  . setEnabled(true);
               }
               else
               {
                    BOk  . setEnabled(false);
               }
          }
     }

     public void setDataIntoVector()
     {
          ResultSet result    = null;

          String QS      =    "";
          String SFrDate = DFromDate    . toNormal();
          String SToDate = DToDate      . toNormal();

          VGrnNo         = new Vector();
          VGrnDate       = new Vector();
          VSupplierName  = new Vector();

          QS = " select distinct Grn.GrnNo,Grn.GrnDate,"+SSupTable+".Name"+
               " from Grn "+
               " inner join "+SSupTable+" on "+SSupTable+".AC_Code = Grn.Sup_Code"+
               " and Grn.GrnDate>='"+SFrDate+"'"+
               " and Grn.GrnDate<='"+SToDate+"' and Grn.rejflag = 1 and Grn.millcode = "+iMillCode;

          try
          {
               if(theConnection==null)
               {
                    ORAConnection  oraConnection = ORAConnection.getORAConnection();
                                   theConnection = oraConnection.getConnection();
               }
                    Statement stat      = theConnection.createStatement();
                              result    = stat.executeQuery(QS);
                    
               while(result.next())
               {
                    VGrnNo         . addElement(common.parseNull((String)result.getString(1)));
                    VGrnDate       . addElement(common.parseNull((String)result.getString(2)));
                    VSupplierName  . addElement(common.parseNull((String)result.getString(3)));
               }
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VGrnNo.size()][ColumnData.length];

          for(int i=0;i<VGrnNo.size();i++)
          {
               RowData[i][0]  = (String)VGrnNo         . elementAt(i);
               RowData[i][1]  = common.parseDate((String)VGrnDate       . elementAt(i));
               RowData[i][2]  = (String)VSupplierName  . elementAt(i);
               RowData[i][3]  = new Boolean(false);
          }
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               Vector VSelectedGrnNo = new Vector();

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][3];   
                    if(Bselected.booleanValue())
                    {
                         VSelectedGrnNo . addElement(String.valueOf(RowData[i][0]));
                    }
               }

               try
               {
                         FW   = new FileWriter(common.getPrintPath()+"GrnRejectionNote.prn");
                         new GrnRejectionFormPrint(VSelectedGrnNo,iMillCode,SSupTable,SMillName,FW);
                         FW   . close();
               }catch(Exception ex)
               {System.out.println(ex);}
               removeHelpFrame();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop   . remove(this);
               DeskTop   . repaint();
               DeskTop   . updateUI();
          }
          catch(Exception ex) { }
     }

}
