package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GrnRejectionFormPrint
{
     Common         common         = new Common();
     Connection     theConnection  = null;
     FileWriter     FW;

     Vector         VSelectedGrnNo;
     Vector         VSSelectedupplierCode;

     Vector         VSupplierCode,VSupplierName,VAdd1,VAdd2,VAdd3;
     Vector         theVect;
     Vector         VOrderNo,VOrderDate;

     int            iMillCode = 0,iTEnd = 0,iPage = 1,TLine = 0;
     String         SSupTable,SMillName;

     public GrnRejectionFormPrint(Vector VSelectedGrnNo,int iMillCode,String SSupTable,String SMillName,FileWriter FW)
     {
          this . VSelectedGrnNo    = VSelectedGrnNo;
          this . iMillCode         = iMillCode;
          this . SSupTable         = SSupTable;
          this . SMillName         = SMillName;
          this . FW                = FW;

          setDataVector();
          getAddr();
          printFile();
     }

     public void PrnHead()
     {
     
          try
          {
               if(iMillCode==0)
               {
                    FW.write("M");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                    FW.write("|                                                        EAMARJOTHI SPINNING MILLS LIMITEDF@M                                                                                                                                                                            |\n");
                    FW.write("|                                                                     E  PUDUSURIPALAYAMF@M                                                                                                                                                                            |\n");
                    FW.write("|                                                                    ENAMBIYUR  -  638 458F@M                                                                                                                                                                            |\n");
                    FW.write("|                                                               EE-Mail : amarjoth@eth.net       TIN No. 33632960864F                                                         |\n");
                    FW.write("|                                                               EPhones : 04285 - 267201,267301, Fax - 04285-267565F                                                          |\n");
                    FW.write("|                                                                    EOur C.S.T. No. 440691 dt. 24.09.1990F                                                                   |\n");
                    FW.write("|                                                                 ET.N.G.S.T No. R.C. No. 2960864  dt. 01.04.1995F                                                            |\n");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                    FW.write("|                                                                       EREJECTION NOTEF@M                                                                                                                                                                            |\n");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               }

               if(iMillCode ==1)
               {
                    FW.write("M\n");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                    FW.write("|                                                         EAMARJOTHI SPINNING MILLS LTDF@M                                                                                                                                                                            |\n");
                    FW.write("|                                                                 E(DYEING DIVISION)F@M                                                                                                                                                                            |\n");
                    FW.write("|                                                                   EPlot No : E-7-9 & G11-13, SIPCOTF                                                                        |\n");
                    FW.write("|                                                                EPerundurai - 638 052F@M                                                                                                                                                                            |\n");
                    FW.write("|                                                              EPhone : 04294 - 309339  Fax : 04294 - 230392F                                                                 |\n");
                    FW.write("|                                                                          E Regd. OfficeF                                                                                    |\n");
                    FW.write("|                                                           E`AMARJOTHI HOUSE` 157, Kumaran Road, Tirupur - 641 601.F                                                         |\n");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                    FW.write("|                                                                       EREJECTION NOTEF@M                                                                                                                                                                            |\n");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               }
          
               if(iMillCode==3)
               {
                    FW.write("M");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                    FW.write("|                                              EAMARJOTHI COLOUR MELANGE SPINNING MILLS LIMITEDF@M                                                                                                                                                                              |\n");
                    FW.write("|                                                                E  PUDUSURIPALAYAMF@M                                                                                                                                                                            |\n");
                    FW.write("|                                                                ENAMBIYUR  -  638 458F@M                                                                                                                                                                            |\n");
                    FW.write("|                                                            EGOBI(TK),ERODE(DT),TAMILNADU.F@M                                                                                                                                                                            |\n");
                    FW.write("|                                                           EE-Mail : amarjoth@eth.net       TIN No. 33712404816F                                                             |\n");
                    FW.write("|                                                           EPhones : 04285 - 267201,267301, Fax - 04285-267565F                                                              |\n");
                    FW.write("|                                                              EOur C.S.T. No.1031365  dt.05.05.2010                                                                         |\n");
                 // FW.write("|                                                                 ET.N.G.S.T No. R.C. No. 2960864  dt. F                                                                       |\n");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                    FW.write("|                                                                       EREJECTION NOTEF@M                                                                                                                                                                            |\n");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               }

               if(iMillCode==4)
               {
                    FW.write("M");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                    FW.write("|                                                                  ESRI KAMADHENU TEXTILESF@M                                                                                                                                                                            |\n");
                    FW.write("|                                                                    E   PUDUSURIPALAYAM F@M                                                                                                                                                                            |\n");
                    FW.write("|                                                                   ENAMBIYUR  -  638 458F@M                                                                                                                                                                            |\n");
                    FW.write("|                                                                EGOBI(TK),ERODE(DT),TAMILNADU.F@M                                                                                                                                                                            |\n");
                    FW.write("|                                                                EE-Mail : amarjoth@eth.net       TIN No.33612404386                                                         |\n");
                    FW.write("|                                                                EPhones : 04285 - 267201,267301, Fax - 04285-267565F                                                         |\n");
                    FW.write("|                                                                   EOur C.S.T. No.972942 dt.14.12.2008                                                                      |\n");
                 // FW.write("|                                                                ET.N.G.S.T No. R.C. No. 2960864  dt.      F                                                                   |\n");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                    FW.write("|                                                                       EREJECTION NOTEF@M                                                                                                                                                                            |\n");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               }


          }
          catch(Exception ex)
          {System.out.println(ex);}
     }
     
     private void printFile()
     {
          try
          {
               for(int i=0;i<theVect.size();i++)
               {
                    int iPageCount = 1;

                    PrnHead();
                    double dValue  = 0;

                    GrnRejectionFormClass    grnRejection   = (GrnRejectionFormClass)theVect.elementAt(i);
                    int                      iAdIndex       = getAdd(grnRejection.SSupplierCode);
                    
                    FW.write("|"+common.Pad("To,",92)                                         +"| Rejection No  |E"+common.Pad(String.valueOf(grnRejection.iRejNo),32)+"F@M"+common.Space(142)+"F|"+" Rejection Date|"+common.Pad(" "+common.parseDate(grnRejection.SRejDate),12)+" |\n");
                    FW.write("|E"+common.Pad((String)VSupplierName. elementAt(iAdIndex),92)  +"F|"+common.Replicate("-",15)+"-"+common.Replicate("-",32)+"-"+common.Replicate("-",15)+"-"+common.Replicate("-",12)+"-|\n");
                    FW.write("|"+common.Pad((String)VAdd1        . elementAt(iAdIndex),92)    +"| D.C. No       |E"+common.Pad(String.valueOf(grnRejection.iNrDcNo),32)+"F|"+" D.C. Date     |"+common.Pad(" "+common.parseDate(grnRejection.SNrDcDate),12)+" |\n");
                    FW.write("|"+common.Pad((String)VAdd2        . elementAt(iAdIndex),92)    +"|"+common.Replicate("-",15)+"-"+common.Replicate("-",32)+"-"+common.Replicate("-",15)+"-"+common.Replicate("-",12)+"-|\n");
                    FW.write("|"+common.Pad((String)VAdd3        . elementAt(iAdIndex),92)    +"| InWard No     |E"+common.Pad(String.valueOf(grnRejection.iGrnNo),32)+"F|"+" InWard Date   |"+common.Pad(" "+common.parseDate(grnRejection.SGrnDate),12)+" |\n");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                    FW.write("|   EBill NoF    |EBill  DateF|  EOrder NoF  |EOrder DateF|  EGaInWardNoF |EGateInDateF|        EDescriptionF         |    EQtyF     |      EValueF     |              EReasonF                 |\n");
                    FW.write("|              |          |            |          |             |          |                            |       EKgF   |           ERs.P.F|                                     |\n");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");


                    for(int j=0;j<grnRejection.VOrdNo.size();j++)
                    {
                         FW.write("|"+ common.Pad((String) grnRejection.VInvNo       . elementAt(j),14)+"|"+
                                   common.Pad(common.parseDate((String) grnRejection.VInvDate     . elementAt(j)),10)+"|"+
                                   common.Pad((String) grnRejection.VOrdNo       . elementAt(j),12)+"|"+
                                   common.Pad(common.parseDate(getOrderDate((String) grnRejection . VOrdNo        . elementAt(j))),10)+"|"+
                                   common.Pad((String) grnRejection.VGateInNo    . elementAt(j),13)+"|"+
                                   common.Pad(common.parseDate((String) grnRejection.VGateInDate  . elementAt(j)),10)+"|"+
                                   common.Pad((String) grnRejection.VDescription . elementAt(j),28)+"|"+
                                   common.Rad(common.getRound(String.valueOf(-1*common.toDouble((String) grnRejection.VGrnQty      . elementAt(j))),3),12)+"|"+
                                   common.Rad(common.getRound(String.valueOf(-1*common.toDouble((String) grnRejection.VGrnValue    . elementAt(j))),2),16)+"|"+
                                   common.Pad((String) grnRejection.VReason      . elementAt(j),36)+" |\n");

                         FW.write("|"+ common.Pad("",14)+"|"+
                                   common.Pad("",10)+"|"+
                                   common.Pad("",12)+"|"+
                                   common.Pad("",10)+"|"+
                                   common.Pad("",13)+"|"+
                                   common.Pad("",10)+"|"+
                                   common.Pad("",28)+"|"+
                                   common.Rad("",12)+"|"+
                                   common.Rad("",16)+"|"+
                                   common.Pad("",36)+" |\n");

                                   dValue    += -1*common.toDouble((String) grnRejection.VGrnValue    . elementAt(j));

                         if(iPageCount%18==0)
                         {
//                              FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
//                              FW.write("|                                                                                                   ETotalF            |  E"+common.Rad(common.getRound(String.valueOf(dValue),2),14)+"F|                                     |\n");
//                              FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
//                              FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                              FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                              FW.write("|                                                                                                                                                                           |\n");
                              FW.write("|                                                                                                                                                                           |\n");
                              FW.write("|                                                                                                                                                                           |\n");
                              FW.write("|      ES.OF                                                                                                                                        ES.KF                       |\n");
                              FW.write("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                              FW.write("                                                                                                                                                                   ...contd.");
                              PrnHead();
                              FW.write("|"+common.Pad("To,",92)                                         +"| Rejection No  |E"+common.Pad(String.valueOf(grnRejection.iRejNo),32)+"F@M"+common.Space(142)+"F|"+" Rejection Date|"+common.Pad(" "+common.parseDate(grnRejection.SRejDate),12)+" |\n");
                              FW.write("|E"+common.Pad((String)VSupplierName. elementAt(iAdIndex),92)  +"F|"+common.Replicate("-",15)+"-"+common.Replicate("-",32)+"-"+common.Replicate("-",15)+"-"+common.Replicate("-",12)+"-|\n");
                              FW.write("|"+common.Pad((String)VAdd1        . elementAt(iAdIndex),92)    +"| D.C. No       |E"+common.Pad(String.valueOf(grnRejection.iNrDcNo),32)+"F|"+" D.C. Date     |"+common.Pad(" "+common.parseDate(grnRejection.SNrDcDate),12)+" |\n");
                              FW.write("|"+common.Pad((String)VAdd2        . elementAt(iAdIndex),92)    +"|"+common.Replicate("-",15)+"-"+common.Replicate("-",32)+"-"+common.Replicate("-",15)+"-"+common.Replicate("-",12)+"-|\n");
                              FW.write("|"+common.Pad((String)VAdd3        . elementAt(iAdIndex),92)    +"| InWard No     |E"+common.Pad(String.valueOf(grnRejection.iGrnNo),32)+"F|"+" InWard Date   |"+common.Pad(" "+common.parseDate(grnRejection.SGrnDate),12)+" |\n");
                              FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                              FW.write("|   EBill NoF    |EBill  DateF|  EOrder NoF  |EOrder DateF|  EGaInWardNoF |EGateInDateF|        EDescriptionF         |    EQtyF     |      EValueF     |              EReasonF                 |\n");
                              FW.write("|              |          |            |          |             |          |                            |       EKgF   |           ERs.P.F|                                     |\n");
                              FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                         }
                         iPageCount++;
                    }

                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                    FW.write("|                                                                                                   ETotalF            |  E"+common.Rad(common.getRound(String.valueOf(dValue),2),14)+"F|                                     |\n");
                    FW.write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                    FW.write("|                                                                                                                                                                           |\n");
                    FW.write("|                                                                                                                                                                           |\n");
                    FW.write("|                                                                                                                                                                           |\n");
                    FW.write("|      ES.OF                                                                                                                                        ES.KF                       |\n");
                    FW.write("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               }
          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public String getQsOrdeDate()
     {
          String    QS   =    " select distinct PurchaseOrder.Orderno,Purchaseorder.OrderDate from PurchaseOrder"+
                              " inner join Grn on Grn.Orderno    = PurchaseOrder.Orderno"+
                              " and Grn.GrnNo in (";

                              for(int i = 0;i<VSelectedGrnNo.size();i++)
                              {
                                   QS = QS + (String) VSelectedGrnNo.elementAt(i);
                                   if(i!=(VSelectedGrnNo.size()-1))
                                        QS = QS + ",";
                              }
                              QS   = QS + ") and rejflag = 1 and ";
          
                              QS   = QS + " Grn.MillCode = "+iMillCode+" order by PurchaseOrder.OrderNo";


          return QS;
     }

     public String getQsString()
     {
          String    QS = "";

                    QS = " select Grn.rejno,Grn.rejdate,Grn.NrDcNo,Grn.NrDcDate,Grn.grnno,Grn.grndate,"+
                         " Grn.InvNo,Grn.InvDate,Grn.orderNo,Grn.GateInNo,Grn.GateinDate,"+
                         " InvItems.Item_Name,Grn.grnqty,(Grn.InvNet+Grn.Misc) as GValue,Grn.reason,Grn.sup_code from grn"+
                         " inner join Invitems on Invitems.item_code = Grn.Code"+
                         " and Grn.GrnNo in (";

                    for(int i = 0;i<VSelectedGrnNo.size();i++)
                    {
                         QS = QS + (String) VSelectedGrnNo.elementAt(i);
                         if(i!=(VSelectedGrnNo.size()-1))
                              QS = QS + ",";
                    }
                    QS   = QS + ") and rejflag = 1 and ";

                    QS   = QS + " Grn.MillCode = "+iMillCode+" order by Grn.rejNo";

          return QS;
     }

     private void setDataVector()
     {
          theVect                  = new Vector();
          VOrderNo                 = new Vector();
          VOrderDate               = new Vector();
          VSSelectedupplierCode    = new Vector();

          try
          {
               if(theConnection==null)
               {
                    ORAConnection  oraConnection = ORAConnection.getORAConnection();
                                   theConnection = oraConnection.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(getQsString());

               while(theResult.next())
                    organizeData(theResult);
               theResult.close();

               theResult = theStatement.executeQuery(getQsOrdeDate());

               while(theResult.next())
               {
                    VOrderNo       . addElement(common.parseNull(theResult.getString(1)));
                    VOrderDate     . addElement(common.parseNull(theResult.getString(2)));
               }
               theResult.close();

               theStatement.close();
           }
           catch(Exception e)
           {
               System.out.println(e);
           }
     }

     private void organizeData(ResultSet theResult) throws Exception
     {
          int       iRejNo         = theResult.getInt(1);
          String    SRejDate       = common.parseNull((String)theResult.getString(2));

          int       iNrDcNo        = theResult.getInt(3);
          String    SNrDcDate      = common.parseNull((String)theResult.getString(4));

          int       iGrnNo         = theResult.getInt(5);
          String    SGrnDate       = common.parseNull((String)theResult.getString(6));

          String    SInvNo         = common.parseNull((String)theResult.getString(7));
          String    SInvDate       = common.parseNull((String)theResult.getString(8));

          int       iOrderNo       = theResult.getInt(9);

          int       iGateInNo      = theResult.getInt(10);
          String    SGateinDate    = common.parseNull((String)theResult.getString(11));

          String    SItemName      = common.parseNull((String)theResult.getString(12));

          double    dGrnQty        = theResult.getDouble(13);
          double    dGrnValue      = theResult.getDouble(14);

          String    SReason        = common.parseNull((String)theResult.getString(15));
          String    SSupplierCode  = common.parseNull((String)theResult.getString(16));

          VSSelectedupplierCode    . addElement(SSupplierCode);

          int iIndex = getIndexOf(iRejNo);

          if(iIndex==-1)
          {
               GrnRejectionFormClass    grnRejection   = null;
                                        grnRejection   = new GrnRejectionFormClass(iRejNo,iGrnNo,SRejDate,SGrnDate,SSupplierCode,iNrDcNo,SNrDcDate);
                                        grnRejection   . setData(SInvNo,SInvDate,iOrderNo,iGateInNo,SGateinDate,SItemName,dGrnQty,dGrnValue,SReason);

                                        theVect        . addElement(grnRejection);
                                        iIndex         = theVect.size()-1;
          }
          else
          {
                GrnRejectionFormClass   grnRejection   = (GrnRejectionFormClass)theVect.elementAt(iIndex);
                                        grnRejection   . setData(SInvNo,SInvDate,iOrderNo,iGateInNo,SGateinDate,SItemName,dGrnQty,dGrnValue,SReason);
          }
     }

     private int getIndexOf(int iRejNo)
     {
          int iIndex =-1;

          for(int i=0; i<theVect.size(); i++)
          {
               GrnRejectionFormClass grnRejection    = (GrnRejectionFormClass)theVect.elementAt(i);

               if(grnRejection.iRejNo==iRejNo)
               {
                    iIndex = i;
                    break;
               }
          }
          return iIndex;
     }

     private String getOrderDate(String SOrderNo)
     {
          int iIndex     =    VOrderNo       . indexOf(SOrderNo);
          if(iIndex!=-1)
          {
               return (String)VOrderDate     . elementAt(iIndex);
          }
          return " ";
     }

     public void getAddr()
     {
          VSupplierCode  = new Vector();
          VSupplierName  = new Vector();
          VAdd1          = new Vector();
          VAdd2          = new Vector();
          VAdd3          = new Vector();

          String QS =    " Select "+SSupTable+".AC_Code,"+SSupTable+".Name,"+SSupTable+".Addr1,"+SSupTable+".Addr2,"+SSupTable+".Addr3,Place.PlaceName "+
                         " From "+SSupTable+" LEFT Join Place On PlaceCode = "+SSupTable+".City_Code "+
                         " Where "+SSupTable+".Ac_Code in (";

                    for(int i = 0;i<VSSelectedupplierCode.size();i++)
                    {
                         QS = QS   +"'"+(String)VSSelectedupplierCode.elementAt(i)+"'";
                         if(i!=(VSSelectedupplierCode.size()-1))
                              QS = QS   +",";
                    }

                    QS   = QS +")";

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(QS);

               while (res.next())
               {
                    VSupplierCode  . addElement(common.parseNull(res.getString(1)));
                    VSupplierName  . addElement(common.parseNull(res.getString(2)));
                    VAdd1          . addElement(common.parseNull(res.getString(3)));
                    VAdd2          . addElement(common.parseNull(res.getString(4)));
                    VAdd3          . addElement(common.parseNull(res.getString(5)));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);ex.printStackTrace();}
     }

     private int getAdd(String SSupCode)
     {
          int iAddIndex  = VSupplierCode.indexOf(SSupCode);
          if(iAddIndex!=-1)
               return iAddIndex;

          return 0;
     }
}
