package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GrnRejectionFrame extends JInternalFrame
{
     JComboBox      JCGrn;
     JButton        BApply,BOk;
     JPanel         TopPanel,MiddlePanel,BottomPanel;
     
     InsTabReport   instabreport;
     Connection     theConnection = null;
     
     Object         RowData[][];
     
     String         ColumnData[] = {"GRN No","Block","Date","Supplier","Code","Name","Order No","Order Qty","Pending Qty","DC/Inv Qty","Recd Qty","Accepted","Rejection","Over","Reason","NRdc No","NRdc Date"};
     String         ColumnType[] = {"N"     ,"S"    ,"S"   ,"S"       ,"S"   ,"S"   ,"N"       ,"N"        ,"N"          ,"N"         ,"N"       ,"E"       ,"N"        ,"B"   ,"S"     ,"S"      ,"S"};
     
     Common         common = new Common();
     
     Vector         VGrnId,VGrnNo,VBlock,VDate,VSupName,VOrdNo,VGrnCode,VGrnName,VOrdQty,VPending,VInvQty,VRecdQty,VPId,VOldInvQty;
     Vector         VBlockCode,VSupCode,VGiNo,VGiDate,VInvNo,VInvDate,VDcNo,VDcDate,VSlNo,VMrsSlNo,VMrsUserCode,VOrderSlNo;
     Vector         VRate,VDiscPer,VCenvatPer,VTaxPer,VSurPer,VMrsNo;
     Vector         VDeptCode,VGroupCode,VUnitCode,VTaxC;
     
     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     Vector         VCode,VName;
     int            iMillCode,iUserCode;
     String         SYearCode;
     String         SItemTable,SSupTable;
     
     String         SGrnNo="";
     
     RejectionReasonFrame reasonframe;
     
     String         SRejNo    = "";
     
     boolean        bFlag     = false;
     boolean        bComflag  = true;

     Vector VSUserCode,VSItemCode,VSGrnQty;

     public GrnRejectionFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,int iUserCode,String SYearCode,String SItemTable,String SSupTable)
     {
          super("Materials Pending for Inspection");
          this.DeskTop    = DeskTop;
          this.SPanel     = SPanel;
          this.VCode      = VCode;
          this.VName      = VName;
          this.iMillCode  = iMillCode;
          this.iUserCode  = iUserCode;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
     

          ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                          theConnection =  oraConnection.getConnection();

          getGrnData();
          createVectors();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          BApply      = new JButton("Apply");
          BOk         = new JButton("Update");
          TopPanel    = new JPanel();
          MiddlePanel = new JPanel();
          BottomPanel = new JPanel();
          JCGrn       = new JComboBox(VGrnNo);
     }

     public void setLayouts()
     {
          TopPanel       . setLayout(new FlowLayout(0,0,0));
          MiddlePanel    . setLayout(new BorderLayout());
          
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("Select Grn Number"));
          TopPanel.add(JCGrn);
          TopPanel.add(BApply);
          BottomPanel.add(BOk);
          
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     
     public void addListeners()
     {
          BApply    . addActionListener(new ApplyList());
          BOk       . addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validData())
               {
                    BOk  . setEnabled(false);
                    getRejectionNo();
                    insertInspection();
                    getACommit();
                    removeHelpFrame();
               }
               else
               {
                    BOk  . setEnabled(true);
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     
     public void createVectors()
     {
          VGrnId       = new Vector();
          VBlock       = new Vector();
          VDate        = new Vector();
          VSupName     = new Vector();
          VOrdNo       = new Vector();
          VGrnCode     = new Vector();
          VGrnName     = new Vector();
          VOrdQty      = new Vector();
          VPending     = new Vector();
          VInvQty      = new Vector();
          VRecdQty     = new Vector();
          VPId         = new Vector();
          VOldInvQty   = new Vector();
          VDiscPer     = new Vector();
          VCenvatPer   = new Vector();
          VTaxPer      = new Vector();
          VSurPer      = new Vector();
          VRate        = new Vector();
          VMrsNo       = new Vector();
          
          VBlockCode   = new Vector();
          VSupCode     = new Vector();
          VGiNo        = new Vector();
          VGiDate      = new Vector();
          
          VInvNo       = new Vector();
          VInvDate     = new Vector();
          VDcNo        = new Vector();
          VDcDate      = new Vector();
          
          VSlNo        = new Vector();
          VMrsSlNo     = new Vector();
          VMrsUserCode = new Vector();
          VOrderSlNo   = new Vector();
          
          VDeptCode    = new Vector();
          VGroupCode   = new Vector();
          VUnitCode    = new Vector();
          VTaxC        = new Vector();
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BApply.setEnabled(false);
               try
               {
                    MiddlePanel.remove(instabreport);
                    MiddlePanel.updateUI();
               }
               catch(Exception ex){}
               setDataIntoVector();
               setRowData();
               try
               {
                    instabreport = new InsTabReport(RowData,ColumnData,ColumnType);
                    MiddlePanel.add(instabreport,BorderLayout.CENTER);
                    MiddlePanel.updateUI();
                    instabreport.ReportTable.addKeyListener(new KeyList());
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
               }
          }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if (ke.getKeyCode() == KeyEvent.VK_INSERT)
               {
                    int i = instabreport.ReportTable.getSelectedRow();
                    double dRejQty  = common.toDouble(((String)RowData[i][12]).trim());
                    if(dRejQty>0)
                    {
                         showReasonFrame();
                    }
               }
          } 
     }

     private void showReasonFrame()
     {
          try
          {
               DeskTop   . remove(reasonframe);
               DeskTop   . updateUI();
          }
          catch(Exception ex){}
          
          try
          {
               Frame     dummy          = new Frame();
               JDialog   theDialog      = new JDialog(dummy,"Description Dialog",true);
                         reasonframe    = new RejectionReasonFrame(DeskTop,instabreport,theDialog);
               
               theDialog.getContentPane().add(reasonframe.ModiPanel);
               theDialog.setBounds(80,100,450,350);
               theDialog.setVisible(true);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private boolean validData()
     {
          BOk.setEnabled(false);
          boolean bflag=false;
          
          VSUserCode = new Vector();
          VSItemCode = new Vector();
          VSGrnQty   = new Vector();

          int iIndex=-1;


          int iCount=0;
          
          for(int i=0;i<RowData.length;i++)
          {
               Boolean Bselected = (Boolean)RowData[i][13];
               
               if(Bselected.booleanValue())
               {
                    double dMillQty = common.toDouble(((String)RowData[i][10]).trim());
                    double dAccQty  = common.toDouble(((String)RowData[i][11]).trim());
                    double dRejQty  = common.toDouble(((String)RowData[i][12]).trim());
                    
                    if(dAccQty>dMillQty)
                    {
                         JOptionPane.showMessageDialog(null,"Invalid Accepted Quantity","Error",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }
                    if(dRejQty>0)
                    {
                         String SReason = ((String)RowData[i][14]).trim();
                         
                         if(SReason.equals(""))
                         {
                              JOptionPane.showMessageDialog(null,"Reason Field is Empty","Error",JOptionPane.ERROR_MESSAGE);
                              return false;
                         }

                         String SBlockCode   = (String)VBlockCode  . elementAt(i);
                         String SItemCode    = (String)VGrnCode    . elementAt(i);
                         String SUserCode    = (String)VMrsUserCode. elementAt(i);

                         if(common.toInt(SBlockCode)>=2)
                              continue;
     
                         iIndex=-1;
     
                         iIndex = getIndexOf(SUserCode,SItemCode);
     
     
                         if(iIndex>=0)
                         {
                              double dOldQty = common.toDouble((String)VSGrnQty.elementAt(iIndex));
     
                              double dNewQty = dOldQty + dRejQty;
     
                              VSGrnQty.setElementAt(common.getRound(dNewQty,3),iIndex);
                         }
                         else
                         {
                              VSUserCode.addElement(SUserCode);
                              VSItemCode.addElement(SItemCode);
                              VSGrnQty.addElement(""+dRejQty);
                         }
                    }
               }
               else
               {
                    iCount++;
               }
          }
          
          if(iCount>0)
          {
               JOptionPane.showMessageDialog(null,"Some Rows Not Selected","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          try
          {
               Statement       stat          =  theConnection.createStatement();

               if(VSUserCode.size()>0)
               {
                    for(int i=0;i<VSUserCode.size();i++)
                    {
     
                         String SUserCode = (String)VSUserCode.elementAt(i);
                         String SItemCode = (String)VSItemCode.elementAt(i);
                         double dGrnQty   = common.toDouble((String)VSGrnQty.elementAt(i));
     
                         double dStock = 0;
     
                         String QS = " Select Sum(Stock) from ItemStock Where MillCode="+iMillCode+
                                     " and HodCode="+SUserCode+" and ItemCode='"+SItemCode+"'";
     
                         ResultSet result = stat.executeQuery(QS);
     
                         while(result.next())
                         {
                              dStock = common.toDouble(common.parseNull(result.getString(1)));
                         }
                         result.close();
     
                         if(dStock<dGrnQty)
                         {
                              JOptionPane.showMessageDialog(null,"Stock Not Available for Item - "+SItemCode,"Attention",JOptionPane.INFORMATION_MESSAGE);
                              return false;
                         }
                    }
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return true;
     }

     public int getIndexOf(String SUserCode,String SItemCode)
     {
          int iIndex=-1;

          for(int i=0;i<VSUserCode.size();i++)
          {
               String SOUserCode = (String)VSUserCode.elementAt(i);
               String SOItemCode = (String)VSItemCode.elementAt(i);

               if(SUserCode.equals(SOUserCode) && SItemCode.equals(SOItemCode))
               {
                    iIndex = i;
                    break;
               }
               else
               {
                    continue;
               }
          }
          return iIndex;
     }

     public void setDataIntoVector()
     {
          VGrnId       .removeAllElements();
          VBlock       .removeAllElements();
          VDate        .removeAllElements();
          VSupName     .removeAllElements();
          VOrdNo       .removeAllElements();
          VGrnCode     .removeAllElements();
          VGrnName     .removeAllElements();
          VOrdQty      .removeAllElements();
          VPending     .removeAllElements();
          VInvQty      .removeAllElements();
          VRecdQty     .removeAllElements();
          VPId         .removeAllElements();
          VOldInvQty   .removeAllElements();
          VDiscPer     .removeAllElements();
          VCenvatPer   .removeAllElements();
          VTaxPer      .removeAllElements();
          VSurPer      .removeAllElements();
          VRate        .removeAllElements();
          VMrsNo       .removeAllElements();
          
          VBlockCode   .removeAllElements();
          VSupCode     .removeAllElements();
          VGiNo        .removeAllElements();
          VGiDate      .removeAllElements();
          
          VInvNo       .removeAllElements();
          VInvDate     .removeAllElements();
          VDcNo        .removeAllElements();
          VDcDate      .removeAllElements();
          
          VSlNo        .removeAllElements();
          VMrsSlNo     .removeAllElements();
          VMrsUserCode .removeAllElements();
          VOrderSlNo   .removeAllElements();
          VDeptCode    .removeAllElements();
          VGroupCode   .removeAllElements();
          VUnitCode    .removeAllElements();
          VTaxC        .removeAllElements();
          
          try
          {
               Statement       stat          =  theConnection.createStatement();
               
               String QString = getQString();
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    String str1  = res.getString(1);  
                    String str2  = res.getString(2);
                    String str3  = res.getString(3);
                    String str4  = res.getString(4);
                    String str5  = res.getString(5);
                    String str6  = res.getString(6);
                    String str7  = res.getString(7);
                    String str8  = res.getString(8);
                    String str9  = res.getString(9);
                    String str10 = res.getString(10);
                    String str11 = res.getString(11);
                    String str12 = res.getString(12);
                    String str13 = res.getString(13);
                    String str14 = res.getString(14);
                    String str15 = res.getString(15);
                    String str16 = res.getString(16);
                    String str17 = res.getString(17);
                    String str18 = res.getString(18);
                    String str19 = res.getString(19);
                    String str20 = res.getString(20);
                    str4         = common.parseDate(str4);
                    
                    VGrnId      . addElement(str1);
                    VBlock      . addElement(str3);
                    VDate       . addElement(str4);
                    VSupName    . addElement(str5);
                    VOrdNo      . addElement(str6);
                    VGrnCode    . addElement(str7);
                    VGrnName    . addElement(str8);
                    VOrdQty     . addElement(str9);
                    VPending    . addElement(str10);
                    VInvQty     . addElement(str11);
                    VRecdQty    . addElement(str12);
                    VPId        . addElement(str13);
                    VOldInvQty  . addElement(str14);
                    VDiscPer    . addElement(str15);
                    VCenvatPer  . addElement(str16);
                    VTaxPer     . addElement(str17);
                    VSurPer     . addElement(str18);
                    VRate       . addElement(str19);
                    VMrsNo      . addElement(str20);
                    
                    VBlockCode  . addElement(res.getString(21));
                    VSupCode    . addElement(res.getString(22));
                    VGiNo       . addElement(res.getString(23));
                    VGiDate     . addElement(common.parseDate(res.getString(24)));
                           
                    VInvNo      . addElement(res.getString(25));
                    VInvDate    . addElement(common.parseDate(res.getString(26)));   
                    VDcNo       . addElement(res.getString(27));
                    VDcDate     . addElement(common.parseDate(res.getString(28)));
                    
                    VSlNo       . addElement(res.getString(29));
                    VMrsSlNo    . addElement(res.getString(30));
                    VOrderSlNo  . addElement(res.getString(31));
                    VDeptCode   . addElement(res.getString(32));
                    VGroupCode  . addElement(res.getString(33));
                    VUnitCode   . addElement(res.getString(34));
                    VTaxC       . addElement(res.getString(35));
                    VMrsUserCode. addElement(res.getString(36));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VGrnId.size()][ColumnData.length];
          for(int i=0;i<VGrnId.size();i++)
          {
               RowData[i][0]  = SGrnNo;
               RowData[i][1]  = (String)VBlock       .elementAt(i);
               RowData[i][2]  = (String)VDate        .elementAt(i);
               RowData[i][3]  = (String)VSupName     .elementAt(i);
               RowData[i][4]  = (String)VGrnCode     .elementAt(i);
               RowData[i][5]  = (String)VGrnName     .elementAt(i);
               RowData[i][6]  = (String)VOrdNo       .elementAt(i);
               RowData[i][7]  = (String)VOrdQty      .elementAt(i);
               RowData[i][8]  = (String)VPending     .elementAt(i);
               RowData[i][9]  = (String)VInvQty      .elementAt(i);
               RowData[i][10] = (String)VRecdQty     .elementAt(i);
               RowData[i][11] = (String)VRecdQty     .elementAt(i);
               RowData[i][12] = "0";
               RowData[i][13] = new Boolean(false);
               RowData[i][14] = "";
               RowData[i][15] = "";
               RowData[i][16] = "";
          }  
     }

     public String getQString()
     {
          String QString  = "";
          
          SGrnNo = (String)VGrnNo.elementAt(JCGrn.getSelectedIndex());
          
          QString = " SELECT GRN.Id, GRN.GrnNo, OrdBlock.BlockName, "+
                    " GRN.GrnDate, "+SSupTable+".Name, GRN.OrderNo, GRN.Code, "+
                    " InvItems.Item_Name, Sum(GRN.OrderQty) AS Expr1, "+
                    " Sum(GRN.Pending) AS Expr2, Sum(GRN.InvQty) AS SumOfInvQty, "+
                    " Sum(GRN.MillQty) AS Expr3, PurchaseOrder.ID, "+
                    " Sum(PurchaseOrder.InvQty-GRN.MillQty) as OldInvQty, "+
                    " GRN.DiscPer,GRN.CenvatPer,GRN.TaxPer,"+
                    " GRN.SurPer,GRN.InvRate,GRN.MrsNo ,Grn.GrnBlock ,Grn.Sup_Code,"+
                    " Grn.GateInNo,Grn.GateInDate,Grn.InvNo,Grn.InvDate,Grn.DcNo,Grn.DcDate,"+
                    " Grn.SlNo,Grn.MrsSlno,Grn.OrderSlNo,Grn.Dept_Code,Grn.Group_Code,"+
                    " Grn.Unit_Code,Grn.TaxClaimable,Grn.MrsAuthUserCode "+
                    " FROM (((GRN INNER JOIN "+SSupTable+" ON GRN.Sup_Code="+SSupTable+".Ac_Code "+
                    " and Grn.Inspection=0 and Grn.GrnNo="+SGrnNo+" And GRN.MillCode="+iMillCode+") "+
                    " INNER JOIN InvItems ON GRN.Code=InvItems.Item_Code and InvItems.QualityCheck=0) "+ //StkGroupCode!='A08'
                    " INNER JOIN OrdBlock ON GRN.GrnBlock=OrdBlock.Block ) "+
                    " LEFT JOIN PurchaseOrder ON (GRN.OrderQty=PurchaseOrder.Qty) "+
                    " AND (GRN.Code=PurchaseOrder.Item_Code) "+
                    " AND (GRN.OrderNo=PurchaseOrder.OrderNo) "+
                    " AND (GRN.OrderSlNo=PurchaseOrder.SlNo) "+
                    " GROUP BY GRN.Id, GRN.GrnNo, OrdBlock.BlockName, "+
                    " GRN.GrnDate, "+SSupTable+".Name, GRN.OrderNo, GRN.Code, "+
                    " InvItems.Item_Name, PurchaseOrder.ID,GRN.DiscPer, "+
                    " GRN.CenvatPer,GRN.TaxPer,GRN.SurPer,GRN.InvRate,GRN.MrsNo,Grn.GrnBlock ,Grn.Sup_Code,Grn.GateInNo,"+
                    " Grn.GateInDate,Grn.InvNo,Grn.InvDate,Grn.DcNo,Grn.DcDate,Grn.SlNo,Grn.MrsSlno,Grn.OrderSlNo, "+
                    " Grn.Dept_Code,Grn.Group_Code,Grn.Unit_Code,Grn.TaxClaimable,Grn.MrsAuthUserCode "+
                    " Order by 1 ";

          return QString;
     }
     
     public void getRejectionNo()
     {
          String QS ="";
          try
          {
               QS = "  Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 4";
               
               Statement       stat =  theConnection.createStatement();
               
               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    SRejNo = res.getString(1);    
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E1"+ex );
          }
     }

     public void insertInspection()
     {
          try
          {                                                          
               String QString = " Insert into Grn (GrnNo,GrnBlock,GrnDate,Sup_Code,GateInNo,GateInDate,InvNo,InvDate,DcNo,DcDate,MrsNo,OrderNo,OrderQty,Pending,InvQty,InvRate,DiscPer,Disc,CenvatPer,CenVat,TaxPer,Tax,SurPer,Sur,InvAmount,Inspection,MillQty,RejQty,SlNo,MrsSlno,MrsAuthUserCode,OrderSlno,RejFlag,RejDate,Id,Code,InvNet,Dept_Code,Group_Code,Unit_Code,Qty,MillCode,UserCode,CreationDate,TaxClaimable,RejNo,GrnQty,GrnValue,Reason,NRDCNo,NRDCDate) values (";
               
               Statement           stat           = theConnection.createStatement();
               
               for(int i=0;i<VGrnId.size();i++)
               {
                    double dRate=0,dDiscPer=0,dCenvatPer=0,dTaxPer=0,dSurPer=0;
                    double dDisc=0,dCenvat=0,dTax=0,dSur=0,dGross=0,dNet=0;
                    double dMillQty=0,dRejQty=0,dAccQty=0;
                    
                    dRejQty  = common.toDouble(((String)RowData[i][12]).trim());
                    
                    if(dRejQty==0)
                         continue;
                    
                    dRate      = common.toDouble((String)VRate.elementAt(i));
                    dDiscPer   = common.toDouble((String)VDiscPer.elementAt(i));
                    dCenvatPer = common.toDouble((String)VCenvatPer.elementAt(i));
                    dTaxPer    = common.toDouble((String)VTaxPer.elementAt(i));
                    dSurPer    = common.toDouble((String)VSurPer.elementAt(i));
                    
                    double dQty = dRejQty * (-1);
                    
                    dGross  = dQty*dRate;
                    dDisc   = dGross*dDiscPer/100;
                    dCenvat = (dGross-dDisc)*dCenvatPer/100;
                    dTax    = (dGross-dDisc+dCenvat)*dTaxPer/100;
                    dSur    = (dTax)*dSurPer/100;
                    dNet    = dGross-dDisc+dCenvat+dTax+dSur;
                    
                    String STaxC     = common.parseNull((String)VTaxC.elementAt(i));
                    
                    String SGrnQty = common.getRound(dQty,3);
                    
                    String SGrnValue = "";
                    
                    /*if(STaxC.equals("0"))
                         SGrnValue = common.getRound(dNet,2);
                    else
                         SGrnValue = common.getRound((dNet-dTax),2);*/
                    
                    SGrnValue = common.getRound(dNet,2);

                    String SReason   = ((String)RowData[i][14]).trim();
                    String SNRDCNo   = ((String)RowData[i][15]).trim();
                    String SNRDCDate = common.pureDate(((String)RowData[i][16]).trim());
                    
                    String SItemCode    = common.parseNull((String)VGrnCode.elementAt(i));
                    String SBlockCode   = common.parseNull((String)VBlockCode.elementAt(i));
                    String SOrderNo     = common.parseNull((String)VOrdNo.elementAt(i));
                    String SOrdSlNo     = common.parseNull((String)VOrderSlNo.elementAt(i));
                    String SMrsUserCode = common.parseNull((String)VMrsUserCode.elementAt(i));
                    
                    String    QS=QString;
                              QS=QS+"0"+SGrnNo+",";
                              QS=QS+"0"+SBlockCode+",";
                              QS=QS+"'"+common.pureDate((String)VDate.elementAt(i))+"',";
                              QS=QS+"'"+common.parseNull((String)VSupCode.elementAt(i))+"',";
                              QS=QS+"0"+common.parseNull((String)VGiNo.elementAt(i))+",";
                              QS=QS+"'"+common.pureDate((String)VGiDate.elementAt(i))+"',";
                              QS=QS+"'"+common.parseNull((String)VInvNo.elementAt(i))+"',";
                              QS=QS+"'"+common.pureDate((String)VInvDate.elementAt(i))+"',";
                              QS=QS+"'"+common.parseNull((String)VDcNo.elementAt(i))+"',";
                              QS=QS+"'"+common.pureDate(common.parseNull((String)VDcDate.elementAt(i)))+"',";
                              QS=QS+"0"+common.parseNull((String)VMrsNo.elementAt(i))+",";
                              QS=QS+"0"+SOrderNo+",";
                              QS=QS+"0"+common.parseNull((String)VOrdQty.elementAt(i))+",";
                              QS=QS+"0"+common.parseNull((String)VPending.elementAt(i))+",";
                              QS=QS+"0"+common.parseNull((String)VInvQty.elementAt(i))+",";
                              QS=QS+"0"+common.parseNull((String)VRate.elementAt(i))+",";
                              QS=QS+"0"+common.getRound(dDiscPer,2)+",";
                              QS=QS+"0"+common.getRound(dDisc,2)+",";
                              QS=QS+"0"+common.getRound(dCenvatPer,2)+",";
                              QS=QS+"0"+common.getRound(dCenvat,2)+",";
                              QS=QS+"0"+common.getRound(dTaxPer,2)+",";
                              QS=QS+"0"+common.getRound(dTax,2)+",";
                              QS=QS+"0"+common.getRound(dSurPer,2)+",";
                              QS=QS+"0"+common.getRound(dSur,2)+",";
                              QS=QS+"0"+common.getRound(dNet,2)+",";
                              QS=QS+"0"+"1"+",";//Inspection
                              QS=QS+"0"+common.getRound(dMillQty,3)+",";
                              QS=QS+"0"+common.getRound(dRejQty,3)+",";
                              QS=QS+"0"+common.parseNull((String)VSlNo.elementAt(i))+",";
                              QS=QS+"0"+common.parseNull((String)VMrsSlNo.elementAt(i))+",";
                              QS=QS+"0"+SMrsUserCode+",";
                              QS=QS+"0"+SOrdSlNo+",";
                              QS=QS+"0"+"1"+",";//Rej Flag
                              QS=QS+"'"+common.getServerPureDate()+"',";
                              QS=QS+"0"+getNextId()+",";
                              QS=QS+"'"+SItemCode+"',";
                              QS=QS+"0"+common.getRound(dNet,2)+",";
                              QS=QS+"0"+common.parseNull((String)VDeptCode.elementAt(i))+",";
                              QS=QS+"0"+common.parseNull((String)VGroupCode.elementAt(i))+",";
                              QS=QS+"0"+common.parseNull((String)VUnitCode.elementAt(i))+",";
                              QS=QS+"0"+SGrnQty+",";
                              QS=QS+"0"+iMillCode+",";
                              QS=QS+"0"+iUserCode+",";
                              QS=QS+"'"+common.getServerDateTime()+"',";
                              QS=QS+"0"+STaxC+",";
                              QS=QS+"0"+SRejNo+",";
                              QS=QS+"0"+SGrnQty+",";
                              QS=QS+"0"+SGrnValue+",";
                              QS=QS+"'"+SReason+"',";
                              QS=QS+"0"+SNRDCNo+",";
                              QS=QS+"'"+SNRDCDate+"')";
                    
                    if(theConnection   . getAutoCommit())
                         theConnection   . setAutoCommit(false);

                    stat.execute(QS);
                    
                    updatePurchaseOrder(stat,SOrderNo,SOrdSlNo,SItemCode,SGrnQty);
                    updateItemMaster(stat,SItemCode,SBlockCode,SGrnQty,SGrnValue);

                    if(common.toInt(SBlockCode)<=1)
                    {
                         updateUserItemStock(stat,SItemCode,SGrnQty,SGrnValue,SMrsUserCode);
                    }

                    bFlag=true;
               }
	       
		if(iMillCode==0)
		stat.execute("Update Grn Set Inspection=1 Where MillCode="+iMillCode+" and RejFlag=0 and GrnNo="+SGrnNo+" and Grn.Code in (select Code from grn inner join invitems on grn.code=invitems.item_code Where grn.MillCode="+iMillCode+" and grn.RejFlag=0 and grn.GrnNo="+SGrnNo+" and invitems.QualityCheck=0) ");//stkgroupcode!='A08'
		else
                stat.execute("Update Grn Set Inspection=1 Where MillCode="+iMillCode+" and RejFlag=0 and GrnNo="+SGrnNo);
               
               if(bFlag)
                    updateConfig(stat);
               
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println(e);
               bComflag  = false;
          }
     }

     public void updatePurchaseOrder(Statement stat,String SOrderNo,String SOrdSlNo,String SItemCode,String SGrnQty)
     {
          try
          {
               String    QString = "Update PurchaseOrder Set ";
                         QString = QString+" InvQty  = InvQty"+SGrnQty;
                         QString = QString+" Where MillCode="+iMillCode+" and Item_Code='"+SItemCode+"' And OrderNo="+SOrderNo+" and SlNo="+SOrdSlNo;

               if(theConnection   . getAutoCommit())
                    theConnection   . setAutoCommit(false);

               stat.execute(QString);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag  = false;
          }
     }

     public void updateItemMaster(Statement stat,String SItemCode,String SBlockCode,String SGrnQty,String SGrnValue)
     {
          try
          {
               String QS = "";

               int iBlockCode = common.toInt(SBlockCode);

               if(iBlockCode>1)
               {
                    QS = "Update "+SItemTable+" Set ";
                    QS = QS+" RecVal =RecVal+"+SGrnValue+",";
                    QS = QS+" RecQty=RecQty+"+SGrnQty+",";
                    QS = QS+" IssQty=IssQty+"+SGrnQty+",";
                    QS = QS+" IssVal=IssVal+"+SGrnValue+" ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";
               }
               else
               {
                    QS = "Update "+SItemTable+" Set ";
                    QS = QS+" RecVal =RecVal+"+SGrnValue+",";
                    QS = QS+" RecQty=RecQty+"+SGrnQty+" ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";
               }

               if(theConnection   . getAutoCommit())
                    theConnection   . setAutoCommit(false);

               stat.execute(QS);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag  = false;
          }
     }

     private void updateUserItemStock(Statement stat,String SItemCode,String SGrnQty,String SGrnValue,String SMrsUserCode)
     {
          try
          {
               Item IC = new Item(SItemCode,iMillCode,SItemTable,SSupTable);
     
               double dAllStock = common.toDouble(IC.getClStock());
               double dAllValue = common.toDouble(IC.getClValue());
     
               double dRate   = 0;
               try
               {
                    dRate = dAllValue/dAllStock;
               }
               catch(Exception ex)
               {
                    dRate=0;
               }
     
               if(dAllStock==0)
               {
                    dRate = common.toDouble(IC.SRate);
               }

               int iCount=0;

               String QS = " Select count(*) from ItemStock Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+iMillCode;
               
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    iCount   =    res.getInt(1);
               }
               res.close();

               String QS1 = "";

               if(iCount>0)
               {
                    QS1 = "Update ItemStock Set Stock=nvl(Stock,0)+"+SGrnQty+" ";
                    QS1 = QS1+" Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+iMillCode;
               }
               else
               {
                    QS1 = " Insert into ItemStock (MillCode,HodCode,ItemCode,Stock,StockValue) Values (";
                    QS1 = QS1+"0"+iMillCode+",";
                    QS1 = QS1+"0"+SMrsUserCode+",";
                    QS1 = QS1+"'"+SItemCode+"',";
                    QS1 = QS1+"0"+SGrnQty+",";
                    QS1 = QS1+"0"+common.getRound(dRate,4)+")";
               }

               String QS2 = " Update ItemStock set StockValue="+common.getRound(dRate,4)+
                            " Where ItemCode='"+SItemCode+"' and MillCode="+iMillCode;


               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);

               stat.execute(QS1);
               stat.execute(QS2);
          }
          catch(Exception e)
          {
               System.out.println("E7"+e);
               bComflag  = false;
          }
     }

     public void updateConfig(Statement stat)
     {
          try
          {
               String QS = "";

               QS = " Update config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1  where Id = 4";

               if(theConnection   . getAutoCommit())
                    theConnection   . setAutoCommit(false);

               stat.execute(QS);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag  = false;
          }
     }

     private int  getNextId()
     {
          int   id = 0;

          try
          {
               Statement      stat           =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery("Select Grn_seq.nextval from Dual");
               while (res.next())
               {
                    id = res.getInt(1);
               }
               res  . close();
               stat . close();
          }
          catch(Exception e)
          {
               System.out.println();
          }
          return id ;
     }

     public void getGrnData()
     {
          VGrnNo = new Vector();

          try
          {
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet res  = stat.executeQuery("Select distinct GrnNo from Grn Where Inspection=0 and GstStatus=1 and MillCode="+iMillCode+" order by GrnNo");
               
               while (res.next())
               {
                    VGrnNo.addElement(res.getString(1));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnection  . commit();
                    JOptionPane    . showMessageDialog(null,"The Entered Data is Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("Commit");
               }
               else
               {
                    theConnection  . rollback();
                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theConnection   . setAutoCommit(true);

          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
