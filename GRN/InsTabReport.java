package GRN;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import util.*;
import jdbc.*;
import guiutil.*;

public class InsTabReport extends JPanel
{
   JTable         ReportTable;
   Object         RowData[][];
   String         ColumnData[],ColumnType[];
   boolean        Flag;
   JPanel         thePanel;
   Common common = new Common();
   InsTabReport(Object RowData[][],String ColumnData[],String ColumnType[])
   {
         this.RowData     = RowData;
         this.ColumnData  = ColumnData;
         this.ColumnType  = ColumnType;
         thePanel         = new JPanel();
         setReportTable();
   }
   public void setReportTable()
   {
         AbstractTableModel dataModel = new AbstractTableModel()
         {
               public int getColumnCount(){return ColumnData.length;}
               public int getRowCount(){return RowData.length;}
               public Object getValueAt(int row,int col){return RowData[row][col];}
               public String getColumnName(int col){ return ColumnData[col];}
               public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }
               public boolean isCellEditable(int row,int col)
               {
                    if(ColumnType[col]=="B" || ColumnType[col]=="E")
                         return true;
                    return false;
               }
               public void setValueAt(Object element,int row,int col)
               {
                    RowData[row][col]=element;
                    if(col>=10 && col<=12)
                          setInsValue(row,col);
               }
         };
         ReportTable  = new JTable(dataModel);
         ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<ReportTable.getColumnCount();col++)
         {
               if(ColumnType[col]=="N" || ColumnType[col]=="E") 
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
         }
         ReportTable.setShowGrid(false);
         setLayout(new BorderLayout());
         thePanel.setLayout(new BorderLayout());
         thePanel.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
         thePanel.add(new JScrollPane(ReportTable),BorderLayout.CENTER);
         add("Center",thePanel);
   }
   public void setInsValue(int row,int col)
   {
      for(int i=0;i<RowData.length;i++)
      {
             double dRecQty = common.toDouble(((String)RowData[i][10]).trim());
             double dAccQty = common.toDouble(((String)RowData[i][11]).trim());
             double dRejQty = dRecQty-dAccQty;
             RowData[i][12] = String.valueOf(dRejQty);
      }
      ReportTable.updateUI();
   }


}

