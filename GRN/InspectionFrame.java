package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class InspectionFrame extends JInternalFrame
{
     JComboBox      JCOrder;
     JButton        BApply,BOk;
     JPanel         TopPanel,MiddlePanel,BottomPanel;
     
     TabReport      tabreport;
     Connection     theConnection = null;
     
     Object         RowData[][];
     
     String         ColumnData[] = {"GRN No","Block","Date","Supplier","Code","Name","Order No","Order Qty","Pending Qty","DC/Inv Qty","Recd Qty","Accepted","Rejection","Over"};
     String         ColumnType[] = {"N"     ,"S"    ,"S"   ,"S"       ,"S"   ,"S"   ,"N"       ,"N"        ,"N"          ,"N"         ,"N"       ,"N"       ,"N"        ,"B"   };
     
     Common         common = new Common();
     
     Vector         VGrnId,VGrnNo,VBlock,VDate,VSupName,VOrdNo,VGrnCode,VGrnName,VOrdQty,VPending,VInvQty,VRecdQty,VPId,VOldInvQty;
     Vector         VBlockCode,VSupCode,VGiNo,VGiDate,VInvNo,VInvDate,VDcNo,VDcDate,VSlNo,VMrsSlNo,VOrderSlNo;
     Vector         VRate,VDiscPer,VCenvatPer,VTaxPer,VSurPer,VMrsNo;
     Vector         VDeptCode,VGroupCode,VUnitCode,VTaxC;
     
     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     Vector         VCode,VName;
     int            iMillCode,iUserCode;
     String         SSupTable;
     
     String         SGrnNo="";
     
     boolean        bComflag  = true;

     public InspectionFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,int iUserCode,String SSupTable)
     {
          super("Materials Pending for Inspection");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.VCode     = VCode;
          this.VName     = VName;
          this.iMillCode = iMillCode;
          this.iUserCode = iUserCode;
          this.SSupTable = SSupTable;
     

          ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                          theConnection =  oraConnection.getConnection();

          createVectors();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          BApply      = new JButton("Apply");
          BOk         = new JButton("Update");
          TopPanel    = new JPanel();
          MiddlePanel = new JPanel();
          BottomPanel = new JPanel();
          JCOrder     = new JComboBox();
     }

     public void setLayouts()
     {
          TopPanel       . setLayout(new FlowLayout(0,0,0));
          MiddlePanel    . setLayout(new BorderLayout());
          
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }

     public void addComponents()
     {
          JCOrder.addItem("GRN No");
          JCOrder.addItem("Supplierwise");
          TopPanel.add(new JLabel("Sorted On"));
          TopPanel.add(JCOrder);
          TopPanel.add(BApply);
          BottomPanel.add(BOk);
          
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     
     public void addListeners()
     {
          BApply    . addActionListener(new ApplyList());
          BOk       . addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk  . setEnabled(false);
               updateInspection();
               getACommit();
               removeHelpFrame();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     
     public void createVectors()
     {
          VGrnId       = new Vector();
          VGrnNo       = new Vector();  
          VBlock       = new Vector();
          VDate        = new Vector();
          VSupName     = new Vector();
          VOrdNo       = new Vector();
          VGrnCode     = new Vector();
          VGrnName     = new Vector();
          VOrdQty      = new Vector();
          VPending     = new Vector();
          VInvQty      = new Vector();
          VRecdQty     = new Vector();
          VPId         = new Vector();
          VOldInvQty   = new Vector();
          VDiscPer     = new Vector();
          VCenvatPer   = new Vector();
          VTaxPer      = new Vector();
          VSurPer      = new Vector();
          VRate        = new Vector();
          VMrsNo       = new Vector();
          
          VBlockCode   = new Vector();
          VSupCode     = new Vector();
          VGiNo        = new Vector();
          VGiDate      = new Vector();
          
          VInvNo       = new Vector();
          VInvDate     = new Vector();
          VDcNo        = new Vector();
          VDcDate      = new Vector();
          
          VSlNo        = new Vector();
          VMrsSlNo     = new Vector();
          VOrderSlNo   = new Vector();
          
          VDeptCode    = new Vector();
          VGroupCode   = new Vector();
          VUnitCode    = new Vector();
          VTaxC        = new Vector();
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BApply.setEnabled(false);
               try
               {
                    MiddlePanel.remove(tabreport);
                    MiddlePanel.updateUI();
               }
               catch(Exception ex){}
               setDataIntoVector();
               setRowData();
               try
               {
                    tabreport = new TabReport(RowData,ColumnData,ColumnType);
                    MiddlePanel.add(tabreport,BorderLayout.CENTER);
                    MiddlePanel.updateUI();
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
               }
          }
     }

     public void setDataIntoVector()
     {
          VGrnId       .removeAllElements();
          VGrnNo       .removeAllElements();
          VBlock       .removeAllElements();
          VDate        .removeAllElements();
          VSupName     .removeAllElements();
          VOrdNo       .removeAllElements();
          VGrnCode     .removeAllElements();
          VGrnName     .removeAllElements();
          VOrdQty      .removeAllElements();
          VPending     .removeAllElements();
          VInvQty      .removeAllElements();
          VRecdQty     .removeAllElements();
          VPId         .removeAllElements();
          VOldInvQty   .removeAllElements();
          VDiscPer     .removeAllElements();
          VCenvatPer   .removeAllElements();
          VTaxPer      .removeAllElements();
          VSurPer      .removeAllElements();
          VRate        .removeAllElements();
          VMrsNo       .removeAllElements();
          
          VBlockCode   .removeAllElements();
          VSupCode     .removeAllElements();
          VGiNo        .removeAllElements();
          VGiDate      .removeAllElements();
          
          VInvNo       .removeAllElements();
          VInvDate     .removeAllElements();
          VDcNo        .removeAllElements();
          VDcDate      .removeAllElements();
          
          VSlNo        .removeAllElements();
          VMrsSlNo     .removeAllElements();
          VOrderSlNo   .removeAllElements();
          VDeptCode    .removeAllElements();
          VGroupCode   .removeAllElements();
          VUnitCode    .removeAllElements();
          VTaxC        .removeAllElements();
          
          try
          {
              /* ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                               theConnection =  oraConnection.getConnection();*/
               Statement       stat          =  theConnection.createStatement();
               
               String QString = getQString();
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    String str1  = res.getString(1);  
                    String str2  = res.getString(2);
                    String str3  = res.getString(3);
                    String str4  = res.getString(4);
                    String str5  = res.getString(5);
                    String str6  = res.getString(6);
                    String str7  = res.getString(7);
                    String str8  = res.getString(8);
                    String str9  = res.getString(9);
                    String str10 = res.getString(10);
                    String str11 = res.getString(11);
                    String str12 = res.getString(12);
                    String str13 = res.getString(13);
                    String str14 = res.getString(14);
                    String str15 = res.getString(15);
                    String str16 = res.getString(16);
                    String str17 = res.getString(17);
                    String str18 = res.getString(18);
                    String str19 = res.getString(19);
                    String str20 = res.getString(20);
                    str4         = common.parseDate(str4);
                    
                    VGrnId    . addElement(str1);
                    VGrnNo    . addElement(str2);
                    VBlock    . addElement(str3);
                    VDate     . addElement(str4);
                    VSupName  . addElement(str5);
                    VOrdNo    . addElement(str6);
                    VGrnCode  . addElement(str7);
                    VGrnName  . addElement(str8);
                    VOrdQty   . addElement(str9);
                    VPending  . addElement(str10);
                    VInvQty   . addElement(str11);
                    VRecdQty  . addElement(str12);
                    VPId      . addElement(str13);
                    VOldInvQty. addElement(str14);
                    VDiscPer  . addElement(str15);
                    VCenvatPer. addElement(str16);
                    VTaxPer   . addElement(str17);
                    VSurPer   . addElement(str18);
                    VRate     . addElement(str19);
                    VMrsNo    . addElement(str20);
                    
                    VBlockCode. addElement(res.getString(21));
                    VSupCode  . addElement(res.getString(22));
                    VGiNo     . addElement(res.getString(23));
                    VGiDate   . addElement(common.parseDate(res.getString(24)));
                    
                    VInvNo    . addElement(res.getString(25));
                    VInvDate  . addElement(common.parseDate(res.getString(26)));   
                    VDcNo     . addElement(res.getString(27));
                    VDcDate   . addElement(common.parseDate(res.getString(28)));
                    
                    VSlNo     . addElement(res.getString(29));
                    VMrsSlNo  . addElement(res.getString(30));
                    VOrderSlNo. addElement(res.getString(31));
                    VDeptCode . addElement(res.getString(32));
                    VGroupCode. addElement(res.getString(33));
                    VUnitCode . addElement(res.getString(34));
                    VTaxC     . addElement(res.getString(35));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VGrnId.size()][ColumnData.length];
          for(int i=0;i<VGrnId.size();i++)
          {
               RowData[i][0]  = (String)VGrnNo       .elementAt(i);
               RowData[i][1]  = (String)VBlock       .elementAt(i);
               RowData[i][2]  = (String)VDate        .elementAt(i);
               RowData[i][3]  = (String)VSupName     .elementAt(i);
               RowData[i][4]  = (String)VGrnCode     .elementAt(i);
               RowData[i][5]  = (String)VGrnName     .elementAt(i);
               RowData[i][6]  = (String)VOrdNo       .elementAt(i);
               RowData[i][7]  = (String)VOrdQty      .elementAt(i);
               RowData[i][8]  = (String)VPending     .elementAt(i);
               RowData[i][9]  = (String)VInvQty      .elementAt(i);
               RowData[i][10] = (String)VRecdQty     .elementAt(i);
               RowData[i][11] = (String)VRecdQty     .elementAt(i);
               RowData[i][12] = "0";
               RowData[i][13] = new Boolean(true);
          }  
     }

     public String getQString()
     {
          String QString  = "";
          
          QString = " SELECT GRN.Id, GRN.GrnNo, OrdBlock.BlockName, "+
                    " GRN.GrnDate, "+SSupTable+".Name, GRN.OrderNo, GRN.Code, "+
                    " InvItems.Item_Name, Sum(GRN.OrderQty) AS Expr1, "+
                    " Sum(GRN.Pending) AS Expr2, Sum(GRN.InvQty) AS SumOfInvQty, "+
                    " Sum(GRN.MillQty) AS Expr3, PurchaseOrder.ID, "+
                    " Sum(PurchaseOrder.InvQty-GRN.MillQty) as OldInvQty, "+
                    " GRN.DiscPer,GRN.CenvatPer,GRN.TaxPer,"+
                    " GRN.SurPer,GRN.InvRate,GRN.MrsNo ,Grn.GrnBlock ,Grn.Sup_Code,"+
                    " Grn.GateInNo,Grn.GateInDate,Grn.InvNo,Grn.InvDate,Grn.DcNo,Grn.DcDate,"+
                    " Grn.SlNo,Grn.MrsSlno,Grn.OrderSlNo,Grn.Dept_Code,Grn.Group_Code,"+
                    " Grn.Unit_Code,Grn.TaxClaimable "+
                    " FROM (((GRN INNER JOIN "+SSupTable+" ON GRN.Sup_Code="+SSupTable+".Ac_Code "+
                    " and Grn.Inspection=0 And GRN.MillCode="+iMillCode+") "+
                    " INNER JOIN InvItems ON GRN.Code=InvItems.Item_Code  ";
if(iMillCode==0)
	QString = QString + " and InvItems.QualityCheck = 0 ";

         QString = QString +   " )";   

        QString = QString +   " INNER JOIN OrdBlock ON GRN.GrnBlock=OrdBlock.Block) "+
                    " LEFT JOIN PurchaseOrder ON ((GRN.OrderQty=PurchaseOrder.Qty) "+
                    " AND (GRN.Code=PurchaseOrder.Item_Code) "+
                    " AND (GRN.OrderNo=PurchaseOrder.OrderNo) "+
                    " AND (GRN.OrderSlNo=PurchaseOrder.SlNo)) "+
                    " GROUP BY GRN.Id, GRN.GrnNo, OrdBlock.BlockName, "+
                    " GRN.GrnDate, "+SSupTable+".Name, GRN.OrderNo, GRN.Code, "+
                    " InvItems.Item_Name, PurchaseOrder.ID,GRN.DiscPer, "+
                    " GRN.CenvatPer,GRN.TaxPer,GRN.SurPer,GRN.InvRate,GRN.MrsNo,Grn.GrnBlock ,Grn.Sup_Code,Grn.GateInNo,"+
                    " Grn.GateInDate,Grn.InvNo,Grn.InvDate,Grn.DcNo,Grn.DcDate,Grn.SlNo,Grn.MrsSlno,Grn.OrderSlNo, "+
                    " Grn.Dept_Code,Grn.Group_Code,Grn.Unit_Code,Grn.TaxClaimable ";

          if(JCOrder.getSelectedIndex() == 0)
               QString = QString+" Order By GRN.GrnNo";
          if(JCOrder.getSelectedIndex() == 1)      
               QString = QString+" Order By "+SSupTable+".Name,GRN.GrnNo";

//System.out.println("QS"+QString);

          return QString;
     }
     
     public void updateInspection()
     {
          try
          {                                                          
               /*ORAConnection       oraConnection  = ORAConnection.getORAConnection();
                                   theConnection  = oraConnection.getConnection();*/

               Statement           stat           = theConnection.createStatement();
               
               for(int i=0;i<VGrnId.size();i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][13];
                    if(Bselected.booleanValue())
                    {
                         String QString = "Update GRN Set ";
                         QString = QString+" Inspection  = 1 ,inspectiondate = sysdate, DateTime = to_Char(sysdate,'DD-MON-YYYY:hh:mi:ss') ";
                         QString = QString+" Where id = "+(String)VGrnId.elementAt(i);
                    
                         if(theConnection   . getAutoCommit())
                              theConnection   . setAutoCommit(false);

                         stat.execute(QString);
                    }
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println(e);
               bComflag  = false;
          }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnection  . commit();
                    JOptionPane    . showMessageDialog(null,"The Entered Data is Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("Commit");
               }
               else
               {
                    theConnection  . rollback();
                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theConnection   . setAutoCommit(true);
//               theConnection   . close();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}