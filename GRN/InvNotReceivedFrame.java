
package GRN;
import javax.swing.*;
import java.sql.*;
import util.*;
import jdbc.*;
import guiutil.*;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;
import java.util.HashMap;
import javax.swing.border.TitledBorder;
import java.net.*;
import java.io.*;


public class InvNotReceivedFrame extends JInternalFrame {
    JPanel                          pnlMain, pnlFilter, pnlData, pnlControl;
    JComboBox                       cmbSupplierList;
    JButton                         btnApply, btnExit, btnSave, btnGenerateInvReminder,btnCheck,btnPrint;
    InvNotReceivedModel             theModel;
    JTable                          theTable;
    Connection                      theMConnection = null;
    Connection                      theDConnection = null;
    Common              common      = new Common();
    
    int                             iMillCode                   = 1;
    Vector                          VSupName, VSupCode;
//    PDFCreation                     pdfcreation;
    ArrayList                       AInvNotReceivedList;
    Supplier                        supplier;
    String                          sSupAdd1, sSupAdd2, sSupAdd3,sSupName;
    JLayeredPane		      	  Layer;
    String                          sAuthSysName;
    int                             iUserCode;
    PrnGeneration                   prngeneration=null;
    FileWriter                      FW;   
    DateField                       TStDate,TEnDate;
    JCheckBox                       chkDate;
    PDFCreation			      pdfCreation;	
    String[]   			      SBody;
    String 			            SFile="";	

    int            iOrderNo         = 0;
    int            iOrderDate       = 1;
    int            iName            = 2;
    int            iDCNo            = 3;
    int            iDCDate          = 4;
    int            iInvNo           = 5;
    int            iInvDate         = 6;
    int            iGRNNo           = 7;
    int            iGRNDate         = 8;
    int            iGRNQty          = 9;
    int            iNoofRemainder   = 10;
    int            iSelect          = 11;
    
    
public InvNotReceivedFrame(JLayeredPane	Layer){
         
          this.Layer			= Layer;
          createComponent();
          setLayout();
          addComponent();
          addListener();
          setTableData();

try{
          sAuthSysName=InetAddress.getLocalHost().getHostName();
       }
     catch(Exception e){
          e.printStackTrace();
       }    
                           
}    
    
private void createComponent(){
    pnlMain                         = new JPanel();
    pnlFilter                       = new JPanel();
    pnlData                         = new JPanel();
    pnlControl                      = new JPanel();
    setSupplierDataIntoVector();
    cmbSupplierList                 = new JComboBox(VSupName);
    btnApply                        = new JButton("Apply");
    btnExit                         = new JButton("Exit");
    btnSave                         = new JButton("Save");
    btnCheck                        = new JButton("Check");
    btnPrint                        = new JButton("Create PDF");
    btnGenerateInvReminder          = new JButton("Generate Invoice Reminder");
    theModel                        = new InvNotReceivedModel();
    theTable                        = new JTable(theModel);
    chkDate                         = new JCheckBox();
    chkDate                         . setSelected(true);
    TStDate                         = new DateField();
    TEnDate                         = new DateField();

}   

private void setLayout(){
    pnlMain                            . setLayout(new BorderLayout());
    pnlFilter                          . setBorder(new TitledBorder("Filter"));
    pnlFilter                          . setLayout(new GridLayout(1,7));
    pnlControl                         . setBorder(new TitledBorder("Control"));
    pnlControl                         . setLayout(new FlowLayout());
    
    pnlData                            . setBorder(new TitledBorder("Invoice Not Received Data"));
    pnlData                            . setLayout(new BorderLayout());
    
    this.setSize(1000,600);
    this.setVisible(true);
    this.setMaximizable(true);
    this.setClosable(true);
//    this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    this.setTitle("INVOICE NOT RECEIVED DATA");
}
private void addListener(){
    btnApply                           . addActionListener(new ActList());
    btnSave                            . addActionListener(new ActList());
    btnGenerateInvReminder             . addActionListener(new ActList());
    btnCheck                           . addActionListener(new ActList());
    btnPrint                           . addActionListener(new ActList());

}

private void addComponent(){

    pnlFilter                      . add(new JLabel("AsonDate"));
    pnlFilter                      . add(chkDate);
    pnlFilter                      . add(new JLabel("Enter To Date"));
    pnlFilter                      . add(TStDate);
   // pnlFilter                      . add(new JLabel("To Date"));
//    pnlFilter                      . add(TEnDate);
    pnlFilter                      . add(btnApply);

    pnlData                        . add(new JScrollPane(theTable));
    pnlControl                     . add(btnSave);
    pnlControl                     . add(btnCheck);
    pnlControl                     . add(btnPrint);
   // pnlControl                     . add(btnGenerateInvReminder);
    pnlMain                        . add(pnlFilter,BorderLayout.NORTH);
    pnlMain                        . add(pnlData,BorderLayout.CENTER);
    pnlMain                        . add(pnlControl,BorderLayout.SOUTH);
    
    this.add(pnlMain);
    
}

private class ActList implements ActionListener{
     public void actionPerformed(ActionEvent ae){
         if (ae.getSource()==btnApply){
         }
     
         if(ae.getSource()==btnSave){
             setSave();
         }
	  if(ae.getSource()==btnCheck){
          setCheck();
         }

	  if(ae.getSource()==btnPrint){
          setPrint();
         }
	  if(ae.getSource()==btnApply){
          setTableData();
         }

     if(ae.getSource()==btnGenerateInvReminder){
         String sSupplierName       = (String)cmbSupplierList.getSelectedItem();
         setSupplierData(sSupplierName);
         String SFile    ="InvoiceReminder.pdf";
         SFile = common.getPrintPath()+SFile;
//         pdfcreation    = new PDFCreation(SFile,sSupplierName,sSupAdd1,sSupAdd2,sSupAdd3);
         
     }
   }
}


 public void setSupplierDataIntoVector()
      {
          VSupName      = new Vector();
          VSupCode      = new Vector();
               VSupName.removeAllElements();
               VSupCode.removeAllElements();
               String QString = "Select Name,Ac_Code From Supplier Order By Name";
               //System.out.println("Qry:"+QString);
               try
               {
                    if(theMConnection == null)
                    {
                         ORAConnection jdbc    = ORAConnection.getORAConnection();
                         theMConnection        = jdbc.getConnection();
                    }

                    Statement theStatement    = theMConnection.createStatement();

                    ResultSet res = theStatement.executeQuery(QString);
                    while(res.next())
                    {
                         VSupName.addElement(res.getString(1));
                         VSupCode.addElement(res.getString(2));
                    }
                    res            . close();
                    theStatement   . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
      }

 public void setInvNotReceivedData(String sStDate,String sEnDate,int iStatus)
{
              AInvNotReceivedList          = new ArrayList();
            
               StringBuffer            sb  = new StringBuffer();
		   String sSDate               = common.getServerDate();
		   String sServerDate		 = sSDate.substring(0, 8);
               
               sb.append(" Select PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,DCNo,DCDate,Name,GRN.Sup_Code,");
               sb.append(" ADDR1,ADDR2,ADDR3,sum(GRNQty),InvNo,InvDate,GRNNo,GRNDate,"); 
		   sb.append(" ( Select Count(*) from InvoiceRemainderMailDC  Where GRNO=GRN.GRNNo  and ");
	         if(iStatus==1) {
  	         sb.append("  EntryDate<="+sServerDate );
        	   }
              else{
          	   sb.append(" EntryDate<= "+sServerDate);
 		   }
		   sb.append(" ) as InvoiceRemainder ");
  //		   sb.append(" (Select (Count(InvoiceRemainderMail.GRNNo)) from InvoiceRemainderMail ");
  //           sb.append(" Where InvoiceRemainderMail.GRNNo=GRN.GRNNo and InvoiceRemainderMail.SupCode=GRN.SUP_CODE  and SendStatus=1) as InvRemainder");
               sb.append(" from GRN"); 
               sb.append(" Inner Join PurchaseOrder on PurchaseOrder.OrderNo=GRN.OrderNo and GRN.Code=PurchaseOrder.Item_Code");
               sb.append(" Inner Join Supplier on Supplier.AC_Code=GRN.Sup_Code");
               sb.append(" Where InvoiceType = 2 and GRN.MillCode=0 ");

	       if(iStatus==1) {
	       sb.append("  and GRN.GRNDate<="+sStDate );
        	}
        
	      else{
        	sb.append(" and GRN.GRNDate<= "+sStDate);

//        	sb.append(" and GRN.GRNDate>= "+sStDate);
//	        sb.append(" and GRN.GRNDate<="+sEnDate);
        
                }
               sb.append(" Group by PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,DCNo,DCDate,Name,GRN.Sup_Code,ADDR1,ADDR2,ADDR3,InvNo,InvDate,GRNNo,GRNDate");
//               sb.append(" Order by DCDate");
               sb.append(" Order by Name");


            System.out.println("Qry:"+sb.toString()); 
               try
               {
                 if(theMConnection ==null){
                         ORAConnection jdbc     = ORAConnection.getORAConnection();
                         theMConnection          = jdbc.getConnection();
                   }
                PreparedStatement  theStatement = theMConnection.prepareStatement(sb.toString());
                ResultSet rst                   = theStatement.executeQuery();
                while(rst.next())
                 {
                    String  sOrderNo            = rst.getString(1)  ;
                    String  sOrderDate          = rst.getString(2)  ;
                    String  sDCNo               = rst.getString(3)  ;
                    String  sDCDate             = rst.getString(4)  ;
                    String  sSupplierName       = rst.getString(5)  ;
                    String  sSupplierCode       = rst.getString(6)  ;
                    String  sAddress1           = rst.getString(7)  ;
                    String  sAddress2           = rst.getString(8)  ;
                    String  sAddress3           = rst.getString(9)  ;
                    String  sGRNQty             = rst.getString(10) ;
                    String  sInvNo              = rst.getString(11) ;
                    String  sInvDate            = rst.getString(12) ;
                    String  sGRNNo              = rst.getString(13) ;
                    String  sGRNDate            = rst.getString(14) ;
                    String  sNoofRemainders     = rst.getString(15) ;

              int iIndex                        = getIndexOf(sSupplierCode,sSupplierName);
              if(iIndex==-1)
              {
                supplier                        = new Supplier(sSupplierCode,sSupplierName);
                AInvNotReceivedList      .add(supplier);
                iIndex= AInvNotReceivedList.size()-1;
              }
              supplier       = (Supplier)AInvNotReceivedList.get(iIndex);
              supplier       . appendDetails(sOrderNo,sOrderDate,sDCNo,sDCDate,sAddress1, sAddress2, sAddress3,sGRNQty,sInvNo,sInvDate,sGRNNo,sGRNDate,sNoofRemainders);
            }
                    rst            . close();
                    theStatement   . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
      }
 
public void setSupplierData(String sSupplierName)
{
               StringBuffer            sb  = new StringBuffer();
               
               sb.append(" Select Name, ADDR1,ADDR2,ADDR3 from Supplier"); 
               sb.append(" Where Name = '"+sSupplierName+"'");
               
          //    System.out.println("Qry:"+sb.toString()); 
               try
               {
                 if(theMConnection ==null){
                         ORAConnection jdbc     = ORAConnection.getORAConnection();
                         theMConnection          = jdbc.getConnection();
                   }
                PreparedStatement  theStatement = theMConnection.prepareStatement(sb.toString());
                ResultSet rst                   = theStatement.executeQuery();
                while(rst.next())
                 {
                    sSupName                    = rst.getString(1)  ;
                    sSupAdd1                    = rst.getString(2)  ;
                    sSupAdd2                    = rst.getString(3)  ;
                    sSupAdd3                    = rst.getString(4)  ;
                 }
                    rst            . close();
                    theStatement   . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
           }
 
 
 
 
 private int getIndexOf(String sSupplierCode, String sSupplierName)
        
 {
       int iIndex=-1;
        for(int i=0; i<AInvNotReceivedList.size(); i++)
        {    
             Supplier  supplier  = (Supplier)AInvNotReceivedList.get(i);
             if(supplier.sSupName.equals(sSupplierName) && supplier.sSupCode.equals(sSupplierCode)) 
                 
             {
                iIndex = i;
                break;
            }
        }
        return iIndex;    
 }

 private void setTableData(){
     theModel.setNumRows(0);
    int iStatus=-1;
  String sStDate            = TStDate.toNormal();
  String sEnDate            = "";
  String sUserCode          = "";
  if(chkDate.isSelected()==true){
      TStDate               . setTodayDate();
//      TEnDate               . setTodayDate();
//      TStDate               . setEditable(false);
  //    TEnDate               . setEditable(false);
      sStDate               = TStDate.toNormal();
     // sEnDate               = TEnDate.toNormal();
     

      iStatus               = 1;   
  }
  else
  iStatus                   = 0;

     setInvNotReceivedData(sStDate,sEnDate,iStatus);

     for(int i=0;i<AInvNotReceivedList.size();i++){
         Supplier           supplier        = (Supplier)AInvNotReceivedList.get(i);
           for(int j=0;j<supplier.AInvNotReceivedList.size();j++){
               HashMap      theMap          = (HashMap)supplier.AInvNotReceivedList.get(j);
               Vector           theVect     = new Vector();
               theVect.addElement(common.parseNull(String.valueOf(theMap.get("OrderNo"))));
               theVect.addElement(common.parseNull(common.parseDate(String.valueOf(theMap.get("OrderDate")))));               
               theVect.addElement(common.parseNull(supplier.sSupName));
               theVect.addElement(common.parseNull(String.valueOf(theMap.get("DCNo"))));
               theVect.addElement(common.parseNull(common.parseDate(String.valueOf(theMap.get("DCDate")))));
               theVect.addElement(common.parseNull(String.valueOf(theMap.get("InvNo"))));
               theVect.addElement(common.parseNull(common.parseDate(String.valueOf(theMap.get("InvDate")))));
               theVect.addElement(common.parseNull(String.valueOf(theMap.get("GRNNo"))));
               theVect.addElement(common.parseNull(common.parseDate(String.valueOf(theMap.get("GRNDate")))));
               theVect.addElement(common.parseNull(String.valueOf(theMap.get("GRNQty"))));
               theVect.addElement(common.parseNull(String.valueOf(theMap.get("NoofRemainder"))));
               theVect.addElement(new Boolean(false));
               theModel.appendRow(theVect);
           }
           
     }
     
 }

 public int updateStatus( String sDCNo, String sOrderNo, String sGRNQty,String sInvNo,int iStatus){
     int iSaveValue=0;
//System.out.println("Status:"+iStatus);
  try{
         StringBuffer sb=new StringBuffer(); 
     if(iStatus==1){    
         sb.append(" Update GRN set InvoiceType=1,InvoiceReceivedDateTime=to_Char(sysdate,'DD-MON-YYYY:hh:mi:ss') ");
         sb.append(" Where DCNo='"+sDCNo+"'"+" and OrderNo='"+sOrderNo+"'");
         }
    else if(iStatus==0){
         sb.append(" Update GRN set InvoiceType=1,InvoiceReceivedDateTime=to_Char(sysdate,'DD-MON-YYYY:hh:mi:ss') ");
         sb.append(" Where InvNo='"+sInvNo+"'"+" and OrderNo='"+sOrderNo+"'");
    }

   //      System.out.println("updateStatus Qry"+sb.toString());
         
             if(theMConnection ==null){
                         ORAConnection jdbc     = ORAConnection.getORAConnection();
                         theMConnection         = jdbc.getConnection();
                   }
             
             PreparedStatement  ps = theMConnection.prepareStatement(sb.toString());

        ResultSet rst=ps.executeQuery();
            rst.close();
            ps.close();
            ps=null;
            rst=null;
            theMConnection=null;
        
      }    
      catch(Exception e)
      {
          iSaveValue=1;
          System.out.println("updateStatus");
          e.printStackTrace();
      }
  return iSaveValue;
 }
 
 private void setSave(){
     int iSave=-1;
     int iStatus=-1;
     try{
         
         for(int i=0;i<theModel.getRowCount();i++){
             Boolean isSelected      = ( Boolean)theModel.getValueAt(i,iSelect);
             if(isSelected.booleanValue()){
             String sDCNo            = (String) theModel.getValueAt(i,iDCNo);
             String sOrderNo         = (String) theModel.getValueAt(i,iOrderNo);
             String sGRNQty          = (String) theModel.getValueAt(i,iGRNQty);
             String sInvNo           = (String) theModel.getValueAt(i,iInvNo);
	      if(!sDCNo.isEmpty()){
		  iStatus=1;	
	      }
		else{
		  iStatus=0;
		}
		
             iSave                   = updateStatus(sDCNo, sOrderNo,sGRNQty,sInvNo,iStatus);
         }
      }
   setTableData();
   if(iSave==0){
                  JOptionPane.showMessageDialog(null,"Data Saved Sucessefully ","Information",JOptionPane.INFORMATION_MESSAGE);
        }
   if(iSave==1){
                  JOptionPane.showMessageDialog(null,"Data not Save ","Information",JOptionPane.INFORMATION_MESSAGE);
   }
 }
    catch(Exception ex){
        ex.printStackTrace();
        System.out.println("set Save:"+ex);
    }
 }

private void setCheck(){
     int                iNoofInvoice, iSaveValue=-1;
     iNoofInvoice       = theModel.getRowCount();
if(iNoofInvoice>0){
     iSaveValue         = setInsertCheckData(iUserCode,sAuthSysName,iNoofInvoice);
}
else{
        JOptionPane.showMessageDialog(null,"There is No item to Check","Information",JOptionPane.INFORMATION_MESSAGE);

}
     if(iSaveValue==0){
        JOptionPane.showMessageDialog(null,"Data Checked, Invoice Not Received From Party is -- "+iNoofInvoice+ "-- ","Information",JOptionPane.INFORMATION_MESSAGE);
     }
 }
 
 
 public int setInsertCheckData (int iUserCode,String sSysName,int iNoofInvoice)
 { 
     int isave=0; 
      if(isave!=2){
     try{
    
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" insert into PendingInvoiceCheck (ID,CheckDate,CheckDateTime,UserCode,SysName,NoofInvoice)");
         sb.append(" values (PendingInv_Seq.nextVal,to_Char(sysdate,'YYYYMMDD'),to_Char(sysdate,'DD-MON-YYYY:hh:mi:ss'),?,?,?)");
         
       //  System.out.println("Qry:"+sb.toString());
         
         if(theMConnection ==null){
                         ORAConnection jdbc     = ORAConnection.getORAConnection();
                         theMConnection         = jdbc.getConnection();
           }
        PreparedStatement ps=theMConnection.prepareStatement(sb.toString());

              ps                    . setInt(1,iUserCode);
              ps                    . setString(2,sAuthSysName);
              ps                    . setInt(3,iNoofInvoice);
              ps . executeUpdate();
            //rst.close();
            ps.close();
            ps=null;
      }  
   
      catch(Exception e)
      {
          isave=1;
          e.printStackTrace();
          System.out.println("InsertMethod"+e);
       }
     }
   //}
 /*else{
          isave=1;
      }*/
 return isave;
}

 	private void setPrint(){
	String[]    SBody;
		    int rowcount=theModel.getRowCount();
			System.out.println("RowCount:"+rowcount);
                  try{
            //    **            SFile    ="D:/Reports/Invoice NotReceived List.pdf";
            			  SFile    ="D:Invoice NotReceived List.pdf";
                                initPrn();
                        }
                        catch(Exception ex){
                            System.out.println("PrnCreation"+ex);
                            ex.printStackTrace();
				}
  
	}


	private void initPrn()
     {
	  String sFromDate    = TStDate.toNormal();
	  String sToDate      = TEnDate.toNormal();

	 int iStatus=-1;
	  if(chkDate.isSelected()==true){
          iStatus               = 1;   
          }
	  else
        iStatus                 = 0;


	
/*	   String[] SSubTitles   = { "INVOICE NOT RECEIVED LIST FROM : "+ common.parseDate(sFromDate)+"--"+common.parseDate(sToDate)};
	  
//          String[] SSubTitles = { "INVOICE NOT RECEIVED LIST FROM : "+ common.parseDate(sFromDate)+"--"+common.parseDate(sToDate)};
          String SMainTitle   = "AMARJOTHI SPINNING MILLS LIMITED" ;
          String[] SHead1     = {""     ,""         ,""          ,""                    , ""               , ""            , ""         , ""           , ""         };
          String[] SHead2     = {"SL.NO","ORDER NO" ,"ORDER DATE","SUPPLIER NAME "      ,"DC.NO"	     , "DC.DATE"     ,"INV.NO"    ,"INV.DATE"    ,"GRN QTY"   };
          String[] SHead3     = {""     ,""         ,""          ,""                    , ""               , ""            , ""         , ""           , ""         };
          String[] SAlign     = {"C"    ,"L"        ,"L"         ,"L"                   , "L"              , "L"           , "L"        , "L"          , "L"        };
          int[] iLength       = {8      ,10        ,15           ,75                    , 20               , 15            , 15         , 15           , 10         };
          String SPitch       = "P";
          prngeneration         = new PrnGeneration(FW,SMainTitle,SSubTitles,SAlign,iLength,SPitch,SHead1,SHead2,SHead3);*/

	  pdfCreation		= new PDFCreation(SFile, sFromDate, sToDate,AInvNotReceivedList,iStatus);
	  
     }
 
/*public static void main(String args[]){
    InvNotReceivedFrame             frame       = new InvNotReceivedFrame();
}*/
    
    
}
