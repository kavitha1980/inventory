
package GRN;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

public class InvNotReceivedModel extends DefaultTableModel{

String ColumnName[]={"OrderNo","OrderDate","Supplier Name","DCNo","DCDate","INVOICENO","INVOICEDATE","GRNNo","GRNDate","GRNQty","Noof Remainders","Select"};
String  ColumnType[]={"S","S","S","S","S","S","S","S","S","S","S","E"};
int iColumnWidth[] ={40,10,10,20,20,20,20,20,20,20,20,10};


public InvNotReceivedModel(){
             setDataVector(getRowData(),ColumnName);
}

  public Class getColumnClass(int iCol)
    {
    return getValueAt(0,iCol).getClass();
    
    }
  public boolean isCellEditable(int iRow,int iCol)
    {
        if (ColumnType[iCol]=="E"||ColumnType[iCol]=="B")
            
            return true;
            return false;
        
    }
    
  private Object[][]getRowData()
    {
        Object RowData[][]=new Object[1][ColumnName.length];
        
        for(int i=0;i<ColumnName.length;i++)
            RowData[0][i]="";
        return RowData;
    }
    
  public void appendRow(Vector theVect)
    {
        insertRow(getRows(),theVect);
    }
  public int getRows()
    {
        return super.dataVector.size();
    }


    
}
