package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class InwModiFrame extends JInternalFrame
{
     JLayeredPane Layer;
     Vector VSupCode,VSupName,VInwNo,VInwName,VSupCodex,VInwNox;
     TabReport tabreportx;
     Vector VIssStatusx;
     JButton BSupplier,BOk;
     JTextField TSupCode;
     JPanel TopPanel,BottomPanel,ModiPanel;
     JComboBox JCType;
     JTextArea TA;
     JDialog ModiDialog;
     int index=0;
     Common common = new Common();
     int iMillCode;
     int iIssStatus=0;
     String SWhere="";
     String SSupTable;

     InwModiFrame(JLayeredPane Layer,Vector VSupCode,Vector VSupName,Vector VInwNo,Vector VInwName,Vector VSupCodex,Vector VInwNox,TabReport tabreportx,JDialog ModiDialog,int iMillCode,String SWhere,String SSupTable)
     {
          this.Layer      = Layer;
          this.VSupCode   = VSupCode;
          this.VSupName   = VSupName;
          this.VInwNo     = VInwNo;
          this.VInwName   = VInwName;
          this.VSupCodex  = VSupCodex;
          this.VInwNox    = VInwNox;
          this.tabreportx = tabreportx;
          this.ModiDialog = ModiDialog;
          this.iMillCode  = iMillCode;
          this.SWhere     = SWhere;
          this.SSupTable  = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setVisible(false);
     }
     InwModiFrame(JLayeredPane Layer,Vector VSupCode,Vector VSupName,Vector VInwNo,Vector VInwName,Vector VSupCodex,Vector VInwNox,TabReport tabreportx,JDialog ModiDialog,int iMillCode,Vector VIssStatusx,String SWhere,String SSupTable)
     {
          this.Layer      = Layer;
          this.VSupCode   = VSupCode;
          this.VSupName   = VSupName;
          this.VInwNo     = VInwNo;
          this.VInwName   = VInwName;
          this.VSupCodex  = VSupCodex;
          this.VInwNox    = VInwNox;
          this.tabreportx = tabreportx;
          this.ModiDialog = ModiDialog;
          this.iMillCode  = iMillCode;
          this.VIssStatusx= VIssStatusx;
          this.SWhere     = SWhere;
          this.SSupTable  = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setVisible(false);
     }

     private void createComponents()
     {
         BSupplier   = new JButton("Select Supplier");
         BOk         = new JButton("Okay");
         JCType      = new JComboBox(VInwName);
         TA          = new JTextArea(10,70); 
         TSupCode    = new JTextField();
         TopPanel    = new JPanel(true);
         BottomPanel = new JPanel(true);
         ModiPanel      = new JPanel(true);

         index   = tabreportx.ReportTable.getSelectedRow();
         String SSupCode = (String)VSupCodex.elementAt(index);
         String SSupName = "";
         if(SWhere.equals("Others"))
         {
              SSupName   = (String)tabreportx.ReportTable.getValueAt(index,7);
              iIssStatus = common.toInt((String)VIssStatusx.elementAt(index));
         }
         else
         {
              SSupName = (String)tabreportx.ReportTable.getValueAt(index,2);
         }
         int iType       = common.toInt((String)VInwNox.elementAt(index));

         BSupplier.setText(SSupName);
         TSupCode.setText(SSupCode);
         JCType.setSelectedIndex(iType);
         TA.setEditable(false);
         if(iIssStatus>0)
         {
              JCType.setEnabled(false);
              BSupplier.setEnabled(false);
         }
         setDescript();
     }

     private void setLayouts()
     {
         TopPanel.setLayout(new GridLayout(2,2));
         ModiPanel.setLayout(new BorderLayout());
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,350,300);
     }
     private void addComponents()
     {
         TopPanel.add(new JLabel("Supplier"));
         TopPanel.add(BSupplier);
         TopPanel.add(new JLabel("Inward Type"));
         TopPanel.add(JCType);
         BottomPanel.add(BOk);

         getContentPane().add("North",TopPanel);
         getContentPane().add("Center",new JScrollPane(TA));
         getContentPane().add("South",BOk);

         ModiPanel.add("North",TopPanel);
         ModiPanel.add("Center",new JScrollPane(TA));
         ModiPanel.add("South",BOk);
     }
                    
     private void addListeners()
     {
          BSupplier.addActionListener(new SupplierList());
          BOk.addActionListener(new ActList());

          BSupplier.setMnemonic('S');
          BOk.setMnemonic('A');
     }
     
     private class SupplierList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    Frame dummy = new Frame();
                    JDialog theDialog = new JDialog(dummy,"Description Dialog",true);
                    SupplierSearch1 SS = new SupplierSearch1(Layer,TSupCode,VSupName,VSupCode,theDialog,BSupplier,SSupTable);
                    theDialog.getContentPane().add(SS.SupplierPanel);
                    theDialog.setBounds(80,100,450,350);
                    theDialog.setVisible(true);
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
		  }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(false);
               String SSupName = BSupplier.getText();
               String SSupCode = TSupCode.getText();
               String SInwNo   = (String)VInwNo.elementAt(JCType.getSelectedIndex());

               if(SWhere.equals("Others"))
               {
                    tabreportx.ReportTable.setValueAt(new Boolean(true),index,9);
                    tabreportx.ReportTable.setValueAt(SSupName,index,7);
               }
               else
               {
                    tabreportx.ReportTable.setValueAt(new Boolean(true),index,8);
                    tabreportx.ReportTable.setValueAt(SSupName,index,2);
               }

               VSupCodex.setElementAt(SSupCode,index);
               VInwNox.setElementAt(SInwNo,index);

               removeHelpFrame();
               tabreportx.ReportTable.requestFocus();
          }
     }

     private void removeHelpFrame()
     {
          try{ModiDialog.setVisible(false);}catch(Exception e){System.out.println(e);}
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }

     public void setDescript()
     {
          String SGINo = (String)tabreportx.RowData[index][0];
          String QS = "";

          QS = " SELECT GateInward.Item_Name,GateInward.SupQty,GateInward.Id,GateInward.GateQty "+
               " From GateInward "+
               " WHERE GateInward.GINo="+SGINo+" And GrnNo = 0 And GateInward.Authentication=1 "+
               " And trim(GateInward.Item_Code) is Null "+
               " And GateInward.MillCode="+iMillCode+
               " Union All "+
               " SELECT InvItems.Item_Name,GateInward.SupQty,GateInward.Id,GateInward.GateQty "+
               " From GateInward Inner Join InvItems On InvItems.Item_Code = GateInward.Item_Code "+
               " WHERE GateInward.GINo="+SGINo+" And GrnNo = 0 And GateInward.Authentication=1 "+
               " And trim(GateInward.Item_Code) is Not Null "+
               " And GateInward.MillCode="+iMillCode+
               " Order By 1";

          TA.setText("");   
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
               ResultSet res  = stat.executeQuery(QS);

               while (res.next())
               {
                    String str = common.parseNull(res.getString(1));
                    TA.append(str+"\n");
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          } 
     }
}
