package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class IssuedMaterialGRNCollectionFrame extends JInternalFrame
{
     JTextField                    TGINo,TGIDate,TInvDate,TDCDate;
     DateField                     TDate;
     JTextField                    TInvNo,TDCNo,TSupName;
     WholeNumberField              TInvoice;
     IssuedMaterialGRNMiddlePanel  MiddlePanel;
     JPanel                        TopPanel,BottomPanel;
     JButton                       BOk,BCancel;
     
     JLayeredPane                  DeskTop;
     Vector                        VCode,VName;
     StatusPanel                   SPanel;
     String                        SIndex;
     int                           iIndex=0;
     JTable                        ReportTable;
     String                        SSupCode;
     String                        SGId;
     String                        SIndentNo;
     int                           iMillCode,iUserCode;
     String                        SItemTable,SSupTable,SYearCode;
     
     String                        SItemCode = "";
     
     Common                        common   = new Common();
     
     Connection                    theMConnection =    null;
     Connection                    theDConnection =    null;

     boolean                       bComflag  = true;
     
     JLabel                        LGrnNo;
     double                        dIssQty= 0,dIssVal= 0;
     
     public IssuedMaterialGRNCollectionFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,String SIndex,JTable ReportTable,String SSupCode,String SGId,String SIndentNo,int iMillCode,int iUserCode,String SItemTable,String SSupTable,String SYearCode)
     {
          this.DeskTop            = DeskTop;
          this.VCode              = VCode;
          this.VName              = VName;
          this.SPanel             = SPanel;
          this.SIndex             = SIndex;
          this.ReportTable        = ReportTable;
          this.SSupCode           = SSupCode;
          this.SGId               = SGId;
          this.SIndentNo          = SIndentNo;
          this.iMillCode          = iMillCode;
          this.iUserCode          = iUserCode;
          this.SItemTable         = SItemTable;
          this.SSupTable          = SSupTable;
          this.SYearCode          = SYearCode;

          if(iMillCode==1)
          {
               DORAConnection jdbc           = DORAConnection.getORAConnection();
                              theDConnection = jdbc.getConnection();
          }
          ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                         theMConnection = oraConnection.getConnection();

          iIndex = common.toInt(SIndex);
          
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }

     public void createComponents()
     {
          BOk        = new JButton("Okay");
          BCancel    = new JButton("Abort");
          
          TDate       = new DateField();
          TGINo       = new JTextField();
          TGIDate     = new JTextField();
          TInvDate    = new JTextField();
          TDCDate     = new JTextField();
          TDCDate     = new JTextField();
          TInvNo      = new JTextField();
          TDCNo       = new JTextField();
          TSupName    = new JTextField();
          TInvoice    = new WholeNumberField(2);
          LGrnNo      = new JLabel();
          MiddlePanel = new IssuedMaterialGRNMiddlePanel(DeskTop,SSupCode,iMillCode,LGrnNo,SYearCode);
          
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();
          
          TDate     . setTodayDate();
          
          TGINo     . setEditable(false);
          TGIDate   . setEditable(false);
          TSupName  . setEditable(false);
     }

     public void setLayouts()
     {
          setTitle("Invoice Valuation of Issued Materials");
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          
          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(5,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("Gate Inward No"));
          TopPanel.add(TGINo);
          
          TopPanel.add(new JLabel("Gate Inward Date"));
          TopPanel.add(TGIDate);
          
          TopPanel.add(new JLabel("GRN No"));
          TopPanel.add(LGrnNo);
          
          TopPanel.add(new JLabel("GRN Date"));
          TopPanel.add(TDate);
          
          TopPanel.add(new JLabel("Supplier"));
          TopPanel.add(TSupName);
          
          TopPanel.add(new JLabel("No. of Bills"));
          TopPanel.add(TInvoice);
          
          TopPanel.add(new JLabel("Invoice No"));
          TopPanel.add(TInvNo);
          
          TopPanel.add(new JLabel("Invoice Date"));
          TopPanel.add(TInvDate);
          
          TopPanel.add(new JLabel("DC No"));
          TopPanel.add(TDCNo);
          
          TopPanel.add(new JLabel("DC Date"));
          TopPanel.add(TDCDate);
          
          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);
          
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
          
          TGINo          . setText((String)ReportTable.getModel().getValueAt(iIndex,0));
          TGIDate        . setText((String)ReportTable.getModel().getValueAt(iIndex,1));
          TSupName       . setText((String)ReportTable.getModel().getValueAt(iIndex,6));
          TInvNo         . setText((String)ReportTable.getModel().getValueAt(iIndex,2));
          TInvDate       . setText((String)ReportTable.getModel().getValueAt(iIndex,3));
          TDCNo          . setText((String)ReportTable.getModel().getValueAt(iIndex,4));
          TDCDate        . setText((String)ReportTable.getModel().getValueAt(iIndex,5));
          SItemCode      = (String)ReportTable.getModel().getValueAt(iIndex,7);
          MiddlePanel    . createComponents(SGId,SItemCode);
     }

     public void addListeners()
     {
          BOk.addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validateGRN())
                    insertGRNCollectedData();
          }
     }

     private void insertGRNCollectedData()
     {
          BOk.setEnabled(false);
          MiddlePanel.setGRNNo();
          insertGRNDetails();
          insertIssueDetails();
          setGILink();
          setOrderLink();
          setMRSLink();
          if(iMillCode==1)
          {
               updateSubStoreMasterData();
          }
          getACommit();
          removeHelpFrame();
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop. remove(this);
               DeskTop. repaint();
               DeskTop. updateUI();
          }
          catch(Exception ex) { }
     }

     public void setGILink()
     {
          try
          {
               Statement       stat =  theMConnection.createStatement();

               for(int i=0;i<MiddlePanel.RD.length;i++)
               {
                    Boolean BValue = (Boolean)MiddlePanel.RD[i][3];
                    if(BValue.booleanValue())
                    {
                         String    QS = "Update GateInward Set ";
                                   QS = QS+" GrnNo=1";
                                   QS = QS+" Where Id = "+(String)MiddlePanel.VGId.elementAt(i); 

                         if(theMConnection  . getAutoCommit())
                              theMConnection  . setAutoCommit(false);

                         stat.execute(QS);
                    }
                    stat.close();
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag  = false;
          }
     }

     public void insertGRNDetails()
     {
          String QString = "Insert Into GRN (OrderNo,MRSNo,GrnNo,GrnDate,GrnBlock,Sup_Code,GateInNo,GateInDate,InvNo,InvDate,DcNo,DcDate,Code,InvQty,MillQty,Pending,OrderQty,Qty,InvRate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,InvAmount,InvNet,plus,Less,Misc,Dept_Code,Group_Code,Unit_Code,InvSlNo,ActualModVat,NoOfBills,MillCode,id,slno,mrsslno,orderslno,taxclaimable,usercode,creationdate,grnqty,grnvalue) Values (";
          String QSL     = "Select Max(InvSlNo) From GRN";
          String QS      = "";
          int iInvSlNo   = 0,iSlNo=0;
          try
          {
               Statement      stat           = theMConnection.createStatement();
               ResultSet      res            = stat.executeQuery(QSL);

               while(res.next())
               {
                    iInvSlNo = res.getInt(1);
               }
               res.close();
               iInvSlNo++;
               
               Object RowData[][]= MiddlePanel.MiddlePanel.getFromVector();                 
               String SAdd       = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess      = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm        = common.toDouble(SAdd)-common.toDouble(SLess);
               double dBasic     = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio     = dpm/dBasic;
               
               String SGrnNo     = LGrnNo.getText();
               
               for(int i=0;i<RowData.length;i++)
               {
                    double dDcQty       = common.toDouble((String)RowData[i][6]);
                    if(dDcQty == 0)
                         continue;
                    
                    String SUnitCode    = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode    = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode   = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SBlockCode   = MiddlePanel.getBlockCode(i);
                    String SOrderNo     = MiddlePanel.getOrderNo(i);
                    String SMRSNo       = MiddlePanel.getMRSNo(i);
                    String SOrdQty      = MiddlePanel.getOrdQty(i);
                    
                    iSlNo++;

                    String SMrsSLNo     = MiddlePanel.getMrsSLNo(i);
                    String SOrderSLNo   = MiddlePanel.getOrderSLNo(i);
                    String STaxC        = MiddlePanel.getTaxC(i);
                    
                    String  SItemCode   = (String)RowData[i][0];
                    
                    String  SBasic      = (String)RowData[i][13];
                    String  SInvAmount  = (String)RowData[i][18];
                    String  SVat        = (String)RowData[i][16];
                    
                    String  SMisc       = common.getRound(common.toDouble(SBasic)*dRatio,3);
                    
                    String  SMillQty    = (String)RowData[i][7];
                    String  SGrnQty     = SMillQty;
                    
                    String  SGrnValue   = "";
                    
                    /*if(STaxC.equals("0"))
                         SGrnValue = common.getRound(common.toDouble(SInvAmount) + common.toDouble(SMisc),2);
                    else
                         SGrnValue = common.getRound(common.toDouble(SInvAmount) + common.toDouble(SMisc) - common.toDouble(SVat),2);*/
                    
                    
                    SGrnValue = common.getRound(common.toDouble(SInvAmount) + common.toDouble(SMisc),2);

                    String    QS1 = QString;
                              QS1 = QS1+"0"+SOrderNo+",";
                              QS1 = QS1+"0"+SMRSNo+",";
                              QS1 = QS1+"0"+SGrnNo+",";
                              QS1 = QS1+"'"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                              QS1 = QS1+"0"+SBlockCode+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"0"+TGINo.getText()+",";
                              QS1 = QS1+"'"+common.pureDate(TGIDate.getText())+"',";
                              QS1 = QS1+"'"+TInvNo.getText()+"',";
                              QS1 = QS1+"'"+common.pureDate(TInvDate.getText())+"',";
                              QS1 = QS1+"'"+TDCNo.getText()+"',";
                              QS1 = QS1+"'"+common.pureDate(TDCDate.getText())+"',";
                              QS1 = QS1+"'"+SItemCode+"',";
                              QS1 = QS1+"0"+(String)RowData[i][6]+",";
                              QS1 = QS1+"0"+SMillQty+",";
                              QS1 = QS1+"0"+(String)RowData[i][5]+",";
                              QS1 = QS1+"0"+SOrdQty+",";
                              QS1 = QS1+"0"+(String)RowData[i][6]+",";
                              QS1 = QS1+"0"+(String)RowData[i][8]+",";
                              QS1 = QS1+"0"+(String)RowData[i][9]+",";
                              QS1 = QS1+"0"+(String)RowData[i][14]+",";
                              QS1 = QS1+"0"+(String)RowData[i][10]+",";
                              QS1 = QS1+"0"+(String)RowData[i][15]+",";
                              QS1 = QS1+"0"+(String)RowData[i][11]+",";
                              QS1 = QS1+"0"+(String)RowData[i][16]+",";
                              QS1 = QS1+"0"+(String)RowData[i][12]+",";
                              QS1 = QS1+"0"+(String)RowData[i][17]+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SAdd+",";
                              QS1 = QS1+"0"+SLess+",";
                              QS1 = QS1+"0"+SMisc+",";
                              QS1 = QS1+"0"+SDeptCode+",";
                              QS1 = QS1+"0"+SGroupCode+",";
                              QS1 = QS1+"0"+SUnitCode+",";
                              QS1 = QS1+"0"+iInvSlNo+",";
                              QS1 = QS1+"0"+0+",";
                              QS1 = QS1+"0"+TInvoice.getText()+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"grn_seq.nextval"+",";
                              QS1 = QS1+"0"+iSlNo+",";
                              QS1 = QS1+"0"+SMrsSLNo+",";
                              QS1 = QS1+"0"+SOrderSLNo+",";
                              QS1 = QS1+"0"+STaxC +",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+common.getServerDateTime()+"',";
                              QS1 = QS1+"0"+SGrnQty+",";
                              QS1 = QS1+"0"+SGrnValue+")";
                              
                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.executeUpdate(QS1);
                    
                    updateItemMaster(stat,SItemCode,SBlockCode,SGrnQty,SGrnValue);
               }

               QS = " Update config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1  where Id = 2";

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               stat. executeUpdate(QS);
               stat. close();
          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

     public void insertIssueDetails()
     {
          String SIssueNo = "";
          String str = "";
          String str1 = "";
          
          try
          {
               Statement       stat =  theMConnection.createStatement();
               ResultSet res        = stat.executeQuery("Select (maxno+1) From Config"+iMillCode+""+SYearCode+" where Id = 5 ");
               while(res.next())
               {
                    SIssueNo = res.getString(1);
               }
               res.close();
               
               String SDate     = TDate.toNormal();
               
               Item1 IC = new Item1(SItemCode,iMillCode,SItemTable,SSupTable);
               double dStock = common.toDouble(IC.getClStock());
               double dValue = common.toDouble(IC.getClValue());
               double dRate  = 0;
               try
               {
                    dRate = dValue/dStock;
               }
               catch(Exception ex)
               {
                    dRate=0;
               }

               if(dStock==0)
               {
                    dRate = common.toDouble(IC.SRate);
               }

               dIssVal = dIssQty * dRate;
               
               String SStockGroup = IC.getStockGroupCode();
               String SGroupCode="";
               
               if(SStockGroup.equals("C01"))
               {
                    SGroupCode = "170";
               }
               else
               if(SStockGroup.equals("C02"))
               {
                    SGroupCode = "169";
               }
               String SDeptCode = "26";
               String SUnitCode = "5";
               
               str = "Insert Into Issue(IssueNo,IssueDate,Code,Qty,IssRate,UIRefNo,Dept_Code,Group_Code,Unit_Code,UserCode,MillCode,Authentication,Id,IssueValue) Values (";
               str = str+SIssueNo+",";
               str = str+"'"+SDate+"',";
               str = str+"'"+SItemCode+"',";
               str = str+common.getRound(dIssQty,3)+",";
               str = str+common.getRound(dRate,4)+",";
               str = str+"0"+SIndentNo+",";
               str = str+"0"+SDeptCode+",";
               str = str+"0"+SGroupCode+",";
               str = str+"0"+SUnitCode+",";
               str = str+String.valueOf(iUserCode)+",";
               str = str+String.valueOf(iMillCode)+",";
               str = str+"1"+",";
               str = str+"Issue_Seq.nextval"+",";
               str = str+common.getRound(dIssVal,2)+")";
               
               str1 = " Update "+SItemTable+" Set IssVal =nvl(IssVal,0)+"+common.getRound(dIssVal,2)+",";
               str1 = str1 + " IssQty=nvl(IssQty.0)+"+common.getRound(dIssQty,3)+" ";
               str1 = str1 + " Where Item_Code = '"+SItemCode+"'";
               
               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               stat.executeUpdate(str);

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               stat.executeUpdate(str1);
               
               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               stat.executeUpdate(" Update config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1 where Id = 5");
               
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("@str : "+ex);
               bComflag  = false;
          }
     }

     public void setOrderLink()
     {
          try
          {
               Statement       stat =  theMConnection.createStatement();

               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();                 
               for(int i=0;i<RowData.length;i++)
               {
                    double dDcQty     = common.toDouble((String)RowData[i][6]);
                    if(dDcQty == 0)
                         continue;

                    String    QS = "Update PurchaseOrder Set ";
                              QS = QS+" InvQty=InvQty+"+(String)RowData[i][7];
                              QS = QS+" Where Id = "+(String)MiddlePanel.VId.elementAt(i);

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag  = false;
          }
     }

     public void setMRSLink()
     {
          try
          {
               Statement       stat          =  theMConnection.createStatement();

               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();

               for(int i=0;i<RowData.length;i++)
               {
                    double dDcQty     = common.toDouble((String)RowData[i][6]);
                    if(dDcQty == 0)
                         continue;
                    
                    String SMrsSLNo   = MiddlePanel.getMrsSLNo(i);
                    
                    String    QS = "Update MRS Set ";
                              QS = QS+" GrnNo="+MiddlePanel.getGRNNo(i);
                              QS = QS+" Where MRSNo = "+(String)MiddlePanel.VGMRSNo.elementAt(i)+" and Item_Code = '"+(String)MiddlePanel.RowData[i][0]+"' And SlNo="+SMrsSLNo;
                              QS = QS+" And Mrs.MillCode = "+iMillCode;

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag  = false;
          }
     }

     private boolean validateGRN()
     {
          BOk.setEnabled(false);
          boolean bflag=false;
          String str="";
          
          dIssQty   = common.toDouble((String)ReportTable.getModel().getValueAt(iIndex,10));
          
          int iInvoice = common.toInt(TInvoice.getText());
          if(iInvoice==0)
          {
               str="No of Bills Field is Empty";
               bflag=true;
          }
          if(isQtyDiff())
          {
               str="Quantity Mismatch";
               bflag=true;
          }
          if(isQtyLess())
          {
               str="Qty Less than Issued Qty";
               bflag=true;
          }
          if(isQtyNotTouched())
          {
               str=str+"\n"+"Invalid GRN - All Zero";
               bflag=true;
          }
          
          if(isGateNotTicked())
          {
               str = str+"\n"+"Some Gate Entries are Not Closed";
               bflag=true;
          }
          if(!bflag)
               return true;
          
          AcceptDeny AD = new AcceptDeny(str);
          AD.show();
          //AD.BAccept.addActionListener(new ADList(AD));
          AD.BDeny.addActionListener(new ADList(AD));
          return false;                              
     }

     private boolean isQtyDiff()
     {
          Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<RowData.length;i++)
          {
               double dDcQty   = common.toDouble((String)RowData[i][6]);
               double dInvQty  = common.toDouble((String)RowData[i][7]);

               if(dDcQty != dInvQty)
                    return true;
          }
          return false;
     }

     private boolean isQtyLess()
     {
          double dRecdQty = 0;
          
          Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<RowData.length;i++)
          {
               dRecdQty = dRecdQty + common.toDouble((String)RowData[i][6]);
          }
          if(dRecdQty < dIssQty)
               return true;
          
          return false;
     }

     private boolean isQtyNotTouched()
     {
          double dTDCQty=0;
          Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<RowData.length;i++)
          {
               dTDCQty   = dTDCQty+common.toDouble((String)RowData[i][6]);
          }
          if(dTDCQty == 0)
               return true;
          return false;
     }

     private boolean isGateNotTicked()
     {
          for(int i=0;i<MiddlePanel.RD.length;i++)
          {
               Boolean BValue = (Boolean)MiddlePanel.RD[i][3];
               if(!BValue.booleanValue())
               return true;
          }
          return false;
     }

     private class ADList implements ActionListener
     {
          AcceptDeny AD;
          ADList(AcceptDeny AD)
          {
               this.AD = AD;
          }
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==AD.BDeny)
               {
                    BOk.setEnabled(true);
                    AD.setVisible(false);
               }
          }
     }

     private void updateSubStoreMasterData()
     {
          try
          {
               Statement stat   =    theDConnection.createStatement();
               
               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
               for(int i=0;i<RowData.length;i++)
               {
                    double dDcQty     = common.toDouble((String)RowData[i][6]);
                    if(dDcQty == 0)
                         continue;
                    
                    String QS = "";
                    
                    QS = "Update InvItems Set ";
                    QS = QS+" MSRecQty=nvl(MSRecQty,0)+"+(String)RowData[i][7]+",";
                    QS = QS+" MSStock=nvl(MSStock,0)+"+(String)RowData[i][7]+" ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";

                    if(theDConnection  . getAutoCommit())
                         theDConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               String    QS1 = "Update InvItems Set ";
                         QS1 = QS1+" MSIssQty=nvl(MSIssQty,0)+"+common.getRound(dIssQty,3)+",";
                         QS1 = QS1+" MSStock=nvl(MSStock,0)-"+common.getRound(dIssQty,3)+" ";
                         QS1 = QS1+" Where Item_Code = '"+SItemCode+"'";
                         
               if(theDConnection  . getAutoCommit())
                    theDConnection  . setAutoCommit(false);

               stat.execute(QS1);
               
               stat.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
               bComflag  = false;
          }
     }

     private void updateItemMaster(Statement stat,String SItemCode,String SBlockCode,String SGrnQty,String SGrnValue)
     {
          try
          {
               int       iBlockCode= common.toInt(SBlockCode);
               
               String    QS        = "";
               
               QS = "Update "+SItemTable+" Set RecVal =nvl(RecVal,0)+"+SGrnValue+",";
               QS = QS+" RecQty=nvl(RecQty,0)+"+SGrnQty+" ";
               QS = QS+" Where Item_Code = '"+SItemCode+"'";

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               stat.execute(QS);
          }
          catch(Exception e)
          {
               System.out.println("E6"+e);
               bComflag  = false;
          }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theMConnection . commit();
                    if(iMillCode==1)
                         theDConnection . commit();
                    JOptionPane    . showMessageDialog(null,"The Entered Data is Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("Commit");
               }
               else
               {
                    theMConnection . rollback();
                    if(iMillCode==1)
                         theDConnection . rollback();
                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theMConnection   . setAutoCommit(true);
               if(iMillCode==1)
                    theDConnection   . setAutoCommit(true);
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
