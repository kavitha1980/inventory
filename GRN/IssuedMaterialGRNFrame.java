package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class IssuedMaterialGRNFrame extends JInternalFrame
{
     JPanel MiddlePanel,BottomPanel;
     JTabbedPane JTP;
     TabReport tabreport;

     Object RowData[][];
     String ColumnData[] = {"GI No","Date","Inv No","Inv Date","DC No","DC Date","Supplier","Material Code","Material Name","Recd Qty","Issued Qty"};
     String ColumnType[] = {"N"    ,"S"   ,"S"     ,"S"       ,"S"    ,"S"      ,"S"       ,"S"            ,"S"            ,"N"       ,"N"         };

     Common  common  = new Common();

     JLayeredPane DeskTop;
     StatusPanel SPanel;
     Vector VCode,VName;
     int iMillCode,iAuthCode,iUserCode;
     String SItemTable,SSupTable,SYearCode;
     
     Vector VGINo,VGIDate,VInvNo,VInvDate,VDCNo,VDCDate,VSupName,VMName,VMCode,VRecQty,VIssQty,VId,VSupCode,VIndentNo;

     Connection theConnection=null;

     public IssuedMaterialGRNFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,int iAuthCode,int iUserCode,String SItemTable,String SSupTable,String SYearCode)
     {
         super("Issue of Free,Trial & Cash Materials to SubStore");
         this.DeskTop    = DeskTop;
         this.SPanel     = SPanel;
         this.VCode      = VCode;
         this.VName      = VName;
         this.iMillCode  = iMillCode;
         this.iAuthCode  = iAuthCode;
         this.iUserCode  = iUserCode;
         this.SItemTable = SItemTable;
         this.SSupTable  = SSupTable;
         this.SYearCode  = SYearCode;

         createComponents();
         setLayouts();
         addComponents();
     }
     public void createComponents()
     {
          MiddlePanel  = new JPanel();
          BottomPanel  = new JPanel();
          JTP          = new JTabbedPane();
     }

     public void setLayouts()
     {
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,650,500);
         MiddlePanel.setLayout(new BorderLayout());
     }

     public void addComponents()
     {
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

          MiddlePanel.add("Center",JTP);

          setDataIntoVector();
          setRowData();

          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
             tabreport.ReportTable.addKeyListener(new KeyList());

             JTP.addTab("Materials Issued Without GRN",tabreport);

             setSelected(true);
             DeskTop.repaint();
             DeskTop.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println("Prob"+ex);
          }
     }

     public void setDataIntoVector()
     {
          String QString = "";

          QString  = " SELECT GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.Item_Code,InvItems.Item_Name,"+
                     " GateInward.Id,GateInward.GateQty,GateInward.IssueQty,GateInward.IndentNo,"+
                     " GateInward.InvNo,GateInward.InvDate,GateInward.DCNo,GateInward.DCDate,GateInward.Sup_Code "+
                     " From ((GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                     " Inner Join InvItems on GateInward.Item_Code = InvItems.Item_Code) "+
                     " Inner Join InwType on GateInward.InwNo=InwType.InwNo "+
                     " Where GateInward.GrnNo = 0 And GateInward.IssueStatus = 2 "+
                     " And GateInward.GrnType = 1 "+
                     " And GateInward.MillCode = 1 "+
                     " Order By 9,2,1,5";


            VGINo    = new Vector();
            VGIDate  = new Vector();
            VInvNo   = new Vector();
            VInvDate = new Vector();
            VDCNo    = new Vector();
            VDCDate  = new Vector();
            VSupName = new Vector();
            VMName   = new Vector();
            VMCode   = new Vector();
            VRecQty  = new Vector();
            VIssQty  = new Vector();
            VId      = new Vector();
            VSupCode = new Vector();
            VIndentNo= new Vector();

            try
            {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();

              ResultSet res = stat.executeQuery(QString);
              while (res.next())
              {
                    VGINo   .addElement(""+ res.getString(1));
                    VGIDate .addElement(""+ common.parseDate(res.getString(2)));
                    VSupName.addElement(""+ res.getString(3));
                    VMCode  .addElement(""+ res.getString(4));
                    VMName  .addElement(""+ res.getString(5));
                    VId     .addElement(""+ res.getString(6));
                    VRecQty .addElement(""+ res.getString(7));
                    VIssQty .addElement(""+ res.getString(8));
                    VIndentNo.addElement(""+ res.getString(9));
                    VInvNo   .addElement(""+ res.getString(10));
                    VInvDate .addElement(""+ common.parseDate(res.getString(11)));
                    VDCNo   .addElement(""+ res.getString(12));
                    VDCDate .addElement(""+ common.parseDate(res.getString(13)));
                    VSupCode.addElement(""+ res.getString(14));
              }
              res.close();
              stat.close();
           }
           catch(Exception ex){System.out.println("sql "+ex);}
     }
     public void setRowData()
     {
         RowData      = new Object[VGINo.size()][ColumnData.length];

         for(int i=0;i<VGINo.size();i++)
         {
               RowData[i][0]  = (String)VGINo.elementAt(i);
               RowData[i][1]  = (String)VGIDate.elementAt(i);
               RowData[i][2]  = (String)VInvNo.elementAt(i);
               RowData[i][3]  = (String)VInvDate.elementAt(i);
               RowData[i][4]  = (String)VDCNo.elementAt(i);
               RowData[i][5]  = (String)VDCDate.elementAt(i);
               RowData[i][6]  = (String)VSupName.elementAt(i);
               RowData[i][7]  = (String)VMCode.elementAt(i);
               RowData[i][8]  = (String)VMName.elementAt(i);
               RowData[i][9]  = (String)VRecQty.elementAt(i);
               RowData[i][10] = (String)VIssQty.elementAt(i);
        }
     }

     public class KeyList extends KeyAdapter
     {
           public void keyPressed(KeyEvent ke)
           {
                if (ke.getKeyCode()==10)
                {
					 try
					 {
						 JTable theTable = (JTable)ke.getSource();
						 int i = theTable.getSelectedRow();
                               String SIndex    = String.valueOf(i);
                               String SSupCode  = (String)VSupCode.elementAt(i);
                               String SGId      = (String)VId.elementAt(i);
                               String SIndentNo = (String)VIndentNo.elementAt(i);
                               IssuedMaterialGRNCollectionFrame issuedmaterialgrncollectionframe = new IssuedMaterialGRNCollectionFrame(DeskTop,VCode,VName,SPanel,SIndex,theTable,SSupCode,SGId,SIndentNo,iMillCode,iUserCode,SItemTable,SSupTable,SYearCode);
                               DeskTop.add(issuedmaterialgrncollectionframe);
                               issuedmaterialgrncollectionframe.show();
                               issuedmaterialgrncollectionframe.moveToFront();
                               issuedmaterialgrncollectionframe.setMaximum(true);
						 DeskTop.repaint();
						 DeskTop.updateUI();
					 }
					 catch(Exception e){}
                }
           } 
     }


}
