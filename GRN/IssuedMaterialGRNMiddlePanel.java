
package GRN;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class IssuedMaterialGRNMiddlePanel extends JTabbedPane 
{
         IssuedMaterialGRNInvMiddlePanel MiddlePanel;
         Object RowData[][],RD[][];

         String ColumnData[] = {"Code","Name","GRN No","Order No","MRS No","Pending Qty","Inv/DC Qty","Recd Qty","Rate","Disc (%)","Cenvat (%)","Tax (%)","Surcharge (%)","Basic","Disc (Rs)","Vat (Rs)","Tax (Rs)","Surcharge (Rs)","Net (Rs)","Department","Group","Unit","VAT"};
         String ColumnType[] = {"S","S","S","S","N","N","B","B","B","B","B","B","B","N","N","N","N","N","N","B","B","B","S"};

         String CD[] = {"Description","DC/Inv Qty","Qty Recd @ Gate","Click To Close"};
         String CT[] = {"S","N","N","B"};

         JLayeredPane DeskTop;
         String SSupCode="";

         Vector VIName,VDCQty,VQty,VGId;

         Vector VGCode,VGName,VGBlock,VGGrnNo,VGOrderNo,VGMRSNo;
         Vector VGPendQty,VGRate,VGDiscPer,VGVatPer,VGSTPer,VGSurPer;
         Vector VGDept,VGDeptCode,VGGroup,VGGroupCode,VGUnit,VGUnitCode;
         Vector VGBlockCode,VId,VGOrdQty,VGOrdDate;
         Vector VMrsSlNo,VOrderSlNo,VTaxC;

         Common common   = new Common();
         int iMillCode;
         JLabel LGrnNo;
         String SYearCode;

         public IssuedMaterialGRNMiddlePanel(JLayeredPane DeskTop,String SSupCode,int iMillCode,JLabel LGrnNo,String SYearCode)
         {
              this.DeskTop   = DeskTop;
              this.SSupCode  = SSupCode;
              this.iMillCode = iMillCode;
              this.LGrnNo    = LGrnNo;
              this.SYearCode = SYearCode;
         }

         public void createComponents(String SGId,String SItemCode)
         {
            setVectorData(SGId,SItemCode);
            setRowData();
            try
            {
                  MiddlePanel = new IssuedMaterialGRNInvMiddlePanel(DeskTop,RowData,ColumnData,ColumnType,iMillCode);
                  TabReport tabreport = new TabReport(RD,CD,CT);
                  addTab("Gate Inward ",tabreport);
                  addTab("Materials Pending @ Order",MiddlePanel);
                  tabreport.setBorder(new TitledBorder("Reference From Gate Entry"));
                  MiddlePanel.setBorder(new TitledBorder("Pending Purchase Orders"));
                  tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            }
            catch(Exception ex)
            {
                  System.out.println("GRNInvMiddlePanel: "+ex);
            }
         }
        public void setVectorData(String SGId,String SItemCode)
        {
               
          String QS = "";
          String QS1= "";

          QS  =   " SELECT InvItems.Item_Name,GateInward.SupQty,GateInward.GateQty "+
                  " From GateInward Inner Join InvItems On InvItems.Item_Code = GateInward.Item_Code "+
                  " WHERE GateInward.Id="+SGId+" And GrnNo=0 "+
                  " And GateInward.MillCode = 1 "+
                  " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                  " Order By 1";
   
          QS1 =   " SELECT PurchaseOrder.Item_Code, InvItems.Item_Name, OrdBlock.BlockName, "+
                  " PurchaseOrder.OrderNo, PurchaseOrder.MrsNo, "+
                  " PurchaseOrder.Qty-PurchaseOrder.InvQty AS Pending, "+
                  " PurchaseOrder.Rate, PurchaseOrder.DiscPer, PurchaseOrder.CenVatPer, "+
                  " PurchaseOrder.TaxPer, PurchaseOrder.SurPer, Dept.Dept_Name, "+
                  " PurchaseOrder.Dept_Code, Cata.Group_Name, PurchaseOrder.Group_Code, "+
                  " Unit.Unit_Name, PurchaseOrder.Unit_Code,PurchaseOrder.Id, "+
                  " PurchaseOrder.OrderBlock,PurchaseOrder.Qty,PurchaseOrder.OrderDate,purchaseOrder.Mrsslno,purchaseOrder.Slno,purchaseOrder.taxclaimable "+
                  " FROM ((((PurchaseOrder INNER JOIN InvItems ON "+
                  " PurchaseOrder.Item_Code = InvItems.Item_Code) "+
                  " INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock = OrdBlock.Block) "+
                  " INNER JOIN Dept ON PurchaseOrder.Dept_Code = Dept.Dept_code) "+
                  " INNER JOIN Cata ON PurchaseOrder.Group_Code = Cata.Group_Code) "+
                  " INNER JOIN Unit ON PurchaseOrder.Unit_Code = Unit.Unit_Code "+
                  " Where PurchaseOrder.Sup_Code = '"+SSupCode+"' "+
                  " And PurchaseOrder.Item_Code = '"+SItemCode+"' "+
                  " And PurchaseOrder.OrderBlock<=1 "+
                  " And PurchaseOrder.InvQty < PurchaseOrder.Qty  "+
                  " And PurchaseOrder.MillCode  = 1 "+
                  " Order By 3,4";

            VIName    = new Vector();
            VDCQty    = new Vector();
            VQty      = new Vector();
            VGId      = new Vector();

            VGCode     = new Vector();
            VGName     = new Vector(); 
            VGBlock    = new Vector();
            VGGrnNo    = new Vector();
            VGOrderNo  = new Vector();
            VGMRSNo    = new Vector();
            VGPendQty  = new Vector();
            VGRate     = new Vector();
            VGDiscPer  = new Vector();
            VGVatPer   = new Vector();
            VGSTPer    = new Vector();
            VGSurPer   = new Vector();
            VGDept     = new Vector();
            VGDeptCode = new Vector(); 
            VGGroup    = new Vector();
            VGGroupCode= new Vector(); 
            VGUnit     = new Vector();
            VGUnitCode = new Vector();
            VGBlockCode= new Vector();
            VGOrdQty   = new Vector();
            VGOrdDate  = new Vector();
            VId        = new Vector();

            VMrsSlNo   = new Vector();
            VOrderSlNo = new Vector();
            VTaxC      = new Vector();

            try
            {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
                   ResultSet res  = stat.executeQuery(QS);
                   while (res.next())
                   {
                         VIName    .addElement(res.getString(1));
                         VDCQty    .addElement(res.getString(2));
                         VQty      .addElement(res.getString(3));
                         VGId      .addElement(SGId);
                   }
                   res.close();

                   ResultSet res1 = stat.executeQuery(QS1);
                   while(res1.next())
                   {
                           VGCode    .addElement(res1.getString(1));
                           VGName    .addElement(res1.getString(2)); 
                           VGBlock   .addElement(res1.getString(3));
                           VGGrnNo   .addElement("0");
                           VGOrderNo .addElement(res1.getString(4));
                           VGMRSNo   .addElement(res1.getString(5));
                           VGPendQty .addElement(res1.getString(6));
                           VGRate    .addElement(res1.getString(7));
                           VGDiscPer .addElement(res1.getString(8));
                           VGVatPer  .addElement(res1.getString(9));
                           VGSTPer   .addElement(res1.getString(10));
                           VGSurPer  .addElement(res1.getString(11));
                           VGDept    .addElement(res1.getString(12));
                           VGDeptCode.addElement(res1.getString(13)); 
                           VGGroup   .addElement(res1.getString(14));
                           VGGroupCode.addElement(res1.getString(15)); 
                           VGUnit     .addElement(res1.getString(16));
                           VGUnitCode .addElement(res1.getString(17));
                           VId        .addElement(res1.getString(18));
                           VGBlockCode.addElement(res1.getString(19));
                           VGOrdQty   .addElement(res1.getString(20));
                           VGOrdDate  .addElement(res1.getString(21));
                           VMrsSlNo   .addElement(res1.getString(22));
                           VOrderSlNo .addElement(res1.getString(23));
                           VTaxC      .addElement(res1.getString(24));
                   }
                   res1.close();
                   stat.close();
                   setGRNNo();
            }
            catch(Exception ex)
            {
                System.out.println(ex);
            }
        }
        public void setGRNNo()
        {
               String SGNo = getId();
               LGrnNo.setText(SGNo);

               for(int i=0;i<VGBlock.size();i++)
               {
                    VGGrnNo.setElementAt(SGNo,i);    
               }
        }

        public boolean setRowData()
        {
            RD      = new Object[VIName.size()][CD.length];
            RowData = new Object[VGCode.size()][ColumnData.length];

            for(int i=0;i<VIName.size();i++)
            {
                    RD[i][0] = (String)VIName.elementAt(i);
                    RD[i][1] = (String)VDCQty.elementAt(i);
                    RD[i][2] = (String)VQty.elementAt(i);
                    RD[i][3] = new Boolean(false);
            }

            for(int i=0;i<VGCode.size();i++)
            {
                      RowData[i][0]  = (String)VGCode    .elementAt(i);
                      RowData[i][1]  = (String)VGName    .elementAt(i);
                      RowData[i][2]  = (String)VGBlock.elementAt(i)+"-"+(String)VGGrnNo   .elementAt(i);
                      RowData[i][3]  = (String)VGBlock.elementAt(i)+"-"+(String)VGOrderNo .elementAt(i)+"/"+common.parseDate((String)VGOrdDate.elementAt(i));
                      RowData[i][4]  = (String)VGMRSNo   .elementAt(i);
                      RowData[i][5]  = (String)VGPendQty .elementAt(i);
                      RowData[i][6]  = "";
                      RowData[i][7]  = "";
                      RowData[i][8]  = (String)VGRate    .elementAt(i);
                      RowData[i][9]  = (String)VGDiscPer .elementAt(i);
                      RowData[i][10] = (String)VGVatPer  .elementAt(i);
                      RowData[i][11] = (String)VGSTPer   .elementAt(i);
                      RowData[i][12] = (String)VGSurPer  .elementAt(i);
                      RowData[i][13] = "0";
                      RowData[i][14] = "0";
                      RowData[i][15] = "0";
                      RowData[i][16] = "0";
                      RowData[i][17] = "0";
                      RowData[i][18] = "0";
                      RowData[i][19] = (String)VGDept    .elementAt(i);
                      RowData[i][20] = (String)VGGroup   .elementAt(i);
                      RowData[i][21] = (String)VGUnit    .elementAt(i);
                      if(((String)VTaxC.elementAt(i)).equals("0"))
                      {
                          RowData[i][22] = "Not Claimable";
                      }
                      else
                      {
                          RowData[i][22] = "Claimable";
                      }
            }
            return true;
        }
        public String getBlockCode(int i)
        {
               return (String)VGBlockCode.elementAt(i);
        }
        public String getOrderNo(int i)
        {
               return (String)VGOrderNo.elementAt(i);
        }
        public String getMRSNo(int i)
        {
               return (String)VGMRSNo.elementAt(i);
        }
        public String getGRNNo(int i)
        {
               return (String)VGGrnNo.elementAt(i);
        }
        public String getOrdQty(int i)
        {
               return (String)VGOrdQty.elementAt(i);
        }
        public String getMrsSLNo(int i)
        {
               return (String)VMrsSlNo.elementAt(i);
        }
        public String getOrderSLNo(int i)
        {
               return (String)VOrderSlNo.elementAt(i);
        }
        public String getTaxC(int i)
        {
               return (String)VTaxC.elementAt(i);
        }

        private String  getId()
        {

            String SId ="";
            String QS ="";
            try
            {
                    QS = "  Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 2 for update of MaxNo noWait";

                    ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                    Connection      theConnection =  oraConnection.getConnection();

                    if(theConnection  . getAutoCommit())
                         theConnection  . setAutoCommit(false);

                    Statement       stat =  theConnection.createStatement();

                   ResultSet res  = stat.executeQuery(QS);
                   while (res.next())
                   {
                     SId = res.getString(1);    
                   }
                   res.close();
                   stat.close();
            }
            catch(Exception ex)
            {
                System.out.println("E1"+ex );
            }

           return SId;
        }

}
