
package GRN;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.border.TitledBorder;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Vector;
import java.util.HashMap;
import jdbc.*;
import java.io.*;
import java.sql.*;
import util.*;


public class MRSAuthUser extends JDialog{
 JPanel                         pnlMain,pnlReason,pnlButton,pnlList;
 
 JTable                         reasonTable;
 JButton                        btnOK;
 
  int                            iRow, iUserCode;
  String                        sUserName;
  JList                         theList;
  DefaultListModel              theListModel;
  Vector				  VUserNameList, VUserCode;
  Common		common		= new Common();
  
  
public MRSAuthUser( int iRow) {
this.iRow               = iRow;
 createComponent();
 setLayout();
 addComboBoxItem();
 addComponent();
 addListener();
 setUserData();
}

private void createComponent(){
 
 pnlMain                        = new JPanel();
 pnlButton                      = new JPanel();
 pnlReason                      = new JPanel();
 pnlList                        = new JPanel();
 btnOK                          = new JButton("Ok");

 theListModel                   = new DefaultListModel();
 theList                        = new JList(theListModel);
 
 
}
private void addComboBoxItem(){
}
private void setLayout(){
    
    pnlMain                    . setLayout(new BorderLayout());
    pnlList                    . setBorder(new TitledBorder("Select User"));
    pnlList                    . setLayout(new BorderLayout());

    this                        . setModal(true);
    this                        . setSize(250,300);
    this                        . setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    this                        . setTitle("MRS Authenticate Users");
    this                        . setResizable(false);
    
}
private void addComponent(){
    
  pnlList                       . add(new JScrollPane(theList));
  this                          . add(pnlList);
          
}
private void addListener(){
    //reasonTable                 .  addKeyListener(new KeyList());
      theList                     .addKeyListener(new KeyList());
}
private class KeyList extends KeyAdapter{
   public KeyList(){
        
    }
public void keyReleased(KeyEvent ke){
    if(ke.getKeyCode()==KeyEvent.VK_ENTER)
 {
   try{
        sUserName               = (String)theList.getSelectedValue();
	  iUserCode			  = getUserCode();
	  
        dispose();
     }
   catch(Exception e)
     {
          e.printStackTrace();
          System.out.println("Inside KeyListener:"+e);
     }
   }
 }
}

public void setMRSUser(){
		VUserCode		= new Vector();
		VUserNameList	= new Vector();

               String QString = " Select MRSUserAuthentication.AuthUserCode,UserName from MRSUSERAUTHENTICATION Inner Join RawUser on RawUSer.USerCode=MRSUserAuthentication.UserCode";
		   	    QString = QString+ " Where MRSAuthStatus=1 Order by 2";
               try
               {

                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       stat =  theConnection.createStatement();
                    
                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         VUserCode	  . addElement(res.getString(1));
                         VUserNameList. addElement(res.getString(2));
                    }
                    res            . close();
                    stat           . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
}


private void setUserData(){
	setMRSUser();
    for(int i=0;i<VUserNameList.size();i++){
        String sUserName      = (String)VUserNameList.elementAt(i);
        theListModel.addElement(sUserName);
    }
  }

public int	getUserCode(){
for(int i=0;i<VUserNameList.size();i++){
    String UserName         = (String)VUserNameList.get(i);
    String  sUserName       = (String)theList.getSelectedValue();
    String sUserCode	    ="";
    
    if(sUserName.equals(UserName)){
	     sUserCode	    = (String)VUserCode.get(i);
	     iUserCode	    = common.toInt(sUserCode);	
        break;    
    }
    else{
      }
    }
//System.out.println("Usercode:"+iUserCode);
return iUserCode; 
  }
}
