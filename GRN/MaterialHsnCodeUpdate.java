package GRN;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import javax.swing.border.*;
import java.io.*;
import java.util.*;
import java.awt.*;
import java.sql.*;
import java.net.InetAddress;

import guiutil.*;
import util.*;
import jdbc.*;

public class MaterialHsnCodeUpdate
{

     JLayeredPane        Layer;
     DirectGRNModelGst   GrnModel;
     JTable              GrnTable;
     DirectGRNMiddlePanelGst  MiddlePanel;
     String SHCode,SItemCode;

     java.sql.Connection theConnection = null;
     private   int                      iSelectedRow;

     private   JDialog                  theDialog;

     private   JPanel                   TopPanel, BottomPanel;
     private   JPanel			Top1Panel, Top2Panel;
	
     JLabel LICode,LPHsnCode;

     private   MyTextField  		THsnCode;
     private   FractionNumberField 	FnGstRate,FnCgst,FnSgst,FnIgst,FnCess;
	
     private   JComboBox            	JCHSNType,JCTaxCat;

     private   JButton                  BApply,BSave,BExit;


     private   ArrayList                     theItemList;
	 private   String sSupplierCode="",SSupTable="",sGstStateCode="",sGstPartyType="";
     double dCGST=0,dSGST=0,dIGST=0,dCess=0;
	 Vector vTaxCatCode,vTaxCatName;
	 boolean bAutoCommitFlag = true;

     Common common = new Common();

     // Creating Constructor...

     public MaterialHsnCodeUpdate(JLayeredPane Layer,DirectGRNModelGst GrnModel,JTable GrnTable,DirectGRNMiddlePanelGst MiddlePanel,String SHCode,String SItemCode)
     {
          this.Layer       = Layer;
          this.GrnModel    = GrnModel;
          this.GrnTable    = GrnTable;
          this.MiddlePanel = MiddlePanel;
	  this.SHCode	   = SHCode;
	  this.SItemCode   = SItemCode;

	  try
	  {
	       getTaxCategory();
	       createComponents();
	       setLayouts();
	       addComponents();
	       addListeners();
	       showDialog();
	  }
	  catch(Exception ex){ex.printStackTrace();} 
     }

     // creating Components, setLayouts and Add Components..

     private void createComponents()
     {
          theDialog      = new JDialog(new JFrame(), "New HsnCode and Gst Rate Insert", true);
		
	  LICode	 = new JLabel("");
	  LPHsnCode	 = new JLabel("");
	  THsnCode	 = new MyTextField(8);
	  JCTaxCat       = new MyComboBox(vTaxCatName);
          FnGstRate	 = new FractionNumberField();
	  FnCgst	 = new FractionNumberField();
	  FnSgst	 = new FractionNumberField();
	  FnIgst         = new FractionNumberField();
	  FnCess	 = new FractionNumberField();
          
          TopPanel        = new JPanel();
          BottomPanel     = new JPanel();

          Top1Panel       = new JPanel();
          Top2Panel       = new JPanel();

          BApply  	  = new JButton("Apply");
          BSave  	  = new JButton("Save");		  		  
          BExit           = new JButton("Exit");

	  LICode.setText(SItemCode);
	  LPHsnCode.setText(SHCode);

	  BSave.setEnabled(false);
     }

     private void setLayouts()
     {
          TopPanel                 . setLayout(new BorderLayout());
          Top1Panel                . setLayout(new GridLayout(4,2,5,5));
          Top2Panel                . setLayout(new GridLayout(6,2,5,5));
          BottomPanel              . setLayout(new GridLayout(1,2,5,5));

          Top1Panel	           . setBorder(new TitledBorder("Hsn Details"));
          Top2Panel	           . setBorder(new TitledBorder("Gst Rate Details"));
          BottomPanel              . setBorder(new TitledBorder("Controls"));
     }

     private void addComponents()
     {		
	  Top1Panel.add(new JLabel("Material Code "));
	  Top1Panel.add(LICode);

	  Top1Panel.add(new JLabel("Previous Hsn Code "));
	  Top1Panel.add(LPHsnCode);

	  Top1Panel.add(new JLabel("New Hsn Code "));
	  Top1Panel.add(THsnCode);

	  Top1Panel.add(new JLabel(""));
	  Top1Panel.add(BApply);

	  TopPanel.add("North",Top1Panel);

          BottomPanel             . add(BSave);
          BottomPanel             . add(BExit);
     }

     private void addListeners()
     {
	  BApply.addActionListener(new ActionList());
	  BSave.addActionListener(new ActionList());
          BExit.addActionListener(new ActionList());
	  JCTaxCat.addActionListener(new TaxSelectList());
	  FnGstRate.addFocusListener(new GstRateFocusList());
     }

     private void showDialog()
     {
	  try
	  {
          	theDialog                . getContentPane()  . removeAll();
	  }
	  catch(Exception ex){}

          theDialog                . getContentPane()  . add("North", TopPanel);
          theDialog                . getContentPane()  . add("South", BottomPanel);

          theDialog                . setBounds(80, 90, 640, 510);
          theDialog                . setVisible(true);
     }

     private class ActionList implements ActionListener 
     {
          public void actionPerformed(ActionEvent ae) 
	  {
               if(ae.getSource() == BApply) 
	       {
	            if(isValidHsnCode())
		    {
		         setRatePanel();
		    }
	       }

               if(ae.getSource() == BSave) 
	       {
	            if(isValidData())
		    {
		         if(updateDetails()) 
			 {
			      JOptionPane.showMessageDialog(null, " Data Saved ", "Information", JOptionPane.INFORMATION_MESSAGE);
			 }
			 else
			 {
			      JOptionPane.showMessageDialog(null, " Problem in Updation ", "Error", JOptionPane.INFORMATION_MESSAGE);
			 }

			 try
		         {
	                      if(bAutoCommitFlag)
	                      {
	                           theConnection  . commit();
				   System.out.println("Commit Completed");
	                           theConnection  . setAutoCommit(true);

				   String SNewHsnCode  = THsnCode.getText();
				   double dGstRate     = common.toDouble((String)FnGstRate.getText());
				   double dGstCess     = common.toDouble((String)FnCess.getText());

			      	   double dCGST=0;
			      	   double dSGST=0;
			      	   double dIGST=0;
			      	   double dCess=0;

				   if(!MiddlePanel.SSeleSupType.equals("1"))
				   {
					   dCGST=0;
					   dSGST=0;
					   dIGST=0;
					   dCess=0;
				   }
				   else
				   {
					   if(MiddlePanel.SSeleStateCode.equals("33"))
					   {
						dCGST = dGstRate / 2;
						dSGST = dGstRate / 2;
						dIGST = 0;
					   }
					   else
					   {
						dCGST = 0;
						dSGST = 0;
						dIGST = dGstRate;
					   }

					   dCess = dGstCess;
				   }


				   for(int k=0;k<GrnModel.getRows();k++)
				   {
				        String SICode = (String)GrnModel.getValueAt(k,0);

				        if(SICode.equals(SItemCode))
					{
					     Boolean bValue = (Boolean)GrnModel.getValueAt(k,10);

					     if(bValue.booleanValue())
						  continue;

			       	    	     String SGrnStatus = (String)MiddlePanel.VGGrnStatus.elementAt(k);

					     if(SGrnStatus.equals("1"))
						  continue;

					     MiddlePanel.VGRateStatus.setElementAt("1",k);
					     MiddlePanel.VGNewHsnCode.setElementAt(SNewHsnCode,k);
					     MiddlePanel.VGNewGstRate.setElementAt(common.getRound(dGstRate,2),k);
					     MiddlePanel.VGNewCGSTPer.setElementAt(common.getRound(dCGST,2),k);
					     MiddlePanel.VGNewSGSTPer.setElementAt(common.getRound(dSGST,2),k);
					     MiddlePanel.VGNewIGSTPer.setElementAt(common.getRound(dIGST,2),k);
					     MiddlePanel.VGNewCessPer.setElementAt(common.getRound(dCess,2),k);

					     GrnModel.setValueAt(SNewHsnCode,k,11);
					     GrnModel.setValueAt(common.getRound(dGstRate,2),k,12);
					}
				   }	

				   removeHelpFrame();
	                      }
	                      else
	                      {
	                           theConnection  . rollback();
	                           theConnection  . setAutoCommit(true);
	                      }
		         }
			 catch(Exception ex)
		         {
		              ex.printStackTrace();
		         }
		    }
               }
               if (ae.getSource() == BExit) 
	       {
                     removeHelpFrame();
               }
          }
     }

     public boolean isValidHsnCode()
     {
          String SHsnCode = THsnCode.getText().trim().toUpperCase();

	  if(SHsnCode.length()==0)
	  {
	       JOptionPane.showMessageDialog(null," Please Enter HSN Code","Information",JOptionPane.INFORMATION_MESSAGE);
	       THsnCode.requestFocus();
	       return false;
	  }

          if(SHsnCode.length() >8 || SHsnCode.length() <8) 
	  {
	       JOptionPane.showMessageDialog(null," HSN Code Length must be 8 Characters","Information",JOptionPane.INFORMATION_MESSAGE);
	       THsnCode.requestFocus();
	       return false;
	  }

          int iCheck = checkHsnGstRate(SHsnCode);

	  if(iCheck>0)
	  {
	       JOptionPane.showMessageDialog(null," This HSN Code Already Exists. Use F2 to Select","Information",JOptionPane.INFORMATION_MESSAGE);
	       THsnCode.requestFocus();
	       return false;
	  }

          return true;
     }

     private void setRatePanel()
     {		
	  Top2Panel.add(new JLabel("Tax Type"));
	  Top2Panel.add(JCTaxCat);

	  Top2Panel.add(new JLabel("Gst Rate % "));
	  Top2Panel.add(FnGstRate);

	  Top2Panel.add(new JLabel("CGST % "));
	  Top2Panel.add(FnCgst);

	  Top2Panel.add(new JLabel("SGST % "));
	  Top2Panel.add(FnSgst);

	  Top2Panel.add(new JLabel("IGST % "));
	  Top2Panel.add(FnIgst);

	  Top2Panel.add(new JLabel("CESS % "));
	  Top2Panel.add(FnCess);

	  TopPanel.add("Center",Top2Panel);

	  THsnCode.setEditable(false);
	  BApply.setEnabled(false);
	  BSave.setEnabled(true);

	  FnIgst	 .setEditable(false);

	  showDialog();
     }

     public class TaxSelectList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               int iIndex = JCTaxCat.getSelectedIndex();
	       if(iIndex==0)
	       {
		    FnGstRate.setEditable(true);
		    FnCgst.setEditable(true);
		    FnSgst.setEditable(true);
		    FnCess.setEditable(true);

		    FnGstRate.setText("");
		    FnCgst.setText("");
		    FnSgst.setText("");
		    FnIgst.setText("");
		    FnCess.setText("");
	       }		
	       if(iIndex==1 || iIndex==2)
	       {
		    FnGstRate.setEditable(false);
		    FnCgst.setEditable(false);
		    FnSgst.setEditable(false);
		    FnCess.setEditable(false);

		    FnGstRate.setText("0");
		    FnCgst.setText("0");
		    FnSgst.setText("0");
		    FnIgst.setText("0");
		    FnCess.setText("0");
	       }
          }
     }

     public class GstRateFocusList implements FocusListener
     {
          public void focusGained(FocusEvent e) 
          {
	  };
          public void focusLost(FocusEvent e)
          {
	       if(JCTaxCat.getSelectedIndex()==0)
	       {
	            JCTaxCat.setEnabled(true);
		    FnGstRate.setEditable(true);
		    FnCgst.setEditable(true);
		    FnSgst.setEditable(true);
		    FnCess.setEditable(true);
	     
		    String SGstRate=(String)FnGstRate.getText() ;
		    double dGstRate=common.toDouble(common.getRound(SGstRate,2));

		    double dCGST=dGstRate/2;
		    double dSGST=dGstRate/2;

		    FnIgst.setText(FnGstRate.getText());
		    FnCgst.setText(String.valueOf(dCGST));
		    FnSgst.setText(String.valueOf(dSGST));
	       }
	       else
	       {
	            JCTaxCat.setEnabled(true);
		    FnGstRate.setEditable(false);
		    FnCgst.setEditable(false);
		    FnSgst.setEditable(false);
		    FnCess.setEditable(false);

		    FnGstRate.setText("0");
		    FnCgst.setText("0");
		    FnSgst.setText("0");
		    FnIgst.setText("0");
		    FnCess.setText("0");
	       }
          }
     }

     public boolean isValidData()
     {
	  int iIndex = JCTaxCat.getSelectedIndex();

	  if(THsnCode.getText().trim().length()==0)
	  {
	       JOptionPane.showMessageDialog(null," Please Enter HSN Code","Information",JOptionPane.INFORMATION_MESSAGE);
	       THsnCode.requestFocus();
	       return false;
	  }

          if(THsnCode.getText().trim().length() >8 || THsnCode.getText().trim().length() <8 ) 
	  {
	       JOptionPane.showMessageDialog(null,"   HSN Code should contains 8 - Digits","Information",JOptionPane.INFORMATION_MESSAGE);
	       THsnCode.requestFocus();
	       return false;
	  }
	  if(iIndex==0 && common.toDouble(FnGstRate.getText())<=0)
	  {
	       JOptionPane.showMessageDialog(null," Please Enter Gst Rate ","Information",JOptionPane.INFORMATION_MESSAGE);
	       FnGstRate.requestFocus();
	       return false;
	  }
	  if(common.toDouble(FnGstRate.getText())>0)
	  {
	       double dCgst =0,dSgst=0,dIgst=0,dGstRate=0;
	       double dTot = 0;
	       dCgst = common.toDouble(FnCgst.getText());
	       dSgst = common.toDouble(FnSgst.getText());
	       dIgst = common.toDouble(FnIgst.getText());
	       dGstRate = common.toDouble(FnGstRate.getText());
	       dTot  = dCgst+dSgst;
	       if( dTot != dGstRate || dCgst==0 || dSgst==0)
	       {
		     JOptionPane.showMessageDialog(null,"Total of Cgst & Sgst Must be equal to Gst Rate","Information",JOptionPane.INFORMATION_MESSAGE);
		     return false;
	       }
	       if(dGstRate==0 && (dCgst>0 || dSgst>0 || dIgst>0))
	       {
		     JOptionPane.showMessageDialog(null,"Please Enter GST Rate","Information",JOptionPane.INFORMATION_MESSAGE);
		     return false;
	       }
	  }
          return true;
     }

     private boolean updateDetails() 
     {
	  StringBuffer sbinsert = new StringBuffer();
	  PreparedStatement ps = null;

	  sbinsert.append(" Insert into HsnGstRate (ID,HSNCODE,GSTRATE,CGST,SGST,IGST,CESS,USERCODE,DATETIME,SYSNAME,TAXCATCODE) ");
	  sbinsert.append(" Values(HsnGstRate_Seq.nextval,?,?,?,?,?,?,?,sysdate,?,?) ");

          try 
	  {
	       String iip = InetAddress.getLocalHost().getHostAddress();
               if(theConnection == null)
	       {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

	       theConnection   .	setAutoCommit(true);

 	       if(theConnection. getAutoCommit())
		     theConnection   . setAutoCommit(false);

	       ps = theConnection.prepareStatement(sbinsert.toString());

	       ps.setString(1,THsnCode.getText().trim());
	       ps.setString(2,String.valueOf(common.toDouble(common.parseNull(FnGstRate.getText()))));
	       ps.setString(3,String.valueOf(common.toDouble(common.parseNull(FnCgst.getText()))));
	       ps.setString(4,String.valueOf(common.toDouble(common.parseNull(FnSgst.getText()))));
	       ps.setString(5,String.valueOf(common.toDouble(common.parseNull(FnIgst.getText()))));
	       ps.setString(6,String.valueOf(common.toDouble(common.parseNull(FnCess.getText()))));
	       ps.setString(7,String.valueOf(MiddlePanel.iUserCode));
	       ps.setString(8,common.parseNull(iip));
	       ps.setString(9,common.parseNull(getTaxCode((String)JCTaxCat.getSelectedItem())));
	       ps.executeUpdate();

	       if(ps != null)
	       {
	            ps.close();
	       }

	       /*String QS1 = " Update InvItems set HsnCode='"+THsnCode.getText().trim()+"' Where Item_Code='"+SItemCode+"'";

               Statement stat =  theConnection.createStatement();

	       stat.execute(QS1);
	       stat.close();*/
          } 
	  catch (Exception ex) 
	  {
               ex.printStackTrace();
               bAutoCommitFlag     = false;
	       return false;
          }

          return true;
     }

     private String getTaxCode(String STaxName)
     {
          int iIndex=-1;
          iIndex = vTaxCatName.indexOf(STaxName);
          if(iIndex!=-1)
               return common.parseNull((String)vTaxCatCode.elementAt(iIndex));
          else
               return "";
     }

     private void removeHelpFrame()
     {
          try
          {
               theDialog . setVisible(false);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private int checkHsnGstRate(String SHsnCode)
     {
	  int count = 0;
	  StringBuffer sb = new StringBuffer();
	  sb.append(" select Count(*) From HsnGstRate where HSNCODE='"+SHsnCode+"'"); 

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();
               Statement      stat           = theConnection.createStatement();
               ResultSet      result         = stat.executeQuery(sb.toString());
               while(result.next())
               {
		    count = result.getInt(1);
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("checkHsnGstRate :"+ex);
          }

	  return count;
     }

     private void getTaxCategory() 
     {
	  vTaxCatCode = new Vector();
	  vTaxCatName = new Vector();

          StringBuffer sb = new StringBuffer();

	  sb.append(" Select CODE,NAME from taxcategory ");

          try 
	  {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               java.sql.PreparedStatement pst = theConnection.prepareStatement(sb.toString());
               java.sql.ResultSet rst = pst.executeQuery();
               java.util.HashMap theMap = null;
               while (rst.next()) 
	       {
		    vTaxCatCode.addElement(rst.getString(1));
		    vTaxCatName.addElement(rst.getString(2));
               }
               rst.close();
               pst.close();
          } 
	  catch (Exception ex) 
	  {
               ex.printStackTrace();
          }
     }

}
