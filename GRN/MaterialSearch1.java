package GRN;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class MaterialSearch1 implements ActionListener
{
     JLayeredPane   Layer;
     JTextField     TMatCode;
     JTextField     TMatCatl;
     JTextField     TMatDraw;
     JButton        BMatName;
     Vector         VName,VCode;
     JTextField     TIndicator;          
     JList          BrowList;
     JScrollPane    BrowScroll;
     JPanel         LeftPanel;
     JTextArea      JTA;
     JInternalFrame MaterialFrame;
     JPanel         MaterialPanel= new JPanel(true);
     Vector VECFData=new Vector();
     Vector         VNameCode,VUom;
     String SName="",SCode="";

     String SECFCode="",SECFName="",SECFUom="";
     int Flag;
     ServTableModel MatModel;
     int row,column;
     int iSelectedRow;
     int iFlag = 0;
     String str="";
     String SWhere="";
     JComboBox JCMake;
      
     Vector VMake,VId;
     ActionEvent ae;
     String SCatl="",SDraw="";
     Common common = new Common();
     JPanel BottomPanel;
     JButton BRefresh;
     JDialog MaterialDialog;
     int iMillCode=0;

     MaterialSearch1(JLayeredPane Layer,JTextField TMatCode,JButton BMatName,Vector VCode,Vector VName,JDialog MaterialDialog,int iMillCode)
     {
          this.Layer       = Layer;
          this.TMatCode    = TMatCode;
          this.BMatName    = BMatName;
          this.VName       = VName;
          this.VCode       = VCode;
          this.MaterialDialog = MaterialDialog;
          this.iMillCode   = iMillCode;

          VNameCode     = new Vector();
          for (int i=0;i<VCode.size();i++)
          {
               SName = (String)VName.elementAt(i);
               SCode = (String)VCode.elementAt(i);
               VNameCode.addElement(SName+" (Code : "+SCode+")");
          }

          BrowList      = new JList(VNameCode);
          BrowList.setFont(new Font("monospaced", Font.PLAIN, 11));

          BrowScroll    = new JScrollPane(BrowList);

          LeftPanel     = new JPanel(true);
          TIndicator    = new JTextField();
          TIndicator.setEditable(false);
          MaterialFrame     = new JInternalFrame("Materials Selector");
          MaterialFrame.show();
          MaterialFrame.setBounds(80,100,550,350);
          MaterialFrame.setClosable(true);
          MaterialFrame.setResizable(true);
          BrowList.addKeyListener(new KeyList());

          BRefresh      = new JButton("Refresh");
          BottomPanel   = new JPanel(true);
          BottomPanel.setLayout(new GridLayout(1,2));
          BottomPanel.add(TIndicator);
          BottomPanel.add(BRefresh);

          MaterialFrame.getContentPane().setLayout(new BorderLayout());
          MaterialFrame.getContentPane().add("South",BottomPanel);
          MaterialFrame.getContentPane().add("Center",BrowScroll);

          iFlag = 0;

          BRefresh.addActionListener(new RefreshList());
		  MaterialPanel.setLayout(new BorderLayout());
          MaterialPanel.add("South",BottomPanel);
          MaterialPanel.add("Center",BrowScroll);
		  MaterialFrame.setVisible(false);
     }
     MaterialSearch1(JLayeredPane Layer,JTextField TMatCode,JButton BMatName,Vector VCode,Vector VName,JTextField TMatCatl,JTextField TMatDraw)
     {
          this.Layer       = Layer;
          this.TMatCode    = TMatCode;
          this.BMatName    = BMatName;
          this.VCode       = VCode;
          this.VName       = VName;
          this.TMatCatl    = TMatCatl;
          this.TMatDraw    = TMatDraw;

          VNameCode     = new Vector();
          for (int i=0;i<VCode.size();i++)
          {
               SName = (String)VName.elementAt(i);
               SCode = (String)VCode.elementAt(i);
               VNameCode.addElement(SName+" (Code : "+SCode+")");
          }

          BrowList      = new JList(VNameCode);
          BrowList.setFont(new Font("monospaced", Font.PLAIN, 11));

          BrowScroll    = new JScrollPane(BrowList);

          LeftPanel     = new JPanel(true);
          TIndicator    = new JTextField();
          TIndicator.setEditable(false);
          MaterialFrame     = new JInternalFrame("Materials Selector");
          MaterialFrame.show();
          MaterialFrame.setBounds(80,100,550,350);
          MaterialFrame.setClosable(true);
          MaterialFrame.setResizable(true);
          BrowList.addKeyListener(new KeyList());

          BRefresh      = new JButton("Refresh");
          BottomPanel   = new JPanel(true);
          BottomPanel.setLayout(new GridLayout(1,2));
          BottomPanel.add(TIndicator);
          BottomPanel.add(BRefresh);

          MaterialFrame.getContentPane().setLayout(new BorderLayout());
          MaterialFrame.getContentPane().add("South",BottomPanel);
          MaterialFrame.getContentPane().add("Center",BrowScroll);
          SWhere = "MRS";      
          iFlag = 0;
          BRefresh.addActionListener(new RefreshList());
     }

     MaterialSearch1(JLayeredPane Layer,Vector VCode,Vector VName,ServTableModel MatModel,int iSelectedRow)
     {
          this.Layer        = Layer;
          this.VName        = VName;
          this.VCode        = VCode;
          this.MatModel     = MatModel;
          this.iSelectedRow = iSelectedRow;

          VNameCode     = new Vector();
          for (int i=0;i<VCode.size();i++)
          {
               SName = (String)VName.elementAt(i);
               SCode = (String)VCode.elementAt(i);
               VNameCode.addElement(SName+" (Code : "+SCode+")");
          }

          BrowList      = new JList(VNameCode);
          BrowList.setFont(new Font("monospaced", Font.PLAIN, 11));

          TIndicator    = new JTextField();
          BrowScroll    = new JScrollPane(BrowList);

          LeftPanel     = new JPanel(true);

          MaterialFrame     = new JInternalFrame("Materials Selector");
          MaterialFrame.show();
          MaterialFrame.setClosable(true);
          MaterialFrame.setBounds(80,100,550,350);
          BrowList.addKeyListener(new KeyList());

          BRefresh      = new JButton("Refresh");
          BottomPanel   = new JPanel(true);
          BottomPanel.setLayout(new GridLayout(1,2));
          BottomPanel.add(TIndicator);
          BottomPanel.add(BRefresh);

          MaterialFrame.getContentPane().setLayout(new BorderLayout());
          MaterialFrame.getContentPane().add("South",BottomPanel);
          MaterialFrame.getContentPane().add("Center",BrowScroll);

          iFlag=1;
          BRefresh.addActionListener(new RefreshList());
     }
     MaterialSearch1(JLayeredPane Layer,Vector VCode,Vector VName,JTextField TMatCode,JTextArea JTA,String SWhere)
     {
          this.Layer        = Layer;
          this.VName        = VName;
          this.VCode        = VCode;
          this.TMatCode     = TMatCode;
          this.JTA          = JTA;
          this.SWhere       = SWhere;

          VNameCode     = new Vector();
          for (int i=0;i<VCode.size();i++)
          {
               SName = (String)VName.elementAt(i);
               SCode = (String)VCode.elementAt(i);
               VNameCode.addElement(SName+" (Code : "+SCode+")");
          }

          BrowList      = new JList(VNameCode);
          BrowList.setFont(new Font("monospaced", Font.PLAIN, 11));

          TIndicator    = new JTextField();
          BrowScroll    = new JScrollPane(BrowList);

          LeftPanel     = new JPanel(true);

          MaterialFrame     = new JInternalFrame("Materials Selector");
          MaterialFrame.show();
          MaterialFrame.setClosable(true);
          MaterialFrame.setBounds(80,100,550,350);
          BrowList.addKeyListener(new KeyList());

          BRefresh      = new JButton("Refresh");
          BottomPanel   = new JPanel(true);
          BottomPanel.setLayout(new GridLayout(1,2));
          BottomPanel.add(TIndicator);
          BottomPanel.add(BRefresh);

          MaterialFrame.getContentPane().setLayout(new BorderLayout());
          MaterialFrame.getContentPane().add("South",BottomPanel);
          MaterialFrame.getContentPane().add("Center",BrowScroll);

          iFlag=1;
          BRefresh.addActionListener(new RefreshList());
     }
      
     public void actionPerformed(ActionEvent ae)
     {
          this.ae = ae;
          removeHelpFrame();
          try
          {
               Layer.add(MaterialFrame);
               MaterialFrame.moveToFront();
               MaterialFrame.setSelected(true);
               MaterialFrame.show();
               BrowList.requestFocus();
               setCursor(BMatName.getText());
               Layer.repaint();
          }
          catch(Exception ex){}
     }
     public class RefreshList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
                    setDataIntoVector();
                    BrowList.setListData(VName);
          }
     }

     public class KeyListECF extends KeyAdapter
     {
          JComboBox JCTemp;
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    SECFCode     = (String)VCode.elementAt(index);
                    SECFName     = (String)VName.elementAt(index);
                    SECFUom      = (String)VUom.elementAt(index);
                     
                    if (Flag==1)
                    {
//                         enquiryTabbedPane.MiddlePanel.MiddlePanel.ReportTable.setValueAt(SECFCode,row,0);
//                         enquiryTabbedPane.MiddlePanel.MiddlePanel.ReportTable.setValueAt(SECFName,row,1);
//                         enquiryTabbedPane.MiddlePanel.MiddlePanel.ReportTable.setValueAt(SECFUom,row,2);
                          
                         removeHelpFrame();
                    }
                    else
                    {
                         Vector mat=new Vector();
                         mat.addElement(SECFCode);
                         mat.addElement(SECFName);
                         mat.addElement(SECFUom);
                         mat.addElement("0");
                         mat.addElement("0.0");
                         mat.addElement("");
                         mat.addElement("");
                         mat.addElement("");
//                         enquiryTabbedPane.MiddlePanel.MiddlePanel.dataModel.addRow(mat);
                    }

					 
               }
          }
     }
	 
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
                }
                catch(Exception ex){}
          }
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==116)    // F5 is pressed
               {
                    setDataIntoVector();
                    BrowList.setListData(VNameCode);
               }
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SMatNameCode = (String)VNameCode.elementAt(index);
                    String SMatName     = (String)VName.elementAt(index);
                    String SMatCode     = (String)VCode.elementAt(index);
                    addMatDet(SMatName,SMatCode,index);
                    str="";
                    removeHelpFrame();
                }
          }
     }

     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<VName.size();index++)
          {
               String str1 = ((String)VName.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }

     public void setCursor(String xtr)
     {
          int index=0;
          for(index=0;index<VName.size();index++)
          {
               String str1 = ((String)VName.elementAt(index)).toUpperCase();
               if(str1.startsWith(xtr))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(MaterialFrame);
               Layer.repaint();
               Layer.updateUI();
               BMatName.requestFocus();
               MaterialDialog.setVisible(false);
          }
          catch(Exception ex) { }
     }
     public boolean addMatDet(String SMatName,String SMatCode,int index)
     {
          if(iFlag==0)
          {
               TMatCode.setText(SMatCode);
               BMatName.setText(SMatName);
               if(SWhere=="MRS")
               {
                   setCDM(SMatCode);
                   TMatCatl.setText(SCatl);
                   TMatDraw.setText(SDraw);
               }
               return true;    
          }
          if(SWhere=="Make")
          {
               setMake(SMatCode,SMatName);    
               return true;    
          }
          MatModel.setElement(SMatCode,SMatName,iSelectedRow);
          return true;
     }
     public void setCDM(String SMatCode)
     {
          try
          {
               Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
               Connection conn  = DriverManager.getConnection(common.getDSN(),"","");
               Statement stat   = conn.createStatement();
               ResultSet res1 = stat.executeQuery("Select Catl,Draw From InvItems Where Item_Code='"+SMatCode+"'");
               while(res1.next())                                    
               {                                                     
                    SCatl = common.parseNull(res1.getString(1));  
                    SDraw = common.parseNull(res1.getString(2));  
               }                                             
               //ResultSet res2 = stat.executeQuery("Select Make From Make Where Item_Code='"+SMatCode+"' Order By Make");
          
               /*ResultSet res2 = stat.executeQuery("Select Make From Make Order By Make");
               while(res2.next())                                    
               {                                                     �
                    JCMake.addItem(res2.getString(1));            
               }*/                                                   
          }
          catch(Exception ex)
          {
               System.out.println("@ setCDM");
               System.out.println(ex);
          }
     }
     public void setMake(String SMatCode,String SMatName)
     {
          JButton jb = (JButton)ae.getSource();
          jb.setText(SMatName);

          VMake = new Vector();
          VId   = new Vector();
          JTA.setText("");
          JTA.setEditable(true);
          TMatCode.setText(SMatCode);
          try
          {
               Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
               Connection conn  = DriverManager.getConnection(common.getDSN(),"","");
               Statement stat   = conn.createStatement();
               ResultSet res = stat.executeQuery("Select Id,Make From Make Where Item_Code='"+SMatCode+"' Order By Make");
               while(res.next())
               {
                    VId.addElement(res.getString(1));
                    VMake.addElement(res.getString(2));
               }
               for(int i=0;i<VMake.size();i++)
               {
                    JTA.append((String)VMake.elementAt(i)+"\n");
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          JTA.requestFocus();
     }
     public void setDataIntoVector()
     {
          VName.removeAllElements();
          VCode.removeAllElements();

          String QString = "";
          if(iMillCode==0)
          {
               QString = " Select Item_Name,Item_Code,Uom.UoMName From InvItems "+
                         " Inner Join Uom on InvItems.UomCode=Uom.UomCode "+
                         " Order By Item_Name";
          }
          else
          {
               QString = " Select Item_Name,Item_Code,Uom.UoMName From InvItems "+
                         " Inner Join Uom on InvItems.UomCode=Uom.UomCode "+
                         " Where MillCode = 1 "+
                         " Order By Item_Name";
          }

          try
          {
               Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
               Connection conn  = DriverManager.getConnection(common.getDSN(),"","");
               Statement stat   = conn.createStatement();
               ResultSet res = stat.executeQuery(QString);
               while(res.next())
               {
                    VName.addElement(res.getString(1)+"-"+res.getString(3));
                    VCode.addElement(res.getString(2));
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}
