package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;
public class MaterialsSearch implements ActionListener
{
      JLayeredPane       Layer;
      Vector             VName,VCode;
      Vector             VNameCode;
      String             SName,SCode;
      OrderMiddlePanel   MiddlePanel;
      EnquiryMiddlePanel EnqMiddlePanel;
      QuoMiddlePanel     QuoMiddlePanel;
      MRSMiddlePanel     MRSMiddlePanel;
      DirectIssueTabReport tabreport;

      ServTableModel     MatModel;

      JList          BrowList,SelectedList;
      JScrollPane    BrowScroll,SelectedScroll;
      JTextField     TIndicator;
      JButton        BOk;
      JPanel         LeftPanel,RightPanel;
      JInternalFrame MaterialFrame;
      JPanel         MFMPanel,MFBPanel;
      Vector VSelectedName,VSelectedCode;
      Vector VSelectedNameCode;
      String SFrameTitle,SWhere;
      String str="";
      int iMFSig=0;
      ActionEvent ae;
      Common common = new Common();
      int iMillCode = 0;

      MaterialsSearch(JLayeredPane Layer,Vector VCode,Vector VName,ServTableModel MatModel)
      {
          this.Layer         = Layer;
          this.VName         = VName;
          this.VCode         = VCode;
          this.MatModel      = MatModel;
          this.SFrameTitle   = "Materials Required to Render this Service";
          this.SWhere        = "Service";

          createComponents();
      }
      
      MaterialsSearch(JLayeredPane Layer,Vector VCode,Vector VName,OrderMiddlePanel MiddlePanel,String SFrameTitle,String SWhere,int iMillCode)
      {
          this.Layer         = Layer;
          this.VName         = VName;
          this.VCode         = VCode;
          this.MiddlePanel   = MiddlePanel;
          this.SFrameTitle   = SFrameTitle;
          this.SWhere        = SWhere;
          this.iMillCode     = iMillCode;

          createComponents();
      }
      MaterialsSearch(JLayeredPane Layer,Vector VCode,Vector VName,EnquiryMiddlePanel EnqMiddlePanel,String SFrameTitle,String SWhere,int iMillCode)
      {
          this.Layer         = Layer;
          this.VName         = VName;
          this.VCode         = VCode;
          this.EnqMiddlePanel= EnqMiddlePanel;
          this.SFrameTitle   = SFrameTitle;
          this.SWhere        = SWhere;
          this.iMillCode     = iMillCode;

          createComponents();
      }
      MaterialsSearch(JLayeredPane Layer,Vector VCode,Vector VName,String SFrameTitle,String SWhere,int iMillCode)
      {
          this.Layer         = Layer;
          this.VName         = VName;
          this.VCode         = VCode;
          this.SFrameTitle   = SFrameTitle;
          this.SWhere        = SWhere;
          this.iMillCode     = iMillCode;

          createComponents();
      }

      MaterialsSearch(JLayeredPane Layer,Vector VCode,Vector VName,QuoMiddlePanel QuoMiddlePanel,String SFrameTitle,String SWhere,int iMillCode)
      {
          this.Layer         = Layer;
          this.VName         = VName;
          this.VCode         = VCode;
          this.QuoMiddlePanel= QuoMiddlePanel;
          this.SFrameTitle   = SFrameTitle;
          this.SWhere        = SWhere;
          this.iMillCode     = iMillCode;

          createComponents();
      }

      MaterialsSearch(JLayeredPane Layer,Vector VCode,Vector VName,MRSMiddlePanel MRSMiddlePanel,String SFrameTitle,String SWhere)
      {
          this.Layer         = Layer;
          this.VName         = VName;
          this.VCode         = VCode;
          this.MRSMiddlePanel= MRSMiddlePanel;
          this.SFrameTitle   = SFrameTitle;
          this.SWhere        = SWhere;

          createComponents();
      }

      public void createComponents()
      {
          VNameCode     = new Vector();
          for (int i=0;i<VCode.size();i++)
          {
               SName = (String)VName.elementAt(i);
               SCode = (String)VCode.elementAt(i);
               VNameCode.addElement(common.Pad(SName,40)+" ("+SCode+")");
          }
          BrowList      = new JList(VNameCode);
          BrowList.setFont(new Font("monospaced", Font.PLAIN, 11));
          SelectedList  = new JList();
          BrowScroll    = new JScrollPane(BrowList);
          SelectedScroll= new JScrollPane(SelectedList);
          LeftPanel     = new JPanel(true);
          RightPanel    = new JPanel(true);
          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator.setEditable(false);
          MFMPanel      = new JPanel(true);
          MFBPanel      = new JPanel(true);
          MaterialFrame = new JInternalFrame(SFrameTitle);
          MaterialFrame.show();
          MaterialFrame.setBounds(50,50,650,350);
          MaterialFrame.setClosable(true);
          MaterialFrame.setResizable(true);
          BrowList.addKeyListener(new KeyList());
          BrowList.requestFocus();
      }
      public class ActList implements ActionListener
      {
          public void actionPerformed(ActionEvent e)
          {
               if(VSelectedCode.size()==0)
               {
                    JOptionPane.showMessageDialog(null,"No Material is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                    BrowList.requestFocus();
                    return;
               }
               BOk.setEnabled(false);
               setMiddlePanel();
               removeHelpFrame();
               ((JButton)ae.getSource()).setEnabled(false);
               if(SWhere.equals("Order"))
               {
                   MiddlePanel.MiddlePanel.ReportTable.requestFocus();
               }
               if(SWhere.equals("Enquiry"))
               {
                   EnqMiddlePanel.MiddlePanel.ReportTable.requestFocus();
               }
               if(SWhere.equals("Quo"))
               {
                   QuoMiddlePanel.ReportTable.requestFocus();
               }
               str="";
          }
      }
      public void actionPerformed(ActionEvent ae)
      {
          this.ae = ae;
          VSelectedName     = new Vector();
          VSelectedCode     = new Vector();
          VSelectedNameCode = new Vector();
          TIndicator.setText(str);
          BOk.setEnabled(true);
          if(iMFSig==0)
          {
               MFMPanel.setLayout(new GridLayout(1,2));
               MFBPanel.setLayout(new GridLayout(1,2));
               MFMPanel.add(BrowScroll);
               MFMPanel.add(SelectedScroll);
               MFBPanel.add(TIndicator);
               MFBPanel.add(BOk);
               BOk.addActionListener(new ActList());
               MaterialFrame.getContentPane().add("Center",MFMPanel);
               MaterialFrame.getContentPane().add("South",MFBPanel);
               iMFSig=1;
          }
          removeHelpFrame();
          try
          {
               Layer.add(MaterialFrame);
               MaterialFrame.moveToFront();
               MaterialFrame.setSelected(true);
               MaterialFrame.show();
               BrowList.requestFocus();
               Layer.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
      }  
      public class KeyList extends KeyAdapter
      {
             public void keyReleased(KeyEvent ke)
             {
                  char lastchar=ke.getKeyChar();
                  lastchar=Character.toUpperCase(lastchar);
                  try
                  {
                     if(ke.getKeyCode()==8)
                     {
                        str=str.substring(0,(str.length()-1));
                        setCursor();
                     }
                     else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                     {
                        str=str+lastchar;
                        setCursor();
                     }
                  }
                  catch(Exception ex)
                  {
                         Toolkit.getDefaultToolkit().beep();
                  }
             }
             public void keyPressed(KeyEvent ke)
             {
                  if(ke.getKeyCode()==116)    // F5 is pressed
                  {
                         setDataIntoVector();
                         BrowList.setListData(VNameCode);
                  }
                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                     int index = BrowList.getSelectedIndex();
                     String SMatNameCode = (String)VNameCode.elementAt(index);
                     String SMatName     = (String)VName.elementAt(index);
                     String SMatCode     = (String)VCode.elementAt(index);
                     addMatDet(SMatName,SMatCode,SMatNameCode);
                     str="";
                     TIndicator.setText(str);
                  }
                  if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
                  {
                     setMiddlePanel();
                     removeHelpFrame();
                     ((JButton)ae.getSource()).setEnabled(false);
                     if(SWhere.equals("Order"))
                     {
                         MiddlePanel.MiddlePanel.ReportTable.requestFocus();
                     }
                     if(SWhere.equals("Enquiry"))
                     {
                         EnqMiddlePanel.MiddlePanel.ReportTable.requestFocus();
                     }
                     if(SWhere.equals("Quo"))
                     {
                         QuoMiddlePanel.ReportTable.requestFocus();
                     }
                     str="";
                  }

             }
         }
         public void setCursor()
         {
            int index=0;
            TIndicator.setText(str);
            for(index=0;index<VNameCode.size();index++)
            {
                 String str1 = ((String)VNameCode.elementAt(index)).toUpperCase();
                 if(str1.startsWith(str))
                 {
                      BrowList.setSelectedIndex(index);
                      BrowList.ensureIndexIsVisible(index+10);
                      break;
                 }
            }
            if(index >= VNameCode.size())
               Toolkit.getDefaultToolkit().beep();
         }
         public void removeHelpFrame()
         {
            try
            {
               Layer.remove(MaterialFrame);
               Layer.repaint();
               Layer.updateUI();
            }
            catch(Exception ex) { }
         }

         public boolean addMatDet(String SMatName,String SMatCode,String SMatNameCode)
         {
               int iIndex=VSelectedCode.indexOf(SMatCode);
               if (iIndex==-1)
               {
                    VSelectedName.addElement(SMatName);
                    VSelectedCode.addElement(SMatCode);
                    VSelectedNameCode.addElement(SMatNameCode);
               }
               else
               {
                    VSelectedName.removeElementAt(iIndex);
                    VSelectedCode.removeElementAt(iIndex);
                    VSelectedNameCode.removeElementAt(iIndex);
               }
               SelectedList.setListData(VSelectedNameCode);
               return true;
         }
         public void setMiddlePanel()
         {

            if(SWhere.equals("MRS"))
            {
                MiddlePanel.setRowData(VSelectedCode,VSelectedName);
            }

            if(SWhere.equals("Order"))
            {
                MiddlePanel.setRowData(VSelectedCode,VSelectedName);
                MiddlePanel.createComponents();
            }
            if(SWhere.equals("Enquiry"))
            {
                EnqMiddlePanel.setRowData(VSelectedCode,VSelectedName);
                EnqMiddlePanel.createComponents();
            }
            if(SWhere.equals("Quo"))
            {
                QuoMiddlePanel.setRowData(VSelectedCode,VSelectedName);
                QuoMiddlePanel.createComponents();
            }
            if(SWhere.equals("Service"))
                MatModel.appendElements(VSelectedCode,VSelectedName);
         }

         public void setDataIntoVector()
         {
               VName.removeAllElements();
               VCode.removeAllElements();
               VNameCode.removeAllElements();

               String QString = "";
               if(iMillCode==0)
               {
                    QString = "Select Item_Name,Item_Code From InvItems Order By Item_Name";
               }
               else
               {
                    QString = "Select Item_Name,Item_Code From InvItems Where MillCode = 1 Order By Item_Name";
               }

               try
               {
                    ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                    Connection      theConnection =  oraConnection.getConnection();
                    Statement       stat =  theConnection.createStatement();

                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         String str1 = res.getString(1);
                         String str2 = res.getString(2);
                         VName.addElement(str1);
                         VCode.addElement(str2);
                         VNameCode.addElement(str1+" (Code : "+str2+")");
                    }
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
         }
                                    
}
