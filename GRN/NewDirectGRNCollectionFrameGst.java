package GRN;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class NewDirectGRNCollectionFrameGst extends JInternalFrame
{
     DateField                TDate;
     JTextField               TSupName;
     WholeNumberField         TInvoice;
     NewDirectGRNMiddlePanelGst  MiddlePanel;
     JPanel                   TopPanel,BottomPanel;
     JButton                  BOk,BCancel;
     
     JLayeredPane             DeskTop;
     Vector                   VCode,VName,VNameCode;
     StatusPanel              SPanel;
     int                      iMillCode,iUserCode,iAuthCode, iInvTypeCode;
     Vector                   VSeleGINo;
     String                   SSupCode,SSupName;
     String                   SYearCode;
     String                   SItemTable,SSupTable;
     String		      SSeleSupType,SSeleStateCode;

     JLabel                   LGrnNo;

     Connection               theMConnection = null;
     Connection               theDConnection = null;

     boolean                  bComflag       = true;
     Common                   common         = new Common();

     NewDirectGRNCollectionFrameGst(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iMillCode,int iAuthCode,int iUserCode,Vector VSeleGINo,String SSupCode,String SSupName,String SYearCode,String SItemTable,String SSupTable, int iInvTypeCode,String SSeleSupType,String SSeleStateCode)
     {
          this.DeskTop            = DeskTop;
          this.VCode              = VCode;
          this.VName              = VName;
          this.VNameCode          = VNameCode;
          this.SPanel             = SPanel;
          this.iMillCode          = iMillCode;
          this.iAuthCode          = iAuthCode;
          this.iUserCode          = iUserCode;
          this.VSeleGINo          = VSeleGINo;
          this.SSupCode           = SSupCode;
          this.SSupName           = SSupName;
          this.SYearCode          = SYearCode;
          this.SItemTable         = SItemTable;
          this.SSupTable          = SSupTable;
          this.iInvTypeCode	  = iInvTypeCode;
	  this.SSeleSupType	  = SSeleSupType;
	  this.SSeleStateCode     = SSeleStateCode;

          if(iMillCode==1)
          {
               DORAConnection jdbc           = DORAConnection.getORAConnection();
                              theDConnection = jdbc.getConnection();
          }

          ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                         theMConnection = oraConnection.getConnection();
          
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }

     public void createComponents()
     {
          BOk        = new JButton("Okay");
          BCancel    = new JButton("Abort");
          
          TDate       = new DateField();
          LGrnNo      = new JLabel();
          
          TSupName    = new JTextField();
          TInvoice    = new WholeNumberField(2);
          
          MiddlePanel = new NewDirectGRNMiddlePanelGst(DeskTop,SSupCode,SSupName,iMillCode,SSeleSupType,SSeleStateCode);
          
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();
          
          TDate.setTodayDate();
          
          TDate.setEditable(false);
          TSupName.setEditable(false);
     }

     public void setLayouts()
     {
          setTitle("Invoice Valuation of Materials recd against order");
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          
          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(2,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          try
          {
               TopPanel.add(new JLabel("GRN No"));
               TopPanel.add(LGrnNo);
               
               TopPanel.add(new JLabel("GRN Date"));
               TopPanel.add(TDate);
               
               TopPanel.add(new JLabel("Supplier"));
               TopPanel.add(TSupName);
               
               TopPanel.add(new JLabel("No. of Bills"));
               TopPanel.add(TInvoice);
               
               BottomPanel.add(BOk);
               BottomPanel.add(BCancel);
               getContentPane().add(TopPanel,BorderLayout.NORTH);
               getContentPane().add(MiddlePanel,BorderLayout.CENTER);
               getContentPane().add(BottomPanel,BorderLayout.SOUTH);
               
               setGRNNo();
               
               TSupName.setText(SSupName);
               MiddlePanel.createComponents(VSeleGINo);
          }
          catch(Exception e)
          {
               System.out.println(e);
          }
     }

     public void addListeners()
     {
          BOk.addActionListener(new ActList());
     }

     public void setGRNNo()
     {
          String SGNo = getId();

          LGrnNo.setText(SGNo);
     }

     private String  getId()
     {
          String SId ="";
          String QS ="";
          try
          {
               QS = "  Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 2";

               Statement       stat =  theMConnection.createStatement();
               
               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    SId = res.getString(1);    
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E1"+ex );
          }
          return SId;
     }

     private String  getInsertId()
     {
          String SId ="";
          String QS ="";
          try
          {
               QS = " Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 2 for update of MaxNo noWait";

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               Statement       stat =  theMConnection.createStatement();
               
               PreparedStatement thePrepare = theMConnection.prepareStatement(" Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 2"); 
               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    SId = res.getString(1);    
               }
               res.close();

               thePrepare.setInt(1,common.toInt(SId));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println("E1"+ex );
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getInsertId();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }

          }
          LGrnNo.setText(SId);
          return SId;
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validateGRN())
                    insertGRNCollectedData();
          }
     }

     private void insertGRNCollectedData()
     {
          BOk.setEnabled(false);
          insertGRNDetails();
          setGILink();
          setOrderLink();
          if(iMillCode==1)
          {
               updateSubStoreMasterData();
          }
          setMRSLink();
          getACommit();
          removeHelpFrame();
          setPreviousFrame();
     }

     public void setPreviousFrame()
     {
          NewDirectGRNFrameGst directgrnframegst = new NewDirectGRNFrameGst(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,iAuthCode,iUserCode,SYearCode,SItemTable,SSupTable);
          try
          {
               DeskTop.add(directgrnframegst);
               DeskTop.repaint();
               directgrnframegst.setSelected(true);
               DeskTop.updateUI();
               directgrnframegst.show();
          }
          catch(java.beans.PropertyVetoException ex){}
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public void setGILink()
     {
          try
          {
               Statement       stat =  theMConnection.createStatement();
               
               for(int i=0;i<MiddlePanel.GatePanel.VSeleGINo.size();i++)
               {
                    String    QS = "Update GateInward Set ";
                              QS = QS+" GrnNo=1,GrnNumber="+LGrnNo.getText();
                              QS = QS+" Where GrnNo=0 and Authentication=1 and ModiStatus=0 and MillCode="+iMillCode+" and GINo = "+(String)MiddlePanel.GatePanel.VSeleGINo.elementAt(i);

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E1"+ex);
               bComflag  = false;
          }
     }

     public void insertGRNDetails()
     {
          String QString = "Insert Into GRN (OrderNo,MRSNo,GrnNo,GrnDate,GrnBlock,Sup_Code,GateInNo,GateInDate,InvNo,InvDate,DcNo,DcDate,Code,InvQty,MillQty,Pending,OrderQty,Qty,InvRate,DiscPer,Disc,CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,Cess,CessVal,InvAmount,InvNet,plus,Less,Misc,Dept_Code,Group_Code,Unit_Code,InvSlNo,ActualModVat,NoOfBills,MillCode,id,slno,mrsslno,mrsauthusercode,orderslno,orderapprovalstatus,taxclaimable,usercode,creationdate,grnqty,grnvalue,HsnCode,GstRate,GstPartyTypeCode,GstStateCode,InvoiceType) Values (";
          String QSL     = "Select Max(InvSlNo) From GRN";
          String QS="";
          int iInvSlNo=0,iSlNo=0;
          try
          {

               Statement       stat =  theMConnection.createStatement();
               ResultSet res        = stat.executeQuery(QSL);
               while(res.next())
               {
                    iInvSlNo = res.getInt(1);
               }
               res.close();
               iInvSlNo++;
               
               Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();
               Object InvData[][] = MiddlePanel.InvPanel.getFromVector();
               
               String SAdd        = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess       = MiddlePanel.MiddlePanel.TLess.getText();
               double dOthers     = common.toDouble(MiddlePanel.MiddlePanel.LOthers.getText());
               
               double dpm         = common.toDouble(SAdd)-common.toDouble(SLess)+dOthers;

               double dNet       = common.toDouble(MiddlePanel.MiddlePanel.LNet.getText())-dpm;
               double dRatio     = dpm/dNet;

               String SGrnNo      = getInsertId();

               String SDate    = TDate.toNormal();

               String SDateTime = common.getServerDateTime();

               
               for(int i=0;i<RowData.length;i++)
               {
                    String SSeleId    = (String)MiddlePanel.MiddlePanel.VSeleId.elementAt(i);

                    int iMidRow       = common.exactIndexOf(MiddlePanel.VId,SSeleId);

		    String SHsnType   = (String)MiddlePanel.MiddlePanel.VHsnType.elementAt(i);

                    String SBlockCode     = "";
                    String SOrdQty        = "";
                    String SMrsSLNo       = "";
                    String SMrsUserCode   = "";
                    String SOrderSLNo     = "";
                    String STaxC          = "";
                    String SOrderApproval = "";

		    if(SHsnType.equals("1"))
		    {
		            SBlockCode     = "0";
		            SOrdQty        = "0";
		            SMrsSLNo       = "0";
		            SMrsUserCode   = "0";
		            SOrderSLNo     = "0";
		            STaxC          = "0";
		            SOrderApproval = "0";
		    }
		    else
		    {
		            SBlockCode     = MiddlePanel.getBlockCode(iMidRow);
		            SOrdQty        = MiddlePanel.getOrdQty(iMidRow);
		            SMrsSLNo       = MiddlePanel.getMrsSLNo(iMidRow);
		            SMrsUserCode   = MiddlePanel.getMrsUserCode(iMidRow);
		            SOrderSLNo     = MiddlePanel.getOrderSLNo(iMidRow);
		            STaxC          = MiddlePanel.getTaxC(iMidRow);
		            SOrderApproval = MiddlePanel.getOrderApprovalStatus(iMidRow);
	            }


                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    
		    String SGINo   = "";
		    String SGIDate = "";

		    if(SHsnType.equals("1"))
		    {
                         SGINo       = (String)RowData[0][0];
			 SGIDate     = common.pureDate((String)RowData[0][1]);
		    }
		    else
		    {
                         SGINo       = (String)RowData[i][0];
			 SGIDate     = common.pureDate((String)RowData[i][1]);
		    }
                    
                    int iInvRow        = getRowIndex(SGINo,InvData);
                    
                    String SInvNo      = (String)InvData[iInvRow][2];
                    String SInvDate    = common.pureDate((String)InvData[iInvRow][3]);
                    String SDCNo       = (String)InvData[iInvRow][4];
                    String SDCDate     = common.pureDate((String)InvData[iInvRow][5]);
                    
                    iSlNo++;

                    String SOrderNo   = (String)RowData[i][8];
                    String SMRSNo     = (String)RowData[i][7];
                    
                    String  SItemCode  = (String)RowData[i][3];
                    String  SHsnCode   = ((String)RowData[i][5]).trim();
                    
                    String  SBasic     = (String)RowData[i][19];
                    String  SInvAmount = (String)RowData[i][25];
                    
		    String SMisc      = "0";

		    if(SHsnType.equals("0"))
		    {
                         SMisc = common.getRound(common.toDouble(SInvAmount)*dRatio,3);
		    }
                    
                    String  SMillQty   = (String)RowData[i][12];
                    
                    String SGrnQty   = SMillQty;
                    String SGrnValue = "";
                    
                    SGrnValue = common.getRound(common.toDouble(SInvAmount) + common.toDouble(SMisc),2);

		    String SCGSTPer   = common.getRound(common.toDouble(((String)RowData[i][15]).trim()),2);
		    String SSGSTPer   = common.getRound(common.toDouble(((String)RowData[i][16]).trim()),2);
		    String SIGSTPer   = common.getRound(common.toDouble(((String)RowData[i][17]).trim()),2);
		    String SGstRate   = common.getRound(common.toDouble(SCGSTPer)+common.toDouble(SSGSTPer)+common.toDouble(SIGSTPer),2);

                    String    QS1 = QString;
                              QS1 = QS1+"0"+SOrderNo+",";
                              QS1 = QS1+"0"+SMRSNo+",";
                              QS1 = QS1+"0"+SGrnNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"0"+SBlockCode+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"0"+SGINo+",";
                              QS1 = QS1+"'"+SGIDate+"',";
                              QS1 = QS1+"'"+SInvNo+"',";
                              QS1 = QS1+"'"+SInvDate+"',";
                              QS1 = QS1+"'"+SDCNo+"',";
                              QS1 = QS1+"'"+SDCDate+"',";
                              QS1 = QS1+"'"+SItemCode+"',";
                              QS1 = QS1+"0"+(String)RowData[i][11]+",";
                              QS1 = QS1+"0"+SMillQty+",";
                              QS1 = QS1+"0"+(String)RowData[i][10]+",";
                              QS1 = QS1+"0"+SOrdQty+",";
                              QS1 = QS1+"0"+SMillQty+",";
                              QS1 = QS1+"0"+(String)RowData[i][13]+",";
                              QS1 = QS1+"0"+(String)RowData[i][14]+",";
                              QS1 = QS1+"0"+(String)RowData[i][20]+",";
                              QS1 = QS1+"0"+SCGSTPer+",";
                              QS1 = QS1+"0"+(String)RowData[i][21]+",";
                              QS1 = QS1+"0"+SSGSTPer+",";
                              QS1 = QS1+"0"+(String)RowData[i][22]+",";
                              QS1 = QS1+"0"+SIGSTPer+",";
                              QS1 = QS1+"0"+(String)RowData[i][23]+",";
                              QS1 = QS1+"0"+(String)RowData[i][18]+",";
                              QS1 = QS1+"0"+(String)RowData[i][24]+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SAdd+",";
                              QS1 = QS1+"0"+SLess+",";
                              QS1 = QS1+"0"+SMisc+",";
                              QS1 = QS1+"0"+SDeptCode+",";
                              QS1 = QS1+"0"+SGroupCode+",";
                              QS1 = QS1+"0"+SUnitCode+",";
                              QS1 = QS1+"0"+iInvSlNo+",";
                              QS1 = QS1+"0"+0+",";
                              QS1 = QS1+"0"+TInvoice.getText()+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"grn_seq.nextval"+",";
                              QS1 = QS1+"0"+iSlNo+",";
                              QS1 = QS1+"0"+SMrsSLNo+",";
                              QS1 = QS1+"0"+SMrsUserCode+",";
                              QS1 = QS1+"0"+SOrderSLNo+",";
                              QS1 = QS1+"0"+SOrderApproval+",";
                              QS1 = QS1+"0"+STaxC+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+SDateTime+"',";
                              QS1 = QS1+"0"+SGrnQty+",";
                              QS1 = QS1+"0"+SGrnValue+",";
                    	      QS1 = QS1+"'"+SHsnCode+"',";
                    	      QS1 = QS1+"0"+SGstRate+",";
                    	      QS1 = QS1+"0"+SSeleSupType+",";
                    	      QS1 = QS1+"'"+SSeleStateCode+"',";
                              QS1 = QS1+"0"+iInvTypeCode+" )";

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.executeUpdate(QS1);
                    
		    if(SHsnType.equals("0"))
		    {
                    	updateItemMaster(stat,SItemCode,SBlockCode,SGrnQty,SGrnValue);
			updateHsnMaster(stat,SItemCode,SHsnCode);
		    }

		    if(SHsnType.equals("0"))
		    {
		            if(common.toInt(SBlockCode)<=1)
		            {
		                 updateUserItemStock(stat,SItemCode,SGrnQty,SGrnValue,SMrsUserCode);
		            } 
		    }
               }

               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

     private int getRowIndex(String SGINo,Object InvData[][])
     {
          int iIndex=-1;
          
          for(int i=0;i<InvData.length;i++)
          {
               String SInvGINo = (String)InvData[i][0];
               
               if(!SInvGINo.equals(SGINo))
                    continue;
               
               iIndex=i;
               return iIndex;
          }
          return iIndex;
     }
          
     public void setOrderLink()
     {
          try
          {
               Statement       stat =  theMConnection.createStatement();
               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();                 
               for(int i=0;i<RowData.length;i++)
               {
		    String SId = (String)MiddlePanel.MiddlePanel.VSeleId.elementAt(i);

		    if(SId.equals("0"))
			continue;
		    
                    String  SHsnCode  = ((String)RowData[i][5]).trim();
		    String SCGSTPer   = common.getRound(common.toDouble(((String)RowData[i][15]).trim()),2);
		    String SSGSTPer   = common.getRound(common.toDouble(((String)RowData[i][16]).trim()),2);
		    String SIGSTPer   = common.getRound(common.toDouble(((String)RowData[i][17]).trim()),2);
		    String SGstRate   = common.getRound(common.toDouble(SCGSTPer)+common.toDouble(SSGSTPer)+common.toDouble(SIGSTPer),2);

                    String    QS = "Update PurchaseOrder Set ";
                              QS = QS+" InvQty=InvQty+"+(String)RowData[i][12]+",";
                              QS = QS+" HsnCode='"+SHsnCode+"',";
                              QS = QS+" GstRate=0"+SGstRate+" ";
                              QS = QS+" Where Id = "+(String)MiddlePanel.MiddlePanel.VSeleId.elementAt(i);

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E3"+ex);
               bComflag  = false;
          }
     }
     
     public void setMRSLink()
     {
          try
          {
               Statement       stat =  theMConnection.createStatement();

               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();                 
               for(int i=0;i<RowData.length;i++)
               {
                    String SMrsNo     = (String)RowData[i][7];
                    
                    if(common.toInt(SMrsNo)==0)
                         continue;
                    
		    String SSeleId = (String)MiddlePanel.MiddlePanel.VSeleId.elementAt(i);

		    if(SSeleId.equals("0"))
			continue;

                    int iMidRow       = common.exactIndexOf(MiddlePanel.VId,SSeleId);

                    String SMrsSLNo   = MiddlePanel.getMrsSLNo(iMidRow);
                    
                    String    QS = "Update MRS Set ";
                              QS = QS+" GrnNo="+LGrnNo.getText();
                              QS = QS+" Where MRSNo ="+SMrsNo+" and Item_Code = '"+(String)RowData[i][3]+"' And SlNo="+SMrsSLNo;
                              QS = QS+" And Mrs.MillCode = "+iMillCode;

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E4"+ex);
               bComflag  = false;
          }
     }

     private boolean validateGRN()
     {
          BOk.setEnabled(false);
          boolean bflag=false;
          String str="";
          
          int iInvoice = common.toInt(TInvoice.getText());
          if(iInvoice==0)
          {
               str="No of Bills Field is Empty";
               bflag=true;
          }
          if(isGateNotTicked())
          {
               str = str+"\n"+"Some Gate Entries are Not Closed";
               bflag=true;
          }
          if(isInvoiceNotTicked())
          {
               str = str+"\n"+"Either Invoice or DC must be filled";
               bflag=true;
          }
          if(isNoRowData())
          {
               str = str+"\n"+"No Item Selected for GRN";
               bflag=true;
          }
          
          if(!bflag)
               return true;
          
          AcceptDeny AD = new AcceptDeny(str);
          AD.show();
          AD.BDeny.addActionListener(new ADList(AD));
          return false;                              
     }
     
     private boolean isGateNotTicked()
     {
          for(int i=0;i<MiddlePanel.GatePanel.gateModel.getRows();i++)
          {
               Boolean bValue = (Boolean)MiddlePanel.GatePanel.gateModel.getValueAt(i,5);
               
               if(!bValue.booleanValue())
                    return true;
          }
          return false;
     }
     
     private boolean isInvoiceNotTicked()
     {
          for(int i=0;i<MiddlePanel.InvPanel.invModel.getRows();i++)
          {
               String SInvNo = (String)MiddlePanel.InvPanel.invModel.getValueAt(i,2);
               String SDCNo  = (String)MiddlePanel.InvPanel.invModel.getValueAt(i,4);
               
               if((SInvNo.trim().equals("")) && (SDCNo.trim().equals("")))
                    return true;
          }
          return false;
     }

     private boolean isNoRowData()
     {
          Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();
          
          if(RowData.length==0)
               return true;
          
          return false;
     }

     private class ADList implements ActionListener
     {
          AcceptDeny AD;
          ADList(AcceptDeny AD)
          {
               this.AD = AD;
          }
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==AD.BDeny)
               {
                    BOk.setEnabled(true);
                    AD.setVisible(false);
               }
          }
     }

     private void updateSubStoreMasterData()
     {
          try
          {
               Statement stat   =    theDConnection.createStatement();
               
               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();                 
               for(int i=0;i<RowData.length;i++)
               {
                    String SItemCode  = (String)RowData[i][3];
                    int iCount=0;
                    
                    ResultSet res = stat.executeQuery("Select count(*) from InvItems Where Item_Code='"+SItemCode+"'");
                    while(res.next())
                    {
                         iCount   =    res.getInt(1);
                    }
                    res.close();
                    
                    if(iCount==0)
                         continue;
                         

		    String SSeleId = (String)MiddlePanel.MiddlePanel.VSeleId.elementAt(i);

		    if(SSeleId.equals("0"))
			continue;

                    int iMidRow       = common.exactIndexOf(MiddlePanel.VId,SSeleId);

                    int iBlockCode = common.toInt(MiddlePanel.getBlockCode(iMidRow));
                    
                    String QS = "";
                    
                    if(iBlockCode>1)
                    {
                         QS = "Update InvItems Set ";
                         QS = QS+" MSRecQty=nvl(MSRecQty,0)+"+(String)RowData[i][12]+",";
                         QS = QS+" MSIssQty=nvl(MSIssQty,0)+"+(String)RowData[i][12]+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    else
                    {
                         QS = "Update InvItems Set ";
                         QS = QS+" MSRecQty=nvl(MSRecQty,0)+"+(String)RowData[i][12]+",";
                         QS = QS+" MSStock=nvl(MSStock,0)+"+(String)RowData[i][12]+" ";
                         QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    if(theDConnection  . getAutoCommit())
                         theDConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("E6"+e);
               bComflag  = false;
          }
     }

     private void updateItemMaster(Statement stat,String SItemCode,String SBlockCode,String SGrnQty,String SGrnValue)
     {
          try
          {
               int iBlockCode = common.toInt(SBlockCode);
               
               String QS = "";
               
               if(iBlockCode>1)
               {
                    QS = "Update "+SItemTable+" Set  RecVal =nvl(RecVal,0)+"+SGrnValue+",";
                    QS = QS+" RecQty=nvl(RecQty,0)+"+SGrnQty+",";
                    QS = QS+" IssQty=nvl(IssQty,0)+"+SGrnQty+",";
                    QS = QS+" IssVal=nvl(IssVal,0)+"+SGrnValue+" ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";
               }
               else
               {
                    QS = "Update "+SItemTable+" Set RecVal =nvl(RecVal,0)+"+SGrnValue+",";
                    QS = QS+" RecQty=nvl(RecQty,0)+"+SGrnQty+" ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";
               }

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               stat.execute(QS);
          }
          catch(Exception e)
          {
               System.out.println("E6"+e);
               bComflag  = false;
          }
     }

     private void updateHsnMaster(Statement stat,String SItemCode,String SHsnCode)
     {
          try
          {
	       String SMHsnCode = "";

	       String QS1 = " Select HsnCode from InvItems where Item_Code='"+SItemCode+"'";

               ResultSet result = stat.executeQuery(QS1);
               while(result.next())
               {
                    SMHsnCode = common.parseNull(result.getString(1));
               }
               result.close();

	       if(SMHsnCode.equals(SHsnCode))
		    return;

	       if(SMHsnCode.equals("") || SMHsnCode.equals("0"))
	       {
		    String QS2 = " Update InvItems set HsnCode='"+SHsnCode+"' Where Item_Code='"+SItemCode+"'";

		    if(theMConnection  . getAutoCommit())
		         theMConnection  . setAutoCommit(false);

		    stat.execute(QS2);
	       }
	       else
	       {
		    int iHCount = 0;

		    String QS3a = " Select Count(*) from Grn Where Code='"+SItemCode+"' and HsnCode='"+SMHsnCode+"'";

               	    result = stat.executeQuery(QS3a);
                    while(result.next())
                    {
                         iHCount = result.getInt(1);
                    }
                    result.close();

		    if(iHCount<=0)
		    {
		         String QS3b = " Update InvItems set HsnCode='"+SHsnCode+"' Where Item_Code='"+SItemCode+"'";

		    	 if(theMConnection  . getAutoCommit())
		              theMConnection  . setAutoCommit(false);

		    	 stat.execute(QS3b);
		    }
		    else
		    {
		         int iMCount = 0;

		    	 String QS3 = " Select Count(*) from MultiHsn Where Item_Code='"+SItemCode+"' and HsnCode='"+SHsnCode+"'";

		    	 result = stat.executeQuery(QS3);
		         while(result.next())
		         {
		              iMCount = result.getInt(1);
		         }
		         result.close();

			 if(iMCount>0)
			      return;

		         String SSysName    = common.getLocalHostName();
		         String SServerDate = common.getServerPureDate();


		         String QS4 = " Insert into MultiHsn (ID,ITEM_CODE,HSNCODE,MODIDATE,USERCODE,SYSNAME) "+
			              " Values(MultiHsn_Seq.nextval,'"+SItemCode+"','"+SHsnCode+"',"+SServerDate+","+iUserCode+",'"+SSysName+"') ";

			 String QS5 = " Update InvItems set MultiHsn=1 Where Item_Code='"+SItemCode+"'";

			 if(theMConnection  . getAutoCommit())
			      theMConnection  . setAutoCommit(false);

			 stat.execute(QS4);
			 stat.execute(QS5);
		    }
	       }
          }
          catch(Exception e)
          {
               System.out.println("E6a"+e);
               bComflag  = false;
          }
     }

     private void updateUserItemStock(Statement stat,String SItemCode,String SGrnQty,String SGrnValue,String SMrsUserCode)
     {
          try
          {
               Item IC = new Item(SItemCode,iMillCode,SItemTable,SSupTable);
     
               double dAllStock = common.toDouble(IC.getClStock());
               double dAllValue = common.toDouble(IC.getClValue());
     
               double dRate   = 0;
               try
               {
                    dRate = dAllValue/dAllStock;
               }
               catch(Exception ex)
               {
                    dRate=0;
               }
     
               if(dAllStock==0)
               {
                    dRate = common.toDouble(IC.SRate);
               }

               int iCount=0;

               String QS = " Select count(*) from ItemStock Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+iMillCode;
               
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    iCount   =    res.getInt(1);
               }
               res.close();

               String QS1 = "";

               if(iCount>0)
               {
                    QS1 = "Update ItemStock Set Stock=nvl(Stock,0)+"+SGrnQty+" ";
                    QS1 = QS1+" Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+iMillCode;
               }
               else
               {
                    QS1 = " Insert into ItemStock (MillCode,HodCode,ItemCode,Stock,StockValue) Values (";
                    QS1 = QS1+"0"+iMillCode+",";
                    QS1 = QS1+"0"+SMrsUserCode+",";
                    QS1 = QS1+"'"+SItemCode+"',";
                    QS1 = QS1+"0"+SGrnQty+",";
                    QS1 = QS1+"0"+common.getRound(dRate,4)+")";
               }

               String QS2 = " Update ItemStock set StockValue="+common.getRound(dRate,4)+
                            " Where ItemCode='"+SItemCode+"' and MillCode="+iMillCode;


               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               stat.execute(QS1);
               stat.execute(QS2);
          }
          catch(Exception e)
          {
               System.out.println("E7"+e);
               bComflag  = false;
          }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theMConnection . commit();

                    if(iMillCode==1)
                         theDConnection . commit();

                    JOptionPane    . showMessageDialog(null,"The Entered Data is Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("Commit");
               }
               else
               {
                    theMConnection . rollback();

                    if(iMillCode==1)
                    	theDConnection . rollback();

                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theMConnection   . setAutoCommit(true);

               if(iMillCode==1)
                    theDConnection   . setAutoCommit(true);

          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}

