package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class NewDirectGRNFrame1 extends JInternalFrame
{
     JPanel MiddlePanel,BottomPanel;
     JButton     BOk,BGrn;
     JTabbedPane JTP;
     JComboBox   JCDept;
     TabReport tabreport1,tabreport4,tabreport7;
     JPanel Panel1;
     JPanel Panel1a;
     JButton  BGrn1;
     JComboBox	JInvType;	


     // With P.O.

     Object RowData1[][];
     String ColumnData1[] = {"GI No","Date","Supplier Name","Invoice No","Invoice Date","DC No","DC Date","InvoiceType","Modified","Pass Grn"};
     String ColumnType1[] = {"N","S","S","S","S","S","S","B","S","B"};

     // Cash GRN

     Object RowData4[][];
     String ColumnData4[] = {"GI No","Date","Description","Bill No","Bill Date","Material","Qty","Supplier","Modified","Person","Department"};
     String ColumnType4[] = {"N"    ,"S"   ,"S"          ,"S"      ,"S"        ,"S"       ,"N"  ,"S"       ,"S"       ,"E"     ,"E"         };

     // Transfer Stockable

     Object RowData7[][];
     String ColumnData7[] = {"GI No","Date","Supplier Name","Invoice No","Invoice Date","DC No","DC Date","Modified"};
     String ColumnType7[] = {"N","S","S","S","S","S","S","S"};

     Common  common  = new Common();

     JLayeredPane DeskTop;
     StatusPanel SPanel;
     Vector VCode,VName,VNameCode,VDeptCode,VDeptName;
     
     Vector VGINo1,VGIDate1,VSupName1,VDCNo1,VDCDate1,VInvNo1,VInvDate1,VSupCode1,VInwNo1;
     Vector VGINo4,VGIDate4,VDesc4,VInvNo4,VInvDate4,VInwNo4,VId4,VSup_Name4,VSup_Code4,VPerson4,VDepartment4;
     Vector VCode4,VRate4,VQty4,VNet4,VMrsId4,VIssStatus4;
     Vector VGINo7,VGIDate7,VSupName7,VDCNo7,VDCDate7,VInvNo7,VInvDate7,VSupCode7,VInwNo7;
     Vector VInvTypeCode,VInvTypeName;	

     Vector VInwName,VInwNo;
     Vector VSupplier,VSupCode;

     Vector VSelectedGINo;
     String SSeleSupCode="";
     String SSeleSupName="";

     InwModiFrame imframe;
     CashGrnMrsLinkFrame cashgrnmrslinkframe;
     CodeSelectionFrame codeselectionframe;

     DateField df = new DateField();
     int iMillCode,iAuthCode,iUserCode;
     String SYearCode;
     String SItemTable,SSupTable;
     int iInvTypeCode=-1;	

     public  NewDirectGRNFrame1(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iMillCode,int iAuthCode,int iUserCode,String SYearCode,String SItemTable,String SSupTable)
     {
         super("Materials Received @ Gate But Not Accounted In Stores");
         this.DeskTop    = DeskTop;
         this.SPanel     = SPanel;
         this.VCode      = VCode;
         this.VName      = VName;
         this.VNameCode  = VNameCode;
         this.iMillCode  = iMillCode;
         this.iAuthCode  = iAuthCode;
         this.iUserCode  = iUserCode;
         this.SYearCode  = SYearCode;
         this.SItemTable = SItemTable;
         this.SSupTable  = SSupTable;

         setVectors();
	   setInvoiceTypeVector();
         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     public void createComponents()
     {
          MiddlePanel  = new JPanel();
          BottomPanel  = new JPanel();
          Panel1       = new JPanel();
          Panel1a      = new JPanel();
          JTP          = new JTabbedPane();
          JCDept       = new JComboBox(VDeptName);
          BOk          = new JButton("Modify Gate-Inward");
          BGrn1        = new JButton("Pass Grn");

	    JInvType     = new JComboBox(VInvTypeName);
          df.setTodayDate();
     }

     public void setLayouts()
     {
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,650,500);
         MiddlePanel.setLayout(new BorderLayout());
         Panel1.setLayout(new BorderLayout());
     }

     public void addComponents()
     {
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

          MiddlePanel.add("Center",JTP);
          if(iAuthCode>1)
          {
               BottomPanel.add(BOk);
          }

          setDataIntoVector();
          setRowData();

          try
          {
             tabreport1 = new TabReport(RowData1,ColumnData1,ColumnType1);
             tabreport1.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
             ((TableColumn)tabreport1.ReportTable.getColumn("Supplier Name")).setPreferredWidth(250);
             TableColumn invTypeColumn  = tabreport1.ReportTable.getColumn("InvoiceType");
      	 invTypeColumn.setCellEditor(new DefaultCellEditor(JInvType));
             tabreport1.ReportTable.addKeyListener(new KeyList1());


             tabreport4 = new TabReport(RowData4,ColumnData4,ColumnType4);
             tabreport4.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                 ((TableColumn)tabreport4.ReportTable.getColumn("Description")).setPreferredWidth(250);
             tabreport4.ReportTable.addKeyListener(new KeyList4());

             tabreport7 = new TabReport(RowData7,ColumnData7,ColumnType7);
             tabreport7.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                 ((TableColumn)tabreport7.ReportTable.getColumn("Supplier Name")).setPreferredWidth(250);
             tabreport7.ReportTable.addKeyListener(new KeyList7());

             TableColumn deptColumn  = tabreport4.ReportTable.getColumn("Department");
             deptColumn.setCellEditor(new DefaultCellEditor(JCDept));

             Panel1a.add("Center",BGrn1);
             Panel1.add("Center",tabreport1);
             Panel1.add("South",Panel1a);

             JTP.addTab("Materials With and Without PO",Panel1);
             JTP.addTab("Cash GRN",tabreport4);
             JTP.addTab("Stock Transfer",tabreport7);

             setSelected(true);
             DeskTop.repaint();
             DeskTop.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println("Prob"+ex);
          }
     }
     public void addListeners()
     {
          BOk.addActionListener(new ModiList());
          BGrn1.addActionListener(new ActList());                    
     }
     private class ModiList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(false);
               updateGateInward();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BGrn1)
               {
                    if(isValidData())
                    {
                          try
                          {
					System.out.println("Inside Valid Data Inv Type:--"+iInvTypeCode);


                               NewDirectGRNCollectionFrame directgrncollectionframe = new NewDirectGRNCollectionFrame(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,iAuthCode,iUserCode,VSelectedGINo,SSeleSupCode,SSeleSupName,SYearCode,SItemTable,SSupTable,iInvTypeCode);
                               DeskTop.add(directgrncollectionframe);
                               directgrncollectionframe.show();
                               directgrncollectionframe.moveToFront();
                               directgrncollectionframe.setMaximum(true);
                               DeskTop.repaint();
                               DeskTop.updateUI();
                          }
                          catch(Exception e){System.out.println("In DirectGRn :"+e);}

                          removeHelpFrame();
                    }
               }
          }
     }

     private boolean isValidData()
     {
          int iCount = 0;
          try
          {
               for(int i=0;i<RowData1.length;i++)
               {
                    Boolean BSelected = (Boolean)RowData1[i][9];
                    if(BSelected.booleanValue())
                    {
                         iCount++;
                    }
               }

               if(iCount==0)
               {
                    JOptionPane.showMessageDialog(null,"No Row Selected","Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }
               else
               {
                    String SSupName="";
                    String SPSupName="";
			  String SInvType ="";	
                    int iRowCount=0;
			  

                    VSelectedGINo = new Vector();

                    for(int i=0;i<RowData1.length;i++)
                    {
                         Boolean BSelected = (Boolean)RowData1[i][9];
                         if(BSelected.booleanValue())
                         {
                              iRowCount++;

                              SSupName     = (String)RowData1[i][2];
                              String SGINo = (String)RowData1[i][0];
					SInvType	 = (String)RowData1[i][7];
			            iInvTypeCode = common.toInt((String)VInvTypeCode.elementAt((VInvTypeName.indexOf((String)RowData1[i][7]))));
					System.out.println("Inv Type:"+SInvType+"--"+iInvTypeCode);

                              if(iRowCount==1)
                              {
                                   SPSupName = SSupName;
                              }
                              else
                              {
                                   if(!SSupName.equals(SPSupName))
                                   {
                                        JOptionPane.showMessageDialog(null,"Supplier Mismatch","Error",JOptionPane.ERROR_MESSAGE);
                                        return false;
                                   }
                              }
                              SSeleSupCode = (String)VSupCode1.elementAt(i);
                              SSeleSupName = SSupName;
                              VSelectedGINo.addElement(SGINo);
                         }
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          return true;
     }

     private void updateGateInward()
     {
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
          
               for(int i=0;i<RowData1.length;i++)
               {
                    Boolean bValue = (Boolean)RowData1[i][9];
                    if(!bValue.booleanValue())
                         continue;

                    String SSupCode = (String)VSupCode1.elementAt(i);
                    String SInwNo   = (String)VInwNo1.elementAt(i);
                    String SGINo    = (String)VGINo1.elementAt(i);
     
                    String QS = "Update GateInward Set ";
                    QS = QS+" Sup_Code = '"+SSupCode+"',";
                    QS = QS+" InwNo    ="+SInwNo;
                    QS = QS+" Where Authentication=1 and ModiStatus=0 and GINo = "+SGINo+" and MillCode="+iMillCode;

                    stat.execute(QS);
               }
               for(int i=0;i<RowData4.length;i++)
               {
                    Boolean bValue = (Boolean)RowData4[i][8];
     
                    if(!bValue.booleanValue())
                         continue;

                    String SDept = ((String)RowData4[i][10]).trim();
                    if(SDept.equals(""))
                    {
                         JOptionPane.showMessageDialog(null,"Department Field is Empty","Attention",JOptionPane.INFORMATION_MESSAGE);
                         tabreport4.ReportTable.requestFocus();
                         BOk.setEnabled(true);
                         return;
                    }

                    String SGIId     = (String)VId4.elementAt(i);
                    String SGINo     = (String)VGINo4.elementAt(i);
                    String SPerson   = (String)RowData4[i][9];
                    String SItemName = common.getNarration((String)RowData4[i][5]);
                    String SQty      = (String)RowData4[i][6];
                    int    iDeptCode = common.toInt((String)VDeptCode.elementAt((VDeptName.indexOf((String)RowData4[i][10]))));

                    String QS = "Update GateInward Set ";
                    QS = QS+" Status     = 1 , ";
                    QS = QS+" StatusDate = '"+common.getCurrentDate()+"',";
                    QS = QS+" Person     = '"+SPerson+"',";
                    QS = QS+" Item_Name  = '"+SItemName+"',";
                    QS = QS+" GateQty    = 0"+SQty+",";
                    QS = QS+" DeptCode   = 0"+iDeptCode;
                    QS = QS+" Where Id   = "+SGIId+" and MillCode="+iMillCode;

                    stat.execute(QS);

                    Vector VMrsId = (Vector)VMrsId4.elementAt(i);
                    for(int j=0;j<VMrsId.size();j++)
                    {
                         String SMrsId = (String)VMrsId.elementAt(j);
                         if(SMrsId.equals("0"))
                              continue;

                         String QS1 = "Update MRS Set ";
                         QS1 = QS1+" OrderNo = "+SGINo+", ";
                         QS1 = QS1+" OrderBlock = 99 ";
                         QS1 = QS1+" Where Id = "+SMrsId+" and MillCode="+iMillCode;

                         stat.execute(QS1);
                    }

               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          removeHelpFrame();
     }

     private void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex){}
     }
     public void setDataIntoVector()
     {
          String QString1  = "";
          String QString4  = "";
          String QString7  = "";
          String QStringInv  = "";

          QString1 = " SELECT GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name, "+
                     " GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate, "+
                     " GateInward.Sup_Code,GateInward.InwNo "+
                     " From (GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                     " Where GateInward.InwNo = 0 and GateInward.ModiStatus=0 "+
                     " And GateInward.MillCode = "+iMillCode+" and GateInward.Authentication =1"+
                     " Group By GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.GrnNo,GateInward.Sup_Code,GateInward.InwNo "+
                     " Having GateInward.GrnNo = 0 Order By 3";

          QString4 = " SELECT GateInward.GINo,GateInward.GIDate,GateInward.InvNo,GateInward.InvDate,GateInward.Item_Name,GateInward.Id,GateInward.GateQty,"+SSupTable+".Name,GateInward.Person, "+
                     " GateInward.deptcode,GateInward.Sup_Code,GateInward.IssueStatus,'' as Item_Code "+
                     " From GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code "+
                     " Where GateInward.InwNo = 3 And GateInward.GrnNo = 0 And ( GateInward.Status <> 1 Or GateInward.Status is Null ) and GateInward.Authentication =1 and GateInward.ModiStatus=0 "+
                     " And (trim(GateInward.Item_Code) is Null) "+
                     " And GateInward.MillCode = "+iMillCode+
                     " Union All "+
                     " SELECT GateInward.GINo,GateInward.GIDate,GateInward.InvNo,GateInward.InvDate,InvItems.Item_Name,GateInward.Id,GateInward.GateQty,"+SSupTable+".Name,GateInward.Person, "+
                     " GateInward.deptcode,GateInward.Sup_Code,GateInward.IssueStatus,GateInward.Item_Code "+
                     " From (GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                     " Inner Join InvItems on GateInward.Item_Code=InvItems.Item_Code "+
                     " Where GateInward.InwNo = 3 And GateInward.GrnNo = 0 And ( GateInward.Status <> 1 Or GateInward.Status is Null ) "+
                     " And (trim(GateInward.Item_Name) is Null) "+
                     " And GateInward.MillCode = "+iMillCode+"  and GateInward.Authentication =1 and GateInward.ModiStatus=0 "+
                     " Order By 1,3";

          QString7 = " SELECT GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.Sup_Code,GateInward.InwNo "+
                     " From (GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                     " Where GateInward.InwNo = 13  "+
                     " And GateInward.MillCode = "+iMillCode+" and GateInward.Authentication =1 and GateInward.ModiStatus=0 "+
                     " Group By GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.GrnNo,GateInward.Sup_Code,GateInward.InwNo "+
                     " Having GateInward.GrnNo = 0 Order By 3";
	
            VGINo1    = new Vector();
            VGIDate1  = new Vector();
            VSupName1 = new Vector();
            VDCNo1    = new Vector();
            VDCDate1  = new Vector();
            VInvNo1   = new Vector();
            VInvDate1 = new Vector();
            VSupCode1 = new Vector();
            VInwNo1   = new Vector();


            VGINo4    = new Vector();
            VGIDate4  = new Vector();
            VInvNo4   = new Vector();
            VInvDate4 = new Vector();
            VDesc4    = new Vector();
            VCode4    = new Vector();
            VInwNo4   = new Vector();
            VId4      = new Vector();
            VQty4     = new Vector();
            VRate4    = new Vector();
            VNet4     = new Vector();
            VSup_Name4= new Vector();
            VPerson4  = new Vector();
            VSup_Code4= new Vector();
            VMrsId4   = new Vector();
            VIssStatus4=new Vector();

            VGINo7    = new Vector();
            VGIDate7  = new Vector();
            VSupName7 = new Vector();
            VDCNo7    = new Vector();
            VDCDate7  = new Vector();
            VInvNo7   = new Vector();
            VInvDate7 = new Vector();
            VSupCode7 = new Vector();
            VInwNo7   = new Vector();

            try
            {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(QString1);
               while (res.next())
               {
                    VGINo1   .addElement(""+ res.getString(1));
                    VGIDate1 .addElement(""+ common.parseDate(res.getString(2)));
                    VSupName1.addElement(""+ res.getString(3));
                    VDCNo1   .addElement(""+ res.getString(4));
                    VDCDate1 .addElement(""+ common.parseDate(res.getString(5)));
                    VInvNo1  .addElement(""+ res.getString(6));
                    VInvDate1.addElement(""+ common.parseDate(res.getString(7)));
                    VInvDate1.addElement("");
                    VSupCode1.addElement(res.getString(8));
                    VInwNo1.addElement(res.getString(9));
               }
               res.close();

               
               ResultSet res4 = stat.executeQuery(QString4);
               while (res4.next())
               {
                    VGINo4   .addElement(""+ res4.getString(1));
                    VGIDate4 .addElement(""+ common.parseDate(res4.getString(2)));
                    VInvNo4  .addElement(""+ res4.getString(3));
                    VInvDate4.addElement(""+ common.parseDate(res4.getString(4)));
                    VDesc4.addElement(""+res4.getString(5));
                    VId4.addElement(res4.getString(6));
                    VInwNo4.addElement("3");
                    VRate4.addElement("0");
                    VQty4.addElement(res4.getString(7));
                    VNet4.addElement("0");
                    VSup_Name4.addElement(common.parseNull(res4.getString(8)));
                    VPerson4.addElement("");
                    VSup_Code4.addElement(res4.getString(11));
                    VIssStatus4.addElement(res4.getString(12));
                    VCode4.addElement(res4.getString(13));
                    VMrsId4.addElement("");
               }
               res4.close();

               ResultSet res7 = stat.executeQuery(QString7);
               while (res7.next())
               {          
                    VGINo7   .addElement(""+ res7.getString(1));
                    VGIDate7 .addElement(""+ common.parseDate(res7.getString(2)));
                    VSupName7.addElement(""+ res7.getString(3));
                    VDCNo7   .addElement(""+ res7.getString(4));
                    VDCDate7 .addElement(""+ common.parseDate(res7.getString(5)));
                    VInvNo7  .addElement(""+ res7.getString(6));
                    VInvDate7.addElement(""+ common.parseDate(res7.getString(7)));
                    VSupCode7.addElement(res7.getString(8));
                    VInwNo7.addElement(res7.getString(9));
               }
               res7.close();
               stat.close();
           }
           catch(Exception ex){System.out.println("sql "+ex);}
     }
     public void setRowData()
     {
         RowData1     = new Object[VGINo1.size()][ColumnData1.length];
         RowData4     = new Object[VGINo4.size()][ColumnData4.length];
         RowData7     = new Object[VGINo7.size()][ColumnData7.length];

         for(int i=0;i<VGINo1.size();i++)
         {
               RowData1[i][0]  = (String)VGINo1.elementAt(i);
               RowData1[i][1]  = (String)VGIDate1.elementAt(i);
               RowData1[i][2]  = (String)VSupName1.elementAt(i);
               RowData1[i][3]  = (String)VInvNo1.elementAt(i);
               RowData1[i][4]  = (String)VInvDate1.elementAt(i);
               RowData1[i][5]  = (String)VDCNo1.elementAt(i);
               RowData1[i][6]  = (String)VDCDate1.elementAt(i);
//	         RowData1[i][7]  = (String)VInvName.elementAt(i);
	         RowData1[i][7]  = "";
               RowData1[i][8]  = new Boolean(false);
               RowData1[i][9]  = new Boolean(false);
        }
        for(int i=0;i<VGINo4.size();i++)
        {
               RowData4[i][0]  = (String)VGINo4.elementAt(i);
               RowData4[i][1]  = (String)VGIDate4.elementAt(i);
               RowData4[i][2]  = (String)VDesc4.elementAt(i);
               RowData4[i][3]  = (String)VInvNo4.elementAt(i);
               RowData4[i][4]  = (String)VInvDate4.elementAt(i);
               RowData4[i][5]  = "";
               RowData4[i][6]  = (String)VQty4.elementAt(i);
               RowData4[i][7]  = common.parseNull((String)VSup_Name4.elementAt(i));
               RowData4[i][8]  = new Boolean(false);
               RowData4[i][9]  = "";
               RowData4[i][10] = "";
        }
        for(int i=0;i<VGINo7.size();i++)
        {
               RowData7[i][0]  = (String)VGINo7.elementAt(i);
               RowData7[i][1]  = (String)VGIDate7.elementAt(i);
               RowData7[i][2]  = (String)VSupName7.elementAt(i);
               RowData7[i][3]  = (String)VInvNo7.elementAt(i);
               RowData7[i][4]  = (String)VInvDate7.elementAt(i);
               RowData7[i][5]  = (String)VDCNo7.elementAt(i);
               RowData7[i][6]  = (String)VDCDate7.elementAt(i);
               RowData7[i][7]  = new Boolean(false);
        }  
     }
     public class KeyList1 extends KeyAdapter
     {
           public void keyPressed(KeyEvent ke)
           {
                if (ke.getKeyCode() == KeyEvent.VK_INSERT)
                {
                    int iRow = tabreport1.ReportTable.getSelectedRow();
                    String SGINo = (String)VGINo1.elementAt(iRow);
                    int iCount = checkPrevGRN(SGINo);
                    if(iCount<=0)
                    {
                         showInwModiFrame(1);
                    }
                }
           } 
     }

     private int checkPrevGRN(String SGINo)
     {
          int iCount=0;

          String QS = " Select Count(*) from GateInward where GrnNo=1 and Authentication=1 and ModiStatus=0 and GINo="+SGINo+" and MillCode="+iMillCode;

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();

              ResultSet result = stat.executeQuery(QS);
              while(result.next())
              {
                    iCount = result.getInt(1);
              }
              result.close();
              stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return iCount;
     }

     public class KeyList4 extends KeyAdapter
     {
           public void keyPressed(KeyEvent ke)
           {
                if (ke.getKeyCode() == KeyEvent.VK_INSERT)
                {
                    showCashGRNFrame();
                }
                if(iMillCode==1)
                {
                     if (ke.getKeyCode() == KeyEvent.VK_HOME)
                     {
                         showCodeSelectionFrame();
                     }
                }
           } 
     }

     public class KeyList7 extends KeyAdapter
     {
           public void keyPressed(KeyEvent ke)
           {
                if (ke.getKeyCode()==10)
                {
                     try
                     {
                         JTable theTable = (JTable)ke.getSource();
                         int i = theTable.getSelectedRow();
                         String SIndex = String.valueOf(i);
                         String SSupCode = (String)VSupCode7.elementAt(i);
                         GRNTransferFrame grntransferframe = new GRNTransferFrame(DeskTop,VCode,VName,SPanel,SIndex,theTable,SSupCode,iMillCode,iUserCode,SItemTable,SSupTable,SYearCode);
                         DeskTop.add(grntransferframe);
                         grntransferframe.show();
                         grntransferframe.moveToFront();
                         grntransferframe.setMaximum(true);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                     }
                     catch(Exception e){}
                }
           } 
     }

     private void setVectors()
     {
          VInwName   = new Vector();
          VInwNo     = new Vector();
          VSupplier  = new Vector();
          VSupCode   = new Vector();
          VDeptCode  = new Vector();
          VDeptName  = new Vector();

          String QS1 = "Select InwName,InwNo From InwType Where InwNo Not in (12) Order By 2";
          String QS2 = "Select Name,Ac_Code From "+SSupTable+" Order By 1";
          String QS3 = "Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();

              ResultSet result1 = stat.executeQuery(QS1);
              while(result1.next())
              {
                    VInwName.addElement(result1.getString(1));
                    VInwNo.addElement(result1.getString(2));
              }
              result1.close();

              ResultSet result2 = stat.executeQuery(QS2);
              while(result2.next())
              {
                    VSupplier.addElement(result2.getString(1));
                    VSupCode.addElement(result2.getString(2));
              }
              result2.close();

              ResultSet result3 = stat.executeQuery(QS3);
              while(result3.next())
              {
                    VDeptName.addElement(result3.getString(1));
                    VDeptCode.addElement(result3.getString(2));
              }
              result3.close();
              stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void showInwModiFrame(int iSignal)
     {
          try
          {
               DeskTop.remove(imframe);
               DeskTop.updateUI();
          }
          catch(Exception ex){}

          try
          {
               Frame dummy = new Frame();
               JDialog theDialog = new JDialog(dummy,"Description Dialog",true);
               if(iSignal==1)
                    imframe = new InwModiFrame(DeskTop,VSupCode,VSupplier,VInwNo,VInwName,VSupCode1,VInwNo1,tabreport1,theDialog,iMillCode,"Direct",SSupTable);

               theDialog.getContentPane().add(imframe.ModiPanel);
               theDialog.setBounds(80,100,450,350);
               theDialog.setVisible(true);

          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private void showCashGRNFrame()
     {
          try
          {
               DeskTop.remove(cashgrnmrslinkframe);
               DeskTop.updateUI();
          }
          catch(Exception ex){}

          try
          {
               String SMatName = ""+tabreport4.ReportTable.getValueAt(tabreport4.ReportTable.getSelectedRow(),5);
			   
               Frame dummy = new Frame();
               JDialog theDialog = new JDialog(dummy,"Description Dialog",true);
               cashgrnmrslinkframe = new CashGrnMrsLinkFrame(DeskTop,VInwNo,VInwName,tabreport4,VCode,VName,VNameCode,VInwNo4,VDesc4,VCode4,VQty4,VSup_Name4,VMrsId4,VIssStatus4,theDialog,iMillCode,SItemTable);
               if((SMatName.equals("")) || (SMatName.equals("null")) || (SMatName.equals(" ")))
                  cashgrnmrslinkframe.BMaterial.setText("Material");
               else
                  cashgrnmrslinkframe.BMaterial.setText(SMatName);  
               theDialog.getContentPane().add(cashgrnmrslinkframe.CashPanel);
               theDialog.setBounds(80,100,450,350);
               theDialog.setVisible(true);
          }
          catch(Exception ex){}
     }

     private void showCodeSelectionFrame()
     {
          try
          {
               DeskTop.remove(codeselectionframe);
               DeskTop.updateUI();
          }
          catch(Exception ex){}

          try
          {
               String SMatName = ""+tabreport4.ReportTable.getValueAt(tabreport4.ReportTable.getSelectedRow(),5);
			   
               Frame dummy = new Frame();
               JDialog theDialog = new JDialog(dummy,"Description Dialog",true);
               codeselectionframe = new CodeSelectionFrame(DeskTop,VInwNo,VInwName,tabreport4,VCode,VName,VNameCode,VInwNo4,VDesc4,VCode4,VQty4,VSup_Name4,VId4,VIssStatus4,theDialog,iMillCode,SItemTable);
               if((SMatName.equals("")) || (SMatName.equals("null")) || (SMatName.equals(" ")))
                  codeselectionframe.BMaterial.setText("Material");
               else
                  codeselectionframe.BMaterial.setText(SMatName);  
               theDialog.getContentPane().add(codeselectionframe.CashPanel);
               theDialog.setBounds(80,100,450,350);
               theDialog.setVisible(true);
          }
          catch(Exception ex){}
     }
  public void setInvoiceTypeVector()
      {
          VInvTypeCode      = new Vector();
          VInvTypeName      = new Vector();
              
               String QString = "Select TypeName,TypeCode From InvoiceType Order By TypeCode";
//               System.out.println("Qry:"+QString);
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       stat =  theConnection.createStatement();
                    
                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         VInvTypeName.addElement(res.getString(1));
                         VInvTypeCode.addElement(res.getString(2));
                    }
                    res            . close();
                    stat           . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
      }

 public int getInvTypeCode(int i)                       
     {
       
        int     iInvTypeCode = common.toInt((String)VInvTypeCode.elementAt((VInvTypeName.indexOf((String)RowData1[i][7]))));
	  return  iInvTypeCode;
     }




}
