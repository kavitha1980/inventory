package GRN;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class NewDirectGRNGateModel extends DefaultTableModel
{
        Object    RData[][],CNames[],CType[];
        Common common = new Common();
        public NewDirectGRNGateModel(Object[][] RData, Object[] CNames,Object[] CType)
        {
            super(RData,CNames);
            this.RData       = RData;
            this.CNames      = CNames;
            this.CType       = CType;

            for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
            }
        }
       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }            
       public boolean isCellEditable(int row,int col)
       {
               if(CType[col]=="B" || CType[col]=="E")
                  return true;
               return false;
       }
       public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }
    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][CNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
             {
                if(j==5)
                {
                  FinalData[i][j] = (Boolean)curVector.elementAt(j);
                }
                else
                {
                  FinalData[i][j] = ((String)curVector.elementAt(j)).trim();
                }
             }
        }
        return FinalData;
    }
    public int getRows()
    {
         return super.dataVector.size();
    }
    public Vector getCurVector(int i)
    {
         return (Vector)super.dataVector.elementAt(i);
    }

}
