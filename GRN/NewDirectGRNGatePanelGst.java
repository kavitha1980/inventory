// Used By Order,GRN  and other operations
// The Operation Varies with the state of bSig.
// Initially it is set to true once called from Non-Order activities,
// the same shall be set to false.

package GRN;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class NewDirectGRNGatePanelGst extends JPanel
{
   JTable               GateTable;
   NewDirectGRNGateModelGst  gateModel;

   JPanel         GridPanel,BottomPanel,ControlPanel;
   JPanel         GridBottom;

   JButton        BAdd,BDelete;

   JLayeredPane   DeskTop;
   Object         RData[][];
   String         CData[],CType[];
   int            iMillCode;
   String         SSupCode,SSupName;
   Vector         VSeleGINo,VPendCode,VPendName,theVector;
   NewDirectGRNMiddlePanelGst  MiddlePanel;
   Vector VPOrderNo,VPOrderBlock,VPMrsNo,VPOrderCode,VPOrderName,VPOrderQty,VPQtyAllow;
   NewDirectGRNInvPanelGst InvPanel;

   Common common = new Common();

   Vector VGINo,VGIDate;
   Vector VAIName,VADCQty,VAQty,VAGId,VAGIDate,VAGINo;
   Vector VDeleGINo;

   Vector VAIGIDate,VAIGINo,VAIInvNo,VAIInvDate,VAIDCNo,VAIDCDate;

   GIAddFrameGst    giaddframe;
   GIDeleteFrameGst gideleteframe;
   PendingMaterialFrameGst pendingmaterialframe;

   // Constructor method referred in GRN Collection Frame Operations
    NewDirectGRNGatePanelGst(JLayeredPane DeskTop,Object RData[][],String CData[],String CType[],int iMillCode,Vector VSeleGINo,String SSupCode,String SSupName,Vector VPendCode,Vector VPendName,Vector theVector,NewDirectGRNMiddlePanelGst MiddlePanel,Vector VPOrderNo,Vector VPOrderBlock,Vector VPMrsNo,Vector VPOrderCode,Vector VPOrderName,Vector VPOrderQty,Vector VPQtyAllow,NewDirectGRNInvPanelGst InvPanel)
    {
         this.DeskTop      = DeskTop;
         this.RData        = RData;
         this.CData        = CData;
         this.CType        = CType;
         this.iMillCode    = iMillCode;
         this.VSeleGINo    = VSeleGINo;
         this.SSupCode     = SSupCode;
         this.SSupName     = SSupName;
         this.VPendCode    = VPendCode;
         this.VPendName    = VPendName;
         this.theVector    = theVector;
         this.MiddlePanel  = MiddlePanel;
         this.VPOrderNo    = VPOrderNo;
         this.VPOrderBlock = VPOrderBlock;
         this.VPMrsNo      = VPMrsNo;
         this.VPOrderCode  = VPOrderCode;
         this.VPOrderName  = VPOrderName;
         this.VPOrderQty   = VPOrderQty;
         this.VPQtyAllow   = VPQtyAllow;
         this.InvPanel     = InvPanel;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
         setReportTable();
    }
    public void createComponents()
    {
         GridPanel        = new JPanel(true);
         GridBottom       = new JPanel(true);
         BottomPanel      = new JPanel();
         ControlPanel     = new JPanel();

         BAdd    = new JButton("Add New G.I.");
         BDelete = new JButton("Delete G.I.");
     }
          
     public void setLayouts()
     {
         GridPanel.setLayout(new GridLayout(1,1));
         BottomPanel.setLayout(new BorderLayout());
         ControlPanel   . setLayout(new FlowLayout());
         GridBottom.setLayout(new BorderLayout());         
     }
     public void addComponents()
     {
         ControlPanel   . add(BAdd);
         ControlPanel   . add(BDelete);

         BottomPanel.add("North",ControlPanel);
     }
     public void addListeners()
     {
         BAdd. addActionListener(new ActList());
         BDelete. addActionListener(new ActList());
     }
     public class ActList implements ActionListener
     {
            public void actionPerformed(ActionEvent ae)
            {
                 if(ae.getSource()==BAdd)
                 {
                      setDataIntoVector();
                      if(VGINo.size()>0)
                      {
                           BAdd.setEnabled(false);
                           BDelete.setEnabled(false);
                           showGIAddFrame();
                      }
                      else
                      {
                           JOptionPane.showMessageDialog(null,"No GI Pending","Error",JOptionPane.ERROR_MESSAGE);
                      }
                 }
                 if(ae.getSource()==BDelete)
                 {
                      getDeletedGINo();
                      if(VDeleGINo.size()>0)
                      {
                           BAdd.setEnabled(false);
                           BDelete.setEnabled(false);
                           showGIDeleteFrame();
                      }
                      else
                      {
                           JOptionPane.showMessageDialog(null,"No GI For Deletion","Error",JOptionPane.ERROR_MESSAGE);
                      }
                 }
            }
     }
     public void getDeletedGINo()
     {
          VDeleGINo = new Vector();

          for(int i=0;i<VSeleGINo.size();i++)
          {
              int iCount=0;
              int iGrnCount=0;

              String SGINo = (String)VSeleGINo.elementAt(i);

              for(int j=0;j<gateModel.getRows();j++)
              {
                  String SMGINo = (String)gateModel.getValueAt(j,0);
                  if(!SMGINo.equals(SGINo))
                      continue;

                  Boolean bValue = (Boolean)gateModel.getValueAt(j,5);
                  if(bValue.booleanValue())
                  {
                      iCount++;
                      continue;
                  }
              }
              int iRows = MiddlePanel.MiddlePanel.dataModel.getRows();

              if(iRows==0)
              {
                  iGrnCount=0;
              }
              else
              {
                   Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();

                   for(int j=0;j<RowData.length;j++)
                   {
                        String SGrnGINo   = (String)RowData[j][0];
                        if(!SGrnGINo.equals(SGINo))
                            continue;
     
                        iGrnCount++;
                   }
              }

              if(iCount==0 && iGrnCount==0)
              {
                  VDeleGINo.add(SGINo);
              }
          }
     }
     public void setDataIntoVector()
     {
           VGINo    = new Vector();
           VGIDate  = new Vector();

           String QS = " Select GINo,GIDate from GateInward "+
                       " Where InwNo=0 and GrnNo=0 and Authentication=1 "+
                       " And Sup_Code='"+SSupCode+"'"+
                       " and MillCode = "+iMillCode+
                       " Group by GINo,GIDate ";

           try
           {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
                 
                 ResultSet res = stat.executeQuery(QS);

                 while (res.next())
                 {
                    String SGINo = res.getString(1);

                    int iIndex = common.indexOf(VSeleGINo,SGINo);

                    if(iIndex==-1)
                    {                                                                                                
                         VGINo.addElement(SGINo);
                         VGIDate.addElement(common.parseDate(res.getString(2)));
                    }
                 }
                 res.close();
                 stat.close();
           }
           catch(Exception ex){System.out.println(ex);}
     }

     private void showGIAddFrame()
     {
          try
          {
               DeskTop.remove(giaddframe);
               DeskTop.updateUI();
          }
          catch(Exception ex){}

          try
          {
               Frame dummy = new Frame();
               JDialog theDialog = new JDialog(dummy,"GateInward Addition",true);
               giaddframe = new GIAddFrameGst(DeskTop,theDialog,iMillCode,VGINo,VGIDate,SSupName,this);
               theDialog.getContentPane().add(giaddframe.AddPanel);
               theDialog.setBounds(80,100,450,350);
               theDialog.setVisible(true);
          }
          catch(Exception ex){}
     }

     private void showGIDeleteFrame()
     {
          try
          {
               DeskTop.remove(gideleteframe);
               DeskTop.updateUI();
          }
          catch(Exception ex){}

          try
          {
               Frame dummy = new Frame();
               JDialog theDialog = new JDialog(dummy,"GateInward Deletion",true);
               gideleteframe = new GIDeleteFrameGst(DeskTop,theDialog,iMillCode,VDeleGINo,SSupName,this);
               theDialog.getContentPane().add(gideleteframe.DeletePanel);
               theDialog.setBounds(80,100,450,350);
               theDialog.setVisible(true);
          }
          catch(Exception ex){}
     }

     public void append(Vector VAddGINo)
     {
         setVectorData(VAddGINo);

         for(int i=0;i<VAIName.size();i++)
         {
              Vector VEmpty = new Vector();

              VEmpty.addElement(VAGINo.elementAt(i));
              VEmpty.addElement(VAGIDate.elementAt(i));
              VEmpty.addElement(VAIName.elementAt(i));
              VEmpty.addElement(VADCQty.elementAt(i));
              VEmpty.addElement(VAQty.elementAt(i));
              VEmpty.addElement(new Boolean(false));
              gateModel.addRow(VEmpty);
         }
         GateTable.updateUI();

         for(int i=0;i<VAIGINo.size();i++)
         {
              Vector VEmpty = new Vector();

              VEmpty.addElement(VAIGINo.elementAt(i));
              VEmpty.addElement(VAIGIDate.elementAt(i));
              VEmpty.addElement(VAIInvNo.elementAt(i));
              VEmpty.addElement(VAIInvDate.elementAt(i));
              VEmpty.addElement(VAIDCNo.elementAt(i));
              VEmpty.addElement(VAIDCDate.elementAt(i));
              InvPanel.invModel.addRow(VEmpty);
         }
         InvPanel.InvTable.updateUI();

         BAdd.setEnabled(true);
         BDelete.setEnabled(true);
     }

     public void setVectorData(Vector VAddGINo)
     {
         String QS  = "";
         String QS1 = "";

         VAIName    = new Vector();
         VADCQty    = new Vector();
         VAQty      = new Vector();
         VAGId      = new Vector();
         VAGIDate   = new Vector();
         VAGINo     = new Vector();

         VAIGIDate   = new Vector();
         VAIGINo     = new Vector();
         VAIInvNo    = new Vector();
         VAIInvDate  = new Vector();
         VAIDCNo     = new Vector();
         VAIDCDate   = new Vector();

         try
         {
              ORAConnection   oraConnection =  ORAConnection.getORAConnection();
              Connection      theConnection =  oraConnection.getConnection();
              Statement       stat =  theConnection.createStatement();

              for(int i=0;i<VAddGINo.size();i++)
              {
                   String SGINo = (String)VAddGINo.elementAt(i);

                   VSeleGINo.addElement(SGINo);

                   QS  =   " SELECT GateInward.Item_Name,GateInward.SupQty,GateInward.Id,GateInward.GateQty,GateInward.GIDate "+
                           " From GateInward "+
                           " WHERE GateInward.GINo="+SGINo+" And GrnNo=0 "+
                           " And trim(GateInward.Item_Code) is Null "+
                           " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                           " And GateInward.MillCode = "+iMillCode+
			   " Union All "+
                           " SELECT InvItems.Item_Name,GateInward.SupQty,GateInward.Id,GateInward.GateQty,GateInward.GIDate "+
                           " From GateInward Inner Join InvItems On InvItems.Item_Code = GateInward.Item_Code "+
                           " WHERE GateInward.GINo="+SGINo+" And GrnNo=0 "+
                           " And trim(GateInward.Item_Code) is Not Null "+
                           " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                           " And GateInward.MillCode = "+iMillCode+
                           " Order By 1";

                   QS1 =   " SELECT GateInward.GIDate,GateInward.InvNo,GateInward.InvDate,GateInward.DCNo,GateInward.DCDate "+
                           " From GateInward "+
                           " WHERE Id=(Select Max(Id) from GateInward Where GateInward.GINo="+SGINo+" And GrnNo=0 "+
                           " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                           " And GateInward.MillCode = "+iMillCode+")"+
                           " Order By 1";

                   ResultSet res  = stat.executeQuery(QS);
                   while (res.next())
                   {
                         VAIName    .addElement(common.parseNull(res.getString(1)));
                         VADCQty    .addElement(res.getString(2));
                         VAQty      .addElement(res.getString(4));
                         VAGId      .addElement(res.getString(3));
                         VAGIDate   .addElement(common.parseDate(res.getString(5)));
                         VAGINo     .addElement(SGINo);
                   }
                   res.close();

                   res  = stat.executeQuery(QS1);
                   while (res.next())
                   {
                         VAIGIDate   .addElement(common.parseDate(res.getString(1)));
                         VAIGINo     .addElement(SGINo);
                         VAIInvNo    .addElement(common.parseNull(res.getString(2)));
                         VAIInvDate  .addElement(common.parseDate(res.getString(3)));
                         VAIDCNo     .addElement(common.parseNull(res.getString(4)));
                         VAIDCDate   .addElement(common.parseDate(res.getString(5)));
                   }
                   res.close();
              }
              stat.close();
         }
         catch(Exception ex)
         {
             System.out.println("E1"+ex );
         }
     }

     public void remove(Vector VDeleteGINo)
     {
         Vector VIndex = new Vector();

         Object FinalData[][] = getFromVector();
         for(int i=0;i<FinalData.length;i++)
         {
              String SGINo = (String)FinalData[i][0];

              int iIndex = common.indexOf(VDeleteGINo,SGINo);

              if(iIndex==-1)
                  continue;

              VIndex.addElement(String.valueOf(i));
         }
         int iCount=0;

         for(int i=0;i<VIndex.size();i++)
         {
              int iRow = common.toInt((String)VIndex.elementAt(i));
              iRow = iRow-iCount;

              gateModel.removeRow(iRow);
              iCount++;
         }
         GateTable.updateUI();

         Vector VInvIndex = new Vector();

         Object InvData[][] = InvPanel.getFromVector();
         for(int i=0;i<InvData.length;i++)
         {
              String SGINo = (String)InvData[i][0];

              int iIndex = common.indexOf(VDeleteGINo,SGINo);

              if(iIndex==-1)
                  continue;

              VInvIndex.addElement(String.valueOf(i));
         }
         iCount=0;

         for(int i=0;i<VInvIndex.size();i++)
         {
              int iRow = common.toInt((String)VInvIndex.elementAt(i));
              iRow = iRow-iCount;

              InvPanel.invModel.removeRow(iRow);
              iCount++;
         }
         InvPanel.InvTable.updateUI();

         for(int i=0;i<VDeleteGINo.size();i++)
         {
              String SGINo = (String)VDeleteGINo.elementAt(i);

              int iIndex = common.indexOf(VSeleGINo,SGINo);

              VSeleGINo.removeElementAt(iIndex);
         }
         BAdd.setEnabled(true);
         BDelete.setEnabled(true);
     }

     public void setReportTable()
     {
         gateModel        = new NewDirectGRNGateModelGst(RData,CData,CType);       
         GateTable      = new JTable(gateModel);
         GateTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<GateTable.getColumnCount();col++)
         {
               if(CType[col]=="N" || CType[col]=="B")
                    GateTable.getColumn(CData[col]).setCellRenderer(cellRenderer);
         }
         GateTable.setShowGrid(false);

         setLayout(new BorderLayout());
         GridBottom.add(GateTable.getTableHeader(),BorderLayout.NORTH);
         GridBottom.add(new JScrollPane(GateTable),BorderLayout.CENTER);

         GridPanel.add(GridBottom);

         add(BottomPanel,BorderLayout.SOUTH);
         add(GridPanel,BorderLayout.CENTER);

         GateTable.addKeyListener(new KeyList1());
     }
     private class KeyList1 extends KeyAdapter
     {
         public void keyPressed(KeyEvent ke)
         {
            if(ke.getKeyCode()==KeyEvent.VK_INSERT)
            {
                showMaterialSelectionFrame();
            }
         }
     }

     private void showMaterialSelectionFrame()
     {
          try
          {
               DeskTop.remove(pendingmaterialframe);
               DeskTop.updateUI();
          }
          catch(Exception ex){}

          try
          {
               int iRow     = GateTable.getSelectedRow();

               Boolean bValue = (Boolean)gateModel.getValueAt(iRow,5);

               if(!bValue.booleanValue())
               {
                   String SGINo    = (String)gateModel.getValueAt(iRow,0);
                   String SGIDate  = (String)gateModel.getValueAt(iRow,1);
                   String SDesc    = (String)gateModel.getValueAt(iRow,2);
                   String SInvQty  = (String)gateModel.getValueAt(iRow,3);
                   String SQty     = (String)gateModel.getValueAt(iRow,4);
    
                   pendingmaterialframe = new PendingMaterialFrameGst(DeskTop,iMillCode,SSupCode,SSupName,SDesc,SQty,this,VPendCode,VPendName,theVector,iRow,SGINo,SGIDate,SInvQty,VPOrderNo,VPOrderBlock,VPMrsNo,VPOrderCode,VPOrderName,VPOrderQty,VPQtyAllow);
                   DeskTop.add(pendingmaterialframe);
                   pendingmaterialframe.show();
                   pendingmaterialframe.moveToFront();
                   DeskTop.repaint();
                   DeskTop.updateUI();
               }
               else
               {
                   JOptionPane.showMessageDialog(null,"This Row Already Processed","Error",JOptionPane.ERROR_MESSAGE);
               }
          }
          catch(Exception ex){}
     }

     public Object[][] getFromVector()
     {
          return gateModel.getFromVector();     
     }


}

