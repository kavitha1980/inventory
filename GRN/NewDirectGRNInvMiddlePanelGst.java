// Used By Order,GRN  and other operations
// The Operation Varies with the state of bSig.
// Initially it is set to true once called from Non-Order activities,
// the same shall be set to false.

package GRN;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class NewDirectGRNInvMiddlePanelGst extends JPanel
{
   JTable               ReportTable;
   NewDirectGRNTableModelGst  dataModel;

   JLabel         LBasic,LDiscount,LCGST,LSGST,LIGST,LCess,LNet,LOthers;
   NextField      TAdd,TLess;
   JButton        BAdd,BDel;
   Vector         VHsnType,VSeleId,VRateStatus;

   JPanel         GridPanel,BottomPanel,FigurePanel,ControlPanel;
   JPanel         GridBottom;

   Vector    VDept,VDeptCode,VGroup,VGroupCode,VUnit,VUnitCode;
   Vector    VTaxClaim;
   JComboBox JCDept,JCGroup,JCUnit,JCTax;

   JLayeredPane   DeskTop;
   Object         RowData[][];
   String         ColumnData[],ColumnType[];
   int            iMillCode;
   String         SSupCode,SSupName;
   Vector         VSeleGINo;
   String         SSeleSupType,SSeleStateCode;

   Common common = new Common();

   // Constructor method referred in GRN Collection Frame Operations
    NewDirectGRNInvMiddlePanelGst(JLayeredPane DeskTop,Object RowData[][],String ColumnData[],String ColumnType[],int iMillCode,Vector VSeleGINo,String SSupCode,String SSupName,String SSeleSupType,String SSeleStateCode)
    {
         this.DeskTop     = DeskTop;
         this.RowData     = RowData;
         this.ColumnData  = ColumnData;
         this.ColumnType  = ColumnType;
         this.iMillCode   = iMillCode;
         this.VSeleGINo   = VSeleGINo;
         this.SSupCode    = SSupCode;
         this.SSupName    = SSupName;
	 this.SSeleSupType = SSeleSupType;
	 this.SSeleStateCode = SSeleStateCode;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
         setReportTable();
    }
    public void createComponents()
    {
         LBasic           = new JLabel("0");
         LDiscount        = new JLabel("0");
         LCGST            = new JLabel("0");
         LSGST            = new JLabel("0");
         LIGST            = new JLabel("0");
         LCess            = new JLabel("0");
         TAdd             = new NextField();
         TLess            = new NextField();
         LNet             = new JLabel("0");
         LOthers          = new JLabel("0");

         BAdd             = new JButton("Add New Record");
         BDel             = new JButton("Delete Record");

	 VHsnType         = new Vector();
         VSeleId          = new Vector();
	 VRateStatus      = new Vector();

         GridPanel        = new JPanel(true);
         GridBottom       = new JPanel(true);
         BottomPanel      = new JPanel();
         FigurePanel      = new JPanel();
         ControlPanel     = new JPanel();
     }
          
     public void setLayouts()
     {
         GridPanel.setLayout(new GridLayout(1,1));
         BottomPanel.setLayout(new BorderLayout());
         FigurePanel.setLayout(new GridLayout(2,9));
         GridBottom.setLayout(new BorderLayout());         
         ControlPanel.setLayout(new FlowLayout());
     }
     public void addComponents()
     {
         FigurePanel.add(new JLabel("Basic"));
         FigurePanel.add(new JLabel("Discount"));
         FigurePanel.add(new JLabel("CGST"));
         FigurePanel.add(new JLabel("SGST"));
         FigurePanel.add(new JLabel("IGST"));
         FigurePanel.add(new JLabel("Cess"));
         FigurePanel.add(new JLabel("Plus"));
         FigurePanel.add(new JLabel("Minus"));
         FigurePanel.add(new JLabel("Net"));

         FigurePanel.add(LBasic);
         FigurePanel.add(LDiscount);
         FigurePanel.add(LCGST);
         FigurePanel.add(LSGST);
         FigurePanel.add(LIGST);
         FigurePanel.add(LCess);
         FigurePanel.add(TAdd);
         FigurePanel.add(TLess);
         FigurePanel.add(LNet);

         ControlPanel.add(new JLabel("F2 - For add Other Charges"));
         ControlPanel.add(BAdd);
         ControlPanel.add(BDel);

         BottomPanel.add("North",ControlPanel);
         BottomPanel.add("South",FigurePanel);

         getDeptGroupUnit();

         VTaxClaim      = new Vector();
         VTaxClaim      . insertElementAt("Not Claimable",0);
         VTaxClaim      . insertElementAt("Claimable",1);

         JCDept  = new JComboBox(VDept);
         JCGroup = new JComboBox(VGroup);
         JCUnit  = new JComboBox(VUnit);
         JCTax   = new JComboBox(VTaxClaim);
     }
     public void addListeners()
     {
         BAdd.addActionListener(new ActList());
         BDel.addActionListener(new ActList());
         TAdd.addKeyListener(new KeyList());
         TLess.addKeyListener(new KeyList());
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               calc();
          }
     }
     public void calc()
     {
               double dTNet = common.toDouble(LBasic.getText())-common.toDouble(LDiscount.getText())+common.toDouble(LCGST.getText())+common.toDouble(LSGST.getText())+common.toDouble(LIGST.getText())+common.toDouble(LCess.getText());
               dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
               LNet.setText(common.getRound(dTNet,2));                              
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BAdd)
               {
                    int iRows = dataModel.getRows();

		    if(iRows>=1)
		    {
		            Vector VEmpty1 = new Vector();
		       
		            for(int i=0;i<ColumnData.length;i++)
		            {
		                 VEmpty1   . addElement("");
		            }

			    VHsnType.addElement("1");
			    VSeleId.addElement("0");
			    VRateStatus.addElement("0");

		            dataModel           . addRow(VEmpty1);
		            ReportTable         . updateUI();
		    }
               }

               if(ae.getSource()==BDel)
               {
                    int iRows = dataModel.getRows();
		    if(iRows>1)
		    {
		         int i = ReportTable.getSelectedRow();

			 String SHsnType = (String)VHsnType.elementAt(i);
			 if(SHsnType.equals("1"))
			 {
				 dataModel.removeRow(i);

				 VHsnType.removeElementAt(i);
				 VSeleId.removeElementAt(i);
				 VRateStatus.removeElementAt(i);

				 dataModel.setTaxData();

				 ReportTable.updateUI();
			 }
			 else
			 {
		              JOptionPane.showMessageDialog(null,"This row couldn't be Deleted","Information",JOptionPane.INFORMATION_MESSAGE);
			 }
		    }
               }
          }
     }

     public void setReportTable()
     {
         dataModel        = new NewDirectGRNTableModelGst(RowData,ColumnData,ColumnType,LBasic,LDiscount,LCGST,LSGST,LIGST,LCess,TAdd,TLess,LNet,LOthers,VHsnType,VRateStatus,SSeleSupType,SSeleStateCode);       
         ReportTable      = new JTable(dataModel);
         ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<ReportTable.getColumnCount();col++)
         {
               if(ColumnType[col]=="N" || ColumnType[col]=="B")
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
         }
         ReportTable.setShowGrid(false);

         TableColumn deptColumn  = ReportTable.getColumn("Department");
         TableColumn groupColumn = ReportTable.getColumn("Group");
         TableColumn unitColumn  = ReportTable.getColumn("Unit");
         TableColumn taxColumn   = ReportTable.getColumn("VAT");

         deptColumn.setCellEditor(new DefaultCellEditor(JCDept));
         groupColumn.setCellEditor(new DefaultCellEditor(JCGroup));
         unitColumn.setCellEditor(new DefaultCellEditor(JCUnit));
         taxColumn.setCellEditor(new DefaultCellEditor(JCTax));

         setLayout(new BorderLayout());
         GridBottom.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
         GridBottom.add(new JScrollPane(ReportTable),BorderLayout.CENTER);

         GridPanel.add(GridBottom);

         add(BottomPanel,BorderLayout.SOUTH);
         add(GridPanel,BorderLayout.CENTER);

         ReportTable.addKeyListener(new ServList());

         dataModel.removeRow(0);
     }

     public class ServList extends KeyAdapter
     {
               public void keyPressed(KeyEvent ke)
               {
                    if(ke.getKeyCode()==KeyEvent.VK_F2) 
		    {
			 int i = ReportTable.getSelectedRow();
			 String SHsnType = (String)VHsnType.elementAt(i);
			 if(SHsnType.equals("1"))
			 {
                              setServList();
			 }
			 else
			 {
		              JOptionPane.showMessageDialog(null,"This row couldn't be changed","Information",JOptionPane.INFORMATION_MESSAGE);
			 }
		    }
               }
     }

     private void setServList()
     {
           ServicePicker1 SP = new ServicePicker1(DeskTop,ReportTable,dataModel,VRateStatus);
           try
           {
                DeskTop   . add(SP);
                DeskTop   . repaint();
                SP        . setSelected(true);
                DeskTop   . updateUI();
                SP        . show();
                SP        . BrowList.requestFocus();
           }
           catch(java.beans.PropertyVetoException ex){}
     }

     public Object[][] getFromVector()
     {
          return dataModel.getFromVector();     
     }

     public String getDeptCode(int i)                       // 26
     {
         Vector VCurVector = dataModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(26);
         int iid = VDept.indexOf(str);
         return (iid==-1 ?"0":(String)VDeptCode.elementAt(iid));
     }
     public String getGroupCode(int i)                     // 27
     {
         Vector VCurVector = dataModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(27);
         int iid = VGroup.indexOf(str);
         return (iid==-1?"0":(String)VGroupCode.elementAt(iid));
     }
     public String getUnitCode(int i)                     //  28
     {
         Vector VCurVector = dataModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(28);
         int iid = VUnit.indexOf(str);
         return (iid==-1?"0":(String)VUnitCode.elementAt(iid));
     }
     public void getDeptGroupUnit()
     {
        VDept  = new Vector();
        VGroup = new Vector();

        VDeptCode  = new Vector();
        VGroupCode = new Vector();

        VUnit      = new Vector();
        VUnitCode  = new Vector();

        try
        {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();

               String QS1 = "";
               String QS2 = "";
               String QS3 = "";

               QS1 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               QS2 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               QS3 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";

               ResultSet result1 = stat.executeQuery(QS1);
               while(result1.next())
               {
                    VDept.addElement(result1.getString(1));
                    VDeptCode.addElement(result1.getString(2));
               }
               result1.close();

               ResultSet result2 = stat.executeQuery(QS2);
               while(result2.next())
               {
                    VGroup.addElement(result2.getString(1));
                    VGroupCode.addElement(result2.getString(2));
               }
               result2.close();

               ResultSet result3 = stat.executeQuery(QS3);
               while(result3.next())
               {
                    VUnit.addElement(result3.getString(1));
                    VUnitCode.addElement(result3.getString(2));
               }
               result3.close();
               stat.close();
        }
        catch(Exception ex)
        {
            System.out.println("Dept,Group & Unit :"+ex);
        }
     }
}

