// Used By Order,GRN  and other operations
// The Operation Varies with the state of bSig.
// Initially it is set to true once called from Non-Order activities,
// the same shall be set to false.

package GRN;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class NewDirectGRNInvPanelGst extends JPanel
{
   JTable                InvTable;
   NewDirectGRNInvModelGst  invModel;

   JPanel         GridPanel,BottomPanel,ControlPanel;
   JPanel         GridBottom;

   JLayeredPane   DeskTop;
   Object         IData[][];
   String         CIData[],CIType[];
   int            iMillCode;
   Vector         VSeleGINo;
   NewDirectGRNMiddlePanelGst  MiddlePanel;

   Common common = new Common();

   Vector VGINo,VGIDate;

   // Constructor method referred in GRN Collection Frame Operations
    NewDirectGRNInvPanelGst(JLayeredPane DeskTop,Object IData[][],String CIData[],String CIType[],int iMillCode,Vector VSeleGINo,NewDirectGRNMiddlePanelGst MiddlePanel)
    {
         this.DeskTop      = DeskTop;
         this.IData        = IData;
         this.CIData       = CIData;
         this.CIType       = CIType;
         this.iMillCode    = iMillCode;
         this.VSeleGINo    = VSeleGINo;
         this.MiddlePanel  = MiddlePanel;

         createComponents();
         setLayouts();
         setReportTable();
    }
    public void createComponents()
    {
         GridPanel        = new JPanel(true);
         GridBottom       = new JPanel(true);
         BottomPanel      = new JPanel();
         ControlPanel     = new JPanel();
     }
          
     public void setLayouts()
     {
         GridPanel.setLayout(new GridLayout(1,1));
         GridBottom.setLayout(new BorderLayout());         
     }

     public void setReportTable()
     {
         invModel      = new NewDirectGRNInvModelGst(IData,CIData,CIType);       
         InvTable      = new JTable(invModel);
         InvTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<InvTable.getColumnCount();col++)
         {
               if(CIType[col]=="N" || CIType[col]=="B")
                    InvTable.getColumn(CIData[col]).setCellRenderer(cellRenderer);
         }
         InvTable.setShowGrid(false);

         setLayout(new BorderLayout());
         GridBottom.add(InvTable.getTableHeader(),BorderLayout.NORTH);
         GridBottom.add(new JScrollPane(InvTable),BorderLayout.CENTER);

         GridPanel.add(GridBottom);

         add(BottomPanel,BorderLayout.SOUTH);
         add(GridPanel,BorderLayout.CENTER);
     }

     public Object[][] getFromVector()
     {
          return invModel.getFromVector();     
     }


}

