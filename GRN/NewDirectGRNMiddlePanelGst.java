package GRN;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class NewDirectGRNMiddlePanelGst extends JTabbedPane 
{
         NewDirectGRNInvMiddlePanelGst MiddlePanel;
         NewDirectGRNGatePanelGst GatePanel;
         NewDirectGRNInvPanelGst InvPanel;
         Object RowData[][];
         Object RData[][];
         Object IData[][];

         String ColumnData[] = {"G.I.No.","G.I.Date","Desc @Gate","Code","Name","HsnCode","Block","MRS No","Order No","Order Qty","Pending Qty","Inv/DC Qty","Recd Qty","Rate","Disc (%)","CGST (%)","SGST (%)","IGST (%)","Cess (%)","Basic","Disc (Rs)","CGST (Rs)","SGST (Rs)","IGST (Rs)","Cess (Rs)","Net (Rs)","Department","Group","Unit","VAT"};
         String ColumnType[] = {"S","S","S","S","S","S","S","N","N","N","N","N","N","B","N","N","N","N","N","N","N","N","N","N","N","N","B","B","B","S"};

         String CData[] = {"G.I.No.","G.I.Date","Desc @Gate","Inv/DC Qty","Gate Qty","Status"};
         String CType[] = {"S","S","S","N","N","S"};

         String CIData[] = {"G.I.No.","G.I.Date","Inv No","Inv Date","DC No","DC Date"};
         String CIType[] = {"S","S","E","E","E","E"};

         JLayeredPane DeskTop;
         String SSupCode,SSupName;
	 String SSeleSupType,SSeleStateCode;

         Vector VIName,VDCQty,VQty,VGId,VGIDate,VGINo;

         Vector VIGIDate,VIGINo,VIInvNo,VIInvDate,VIDCNo,VIDCDate;

         Vector theVector;
         Vector VPendCode,VPendName;
         Vector VPOrderNo,VPOrderBlock,VPMrsNo,VPOrderCode,VPOrderName,VPOrderQty,VPQtyAllow;

         Vector VGBlockCode,VId,VGOrdQty,VMrsSlNo,VMrsUserCode,VOrderSlNo,VApproval,VTaxC;

         Common common   = new Common();
         int iMillCode;

         Control control = new Control();

         public NewDirectGRNMiddlePanelGst(JLayeredPane DeskTop,String SSupCode,String SSupName,int iMillCode,String SSeleSupType,String SSeleStateCode)
         {
              this.DeskTop        = DeskTop;
              this.SSupCode       = SSupCode;
              this.SSupName       = SSupName;
              this.iMillCode      = iMillCode;
	      this.SSeleSupType   = SSeleSupType;
	      this.SSeleStateCode = SSeleStateCode;

              VGBlockCode  = new Vector();
              VId          = new Vector();
              VGOrdQty     = new Vector();
              VMrsSlNo     = new Vector();
              VMrsUserCode = new Vector();
              VOrderSlNo   = new Vector();
              VApproval    = new Vector();
              VTaxC        = new Vector();
         }

         public void createComponents(Vector VSeleGINo)
         {
            setVectorData();
            setVectorData(VSeleGINo);
            setRowData();
            setRData();
            setIData();
            try
            {
                  InvPanel    = new NewDirectGRNInvPanelGst(DeskTop,IData,CIData,CIType,iMillCode,VSeleGINo,this);
                  GatePanel   = new NewDirectGRNGatePanelGst(DeskTop,RData,CData,CType,iMillCode,VSeleGINo,SSupCode,SSupName,VPendCode,VPendName,theVector,this,VPOrderNo,VPOrderBlock,VPMrsNo,VPOrderCode,VPOrderName,VPOrderQty,VPQtyAllow,InvPanel);
                  MiddlePanel = new NewDirectGRNInvMiddlePanelGst(DeskTop,RowData,ColumnData,ColumnType,iMillCode,VSeleGINo,SSupCode,SSupName,SSeleSupType,SSeleStateCode);
                  addTab("Invoice & DC No",InvPanel);
                  addTab("Gate Inward",GatePanel);
                  addTab("Materials Pending @ Order",MiddlePanel);
                  InvPanel.setBorder(new TitledBorder("Invoice and DC Details for GateInward"));
                  GatePanel.setBorder(new TitledBorder("Reference From Gate Entry"));
                  MiddlePanel.setBorder(new TitledBorder("Pending Purchase Orders"));
                  setSelectedIndex(1);
            }
            catch(Exception ex)
            {
                  System.out.println("GRNInvMiddlePanel: "+ex);
            }
         }

        public void setVectorData()
        {
            theVector = new Vector();
            VPendCode = new Vector();
            VPendName = new Vector();

            VPOrderNo    = new Vector();
            VPOrderBlock = new Vector();
            VPMrsNo      = new Vector();
            VPOrderCode  = new Vector();
            VPOrderName  = new Vector();
            VPOrderQty   = new Vector();
            VPQtyAllow   = new Vector();

            String QS1= "";

            QS1 =   " SELECT PurchaseOrder.Item_Code, InvItems.Item_Name, OrdBlock.BlockName, "+
                    " PurchaseOrder.OrderNo, PurchaseOrder.MrsNo, "+
                    " PurchaseOrder.Qty-PurchaseOrder.InvQty AS Pending, "+
                    " PurchaseOrder.Rate, PurchaseOrder.DiscPer, PurchaseOrder.CGST, "+
                    " PurchaseOrder.SGST, PurchaseOrder.IGST, PurchaseOrder.Cess, Dept.Dept_Name, "+
                    " PurchaseOrder.Dept_Code, Cata.Group_Name, PurchaseOrder.Group_Code, "+
                    " Unit.Unit_Name, PurchaseOrder.Unit_Code,PurchaseOrder.Id, "+
                    " PurchaseOrder.OrderBlock,PurchaseOrder.Qty,PurchaseOrder.OrderDate,"+
                    " purchaseOrder.Mrsslno,purchaseOrder.Slno,purchaseOrder.taxclaimable,"+
                    " InvItems.QtyAllowance,PurchaseOrder.MrsAuthUserCode,PurchaseOrder.JMDOrderApproval, "+
		    " decode(PurchaseOrder.HsnCode,null,InvItems.HsnCode,PurchaseOrder.HsnCode) as HsnCode, nvl(InvItems.HsnType,0) as HsnType, "+
		    " decode(PurchaseOrder.InvQty,0,0,1) as GrnStatus "+
                    " FROM ((((PurchaseOrder INNER JOIN InvItems ON "+
                    " PurchaseOrder.Item_Code = InvItems.Item_Code) "+
                    " INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock = OrdBlock.Block) "+
                    " INNER JOIN Dept ON PurchaseOrder.Dept_Code = Dept.Dept_code) "+
                    " INNER JOIN Cata ON PurchaseOrder.Group_Code = Cata.Group_Code) "+
                    " INNER JOIN Unit ON PurchaseOrder.Unit_Code = Unit.Unit_Code "+
                    " Where PurchaseOrder.Sup_Code = '"+SSupCode+"' "+
                    " And PurchaseOrder.InvQty < PurchaseOrder.Qty  "+
                    " And PurchaseOrder.Authentication=1 "+
                    " And PurchaseOrder.MillCode="+iMillCode+
                    " Order By 4,1";

            try
            {
                    ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                    Connection      theConnection =  oraConnection.getConnection();
                    Statement       stat =  theConnection.createStatement();

                   ResultSet res1 = stat.executeQuery(QS1);

                   while(res1.next())
                      organizePendingItems(res1);

                   res1.close();
                   stat.close();
            }
            catch(Exception ex)
            {
                System.out.println("E1"+ex );
            }
        }

     private void organizePendingItems(ResultSet res1) throws Exception
     {
           PendingItemGst pendingitem = null;

           String SGCode       = res1.getString(1);
           String SGName       = res1.getString(2); 
           String SGBlock      = res1.getString(3);
           String SGOrderNo    = res1.getString(4);
           String SGMRSNo      = res1.getString(5);
           String SGPendQty    = res1.getString(6);
           String SGGrnQty     = "0";
           String SGRate       = res1.getString(7);
           String SGDiscPer    = res1.getString(8);
           String SGCGSTPer    = res1.getString(9);
           String SGSGSTPer    = res1.getString(10);
           String SGIGSTPer    = res1.getString(11);
           String SGCessPer    = res1.getString(12);
           String SGDept       = res1.getString(13);
           String SGDeptCode   = res1.getString(14); 
           String SGGroup      = res1.getString(15);
           String SGGroupCode  = res1.getString(16); 
           String SGUnit       = res1.getString(17);
           String SGUnitCode   = res1.getString(18);
           String SId          = res1.getString(19);
           String SGBlockCode  = res1.getString(20);
           String SGOrdQty     = res1.getString(21);
           String SGOrdDate    = res1.getString(22);
           String SMrsSlNo     = res1.getString(23);
           String SOrderSlNo   = res1.getString(24);
           String STaxC        = res1.getString(25);
           String SQtyAllow    = res1.getString(26);
           String SMrsUserCode = res1.getString(27);
           String SOrderApproval = res1.getString(28);
           String SHsnCode       = res1.getString(29);
           String SHsnType       = res1.getString(30);
           String SGrnStatus     = res1.getString(31);

	   String SGstRate = common.getRound(common.toDouble(SGCGSTPer) + common.toDouble(SGSGSTPer) + common.toDouble(SGIGSTPer),2);

           VPOrderNo    .addElement(SGOrderNo);
           VPOrderBlock .addElement(SGBlock);
           VPMrsNo      .addElement(SGMRSNo);
           VPOrderCode  .addElement(SGCode);
           VPOrderName  .addElement(SGName);
           VPOrderQty   .addElement(SGOrdQty);
           VPQtyAllow   .addElement(SQtyAllow);
           
           int iIndex = getPendingIndexOf(SGCode);

           if(iIndex==-1)
           {
               pendingitem = new PendingItemGst(SGCode);
               theVector.addElement(pendingitem);
               pendingitem.setPendingData(SGName,SGBlock,SGOrderNo,SGMRSNo,SGPendQty,SGGrnQty,SGRate,SGDiscPer,SGCGSTPer,SGSGSTPer,SGIGSTPer,SGCessPer,SGDept,SGDeptCode,SGGroup,SGGroupCode,SGUnit,SGUnitCode,SGBlockCode,SId,SGOrdQty,SGOrdDate,SMrsSlNo,SMrsUserCode,SOrderSlNo,SOrderApproval,STaxC,SHsnCode,SHsnType,SGrnStatus,SGstRate);
               VPendCode.addElement(SGCode);
               VPendName.addElement(SGName);
           }
           else
           {
               pendingitem = (PendingItemGst)theVector.elementAt(iIndex);
               pendingitem.setPendingData(SGName,SGBlock,SGOrderNo,SGMRSNo,SGPendQty,SGGrnQty,SGRate,SGDiscPer,SGCGSTPer,SGSGSTPer,SGIGSTPer,SGCessPer,SGDept,SGDeptCode,SGGroup,SGGroupCode,SGUnit,SGUnitCode,SGBlockCode,SId,SGOrdQty,SGOrdDate,SMrsSlNo,SMrsUserCode,SOrderSlNo,SOrderApproval,STaxC,SHsnCode,SHsnType,SGrnStatus,SGstRate);
           }
     }

     private int getPendingIndexOf(String SGCode)
     {
          int iIndex =-1;
          for(int i=0; i<theVector.size(); i++)
          {
               PendingItemGst pendingitem = (PendingItemGst)theVector.elementAt(i);
               if((pendingitem.SGCode).equals(SGCode))
               {
                    iIndex =i;
               }
          }
          return iIndex;
     }

        public void setVectorData(Vector VSeleGINo)
        {
               
            String QS  = "";
            String QS1 = "";

            VIName    = new Vector();
            VDCQty    = new Vector();
            VQty      = new Vector();
            VGId      = new Vector();
            VGIDate   = new Vector();
            VGINo     = new Vector();


            VIGIDate   = new Vector();
            VIGINo     = new Vector();
            VIInvNo    = new Vector();
            VIInvDate  = new Vector();
            VIDCNo     = new Vector();
            VIDCDate   = new Vector();

            try
            {
                 ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                 Connection      theConnection =  oraConnection.getConnection();
                 Statement       stat =  theConnection.createStatement();

                 for(int i=0;i<VSeleGINo.size();i++)
                 {
                      String SGINo = (String)VSeleGINo.elementAt(i);

                      QS  =   " SELECT GateInward.Item_Name,GateInward.SupQty,GateInward.Id,GateInward.GateQty,GateInward.GIDate "+
                              " From GateInward "+
                              " WHERE GateInward.GINo="+SGINo+" And GrnNo=0 "+
                              " And trim(GateInward.Item_Code) is Null "+
                              " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                              " And GateInward.MillCode = "+iMillCode+
			      " Union All "+
                              " SELECT InvItems.Item_Name,GateInward.SupQty,GateInward.Id,GateInward.GateQty,GateInward.GIDate "+
                              " From GateInward Inner Join InvItems On InvItems.Item_Code = GateInward.Item_Code "+
                              " WHERE GateInward.GINo="+SGINo+" And GrnNo=0 "+
                              " And trim(GateInward.Item_Code) is Not Null "+
                              " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                              " And GateInward.MillCode = "+iMillCode+
                              " Order By 1";
   
                      QS1 =   " SELECT GateInward.GIDate,GateInward.InvNo,GateInward.InvDate,GateInward.DCNo,GateInward.DCDate "+
                              " From GateInward "+
                              " WHERE Id=(Select Max(Id) from GateInward Where GateInward.GINo="+SGINo+" And GrnNo=0 "+
                              " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                              " And GateInward.MillCode = "+iMillCode+")"+
                              " Order By 1";

                      ResultSet res  = stat.executeQuery(QS);
                      while (res.next())
                      {
                            VIName    .addElement(common.parseNull(res.getString(1)));
                            VDCQty    .addElement(res.getString(2));
                            VQty      .addElement(res.getString(4));
                            VGId      .addElement(res.getString(3));
                            VGIDate   .addElement(common.parseDate(res.getString(5)));
                            VGINo     .addElement(SGINo);
                      }
                      res.close();

                      res  = stat.executeQuery(QS1);
                      while (res.next())
                      {
                            VIGIDate   .addElement(common.parseDate(res.getString(1)));
                            VIGINo     .addElement(SGINo);
                            VIInvNo    .addElement(common.parseNull(res.getString(2)));
                            VIInvDate  .addElement(common.parseDate(res.getString(3)));
                            VIDCNo     .addElement(common.parseNull(res.getString(4)));
                            VIDCDate   .addElement(common.parseDate(res.getString(5)));
                      }
                      res.close();
                 }
                 stat.close();
            }
            catch(Exception ex)
            {
                System.out.println("E2"+ex );
            }
        }
        public boolean setRowData()
        {
            RowData = new Object[1][ColumnData.length];

            RowData[0][0] = "";
            RowData[0][1] = "";
            RowData[0][2] = "";
            RowData[0][3] = "";
            RowData[0][4]  = "";
            RowData[0][5]  = "";
            RowData[0][6]  = "";
            RowData[0][7]  = "";
            RowData[0][8]  = "";
            RowData[0][9]  = "";
            RowData[0][10] = "";
            RowData[0][11] = "";
            RowData[0][12] = "";
            RowData[0][13] = "";
            RowData[0][14] = "";
            RowData[0][15] = "";
            RowData[0][16] = "";
            RowData[0][17] = "";
            RowData[0][18] = "";
            RowData[0][19] = "";
            RowData[0][20] = "";
            RowData[0][21] = "";
            RowData[0][22] = "";
            RowData[0][23] = "";
            RowData[0][24] = "";
            RowData[0][25] = "";
            RowData[0][26] = "";
            RowData[0][27] = "";
            RowData[0][28] = "";
            RowData[0][29] = "";

            return true;
        }

        public boolean setRData()
        {
            RData = new Object[VIName.size()][CData.length];

            for(int i=0;i<VIName.size();i++)
            {
                    RData[i][0] = (String)VGINo.elementAt(i);
                    RData[i][1] = (String)VGIDate.elementAt(i);
                    RData[i][2] = (String)VIName.elementAt(i);
                    RData[i][3] = (String)VDCQty.elementAt(i);
                    RData[i][4] = (String)VQty.elementAt(i);
                    RData[i][5] = new Boolean(false);
            }
            return true;
        }

        public boolean setIData()
        {
            IData = new Object[VIGINo.size()][CIData.length];

            for(int i=0;i<VIGINo.size();i++)
            {
                    IData[i][0] = (String)VIGINo.elementAt(i);
                    IData[i][1] = (String)VIGIDate.elementAt(i);
                    IData[i][2] = (String)VIInvNo.elementAt(i);
                    IData[i][3] = (String)VIInvDate.elementAt(i);
                    IData[i][4] = (String)VIDCNo.elementAt(i);
                    IData[i][5] = (String)VIDCDate.elementAt(i);
            }
            return true;
        }

        public String getBlockCode(int i)
        {
               return (String)VGBlockCode.elementAt(i);
        }
        public String getOrdQty(int i)
        {
               return (String)VGOrdQty.elementAt(i);
        }
        public String getMrsSLNo(int i)
        {
               return (String)VMrsSlNo.elementAt(i);
        }
        public String getMrsUserCode(int i)
        {
               return common.parseNull((String)VMrsUserCode.elementAt(i));
               //return "1";

        }
        public String getOrderSLNo(int i)
        {
               return (String)VOrderSlNo.elementAt(i);
        }
        public String getTaxC(int i)
        {
               return (String)VTaxC.elementAt(i);
        }
        public String getOrderApprovalStatus(int i)
        {
               return (String)VApproval.elementAt(i);
        }


}
