package GRN;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class NewDirectGRNTableModel extends DefaultTableModel
{
        Object    RowData[][],ColumnNames[],ColumnType[];
        JLabel    LBasic,LDiscount,LCenVat,LTax,LSur,LNet;
        NextField TAdd,TLess,TModVat;
        Common common = new Common();
        public NewDirectGRNTableModel(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType,JLabel LBasic,JLabel LDiscount,JLabel LCenVat,JLabel LTax,JLabel LSur,NextField TAdd,NextField TLess,JLabel LNet,NextField TModVat)
        {
            super(RowData,ColumnNames);
            this.RowData     = RowData;
            this.ColumnNames = ColumnNames;
            this.ColumnType  = ColumnType;
            this.LBasic      = LBasic;
            this.LDiscount   = LDiscount;
            this.LCenVat     = LCenVat; 
            this.LTax        = LTax;
            this.LSur        = LSur;
            this.TAdd        = TAdd;
            this.TLess       = TLess;
            this.LNet        = LNet;
            this.TModVat     = TModVat;

            for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
                setMaterialAmount(curVector);
            }
        }
       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }            
       public boolean isCellEditable(int row,int col)
       {
               if(ColumnType[col]=="B" || ColumnType[col]=="E")
                  return true;
               return false;
       }
       public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
                   if(column>=11 && column<=16)
                      setMaterialAmount(rowVector);
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }
      public void setMaterialAmount(Vector RowVector)
      {
              double dTGross=0,dTDisc=0,dTCenVat=0,dTTax=0,dTSur=0,dTNet=0;

              double dQty        = common.toDouble((String)RowVector.elementAt(11));
              double dRate       = common.toDouble((String)RowVector.elementAt(12));
              double dDiscPer    = common.toDouble((String)RowVector.elementAt(13));
              double dCenVatPer  = common.toDouble((String)RowVector.elementAt(14));
              double dTaxPer     = common.toDouble((String)RowVector.elementAt(15));
              double dSurPer     = common.toDouble((String)RowVector.elementAt(16));

              double dGross  = dQty*dRate;
              double dDisc   = dGross*dDiscPer/100;
              double dCenVat = (dGross-dDisc)*dCenVatPer/100;
              double dTax    = (dGross-dDisc+dCenVat)*dTaxPer/100;
              double dSur    = (dTax)*dSurPer/100;
              double dNet    = dGross-dDisc+dCenVat+dTax+dSur;

              RowVector.setElementAt(common.getRound(dGross,2),17);
              RowVector.setElementAt(common.getRound(dDisc,2),18);
              RowVector.setElementAt(common.getRound(dCenVat,2),19);
              RowVector.setElementAt(common.getRound(dTax,2),20);
              RowVector.setElementAt(common.getRound(dSur,2),21);
              RowVector.setElementAt(common.getRound(dNet,2),22);
              for(int i=0;i<super.dataVector.size();i++)
              {
                  Vector curVector = (Vector)super.dataVector.elementAt(i);
                  dTGross   = dTGross+common.toDouble((String)curVector.elementAt(17));
                  dTDisc    = dTDisc+common.toDouble((String)curVector.elementAt(18));
                  dTCenVat  = dTCenVat+common.toDouble((String)curVector.elementAt(19));
                  dTTax     = dTTax+common.toDouble((String)curVector.elementAt(20));
                  dTSur     = dTSur+common.toDouble((String)curVector.elementAt(21));
                  dTNet     = dTNet+common.toDouble((String)curVector.elementAt(22));
              }
              dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
              LBasic.setText(common.getRound(dTGross,2));
              LDiscount.setText(common.getRound(dTDisc,2));
              LCenVat.setText(common.getRound(dTCenVat,2));
              LTax.setText(common.getRound(dTTax,2));
              LSur.setText(common.getRound(dTSur,2));
              LNet.setText(common.getRound(dTNet,2));
              TModVat.setText(common.getRound(dTCenVat,2));
    }
    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
                FinalData[i][j] = ((String)curVector.elementAt(j)).trim();
        }
        return FinalData;
    }
    public int getRows()
    {
         return super.dataVector.size();
    }
    public Vector getCurVector(int i)
    {
         return (Vector)super.dataVector.elementAt(i);
    }
}
