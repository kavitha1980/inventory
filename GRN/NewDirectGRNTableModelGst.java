package GRN;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class NewDirectGRNTableModelGst extends DefaultTableModel
{
        Object    RowData[][],ColumnNames[],ColumnType[],ColumnData[][];
        JLabel    LBasic,LDiscount,LCGST,LSGST,LIGST,LCess,LNet,LOthers;
        NextField TAdd,TLess;
        Vector VHsnType,VRateStatus;
        String SSeleSupType,SSeleStateCode;
	int iHsnCheck   = 0;
	double dCGST=0,dSGST=0,dIGST=0,dCess=0;

        Common common = new Common();

        Connection       theConnection = null;

        public NewDirectGRNTableModelGst(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType,JLabel LBasic,JLabel LDiscount,JLabel LCGST,JLabel LSGST,JLabel LIGST,JLabel LCess,NextField TAdd,NextField TLess,JLabel LNet,JLabel LOthers,Vector VHsnType,Vector VRateStatus,String SSeleSupType,String SSeleStateCode)
        {
            super(RowData,ColumnNames);
            this.RowData     = RowData;
            this.ColumnNames = ColumnNames;
            this.ColumnType  = ColumnType;
            this.LBasic      = LBasic;
            this.LDiscount   = LDiscount;
            this.LCGST       = LCGST; 
            this.LSGST       = LSGST;
            this.LIGST       = LIGST;
            this.LCess       = LCess;
            this.TAdd        = TAdd;
            this.TLess       = TLess;
            this.LNet        = LNet;
            this.LOthers     = LOthers;
	    this.VHsnType    = VHsnType;
	    this.VRateStatus = VRateStatus;
	    this.SSeleSupType = SSeleSupType;
	    this.SSeleStateCode = SSeleStateCode;

	    ColumnData = new Object[RowData.length][ColumnType.length];

            /*for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
                setMaterialAmount(curVector);
            }*/
        }
       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }            
       public boolean isCellEditable(int row,int col)
       {
     	       String SHsnType    = (String)VHsnType.elementAt(row);
     	       String SRateStatus = (String)VRateStatus.elementAt(row);

	       if(col==13)
	       {
		       if(SHsnType.equals("1"))
		       {
			       if(ColumnType[col]=="B" || ColumnType[col]=="E")
				  return true;
		       }
		       else
			    return false;
	       }
	       else
	       {
		       if(ColumnType[col]=="B" || ColumnType[col]=="E")
		          return true;

	       	       if(SHsnType.equals("1") && SRateStatus.equals("1") && ColumnData[row][col]=="X")
			    return true;

		       return false;
	       }
	       return false;
       }
       public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
	     	   String SHsnType    = (String)VHsnType.elementAt(row);
	     	   String SRateStatus = (String)VRateStatus.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
                   if(column>=13 && column<=18)
                      setMaterialAmount(rowVector,SHsnType,SRateStatus,row);
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }
      public void setMaterialAmount(Vector RowVector,String SHsnType,String SRateStatus,int iRow)
      {
              double dTGross=0,dTDisc=0,dTCGST=0,dTSGST=0,dTIGST=0,dTCess=0,dTNet=0,dTOthers=0;

	      setColType(iRow);

              String SHsnCode    = (String)RowVector.elementAt(5);

	      dCGST=0;
	      dSGST=0;
	      dIGST=0;
	      dCess=0;

	      if(!SSeleSupType.equals("1"))
	      {
	           dCGST=0;
		   dSGST=0;
		   dIGST=0;
		   dCess=0;
	      }
	      else
	      {
		   int iRateCheck = getRateCheck(SHsnCode);
		   if(iRateCheck>0)
		   {
			/*if(SRateStatus.equals("0"))
			{
		             getGstRate(SHsnCode);
			}
			else*/
			{
			     dCGST = common.toDouble((String)RowVector.elementAt(15));
			     dSGST = common.toDouble((String)RowVector.elementAt(16));
			     dIGST = common.toDouble((String)RowVector.elementAt(17));
			     dCess = common.toDouble((String)RowVector.elementAt(18));
			}
		   }
		   else
		   {
		        JOptionPane.showMessageDialog(null,"Gst Rate Not Updated for HsnCode-"+SHsnCode,"Information",JOptionPane.INFORMATION_MESSAGE);
		        VRateStatus.setElementAt("0",iRow);
			setColType(iRow);
		        return;
		   }
	      }

	      double dQty = 0;

	      if(SHsnType.equals("0"))
	      {
                   dQty = common.toDouble((String)RowVector.elementAt(12));
	      }
	      else
	      {
                   dQty = 1;
	      }

              double dRate       = common.toDouble((String)RowVector.elementAt(13));
              double dDiscPer    = common.toDouble((String)RowVector.elementAt(14));

              double dGross   = dQty*dRate;
              double dDisc    = dGross*dDiscPer/100;
	      double dBasic   = dGross - dDisc;
              double dCGSTVal = dBasic*dCGST/100;
              double dSGSTVal = dBasic*dSGST/100;
              double dIGSTVal = dBasic*dIGST/100;
              double dCessVal = dBasic*dCess/100;
              double dNet     = dBasic+dCGSTVal+dSGSTVal+dIGSTVal+dCessVal;

              RowVector.setElementAt(common.getRound(dCGST,2),15);
              RowVector.setElementAt(common.getRound(dSGST,2),16);
              RowVector.setElementAt(common.getRound(dIGST,2),17);
              RowVector.setElementAt(common.getRound(dCess,2),18);
              RowVector.setElementAt(common.getRound(dGross,2),19);
              RowVector.setElementAt(common.getRound(dDisc,2),20);
              RowVector.setElementAt(common.getRound(dCGSTVal,2),21);
              RowVector.setElementAt(common.getRound(dSGSTVal,2),22);
              RowVector.setElementAt(common.getRound(dIGSTVal,2),23);
              RowVector.setElementAt(common.getRound(dCessVal,2),24);
              RowVector.setElementAt(common.getRound(dNet,2),25);

	      if(SHsnType.equals("1"))
	      {
		   if(SSeleSupType.equals("1"))
		   {
			if(SRateStatus.equals("1"))
			{
				if(SSeleStateCode.equals("33"))
				{
				     ColumnData[iRow][15] = "X";
				     ColumnData[iRow][16] = "X";
				     ColumnData[iRow][17] = "N";
				     ColumnData[iRow][18] = "X";
				}
				else
				{
				     ColumnData[iRow][15] = "N";
				     ColumnData[iRow][16] = "N";
				     ColumnData[iRow][17] = "X";
				     ColumnData[iRow][18] = "X";
				}
			}
		   }
	      }

              for(int i=0;i<super.dataVector.size();i++)
              {
		  String SHType = (String)VHsnType.elementAt(i);
                  Vector curVector = (Vector)super.dataVector.elementAt(i);
                  dTGross   = dTGross+common.toDouble((String)curVector.elementAt(19));
                  dTDisc    = dTDisc+common.toDouble((String)curVector.elementAt(20));
                  dTCGST    = dTCGST+common.toDouble((String)curVector.elementAt(21));
                  dTSGST    = dTSGST+common.toDouble((String)curVector.elementAt(22));
                  dTIGST    = dTIGST+common.toDouble((String)curVector.elementAt(23));
                  dTCess    = dTCess+common.toDouble((String)curVector.elementAt(24));
                  dTNet     = dTNet+common.toDouble((String)curVector.elementAt(25));

		  if(SHType.equals("1"))
		  {
                       dTOthers = dTOthers+common.toDouble((String)curVector.elementAt(25));
		  }
              }

              dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
              LBasic.setText(common.getRound(dTGross,2));
              LDiscount.setText(common.getRound(dTDisc,2));
              LCGST.setText(common.getRound(dTCGST,2));
              LSGST.setText(common.getRound(dTSGST,2));
              LIGST.setText(common.getRound(dTIGST,2));
              LCess.setText(common.getRound(dTCess,2));
              LNet.setText(common.getRound(dTNet,2));
              LOthers.setText(common.getRound(dTOthers,2));
    }

    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
                FinalData[i][j] = ((String)curVector.elementAt(j)).trim();
        }
        return FinalData;
    }
    public int getRows()
    {
         return super.dataVector.size();
    }
    public Vector getCurVector(int i)
    {
         return (Vector)super.dataVector.elementAt(i);
    }

    public void setTaxData()
    {
	iHsnCheck = 0;
	ColumnData = new Object[super.dataVector.size()][ColumnType.length];

	setColType();

	boolean bHsnCheck = checkHsnCode();

	if(bHsnCheck)
	{
		iHsnCheck = 1;
		
		for(int i=0;i<super.dataVector.size();i++)
		{
		     Vector curVector   = (Vector)super.dataVector.elementAt(i);
		     String SHsnType    = (String)VHsnType.elementAt(i);
		     String SRateStatus = (String)VRateStatus.elementAt(i);
		     setMaterialAmount(curVector,SHsnType,SRateStatus,i);
		}
	}
	else
	{
	     iHsnCheck = 0;
	}
    }

    public boolean checkHsnCode()
    {
	for(int i=0;i<super.dataVector.size();i++)
	{
	     Vector curVector = (Vector)super.dataVector.elementAt(i);
             String SHsnCode  = (String)curVector.elementAt(5);

	     if(SHsnCode.equals(""))
	     {
                   JOptionPane.showMessageDialog(null,"HsnCode Not Updated @Row-"+String.valueOf(i+1),"Information",JOptionPane.INFORMATION_MESSAGE);
		   return false;
	     }

	     int iRateCheck=0;

	     if(SSeleSupType.equals("1"))
	     {
	          iRateCheck = getRateCheck(SHsnCode);
	     }
	     else
	     {
		  iRateCheck = 1;
	     }

	     if(iRateCheck<=0)
	     {
	          JOptionPane.showMessageDialog(null,"Gst Rate Not Updated for HsnCode-"+SHsnCode,"Information",JOptionPane.INFORMATION_MESSAGE);
	          VRateStatus.setElementAt("0",i);
	          return false;
	     }
	}
	return true;
    }

     public int getRateCheck(String SHsnCode)
     {
	int iCount=0;

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "Select Count(*) from HsnGstRate Where HsnCode='"+SHsnCode+"'";

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    iCount = result.getInt(1);
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getRateCheck :"+ex);
        }
	return iCount;
     }

     public void getGstRate(String SHsnCode)
     {
        dCGST=0;
	dSGST=0;
	dIGST=0;
	dCess=0;

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "";

	       if(SSeleStateCode.equals("33"))
	       {
		    QS = " Select nvl(CGST,0) as CGST,nvl(SGST,0) as SGST,0 as IGST,nvl(Cess,0) as Cess "+
		         " from HsnGstRate Where HsnCode='"+SHsnCode+"'";
	       }
	       else
	       {
		    QS = " Select 0 as CGST,0 as SGST,nvl(IGST,0) as IGST,nvl(Cess,0) as Cess "+
		         " from HsnGstRate Where HsnCode='"+SHsnCode+"'";
	       }

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    dCGST = result.getDouble(1);
                    dSGST = result.getDouble(2);
                    dIGST = result.getDouble(3);
                    dCess = result.getDouble(4);
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getGstRate :"+ex);
        }
     }

     public void setColType()
     {
	     ColumnType[15] = "N";
	     ColumnType[16] = "N";
	     ColumnType[17] = "N";
	     ColumnType[18] = "N";
     }

     public void setColType(int iRow)
     {
	     ColumnData[iRow][15] = "N";
	     ColumnData[iRow][16] = "N";
	     ColumnData[iRow][17] = "N";
	     ColumnData[iRow][18] = "N";
     }

    public void setColumnData()
    {
	ColumnData = new Object[super.dataVector.size()][ColumnType.length];

	setColType();
    }

}
