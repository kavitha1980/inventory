package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;
import java.net.*;

public class OthersGRNFrame1 extends JInternalFrame
{
     JPanel MiddlePanel,BottomPanel;
     JButton     BOk,btnUpdate;
     JTabbedPane JTP;
     JComboBox   JCDept;
     TabReport tabreport3,tabreport5,tabreport8;
     TabReport tabreport9,tabreport10,tabreport11,tabreport12,tabreport13,tabreport14,tabreport15;
     TabReport tabreport16;
     String 	sUser;
     MRSAuthUser			MRSUser;
     int		iUserCode;
     String                         sAuthSysName;

     // Trial Order

     Object RowData3[][];
     String ColumnData3[] = {"GI No","Date","Description","Invoice No","Invoice Date","Material","Qty","Supplier","User","Modified","UserUpdate"};
     String ColumnType3[] = {"N"    ,"S"   ,"S"          ,"S"         ,"S"           ,"S"       ,"N"  ,"S"       ,"S"	,"S"       ,"S"	      };


     // Free GRN

     Object RowData5[][];
     String ColumnData5[] = {"GI No","Date","Description","Invoice No","Invoice Date","Material","Qty","Supplier","User","Modified","UserUdate"};
     String ColumnType5[] = {"N"    ,"S"   ,"S"          ,"S"         ,"S"           ,"S"       ,"N"  ,"S"       ,"S"   ,"S"       ,"S"   };

     // RDC

     Object RowData8[][];
     String ColumnData8[] = {"GI No","Date","Description","Material","Qty","Supplier","Modified"};
     String ColumnType8[] = {"N"    ,"S"   ,"S"          ,"S"       ,"N"  ,"S"       ,"S"       };

     // Civil Direct

     Object RowData9[][];
     String ColumnData9[] = {"GI No","Date","Supplier Name","Invoice No","Invoice Date","DC No","DC Date"};
     String ColumnType9[] = {"N","S","S","S","S","S","S"};

     // Civil Contract

     Object RowData10[][];
     String ColumnData10[] = {"GI No","Date","Supplier Name","Invoice No","Invoice Date","DC No","DC Date"};
     String ColumnType10[] = {"N","S","S","S","S","S","S"};

     // Stationary

     Object RowData11[][];
     String ColumnData11[] = {"GI No","Date","Supplier Name","Invoice No","Invoice Date","DC No","DC Date"};
     String ColumnType11[] = {"N","S","S","S","S","S","S"};

     // AMC

     Object RowData12[][];
     String ColumnData12[] = {"GI No","Date","Supplier Name","Invoice No","Invoice Date","DC No","DC Date"};
     String ColumnType12[] = {"N","S","S","S","S","S","S"};

     // Beyond Service

     Object RowData13[][];
     String ColumnData13[] = {"GI No","Date","Supplier Name","Invoice No","Invoice Date","DC No","DC Date"};
     String ColumnType13[] = {"N","S","S","S","S","S","S"};

     // Rejected

     Object RowData14[][];
     String ColumnData14[] = {"GI No","Date","Supplier Name","Invoice No","Invoice Date","DC No","DC Date"};
     String ColumnType14[] = {"N","S","S","S","S","S","S"};

     // Miscellaneous

     Object RowData15[][];
     String ColumnData15[] = {"GI No","Date","Supplier Name","Invoice No","Invoice Date","DC No","DC Date"};
     String ColumnType15[] = {"N","S","S","S","S","S","S"};

     // Canteen

     Object RowData16[][];
     String ColumnData16[] = {"GI No","Date","Supplier Name","Description","Qty","Invoice No","Invoice Date","DC No","DC Date","Modified"};
     String ColumnType16[] = {"N","S","S","S","S","S","S","S","S","S"};


     Common  common  = new Common();

     JLayeredPane DeskTop;
     StatusPanel SPanel;
     Vector VCode,VName,VNameCode,VDeptCode,VDeptName;
     
     Vector VGINo3,VGIDate3,VDesc3,VInvNo3,VInvDate3,VInwNo3,VId3,VSupName3,VSupCode3,VMRSUser3;
     Vector VCode3,VQty3,VIssStatus3;

     Vector VGINo5,VGIDate5,VDesc5,VInvNo5,VInvDate5,VInwNo5,VId5,VSupName5,VSupCode5,VMRSUser5;
     Vector VCode5,VQty5,VIssStatus5;

     Vector VGINo8,VGIDate8,VDesc8,VMatName8,VQty8,VInwNo8,VId8,VSup_Name8,VSup_Code8;

     Vector VGINo9,VGIDate9,VSupName9,VDCNo9,VDCDate9,VInvNo9,VInvDate9,VSupCode9,VInwNo9;
     Vector VGINo10,VGIDate10,VSupName10,VDCNo10,VDCDate10,VInvNo10,VInvDate10,VSupCode10,VInwNo10;
     Vector VGINo11,VGIDate11,VSupName11,VDCNo11,VDCDate11,VInvNo11,VInvDate11,VSupCode11,VInwNo11;
     Vector VGINo12,VGIDate12,VSupName12,VDCNo12,VDCDate12,VInvNo12,VInvDate12,VSupCode12,VInwNo12;
     Vector VGINo13,VGIDate13,VSupName13,VDCNo13,VDCDate13,VInvNo13,VInvDate13,VSupCode13,VInwNo13;
     Vector VGINo14,VGIDate14,VSupName14,VDCNo14,VDCDate14,VInvNo14,VInvDate14,VSupCode14,VInwNo14;
     Vector VGINo15,VGIDate15,VSupName15,VDCNo15,VDCDate15,VInvNo15,VInvDate15,VSupCode15,VInwNo15;
     Vector VGINo16,VGIDate16,VSupName16,VDesc16,VQty16,VDCNo16,VDCDate16,VInvNo16,VInvDate16,VSupCode16,VInwNo16,VId16;

     Vector VInwName,VInwNo;
     Vector VSupplier,VSupCode;

     InwModiFrame imframe;
     CanteenFrame canteenframe;
     GIRDCLinkFrame girdclinkframe;
     CodeSelectionFrame codeselectionframe;

     DateField df = new DateField();
     int iMillCode,iAuthCode;
     String SItemTable,SSupTable;

     public OthersGRNFrame1(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iMillCode,int iAuthCode,String SItemTable,String SSupTable)
     {
         super("Materials Received @ Gate But Not Accounted In Stores");
         this.DeskTop    = DeskTop;
         this.VCode      = VCode;
         this.VName      = VName;
         this.VNameCode  = VNameCode;
         this.SPanel     = SPanel;
         this.iMillCode  = iMillCode;
         this.iAuthCode  = iAuthCode;
         this.SItemTable = SItemTable;
         this.SSupTable  = SSupTable;
	   

         setVectors();
	    getTrialPendingCount();	
         createComponents();
         setLayouts();
         addComponents();
         addListeners();
	try{
          sAuthSysName=InetAddress.getLocalHost().getHostName();
       }
     catch(Exception e){
          e.printStackTrace();
       }

     }
     public void createComponents()
     {
          MiddlePanel  = new JPanel();
          BottomPanel  = new JPanel();
          JTP          = new JTabbedPane();
          JCDept       = new JComboBox(VDeptName);
          BOk          = new JButton("Modify Gate-Inward");
          btnUpdate    = new JButton("User Update");
          df.setTodayDate();
     }

     public void setLayouts()
     {
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,650,500);
         MiddlePanel.setLayout(new BorderLayout());
     }

     public void addComponents()
     {
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

          MiddlePanel.add("Center",JTP);
          if(iAuthCode>1)
          {
               BottomPanel.add(BOk);
		   BottomPanel.add(btnUpdate);
	
          }

          setDataIntoVector();
          setRowData();

          try
          {
             tabreport3 = new TabReport(RowData3,ColumnData3,ColumnType3);
             tabreport3.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                 ((TableColumn)tabreport3.ReportTable.getColumn("Description")).setPreferredWidth(250);
             tabreport3.ReportTable.addKeyListener(new KeyList3());

             tabreport5 = new TabReport(RowData5,ColumnData5,ColumnType5);
             tabreport5.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                 ((TableColumn)tabreport5.ReportTable.getColumn("Description")).setPreferredWidth(250);
             tabreport5.ReportTable.addKeyListener(new KeyList5());

             tabreport8 = new TabReport(RowData8,ColumnData8,ColumnType8);
             tabreport8.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                 ((TableColumn)tabreport8.ReportTable.getColumn("Description")).setPreferredWidth(250);
             tabreport8.ReportTable.addKeyListener(new KeyList8());

             tabreport9 = new TabReport(RowData9,ColumnData9,ColumnType9);
             tabreport9.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                 ((TableColumn)tabreport9.ReportTable.getColumn("Supplier Name")).setPreferredWidth(250);

             tabreport10 = new TabReport(RowData10,ColumnData10,ColumnType10);
             tabreport10.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                 ((TableColumn)tabreport10.ReportTable.getColumn("Supplier Name")).setPreferredWidth(250);

             tabreport11 = new TabReport(RowData11,ColumnData11,ColumnType11);
             tabreport11.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                 ((TableColumn)tabreport11.ReportTable.getColumn("Supplier Name")).setPreferredWidth(250);

             tabreport12 = new TabReport(RowData12,ColumnData12,ColumnType12);
             tabreport12.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                 ((TableColumn)tabreport12.ReportTable.getColumn("Supplier Name")).setPreferredWidth(250);

             tabreport13 = new TabReport(RowData13,ColumnData13,ColumnType13);
             tabreport13.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                 ((TableColumn)tabreport13.ReportTable.getColumn("Supplier Name")).setPreferredWidth(250);

             tabreport14 = new TabReport(RowData14,ColumnData14,ColumnType14);
             tabreport14.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                 ((TableColumn)tabreport14.ReportTable.getColumn("Supplier Name")).setPreferredWidth(250);

             tabreport15 = new TabReport(RowData15,ColumnData15,ColumnType15);
             tabreport15.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                 ((TableColumn)tabreport15.ReportTable.getColumn("Supplier Name")).setPreferredWidth(250);

             tabreport16 = new TabReport(RowData16,ColumnData16,ColumnType16);
             tabreport16.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                 ((TableColumn)tabreport16.ReportTable.getColumn("Supplier Name")).setPreferredWidth(250);
             tabreport16.ReportTable.addKeyListener(new KeyList16());


             JTP.addTab("Trial Order",tabreport3);
             JTP.addTab("Free GRN",tabreport5);
             JTP.addTab("RDC",tabreport8);
             JTP.addTab("Civil Direct",tabreport9);
             JTP.addTab("Civil Contract",tabreport10);
             JTP.addTab("Stationery",tabreport11);
             JTP.addTab("AMC",tabreport12);
             JTP.addTab("Beyond Service",tabreport13);
             JTP.addTab("Rejected",tabreport14);
             JTP.addTab("Miscellaneous",tabreport15);
             JTP.addTab("Canteen",tabreport16);

             setSelected(true);
             DeskTop.repaint();
             DeskTop.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println("Prob"+ex);
          }
     }
     public void addListeners()
     {
          BOk.addActionListener(new ModiList());   
          btnUpdate.addActionListener(new ModiList());   

     }
     private class ModiList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
		if(ae.getSource()==BOk){
               BOk.setEnabled(false);
               updateGateInward();
               }
		if(ae.getSource()==btnUpdate){
		   updateDeptUser();

		}
          }
     }

     private void updateDeptUser(){

          try
          {
		   String 		sUserName;	
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
          	
               for(int i=0;i<RowData3.length;i++)
               {
                    Boolean bValue = (Boolean)RowData3[i][10];
                    if(!bValue.booleanValue())
                         continue;
                    String SGINo    = (String)VGINo3.elementAt(i);
			  sUserName		= (String)tabreport3.ReportTable.getModel().getValueAt(i,8);
			  
                    String QS = "Update GateInward Set ";
 			  QS = QS+" MRSAuthUserCode    ="+iUserCode+",";
                    QS = QS+" SysName    ='"+sAuthSysName+"',";
                    QS = QS+" DateandTime  = to_Char(sysdate,'DD-MON-YYYY:hh:mi:ss')";
                    QS = QS+" Where GINo = "+SGINo;
                    QS = QS+" and MillCode = "+iMillCode;

                    stat.execute(QS);
			//System.out.println("QS:"+QS);
               }

		    for(int i=0;i<RowData5.length;i++)
               {
                    Boolean bValue = (Boolean)RowData5[i][10];
                    if(!bValue.booleanValue())
                         continue;
                    String SGINo    = (String)VGINo5.elementAt(i);
			  sUserName		= (String)tabreport5.ReportTable.getModel().getValueAt(i,8);
			  
                    String QS = "Update GateInward Set ";
 			  QS = QS+" MRSAuthUserCode    ="+iUserCode+",";
                    QS = QS+" SysName    ='"+sAuthSysName+"',";
                    QS = QS+" DateandTime  = to_Char(sysdate,'DD-MON-YYYY:hh:mi:ss')";
                    QS = QS+" Where GINo = "+SGINo;
                    QS = QS+" and MillCode = "+iMillCode;

                    stat.execute(QS);
		//	System.out.println("QS:"+QS);
               }

	    }
	   catch(Exception ex)
          {
               System.out.println(ex);
          }
          removeHelpFrame();

     }	

     private void updateGateInward()
     {
          try
          {
		   String 		sUserName;	
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
          
               for(int i=0;i<RowData3.length;i++)
               {
                    Boolean bValue = (Boolean)RowData3[i][8];
                    if(!bValue.booleanValue())
                         continue;
                    String SSupCode = (String)VSupCode3.elementAt(i);
                    String SInwNo   = (String)VInwNo3.elementAt(i);
                    String SGINo    = (String)VGINo3.elementAt(i);
			  sUserName		= (String)tabreport3.ReportTable.getModel().getValueAt(i,8);


               String QS = "Update GateInward Set ";
                    QS = QS+" Sup_Code = '"+SSupCode+"',";
                    QS = QS+" InwNo    ="+SInwNo;
                    QS = QS+" Where GINo = "+SGINo;
                    QS = QS+" and MillCode = "+iMillCode;

                    stat.execute(QS);
			System.out.println("QS:"+QS);
               }
               for(int i=0;i<RowData5.length;i++)
               {
                    Boolean bValue = (Boolean)RowData5[i][8];
                    if(!bValue.booleanValue())
                         continue;
                    String SSupCode = (String)VSupCode5.elementAt(i);
                    String SInwNo   = (String)VInwNo5.elementAt(i);
                    String SGINo    = (String)VGINo5.elementAt(i);
     
                    String QS = "Update GateInward Set ";
                    QS = QS+" Sup_Code = '"+SSupCode+"',";
                    QS = QS+" InwNo    ="+SInwNo;
                    QS = QS+" Where GINo = "+SGINo;
                    QS = QS+" and MillCode = "+iMillCode;

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          removeHelpFrame();
     }

     private void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex){}
     }
     public void setDataIntoVector()
     {
          String QString3  = "";
          String QString5  = "";
          String QString8  = "";
          String QString9  = "";
          String QString10 = "";
          String QString11 = "";
          String QString12 = "";
          String QString13 = "";
          String QString14 = "";
          String QString15 = "";
          String QString16 = "";

            QString3 = " SELECT GateInward.GINo,GateInward.GIDate,GateInward.InvNo,GateInward.InvDate,GateInward.Item_Name,GateInward.Id,GateInward.GateQty,"+SSupTable+".Name, "+
                       " GateInward.Sup_Code,GateInward.IssueStatus,'' as Item_Code, UserName "+
                       " From GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code "+
			     " Left Join RawUser on GateInward.MRSAuthUserCode = RawUser.UserCode "+	
                       " Where GateInward.InwNo = 1 and GateInward.IssueStatus = 0 "+
                       " And GateInward.GrnNo = 0 And (trim(GateInward.Item_Code) is Null) "+
                       " And GateInward.MillCode = "+iMillCode+
                       " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1  and GateInward.TrialOrderAuthStatus=0"+
                       " Union All "+
                       " SELECT GateInward.GINo,GateInward.GIDate,GateInward.InvNo,GateInward.InvDate,InvItems.Item_Name,GateInward.Id,GateInward.GateQty,"+SSupTable+".Name, "+
                       " GateInward.Sup_Code,GateInward.IssueStatus,GateInward.Item_Code, UserName "+
                       " From (GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                       " Inner Join InvItems on GateInward.Item_Code=InvItems.Item_Code "+
			     " Left Join RawUser on GateInward.MRSAuthUserCode = RawUser.UserCode "+	
                       " Where GateInward.InwNo = 1 and GateInward.IssueStatus = 0 "+
                       " And GateInward.GrnNo = 0 And (trim(GateInward.Item_Name) is Null) "+
                       " And GateInward.MillCode = "+iMillCode+
                       " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 and GateInward.TrialOrderAuthStatus=0"+
                       " Order By 1,5";

            QString5 = " SELECT GateInward.GINo,GateInward.GIDate,GateInward.InvNo,GateInward.InvDate,GateInward.Item_Name,GateInward.Id,GateInward.GateQty,"+SSupTable+".Name, "+
                       " GateInward.Sup_Code,GateInward.IssueStatus,'' as Item_Code, UserName"+
                       " From GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code "+
			     " Left Join RawUser on GateInward.MRSAuthUserCode = RawUser.UserCode "+	
                       " Where GateInward.InwNo = 2 and GateInward.IssueStatus = 0 "+
                       " And GateInward.GrnNo = 0 And (trim(GateInward.Item_Code) is Null) "+
                       " And GateInward.MillCode = "+iMillCode+
                       " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 And TrialOrderAuthStatus=0"+
                       " Union All "+
                       " SELECT GateInward.GINo,GateInward.GIDate,GateInward.InvNo,GateInward.InvDate,InvItems.Item_Name,GateInward.Id,GateInward.GateQty,"+SSupTable+".Name, "+
                       " GateInward.Sup_Code,GateInward.IssueStatus,GateInward.Item_Code, UserName"+
                       " From (GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                       " Inner Join InvItems on GateInward.Item_Code=InvItems.Item_Code "+
			     " Left Join RawUser on GateInward.MRSAuthUserCode = RawUser.UserCode "+	
                       " Where GateInward.InwNo = 2 and GateInward.IssueStatus = 0 "+
                       " And GateInward.GrnNo = 0 And (trim(GateInward.Item_Name) is Null) "+
                       " And GateInward.MillCode = "+iMillCode+
                       " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 And TrialOrderAuthStatus=0"+
                       " Order By 1,5";

            QString8 = " SELECT GateInward.GINo,GateInward.GIDate,GateInward.Item_Name,GateInward.Id,GateInward.GateQty,"+SSupTable+".Name,GateInward.Sup_Code "+
                       " From GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code "+
                       " Where GateInward.InwNo = 10 And GateInward.GrnNo = 0  "+
                       " And GateInward.MillCode = "+iMillCode+
                       " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                       " Order By 1,3";

            QString9 = " SELECT GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.Sup_Code,GateInward.InwNo "+
                       " From (GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                       " Where GateInward.InwNo = 4 "+
                       " And GateInward.MillCode = "+iMillCode+
                       " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                       " Group By GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.GrnNo,GateInward.Sup_Code,GateInward.InwNo "+
                       " Having GateInward.GrnNo = 0 Order By 3";

            QString10 = " SELECT GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.Sup_Code,GateInward.InwNo "+
                        " From (GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                        " Where GateInward.InwNo = 5 "+
                        " And GateInward.MillCode = "+iMillCode+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                        " Group By GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.GrnNo,GateInward.Sup_Code,GateInward.InwNo "+
                        " Having GateInward.GrnNo = 0 Order By 3";

            QString11 = " SELECT GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.Sup_Code,GateInward.InwNo "+
                        " From (GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                        " Where GateInward.InwNo = 6 "+
                        " And GateInward.MillCode = "+iMillCode+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                        " Group By GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.GrnNo,GateInward.Sup_Code,GateInward.InwNo "+
                        " Having GateInward.GrnNo = 0 Order By 3";

            QString12 = " SELECT GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.Sup_Code,GateInward.InwNo "+
                        " From (GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                        " Where GateInward.InwNo = 7 "+
                        " And GateInward.MillCode = "+iMillCode+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                        " Group By GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.GrnNo,GateInward.Sup_Code,GateInward.InwNo "+
                        " Having GateInward.GrnNo = 0 Order By 3";

            QString13 = " SELECT GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.Sup_Code,GateInward.InwNo "+
                        " From (GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                        " Where GateInward.InwNo = 8 "+
                        " And GateInward.MillCode = "+iMillCode+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                        " Group By GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.GrnNo,GateInward.Sup_Code,GateInward.InwNo "+
                        " Having GateInward.GrnNo = 0 Order By 3";

            QString14 = " SELECT GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.Sup_Code,GateInward.InwNo "+
                        " From (GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                        " Where GateInward.InwNo = 9 "+
                        " And GateInward.MillCode = "+iMillCode+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                        " Group By GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.GrnNo,GateInward.Sup_Code,GateInward.InwNo "+
                        " Having GateInward.GrnNo = 0 Order By 3";

            QString15 = " SELECT GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.Sup_Code,GateInward.InwNo "+
                        " From (GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                        " Where GateInward.InwNo = 11 "+
                        " And GateInward.MillCode = "+iMillCode+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                        " Group By GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.GrnNo,GateInward.Sup_Code,GateInward.InwNo "+
                        " Having GateInward.GrnNo = 0 Order By 3";

            QString16 = " SELECT GateInward.GINo,GateInward.GIDate,"+SSupTable+".Name,GateInward.DcNo,GateInward.DCDate,GateInward.InvNo,GateInward.InvDate,GateInward.Sup_Code,GateInward.InwNo,GateInward.Item_Name,GateInward.GateQty,GateInward.Id "+
                        " From (GateInward Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInward.Sup_Code) "+
                        " Where GateInward.InwNo = 14 And GateInward.GrnNo = 0 And ( GateInward.Status <> 1 Or GateInward.Status is Null ) "+
                        " And GateInward.MillCode = "+iMillCode+
                        " And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 "+
                        " Order By 1,3";

            VGINo3      = new Vector();
            VGIDate3    = new Vector();
            VDesc3      = new Vector();
            VInvNo3     = new Vector();
            VInvDate3   = new Vector();
            VInwNo3     = new Vector();
            VId3        = new Vector();
            VSupName3   = new Vector();
            VSupCode3   = new Vector();
            VCode3      = new Vector();
            VQty3       = new Vector();
            VIssStatus3 = new Vector();
            VMRSUser3   = new Vector();


            VGINo5      = new Vector();
            VGIDate5    = new Vector();
            VDesc5      = new Vector();
            VInvNo5     = new Vector();
            VInvDate5   = new Vector();
            VInwNo5     = new Vector();
            VId5        = new Vector();
            VSupName5   = new Vector();
            VSupCode5   = new Vector();
            VCode5      = new Vector();
            VQty5       = new Vector();
            VIssStatus5 = new Vector();
            VMRSUser5   = new Vector();


            VGINo8    = new Vector();
            VGIDate8  = new Vector();
            VDesc8    = new Vector();
            VMatName8 = new Vector();
            VInwNo8   = new Vector();
            VId8      = new Vector();
            VQty8     = new Vector();
            VSup_Name8= new Vector();
            VSup_Code8= new Vector();


            VGINo9    = new Vector();
            VGIDate9  = new Vector();
            VSupName9 = new Vector();
            VDCNo9    = new Vector();
            VDCDate9  = new Vector();
            VInvNo9   = new Vector();
            VInvDate9 = new Vector();
            VSupCode9 = new Vector();
            VInwNo9   = new Vector();

            VGINo10    = new Vector();
            VGIDate10  = new Vector();
            VSupName10 = new Vector();
            VDCNo10    = new Vector();
            VDCDate10  = new Vector();
            VInvNo10   = new Vector();
            VInvDate10 = new Vector();
            VSupCode10 = new Vector();
            VInwNo10   = new Vector();

            VGINo11    = new Vector();
            VGIDate11  = new Vector();
            VSupName11 = new Vector();
            VDCNo11    = new Vector();
            VDCDate11  = new Vector();
            VInvNo11   = new Vector();
            VInvDate11 = new Vector();
            VSupCode11 = new Vector();
            VInwNo11   = new Vector();

            VGINo12    = new Vector();
            VGIDate12  = new Vector();
            VSupName12 = new Vector();
            VDCNo12    = new Vector();
            VDCDate12  = new Vector();
            VInvNo12   = new Vector();
            VInvDate12 = new Vector();
            VSupCode12 = new Vector();
            VInwNo12   = new Vector();

            VGINo13    = new Vector();
            VGIDate13  = new Vector();
            VSupName13 = new Vector();
            VDCNo13    = new Vector();
            VDCDate13  = new Vector();
            VInvNo13   = new Vector();
            VInvDate13 = new Vector();
            VSupCode13 = new Vector();
            VInwNo13   = new Vector();

            VGINo14    = new Vector();
            VGIDate14  = new Vector();
            VSupName14 = new Vector();
            VDCNo14    = new Vector();
            VDCDate14  = new Vector();
            VInvNo14   = new Vector();
            VInvDate14 = new Vector();
            VSupCode14 = new Vector();
            VInwNo14   = new Vector();

            VGINo15    = new Vector();
            VGIDate15  = new Vector();
            VSupName15 = new Vector();
            VDCNo15    = new Vector();
            VDCDate15  = new Vector();
            VInvNo15   = new Vector();
            VInvDate15 = new Vector();
            VSupCode15 = new Vector();
            VInwNo15   = new Vector();


            VGINo16    = new Vector();
            VGIDate16  = new Vector();
            VSupName16 = new Vector();
            VDCNo16    = new Vector();
            VDCDate16  = new Vector();
            VInvNo16   = new Vector();
            VInvDate16 = new Vector();
            VSupCode16 = new Vector();
            VInwNo16   = new Vector();
            VDesc16    = new Vector();
            VQty16     = new Vector();
            VId16      = new Vector();

            try
            {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();
              ResultSet res3 = stat.executeQuery(QString3);
		//System.out.println("Trial Order:"+QString3);
              while (res3.next())
              {
                    VGINo3   .addElement(""+ res3.getString(1));
                    VGIDate3 .addElement(""+ common.parseDate(res3.getString(2)));
                    VInvNo3  .addElement(""+ res3.getString(3));
                    VInvDate3.addElement(""+ common.parseDate(res3.getString(4)));
                    VDesc3.addElement(""+res3.getString(5));
                    VId3.addElement(res3.getString(6));
                    VInwNo3.addElement("1");
                    VQty3.addElement(res3.getString(7));
                    VSupName3.addElement(common.parseNull(res3.getString(8)));
                    VSupCode3.addElement(res3.getString(9));
                    VIssStatus3.addElement(res3.getString(10));
                    VCode3.addElement(res3.getString(11));
                    VMRSUser3.addElement(res3.getString(12));
              }
              res3.close();

              ResultSet res5 = stat.executeQuery(QString5);
              while (res5.next())
              {
                    VGINo5   .addElement(""+ res5.getString(1));
                    VGIDate5 .addElement(""+ common.parseDate(res5.getString(2)));
                    VInvNo5  .addElement(""+ res5.getString(3));
                    VInvDate5.addElement(""+ common.parseDate(res5.getString(4)));
                    VDesc5.addElement(""+res5.getString(5));
                    VId5.addElement(res5.getString(6));
                    VInwNo5.addElement("2");
                    VQty5.addElement(res5.getString(7));
                    VSupName5.addElement(common.parseNull(res5.getString(8)));
                    VSupCode5.addElement(res5.getString(9));
                    VIssStatus5.addElement(res5.getString(10));
                    VCode5.addElement(res5.getString(11));
                    VMRSUser5.addElement(res5.getString(12));
              }
              res5.close();

              ResultSet res8 = stat.executeQuery(QString8);
              while (res8.next())
              {
                    VGINo8   .addElement(""+ res8.getString(1));
                    VGIDate8 .addElement(""+ common.parseDate(res8.getString(2)));
                    VDesc8.addElement(""+res8.getString(3));
                    VId8.addElement(res8.getString(4));
                    VMatName8.addElement("");
                    VInwNo8.addElement("10");
                    VQty8.addElement(res8.getString(5));
                    VSup_Name8.addElement(common.parseNull(res8.getString(6)));
                    VSup_Code8.addElement(res8.getString(7));
              }
              res8.close();

              ResultSet res9 = stat.executeQuery(QString9);
              while (res9.next())
              {
                    VGINo9   .addElement(""+ res9.getString(1));
                    VGIDate9 .addElement(""+ common.parseDate(res9.getString(2)));
                    VSupName9.addElement(""+ res9.getString(3));
                    VDCNo9   .addElement(""+ res9.getString(4));
                    VDCDate9 .addElement(""+ common.parseDate(res9.getString(5)));
                    VInvNo9  .addElement(""+ res9.getString(6));
                    VInvDate9.addElement(""+ common.parseDate(res9.getString(7)));
                    VSupCode9.addElement(res9.getString(8));
                    VInwNo9.addElement(res9.getString(9));
              }
              res9.close();

              ResultSet res10 = stat.executeQuery(QString10);
              while (res10.next())
              {
                    VGINo10   .addElement(""+ res10.getString(1));
                    VGIDate10 .addElement(""+ common.parseDate(res10.getString(2)));
                    VSupName10.addElement(""+ res10.getString(3));
                    VDCNo10   .addElement(""+ res10.getString(4));
                    VDCDate10 .addElement(""+ common.parseDate(res10.getString(5)));
                    VInvNo10  .addElement(""+ res10.getString(6));
                    VInvDate10.addElement(""+ common.parseDate(res10.getString(7)));
                    VSupCode10.addElement(res10.getString(8));
                    VInwNo10.addElement(res10.getString(9));
              }
              res10.close();

              ResultSet res11 = stat.executeQuery(QString11);
              while (res11.next())
              {
                    VGINo11   .addElement(""+ res11.getString(1));
                    VGIDate11 .addElement(""+ common.parseDate(res11.getString(2)));
                    VSupName11.addElement(""+ res11.getString(3));
                    VDCNo11   .addElement(""+ res11.getString(4));
                    VDCDate11 .addElement(""+ common.parseDate(res11.getString(5)));
                    VInvNo11  .addElement(""+ res11.getString(6));
                    VInvDate11.addElement(""+ common.parseDate(res11.getString(7)));
                    VSupCode11.addElement(res11.getString(8));
                    VInwNo11.addElement(res11.getString(9));
              }
              res11.close();

              ResultSet res12 = stat.executeQuery(QString12);
              while (res12.next())
              {
                    VGINo12   .addElement(""+ res12.getString(1));
                    VGIDate12 .addElement(""+ common.parseDate(res12.getString(2)));
                    VSupName12.addElement(""+ res12.getString(3));
                    VDCNo12   .addElement(""+ res12.getString(4));
                    VDCDate12 .addElement(""+ common.parseDate(res12.getString(5)));
                    VInvNo12  .addElement(""+ res12.getString(6));
                    VInvDate12.addElement(""+ common.parseDate(res12.getString(7)));
                    VSupCode12.addElement(res12.getString(8));
                    VInwNo12.addElement(res12.getString(9));
              }
              res12.close();

              ResultSet res13 = stat.executeQuery(QString13);
              while (res13.next())
              {
                    VGINo13   .addElement(""+ res13.getString(1));
                    VGIDate13 .addElement(""+ common.parseDate(res13.getString(2)));
                    VSupName13.addElement(""+ res13.getString(3));
                    VDCNo13   .addElement(""+ res13.getString(4));
                    VDCDate13 .addElement(""+ common.parseDate(res13.getString(5)));
                    VInvNo13  .addElement(""+ res13.getString(6));
                    VInvDate13.addElement(""+ common.parseDate(res13.getString(7)));
                    VSupCode13.addElement(res13.getString(8));
                    VInwNo13.addElement(res13.getString(9));
              }
              res13.close();

              ResultSet res14 = stat.executeQuery(QString14);
              while (res14.next())
              {
                    VGINo14   .addElement(""+ res14.getString(1));
                    VGIDate14 .addElement(""+ common.parseDate(res14.getString(2)));
                    VSupName14.addElement(""+ res14.getString(3));
                    VDCNo14   .addElement(""+ res14.getString(4));
                    VDCDate14 .addElement(""+ common.parseDate(res14.getString(5)));
                    VInvNo14  .addElement(""+ res14.getString(6));
                    VInvDate14.addElement(""+ common.parseDate(res14.getString(7)));
                    VSupCode14.addElement(res14.getString(8));
                    VInwNo14.addElement(res14.getString(9));
              }
              res14.close();

              ResultSet res15 = stat.executeQuery(QString15);
              while (res15.next())
              {
                    VGINo15   .addElement(""+ res15.getString(1));
                    VGIDate15 .addElement(""+ common.parseDate(res15.getString(2)));
                    VSupName15.addElement(""+ res15.getString(3));
                    VDCNo15   .addElement(""+ res15.getString(4));
                    VDCDate15 .addElement(""+ common.parseDate(res15.getString(5)));
                    VInvNo15  .addElement(""+ res15.getString(6));
                    VInvDate15.addElement(""+ common.parseDate(res15.getString(7)));
                    VSupCode15.addElement(res15.getString(8));
                    VInwNo15.addElement(res15.getString(9));
              }
              res15.close();

              ResultSet res16 = stat.executeQuery(QString16);
              while (res16.next())
              {
                    VGINo16   .addElement(""+ res16.getString(1));
                    VGIDate16 .addElement(""+ common.parseDate(res16.getString(2)));
                    VSupName16.addElement(""+ res16.getString(3));
                    VDCNo16   .addElement(""+ res16.getString(4));
                    VDCDate16 .addElement(""+ common.parseDate(res16.getString(5)));
                    VInvNo16  .addElement(""+ res16.getString(6));
                    VInvDate16.addElement(""+ common.parseDate(res16.getString(7)));
                    VSupCode16.addElement(res16.getString(8));
                    VInwNo16.addElement(res16.getString(9));
                    VDesc16.addElement(res16.getString(10));
                    VQty16.addElement(res16.getString(11));
                    VId16.addElement(res16.getString(12));
              }
              res16.close();
              stat.close();
           }
           catch(Exception ex){System.out.println("sql "+ex);}
     }
     public void setRowData()
     {
         RowData3     = new Object[VGINo3.size()][ColumnData3.length];
         RowData5     = new Object[VGINo5.size()][ColumnData5.length];
         RowData8     = new Object[VGINo8.size()][ColumnData8.length];
         RowData9     = new Object[VGINo9.size()][ColumnData9.length];
         RowData10    = new Object[VGINo10.size()][ColumnData10.length];
         RowData11    = new Object[VGINo11.size()][ColumnData11.length];
         RowData12    = new Object[VGINo12.size()][ColumnData12.length];
         RowData13    = new Object[VGINo13.size()][ColumnData13.length];
         RowData14    = new Object[VGINo14.size()][ColumnData14.length];
         RowData15    = new Object[VGINo15.size()][ColumnData15.length];
         RowData16    = new Object[VGINo16.size()][ColumnData16.length];


        for(int i=0;i<VGINo3.size();i++)
        {
               RowData3[i][0]  = (String)VGINo3.elementAt(i);
               RowData3[i][1]  = (String)VGIDate3.elementAt(i);
               RowData3[i][2]  = (String)VDesc3.elementAt(i);
               RowData3[i][3]  = (String)VInvNo3.elementAt(i);
               RowData3[i][4]  = (String)VInvDate3.elementAt(i);
               RowData3[i][5]  = "";
               RowData3[i][6]  = (String)VQty3.elementAt(i);
               RowData3[i][7]  = common.parseNull((String)VSupName3.elementAt(i));
               RowData3[i][8]  = common.parseNull((String)VMRSUser3.elementAt(i));
               RowData3[i][9]  = new Boolean(false);
               RowData3[i][10] = new Boolean(false);

        }  
		System.out.println(" User Size:"+VMRSUser3.size());

        for(int i=0;i<VGINo5.size();i++)
        {
               RowData5[i][0]  = (String)VGINo5.elementAt(i);
               RowData5[i][1]  = (String)VGIDate5.elementAt(i);
               RowData5[i][2]  = (String)VDesc5.elementAt(i);
               RowData5[i][3]  = (String)VInvNo5.elementAt(i);
               RowData5[i][4]  = (String)VInvDate5.elementAt(i);
               RowData5[i][5]  = "";
               RowData5[i][6]  = (String)VQty5.elementAt(i);
               RowData5[i][7]  = common.parseNull((String)VSupName5.elementAt(i));
               RowData5[i][8]  = common.parseNull((String)VMRSUser5.elementAt(i));
               RowData5[i][9]  = new Boolean(false);
               RowData5[i][10] = new Boolean(false);

        }  
        for(int i=0;i<VGINo8.size();i++)
        {
               RowData8[i][0]  = (String)VGINo8.elementAt(i);
               RowData8[i][1]  = (String)VGIDate8.elementAt(i);
               RowData8[i][2]  = (String)VDesc8.elementAt(i);
               RowData8[i][3]  = "";
               RowData8[i][4]  = (String)VQty8.elementAt(i);
               RowData8[i][5]  = common.parseNull((String)VSup_Name8.elementAt(i));
               RowData8[i][6]  = new Boolean(false);
        }
        for(int i=0;i<VGINo9.size();i++)
        {
               RowData9[i][0]  = (String)VGINo9.elementAt(i);
               RowData9[i][1]  = (String)VGIDate9.elementAt(i);
               RowData9[i][2]  = (String)VSupName9.elementAt(i);
               RowData9[i][3]  = (String)VInvNo9.elementAt(i);
               RowData9[i][4]  = (String)VInvDate9.elementAt(i);
               RowData9[i][5]  = (String)VDCNo9.elementAt(i);
               RowData9[i][6]  = (String)VDCDate9.elementAt(i);
        }  
        for(int i=0;i<VGINo10.size();i++)
        {
               RowData10[i][0]  = (String)VGINo10.elementAt(i);
               RowData10[i][1]  = (String)VGIDate10.elementAt(i);
               RowData10[i][2]  = (String)VSupName10.elementAt(i);
               RowData10[i][3]  = (String)VInvNo10.elementAt(i);
               RowData10[i][4]  = (String)VInvDate10.elementAt(i);
               RowData10[i][5]  = (String)VDCNo10.elementAt(i);
               RowData10[i][6]  = (String)VDCDate10.elementAt(i);
        }  
        for(int i=0;i<VGINo11.size();i++)
        {
               RowData11[i][0]  = (String)VGINo11.elementAt(i);
               RowData11[i][1]  = (String)VGIDate11.elementAt(i);
               RowData11[i][2]  = (String)VSupName11.elementAt(i);
               RowData11[i][3]  = (String)VInvNo11.elementAt(i);
               RowData11[i][4]  = (String)VInvDate11.elementAt(i);
               RowData11[i][5]  = (String)VDCNo11.elementAt(i);
               RowData11[i][6]  = (String)VDCDate11.elementAt(i);
        }  
        for(int i=0;i<VGINo12.size();i++)
        {
               RowData12[i][0]  = (String)VGINo12.elementAt(i);
               RowData12[i][1]  = (String)VGIDate12.elementAt(i);
               RowData12[i][2]  = (String)VSupName12.elementAt(i);
               RowData12[i][3]  = (String)VInvNo12.elementAt(i);
               RowData12[i][4]  = (String)VInvDate12.elementAt(i);
               RowData12[i][5]  = (String)VDCNo12.elementAt(i);
               RowData12[i][6]  = (String)VDCDate12.elementAt(i);
        }  
        for(int i=0;i<VGINo13.size();i++)
        {
               RowData13[i][0]  = (String)VGINo13.elementAt(i);
               RowData13[i][1]  = (String)VGIDate13.elementAt(i);
               RowData13[i][2]  = (String)VSupName13.elementAt(i);
               RowData13[i][3]  = (String)VInvNo13.elementAt(i);
               RowData13[i][4]  = (String)VInvDate13.elementAt(i);
               RowData13[i][5]  = (String)VDCNo13.elementAt(i);
               RowData13[i][6]  = (String)VDCDate13.elementAt(i);
        }  
        for(int i=0;i<VGINo14.size();i++)
        {
               RowData14[i][0]  = (String)VGINo14.elementAt(i);
               RowData14[i][1]  = (String)VGIDate14.elementAt(i);
               RowData14[i][2]  = (String)VSupName14.elementAt(i);
               RowData14[i][3]  = (String)VInvNo14.elementAt(i);
               RowData14[i][4]  = (String)VInvDate14.elementAt(i);
               RowData14[i][5]  = (String)VDCNo14.elementAt(i);
               RowData14[i][6]  = (String)VDCDate14.elementAt(i);
        }  
        for(int i=0;i<VGINo15.size();i++)
        {
               RowData15[i][0]  = (String)VGINo15.elementAt(i);
               RowData15[i][1]  = (String)VGIDate15.elementAt(i);
               RowData15[i][2]  = (String)VSupName15.elementAt(i);
               RowData15[i][3]  = (String)VInvNo15.elementAt(i);
               RowData15[i][4]  = (String)VInvDate15.elementAt(i);
               RowData15[i][5]  = (String)VDCNo15.elementAt(i);
               RowData15[i][6]  = (String)VDCDate15.elementAt(i);
        }  
        for(int i=0;i<VGINo16.size();i++)
        {
               RowData16[i][0]  = (String)VGINo16.elementAt(i);
               RowData16[i][1]  = (String)VGIDate16.elementAt(i);
               RowData16[i][2]  = (String)VSupName16.elementAt(i);
               RowData16[i][3]  = (String)VDesc16.elementAt(i);
               RowData16[i][4]  = (String)VQty16.elementAt(i);
               RowData16[i][5]  = (String)VInvNo16.elementAt(i);
               RowData16[i][6]  = (String)VInvDate16.elementAt(i);
               RowData16[i][7]  = (String)VDCNo16.elementAt(i);
               RowData16[i][8]  = (String)VDCDate16.elementAt(i);
               RowData16[i][9]  = new Boolean(false);
        }  


     }
     public class KeyList3 extends KeyAdapter
     {
           public void keyPressed(KeyEvent ke)
           {
                if (ke.getKeyCode() == KeyEvent.VK_INSERT)
                {
                    showInwModiFrame(3);
                }
                if(iMillCode==1)
                {
                     if (ke.getKeyCode() == KeyEvent.VK_HOME)
                     {
                         showCodeSelectionFrame(3);
                     }
                }
           } 


    public void keyReleased(KeyEvent ke){
    
	  if(ke.getKeyCode()==KeyEvent.VK_F2)
	    {
	   try{
      	     int iRow             = tabreport3.ReportTable.getSelectedRow();
//               int iColumn          = theTable.getSelectedColumn();
           
	           MRSUser              = new MRSAuthUser(iRow);
	           MRSUser              . setVisible(true);
      	     setUserIntoTable();
           
	     }
	 catch(Exception e)
	  {
          e.printStackTrace();
          System.out.println("Inside KeyListener Press F2:"+e);
       }
   }
  }


     }
     public class KeyList5 extends KeyAdapter
     {
           public void keyPressed(KeyEvent ke)
           {
                if (ke.getKeyCode() == KeyEvent.VK_INSERT)
                {
                    showInwModiFrame(5);
                }
                if(iMillCode==1)
                {
                     if (ke.getKeyCode() == KeyEvent.VK_HOME)
                     {
                         showCodeSelectionFrame(5);
                     }
                }
           } 

    public void keyReleased(KeyEvent ke){
    
	  if(ke.getKeyCode()==KeyEvent.VK_F2)
	    {
	   try{
      	     int iRow             = tabreport5.ReportTable.getSelectedRow();
//               int iColumn          = theTable.getSelectedColumn();
           
	           MRSUser              = new MRSAuthUser(iRow);
	           MRSUser              . setVisible(true);
      	     setFreeUserIntoTable();
           
	     }
	 catch(Exception e)
	  {
          e.printStackTrace();
          System.out.println("Inside KeyListener Press F2:"+e);
       }
     }
   }
 }

     public class KeyList8 extends KeyAdapter
     {
           public void keyPressed(KeyEvent ke)
           {
                if (ke.getKeyCode() == KeyEvent.VK_INSERT)
                {
                    //showRDCFrame();
                }
           } 
     }

     public class KeyList16 extends KeyAdapter
     {
           public void keyPressed(KeyEvent ke)
           {
                if (ke.getKeyCode() == KeyEvent.VK_INSERT)
                {
                    showCanteenFrame();
                }
           } 
     }

     private void setVectors()
     {
          VInwName   = new Vector();
          VInwNo     = new Vector();
          VSupplier  = new Vector();
          VSupCode   = new Vector();
          VDeptCode  = new Vector();
          VDeptName  = new Vector();

          String QS1 = "Select InwName,InwNo From InwType Order By 2";
          String QS2 = "Select Name,Ac_Code From "+SSupTable+" Order By 1";
          String QS3 = "Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();

              ResultSet result1 = stat.executeQuery(QS1);
              while(result1.next())
              {
                    VInwName.addElement(result1.getString(1));
                    VInwNo.addElement(result1.getString(2));
              }
              result1.close();

              ResultSet result2 = stat.executeQuery(QS2);
              while(result2.next())
              {
                    VSupplier.addElement(result2.getString(1));
                    VSupCode.addElement(result2.getString(2));
              }
              result2.close();

              ResultSet result3 = stat.executeQuery(QS3);
              while(result3.next())
              {
                    VDeptName.addElement(result3.getString(1));
                    VDeptCode.addElement(result3.getString(2));
              }
              result3.close();
              stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void showInwModiFrame(int iSignal)
     {
          try
          {
               DeskTop.remove(imframe);
               DeskTop.updateUI();
          }
          catch(Exception ex){}

          try
          {
               Frame dummy = new Frame();
               JDialog theDialog = new JDialog(dummy,"Description Dialog",true);
               if(iSignal==3)
                    imframe = new InwModiFrame(DeskTop,VSupCode,VSupplier,VInwNo,VInwName,VSupCode3,VInwNo3,tabreport3,theDialog,iMillCode,VIssStatus3,"Others",SSupTable);
               if(iSignal==5)
                    imframe = new InwModiFrame(DeskTop,VSupCode,VSupplier,VInwNo,VInwName,VSupCode5,VInwNo5,tabreport5,theDialog,iMillCode,VIssStatus5,"Others",SSupTable);

               theDialog.getContentPane().add(imframe.ModiPanel);
               theDialog.setBounds(80,100,450,350);
               theDialog.setVisible(true);

          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void showCanteenFrame()
     {
          try
          {
               DeskTop.remove(canteenframe);
               DeskTop.updateUI();
          }
          catch(Exception ex){}

          try
          {
               String SMatName = ""+tabreport16.ReportTable.getValueAt(tabreport16.ReportTable.getSelectedRow(),3);
			   
			   Frame dummy = new Frame();
			   JDialog theDialog = new JDialog(dummy,"Description Dialog",true);
                  canteenframe = new CanteenFrame(DeskTop,VInwNo,VInwName,tabreport16,VInwNo16,VDesc16,VQty16,VSupName16,theDialog,iMillCode);
			   if((SMatName.equals("")) || (SMatName.equals("null")) || (SMatName.equals(" ")))
                    canteenframe.TMaterial.setText(" ");
			   else
                    canteenframe.TMaterial.setText(SMatName);  
                  theDialog.getContentPane().add(canteenframe.CashPanel);
			   theDialog.setBounds(80,100,450,350);
			   theDialog.setVisible(true);
          }
          catch(Exception ex){}
     }

     /*private void showRDCFrame()
     {
          try
          {
               DeskTop.remove(girdclinkframe);
               DeskTop.updateUI();
          }
          catch(Exception ex){}

          try
          {
               String SMatName = ""+tabreport8.ReportTable.getValueAt(tabreport8.ReportTable.getSelectedRow(),3);
			   
			   Frame dummy = new Frame();
			   JDialog theDialog = new JDialog(dummy,"Description Dialog",true);
      girdclinkframe = new GIRDCLinkFrame(DeskTop,VInwNo,VInwName,tabreport8,VInwNo8,VDesc8,VMatName8,VQty8,VSup_Name8,theDialog,iMillCode);
			   if((SMatName.equals("")) || (SMatName.equals("null")) || (SMatName.equals(" ")))
      girdclinkframe.BMaterial.setText("Material");
			   else
      girdclinkframe.BMaterial.setText(SMatName);  
      theDialog.getContentPane().add(girdclinkframe.CashPanel);
			   theDialog.setBounds(80,100,450,350);
			   theDialog.setVisible(true);
          }
          catch(Exception ex){}
     }*/

     private void showCodeSelectionFrame(int iSignal)
     {
          try
          {
               DeskTop.remove(codeselectionframe);
               DeskTop.updateUI();
          }
          catch(Exception ex){}

          try
          {

                  String SMatName = "";
                  Frame dummy = new Frame();
                  JDialog theDialog = new JDialog(dummy,"Description Dialog",true);

                  if(iSignal==3)
                  {
                       SMatName = ""+tabreport3.ReportTable.getValueAt(tabreport3.ReportTable.getSelectedRow(),5);
                       codeselectionframe = new CodeSelectionFrame(DeskTop,VInwNo,VInwName,tabreport3,VCode,VName,VNameCode,VInwNo3,VDesc3,VCode3,VQty3,VSupName3,VId3,VIssStatus3,theDialog,iMillCode,SItemTable);
                  }
                  if(iSignal==5)
                  {
                       SMatName = ""+tabreport5.ReportTable.getValueAt(tabreport5.ReportTable.getSelectedRow(),5);
                       codeselectionframe = new CodeSelectionFrame(DeskTop,VInwNo,VInwName,tabreport5,VCode,VName,VNameCode,VInwNo5,VDesc5,VCode5,VQty5,VSupName5,VId5,VIssStatus5,theDialog,iMillCode,SItemTable);
                  }

                  if((SMatName.equals("")) || (SMatName.equals("null")) || (SMatName.equals(" ")))
                      codeselectionframe.BMaterial.setText("Material");
			   else
                      codeselectionframe.BMaterial.setText(SMatName);  

                  theDialog.getContentPane().add(codeselectionframe.CashPanel);
			   theDialog.setBounds(80,100,450,350);
			   theDialog.setVisible(true);
          }
          catch(Exception ex){}
     }


public void setUserIntoTable()
     {
          try
          {

	        int index     = tabreport3.ReportTable.getSelectedRow();
              sUser	    = MRSUser.sUserName;
		  iUserCode	    = MRSUser.getUserCode();
              int iTableRow = MRSUser.iRow;

              tabreport3.ReportTable.getModel().setValueAt(sUser,iTableRow,8);
              tabreport3.ReportTable.setValueAt(new Boolean(true),index,10);

              //reason.setVisible(false);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               JOptionPane.showMessageDialog(null,ex,"UserCode",JOptionPane.ERROR_MESSAGE);
          }
     }


public void setFreeUserIntoTable()
     {
          try
          {

	        int index     = tabreport5.ReportTable.getSelectedRow();
              sUser	    = MRSUser.sUserName;
		  iUserCode	    = MRSUser.getUserCode();
              int iTableRow = MRSUser.iRow;

              tabreport5.ReportTable.getModel().setValueAt(sUser,iTableRow,8);
              tabreport5.ReportTable.setValueAt(new Boolean(true),index,10);

              //reason.setVisible(false);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               JOptionPane.showMessageDialog(null,ex,"UserCode",JOptionPane.ERROR_MESSAGE);
          }
     }

private class KeyList extends KeyAdapter{
   public KeyList(){
        
    }
public void keyReleased(KeyEvent ke){
    
    if(ke.getKeyCode()==KeyEvent.VK_F2)
    {
   try{
           int iRow             = tabreport3.ReportTable.getSelectedRow();
           
           MRSUser              = new MRSAuthUser(iRow);
           MRSUser              . setVisible(true);
           setUserIntoTable();
           
     }
   catch(Exception e)
     {
          e.printStackTrace();
          System.out.println("Inside KeyListener GRN- Trial Press F2 Listener:"+e);
     }
   }
  }
}

public int getTrialPendingCount(){
int		iCount	=-1;
 try{
        StringBuffer sb     =new StringBuffer(); 

        sb.append("  Select Sum(Cnt) from (");
        sb.append("  Select Count(*) as Cnt from GateInward");
	  sb.append("  Left Join RawUser on GateInward.MRSAuthUserCode = RawUser.UserCode  ");
	  sb.append("   Where GateInward.InwNo= 1 and GateInward.IssueStatus = 0  And GateInward.GrnNo = 0"); 
	  sb.append("   And (trim(GateInward.Item_Code) is Null)  And GateInward.MillCode = 0 And GateInward.ModiStatus = 0");
	  sb.append("   And GateInward.Authentication = 1  and GateInward.TrialOrderAuthStatus=0  and UserName is null");
  
	  sb.append("    Union All  ");
  
	  sb.append("   Select Count(*) as Cnt from GateInward");
	  sb.append("   Left Join RawUser on GateInward.MRSAuthUserCode = RawUser.UserCode  ");
	  sb.append("   Where GateInward.InwNo = 1 and GateInward.IssueStatus = 0  And GateInward.GrnNo = 0 ");
	  sb.append("   And (trim(GateInward.Item_Name) is Null)  And GateInward.MillCode = 0 And GateInward.ModiStatus = 0 ");
	  sb.append("   And GateInward.Authentication = 1 and GateInward.TrialOrderAuthStatus=0 and UserName is null");
        sb.append(")");
         

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
                 
        PreparedStatement ps          = theConnection.prepareStatement(sb.toString());
	//System.out.println("Get Trial Count:"+sb.toString());

        ResultSet rst                 = ps.executeQuery();
       while (rst.next())
         {
		iCount			  = rst.getInt(1);
         }
       
       
            rst.close();
            ps.close();
            ps=null;
            
    }
    catch(Exception e){
        e.printStackTrace();
        System.out.println(e);
    }
System.out.println("Inside get TrialCount Method :"+iCount);
return iCount;
}



}
