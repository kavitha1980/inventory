
package GRN;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.JOptionPane;
import util.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javax.swing.JTable;
import java.io.*;

public class PDFCreation {
    String SFile="",SAdvanceMonth;
    //ArrayList theWageList = null,theProdList = null;
    Common common = new Common();
    FileWriter                      FW;   

     int            Lctr      = 100;
     int            Pctr      = 0;
     String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
     int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font tinyBold   = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
     private static Font tinyNormal = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

     private static Font underBold  = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);

    // String SHead[]  = {"SL.NO","ORDER NO","ORDER DATE","SUPPLIER NAME","DC NO","DC DATE","INV NO","INV DATE","GRN NO","GRN DATE","GRN QTY","NOOF REMAINDER"};
    // int    iWidth[] = {8,10,12,30,10,12,10,12,10,12,8,15};


	String SHead[]  = {"ORDER NO","ORDER DATE","SUPPLIER NAME","DC NO","DC DATE","INV NO","INV DATE","GRN NO","GRN DATE","GRN QTY","NOOF REMAINDER"};
      int   iWidth[]  = {10,12,30,10,12,10,12,10,12,8,15};


     Document document;
     PdfPTable table;
     Connection theConnection = null;
	String 	sFromDate,sToDate;
	ArrayList	AInvNotReceivedList ;
	int 		iStatus=-1;

     
     public PDFCreation(String SFile,String sFromDate,String sToDate,ArrayList AInvNotReceivedList,int iStatus)
     {
          this.SFile            = SFile;
	    this.sFromDate	  = sFromDate;
	    this.sToDate		  = sToDate;
	    this.AInvNotReceivedList = AInvNotReceivedList;		
	    this.iStatus		  = iStatus;
	
          createPDFFile();
          
     }

     public PDFCreation(FileWriter FW,String sFromDate,String sToDate,ArrayList AInvNotReceivedList)
     {
          this.FW               = FW;
	    this.sFromDate	  = sFromDate;
	    this.sToDate		  = sToDate;
	    this.AInvNotReceivedList = AInvNotReceivedList;		
	
          createPDFFile();
          
     }
    
   private void createPDFFile()
   {
          try
          {
               document = new Document(PageSize.A4);
               PdfWriter.getInstance(document, new FileOutputStream(SFile));
               document.open();

//               table = new PdfPTable(12);
               table = new PdfPTable(11);
               table.setWidths(iWidth);
               table.setWidthPercentage(100);

               addHead(document,table);
               addBody(document,table);
               addFoot(document,table,0);

               document.close();
               JOptionPane.showMessageDialog(null, "PDF File Created in "+SFile,"Info",JOptionPane.INFORMATION_MESSAGE);
          }
          catch (Exception e)
          {
               e.printStackTrace();
          }
   }
   
 private void addHead(Document document,PdfPTable table) throws BadElementException
   {
       try
          {
               if(Lctr < 42)
                    return;

               if(Pctr > 0)
                    addFoot(document,table,1);

               Pctr++;

               document.newPage();
             
               Paragraph paragraph;
               String Str1   = "";
               String Str2   = "";


		 if(iStatus==0){

                Str1   = "Amarjothi Spinning Mills Limited";
                Str2   = "Invoice NotReceived List Upto"+common.parseDate(sFromDate);

		  }
		  else if(iStatus==1){	
		    String sServerDate           = common.getServerDate();
		    String sDate			   = sServerDate.substring(0,8);
		    	
                Str1   = "Amarjothi Spinning Mills Limited";
                Str2   = "Invoice NotReceived List  - As on Date :"+common.parseDate(sDate);
              }
               

               paragraph = new Paragraph(Str1,bigBold);
               paragraph.setAlignment(Element.ALIGN_CENTER);
               paragraph.setSpacingAfter(10);
               document.add(paragraph);

               paragraph = new Paragraph(Str2,mediumBold);
               paragraph.setAlignment(Element.ALIGN_CENTER);
               paragraph.setSpacingAfter(20);
               document.add(paragraph);
              
               Lctr = 16;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

   }
   private void addBody(Document document,PdfPTable table) throws BadElementException

   {
       try
       {
       table.setSpacingAfter(30);
       table.setSpacingAfter(50);
       

       table.flushContent();
//       table = new PdfPTable(12);
       table = new PdfPTable(11);
       table.setWidths(iWidth);
       table.setWidthPercentage(100);
       table.setSpacingAfter(50);
       
       addInvNotReceivedList(document,table);
       
       /*table.flushContent();
       table = new PdfPTable(7);
       table.setWidths(iWidth1);
       table.setWidthPercentage(100);
       table.setSpacingAfter(50);*/
       

       }
       catch(Exception ex)
       {
           System.out.println(" set Others Wage "+ex);
           ex.printStackTrace();
       }
   }
   
 private void addInvNotReceivedList(Document document,PdfPTable table)
   {
       try
       {
          Paragraph paragraph;
          PdfPCell c1;
          int iNo=0;
           paragraph = new Paragraph("INVOICE NOTRECEIVED LIST",mediumBold);
           paragraph.setAlignment(Element.ALIGN_LEFT);
           paragraph.setSpacingAfter(10);
           document.add(paragraph);


            for(int i=0;i<SHead.length;i++)
               {
                    c1 = new PdfPCell(new Phrase(SHead[i],smallBold));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                    table.addCell(c1);
               }
	System.out.println("Size:"+AInvNotReceivedList.size());
     for(int i=0;i<AInvNotReceivedList.size();i++){
				iNo	= i+1;
         Supplier           supplier        = (Supplier)AInvNotReceivedList.get(i);
           for(int j=0;j<supplier.AInvNotReceivedList.size();j++){
               HashMap      theMap          = (HashMap)supplier.AInvNotReceivedList.get(j);


                  /*       c1 = new PdfPCell(new Phrase(String.valueOf(iNo),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                         table.addCell(c1);*/

                         c1 = new PdfPCell(new Phrase(String.valueOf(theMap.get("OrderNo")),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                         table.addCell(c1);
                         
                         c1 = new PdfPCell(new Phrase(common.parseDate(String.valueOf(theMap.get("OrderDate"))),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(supplier.sSupName,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(theMap.get("DCNo")),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.parseDate(String.valueOf(theMap.get("DCDate"))),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(theMap.get("InvNo")),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.parseDate(String.valueOf(theMap.get("InvDate"))),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(theMap.get("GRNNo")),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.parseDate(String.valueOf(theMap.get("GRNDate"))),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf(theMap.get("GRNQty")),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.parseDate(String.valueOf(theMap.get("NoofRemainder"))),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                         table.addCell(c1);

				}
			}
                         
                         document.add(table);

       }
       catch(Exception ex)
       {
           
       }
   }


private void addFoot(Document document,PdfPTable table,int iSig)throws BadElementException
   {
       try
       {
         try
       {
           Paragraph paragraph;

	     String sServerDate          =  common.getServerDate();
	     String sDate			   =  sServerDate.substring(0,8);
           String sTime			   =  sServerDate.substring(9,17);

	    
           String Str  = "Report Taken On:"+common.parseDate(sDate)+"   Time:"+sTime;

           paragraph = new Paragraph(Str,bigBold);
           paragraph.setAlignment(Element.ALIGN_LEFT);
           paragraph.setSpacingBefore(10);
           paragraph.setSpacingAfter(10);
           document.add(paragraph);


       }
       catch(Exception ex)
       {
          ex.printStackTrace();
       }

       }
       catch(Exception ex)
       {

       }
       
   }
     private void addEmptyCell(PdfPTable table)
     {
          PdfPCell c1 = new PdfPCell();
          table.addCell(c1);
     }

     private void addEmptyCell(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
          table.addCell(c1);
     }

     private void addEmptyRow(PdfPTable table)
     {
          for(int i=0;i<SHead.length;i++)
          {
               PdfPCell c1 = new PdfPCell();
               table.addCell(c1);
          }
     }

     private void addEmptySpanRow(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
          c1.setColspan(SHead.length);
          table.addCell(c1);
     }

     private static void addEmptyLine(Paragraph paragraph, int number)
     {
          for (int i = 0; i < number; i++)
          {
               paragraph.add(new Paragraph(" "));
          }
     }
    private void AddCellTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	 private void AddCellTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,int iRowSpan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          //c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  c1.addElement(new Chunk(image, 5, -5));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
	 private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  //c1.addElement(new Chunk(image, 5, -5));
		  c1.addElement(image);
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
     private void AddCellTable(double dValue,int iRound,int iRad,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
           Double DValue = new Double(dValue);
           String Str;
           if (DValue.isNaN() || dValue ==0)
           {
               Str="";
           }
           else
           {
               Str=(common.Rad(common.getRound(dValue,iRound),iRad));
           }

          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     

}

     

