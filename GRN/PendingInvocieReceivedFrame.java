package GRN;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.border.*;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Vector;
import util.*;
import java.util.ArrayList;
import java.io.*;
import java.sql.*;
import java.net.*;
import oracle.jdbc.*;
import oracle.sql.*;
import jdbc.*;


public class PendingInvocieReceivedFrame extends JInternalFrame {
    JPanel                          pnlMain, pnlFilter, pnlData, pnlControl;
    JComboBox                       cmbSupplierList;
    JButton                         btnApply, btnExit;
    PendingInvoiceReceivedModel     theModel;
    JTable                          theTable;
    Connection                      theMConnection = null;
    Connection                      theDConnection = null;
    Common              common      = new Common();
    JCheckBox                       chkAson;
    AttDateField                    txtStDate,txtEnDate;
    int                             iMillCode                   = 1;
    Vector                          VSupName, VSupCode;
    PDFCreation                     pdfcreation;
    ArrayList                       APendingInvReceivedList;
    Supplier                        supplier;
    String                          sSupAdd1, sSupAdd2, sSupAdd3,sSupName;
    String                          sAuthSysName;
    int                             iUserCode;
    JLayeredPane				Layer;	
    
    int            iName            = 0;
    int            iOrderNo         = 1;
    int            iOrderDate       = 2;
    int            iGRNNo           = 3;
    int            iGRNDate         = 4;
    int            iReceivedDate    = 5;
    
    
  public  PendingInvocieReceivedFrame(JLayeredPane Layer){
	    this.Layer		= Layer; 
          createComponent();
          setLayout();
          addComponent();
          addListener();
      try{
          sAuthSysName=InetAddress.getLocalHost().getHostName();
       }
     catch(Exception e){
          e.printStackTrace();
       }     
  }
  
  private void createComponent(){
      
    pnlMain                         = new JPanel();
    pnlFilter                       = new JPanel();
    pnlData                         = new JPanel();
    pnlControl                      = new JPanel();
    
    btnApply                        = new JButton("Apply");
    btnExit                         = new JButton("Exit");
    
    theModel                        = new PendingInvoiceReceivedModel();
    theTable                        = new JTable(theModel);
    txtStDate                       = new AttDateField();
    txtEnDate                       = new AttDateField();
    chkAson                         = new JCheckBox();
    chkAson.setSelected(true);
    
}   

private void setLayout(){
    pnlMain                            . setLayout(new BorderLayout());
    pnlFilter                          . setBorder(new TitledBorder("Filter"));
    pnlFilter                          . setLayout(new GridLayout(1,4));
    pnlControl                         . setBorder(new TitledBorder("Control"));
    pnlControl                         . setLayout(new FlowLayout());
    
    pnlData                            . setBorder(new TitledBorder("Pending Invoice Received Data"));
    pnlData                            . setLayout(new BorderLayout());
    
    this.setSize(1000,600);
    this.setVisible(true);
    this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    //this.setMaximizable(true);
    this.setClosable(true);
    this.setTitle("PENDING INVOICE RECEIVED FRAME");
}
private void addListener(){
    btnApply                           . addActionListener(new ActList());
    btnExit                            . addActionListener(new ExtList());
  }

private void addComponent(){
    pnlFilter                      . add(new JLabel("FromDate"));
    pnlFilter                      . add(txtStDate);
    pnlFilter                      . add(new JLabel("ToDate"));
    pnlFilter                      . add(txtEnDate);
    pnlFilter                      . add(btnApply);
    pnlData                        . add(new JScrollPane(theTable));
    pnlControl                     . add(btnExit);
    
    pnlMain                        . add(pnlFilter,BorderLayout.NORTH);
    pnlMain                        . add(pnlData,BorderLayout.CENTER);
    pnlMain                        . add(pnlControl,BorderLayout.SOUTH);
    
    this.add(pnlMain);
    
}

private void setTableData(){
     theModel.setNumRows(0);
     String sFromDate           = txtStDate.toNormal();
     String sToDate             = txtEnDate.toNormal();
     setPendingInvoiceReceivedData(sFromDate , sToDate);
     for(int i=0;i<APendingInvReceivedList.size();i++){
         Supplier           supplier        = (Supplier)APendingInvReceivedList.get(i);
           for(int j=0;j<supplier.APendingInvReceivedList.size();j++){
               HashMap      theMap          = (HashMap)supplier.APendingInvReceivedList.get(j);
               Vector           theVect     = new Vector();
               
               theVect.addElement(common.parseNull(supplier.sSupName));
               theVect.addElement(common.parseNull(String.valueOf(theMap.get("OrderNo"))));
               theVect.addElement(common.parseNull(common.parseDate(String.valueOf(theMap.get("OrderDate")))));
               theVect.addElement(common.parseNull(String.valueOf(theMap.get("GRNNo"))));
               theVect.addElement(common.parseNull(common.parseDate(String.valueOf(theMap.get("GRNDate")))));
               theVect.addElement(common.parseNull(String.valueOf(theMap.get("InvoiceReceivedDate"))));
               theModel.appendRow(theVect);
            
           }
        }
    }

public void setPendingInvoiceReceivedData(String sFromDate, String sToDate)
{
     APendingInvReceivedList      = new ArrayList();
     
     StringBuffer            sb  = new StringBuffer();
     sb.append(" Select GRN.Sup_Code,Supplier.Name,PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,GRN.GRNNo,GRN.GRNDate,");
     sb.append(" InvoiceReceivedDateTime from GRN");
     sb.append(" Inner Join Supplier on Supplier.AC_Code=GRN.Sup_Code ");
     sb.append(" Inner Join PurchaseOrder on PurchaseOrder.OrderNo=GRN.OrderNo and GRN.Code=PurchaseOrder.Item_Code ");
     sb.append(" Where to_Char(to_Date(InvoiceReceivedDateTime,'DD-MON-YYYY:hh:mi:ss'),'YYYYMMDD') >="+sFromDate);
     sb.append(" and to_Char(to_Date(InvoiceReceivedDateTime,'DD-MON-YYYY:hh:mi:ss'),'YYYYMMDD')<="+sToDate);
     sb.append(" and GRN.MillCode=0");
     sb.append(" Group by GRN.Sup_Code,Supplier.Name,PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,GRN.GRNNo,GRN.GRNDate,");
     sb.append(" InvoiceReceivedDateTime ");
     sb.append(" Order by 7");
    
    // System.out.println("Pending Inv Received Qry:"+sb.toString()); 
               try
               {
                 if(theMConnection ==null){
                         ORAConnection jdbc     = ORAConnection.getORAConnection();
                         theMConnection          = jdbc.getConnection();
                   }
                PreparedStatement  theStatement = theMConnection.prepareStatement(sb.toString());
                ResultSet rst                   = theStatement.executeQuery();
                while(rst.next())
                 {
                    String  sSupplierCode       = rst.getString(1)  ;
                    String  sSupplierName       = rst.getString(2)  ;
                    String  sOrderNo            = rst.getString(3)  ;
                    String  sOrderDate          = rst.getString(4)  ;
                    String  sGRNNo              = rst.getString(5)  ;
                    String  sGRNDate            = rst.getString(6)  ;
                    String  sInvReceivedDate    = rst.getString(7)  ;
                    
              int iIndex                        = getIndexOf(sSupplierCode,sSupplierName);
              if(iIndex==-1)
              {
                supplier                        = new Supplier(sSupplierCode,sSupplierName);
                APendingInvReceivedList         . add(supplier);
                iIndex= APendingInvReceivedList . size()-1;
              }
              supplier       = (Supplier)APendingInvReceivedList.get(iIndex);
              supplier       . appendInvDetails(sOrderNo,sOrderDate,sGRNNo,sGRNDate,sInvReceivedDate);
             
              }
                    rst            . close();
                    theStatement   . close();
               }
          catch(Exception ex)
          {
                    System.out.println(ex);
                    ex.printStackTrace();
          }
      }

private int getIndexOf(String sSupplierCode, String sSupplierName)
 {
       int iIndex=-1;
        for(int i=0; i<APendingInvReceivedList.size(); i++)
        {    
             Supplier  supplier  = (Supplier)APendingInvReceivedList.get(i);
             if(supplier.sSupName.equals(sSupplierName) && supplier.sSupCode.equals(sSupplierCode)) 
             {
                iIndex = i;
                break;
            }
        }
   return iIndex;    
 }


private class ActList implements ActionListener{
     public void actionPerformed(ActionEvent ae){
      if (ae.getSource()==btnApply){
             setTableData();
         }
     }
}

private class ExtList implements ActionListener{
     public void actionPerformed(ActionEvent ae){
      if (ae.getSource()==btnExit){
             dispose();
         }
     }
}
}
