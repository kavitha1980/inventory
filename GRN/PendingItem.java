package GRN;

import java.io.*;
import util.*;
import java.util.*;


public class PendingItem
{
     String SGCode;

     String SGName,SGBlock,SGOrderNo,SGMRSNo,SGPendQty,SGGrnQty;
     String SGRate,SGDiscPer,SGVatPer,SGSTPer,SGSurPer,SGDept,SGDeptCode;
     String SGGroup,SGGroupCode,SGUnit,SGUnitCode,SGBlockCode,SId;
     String SGOrdQty,SGOrdDate,SMrsSlNo,SMrsUserCode,SOrderSlNo,SOrderApproval,STaxC;

     Vector VGCode,VGName,VGBlock,VGOrderNo,VGMRSNo;
     Vector VGPendQty,VGGrnQty,VGRate,VGDiscPer,VGVatPer,VGSTPer,VGSurPer;
     Vector VGDept,VGDeptCode,VGGroup,VGGroupCode,VGUnit,VGUnitCode;
     Vector VGBlockCode,VId,VGOrdQty,VGOrdDate;
     Vector VMrsSlNo,VMrsUserCode,VOrderSlNo,VApproval,VTaxC;

     Common common = new Common();

     public PendingItem(String SGCode)
     {                   
          this.SGCode = SGCode;

          VGCode       = new Vector();
          VGName       = new Vector(); 
          VGBlock      = new Vector();
          VGOrderNo    = new Vector();
          VGMRSNo      = new Vector();
          VGPendQty    = new Vector();
          VGGrnQty     = new Vector();
          VGRate       = new Vector();
          VGDiscPer    = new Vector();
          VGVatPer     = new Vector();
          VGSTPer      = new Vector();
          VGSurPer     = new Vector();
          VGDept       = new Vector();
          VGDeptCode   = new Vector(); 
          VGGroup      = new Vector();
          VGGroupCode  = new Vector(); 
          VGUnit       = new Vector();
          VGUnitCode   = new Vector();
          VGBlockCode  = new Vector();
          VGOrdQty     = new Vector();
          VGOrdDate    = new Vector();
          VId          = new Vector();
          VMrsSlNo     = new Vector();
          VMrsUserCode = new Vector();
          VOrderSlNo   = new Vector();
          VApproval    = new Vector();
          VTaxC        = new Vector();

     }
     public void setPendingData(String SGName,String SGBlock,String SGOrderNo,String SGMRSNo,String SGPendQty,String SGGrnQty,String SGRate,String SGDiscPer,String SGVatPer,String SGSTPer,String SGSurPer,String SGDept,String SGDeptCode,String SGGroup,String SGGroupCode,String SGUnit,String SGUnitCode,String SGBlockCode,String SId,String SGOrdQty,String SGOrdDate,String SMrsSlNo,String SMrsUserCode,String SOrderSlNo,String SOrderApproval,String STaxC)
     {
           VGCode      .addElement(SGCode);
           VGName      .addElement(SGName); 
           VGBlock     .addElement(SGBlock);
           VGOrderNo   .addElement(SGOrderNo);
           VGMRSNo     .addElement(SGMRSNo);
           VGPendQty   .addElement(SGPendQty);
           VGGrnQty    .addElement(SGGrnQty);
           VGRate      .addElement(SGRate);
           VGDiscPer   .addElement(SGDiscPer);
           VGVatPer    .addElement(SGVatPer);
           VGSTPer     .addElement(SGSTPer);
           VGSurPer    .addElement(SGSurPer);
           VGDept      .addElement(SGDept);
           VGDeptCode  .addElement(SGDeptCode); 
           VGGroup     .addElement(SGGroup);
           VGGroupCode .addElement(SGGroupCode); 
           VGUnit      .addElement(SGUnit);
           VGUnitCode  .addElement(SGUnitCode);
           VId         .addElement(SId);
           VGBlockCode .addElement(SGBlockCode);
           VGOrdQty    .addElement(SGOrdQty);
           VGOrdDate   .addElement(SGOrdDate);
           VMrsSlNo    .addElement(SMrsSlNo);
           VMrsUserCode.addElement(SMrsUserCode);
           VOrderSlNo  .addElement(SOrderSlNo);
           VApproval   .addElement(SOrderApproval);
           VTaxC       .addElement(STaxC);
     }

}


