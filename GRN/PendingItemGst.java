package GRN;

import java.io.*;
import util.*;
import java.util.*;


public class PendingItemGst
{
     String SGCode;

     String SGName,SGBlock,SGOrderNo,SGMRSNo,SGPendQty,SGGrnQty;
     String SGRate,SGDiscPer,SGCGSTPer,SGSGSTPer,SGIGSTPer,SGCessPer,SGDept,SGDeptCode;
     String SGGroup,SGGroupCode,SGUnit,SGUnitCode,SGBlockCode,SId;
     String SGOrdQty,SGOrdDate,SMrsSlNo,SMrsUserCode,SOrderSlNo,SOrderApproval,STaxC;
     String SHsnCode,SHsnType,SGrnStatus;

     Vector VGCode,VGName,VGBlock,VGOrderNo,VGMRSNo;
     Vector VGPendQty,VGGrnQty,VGRate,VGDiscPer,VGCGSTPer,VGSGSTPer,VGIGSTPer,VGCessPer;
     Vector VGDept,VGDeptCode,VGGroup,VGGroupCode,VGUnit,VGUnitCode;
     Vector VGBlockCode,VId,VGOrdQty,VGOrdDate;
     Vector VMrsSlNo,VMrsUserCode,VOrderSlNo,VApproval,VTaxC;
     Vector VGHsnCode,VGHsnType,VGGrnStatus,VGRateStatus,VGGstRate;
     Vector VGNewHsnCode,VGNewGstRate,VGNewCGSTPer,VGNewSGSTPer,VGNewIGSTPer,VGNewCessPer;

     Common common = new Common();

     public PendingItemGst(String SGCode)
     {                   
          this.SGCode = SGCode;

          VGCode       = new Vector();
          VGName       = new Vector(); 
          VGBlock      = new Vector();
          VGOrderNo    = new Vector();
          VGMRSNo      = new Vector();
          VGPendQty    = new Vector();
          VGGrnQty     = new Vector();
          VGRate       = new Vector();
          VGDiscPer    = new Vector();
          VGCGSTPer    = new Vector();
          VGSGSTPer    = new Vector();
          VGIGSTPer    = new Vector();
          VGCessPer    = new Vector();
          VGDept       = new Vector();
          VGDeptCode   = new Vector(); 
          VGGroup      = new Vector();
          VGGroupCode  = new Vector(); 
          VGUnit       = new Vector();
          VGUnitCode   = new Vector();
          VGBlockCode  = new Vector();
          VGOrdQty     = new Vector();
          VGOrdDate    = new Vector();
          VId          = new Vector();
          VMrsSlNo     = new Vector();
          VMrsUserCode = new Vector();
          VOrderSlNo   = new Vector();
          VApproval    = new Vector();
          VTaxC        = new Vector();
	  VGHsnCode    = new Vector();
	  VGHsnType    = new Vector();
	  VGGrnStatus  = new Vector();
	  VGRateStatus = new Vector();
          VGGstRate    = new Vector();
	  VGNewHsnCode = new Vector();
	  VGNewGstRate = new Vector();
	  VGNewCGSTPer = new Vector();
	  VGNewSGSTPer = new Vector();
	  VGNewIGSTPer = new Vector();
	  VGNewCessPer = new Vector();
     }

     public void setPendingData(String SGName,String SGBlock,String SGOrderNo,String SGMRSNo,String SGPendQty,String SGGrnQty,String SGRate,String SGDiscPer,String SGCGSTPer,String SGSGSTPer,String SGIGSTPer,String SGCessPer,String SGDept,String SGDeptCode,String SGGroup,String SGGroupCode,String SGUnit,String SGUnitCode,String SGBlockCode,String SId,String SGOrdQty,String SGOrdDate,String SMrsSlNo,String SMrsUserCode,String SOrderSlNo,String SOrderApproval,String STaxC,String SHsnCode,String SHsnType,String SGrnStatus,String SGstRate)
     {
           VGCode      .addElement(SGCode);
           VGName      .addElement(SGName); 
           VGBlock     .addElement(SGBlock);
           VGOrderNo   .addElement(SGOrderNo);
           VGMRSNo     .addElement(SGMRSNo);
           VGPendQty   .addElement(SGPendQty);
           VGGrnQty    .addElement(SGGrnQty);
           VGRate      .addElement(SGRate);
           VGDiscPer   .addElement(SGDiscPer);
           VGCGSTPer   .addElement(SGCGSTPer);
           VGSGSTPer   .addElement(SGSGSTPer);
           VGIGSTPer   .addElement(SGIGSTPer);
           VGCessPer   .addElement(SGCessPer);
           VGDept      .addElement(SGDept);
           VGDeptCode  .addElement(SGDeptCode); 
           VGGroup     .addElement(SGGroup);
           VGGroupCode .addElement(SGGroupCode); 
           VGUnit      .addElement(SGUnit);
           VGUnitCode  .addElement(SGUnitCode);
           VId         .addElement(SId);
           VGBlockCode .addElement(SGBlockCode);
           VGOrdQty    .addElement(SGOrdQty);
           VGOrdDate   .addElement(SGOrdDate);
           VMrsSlNo    .addElement(SMrsSlNo);
           VMrsUserCode.addElement(SMrsUserCode);
           VOrderSlNo  .addElement(SOrderSlNo);
           VApproval   .addElement(SOrderApproval);
           VTaxC       .addElement(STaxC);
	   VGHsnCode   .addElement(SHsnCode);
	   VGHsnType   .addElement(SHsnType);
	   VGGrnStatus .addElement(SGrnStatus);
           VGRateStatus.addElement("0");
	   VGGstRate   .addElement(SGstRate);
	   VGNewHsnCode.addElement(SHsnCode);
	   VGNewGstRate.addElement(SGstRate);
           VGNewCGSTPer.addElement(SGCGSTPer);
           VGNewSGSTPer.addElement(SGSGSTPer);
           VGNewIGSTPer.addElement(SGIGSTPer);
           VGNewCessPer.addElement(SGCessPer);
     }

}


