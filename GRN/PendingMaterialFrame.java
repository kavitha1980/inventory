package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class PendingMaterialFrame extends JInternalFrame
{
     
     JLayeredPane   Layer;
     TabReport      tabreport;
     TabReport      tabreport1;

     JButton        BOk;
     JLabel         LMaterial,LMatCode;
     int            iMillCode,iRow;
     String         SSupCode,SSupName,SDesc,SQty;
     String         SGINo,SGIDate,SInvQty;
     NewDirectGRNGatePanel GatePanel;
     Vector         VPendCode,VPendName,theVector;
     Vector VPOrderNo,VPOrderBlock,VPMrsNo,VPOrderCode,VPOrderName,VPOrderQty,VPQtyAllow;

     JPanel         TopPanel,MiddlePanel,BottomPanel;
     JTextField     TSupplier,TMaterial;
     NextField      TQty;

     Common common = new Common();

     Object RowData[][];
     String ColumnData[] = {"Order No","Order Date","Block","Mrs No","Order Qty","Pending Qty","Recd Qty","Click for Select"};
     String ColumnType[] = {"N"       ,"S"         ,"S"    ,"N"     ,"N"        ,"N"          ,"E"       ,"B"};

     Object RowData1[][];
     String ColumnData1[] = {"Code","Item Name","OrderNo","MrsNo","Block","OrderQty"};
     String ColumnType1[] = {"S"   ,"S"        ,"S"      ,"S"    ,"S"    ,"S"       };

     int iQtyAllowance=0;

     PendingMaterialFrame(JLayeredPane Layer,int iMillCode,String SSupCode,String SSupName,String SDesc,String SQty,NewDirectGRNGatePanel GatePanel,Vector VPendCode,Vector VPendName,Vector theVector,int iRow,String SGINo,String SGIDate,String SInvQty,Vector VPOrderNo,Vector VPOrderBlock,Vector VPMrsNo,Vector VPOrderCode,Vector VPOrderName,Vector VPOrderQty,Vector VPQtyAllow)
     {
          
          this.Layer      = Layer;
          this.iMillCode  = iMillCode;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.SDesc      = SDesc;
          this.SQty       = SQty;
          this.GatePanel  = GatePanel;
          this.VPendCode  = VPendCode;
          this.VPendName  = VPendName;
          this.theVector  = theVector;
          this.iRow       = iRow;
          this.SGINo      = SGINo;
          this.SGIDate    = SGIDate;
          this.SInvQty    = SInvQty;
          this.VPOrderNo    = VPOrderNo;
          this.VPOrderBlock = VPOrderBlock;
          this.VPMrsNo      = VPMrsNo;
          this.VPOrderCode  = VPOrderCode;
          this.VPOrderName  = VPOrderName;
          this.VPOrderQty   = VPOrderQty;
          this.VPQtyAllow   = VPQtyAllow;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          LMaterial      = new JLabel("");
          BOk            = new JButton("Okay");
          TQty           = new NextField();
          LMatCode       = new JLabel("");
          TMaterial      = new JTextField();
          TSupplier      = new JTextField();
          TopPanel       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BottomPanel    = new JPanel(true);
     }

     private void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(5,2));
          MiddlePanel.setLayout(new GridLayout(2,1));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,700,490);
     }

     private void addComponents()
     {
          TopPanel       .add(new JLabel("Selected Material Code"));
          TopPanel       .add(LMatCode);
          TopPanel       .add(new JLabel("Selected Material Name"));
          TopPanel       .add(LMaterial);
          TopPanel       .add(new JLabel("Description @Gate"));
          TopPanel       .add(TMaterial);
          TopPanel       .add(new JLabel("Supplier"));
          TopPanel       .add(TSupplier);
          TopPanel       .add(new JLabel("Qty"));
          TopPanel       .add(TQty);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BOk);

          setPresets();
     }

     private void setPresets()
     {
          TSupplier .setText(SSupName);
          TMaterial .setText(SDesc); 
          TQty      .setText(SQty);

          TSupplier .setEditable(false);
          TMaterial .setEditable(false);
          TQty .setEditable(false);
          BOk.setEnabled(false);
          setRowData1();
          try
          {
             tabreport1 = new TabReport(RowData1,ColumnData1,ColumnType1);
             MiddlePanel.add(tabreport1);
             tabreport1.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
             setSelected(true);
             Layer.repaint();
             Layer.updateUI();
             tabreport1.ReportTable.addKeyListener(new KeyList1());
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }

     }

     private void addListeners()
     {
         BOk.addActionListener(new ActList());
     }
               
     public void setRowData1()
     {
            RowData1 = new Object[VPOrderNo.size()][ColumnData1.length];
            for(int i=0;i<VPOrderNo.size();i++)
            {
                   RowData1[i][0] = common.parseNull((String)VPOrderCode.elementAt(i));
                   RowData1[i][1] = common.parseNull((String)VPOrderName.elementAt(i));
                   RowData1[i][2] = common.parseNull((String)VPOrderNo.elementAt(i));
                   RowData1[i][3] = common.parseNull((String)VPMrsNo.elementAt(i));
                   RowData1[i][4] = common.parseNull((String)VPOrderBlock.elementAt(i));
                   RowData1[i][5] = common.parseNull((String)VPOrderQty.elementAt(i));
            }
     }


     public class KeyList1 extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = tabreport1.ReportTable.getSelectedRow();
                    String SMatName     = (String)VPOrderName.elementAt(index);
                    String SMatCode     = (String)VPOrderCode.elementAt(index);
                    iQtyAllowance       = common.toInt((String)VPQtyAllow.elementAt(index));
                    LMaterial.setText(SMatName);
                    LMatCode.setText(SMatCode);
                    setTabReport(SMatCode);
                }
          }
     }

     public void setTabReport(String SMatCode)
     {
          BOk.setEnabled(true);
          GatePanel.BAdd.setEnabled(false);
          GatePanel.BDelete.setEnabled(false);

          setRowData(SMatCode);
          try
          {
             MiddlePanel.remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             MiddlePanel.add(tabreport);
             tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
             setSelected(true);
             Layer.repaint();
             Layer.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }
     }


     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validData())
               {
                  saveData();
               }
               else
               {
                  BOk.setEnabled(true);
               }
          }
     }

     private boolean validData()
     {
         int iCount=0;
         double dRecQty = 0;
         double dOrderQty = 0;
         double dExcessQty = 0;
         double dGateQty = common.toDouble(SQty);

         if(iQtyAllowance==0)
         {
              for(int i=0;i<RowData.length;i++)
              {
                   Boolean bValue = (Boolean)RowData[i][7];
     
                   if(!bValue.booleanValue())
                        continue;
     
                   iCount++;
     
                   double dPendQty   = common.toDouble((String)RowData[i][5]);
                   double dCurRecQty = common.toDouble((String)RowData[i][6]);
     
                   if(dCurRecQty>dPendQty)
                   {
                       JOptionPane.showMessageDialog(null,"Quantity Mismatch","Error",JOptionPane.ERROR_MESSAGE);
                       return false;
                   }
     
                   dRecQty = dRecQty + dCurRecQty;
              }
         }
         else
         {
              for(int i=0;i<RowData.length;i++)
              {
                   Boolean bValue = (Boolean)RowData[i][7];
     
                   if(!bValue.booleanValue())
                        continue;
     
                   iCount++;
     
                   double dPendQty   = common.toDouble((String)RowData[i][5]);
                   double dCurRecQty = common.toDouble((String)RowData[i][6]);
     
                   if(dCurRecQty>dPendQty)
                   {
                       dExcessQty = dExcessQty + (dCurRecQty - dPendQty);
                   }
                   dOrderQty = dOrderQty +  common.toDouble((String)RowData[i][4]);
                   dRecQty = dRecQty + dCurRecQty;
              }
              /*if(dExcessQty>0)
              {
                   double dAllowedQty = (dOrderQty * 200) / 100;
                   if(dExcessQty>dAllowedQty)
                   {
                       JOptionPane.showMessageDialog(null,"Quantity Allowance Exceeded","Error",JOptionPane.ERROR_MESSAGE);
                       return false;
                   }
              }*/
         }

         if(iCount==0)
         {
             JOptionPane.showMessageDialog(null,"No Row Selected","Error",JOptionPane.ERROR_MESSAGE);
             return false;
         }
         if((dRecQty>dGateQty) || (dRecQty<dGateQty))
         {
             JOptionPane.showMessageDialog(null,"Quantity Mismatch","Error",JOptionPane.ERROR_MESSAGE);
             return false;
         }
         return true;

     }
     private void saveData()
     {
         BOk.setEnabled(false);

         String SMatCode = LMatCode.getText();
         int iIndex = common.indexOf(VPendCode,SMatCode);
         PendingItem pendingitem = (PendingItem)theVector.elementAt(iIndex);

         for(int i=0;i<RowData.length;i++)
         {
              Boolean bValue = (Boolean)RowData[i][7];

              if(!bValue.booleanValue())
                   continue;

              double dRecQty     = common.toDouble((String)RowData[i][6]);
              double dPendQty    = common.toDouble((String)RowData[i][5]);
              double dPrevRecQty = common.toDouble((String)pendingitem.VGGrnQty.elementAt(i));
              double dTotRecQty  = dRecQty + dPrevRecQty;
              pendingitem.VGGrnQty.setElementAt(String.valueOf(dTotRecQty),i);

              Vector VEmpty = new Vector();

              VEmpty.addElement(SGINo);
              VEmpty.addElement(SGIDate);
              VEmpty.addElement(SDesc);
              VEmpty.addElement((String)pendingitem.VGCode.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGName.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGBlock.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGMRSNo.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGOrderNo.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGOrdQty.elementAt(i));
              VEmpty.addElement(""+dPendQty);
              VEmpty.addElement(SInvQty);
              VEmpty.addElement(""+dRecQty);
              VEmpty.addElement((String)pendingitem.VGRate.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGDiscPer.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGVatPer.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGSTPer.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGSurPer.elementAt(i));
              VEmpty.addElement("");
              VEmpty.addElement("");
              VEmpty.addElement("");
              VEmpty.addElement("");
              VEmpty.addElement("");
              VEmpty.addElement("");
              VEmpty.addElement((String)pendingitem.VGDept.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGGroup.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGUnit.elementAt(i));
              if(((String)pendingitem.VTaxC.elementAt(i)).equals("0"))
              {
                  VEmpty.addElement("Not Claimable");
              }
              else
              {
                  VEmpty.addElement("Claimable");
              }
              GatePanel.MiddlePanel.MiddlePanel.dataModel.addRow(VEmpty);

              GatePanel.MiddlePanel.VGBlockCode.addElement((String)pendingitem.VGBlockCode.elementAt(i));
              GatePanel.MiddlePanel.VId.addElement((String)pendingitem.VId.elementAt(i));
              GatePanel.MiddlePanel.VGOrdQty.addElement((String)pendingitem.VGOrdQty.elementAt(i));
              GatePanel.MiddlePanel.VMrsSlNo.addElement((String)pendingitem.VMrsSlNo.elementAt(i));
              GatePanel.MiddlePanel.VMrsUserCode.addElement((String)pendingitem.VMrsUserCode.elementAt(i));
              GatePanel.MiddlePanel.VOrderSlNo.addElement((String)pendingitem.VOrderSlNo.elementAt(i));
              GatePanel.MiddlePanel.VApproval.addElement((String)pendingitem.VApproval.elementAt(i));
              GatePanel.MiddlePanel.VTaxC.addElement((String)pendingitem.VTaxC.elementAt(i));

              System.out.println("SlNo:"+GatePanel.MiddlePanel.VMrsSlNo+";");
              System.out.println("User:"+GatePanel.MiddlePanel.VMrsUserCode+";");
         }
         GatePanel.gateModel.setValueAt(new Boolean(true),iRow,5);
         int iRows = GatePanel.MiddlePanel.MiddlePanel.dataModel.getRows();
         for(int i=0;i<iRows;i++)
         {
            Vector rowVector = (Vector)GatePanel.MiddlePanel.MiddlePanel.dataModel.getCurVector(i);
            GatePanel.MiddlePanel.MiddlePanel.dataModel.setMaterialAmount(rowVector);
         }
         GatePanel.BAdd.setEnabled(true);
         GatePanel.BDelete.setEnabled(true);
         removeHelpFrame();
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }

     public void setRowData(String SMatCode)
     {
         int iIndex = common.indexOf(VPendCode,SMatCode);
         PendingItem pendingitem = (PendingItem)theVector.elementAt(iIndex);

         RowData     = new Object[pendingitem.VGCode.size()][ColumnData.length];
         for(int i=0;i<pendingitem.VGCode.size();i++)
         {
               double dPendQty   = common.toDouble((String)pendingitem.VGPendQty.elementAt(i));
               double dCurGrnQty = common.toDouble((String)pendingitem.VGGrnQty.elementAt(i));
               double dCurPendQty = dPendQty - dCurGrnQty;

               RowData[i][0]  = (String)pendingitem.VGOrderNo.elementAt(i);
               RowData[i][1]  = common.parseDate((String)pendingitem.VGOrdDate.elementAt(i));
               RowData[i][2]  = (String)pendingitem.VGBlock.elementAt(i);
               RowData[i][3]  = (String)pendingitem.VGMRSNo.elementAt(i);
               RowData[i][4]  = (String)pendingitem.VGOrdQty.elementAt(i);
               RowData[i][5]  = ""+dCurPendQty;
               RowData[i][6]  = "";
               RowData[i][7]  = new Boolean(false);
        }  
     } 

}
