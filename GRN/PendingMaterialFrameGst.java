package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class PendingMaterialFrameGst extends JInternalFrame
{
     
     JLayeredPane   Layer;
     TabReport      tabreport;
     TabReport      tabreport1;

     JButton        BOk;
     JLabel         LMaterial,LMatCode;
     int            iMillCode,iRow;
     String         SSupCode,SSupName,SDesc,SQty;
     String         SGINo,SGIDate,SInvQty;
     NewDirectGRNGatePanelGst GatePanel;
     Vector         VPendCode,VPendName,theVector;
     Vector VPOrderNo,VPOrderBlock,VPMrsNo,VPOrderCode,VPOrderName,VPOrderQty,VPQtyAllow;

     JPanel         TopPanel,MiddlePanel,BottomPanel;
     JTextField     TSupplier,TMaterial;
     NextField      TQty;

     Common common = new Common();

     Object RowData[][];
     String ColumnData[] = {"Order No","Order Date","Block","Mrs No","Order Qty","Pending Qty","Recd Qty","Click for Select","HsnCode","GstRate"};
     String ColumnType[] = {"N"       ,"S"         ,"S"    ,"N"     ,"N"        ,"N"          ,"E"       ,"B"               ,"S"      ,"N"      };

     Object RowData1[][];
     String ColumnData1[] = {"Code","Item Name","OrderNo","MrsNo","Block","OrderQty"};
     String ColumnType1[] = {"S"   ,"S"        ,"S"      ,"S"    ,"S"    ,"S"       };

     int iQtyAllowance=0;

     Connection theConnection = null;

     PendingMaterialFrameGst(JLayeredPane Layer,int iMillCode,String SSupCode,String SSupName,String SDesc,String SQty,NewDirectGRNGatePanelGst GatePanel,Vector VPendCode,Vector VPendName,Vector theVector,int iRow,String SGINo,String SGIDate,String SInvQty,Vector VPOrderNo,Vector VPOrderBlock,Vector VPMrsNo,Vector VPOrderCode,Vector VPOrderName,Vector VPOrderQty,Vector VPQtyAllow)
     {
          
          this.Layer      = Layer;
          this.iMillCode  = iMillCode;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.SDesc      = SDesc;
          this.SQty       = SQty;
          this.GatePanel  = GatePanel;
          this.VPendCode  = VPendCode;
          this.VPendName  = VPendName;
          this.theVector  = theVector;
          this.iRow       = iRow;
          this.SGINo      = SGINo;
          this.SGIDate    = SGIDate;
          this.SInvQty    = SInvQty;
          this.VPOrderNo    = VPOrderNo;
          this.VPOrderBlock = VPOrderBlock;
          this.VPMrsNo      = VPMrsNo;
          this.VPOrderCode  = VPOrderCode;
          this.VPOrderName  = VPOrderName;
          this.VPOrderQty   = VPOrderQty;
          this.VPQtyAllow   = VPQtyAllow;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          LMaterial      = new JLabel("");
          BOk            = new JButton("Okay");
          TQty           = new NextField();
          LMatCode       = new JLabel("");
          TMaterial      = new JTextField();
          TSupplier      = new JTextField();
          TopPanel       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BottomPanel    = new JPanel(true);
     }

     private void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(5,2));
          MiddlePanel.setLayout(new GridLayout(2,1));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,700,490);
     }

     private void addComponents()
     {
          TopPanel       .add(new JLabel("Selected Material Code"));
          TopPanel       .add(LMatCode);
          TopPanel       .add(new JLabel("Selected Material Name"));
          TopPanel       .add(LMaterial);
          TopPanel       .add(new JLabel("Description @Gate"));
          TopPanel       .add(TMaterial);
          TopPanel       .add(new JLabel("Supplier"));
          TopPanel       .add(TSupplier);
          TopPanel       .add(new JLabel("Qty"));
          TopPanel       .add(TQty);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BOk);

          setPresets();
     }

     private void setPresets()
     {
          TSupplier .setText(SSupName);
          TMaterial .setText(SDesc); 
          TQty      .setText(SQty);

          TSupplier .setEditable(false);
          TMaterial .setEditable(false);
          TQty .setEditable(false);
          BOk.setEnabled(false);
          setRowData1();
          try
          {
             tabreport1 = new TabReport(RowData1,ColumnData1,ColumnType1);
             MiddlePanel.add(tabreport1);
             tabreport1.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
             setSelected(true);
             Layer.repaint();
             Layer.updateUI();
             tabreport1.ReportTable.addKeyListener(new KeyList1());
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }
     }

     private void addListeners()
     {
         BOk.addActionListener(new ActList());
     }
               
     public void setRowData1()
     {
            RowData1 = new Object[VPOrderNo.size()][ColumnData1.length];
            for(int i=0;i<VPOrderNo.size();i++)
            {
                   RowData1[i][0] = common.parseNull((String)VPOrderCode.elementAt(i));
                   RowData1[i][1] = common.parseNull((String)VPOrderName.elementAt(i));
                   RowData1[i][2] = common.parseNull((String)VPOrderNo.elementAt(i));
                   RowData1[i][3] = common.parseNull((String)VPMrsNo.elementAt(i));
                   RowData1[i][4] = common.parseNull((String)VPOrderBlock.elementAt(i));
                   RowData1[i][5] = common.parseNull((String)VPOrderQty.elementAt(i));
            }
     }

     public class KeyList1 extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = tabreport1.ReportTable.getSelectedRow();
                    String SMatName     = (String)VPOrderName.elementAt(index);
                    String SMatCode     = (String)VPOrderCode.elementAt(index);
                    iQtyAllowance       = common.toInt((String)VPQtyAllow.elementAt(index));
                    LMaterial.setText(SMatName);
                    LMatCode.setText(SMatCode);
                    setTabReport(SMatCode);
                }
          }
     }

     public void setTabReport(String SMatCode)
     {
          BOk.setEnabled(false);
          GatePanel.BAdd.setEnabled(false);
          GatePanel.BDelete.setEnabled(false);

          setRowData(SMatCode);
          try
          {
             MiddlePanel.remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             MiddlePanel.add(tabreport);
             tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
             setSelected(true);
             Layer.repaint();
             Layer.updateUI();
             tabreport.ReportTable.addKeyListener(new KeyList2());
             tabreport.ReportTable.addMouseListener(new MouseList2());
             tabreport.ReportTable.addKeyListener(new ServList());
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }
     }

     private class KeyList2 extends KeyAdapter
     {
         public void keyPressed(KeyEvent ke)
         {
            if(ke.getKeyCode()==KeyEvent.VK_SPACE)
            {
                if(validSeleData())
                {
                     if(checkOrderRate())
		     {
		          BOk.setEnabled(true);
		     }
		     else
		     {
		          BOk.setEnabled(false);
		     }
                }
            }
         }
     }
	
     public class MouseList2 extends MouseAdapter
     {
          public void mouseReleased(MouseEvent me)
          {
               if(validSeleData())
               {
                     if(checkOrderRate())
		     {
		          BOk.setEnabled(true);
		     }
		     else
		     {
		          BOk.setEnabled(false);
		     }
               }
          }
     }

     private boolean validSeleData()
     {
         int iCol     = tabreport.ReportTable.getSelectedColumn();

         if(iCol!=7)
             return false;
         
         int iRow     = tabreport.ReportTable.getSelectedRow();

         String SHsnCode = (String)RowData[iRow][8];
         double dGstRate = common.toDouble((String)RowData[iRow][9]);
         String SQty     = (String)RowData[iRow][6];
		 
         if(SHsnCode.equals("") || SHsnCode.equals("0"))
         {
             tabreport.ReportTable.setValueAt(new Boolean(false),iRow,7);
             JOptionPane.showMessageDialog(null,"HsnCode Not Updated for Material @Row-"+String.valueOf(iRow+1),"Error",JOptionPane.ERROR_MESSAGE);
             return false;
         }

         if(GatePanel.MiddlePanel.SSeleSupType.equals("1"))
	 {
	      int iHsnRateCheck = checkHsnGstRate(SHsnCode,dGstRate);

              if(iHsnRateCheck<=0)
              {
                   tabreport.ReportTable.setValueAt(new Boolean(false),iRow,7);
                   JOptionPane.showMessageDialog(null,"This Combination of HsnCode and GstRate Not Exists for Material @Row-"+String.valueOf(iRow+1),"Error",JOptionPane.ERROR_MESSAGE);
                   return false;
              }
	 }

         if(common.toDouble(SQty)<=0)
         {
             tabreport.ReportTable.setValueAt(new Boolean(false),iRow,7);
             JOptionPane.showMessageDialog(null,"Invalid Recd Qty","Error",JOptionPane.ERROR_MESSAGE);
             return false;
         }

         return true;
     }

     public int checkHsnGstRate(String SHsnCode,double dGstRate)
     {
	int iHCount=0;

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "Select Count(*) from HsnGstRate Where HsnCode='"+SHsnCode+"' and GstRate="+dGstRate;

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    iHCount = result.getInt(1);
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("checkHsnGstRate :"+ex);
        }
	return iHCount;
     }

     private boolean checkOrderRate()
     {
	  String SMatCode = LMatCode.getText();
	  int iIndex = common.indexOf(VPendCode,SMatCode);
	  PendingItemGst pendingitem = (PendingItemGst)theVector.elementAt(iIndex);

	  for(int i=0;i<RowData.length;i++)
	  {
	       Boolean bValue = (Boolean)RowData[i][7];

	       if(!bValue.booleanValue())
	            continue;

	       String SOrderNo = (String)pendingitem.VGOrderNo.elementAt(i);

	       String SCGST     = common.getRound(common.toDouble((String)pendingitem.VGNewCGSTPer.elementAt(i)),2);
	       String SSGST     = common.getRound(common.toDouble((String)pendingitem.VGNewSGSTPer.elementAt(i)),2);
	       String SIGST     = common.getRound(common.toDouble((String)pendingitem.VGNewIGSTPer.elementAt(i)),2);
	       String SCess     = common.getRound(common.toDouble((String)pendingitem.VGNewCessPer.elementAt(i)),2);

	       String SOrdCGST = common.getRound(common.toDouble((String)pendingitem.VGCGSTPer.elementAt(i)),2);
	       String SOrdSGST = common.getRound(common.toDouble((String)pendingitem.VGSGSTPer.elementAt(i)),2);
	       String SOrdIGST = common.getRound(common.toDouble((String)pendingitem.VGIGSTPer.elementAt(i)),2);
	       String SOrdCess = common.getRound(common.toDouble((String)pendingitem.VGCessPer.elementAt(i)),2);

	       if(!(SCGST.equals(SOrdCGST) && SSGST.equals(SOrdSGST) && SIGST.equals(SOrdIGST) && SCess.equals(SOrdCess)))
	       {
		     JOptionPane.showMessageDialog(null,getAllFilledMessage(i,SOrderNo,SMatCode,SCGST,SSGST,SIGST,SCess,SOrdCGST,SOrdSGST,SOrdIGST,SOrdCess),"Error",JOptionPane.ERROR_MESSAGE);
		     return false;
	       }
	  }

	  return true;
     }

     private String getAllFilledMessage(int iRow,String SOrderNo,String SItemCode,String SCGST,String SSGST,String SIGST,String SCess,String SOrdCGST,String SOrdSGST,String SOrdIGST,String SOrdCess)
     {
          String str = "<html><body>";
          str = str + "<b>GRN and Order Tax Rate Mismatch at Row-"+String.valueOf(iRow+1)+"</b>"+"<br>";
          str = str + "<b>OrderNo	 : "+SOrderNo+"		Code		: "+SItemCode+"</b>"+"<br>";
          str = str + "Order CGST %	 : "+SOrdCGST+"		Grn CGST %	: "+SCGST+"<br>";
          str = str + "Order SGST %	 : "+SOrdSGST+"		Grn SGST %	: "+SSGST+"<br>";
          str = str + "Order IGST %	 : "+SOrdIGST+"		Grn IGST %	: "+SIGST+"<br>";
          str = str + "Order Cess %	 : "+SOrdCess+"		Grn Cess %	: "+SCess+"<br>";
          str = str + "</body></html>";
          return str;
     }

     public class ServList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_F2) 
	       {
		    String SMatCode = LMatCode.getText();
		    int iIndex = common.indexOf(VPendCode,SMatCode);
		    PendingItemGst pendingitem = (PendingItemGst)theVector.elementAt(iIndex);

		    int i = tabreport.ReportTable.getSelectedRow();
		    int iCol = tabreport.ReportTable.getSelectedColumn();

	            String SGrnStatus = (String)pendingitem.VGGrnStatus.elementAt(i);

		    if(iCol==8)
		    {
		 	 String SHCode     = (String)RowData[i][8];
	       	         String SItemCode  = SMatCode; 
		         String SMasterHsn = getMasterHsn(SItemCode);
		         String SMultiHsn  = getMultiHsn(SItemCode);

			 if((SHCode.equals("") || SHCode.equals("0") || SMasterHsn.equals("0") || SMasterHsn.equals("")))
			 {
			      if(SMultiHsn.equals("1"))
			      {	
		                   setHsnList(SItemCode,1,pendingitem);
			      }
			      else
			      {	
		                   setHsnList(SItemCode,0,pendingitem);
			      }
			 }
			 else 
			 {
              		      Boolean bValue = (Boolean)RowData[i][7];

			      if(bValue.booleanValue())
			      {
			           JOptionPane.showMessageDialog(null,"This row couldn't be changed. Already Selected for GRN","Information",JOptionPane.INFORMATION_MESSAGE);
			      }
			      else
			      {
				   if(SGrnStatus.equals("1"))
				   {
			                JOptionPane.showMessageDialog(null,"This row couldn't be changed. Partial Grn Already Made for  ItemCode-"+SItemCode,"Information",JOptionPane.INFORMATION_MESSAGE);
				   }
				   else
				   {
				        setHsnList(SItemCode,1,pendingitem);
				   }
			      }	
		         }
		    }
	       }
          }
     }

     public String getMasterHsn(String SItemCode)
     {
	String SMHsn = "";

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "Select HsnCode from InvItems where Item_Code='"+SItemCode+"'";

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    SMHsn = common.parseNull(result.getString(1));
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getMasterHsn :"+ex);
        }
	return SMHsn;
     }

     public String getMultiHsn(String SItemCode)
     {
	String SMulti = "";

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "Select nvl(MultiHsn,0) from InvItems where Item_Code='"+SItemCode+"'";

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    SMulti = common.parseNull(result.getString(1));
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getMultiHsn :"+ex);
        }
	return SMulti;
     }

     private void setHsnList(String SItemCode,int iDispStatus,PendingItemGst pendingitem)
     {
           HsnCodePicker1 HP = new HsnCodePicker1(Layer,tabreport,pendingitem,SItemCode,iDispStatus,GatePanel.MiddlePanel.SSeleSupType,GatePanel.MiddlePanel.SSeleStateCode);
           try
           {
                Layer     . add(HP);
                Layer     . repaint();
                HP        . setSelected(true);
                Layer     . updateUI();
                HP        . show();
                HP        . BrowList.requestFocus();
           }
           catch(java.beans.PropertyVetoException ex){}
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validData())
               {
                  saveData();
               }
               else
               {
                  BOk.setEnabled(true);
               }
          }
     }

     private boolean validData()
     {
         int iCount=0;
         double dRecQty = 0;
         double dOrderQty = 0;
         double dExcessQty = 0;
         double dGateQty = common.toDouble(SQty);

         if(iQtyAllowance==0)
         {
              for(int i=0;i<RowData.length;i++)
              {
                   Boolean bValue = (Boolean)RowData[i][7];
     
                   if(!bValue.booleanValue())
                        continue;
     
                   iCount++;
     
                   double dPendQty   = common.toDouble((String)RowData[i][5]);
                   double dCurRecQty = common.toDouble((String)RowData[i][6]);
     
                   if(dCurRecQty>dPendQty)
                   {
                       JOptionPane.showMessageDialog(null,"Quantity Mismatch","Error",JOptionPane.ERROR_MESSAGE);
                       return false;
                   }
     
                   dRecQty = dRecQty + dCurRecQty;
              }
         }
         else
         {
              for(int i=0;i<RowData.length;i++)
              {
                   Boolean bValue = (Boolean)RowData[i][7];
     
                   if(!bValue.booleanValue())
                        continue;
     
                   iCount++;
     
                   double dPendQty   = common.toDouble((String)RowData[i][5]);
                   double dCurRecQty = common.toDouble((String)RowData[i][6]);
     
                   if(dCurRecQty>dPendQty)
                   {
                       dExcessQty = dExcessQty + (dCurRecQty - dPendQty);
                   }
                   dOrderQty = dOrderQty +  common.toDouble((String)RowData[i][4]);
                   dRecQty = dRecQty + dCurRecQty;
              }
              /*if(dExcessQty>0)
              {
                   double dAllowedQty = (dOrderQty * 200) / 100;
                   if(dExcessQty>dAllowedQty)
                   {
                       JOptionPane.showMessageDialog(null,"Quantity Allowance Exceeded","Error",JOptionPane.ERROR_MESSAGE);
                       return false;
                   }
              }*/
         }

         if(iCount==0)
         {
             JOptionPane.showMessageDialog(null,"No Row Selected","Error",JOptionPane.ERROR_MESSAGE);
             return false;
         }
         if((dRecQty>dGateQty) || (dRecQty<dGateQty))
         {
             JOptionPane.showMessageDialog(null,"Quantity Mismatch","Error",JOptionPane.ERROR_MESSAGE);
             return false;
         }
         return true;
     }

     private void saveData()
     {
         BOk.setEnabled(false);

         String SMatCode = LMatCode.getText();
         int iIndex = common.indexOf(VPendCode,SMatCode);
         PendingItemGst pendingitem = (PendingItemGst)theVector.elementAt(iIndex);

         for(int i=0;i<RowData.length;i++)
         {
              Boolean bValue = (Boolean)RowData[i][7];

              if(!bValue.booleanValue())
                   continue;

              double dRecQty     = common.toDouble((String)RowData[i][6]);
              double dPendQty    = common.toDouble((String)RowData[i][5]);
              double dPrevRecQty = common.toDouble((String)pendingitem.VGGrnQty.elementAt(i));
              double dTotRecQty  = dRecQty + dPrevRecQty;
              pendingitem.VGGrnQty.setElementAt(String.valueOf(dTotRecQty),i);

              Vector VEmpty = new Vector();

              VEmpty.addElement(SGINo);
              VEmpty.addElement(SGIDate);
              VEmpty.addElement(SDesc);
              VEmpty.addElement((String)pendingitem.VGCode.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGName.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGNewHsnCode.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGBlock.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGMRSNo.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGOrderNo.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGOrdQty.elementAt(i));
              VEmpty.addElement(""+dPendQty);
              VEmpty.addElement(SInvQty);
              VEmpty.addElement(""+dRecQty);
              VEmpty.addElement((String)pendingitem.VGRate.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGDiscPer.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGCGSTPer.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGSGSTPer.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGIGSTPer.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGCessPer.elementAt(i));
              VEmpty.addElement("");
              VEmpty.addElement("");
              VEmpty.addElement("");
              VEmpty.addElement("");
              VEmpty.addElement("");
              VEmpty.addElement("");
              VEmpty.addElement("");
              VEmpty.addElement((String)pendingitem.VGDept.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGGroup.elementAt(i));
              VEmpty.addElement((String)pendingitem.VGUnit.elementAt(i));
              if(((String)pendingitem.VTaxC.elementAt(i)).equals("0"))
              {
                  VEmpty.addElement("Not Claimable");
              }
              else
              {
                  VEmpty.addElement("Claimable");
              }
              GatePanel.MiddlePanel.MiddlePanel.dataModel.addRow(VEmpty);

              GatePanel.MiddlePanel.MiddlePanel.VHsnType.addElement((String)pendingitem.VGHsnType.elementAt(i));
              GatePanel.MiddlePanel.MiddlePanel.VSeleId.addElement((String)pendingitem.VId.elementAt(i));
              GatePanel.MiddlePanel.MiddlePanel.VRateStatus.addElement((String)pendingitem.VGRateStatus.elementAt(i));

              GatePanel.MiddlePanel.VGBlockCode.addElement((String)pendingitem.VGBlockCode.elementAt(i));
              GatePanel.MiddlePanel.VId.addElement((String)pendingitem.VId.elementAt(i));
              GatePanel.MiddlePanel.VGOrdQty.addElement((String)pendingitem.VGOrdQty.elementAt(i));
              GatePanel.MiddlePanel.VMrsSlNo.addElement((String)pendingitem.VMrsSlNo.elementAt(i));
              GatePanel.MiddlePanel.VMrsUserCode.addElement((String)pendingitem.VMrsUserCode.elementAt(i));
              GatePanel.MiddlePanel.VOrderSlNo.addElement((String)pendingitem.VOrderSlNo.elementAt(i));
              GatePanel.MiddlePanel.VApproval.addElement((String)pendingitem.VApproval.elementAt(i));
              GatePanel.MiddlePanel.VTaxC.addElement((String)pendingitem.VTaxC.elementAt(i));
         }
         GatePanel.gateModel.setValueAt(new Boolean(true),iRow,5);

	 GatePanel.MiddlePanel.MiddlePanel.dataModel.setColumnData();
         int iRows = GatePanel.MiddlePanel.MiddlePanel.dataModel.getRows();
         for(int i=0;i<iRows;i++)
         {
            Vector rowVector   = (Vector)GatePanel.MiddlePanel.MiddlePanel.dataModel.getCurVector(i);
	    String SHsnType    = (String)GatePanel.MiddlePanel.MiddlePanel.VHsnType.elementAt(i);
	    String SRateStatus = (String)GatePanel.MiddlePanel.MiddlePanel.VRateStatus.elementAt(i);

            GatePanel.MiddlePanel.MiddlePanel.dataModel.setMaterialAmount(rowVector,SHsnType,SRateStatus,i);
         }
         GatePanel.BAdd.setEnabled(true);
         GatePanel.BDelete.setEnabled(true);
         removeHelpFrame();
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }

     public void setRowData(String SMatCode)
     {
         int iIndex = common.indexOf(VPendCode,SMatCode);
         PendingItemGst pendingitem = (PendingItemGst)theVector.elementAt(iIndex);

         RowData     = new Object[pendingitem.VGCode.size()][ColumnData.length];
         for(int i=0;i<pendingitem.VGCode.size();i++)
         {
               double dPendQty   = common.toDouble((String)pendingitem.VGPendQty.elementAt(i));
               double dCurGrnQty = common.toDouble((String)pendingitem.VGGrnQty.elementAt(i));
               double dCurPendQty = dPendQty - dCurGrnQty;

	       String SGrnStatus = (String)pendingitem.VGGrnStatus.elementAt(i);

               RowData[i][0]  = (String)pendingitem.VGOrderNo.elementAt(i);
               RowData[i][1]  = common.parseDate((String)pendingitem.VGOrdDate.elementAt(i));
               RowData[i][2]  = (String)pendingitem.VGBlock.elementAt(i);
               RowData[i][3]  = (String)pendingitem.VGMRSNo.elementAt(i);
               RowData[i][4]  = (String)pendingitem.VGOrdQty.elementAt(i);
               RowData[i][5]  = ""+dCurPendQty;
               RowData[i][6]  = "";
               RowData[i][7]  = new Boolean(false);
	       if(SGrnStatus.equals("1"))
	       {
		    double dCGSTPer   = common.toDouble((String)pendingitem.VGCGSTPer.elementAt(i));
		    double dSGSTPer   = common.toDouble((String)pendingitem.VGSGSTPer.elementAt(i));
		    double dIGSTPer   = common.toDouble((String)pendingitem.VGIGSTPer.elementAt(i));
		    String SGstRate   = common.getRound(dCGSTPer + dSGSTPer + dIGSTPer,2);

                    RowData[i][8]  = (String)pendingitem.VGHsnCode.elementAt(i);
                    RowData[i][9]  = SGstRate;
	       }
	       else
	       {
                    RowData[i][8]  = "";
                    RowData[i][9]  = "";
	       }
	
         }  
     } 

}
