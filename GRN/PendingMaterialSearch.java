package GRN;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class PendingMaterialSearch implements ActionListener
{
     JLayeredPane   Layer;
     JTextField     TMatCode;
     JButton        BMatName;
     Vector         VPendName,VPendCode;
     JDialog        MaterialDialog;
     PendingMaterialFrame pendingmaterialframe;
     int iMillCode;
     Vector VPOrderNo,VPOrderBlock,VPMrsNo,VPOrderCode,VPOrderName,VPOrderQty;


     JTextField     TFind;
     JPanel         LeftPanel;
     JPanel         BottomPanel;
     JInternalFrame MaterialFrame;
     JPanel         MaterialPanel= new JPanel(true);

     ActionEvent ae;
     Common common = new Common();

     Object RowData[][];
     String ColumnData[] = {"Code","Item Name","OrderNo","MrsNo","Block","OrderQty"};
     String ColumnType[] = {"S"   ,"S"        ,"S"      ,"S"    ,"S"    ,"S"       };

     TabReport tabreport;

     PendingMaterialSearch(JLayeredPane Layer,JTextField TMatCode,JButton BMatName,Vector VPendCode,Vector VPendName,JDialog MaterialDialog,PendingMaterialFrame pendingmaterialframe,int iMillCode,Vector VPOrderNo,Vector VPOrderBlock,Vector VPMrsNo,Vector VPOrderCode,Vector VPOrderName,Vector VPOrderQty)
     {
          this.Layer       = Layer;
          this.TMatCode    = TMatCode;
          this.BMatName    = BMatName;
          this.VPendCode   = VPendCode;
          this.VPendName   = VPendName;
          this.MaterialDialog = MaterialDialog;
          this.pendingmaterialframe = pendingmaterialframe;
          this.iMillCode    = iMillCode;
          this.VPOrderNo    = VPOrderNo;
          this.VPOrderBlock = VPOrderBlock;
          this.VPMrsNo      = VPMrsNo;
          this.VPOrderCode  = VPOrderCode;
          this.VPOrderName  = VPOrderName;
          this.VPOrderQty   = VPOrderQty;

          LeftPanel = new JPanel(true);
          TFind     = new JTextField();

          setRowData();

          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             LeftPanel.add(tabreport,BorderLayout.CENTER);
             Layer.repaint();
             Layer.updateUI();
             TFind.addKeyListener(new KeyList());
             tabreport.ReportTable.addKeyListener(new KeyList1());
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }

          MaterialFrame     = new JInternalFrame("Materials Selector");
          MaterialFrame.show();
          MaterialFrame.setBounds(80,100,500,450);
          MaterialFrame.setClosable(true);
          MaterialFrame.setResizable(true);

          BottomPanel   = new JPanel(true);
          BottomPanel.setLayout(new GridLayout(1,2));
          BottomPanel.add(TFind);

          MaterialFrame.getContentPane().setLayout(new BorderLayout());
          MaterialFrame.getContentPane().add("South",BottomPanel);
          MaterialFrame.getContentPane().add("Center",LeftPanel);

          MaterialPanel.setLayout(new BorderLayout());
          MaterialPanel.add("South",BottomPanel);
          MaterialPanel.add("Center",LeftPanel);
          MaterialFrame.setVisible(false);
     }

     public void setRowData()
     {
            RowData = new Object[VPOrderNo.size()][ColumnData.length];
            for(int i=0;i<VPOrderNo.size();i++)
            {
                   RowData[i][0] = common.parseNull((String)VPOrderCode.elementAt(i));
                   RowData[i][1] = common.parseNull((String)VPOrderName.elementAt(i));
                   RowData[i][2] = common.parseNull((String)VPOrderNo.elementAt(i));
                   RowData[i][3] = common.parseNull((String)VPMrsNo.elementAt(i));
                   RowData[i][4] = common.parseNull((String)VPOrderBlock.elementAt(i));
                   RowData[i][5] = common.parseNull((String)VPOrderQty.elementAt(i));
            }
     }

      
     public void actionPerformed(ActionEvent ae)
     {
          this.ae = ae;
          removeHelpFrame();
          try
          {
               Layer.add(MaterialFrame);
               MaterialFrame.moveToFront();
               MaterialFrame.setSelected(true);
               MaterialFrame.show();
               Layer.repaint();
          }
          catch(Exception ex){}
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String SFind = TFind.getText();
               int i=indexOf(SFind);
               if (i>-1)
               {
                    tabreport.ReportTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect = tabreport.ReportTable.getCellRect(i,0,true);
                    tabreport.ReportTable.scrollRectToVisible(cellRect);
               }
          }
     }
     public int indexOf(String SFind)
     {
           SFind = SFind.toUpperCase();
           for(int i=0;i<VPOrderName.size();i++)
           {
                   String str = (String)VPOrderName.elementAt(i);
                   str=str.toUpperCase();
                   if(str.startsWith(SFind))
                           return i;
           }
           return -1;
     }

     public class KeyList1 extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = tabreport.ReportTable.getSelectedRow();
                    String SMatName     = (String)VPOrderName.elementAt(index);
                    String SMatCode     = (String)VPOrderCode.elementAt(index);
                    addMatDet(SMatName,SMatCode);
                    pendingmaterialframe.setTabReport(SMatCode);
                    removeHelpFrame();
                }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(MaterialFrame);
               Layer.repaint();
               Layer.updateUI();
               BMatName.requestFocus();
               MaterialDialog.setVisible(false);
          }
          catch(Exception ex) { }
     }
     public boolean addMatDet(String SMatName,String SMatCode)
     {
          TMatCode.setText(SMatCode);
          BMatName.setText(SMatName);
          return true;    
     }
}
