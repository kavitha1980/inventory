package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class PendingRDCMaterialFrame extends JInternalFrame
{
     
     JLayeredPane   Layer;
     int            iMillCode;
     String         SGrnNo,SGrnDate,SSupCode,SSupName,SItemName,SRejQty,SGIDate;
     TabReport      tabreportx;
     Vector         VRdcId;

     JLabel         LGrnNo,LGrnDate,LSupplier,LItemName,LRejQty;

     TabReport      tabreport;

     Vector         VPRdcType,VPRdcNo,VPRdcDate,VPRdcDesc,VPRdcQty,VPRdcId;

     JPanel         TopPanel,MiddlePanel,BottomPanel;

     Common common = new Common();

     Object RowData[][];
     String ColumnData[] = {"RDC Type","RDC No","RDC Date","Description","Qty"};
     String ColumnType[] = {"S"       ,"N"     ,"S"       ,"S"          ,"N"  };

     ORAConnection connect;
     Connection theconnect;

     PendingRDCMaterialFrame(JLayeredPane Layer,int iMillCode,String SGrnNo,String SGrnDate,String SSupCode,String SSupName,String SItemName,String SRejQty,String SGIDate,TabReport tabreportx,Vector VRdcId)
     {
          
          this.Layer      = Layer;
          this.iMillCode  = iMillCode;
          this.SGrnNo     = SGrnNo;
          this.SGrnDate   = SGrnDate;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.SItemName  = SItemName;
          this.SRejQty    = SRejQty;
          this.SGIDate    = SGIDate;
          this.tabreportx = tabreportx;
          this.VRdcId     = VRdcId;

          createComponents();
          setLayouts();
          addComponents();
     }

     private void createComponents()
     {
          LGrnNo         = new JLabel("");
          LGrnDate       = new JLabel("");
          LSupplier      = new JLabel("");
          LItemName      = new JLabel("");
          LRejQty        = new JLabel("");

          TopPanel       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BottomPanel    = new JPanel(true);
     }

     private void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(5,2,10,10));
          MiddlePanel.setLayout(new BorderLayout());

          TopPanel.setBorder(new TitledBorder("Selected Grn Rejection Details"));
          MiddlePanel.setBorder(new TitledBorder("Pending RDC Details"));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,800,500);
     }

     private void addComponents()
     {
          TopPanel       .add(new JLabel("GrnNo"));
          TopPanel       .add(LGrnNo);
          TopPanel       .add(new JLabel("GrnDate"));
          TopPanel       .add(LGrnDate);
          TopPanel       .add(new JLabel("Supplier"));
          TopPanel       .add(LSupplier);
          TopPanel       .add(new JLabel("Material Name"));
          TopPanel       .add(LItemName);
          TopPanel       .add(new JLabel("Rejected Qty"));
          TopPanel       .add(LRejQty);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

          setPresets();
     }

     private void setPresets()
     {
          LGrnNo    .setText(SGrnNo);
          LGrnDate  .setText(SGrnDate);
          LSupplier .setText(SSupName);
          LItemName .setText(SItemName); 
          LRejQty   .setText(SRejQty);

          setDataVector();
          setRowData();

          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             MiddlePanel.add("Center",tabreport);
             tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
             setSelected(true);
             Layer.repaint();
             Layer.updateUI();
             tabreport.ReportTable.addKeyListener(new KeyList());
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }
     }

     private void setDataVector()
     {
          VPRdcType      = new Vector();
          VPRdcNo        = new Vector();
          VPRdcDate      = new Vector();
          VPRdcDesc      = new Vector();
          VPRdcQty       = new Vector();
          VPRdcId        = new Vector();

          String QS = " Select DocType,RdcNo,RdcDate,Descript,Qty,Id "+
                      " From RDC "+
                      " Where RdcDate>="+SGIDate+" and Sup_Code='"+SSupCode+"'"+
                      " and GrnMatchStatus = 0 and MillCode="+iMillCode+" "+
                      " Order By Id,Descript ";

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat       = theconnect.createStatement();
               ResultSet theResult  = stat.executeQuery(QS);

               while(theResult.next())
               {
                    String SDocType = theResult.getString(1);

                    if(SDocType.equals("0"))
                    {
                         VPRdcType.addElement("Returnable");
                    }
                    else
                    {
                         VPRdcType.addElement("Non-Returnable");
                    }
                    VPRdcNo     .addElement(theResult.getString(2));
                    VPRdcDate   .addElement(common.parseDate(theResult.getString(3)));
                    VPRdcDesc   .addElement(theResult.getString(4));
                    VPRdcQty    .addElement(theResult.getString(5));
                    VPRdcId     .addElement(theResult.getString(6));
               }
               theResult.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setRowData()
     {
            RowData = new Object[VPRdcNo.size()][ColumnData.length];
            for(int i=0;i<VPRdcNo.size();i++)
            {
                   RowData[i][0] = common.parseNull((String)VPRdcType.elementAt(i));
                   RowData[i][1] = common.parseNull((String)VPRdcNo.elementAt(i));
                   RowData[i][2] = common.parseNull((String)VPRdcDate.elementAt(i));
                   RowData[i][3] = common.parseNull((String)VPRdcDesc.elementAt(i));
                   RowData[i][4] = common.parseNull((String)VPRdcQty.elementAt(i));
            }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    if(validData())
                    {
                         saveData();
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Quantity MisMatch","Error",JOptionPane.ERROR_MESSAGE);
                    }
               }
          }
     }

     private boolean validData()
     {
         int index = tabreport.ReportTable.getSelectedRow();

         double dRdcQty = common.toDouble((String)VPRdcQty.elementAt(index));
         double dRejQty = common.toDouble(SRejQty);

         if((dRdcQty<dRejQty) || (dRdcQty>dRejQty))
         {
              return false;
         }

         return true;
     }

     private void saveData()
     {

         int index = tabreport.ReportTable.getSelectedRow();
         String SRdcNo   = (String)VPRdcNo.elementAt(index);
         String SRdcDate = (String)VPRdcDate.elementAt(index);
         String SRdcDesc = (String)VPRdcDesc.elementAt(index);
         String SRdcId   = (String)VPRdcId.elementAt(index);


         int iRow = tabreportx.ReportTable.getSelectedRow();

         tabreportx.ReportTable.setValueAt(SRdcNo,iRow,8);
         tabreportx.ReportTable.setValueAt(SRdcDate,iRow,9);
         tabreportx.ReportTable.setValueAt(SRdcDesc,iRow,10);
         tabreportx.ReportTable.setValueAt(new Boolean(true),iRow,11);

         VRdcId.setElementAt(SRdcId,iRow);

         removeHelpFrame();
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }


}
