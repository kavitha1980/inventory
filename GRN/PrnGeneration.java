package GRN;
 
import java.io.*;
import java.sql.*;
import java.util.*;
import util.*;

public class PrnGeneration
{
     String              SMainTitle,SPitch;

     String[]            SSubTitles,SAlign;
     String[]            SHead1,SHead2,SHead3;

     int[]               iLength;

     Common              common;

     FileWriter          FW;

     int                 iLineCount,iPageNo;

     String              SHead= "";
     String              SPaper="";
     //String              sSignPitch="              ";
     int                 iColumns=130;


     public PrnGeneration(FileWriter FW,String SMainTitle,String[] SSubTitles,String[] SAlign,int[] iLength,String SPitch,String[] SHead1,String[] SHead2,String[] SHead3)
             
     {
          this . FW           = FW;
          this . SMainTitle   = SMainTitle;
          this . SSubTitles   = SSubTitles;
          this . SAlign       = SAlign;
          this . iLength      = iLength;
          this . SPitch       = SPitch;
          this . SHead1       = SHead1;
          this . SHead2       = SHead2;
          this . SHead3       = SHead3;
          
          common              = new Common();
            if(iColumns==80)
                   set80Pitch();

               if(iColumns==130)
                   set130Pitch();

               if(iColumns==100)
                   setPitch();
               
          initValues();
          setHeadPrn();
     }

     public PrnGeneration(FileWriter FW,String SHead,String[] SAlign,int[] iLength)
     {
          this . FW           = FW;
          this . SHead        = SHead;
          this . SAlign       = SAlign;
          this . iLength      = iLength;

          common              = new Common();

          initValues();
          setHeadPrn();
     }

     /*--- Generate the Prn Format----*/

     private void initValues()
     {
          iLineCount     = 0;
          iPageNo        = 1;
     }

     private void setHeadPrn()
     {
          try
          {
               if(SHead.length() == 0)
               {
                    FW.write(SPitch+"\n");
                    FW.write("E"+SMainTitle+"F\n\n");
     
                    for(int i=0;i<SSubTitles.length;i++)
                    {
                         FW.write("E"+SSubTitles[i]+"F\n");
                    }
                    FW.write("EPage No : "+(iPageNo++)+"F\n");
                    
                    FW.write("|");
                    for(int i=0;i<iLength.length;i++)
                    {
                         FW.write(common.Replicate("-",iLength[i]));
     
                         if(iLength.length-1 != i)
                         {
                              FW.write("-");
                         }
                    }
                    FW.write("|\n");
     
                    FW.write("|");
                    for(int i=0;i<iLength.length;i++)
                    {
                         FW.write(common.Cad(SHead1[i],iLength[i])+"|");
                    }
                    FW.write("\n");
     
                    FW.write("|");
                    for(int i=0;i<iLength.length;i++)
                    {
                         FW.write(common.Cad(SHead2[i],iLength[i])+"|");
                    }
                    FW.write("\n");
     
                    FW.write("|");
                    for(int i=0;i<iLength.length;i++)
                    {
                         FW.write(common.Cad(SHead3[i],iLength[i])+"|");
                    }
                    FW.write("\n");
     
                    FW.write("|");
                    for(int i=0;i<iLength.length;i++)
                    {
                         FW.write(common.Replicate("-",iLength[i]));
     
                         if(iLength.length-1 != i)
                         {
                              FW.write("-");
                         }
                    }
                    FW.write("|\n");
               }
               else
               {
                    FW.write(SHead);
               }

               iLineCount += 10;
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setBodyPrn(String[] SBody)
     {
          try
          {
               // For Body String

               FW.write("|");
               for(int i=0;i<SBody.length;i++)
               {
                    String SBodyString  = "";

                    if(SAlign[i].equals("L"))
                    {
                       SBodyString    = common.Pad(SBody[i],iLength[i]);
                    }
                    else if(SAlign[i].equals("R"))
                    {
                         SBodyString    = common.Rad(SBody[i],iLength[i]);
                    }
                    else
                    {
                         SBodyString    = common.Cad(SBody[i],iLength[i]);
                    }

                    FW.write(SBodyString+"|");
               }
               FW.write("\n");

               // To Print line For Every Data

               if(iLineCount >= 60)
               {
                    setFootPrn();

                    iLineCount = 0;

                    setHeadPrn();
               }
               else
               {
                    FW.write("|");
                    for(int i=0;i<iLength.length;i++)
                    {
                         FW.write(common.Replicate("-",iLength[i]));
   
                         if(iLength.length-1 != i)
                         {
                              FW.write("-");
                         }
                    }
                    FW.write("|\n");
		     }

               iLineCount += 2;
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setBodyPrn(String[] SBody, int iVectorSize, int iRow)
     {
          try
          {
               // For Body String

             //  FW.write("�");
               for(int i=0;i<SBody.length;i++)
               {
                    String SBodyString  = "";

                    if(SAlign[i].equals("L"))
                    {
                         SBodyString    = common.Pad(SBody[i],iLength[i]);
                    }
                    else if(SAlign[i].equals("R"))
                    {
                         SBodyString    = common.Rad(SBody[i],iLength[i]);
                    }
                    else
                    {
                         SBodyString    = common.Cad(SBody[i],iLength[i]);
                    }

                 //   FW.write(SBodyString+"�");
               
               FW.write("\n");

               // To Print line For Every Data

               if(iLineCount >= 60)
               {
                    setFootPrn();
                    iLineCount = 0;
                    setHeadPrn();
               }
               else
               {
                    if((iRow + 1) != iVectorSize)
                    {
                         //FW.write("-");
                         for(int j=0;j<iLength.length;j++)
                         {
                              FW.write(common.Replicate("-",iLength[j]));
        
                              if(iLength.length-1 != j)
                              {
                                   FW.write("-");
                              }
                         }
                       //  FW.write("-\n");
                    }
               }

               iLineCount += 2;
               }
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
   public void setPageBreak(){
          try{
               FW.write("@"+"\n");
          }
          catch(Exception ex){
              ex.printStackTrace();
          }
      }
     public void setFootPrn()
     {
          try
          {
               FW.write("-");
               for(int i=0;i<iLength.length;i++)
               {
                    FW.write(common.Replicate("-",iLength[i]));

                    if(iLength.length-1 != i)
                    {
                         FW.write("-");
                    }
               }
               FW.write("-\n");
               FW.write("");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setFootPrnWithTotal(String[] STotal)
     {
          try
          {
               FW.write("-");
               for(int i=0;i<STotal.length;i++)
               {
                    String SBodyString  = common.Rad(STotal[i],iLength[i]);

                    FW.write(SBodyString+"-");
               }
               FW.write("\n");

               FW.write("-");
               for(int i=0;i<iLength.length;i++)
               {
                    FW.write(common.Replicate("-",iLength[i]));

                    if(iLength.length-1 != i)
                    {
                         FW.write("-");
                    }
               }
               FW.write("-\n");
               FW.write("-");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
     public void setSignPrn(String sper1,String sper2,String sper3,String sper4)
     {
         String sSignPitch="                             ";
          try
          {
               
              
              for(int i=0;i<6;i++){
                   FW.write("\n");
              }
                    FW.write(SPitch+"\n");
                    FW.write(sper1);    
                    FW.write(sSignPitch);
                    FW.write(sper2);    
                    FW.write(sSignPitch);
                    FW.write(sper3);    
                    FW.write(sSignPitch);
                    FW.write(sper4);    
                    
                    FW.write("\n");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
     
     private void set80Pitch()
     {
         System.out.println("80 Pitch");
         int iPLen=0;
          for(int i=0;i<iLength.length;i++){
              iPLen = iPLen+iLength[i];
          }
          System.out.println("Length"+iPLen);
          if(SHead1.length==0)
               return;
           int iPageLength  = iPLen;

          if(iPageLength<=86)
               SPitch="P\n";
          if(iPageLength>86 && iPageLength<=107)
               SPitch="M\n";
          if(iPageLength>107 && iPageLength<=136)
               SPitch="g\n";
          if(iPageLength>136 && iPageLength<=153)
               SPitch="P\n";
          if(iPageLength>153 && iPageLength<=173)
               SPitch="M\n";
          if(iPageLength>173 && iPageLength<=192)
               SPitch="g\n";

          SPaper="Insert 80 Column Paper";
     }
     private void set130Pitch()
     {
         System.out.println("130 Pitch");
         int iPLen=0;
          for(int i=0;i<iLength.length;i++){
              iPLen = iPLen+iLength[i];
          }
          System.out.println("Length"+iPLen);
          if(SHead1.length==0)
               return;
           int iPageLength  = iPLen;
          //int iPageLength  = ((String)SHead1[0]).length();
          if(iPageLength<=134)
               SPitch="P\n";
          if(iPageLength>134 && iPageLength<=163)
               SPitch="M\n";
          if(iPageLength>163 && iPageLength<=204)
               SPitch="g\n";
          if(iPageLength>204 && iPageLength<=217){
              
               SPitch="P\n";
               System.out.println("Pitch"+SPitch);
          }
          if(iPageLength>217 && iPageLength<=261)
               SPitch="M\n";
          if(iPageLength>261)
               SPitch="g\n";
          SPaper="Insert 130 Column Paper";
    }
    private void setPitch()
    {
        System.out.println("100Pitch");
          if(SHead1.length==0)
               return;

          int iPageLength  = ((String)SHead1[0]).length();
          
          if(iPageLength<=80)
               SPitch="P\n";
          if(iPageLength>80 && iPageLength<=96)
               SPitch="M\n";
          if(iPageLength>96 && iPageLength<=120)
               SPitch="g\n";
          if(iPageLength>120 && iPageLength<=128)
               SPitch="P\n";
          if(iPageLength>128 && iPageLength<=153)
               SPitch="M\n";
          if(iPageLength>153 && iPageLength<=192)
               SPitch="g\n";

          if(iPageLength>163 && iPageLength<=204)
               SPitch="g\n";
          if(iPageLength>204 && iPageLength<=217)
               SPitch="P\n";
          if(iPageLength>217 && iPageLength<=261)
               SPitch="M\n";
          if(iPageLength>261)
               SPitch="g\n";
          if(iPageLength<=192)
               SPaper="Insert 80 Column Paper";
          else
               SPaper="Insert 130 Column Paper";
    }
}
