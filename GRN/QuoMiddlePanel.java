
package GRN;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import javax.swing.border.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class QuoMiddlePanel extends JPanel
{
   JTable         ReportTable;
   Object         RowData[][];
   String         ColumnData[],ColumnType[];
   boolean        Flag;
   JLabel         LBasic,LDiscount,LCenVat,LTax,LSur,LNet;
   JPanel         MiddlePanel,BottomPanel,BotLeftPanel,BotRightPanel;
   JTextField     TPayment,TDelivery;
   JTextField     TFreight;
   Common common = new Common();
   JLayeredPane DeskTop;

   QuoMiddlePanel(JLayeredPane DeskTop,Object RowData[][],String ColumnData[],String ColumnType[])
   {
         this.DeskTop     = DeskTop; 
         this.RowData     = RowData;
         this.ColumnData  = ColumnData;
         this.ColumnType  = ColumnType;
         createComponents();
   }
   public QuoMiddlePanel(JLayeredPane DeskTop,String ColumnData[],String ColumnType[])
   {
         this.DeskTop     = DeskTop;
         this.ColumnData  = ColumnData;
         this.ColumnType  = ColumnType;
         setLayout(new BorderLayout());
   }
   public void createComponents()
   {
         LBasic           = new JLabel("0");
         LDiscount        = new JLabel("0");
         LCenVat          = new JLabel("0");
         LTax             = new JLabel("0");
         LSur             = new JLabel("0");
         LNet             = new JLabel("0");

         TPayment         = new JTextField();
         TDelivery        = new JTextField();
         TFreight         = new JTextField();

         BottomPanel      = new JPanel();
         BotLeftPanel     = new JPanel();
         BotRightPanel    = new JPanel();
         MiddlePanel      = new JPanel();

         BotLeftPanel.setLayout(new GridLayout(2,6));
         BotRightPanel.setLayout(new GridLayout(3,2));
         BottomPanel.setLayout(new GridLayout(1,2));

         MiddlePanel.setLayout(new BorderLayout());

         BotLeftPanel.add(new JLabel("Basic"));
         BotLeftPanel.add(new JLabel("Discount"));
         BotLeftPanel.add(new JLabel("CenVat"));
         BotLeftPanel.add(new JLabel("Tax"));
         BotLeftPanel.add(new JLabel("Surcharge"));
         BotLeftPanel.add(new JLabel("Net"));
         BotLeftPanel.add(LBasic);
         BotLeftPanel.add(LDiscount);
         BotLeftPanel.add(LCenVat);
         BotLeftPanel.add(LTax);
         BotLeftPanel.add(LSur);
         BotLeftPanel.add(LNet);

         BotRightPanel.add(new JLabel("Payment Terms"));
         BotRightPanel.add(TPayment);
         BotRightPanel.add(new JLabel("Delivery"));
         BotRightPanel.add(TDelivery);
         BotRightPanel.add(new JLabel("Freight"));
         BotRightPanel.add(TFreight);

         BottomPanel.add(BotLeftPanel);
         BottomPanel.add(BotRightPanel);

         BotLeftPanel.setBorder(new TitledBorder("Summary Total"));
         BotRightPanel.setBorder(new TitledBorder("Other Details"));

         for(int i=0;i<RowData.length;i++)
            setMaterialAmount(i);
         setReportTable();
   }
   public void setRowData(Vector VSelectedCode,Vector VSelectedName)
   {
          RowData = new Object[VSelectedCode.size()][ColumnData.length];
          for(int i=0;i<VSelectedCode.size();i++)
          {
               RowData[i][0] = (String)VSelectedCode.elementAt(i);
               RowData[i][1] = (String)VSelectedName.elementAt(i);
               for(int j=2;j<ColumnData.length;j++)
                    RowData[i][j] = "";
          }
   }
   public void setReportTable()
   {
         AbstractTableModel dataModel = new AbstractTableModel()
         {
               public int getColumnCount(){return ColumnData.length;}
               public int getRowCount(){return RowData.length;}
               public Object getValueAt(int row,int col){return RowData[row][col];}
               public String getColumnName(int col){ return ColumnData[col];}
               public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }
               public boolean isCellEditable(int row,int col)
               {
                    if(ColumnType[col]=="B" || ColumnType[col] == "E")
                         return true;
                    return false;
               }
               public void setValueAt(Object element,int row,int col)
               {
                    RowData[row][col]=element;
                    setMaterialAmount(row);
               }
         };
         ReportTable  = new JTable(dataModel);
         ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<ReportTable.getColumnCount();col++)
         {
               if(ColumnType[col]=="N" || ColumnType[col]=="E")
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
         }
         ReportTable.setShowGrid(false);
         setLayout(new BorderLayout());
         MiddlePanel.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
         MiddlePanel.add(new JScrollPane(ReportTable),BorderLayout.CENTER);
         add(BottomPanel,BorderLayout.SOUTH);
         add(MiddlePanel,BorderLayout.CENTER);
         ReportTable.addMouseListener(new MouseList());
   }
     public class MouseList extends MouseAdapter
     {
               public void mouseClicked(MouseEvent me)
               {
                    if (me.getClickCount()==2)
                    {
                         RatePicker RP = new RatePicker(DeskTop,ReportTable);
                         try
                         {
                              DeskTop.add(RP);
                              DeskTop.repaint();
                              RP.setSelected(true);
                              DeskTop.updateUI();
                              RP.show();
                         }
                         catch(java.beans.PropertyVetoException ex){}
                    }
               }
     }

      public void setMaterialAmount(int i)
      {
              double dTGross=0,dTDisc=0,dTCenVat=0,dTTax=0,dTSur=0,dTNet=0;      
              double dQty        = common.toDouble((String)RowData[i][4]);
              double dRate       = common.toDouble((String)RowData[i][5]);
              double dDiscPer    = common.toDouble((String)RowData[i][6]);
              double dCenVatPer  = common.toDouble((String)RowData[i][7]);
              double dTaxPer     = common.toDouble((String)RowData[i][8]);
              double dSurPer     = common.toDouble((String)RowData[i][9]);

              double dGross  = dQty*dRate;
              double dDisc   = dGross*dDiscPer/100;
              double dCenVat = (dGross-dDisc)*dCenVatPer/100;
              double dTax    = (dGross-dDisc+dCenVat)*dTaxPer/100;
              double dSur    = (dTax)*dSurPer/100;
              double dNet    = dGross-dDisc+dCenVat+dTax+dSur;

              RowData[i][10] =common.getRound(dGross,2);
              RowData[i][11] =common.getRound(dDisc,2);
              RowData[i][12]=common.getRound(dCenVat,2);
              RowData[i][13]=common.getRound(dTax,2);
              RowData[i][14]=common.getRound(dSur,2);
              RowData[i][15]=common.getRound(dNet,2);

              for(int index=0;index<RowData.length;index++)
              {
                  dTGross   = dTGross+common.toDouble((String)RowData[index][10]);
                  dTDisc    = dTDisc+common.toDouble((String)RowData[index][11]);
                  dTCenVat  = dTCenVat+common.toDouble((String)RowData[index][12]);
                  dTTax     = dTTax+common.toDouble((String)RowData[index][13]);
                  dTSur     = dTSur+common.toDouble((String)RowData[index][14]);
                  dTNet     = dTNet+common.toDouble((String)RowData[index][15]);
              }
              LBasic.setText(common.getRound(dTGross,2));
              LDiscount.setText(common.getRound(dTDisc,2));
              LCenVat.setText(common.getRound(dTCenVat,2));
              LTax.setText(common.getRound(dTTax,2));
              LSur.setText(common.getRound(dTSur,2));
              LNet.setText(common.getRound(dTNet,2));
      }
}

