package GRN;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class QuotationFrame extends JInternalFrame
{
     JButton    BApply,BSupplier,BOk;
     JPanel     TopPanel,BottomPanel;
     JTextField TSupCode;

     Common common   = new Common();

     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;

     QuoMiddlePanel MiddlePanel;

     Object RowData[][],IdData[];

     String ColumnData[] = {"Enquiry No","Date","Code","Name","Qty","Rate","Discount(%)","CenVat(%)","Tax(%)","Surcharge(%)","Basic","Discount (Rs)","CenVat(Rs)","Tax(Rs)","Surcharge(Rs)","Net (Rs)","Reference","CenVat"};
     String ColumnType[] = {"N","S","S","S","N","E","E","E","E","E","N","N","N","N","N","N","E","B"};

     Vector VEnqNo,VEnqDate,VEnqCode,VEnqName,VEnqQty,VEnqId;
     Vector VRate,VDiscPer,VCenvatPer,VTaxPer,VSurPer;

     QuotationFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel)
     {
         super("Quotation Against Pending Enquiries");
         this.DeskTop = DeskTop;
         this.VCode   = VCode;
         this.VName   = VName;
         this.SPanel  = SPanel;
         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     public void createComponents()
     {
         BSupplier   = new JButton("Supplier");
         BApply      = new JButton("Apply");
         BOk         = new JButton("Update Quotation");
         TopPanel    = new JPanel();
         BottomPanel = new JPanel();
         TSupCode    = new JTextField();

         VEnqNo     = new Vector();
         VEnqDate   = new Vector();
         VEnqCode   = new Vector();
         VEnqName   = new Vector();
         VEnqQty    = new Vector();
         VEnqId     = new Vector();
         VRate      = new Vector();
         VDiscPer   = new Vector();
         VCenvatPer = new Vector();
         VTaxPer    = new Vector();
         VSurPer    = new Vector();
     }

     public void setLayouts()
     {
         TopPanel.setLayout(new GridLayout(1,3));
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,650,500);
     }

     public void addComponents()
     {
         TopPanel.add(new JLabel("Select a Supplier",JLabel.RIGHT)); 
         TopPanel.add(BSupplier);
         TopPanel.add(BApply);

         BottomPanel.add(BOk);

         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);

     }
     public void addListeners()
     {
         //BSupplier.addActionListener(new SupplierSearch(DeskTop,TSupCode));
         BApply.addActionListener(new ApplyList());
         BOk.addActionListener(new ActList());
     }
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                  MiddlePanel = new QuoMiddlePanel(DeskTop,RowData,ColumnData,ColumnType);
                  MiddlePanel.updateUI();
                  getContentPane().add(MiddlePanel,BorderLayout.CENTER);
                  setSelected(true);
                  DeskTop.repaint();
                  DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
                  try
                  {
                        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(common.getPrintPath()+"Raj.prn")));
                        ex.printStackTrace(out);
                        out.close();
                  }
                  catch(Exception e)
                  {                      
                  }
               }
          }
     }
     public void setDataIntoVector()
     {

           String QString = " SELECT [Enquiry].[EnqNo], [Enquiry].[EnqDate],[Enquiry].[Item_Code], [InvItems].[Item_Name], [Enquiry].[Qty],[Enquiry].[Id],Enquiry.Rate,Enquiry.DiscPer,Enquiry.CenvatPer,Enquiry.TaxPer,Enquiry.SurPer "+
                            " FROM Enquiry  "+
                            " INNER JOIN InvItems ON [Enquiry].[Item_Code]=[InvItems].[Item_Code] "+
                            " Where Enquiry.Quotation=0 and Enquiry.Sup_Code ='"+TSupCode.getText()+"' Order By 6";                               
           try
           {

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();

              ResultSet res  = stat.executeQuery(QString);
              while (res.next())
              {
                 String str1  = common.parseNull(res.getString(1));  
                 String str2  = common.parseDate(res.getString(2));
                 String str3  = common.parseNull(res.getString(3));
                 String str4  = common.parseNull(res.getString(4));
                 String str5  = common.parseNull(res.getString(5));
                 String str6  = common.parseNull(res.getString(6));
                 String str7  = common.parseNull(res.getString(7));
                 String str8  = common.parseNull(res.getString(8));
                 String str9  = common.parseNull(res.getString(9));
                 String str10 = common.parseNull(res.getString(10));
                 String str11 = common.parseNull(res.getString(11));

                 VEnqNo.addElement(str1);
                 VEnqDate.addElement(str2);
                 VEnqCode.addElement(str3);
                 VEnqName.addElement(str4);
                 VEnqQty.addElement(str5);
                 VEnqId.addElement(str6);
                 VRate.addElement(parseZero(str7));
                 VDiscPer.addElement(parseZero(str8));
                 VCenvatPer.addElement(parseZero(str9));
                 VTaxPer.addElement(parseZero(str10));
                 VSurPer.addElement(parseZero(str11));
              }
           }
           catch(Exception ex){System.out.println(ex);}
     }
     public void setRowData()
     {
         RowData     = new Object[VEnqDate.size()][18];
         IdData      = new Object[VEnqDate.size()];
         for(int i=0;i<VEnqDate.size();i++)
         {
            
                 RowData[i][0]  = (String)VEnqNo.elementAt(i);
                 RowData[i][1]  = (String)VEnqDate.elementAt(i);
                 RowData[i][2]  = (String)VEnqCode.elementAt(i);
                 RowData[i][3]  = (String)VEnqName.elementAt(i);
                 RowData[i][4]  = (String)VEnqQty.elementAt(i);
                 RowData[i][5]  = (String)VRate.elementAt(i);      //Rate
                 RowData[i][6]  = (String)VDiscPer.elementAt(i);   //DiscPer
                 RowData[i][7]  = (String)VCenvatPer.elementAt(i); //CenVatPer
                 RowData[i][8]  = (String)VTaxPer.elementAt(i);    //TaxPer
                 RowData[i][9]  = (String)VSurPer.elementAt(i);    //SurPer
                 RowData[i][10] = "0"; //Basic
                 RowData[i][11] = "0"; //Disc
                 RowData[i][12] = "0"; //Cenvat
                 RowData[i][13] = "0"; //Tax
                 RowData[i][14] = "0"; //Sur
                 RowData[i][15] = "0"; //Net
                 RowData[i][16] = " ";
                 RowData[i][17] = new Boolean(true);
                 IdData[i]      = (String)VEnqId.elementAt(i);
         }  
     }
     public class ActList implements ActionListener
     {
            public void actionPerformed(ActionEvent ae)
            {
                BOk.setEnabled(false);
                updateQuotationDetails();
                removeHelpFrame();
            }
     }
     public void updateQuotationDetails()
     {
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();

               for(int i=0;i<RowData.length;i++)
               {
                    
                   Boolean Bselected = (Boolean)RowData[i][17];
                   int    iCenVat  = (Bselected.booleanValue()?0:2);
                   double dNet     = common.toDouble((String)RowData[i][15]);
                   double dQty     = common.toDouble((String)RowData[i][4]);
                   String SNetRate = "0";
                   try
                   {
                         SNetRate = common.getRound(dNet/dQty,4);
                   }
                   catch(Exception ex){}
                   String QString = "Update Enquiry Set ";
                   QString = QString+" Rate       = 0"+((String)RowData[i][5]).trim()+",";
                   QString = QString+" DiscPer    = 0"+((String)RowData[i][6]).trim()+",";
                   QString = QString+" CenVatPer  = 0"+((String)RowData[i][7]).trim()+",";
                   QString = QString+" TaxPer     = 0"+((String)RowData[i][8]).trim()+",";
                   QString = QString+" SurPer     = 0"+((String)RowData[i][9]).trim()+",";
                   QString = QString+" NetRate    = 0"+SNetRate+"," ;
                   QString = QString+" Reference  = '"+((String)RowData[i][16]).trim()+"',";
                   QString = QString+" Quotation  = 1,";
                   QString = QString+" CenVatApplicable = "+iCenVat+",";
                   QString = QString+" Terms            = '"+MiddlePanel.TPayment.getText()+"',";
                   QString = QString+" Delivery         = '"+MiddlePanel.TDelivery.getText()+"',";
                   QString = QString+" Freight          = '"+MiddlePanel.TFreight.getText()+"' ";
                   QString = QString+" Where id = "+(String)IdData[i];
                   stat.execute(QString);
               }
          }
          catch(Exception ex)
          {
                System.out.println(ex);
          }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
     public String parseZero(String str)
     {
          if(common.toDouble(str)==0)
               return "";
          return str;
     }
}
