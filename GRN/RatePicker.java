package GRN;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RatePicker extends JInternalFrame
{
      JButton        BOk,BCancel;
                  
      JPanel         TopPanel,FigurePanel,BottomPanel;

      JLayeredPane   Layer;
      JTable         ReportTable;

      JComboBox      JCType;

      String str="";

      JTextField TQty    ;
      JTextField TRate   ;
      JTextField TDisPer ;
      JTextField TVatPer ;
      JTextField TTaxPer ;
      JTextField TSurPer ;

      JTextField TNet   ;
      JTextField TBasic ;
      JTextField TDisc  ;
      JTextField TVat   ;
      JTextField TTax   ;
      JTextField TSur   ;

      Common common = new Common();

      RatePicker(JLayeredPane Layer,JTable ReportTable)
      {
          this.Layer       = Layer;
          this.ReportTable = ReportTable;

          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
      }
      public void createComponents()
      {
          BOk           = new JButton("Okay");
          BCancel       = new JButton("Cancel");

          TopPanel      = new JPanel(true);                         
          FigurePanel   = new JPanel(true);
          BottomPanel   = new JPanel(true);

          JCType        = new JComboBox();

          TQty    = new JTextField();
          TRate   = new JTextField();
          TDisPer = new JTextField();
          TVatPer = new JTextField();
          TTaxPer = new JTextField();
          TSurPer = new JTextField();

          TNet   = new JTextField();
          TBasic = new JTextField();
          TDisc  = new JTextField();
          TVat   = new JTextField();
          TTax   = new JTextField();
          TSur   = new JTextField();

          JCType.addItem("Basic");
          JCType.addItem("Net");

      }
      public void setLayouts()
      {
          setBounds(80,100,550,350);
          setClosable(true);
          setTitle("Net Rate Calculation");
          TopPanel.setLayout(new BorderLayout());

      }
      public void addComponents()
      {

          getContentPane().add("North",JCType);               
          getContentPane().add("Center",TopPanel);
          getContentPane().add("South",BottomPanel);

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          TopPanel.add("Center",FigurePanel);

      }
      public void setPresets()
      {
          int i = ReportTable.getSelectedRow();

          String SQty    = (String)ReportTable.getModel().getValueAt(i,4);
          String SRate   = (String)ReportTable.getModel().getValueAt(i,5);
          String SDisPer = (String)ReportTable.getModel().getValueAt(i,6);
          String SVatPer = (String)ReportTable.getModel().getValueAt(i,7);
          String STaxPer = (String)ReportTable.getModel().getValueAt(i,8);
          String SSurPer = (String)ReportTable.getModel().getValueAt(i,9);
          String SNet    = (String)ReportTable.getModel().getValueAt(i,15);

          TQty.setText(SQty);
          TRate.setText(SRate);
          TDisPer.setText(SDisPer);
          TVatPer.setText(SVatPer);
          TTaxPer.setText(STaxPer);
          TSurPer.setText(SSurPer);
          TNet   .setText(SNet);
          FigurePanel.removeAll();
          FigurePanel.setLayout(new BorderLayout());
          FigurePanel.add("Center",getNetPane());
          setCalculation();
      }
      public void addListeners()
      {
          BOk.addActionListener(new ActList());
          JCType.addItemListener(new ItList());
      }
      public class ItList implements ItemListener
      {
          public void itemStateChanged(ItemEvent ie)
          {
               FigurePanel.removeAll();
               FigurePanel.setLayout(new BorderLayout());
               FigurePanel.add("Center",getNetPane());
               updateUI();
          }
     }
     public JScrollPane getNetPane()
     {
          JPanel thePanel    = new JPanel();

          thePanel.setLayout(new GridLayout(5,4));

          thePanel.add(new JLabel("Qty"));
          thePanel.add(TQty);
          thePanel.add(new JLabel("Net Value"));
          thePanel.add(TNet);

          thePanel.add(new JLabel("Surcharge (%)"));
          thePanel.add(TSurPer);
          thePanel.add(new JLabel("Surcharge (Rs)"));
          thePanel.add(TSur);
          TSur.setEditable(false);

          thePanel.add(new JLabel("Tax (%)"));
          thePanel.add(TTaxPer);
          thePanel.add(new JLabel("Tax (Rs)"));
          thePanel.add(TTax);
          TTax.setEditable(false);

          thePanel.add(new JLabel("Vat (%)"));
          thePanel.add(TVatPer);
          thePanel.add(new JLabel("Vat (Rs)"));
          thePanel.add(TVat);
          TVat.setEditable(false);

          thePanel.add(new JLabel("Disc (%)"));
          thePanel.add(TDisPer);
          thePanel.add(new JLabel("Disc (Rs)"));
          thePanel.add(TDisc);
          TDisc.setEditable(false);

          thePanel.add(new JLabel("Basic Rate"));
          thePanel.add(TRate);
          thePanel.add(new JLabel("Basic Value"));
          thePanel.add(TBasic);
          TRate.setEditable(false);
          TBasic.setEditable(false);

          TNet.addKeyListener(new CalcList());
          TQty.addKeyListener(new CalcList());
          TSurPer.addKeyListener(new CalcList());
          TTaxPer.addKeyListener(new CalcList());
          TVatPer.addKeyListener(new CalcList());
          TDisPer.addKeyListener(new CalcList());

          return new JScrollPane(thePanel);
     }
     public class CalcList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               setCalculation();
          }
     }
     public void setCalculation()
     {
               double Q      = common.toDouble(TQty.getText());
               double N      = common.toDouble(TNet.getText());
               double D      = common.toDouble(TDisPer.getText());
               double C      = common.toDouble(TVatPer.getText());
               double S      = common.toDouble(TTaxPer.getText());
               double U      = common.toDouble(TSurPer.getText());

               double B      = 0;
               try
               {
                    B=(((N)/(1+(S/100)*(U/100+1)))/(1+C/100))/(1-D/100);
               }
               catch(Exception ex)
               {
                    B = 0;
               }
               double DV = B*D/100;
               double CV = (B-DV)*(C/100);
               double SV = (B-DV+CV)*(S/100);
               double UV = SV*U/100;

               TBasic.setText(common.getRound(B,2));
               TDisc.setText(common.getRound(DV,2));
               TVat.setText(common.getRound(CV,2));
               TTax.setText(common.getRound(SV,2));
               TSur.setText(common.getRound(UV,2));
               try
               {
                    TRate.setText(common.getRound(B/Q,3));
               }
               catch(Exception ex)
               {

               }
      }
      public class ActList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    int i = ReportTable.getSelectedRow();
                    ReportTable.getModel().setValueAt(TQty.getText(),i,4);
                    ReportTable.getModel().setValueAt(TRate.getText(),i,5);
                    ReportTable.getModel().setValueAt(TDisPer.getText(),i,6);
                    ReportTable.getModel().setValueAt(TVatPer.getText(),i,7);
                    ReportTable.getModel().setValueAt(TTaxPer.getText(),i,8);
                    ReportTable.getModel().setValueAt(TSurPer.getText(),i,9);
               }
               removeHelpFrame();
          }
      }
      public void removeHelpFrame()
      {
            try
            {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
            }
            catch(Exception ex) { }
      }
}
