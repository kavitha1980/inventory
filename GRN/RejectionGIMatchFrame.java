package GRN;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RejectionGIMatchFrame extends JInternalFrame
{
      JLayeredPane Layer;

      JPanel         TopPanel,BottomPanel,MiddlePanel,TopLeft,TopRight;
      MyButton       BApply,BOk,BCancel;
      TabReport      theReport;
      JTabbedPane    thePane;
      DateField      TDate;

      Vector VGrnNo,VGrnBlock,VGrnDate,VSupCode,VSupName,VItemCode,VItemName,VRejQty,VGrnSlNo,VGrnId,VGIDate;
      Vector VRdcId;

      Object RowData[][];

      Common common   = new Common();
      int iUserCode,iMillCode;
      String SSupTable;
      ORAConnection connect;
      Connection theconnect;

      PendingGIMaterialFrame pendinggimaterialframe;

      public RejectionGIMatchFrame(JLayeredPane Layer,int iUserCode,int iMillCode,String SSupTable)
      {
            this.Layer     = Layer;
            this.iUserCode = iUserCode;
            this.iMillCode = iMillCode;
            this.SSupTable = SSupTable;

            createComponents();
            setLayouts();
            addComponents();
            addListeners();
      }
      public void createComponents()
      {
            try
            {
                 thePane     = new JTabbedPane();
                 TDate       = new DateField();

                 TDate.setTodayDate();
                 TDate.setEditable(false);

                 TopPanel    = new JPanel(true);
                 TopLeft     = new JPanel(true);
                 TopRight    = new JPanel(true);
                 BottomPanel = new JPanel(true);
                 MiddlePanel = new JPanel(true);

                 BApply      = new MyButton("Apply");
                 BOk         = new MyButton("Save");
                 BCancel     = new MyButton("Cancel");

                 BOk.setEnabled(false);
            }catch(Exception ex)
            {
                ex.printStackTrace();
            }
      }
      public void setLayouts()
      {
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,800,500);
            setTitle("Rejection and GI Matching Frame");

            TopPanel.setLayout(new GridLayout(1,2));
            TopLeft.setLayout(new GridLayout(1,2));
            TopLeft.setBorder(new TitledBorder("DateInfo"));
            TopRight.setLayout(new GridLayout(1,1));
            TopRight.setBorder(new TitledBorder("Apply"));
            MiddlePanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());
            TopPanel.setBorder(new TitledBorder("Info"));
      }
      public void addComponents()
      {
            TopLeft.add(new MyLabel("As On"));
            TopLeft.add(TDate);

            TopRight.add(BApply);

            TopPanel.add(TopLeft);
            TopPanel.add(TopRight);

            BottomPanel.add(BOk);
            BottomPanel.add(BCancel);

            getContentPane().add("North",TopPanel);
            getContentPane().add("Center",MiddlePanel);
            getContentPane().add("South",BottomPanel);
     }
     public void addListeners()
     {
          BOk         .addActionListener(new ActList());
          BCancel     .addActionListener(new ActList());
          BApply      .addActionListener(new ActList());
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
                    BApply.setEnabled(false);
                    String SDate  = TDate.toNormal();
                    showList(SDate);
                    setTabReport();

                    if(VGrnNo.size()>0)
                         BOk.setEnabled(true);
               }
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
               }
               if(ae.getSource()==BOk)
               {
                    if(isValidData())
                    {
                         BOk.setEnabled(false);
                         saveData();
                         removeHelpFrame();
                    }
               }

          }
     }
     private boolean isValidData()
     {
          int iCount=0;

          for(int i=0;i<RowData.length;i++)
          {
               Boolean BValue = (Boolean)RowData[i][10];

               if(BValue.booleanValue())
               {
                    iCount++;
               }
          }

          if(iCount<=0)
          {
               JOptionPane.showMessageDialog(null,"No Row Selected","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          return true;
     }

     private void saveData()
     {
          try
          {
               String SDateTime = common.getServerDateTime2();

               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat       = theconnect.createStatement();

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean BValue = (Boolean)RowData[i][10];
     
                    if(!BValue.booleanValue())
                         continue;

                    String SRdcId = (String)VRdcId.elementAt(i);
                    String SGrnId = (String)VGrnId.elementAt(i);

                    String QS1 = " Update GateInward Set RdcMatchStatus=1,RdcId="+SRdcId+",RdcMatchUser="+iUserCode+",RdcMatchTime='"+SDateTime+"' Where Id="+SGrnId;
                    String QS2 = " Update RDC Set GIMatchStatus=1,GiId="+SGrnId+" Where Id="+SRdcId;


                    stat.execute(QS1);
                    stat.execute(QS2);
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void setTabReport()
     {
          try
          {
               MiddlePanel.removeAll();
               thePane.removeAll();

               String ColumnName[]  = {"SlNo","GINo","GI Date","Supplier","Code","Name","Qty","Rdc No","Rdc Date","Descript","Click"};
               String ColumnType[]  = {"N"   ,"N"    ,"S"       ,"S"       ,"S"   ,"S"   ,"N"  ,"N"     ,"S"       ,"S"       ,"S"    };
               int    ColumnWidth[] = {50    ,50     ,75        ,100       ,75    ,100   ,75   ,60      ,75        ,100       ,50     };

               RowData = new Object[VGrnNo.size()][ColumnName.length];

               if(VGrnNo.size()>0)
               {
                    for(int i=0;i<VGrnNo.size();i++)
                    {
                         RowData[i][0]   = String.valueOf(i+1);
                         RowData[i][1]   = common.parseNull((String)VGrnNo         .elementAt(i));
                         RowData[i][2]   = common.parseDate((String)VGrnDate       .elementAt(i));
                         RowData[i][3]   = common.parseNull((String)VSupName       .elementAt(i));
                         RowData[i][4]   = common.parseNull((String)VItemCode      .elementAt(i));
                         RowData[i][5]   = common.parseNull((String)VItemName      .elementAt(i));
                         RowData[i][6]   = common.parseNull((String)VRejQty        .elementAt(i));
                         RowData[i][7]   = "";
                         RowData[i][8]   = "";
                         RowData[i][9]   = "";
                         RowData[i][10]  = new Boolean(false);
                    }
                    theReport      = new TabReport(RowData,ColumnName,ColumnType);
                    theReport.setPrefferedColumnWidth(ColumnWidth);

                    MiddlePanel.add("Center",thePane);
                    thePane.addTab("Pending Rejection List",theReport);
                    theReport.ReportTable.addKeyListener(new KeyList());
               }
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }

     private class KeyList extends KeyAdapter
     {
         public void keyPressed(KeyEvent ke)
         {
            if(ke.getKeyCode()==KeyEvent.VK_INSERT)
            {
                showRDCSelectionFrame();
            }
         }
     }

     private void showRDCSelectionFrame()
     {
          try
          {
               Layer.remove(pendinggimaterialframe);
               Layer.updateUI();
          }
          catch(Exception ex){}

          try
          {
               int iRow     = theReport.ReportTable.getSelectedRow();

               Boolean bValue = (Boolean)theReport.ReportTable.getValueAt(iRow,10);

               if(!bValue.booleanValue())
               {
                   String SGrnNo    = (String)theReport.ReportTable.getValueAt(iRow,1);
                   String SGrnDate  = common.pureDate((String)theReport.ReportTable.getValueAt(iRow,2));
                   String SSupName  = (String)theReport.ReportTable.getValueAt(iRow,3);
                   String SItemName = (String)theReport.ReportTable.getValueAt(iRow,5);
                   String SRejQty   = (String)theReport.ReportTable.getValueAt(iRow,6);

                   String SSupCode  = (String)VSupCode.elementAt(iRow);
    
                   pendinggimaterialframe = new PendingGIMaterialFrame(Layer,iMillCode,SGrnNo,SGrnDate,SSupName,SItemName,SRejQty,SSupCode,theReport,VRdcId);
                   Layer.add(pendinggimaterialframe);
                   pendinggimaterialframe.show();
                   pendinggimaterialframe.moveToFront();
                   Layer.repaint();
                   Layer.updateUI();
               }
               else
               {
                   JOptionPane.showMessageDialog(null,"This Row Already Processed","Error",JOptionPane.ERROR_MESSAGE);
               }

          }
          catch(Exception ex){}
     }

     private void showList(String SDate)
     {
          VGrnNo         = new Vector();
          VGrnDate       = new Vector();
          VSupName       = new Vector();
          VItemCode      = new Vector();
          VItemName      = new Vector();
          VRejQty        = new Vector();
          VSupCode       = new Vector();
          VRdcId         = new Vector();
          VGrnId         = new Vector();
                             
          String QS =    " Select GiNo,GIDate,Name,Item_Code,Item_Name,GateQty,Sup_Code,Id from "+
                         " (Select GateInward.GiNo,GateInward.GIDate,"+SSupTable+".Name,GateInward.Item_Code,InvItems.Item_Name,GateInward.GateQty, "+
                         " GateInward.Sup_Code,GateInward.Id from GateInward "+
                         " Inner join "+SSupTable+" on "+SSupTable+".Ac_Code=GateInward.Sup_Code "+
                         " Inner join InvItems on InvItems.Item_Code=GateInward.Item_Code "+
                         " Where GateInward.GiDate<="+SDate+"  and ModiStatus=0 and Authentication=1 and InwNo=9 and  RDCMatchStatus=0 "+
                         " and GateInward.MillCode="+iMillCode+" and GateInward.Item_Code is Not Null "+
                         " Union All "+
                         " Select GateInward.GiNo,GateInward.GIDate,"+SSupTable+".Name,GateInward.Item_Code,GateInward.Item_Name,GateInward.GateQty, "+
                         " GateInward.Sup_Code,GateInward.Id from GateInward "+
                         " Inner join "+SSupTable+" on "+SSupTable+".Ac_Code=GateInward.Sup_Code "+
                         " Where GateInward.GiDate<="+SDate+"  and ModiStatus=0 and Authentication=1 and InwNo=9 and  RDCMatchStatus=0 "+
                         " and GateInward.MillCode="+iMillCode+" and GateInward.Item_Code is Null) "+
                         " Order By Name,GINo,Item_Name ";


          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat       = theconnect.createStatement();
               ResultSet theResult  = stat.executeQuery(QS);

               while(theResult.next())
               {
                    VGrnNo      .addElement(theResult.getString(1));
                    VGrnDate    .addElement(theResult.getString(2));
                    VSupName    .addElement(theResult.getString(3));
                    VItemCode   .addElement(theResult.getString(4));
                    VItemName   .addElement(theResult.getString(5));
                    VRejQty     .addElement(theResult.getString(6));
                    VSupCode    .addElement(theResult.getString(7));
                    VGrnId      .addElement(theResult.getString(8));   
                    VRdcId      .addElement("");

               }
               theResult.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
}
