package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RejectionReasonFrame extends JInternalFrame
{
     JLayeredPane Layer;
     InsTabReport tabreportx;
     JDialog ModiDialog;

     JButton BOk;
     JTextField TMatCode,TMatName,TRecQty,TRejQty,TReason;
     WholeNumberField TDCNo;
     DateField TDCDate;

     JPanel TopPanel,BottomPanel,ModiPanel;

     int index=0;

     Common common = new Common();

     RejectionReasonFrame(JLayeredPane Layer,InsTabReport tabreportx,JDialog ModiDialog)
     {
          this.Layer      = Layer;
          this.tabreportx = tabreportx;
          this.ModiDialog  = ModiDialog;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
         BOk         = new JButton("Okay");
         TMatCode    = new JTextField();
         TMatName    = new JTextField();
         TRecQty     = new JTextField();
         TRejQty     = new JTextField();
         TReason     = new JTextField();
         TDCNo       = new WholeNumberField();
         TDCDate     = new DateField();
         TopPanel    = new JPanel(true);
         BottomPanel = new JPanel(true);
         ModiPanel   = new JPanel(true);

         index   = tabreportx.ReportTable.getSelectedRow();
         String SMatCode = (String)tabreportx.ReportTable.getValueAt(index,4);
         String SMatName = (String)tabreportx.ReportTable.getValueAt(index,5);
         String SRecQty  = (String)tabreportx.ReportTable.getValueAt(index,10);
         String SRejQty  = (String)tabreportx.ReportTable.getValueAt(index,12);
         String SReason  = (String)tabreportx.ReportTable.getValueAt(index,14);
         String SDCNo    = (String)tabreportx.ReportTable.getValueAt(index,15);
         String SDCDate  = (String)tabreportx.ReportTable.getValueAt(index,16);

         TMatCode.setText(SMatCode);
         TMatName.setText(SMatName);
         TRecQty.setText(SRecQty);
         TRejQty.setText(SRejQty);
         TMatCode.setEditable(false);
         TMatName.setEditable(false);
         TRecQty.setEditable(false);
         TRejQty.setEditable(false);

         TReason.setText(SReason);
         TDCNo.setText(SDCNo);
         TDCDate.fromString(SDCDate);
     }

     private void setLayouts()
     {
         TopPanel.setLayout(new GridLayout(7,2));
         ModiPanel.setLayout(new BorderLayout());
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,350,300);
     }
     private void addComponents()
     {
         TopPanel.add(new JLabel("Material Code"));
         TopPanel.add(TMatCode);
         TopPanel.add(new JLabel("Material Name"));
         TopPanel.add(TMatName);
         TopPanel.add(new JLabel("Received Qty"));
         TopPanel.add(TRecQty);
         TopPanel.add(new JLabel("Rejected Qty"));
         TopPanel.add(TRejQty);
         TopPanel.add(new JLabel("Reason for Rejection"));
         TopPanel.add(TReason);
         TopPanel.add(new JLabel("D.C. No"));
         TopPanel.add(TDCNo);
         TopPanel.add(new JLabel("D.C. Date"));
         TopPanel.add(TDCDate);
         BottomPanel.add(BOk);

         getContentPane().add("North",TopPanel);
         getContentPane().add("South",BOk);

         ModiPanel.add("North",TopPanel);
         ModiPanel.add("South",BOk);
     }
                    
     private void addListeners()
     {
          BOk.addActionListener(new ActList());
     }
     
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validData())
               {
                    BOk.setEnabled(false);
                    String SReason = TReason.getText().toUpperCase();
                    String SDCNo   = TDCNo.getText();
                    String SDCDate = TDCDate.toString();
     
                    tabreportx.ReportTable.setValueAt(SReason,index,14);
                    tabreportx.ReportTable.setValueAt(SDCNo,index,15);
                    tabreportx.ReportTable.setValueAt(SDCDate,index,16);
                    removeHelpFrame();
                    tabreportx.ReportTable.requestFocus();
               }
               else
               {
                    BOk.setEnabled(true);
               }
          }
     }

     private boolean validData()
     {
          BOk.setEnabled(false);
          boolean bflag=false;

          String SReason = (TReason.getText()).trim();
          int iDCNo      = common.toInt((TDCNo.getText()).trim());
          String SDCDate = TDCDate.toString();

          if(SReason.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Reason Field is Empty","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          if(iDCNo==0)
          {
               JOptionPane.showMessageDialog(null,"D.C.No Field is Empty","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          if(SDCDate.length()!=10)
          {
               JOptionPane.showMessageDialog(null,"Invalid D.C. Date","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          return true;
     }


     private void removeHelpFrame()
     {
          try{ModiDialog.setVisible(false);}catch(Exception e){System.out.println(e);}
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }

}
