package GRN;
import javax.swing.*;
import java.sql.*;
import java.util.*;
import util.*;

public class SPJAuto 
{

	int iUserCode     = 0;
    int iDivisonCode = 0;
	
	String SYearCode  = "0";
	String SGrnNo     = "";
	
	Common     common = new Common();
	
	
	String SpjNo   = "0";
	String SpjDate = "";
	
  	double dAmount = 0, dCenVat = 0;
  	String Sup_Code = "", SInvNo = "", SInvDate = "", SGINo = "", SGIDate = "", SGrnDet = "", SGrnDate = "",STaxPer = "", SNarr = "", SGrnStatus = ""; 

     /* CtCode to be changed According to HSN Code....*/

  	String SCtCode = "A_1216";
	
    Vector     VGrn,VGrnBlock,VGrnDet,VGrnDate,VInvNo,VInvDate,VGINo,VGIDate,VAmount,VCenVat,VGrnSlNo,VGrnStatus,VTaxPer;
	Vector     VStateCode, VCgstPer, VCgstVal, VSgstPer, VSgstVal, VIgstPer, VIgstVal, VCessPer, VCessVal, VGstStatus;
	
	java.util.List theGstList = null; 
	
	java.sql.Connection     	theAlpaConnection;
	
	
	boolean        bComflag = true;

                    
     public SPJAuto(int iDivisonCode,String SYearCode, String SGrnNo, String Sup_Code, int iUserCode)
     {
		
        this.iDivisonCode = iDivisonCode;
		this.SYearCode     = SYearCode;
		this.SGrnNo        = SGrnNo;
		this.iUserCode     = iUserCode;
		this.Sup_Code      = Sup_Code;

        if(iDivisonCode==0)
             this.iDivisonCode=1;

        if(iDivisonCode==1)
             this.iDivisonCode=2;


		setDataIntoVector();
		setSPJDetails();
		
		getSPJNO();
		
		updateGRN();
          UpdateSPJNo();
          getACommit();
		
	}	

     private void setDataIntoVector()
     {
     	 
     	
          VGrn            = new Vector();
          VGrnBlock       = new Vector();
          VGrnDet         = new Vector();
          VGrnDate        = new Vector();
          VInvNo          = new Vector();
          VInvDate        = new Vector();
          VGINo           = new Vector();
          VGIDate         = new Vector();
          VAmount         = new Vector();
          VCenVat         = new Vector();
          VGrnSlNo        = new Vector();
          VGrnStatus      = new Vector();
          VTaxPer         = new Vector();
          
          VStateCode      = new Vector();
          VCgstPer		  = new Vector();
          VCgstVal		  = new Vector();
          VSgstPer		  = new Vector();
          VSgstVal		  = new Vector();
          VIgstPer		  = new Vector();
          VIgstVal		  = new Vector();
          VCessPer		  = new Vector();
          VCessVal		  = new Vector();
          VGstStatus	  = new Vector();
     
          String QS = " Select GrnNo,GrnBlock,BlockName,GrnDate,InvNo,InvDate,GateInNo, "+
                      " GateInDate,Sum(InvNet),Sum(CenVat), 0 as SLNo, TaxPer, "+
                      " STATECODE, CGST, sum(CGSTVAL), SGST, sum(SGSTVAL), IGST, sum(IGSTVAL), CESS, sum(CESSVAL), GSTSTATUS, Status "+
                      " From ( "+
                      " Select Grn.GrnNo,Grn.GrnBlock,OrdBlock.BlockName,Grn.GrnDate, "+
                      " Grn.InvNo,Grn.InvDate,Grn.GateInNo,Grn.GateInDate,"+
                      " Sum((Grn.InvQty*Grn.InvRate)+Grn.CGSTVAL+Grn.SGSTVAL+Grn.IGSTVAL-Disc)  as InvNet,Sum(Grn.CenVat) as CenVat,Grn.SlNo,Grn.TaxPer, "+
                      " partymaster.STATECODE, Grn.CGST, Sum(Grn.CGSTVAL) as CGSTVAL, Grn.SGST, Sum(Grn.SGSTVAL) as SGSTVAL, Grn.IGST, Sum(Grn.IGSTVAL) as IGSTVAL, Grn.CESS, Sum(Grn.CESSVAL) as CESSVAL, Grn.GSTSTATUS, 0 as Status  "+
                      " From Grn "+
                      " Inner Join OrdBlock On Grn.GrnBlock = OrdBlock.Block "+
                      " Inner join MIll on Mill.MIllCode=Grn.MillCode"+
                      " Inner join partymaster on partymaster.partycode = grn.SUP_CODE "+
                      " Where (Grn.noofbills>Grn.Noofspj) and Mill.DivCode= "+iDivisonCode+"  And Grn.grnno ='"+SGrnNo+"' and Grn.Inspection=1 "+
                      " Group By Grn.GrnNo,Grn.GrnBlock,OrdBlock.BlockName,Grn.GrnDate, "+
                      " Grn.InvNo,Grn.InvDate,Grn.GateInNo,Grn.GateInDate,Grn.SlNo,Grn.TaxPer, "+
                      " partymaster.STATECODE, Grn.CGST, Grn.SGST, Grn.IGST, Grn.CESS, Grn.GSTSTATUS "+
                      " Union All "+
                      " Select WorkGrn.GrnNo,6 as GrnBlock,'WO' as BlockName,WorkGrn.GrnDate, "+
                      " WorkGrn.InvNo,WorkGrn.InvDate,WorkGrn.GateInNo,WorkGrn.GateInDate, "+
                      " Sum((WorkGrn.InvQty*WorkGrn.InvRate)+WorkGrn.CGSTVAL+WorkGrn.SGSTVAL+WorkGrn.IGSTVAL-Disc) as InvNet,0 as CenVat,SlNo,WorkGrn.TaxPer,"+
                      " partymaster.STATECODE, WorkGrn.CGST, Sum(WorkGrn.CGSTVAL) as CGSTVAL, WorkGrn.SGST, Sum(WorkGrn.SGSTVAL) as SGSTVAL, WorkGrn.IGST, Sum(WorkGrn.IGSTVAL) as IGSTVAL, WorkGrn.CESS, Sum(WorkGrn.CESSVAL) as CESSVAL, WorkGrn.GSTSTATUS, 1 as Status "+
                      " From WorkGrn "+
                      " Inner join Mill on Mill.MIllCode=WorkGrn.MillCode"+
                      " Inner join partymaster on partymaster.partycode = WorkGrn.SUP_CODE "+
                      " Where WorkGrn.noofbills>WorkGrn.Noofspj and Mill.DivCode= "+iDivisonCode+" and  WorkGrn.grnno ='"+SGrnNo+"'"+
                      " Group By WorkGrn.GrnNo,WorkGrn.GrnDate, "+
                      " WorkGrn.InvNo,WorkGrn.InvDate,WorkGrn.GateInNo,WorkGrn.GateInDate,SlNo,WorkGrn.TaxPer, "+
                      " partymaster.STATECODE, WorkGrn.CGST, WorkGrn.SGST, WorkGrn.IGST, WorkGrn.CESS, WorkGrn.GSTSTATUS "+
                      " Union All "+
                      " Select Grn.GrnNo,Grn.GrnBlock,OrdBlock.BlockName,Grn.GrnDate, "+
                      " Grn.InvNo,Grn.InvDate,Grn.GateInNo,Grn.GateInDate,"+
                      " Sum((Grn.InvQty*Grn.InvRate*-1)+Grn.CGSTVAL+Grn.SGSTVAL+Grn.IGSTVAL-Disc)  as InvNet,Sum(Grn.CenVat) as CenVat,Grn.SlNo,Grn.TaxPer, "+
                      " partymaster.STATECODE, Grn.CGST, Sum(Grn.CGSTVAL) as CGSTVAL, Grn.SGST, Sum(Grn.SGSTVAL) as SGSTVAL, Grn.IGST, Sum(Grn.IGSTVAL) as IGSTVAL, Grn.CESS, Sum(Grn.CESSVAL) as CESSVAL, Grn.GSTSTATUS, 0 as Status  "+
                      " From Grn"+
                      " Inner join Mill on Mill.MIllCode=Grn.MillCode"+
                      " Inner Join OrdBlock On Grn.GrnBlock = OrdBlock.Block "+
                      " Inner join partymaster on partymaster.partycode = Grn.SUP_CODE "+
                      " Where (REJFlag=1) and Mill.DivCode= "+iDivisonCode+"  And Grn.GrnNo ='"+SGrnNo+"'"+
                      " Group By Grn.GrnNo,Grn.GrnBlock,OrdBlock.BlockName,Grn.GrnDate, "+
                      " Grn.InvNo,Grn.InvDate,Grn.GateInNo,Grn.GateInDate,Grn.SlNo,Grn.TaxPer, "+
                      " partymaster.STATECODE, Grn.CGST, Grn.SGST, Grn.IGST, Grn.CESS, Grn.GSTSTATUS "+
                      " ) "+
                      " Group By GrnNo,GrnBlock,BlockName,GrnDate,InvNo,InvDate,GateInNo,GateInDate,TaxPer, "+
                      " STATECODE, CGST, SGST, IGST, CESS, GSTSTATUS, Status "+
                      " Order By 8,2,1 ";

     					System.out.println(QS);


          String AmendQS = " select OrdBlock.BlockName,PurchaseOrder.OrderNo from (((PurchaseOrder "+
                           " inner join OrdBlock on Purchaseorder.OrderBlock = OrdBlock.Block) "+
                           " inner join grn on PurchaseOrder.OrderNo = Grn.OrderNo and PurchaseOrder.OrderBlock = Grn.GrnBlock) "+
                           " Inner join Mill on Mill.MillCode=Grn.MillCode) "+
                           " Where Grn.noofbills>Grn.Noofspj and Mill.DivCode= "+iDivisonCode+"  and PurchaseOrder.Amended = 1 and  Grn.GrnNo ='"+SGrnNo+"' "+
                           " Group By Grn.GrnNo,Grn.GrnBlock,OrdBlock.BlockName,PurchaseOrder.OrderNo ";
                           
           try{
               Class.forName("oracle.jdbc.OracleDriver");
               java.sql.Connection theConnection  = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
               java.sql.Statement  theStatement   = theConnection.createStatement();
               java.sql.ResultSet  theResult      = theStatement.executeQuery(QS);
               
               int iCount = 0;
               
               while(theResult.next())
               {
                    iCount = iCount + 1;
                    
                    VGrn.addElement(theResult.getString(1));
                    VGrnBlock.addElement(theResult.getString(2));
                    VGrnDet.addElement(theResult.getString(3));
                    VGrnDate.addElement(common.parseDate(theResult.getString(4)));
                    VInvNo.addElement(theResult.getString(5));
                    VInvDate.addElement(common.parseDate(theResult.getString(6)));
                    VGINo.addElement(theResult.getString(7));
                    VGIDate.addElement(common.parseDate(theResult.getString(8)));
                    VAmount.addElement(theResult.getString(9));
                    VCenVat.addElement(theResult.getString(10));
                    VGrnSlNo.addElement(theResult.getString(11));
                    VTaxPer.addElement(theResult.getString(12));
                    
                    VStateCode        .addElement(theResult.getString(13));
					VCgstPer		  .addElement(theResult.getString(14));
					VCgstVal		  .addElement(theResult.getString(15));
					VSgstPer		  .addElement(theResult.getString(16));
					VSgstVal		  .addElement(theResult.getString(17));
					VIgstPer		  .addElement(theResult.getString(18));
					VIgstVal		  .addElement(theResult.getString(19));
					VCessPer		  .addElement(theResult.getString(20));
					VCessVal		  .addElement(theResult.getString(21));
					VGstStatus		  .addElement(theResult.getString(22));
					VGrnStatus		  .addElement(theResult.getString(23));
               }
               theResult	  . close();
               System.out.println(" 1-->");

               
               java.sql.ResultSet theResult1      = theStatement.executeQuery(AmendQS);
               while(theResult1.next())
               {
                    JOptionPane.showMessageDialog(null,"Amendment Order "+theResult1.getString(1)+theResult1.getString(2),"Information",JOptionPane.INFORMATION_MESSAGE);                                   
               }
               System.out.println(" 3-->");


               theConnection.close();

               if (iCount == 0)
               {
                    JOptionPane.showMessageDialog(null,"GRN not Available","Information",JOptionPane.INFORMATION_MESSAGE);                    
               }
               else
               {
                    
               }
               theStatement.close();
               theConnection.close();
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"Connection Problem - contact System Administrator","Information",JOptionPane.INFORMATION_MESSAGE);
               System.out.println("Selection : "+ex);
          }
     }


     private void updateGRN()
     {
          try
          {
               if(theAlpaConnection    . getAutoCommit())
                    theAlpaConnection  . setAutoCommit(false);

               java.sql.Statement theStatement   = theAlpaConnection.createStatement();

               for(int i=0;i<VGrn.size();i++)
               {
                    String SGrnNo    = (String)VGrn.elementAt(i);
                    int    iStatus   = common.toInt((String)VGrnStatus.elementAt(i));

                    String QS        = "";

                    if(iStatus==1)
                    {
                         QS = " Update inventory.WorkGrn Set spjdate="+SpjDate+",noofspj=noofspj+1 , SPJNO = "+SpjNo;
                         QS = QS+" Where GrnNo = "+SGrnNo+" and noofspj<NOOFBILLS and  WorkGrn.MillCode=(select MillCode from Inventory.Mill where DivCode="+iDivisonCode+")"; //and SlNo="+SGrnSlNo
                    }
                    else
                    {
                         QS = " Update inventory.Grn Set spjdate="+SpjDate+",noofspj=noofspj+1 , SPJNO = "+SpjNo;
                         QS = QS+" Where GrnNo = "+SGrnNo+" and noofspj<NOOFBILLS  and  Grn.MillCode=(select MillCode from Inventory.Mill where DivCode="+iDivisonCode+")"; //And GrnBlock = "+SGrnBlock+" and SlNo="+SGrnSlNo
                    }
                   theStatement.execute(QS);
          //         theStatement.close();
               }
               
               insertSPJ();
               
          }catch(Exception ex) {
               System.out.println("updateGRN "+ex);
               bComflag = false;
               JOptionPane . showMessageDialog(null,"Connection Problem - contact System Administrator","Information",JOptionPane.INFORMATION_MESSAGE);
               return;
          }
     }

	
    private void insertSPJ() {
    	  /*
          int iClass   = comfun.getClassCode(JCClass);
          int iProject = comfun.getProjectCode(JCProject);
          int iFormNo  = getFormCode(JCForm);
		  */
		  
          int iClass   = 0;
          int iProject = 0;
          int iFormNo  = 0;
	
       String QS = "Insert Into SPJ (Id, SPJNO, SPJDate, GrnNo, GrnDate, InvNo, InvDate, GINo, GIDate, AcCode, CtCode, Credit, ClassCode, FormCode, Narr, Class, Project, UserCode, DivisionCode, YearCode, VatPer, VatAmount, BalanceAmount, VatCtCode, CreationDate, ComputerName, GRNSTATUS, autostatus) Values ( ";
          QS = QS+" SPJID_SEQ.nextval ,";
          QS = QS+""+SpjNo+",";
          QS = QS+""+SpjDate+",";
          QS = QS+"'"+SGrnNo+"',";
          QS = QS+""+common.pureDate(SGrnDate)+",";
          QS = QS+"'"+SInvNo+"',";
          QS = QS+""+common.pureDate(SInvDate)+",";
          QS = QS+"'"+SGINo+"',";
          QS = QS+""+common.pureDate(SGIDate)+",";
          
          QS = QS+"'"+Sup_Code+"',";
          QS = QS+"'"+SCtCode+"',";			//Default Stores Material
          QS = QS+"0"+dAmount+",";		//Total Amount
          QS = QS+"1,";					//Default Stores Material ClassCode
          QS = QS+"0"+iFormNo+",";
          QS = QS+"'"+common.getNarration(SNarr)+"',";
          QS = QS+""+iClass+",";
          QS = QS+""+iProject+",";
          QS = QS+""+iUserCode+",";
          QS = QS+""+iDivisonCode+",";
          QS = QS+""+SYearCode+",";
          
          QS = QS+"0,";
          QS = QS+"0,";
        //QS = QS+"0"+TVatPer.getText()+",";
        //QS = QS+"0"+TVatAmt.getText()+",";
//        QS = QS+"0"+TBalanceAmt.getText()+",";
          QS = QS+"0,";
          QS = QS+"'"+SCtCode+"',";
          QS = QS+"'"+common.getServerDateandTime()+"',";
          QS = QS+"'"+common.getLocalHostName()+"',";
          QS = QS+"'"+SGrnStatus+"',";
          QS = QS+"'1')";

          System.out.println(QS);
          
          try{
               if(theAlpaConnection    . getAutoCommit())
                    theAlpaConnection  . setAutoCommit(false);

               java.sql.Statement theStatement = theAlpaConnection.createStatement();
								  theStatement . execute(QS);
								  theStatement . close();
               
               for(int i=0;i<theGstList.size();i++) {
               		java.util.HashMap theMap = (java.util.HashMap)theGstList.get(i);
               		
		           String SQsa  = " INSERT INTO SPJ_DETAILS(id, spjno, yearcode, divisioncode, GSTACCODE, GSTPER, GSTVAL, CREATIONDATE, COMPUTERNAME ) ";
		           		  SQsa += " VALUES(SPJ_DETAILS_SEQ.nextVal, ";
		           		  SQsa += " "+SpjNo+", ";
						  SQsa += " "+SYearCode+",";
                          SQsa += " "+iDivisonCode+",";
						  
						  if(common.parseNull((String)theMap.get("GST_TYPE")).equals("CGST")){
						  SQsa += " 'A_5997', ";
						  }else if(common.parseNull((String)theMap.get("GST_TYPE")).equals("SGST")){
						  SQsa += " 'A_5998', ";
						  }else if(common.parseNull((String)theMap.get("GST_TYPE")).equals("IGST")){
						  SQsa += " 'A_5999', ";
						  }
						  SQsa += " "+common.parseNull((String)theMap.get("GST_PER"))+", ";
						  SQsa += " "+common.parseNull((String)theMap.get("GST_VAL"))+", ";
						  SQsa += " SYSDATE, ";
						  SQsa += " '"+common.getLocalHostName()+"' ) ";
		           
		           theStatement   = theAlpaConnection.createStatement();
		           theStatement   . execute(SQsa);
		       }
		           theStatement.close();
               
               
          }
          catch(Exception ex)
          {
               System.out.println("insertSPJ "+ex);
               bComflag = false;
          }
     }
     
     
     
    private void setSPJDetails() {
    
	  	theGstList = new java.util.ArrayList();
      
		dAmount	= 0;
		dCenVat	= 0;
		
		SInvNo		= "";
		SInvDate  	= "";
		SGINo		= "";
		SGIDate		= "";
		SGrnDet		= "";
		SGrnDate	= "";
		STaxPer		= "";
		SNarr 		= "";
		SGrnStatus  = "";
		
		int l = 0;
		
   		for(int i=0;i<VGrn.size();i++)  {
           
         	if(l==0)  {
              SGrnDet  += (String)VGrn.elementAt(i)+",";
              SGrnDate  = (String)VGrnDate.elementAt(i);
              SGINo    += (String)VGINo.elementAt(i)+",";
              SGIDate   = (String)VGIDate.elementAt(i);
              SInvNo   += (String)VInvNo.elementAt(i)+",";
              SInvDate  = (String)VInvDate.elementAt(i)+",";
              STaxPer   = (String)VTaxPer.elementAt(i)+"";
              l++;
         	}
              dAmount  = dAmount+common.toDouble((String)VAmount.elementAt(i));
              dCenVat  = dCenVat+common.toDouble((String)VCenVat.elementAt(i));
             	
              SGrnStatus = (String)VGrnStatus.elementAt(i)+"";
             
      		if(common.toInt((String)VStateCode.elementAt(i))==0){
	      		
	      		java.util.HashMap theMap = new java.util.HashMap();
	      						  theMap . put("GST_TYPE","CGST");
	      						  theMap . put("GST_PER",common.parseNull((String)VCgstPer.elementAt(i)));
	      						  theMap . put("GST_VAL",common.parseNull((String)VCgstVal.elementAt(i)));
				theGstList.add(theMap);
				
								  theMap = new java.util.HashMap();
	      						  theMap . put("GST_TYPE","SGST");
	      						  theMap . put("GST_PER",common.parseNull((String)VSgstPer.elementAt(i)));
	      						  theMap . put("GST_VAL",common.parseNull((String)VSgstVal.elementAt(i)));
									  		
		  		theGstList.add(theMap);
		      		
	      	}else{
	      		java.util.HashMap theMap = new java.util.HashMap();
	      						  theMap . put("GST_TYPE","IGST");
	      						  theMap . put("GST_PER",common.parseNull((String)VIgstPer.elementAt(i)));
	      						  theMap . put("GST_VAL",common.parseNull((String)VIgstVal.elementAt(i)));
	      	
		  		theGstList.add(theMap);
	      	}
		       
		}
          

          try
          {
               SGrnDet = SGrnDet.substring(0,SGrnDet.length()-1);
          }catch(Exception ex){}
          try
          {
               SGINo   = SGINo.substring(0,SGINo.length()-1);
          }catch(Exception ex){}
          try
          {
               SInvNo  = SInvNo.substring(0,SInvNo.length()-1);
               SInvDate= SInvDate.substring(0,SInvDate.length()-1);
          }catch(Exception ex){}
          
			/*
          		setVatPer();
          	*/
          
          SNarr = "Bill No. "+SInvNo+"/ Bill Date. "+SInvDate;
          
          
          System.out.println("TaxPer"+STaxPer);
          
          if(STaxPer.equals("2"))
          {
          //JCForm.setSelectedItem("C Form");
          }

     }
     
	
     public void getSPJNO()
     {
          String QS      = " Select MaxNUmber From NoConfig Where Divisioncode="+iDivisonCode+" and YearCode="+SYearCode+" and VocType='SPJNO' for update of MaxNumber noWait";
          String SSPJNO  = "";

          try
          {
               Class               . forName("oracle.jdbc.OracleDriver");
               theAlpaConnection   = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","alpa","alpa");

               if(theAlpaConnection   . getAutoCommit())
                    theAlpaConnection   . setAutoCommit(false);

               java.sql.Statement   theStatement        = theAlpaConnection . createStatement();
               java.sql.ResultSet   theResult           = theStatement      . executeQuery(QS);

               while(theResult.next())
               {
                    SSPJNO = theResult.getString(1);
               }
               int iSPJNum    = common.toInt(SSPJNO);

               iSPJNum    = iSPJNum+1;

               SpjNo 	  = String.valueOf(iSPJNum);
               SpjDate    = common.getCurrentDate();
               
               theStatement.close();
          }
          catch(Exception ex)
          {
               String SException = String.valueOf(ex);
               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex){}
                    getSPJNO();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }

               try
               {
               }catch(Exception Ex1)
               {
               }
          }
	}
	
     public void getPrevSPJNO()
     {
          String QS      = " Select MaxNumber From NoConfig Where Divisioncode="+iDivisonCode+" and YearCode="+SYearCode+" and VocType='SPJNO' ";
          String SSPJNO  = "";

          try {
               Class.forName("oracle.jdbc.OracleDriver");
               java.sql.Connection theConnection  	= DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","alpa","alpa");
               java.sql.Statement      theStatement = theConnection     . createStatement();
               java.sql.ResultSet      theResult    = theStatement      . executeQuery(QS);

               while(theResult.next())
               {
                    SSPJNO = theResult.getString(1);
               }
               int iSPJNum    = common.toInt(SSPJNO);
               //TVocNo         . setText(String.valueOf(iSPJNum));
               
               theStatement.close();
               theConnection.close();
          }
          catch(Exception ex)
          {
          }
	}
	
	
     
     private void UpdateSPJNo()
     {
          String Qs      = " Update noconfig set MaxNumber= "+SpjNo+" where DivisionCode = "+iDivisonCode+" and YearCode = "+SYearCode+" and VocType='SPJNO'";

          System.out.println("Update--"+Qs);

          try
          {
               if(theAlpaConnection   . getAutoCommit())
                    theAlpaConnection   . setAutoCommit(false);

               java.sql.Statement      theStatement   = theAlpaConnection.createStatement();
                              theStatement   . execute(Qs);
                              theStatement.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               bComflag = false;
          }
     }
	
	
	
     private boolean valid()
     {
          String str = "1"; //(TCtCode.getText()).trim();
          if(str.length() == 0)
               return false;
          return true;        
     }
     
     private boolean valid1()
     {
          int iCount=0;
          for(int i=0;i<VGrn.size();i++)
          {
               String SGrnNo    = (String)VGrn.elementAt(i);
               int    iStatus   = common.toInt((String)VGrnStatus.elementAt(i));

               iCount          += getGrnStatus(SGrnNo,iStatus);
               System.out.println("CoutnCheck"+iCount);
          }
          if(iCount>0)
               return false;
          else
               return true;        
     }
     
     private boolean valid2()
     {
          int iCount = VGrn.size();
          if(iCount>0)
               return true;
          else
               return false;        
     }



     public int getGrnStatus(String SGrnNo,int iStatus)
     {
          String SGrn="";

          String QS="";

          if(iStatus==1)
               QS      = " Select GrnNo From WorkGrn "+
                         " Inner join Mill on Mill.MillCode=WorkGrn.MillCode"+
                         " Where WorkGrn.noofbills>WorkGrn.Noofspj"+
                         " and  GrnNo='"+SGrnNo+"' and Sup_Code='"+Sup_Code+"' and Mill.DivCode="+iDivisonCode;   
          else
               QS      = " Select GrnNo From Grn"+
                         " Inner join Mill on Mill.MillCode=Grn.MillCode"+
                         " Where Grn.noofbills>Grn.Noofspj"+
                         " and  GrnNo='"+SGrnNo+"' and Sup_Code='"+Sup_Code+"'  and Mill.DivCode="+iDivisonCode; 

             System.out.println(QS);

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               java.sql.Connection theConnection           = DriverManager     . getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
               java.sql.Statement      theStatement        = theConnection     . createStatement();
               java.sql.ResultSet      theResult           = theStatement      . executeQuery(QS);

               while(theResult.next())
               {
                    SGrn = theResult.getString(1);
               }
               theStatement.close();
               theConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          if(!SGrn.equals(""))
               return 1;
          else
               return 0;
	}


	
     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theAlpaConnection   . commit();
                    JOptionPane         . showMessageDialog(null,"SPJNO  is "+SpjNo,"Information",JOptionPane.INFORMATION_MESSAGE);
                    System              . out.println("Commit");
               }
               else
               {
                    theAlpaConnection   . rollback();
                    System              . out.println("RollBack");
               }
               theAlpaConnection   . setAutoCommit(true);
               theAlpaConnection   . close();
               
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }


    public static void main(String args[]){
		
		//new SPJAuto(1,"11", "162018", "850352", 7565);

        new SPJAuto(0,"12", "171110", "950368", -1);
		
		
     } 

	

}
