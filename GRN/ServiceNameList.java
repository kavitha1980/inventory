package GRN;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class ServiceNameList extends JInternalFrame
{

	 private JDialog  thedialog = null;
	
     JTextField     TMatCode,TMatName,THsnCode, TCgstPer, TSgstPer, TIgstPer, TCessPer;
     JTextField     TIndicator;
     JButton        BOk,BCancel;
     
     JList          BrowList;
     JScrollPane    BrowScroll;
     JPanel         LeftPanel;
     JPanel         LeftCenterPanel,LeftBottomPanel;
     
     JLayeredPane      Layer;
     JTable            ReportTable;
     DirectGRNUpdateModel  dataModel;
     Vector            VRateStatus;

     Vector            VSName,VSCode;
     Vector            VSNameCode, VSHsnCode, VCgstPer, VSgstPer, VIgstPer, VCessPer;

     String         str ="";
     
     int            iLastIndex= 0;
     Common         common    = new Common();
     
     String 		SSeleSupType = "", SSeleStateCode = "";

     ServiceNameList(JLayeredPane Layer,JTable ReportTable,DirectGRNUpdateModel dataModel,Vector VRateStatus, String SSeleSupType, String SSeleStateCode)
     {
          this.Layer          = Layer;
          this.ReportTable    = ReportTable;
          this.dataModel      = dataModel;
	  	  this.VRateStatus    = VRateStatus;
	  	  this.SSeleSupType	  = SSeleSupType;
	  	  this.SSeleStateCode = SSeleStateCode;

	  	  setDataIntoVector();
          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
          
		  thedialog   . setBounds(50,50,650,500);
		  thedialog   . setVisible(true);
		  thedialog   . setFocusable(true);
     }

     public void createComponents()
     {
          BrowList            = new JList(VSNameCode);
          BrowScroll          = new JScrollPane();
          TMatCode            = new JTextField();
          TMatName            = new JTextField();	
          THsnCode            = new JTextField();
          TCgstPer            = new JTextField();
          TSgstPer            = new JTextField();
          TIgstPer            = new JTextField();
          TCessPer            = new JTextField();
          TIndicator          = new JTextField();
          TIndicator          . setEditable(false);
          
          BOk                 = new JButton("Okay");
          BCancel             = new JButton("Cancel");
          
          LeftPanel           = new JPanel(true);
          LeftCenterPanel     = new JPanel(true);
          LeftBottomPanel     = new JPanel(true);
          
          TMatCode            . setEditable(false);
          TMatName            . setEditable(false);
          THsnCode            . setEditable(false);
          TCgstPer            . setEditable(false);
          TSgstPer            . setEditable(false);
          TIgstPer            . setEditable(false);
          TCessPer            . setEditable(false);
          
          thedialog   = new JDialog(new Frame(),"Grn Update Frame",true);
     }

     public void setLayouts()
     {		
     	  /*
          setBounds(80,50,650,500);
          setResizable(true);
          setClosable(true);
          setTitle("Service Selection");
          */
          
          getContentPane()    . setLayout(new GridLayout(1,1));
          LeftPanel           . setLayout(new BorderLayout());
          LeftCenterPanel     . setLayout(new BorderLayout());
          LeftBottomPanel     . setLayout(new GridLayout(4,2));
     }

     public void addComponents()
     {
          LeftPanel           . add("Center",LeftCenterPanel);
          LeftPanel           . add("South",LeftBottomPanel);
          
          LeftCenterPanel     . add("Center",BrowScroll);
          LeftCenterPanel     . add("South",TIndicator);
          LeftBottomPanel     . add(new JLabel("Material Name"));
          LeftBottomPanel     . add(TMatName);
          LeftBottomPanel     . add(new JLabel("Material Code"));
          LeftBottomPanel     . add(TMatCode);
          LeftBottomPanel     . add(new JLabel(""));
          LeftBottomPanel     . add(new JLabel(""));
          LeftBottomPanel     . add(BOk);
          LeftBottomPanel     . add(BCancel);
          
          thedialog 		  . getContentPane()    . add(LeftPanel);
     }

     public void setPresets()
     {
          int i = ReportTable.getSelectedRow();
          String SNameCode="";
          String SCode   = (String)ReportTable.getModel().getValueAt(i,0);
          String SName   = (String)ReportTable.getModel().getValueAt(i,1);
          int iindex     = common.indexOf(VSCode,SCode);
          if(iindex > -1)
               SNameCode = (String)VSNameCode.elementAt(iindex);
          
          BrowList            . setAutoscrolls(true);
          BrowScroll          . getViewport().setView(BrowList);
          TMatCode            . setText(SCode);
          TMatName            . setText(SName);

          show();
          ensureIndexIsVisible(SNameCode);
          LeftCenterPanel.updateUI();
     }

     public void addListeners()
     {
          BrowList  .addKeyListener(new KeyList());
          BOk       .addActionListener(new ActList());
          BCancel   .addActionListener(new ActList());
                    addMouseListener(new MouseList());
     }

     public class MouseList extends MouseAdapter
     {
          public void mouseEntered(MouseEvent me)
          {
               BrowList.ensureIndexIsVisible(iLastIndex);               
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                     int       i = ReportTable.getSelectedRow();
 
                     String SNewCode  = TMatCode.getText();
 
                     ReportTable.getModel().setValueAt(TMatCode.getText(),i,0);
                     ReportTable.getModel().setValueAt(TMatName.getText(),i,1);
                     ReportTable.getModel().setValueAt(THsnCode.getText(),i,2);

 					 ReportTable.getModel().setValueAt(TCgstPer.getText(),i,12);
 					 ReportTable.getModel().setValueAt(TSgstPer.getText(),i,13);
 					 ReportTable.getModel().setValueAt(TIgstPer.getText(),i,14);
 					 ReportTable.getModel().setValueAt(TCessPer.getText(),i,15);
 					 
					 //dataModel.setTaxData();
				 	 VRateStatus.setElementAt("1",i);
					 dataModel.setTaxData();
					 
                     //removeHelpFrame();
                     thedialog   . setVisible(false);
                     ReportTable.requestFocus();
               }
          }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='/') || (lastchar=='*') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==116)    // F5 is pressed
               {
                    setDataIntoVector();
                    BrowList.setListData(VSNameCode);
               }
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SMatName     = (String)VSName.elementAt(index);
                    String SMatCode     = (String)VSCode.elementAt(index);
                    String SMatNameCode = (String)VSNameCode.elementAt(index);
                    String SHsnCode     = (String)VSHsnCode.elementAt(index);
                    String SCgstPer     = (String)VCgstPer.elementAt(index);
                    String SSgstPer     = (String)VSgstPer.elementAt(index);
                    String SIgstPer     = (String)VIgstPer.elementAt(index);
                    String SCessPer     = (String)VCessPer.elementAt(index);
                    
                    addMatDet(SMatName,SMatCode,SMatNameCode,SHsnCode, SCgstPer, SSgstPer, SIgstPer, SCessPer,index);
                    str="";
               }
          }
     }

     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<VSNameCode.size();index++)
          {
               String str1 = ((String)VSNameCode.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index);
                    break;
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

     public boolean addMatDet(String SMatName,String SMatCode,String SMatNameCode,String SHsnCode, String CgstPer, String SgstPer, String IgstPer, String SCessPer,int index)
     {
          TMatCode       . setText(SMatCode);
          TMatName       . setText(SMatName);
          THsnCode       . setText(SHsnCode);
          TCgstPer       . setText(CgstPer);
          TSgstPer       . setText(SgstPer);
          TIgstPer       . setText(IgstPer);
          TCessPer       . setText(SCessPer);

          return true;    
     }

     public boolean ensureIndexIsVisible(String SNameCode)
     {
          SNameCode = SNameCode.trim();
          int i=0;

          for(i=0;i<VSNameCode.size();i++)
          {
               String SCurName = (String)VSNameCode.elementAt(i);
               SCurName = SCurName.trim();
               if(SCurName.startsWith(SNameCode))
               break;
          }

          if(i==VSNameCode.size())
          {
               return false;
          }
          iLastIndex=i;
          BrowList.setSelectedIndex(i);
          BrowList.ensureIndexIsVisible(i);
          BrowList.requestFocus();
          BrowList.updateUI();
          return true;
     }

     public void setDataIntoVector()
     {
          VSName     = new Vector();
          VSCode     = new Vector();
          VSNameCode = new Vector();
          VSHsnCode  = new Vector();
          VCgstPer   = new Vector();
          VSgstPer   = new Vector();
          VIgstPer   = new Vector();
          VCessPer   = new Vector();
          
          //String QString = " Select Item_Name,Item_Code,HsnCode From InvItems Where HsnType=1 and HsnCode is Not Null and HsnCode<>'0' Order By Item_Name";
          
          String QString = " ";
          
          if(SSeleStateCode.equals("33")){
          
           QString = " Select InvItems.Item_Name, InvItems.Item_Code, InvItems.HsnCode, HsnGstRate.GstRate, HsnGstRate.Cgst, HsnGstRate.Sgst, 0 as Igst, HsnGstRate.Cess From InvItems "+
  			 	     " Inner Join HsnGstRate on HsnGstRate.HsnCode = InvItems.HsnCode "+
  				     " Where InvItems.HsnType=1 and InvItems.HsnCode is Not Null and InvItems.HsnCode<>'0' Order By InvItems.Item_Name ";
          
          }else{
           QString = " Select InvItems.Item_Name, InvItems.Item_Code, InvItems.HsnCode, HsnGstRate.GstRate, 0 as Cgst, 0 as Sgst, HsnGstRate.Igst, HsnGstRate.Cess From InvItems "+
  			 	     " Inner Join HsnGstRate on HsnGstRate.HsnCode = InvItems.HsnCode "+
  				     " Where InvItems.HsnType=1 and InvItems.HsnCode is Not Null and InvItems.HsnCode<>'0' Order By InvItems.Item_Name ";
	      }
          
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
               ResultSet      res            = stat.executeQuery(QString);

               while(res.next())
               {
                    String    str1      = res.getString(1);
                    String    str2      = res.getString(2);
                    String    str3      = res.getString(3);
                              VSName    . addElement(str1);
                              VSCode    . addElement(str2);
                              VSNameCode. addElement(str1+" (Code : "+str2+")"+"  (GST : "+res.getString(4)+")");
                              VSHsnCode . addElement(str3);
                              VCgstPer . addElement(res.getString(5));
                              VSgstPer . addElement(res.getString(6));
                              VIgstPer . addElement(res.getString(7));
                              VCessPer . addElement(res.getString(8));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

}
