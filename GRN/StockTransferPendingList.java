package GRN;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class StockTransferPendingList extends JInternalFrame
{
      JPanel     TopPanel;
      JPanel     MiddlePanel;
      JPanel     BottomPanel;
 
      String     SDate;
      JButton    BApply;
      DateField  TDate;

      JLayeredPane DeskTop;
      StatusPanel SPanel;
      int iMillCode;

      Common common = new Common();
      ORAConnection connect;
      Connection theconnect;
      TabReport tabreport;

      Vector VConcName,VTranNo,VTranDate,VRDCNo,VItemCode,VItemName,VTranQty,VGrnQty,VPendQty;

      String ColumnData[] = {"Concern","TranNo","TranDate","RDC No","ItemCode","ItemName","Transfer Qty","Grn Qty","Pending Qty"};
      String ColumnType[] = {"S"      ,"N"     ,"S"       ,"N"     ,"S"       ,"S"       ,"N"           ,"N"      ,"N"          };
      Object RowData[][];


      public StockTransferPendingList(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode)
      {
          super("Stock Transfer Pending for GRN As On ");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iMillCode = iMillCode;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
      }

      public void createComponents()
      {
          TopPanel    = new JPanel();
          MiddlePanel = new JPanel();
          BottomPanel = new JPanel();
 
          TDate    = new DateField();
          BApply   = new JButton("Apply");
 
          TDate.setTodayDate();
      }

      public void setLayouts()
      {
          TopPanel.setLayout(new GridLayout(2,3));
          MiddlePanel.setLayout(new BorderLayout());
            
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
      }
                                                                        
      public void addComponents()
      {
          TopPanel.add(new JLabel("As On "));
          TopPanel.add(TDate);
          TopPanel.add(BApply);
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));
 
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
      }
      public void addListeners()
      {
          BApply.addActionListener(new ApplyList());
      }
      public class ApplyList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setDataIntoVector();
                  if(VConcName.size()>0)
                  {
                       setVectorIntoRowData();
                       try
                       {
                             MiddlePanel.remove(tabreport);
                       }
                       catch(Exception ex){}
     
                       tabreport = new TabReport(RowData,ColumnData,ColumnType);
                       MiddlePanel.add("Center",tabreport);
                       MiddlePanel.updateUI();
                  }
                  else
                  {
                       JOptionPane.showMessageDialog(null,"No Pending Transaction ","Information",JOptionPane.INFORMATION_MESSAGE);
                  }
            }
      }
      public void removeHelpFrame()
      {
            try
            {
                  DeskTop.remove(this);
                  DeskTop.repaint();
                  DeskTop.updateUI();
            }
            catch(Exception ex) { }
      }

      private void setDataIntoVector()
      {
            VConcName      = new Vector();
            VTranNo        = new Vector();
            VTranDate      = new Vector();
            VRDCNo         = new Vector();
            VItemCode      = new Vector();
            VItemName      = new Vector();
            VTranQty       = new Vector();
            VGrnQty        = new Vector();
            VPendQty       = new Vector();

            String QS  = "";

            QS  = " SELECT Mill.ShortName, StockTransfer.TranNo,StockTransfer.TranDate,StockTransfer.DCNo,"+
                  " StockTransfer.Code,InvItems.Item_Name,StockTransfer.Qty,StockTransfer.InvQty "+
                  " FROM StockTransfer "+
                  " Inner Join Mill On StockTransfer.MillCode = Mill.MillCode "+
                  " And StockTransfer.TranDate <= '"+TDate.toNormal()+"' And StockTransfer.Qty>StockTransfer.InvQty "+
                  " And StockTransfer.ToMillCode="+iMillCode+
                  " Inner Join InvItems On StockTransfer.Code = InvItems.Item_Code "+
                  " Order by StockTransfer.TranDate,Mill.ShortName,InvItems.Item_Name ";

            try
            {
                  if(theconnect==null)
                  {
                       connect=ORAConnection.getORAConnection();
                       theconnect=connect.getConnection();
                  }
                  Statement stat  = theconnect.createStatement();
                  ResultSet theResult = stat.executeQuery(QS);
                  while(theResult.next())
                  {
                        double dQty     = theResult.getDouble(7);
                        double dGrnQty  = theResult.getDouble(8);
                        double dPendQty = dQty - dGrnQty;

                        VConcName      .addElement(theResult.getString(1));
                        VTranNo        .addElement(theResult.getString(2));
                        VTranDate      .addElement(common.parseDate(theResult.getString(3)));
                        VRDCNo         .addElement(theResult.getString(4));
                        VItemCode      .addElement(theResult.getString(5));
                        VItemName      .addElement(theResult.getString(6));
                        VTranQty       .addElement(common.getRound(dQty,3));
                        VGrnQty        .addElement(common.getRound(dGrnQty,3));
                        VPendQty       .addElement(common.getRound(dPendQty,3));
                  }
                  theResult.close();
                  stat.close();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      private void setVectorIntoRowData()
      {
            RowData = new Object[VConcName.size()][ColumnData.length];
            for(int i=0;i<VConcName.size();i++)
            {
                  RowData[i][0] = (String)VConcName.elementAt(i);
                  RowData[i][1] = (String)VTranNo.elementAt(i);
                  RowData[i][2] = (String)VTranDate.elementAt(i);
                  RowData[i][3] = (String)VRDCNo.elementAt(i);
                  RowData[i][4] = (String)VItemCode.elementAt(i);
                  RowData[i][5] = (String)VItemName.elementAt(i);
                  RowData[i][6] = (String)VTranQty.elementAt(i);
                  RowData[i][7] = (String)VGrnQty.elementAt(i);
                  RowData[i][8] = (String)VPendQty.elementAt(i);
            }
      }

}
