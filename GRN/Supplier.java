
package GRN;

import java.util.ArrayList;
import java.util.HashMap;


public class Supplier {
    String                       sSupCode, sSupName;
    ArrayList                    AInvNotReceivedList,APendingInvReceivedList ;
    

public Supplier(String sSupCode, String sSupName){
    
    this.sSupCode               = sSupCode;
    this.sSupName               = sSupName;
    AInvNotReceivedList         = new ArrayList();
    APendingInvReceivedList     = new ArrayList();
}

public void appendDetails(String sOrderNo,String sOrderDate,String sDCNo,String sDCDate, String sAddress1, String sAddress2, String sAddress3, String sGRNQty,String sInvNo, String sInvDate,String sGRNNo,String sGRNDate,String sNoofRemainders){
    
    HashMap             theMap      = new HashMap();
    theMap.put("OrderNo",sOrderNo);
    theMap.put("OrderDate",sOrderDate);
    theMap.put("DCNo",sDCNo);
    theMap.put("DCDate",sDCDate);
    theMap.put("Address1",sAddress1);
    theMap.put("Address2",sAddress2);
    theMap.put("Address3",sAddress3);
    theMap.put("GRNQty",sGRNQty);
    theMap.put("InvNo",sInvNo);
    theMap.put("InvDate",sInvDate);
    theMap.put("GRNNo",sGRNNo);
    theMap.put("GRNDate",sGRNDate);
    theMap.put("NoofRemainder",sNoofRemainders);

    AInvNotReceivedList.add(theMap);
    
}

public void appendInvDetails(String sOrderNo,String sOrderDate,String sGRNNo,String sGRNDate,String sInvReceivedDate){
    
    HashMap             theMap      = new HashMap();
    theMap.put("OrderNo",sOrderNo);
    theMap.put("OrderDate",sOrderDate);
    theMap.put("GRNNo",sGRNNo);
    theMap.put("GRNDate",sGRNDate);
    theMap.put("InvoiceReceivedDate",sInvReceivedDate);
    
   APendingInvReceivedList.add(theMap);
    
}

}
