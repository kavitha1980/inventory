package GRN;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class SupplierGrnList extends JInternalFrame
{
     JPanel         TopPanel;
     JPanel         MiddlePanel;
     JPanel         BottomPanel;
     
     String         SDate;
     JButton        BApply,BPrint;
     JTextField     TFile;
     DateField      TStDate,TEnDate;
     
     JLayeredPane   DeskTop;
     Vector         VCode,VName;
     StatusPanel    SPanel;
     
     Common         common = new Common();
     TabReport      tabreport;
     
     Vector         VAcCode,VAcName,VGrnNo,VGrnBlock,VGrnDate,VGateNo,VGateDate,VOrderNo;
     Vector         VInvNo,VInvDate;
     Vector         VMatName,VInvQty,VRecQty,VRejQty,VAcQty;
     Vector         VInvValue,VBlock,VOrdQty,VFinDelay,VGrnDelay;
     Vector         VGrnType,VRejNo,VRejDate;
     
     Vector         VXOrderDate,VXOrderBlock,VXOrderNo,VXIndex;   
     
     String         ColumnData[] = {"Supplier","Grn Type","Grn No","Block","Grn Date","Rej No","Rej Date","Order No","Order Date","Invoice No","Invoice Date","Material Name","Qty Accepted","Invoice Value","GRN Vs Finance","GI Vs GRN"};
     String         ColumnType[] = {"S"       ,"S"       ,"S"     ,"S"    ,"S"       ,"S"     ,"S"       ,"S"       ,"S"         ,"S"         ,"S"           ,"S"            ,"N"           ,"V"            ,"N"             ,"N"};
     Object         RowData[][];
     
     String         SHead1 = "|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
     String         SHead2 = "|                       GRN                                 |          Gate           |           Order           |        Invoice          |                                          |                        Q U A N T I T Y                         |    Invoice   |  Delayed Days |";
     String         SHead3 = "|-------------------------------------------------------------------------------------------------------------------------------------------|              Material Name               |----------------------------------------------------------------|     Value    |---------------|";
     String         SHead4 = "| GrnType |  Grn No  | Grn Date |Block|  Rej No  | Rej Date |     No     |    Date    |    No    |Block|   Date   |     No     |    Date    |                                          |  As Per DC |  Received  |  Rejected  |  Accepted  |   Ordered  |           Rs |  Grn | GI-GRN |";
     String         SHead5 = "|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
     String         SHead6 = "|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
     String         SHead7 = "|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
     String         SHead8 = "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
     String         SHead9 = "|         |          |          |     |          |          |            |            |          |     |          |            |            |                                          |            |            |            |            |            |              |      |        |";
     
     int            Lctr = 100,Pctr     = 0;
     FileWriter     FW;
     double         dInvValue = 0;
     int            iMillCode;
     String         SSupTable,SMillName;
     
     public SupplierGrnList(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,String SSupTable,String SMillName)
     {
          super("Supplierwise Grn's Passed during the period");
     
          this.DeskTop   = DeskTop;
          this.VCode     = VCode;
          this.VName     = VName;
          this.SPanel    = SPanel;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SMillName = SMillName;
          
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel       = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();
          
          TStDate        = new DateField();
          TEnDate        = new DateField();
          
          BApply         = new JButton("Apply");
          BPrint         = new JButton("Print");
          TFile          = new JTextField(15);
          
          TStDate        . setTodayDate();
          TEnDate        . setTodayDate();
     }

     public void setLayouts()
     {
          TopPanel       . setLayout(new GridLayout(1,5));
          MiddlePanel    . setLayout(new BorderLayout());

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          TopPanel            . add(new JLabel("As On "));
          TopPanel            . add(TStDate);
          TopPanel            . add(TEnDate);
          
          TopPanel            . add(BApply);
          
          BottomPanel         . add(BPrint);
          BottomPanel         . add(TFile);
          
          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(MiddlePanel,BorderLayout.CENTER);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply    . addActionListener(new ApplyList());
          BPrint    . addActionListener(new PrintList());
     }
     
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setVectorIntoRowData();
               try
               {
                    MiddlePanel    . remove(tabreport);
               }
               catch(Exception ex){}
               
               tabreport      = new TabReport(RowData,ColumnData,ColumnType);                        
               MiddlePanel    . add("Center",tabreport);
               MiddlePanel    . updateUI();
          }
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setPendingReport();
               removeHelpFrame();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop   . remove(this);
               DeskTop   . repaint();
               DeskTop   . updateUI();
          }
          catch(Exception ex) { }
     }

     private void setDataIntoVector()
     {
          VAcCode        = new Vector();
          VAcName        = new Vector();
          VGrnNo         = new Vector();
          VGrnBlock      = new Vector();
          VGrnDate       = new Vector();
          VGateNo        = new Vector();
          VGateDate      = new Vector();
          VOrderNo       = new Vector();
          VInvNo         = new Vector();
          VInvDate       = new Vector();
          VMatName       = new Vector();
          VInvQty        = new Vector();
          VRecQty        = new Vector();
          VRejQty        = new Vector();
          VAcQty         = new Vector();
          VInvValue      = new Vector();
          VBlock         = new Vector();
          VOrdQty        = new Vector();
          VGrnType       = new Vector();
          VRejNo         = new Vector();
          VRejDate       = new Vector();
          
          VXOrderDate    = new Vector();
          VXOrderBlock   = new Vector();
          VXOrderNo      = new Vector();
          
          String QS      = "";
          String QS1     = "";

          QS =      " select SupplierCode,Suppliername,GrnNum,"+
                    " OrdBlockName,grnDate,GInNum,GinDate,OrdNum,"+
                    " GrnInvNum,GrnInvDate,ItemName,InvQty,MilQty,sum(RejQty),sum(GrnQty),"+
                    " sum(GrnValue),BlockCode,PendingQty,RejecNo,RejecDate,GrnType,ReFlag,Grnslno"+
                    " from"+
                    " (select grn.sup_code as Suppliercode,"+SSupTable+".name as Suppliername,"+
                    " grn.grnno as GrnNum,ordblock.blockname as OrdBlockName,"+
                    " grn.grndate as grnDate,grn.gateinno as GInNum,"+
                    " grn.gateindate as GinDate,grn.orderno as OrdNum,"+
                    " GRN.InvNo as GrnInvNum,GRN.InvDate as GrnInvDate,"+
                    " InvItems.ITEM_NAME as ItemName,GRN.InvQty as InvQty,GRN.MillQty as MilQty,"+
                    " GRN.RejQty as RejQty,GRN.GrnQty as GrnQty,GRN.GrnValue as GrnValue,"+
                    " Grn.GrnBlock as BlockCode,Grn.Pending as PendingQty,"+
                    " 0 as RejecNo,' ' as RejecDate,'Grn' as GrnType,Grn.Rejflag as ReFlag,Grn.SlNo as Grnslno"+
                    " from grn"+
                    " inner join "+SSupTable+" on "+SSupTable+".ac_code = grn.sup_code"+
                    " inner join invitems on grn.code = invitems.item_code and invitems.hsntype=0"+
                    " inner join ordblock on grn.grnblock = ordblock.block"+
                    " Where Grn.GrnDate >='"+TStDate.toNormal()+"' And "+
                    " Grn.GrnDate <= '"+TEnDate.toNormal()+"'"+
                    " and Grn.millcode="+iMillCode+" and rejflag = 0"+
                    " union all"+
                    " select grn.sup_code as Suppliercode,"+SSupTable+".name as Suppliername,"+
                    " grn.grnno as GrnNum,ordblock.blockname as OrdBlockName,"+
                    " grn.grndate as grnDate,grn.gateinno as GInNum,"+
                    " grn.gateindate as GinDate,grn.orderno as OrdNum,"+
                    " GRN.InvNo as GrnInvNum,GRN.InvDate as GrnInvDate,"+
                    " InvItems.ITEM_NAME as ItemName,GRN.InvQty as InvQty,GRN.MillQty as MilQty,"+
                    " GRN.RejQty as RejQty,GRN.GrnQty as GrnQty,GRN.GrnValue as GrnValue,"+
                    " Grn.GrnBlock as BlockCode,Grn.Pending as PendingQty,"+
                    " Grn.RejNo as RejecNo,Grn.RejDate as RejecDate,'Rejection' as GrnType,Grn.Rejflag as ReFlag,Grn.SlNo as Grnslno"+
                    " from grn"+
                    " inner join "+SSupTable+" on "+SSupTable+".ac_code = grn.sup_code"+
                    " inner join invitems on grn.code = invitems.item_code and invitems.hsntype=0"+
                    " inner join ordblock on grn.grnblock=ordblock.block"+
                    " where grn.Grnno in (select grnno from grn"+
                    " Where Grn.GrnDate >='"+TStDate.toNormal()+"' And "+
                    " Grn.GrnDate <= '"+TEnDate.toNormal()+"'"+
                    " and rejflag = 0 and Grn.millcode="+iMillCode+")"+
                    " and grn.millcode="+iMillCode+" and rejflag = 1)"+
                    " group by SupplierCode,Suppliername,GrnNum,OrdBlockName,grnDate,GInNum,"+
                    " GinDate,OrdNum,GrnInvNum,GrnInvDate,ItemName,InvQty,MilQty,"+
                    " BlockCode,PendingQty,RejecNo,RejecDate,GrnType,ReFlag,Grnslno Order by SupplierCode,"+
                    " GrnNum,grnDate,Grnslno,ReFlag,OrdBlockName,OrdNum,ItemName";
          
          QS1 =     " Select PurchaseOrder.OrderDate,Grn.GrnBlock, "+
                    " Grn.OrderNo From PurchaseOrder Inner Join GRN On "+
                    " (PurchaseOrder.OrderBlock = Grn.GrnBlock And "+
                    " PurchaseOrder.OrderNo = Grn.OrderNo and PurchaseOrder.SlNo = Grn.OrderSlNo) "+
                    " Where Grn.GrnDate >='"+TStDate.toNormal()+"' "+
                    " And Grn.GrnDate <= '"+TEnDate.toNormal()+"'"+
                    " And Grn.MillCode="+iMillCode+
                    " Order By 2,3 ";


          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();
               Statement      stat           = theConnection.createStatement();
               ResultSet      theResult      = stat.executeQuery(QS);

               while(theResult.next())
               {
                    VAcCode         . addElement(theResult.getString(1));
                    VAcName         . addElement(theResult.getString(2));
                    VGrnNo          . addElement(theResult.getString(3));
                    VGrnBlock       . addElement(theResult.getString(4));
                    VGrnDate        . addElement(theResult.getString(5));
                    VGateNo         . addElement(theResult.getString(6));
                    VGateDate       . addElement(theResult.getString(7));
                    VOrderNo        . addElement(theResult.getString(8));
                    VInvNo          . addElement(theResult.getString(9));
                    VInvDate        . addElement(theResult.getString(10));
                    VMatName        . addElement(theResult.getString(11));
                    VInvQty         . addElement(theResult.getString(12));
                    VRecQty         . addElement(theResult.getString(13));
                    VRejQty         . addElement(theResult.getString(14));
                    VAcQty          . addElement(theResult.getString(15));
                    VInvValue       . addElement(theResult.getString(16));
                    VBlock          . addElement(theResult.getString(17));
                    VOrdQty         . addElement(common.getRound(theResult.getString(18),2));
                    VRejNo          . addElement(common.parseNull(theResult.getString(19)));
                    VRejDate        . addElement(common.parseNull(theResult.getString(20)));
                    VGrnType        . addElement(common.parseNull(theResult.getString(21)));
               }
               theResult.close();
               
               ResultSet theResult1 = stat.executeQuery(QS1);
               while(theResult1.next())
               {
                    VXOrderDate     . addElement(theResult1.getString(1));    
                    VXOrderBlock    . addElement(theResult1.getString(2));    
                    VXOrderNo       . addElement(theResult1.getString(3));    
               }
               theResult1.close();
               stat.close();

               setIndexTable();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     
     private void setVectorIntoRowData()
     {
          RowData = new Object[VAcCode.size()][ColumnData.length];
          for(int i=0;i<VAcCode.size();i++)
          {
               String    SFinDelay      = "";
               String    SGateDelay     = common.getDateDiff(common.parseDate((String)VGrnDate.elementAt(i)),common.parseDate((String)VGateDate.elementAt(i)));
               String    SOrderDate     = common.parseDate(getOrdDate(i));
               
               RowData[i][0]            = ""+(String)VAcName                    . elementAt(i);
               RowData[i][1]            = ""+(String)VGrnType                   . elementAt(i);
               RowData[i][2]            = ""+(String)VGrnNo                     . elementAt(i);
               RowData[i][3]            = ""+(String)VGrnBlock                  . elementAt(i);
               RowData[i][4]            = ""+common.parseDate((String)VGrnDate  . elementAt(i));
               RowData[i][5]            = ""+getConStr((String)VRejNo           . elementAt(i));
               RowData[i][6]            = ""+common.parseDate((String)VRejDate  . elementAt(i));
               RowData[i][7]            = ""+(String)VOrderNo                   . elementAt(i);
               RowData[i][8]            = ""+SOrderDate;
               RowData[i][9]            = ""+(String)VInvNo                     . elementAt(i);
               RowData[i][10]           = common.parseDate((String)VInvDate     . elementAt(i));
               RowData[i][11]           = ""+(String)VMatName                   . elementAt(i);
               RowData[i][12]           = ""+(String)VAcQty                     . elementAt(i);
               RowData[i][13]           = ""+(String)VInvValue                  . elementAt(i);
               RowData[i][14]           = ""+SFinDelay;
               RowData[i][15]           = ""+SGateDelay;
          }
     }

     private void setPendingReport()
     {
          if(VAcCode.size() == 0)
               return;
          
          Lctr = 100;
          Pctr = 0;
          
          String    PAcCode   = (String)VAcCode.elementAt(0);
          String    SPGrnNo   = "";
          String    SPBlock   = "";
          String    SFile     = TFile.getText();
                    dInvValue = 0;
          
          if((SFile.trim()).length()==0)
               SFile = "1.prn";
          
          try
          {
               FW = new FileWriter(common.getPrintPath()+SFile);
               for(int i=0;i<VAcCode.size();i++)
               {
                    setHead((String)VAcName.elementAt(i));

                    String    SAcCode   = (String)VAcCode.elementAt(i);
                    String    SGrnNo    = (String)VGrnNo.elementAt(i);
                    String    SBlock    = (String)VGrnBlock.elementAt(i);
                    if(!SAcCode.equals(PAcCode))
                    {
                         setContBreak();
                         PAcCode   = SAcCode;
                         setFirstLine(0,(String)VAcName.elementAt(i));
                         dInvValue = 0;
                    }
                    if(SPGrnNo.equals(SGrnNo))
                         setBody(i,false);
                    else
                    {
                         setBody(i,true);
                    }
                    SPGrnNo   = SGrnNo;
                    SPBlock   = SBlock;
               }
               setContBreak();
               FW   . write(SHead8+"\n");
               new  ControlFigures(FW,iMillCode);
               FW   . write("< End of Report >\n\n");
               FW   . close();
          }
          catch(Exception ex){ System.out.println(ex);}
     }
     
     private void setHead(String SName) throws Exception
     {
          if (Lctr < 60)
               return;
          
          if (Pctr > 0)
               setPageBreak();
          
          Pctr++;

          String str1 = "gCompany  : "+SMillName;
          String str2 = "Document : Report on Supplierwise GRN's Passed During the Period "+TStDate.toString()+"-"+TEnDate.toString();
          String str3 = "Page No  : "+Pctr;
                         FW   . write(str1+"\n");
                         FW   . write(str2+"\n");
                         FW   . write(str3+"\n");
                         FW   . write(SHead1+"\n");
                         FW   . write(SHead2+"\n");
                         FW   . write(SHead3+"\n");
                         FW   . write(SHead4+"\n");
                         FW   . write(SHead5+"\n");
                    Lctr = 8;
          if(Pctr == 1)
               setFirstLine(0,SName);
     }

     private void setFirstLine(int i,String SName) throws Exception 
     {
          String str     = "";
          if(i==1)
               SName = SName+"(Contd...)";               

          String    strx      =    "| "+common.Pad(SName,10+3+10+3+10+3+10+3+10+3+10+3+4+10+9+9+4)+
                                   common.Rad(" ",10) +"   "+common.Pad(" ",10)+"   "+
                                   common.Pad(" ",40)+"   "+
                                   common.Rad(" ",10)+"   "+common.Rad(" ",10)+"   "+
                                   common.Rad(" ",10)+"   "+common.Rad(" ",10)+"   "+
                                   common.Rad(". ",10)+"   "+common.Rad(" ",12)+"   "+
                                   common.Rad(" ",4)+"   "+common.Rad(" ",6)+" |";
          
          FW   . write(SHead6+"\n");
          FW   . write(strx+"\n");
          FW   . write(SHead7+"\n");
          Lctr = Lctr+3;
     }

     private void setContBreak() throws Exception
     {
          if(Lctr == 8)
               return;
          FW.write(SHead5+"\n");
          
          String    str       =    "|"+common.Rad(" ",9)+"|"+common.Rad(" ",10)+"|"+common.Pad(" ",10)+"|"+common.Pad(" ",5)+"|"+
                                   common.Rad(" ",10)+"|"+common.Pad(" ",10)+"| "+common.Rad(" ",10)+" | "+common.Pad(" ",10)+" |"+
                                   common.Rad(" ",10)+"|"+common.Pad(" ",5)+"|"+common.Pad(" ",10)+"| "+
                                   common.Rad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                                   common.Pad("T O T A L",40)+" | "+
                                   common.Rad(" ",10)+" | "+common.Rad(" ",10)+" | "+
                                   common.Rad(" ",10)+" | "+common.Rad(" ",10)+" | "+
                                   common.Rad(" ",10)+" | "+common.Rad(common.getRound(dInvValue,2),12)+" | "+
                                   common.Rad(" ",4)+" | "+common.Rad(" ",6)+" |";
          
          FW   . write(str+"\n");
          FW   . write(SHead5+"\n");

          Lctr = Lctr+3;
     }

     private void setPageBreak() throws Exception
     {
          FW   . write(SHead8+"\n");
     }

     private void setBody(int i,boolean bFlag) throws Exception
     {
          if(Lctr == 8)
          {
               setFirstLine(1,(String)VAcName.elementAt(i));
               bFlag = true;
          }
          if(bFlag && Lctr > 8)
          {
               FW   . write(SHead9+"\n");
               Lctr++;
          }
          String    SGrnNo    = "",SGrnDate  = "",SGINo     = "",
                    SGIDate   = "",SInvNo    = "",SInvDate  = "",
                    SRejDate  ="", SRejNo    = "",SGrnType  = "",
                    SGrnBlock ="";

          if(bFlag)
          {
                    SGrnNo         = (String)VGrnNo    . elementAt(i);
                    SGrnDate       = common.parseDate((String)VGrnDate     . elementAt(i));
          }
                    SGINo          = (String)VGateNo   . elementAt(i);
                    SGIDate        = common.parseDate((String)VGateDate    . elementAt(i));
                    SInvNo         = (String)VInvNo    . elementAt(i);
                    SInvDate       = common.parseDate((String)VInvDate     . elementAt(i));
                    SRejDate       = common.parseDate((String)VRejDate     . elementAt(i));
                    SRejNo         =                   (String)VRejNo      . elementAt(i);
                    SGrnType       =                   (String)VGrnType    . elementAt(i);
                    SGrnBlock      =                   (String)VGrnBlock   . elementAt(i);

          String    SOrderNo       = (String)VOrderNo  . elementAt(i);
          String    SOrdBlock      = (String)VGrnBlock . elementAt(i);
          String    SOrderDate     = common.parseDate(getOrdDate(i));
          
          String    SMatName       = (String)VMatName.elementAt(i);
          
          String    SInvQty        = common.getRound((String)VInvQty  . elementAt(i),3);
          String    SRecQty        = common.getRound((String)VRecQty  . elementAt(i),3);        
          String    SRejQty        = common.getRound((String)VRejQty  . elementAt(i),3);        
          String    SAcQty         = common.getRound((String)VAcQty   . elementAt(i),3);         
          String    SOrdQty        = common.getRound((String)VOrdQty  . elementAt(i),3);         
          
          String    SInvValue      = common.getRound((String)VInvValue.elementAt(i),2);
          String    SFinDelay      = "";
          String    SGateDelay     = common.getDateDiff(common.parseDate((String)VGrnDate.elementAt(i)),common.parseDate((String)VGateDate.elementAt(i)));

                    dInvValue      = dInvValue+common.toDouble(SInvValue);
          
          String    str            =    "|"+common.Pad(SGrnType,9)+"|"+common.Pad(SGrnNo,10)+"|"+
                                        common.Pad(SGrnDate,10)+"|"+common.Pad(SGrnBlock,5)+"|"+common.Pad(" "+getConStr(SRejNo.trim()),10)+"|"+
                                        common.Pad(SRejDate,10)+"| "+
                                        common.Rad(SGINo,10)+" | "+common.Pad(SGIDate,10)+" |"+
                                        common.Rad(SOrderNo,10)+"|"+common.Pad(SOrdBlock,5)+"|"+
                                        common.Pad(SOrderDate,10)+"| "+common.Rad(SInvNo,10)+" | "+
                                        common.Pad(SInvDate,10)+" | "+common.Pad(SMatName,40)+" | "+
                                        common.Rad(SInvQty,10)+" | "+common.Rad(SRecQty,10)+" | "+
                                        common.Rad(SRejQty,10)+" | "+common.Rad(SAcQty,10)+" | "+
                                        common.Rad(SOrdQty,10)+" | "+common.Rad(SInvValue,12)+" | "+
                                        common.Rad(SFinDelay,4)+" | "+common.Rad(SGateDelay,6)+" |";
          
          FW   . write(str+"\n");
          Lctr++;
     }

     private void setIndexTable()
     {
          VXIndex = new Vector();
          for(int i=0;i<VXOrderBlock.size();i++)
          {
               String    str1      = (String)VXOrderBlock   . elementAt(i);
               String    str2      = (String)VXOrderNo      . elementAt(i);
                         VXIndex   . addElement(str1+"|"+str2);
          }
     }

     private int getOrderIndex(int i)
     {
          String    str1      = (String)VBlock    . elementAt(i);
          String    str2      = (String)VOrderNo  . elementAt(i);
          String    str3      = str1+"|"+str2;

          return VXIndex.indexOf(str3);
     }

     private String getOrdDate(int i)
     {
          int iIndex = getOrderIndex(i);
          if (iIndex == -1)
               return " ";
          return (String)VXOrderDate.elementAt(iIndex);
     }

     private String getConStr(String SRejNo)
     {
          String SConRejNo = "";

                    if(!SRejNo.equals("0"))
                         SConRejNo = SRejNo;

          return SConRejNo;
     }
}
