package GRN;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class SupplierSearch1 implements ActionListener
{
      JLayeredPane   Layer;
      JTextField     TSupCode;
      JButton        BName;
      String         SSupTable;
      JButton        BOk;
      Vector         VSupName,VSupCode;
      JTextField     TIndicator;          
      JList          BrowList;
	  JDialog        theDialog;
      JScrollPane    BrowScroll;
      JPanel         BottomPanel,SupplierPanel;
      JInternalFrame MediFrame;

      String str="";
      ActionEvent ae;
	  boolean QFFlag=false;
	  QuotationFrame QF;

      Common common = new Common();
	  

      SupplierSearch1(JLayeredPane Layer,JTextField TSupCode,Vector VSupName,Vector VSupCode,JDialog theDialog,JButton BName,String SSupTable)
      {
          this.Layer       = Layer;
          this.TSupCode    = TSupCode;
          this.VSupName    = VSupName;
          this.VSupCode    = VSupCode;
          this.theDialog   = theDialog;
          this.BName       = BName;
          this.SSupTable   = SSupTable;

          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator.setEditable(false);

          BrowList      = new JList(VSupName);

          BrowScroll    = new JScrollPane(BrowList);

          BottomPanel   = new JPanel(true);
		  SupplierPanel = new JPanel(true);
          BottomPanel.setLayout(new GridLayout(1,2));
          MediFrame     = new JInternalFrame("Supplier Selector");
          MediFrame.show();
          MediFrame.setBounds(80,100,550,350);
          MediFrame.setClosable(true);
          MediFrame.setResizable(true);
          BrowList.addKeyListener(new KeyList());
          MediFrame.getContentPane().setLayout(new BorderLayout());
          MediFrame.getContentPane().add("South",BottomPanel);
          MediFrame.getContentPane().add("Center",BrowScroll);
          BottomPanel.add(TIndicator);
          BottomPanel.add(BOk);

		  SupplierPanel.setLayout(new BorderLayout());
          SupplierPanel.add("South",BottomPanel);
          SupplierPanel.add("Center",BrowScroll);

          BOk.addActionListener(new ActList());
		  MediFrame.setVisible(false);
      }

	  public void setComponents()
	  {
		  try
		  {

			  TIndicator    = new JTextField();
			  BOk           = new JButton("Selection Over");
			  TIndicator.setEditable(false);

			  BrowList      = new JList(VSupName);
			  BrowScroll    = new JScrollPane(BrowList);

			  BottomPanel   = new JPanel(true);
			  SupplierPanel = new JPanel(true);
			  BottomPanel.setLayout(new GridLayout(1,2));
			  BrowList.addKeyListener(new KeyList());
			  SupplierPanel.setLayout(new BorderLayout());
			  SupplierPanel.add("South",BottomPanel);
			  SupplierPanel.add("Center",BrowScroll);
			  BottomPanel.add(TIndicator);
			  BottomPanel.add(BOk);
			  BOk.addActionListener(new ActList());

		  }
		  catch(Exception e){System.out.println("In SS SEt Components :"+e);}
	  }

      public void setPreset()
      {
          int index=0;
          for(index=0;index<VSupCode.size();index++)
          {
               String str1 = (String)VSupCode.elementAt(index);
               String str  = ((String)VSupName.elementAt(index)).toUpperCase();
               if(str1.startsWith(TSupCode.getText()))
               {
                    BrowList.setSelectedValue(str,true);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
          BrowList.requestFocus();
          BrowList.updateUI();
      }

      public void setDataIntoVector()
      {
               VSupName.removeAllElements();
               VSupCode.removeAllElements();
               String QString = "Select Name,Ac_Code From "+SSupTable+" Order By Name";
               try
               {
                    ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                    Connection      theConnection =  oraConnection.getConnection();
                    Statement       stat =  theConnection.createStatement();

                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         VSupName.addElement(res.getString(1));
                         VSupCode.addElement(res.getString(2));
                    }
                    res.close();
                    stat.close();
               }
               catch(Exception ex)
               {
                    System.out.println("In SDupplier sEarch :"+ex);
               }
      }
      public class ActList implements ActionListener
      {
          public void actionPerformed(ActionEvent e)
          {
               int index = BrowList.getSelectedIndex();
               setGrnDet(index);
               str="";
               removeHelpFrame();
          }
      }

      public void actionPerformed(ActionEvent ae)
      {
          this.ae = ae;  
          BName = (JButton)ae.getSource();
          removeHelpFrame();
          try
          {
               Layer.add(MediFrame);
               MediFrame.moveToFront();
               MediFrame.setSelected(true);
               MediFrame.show();
               BrowList.requestFocus();
               setPreset();
          }
          catch(java.beans.PropertyVetoException ex){}
      }  

      public class KeyList extends KeyAdapter
      {
             public void keyReleased(KeyEvent ke)
             {
                  char lastchar=ke.getKeyChar();
                  lastchar=Character.toUpperCase(lastchar);
                  try
                  {
                     if(ke.getKeyCode()==8)
                     {
                        str=str.substring(0,(str.length()-1));
                        setCursor();
                     }
                     else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar>='0' && lastchar <= '9'))
                     {
                        str=str+lastchar;
                        setCursor();
                     }
                  }
                  catch(Exception ex){}
             }
             public void keyPressed(KeyEvent ke)
             {
                  if(ke.getKeyCode()==116)    // F5 is pressed
                  {
                         setDataIntoVector();
                         BrowList.setListData(VSupName);
                  }
                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                     int index = BrowList.getSelectedIndex();
                     setGrnDet(index);
                     str="";
                     removeHelpFrame();
                  }
				  if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
                  {
                     //int index = BrowList.getSelectedIndex();
                     //setGrnDet(index);
                     str="";
                     removeHelpFrame();
                  }
             }
         }
         public void setCursor()
         {
            TIndicator.setText(str);
            int index=0;
            for(index=0;index<VSupName.size();index++)
            {
                 String str1 = ((String)VSupName.elementAt(index)).toUpperCase();
                 if(str1.startsWith(str))
                 {
                      BrowList.setSelectedValue(str1,true);
                      BrowList.ensureIndexIsVisible(index+10);
                      break;
                 }
            }
         }
         public void removeHelpFrame()
         {
            try
            {
               theDialog.setVisible(false); 
               Layer.remove(MediFrame);
               Layer.repaint();
               Layer.updateUI();
               ((JButton)ae.getSource()).requestFocus();
			   
            }
            catch(Exception ex) { }
         }
         public boolean setGrnDet(int index)
         {
               BName.setText((String)VSupName.elementAt(index));
               TSupCode.setText((String)VSupCode.elementAt(index));
			   try
			   {
					if(QFFlag)
						QF.setDataIntoVector();
			   }
			   catch(Exception e){}

//                  try{AF.setData();}catch(Exception e){}

               return true;
         } 
}
