package GRNTest;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGRNFrameN extends JInternalFrame
{
     DateField                TDate;
     JTextField               TSupCode, TSupData;
     WholeNumberField         TInvoice;
     DateField4                TInvDate, TDCDate;
     MyTextField              TInvNo, TDCNo;
     InwardModeModelN         InwMode;

     DirectGRNMiddlePanelN    MiddlePanel;
     JPanel                   TopPanel, BottomPanel;
     JButton                  BOk, BApply, BSupplier;
     
     JLayeredPane             DeskTop;
     Vector                   VCode, VName, VNameCode;
     StatusPanel              SPanel;
     int                      iMillCode, iUserCode, iAuthCode;
     String                   SYearCode;
     String                   SItemTable, SSupTable;

     JLabel                   LGrnNo;
     JComboBox                JCInwType, JCSort, JCEntryType;

     Vector                   VInwName, VInwNo;

     Connection               theMConnection = null;
     Connection               theDConnection = null;

     boolean                  bComflag       = true;
     Common                   common         = new Common();
     JComboBox       	      cmbInvoiceType ;
     Vector   				  VInvTypeCode, VInvTypeName, VEntryType, VEntryTypeCode,VEntryPartyTypeCode;
	 
	
	 int iInvSlNo = 0;
     int iEntryCount=0, iDivisionCode, iMaxLimit=0, iGSTPartyTypeCode;
     int iInvCheckCount		= -1, iTrialPendingCount=-1;

     String SGrnNo="", SGateNo="";

     String SSeleSupCode,SSeleSupType,SSeleStateCode;
     int iStateCheck = 0;
     int iTypeCheck  = 0;
     // set DirectGate Model  Index
     int ItemName=0, ItemCode=1, HSNCode=2, Qty=3, Rate=4, Value=5, DiscPer=6, Discount=7, BillValue=8, CGRatePer=9, CGValue=10, SGRatePer=11, SGValue=12, IGRatePer=13, IGValue=14, TotalValue=15, Select=16;   
     
	 int  iCODE=0, iNAME=1, iHSNCODE=2, iTAXTYPE=3, iBLOCK=4, iMRSNO=5, iORDERNO=6, iORDERQTY=7, iPENDINGQTY=8, iINVQTY=9, iRECDQTY=10, iINVRATE=11, iORDERRATE=12, iINVDISC=13, iORDERDISC=14, iCGST=15, iSGST=16, iIGST=17, iCESS=18, iBASIC=19, iDISCVAL=20, iCGSTVAL=21, iSGSTVAL=22, iIGSTVAL=23, iCESSVAL=24, iNET=25, iDEPARTMENT=26, iGROUP=27, iUNIT=28, iVAT=29;
	
	 java.sql.Connection 	theAlpaConnection = null;

     public DirectGRNFrameN(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iMillCode,int iAuthCode,int iUserCode,String SYearCode,String SItemTable,String SSupTable)
     {
          this.DeskTop            = DeskTop;
          this.VCode              = VCode;
          this.VName              = VName;
          this.VNameCode          = VNameCode;
          this.SPanel             = SPanel;
          this.iMillCode          = iMillCode;
          this.iAuthCode          = iAuthCode;
          this.iUserCode          = iUserCode;
          this.SYearCode          = SYearCode;
          this.SItemTable         = SItemTable;
          this.SSupTable          = SSupTable;
          this.iDivisionCode      = (iMillCode+1);

          if(iMillCode==1)
          {
               DORAConnection jdbc           = DORAConnection.getORAConnection();
                              theDConnection = jdbc.getConnection();
          }

          ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                         theMConnection = oraConnection.getConnection();

          setInwardData();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
   
		/*
           iInvCheckCount     =       setPendingInvCheck();
           iTrialPendingCount	= 	  getTrialPendingCount();


	if(iTrialPendingCount==0){	   
	  if(iInvCheckCount!=0){
          
          setInwardData();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
	 }
  else{
	return;
    }
  }
else{
	JOptionPane.showMessageDialog(null," Please Select Users for Corresponding TrialOrder (OR) FreeOrder","Information",JOptionPane.INFORMATION_MESSAGE);
	return;
 }
		*/
 
}

     public void createComponents()
     {
          BOk        = new JButton("Okay");
          BApply     = new JButton("Apply");
          BSupplier  = new JButton("Supplier");
          
          TDate       = new DateField();
          LGrnNo      = new JLabel();
          
          TSupCode    = new JTextField();
          TSupData    = new JTextField();
          TInvoice    = new WholeNumberField(2);

          TInvDate    = new DateField4();
          TDCDate     = new DateField4();
          TInvNo      = new MyTextField(25);
          TDCNo       = new MyTextField(15);

          JCInwType   = new JComboBox(VInwName);
          InwMode     = new InwardModeModelN(iUserCode,iAuthCode,iMillCode);
          JCSort      = new JComboBox();
          
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();
   		  setInvoiceTypeVector();
   	      cmbInvoiceType   = new JComboBox(VInvTypeName);
	      cmbInvoiceType   . setSelectedItem(VInvTypeName.get(0));
          
		  JCEntryType    = new JComboBox(VEntryType);
		  
          TDate.setTodayDate();
          
          TDate.setEditable(false);
          TSupCode.setEditable(false);

          JCInwType.setSelectedIndex(0);
          JCInwType.setEnabled(false);

          LGrnNo.setText("To be determined");

          InwMode.TCode.setText("11");
          InwMode.setText("RENGAVILAS");
     }

     public void setLayouts()
     {
          setTitle("Invoice Valuation of Materials recd against order");
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,1000,650);
          
          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(7,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());

          BSupplier.setBorder(new javax.swing.border.BevelBorder(0));
          BSupplier.setBackground(new Color(128,128,255));
          BSupplier.setForeground(Color.RED);

          TopPanel.setBorder(new TitledBorder(""));
     }

     public void addComponents()
     {
          try
          {
               JCSort.addItem("OrderNo");
               JCSort.addItem("Name");
               JCSort.addItem("Code");

               TopPanel.add(new JLabel("GRN No"));
               TopPanel.add(LGrnNo);
               
               TopPanel.add(new JLabel("GRN Date"));
               TopPanel.add(TDate);
               
               TopPanel.add(new JLabel("Supplier"));
               TopPanel.add(BSupplier);
               
               TopPanel.add(new JLabel("No. of Bills"));
               TopPanel.add(TInvoice);

               TopPanel.add(new JLabel("Invoice No"));
               TopPanel.add(TInvNo);
     
               TopPanel.add(new JLabel("Invoice Date"));
               TopPanel.add(TInvDate);
     
               TopPanel.add(new JLabel("DC No"));
               TopPanel.add(TDCNo);

               TopPanel.add(new JLabel("DC Date"));
               TopPanel.add(TDCDate);

               TopPanel.add(new JLabel("Select Inward Type"));
               TopPanel.add(JCInwType);

               TopPanel.add(new JLabel("Sorting Pending Orders By"));
               TopPanel.add(JCSort);

               TopPanel.add(new JLabel("Select Inward Mode"));
               TopPanel.add(InwMode);

               TopPanel.add(new JLabel("Gst Type"));
               TopPanel.add(JCEntryType);


               TopPanel.add(new JLabel("Invoice Type"));
               TopPanel.add(cmbInvoiceType);
			   
               TopPanel.add(new JLabel(""));
               TopPanel.add(BApply);
               
               BottomPanel.add(BOk);
               getContentPane().add(TopPanel,BorderLayout.NORTH);
          }
          catch(Exception e)
          {
               System.out.println(e);
          }
     }

     public void addListeners()
     {
          BApply.addActionListener(new ActList());
          BOk.addActionListener(new ActList());
          BSupplier.addActionListener(new GrnSupplierSearchN(DeskTop,TSupCode,SSupTable,TSupData, JCEntryType, VEntryType, VEntryTypeCode,VEntryPartyTypeCode));
		 /* TInvDate.TDay.addKeyListener(new KeyList());
		  TInvDate.TMonth.addKeyListener(new KeyList());
		  TInvDate.TYear.addKeyListener(new KeyList());*/

		  /*TInvDate.theDate.addKeyListener(new KeyList());
		  TDCDate.theDate.addKeyListener(new KeyList());*/
			TInvDate.theDate.addFocusListener(new FocusList());
			TDCDate.theDate.addFocusListener(new FocusList());

     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
                if(ae.getSource()==BApply)
                {
					setSupData();

				    if(iStateCheck==0)
				    {
						JOptionPane.showMessageDialog(null,"Party State Data Not Updated","Information",JOptionPane.INFORMATION_MESSAGE);
				    }

				    if(iTypeCheck==0)
				    {
						JOptionPane.showMessageDialog(null,"Party Gst Classification Not Updated","Information",JOptionPane.INFORMATION_MESSAGE);
				    }

				    if(iStateCheck==1 && iTypeCheck==1)
				    {
						setMiddlePanel();
				    }
                }

               if(ae.getSource()==BOk)
               {
                    BOk.setEnabled(false);

                    if(validateGRN())
                    {
					    TDate.setTodayDate();
                        insertGRNCollectedData();
						getACommit();
						cmbInvoiceType   . setSelectedItem(VInvTypeName.get(0));
					
                    }
                    else
                    {
                         BOk.setEnabled(true);
                    }
               }
          }
     }

	private class KeyList extends KeyAdapter
	{
		public void keyReleased(KeyEvent ke)
		{

			String SMsg = getValidDaysCount();
			if(SMsg.length() > 0)
			{
				JOptionPane.showMessageDialog(null,SMsg,"Dear user,",JOptionPane.ERROR_MESSAGE);
				BApply.setEnabled(false);
			}
			else	
			BApply.setEnabled(true);
		}	
	}

	 private class FocusList extends FocusAdapter
    {

        public void focusLost(FocusEvent fe)
        {
            
			String SMsg = getValidDaysCount();
			if(SMsg.length() > 0)
			{
				JOptionPane.showMessageDialog(null,SMsg,"Dear user,",JOptionPane.ERROR_MESSAGE);
				BApply.setEnabled(false);
			}
			else	
			BApply.setEnabled(true);
        }
    }

	
	private String getValidDaysCount()
	{
		String SMsg="";
		try
		{
			String SPartyCode = TSupCode.getText();

			int iGRNDate 		= common.toInt(TDate.toNormal());
			int iDcDate 		= common.toInt(TDCDate.toNormal());
			int iInvoiceDate 	= common.toInt(TInvDate.toNormal());
			SMsg		 		= common.getInvoiceValidDays(iGRNDate,iDcDate,iInvoiceDate,SPartyCode);

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			System.out.println("ex:"+ex);
		}
		return SMsg;
	}
    public void setSupData()
    {
	 iStateCheck = 0;
	 iTypeCheck  = 0;

	 SSeleSupCode   = "";
	 SSeleSupType   = "";
	 SSeleStateCode = "";

	 String Str = TSupData.getText();
	 StringTokenizer ST = new StringTokenizer(Str,"`");
	 
         while(ST.hasMoreElements())
	 {
              SSeleSupCode   = ST.nextToken();
              SSeleStateCode = ST.nextToken();
              SSeleSupType   = ST.nextToken();
	 }

         if(SSeleSupType.equals("") || SSeleSupType.equals("0"))
	 {
	      iTypeCheck = 0;
	 }
	 else
	 {
	      iTypeCheck = 1;
	 }

         if(SSeleStateCode.equals("") || SSeleStateCode.equals("0"))
	 {
	      iStateCheck = 0;
	 }
	 else
	 {
	      iStateCheck = 1;
	 }
    }

     private void setMiddlePanel()
     {
          String SSupCode = TSupCode.getText();
          String SSupName = BSupplier.getText();
          int iSortIndex  = JCSort.getSelectedIndex();

          if(SSupCode.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Please Select the Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
          }
          else
          {
               int iCount = checkPendingOrder(SSupCode);

               if(iCount<=0)
               {
                    JOptionPane.showMessageDialog(null,"There is no Pending Orders for this Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
               }

               BSupplier.setEnabled(false);
               BApply.setEnabled(false);
               JCInwType.setEnabled(false);
               JCSort.setEnabled(false);

              // MiddlePanel = new DirectGRNMiddlePanelN(DeskTop,iMillCode,SSupCode,SSupName,iSortIndex,SSeleSupType,SSeleStateCode);
               MiddlePanel = new DirectGRNMiddlePanelN(DeskTop,iMillCode,SSupCode,SSupName,iSortIndex,SSeleSupType,SSeleStateCode,VCode,VName,iUserCode,SItemTable);
               getContentPane().add(MiddlePanel,BorderLayout.CENTER);
               getContentPane().add(BottomPanel,BorderLayout.SOUTH);

               DeskTop.repaint();
               DeskTop.updateUI();
        }
    }

     private int checkPendingOrder(String SSupCode)
     {
          int iCount=0;

          try
          {
               String QS = " Select Count(*) from PurchaseOrder "+
                           " Where Sup_Code = '"+SSupCode+"' "+
                           " And InvQty < Qty And Authentication=1 "+
                           " And MillCode="+iMillCode;

               Statement       stat =  theMConnection.createStatement();
               
               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    iCount = res.getInt(1);    
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E1"+ex );
          }
          return iCount;
     }

     private boolean validateGRN()
     {
          iEntryCount=0;
          
          String SInvNo       = common.parseNull(TInvNo.getText().trim());
          String SInvDate     = TInvDate.toNormal();
          String SDCNo        = common.parseNull(TDCNo.getText().trim());
          String SDCDate      = TDCDate.toNormal();
          String SInwMode     = common.parseNull(InwMode.getText().trim());

		/*  if(!isBinNo())
          {
               JOptionPane.showMessageDialog(null,"Bin No Must Be Entered","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
		  */
          if(!SDCNo.equals(""))
          {
               if(common.toInt(SDCDate) ==0)
               {
                    JOptionPane.showMessageDialog(null,"Enter Dc Date ","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(common.toInt(SDCDate)>0)
          {
               if(SDCNo.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Enter Dc No ","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(!SInvNo.equals(""))
          {
               if(common.toInt(SInvDate) ==0)
               {
                    JOptionPane.showMessageDialog(null,"Enter Invoice Date ","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(common.toInt(SInvDate)>0)
          {
               if(SInvNo.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Enter Invoice No ","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(SDCNo.equals("") && SInvNo.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Select DC No Or Invoice No","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
               return false;

          }
          if(SInwMode.equals("Mode"))
          {
               JOptionPane.showMessageDialog(null,"Mode Is Not Select","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          int iInvoice = common.toInt(TInvoice.getText());
          if(iInvoice==0)
          {
               JOptionPane.showMessageDialog(null,"No of Bills Field is Empty","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          if(isGateNotFilled())
          {
               JOptionPane.showMessageDialog(null,"Some Gate Entries are Not Filled","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          if(isGateGstNotFilled())
          {
               JOptionPane.showMessageDialog(null,"Some Gate Entries are Not Filled","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          if(isGRNNotTicked())
          {
               JOptionPane.showMessageDialog(null,"Some GRN Entries are Not filled","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          if(isGstRateNotUpdated())
          {
               return false;
          }

          if(iEntryCount<=0)
          {
               JOptionPane.showMessageDialog(null,"No Entries Made","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
		  /*
		  if(isOrdRateEqualToInvRate()){
			  return false;
		  }
		  */
          /*String SAdd       = MiddlePanel.MiddlePanel.TAdd.getText();
          String SLess      = MiddlePanel.MiddlePanel.TLess.getText();
          double dRound     = common.toDouble(SAdd)-common.toDouble(SLess);

          if(dRound > 50)
          {
               JOptionPane.showMessageDialog(null,"Plus and Minus Amt Can't Exceed 50","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }*/

          return true;
     }
     
     private boolean isGateNotFilled()
     {
          if(MiddlePanel.GatePanel.GateModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.GatePanel.GateModel.getRows();i++)
               {
                    String SDesc = (String)MiddlePanel.GatePanel.GateModel.getValueAt(i,1);
                    String SQty  = (String)MiddlePanel.GatePanel.GateModel.getValueAt(i,2);

                    if(SDesc.equals("") || SQty.equals("") || common.toDouble(SQty)<=0)
                         return true;

                    iEntryCount++;

               }
          }
          return false;
     }
     
     
     private boolean isGateGstNotFilled()
     {
          if(MiddlePanel.GatePanelGst.GateModelGst.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.GatePanelGst.GateModelGst.getRows();i++)
               {
                    String SItemName = (String)MiddlePanel.GatePanelGst.GateModelGst.getValueAt(i,ItemName);
                    String SQty      = (String)MiddlePanel.GatePanelGst.GateModelGst.getValueAt(i,Qty);
                    String SHsnCode  = (String)MiddlePanel.GatePanelGst.GateModelGst.getValueAt(i,HSNCode);
                    Boolean bValue   = (Boolean)MiddlePanel.GatePanelGst.GateModelGst.getValueAt(i,Select);
                     
                    if(!bValue.booleanValue() || (SItemName.equals("") || SQty.equals("") || common.toDouble(SQty)<=0 || SHsnCode.equals("")))
                        return true;
                    
                    if(bValue.booleanValue() && (!SItemName.equals("") && !SQty.equals("") && common.toDouble(SQty)>0 && !SHsnCode.equals("")))
                        iEntryCount++;
                    
               }
          }
          return false;
     }
     
     private boolean isGRNNotTicked()
     {
          if(MiddlePanel.GrnPanel.GrnModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.GrnPanel.GrnModel.getRows();i++)
               {
                    String SQty    = (String)MiddlePanel.GrnPanel.GrnModel.getValueAt(i,8);
                    Boolean bValue = (Boolean)MiddlePanel.GrnPanel.GrnModel.getValueAt(i,10);
                    
                    if(common.toDouble(SQty)>0 && !bValue.booleanValue())
                         return true;

                    if(common.toDouble(SQty)>0 && bValue.booleanValue())
                         iEntryCount++;
               }
          }
          return false;
     }

     private boolean isGstRateNotUpdated()
     {
          if(MiddlePanel.MiddlePanel.dataModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.MiddlePanel.dataModel.getRows();i++)
               {
                    String SHsnCode = (String)MiddlePanel.MiddlePanel.dataModel.getValueAt(i,2);

		    int iRateCheck = getRateCheck(SHsnCode);

		    if(iRateCheck<=0)
		    {
			  JOptionPane.showMessageDialog(null,"Gst Rate Not Updated for HsnCode-"+SHsnCode,"Information",JOptionPane.INFORMATION_MESSAGE);
			  return true;
		    }
               }
          }
          return false;
     }
	 
     private boolean isOrdRateEqualToInvRate()
     {
		//int  iCODE=0, iNAME=1, iHSNCODE=2, iTAXTYPE=3, iBLOCK=4, iMRSNO=5, iORDERNO=6, iORDERQTY=7, iPENDINGQTY=8, iINVQTY=9, iRECDQTY=10, iORDERRATE=11, iINVRATE=12, iORDERDISC=14, iINVDISC=13, iCGST=15, iSGST=16, iIGST=17, iCESS=18, iBASIC=19, iDISCVAL=20, iCGSTVAL=21, iSGSTVAL=22, iIGSTVAL=23, iCESSVAL=24, iNET=25, iDEPARTMENT=26, iGROUP=27, iUNIT=28, iVAT=29;
		 
          if(MiddlePanel.MiddlePanel.dataModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.MiddlePanel.dataModel.getRows();i++)
               {
                    String SOrderRate = (String)MiddlePanel.MiddlePanel.dataModel.getValueAt(i,MiddlePanel.MiddlePanel.dataModel.iORDERRATE);
					String SInvRate   = (String)MiddlePanel.MiddlePanel.dataModel.getValueAt(i,MiddlePanel.MiddlePanel.dataModel.iINVRATE);

					String SOrdDisc   = (String)MiddlePanel.MiddlePanel.dataModel.getValueAt(i,MiddlePanel.MiddlePanel.dataModel.iORDERDISC);
					String SInvDisc   = (String)MiddlePanel.MiddlePanel.dataModel.getValueAt(i,MiddlePanel.MiddlePanel.dataModel.iINVDISC);

					if(common.toDouble(SOrderRate)!=common.toDouble(SInvRate)){
					  JOptionPane.showMessageDialog(null,"Order Rate is not Equal to Invoice Rate ","Information",JOptionPane.INFORMATION_MESSAGE);
					  return true;
					}
					
					if(common.toDouble(SOrdDisc)!=common.toDouble(SInvDisc)) {
					  JOptionPane.showMessageDialog(null,"Order Disc% is not Equal to Invoice Disc% ","Information",JOptionPane.INFORMATION_MESSAGE);
					  return true;
					}
               }
          }
		  
          return false;
     }
	 

     public int getRateCheck(String SHsnCode)
     {
		int iCount=0;

        try
        {
               Statement theStatement =  theMConnection.createStatement();

               String QS = "Select Count(*) from HsnGstRate Where HsnCode='"+SHsnCode+"'";

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    iCount = result.getInt(1);
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getRateCheck :"+ex);
        }
	return iCount;
     }

	 
	/*  private boolean isBinNo()
     {
          if(MiddlePanel.GrnPanel.GrnModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.GrnPanel.GrnModel.getRows();i++)
               {
                    String SBinNo  = (String)MiddlePanel.GrnPanel.GrnModel.getValueAt(i,9);
                    Boolean bValue = (Boolean)MiddlePanel.GrnPanel.GrnModel.getValueAt(i,10);
                    System.out.println("bValue:"+bValue.booleanValue());
					System.out.println("SBinN0 INt:"+SBinNo);
					
					if(bValue.booleanValue())
					{
					
						if(!SBinNo.equals(""))
							return true;
						else
							return false;
					}

                }
          }
          return false;
     }*/

     private void insertGRNCollectedData()
     {
		try
		{
			  if(MiddlePanel.MiddlePanel.dataModel.getRows()>0 || MiddlePanel.GatePanel.GateModel.getRows()>0 || MiddlePanel.GatePanelGst.GateModelGst.getRows()>0 )
			  {
				 setGateNo();
			  }

	          if(MiddlePanel.MiddlePanel.dataModel.getRows()>0)
	          {
		       	   setGRNNo();
	               insertGRNDetails();
				   insertInvoiceGRNDetails();
	               insertGateDetails();
	               setOrderLink();
	               setMRSLink();

	               if(iMillCode==1)
	               {
	                    updateSubStoreMasterData();
	               }
	          }

	          if(MiddlePanel.GatePanel.GateModel.getRows()>0)
	          {
	               insertDirectGateDetails();
	          }
			  
	          if(MiddlePanel.GatePanelGst.GateModelGst.getRows()>0)
	          {
	               insertDirectGateInwDetails();
                   insertData();
	          }

		}
		catch(Exception ex)
          {
               System.out.println("I1 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

     private void setGRNNo()
     {
          SGrnNo="";

          String QS ="";
          try
          {
               QS = "  Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 2 for update of MaxNo noWait";

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               Statement       stat =  theMConnection.createStatement();
               
               PreparedStatement thePrepare = theMConnection.prepareStatement("Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 2"); 

               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    SGrnNo = res.getString(1);    
               }
               res.close();

               thePrepare.setInt(1,common.toInt(SGrnNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println("E2"+ex );
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    setGRNNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
     }

     private void setGateNo()
     {
          SGateNo="";

          String QS ="";
          try
          {
               QS = " Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 3 for update of MaxNo noWait";

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               Statement       stat =  theMConnection.createStatement();

               PreparedStatement thePrepare = theMConnection.prepareStatement(" Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 3"); 
               
               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    SGateNo = res.getString(1);    
               }
               res.close();

               thePrepare.setInt(1,common.toInt(SGateNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println("E3"+ex );
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    setGateNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
     }

     public void insertGRNDetails()
     {
		 					
          String QString = "Insert Into GRN (OrderNo,MRSNo,GrnNo,GrnDate,GrnBlock,Sup_Code,GateInNo,GateInDate,InvNo,InvDate,DcNo,DcDate,Code,InvQty,MillQty,Pending,OrderQty,Qty,InvRate,DiscPer,Disc,CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,Cess,CessVal,InvAmount,InvNet,plus,Less,Misc,Dept_Code,Group_Code,Unit_Code,InvSlNo,ActualModVat,NoOfBills,MillCode,id,slno,mrsslno,mrsauthusercode,orderslno,orderapprovalstatus,taxclaimable,usercode,creationdate,entrystatus,grnqty,grnvalue,HsnCode,GstRate,GstPartyTypeCode,GstStateCode,BinNo,InvoiceType) Values (";
          String QSL     = "Select Max(InvSlNo) From GRN";
          String QS="";
          //int iInvSlNo=0,iSlNo=0;
		  int iSlNo=0;
			  iInvSlNo = 0;
	    int iInvoiceTypeCode =-1;

          try
          {

               Statement       stat =  theMConnection.createStatement();
               ResultSet res        = stat.executeQuery(QSL);
               while(res.next())
               {
                    iInvSlNo = res.getInt(1);
               }
               res.close();
               iInvSlNo++;
			   

               Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();
               Object GrnData[][] = MiddlePanel.GrnPanel.getFromVector();
               
               String SAdd        = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess       = MiddlePanel.MiddlePanel.TLess.getText();
               double dOthers     = common.toDouble(MiddlePanel.MiddlePanel.LOthers.getText());
               
               double dpm         = common.toDouble(SAdd)-common.toDouble(SLess)+dOthers;

               double dNet       = common.toDouble(MiddlePanel.MiddlePanel.LNet.getText())-dpm;
               double dRatio     = dpm/dNet;
               
               String SInvNo       = common.parseNull(TInvNo.getText().trim());
               String SInvDate     = TInvDate.toNormal();
               String SDCNo        = common.parseNull(TDCNo.getText().trim());
               String SDCDate      = TDCDate.toNormal();

               String SSupCode = TSupCode.getText();
               String SDate    = TDate.toNormal();

               String SDateTime = common.getServerDateTime();

               String SEntryStatus = "1";
				
				int iQualityCheckSms = 0;

               for(int i=0;i<RowData.length;i++)
               {
                    String SSeleId = (String)MiddlePanel.MiddlePanel.VSeleId.elementAt(i);

                    int iMidRow       = getRowIndex(SSeleId,GrnData);

		    String SHsnType   = (String)MiddlePanel.MiddlePanel.VHsnType.elementAt(i);

                    String SBlockCode     = "";
                    String SOrdQty        = "";
                    String SMrsSLNo       = "";
                    String SMrsUserCode   = "";
                    String SOrderSLNo     = "";
                    String STaxC          = "";
                    String SOrderApproval = "";
					String SBinNo         = "";

				//if(iQualityCheckSms==0){
					//iQualityCheckSms = MiddlePanel.getQualityCheckStatus(iMidRow);
				//}

		    if(SHsnType.equals("1"))
		    {
		            SBlockCode     = "0";
		            SOrdQty        = "0";
		            SMrsSLNo       = "0";
		            SMrsUserCode   = "0";
		            SOrderSLNo     = "0";
		            STaxC          = "0";
		            SOrderApproval = "0";
					SBinNo         = "0";
		    }
		    else
		    {
		            SBlockCode     = MiddlePanel.getBlockCode(iMidRow);
		            SOrdQty        = MiddlePanel.getOrdQty(iMidRow);
		            SMrsSLNo       = MiddlePanel.getMrsSLNo(iMidRow);
		            SMrsUserCode   = MiddlePanel.getMrsUserCode(iMidRow);
		            SOrderSLNo     = MiddlePanel.getOrderSLNo(iMidRow);
		            STaxC          = MiddlePanel.getTaxC(iMidRow);
		            SOrderApproval = MiddlePanel.getOrderApproval(iMidRow);
					SBinNo         = (String)MiddlePanel.GrnPanel.GrnModel.getValueAt(iMidRow,9);
			}


                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String sInvTypeName		= (String)cmbInvoiceType.getSelectedItem();
					iInvoiceTypeCode  = getInvTypeCode(sInvTypeName);
             
                    iSlNo++;
					
//	 int  iCODE=0, iNAME=1, iHSNCODE=2, iTAXTYPE=3, iBLOCK=4, iMRSNO=5, iORDERNO=6, iORDERQTY=7, iPENDINGQTY=8, iINVQTY=9, iRECDQTY=10, iORDERRATE=11, iINVRATE=12, iORDERDISC=14, iINVDISC=13, iCGST=15, iSGST=16, iIGST=17, iCESS=18, iBASIC=19, iDISCVAL=20, iCGSTVAL=21, iSGSTVAL=22, iIGSTVAL=23, iCESSVAL=24, iNET=25, iDEPARTMENT=26, iGROUP=27, iUNIT=28, iVAT=29;
                    
                    String SOrderNo     = (String)RowData[i][iORDERNO];
                    String SMRSNo       = (String)RowData[i][iMRSNO];
                    
                    String  SItemCode  = (String)RowData[i][iCODE];
                    String  SHsnCode   = ((String)RowData[i][iHSNCODE]).trim();
                    
                    String  SBasic     = (String)RowData[i][iBASIC];
                    String  SInvAmount = (String)RowData[i][iNET];

		    String SMisc      = "0";

		    if(SHsnType.equals("0"))
		    {
                         SMisc = common.getRound(common.toDouble(SInvAmount)*dRatio,3);
		    }

                    String  SMillQty   = (String)RowData[i][iRECDQTY];
                    
                    String SGrnQty   = SMillQty;
                    String SGrnValue = "";
                    
                    SGrnValue = common.getRound(common.toDouble(SInvAmount) + common.toDouble(SMisc),2);

		    String SCGSTPer   = common.getRound(common.toDouble(((String)RowData[i][iCGST]).trim()),2);
		    String SSGSTPer   = common.getRound(common.toDouble(((String)RowData[i][iSGST]).trim()),2);
		    String SIGSTPer   = common.getRound(common.toDouble(((String)RowData[i][iIGST]).trim()),2);
		    String SGstRate   = common.getRound(common.toDouble(SCGSTPer)+common.toDouble(SSGSTPer)+common.toDouble(SIGSTPer),2);
                    
//	 int  iCODE=0, iNAME=1, iHSNCODE=2, iTAXTYPE=3, iBLOCK=4, iMRSNO=5, iORDERNO=6, iORDERQTY=7, iPENDINGQTY=8, iINVQTY=9, iRECDQTY=10, iORDERRATE=11, iINVRATE=12, iORDERDISC=14, iINVDISC=13, iCGST=15, iSGST=16, iIGST=17, iCESS=18, iBASIC=19, iDISCVAL=20, iCGSTVAL=21, iSGSTVAL=22, iIGSTVAL=23, iCESSVAL=24, iNET=25, iDEPARTMENT=26, iGROUP=27, iUNIT=28, iVAT=29;
					
                    String    QS1 = QString;
                              QS1 = QS1+"0"+SOrderNo+",";
                              QS1 = QS1+"0"+SMRSNo+",";
                              QS1 = QS1+"0"+SGrnNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"0"+SBlockCode+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"0"+SGateNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"'"+SInvNo+"',";
                              QS1 = QS1+"'"+SInvDate+"',";
                              QS1 = QS1+"'"+SDCNo+"',";
                              QS1 = QS1+"'"+SDCDate+"',";
                              QS1 = QS1+"'"+SItemCode+"',";
                              QS1 = QS1+"0"+(String)RowData[i][iINVQTY]+",";
                              QS1 = QS1+"0"+SMillQty+",";
                              QS1 = QS1+"0"+(String)RowData[i][iPENDINGQTY]+",";
                              QS1 = QS1+"0"+SOrdQty+",";
                              QS1 = QS1+"0"+SMillQty+",";
                              QS1 = QS1+"0"+(String)RowData[i][iORDERRATE]+",";	//iORDERRATE
                              QS1 = QS1+"0"+(String)RowData[i][iORDERDISC]+",";
                              QS1 = QS1+"0"+(String)RowData[i][iDISCVAL]+",";
                              QS1 = QS1+"0"+SCGSTPer+",";
                              QS1 = QS1+"0"+(String)RowData[i][iCGSTVAL]+",";
                              QS1 = QS1+"0"+SSGSTPer+",";
                              QS1 = QS1+"0"+(String)RowData[i][iSGSTVAL]+",";
                              QS1 = QS1+"0"+SIGSTPer+",";
                              QS1 = QS1+"0"+(String)RowData[i][iIGSTVAL]+",";
                              QS1 = QS1+"0"+(String)RowData[i][iCESS]+",";
                              QS1 = QS1+"0"+(String)RowData[i][iCESSVAL]+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SAdd+",";
                              QS1 = QS1+"0"+SLess+",";
                              QS1 = QS1+"0"+SMisc+",";
                              QS1 = QS1+"0"+SDeptCode+",";
                              QS1 = QS1+"0"+SGroupCode+",";
                              QS1 = QS1+"0"+SUnitCode+",";
                              QS1 = QS1+"0"+iInvSlNo+",";
                              QS1 = QS1+"0"+0+",";
                              QS1 = QS1+"0"+TInvoice.getText()+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"grn_seq.nextval"+",";
                              QS1 = QS1+"0"+iSlNo+",";
                              QS1 = QS1+"0"+SMrsSLNo+",";
                              QS1 = QS1+"0"+SMrsUserCode+",";
                              QS1 = QS1+"0"+SOrderSLNo+",";
                              QS1 = QS1+"0"+SOrderApproval+",";
                              QS1 = QS1+"0"+STaxC+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+SDateTime+"',";
                              QS1 = QS1+"0"+SEntryStatus+",";
                              QS1 = QS1+"0"+SGrnQty+",";
                              QS1 = QS1+"0"+SGrnValue+",";
                    	      QS1 = QS1+"'"+SHsnCode+"',";
                    	      QS1 = QS1+"0"+SGstRate+",";
                    	      QS1 = QS1+"0"+SSeleSupType+",";
                    	      QS1 = QS1+"'"+SSeleStateCode+"',";
			      QS1 = QS1+"'"+SBinNo+"',";
			      QS1 = QS1+"0"+iInvoiceTypeCode+ ")";

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.executeUpdate(QS1);

		    if(SHsnType.equals("0"))
		    {
                updateItemMaster(stat, SItemCode, SBlockCode, SGrnQty, SGrnValue, SBinNo);
				updateHsnMaster(stat, SItemCode, SHsnCode);
		    }

                    String SMsg = " Ur Goods Received GrnNo - "+SGrnNo+" , ItemName - "+(String)RowData[i][iNAME]+" ";

//                    insertSMS(SMsg,SMrsUserCode);

		    if(SHsnType.equals("0"))
		    {
		            if(common.toInt(SBlockCode)<=1)
		            {
		                 updateUserItemStock(stat,SItemCode,SGrnQty,SGrnValue,SMrsUserCode);
		            } 
		    }
               }
               stat.close();
			   
			if(iInvoiceTypeCode==2){
				setSupDetails();
				setInsertDCData(SSupCode, SDCNo,SGrnNo, SDCDate);
  	    	} 
			
			
			//Send Sms to QAD
			if(iQualityCheckSms==1){
				SendSms(SGrnNo);
			}
			

          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

	private void SendSms(String SGrnNo){
		String SSMgs = " Quality Check Grn No : "+SGrnNo;
		
		String SQsb = " insert into  SCM.TBL_ALERT_COLLECTION ( COLLECTIONID, STATUSID, ALERTMESSAGE, CUSTOMERCODE, STATUS ) "+
					  " values (scm.AlertCollection_Seq.nextval, 'STATUS324', '"+SSMgs+"', '9791', 0) ";
		
		
		try{
			ORAConnection3 jdbc = ORAConnection3.getORAConnection();
			Connection theScmConnection = jdbc.getConnection();
			
			Statement stat = theScmConnection.createStatement();
					  stat . executeQuery(SQsb);
					  stat . close();
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}


	
	private String getInvoiceRateNetAmount(Object[][] RowData, int iOthers) {
		
		double dTotal = 0;
		
		double dQty     = 0, dRate    = 0, dDiscPer = 0;
		
		double dCGST    = 0, dSGST=0, dIGST=0, dCESS=0;
		double dGross   = 0, dDisc    = 0, dBasic   = 0, dCGSTVal = 0, dSGSTVal = 0, dIGSTVal = 0, dCessVal = 0, dNet     = 0;
		
	    for(int i=0;i<RowData.length;i++)
	    {
			String SHsnType  = (String)MiddlePanel.MiddlePanel.VHsnType.elementAt(i);
			dQty     = 0;
            dRate    = 0;
            dDiscPer = 0;

			dCGST    = 0;
			dSGST	 = 0;
			dIGST	 = 0;
			dCESS	 = 0;

            dGross   = 0;
            dDisc    = 0;
	      	dBasic   = 0;
            dCGSTVal = 0;
            dSGSTVal = 0;
            dIGSTVal = 0;
            dCessVal = 0;
            dNet     = 0;
			
			dQty     = 0;
			
			if(common.toInt(SHsnType)==0){
				dQty     = common.toDouble((String)RowData[i][iRECDQTY]);
			}else{
				dQty     = 1;
			}
			
            dRate    = common.toDouble((String)RowData[i][iINVRATE]);	//Inv Rate
            dDiscPer = common.toDouble((String)RowData[i][iINVDISC]);	//Inv Disc Per

			dCGST    = common.toDouble(((String)RowData[i][iCGST]).trim());
			dSGST    = common.toDouble(((String)RowData[i][iSGST]).trim());
			dIGST    = common.toDouble(((String)RowData[i][iIGST]).trim());
			dCESS    = common.toDouble(((String)RowData[i][iCESS]).trim());

            dGross   = common.toDouble(common.getRound((dQty*dRate),2));
            dDisc    = common.toDouble(common.getRound((dGross*dDiscPer/100),2));
	      	dBasic   = common.toDouble(common.getRound((dGross-dDisc),2));
            dCGSTVal = common.toDouble(common.getRound((dBasic*dCGST/100),2));
            dSGSTVal = common.toDouble(common.getRound((dBasic*dSGST/100),2));
            dIGSTVal = common.toDouble(common.getRound((dBasic*dIGST/100),2));
            dCessVal = common.toDouble(common.getRound((dBasic*dCESS/100),2));
            dNet     = common.toDouble(common.getRound((dBasic+dCGSTVal+dSGSTVal+dIGSTVal+dCessVal),2));
			
			if(iOthers==common.toInt(SHsnType)) {
				dTotal += dNet;
			}
		}
	
		return common.getRound(dTotal,2);
	}

	

    //Invoice Grn Details
    public void insertInvoiceGRNDetails() {
		 					
          String QString = " Insert Into InvoiceGRN(OrderNo,MRSNo,GrnNo,GrnDate,GrnBlock,Sup_Code,GateInNo,GateInDate,InvNo,InvDate,DcNo,DcDate,Code,InvQty,MillQty,Pending,OrderQty,Qty,InvRate,DiscPer,Disc,CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,Cess,CessVal,InvAmount,InvNet,plus,Less,Misc,Dept_Code,Group_Code,Unit_Code,InvSlNo,ActualModVat,NoOfBills,MillCode,id,slno,mrsslno,mrsauthusercode,orderslno,orderapprovalstatus,taxclaimable,usercode,creationdate,entrystatus,grnqty,grnvalue,HsnCode,GstRate,GstPartyTypeCode,GstStateCode,BinNo,InvoiceType) Values ( ";
          
          String QS="";
          int iSlNo=0;
	      int iInvoiceTypeCode =-1;
		  
		  double dPartyDebitAmt = 0;

          try{
               Statement       stat =  theMConnection.createStatement();
			   

               Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();
               Object GrnData[][] = MiddlePanel.GrnPanel.getFromVector();
               
               //String SAdd        = MiddlePanel.MiddlePanel.TAdd.getText();
               //String SLess       = MiddlePanel.MiddlePanel.TLess.getText();
			   
               String SAdd        = "0";
               String SLess       = "0";
               double dOthers     = common.toDouble(getInvoiceRateNetAmount(RowData,1));
			   //double dOthers     = common.toDouble(MiddlePanel.MiddlePanel.LOthers.getText());
/*
               double dOthers     = common.toDouble(MiddlePanel.MiddlePanel.LOthers.getText());
               
               double dpm         = common.toDouble(SAdd)-common.toDouble(SLess)+dOthers;

               double dNet       = common.toDouble(MiddlePanel.MiddlePanel.LNet.getText())-dpm;
               double dRatio     = dpm/dNet;
*/			   
			   
			   System.out.println("Insert Invoice Grn dOthers=="+dOthers);
               
			   double dTNet  	  = common.toDouble(getInvoiceRateNetAmount(RowData,0));
			   
			   double dRoundOff   = common.toDouble(common.getRound((Math.round((dTNet+dOthers))-(dTNet+dOthers)),2));
			   
			  if(dRoundOff>0){
                SAdd        = String.valueOf(dRoundOff);
                SLess       = "0";
			  }else if(dRoundOff<0){
                SAdd        = "0";
                SLess       = String.valueOf((dRoundOff*-1));
			  }
			   
			   System.out.println("Invoice Grn dTNet="+dTNet+"=dOthers="+dOthers+"=SAdd=="+SAdd+"=SLess="+SLess+"=");
			   
               double dpm         = common.toDouble(SAdd)-common.toDouble(SLess)+dOthers;
               double dNet        = dTNet-dpm;
               //double dNet       = common.toDouble(MiddlePanel.MiddlePanel.LNet.getText())-dpm;
               double dRatio      = dpm/dTNet;
               
			   System.out.println("Insert Invoice Grn dTNet=="+dTNet+"=dpm="+dpm+"=dRatio="+dRatio);
			   
               String SInvNo       = common.parseNull(TInvNo.getText().trim());
               String SInvDate     = TInvDate.toNormal();
               String SDCNo        = common.parseNull(TDCNo.getText().trim());
               String SDCDate      = TDCDate.toNormal();

               String SSupCode = TSupCode.getText();
               String SDate    = TDate.toNormal();

               String SDateTime = common.getServerDateTime();

               String SEntryStatus = "1";

               for(int i=0;i<RowData.length;i++)
               {
                    String SSeleId = (String)MiddlePanel.MiddlePanel.VSeleId.elementAt(i);

                    int iMidRow       = getRowIndex(SSeleId,GrnData);

					String SHsnType   = (String)MiddlePanel.MiddlePanel.VHsnType.elementAt(i);

                    String SBlockCode     = "";
                    String SOrdQty        = "";
                    String SMrsSLNo       = "";
                    String SMrsUserCode   = "";
                    String SOrderSLNo     = "";
                    String STaxC          = "";
                    String SOrderApproval = "";
					String SBinNo         = "";

				if(SHsnType.equals("1"))
				{
		            SBlockCode     = "0";
		            SOrdQty        = "0";
		            SMrsSLNo       = "0";
		            SMrsUserCode   = "0";
		            SOrderSLNo     = "0";
		            STaxC          = "0";
		            SOrderApproval = "0";
					SBinNo         = "0";
				}
				else
				{
		            SBlockCode     = MiddlePanel.getBlockCode(iMidRow);
		            SOrdQty        = MiddlePanel.getOrdQty(iMidRow);
		            SMrsSLNo       = MiddlePanel.getMrsSLNo(iMidRow);
		            SMrsUserCode   = MiddlePanel.getMrsUserCode(iMidRow);
		            SOrderSLNo     = MiddlePanel.getOrderSLNo(iMidRow);
		            STaxC          = MiddlePanel.getTaxC(iMidRow);
		            SOrderApproval = MiddlePanel.getOrderApproval(iMidRow);
                     	    SBinNo = (String)MiddlePanel.GrnPanel.GrnModel.getValueAt(iMidRow,9);
	            }


                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String sInvTypeName		= (String)cmbInvoiceType.getSelectedItem();
					iInvoiceTypeCode  = getInvTypeCode(sInvTypeName);
             
                    iSlNo++;
					
//	 int  iCODE=0, iNAME=1, iHSNCODE=2, iTAXTYPE=3, iBLOCK=4, iMRSNO=5, iORDERNO=6, iORDERQTY=7, iPENDINGQTY=8, iINVQTY=9, iRECDQTY=10, iORDERRATE=11, iINVRATE=12, iORDERDISC=14, iINVDISC=13, iCGST=15, iSGST=16, iIGST=17, iCESS=18, iBASIC=19, iDISCVAL=20, iCGSTVAL=21, iSGSTVAL=22, iIGSTVAL=23, iCESSVAL=24, iNET=25, iDEPARTMENT=26, iGROUP=27, iUNIT=28, iVAT=29;
                    
                    String SOrderNo     = (String)RowData[i][iORDERNO];
                    String SMRSNo       = (String)RowData[i][iMRSNO];
                    
                    String  SItemCode  = (String)RowData[i][iCODE];
                    String  SHsnCode   = ((String)RowData[i][iHSNCODE]).trim();
                    
					String SCGSTPer   = common.getRound(common.toDouble(((String)RowData[i][iCGST]).trim()),2);
					String SSGSTPer   = common.getRound(common.toDouble(((String)RowData[i][iSGST]).trim()),2);
					String SIGSTPer   = common.getRound(common.toDouble(((String)RowData[i][iIGST]).trim()),2);
					String SCESSPer   = common.getRound(common.toDouble(((String)RowData[i][iCESS]).trim()),2);
					
					String SGstRate   = common.getRound(common.toDouble(SCGSTPer)+common.toDouble(SSGSTPer)+common.toDouble(SIGSTPer),2);
					
				    double dRate       = common.toDouble((String)RowData[i][iINVRATE]);	//Invoice Rate
				    double dDiscPer    = common.toDouble((String)RowData[i][iINVDISC]);	//Invoice Disc Per
					double dQty        = 0;
					
					if(SHsnType.equals("0")) {
						dQty        = common.toDouble((String)RowData[i][iRECDQTY]);
					}else{
						dQty        = 1;
					}
					
				    double dGross   = common.toDouble(common.getRound((dQty*dRate),2));
				    double dDisc    = common.toDouble(common.getRound((dGross*dDiscPer/100),2));
				    double dBasic   = common.toDouble(common.getRound((dGross - dDisc),2));
				    double dCGSTVal = common.toDouble(common.getRound((dBasic*common.toDouble(SCGSTPer)/100),2));
				    double dSGSTVal = common.toDouble(common.getRound((dBasic*common.toDouble(SSGSTPer)/100),2));
				    double dIGSTVal = common.toDouble(common.getRound((dBasic*common.toDouble(SIGSTPer)/100),2));
				    double dCessVal = common.toDouble(common.getRound((dBasic*common.toDouble(SCESSPer)/100),2));
				    double dInvAmt  = common.toDouble(common.getRound((dBasic+dCGSTVal+dSGSTVal+dIGSTVal+dCessVal),2));
					
					if(dRate!=common.toDouble((String)RowData[i][iORDERRATE]) || dDiscPer!=common.toDouble((String)RowData[i][iORDERDISC])) {
						dPartyDebitAmt += (dInvAmt-common.toDouble((String)RowData[i][iNET]));
					}
					
                    String  SBasic     = common.getRound(dBasic,3);
                    String  SInvAmount = common.getRound(dInvAmt,3);

					String SMisc       = "0";

					if(SHsnType.equals("0"))
					{
                        SMisc = common.getRound(common.toDouble(SInvAmount)*dRatio,3);
					}
					
					
					System.out.println(SItemCode+"=Insert Invoice Grn SMisc SInvAmount=="+SInvAmount+"=dRatio="+dRatio);
					
                    String SMillQty  = (String)RowData[i][iRECDQTY];
                    
                    String SGrnQty   = SMillQty;
                    String SGrnValue = "";
                    
                    SGrnValue = common.getRound(common.toDouble(SInvAmount) + common.toDouble(SMisc),2);

                    
//int  iCODE=0, iNAME=1, iHSNCODE=2, iTAXTYPE=3, iBLOCK=4, iMRSNO=5, iORDERNO=6, iORDERQTY=7, iPENDINGQTY=8, iINVQTY=9, iRECDQTY=10, iORDERRATE=11, iINVRATE=12, iORDERDISC=14, iINVDISC=13, iCGST=15, iSGST=16, iIGST=17, iCESS=18, iBASIC=19, iDISCVAL=20, iCGSTVAL=21, iSGSTVAL=22, iIGSTVAL=23, iCESSVAL=24, iNET=25, iDEPARTMENT=26, iGROUP=27, iUNIT=28, iVAT=29;
					
                    String    QS1 = QString;
                              QS1 = QS1+"0"+SOrderNo+",";
                              QS1 = QS1+"0"+SMRSNo+",";
                              QS1 = QS1+"0"+SGrnNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"0"+SBlockCode+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"0"+SGateNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"'"+SInvNo+"',";
                              QS1 = QS1+"'"+SInvDate+"',";
                              QS1 = QS1+"'"+SDCNo+"',";
                              QS1 = QS1+"'"+SDCDate+"',";
                              QS1 = QS1+"'"+SItemCode+"',";
                              QS1 = QS1+"0"+(String)RowData[i][iINVQTY]+",";
                              QS1 = QS1+"0"+SMillQty+",";
                              QS1 = QS1+"0"+(String)RowData[i][iPENDINGQTY]+",";
                              QS1 = QS1+"0"+SOrdQty+",";
                              QS1 = QS1+"0"+SMillQty+",";
                              QS1 = QS1+"0"+common.getRound(dRate,3)+",";	//Invoice Rate
                              QS1 = QS1+"0"+common.getRound(dDiscPer,3)+","; //Invoice Disc %
                              QS1 = QS1+"0"+common.getRound(dDisc,3)+",";
                              QS1 = QS1+"0"+SCGSTPer+",";
                              QS1 = QS1+"0"+common.getRound(dCGSTVal,3)+",";
                              QS1 = QS1+"0"+SSGSTPer+",";
                              QS1 = QS1+"0"+common.getRound(dSGSTVal,3)+",";
                              QS1 = QS1+"0"+SIGSTPer+",";
                              QS1 = QS1+"0"+common.getRound(dIGSTVal,3)+",";
                              QS1 = QS1+"0"+SCESSPer+",";
                              QS1 = QS1+"0"+common.getRound(dCessVal,3)+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SInvAmount+",";
                              QS1 = QS1+"0"+SAdd+",";
                              QS1 = QS1+"0"+SLess+",";
                              QS1 = QS1+"0"+SMisc+",";
                              QS1 = QS1+"0"+SDeptCode+",";
                              QS1 = QS1+"0"+SGroupCode+",";
                              QS1 = QS1+"0"+SUnitCode+",";
                              QS1 = QS1+"0"+iInvSlNo+",";
                              QS1 = QS1+"0"+0+",";
                              QS1 = QS1+"0"+TInvoice.getText()+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"InvoiceGrn_seq.nextval"+",";
                              QS1 = QS1+"0"+iSlNo+",";
                              QS1 = QS1+"0"+SMrsSLNo+",";
                              QS1 = QS1+"0"+SMrsUserCode+",";
                              QS1 = QS1+"0"+SOrderSLNo+",";
                              QS1 = QS1+"0"+SOrderApproval+",";
                              QS1 = QS1+"0"+STaxC+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+SDateTime+"',";
                              QS1 = QS1+"0"+SEntryStatus+",";
                              QS1 = QS1+"0"+SGrnQty+",";
                              QS1 = QS1+"0"+SGrnValue+",";
                    	      QS1 = QS1+"'"+SHsnCode+"',";
                    	      QS1 = QS1+"0"+SGstRate+",";
                    	      QS1 = QS1+"0"+SSeleSupType+",";
                    	      QS1 = QS1+"'"+SSeleStateCode+"',";
			      QS1 = QS1+"'"+SBinNo+"',";
			      QS1 = QS1+"0"+iInvoiceTypeCode+ ")";

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.executeUpdate(QS1);

               }
               stat.close();

				if(dPartyDebitAmt>0){
					insertDebitNote(SSupCode, dPartyDebitAmt);
				}
			
          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
    }
	

	private void insertDebitNote(String SPartyCode, double dDebit) {
		String SDBNO = "0";
		String SQs = " Select nvl(Max(VocNo),0) From InvoiceDebitNote Where Divisioncode="+iDivisionCode+" and YearCode="+SYearCode+"  ";
		
		
		try{
		    if(theMConnection   . getAutoCommit())
				theMConnection  . setAutoCommit(false);

            Statement      theStatement = theMConnection . createStatement();
            ResultSet      theResult    = theStatement      . executeQuery(SQs);
			
		    while(theResult.next())
		    {
				SDBNO     = theResult.getString(1);
		    }
			theResult.close();
			
		    int  iDBNNum   = common.toInt(SDBNO)+1;
						 
		    String SInvNo       = common.parseNull(TInvNo.getText().trim());
		    String SInvDate     = TInvDate.toNormal();
	        String SNarr = " DN for STORES MATERIAL Rate Differance vd. B.No."+SInvNo+"/"+common.parseDate(SInvDate)+" ";
				
			String SCat = SPartyCode;
			
			String SJournalNo = SGrnNo;
			String SCDays = "0";
			String SQty   = "0";
			
			String  SInQs  = " ";				
					SInQs  = " Insert into InvoiceDebitNote (Id, VOCNO, VOCDATE, VOCTYPE, ACCODE, NARR, INVNO, INVDATE, DEBIT, QUANTITY, CREDITDAYS, USERCODE, DIVISIONCODE, YEARCODE, CREATIONDATE, COMPUTERNAME, CATCODE, JOURNALNO, TYPE, JournalDate) ";
					SInQs += " Values(InvoiceDebitNote_Seq.nextVal, "+String.valueOf(iDBNNum)+",to_char(sysdate,'yyyymmdd'),"+"'DBN',"+"'"+SPartyCode+"',"+"'"+SNarr+"',"+"'"+SInvNo+"',"+"'"+SInvDate+"',"+common.getRound(dDebit,2)+","+SQty+","+SCDays+","+iUserCode+","+iDivisionCode+","+SYearCode+","+"'"+common.getServerDateTime()+"','"+common.getLocalHostName()+"','"+SCat+"','"+SJournalNo+"',0,to_char(sysdate,'yyyymmdd')";
					SInQs += " ) ";
						   
				theStatement . execute(SInQs);
				
				
				SPartyCode ="A_1216";
				
					SInQs  = " ";				
					SInQs  = " Insert into InvoiceDebitNote (Id, VOCNO, VOCDATE, VOCTYPE, ACCODE, NARR, INVNO, INVDATE, CREDIT, QUANTITY, CREDITDAYS, USERCODE, DIVISIONCODE, YEARCODE, CREATIONDATE, COMPUTERNAME, CATCODE, JOURNALNO, TYPE, JournalDate) ";
					SInQs += " Values(InvoiceDebitNote_Seq.nextVal, "+String.valueOf(iDBNNum)+",to_char(sysdate,'yyyymmdd'),"+"'DBN',"+"'"+SPartyCode+"',"+"'"+SNarr+"',"+"'"+SInvNo+"',"+"'"+SInvDate+"',"+common.getRound(dDebit,2)+","+SQty+","+SCDays+","+iUserCode+","+iDivisionCode+","+SYearCode+","+"'"+common.getServerDateTime()+"','"+common.getLocalHostName()+"','"+SCat+"','"+SJournalNo+"',0,to_char(sysdate,'yyyymmdd')";
					SInQs += ")";
			   
			   theStatement   . execute(SInQs);
			   theStatement   . close();
				
				
		}catch(Exception ex){
			ex.printStackTrace();
			bComflag  = false;
		}
	 
	}


	public int setInsertDCData ( String sSupCode, String sDCNo, String sGRNNo,String sDCDate)
	{ 
         int isave=0; 
         
      if(isave!=2){
     try{
    
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" insert into InvoiceRemainderMailDC (ID,SupCode,DCNo,DCDate,GRNo,CreatedDateTime,EntryDate)");
         sb.append(" values (InvoiceRemainderMailDC_Seq.nextVal,?,?,?,?,to_Char(sysdate,'DD-MON-YYYY:hh:mi:ss'),to_Char(sysdate,'YYYYMMDD'))");
         
         	 if(theMConnection ==null){
                         ORAConnection jdbc     = ORAConnection.getORAConnection();
                         theMConnection         = jdbc.getConnection();
              }
           PreparedStatement  ps           = theMConnection.prepareStatement(sb.toString());

              ps                    . setString(1,sSupCode);
              ps                    . setString(2,sDCNo);
              ps                    . setString(3,sDCDate);
//              ps                    . setString(4,sOrderNo);
              ps                    . setString(4,sGRNNo);
              
              ps . executeUpdate();
              ps.close();
              ps=null;
      }  
   
      catch(Exception e)
      {
          isave=1;
          e.printStackTrace();
          System.out.println("InsertMethod"+e);
       }
     }
  return isave;
}



    private void setSupDetails(){

		   String sSupCode	= TSupCode.getText();
		   StringBuffer      sb = new StringBuffer();
		   String 			 sSupName="",sEMail="";
		   int			 iSendStatus=0;	
      
               sb.append("  Select Name,Email from Supplier Where AC_Code='"+sSupCode+"'");
        
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       theStatement  =  theConnection.createStatement();

                    ResultSet res = theStatement.executeQuery(sb.toString());
                    while(res.next())
                    {
                         	  sSupName        = res.getString(1);
					  sEMail          = res.getString(2);
					  iSendStatus	= 0;
                    }
                   insertInvRemainderMail(sSupCode,sSupName,sEMail,iSendStatus,iMillCode); 
                    
                    res            . close();
                    theStatement   . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }

}

  private void insertInvRemainderMail(String sSupCode,String sSupName,String sMailID,int iSendStatus,int iMillCode){
	
 try{
	
        StringBuffer sb = new StringBuffer(); 

          sb.append(" Insert Into InvoiceRemainderMail (SupCode,SendStatus,MillCode,MailID,SupName,CreatedDateTime,EntryDate)");
	    sb.append(" Values (?,?,?,?,?,to_Char(sysdate,'DD-MON-YYYY:hh:mi:ss'),to_Char(sysdate,'YYYYMMDD'))");

                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();

        PreparedStatement ps=theConnection.prepareStatement(sb.toString());

              ps                    . setString(1,sSupCode);
              ps                    . setInt(2,iSendStatus);
              ps                    . setInt(3,iMillCode);
              ps                    . setString(4,sMailID);
              ps                    . setString(5,sSupName);

              ps . executeUpdate();
            //rst.close();
            ps.close();
            ps=null;
      }  
   
      catch(Exception e)
      {
          e.printStackTrace();
          System.out.println("InsertMethod"+e);
       }


  }


     private int getRowIndex(String SSeleId,Object GrnData[][])
     {
          int iIndex=-1;
          
          for(int i=0;i<GrnData.length;i++)
          {
               String SId = (String)MiddlePanel.VId.elementAt(i);
               
               if(!SSeleId.equals(SId))
                    continue;
               
               iIndex=i;
               return iIndex;
          }
          return iIndex;
     }

     public void insertGateDetails()
     {
          String QString = " Insert Into GateInward (GINo,GIDate,InwNo,Sup_Code,InvNo,InvDate,DcNo,DcDate,ModeCode,Item_Code,Item_Name,SupQty,GateQty,UnmeasuredQty,UserCode,UserTime,BaseCode,MillCode,id,GrnNo,GrnNumber,EntryStatus,Authentication) Values(";
          try
          {
               Statement       stat =  theMConnection.createStatement();

               Object GrnData[][] = MiddlePanel.GrnPanel.getFromVector();
               
               Vector VGIName = new Vector();
               Vector VGICode = new Vector();
               Vector VGIQty  = new Vector();

               String SDateTime = common.getServerDateTime();

               String SEntryStatus = "1";

               for(int i=0;i<GrnData.length;i++)
               {
                    Boolean bValue = (Boolean)GrnData[i][10];
     
                    if(!bValue.booleanValue())
                         continue;

                    String  SItemCode  = (String)GrnData[i][0];
                    String  SItemName  = (String)GrnData[i][1];
                    String  SQty       = (String)GrnData[i][8];

                    int iIndex = common.indexOf(VGICode,SItemCode);

                    if(iIndex>=0)
                    {
                         double dRecQty     = common.toDouble(SQty);
                         double dPrevRecQty = common.toDouble((String)VGIQty.elementAt(iIndex));
                         double dTotRecQty  = dRecQty + dPrevRecQty;
                         VGIQty.setElementAt(String.valueOf(dTotRecQty),iIndex);
                    }
                    else
                    {
                         VGIName.addElement(SItemName);
                         VGICode.addElement(SItemCode);
                         VGIQty.addElement(SQty);
                    }
               }


               String SInvNo       = common.parseNull(TInvNo.getText().trim());
               String SInvDate     = TInvDate.toNormal();
               String SDCNo        = common.parseNull(TDCNo.getText().trim());
               String SDCDate      = TDCDate.toNormal();

               String SSupCode = TSupCode.getText();
               String SDate    = TDate.toNormal();

               String SInwNo = (String)VInwNo.elementAt(JCInwType.getSelectedIndex());

               String SModeCode = InwMode.TCode.getText();

               for(int i=0;i<VGICode.size();i++)
               {
                    String  SItemCode  = (String)VGICode.elementAt(i);
                    String  SItemName  = (String)VGIName.elementAt(i);
                    String  SQty       = (String)VGIQty.elementAt(i);
                    
                    String    QS1 = QString;
                              QS1 = QS1+"0"+SGateNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"0"+SInwNo+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"'"+SInvNo+"',";
                              QS1 = QS1+"'"+SInvDate+"',";
                              QS1 = QS1+"'"+SDCNo+"',";
                              QS1 = QS1+"'"+SDCDate+"',";
                              QS1 = QS1+"0"+SModeCode+",";
                              QS1 = QS1+"'"+SItemCode+"',";
                              QS1 = QS1+"'"+SItemName+"',";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+"0"+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+SDateTime+"',";
                              QS1 = QS1+"0"+iAuthCode+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"gateinward_seq.nextval"+",";
                              QS1 = QS1+"0"+"1"+",";
                              QS1 = QS1+"0"+SGrnNo+",";
                              QS1 = QS1+"0"+SEntryStatus+",";
                              QS1 = QS1+"0"+"1"+")";

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

     public void setOrderLink()
     {
//	 int  iCODE=0, iNAME=1, iHSNCODE=2, iTAXTYPE=3, iBLOCK=4, iMRSNO=5, iORDERNO=6, iORDERQTY=7, iPENDINGQTY=8, iINVQTY=9, iRECDQTY=10, iORDERRATE=11, iINVRATE=12, iORDERDISC=14, iINVDISC=13, iCGST=15, iSGST=16, iIGST=17, iCESS=18, iBASIC=19, iDISCVAL=20, iCGSTVAL=21, iSGSTVAL=22, iIGSTVAL=23, iCESSVAL=24, iNET=25, iDEPARTMENT=26, iGROUP=27, iUNIT=28, iVAT=29;		 
          try
          {
               Statement       stat =  theMConnection.createStatement();
               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();                 
               for(int i=0;i<RowData.length;i++)
               {
		    String SId = (String)MiddlePanel.MiddlePanel.VSeleId.elementAt(i);

		    if(SId.equals("0"))
			continue;
		    
            String SHsnCode   = ((String)RowData[i][iHSNCODE]).trim();
		    String SCGSTPer   = common.getRound(common.toDouble(((String)RowData[i][iCGST]).trim()),2);
		    String SSGSTPer   = common.getRound(common.toDouble(((String)RowData[i][iSGST]).trim()),2);
		    String SIGSTPer   = common.getRound(common.toDouble(((String)RowData[i][iIGST]).trim()),2);
		    String SGstRate   = common.getRound(common.toDouble(SCGSTPer)+common.toDouble(SSGSTPer)+common.toDouble(SIGSTPer),2);


                    String    QS = "Update PurchaseOrder Set ";
                              QS = QS+" InvQty=InvQty+"+(String)RowData[i][iRECDQTY]+",";
                              QS = QS+" HsnCode='"+SHsnCode+"',";
                              QS = QS+" GstRate=0"+SGstRate+" ";
                              QS = QS+" Where Id = "+(String)MiddlePanel.MiddlePanel.VSeleId.elementAt(i);

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E3"+ex);
               bComflag  = false;
          }
     }
     
     public void setMRSLink()
     {
//	 int  iCODE=0, iNAME=1, iHSNCODE=2, iTAXTYPE=3, iBLOCK=4, iMRSNO=5, iORDERNO=6, iORDERQTY=7, iPENDINGQTY=8, iINVQTY=9, iRECDQTY=10, iORDERRATE=11, iINVRATE=12, iORDERDISC=14, iINVDISC=13, iCGST=15, iSGST=16, iIGST=17, iCESS=18, iBASIC=19, iDISCVAL=20, iCGSTVAL=21, iSGSTVAL=22, iIGSTVAL=23, iCESSVAL=24, iNET=25, iDEPARTMENT=26, iGROUP=27, iUNIT=28, iVAT=29;		 
          try
          {
               Statement       stat =  theMConnection.createStatement();

               Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();
               Object GrnData[][] = MiddlePanel.GrnPanel.getFromVector();

               for(int i=0;i<RowData.length;i++)
               {
                    String SMrsNo     = (String)RowData[i][iMRSNO];
                    
                    if(common.toInt(SMrsNo)==0)
                         continue;
                    
		    String SSeleId = (String)MiddlePanel.MiddlePanel.VSeleId.elementAt(i);

		    if(SSeleId.equals("0"))
			continue;

                    int iMidRow       = getRowIndex(SSeleId,GrnData);

                    String SMrsSLNo   = MiddlePanel.getMrsSLNo(iMidRow);
                    
                    String    QS = "Update MRS Set ";
                              QS = QS+" GrnNo="+SGrnNo;
                              QS = QS+" Where MRSNo ="+SMrsNo+" and Item_Code = '"+(String)RowData[i][iCODE]+"' And SlNo="+SMrsSLNo;
                              QS = QS+" And Mrs.MillCode = "+iMillCode;

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E4"+ex);
               bComflag  = false;
          }
     }

     private void updateSubStoreMasterData()
     {
//	 int  iCODE=0, iNAME=1, iHSNCODE=2, iTAXTYPE=3, iBLOCK=4, iMRSNO=5, iORDERNO=6, iORDERQTY=7, iPENDINGQTY=8, iINVQTY=9, iRECDQTY=10, iORDERRATE=11, iINVRATE=12, iORDERDISC=14, iINVDISC=13, iCGST=15, iSGST=16, iIGST=17, iCESS=18, iBASIC=19, iDISCVAL=20, iCGSTVAL=21, iSGSTVAL=22, iIGSTVAL=23, iCESSVAL=24, iNET=25, iDEPARTMENT=26, iGROUP=27, iUNIT=28, iVAT=29;		 
		 
          try
          {
               Statement stat   =    theDConnection.createStatement();
               
               Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();
               Object GrnData[][] = MiddlePanel.GrnPanel.getFromVector();

               for(int i=0;i<RowData.length;i++)
               {
                    String SItemCode  = (String)RowData[i][iCODE];
                    int iCount=0;
                    
                    ResultSet res = stat.executeQuery("Select count(*) from InvItems Where Item_Code='"+SItemCode+"'");
                    while(res.next())
                    {
                         iCount   =    res.getInt(1);
                    }
                    res.close();
                    
                    if(iCount==0)
                         continue;
                         

                    String SSeleId = (String)MiddlePanel.MiddlePanel.VSeleId.elementAt(i);

		    if(SSeleId.equals("0"))
			continue;

                    int iMidRow       = getRowIndex(SSeleId,GrnData);

                    int iBlockCode = common.toInt(MiddlePanel.getBlockCode(iMidRow));
					
	                        
                    String QS = "";
                    
                    if(iBlockCode>1)
                    {
                         QS = "Update InvItems Set ";
                         QS = QS+" MSRecQty=nvl(MSRecQty,0)+"+(String)RowData[i][iRECDQTY]+",";
                         QS = QS+" MSIssQty=nvl(MSIssQty,0)+"+(String)RowData[i][iRECDQTY]+" ";
						 QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    else
                    {
                         QS = "Update InvItems Set ";
                         QS = QS+" MSRecQty=nvl(MSRecQty,0)+"+(String)RowData[i][iRECDQTY]+",";
                         QS = QS+" MSStock=nvl(MSStock,0)+"+(String)RowData[i][iRECDQTY]+" ";
						 QS = QS+" Where Item_Code = '"+SItemCode+"'";
                    }
                    if(theDConnection  . getAutoCommit())
                         theDConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("E6"+e);
               bComflag  = false;
          }
     }

     private void updateItemMaster(Statement stat,String SItemCode,String SBlockCode,String SGrnQty,String SGrnValue,String SBinNo)
     {
          try
          {
               int iBlockCode = common.toInt(SBlockCode);
               
               String QS = "";
               if(iBlockCode>1)
               {
                    QS = "Update "+SItemTable+" Set  RecVal =nvl(RecVal,0)+"+SGrnValue+",";
                    QS = QS+" RecQty=nvl(RecQty,0)+"+SGrnQty+",";
                    QS = QS+" IssQty=nvl(IssQty,0)+"+SGrnQty+",";
					QS = QS+" LocName = '"+SBinNo+"',";
                    QS = QS+" IssVal=nvl(IssVal,0)+"+SGrnValue+" ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";
               }
               else
               {
                    QS = "Update "+SItemTable+" Set RecVal =nvl(RecVal,0)+"+SGrnValue+",";
                    QS = QS+" RecQty=nvl(RecQty,0)+"+SGrnQty+", ";
					QS = QS+" LocName = '"+SBinNo+"' ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";
               }

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               stat.execute(QS);
          }
          catch(Exception e)
          {
               System.out.println("E6"+e);
               bComflag  = false;
          }
     }

     private void updateHsnMaster(Statement stat,String SItemCode,String SHsnCode)
     {
          try
          {
	       String SMHsnCode = "";

	       String QS1 = " Select HsnCode from InvItems where Item_Code='"+SItemCode+"'";

               ResultSet result = stat.executeQuery(QS1);
               while(result.next())
               {
                    SMHsnCode = common.parseNull(result.getString(1));
               }
               result.close();

	       if(SMHsnCode.equals(SHsnCode))
		    return;

	       if(SMHsnCode.equals("") || SMHsnCode.equals("0"))
	       {
		    String QS2 = " Update InvItems set HsnCode='"+SHsnCode+"' Where Item_Code='"+SItemCode+"'";

		    if(theMConnection  . getAutoCommit())
		         theMConnection  . setAutoCommit(false);

		    stat.execute(QS2);
	       }
	       else
	       {
		    int iHCount = 0;

		    String QS3a = " Select Count(*) from Grn Where Code='"+SItemCode+"' and HsnCode='"+SMHsnCode+"'";

               	    result = stat.executeQuery(QS3a);
                    while(result.next())
                    {
                         iHCount = result.getInt(1);
                    }
                    result.close();

		    if(iHCount<=0)
		    {
		         String QS3b = " Update InvItems set HsnCode='"+SHsnCode+"' Where Item_Code='"+SItemCode+"'";

		    	 if(theMConnection  . getAutoCommit())
		              theMConnection  . setAutoCommit(false);

		    	 stat.execute(QS3b);
		    }
		    else
		    {
		         int iMCount = 0;

		    	 String QS3 = " Select Count(*) from MultiHsn Where Item_Code='"+SItemCode+"' and HsnCode='"+SHsnCode+"'";

		    	 result = stat.executeQuery(QS3);
		         while(result.next())
		         {
		              iMCount = result.getInt(1);
		         }
		         result.close();

			 if(iMCount>0)
			      return;

		         String SSysName    = common.getLocalHostName();
		         String SServerDate = common.getServerPureDate();


		         String QS4 = " Insert into MultiHsn (ID,ITEM_CODE,HSNCODE,MODIDATE,USERCODE,SYSNAME) "+
			              " Values(MultiHsn_Seq.nextval,'"+SItemCode+"','"+SHsnCode+"',"+SServerDate+","+iUserCode+",'"+SSysName+"') ";

			 String QS5 = " Update InvItems set MultiHsn=1 Where Item_Code='"+SItemCode+"'";

			 if(theMConnection  . getAutoCommit())
			      theMConnection  . setAutoCommit(false);

			 stat.execute(QS4);
			 stat.execute(QS5);
		    }
	       }
          }
          catch(Exception e)
          {
               System.out.println("E6a"+e);
               bComflag  = false;
          }
     }

     private void updateUserItemStock(Statement stat,String SItemCode,String SGrnQty,String SGrnValue,String SMrsUserCode)
     {
          try
          {
               Item IC = new Item(SItemCode,iMillCode,SItemTable,SSupTable);
     
               double dAllStock = common.toDouble(IC.getClStock());
               double dAllValue = common.toDouble(IC.getClValue());
     
               double dRate   = 0;
               try
               {
                    dRate = dAllValue/dAllStock;
               }
               catch(Exception ex)
               {
                    dRate=0;
               }
     
               if(dAllStock==0)
               {
                    dRate = common.toDouble(IC.SRate);
               }

               int iCount=0;

               String QS = " Select count(*) from ItemStock Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+iMillCode;
               
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    iCount   =    res.getInt(1);
               }
               res.close();

               String QS1 = "";

               if(iCount>0)
               {
                    QS1 = "Update ItemStock Set Stock=nvl(Stock,0)+"+SGrnQty+" ";
                    QS1 = QS1+" Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+iMillCode;
               }
               else
               {
                    QS1 = " Insert into ItemStock (MillCode,HodCode,ItemCode,Stock,StockValue) Values (";
                    QS1 = QS1+"0"+iMillCode+",";
                    QS1 = QS1+"0"+SMrsUserCode+",";
                    QS1 = QS1+"'"+SItemCode+"',";
                    QS1 = QS1+"0"+SGrnQty+",";
                    QS1 = QS1+"0"+common.getRound(dRate,4)+")";
               }

               String QS2 = " Update ItemStock set StockValue="+common.getRound(dRate,4)+
                            " Where ItemCode='"+SItemCode+"' and MillCode="+iMillCode;


               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               stat.execute(QS1);
               stat.execute(QS2);
          }
          catch(Exception e)
          {
               System.out.println("E7"+e);
               bComflag  = false;
          }
     }

     public void insertDirectGateDetails()
     {
          String QString = " Insert Into GateInward (GINo,GIDate,InwNo,Sup_Code,InvNo,InvDate,DcNo,DcDate,ModeCode,Item_Code,Item_Name,SupQty,GateQty,UnmeasuredQty,UserCode,UserTime,BaseCode,MillCode,id,EntryStatus,Authentication) Values(";
          try
          {
               Statement       stat =  theMConnection.createStatement();

               Object GateData[][] = MiddlePanel.GatePanel.getFromVector();
               
               String SInvNo       = common.parseNull(TInvNo.getText().trim());
               String SInvDate     = TInvDate.toNormal();
               String SDCNo        = common.parseNull(TDCNo.getText().trim());
               String SDCDate      = TDCDate.toNormal();

               String SSupCode = TSupCode.getText();
               String SDate    = TDate.toNormal();

               String SInwNo = (String)VInwNo.elementAt(JCInwType.getSelectedIndex());

               String SModeCode = InwMode.TCode.getText();

               String SDateTime = common.getServerDateTime();

               String SEntryStatus = "1";

               for(int i=0;i<GateData.length;i++)
               {
                    String  SItemCode  = ((String)GateData[i][0]).toUpperCase();
                    String  SItemName  = ((String)GateData[i][1]).toUpperCase();
                    String  SQty       = (String)GateData[i][2];
                    
                    String    QS1 = QString;
                              QS1 = QS1+"0"+SGateNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"0"+SInwNo+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"'"+SInvNo+"',";
                              QS1 = QS1+"'"+SInvDate+"',";
                              QS1 = QS1+"'"+SDCNo+"',";
                              QS1 = QS1+"'"+SDCDate+"',";
                              QS1 = QS1+"0"+SModeCode+",";
                              QS1 = QS1+"'"+SItemCode+"',";
                              QS1 = QS1+"'"+SItemName+"',";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+"0"+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+SDateTime+"',";
                              QS1 = QS1+"0"+iAuthCode+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"gateinward_seq.nextval"+",";
                              QS1 = QS1+"0"+SEntryStatus+",";
                              QS1 = QS1+"0"+"1"+")";

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

     public void insertDirectGateInwDetails()
     {
          String QString = " Insert Into GateInward (GINo,GIDate,InwNo,Sup_Code,InvNo,InvDate,DcNo,DcDate,ModeCode,Item_Name,SupQty,GateQty,UnmeasuredQty,UserCode,UserTime,BaseCode,MillCode,id,EntryStatus,Authentication,Item_Code) Values(";
          try
          {
               Statement       stat =  theMConnection.createStatement();

               Object GateGstData[][]  = MiddlePanel.GatePanelGst.getFromVector();
               
               String SInvNo        = common.parseNull(TInvNo.getText().trim());
               String SInvDate      = TInvDate.toNormal();
               String SDCNo         = common.parseNull(TDCNo.getText().trim());
               String SDCDate       = TDCDate.toNormal();

               String SSupCode      = TSupCode.getText();
               String SDate         = TDate.toNormal();

               String SInwNo        = (String)VInwNo.elementAt(JCInwType.getSelectedIndex());

               String SModeCode     = InwMode.TCode.getText();

               String SDateTime     = common.getServerDateTime();

               String SEntryStatus = "1";

               for(int i=0;i<GateGstData.length;i++)
               {
                    String  SItemName  = ((String)GateGstData[i][ItemName]).toUpperCase();
                    String  SQty       = (String)GateGstData[i][Qty];
                    String  SItemCode  = ((String)GateGstData[i][ItemCode]).toUpperCase();
                    String    QS1 = QString;
                              QS1 = QS1+"0"+SGateNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"0"+SInwNo+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"'"+SInvNo+"',";
                              QS1 = QS1+"'"+SInvDate+"',";
                              QS1 = QS1+"'"+SDCNo+"',";
                              QS1 = QS1+"'"+SDCDate+"',";
                              QS1 = QS1+"0"+SModeCode+",";
                              QS1 = QS1+"'"+SItemName+"',";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+"0"+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+SDateTime+"',";
                              QS1 = QS1+"0"+iAuthCode+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"gateinward_seq.nextval"+",";
                              QS1 = QS1+"0"+SEntryStatus+",";
                              //QS1 = QS1+"0"+"1"+")";
                              QS1 = QS1+"0"+"1"+",";
                              QS1 = QS1+"'"+SItemCode+"')";

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

     public void insertSMS(String Msg,String SCode)
     {
          String QS1 = " INSERT INTO TBL_ALERT_COLLECTION (COLLECTIONID,STATUSID,ALERTMESSAGE,CUSTOMERCODE,STATUS,SMSTYPE) VALUES (scm.alertCollection_seq.nextval,'STATUS56','"+Msg+"',"+SCode+",0,0) ";

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat =  theConnection.createStatement();

               stat      . executeUpdate(QS1);
               stat      . close();

          }catch(Exception Ex)
          {
               System.out.println("UpdateGINo method"+Ex);
          }
     } 
//  For Reverse Charge GST
    private int insertData() 
    {
        
        Vector VCGST = new Vector();
        Vector VSGST = new Vector();
        Vector VIGST = new Vector();
        int iSave			= 0;

        if(iGSTPartyTypeCode==0)
        {
           JOptionPane.showMessageDialog(null,"Unknown Party ");
           return iSave;
        }

        String SInvoiceQS   = "";
        SInvoiceQS			= getInvoiceInsertQS(iGSTPartyTypeCode);
        
        JDBCConnection1 connect    = JDBCConnection1.getJDBCConnection();
        Connection theConnection   = connect.getConnection();


        ORAConnection oraconnect      = ORAConnection.getORAConnection();
        Connection theConnect         = oraconnect.getConnection();

        try 
        {
            
            if(theConnection.getAutoCommit())
               theConnection.setAutoCommit(false);
            String SMaxId 		=  "";
            int    iMaxNumber	=  0;

//	    PreparedStatement theState     = theConnection.prepareStatement("Select to_char(sysdate,'yyyy')||'-'|| (MaxNUmber+1),(MaxNumber+1)  From NoConfig Where Divisioncode="+iDivisionCode+" and YearCode="+SYearCode+" and VocType='RCVOCNO'  for update of MaxNumber noWait"); 
	    PreparedStatement theState     = theConnection.prepareStatement("Select to_char(sysdate,'yyyy')||'-'|| (MaxNUmber+1),(MaxNumber+1)  From NoConfig Where Divisioncode=1 and YearCode="+SYearCode+" and VocType='RCVOCNO'  for update of MaxNumber noWait"); 

            ResultSet         theResult    = theState.executeQuery();
            while(theResult.next())
            {
                SMaxId  	= theResult.getString(1);
                iMaxNumber	= theResult.getInt(2);	
            }

            theResult     . close();
            theState  . close();                            
//            System.out.println(" VOUCHER QS==> Select to_char(sysdate,'yyyy')||'-'|| (MaxNUmber+1),(MaxNumber+1)  From NoConfig Where Divisioncode=1 and YearCode="+SYearCode+" and VocType='RCVOCNO'");
            
            
            PreparedStatement theStatement = theConnection.prepareStatement(SInvoiceQS);
            
            Object GateGstData[][]  = MiddlePanel.GatePanelGst.getFromVector();
               
               String SInvNo        = common.parseNull(TInvNo.getText().trim());
               String SInvDate      = TInvDate.toNormal();

               String SSupCode      = TSupCode.getText();
               String SDate         = TDate.toNormal();
            
            
            for (int i = 0; i < GateGstData.length; i++) 
            {
                boolean bSelect = ((Boolean)GateGstData[i][Select]).booleanValue();
                if (!bSelect) 
                {
                    continue;
                }

                String SItemCode    = (String) GateGstData[i][ItemCode];
                String SItemName    = (String) GateGstData[i][ItemName];
                String SHsnCode     = (String) GateGstData[i][HSNCode];

                double dRate        = common.toDouble(String.valueOf(GateGstData[i][Rate]));
                if (SItemCode.length()<=0 || SItemName.length()<=0) 
                {
                    JOptionPane.showMessageDialog(null, "Not a Valid InvItem" );
                    break;
                }
                if(SHsnCode.length()<=0)
                {
                    JOptionPane.showMessageDialog(null, "Plz Update HSNCode for Selected Item ");
                    break;
                }
                if(dRate<=0)
                {
                    JOptionPane.showMessageDialog(null, "Rate Must be Entered " );
                    break;
                }

                double dQty         = common.toDouble(String.valueOf(GateGstData[i][Qty]));
                double dValue       = common.toDouble(String.valueOf(GateGstData[i][Value]));
                double dDiscPer     = common.toDouble(String.valueOf(GateGstData[i][DiscPer]));
                double dDiscValue   = common.toDouble(String.valueOf(GateGstData[i][Discount]));
                double dCGSTPer     = common.toDouble(String.valueOf(GateGstData[i][CGRatePer]));
                double dCGSTValue   = common.toDouble(String.valueOf(GateGstData[i][CGValue]));
                double dSGSTPer     = common.toDouble(String.valueOf(GateGstData[i][SGRatePer]));
                double dSGSTValue   = common.toDouble(String.valueOf(GateGstData[i][SGValue]));
                double dIGSTPer     = common.toDouble(String.valueOf(GateGstData[i][IGRatePer]));
                double dIGSTValue   = common.toDouble(String.valueOf(GateGstData[i][IGValue]));
                double dTotalValue  = common.toDouble(String.valueOf(GateGstData[i][TotalValue]));
                double dRoundOff    = common.toDouble(MiddlePanel.GatePanelGst.txtRoundOff.getText());
                double dNetAmount   = common.toDouble(MiddlePanel.GatePanelGst.lblNetAmount.getText());
                int    iClaimStatus = MiddlePanel.GatePanelGst.iClaimStatus;
                int    iTaxTypeCode = getTaxCatCode(SHsnCode);
                int    iHsnType     = MiddlePanel.GatePanelGst.iHsnType;
                int iImportTypeCode = getImportTypeCode(iHsnType);
                int    iGstTypeCode = MiddlePanel.GatePanelGst.iGstTypeCode;

                if(dValue<iMaxLimit)
                {
                    JOptionPane.showMessageDialog(null, "Invoice Amount is Less than MaxLimit("+iMaxLimit+") for   " + SItemName);
                    break;
	        }                                
                HashMap theMap = new HashMap();
                
                theMap.put("CGSTPer",String.valueOf(dCGSTPer));
                theMap.put("CGSTVal",String.valueOf(dCGSTValue));
                if(!VCGST.contains(dCGSTPer))
                    VCGST.addElement(dCGSTPer);
                    
                theMap.put("SGSTPer",String.valueOf(dSGSTPer));
                theMap.put("SGSTVal",String.valueOf(dSGSTValue));
                if(!VSGST.contains(dSGSTPer))
                    VSGST.addElement(dSGSTPer);
                
                theMap.put("IGSTPer",String.valueOf(dIGSTPer));
                theMap.put("IGSTVal",String.valueOf(dIGSTValue));
                if(!VIGST.contains(dIGSTPer))
                    VIGST.addElement(dIGSTPer);

                
                theStatement.setString(1, SMaxId);
                theStatement.setString(2, SSupCode);
                theStatement.setInt(3,iDivisionCode );
                theStatement.setString(4, SYearCode);
                theStatement.setString(5, SGateNo);
                theStatement.setString(6, "0");
                theStatement.setString(7, "0");
                theStatement.setString(8, SItemCode);
                theStatement.setString(9, SHsnCode);
                theStatement.setDouble(10,dQty);
                theStatement.setDouble(11,dRate);
                theStatement.setDouble(12,dValue);
                theStatement.setDouble(13, dCGSTPer);
                theStatement.setDouble(14, dCGSTValue);
                theStatement.setDouble(15, dSGSTPer);
                theStatement.setDouble(16, dSGSTValue);
                theStatement.setDouble(17, dIGSTPer);
                theStatement.setDouble(18, dIGSTValue);
                theStatement.setInt(19, iUserCode);
                theStatement.setString(20, common.getLocalHostName());
               // if(iGSTPartyTypeCode==3)
                //{
	                theStatement.setDouble(21, dTotalValue);
	                theStatement.setString(22, SInvNo);
	                theStatement.setString(23, SInvDate);
                    theStatement.setDouble(24, dDiscPer);
                    theStatement.setDouble(25, dDiscValue);
                    if(iSave==0)
                    {
                      theStatement.setDouble(26, dRoundOff);
                      theStatement.setDouble(27, dNetAmount);
                    }
                    else
                    {
                      theStatement.setDouble(26, 0);  
                      theStatement.setDouble(27, 0);  
                    }
//                }               
                    theStatement  .setInt(28,iClaimStatus);
		            theStatement  .setInt(29,iTaxTypeCode);
		            theStatement  .setInt(30,iImportTypeCode);
		            theStatement  .setInt(31,iGstTypeCode);

                theStatement.executeUpdate();

                iSave += 1;

            }
                  
            String STaxQS   = "";
            STaxQS   		= getTaxDetailsInsertQS(iGSTPartyTypeCode);

            java.util.HashMap  themap = new java.util.HashMap();
            java.util.ArrayList  AList = new java.util.ArrayList();
             
            AList = getCGSTValue(VCGST);
                     
             for(int j=0;j<AList.size();j++)
             {
                 HashMap theMap = (HashMap)AList.get(j);
                                
                 String CGSTAcCode = (String)theMap.get("GSTCode");
                 double dPer       = common.toDouble((String)theMap.get("GSTPer"));
                 double dValue     = common.toDouble((String)theMap.get("GSTValue"));
                 if(dValue<=0)
                   break;     
                 theStatement = theConnection.prepareStatement(STaxQS);
            
                theStatement.setString(1, SMaxId);
                theStatement.setString(2, SYearCode);
                theStatement.setInt(3,iDivisionCode );
                theStatement.setString(4, CGSTAcCode);
                theStatement.setDouble(5, dPer);
                theStatement.setDouble(6,dValue );
                theStatement.setString(7, common.getLocalHostName());
                
                theStatement.executeUpdate();
                 
             }
             
             AList = new java.util.ArrayList();
             
            AList = getSGSTValue(VSGST);
             
             for(int j=0;j<AList.size();j++)
             {
                 HashMap theMap = (HashMap)AList.get(j);
                
                 String SGSTAcCode = (String)theMap.get("GSTCode");
                 double dPer       = common.toDouble((String)theMap.get("GSTPer"));
                 double dValue     = common.toDouble((String)theMap.get("GSTValue"));
                 if(dValue<=0)
                   break;     
                 
                  theStatement = theConnection.prepareStatement(STaxQS);
            
                theStatement.setString(1, SMaxId);
                theStatement.setString(2, SYearCode);
                theStatement.setInt(3,iDivisionCode );
                theStatement.setString(4, SGSTAcCode);
                theStatement.setDouble(5, dPer);
                theStatement.setDouble(6,dValue );
                theStatement.setString(7, common.getLocalHostName());
                theStatement.executeUpdate();
                 
             }
             
              AList = new java.util.ArrayList();
             
            AList = getIGSTValue(VIGST);
             
             for(int j=0;j<AList.size();j++)
             {
                 HashMap theMap = (HashMap)AList.get(j);
                
                 String IGSTAcCode = (String)theMap.get("GSTCode");
                 double dPer       = common.toDouble((String)theMap.get("GSTPer"));
                 double dValue     = common.toDouble((String)theMap.get("GSTValue"));
                 if(dValue<=0)
                   break;     
                 
                  theStatement = theConnection.prepareStatement(STaxQS);
            
                theStatement.setString(1, SMaxId);
                theStatement.setString(2, SYearCode);
                theStatement.setInt(3,iDivisionCode );
                theStatement.setString(4, IGSTAcCode);
                theStatement.setDouble(5, dPer);
                theStatement.setDouble(6,dValue );
                theStatement.setString(7, common.getLocalHostName());
                theStatement.executeUpdate();
                 
             }
            
            theStatement    .   close();
            
//            theStatement    =   theConnection.prepareStatement(" Update NoConfig set MaxNumber="+iMaxNumber+" where Divisioncode="+iDivisionCode+" and YearCode="+SYearCode+" and VocType='RCVOCNO'  ");
          theStatement    =   theConnection.prepareStatement(" Update NoConfig set MaxNumber="+iMaxNumber+" where Divisioncode=1 and YearCode="+SYearCode+" and VocType='RCVOCNO'  ");
//            System.out.println(" UPDATE CONFIG==>   Update NoConfig set MaxNumber="+iMaxNumber+" where Divisioncode="+iDivisionCode+" and YearCode="+SYearCode+" and VocType='RCVOCNO'  ");
            theStatement    .   executeUpdate();

            theStatement    .   close();


            theStatement    =   theConnect.prepareStatement(" Update GateInward set CashReceiptStatus=1,InwNo=3 where GiNo="+SGateNo+" and GiDate="+TDate.toNormal());
//            System.out.println(" UPDATE GINW==>  Update GateInward set CashReceiptStatus=1,InwNo=3 where GiNo="+SGateNo+" and GiDate="+TDate.toNormal());
            theStatement    .   executeUpdate();

            theStatement    .   close();

            theConnection . commit();
            theConnection . setAutoCommit(true);

        } 
        catch (Exception ex) 
        {
//            System.out.println(" insertdata "+ex);
            ex.printStackTrace();
            String SException = String.valueOf(ex);
            if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
            {
                 JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                 try
                 {
                 }catch(Exception Ex)
                 {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                 }
                 insertData();
                 bComflag  = true;
            }
            else
            {
				    try
				    {
				        theConnection   .   rollback();
				    }
				    catch(Exception e)
				    {
				        System.out.println(" rollback "+ex);
				        e.printStackTrace();
				    }
                                    bComflag  = false;
            }
        }
        return iSave;
    }
    private String getInvoiceInsertQS(int iGSTPartyTypeCode)
    {
         String SCurrentDate 	= common.getCurrentDate();
         StringBuffer sb	=	new StringBuffer();

    /*     if(iGSTPartyTypeCode	==	3)
	 {
		     sb.append(" Insert into ReverseCharge_Invoice(ID,ReceiptVocNo,ReceiptDate,SupCode,DivisionCode,YearCode,GiNo,GRNNo,GRNDate,ItemCode,");
		     sb.append(" HsnCode,InvQty,InvRate, ");
		     sb.append(" InvAmount,CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,UserCode,CreationDate,ComputerName,TotalValue,InvNo,InvDate,DiscPer,Discount,RoundOff,NetAmount)");
		     sb.append(" Values(ReverseCharge_Invoice_Seq.nextVal,?,to_Date("+SCurrentDate+",'yyyymmdd'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,Sysdate,?,?,?,?,?,?,?,?)");
         }
         else
         {
		     sb.append(" Insert into RegisteredParty_Invoice(ID,ReceiptVocNo,ReceiptDate,SupCode,DivisionCode,YearCode,GiNo,GRNNo,GRNDate,ItemCode,");
		     sb.append(" HsnCode,InvQty,InvRate, ");
		     sb.append(" InvAmount,CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,UserCode,CreationDate,ComputerName)");
		     sb.append(" Values(RegisteredParty_Invoice_Seq.nextVal,?,to_Date("+SCurrentDate+",'yyyymmdd'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,Sysdate,?)");
         }*/
    if(iGSTPartyTypeCode	==	3)
	 {
		     sb.append(" Insert into ReverseCharge_Invoice(ID,ReceiptVocNo,ReceiptDate,SupCode,DivisionCode,YearCode,GiNo,GRNNo,GRNDate,ItemCode,");
		     sb.append(" HsnCode,InvQty,InvRate, ");
		     sb.append(" InvAmount,CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,UserCode,CreationDate,ComputerName,TotalValue,InvNo,InvDate,DiscPer,Discount,RoundOff,NetAmount)");
		     sb.append(" Values(ReverseCharge_Invoice_Seq.nextVal,?,to_Date("+SCurrentDate+",'yyyymmdd'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,Sysdate,?,?,?,?,?,?,?,?)");
         }
         else
         {
		     sb.append(" Insert into RegisteredParty_Invoice(ID,ReceiptVocNo,ReceiptDate,SupCode,DivisionCode,YearCode,GiNo,GRNNo,GRNDate,ItemCode,");
		     sb.append(" HsnCode,InvQty,InvRate, ");
		     sb.append(" InvAmount,CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,UserCode,CreationDate,ComputerName)");
		     sb.append(" Values(RegisteredParty_Invoice_Seq.nextVal,?,to_Date("+SCurrentDate+",'yyyymmdd'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,Sysdate,?)");
         }
         return sb.toString();
	}
    private String getTaxDetailsInsertQS(int iGSTPartyTypeCode)
	{
         String SCurrentDate 	= common.getCurrentDate();
         StringBuffer SB	=	new StringBuffer();
         if(iGSTPartyTypeCode==3)
		 {
	         SB.append(" insert into ReverseCharge_TaxDetails (ID,ReceiptVocNo,YearCode,DivisionCode,GSTAcCode,GSTPer,GSTVal,CreationDate,ComputerName) ");
    	     SB.append(" Values(ReverseCharge_TaxDetails_Seq.nextVal,?,?,?,?,?,?,Sysdate,?)");
		 }
         else
         {
	         SB.append(" insert into RegisteredParty_TaxDetails (ID,ReceiptVocNo,YearCode,DivisionCode,GSTAcCode,GSTPer,GSTVal,CreationDate,ComputerName) ");
    	     SB.append(" Values(RegisteredParty_TaxDetails_Seq.nextVal,?,?,?,?,?,?,Sysdate,?)");
         } 
         return SB.toString();

	}
    public ArrayList getCGSTValue(Vector VCGST)
    {
        ArrayList AList = new ArrayList();
        double dValue = 0;
        
        for(int V=0;V<VCGST.size();V++)
        {
            HashMap theMap = new HashMap();
            double dCGSTPer      = common.toDouble(String.valueOf(VCGST.elementAt(V)));
            Object GateData[][]  = MiddlePanel.GatePanel.getFromVector();
            for (int i = 0; i < GateData.length; i++) 
            {
                boolean bSelect = ((Boolean) GateData[i][Select]).booleanValue();
                if(bSelect) 
                {
                    double dCGSTPer1     = common.toDouble(String.valueOf(GateData[i][CGRatePer]));
                    double dCGSTValue1   = common.toDouble(String.valueOf(GateData[i][CGValue]));
                    
                    if(dCGSTPer==dCGSTPer1)
                    {
                        dValue += dCGSTValue1;
                    }
                }
            }
            theMap.put("GSTCode","A_5994");
            theMap.put("GSTPer",String.valueOf(dCGSTPer));
            theMap.put("GSTValue",String.valueOf(dValue));
            
            AList.add(theMap);
        }
        return AList;
        
    }
    
     public ArrayList getSGSTValue(Vector VSGST)
    {
        double dValue = 0;
        ArrayList AList = new ArrayList();
        
        for(int V=0;V<VSGST.size();V++)
        {
            HashMap theMap       = new HashMap();
            double dSGSTPer      = common.toDouble(String.valueOf(VSGST.elementAt(V)));
            Object GateData[][]  = MiddlePanel.GatePanel.getFromVector();
            for (int i = 0; i < GateData.length; i++) 
            {
                boolean bSelect = ((Boolean) GateData[i][Select]).booleanValue();
                if(bSelect) 
                {
                    double dSGSTPer1     = common.toDouble(String.valueOf(GateData[i][SGRatePer]));
                    double dSGSTValue1   = common.toDouble(String.valueOf(GateData[i][SGValue]));
                    
                    if(dSGSTPer==dSGSTPer1)
                    {
                        dValue += dSGSTValue1;
                    }
                }
            }
            theMap.put("GSTCode","A_5995");
            theMap.put("GSTPer",String.valueOf(dSGSTPer));
            theMap.put("GSTValue",String.valueOf(dValue));
             AList.add(theMap);
        }
        
        
        return AList;
        
    }
     
    public ArrayList getIGSTValue(Vector VIGST)
    {
        ArrayList AList = new ArrayList();
        double dValue = 0;
        
        for(int V=0;V<VIGST.size();V++)
        {
            HashMap theMap       = new HashMap();
            double dIGSTPer      = common.toDouble(String.valueOf(VIGST.elementAt(V)));
            Object GateData[][]  = MiddlePanel.GatePanel.getFromVector();
            for (int i = 0; i < GateData.length; i++) 
            {
                boolean bSelect = ((Boolean) GateData[i][Select]).booleanValue();
                if(bSelect) 
                {
                    double dIGSTPer1     = common.toDouble(String.valueOf(GateData[i][IGRatePer]));
                    double dIGSTValue1   = common.toDouble(String.valueOf(GateData[i][IGValue]));
                    
                    if(dIGSTPer==dIGSTPer1)
                    {
                        dValue += dIGSTValue1;
                    }
                }
            }
            theMap.put("GSTCode","A_5996");
            theMap.put("GSTPer",String.valueOf(dIGSTPer));
            theMap.put("GSTValue",String.valueOf(dValue));
            AList.add(theMap);
        }
        
        
        return AList;
        
    }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theMConnection . commit();

                    if(iMillCode==1)
                         theDConnection . commit();

                    JOptionPane    . showMessageDialog(null,"The Entered Data is Saved with GRN No - "+SGrnNo+" and GI No - "+SGateNo,"Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("Commit");
               }
               else
               {
                    theMConnection . rollback();

                    if(iMillCode==1)
                         theDConnection . rollback();

                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theMConnection   . setAutoCommit(true);

               if(iMillCode==1)
                    theDConnection   . setAutoCommit(true);


               if(bComflag)
               {
                    preset();
               }
               else
               {
                    BOk.setEnabled(true);
               }

          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void preset()
     {
          MiddlePanel.removeAll();
          getContentPane().remove(MiddlePanel);
          getContentPane().remove(BottomPanel);

          LGrnNo.setText("To be determined");
          BSupplier.setText("Supplier");
          BSupplier.setEnabled(true);

          TInvoice.setText("");

          TInvNo.setText("");
          TDCNo.setText("");

          /*TInvDate.TDay.setText("");
          TInvDate.TMonth.setText("");
          TInvDate.TYear.setText("");

          TDCDate.TDay.setText("");
          TDCDate.TMonth.setText("");
          TDCDate.TYear.setText("");*/

		  TDCDate.theDate.setText("");	
		  TInvDate.theDate.setText("");	

          JCSort.setSelectedIndex(0);
          JCSort.setEnabled(true);

          InwMode.TCode.setText("11");
          InwMode.setText("RENGAVILAS");

          BApply.setEnabled(true);
          BOk.setEnabled(true);

          DeskTop.repaint();
          DeskTop.updateUI();
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     private void setInwardData()
     {
          VInwName   = new Vector();
          VInwNo     = new Vector();

          String QS1 = "Select InwName,InwNo From InwType Where InwNo Not in (12) Order By 2";

          try
          {
              ORAConnection   oraConnection =  ORAConnection.getORAConnection();
              Connection      theConnection =  oraConnection.getConnection();
              Statement       stat =  theConnection.createStatement();

              ResultSet result1 = stat.executeQuery(QS1);
              while(result1.next())
              {
                    VInwName.addElement(result1.getString(1));
                    VInwNo.addElement(result1.getString(2));
              }
              result1.close();
              stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

  public void setInvoiceTypeVector()
      {
          VInvTypeCode      = new Vector();
          VInvTypeName      = new Vector();
              
               String QString = "Select TypeName,TypeCode From InvoiceType Order By TypeCode";
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       stat =  theConnection.createStatement();
                    
                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         VInvTypeName.addElement(res.getString(1));
                         VInvTypeCode.addElement(res.getString(2));
                    }
                    res            . close();
					stat.close();
					
				//GST Type(GST or RCM with GST or RCM without GST)
					VEntryTypeCode     	= new Vector();
					VEntryType  	 	= new Vector();
					VEntryPartyTypeCode	= new Vector();	

					VEntryTypeCode		. removeAllElements();
					VEntryType			. removeAllElements();
					VEntryPartyTypeCode	. removeAllElements();
					
			
					if(theAlpaConnection==null){
						JDBCConnection1     jdbc = JDBCConnection1.getJDBCConnection();
						theAlpaConnection = jdbc . getConnection();
					}
								     stat =  theAlpaConnection.createStatement();
			
					ResultSet result1 = stat.executeQuery(" select entrytypecode, entrytypename, gstpartytypecode from gstentrytype order by 1 "); 
               
					while(result1.next()) {
						VEntryTypeCode     . addElement(common.parseNull(result1.getString(1)));
						VEntryType     	   . addElement(common.parseNull(result1.getString(2)));
						VEntryPartyTypeCode. addElement(common.parseNull(result1.getString(3)));
					}
					result1.close();
                    stat.close();
					
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
      }


    public int getEntryTypeCode()
	{
        int iIndex = VEntryType.indexOf(JCEntryType.getSelectedItem());
		  
        return common.toInt((String)VEntryTypeCode.elementAt(iIndex));
	}

    public int getInvTypeCode(String SName)
	{
          int iIndex = VInvTypeName.indexOf(SName);
          return common.toInt((String)VInvTypeCode.elementAt(iIndex));
	}
	
    private int getImportTypeCode(int iHsnType)
    {
		   String sSupCode		= TSupCode.getText();
		   StringBuffer  sb 	= new StringBuffer();
		   int	iImportTypeCode = -1;	
           int iCountryCode     = 0;
//           sb.append("  Select CountryCode from State Where StateCode='"+SSeleStateCode+"'");
           sb.append(" Select State.CountryCode from State ");
           sb.append(" Inner join PartyMaster on PartyMaster.StateCode = State.StateCode");
           sb.append(" Where PartyMaster.PartyCode='"+sSupCode+"'");
      
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       theStatement  =  theConnection.createStatement();

                    ResultSet res = theStatement.executeQuery(sb.toString());
                    while(res.next())
                    {
                       iCountryCode = res.getInt(1);
                    }
                    res            . close();
                    theStatement   . close();
                    if(iCountryCode==61)
                    {
                      iImportTypeCode  = 0;
                    }    
                    else
                    {
                      if(iHsnType==0) // Service
                      {
                          iImportTypeCode = 2;
                      }
                      else if(iHsnType==1) // Material
                      {
                          iImportTypeCode = 1;
                      }
                    }       
                     

               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
           return iCountryCode;
   }
   private int getTaxCatCode(String SHsnCode)
   {
        int iTaxCatCode = -1;
        int iIndex      = MiddlePanel.GatePanelGst.VHSNCode.indexOf(SHsnCode);
        if(iIndex>=0)
        {
          iTaxCatCode   = common.toInt((String)MiddlePanel.GatePanelGst.VTaxCatCode.elementAt(iIndex));
        }
        return iTaxCatCode;
   }

/*
private int setPendingInvCheck(){
int		iPendingCount	= -1, iCount=-1;;
String 	sCheckDate		= "", sGRNDate="";
int		iCheckDate=-1, iGRNDate=-1;

iPendingCount			= getPendingInvCount();
if(iPendingCount>0){
	sCheckDate			= getInvCount();
	sGRNDate			= getGRNDate();
	iCheckDate			= common.toInt(sCheckDate);
	iGRNDate			= common.toInt(sGRNDate);

System.out.println("CheckDate:"+sCheckDate+"-"+sGRNDate+"MaxCheckDate--"+iCheckDate+"Min GRNDate"+iGRNDate);

if(iCheckDate>iGRNDate){
if(!sCheckDate.equals("")){
	JOptionPane.showMessageDialog(null,"Is there any Pending Invoice Received From Party? If Received, Please Close the Status.","Information",JOptionPane.INFORMATION_MESSAGE);
	iCount=0;
   }
  }
  
}
 return iCount;
}

   public int getPendingInvCount(){
		int iCount=-1;

               String QString = "Select Count(1) From GRN Where InvoiceType=2 and MillCode=0";
               System.out.println("Qry:"+QString);
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       stat =  theConnection.createStatement();
                    
                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         iCount	= res.getInt(1);
                    }
                    res            . close();
                    stat           . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }

	return iCount;
}


  public String getInvCount(){
		int iCount=-1;
		String sCheckDate="";

               String QString = "Select ChkDate from ( Select Max(CheckDate) as ChkDate from PendingInvoiceCheck ) Where ChkDate<=to_Char(SysDate-5,'YYYYMMDD')";
               System.out.println("Qry:"+QString);
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       stat =  theConnection.createStatement();
                    
                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         sCheckDate	= res.getString(1);
                    }
                    res            . close();
                    stat           . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }

	return sCheckDate;
}


   public String getGRNDate(){
		int iCount=-1;
		String sGRNDate="";

               String QString = "Select Min(GRNDate) from GRN Where InvoiceType=2 and MillCode=0";
               System.out.println("Qry:"+QString);
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       stat =  theConnection.createStatement();
                    
                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         sGRNDate	= res.getString(1);
                    }
                    res            . close();
                    stat           . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }

	return sGRNDate;
}

public int getTrialPendingCount(){
int		iCount	=-1;
 try{
        StringBuffer sb     =new StringBuffer(); 

        sb.append("  Select Sum(Cnt) from (");
        sb.append("  Select Count(*) as Cnt from GateInward");
	  sb.append("  Left Join RawUser on GateInward.MRSAuthUserCode = RawUser.UserCode  ");
	  sb.append("  Where GateInward.InwNo= 1 and GateInward.IssueStatus = 0  And GateInward.GrnNo = 0"); 
	  sb.append("  And (trim(GateInward.Item_Code) is Null)  And GateInward.MillCode = 0 And GateInward.ModiStatus = 0");
	  sb.append("  And GateInward.Authentication = 1  and GateInward.TrialOrderAuthStatus=0  and UserName is null");
  
	  sb.append("    Union All  ");
  
	  sb.append("   Select Count(*) as Cnt from GateInward");
	  sb.append("   Left Join RawUser on GateInward.MRSAuthUserCode = RawUser.UserCode  ");
	  sb.append("   Where GateInward.InwNo = 1 and GateInward.IssueStatus = 0  And GateInward.GrnNo = 0 ");
	  sb.append("   And (trim(GateInward.Item_Name) is Null)  And GateInward.MillCode = 0 And GateInward.ModiStatus = 0 ");
	  sb.append("   And GateInward.Authentication = 1 and GateInward.TrialOrderAuthStatus=0 and UserName is null");

	  sb.append("    Union All  ");

	  sb.append("   SELECT count(*)  as Cnt  From GateInward ");
	  sb.append("   Inner Join Supplier on Supplier.Ac_Code = GateInward.Sup_Code ");
	  sb.append("   Left Join RawUser on GateInward.MRSAuthUserCode = RawUser.UserCode 	");
	  sb.append("   Where GateInward.InwNo = 2 and GateInward.IssueStatus = 0 And GateInward.MillCode=0");
	  sb.append("   And GateInward.GrnNo = 0 And (trim(GateInward.Item_Code) is Null) ");
	  sb.append("   And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 And TrialOrderAuthStatus=0");
	  sb.append("   and UserName is Null");

	  sb.append("  Union All ");

	  sb.append("  SELECT Count(*) as Cnt from GateInward");
	  sb.append("  Inner Join InvItems on GateInward.Item_Code=InvItems.Item_Code"); 
	  sb.append("  Left Join RawUser on GateInward.MRSAuthUserCode = RawUser.UserCode 	");
	  sb.append("  Where GateInward.InwNo = 2 and GateInward.IssueStatus = 0 ");
	  sb.append("  And GateInward.GrnNo = 0 And (trim(GateInward.Item_Name) is Null) ");
	  sb.append("  And GateInward.MillCode =0");
	  sb.append("  And GateInward.ModiStatus = 0 And GateInward.Authentication = 1 And TrialOrderAuthStatus=0");
	  sb.append("  and UserName is Null");
	  sb.append("  )");

         

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
                 
        PreparedStatement ps          = theConnection.prepareStatement(sb.toString());
	//System.out.println("Get Trial Count:"+sb.toString());

        ResultSet rst                 = ps.executeQuery();
       while (rst.next())
         {
		iCount			  = rst.getInt(1);
         }
       
            rst.close();
            ps.close();
            ps=null;
            
    }
    catch(Exception e){
        e.printStackTrace();
        System.out.println(e);
    }
System.out.println("Inside get TrialCount Method :"+iCount);
return iCount;
}
*/


}

