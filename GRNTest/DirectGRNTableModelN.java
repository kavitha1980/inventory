package GRNTest;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGRNTableModelN extends DefaultTableModel
{
        Object    RowData[][], ColumnNames[], ColumnType[], ColumnData[][];
		
		public int  iCODE=0, iNAME=1, iHSNCODE=2, iTAXTYPE=3, iBLOCK=4, iMRSNO=5, iORDERNO=6, iORDERQTY=7, iPENDINGQTY=8, iINVQTY=9, iRECDQTY=10, iINVRATE=11, iORDERRATE=12, iINVDISC=13, iORDERDISC=14, iCGST=15, iSGST=16, iIGST=17, iCESS=18, iBASIC=19, iDISCVAL=20, iCGSTVAL=21, iSGSTVAL=22, iIGSTVAL=23, iCESSVAL=24, iNET=25, iDEPARTMENT=26, iGROUP=27, iUNIT=28, iVAT=29;
		
        JLabel     LBasic, LDiscount, LCGST, LSGST, LIGST, LCess, LNet, LOthers;
        NextField  TAdd, TLess;
        Vector 	   VHsnType, VRateStatus;
	    String 	   SSeleSupType, SSeleStateCode;
	    
		int        iHsnCheck = 0;
		
	    double     dCGST=0, dSGST=0, dIGST=0, dCess=0;
		
        Common common = new Common();

   	    Connection       theConnection = null;

        public DirectGRNTableModelN(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType,JLabel LBasic,JLabel LDiscount,JLabel LCGST,JLabel LSGST,JLabel LIGST,JLabel LCess,NextField TAdd,NextField TLess,JLabel LNet,JLabel LOthers,Vector VHsnType,Vector VRateStatus,String SSeleSupType,String SSeleStateCode)
        {
            super(RowData,ColumnNames);
			
            this.RowData     = RowData;
            this.ColumnNames = ColumnNames;
            this.ColumnType  = ColumnType;
            this.LBasic      = LBasic;
            this.LDiscount   = LDiscount;
            this.LCGST       = LCGST; 
            this.LSGST       = LSGST;
            this.LIGST       = LIGST;
            this.LCess       = LCess;
            this.TAdd        = TAdd;
            this.TLess       = TLess;
            this.LNet        = LNet;
            this.LOthers     = LOthers;
			this.VHsnType    = VHsnType;
			this.VRateStatus = VRateStatus;
			this.SSeleSupType = SSeleSupType;
			this.SSeleStateCode = SSeleStateCode;

			ColumnData = new Object[RowData.length][ColumnType.length];

            /*for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
                setMaterialAmount(curVector);
            }*/
        }
		
        public Class getColumnClass(int col){ 
		    return getValueAt(0,col).getClass(); 
	    }
	   
       public boolean isCellEditable(int row,int col)
       {
     	       String SHsnType    = (String)VHsnType.elementAt(row);
     	       String SRateStatus = (String)VRateStatus.elementAt(row);

		   if(col==iORDERRATE)	//if(col==10)
	       {
		       if(SHsnType.equals("1"))
		       {
			       if(ColumnType[col]=="B" || ColumnType[col]=="E")
				      return true;
		       }
		       else
			     return false;
	       }
	       else
	       {
		        if(ColumnType[col]=="B" || ColumnType[col]=="E")
		            return true;

	       	    if(SHsnType.equals("1") && SRateStatus.equals("1") && ColumnData[row][col]=="X")
					return true;

		        return false;
	       }
		   
	       return false;
       }
       
       public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
			    Vector rowVector   = (Vector)super.dataVector.elementAt(row);
				
				String SHsnType    = (String)VHsnType.elementAt(row);
				String SRateStatus = (String)VRateStatus.elementAt(row);
			   
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));

                    //if(column>=iORDERDISC && column<=iCESS)
						//setMaterialAmount(rowVector,SHsnType,SRateStatus,row);
					
				    
				    if(SHsnType.equals("1") && column>=iINVRATE && column<=iCESS){
						setMaterialAmount(rowVector,SHsnType,SRateStatus,row);
                    }else if(column>=iORDERDISC && column<=iCESS){
						setMaterialAmount(rowVector,SHsnType,SRateStatus,row);
					}
					
					
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }
      public void setMaterialAmount(Vector RowVector,String SHsnType,String SRateStatus,int iRow)
      {
            double dTGross=0,dTDisc=0,dTCGST=0,dTSGST=0,dTIGST=0,dTCess=0,dTNet=0,dTOthers=0;

			setColType(iRow);

            String SHsnCode    = (String)RowVector.elementAt(2);

	      dCGST=0;
	      dSGST=0;
	      dIGST=0;
	      dCess=0;

	      if(!SSeleSupType.equals("1"))
	      {
	        dCGST=0;
			dSGST=0;
			dIGST=0;
			dCess=0;
	      }
	      else
	      {
		   int iRateCheck = getRateCheck(SHsnCode);
		   if(iRateCheck>0)
		   {
			/*if(SRateStatus.equals("0"))
			{
		             getGstRate(SHsnCode);
			}
			else*/
			{
			     dCGST = common.toDouble((String)RowVector.elementAt(iCGST));
			     dSGST = common.toDouble((String)RowVector.elementAt(iSGST));
			     dIGST = common.toDouble((String)RowVector.elementAt(iIGST));
			     dCess = common.toDouble((String)RowVector.elementAt(iCESS));
			}
		   }
		   else
		   {
		        JOptionPane.showMessageDialog(null,"Gst Rate Not Updated for HsnCode-"+SHsnCode,"Information",JOptionPane.INFORMATION_MESSAGE);
		        VRateStatus.setElementAt("0",iRow);
				setColType(iRow);
		        return;
		   }
	      }

	      double dQty = 0;

	      if(SHsnType.equals("0"))
	      {
                   dQty = common.toDouble((String)RowVector.elementAt(iRECDQTY));
	      }
	      else
	      {
                   dQty = 1;
				   RowVector.setElementAt((String)RowVector.elementAt(iINVRATE),iORDERRATE);
	      }

              double dRate       = common.toDouble((String)RowVector.elementAt(iORDERRATE));	//Order Rate
              double dDiscPer    = common.toDouble((String)RowVector.elementAt(iORDERDISC));	//Order Disc Per

              double dGross   = common.toDouble(common.getRound((dQty*dRate),2));
              double dDisc    = common.toDouble(common.getRound((dGross*dDiscPer/100),2));
	      	  double dBasic   = common.toDouble(common.getRound((dGross-dDisc),2));
              double dCGSTVal = common.toDouble(common.getRound((dBasic*dCGST/100),2));
              double dSGSTVal = common.toDouble(common.getRound((dBasic*dSGST/100),2));
              double dIGSTVal = common.toDouble(common.getRound((dBasic*dIGST/100),2));
              double dCessVal = common.toDouble(common.getRound((dBasic*dCess/100),2));
              double dNet     = common.toDouble(common.getRound((dBasic+dCGSTVal+dSGSTVal+dIGSTVal+dCessVal),2));

              RowVector.setElementAt(common.getRound(dCGST,2),iCGST);
              RowVector.setElementAt(common.getRound(dSGST,2),iSGST);
              RowVector.setElementAt(common.getRound(dIGST,2),iIGST);
              RowVector.setElementAt(common.getRound(dCess,2),iCESS);
              RowVector.setElementAt(common.getRound(dGross,2),iBASIC);
              RowVector.setElementAt(common.getRound(dDisc,2),iDISCVAL);
              RowVector.setElementAt(common.getRound(dCGSTVal,2),iCGSTVAL);
              RowVector.setElementAt(common.getRound(dSGSTVal,2),iSGSTVAL);
              RowVector.setElementAt(common.getRound(dIGSTVal,2),iIGSTVAL);
              RowVector.setElementAt(common.getRound(dCessVal,2),iCESSVAL);
              RowVector.setElementAt(common.getRound(dNet,2),iNET);

	      if(SHsnType.equals("1"))
	      {
		   if(SSeleSupType.equals("1"))
		   {
			if(SRateStatus.equals("1"))
			{
				if(SSeleStateCode.equals("33"))
				{
				     ColumnData[iRow][iCGST] = "X";
				     ColumnData[iRow][iSGST] = "X";
				     ColumnData[iRow][iIGST] = "N";
				     ColumnData[iRow][iCESS] = "X";
				}
				else
				{
				     ColumnData[iRow][iCGST] = "N";
				     ColumnData[iRow][iSGST] = "N";
				     ColumnData[iRow][iIGST] = "X";
				     ColumnData[iRow][iCESS] = "X";
				}
			}
		   }
	      }
		  

              for(int i=0;i<super.dataVector.size();i++)
              {
				  String SHType = (String)VHsnType.elementAt(i);
                  Vector curVector = (Vector)super.dataVector.elementAt(i);
				  
                  dTGross   = dTGross+common.toDouble((String)curVector.elementAt(iBASIC));
                  dTDisc    = dTDisc+common.toDouble((String)curVector.elementAt(iDISCVAL));
                  dTCGST    = dTCGST+common.toDouble((String)curVector.elementAt(iCGSTVAL));
                  dTSGST    = dTSGST+common.toDouble((String)curVector.elementAt(iSGSTVAL));
                  dTIGST    = dTIGST+common.toDouble((String)curVector.elementAt(iIGSTVAL));
                  dTCess    = dTCess+common.toDouble((String)curVector.elementAt(iCESSVAL));
                  dTNet     = dTNet+common.toDouble((String)curVector.elementAt(iNET));

					if(SHType.equals("1"))
					{
                       dTOthers = dTOthers+common.toDouble((String)curVector.elementAt(iNET));
					}
              }

              //dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
              LBasic.setText(common.getRound(dTGross,2));
              LDiscount.setText(common.getRound(dTDisc,2));
              LCGST.setText(common.getRound(dTCGST,2));
              LSGST.setText(common.getRound(dTSGST,2));
              LIGST.setText(common.getRound(dTIGST,2));
              LCess.setText(common.getRound(dTCess,2));
              //LNet.setText(common.getRound(dTNet,2));
              LOthers.setText(common.getRound(dTOthers,2));
			  
			  double dRoundOff = common.toDouble(common.getRound((Math.round(dTNet)-dTNet),2));
			  
			  TAdd.setText("0");
			  TLess.setText("0");
			  
			  if(dRoundOff>0){
				  TAdd.setText(String.valueOf(dRoundOff));
			  }else if(dRoundOff<0){
				  TLess.setText(String.valueOf((dRoundOff*-1)));
			  }
			  
			LNet.setText(common.getRound(Math.round(dTNet),2));
			  
    }

    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
                FinalData[i][j] = ((String)curVector.elementAt(j)).trim();
        }
        return FinalData;
    }
	
    public int getRows()
    {
         return super.dataVector.size();
    }
    public Vector getCurVector(int i)
    {
         return (Vector)super.dataVector.elementAt(i);
    }

    public void setTaxData()
    {
	iHsnCheck  = 0;
	ColumnData = new Object[super.dataVector.size()][ColumnType.length];

	setColType();

	boolean bHsnCheck = checkHsnCode();

	if(bHsnCheck)
	{
		iHsnCheck = 1;
		
		for(int i=0;i<super.dataVector.size();i++)
		{
		     Vector curVector   = (Vector)super.dataVector.elementAt(i);
		     String SHsnType    = (String)VHsnType.elementAt(i);
		     String SRateStatus = (String)VRateStatus.elementAt(i);
		     setMaterialAmount(curVector,SHsnType,SRateStatus,i);
		}
	}
	else
	{
	     iHsnCheck = 0;
	}
    }

    public boolean checkHsnCode()
    {
	for(int i=0;i<super.dataVector.size();i++)
	{
	     Vector curVector = (Vector)super.dataVector.elementAt(i);
		 String SHsnCode  = (String)curVector.elementAt(iHSNCODE);

	     if(SHsnCode.equals(""))
	     {
                   JOptionPane.showMessageDialog(null,"HsnCode Not Updated @Row-"+String.valueOf(i+1),"Information",JOptionPane.INFORMATION_MESSAGE);
		   return false;
	     }

	     int iRateCheck=0;

	     if(SSeleSupType.equals("1"))
	     {
	          iRateCheck = getRateCheck(SHsnCode);
	     }
	     else
	     {
		  iRateCheck = 1;
	     }

	     if(iRateCheck<=0)
	     {
	          JOptionPane.showMessageDialog(null,"Gst Rate Not Updated for HsnCode-"+SHsnCode,"Information",JOptionPane.INFORMATION_MESSAGE);
	          VRateStatus.setElementAt("0",i);
	          return false;
	     }
	}
	return true;
    }

     public int getRateCheck(String SHsnCode)
     {
		int iCount=0;

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "Select Count(*) from HsnGstRate Where HsnCode='"+SHsnCode+"'";

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    iCount = result.getInt(1);
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getRateCheck :"+ex);
        }
		
		return iCount;
     }

     public void getGstRate(String SHsnCode)
     {
        dCGST=0;
		dSGST=0;
		dIGST=0;
		dCess=0;

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "";

	       if(SSeleStateCode.equals("33"))
	       {
		    QS = " Select nvl(CGST,0) as CGST,nvl(SGST,0) as SGST,0 as IGST,nvl(Cess,0) as Cess "+
		         " from HsnGstRate Where HsnCode='"+SHsnCode+"'";
	       }
	       else
	       {
		    QS = " Select 0 as CGST,0 as SGST,nvl(IGST,0) as IGST,nvl(Cess,0) as Cess "+
		         " from HsnGstRate Where HsnCode='"+SHsnCode+"'";
	       }

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    dCGST = result.getDouble(1);
                    dSGST = result.getDouble(2);
                    dIGST = result.getDouble(3);
                    dCess = result.getDouble(4);
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getGstRate :"+ex);
        }
     }

     public void setColType()
     {
	     ColumnType[iCGST] = "N";
	     ColumnType[iSGST] = "N";
	     ColumnType[iIGST] = "N";
	     ColumnType[iCESS] = "N";
     }

     public void setColType(int iRow)
     {
	     ColumnData[iRow][iCGST] = "N";
	     ColumnData[iRow][iSGST] = "N";
	     ColumnData[iRow][iIGST] = "N";
	     ColumnData[iRow][iCESS] = "N";
     }

}
