package GRNTest;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGateModelWO extends DefaultTableModel
{
        Object RData[][],CNames[],CType[],ColumnDatas[][];
        Common common = new Common();
		
        public DirectGateModelWO(Object[][] RData, Object[] CNames,Object[] CType)
        {
            super(RData,CNames);
            this.RData       = RData;
            this.CNames      = CNames;
            this.CType       = CType;

            for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
            }

			setColumnData();
        }
       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }            
       public boolean isCellEditable(int row,int col)
       {
	       if(col==1)
	       {
       	            if(ColumnDatas[row][col]=="X")
			    return true;

		    return false;
	       }
	       else
	       {	
                    if(CType[col]=="B" || CType[col]=="E")
                       return true;

		    return false;
	       }
       }
       public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }
    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][CNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
		FinalData[i][j] = ((String)curVector.elementAt(j)).trim();
        }
        return FinalData;
    }
    public int getRows()
    {
         return super.dataVector.size();
    }
    public Vector getCurVector(int i)
    {
         return (Vector)super.dataVector.elementAt(i);
    }

    public void setColumnData()
    {
	ColumnDatas = new Object[super.dataVector.size()][CType.length];

	setColType();

	for(int i=0;i<super.dataVector.size();i++)
	{
	     Vector curVector   = (Vector)super.dataVector.elementAt(i);
	     setColType(curVector,i);
	}
    }

     public void setColType()
     {
	     CType[1] = "S";
     }

     public void setColType(Vector RowVector,int iRow)
     {
             String SItemCode = ((String)RowVector.elementAt(0)).trim();
	     if(SItemCode.equals(""))
	     {
	          ColumnDatas[iRow][1] = "X";
	     }
	     else
	     {
	          ColumnDatas[iRow][1] = "S";
	     }
     }

}
