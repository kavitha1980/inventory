package GRNTest;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class GrnSupplierSearchN implements ActionListener
{
      JLayeredPane   Layer;
      JTextField     TSupCode;
      String         SSupTable;
      JTextField     TSupData;
      JButton        BName;
      JButton        BOk;
      Vector         VSupName,VSupCode,VSupData, VGstPartyTypeCode;
      JTextField     TIndicator;          
      JList          BrowList;
      JScrollPane    BrowScroll;
      JPanel         BottomPanel;
      JInternalFrame MediFrame;
      String str="";
      ActionEvent ae;

      Common common = new Common();

      Connection theConnection = null;
	  
	  protected JComboBox      JCEntryType;
	  
	  Vector  		VEntryType, VEntryTypeCode, VEntryPartyTypeCode;

      GrnSupplierSearchN(JLayeredPane Layer,JTextField TSupCode,String SSupTable,JTextField TSupData,JComboBox JCEntryType,Vector VEntryType,Vector VEntryTypeCode,Vector VEntryPartyTypeCode)
      {
          this.Layer       = Layer;
          this.TSupCode    = TSupCode;
          this.SSupTable   = SSupTable;
          this.TSupData    = TSupData;

          this.JCEntryType         = JCEntryType;
     	  this.VEntryType  		   = VEntryType;
          this.VEntryTypeCode	   = VEntryTypeCode;
		  this.VEntryPartyTypeCode = VEntryPartyTypeCode;

          VSupName         = new Vector();
          VSupCode         = new Vector();
          VSupData         = new Vector();
		  VGstPartyTypeCode= new Vector();

          setDataIntoVector();

          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator.setEditable(false);

          BrowList         = new JList(VSupName);

          BrowScroll    = new JScrollPane(BrowList);

          BottomPanel   = new JPanel(true);
          BottomPanel.setLayout(new GridLayout(1,2));
          MediFrame     = new JInternalFrame("Supplier Selector");
          MediFrame.show();
          MediFrame.setBounds(80,100,550,350);
          MediFrame.setClosable(true);
          MediFrame.setResizable(true);
          BrowList.addKeyListener(new KeyList());
          MediFrame.getContentPane().setLayout(new BorderLayout());
          MediFrame.getContentPane().add("South",BottomPanel);
          MediFrame.getContentPane().add("Center",BrowScroll);
          BottomPanel.add(TIndicator);
          BottomPanel.add(BOk);
          BOk.addActionListener(new ActList());
      }

      public void setPreset()
      {
          int index=0;
          for(index=0;index<VSupCode.size();index++)
          {
               String str1 = (String)VSupCode.elementAt(index);
               String str  = ((String)VSupName.elementAt(index)).toUpperCase();
               if(str1.startsWith(TSupCode.getText()))
               {
                    BrowList.setSelectedValue(str,true);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
          BrowList.requestFocus();
          BrowList.updateUI();
      }

      public void setDataIntoVector()
      {
               VSupName.removeAllElements();
               VSupCode.removeAllElements();
               VSupData.removeAllElements();
			   VGstPartyTypeCode.removeAllElements();

               String QString = " Select "+SSupTable+".Name,"+SSupTable+".Ac_Code,decode(PartyMaster.StateCode,0,'0',decode(PartyMaster.CountryCode,'61',nvl(State.GstStateCode,0),'999')) as GstStateCode, "+
			        " nvl(PartyMaster.GstPartyTypeCode,0) as GstPartyTypeCode From "+SSupTable+
				" Inner Join PartyMaster on "+SSupTable+".Ac_Code=PartyMaster.PartyCode "+
				" Inner Join State on PartyMaster.StateCode=State.StateCode "+
				" Order By Name ";

               try
               {
                    if(theConnection == null)
                    {
                         ORAConnection jdbc   = ORAConnection.getORAConnection();
                         theConnection        = jdbc.getConnection();
                    }

                    Statement theStatement    = theConnection.createStatement();

                    ResultSet res = theStatement.executeQuery(QString);
                    while(res.next())
                    {
                         VSupName.addElement(res.getString(1));
                         VSupCode.addElement(res.getString(2));
                         VSupData.addElement(res.getString(2)+"`"+res.getString(3)+"`"+res.getString(4));
						 VGstPartyTypeCode.addElement(res.getString(4));
                    }
                    res            . close();
                    theStatement   . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
      }
      public class ActList implements ActionListener
      {
          public void actionPerformed(ActionEvent e)
          {
               int index = BrowList.getSelectedIndex();
               setGrnDet(index);
               str="";
               removeHelpFrame();
          }
      }

      public void actionPerformed(ActionEvent ae)
      {
          this.ae = ae;  
          BName = (JButton)ae.getSource();
          removeHelpFrame();
          try
          {
               Layer.add(MediFrame);
               MediFrame.moveToFront();
               MediFrame.setSelected(true);
               MediFrame.show();
               BrowList.requestFocus();
               setPreset();
               Layer.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
      }  

      public class KeyList extends KeyAdapter
      {
             public void keyReleased(KeyEvent ke)
             {
                  char lastchar=ke.getKeyChar();
                  lastchar=Character.toUpperCase(lastchar);
                  try
                  {
                     if(ke.getKeyCode()==8)
                     {
                        str=str.substring(0,(str.length()-1));
                        setCursor();
                     }
                     else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar>='0' && lastchar <= '9'))
                     {
                        str=str+lastchar;
                        setCursor();
                     }
                  }
                  catch(Exception ex){}
             }
             public void keyPressed(KeyEvent ke)
             {
                  if(ke.getKeyCode()==116)    // F5 is pressed
                  {
                         setDataIntoVector();
                         BrowList.setListData(VSupName);
                  }
                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                     int index = BrowList.getSelectedIndex();
                     setGrnDet(index);
                     str="";
                     removeHelpFrame();
                  }
             }
         }
         public void setCursor()
         {
            TIndicator.setText(str);
            int index=0;
            for(index=0;index<VSupName.size();index++)
            {
                 String str1 = ((String)VSupName.elementAt(index)).toUpperCase();
                 if(str1.startsWith(str))
                 {
                      BrowList.setSelectedValue(str1,true);
                      BrowList.ensureIndexIsVisible(index+10);
                      break;
                 }
            }
         }
		 
         public void removeHelpFrame()
         {
            try
            {
               Layer.remove(MediFrame);
               Layer.repaint();
               Layer.updateUI();
               ((JButton)ae.getSource()).requestFocus();
            }
            catch(Exception ex) { }
         }
		 
         public boolean setGrnDet(int index)
         {
               BName.setText((String)VSupName.elementAt(index));
               TSupCode.setText((String)VSupCode.elementAt(index));
               TSupData.setText((String)VSupData.elementAt(index));
			   
			if(VGstPartyTypeCode.size()>0){
				int iGstPartyTypeCode =	common.toInt(String.valueOf(VGstPartyTypeCode.elementAt(index)));
				
				if(iGstPartyTypeCode==1 || iGstPartyTypeCode==3)
					JCEntryType . setSelectedItem((String)VEntryType.elementAt(VEntryPartyTypeCode.indexOf(String.valueOf(iGstPartyTypeCode))));
			}
			   
            return true;
         } 
}
