package GRNTest;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class InvItemSearchN extends JDialog
{
      JLayeredPane   Layer;
      JTextField     TSupCode;
      String         SSupTable;
      JButton        BName;
      JButton        BOk;
      Vector         VItemName,VItemCode,VHSNCode;
      JTextField     TIndicator;          
      JList          BrowList;
      JScrollPane    BrowScroll;
      JPanel         BottomPanel;
      String         str="",SHSNCode="",SHSNType="";
      ActionEvent    ae;

      Common common = new Common();

      Connection theConnection   = null;
      
      DirectGateModelN  theModel = null;
      int               iRow,iCol,iCode,iHsnType,iGstTypeCode,iClaimStatus;
     // set DirectGate Model  Index
      int ItemName=0,ItemCode=1,HSNCode=2,Qty=3,Rate=4,Value=5,DiscPer=6,Discount=7,BillValue=8,CGRatePer=9,CGValue=10,SGRatePer=11,SGValue=12,IGRatePer=13,IGValue=14,TotalValue=15,Select=16;   
//      int ItemName=0,ItemCode=1,HSNCode=2,Qty=3,Rate=4,Value=5,CGRatePer=6,CGValue=7,SGRatePer=8,SGValue=9,IGRatePer=10,IGValue=11,TotalValue=12,Select=13;   
      InvItemSearchN(DirectGateModelN theModel,int iRow,int iCol,Vector VItemCode,Vector VItemName,Vector VHSNCode)
      {
          this.Layer       = Layer;
          this.theModel    = theModel;
          this.iRow        = iRow;
          this.iCol        = iCol;
          this.iCode       = iCode;
          
          this.VItemName   = VItemName;
          this.VItemCode   = VItemCode;
          this.VHSNCode    = VHSNCode;

          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator    . setEditable(false);

          BrowList      = new JList(VItemName);

          BrowScroll    = new JScrollPane(BrowList);

          BottomPanel   =   new JPanel(true);
          BottomPanel   .   setLayout(new GridLayout(1,2));
          this          .   setTitle("InvItem Selector");
          this          .   setBounds(250,100,550,350);
          BrowList      .   addKeyListener(new KeyList());
          this          .   getContentPane()    .   setLayout(new BorderLayout());
          this          .   getContentPane()    .   add("South",BottomPanel);
          this          .   getContentPane()    .   add("Center",BrowScroll);
          
          BottomPanel   .   add(TIndicator);
          BottomPanel   .   add(BOk);
       //   BOk           .   addActionListener(new ActList());
          this          .   setModal(true);
          this          .   setSize(700,500);

      }
      InvItemSearchN(DirectGateModelN theModel,int iRow,int iCol,Vector VItemCode,Vector VItemName)
      {
          this.Layer       = Layer;
          this.theModel    = theModel;
          this.iRow        = iRow;
          this.iCol        = iCol;
          this.iCode       = iCode;
          
          this.VItemName   = VItemName;
          this.VItemCode   = VItemCode;

          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator    . setEditable(false);

          BrowList      = new JList(VItemName);

          BrowScroll    = new JScrollPane(BrowList);

          BottomPanel   =   new JPanel(true);
          BottomPanel   .   setLayout(new GridLayout(1,2));
          this          .   setTitle("InvItem Selector");
          this          .   setBounds(250,100,550,350);
          BrowList      .   addKeyListener(new KeyList());
          this          .   getContentPane()    .   setLayout(new BorderLayout());
          this          .   getContentPane()    .   add("South",BottomPanel);
          this          .   getContentPane()    .   add("Center",BrowScroll);
          
          BottomPanel   .   add(TIndicator);
          BottomPanel   .   add(BOk);
       //   BOk           .   addActionListener(new ActList());
          this          .   setModal(true);
          this          .   setSize(700,500);

      }
      InvItemSearchN(DirectGateModelN theModel,int iRow,int iCol,Vector VItemCode,Vector VItemName,int iHsnType,int iGstTypeCode,int iClaimStatus)
      {
          this.Layer       = Layer;
          this.theModel    = theModel;
          this.iRow        = iRow;
          this.iCol        = iCol;
          this.iCode       = iCode;
          
          this.VItemName   = VItemName;
          this.VItemCode   = VItemCode;

          this.iHsnType     = iHsnType;
          this.iGstTypeCode = iGstTypeCode; 
          this.iClaimStatus = iClaimStatus;

          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator    . setEditable(false);

          BrowList      = new JList(VItemName);

          BrowScroll    = new JScrollPane(BrowList);

          BottomPanel   =   new JPanel(true);
          BottomPanel   .   setLayout(new GridLayout(1,2));
          this          .   setTitle("InvItem Selector");
          this          .   setBounds(250,100,550,350);
          BrowList      .   addKeyListener(new KeyList());
          this          .   getContentPane()    .   setLayout(new BorderLayout());
          this          .   getContentPane()    .   add("South",BottomPanel);
          this          .   getContentPane()    .   add("Center",BrowScroll);
          
          BottomPanel   .   add(TIndicator);
          BottomPanel   .   add(BOk);
       //   BOk           .   addActionListener(new ActList());
          this          .   setModal(true);
          this          .   setSize(700,500);

      }

      public void setPreset()
      {
          int index=0;
          for(index=0;index<VItemCode.size();index++)
          {
               String str1 = (String)VItemCode.elementAt(index);
               String str  = ((String)VItemName.elementAt(index)).toUpperCase();
               if(str1.startsWith(TSupCode.getText()))
               {
                    BrowList.setSelectedValue(str,true);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
          BrowList.requestFocus();
          BrowList.updateUI();
      }

    /*  public class ActList implements ActionListener
      {
          public void actionPerformed(ActionEvent e)
          {
               int index = BrowList.getSelectedIndex();
               setItemDetails(index);
               str="";
               removeHelpFrame();
          }
      }
*/

      public class KeyList extends KeyAdapter
      {
             public void keyReleased(KeyEvent ke)
             {
                  char lastchar=ke.getKeyChar();
                  lastchar=Character.toUpperCase(lastchar);
                  try
                  {
                     if(ke.getKeyCode()==8)
                     {
                        str=str.substring(0,(str.length()-1));
                        setCursor();
                     }
                     else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar>='0' && lastchar <= '9'))
                     {
                        str=str+lastchar;
                        setCursor();
                     }
                  }
                  catch(Exception ex){}
             }
             public void keyPressed(KeyEvent ke)
             {
                  if(ke.getKeyCode()==116)    // F5 is pressed
                  {
                         BrowList.setListData(VItemName);
                  }
                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                     int index = BrowList.getSelectedIndex();
                     setItemDetails(index);
                     str="";
                     dispose();
//                     removeHelpFrame();
                  }
             }
         }
         public void setCursor()
         {
            TIndicator.setText(str);
            int index=0;
            for(index=0;index<VItemName.size();index++)
            {
                 String str1 = ((String)VItemName.elementAt(index)).toUpperCase();
                 if(str1.startsWith(str))
                 {
                      BrowList.setSelectedValue(str1,true);
                      BrowList.ensureIndexIsVisible(index+10);
                      break;
                 }
            }
         }
         public void removeHelpFrame()
         {
            try
            {
               dispose();
            }
            catch(Exception ex) { }
         }
         public void setItemDetails(int index)
         {
               String SItemCode =   String.valueOf(VItemCode.elementAt(index));
               String SItemName =   String.valueOf(VItemName.elementAt(index)); 
               getHSNCode(SItemCode);
               if(SHSNCode.length()<=0)
               {
                   JOptionPane.showMessageDialog(null," Plz Update HSNCode of Selected Item");
                   return;
               }
               theModel .   setValueAt(SHSNCode,iRow,HSNCode);
               theModel .   setValueAt(SItemCode,iRow,ItemCode);
               theModel .   setValueAt(SItemName,iRow,ItemName);
               iHsnType =   common.toInt(SHSNType);

//               return true;
         } 
 	     private void getHSNCode(String SItemCode)
		 {
              SHSNCode="";SHSNType="";
		      try
		      {

		           String QS=" select hsncode,HsnType,GstTypeCode,ClaimStatus from invitems where item_code ='"+SItemCode+"' ";


		           ORAConnection   oraConnection =  ORAConnection.getORAConnection();
		           Connection      theConnection =  oraConnection.getConnection();               
		           Statement       theStatement  =  theConnection.createStatement();
		           ResultSet theResult           = theStatement.executeQuery(QS);
		           while(theResult.next())
		           {
		                SHSNCode = common.parseNull(theResult.getString(1));
		                SHSNType = common.parseNull(theResult.getString(2));
                        iGstTypeCode = theResult.getInt(3); 
                        iClaimStatus = theResult.getInt(4); 
		           }
		           theResult.close();
		           theStatement.close();
		      }
		      catch(Exception ex)
		      {
		           System.out.println(ex);  
		      }
		     
		 }

}
