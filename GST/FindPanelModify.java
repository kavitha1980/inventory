package GST;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class FindPanelModify extends JPanel
{
     JPanel DeptPanel,SortPanel;
     JPanel ApplyPanel,UpdatePanel;

     Common common = new Common();
     Vector VDept,VDeptCode,VHsnCode;
     JComboBox JCDept,JCHSNType,JCHsnCode;

     JRadioButton JRAllDept,JRSeleDept,JRAll,JRUpdate,JRNotUpdate;

     JTextField   TSort,TFind,TFind1,TCatl,TDraw;

     JButton      BApply;

     Connection theConnection = null;
     FindPanelModify()
     {
          getDept();     
          getHsnCode(); 
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          DeptPanel      = new JPanel();
          ApplyPanel     = new JPanel();
          SortPanel      = new JPanel();
          UpdatePanel    = new JPanel();

          JRAllDept      = new JRadioButton("All",true);
          JRSeleDept     = new JRadioButton("Selected");
         
          JCDept         = new JComboBox(VDept);
          JCHsnCode      = new JComboBox(VHsnCode);
          JCHsnCode       . addItem("ALL");
          JCHsnCode      .setSelectedItem("ALL");
          TSort          = new JTextField();
          TFind          = new JTextField();
          TFind1         = new JTextField();
          TCatl          = new JTextField();
          TDraw          = new JTextField();
          BApply         = new JButton("Apply");


		 JCHSNType      = new JComboBox();
		 JCHSNType		. addItem("Material");
		 JCHSNType		. addItem("Service");


       /*  JRAll          = new JRadioButton("All",true);
         JRUpdate       = new JRadioButton("Updated");
         JRNotUpdate     = new JRadioButton("Not Updated");*/
     }

     public void setLayouts()
     {
          setLayout(new GridLayout(1,4));
          DeptPanel.setLayout(new GridLayout(3,1));   
          DeptPanel.setBorder(new TitledBorder("Stock Group"));
          SortPanel.setLayout(new GridLayout(3,2));
          SortPanel.setBorder(new TitledBorder("Sort & Find"));
		  ApplyPanel.setLayout(new GridLayout(4,1));
		  ApplyPanel.setBorder(new TitledBorder("Period & Type"));
          UpdatePanel.setLayout(new GridLayout(3,1));
          UpdatePanel.setBorder(new TitledBorder("HsnCode Info."));
     }

     public void addComponents()
     {
          add(DeptPanel);
          add(ApplyPanel);
          add(SortPanel);
          add(UpdatePanel);


        /* add(JRAll);
         add(JRUpdate);
         add(JRNotUpdate);   */

          DeptPanel.add(JRAllDept);
          DeptPanel.add(JRSeleDept);
          DeptPanel.add(JCDept);  
         // DeptPanel.add(new JLabel(""));  
 
          UpdatePanel.add(JCHsnCode);
          UpdatePanel.add(new JLabel(""));
          UpdatePanel. add(new JLabel(""));
        //  DeptPanel.add(new JLabel(""));     


  

          SortPanel.add(new JLabel("Sort Order"));
          SortPanel.add(TSort);
          SortPanel.add(new JLabel("By Name"));
          SortPanel.add(TFind);
          SortPanel.add(new JLabel("By Code"));
          SortPanel.add(TFind1);

     
			ApplyPanel.add(JCHSNType);

			ApplyPanel.add(BApply);


			ApplyPanel.add(new JLabel(""));

			ApplyPanel.add(new JLabel(""));
     }

     public void addListeners()
     {
		JRAllDept.addActionListener(new DeptList());
		JRSeleDept.addActionListener(new DeptList());
             

		//JRAllTime.addActionListener(new DeptList());
		//JRLastYear.addActionListener(new DeptList());

		JCHSNType.addActionListener(new HsnSelectList());

        

               // JRAll.addActionListener(new RadioList());
                //JRUpdate.addActionListener(new RadioList());
                //JRNotUpdate .addActionListener(new RadioList());

     }

     public class DeptList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllDept)
               {
                    JRAllDept.setSelected(true);
                    JRSeleDept.setSelected(false);
                    JCDept.setEnabled(false);
               }
               if(ae.getSource()==JRSeleDept)
               {
                    JRAllDept.setSelected(false);
                    JRSeleDept.setSelected(true);
					int iIndex = JCHSNType.getSelectedIndex();
					if(iIndex ==0){JCDept.setEnabled(true);}
					else{JCDept.setSelectedItem("MISCELLANEOUS");JCDept.setEnabled(false);}
               }
	    /*   if(ae.getSource()==JRAllTime)
	       {
		   JRAllTime.setSelected(true);
                   JRLastYear.setSelected(false);
	       }
	       if(ae.getSource()==JRLastYear)
	       {
		   JRLastYear.setSelected(true);
                   JRAllTime.setSelected(false);
	       }*/
          }
     }
  /*   public class RadioList implements ActionListener
    {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAll)
               {
                    JRAll.setSelected(true);
                    JRUpdate.setSelected(false);
                    JRNotUpdate.setSelected(false);
               }

              if(ae.getSource()==JRUpdate)
               {
                    JRUpdate.setSelected(true);
                    JRAll.setSelected(false);
                    JRNotUpdate.setSelected(false);
               }

             if(ae.getSource()==JRNotUpdate)
               {
                    JRNotUpdate.setSelected(true);
                    JRAll.setSelected(false);
                    JRUpdate.setSelected(false);
               }

         }

     }    */
     public class HsnSelectList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               int iIndex = JCHSNType.getSelectedIndex();
			   setControls(iIndex);
          }
     }
     private void setControls(int iIndex){
		try{
			if(iIndex==0)
			{
				JCDept.setSelectedIndex(0);				
				JCDept.setEnabled(true);
			}
			if(iIndex==1)
			{
				JCDept.setSelectedItem("MISCELLANEOUS");
				JCDept.setEnabled(false);
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}

     public void getDept()
     {
          VDept     = new Vector();
          VDeptCode = new Vector();

          try
          {
                 if(theConnection == null)
                 {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
                  }
                  Statement theStatement    = theConnection.createStatement();
               ResultSet result1= theStatement.executeQuery("Select GroupName,GroupCode From StockGroup Order By 1");

               while(result1.next())
               {
                    VDept     .addElement(result1.getString(1));
                    VDeptCode .addElement(result1.getString(2));
               }
               result1.close();

                
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("Err3 :"+ex);
          }
     }

       public void getHsnCode()
     {
          VHsnCode     = new Vector();
          

      String QS=" Select distinct InvItems.HsnCode From (InvItems "+
                " Inner Join StockGroup On StockGroup.GroupCode = InvItems.StkGroupCode)  "+
                " Inner Join Uom On Uom.UomCode = InvItems.UomCode "+
                "  Where InvItems.HsnType=0  "+
                " and invitems.hsncode <>'0' and invitems.hsncode is not null  "+
                " union all select hsncode from multihsn  "+
                " Order By 1 ";

          try
          {
                 if(theConnection == null)
                 {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
                  }



                  Statement theStatement    = theConnection.createStatement();
               ResultSet result1= theStatement.executeQuery(QS);

               while(result1.next())
               {
                    VHsnCode     .addElement(result1.getString(1));
                   
               }
               result1.close();

                
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("Err3 :"+ex);
          }
     }

}
