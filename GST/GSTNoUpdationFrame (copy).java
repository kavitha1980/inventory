package GST;

import java.awt.*;
import javax.swing.*;
import java.util.*;
import javax.swing.border.*;
import java.awt.event.*;
import javax.swing.table.TableRowSorter;
import javax.swing.table.*;
import java.io.IOException;
import java.net.*;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.event.*;
import javax.swing.border.TitledBorder;

import jdbc.*;
import util.*;
import guiutil.*;

public class GSTNoUpdationFrame extends JInternalFrame {
    
    Connection              theConnection;
    JPanel                  pnlMain,pnlData,pnlEntry,pnlControl;
    JComboBox               cmbPartyName,cmbPartyType,cmbStateName;
    JButton                 btnUpdate,btnExit;
    JTextField              txtAdd1, txtAdd2, txtAdd3,txtState, txtGSTPartyType,txtGSTNo,
                            txtCountry;
     AddressField           txtGSTINID;
    Vector                  VPartyCode,VPartyName, VGSTPartyTypeCode,VGSTPartyTypeName,
                            VStateCode, VStateName, VCountryCode , VCountryName;
    ArrayList               APartyDetailsList;
    
    Common                  common  = new Common();
    JLayeredPane	    Layer;
    
public GSTNoUpdationFrame(    JLayeredPane	    Layer){

    this.Layer		= 	Layer;
    setParty();
    setPartyType();
    setStateName();
    setPartyDetails();
    createComponent();
    addLayout();
    addListener();
    addComponent();
}

private void createComponent(){
    pnlMain             =       new JPanel();
    pnlData             =       new JPanel();
    pnlEntry            =       new JPanel();
    pnlControl          =       new JPanel();
    
    cmbPartyName        =       new JComboBox(VPartyName);
    cmbPartyType        =       new JComboBox(VGSTPartyTypeName);
    cmbStateName        =       new JComboBox(VStateName);
    
    btnUpdate           =       new JButton("Update");
    btnExit             =       new JButton("Exit");
    
    txtAdd1             =       new JTextField(10);
    txtAdd2             =       new JTextField(10);
    txtAdd3             =       new JTextField(10);
    txtState            =       new JTextField(10);
    txtGSTINID          =       new AddressField();
    txtGSTNo            =       new JTextField(10);
    txtGSTPartyType     =       new JTextField(10);
    txtCountry          =       new JTextField(10);
   
}
private void addLayout(){
        
        pnlData         .       setBorder(new TitledBorder("Data"));
        pnlData         .       setLayout(new GridLayout(7, 2, 5, 5));
        
        pnlEntry        .       setBorder(new TitledBorder("Entry"));
        pnlEntry        .       setLayout(new GridLayout(4,2,5,5));

        pnlControl      .       setBorder(new TitledBorder("Control"));
        pnlControl      .       setLayout(new FlowLayout(FlowLayout.CENTER));
        
        pnlMain         .       setLayout(new BorderLayout());
        
        this            .       setSize(650, 500);
        this            .       setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this            .       setTitle("GST No / State Updation Frame");
        this             .    setMaximizable(true);
        this             .    setClosable(true);
        this            .       setResizable(true);
    
}
private void addListener(){
     cmbPartyName        .      addItemListener (new ItemListene());
     btnExit             .      addActionListener(new ActList());
     btnUpdate           .      addActionListener(new ActList());
     txtGSTINID          .      addKeyListener(new KeyList());
    
}


private void addComponent(){
    pnlData             .       add(new JLabel("Party Name"));
    pnlData             .       add(cmbPartyName);
    pnlData             .       add(new JLabel("Address1"));
    pnlData             .       add(txtAdd1);
    pnlData             .       add(new JLabel("Address2"));
    pnlData             .       add(txtAdd2);
    pnlData             .       add(new JLabel("Address3"));
    pnlData             .       add(txtAdd3);
    pnlData             .       add(new JLabel("State"));
    pnlData             .       add(txtState);
    pnlData             .       add(new JLabel("GSTIN"));
    pnlData             .       add(txtGSTNo);
    pnlData             .       add(new JLabel("GST PartyType"));
    pnlData             .       add(txtGSTPartyType);
    
    pnlEntry            .       add(new JLabel("State"));
    pnlEntry            .       add(cmbStateName);
    //pnlEntry            .       add(new JLabel("Country"));
    //pnlEntry            .       add(txtCountry);
    pnlEntry            .       add(new JLabel("GSTIN"));
    pnlEntry            .       add(txtGSTINID);
    pnlEntry            .       add(new JLabel("GST PartyType"));
    pnlEntry            .       add(cmbPartyType);
    pnlEntry            .       add(new JLabel(""));
    pnlEntry            .       add(new JLabel(""));
    
    txtAdd1             .       setEditable(false);
    txtAdd2             .       setEditable(false);
    txtAdd3             .       setEditable(false);
    txtState            .       setEditable(false);
    txtGSTNo            .       setEditable(false);
    txtGSTPartyType     .       setEditable(false);
    txtCountry          .       setEditable(false);
    
    
    pnlControl          .       add(btnUpdate);
    pnlControl          .       add(btnExit);
    
    pnlMain             .       add(pnlData,BorderLayout.NORTH);
    pnlMain             .       add(pnlEntry,BorderLayout.CENTER);
    pnlMain             .       add(pnlControl,BorderLayout.SOUTH);
    
    this.add(pnlMain);
    
}

private class ItemListene implements ItemListener{
    public void itemStateChanged(ItemEvent ie){
        if(ie.getSource()==cmbPartyName){
            setAddressDetails();
        }
    }
}
private class KeyList extends KeyAdapter{
   public KeyList(){
        
    }
    public void keyReleased(KeyEvent ke){
        char lastchar       = ke.getKeyChar();
        lastchar            = Character.toUpperCase(lastchar);
        try{
            int iLength     = txtGSTINID.getText().length();

            if(iLength==0){
               cmbPartyType.setSelectedIndex(0);
             }
           else{
               cmbPartyType.setSelectedIndex(1);
            }
        }
       catch(Exception ex){}
 }
      
 public void keyPressed(KeyEvent ke)
  {
   if (ke.getKeyCode()==KeyEvent.VK_ESCAPE)
    {
        txtGSTINID.setText("");
    }
   }
 }
private class ActList implements ActionListener {
    public void actionPerformed(ActionEvent ae){
      if(ae.getSource()==btnExit){
            dispose();
        }
      if(ae.getSource()==btnUpdate)
      {
          int iSaveValue             = -1;

          String   sGSTINID          = txtGSTINID.getText();
          String   sGSTPartyType     = (String)cmbPartyType.getSelectedItem();
          String   sGSTPartyTypeCode = getGSTPartyTypeCode(sGSTPartyType);
          String   sPartyName        = (String)cmbPartyName.getSelectedItem();
          String   sPartyCode        = getPartyCode(sPartyName);
          String   sStateName        = (String)cmbStateName.getSelectedItem();  
          String   sStateCode        = getStateCode(sStateName);
          String   sCountryCode      = getCountryCode(sStateCode) ;
          
          //System.out.println("Update Data's===>"+sGSTINID+"<--->"+sGSTPartyTypeCode+"<--->"+sPartyCode+"<--->"+sStateCode+"<--->"+sCountryCode);
           if(isGSTNoValidate())
           {

               if((JOptionPane.showConfirmDialog(null, "Are you sure you want to Save the Data?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0))
               {
                 iSaveValue              =  updateGSTNo (sGSTINID,sGSTPartyTypeCode,sPartyCode,sStateCode,sCountryCode);
                 if(iSaveValue==0)
                 {
                     JOptionPane.showMessageDialog(null, "Data Updated in PartyMaster", "Information", JOptionPane.INFORMATION_MESSAGE);
                     setPartyDetails();
                     setAddressDetails();
                 }
                 else
                 {
                     JOptionPane.showMessageDialog(null, "GSTNo Not Updated ", "Information", JOptionPane.INFORMATION_MESSAGE);
                 }
               }
          }
           else
           {
                JOptionPane.showMessageDialog(null, "GSTNo should be in 15 Digit", "Information", JOptionPane.INFORMATION_MESSAGE);
           }
      }
    }
}


private void setParty() {
      
        VPartyCode                  = new Vector();
        VPartyName                  = new Vector();
        
        String sDescription         = "";
        PreparedStatement pst       = null;
        ResultSet rst               = null;
        StringBuffer sb             = new StringBuffer();
       

        try {
                sb.append("    Select PartyCode,PartyName from PartyMaster");
                sb.append("    Order by 2");

         //     System.out.println("set PartyName =>"+sb.toString());
              
           if (theConnection == null) {
                ORAConnection3 jdbc   = ORAConnection3.getORAConnection();
                    theConnection        = jdbc.getConnection();
            }
            pst = theConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd  = rst.getMetaData();
            while (rst.next()) {
                VPartyCode          .   add(rst.getString(1));
                VPartyName          .   add(rst.getString(2));
            }
            rst.close();
            pst.close();
         } catch (Exception e) {
            System.out.println(e);
        }
    }
    

private void setPartyType() {
      
        VGSTPartyTypeCode           = new Vector();
        VGSTPartyTypeName           = new Vector();
        
        String sDescription         = "";
        PreparedStatement pst       = null;
        ResultSet rst               = null;
        StringBuffer sb             = new StringBuffer();

        try {
                sb.append("    Select Code,Name from GSTPartyType");
                sb.append("    Order by 1");

        //      System.out.println("set PartyType =>"+sb.toString());
              
           if (theConnection == null) {
               ORAConnection3 jdbc   = ORAConnection3.getORAConnection();
                    theConnection        = jdbc.getConnection();
            }
            pst = theConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd  = rst.getMetaData();
            
            while (rst.next()) {
                VGSTPartyTypeCode   .   add(rst.getString(1));
                VGSTPartyTypeName   .   add(rst.getString(2));
            }
            
            rst.close();
            pst.close();
         } catch (Exception e) {
            System.out.println(e);
        }
    }

public String getGSTPartyTypeCode(String sPartyTypeName){
          int iIndex    = VGSTPartyTypeName.indexOf(sPartyTypeName);
          return (String)VGSTPartyTypeCode.elementAt(iIndex);
 }

public String getGSTPartyTypeName(String sPartyTypeCode){
         int index      = VGSTPartyTypeCode.indexOf(sPartyTypeCode);
         return String.valueOf(VGSTPartyTypeName.elementAt(index));
 }


private void setPartyDetails() {
    
        APartyDetailsList           = new ArrayList();
        PreparedStatement pst       = null;
        ResultSet rst               = null;
        StringBuffer sb             = new StringBuffer();
//        String sPartyName           = (String)cmbPartyName.getSelectedItem();    
  //      String sPartyCode           = getPartyCode(sPartyName.trim());

        try {
                sb.append("    Select Address1,Address2,Address3,PartyMAster.STatecode,StateName ,PartyMaster.GSTINID, ");
                sb.append("    GSTPartyType.Name as GSTPartyType,PartyMaster.PartyCode,PartyMaster.PartyName ");
                sb.append("    from PartyMaster ");
                sb.append("    Inner Join State on PartyMaster.StateCode = State.StateCode ");
                sb.append("    Inner Join GSTPartyType on PartyMaster.GSTPartyTypeCode = GSTPartyType.Code ");
                sb.append("    Order by PartyMaster.PartyName");
             //   sb.append("    Where PartyMaster.PartyCode='"+sPartyCode+"'");
                
             // System.out.println("Address Details =>"+sb.toString());
              
           if (theConnection == null) {
                 ORAConnection3 jdbc   = ORAConnection3.getORAConnection();
                    theConnection        = jdbc.getConnection();
            }
            pst = theConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd  = rst.getMetaData();
            while (rst.next()) {
              HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                 }
                 APartyDetailsList .add(themap);   
            }
            rst.close();
            pst.close();
           } catch (Exception e) {
            System.out.println(e);
        }
    }

public String getPartyCode(String sPartyName){
          int iIndex    = VPartyName.indexOf(sPartyName);
          return (String)VPartyCode.elementAt(iIndex);
 }

public String getPartyName(String sPartyCode){
         int index      = VPartyCode.indexOf(sPartyCode);
         return String.valueOf(VPartyName.elementAt(index));
 }

  private void setAddressDetails(){
        String sPartyName           = (String)cmbPartyName.getSelectedItem();    
        String sPartyCode           = getPartyCode(sPartyName.trim());
        int iPartyindex             = VPartyCode.indexOf(sPartyCode);
        
      //for(int i=0;i< APartyDetailsList.size();i++){
          
          HashMap       theMap      =   (HashMap)APartyDetailsList.get(iPartyindex);
          
          String       sAddress1    =   common.parseNull((String)theMap.get("ADDRESS1"));
          String       sAddress2    =   common.parseNull((String)theMap.get("ADDRESS2"));
          String       sAddress3    =   common.parseNull((String)theMap.get("ADDRESS3"));
          String       sState       =   common.parseNull((String)theMap.get("STATENAME"));
          String       sGSTINID     =   common.parseNull((String)theMap.get("GSTINID"));
          String       sGSTPartyType =   common.parseNull((String)theMap.get("GSTPARTYTYPE"));
          String       SPartyCode    =   common.parseNull((String)theMap.get("PARTYCODE"));
          String       SPartyName    =   common.parseNull((String)theMap.get("PARTYNAME"));
          
                       cmbStateName .   setSelectedItem(sState);
                       txtAdd1      .   setText(sAddress1);
                       txtAdd2      .   setText(sAddress2);
                       txtAdd3      .   setText(sAddress3);
                       txtState     .   setText(sState);
                       txtGSTNo     .   setText(sGSTINID);
                       txtGSTPartyType  .   setText(sGSTPartyType);

                       txtGSTINID       .   setText(sGSTINID);

                       cmbPartyType     .   setSelectedItem(sGSTPartyType);

                     //  txtCountry    .   setText(sCountryName);
                       
           if(!sGSTINID.isEmpty()){            
                        //txtGSTINID  .   setEditable(false)  ;
                        cmbPartyType.   setSelectedIndex(1);
            }
           else{
                        //txtGSTINID  .   setEditable(true)  ;     
                        cmbPartyType.   setSelectedIndex(0);
             }
  }
    
  
public int updateGSTNo (String sGSTINID,String sGSTPartyTypeCode,String sPartyCode,String sStateCode, String sCountryCode){
  int iSaveValue=-1;
  try{
         StringBuffer sb=new StringBuffer(); 
         
         sb.append(" Update PartyMaster  set GSTINID='"+sGSTINID+"',GSTPartyTypeCode='"+sGSTPartyTypeCode+"',");
         sb.append(" StateCode='"+sStateCode+"',CountryCode='"+sCountryCode+"'");
         sb.append(" Where PartyCode='"+sPartyCode+"'");
        
      //   System.out.println("Update Qry:"+sb.toString());
         
           if (theConnection==null){
                 ORAConnection3 jdbc   = ORAConnection3.getORAConnection();
                    theConnection        = jdbc.getConnection();
              }
        PreparedStatement ps          = theConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
           
       ps . executeUpdate();
       iSaveValue=0;
       }  

      catch(Exception e)
      {
         try{
          e.printStackTrace();
          iSaveValue=1;
  //        theProcessConnection.rollback();
       //   System.out.println("InsertMethod"+e);
          }
          catch(Exception ex){
              ex.printStackTrace();
          }
       }
 //   System.out.println("update Rt Value--->"+iSaveValue);
  return iSaveValue;
  
}  

private void setStateName() {
      
        VStateCode                  = new Vector();
        VStateName                  = new Vector();
        VCountryCode                = new Vector();
        VCountryName                = new Vector();
        
        String sDescription         = "";
        PreparedStatement pst       = null;
        ResultSet rst               = null;
        StringBuffer sb             = new StringBuffer();
       

        try {
                sb.append("    Select StateCode,StateName,CountryCode from State");
                sb.append("    Where YarnGSTTypeCode>0");
                sb.append("    Order by 1");

          //    System.out.println("set State =>"+sb.toString());
              
           if (theConnection == null) {
                ORAConnection3 jdbc   = ORAConnection3.getORAConnection();
                    theConnection        = jdbc.getConnection();
            }
            pst = theConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd  = rst.getMetaData();
            
            while (rst.next()) {
                VStateCode            .   add(rst.getString(1));
                VStateName            .   add(rst.getString(2));
                VCountryCode          .   add(rst.getString(3));
                //VCountryName          .   add(rst.getString(4));
            }
            
            rst.close();
            pst.close();
         } catch (Exception e) {
            System.out.println(e);
        }
    }

public String getStateCode(String sStateName){
          int iIndex    = VStateName.indexOf(sStateName);
          return (String)VStateCode.elementAt(iIndex);
 }

public String getStateName(String sStateCode){
         int index      = VStateCode.indexOf(sStateCode);
         return String.valueOf(VStateName.elementAt(index));
 }

public String getCountryCode(String sStateCode){
          int iIndex    = VStateCode.indexOf(sStateCode);
          return (String)VCountryCode.elementAt(iIndex);
 }



private boolean isGSTNoValidate(){
    boolean     bool    = false;
    String sGSTNo       = txtGSTINID.getText();
    int iGSTNolength    = sGSTNo.length();
//    System.out.println("GSTNo==>"+sGSTNo.length()+"---"+iGSTNolength);
   if(iGSTNolength>0){ 
    if(iGSTNolength==15){
        bool            =  true;
    }
    else{
        bool            =  false;
    }
     }
   else{
       bool            =  true; 
   }
     return bool;
}

/*public static void main(String args[]){
    GSTNoUpdationFrame     frame   = new GSTNoUpdationFrame();
                           frame   . setVisible(true) ;
}
  */
  
}
