package GST;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JFileChooser;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;
import java.net.InetAddress;

public class GstMaterialEntryFrame extends JInternalFrame
{
     JLayeredPane   Layer;

     JTextField     TMatCode,TMatPaperColour,TMatPaperSize;
     MyTextField    TMatName,TMatCatl,TMatDraw,TShortName,TPartNo;
     JComboBox      JCStkGroup,JCUom,JMatPaperSets,JMatPaperSide,JCMill;
     JComboBox      JCHSNType,JCTaxCat;
     MyComboBox     JCSector,JCSubSector,JCDept,JCMachine,JCMatGroup,JCBrand,JCVat,JCTemp;
     MyTextField    TInner,TOuter,TLength,TThick,TWidth,TViscosity,THeight;
     MyTextField    TChapter,TCommodity,THsnCode;

	 FractionNumberField FnGstRate,FnCgst,FnSgst,FnIgst,FnCess;

     JButton        BOk,BCancel,BPhoto;
     JPanel         NamePanel,GroupPanel,DimPanel,StorePanel,StatPanel,PicPanel;
     JPanel         Name1Panel,Group1Panel,Dim1Panel,Store1Panel,Stat1Panel,BottomPanel;
	 JPanel 		GstPanel,Gst1Panel;

     JTabbedPane    JTP;

     Vector         VStkGroup,VStkGroupCode,VSeries,VUom,VUomCode;
     Vector         VPaperSet,VSetCode,VSideName,VSideCode;
     Vector         VSector,VSectorCode,VSubSector,VSubSectorCode,VDept,VDeptCode;
     Vector         VMacName,VMacCode,VMatGroup,VMatGroupCode,VBrand,VBrandCode,VTemp,VTempCode;
     Vector         VSTGroup,VSTGroupCode,vTaxCatCode,vTaxCatName;

     Common         common    = new Common();
     int            iMillCode,iAuthCode,iUserCode;
     String         SItemTable;

     boolean        bStatFlag = false;
     boolean        bAutoCommitFlag = true;

     Connection     theConnection  = null;
     Connection     theDConnection = null;

     public GstMaterialEntryFrame(JLayeredPane Layer,int iMillCode,int iAuthCode,int iUserCode,String SItemTable)
     {
          this.Layer      = Layer;
          this.iMillCode  = iMillCode;
          this.iAuthCode  = iAuthCode;
          this.iUserCode  = iUserCode;
          this.SItemTable = SItemTable;

          getMasterData();
		  getTaxCategory();	
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setStationary();
          JCSector.setSelectedItem("N.A.");
     }

     public void createComponents()
     {
          TMatCode       = new JTextField();
          TMatName       = new MyTextField(50);
          TMatCatl       = new MyTextField(30);
          TMatDraw       = new MyTextField(40);
          TMatPaperColour= new JTextField();
          TMatPaperSize  = new JTextField();

          TShortName     = new MyTextField(8);
          TPartNo        = new MyTextField(20);

          JCSector       = new MyComboBox(VSector);
          JCSubSector    = new MyComboBox();
          JCDept         = new MyComboBox(VDept);
          JCMachine      = new MyComboBox(VMacName);
          JCMatGroup     = new MyComboBox(VMatGroup);
          JCBrand        = new MyComboBox(VBrand);
          JCVat          = new MyComboBox(VSTGroup);
          JCTemp         = new MyComboBox(VTemp);

          TInner         = new MyTextField(15);
          TOuter         = new MyTextField(15);
          TLength        = new MyTextField(15);
          TThick         = new MyTextField(15);
          TWidth         = new MyTextField(15);
          TViscosity     = new MyTextField(15);
          THeight        = new MyTextField(15);

          TChapter       = new MyTextField(15);
          TCommodity     = new MyTextField(15);

		  THsnCode		 = new MyTextField(8);
		  JCTaxCat       = new MyComboBox(vTaxCatName);
          FnGstRate		 = new FractionNumberField();
		  FnCgst		 = new FractionNumberField();
		  FnSgst		 = new FractionNumberField();
		  FnIgst  	     = new FractionNumberField();
		  FnCess		 = new FractionNumberField();

		  JCHSNType      = new JComboBox();
		  JCHSNType		 . addItem("Material");
		  JCHSNType		 . addItem("Service");

          JMatPaperSide  = new JComboBox(VSideName);
          JMatPaperSets  = new JComboBox(VPaperSet);
          JCStkGroup     = new JComboBox(VStkGroup);
          JCUom          = new JComboBox(VUom);
          JCMill         = new JComboBox();

          JTP            = new JTabbedPane(JTabbedPane.RIGHT);

          BOk            = new JButton("Okay");
          BCancel        = new JButton("Cancel");
          BPhoto         = new JButton("Attach Photo");

          NamePanel      = new JPanel();
          GroupPanel     = new JPanel();
          DimPanel       = new JPanel();
          StorePanel     = new JPanel();
          StatPanel      = new JPanel();
          PicPanel       = new JPanel();
          BottomPanel    = new JPanel();

          Name1Panel     = new JPanel();
          Group1Panel    = new JPanel();
          Dim1Panel      = new JPanel();
          Store1Panel    = new JPanel();
          Stat1Panel     = new JPanel();
		  GstPanel		 = new JPanel();
		  Gst1Panel      = new JPanel();
          
          TMatCode       . setEditable(false);
          JCTaxCat       .setEnabled(false);

          JCBrand.setSelectedItem("N.A.");
          JCTemp.setSelectedItem("N.A.");
          JCMachine.setSelectedItem("N.A.");

          if(iMillCode==0)
          {
               JCMill.setEnabled(true);
          }
          else
          {
               JCMill.setEnabled(false);
          }
		  FnIgst.setEditable(false);
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          setTitle("Administration Utility for Materials");

          NamePanel.setLayout(new BorderLayout());
          GroupPanel.setLayout(new BorderLayout());
          DimPanel.setLayout(new BorderLayout());
          StorePanel.setLayout(new BorderLayout());
          StatPanel.setLayout(new BorderLayout());
          PicPanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());
		  GstPanel.setLayout(new BorderLayout());

          Name1Panel.setLayout(new GridLayout(11,2,10,10));
          Group1Panel.setLayout(new GridLayout(7,2,10,10));
          Dim1Panel.setLayout(new GridLayout(7,2,10,10));
          Store1Panel.setLayout(new GridLayout(3,2,10,10));
          Stat1Panel.setLayout(new GridLayout(4,2,10,10));
          Gst1Panel.setLayout(new GridLayout(7,2,10,10));
     }

     public void addComponents()
     {
          JCMill.addItem("NO");
          JCMill.addItem("YES");
		
		  Name1Panel.add(new JLabel("HSN Type"));
          Name1Panel.add(JCHSNType);

          Name1Panel.add(new JLabel("Stock Group"));
          Name1Panel.add(JCStkGroup);
          Name1Panel.add(new JLabel("Material Belongs to All Company"));
          Name1Panel.add(JCMill);
          Name1Panel.add(new JLabel("Material Code"));
          Name1Panel.add(TMatCode);
          Name1Panel.add(new JLabel("Material Name"));
          Name1Panel.add(TMatName);
          Name1Panel.add(new JLabel("Material Short Name"));
          Name1Panel.add(TShortName);
          Name1Panel.add(new JLabel("Unit of Measure"));
          Name1Panel.add(JCUom);
          Name1Panel.add(new JLabel("Catalogue No"));
          Name1Panel.add(TMatCatl);
          Name1Panel.add(new JLabel("Drawing No"));
          Name1Panel.add(TMatDraw);
          Name1Panel.add(new JLabel("Part Number"));
          Name1Panel.add(TPartNo);
          Name1Panel.add(new JLabel("Select Image of the Material"));
          Name1Panel.add(BPhoto);

          Group1Panel.add(new JLabel("Industry Sector"));
          Group1Panel.add(JCSector);
          Group1Panel.add(new JLabel("Industry Sector SubGroup"));
          Group1Panel.add(JCSubSector);
          Group1Panel.add(new JLabel("Department"));
          Group1Panel.add(JCDept);
          Group1Panel.add(new JLabel("Machine Model"));
          Group1Panel.add(JCMachine);
          Group1Panel.add(new JLabel("Material SubGroup"));
          Group1Panel.add(JCMatGroup);
          Group1Panel.add(new JLabel("Brand"));
          Group1Panel.add(JCBrand);
          Group1Panel.add(new JLabel("Sales Tax Group"));
          Group1Panel.add(JCVat);

          Dim1Panel.add(new JLabel("Inner Dia"));
          Dim1Panel.add(TInner);
          Dim1Panel.add(new JLabel("Outer Dia"));
          Dim1Panel.add(TOuter);
          Dim1Panel.add(new JLabel("Length"));
          Dim1Panel.add(TLength);
          Dim1Panel.add(new JLabel("Thickness"));
          Dim1Panel.add(TThick);
          Dim1Panel.add(new JLabel("Width"));
          Dim1Panel.add(TWidth);
          Dim1Panel.add(new JLabel("Viscosity"));
          Dim1Panel.add(TViscosity);
          Dim1Panel.add(new JLabel("Height"));
          Dim1Panel.add(THeight);

          Store1Panel.add(new JLabel("Temperature to Maintain"));
          Store1Panel.add(JCTemp);
          Store1Panel.add(new JLabel("Chapter Number"));
          Store1Panel.add(TChapter);
          Store1Panel.add(new JLabel("Commodity Code"));
          Store1Panel.add(TCommodity);

          Stat1Panel.add(new JLabel("Paper Colour"));
          Stat1Panel.add(TMatPaperColour);
          Stat1Panel.add(new JLabel("Paper Sets"));
          Stat1Panel.add(JMatPaperSets);
          Stat1Panel.add(new JLabel("Paper Size"));
          Stat1Panel.add(TMatPaperSize);
          Stat1Panel.add(new JLabel("Paper Side"));
          Stat1Panel.add(JMatPaperSide);

		  Gst1Panel.add(new JLabel("Hsn / SASC Code "));
		  Gst1Panel.add(THsnCode);
		  Gst1Panel.add(new JLabel("Tax Type"));
		  Gst1Panel.add(JCTaxCat);
		  Gst1Panel.add(new JLabel("Gst Rate % "));
		  Gst1Panel.add(FnGstRate);
		  Gst1Panel.add(new JLabel("CGST % "));
		  Gst1Panel.add(FnCgst);
		  Gst1Panel.add(new JLabel("SGST % "));
		  Gst1Panel.add(FnSgst);
		  Gst1Panel.add(new JLabel("IGST % "));
		  Gst1Panel.add(FnIgst);
		  Gst1Panel.add(new JLabel("CESS % "));
		  Gst1Panel.add(FnCess);

          if(iAuthCode>0)
          {
               BottomPanel.add(BOk);
          }
          BottomPanel.add(BCancel);

          NamePanel.add("North",Name1Panel);
          GroupPanel.add("North",Group1Panel);
          DimPanel.add("North",Dim1Panel);
          StorePanel.add("North",Store1Panel);
          StatPanel.add("North",Stat1Panel);
		  GstPanel.add("North",Gst1Panel);

          JTP.addTab("Naming & Identification",NamePanel);
          JTP.addTab("Photo",PicPanel);
          JTP.addTab("Grouping",GroupPanel);
          JTP.addTab("Dimension",DimPanel);
          JTP.addTab("Storage & Statutory",StorePanel);
          JTP.addTab("Stationary",StatPanel);
		  JTP.addTab("Gst Data",GstPanel);

          getContentPane().add("Center",JTP);
          getContentPane().add("South",BottomPanel);
     }

     public void addListeners()
     {
          BPhoto.addActionListener(new PhotoList());
          BOk.addActionListener(new ActList());
		  BCancel.addActionListener(new ActList());
          JCStkGroup.addActionListener(new SelectList());
          JCSector.addActionListener(new SectorList());
		  JCHSNType.addActionListener(new HsnSelectList());
		  JCTaxCat.addActionListener(new TaxSelectList());
		  FnGstRate.addFocusListener(new GstRateFocusList());
		 // THsnCode.addFocusListener(new HsnCodeFocusList());
           THsnCode.addKeyListener(new KeyList());
          
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
			if (ae.getSource() == BOk) {
               if(isValidData())
               {
                    BOk  . setEnabled(false);
                    setItemCode();
     
                    if (InsertMaterial())
					{
						if(THsnCode.getText().trim().length()>0)
                        {
                           String sHsnCode = THsnCode.getText().trim().toUpperCase();
                           int iHsncodeCheck=getHsnCodeCheck(sHsnCode);
                           if(iHsncodeCheck == 0)
                            {  
                            
				                if(insertHsnGstRate())
				            	refresh();
                             }else{
                                 refresh();
                             }
                            
						 }else{refresh();}
					}
     
                    try
                    {
                         if(bAutoCommitFlag)
                         {
                              theConnection  . commit();
                              theDConnection . commit();
                              System         . out.println("Commit");
                              theConnection  . setAutoCommit(true);
                              theDConnection . setAutoCommit(true);
                         }
                         else
                         {
                              theConnection  . rollback();
                              theDConnection . rollback();
                              System         . out.println("RollBack");
                              theConnection  . setAutoCommit(true);
                              theDConnection . setAutoCommit(true);
                         }
                    }catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
                    BOk.setEnabled(true);
                    JCStkGroup.requestFocus();
               }
			}
			if (ae.getSource() == BCancel) {
					removeHelpFrame();
			}
          }
     }

     private class PhotoList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               JFileChooser fc = new JFileChooser();

               int rVal = fc.showOpenDialog(GstMaterialEntryFrame.this);

               if (rVal == JFileChooser.APPROVE_OPTION)
               {
                    if((fc.getSelectedFile().getName()).endsWith(".jpg") || (fc.getSelectedFile().getName()).endsWith(".JPG")  )
                    {
                         String sCurDirectory = fc.getCurrentDirectory().toString();
     
                         if(sCurDirectory.startsWith("/"))
                         {
                              BPhoto.setText((fc.getCurrentDirectory().toString())+"/"+(fc.getSelectedFile().getName()));
                         }
                         else
                         {
                              BPhoto.setText((fc.getCurrentDirectory().toString())+"\\"+(fc.getSelectedFile().getName()));
                         }
                         setPhoto(BPhoto.getText());
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Only jpg format allowed","Information",JOptionPane.INFORMATION_MESSAGE);
                         BPhoto.setText("Attach Photo");
                         setPhoto(BPhoto.getText());
                    }
               }

               if (rVal == JFileChooser.CANCEL_OPTION)
               {
                    BPhoto.setText("Attach Photo");
                    setPhoto(BPhoto.getText());
               }
          }
     }

     public class SelectList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setStationary();
          }
     }

     public class SectorList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               int iIndex = JCSector.getSelectedIndex();
               setSubSector(iIndex);
          }
     }
	 public class HsnSelectList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               int iIndex = JCHSNType.getSelectedIndex();
			   setControls(iIndex);
          }
     }
	 public class TaxSelectList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               int iIndex = JCTaxCat.getSelectedIndex();
			    if(iIndex==0){
						FnGstRate.setEditable(true);
						FnCgst.setEditable(true);
						FnSgst.setEditable(true);
						FnCess.setEditable(true);
			   }		
			   if(iIndex==1 || iIndex==2){
						FnGstRate.setEditable(false);
						FnCgst.setEditable(false);
						FnSgst.setEditable(false);
						FnCess.setEditable(false);

						FnGstRate.setText("0");
						FnCgst.setText("0");
						FnSgst.setText("0");
						FnIgst.setText("0");
						FnCess.setText("0");
			   }
          }
     }
	 public class GstRateFocusList implements FocusListener
     {
		  public void focusGained(FocusEvent e) {
	      };
          public void focusLost(FocusEvent e)
          {

             if(THsnCode.getText().trim().length()>0){
              JCTaxCat       .setEnabled(true);
			  FnGstRate.setEditable(true);
			  FnCgst.setEditable(true);
			  FnSgst.setEditable(true);
			  FnCess.setEditable(true);
             

              String SGstRate=(String)FnGstRate.getText() ;
              double dGstRate=common.toDouble(common.getRound(SGstRate,2));

              double dCGST=dGstRate/2;
              double dSGST=dGstRate/2;

              FnIgst.setText(FnGstRate.getText());
              FnCgst.setText(String.valueOf(dCGST));
              FnSgst.setText(String.valueOf(dSGST));
              } else{

                  JOptionPane.showMessageDialog(null,"HSN Code is Empty","Information",JOptionPane.INFORMATION_MESSAGE);
              
                        FnGstRate.setEditable(false);
						FnCgst.setEditable(false);
						FnSgst.setEditable(false);
						FnCess.setEditable(false);
               }
         
          }
     }
//changed key listener
	 public class KeyList extends KeyAdapter 
     {
		
           public void keyPressed(KeyEvent ke) 
          {              
			  try{
				  String sHsnCode = THsnCode.getText().trim().toUpperCase();
				  if(sHsnCode.length()>0)
				  {	
					  getHsnGstRate(sHsnCode);
               
                        JCTaxCat.setEnabled(true);  
						FnGstRate.setEditable(true);
						FnCgst.setEditable(true);
						FnSgst.setEditable(true);
						FnCess.setEditable(true);
                    }else{  

                        JCTaxCat.setEnabled(false);  
						FnGstRate.setEditable(false);
						FnCgst.setEditable(false);
						FnSgst.setEditable(false);
						FnCess.setEditable(false);

						FnGstRate.setText("");
						FnCgst.setText("");
						FnSgst.setText("");
						FnIgst.setText("");
						FnCess.setText("");
 
				  }					
			  }
			  catch(Exception ex)
			  {
				ex.printStackTrace();
			  }
          }
     }


     public void setStationary()
     {
          JMatPaperSets  . setSelectedIndex(0);
          JMatPaperSide  . setSelectedIndex(0);
          TMatPaperColour. setText("");
          TMatPaperSize  . setText("");

          if(((String)JCStkGroup.getSelectedItem()).equals("STATIONARY"))
          {
               bStatFlag = true;
               JTP.setEnabledAt(5,true);
               StatPanel.updateUI();
          }
          else
          {
               bStatFlag = false;
               JTP.setEnabledAt(5,false);
               StatPanel.updateUI();
          }
     }

     public void setPhoto(String SFileName)
     {
          try
          {
               PicPanel.removeAll();
               PicPanel.updateUI();
          }
          catch(Exception ex){}

          if(!SFileName.equals("Attach Photo"))
          {
               PicPanel.add("Center",new JScrollPane(new JLabel(new ImageIcon(SFileName))));
               PicPanel.updateUI();
          }
     }

	private void setControls(int iIndex){
		try{
			if(iIndex==0)
			{
				JCStkGroup.setSelectedIndex(0);
				JCMill.setSelectedIndex(0);
				JCUom.setSelectedIndex(0);
				JCStkGroup.setEnabled(true);
				JCMill.setEnabled(true);
				JCUom.setEnabled(true);

				TMatCatl.setEditable(true);
				TMatDraw.setEditable(true);
				TPartNo.setEditable(true);
				BPhoto.setEnabled(true);

				JTP.setEnabledAt(1,true);
				JTP.setEnabledAt(2,true);
				JTP.setEnabledAt(3,true);
				JTP.setEnabledAt(4,true);
			}
			if(iIndex==1)
			{
				JCStkGroup.setSelectedItem("MISCELLANEOUS");
				JCMill.setSelectedItem("YES");
				JCUom.setSelectedItem("N.A.");
				JCStkGroup.setEnabled(false);
				JCMill.setEnabled(false);
				JCUom.setEnabled(false);

				TMatCatl.setEditable(false);
				TMatDraw.setEditable(false);
				TPartNo.setEditable(false);
				BPhoto.setEnabled(false);

				JTP.setEnabledAt(1,false);
				JTP.setEnabledAt(2,false);
				JTP.setEnabledAt(3,false);
				JTP.setEnabledAt(4,false);
				JTP.setEnabledAt(5,false);
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}

     public void setSubSector(int iIndex)
     {
          ResultSet result    = null;

          VSubSector     = new Vector();
          VSubSectorCode = new Vector();

          JCSubSector.removeAllItems();

          String SSectorCode = (String)VSectorCode.elementAt(iIndex);

          String QS = " Select Name,Code from IndustrySubSector Where SectorCode=0 or SectorCode="+SSectorCode+" Order by Name ";

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
                               theConnection .  setAutoCommit(true);
               Statement       stat          =  theConnection.createStatement();

               result    = stat.executeQuery(QS);

               while(result.next())
               {
                    VSubSector.addElement(result.getString(1));
                    VSubSectorCode.addElement(result.getString(2));
               }
               result    . close();
               stat      . close();

               for(int i=0;i<VSubSector.size();i++)
               {
                    JCSubSector.addItem((String)VSubSector.elementAt(i));
               }

               JCSubSector.setSelectedItem("N.A.");
          }
          catch(Exception ex)
          {
               System.out.println("setSubSector :"+ex);
          }
     }


     public boolean isValidData()
     {
          String SName = common.getNarration(TMatName.getText());
          String SCatl = common.getNarration(TMatCatl.getText());
          String SDraw = common.getNarration(TMatDraw.getText());
		  String Sstkgrp = (String)JCStkGroup.getSelectedItem();	
		  int iIndex = JCHSNType.getSelectedIndex();
		  int iTaxIndex = JCTaxCat.getSelectedIndex();


                 SName = SName     . trim();
                 SCatl = SCatl     . trim();
                 SDraw = SDraw     . trim();

          if(SName.length()==0)
          {
               JOptionPane.showMessageDialog(null,"Item Name Must be filled","Information",JOptionPane.INFORMATION_MESSAGE);
               TMatName.requestFocus();
               return false;
          }

          if(SName.length()>50)
          {
               JOptionPane.showMessageDialog(null,"Name Can't Exceed 50 Char. Current Length - "+SName.length(),"Information",JOptionPane.INFORMATION_MESSAGE);
               TMatName.requestFocus();
               return false;
          }

          if(SCatl.length()>30)
          {
               JOptionPane.showMessageDialog(null,"Catalogue Can't Exceed 30 Char. Current Length - "+SCatl.length(),"Information",JOptionPane.INFORMATION_MESSAGE);
               TMatCatl.requestFocus();
               return false;
          }

          if(SDraw.length()>40)
          {
               JOptionPane.showMessageDialog(null,"DrawingNo Can't Exceed 40 Char. Current Length - "+SDraw.length(),"Information",JOptionPane.INFORMATION_MESSAGE);
               TMatDraw.requestFocus();
               return false;
          }

          /*if(THsnCode.getText().trim().length()==0){
				JOptionPane.showMessageDialog(null," Please Enter HSN Code","Information",JOptionPane.INFORMATION_MESSAGE);
				THsnCode.requestFocus();
				return false;
		  }*/
		  if(THsnCode.getText().trim().length()>0){
		  	if(THsnCode.getText().trim().length()>8   || THsnCode.getText().trim().length()<8 ){
				JOptionPane.showMessageDialog(null,"  HSN Code should contains 8 - Digit","Information",JOptionPane.INFORMATION_MESSAGE);
				THsnCode.requestFocus();
				return false;
		  	}
		  }
		  if(iTaxIndex==0 && common.toDouble(FnGstRate.getText())<=0 && THsnCode.getText().trim().length()>0){
				JOptionPane.showMessageDialog(null," Please Enter Gst Rate ","Information",JOptionPane.INFORMATION_MESSAGE);
				FnGstRate.requestFocus();
				return false;
		  }
		  if( iIndex == 0 && Sstkgrp.equals("MISCELLANEOUS") ){
			   JOptionPane.showMessageDialog(null,"MISCELLANEOUS Not Allowed AS Stock Group","Information",JOptionPane.INFORMATION_MESSAGE);
               return false;
		  }
		  
		  /*if(BPhoto.getText().equals("Attach Photo")) {
			  
			   JOptionPane.showMessageDialog(null,"Photo Must be Attached...","Information",JOptionPane.INFORMATION_MESSAGE);
               return false;
			  
		  }*/
		  
		  //BPhoto.setText("Attach Photo");
		  
		  
		 if(common.toDouble(FnGstRate.getText())>0)
		 {
			double dCgst =0,dSgst=0,dIgst=0,dGstRate=0;
			double dTot = 0;
			dCgst = common.toDouble(FnCgst.getText());
			dSgst = common.toDouble(FnSgst.getText());
			dIgst = common.toDouble(FnIgst.getText());
			dGstRate = common.toDouble(FnGstRate.getText());
			dTot  = dCgst+dSgst;
			if( dTot != dGstRate || dCgst==0 || dSgst==0)
			{
				JOptionPane.showMessageDialog(null,"Total of Cgst & Sgst Must be equal to Gst Rate","Information",JOptionPane.INFORMATION_MESSAGE);
				return false;
			}
			if(dGstRate==0 && (dCgst>0 || dSgst>0 || dIgst>0)){
				JOptionPane.showMessageDialog(null,"Please Enter GST Rate","Information",JOptionPane.INFORMATION_MESSAGE);
				return false;
			}
		 }

          return true;
     }
	 private String getTaxCode(String STaxName)
     {
          int iIndex=-1;
          iIndex = vTaxCatName.indexOf(STaxName);
          if(iIndex!=-1)
               return common.parseNull((String)vTaxCatCode.elementAt(iIndex));
          else
               return "";
     }

     public boolean InsertMaterial()
     {
          Vector VMillTable = new Vector();

          String SName = common.getNarration(TMatName.getText());
          String SCode = TMatCode  . getText();
          String SCatl = common.getNarration(TMatCatl.getText());
          String SDraw = common.getNarration(TMatDraw.getText());
		  String sHsnCode = THsnCode.getText().trim().toUpperCase();
		  String sHsnType = String.valueOf(JCHSNType.getSelectedIndex());

                 SName = SName     . trim();
                 SCode = SCode     . trim();
                 SCatl = SCatl     . trim();
                 SDraw = SDraw     . trim();

          if(SName.length()==0)
               return false;
          
          if(SCode.length()==0)
               return false;
          
          if(iMillCode == 0)
          {
               if(JCMill.getSelectedIndex()==1)
                    VMillTable = getAllItemTables();

               String    QS = "";

               try
               {
                    ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                                   theConnection  = oraConnection.getConnection();
                                   theConnection  . setAutoCommit(true);
                    Statement      stat           = theConnection.createStatement();
                    PreparedStatement ps          = theConnection.prepareStatement(getInvItemsQS());


                  DORAConnection DoraConnection = DORAConnection.getORAConnection();
                                   theDConnection = DoraConnection.getConnection();
                                   theDConnection . setAutoCommit(true);
                                   Statement dyestat =  theDConnection.createStatement(); 
     
                    if(theConnection.getAutoCommit())
                              theConnection  . setAutoCommit(false);
     
                  if(theDConnection.getAutoCommit())
                         theDConnection . setAutoCommit(false); 


                    ps.setString(1,TMatCode.getText());
                    ps.setString(2,SName.toUpperCase());
                    ps.setString(3,(String)VUomCode.elementAt(JCUom.getSelectedIndex()));
                    ps.setString(4,SCatl.toUpperCase());
                    ps.setString(5,SDraw.toUpperCase());
                    ps.setString(6,(String)VStkGroupCode.elementAt(JCStkGroup.getSelectedIndex()));
                    ps.setInt(7,iMillCode);
                    ps.setInt(8,iUserCode);
                    ps.setString(9,common.getServerDate());
                    ps.setString(10,(((String)TMatPaperColour.getText()).trim()).toUpperCase());
                    ps.setString(11,(String)VSetCode.elementAt(JMatPaperSets.getSelectedIndex()));
                    ps.setString(12,(((String)TMatPaperSize.getText()).trim()).toUpperCase());
                    ps.setString(13,(String)VSideCode.elementAt(JMatPaperSide.getSelectedIndex()));
                    ps.setString(14,(common.getNarration(TShortName.getText())).toUpperCase());
                    ps.setString(15,(common.getNarration(TPartNo.getText())).toUpperCase());
                    ps.setString(16,(String)VSectorCode.elementAt(JCSector.getSelectedIndex()));
                    ps.setString(17,(String)VSubSectorCode.elementAt(JCSubSector.getSelectedIndex()));
                    ps.setString(18,(String)VDeptCode.elementAt(JCDept.getSelectedIndex()));
                    ps.setString(19,(String)VMacCode.elementAt(JCMachine.getSelectedIndex()));
                    ps.setString(20,(String)VBrandCode.elementAt(JCBrand.getSelectedIndex()));
                    ps.setString(21,(String)VSTGroupCode.elementAt(JCVat.getSelectedIndex()));
                    ps.setString(22,(String)VTempCode.elementAt(JCTemp.getSelectedIndex()));
                    ps.setString(23,common.getNarration(TInner.getText()));
                    ps.setString(24,common.getNarration(TOuter.getText()));
                    ps.setString(25,common.getNarration(TLength.getText()));
                    ps.setString(26,common.getNarration(TThick.getText()));
                    ps.setString(27,common.getNarration(TWidth.getText()));
                    ps.setString(28,common.getNarration(TViscosity.getText()));
                    ps.setString(29,common.getNarration(THeight.getText()));
                    ps.setString(30,common.getNarration(TChapter.getText()));
                    ps.setString(31,common.getNarration(TCommodity.getText()));
                    ps.setString(32,(String)VMatGroupCode.elementAt(JCMatGroup.getSelectedIndex()));
					ps.setString(33,sHsnCode);
					ps.setString(34,sHsnType);


                    if(!(BPhoto.getText()).equals("Attach Photo"))
                    {
                         FileInputStream fin = new FileInputStream(BPhoto.getText());
                         ps.setBinaryStream(35,fin,fin.available());
                         ps.setString(36,"jpg");
						
                    }
					

                    ps.executeUpdate();


                    if(JCMill.getSelectedIndex()==1)
                    {
                         String    QS1  = "";
                         String    QS2  = "";

                         if(VMillTable.size()>0)
                         {
                              for(int i=0;i<VMillTable.size();i++)
                              {
                                   QS1 = "";

                                   String SMillTable = (String)VMillTable.elementAt(i);

                                   String SSeq = SMillTable+"_SEQ";
               
                                   QS1 = " Insert Into "+SMillTable+" (Item_Code,StkGroupCode,MatGroupCode,USERCODE,ID,CREATIONDATE) Values (";
                                   QS1 = QS1+"'"+TMatCode.getText()+"',";
                                   QS1 = QS1+"'"+(String)VStkGroupCode.elementAt(JCStkGroup.getSelectedIndex())+"',";
                                   QS1 = QS1+(String)VMatGroupCode.elementAt(JCMatGroup.getSelectedIndex())+",";
                                   QS1 = QS1+iUserCode+",";
                                   QS1 = QS1+getNextVal(SSeq)+",";
                                   QS1 = QS1+"'"+common.getServerDate()+"')";

                                   stat . execute(QS1);
                              }
                         }

                         String SStkGroupCode = (String)VStkGroupCode.elementAt(JCStkGroup.getSelectedIndex());
     
                         if((SStkGroupCode.equals("C01")) || (SStkGroupCode.equals("C02")))
                         {
                              QS2 = " Insert Into InvItems (Item_Code,Item_Name,UOM,StkGroupCode) Values (";
                              QS2 = QS2+"'"+TMatCode.getText()+"',";
                              QS2 = QS2+"'"+SName.toUpperCase()+"',";
                              QS2 = QS2+"'"+(String)VUomCode.elementAt(JCUom.getSelectedIndex())+"',";
                              QS2 = QS2+"'"+(String)VStkGroupCode.elementAt(JCStkGroup.getSelectedIndex())+"')";

                              dyestat.execute(QS2);
                         }
                    }
                    stat.close();
                    dyestat.close();
                    ps.close();

               }catch(Exception e)
               {
                    System.out.println(e);
                    bAutoCommitFlag     = false;
                    e.printStackTrace();
               }
          }
          else
          {
               String    QS   = "";
               String    QS1  = "";
               String    QS2  = "";
               try
               {
                    ORAConnection  oraConnection =  ORAConnection.getORAConnection();
                                   theConnection =  oraConnection.getConnection();
                                   theConnection .  setAutoCommit(true);
                    Statement      stat          =  theConnection.createStatement();
                    PreparedStatement ps         =  theConnection.prepareStatement(getInvItemsQS());

                  DORAConnection DoraConnection = DORAConnection.getORAConnection();
                                   theDConnection = DoraConnection.getConnection();
                                   theDConnection . setAutoCommit(true);
                                   Statement dyestat =  theDConnection.createStatement(); 
               

                    if(theConnection.getAutoCommit())
                              theConnection  . setAutoCommit(false);

                  if(theDConnection.getAutoCommit())
                         theDConnection . setAutoCommit(false);


                    ps.setString(1,TMatCode.getText());
                    ps.setString(2,SName.toUpperCase());
                    ps.setString(3,(String)VUomCode.elementAt(JCUom.getSelectedIndex()));
                    ps.setString(4,SCatl.toUpperCase());
                    ps.setString(5,SDraw.toUpperCase());
                    ps.setString(6,(String)VStkGroupCode.elementAt(JCStkGroup.getSelectedIndex()));
                    ps.setInt(7,iMillCode);
                    ps.setInt(8,iUserCode);
                    ps.setString(9,common.getServerDate());
                    ps.setString(10,(((String)TMatPaperColour.getText()).trim()).toUpperCase());
                    ps.setString(11,(String)VSetCode.elementAt(JMatPaperSets.getSelectedIndex()));
                    ps.setString(12,(((String)TMatPaperSize.getText()).trim()).toUpperCase());
                    ps.setString(13,(String)VSideCode.elementAt(JMatPaperSide.getSelectedIndex()));
                    ps.setString(14,(common.getNarration(TShortName.getText())).toUpperCase());
                    ps.setString(15,(common.getNarration(TPartNo.getText())).toUpperCase());
                    ps.setString(16,(String)VSectorCode.elementAt(JCSector.getSelectedIndex()));
                    ps.setString(17,(String)VSubSectorCode.elementAt(JCSubSector.getSelectedIndex()));
                    ps.setString(18,(String)VDeptCode.elementAt(JCDept.getSelectedIndex()));
                    ps.setString(19,(String)VMacCode.elementAt(JCMachine.getSelectedIndex()));
                    ps.setString(20,(String)VBrandCode.elementAt(JCBrand.getSelectedIndex()));
                    ps.setString(21,(String)VSTGroupCode.elementAt(JCVat.getSelectedIndex()));
                    ps.setString(22,(String)VTempCode.elementAt(JCTemp.getSelectedIndex()));
                    ps.setString(23,common.getNarration(TInner.getText()));
                    ps.setString(24,common.getNarration(TOuter.getText()));
                    ps.setString(25,common.getNarration(TLength.getText()));
                    ps.setString(26,common.getNarration(TThick.getText()));
                    ps.setString(27,common.getNarration(TWidth.getText()));
                    ps.setString(28,common.getNarration(TViscosity.getText()));
                    ps.setString(29,common.getNarration(THeight.getText()));
                    ps.setString(30,common.getNarration(TChapter.getText()));
                    ps.setString(31,common.getNarration(TCommodity.getText()));
                    ps.setString(32,(String)VMatGroupCode.elementAt(JCMatGroup.getSelectedIndex()));
					ps.setString(33,sHsnCode);
					ps.setString(34,sHsnType);


                    if(!(BPhoto.getText()).equals("Attach Photo"))
                    {
                         FileInputStream fin = new FileInputStream(BPhoto.getText());
                         ps.setBinaryStream(35,fin,fin.available());
                         ps.setString(36,"jpg");
                    }
					

                    ps.executeUpdate();


                    String SSeq = SItemTable+"_SEQ";

                    QS1 = " Insert Into "+SItemTable+" (Item_Code,StkGroupCode,MatGroupCode,USERCODE,ID,CREATIONDATE) Values (";
                    QS1 = QS1+"'"+TMatCode.getText()+"',";
                    QS1 = QS1+"'"+(String)VStkGroupCode.elementAt(JCStkGroup.getSelectedIndex())+"',";
                    QS1 = QS1+(String)VMatGroupCode.elementAt(JCMatGroup.getSelectedIndex())+",";
                    QS1 = QS1+iUserCode+",";
                    QS1 = QS1+getNextVal(SSeq)+",";
                    QS1 = QS1+"'"+common.getServerDate()+"')";


                    String SStkGroupCode = (String)VStkGroupCode.elementAt(JCStkGroup.getSelectedIndex());

                    if(iMillCode==1)
                    {
                         if((SStkGroupCode.equals("C01")) || (SStkGroupCode.equals("C02")))
                         {
                              QS2 = " Insert Into InvItems (Item_Code,Item_Name,UOM,StkGroupCode) Values (";
                              QS2 = QS2+"'"+TMatCode.getText()+"',";
                              QS2 = QS2+"'"+SName.toUpperCase()+"',";
                              QS2 = QS2+"'"+(String)VUomCode.elementAt(JCUom.getSelectedIndex())+"',";
                              QS2 = QS2+"'"+(String)VStkGroupCode.elementAt(JCStkGroup.getSelectedIndex())+"')";
                         }
                    }


                    stat . execute(QS1);

                  if((SStkGroupCode.equals("C01")) || (SStkGroupCode.equals("C02")))
                         dyestat.execute(QS2); 

                    stat.close();
                    dyestat.close();
                    ps.close();
               }
               catch(Exception ex)
               {
                    bAutoCommitFlag     = false;
                    System.out.println(ex);
               }
          }
          return true;
     }

     public String getInvItemsQS()
     {
          String QS = "";

          if((BPhoto.getText()).equals("Attach Photo"))
          {
               QS = " Insert Into InvItems (Id,Item_Code,Item_Name,UOMCode,Catl,Draw,StkGroupCode,"+
                    " millcode,USERCODE,CREATIONDATE,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE,"+
                    " Short_Name,PartNumber,SectorCode,SubSectorCode,DeptCode,MacModelCode,"+
                    " BrandCode,STGroup,TempCode,InnerDia,OuterDia,MatLength,Thickness,Width,"+
                    " Viscosity,Height,ChapterNo,CommodityCode,MatGroupCode,HsnCode,HsnType) "+
                    " Values (InvItems_seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
          }
          else
          {
               QS = " Insert Into InvItems (Id,Item_Code,Item_Name,UOMCode,Catl,Draw,StkGroupCode,"+
                    " millcode,USERCODE,CREATIONDATE,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE,"+
                    " Short_Name,PartNumber,SectorCode,SubSectorCode,DeptCode,MacModelCode,"+
                    " BrandCode,STGroup,TempCode,InnerDia,OuterDia,MatLength,Thickness,Width,"+
                    " Viscosity,Height,ChapterNo,CommodityCode,MatGroupCode,HsnCode,HsnType,Photo,PhotoFormat) "+
                    " Values (InvItems_seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
          }

          return QS;
     }
	
	 private boolean insertHsnGstRate(){
		try{
			String iip = InetAddress.getLocalHost().getHostAddress();
			StringBuffer sb = new StringBuffer();
						 sb.append(" Insert into HsnGstRate (ID,HSNCODE,GSTRATE,CGST,SGST,IGST,CESS,USERCODE,DATETIME,SYSNAME,TAXCATCODE) ");
						 sb.append(" Values(HsnGstRate_Seq.nextval,?,?,?,?,?,?,?,sysdate,?,?) ");

			ORAConnection  oraConnection  = ORAConnection.getORAConnection();
							theConnection  = oraConnection.getConnection();
							theConnection  . setAutoCommit(true);
            PreparedStatement ps          = theConnection.prepareStatement(sb.toString());

			if(theConnection.getAutoCommit())
				theConnection  . setAutoCommit(false);

				ps.setString(1,THsnCode.getText().trim());
				ps.setString(2,String.valueOf(common.toDouble(common.parseNull(FnGstRate.getText()))));
				ps.setString(3,String.valueOf(common.toDouble(common.parseNull(FnCgst.getText()))));
				ps.setString(4,String.valueOf(common.toDouble(common.parseNull(FnSgst.getText()))));
				ps.setString(5,String.valueOf(common.toDouble(common.parseNull(FnIgst.getText()))));
				ps.setString(6,String.valueOf(common.toDouble(common.parseNull(FnCess.getText()))));
				ps.setString(7,String.valueOf(iUserCode));
				ps.setString(8,common.parseNull(iip));
				ps.setString(9,common.parseNull(getTaxCode((String)JCTaxCat.getSelectedItem())));
				ps.executeUpdate();
		}
		catch(Exception ex)
		{
			bAutoCommitFlag     = false;
			ex.printStackTrace();
		}
		return true;
 	 }

     public Vector getAllItemTables()
     {
          Vector vect = new Vector();

          String QS = " Select distinct(Item_Table) from Mill where MillCode>0 ";

          try
          {
               ORAConnection   oraConnection = ORAConnection.getORAConnection();
               Connection      theConnection = oraConnection.getConnection();
                               theConnection . setAutoCommit(true);
               Statement stat                = theConnection.createStatement();
               ResultSet res = stat.executeQuery(QS);

               while(res.next())
                    vect . addElement(res.getString(1));

               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System         . out.println(ex);
          }
          return vect;
     }

     public void setItemCode()
     {
          String SCode="";

          String str   =  (String)VStkGroupCode.elementAt(JCStkGroup.getSelectedIndex());

          String QS = " Select (maxno+1) From StockGroup Where GroupCode='"+str+"' for update of MaxNo noWait";

          Vector vect  = new Vector();
          try
          {
               ORAConnection   oraConnection = ORAConnection.getORAConnection();
               Connection      theConnection = oraConnection.getConnection();

               if(theConnection . getAutoCommit())
                    theConnection  . setAutoCommit(false);

               Statement stat               = theConnection.createStatement();

               PreparedStatement thePrepare = theConnection.prepareStatement(" Update StockGroup set MaxNo = ?  where GroupCode='"+str+"'");

               ResultSet res = stat.executeQuery(QS);

               while(res.next())
                    SCode = common.parseNull((String)res.getString(1));

               res.close();


               thePrepare.setInt(1,common.toInt(SCode));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();

               SCode = common.stuff(SCode);

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    setItemCode();
                    bAutoCommitFlag  = true;
               }
               else
               {
                    TMatCode       . setText("");
                    JOptionPane    . showMessageDialog(null,"Problem in Assigning Material Code","Information",JOptionPane.INFORMATION_MESSAGE);
                    bAutoCommitFlag  = false;
               }
          }
          TMatCode       . setText(str+SCode);
          JOptionPane    . showMessageDialog(null,"Material Code is "+str+SCode,"Information",JOptionPane.INFORMATION_MESSAGE);
     }

     /*public void setItemCode()
     {
          String str   =  (String)VStkGroupCode.elementAt(JCStkGroup.getSelectedIndex());

          String    QS = " SELECT substr(Item_Code,4,6)"+
                         " FROM InvItems "+
                         " WHERE substr(item_code,1,3)='"+str+"'"+
                         " ORDER BY 1 ";

          Vector vect  = new Vector();
          try
          {
               ORAConnection   oraConnection = ORAConnection.getORAConnection();
               Connection      theConnection = oraConnection.getConnection();
                               theConnection . setAutoCommit(true);
               Statement stat                = theConnection.createStatement();
               ResultSet res = stat.executeQuery(QS);

               while(res.next())
                    vect . addElement(res.getString(1));                        

               res.close();
               stat.close();

               String SCode = common.stuff(String.valueOf(common.getMissing(vect)));

               TMatCode       . setText(str+SCode);
               JOptionPane    . showMessageDialog(null,"Material Code is "+str+SCode,"Information",JOptionPane.INFORMATION_MESSAGE);
          }
          catch(Exception ex)
          {
               System         . out.println(ex);
               TMatCode       . setText("");
               JOptionPane    . showMessageDialog(null,"Problem in Assigning Material Code","Information",JOptionPane.INFORMATION_MESSAGE);
          }
     }*/

     public void refresh()
     {
          TMatCode       . setText(""); 
          TMatName       . setText("");
          TMatCatl       . setText("");
          TMatDraw       . setText("");
          TShortName     . setText("");
          TPartNo        . setText("");

          TInner         . setText("");
          TOuter         . setText("");
          TLength        . setText("");
          TThick         . setText("");
          TWidth         . setText("");
          TViscosity     . setText("");
          THeight        . setText("");

          TChapter       . setText("");
          TCommodity     . setText("");
          int iIndex = JCHSNType.getSelectedIndex();
		  if(iIndex==1)
		  {
				JCStkGroup.setSelectedItem("MISCELLANEOUS");
				JCMill.setSelectedItem("YES");
				JCUom.setSelectedItem("N.A.");	
		  }	
		  else{
				JCStkGroup     . setSelectedIndex(0);
			    JCUom          . setSelectedIndex(0);
		  }

          
          
          JCDept         . setSelectedIndex(0);
          JCMachine      . setSelectedIndex(0);
          JCMatGroup     . setSelectedIndex(0);
          JCVat          . setSelectedIndex(0);

          JCMill.setSelectedIndex(0);

          if(iMillCode==0)
          {
               JCMill.setEnabled(true);
          }
          else
          {
               JCMill.setEnabled(false);
          }

          JCBrand.setSelectedItem("N.A.");
          JCTemp.setSelectedItem("N.A.");
          JCMachine.setSelectedItem("N.A.");

          setStationary();
          JCSector.setSelectedItem("N.A.");

          BPhoto.setText("Attach Photo");
          setPhoto(BPhoto.getText());
          JTP.setSelectedIndex(0);
		  THsnCode. setText("");
		  FnGstRate. setText("");
	      FnCgst. setText("");
          FnSgst. setText("");
          FnIgst. setText("");
          FnCess. setText("");

     }
	private void removeHelpFrame() {
        try {
            this.dispose();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

     public void getMasterData()
     {
          ResultSet result    = null;

          VStkGroup      = new Vector();
          VStkGroupCode  = new Vector();
          VUom           = new Vector();
          VUomCode       = new Vector();
          VPaperSet      = new Vector();
          VSetCode       = new Vector();
          VSideName      = new Vector();
          VSideCode      = new Vector();

          VSector        = new Vector();
          VSectorCode    = new Vector();
          VDept          = new Vector();
          VDeptCode      = new Vector();
          VMacName       = new Vector();
          VMacCode       = new Vector();
          VMatGroup      = new Vector();
          VMatGroupCode  = new Vector();
          VBrand         = new Vector();
          VBrandCode     = new Vector();
          VTemp          = new Vector();
          VTempCode      = new Vector();

          VSTGroup       = new Vector();
          VSTGroupCode   = new Vector();


          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
                               theConnection .  setAutoCommit(true);
               Statement       stat          =  theConnection.createStatement();

               result = stat.executeQuery("Select GroupName,GroupCode From StockGroup Order By 1");

               while(result.next())
               {
                    VStkGroup.addElement(result.getString(1));
                    VStkGroupCode.addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select UomName,UomCode From UOM Order By UomName");

               while(result.next())
               {
                    VUom      . addElement(result.getString(1));
                    VUomCode  . addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select SetName,SetCode From PaperSet Order By SetCode");

               while(result.next())
               {
                    VPaperSet . addElement(result.getString(1));
                    VSetCode  . addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select SideName,SideCode From PaperSide Order By SideCode");

               while(result.next())
               {
                    VSideName . addElement(result.getString(1));
                    VSideCode . addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select Name,Code From IndustrySector Order By Name");

               while(result.next())
               {
                    VSector.addElement(result.getString(1));
                    VSectorCode.addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name");

               while(result.next())
               {
                    VDept.addElement(result.getString(1));
                    VDeptCode.addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select ModelName,ModelCode From scm.SqcModelMaster Order By ModelName");

               while(result.next())
               {
                    VMacName.addElement(result.getString(1));
                    VMacCode.addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select GroupName,GroupCode From MatGroup Order By GroupName");

               while(result.next())
               {
                    VMatGroup.addElement(result.getString(1));
                    VMatGroupCode.addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select Name,Code From Brand Order By Name");

               while(result.next())
               {
                    VBrand.addElement(result.getString(1));
                    VBrandCode.addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select Name,Code From Temperature Order By Name");

               while(result.next())
               {
                    VTemp.addElement(result.getString(1));
                    VTempCode.addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select Name,Code From STGroup Order By Code");

               while(result.next())
               {
                    VSTGroup.addElement(result.getString(1));
                    VSTGroupCode.addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("getMasterData :"+ex);
          }
     }

     private int getNextVal(String SSequence) throws Exception
     {
          int       iValue    = -1;
          String    QS        = " Select "+SSequence+".NextVal from dual ";
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();
               Statement      stat           = theConnection.createStatement();
               ResultSet      result         = stat.executeQuery(QS);
               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("getNextVal :"+SSequence+ex);
          }
          return iValue;
     }

	private void getHsnGstRate(String SHsnCde) throws Exception
     {
			int count = 0;
			StringBuffer sb = new StringBuffer();
						 sb.append(" select GSTRATE,CGST,SGST,IGST,CESS From HsnGstRate where HSNCODE='"+SHsnCde+"' "); 

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();
               Statement      stat           = theConnection.createStatement();
               ResultSet      result         = stat.executeQuery(sb.toString());
               while(result.next())
               {
					FnGstRate.setText(common.parseNull(result.getString(1)));
					FnCgst.setText(common.parseNull(result.getString(2)));
					FnSgst.setText(common.parseNull(result.getString(3)));
					FnIgst.setText(common.parseNull(result.getString(4)));
				    FnCess.setText(common.parseNull(result.getString(5)));
					count++;
               }
               result.close();
               stat.close();
			   if (count > 0) {
			   	if(common.toDouble(FnGstRate.getText())>0)
			   	{
						FnGstRate.setEditable(false);
						FnCgst.setEditable(false);
						FnSgst.setEditable(false);
						FnCess.setEditable(false);
			  	}
			  }
			  else{
						FnGstRate.setEditable(true);
						FnCgst.setEditable(true);
						FnSgst.setEditable(true);
						FnCess.setEditable(true);

						FnGstRate.setText("");
						FnCgst.setText("");
						FnSgst.setText("");
						FnIgst.setText("");
						FnCess.setText("");
			  }
          }
          catch(Exception ex)
          {
               System.out.println("getHsnGstRate :"+ex);
          }
     }
// Check hsncode in hsngstrate

     private int getHsnCodeCheck(String sHsnCode)
     {

       
          int       iCount=0;
          String    QS        = " Select count(hsncode) from HsnGstRate  where hsncode='"+sHsnCode+"'";
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();
               Statement      stat           = theConnection.createStatement();
               ResultSet      result         = stat.executeQuery(QS);
               if(result.next())
               {
                  iCount= result.getInt(1);
                   
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("gethsncodecheck :"+ex);
          }
          return iCount;
     }
 
	private void getTaxCategory() {
		
		vTaxCatCode = new Vector();
		vTaxCatName = new Vector();

        StringBuffer sb = new StringBuffer();

		sb.append(" Select CODE,NAME from taxcategory ");

        try {

            if(theConnection == null)
            {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
            }

            java.sql.PreparedStatement pst = theConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rst = pst.executeQuery();
            java.util.HashMap theMap = null;
            while (rst.next()) {
				vTaxCatCode.addElement(rst.getString(1));
				vTaxCatName.addElement(rst.getString(2));
            }
            rst.close();
            pst.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

/*
        
            class ComboItem
            {
                private String key;
                private String value;
        
                public ComboItem(String key, String value)
                {
                    this.key = key;
                    this.value = value;
                }
        
                @Override
                public String toString()
                {
                    return key;
                }
        
                public String getKey()
                {
                    return key;
                }
        
                public String getValue()
                {
                    return value;
                }
            }

            Add the ComboItem to your comboBox.

            comboBox.addItem(new ComboItem("Visible String 1", "Value 1"));
            comboBox.addItem(new ComboItem("Visible String 2", "Value 2"));
            comboBox.addItem(new ComboItem("Visible String 3", "Value 3"));
            
            Whenever you get the selected item.
            
            Object item = comboBox.getSelectedItem();
            String value = ((ComboItem)item).getValue();
*/
