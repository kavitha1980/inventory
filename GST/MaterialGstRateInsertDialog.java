package GST;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import javax.swing.border.*;
import java.io.*;
import java.util.*;
import java.awt.*;
import java.sql.*;
import java.net.InetAddress;

import guiutil.*;
import util.*;
import jdbc.*;

public class MaterialGstRateInsertDialog
{

     JLayeredPane        Layer;
     java.sql.Connection theConnection = null;
     private   int                           iUserCode;
     private   int                           iSelectedRow,iStatus;

     private   Common                        common;

     private   JDialog                       theDialog;

     private   JPanel                        TopPanel, MiddlePanel, ControlPanel, InfoPanel, BottomPanel,SearchPanel,HsnTyepPanel;
	
	 private   MyTextField    				 THsnCode;
	 private   FractionNumberField			 FnGstRate,FnCgst,FnSgst,FnIgst,FnCess;
	
	 private   JComboBox                     JCHSNType,JCTaxCat;

     private   JButton                       BSave,BExit;


     private   ArrayList                     theItemList;
	 private   String sSupplierCode="",SSupTable="",sGstStateCode="",sGstPartyType="";
     double dCGST=0,dSGST=0,dIGST=0,dCess=0;
	 Vector vTaxCatCode,vTaxCatName,VOldGSTRate,VOldTax,VHsnCode,VTaxcatcode;
	 boolean bAutoCommitFlag = true;
     String SHsnCode;

     // Creating Constructor...

     public MaterialGstRateInsertDialog(JLayeredPane Layer,int iMillCode,int iAuthCode,int iUserCode,int iStatus)
     {
			common                   = null;
			common                   = new Common();

			this.Layer            	= Layer;
			this.iUserCode			= iUserCode;
            this.iStatus            = iStatus;
			try{
			getTaxCategory();
            GetHsncode();
			createComponents();
			setLayouts();
			addComponents();
			showDialog();
			}
			catch(Exception ex){ex.printStackTrace();} 
     }


        public MaterialGstRateInsertDialog(JLayeredPane Layer,int iMillCode,int iAuthCode,int iUserCode,int iStatus,String SHsnCode)
     {
			common                   = null;
			common                   = new Common();

			this.Layer            	= Layer;
			this.iUserCode			= iUserCode;
            this.iStatus            = iStatus;  
            this.SHsnCode           = SHsnCode;
			try{
			getTaxCategory();
            GetHsncode();
			createComponents();
			setLayouts();
			addComponents();
			showDialog();
			}
			catch(Exception ex){ex.printStackTrace();} 
     } 

     // creating Components, setLayouts and Add Components..

     private void createComponents()
     {
          theDialog                = new JDialog(new JFrame(), "Gst Rate Insert", true);
		
		  THsnCode		 = new MyTextField(8);
		  JCTaxCat       = new MyComboBox(vTaxCatName);
          FnGstRate		 = new FractionNumberField();
		  FnCgst		 = new FractionNumberField();
		  FnSgst		 = new FractionNumberField();
		  FnIgst  	     = new FractionNumberField();
		  FnCess		 = new FractionNumberField();
          if(iStatus==1){
		  FnIgst.setEditable(false);
		  JCTaxCat       .setEnabled(false);
          }else{
             THsnCode        .setText(SHsnCode);     
           THsnCode       .setEditable(false);
		  FnCgst         .setEditable(false);
		  FnSgst         .setEditable(false);
		  FnIgst.setEditable(false);
          }
          TopPanel                 = new JPanel();
          BottomPanel              = new JPanel();

          BSave  				   = new JButton("Save");		  
          BExit                    = new JButton("Exit");

          // add Listeners..

		  BSave		. addActionListener(new ActionList());
          BExit     . addActionListener(new ActionList());
		  //THsnCode.addFocusListener(new HsnCodeFocusList());
           THsnCode.addKeyListener(new KeyList());
		  JCTaxCat	. addActionListener(new TaxSelectList());
		  FnGstRate.addFocusListener(new GstRateFocusList());
     }

     private void setLayouts()
     {
          TopPanel                 . setLayout(new GridLayout(7,2,5,5));
          BottomPanel              . setLayout(new GridLayout(1,2,5,5));

          TopPanel	               . setBorder(new TitledBorder("Hsn Details"));
          BottomPanel              . setBorder(new TitledBorder("Controls"));
     }

     private void addComponents()
     {		
		  TopPanel.add(new JLabel("Hsn / SASC Code "));
		  TopPanel.add(THsnCode);

		  TopPanel.add(new JLabel("Tax Type"));
		  TopPanel.add(JCTaxCat);

		  TopPanel.add(new JLabel("Gst Rate % "));
		  TopPanel.add(FnGstRate);

		  TopPanel.add(new JLabel("CGST % "));
		  TopPanel.add(FnCgst);

		  TopPanel.add(new JLabel("SGST % "));
		  TopPanel.add(FnSgst);

		  TopPanel.add(new JLabel("IGST % "));
		  TopPanel.add(FnIgst);

		  TopPanel.add(new JLabel("CESS % "));
		  TopPanel.add(FnCess);

          BottomPanel             . add(BSave);
          BottomPanel             . add(BExit);

     }

     private void showDialog()
     {
           theDialog                . getContentPane()  . add("North", TopPanel);
          //theDialog                . getContentPane()  . add("Center", MiddlePanel);
          theDialog                . getContentPane()  . add("South", BottomPanel);

          theDialog                . setBounds(80, 90, 640, 610);
          theDialog                . setVisible(true);
     }
	private class ActionList implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            if (ae.getSource() == BSave) {
                 

                        AlreadyExistsVector(String.valueOf(THsnCode.getText()));
 					if(isValidData())
                    {
                       /*  if(iStatus==2){
			             InsertDetails();}*/
			               
			            
						try
		                {
		                     if(bAutoCommitFlag)
		                     {
		                          theConnection  . commit();
								  System.out.println("Commit Completed");
		                          theConnection  . setAutoCommit(true);
		                     }
		                     else
		                     {
		                          theConnection  . rollback();
		                          theConnection  . setAutoCommit(true);
		                     }
		                }catch(Exception ex)
		                {
		                     ex.printStackTrace();
		                }
					}
            }
            if (ae.getSource() == BExit) {
                removeHelpFrame();
            }
        }
    }


  //changed key listener
	 public class KeyList extends KeyAdapter 
     {
		
           public void keyPressed(KeyEvent ke) 
          {              
			  try{
				  String sHsnCode = THsnCode.getText().trim().toUpperCase();

				  if(sHsnCode.length()>0)
				  {	
					  getHsnGstRate(sHsnCode);
               
                     /*  if(iStatus==1){
                        JCTaxCat.setEnabled(true);  
						FnGstRate.setEditable(true);
						FnCgst.setEditable(true);
						FnSgst.setEditable(true);
						FnCess.setEditable(true);

                        }else{*/
                        JCTaxCat.setEnabled(true);  
						FnGstRate.setEditable(true);
						FnCgst.setEditable(false);
						FnSgst.setEditable(false);
						FnCess.setEditable(true);
                       

                    }else{  

                        JCTaxCat.setEnabled(false);  
						FnGstRate.setEditable(false);
						FnCgst.setEditable(false);
						FnSgst.setEditable(false);
						FnCess.setEditable(false);

						FnGstRate.setText("");
						FnCgst.setText("");
						FnSgst.setText("");
						FnIgst.setText("");
						FnCess.setText("");
 
				  }					
			  }
			  catch(Exception ex)
			  {
				ex.printStackTrace();
			  }
          }
     }

	public class TaxSelectList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               int iIndex = JCTaxCat.getSelectedIndex();
			    if(iIndex==0){
                     /* if(iStatus==1){
						FnGstRate.setEditable(true);
						FnCgst.setEditable(true);
						FnSgst.setEditable(true);
						FnCess.setEditable(true);
                        }else{*/
                        FnGstRate.setEditable(true);
						FnCgst.setEditable(false);
						FnSgst.setEditable(false);
						FnCess.setEditable(true); 
                        
			   }		
			   if(iIndex==1 || iIndex==2){
						FnGstRate.setEditable(false);
						FnCgst.setEditable(false);
						FnSgst.setEditable(false);
						FnCess.setEditable(false);

						FnGstRate.setText("0");
						FnCgst.setText("0");
						FnSgst.setText("0");
						FnIgst.setText("0");
						FnCess.setText("0");
			   }
          }
     }
	public class HsnCodeFocusList implements FocusListener
     {
		  public void focusGained(FocusEvent e) {
	      };
          public void focusLost(FocusEvent e)
          {              
			  try{
				  String sHsnCode = THsnCode.getText().trim().toUpperCase();
				  if(sHsnCode.length()>0)
				  {	
					  getHsnGstRate(sHsnCode);
                  }
				  else{
                      /*  if(iStatus==1){
						FnGstRate.setEditable(true);
						FnCgst.setEditable(true);
						FnSgst.setEditable(true);
						FnCess.setEditable(true);
                        }else{*/
                       FnGstRate.setEditable(true);
						FnCgst.setEditable(false);
						FnSgst.setEditable(false);
						FnCess.setEditable(false);
                        
						FnGstRate.setText("");
						FnCgst.setText("");
						FnSgst.setText("");
						FnIgst.setText("");
						FnCess.setText("");
				  }					
			  }
			  catch(Exception ex)
			  {
				ex.printStackTrace();
			  }
          }
     }
	 public class GstRateFocusList implements FocusListener
     {
		  public void focusGained(FocusEvent e) {
	      };
          public void focusLost(FocusEvent e)
          {

                if(THsnCode.getText().trim().length()>0){
             /* if(iStatus==1){
              JCTaxCat       .setEnabled(true);
			  FnGstRate.setEditable(true);
			  FnCgst.setEditable(true);
			  FnSgst.setEditable(true);
			  FnCess.setEditable(true);
              }else{*/
                 JCTaxCat       .setEnabled(true);
			  FnGstRate.setEditable(true);
			  FnCgst.setEditable(false);
			  FnSgst.setEditable(false);
			  FnCess.setEditable(false);
                
                String SGstRate=(String)FnGstRate.getText() ;
                double dGstRate=common.toDouble(common.getRound(SGstRate,2));

              double dCGST=dGstRate/2;
              double dSGST=dGstRate/2;

              FnIgst.setText(FnGstRate.getText());
              FnCgst.setText(String.valueOf(dCGST));
              FnSgst.setText(String.valueOf(dSGST));
              } else{

                  JOptionPane.showMessageDialog(null,"HSN Code is Empty","Information",JOptionPane.INFORMATION_MESSAGE);
              
                        FnGstRate.setEditable(false);
						FnCgst.setEditable(false);
						FnSgst.setEditable(false);
						FnCess.setEditable(false);
               }
          }
     }

 	 public boolean isValidData()
     {
		 int iIndex = JCTaxCat.getSelectedIndex();

		  if(THsnCode.getText().trim().length()==0){
				JOptionPane.showMessageDialog(null," Please Enter HSN Code","Information",JOptionPane.INFORMATION_MESSAGE);
				THsnCode.requestFocus();
				return false;
		  }

          if(THsnCode.getText().trim().length() >8 || THsnCode.getText().trim().length() <8 ) {
				JOptionPane.showMessageDialog(null,"   HSN Code should contains 8 - Digits","Information",JOptionPane.INFORMATION_MESSAGE);
				THsnCode.requestFocus();
				return false;
		  }
		 if(iIndex==0 && common.toDouble(FnGstRate.getText())<=0){
				JOptionPane.showMessageDialog(null," Please Enter Gst Rate ","Information",JOptionPane.INFORMATION_MESSAGE);
				FnGstRate.requestFocus();
				return false;
		 }
		 if(common.toDouble(FnGstRate.getText())>0)
		 {
			double dCgst =0,dSgst=0,dIgst=0,dGstRate=0;
			double dTot = 0;
			dCgst = common.toDouble(FnCgst.getText());
			dSgst = common.toDouble(FnSgst.getText());
			dIgst = common.toDouble(FnIgst.getText());
			dGstRate = common.toDouble(FnGstRate.getText());
			dTot  = dCgst+dSgst;
			if( dTot != dGstRate || dCgst==0 || dSgst==0)
			{
				JOptionPane.showMessageDialog(null,"Total of Cgst & Sgst Must be equal to Gst Rate","Information",JOptionPane.INFORMATION_MESSAGE);
				return false;
			}
			if(dGstRate==0 && (dCgst>0 || dSgst>0 || dIgst>0)){
				JOptionPane.showMessageDialog(null,"Please Enter GST Rate","Information",JOptionPane.INFORMATION_MESSAGE);
				return false;
			}
 
		 }

                    int iGSTRate= common.toInt(FnGstRate.getText());

				   if(VOldGSTRate.contains(String.valueOf(iGSTRate)))
				    {

				        JOptionPane.showMessageDialog(null,"This GstRate Already Exists.","Information",JOptionPane.INFORMATION_MESSAGE);
						return false;
				    }

          
      
					
			       if(!VHsnCode.contains(String.valueOf(THsnCode.getText())))
				  {
			            InsertDetails();
			      }
					   
						
		 
                   
				     if( VHsnCode.contains(String.valueOf(THsnCode.getText()))    )
					 {

			          if(iStatus==1)
	                  {
			
              
					  if(JOptionPane.showConfirmDialog(null, "Are you sure you want to go Additional Gst Rate  For  Existing Hsncode?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
                         InsertDetails();
                      }else
                      {  
					     InsertDetails();
	                   }

					  }

     
		  
				  return true;
			 }
	private boolean InsertDetails() {

        int ireturn = 1;
	    StringBuffer sbinsert = new StringBuffer();
    
		PreparedStatement ps = null;

		sbinsert.append(" Insert into HsnGstRate (ID,HSNCODE,GSTRATE,CGST,SGST,IGST,CESS,USERCODE,DATETIME,SYSNAME,TAXCATCODE) ");
		sbinsert.append(" Values(HsnGstRate_Seq.nextval,?,?,?,?,?,?,?,sysdate,?,?) ");

        try {
			String iip = InetAddress.getLocalHost().getHostAddress();
           	if(theConnection == null)
			{
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
					theConnection   .	setAutoCommit(true);
					if(theConnection. getAutoCommit())
					theConnection   . setAutoCommit(false);

					ps = theConnection.prepareStatement(sbinsert.toString());
System.out.println("sbinsert.toString()==>"+sbinsert.toString());
					ps.setString(1,THsnCode.getText().trim());
					ps.setString(2,String.valueOf(common.toDouble(common.parseNull(FnGstRate.getText()))));
					ps.setString(3,String.valueOf(common.toDouble(common.parseNull(FnCgst.getText()))));
					ps.setString(4,String.valueOf(common.toDouble(common.parseNull(FnSgst.getText()))));
					ps.setString(5,String.valueOf(common.toDouble(common.parseNull(FnIgst.getText()))));
					ps.setString(6,String.valueOf(common.toDouble(common.parseNull(FnCess.getText()))));
					ps.setString(7,String.valueOf(iUserCode));
					ps.setString(8,common.parseNull(iip));
					ps.setString(9,common.parseNull(getTaxCode((String)JCTaxCat.getSelectedItem())));
					ps.executeUpdate();
    				if(ps != null){ps.close();}
                 JOptionPane.showMessageDialog(null, " Data Saved ", "Error", JOptionPane.INFORMATION_MESSAGE);

	
                removeHelpFrame();
               
			
        } catch (Exception ex) {

            ex.printStackTrace();
            bAutoCommitFlag     = false;
        }
        return true;
    }

   //Update

    
     private String getTaxCode(String STaxName)
     {
          int iIndex=-1;
          iIndex = vTaxCatName.indexOf(STaxName);
          if(iIndex!=-1)
               return common.parseNull((String)vTaxCatCode.elementAt(iIndex));
          else
               return "";
     }

     private void removeHelpFrame()
     {
          try
          {
               theDialog . setVisible(false);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
	 private void getHsnGstRate(String SHsnCde) throws Exception
     {
			int count = 0;
			StringBuffer sb = new StringBuffer();
						 sb.append(" select GSTRATE,CGST,SGST,IGST,CESS From HsnGstRate where HSNCODE='"+SHsnCde+"' "); 

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();
               Statement      stat           = theConnection.createStatement();
               ResultSet      result         = stat.executeQuery(sb.toString());
               while(result.next())
               {
					FnGstRate.setText(common.parseNull(result.getString(1)));
					FnCgst.setText(common.parseNull(result.getString(2)));
					FnSgst.setText(common.parseNull(result.getString(3)));
					FnIgst.setText(common.parseNull(result.getString(4)));
				    FnCess.setText(common.parseNull(result.getString(5)));
					count++;
               }
               result.close();
               stat.close();
			   if (count > 0) {
			   	if(common.toDouble(FnGstRate.getText())>0)
			   	{
						FnGstRate.setEditable(false);
						FnCgst.setEditable(false);
						FnSgst.setEditable(false);
						FnCess.setEditable(false);
                         if(iStatus==1){
						BSave.setEnabled(false);
                        }else{
                        BSave.setEnabled(true);
                        }
			  	}
			  }
			  else{
						FnGstRate.setEditable(true);
						FnCgst.setEditable(true);
						FnSgst.setEditable(true);
						FnCess.setEditable(true);

						FnGstRate.setText("");
						FnCgst.setText("");
						FnSgst.setText("");
						FnIgst.setText("");
						FnCess.setText("");
						BSave.setEnabled(true);
			  }
          }
          catch(Exception ex)
          {
               System.out.println("getHsnGstRate :"+ex);
          }
     }
	private void getTaxCategory() {
		
		vTaxCatCode = new Vector();
		vTaxCatName = new Vector();

        StringBuffer sb = new StringBuffer();

		sb.append(" Select CODE,NAME from taxcategory ");

        try {

            if(theConnection == null)
            {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
            }

            java.sql.PreparedStatement pst = theConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rst = pst.executeQuery();
            java.util.HashMap theMap = null;
            while (rst.next()) {
				vTaxCatCode.addElement(rst.getString(1));
				vTaxCatName.addElement(rst.getString(2));
            }
            rst.close();
            pst.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

 
 public void AlreadyExistsVector(String SHsnCode1)
      {
            VOldGSTRate       = new Vector();
            VOldTax           = new Vector();
           
         
            try
            {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
              ResultSet res ;
                 if(iStatus==1){
                     res  = theStatement.executeQuery("select gstrate,taxcatcode from HsnGstRate where hsncode='"+SHsnCode1+"'"); 
                  }else{
                   res  = theStatement.executeQuery("select gstrate,taxcatcode from HsnGstRate where hsncode='"+SHsnCode+"'");
                  }
                  while(res.next())
                  {
                        VOldGSTRate    .addElement(res.getString(1));
                        VOldTax        .addElement(res.getString(2));
                       
                   
                  }
                  res.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  System.out.println("Err3"+ex);
            }
      }

    public void GetHsncode()
      {
            VHsnCode      = new Vector();
           VTaxcatcode     = new Vector();
           
         
            try
            {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
                  ResultSet res  = theStatement.executeQuery("select hsncode,taxcatcode from HsnGstRate ");
                  while(res.next())
                  {
                        VHsnCode    .addElement(res.getString(1));
                        VTaxcatcode .addElement(res.getString(2));
                   
                  }
                  res.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  System.out.println("Err3"+ex);
            }
      }
}
