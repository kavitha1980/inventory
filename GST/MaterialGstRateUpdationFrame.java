package GST;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import javax.swing.border.*;
import java.util.*;
import java.awt.*;
import java.net.InetAddress;
import java.sql.*;
import jdbc.*;

import util.*;
import guiutil.*;

import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;

public class MaterialGstRateUpdationFrame extends JInternalFrame {
    //Global Declarations..

    JLayeredPane  Layer;
    private MaterialGstRateUpdationModel theAuthModel;
    private JTable thePrintTable;
    java.sql.Connection theConnection = null;
    private ArrayList theUpdateList;
    private JPanel TopPanel, MiddlePanel, BottomPanel;
    private JTextField searchField;
	private JComboBox JCTaxCat;
    private Common common;
    private JButton BInsert, BExit, BPrint, BAuth;
    private Vector theTableVector,vTaxCatCode,vTaxCatName,VTaxCode,VOldGSTRate,VOldTax;
    private int iMillCode,iAuthCode,iUserCode;
    String SItemTable;
    JRadioButton JRAll,JRUpdate,JRNotUpdate;
    boolean bAutoCommitFlag = true;
    InetAddress ip;
    Vector VStatus;
    public MaterialGstRateUpdationFrame(JLayeredPane Layer,int iMillCode,int iAuthCode,int iUserCode,String SItemTable) {
        common = null;
        common = new Common();

		this.Layer        = Layer;
		this.iMillCode    = iMillCode;
		this.iAuthCode    = iAuthCode;
		this.iUserCode    = iUserCode;
		this.SItemTable   = SItemTable;

		try{
        createComponents();
        setLayouts();
        addComponents();
		getTaxCategory();
		getMaterialDetails();
        setTableDetails();
        showDialog();
		}
		catch(Exception ex){ex.printStackTrace();}
        
    }

    // creating Components, setLayouts and Add Components..
    private void createComponents() {
        try {
			
			JCTaxCat = new JComboBox();
           
            TopPanel = new JPanel();
            MiddlePanel = new JPanel();
            BottomPanel = new JPanel();

            theAuthModel = new MaterialGstRateUpdationModel();
            thePrintTable = new JTable(theAuthModel);

            searchField = new JTextField();

            JRAll          = new JRadioButton("All",true);
            JRUpdate       = new JRadioButton("Updated");
            JRNotUpdate     = new JRadioButton("Not Updated");

            BInsert = new JButton("Insert");
            BAuth = new JButton("Update");
            BExit = new JButton("Exit");

            // add Listeners..

            BInsert.addActionListener(new ActionList());
            BAuth.addActionListener(new ActionList());
            BExit.addActionListener(new ActionList());
			searchField. addKeyListener(new SearchKeyList());
			thePrintTable   .   addMouseListener(new MouseList());
            JRAll.addActionListener(new RadioList());
            JRUpdate.addActionListener(new RadioList());
            JRNotUpdate .addActionListener(new RadioList());
            thePrintTable.addKeyListener(new KeyList());
          //JCTaxCat.addActionListener(new TaxSelectList());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setLayouts() {
        setTitle("Material GST Rate Updation Frame");
        TopPanel.setLayout(new GridLayout(2, 2, 5, 5));
        MiddlePanel.setLayout(new GridLayout(1, 1, 5, 5));

        BottomPanel.setLayout(new FlowLayout());
        TopPanel.setBorder(new TitledBorder("Search"));
        MiddlePanel.setBorder(new TitledBorder("Materials List"));
        BottomPanel.setBorder(new TitledBorder("Controls"));
    }

    private void addComponents() {
        try {
						
            TopPanel.add(new JLabel("HSN Code "));
            TopPanel.add(searchField);

            TopPanel.add(new JLabel("Insert HSN"));
		    TopPanel.add(BInsert);



            TopPanel.add(JRAll);
            TopPanel.add(JRUpdate);

            TopPanel.add(JRNotUpdate);
 	        TopPanel.add(new JLabel(""));

            MiddlePanel.add(new JScrollPane(thePrintTable));

           BottomPanel.add(new JLabel("F4 ===> To Add Additional GSTRate "));  
            BottomPanel.add(BAuth);
            BottomPanel.add(BExit);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void showDialog() {
        getContentPane().add("North", TopPanel);
        getContentPane().add("Center", MiddlePanel);
        getContentPane().add("South", BottomPanel);

        setBounds(80, 90, 800, 500);
        setVisible(true);
    }

//key listener

    private class KeyList extends KeyAdapter {

        public void keyPressed(KeyEvent ke) {

              int iRow = thePrintTable.getSelectedRow();
              int iCol = thePrintTable.getSelectedColumn();
             

               if (ke.getKeyCode() == KeyEvent.VK_F4)
               {


                   
                    if(iCol==1){

                   
 
                     String SGstRate    =   common.parseNull((String)theAuthModel.getValueAt(iRow, 3));

                      int iTaxCode=common.toInt((String)VTaxCode.elementAt(iRow));

                     
                   
                     if((common.toInt(SGstRate) > 0 ) || iTaxCode >1 )
                      {
							String SHsnCode    = common.parseNull((String)theAuthModel.getValueAt(iRow, 1));
							new MaterialGstRateInsertDialog (Layer,iMillCode,iAuthCode,iUserCode,2,SHsnCode);
							getMaterialDetails();
							setTableDetails();
                   //new AdditionalGstRateInsertDialog (Layer,iMillCode,iAuthCode,iUserCode,SHsnCode);
                      }

                }
                 
              }

      }
}

    private class KList extends KeyAdapter {

        public void keyReleased(KeyEvent ke) {

              
              int iRow = thePrintTable.getSelectedRow();
              int iCol = thePrintTable.getSelectedColumn();
              int iIndex = JCTaxCat.getSelectedIndex();

		  if(iCol==2 || iCol==3 || iCol==4)
          	keyTypedEvent(iRow,iCol,iIndex);

       }


   }

       private void keyTypedEvent(int iRow,int iCol,int iIndex) 
       {   

           // if (ke.getKeyCode() == KeyEvent.VK_TAB) {
 System.out.println(iRow+":"+iCol+":"+iIndex);
              //if(iCol==3)
               {  

		            if(iIndex==0)
		            {

		            double dGstrate = common.toDouble(common.getRound((String.valueOf( theAuthModel.getValueAt(iRow, 3))),2));  
		            double dCgst    =dGstrate/2;
		            double dSgst    =dGstrate/2;
		            double dIgst    =dGstrate;


		            thePrintTable . setValueAt(String.valueOf(dCgst),iRow,4);
		            thePrintTable . setValueAt(String.valueOf(dSgst),iRow,5);
		            thePrintTable . setValueAt(String.valueOf(dIgst),iRow,6);  
		            }
		             if(iIndex==1 || iIndex==2){

		            thePrintTable . setValueAt("0",iRow,3);
		            thePrintTable . setValueAt("0",iRow,4);
		            thePrintTable . setValueAt("0",iRow,5);
		            thePrintTable . setValueAt("0",iRow,6);  
		            thePrintTable . setValueAt("0",iRow,7);  
		            }

           }

        }

  //  }



    private class ActionList implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            if (ae.getSource() == BInsert) {
                    new MaterialGstRateInsertDialog (Layer,iMillCode,iAuthCode,iUserCode,1);
                    getMaterialDetails();
				   setTableDetails();
            }
            if (ae.getSource() == BAuth) {

                  int iRow = thePrintTable.getSelectedRow();
             String SHsnCode    = common.parseNull((String)theAuthModel.getValueAt(iRow,1));
                AlreadyExistsVector(SHsnCode);
                if(validClick() ) {
					if(checkRate()==0  ){//&& updatevalid()
			            if (updateDetails()) {
			                JOptionPane.showMessageDialog(null, " Data Saved ", "Error", JOptionPane.INFORMATION_MESSAGE);
			                getMaterialDetails();
			                setTableDetails();
			            }
						try
		                {
		                     if(bAutoCommitFlag)
		                     {
		                          theConnection  . commit();
								  System.out.println("Commit Completed");
		                          theConnection  . setAutoCommit(true);
		                     }
		                     else
		                     {
		                          theConnection  . rollback();
		                          theConnection  . setAutoCommit(true);
		                     }
		                }catch(Exception ex)
		                {
		                     ex.printStackTrace();
		                }
					}
               }
			   else{
					JOptionPane.showMessageDialog(null, "Select Check Box to Save", "Information", JOptionPane.ERROR_MESSAGE);
			   }	
            }
            if (ae.getSource() == BExit) {
                removeHelpFrame();
            }
        }
    }

  //radiolist

   public class RadioList implements ActionListener
    {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAll)
               {
                    JRAll.setSelected(true);
                    JRUpdate.setSelected(false);
                    JRNotUpdate.setSelected(false);
                    getMaterialDetails();
                    setTableDetails();
               }

              if(ae.getSource()==JRUpdate)
               {
                    JRUpdate.setSelected(true);
                    JRAll.setSelected(false);
                    JRNotUpdate.setSelected(false);
                    getMaterialDetails();
                    setTableDetails();
               }

             if(ae.getSource()==JRNotUpdate)
               {
                    JRNotUpdate.setSelected(true);
                    JRAll.setSelected(false);
                    JRUpdate.setSelected(false);
                    getMaterialDetails();
                    setTableDetails();
               }

         }

     }    
	private class SearchKeyList extends KeyAdapter
    {
            public void keyReleased(KeyEvent ke)
            {
                searchItems();     
            }

            public void keyPressed(KeyEvent ke)
            {
                searchItems();
            }    

            public void keyTyped(KeyEvent ke)
            {
                searchItems();
            }
    }
	
	private class MouseList extends MouseAdapter
    {
        public void mouseReleased(MouseEvent me)
        {
            getValidGst();
        }
    }
	public class TaxSelectList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               String sTaxName = common.parseNull((String)JCTaxCat.getSelectedItem());
			   System.out.println("sTaxName-->"+sTaxName);	
			   //setControls(iIndex);
          }
     }
    private void removeHelpFrame() {
        try {
            this.dispose();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
	private void searchItems()
     {
          String SSearchStr        = common.parseNull(searchField.getText()).trim();

          int iIndex               = -1;

          for(int i=0; i<theAuthModel.getRowCount(); i++)
          {
               String SHsnCode    = common.parseNull((String)theAuthModel.getValueAt(i, 1));

               if(SHsnCode.startsWith(SSearchStr.toUpperCase()))
               {
                    iIndex         = i;
                    break;
               }
          }
          if(iIndex != -1)
          {
                 thePrintTable       . setRowSelectionInterval(iIndex, iIndex);
                 Rectangle theRect   = thePrintTable.getCellRect(iIndex, 0, true);
                 thePrintTable       . scrollRectToVisible(theRect);
                 thePrintTable       . setSelectionBackground(Color.green);	
                  	   
          }
     }

    private void setTableDetails() {
        theAuthModel.setNumRows(0);
        theAuthModel.VStatus=new Vector();
            VTaxCode  = new Vector();

        java.util.HashMap theMap = new java.util.HashMap();
		try{
        for (int i = 0; i < theUpdateList.size(); i++) {

            theMap = (java.util.HashMap) theUpdateList.get(i);
            theTableVector = new Vector();
			TableColumn SColumnTaxType    = thePrintTable.getColumn("TaxType");
			JCTaxCat	                  = new MyComboBox(vTaxCatName);
			SColumnTaxType				  . setCellEditor(new DefaultCellEditor(JCTaxCat));
			String STaxCode=common.parseNull((String) theMap.get("TAXCATCODE"));
            VTaxCode.addElement(STaxCode);
            
            String SGstRate=common.parseNull((String) theMap.get("GSTRATE"));
            String SStatus="";
           if((common.toInt(SGstRate) > 0 ) || (common.toInt(STaxCode) > 1) )
            {

              SStatus="1";
            }else {
              SStatus="0";
            }
             theAuthModel.VStatus.addElement(SStatus);
       

            String STax="";
            if(STaxCode.equals("1"))
            {
                 STax="Taxable";
             } if (STaxCode.equals("2"))
             {
                 STax="Zero Tax";
             } if(STaxCode.equals("3")){
                  STax="Tax Extempted";
             } 

            theTableVector.addElement(String.valueOf(i + 1));
            theTableVector.addElement(common.parseNull((String) theMap.get("HSNCODE")));
            //theTableVector.addElement("Taxable");
            theTableVector.addElement(STax);
            theTableVector.addElement(common.parseNull((String) theMap.get("GSTRATE")));
            theTableVector.addElement(common.parseNull((String) theMap.get("CGST")));
            theTableVector.addElement(common.parseNull((String) theMap.get("SGST")));
			theTableVector.addElement(common.parseNull((String) theMap.get("IGST")));
			theTableVector.addElement(common.parseNull((String) theMap.get("CESS")));
            theTableVector.addElement(new Boolean(false));

            theAuthModel.appendRow(theTableVector);
        }
		}
		catch(Exception ex){ex.printStackTrace();}
    }

    private boolean updateDetails() {

        int ireturn = 1;
        java.util.HashMap theMap = new java.util.HashMap();
        StringBuffer sb = new StringBuffer();
	    StringBuffer sbinsert = new StringBuffer();
        PreparedStatement ps = null;
		PreparedStatement psins = null;

		sb.append(" Update HsnGstRate set GSTRATE=?,CGST=?,SGST=?,IGST=?,CESS=?,TAXCATCODE=? ");
		sb.append(" Where HSNCODE=? And Id=? ");

		sbinsert.append(" Insert into HsnGstRate (ID,HSNCODE,GSTRATE,CGST,SGST,IGST,CESS,USERCODE,DATETIME,SYSNAME,TAXCATCODE) ");
		sbinsert.append(" Values(HsnGstRate_Seq.nextval,?,?,?,?,?,?,?,sysdate,?,?) ");

        try {
			String iip = InetAddress.getLocalHost().getHostAddress();
           	if(theConnection == null)
			{
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
			  theConnection   .	setAutoCommit(true);
			  if(theConnection. getAutoCommit())
			  theConnection   . setAutoCommit(false);

            for (int index = 0; index < theAuthModel.getRows(); index++) {
                Boolean isSave = (Boolean) theAuthModel.getValueAt(index, 8);
                if (isSave.booleanValue()) {
                    theMap = (java.util.HashMap) theUpdateList.get(index);

                    String SCode = common.parseNull((String) theMap.get("HSNCODE")).trim().toUpperCase();
                    String sId = common.parseNull((String) theMap.get("ID"));
				    int iRateStatus = common.toInt(common.parseNull((String) theMap.get("RATESTATUS")));
					if(iRateStatus==1){
	                    ps = theConnection.prepareStatement(sb.toString());
	
	                    ps.setString(1, String.valueOf(common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index,3))))));
	                    ps.setString(2, String.valueOf(common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index,4))))));
	                    ps.setString(3, String.valueOf(common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index,5))))));
	                    ps.setString(4, String.valueOf(common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index,3))))));
	                    ps.setString(5, String.valueOf(common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index,7))))));
						ps.setString(6, common.parseNull(getTaxCode(String.valueOf( theAuthModel.getValueAt(index,2)))));
						ps.setString(7, SCode);
						ps.setString(8, sId);
	                   ps.executeUpdate();
				  }	
				  if(iRateStatus==2){
						psins = theConnection.prepareStatement(sbinsert.toString());
	
						psins.setString(1, SCode);
						psins.setString(2, String.valueOf(common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index,3))))));
	                    psins.setString(3, String.valueOf(common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index,4))))));
	                    psins.setString(4, String.valueOf(common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index,5))))));
	                    psins.setString(5, String.valueOf(common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index,3))))));
	                    psins.setString(6, String.valueOf(common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index,7))))));
						psins.setString(7, String.valueOf(iUserCode));
						psins.setString(8, iip);
						psins.setString(9, common.parseNull(getTaxCode(String.valueOf( theAuthModel.getValueAt(index,2)))));
	                    psins.executeUpdate();
				  }
                }
            }
			if(ps != null){ps.close();}
			if(psins != null){psins.close();}
			
        } catch (Exception ex) {

            ex.printStackTrace();
            bAutoCommitFlag     = false;
        }
        return true;
    }
	 private String getTaxCode(String STaxName)
     {
          int iIndex=-1;
          iIndex = vTaxCatName.indexOf(STaxName);
          if(iIndex!=-1)
               return common.parseNull((String)vTaxCatCode.elementAt(iIndex));
          else
               return "";
     }

    private void getMaterialDetails() {
        theUpdateList = new ArrayList();
        theUpdateList.clear();
        StringBuffer sb = new StringBuffer();

		/* sb.append(" select Id,HSNCODE,GSTRATE,CGST,SGST,IGST,CESS,RateStatus,taxcatcode from( ");
		sb.append(" select HsnGstRate.ID,HSNCODE,GSTRATE,CGST,SGST,IGST,CESS,1 as RateStatus,HsnGstRate.TAXCATCODE ");
		sb.append(" From HsnGstRate  "); //Where (GSTRATE=0 or GSTRATE is null)
        if(!JRAll.isSelected()) 
        {
		    if(JRUpdate.isSelected())
            {
			  sb.append(" where (HsnGstRate.GSTRATE >0 or HsnGstRate.TaxCatCode>1) ");   
			}
            if(JRNotUpdate.isSelected())
            {
			  sb.append(" where (HsnGstRate.GSTRATE =0 and   HsnGstRate.TaxCatCode<=1)");   
			} 

        } 
		sb.append(" Union all ");
		sb.append(" select distinct invitems.Id,invitems.HsnCode,0 as GSTRATE,0 as CGST,0 as SGST,0 as IGST,0 as CESS,2 as RateStatus,0 as taxcatcode");
		sb.append(" from invitems where (invitems.HsnCode<>'0' or invitems.HsnCode is not null)   ");

		sb.append(" and invitems.HsnCode not in(select HSNCODE from HsnGstRate  ");
       if(!JRAll.isSelected()) 
        {
			 if(JRUpdate.isSelected())
             {
			  sb.append(" where (HsnGstRate.GSTRATE >0  or HsnGstRate.TaxCatCode>1)  ");   
			 }
             if(JRNotUpdate.isSelected())
             {
			  sb.append(" where (HsnGstRate.GSTRATE =0  and   HsnGstRate.TaxCatCode<=1) ");   
			 }  
        }
        
		sb.append(" )) where  HSNCODE<>'0' ");*/

         if(JRUpdate.isSelected())
         {

			sb.append(" select Id,HSNCODE,GSTRATE,CGST,SGST,IGST,CESS,RateStatus,taxcatcode from(  ");
			sb.append(" select HsnGstRate.ID,HSNCODE,GSTRATE,CGST,SGST,IGST,CESS,1 as RateStatus,HsnGstRate.TAXCATCODE  ");
			sb.append(" From HsnGstRate  ");
			sb.append(" where HsnGstRate.GSTRATE >0 or HsnGstRate.TaxCatCode>1 ) ");
            sb.append("  order by HSNCode  ");

         }
         if(JRNotUpdate.isSelected())
         {
			sb.append(" select HSNCODE,id,GSTRATE,CGST,SGST,IGST,CESS,RateStatus,taxcatcode from(  "); 
			sb.append(" select HsnGstRate.ID,HSNCODE,GSTRATE,CGST,SGST,IGST,CESS,1 as RateStatus,HsnGstRate.TAXCATCODE  ");
			sb.append(" From HsnGstRate    where HsnGstRate.GSTRATE =0 and   HsnGstRate.TaxCatCode<=1)    ");
			sb.append(" Union all    ");
			sb.append(" select  distinct HsnCode,0 as id,0 as GSTRATE,0 as CGST,0 as SGST,0 as IGST,0 as CESS,   ");
			sb.append(" 2 as RateStatus,0 as taxcatcode from invitems where invitems.HsnCode<>'0'    ");
			sb.append(" and invitems.HsnCode is not null and hsncode not in (select hsncode from hsngstrate)      ");
            sb.append(" union all select hsncode ,0 as id,0 as gstrate,0 as cgst,0 as sgst,0 as Igst,0 as cess,2 as ratestatus,0 as taxcatcode  from multihsn   " );
            sb.append(" where hsncode not in (select hsncode from hsngstrate)  ");
            sb.append("  order by HSNCode  ");
         }
          if(JRAll.isSelected()) 
         {
				   
		sb.append(" select HSNCODE,id,GSTRATE,CGST,SGST,IGST,CESS,RateStatus,taxcatcode from(   ");
		sb.append(" select HSNCODE,HsnGstRate.ID,GSTRATE,CGST,SGST,IGST,CESS,1 as RateStatus,HsnGstRate.TAXCATCODE  ");
		sb.append(" From HsnGstRate )   Union all     ");
		sb.append(" select distinct HsnCode,0 as id,0 as GSTRATE,0 as CGST,0 as SGST,0 as IGST,0 as CESS,2 as RateStatus,0 as taxcatcode   ");
		sb.append(" from invitems where invitems.HsnCode<>'0' and invitems.HsnCode is not null and invitems.hsncode not in (select hsncode from hsngstrate)    "); 
		sb.append(" union all select   hsncode ,0 as id,0 as gstrate,0 as cgst,0 as sgst,0 as Igst,0 as cess,2 as ratestatus,0 as taxcatcode  from multihsn ");
        sb.append(" where hsncode not in (select hsncode from hsngstrate)  ");
        sb.append("  order by HSNCode  ");   

          }
         
               
         System.out.println("QS==>"+sb.toString());

        try {

            if(theConnection == null)
            {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
            }

            java.sql.PreparedStatement pst = theConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rst = pst.executeQuery();
            java.sql.ResultSetMetaData rsm = rst.getMetaData();
            java.util.HashMap theMap = null;
            while (rst.next()) {
                theMap = new java.util.HashMap();
                for (int i = 0; i < rsm.getColumnCount(); i++) {
                    theMap.put(rsm.getColumnName(i + 1), rst.getString(i + 1));
                }
                theUpdateList.add(theMap);
            }
            rst.close();
            pst.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
	private void getTaxCategory() {
		
		vTaxCatCode = new Vector();
		vTaxCatName = new Vector();

        StringBuffer sb = new StringBuffer();

		sb.append(" Select CODE,NAME from taxcategory ");

        try {

            if(theConnection == null)
            {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
            }

            java.sql.PreparedStatement pst = theConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rst = pst.executeQuery();
            java.util.HashMap theMap = null;
            while (rst.next()) {
				vTaxCatCode.addElement(rst.getString(1));
				vTaxCatName.addElement(rst.getString(2));
            }
            rst.close();
            pst.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean validations()
    {
        String SError       = "";
        int iSlNo           = 1;
        String sTid = common.parseNull(searchField.getText()).trim();

        if(sTid.length() <= 0)
        {
            SError   += (iSlNo++)+"). Please Enter HsN Code Id .\n";
        }
        if(SError.length() > 0)
        {
            JOptionPane.showMessageDialog(null, SError, "Information", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
    private boolean validClick()
    {
        for(int j=0;j<theAuthModel.getRowCount();j++)
        {
            Boolean isSave = (Boolean)theAuthModel.getValueAt(j,8);
            if(isSave.booleanValue())
            {
                return true;
            }
            

        }
         return false;
    }
	private void getValidGst(){
    	int iExisit = 0;
    	java.util.HashMap theMap = new java.util.HashMap();
    	int index  = thePrintTable.getSelectedRow();
    	Boolean isSave = (Boolean) theAuthModel.getValueAt(index, 8);
        
    	if (isSave.booleanValue()) {

  int iIndex = JCTaxCat.getSelectedIndex();
                 if(iIndex==0)
		            {

		            double dGstrate = common.toDouble(common.getRound((String.valueOf( theAuthModel.getValueAt(index, 3))),2));  
		            double dCgst    =dGstrate/2;
		            double dSgst    =dGstrate/2;
		            double dIgst    =dGstrate;


		            thePrintTable . setValueAt(String.valueOf(dCgst),index,4);
		            thePrintTable . setValueAt(String.valueOf(dSgst),index,5);
		            thePrintTable . setValueAt(String.valueOf(dIgst),index,6);  
		            }
		             if(iIndex==1 || iIndex==2){

		            thePrintTable . setValueAt("0",index,3);
		            thePrintTable . setValueAt("0",index,4);
		            thePrintTable . setValueAt("0",index,5);
		            thePrintTable . setValueAt("0",index,6);  
		            thePrintTable . setValueAt("0",index,7);  
		            }

        	double dGstRate  = common.toDouble(common.parseNull((String.valueOf(theAuthModel.getValueAt(index, 3)))));
			double dCgst  = common.toDouble(common.parseNull((String.valueOf(theAuthModel.getValueAt(index, 4)))));
			double dSgst  = common.toDouble(common.parseNull((String.valueOf(theAuthModel.getValueAt(index, 5)))));
			double dIgst  = common.toDouble(common.parseNull((String.valueOf( theAuthModel.getValueAt(index, 6)))));
            String STax   = common.parseNull((String.valueOf(theAuthModel.getValueAt(index, 2))));

            System.out.println("TAX==>"+STax); 
			double dTot = 0;
			dTot  = dCgst+dSgst;						
		    if(dGstRate>0){
				if( dTot != dGstRate || dCgst==0 || dSgst==0)
		    	{
		        	JOptionPane.showMessageDialog(null, "Total of Cgst & Sgst Must be equal to Gst Rate", "Error Information", JOptionPane.ERROR_MESSAGE);
		        	theAuthModel.setValueAt(new java.lang.Boolean(false),index,8);
		    	}
				else{
					theAuthModel.setValueAt(theAuthModel.getValueAt(index, 3),index, 6);
				}
			}
			if(dGstRate == 0 && (dCgst>0 || dSgst>0 || dIgst>0))
			{
				JOptionPane.showMessageDialog(null, "Please Enter GST Rate", "Error Information", JOptionPane.ERROR_MESSAGE);
				theAuthModel.setValueAt(new java.lang.Boolean(false),index,8);
			}


            
			   int iTax = -1;

			   if(!STax.equals(""))
			   {
               		iTax = JCTaxCat.getSelectedIndex();
			   }

			   System.out.println("iTax:"+iTax);

                   
				   if( STax.equals("") || ( iTax==-1 ) )
				     {
                        
                        JOptionPane.showMessageDialog(null, "Please select Tax Type ", "Error Information", JOptionPane.ERROR_MESSAGE);
					     theAuthModel.setValueAt(new java.lang.Boolean(false),index,8);
						return;

				     } 
  
                   if(iTax==0)
		           {
		            	if(dGstRate <=0 )
						{
							JOptionPane.showMessageDialog(null, "GSTRATE must be greater than Zero ", "Error Information", JOptionPane.ERROR_MESSAGE);
					      theAuthModel.setValueAt(new java.lang.Boolean(false),index,8);
						   return;
						}

		           }

		           if(iTax==1 || iTax==2)
		           {
		            	if(dGstRate >0 && (dCgst>0 || dSgst>0 || dIgst>0))
						{
							JOptionPane.showMessageDialog(null, "GSTRate must be in Zero ", "Error Information", JOptionPane.ERROR_MESSAGE);
					      theAuthModel.setValueAt(new java.lang.Boolean(false),index,8);
						}

		           }

                if(iTax==1 || iTax==2 || iTax==0)
		           {
		            	if(dGstRate >100 || (dCgst>100 || dSgst>100 || dIgst>100))
						{
							JOptionPane.showMessageDialog(null, "All Values not exceeded 100 ", "Error Information", JOptionPane.ERROR_MESSAGE);
					     theAuthModel.setValueAt(new java.lang.Boolean(false),index,8);
						}

		           }

            
		         

    	}
	    else
    	{
    	}
	}
	private int checkRate()
    {
        int iRow = thePrintTable.getSelectedRow();
		int icnt = 0;
        for(int index=0;index<theAuthModel.getRowCount();index++)
        {
            Boolean isSave = (Boolean)theAuthModel.getValueAt(index,8);
            if(isSave.booleanValue())
            {
				double dGstRate  = common.toDouble(common.parseNull(String.valueOf(theAuthModel.getValueAt(index, 3))));
				double dCgst  = common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index, 4))));
				double dSgst  = common.toDouble(common.parseNull(String.valueOf(theAuthModel.getValueAt(index, 5))));
				double dIgst  = common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index, 6))));
				double dTot = 0;
                 String STax   = common.parseNull((String.valueOf(theAuthModel.getValueAt(index, 2))));

				dTot  = dCgst+dSgst;
           
        
						if(dGstRate>0){
							if( dTot != dGstRate || dCgst==0 || dSgst==0)
							{
								JOptionPane.showMessageDialog(null, "Total of Cgst & Sgst Must be equal to Gst Rate", "Error Information", JOptionPane.ERROR_MESSAGE);
								icnt++;
							}
						}
				
						if(dGstRate==0 && (dCgst>0 || dSgst>0 || dIgst>0))
						{
							JOptionPane.showMessageDialog(null, "Please Enter GST Rate", "Error Information", JOptionPane.ERROR_MESSAGE);
							icnt++;
						}

                       int iTax = JCTaxCat.getSelectedIndex();

                           
						   if(STax.equals("") || (iTax==-1))
						     {
                                
                                JOptionPane.showMessageDialog(null, "Please select Tax Type in Combo Box", "Error Information", JOptionPane.ERROR_MESSAGE);
							        icnt++;

						     } 

				           if(iTax==1 || iTax==2)
				           {
				            	if(dGstRate >0 && (dCgst>0 || dSgst>0 || dIgst>0))
								{
									JOptionPane.showMessageDialog(null, "GSTRate must be in Zero ", "Error Information", JOptionPane.ERROR_MESSAGE);
							        icnt++;
								}

				           }

                        if(iTax==1 || iTax==2 || iTax==0)
				           {
				            	if(dGstRate >100 || (dCgst>100 || dSgst>100 || dIgst>100))
								{
									JOptionPane.showMessageDialog(null, "GSTRate must be in Zero ", "Error Information", JOptionPane.ERROR_MESSAGE);
							        icnt++;
								}

				           }

                     if(VOldGSTRate.contains(String.valueOf(dGstRate)))
				    {

				        JOptionPane.showMessageDialog(null,"This GstRate Already Exists.","Information",JOptionPane.INFORMATION_MESSAGE);
						icnt++;
				    }
                  
    		}
	 	}
		return icnt;
	}

 //valid

  private boolean updatevalid()
    {
        int iRow = thePrintTable.getSelectedRow();
		int icnt = 0;
        for(int index=0;index<theAuthModel.getRowCount();index++)
        {
            Boolean isSave = (Boolean)theAuthModel.getValueAt(index,8);
            if(isSave.booleanValue())
            {
				double dGstRate  = common.toDouble(common.parseNull(String.valueOf(theAuthModel.getValueAt(index, 3))));
				double dCgst  = common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index, 4))));
				double dSgst  = common.toDouble(common.parseNull(String.valueOf(theAuthModel.getValueAt(index, 5))));
				double dIgst  = common.toDouble(common.parseNull(String.valueOf( theAuthModel.getValueAt(index, 6))));
				double dTot = 0;
				dTot  = dCgst+dSgst;
           
				 int iTax = JCTaxCat.getSelectedIndex();


				  if(iTax==1 || iTax==2)
				           {
				            	if(dGstRate >0 && (dCgst>0 || dSgst>0 || dIgst>0))
								{
									JOptionPane.showMessageDialog(null, "GSTRate must be in Zero ", "Error Information", JOptionPane.ERROR_MESSAGE);
							        return false;
								}

				           }
                  
    		}
	 	}
		return false;
	}

 public void AlreadyExistsVector(String SHsnCode)
      {
            VOldGSTRate       = new Vector();
            VOldTax           = new Vector();
           
         
            try
            {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
            
               
                 ResultSet  res  = theStatement.executeQuery("select gstrate,taxcatcode from HsnGstRate where hsncode='"+SHsnCode+"'");
                 
                  while(res.next())
                  {
                        VOldGSTRate    .addElement(res.getString(1));
                        VOldTax        .addElement(res.getString(2));
                       
                   
                  }
                  res.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  System.out.println("Err3"+ex);
            }
      }



}

/*

  int iTax = JCTaxCat.getSelectedIndex();

          if(iTax==1 || iTax==2)
                   {
                    	if(dGstRate==0 && (dCgst>0 || dSgst>0 || dIgst>0))
						{
							JOptionPane.showMessageDialog(null, "Tax Type not in Taxable Type", "Error Information", JOptionPane.ERROR_MESSAGE);
							
						}

                   }

*/
