package GST;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import jdbc.*;

import util.*;
import guiutil.*;


public class MaterialGstRateUpdationModel extends DefaultTableModel
{
     String ColumnName[] = {"S.No" , "HSN Code" ,"TaxType" ,"GSTRATE", "CGST", "SGST", "IGST","CESS","Update" };
     String ColumnType[] = {"S"    , "S"        ,"E" 	   , "E"     , "E"   , "E"   , "E"   ,"E"   , "B"};
     int  iColumnWidth[] = {30     , 60         ,30 	   , 30      , 30    , 30    , 30    ,30    , 30};
     Vector VStatus;
     Common common = new Common();
     public MaterialGstRateUpdationModel()
     {
         
	      VStatus = new Vector(); 
          setDataVector(getRowData(),ColumnName);
 
     }


     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
       System.out.println("VStatus==>"+common.toInt((String)VStatus.elementAt(iRow)));
	   	 if(common.toInt((String)VStatus.elementAt(iRow))==0)
         {          
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;
		  }


          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";

          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}

