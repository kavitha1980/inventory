package GST;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import javax.swing.table.*;
import java.net.InetAddress;
import guiutil.*;
import util.*;
import jdbc.*;


public class MaterialHsnCodeModification extends JInternalFrame
{
      JLayeredPane  Layer;
      Object        RowData[][],RowData1[][];

      String        ColumnData[] = {"Code","Item Name","UoM","Stock Group","Drawing","Catl","Old HSN Code","New Hsn Code","Click for Modify"};
      String        ColumnType[] = {"S"   ,"S"        ,"S"  ,"S"          ,"S"      ,"S"   , "S"      , "E"    ,"B"              };


      String        ColumnData1[] = {"Code","Item Name","UoM","Stock Group","Drawing","Catl","Old HSN Code","Additional Hsn Code","Click for Modify"};
      String        ColumnType1[] = {"S"   ,"S"        ,"S"  ,"S"          ,"S"      ,"S"   , "S"      , "E"    ,"B"              };
      int iColWidth[]              = {80   ,200         ,80    ,80          ,80       ,80     ,250        , 100  ,80               };
 

      Vector        VMCode,VMName,VMDept,VMDraw,VMCatl,VMUoM,VHsnCode,VNewHsnCode,VMultiType;
      Vector        VMCode1,VMName1,VMDept1,VMDraw1,VMCatl1,VMUoM1,VHsnCode1,VNewHsnCode1,VMultiType1,VGrnItemCode;

      JTabbedPane   JTP;
      JPanel        ListPanel,BottomPanel,MiddlePanel;
      JButton       BOk;
      Common        common = new Common();
      FindPanelModify     findPanel;
    
      TabReport     tabreport;
      TabReport     tabreport1;
      String        SFind="";
      int           iMillCode,iAuthCode;
      String        SItemTable;

     Connection theConnection = null;
     boolean bAutoCommitFlag = true;

      public MaterialHsnCodeModification(JLayeredPane Layer,int iMillCode,int iAuthCode,String SItemTable)
      {
            this.Layer        = Layer;
            this.iMillCode    = iMillCode;
            this.iAuthCode    = iAuthCode;
            this.SItemTable   = SItemTable;
			try{
            findPanel         = new FindPanelModify();
            BottomPanel      = new JPanel(true);
            MiddlePanel        = new JPanel(true);  
            JTP               = new JTabbedPane();
            ListPanel         = new JPanel(true);
            BOk               = new JButton("Update");
            setLayouts();
			}
			catch(Exception ex){ex.printStackTrace();}
      }

      public void SetAllVectors(){

			setVector();
			setVector1();
			setRowData();
			setRowData1();

			tabreport = new TabReport(RowData,ColumnData,ColumnType);
			tabreport1 = new TabReport(RowData1,ColumnData1,ColumnType1);
			tabreport1.setPrefferedColumnWidth1(iColWidth);  
            tabreport.ReportTable.getTableHeader().setReorderingAllowed(false);
            tabreport1.ReportTable.getTableHeader().setReorderingAllowed(false); 

			JTP.addTab(" HSN Code Replacement ",tabreport);
			JTP.addTab("Additional  HSN Code  ",tabreport1);

			MiddlePanel.add(JTP);

      }
         
      public void setLayouts()
      {
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,650,500);
            setTitle("Material For HSN Code Modification");

            ListPanel.setLayout(new BorderLayout());
            ListPanel.add("North",findPanel);
             
            MiddlePanel.setLayout(new BorderLayout()); 
            MiddlePanel.add("Center",JTP);
          
            SetAllVectors();
		    if(iAuthCode>1)
            {
              BottomPanel.add("South",BOk);
            }

            findPanel.BApply.addActionListener(new ActList());
            findPanel.TFind.addKeyListener(new KeyList());
            findPanel.TFind1.addKeyListener(new KeyList1());
	        tabreport.ReportTable.addKeyListener(new funKeyList());
            tabreport1.ReportTable.addKeyListener(new funKeyList1());
          
            BOk.addActionListener(new UpdtList());
         

            getContentPane().add("North",ListPanel);
            getContentPane().add("Center",MiddlePanel);
            getContentPane().add("South",BottomPanel); 
           
          
      }


      public class ActList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  findPanel.TFind.setEditable(true);
                  findPanel.TFind1.setEditable(true);
                  BOk.setEnabled(true);
                  try
                  {
                        JTP.remove(tabreport);
                        JTP.remove(tabreport1);
                      
                        MiddlePanel.remove(JTP); 
                       
                  }
                  catch(Exception ex)
                  {
                  }
                  try
                  {
					 SetAllVectors() ;
                     Layer.repaint();
                     Layer.updateUI();
      
                  }
                  catch(Exception ex)
                  {
                     System.out.println("Tab 1"+ex);
                  }

                
            }
      }


     
  

      public class UpdtList implements ActionListener
      {
         public void actionPerformed(ActionEvent ae)
         {
                 AlreadyExistsVector();
             //BOk.setEnabled(false);
				if(Validation() ){
             		  try{
                      
                         if(bAutoCommitFlag)
                         {
                             

							  UpdateDetails();
                              theConnection  . commit();
							  JOptionPane.showMessageDialog(null, " Data Saved ", "Error", JOptionPane.INFORMATION_MESSAGE);
							  JTP.remove(tabreport);
							  JTP.remove(tabreport1);

							  MiddlePanel.remove(JTP); 
							  SetAllVectors();
							  System.out.println("Commit Completed");
                              theConnection  . setAutoCommit(true);
                         }
                         else
                         {
                              theConnection  . rollback();
                              theConnection  . setAutoCommit(true);
                         }
                  
                      
                       }catch(Exception ex){}
			 		
				}
			 
	             
         }
      }
	  public class funKeyList extends KeyAdapter
      {

          public void keyPressed(KeyEvent ke)
          {

			  try{

                  int iCol = tabreport.ReportTable.getSelectedColumn();
				  int iRow = tabreport.ReportTable.getSelectedRow();
				  if (ke.getKeyCode() == KeyEvent.VK_F3) {	
					try {
					if (iCol == 7) {

						if(iRow>=1)
						{
							System.out.println("iRow-->"+iRow);
							String sHsnCode = (common.parseNull((String)tabreport.ReportTable.getValueAt(iRow-1, 7))).trim();
							Boolean bValue  = (Boolean)tabreport.ReportTable.getValueAt(iRow-1, 8);

						    if(bValue.booleanValue() && sHsnCode.length()>0)
						    {

								tabreport.ReportTable.setValueAt(sHsnCode,iRow, 7);
								tabreport.ReportTable.setValueAt(new java.lang.Boolean(true),iRow,8);
						    }
						}
					} else {
						JOptionPane.showMessageDialog(null, " Press F3 Key On HSN Code Column ", "Message", JOptionPane.INFORMATION_MESSAGE);
					}
					} catch (Exception ex) {
					ex.printStackTrace();
					}
				  }
			  }	
			  catch(Exception ex){ex.printStackTrace();}
          }
}

         public class funKeyList1 extends KeyAdapter
         {

          public void keyPressed(KeyEvent ke)
          {

			  try{

                  int iCol = tabreport1.ReportTable.getSelectedColumn();
				  int iRow = tabreport1.ReportTable.getSelectedRow();
				  if (ke.getKeyCode() == KeyEvent.VK_F3) {	
					try {
					if (iCol == 7) {

						if(iRow>=1)
						{
							System.out.println("iRow-->"+iRow);
							String sHsnCode = (common.parseNull((String)tabreport1.ReportTable.getValueAt(iRow-1, 7))).trim();
							Boolean bValue  = (Boolean)tabreport1.ReportTable.getValueAt(iRow-1, 8);

						    if(bValue.booleanValue() && sHsnCode.length()>0)
						    {

								tabreport1.ReportTable.setValueAt(sHsnCode,iRow, 7);
								tabreport1.ReportTable.setValueAt(new java.lang.Boolean(true),iRow,8);
						    }
						}
					} else {
						JOptionPane.showMessageDialog(null, " Press F3 Key On HSN Code Column ", "Message", JOptionPane.INFORMATION_MESSAGE);
					}
					} catch (Exception ex) {
					ex.printStackTrace();
					}
				  }
			  }	
			  catch(Exception ex){ex.printStackTrace();}
          }
 
            /*public void keyPressed(KeyEvent ke)
            {

					int iCol = tabreport.ReportTable.getSelectedColumn();
					int iRow = tabreport.ReportTable.getSelectedRow();
					if (ke.getKeyCode() == KeyEvent.VK_F3) {
					try {
					if (iCol == 6) {

					if(iRow>=0)
					{

					String sHsnCode = common.parseNull((String)tabreport.ReportTable.getValueAt(iRow, 6));
					tabreport.ReportTable.setValueAt(sHsnCode,iRow+1, 6);
					tabreport.ReportTable.setValueAt(new java.lang.Boolean(true),iRow+1,7);
					}
					} else {
					JOptionPane.showMessageDialog(null, " Press F3 Key On HSN Code Column ", "Message", JOptionPane.INFORMATION_MESSAGE);
					}
					} catch (Exception ex) {
					ex.printStackTrace();
					}
					}
            }    

            public void keyTyped(KeyEvent ke)
            {

					int iCol = tabreport.ReportTable.getSelectedColumn();
					int iRow = tabreport.ReportTable.getSelectedRow();
					if (ke.getKeyCode() == KeyEvent.VK_F3) {
					try {

					if (iCol == 6) {

					if(iRow>=0)
					{
						
						String sHsnCode = common.parseNull((String)tabreport.ReportTable.getValueAt(iRow, 6));
						tabreport.ReportTable.setValueAt(sHsnCode,iRow+1, 6);
						tabreport.ReportTable.setValueAt(new java.lang.Boolean(true),iRow+1,7);
					}
					} else {
					JOptionPane.showMessageDialog(null, " Press F3 Key On HSN Code Column ", "Message", JOptionPane.INFORMATION_MESSAGE);
					}
					} catch (Exception ex) {
					ex.printStackTrace();
					}
					}
            }*/
      }


      public class KeyList extends KeyAdapter
      {
          public void keyReleased(KeyEvent ke)
          {
               String SFind = findPanel.TFind.getText();
               int i1=indexOf(SFind,1);
               int i2=indexOf(SFind,2);
              if (i1>-1)
               {    
                    
                    tabreport.ReportTable.setRowSelectionInterval(i1,i1);
                    Rectangle cellRect = tabreport.ReportTable.getCellRect(i1,0,true);
                    tabreport.ReportTable.scrollRectToVisible(cellRect);
                   

               }



             if(i2>-1){
                
                     tabreport1.ReportTable.setRowSelectionInterval(i2,i2);
                     Rectangle cellRect = tabreport1.ReportTable.getCellRect(i2,0,true);
                     tabreport1.ReportTable.scrollRectToVisible(cellRect);
                   
               }
          }
      }

      public class KeyList1 extends KeyAdapter
      {
           public void keyReleased(KeyEvent ke)
           {
                   String SFind = findPanel.TFind1.getText();
                   int i1=codeIndexOf(SFind,1);
                   int i2=codeIndexOf(SFind,2);
                   if (i1>-1)
                   {
                           
                           tabreport.ReportTable.setRowSelectionInterval(i1,i1);
                           Rectangle cellRect = tabreport.ReportTable.getCellRect(i1,0,true);
                           tabreport.ReportTable.scrollRectToVisible(cellRect);

                     }

                    if(i2>-1)
                     {
                           
                           tabreport1.ReportTable.setRowSelectionInterval(i2,i2);
                           Rectangle cellRect = tabreport1.ReportTable.getCellRect(i2,0,true);
                           tabreport1.ReportTable.scrollRectToVisible(cellRect);
                     }
           }
      }
	

      public int indexOf(String SFind,int iStatus)
      {
         

            if(iStatus==1){


				   for(int i=0;i<VMName.size();i++)
				   {
                           SFind = SFind.toUpperCase();

				           String str = (String)VMName.elementAt(i);
				           str=str.toUpperCase();
				           if(str.startsWith(SFind))
				                   return i;
				   }


            }

           else
            {

     

				     for(int i=0;i<VMName1.size();i++)
				     {
                           SFind = SFind.toUpperCase();
           

				           String str = (String)VMName1.elementAt(i);
				           str=str.toUpperCase();
				           if(str.startsWith(SFind))
				                   return i;
				     }

             }
           return -1;
      }

      public int codeIndexOf(String SFind ,int iStatus)
      {

            if(iStatus == 1)
             {
				   for(int i=0;i<VMCode.size();i++)
				   {
				           String str = (String)VMCode.elementAt(i);
				           if(str.startsWith(SFind))
				                   return i;
				   }

              }

             else
              { 

				  for(int i=0;i<VMCode1.size();i++)
				   {
				           String str = (String)VMCode1.elementAt(i);
				           if(str.startsWith(SFind))
				                   return i;
				   }

              }
           return -1;
      }

     

      public void setVector()
      {
            VMCode       = new Vector();
            VMName       = new Vector();
            VMDept       = new Vector();
            VMDraw       = new Vector();
            VMCatl       = new Vector();
            VMUoM        = new Vector();
            VHsnCode     = new Vector();
            VMultiType   = new Vector();
          
            try
            {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

             

                  Statement theStatement    = theConnection.createStatement();

                
                  ResultSet res  = theStatement.executeQuery(getQString());
                  while(res.next())
                  {
                        VMCode     .addElement(res.getString(1));
                        VMName     .addElement(res.getString(2));
                        VMDept     .addElement(res.getString(3));
                        VMDraw     .addElement(common.parseNull(res.getString(4)));
                        VMCatl     .addElement(common.parseNull(res.getString(5)));
                        VMUoM      .addElement(res.getString(6));
                        String SHSNStatus ="";
                        String sHsnCode=common.parseNull(res.getString(7));
                        VHsnCode   .addElement(sHsnCode);

                  }
                  res.close();
                  theStatement.close();

            }
            catch(Exception ex)
            {
                  System.out.println("Err3"+ex);
            }
      }


//set vector 2

  public void setVector1()
      {
            VMCode1       = new Vector();
            VMName1       = new Vector();
            VMDept1       = new Vector();
            VMDraw1       = new Vector();
            VMCatl1       = new Vector();
            VMUoM1        = new Vector();
            VHsnCode1     = new Vector();
            VMultiType1   = new Vector();
           
            try
            {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

         Statement theStatement    = theConnection.createStatement();

                
                  ResultSet res  = theStatement.executeQuery(getQString1());
                  while(res.next())
                  {
                        VMCode1     .addElement(res.getString(1));
                        VMName1     .addElement(res.getString(2));
                        VMDept1     .addElement(res.getString(3));
                        VMDraw1     .addElement(common.parseNull(res.getString(4)));
                        VMCatl1     .addElement(common.parseNull(res.getString(5)));
                        VMUoM1      .addElement(res.getString(6));

                        String sHsnCode=common.parseNull(res.getString(7));
                        String SMultiStatus=common.parseNull(res.getString(8));

                         //VHsnCode1   .addElement(common.parseNull(res.getString(7)));
                       if(SMultiStatus.equals("1")){
                         
                        String SMultipleHsnCode=GetMultipleHsnCode(res.getString(1));

                        sHsnCode = sHsnCode+","+SMultipleHsnCode;

                       
                   

                        }
                         VHsnCode1   .addElement(sHsnCode);
                         VMultiType1  .addElement(common.parseNull(res.getString(8)));
                   
                  }
                  res.close();
                  theStatement.close();


  
            }
            catch(Exception ex)
            {
                  System.out.println("Err3"+ex);
            }
      }


      public void setRowData()
      {
            RowData = new Object[VMCode.size()][ColumnData.length];
            for(int i=0;i<VMCode.size();i++)
            {

                        RowData[i][0] = common.parseNull((String)VMCode.elementAt(i));      
                        RowData[i][1] = common.parseNull((String)VMName.elementAt(i));
                        RowData[i][2] = common.parseNull((String)VMUoM.elementAt(i));
                        RowData[i][3] = common.parseNull((String)VMDept.elementAt(i));
                        RowData[i][4] = common.parseNull((String)VMDraw.elementAt(i));
                        RowData[i][5] = common.parseNull((String)VMCatl.elementAt(i));
						RowData[i][6] = common.parseNull((String)VHsnCode.elementAt(i));
                        RowData[i][7] = "";
					    RowData[i][8] = new Boolean(false);


            }
      }

//row data 2

   public void setRowData1()
      {
            RowData1 = new Object[VMCode1.size()][ColumnData1.length];
            for(int i=0;i<VMCode1.size();i++)
            {

                        RowData1[i][0] = common.parseNull((String)VMCode1.elementAt(i));      
                        RowData1[i][1] = common.parseNull((String)VMName1.elementAt(i));
                        RowData1[i][2] = common.parseNull((String)VMUoM1.elementAt(i));
                        RowData1[i][3] = common.parseNull((String)VMDept1.elementAt(i));
                        RowData1[i][4] = common.parseNull((String)VMDraw1.elementAt(i));
                        RowData1[i][5] = common.parseNull((String)VMCatl1.elementAt(i));
                        RowData1[i][6]   = common.parseNull((String)VHsnCode1.elementAt(i));
                        RowData1[i][7] = "";
						RowData1[i][8] = new Boolean(false);

          


            }
      }


 
      public void UpdateDetails()
      {
           try
           {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

			   theConnection   .	setAutoCommit(true);
			   if(theConnection.	getAutoCommit())
			   theConnection   .	setAutoCommit(false);

               Statement theStatement    = theConnection.createStatement();

                     String iip = InetAddress.getLocalHost().getHostAddress();
                     String SSysName = common.getLocalHostName();
                     String SServerDateTime=common.getServerPureDate();


                for(int i=0;i<VMCode.size();i++)
                {
                     Boolean Bselected = (Boolean)RowData[i][8];
                     if(Bselected.booleanValue())
                     {
					 String sCode        = common.parseNull((String)RowData[i][7]).trim().toUpperCase();
                     String sOldHsnCode   = common.parseNull((String)RowData[i][6]).trim(); 

						 String QString = " Update InvItems Set ";
                         QString = QString+" HSNCODE = '"+sCode+"'";
                         QString = QString+" Where Item_Code = '"+(String)RowData[i][0]+"'";


                        String QS=" Insert into HsnCodeChanges (ID,ITEM_CODE,OLDHSNCODE,NEWHSNCODE,MODIDATE,USERCODE,SYSNAME) "+
		                          " Values(HsnCodeChanges_Seq.nextval,'"+(String)RowData[i][0]+"','"+sOldHsnCode+"','"+sCode+"',"+SServerDateTime+","+iAuthCode+",'"+SSysName+"') ";


					

                         theStatement.execute(QString);
                         theStatement.execute(QS);
                    }
                }


      for(int i=0;i<VMCode1.size();i++)
                {
                     Boolean Bselected = (Boolean)RowData1[i][8];
                     if(Bselected.booleanValue())
                     {
					 String sCode        = common.parseNull((String)RowData1[i][7]).trim().toUpperCase();

						 String QString = " Update InvItems Set ";
                         QString = QString+" multihsn = 1 ";
                         QString = QString+" Where multihsn=0 and Item_Code = '"+(String)RowData1[i][0]+"'";


                        String QS=" Insert into MultiHsn (ID,ITEM_CODE,HSNCODE,MODIDATE,USERCODE,SYSNAME) "+
		                          " Values(MultiHsn_Seq.nextval,'"+(String)RowData1[i][0]+"','"+sCode+"',"+SServerDateTime+","+iAuthCode+",'"+SSysName+"') ";

					

                         theStatement.execute(QString);
                         theStatement.execute(QS);
                    }
                }
                theStatement.close();


    
  
           }
           catch(Exception ex)
           {
                 System.out.println("Error In Updation "+ ex);
			     bAutoCommitFlag     = false;
           }



			
      }

      public String getQString()
      {

          if(iMillCode == 0)
          {
               String QString = " Select distinct InvItems.Item_Code,InvItems.Item_Name,"+
                                " StockGroup.GroupName,InvItems.Draw,"+
                                " InvItems.Catl,"+
                                " UoM.UomName,InvItems.HsnCode"+
                                " From (InvItems Inner Join StockGroup On StockGroup.GroupCode = InvItems.StkGroupCode  and InvItems.MultiHsn=0 and InvItems.ITEM_CODE not in (select code from grn where gststatus=0 ) ) ";
							
                                QString = QString+" Inner Join Uom On Uom.UomCode = InvItems.UomCode "+
								" Where InvItems.HsnType="+findPanel.JCHSNType.getSelectedIndex()+"";
     
							   if (findPanel.JRSeleDept.isSelected())
							   {
								    QString = QString+" And InvItems.StkGroupCode = '"+(String)findPanel.VDeptCode.elementAt(findPanel.JCDept.getSelectedIndex())+"'";
							   }
                            
                              if(findPanel.JCHsnCode.getSelectedItem().equals("ALL"))
                              {
                                  QString =QString+" and invitems.hsncode <>'0' and invitems.hsncode is not null";   
                               }else{ 
                             
		                       QString =QString+" and (invitems.hsncode ="+findPanel.JCHsnCode.getSelectedItem()+" )";  
		                        }
							   if(((findPanel.TSort.getText()).trim()).length()==0)
								       QString = QString+" Order By InvItems.Item_Name ";
							   else
								       QString = QString+"  Order By "+findPanel.TSort.getText();

							  

							   return QString;

          }

          else
          {
               String QString = " Select "+SItemTable+".Item_Code,InvItems.Item_Name,"+
                                " StockGroup.GroupName,InvItems.Draw,"+
                                " InvItems.Catl,"+
								" UoM.UomName,InvItems.HsnCode"+
                                //" UoM.UomName,"+SItemTable+".HsnCode"+
                                " From (("+SItemTable+" Inner Join InvItems On "+
                                " "+SItemTable+".Item_Code = InvItems.Item_Code and invitems.multihsn=0  and InvItems.ITEM_CODE not in (select code from grn where gststatus=0)  ) ";
								
                                QString = QString+" Left Join StockGroup On StockGroup.GroupCode = "+SItemTable+".StkGroupCode) "+
                                " Inner Join Uom On Uom.UomCode = InvItems.UomCode "+
								" Where InvItems.HsnType="+findPanel.JCHSNType.getSelectedIndex()+"  ";	
								
							   if (findPanel.JRSeleDept.isSelected())
							   {
							QString = QString+" And "+SItemTable+".StkGroupCode = '"+(String)findPanel.VDeptCode.elementAt(findPanel.JCDept.getSelectedIndex())+"'";
							   }
                             
                              if(findPanel.JCHsnCode.getSelectedItem().equals("ALL"))
                              {
                                  QString =QString+" and invitems.hsncode <>'0' and invitems.hsncode is not null";   // or invitems.hsncode is not null)
                               }else{ 
                             
		                       QString =QString+" and (invitems.hsncode ="+findPanel.JCHsnCode.getSelectedItem()+" )";   // or invitems.hsncode is not null)
		                        }							 

		                          
					    	   if(((findPanel.TSort.getText()).trim()).length()==0)
									   QString = QString+" order by InvItems.Item_Name ";
							   else
									   QString = QString+"  Order By "+findPanel.TSort.getText();

							   return QString;
				
          }

			
      }


       public String getQString1()
      {

          if(iMillCode == 0)
          {
               String QString = " Select distinct InvItems.Item_Code,InvItems.Item_Name,"+
                                " StockGroup.GroupName,InvItems.Draw,"+
                                " InvItems.Catl,"+
                                " UoM.UomName,InvItems.HsnCode,invitems.multihsn"+
                                " From (InvItems Inner Join StockGroup On StockGroup.GroupCode = InvItems.StkGroupCode   and InvItems.ITEM_CODE  in ( select code as item_code from grn where gststatus=0 union all select item_code from multihsn) ) ";
							
                                QString = QString+" Inner Join Uom On Uom.UomCode = InvItems.UomCode "+
                                 "  Left Join Multihsn on multihsn.ITEM_CODE=invitems.item_code   "+ 
								" Where InvItems.HsnType="+findPanel.JCHSNType.getSelectedIndex()+"";
     
							   if (findPanel.JRSeleDept.isSelected())
							   {
								    QString = QString+" And InvItems.StkGroupCode = '"+(String)findPanel.VDeptCode.elementAt(findPanel.JCDept.getSelectedIndex())+"'";
							   }
                            
                              if(findPanel.JCHsnCode.getSelectedItem().equals("ALL"))
                              {
                                  QString =QString+" and invitems.hsncode <>'0' and invitems.hsncode is not null";   
                               }else{ 
                             
		                       QString =QString+" and (invitems.hsncode ="+findPanel.JCHsnCode.getSelectedItem()+"  or multihsn.hsncode="+findPanel.JCHsnCode.getSelectedItem()+" )";  
		                        }
							   if(((findPanel.TSort.getText()).trim()).length()==0)
								       QString = QString+" order by  InvItems.Item_Name ";
							   else
								       QString = QString+"  Order By "+findPanel.TSort.getText();

							   

							   return QString;

          }

          else
          {
               String QString = " Select "+SItemTable+".Item_Code,InvItems.Item_Name,"+
                                " StockGroup.GroupName,InvItems.Draw,"+
                                " InvItems.Catl,"+
								" UoM.UomName,InvItems.HsnCode ,invitems.multihsn "+
                                //" UoM.UomName,"+SItemTable+".HsnCode"+
                                " From (("+SItemTable+" Inner Join InvItems On "+
                                " "+SItemTable+".Item_Code = InvItems.Item_Code   and InvItems.ITEM_CODE  in ( select code as item_code from grn where gststatus=0 union all select item_code from multihsn)  ) ";
								
                                QString = QString+" Left Join StockGroup On StockGroup.GroupCode = "+SItemTable+".StkGroupCode) "+
                                " Inner Join Uom On Uom.UomCode = InvItems.UomCode "+
                                  "  Left Join Multihsn on multihsn.ITEM_CODE=invitems.item_code   "+ 
								" Where InvItems.HsnType="+findPanel.JCHSNType.getSelectedIndex()+"  ";	
								
							   if (findPanel.JRSeleDept.isSelected())
							   {
							QString = QString+" And "+SItemTable+".StkGroupCode = '"+(String)findPanel.VDeptCode.elementAt(findPanel.JCDept.getSelectedIndex())+"'";
							   }
                             
                              if(findPanel.JCHsnCode.getSelectedItem().equals("ALL"))
                              {
                                  QString =QString+" and invitems.hsncode <>'0' and invitems.hsncode is not null";   // or invitems.hsncode is not null)
                               }else{ 
                             
		                       QString =QString+" and (invitems.hsncode ="+findPanel.JCHsnCode.getSelectedItem()+"   or multihsn.hsncode="+findPanel.JCHsnCode.getSelectedItem()+" )";   // or invitems.hsncode is not null)
		                        }							 

		                          
					    	   if(((findPanel.TSort.getText()).trim()).length()==0)
									   QString = QString+" order by  InvItems.Item_Name  ";
							   else
									   QString = QString+"  Order By "+findPanel.TSort.getText();

							   return QString;
				
          }

			
       }

	 private boolean Validation()
    {
		int iCount = 0;
        for(int index=0;index<tabreport.ReportTable.getRowCount();index++)
        {
            Boolean isSave = (Boolean)tabreport.ReportTable.getValueAt(index,8);
           
              
            if(isSave.booleanValue())
            {

                String sOldHsnCode  = common.parseNull((String) tabreport.ReportTable.getValueAt(index, 6));
				String sHsnCode  = common.parseNull((String) tabreport.ReportTable.getValueAt(index, 7));
                String sCode  = common.parseNull((String) tabreport.ReportTable.getValueAt(index, 0));

                if(sHsnCode.equals(""))
                {
                 JOptionPane.showMessageDialog(null, " HSN Code  is Empty ", "Error Information", JOptionPane.ERROR_MESSAGE);
				 JTP.setSelectedIndex(0);
                 tabreport.ReportTable.setRowSelectionInterval(index,index);
	             return false;
                }

				if(sHsnCode.length()>8  || sHsnCode.length()<8)
                {
				JOptionPane.showMessageDialog(null, "Only 8 Characters Allowed In HSN Code  ", "Error Information", JOptionPane.ERROR_MESSAGE);
                JTP.setSelectedIndex(0);
                tabreport.ReportTable.setRowSelectionInterval(index,index);
			    return false;
				}

               if(VGrnItemCode.contains(sCode))
               {
                JOptionPane.showMessageDialog(null, " Grn Complete for this Item", "Error Information", JOptionPane.ERROR_MESSAGE);
                JTP.setSelectedIndex(0);
                tabreport.ReportTable.setRowSelectionInterval(index,index); 
                return false;
               }	
               if(sHsnCode.equals(sOldHsnCode))
               {
                   JOptionPane.showMessageDialog(null, " This HSNcode Already Exists for this Item", "Error Information", JOptionPane.ERROR_MESSAGE);
                   JTP.setSelectedIndex(0);
                   tabreport.ReportTable.setRowSelectionInterval(index,index);
                   return false;

               } 


     
				
               iCount++;
            
    		}
            


	 	}

       

          for(int index=0;index<tabreport1.ReportTable.getRowCount();index++)
        {
            Boolean isSave = (Boolean)tabreport1.ReportTable.getValueAt(index,8);
           
              
            if(isSave.booleanValue())
            {
				String sHsnCode  = common.parseNull((String) tabreport1.ReportTable.getValueAt(index, 7));
                String sOldHsnCode  = common.parseNull((String) tabreport1.ReportTable.getValueAt(index, 6)); 

                if(sHsnCode.equals("")){

                JOptionPane.showMessageDialog(null, " HSN Code  is Empty", "Error Information", JOptionPane.ERROR_MESSAGE);
				 JTP.setSelectedIndex(1);
                 tabreport1.ReportTable.setRowSelectionInterval(index,index);
				return false;
                        
                }

				if(sHsnCode.length()>8  || sHsnCode.length()<8){
					
				    	JOptionPane.showMessageDialog(null, "Only 8 Characters Allowed In HSN Code  ", "Error Information", JOptionPane.ERROR_MESSAGE);
                        JTP.setSelectedIndex(1);
						 tabreport1.ReportTable.setRowSelectionInterval(index,index);
                        return false;
				}

                

                 StringTokenizer     st   = new StringTokenizer(sOldHsnCode,",");

                  while(st.hasMoreTokens())
                  {
                
                     String   SSplitHsnCode    = st.nextToken(",");
                     if(sHsnCode.equals(SSplitHsnCode))
                     {
                        JOptionPane.showMessageDialog(null, " This HSNcode Already Exists for this Item", "Error Information", JOptionPane.ERROR_MESSAGE);
                         JTP.setSelectedIndex(1);
                         tabreport1.ReportTable.setRowSelectionInterval(index,index);
                        return false;

                     } 
 

                  }
          
                     

               

              iCount++;	
            
    		}


	 	}

          
              if(iCount==0)
             {
              JOptionPane.showMessageDialog(null, "No Item is Selected ", "Error Information", JOptionPane.ERROR_MESSAGE);
			  return false;

             } 


		return true;
	}
     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println("Err5"+ex);
          }
     }

   //Alredy exists

     public void AlreadyExistsVector()
      {
            VGrnItemCode       = new Vector();
          
         
            try
            {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
                  ResultSet res  = theStatement.executeQuery("select Code from Grn where gststatus=0");
                  while(res.next())
                  {
                        VGrnItemCode    .addElement(res.getString(1));
                       
                   
                  }
                  res.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  System.out.println("Err3"+ex);
            }
      }

  private boolean  CheckExist(){
 
           for(int i=0;i<VMCode.size();i++)
                {
                     Boolean Bselected = (Boolean)RowData[i][8];

                     if(Bselected.booleanValue())
                     {
					 String sCode        = common.parseNull((String)RowData[i][0]).trim();

                     if(VGrnItemCode.contains(sCode))
                      {

                       JOptionPane.showMessageDialog(null, " Grn Complete for this Item", "Error Information", JOptionPane.ERROR_MESSAGE);
                        return false;
                      }

                     }

               }

  return true;

  }     



 private String GetMultipleHsnCode(String SItemCode)
      {
           
          String SMultiHsnCode="";

	       int 	iCount=0;
         
            try
            {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
                  ResultSet res  = theStatement.executeQuery("select HsnCode from MultiHsn where Item_code = '"+SItemCode+"'");
                  while(res.next())
                  {
	                if(iCount==0)
                    {
                         SMultiHsnCode = res.getString(1);
                    }else
                    {


                         SMultiHsnCode = SMultiHsnCode+","+res.getString(1);
                    }  
				    iCount++;
                   
                  }
                  res.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  System.out.println("Err3"+ex);
            }

         return SMultiHsnCode;

      }





}





/*

private class funKeyList extends KeyAdapter {
		    public void keyReleased(KeyEvent ke)
            {
					System.out.println("keyReleased");
					int iCol = tabreport.ReportTable.getSelectedColumn();
					int iRow = tabreport.ReportTable.getSelectedRow();
					if (ke.getKeyCode() == KeyEvent.VK_F5) {
					try {
					if (iCol == 6) {

						if(iRow>=0)
						{
							System.out.println("iRow-->"+iRow);
							String sHsnCode = common.parseNull((String)tabreport.ReportTable.getValueAt(iRow, 6));
							tabreport.ReportTable.setValueAt(sHsnCode,iRow+1, 6);
							tabreport.ReportTable.setValueAt(new java.lang.Boolean(true),iRow+1,7);
						}
					} else {
						JOptionPane.showMessageDialog(null, " Press F3 Key On Company Name Column ", "Message", JOptionPane.INFORMATION_MESSAGE);
					}
					} catch (Exception ex) {
					ex.printStackTrace();
					}
					}    
            }

            public void keyPressed(KeyEvent ke)
            {
					System.out.println("keyPressed");
					int iCol = tabreport.ReportTable.getSelectedColumn();
					int iRow = tabreport.ReportTable.getSelectedRow();
					if (ke.getKeyCode() == KeyEvent.VK_F5) {
					try {
					if (iCol == 6) {
					System.out.println("iCol-->"+iCol);
					if(iRow>=0)
					{
					System.out.println("iRow-->"+iRow);
					String sHsnCode = common.parseNull((String)tabreport.ReportTable.getValueAt(iRow, 6));
					tabreport.ReportTable.setValueAt(sHsnCode,iRow+1, 6);
					tabreport.ReportTable.setValueAt(new java.lang.Boolean(true),iRow+1,7);
					}
					} else {
					JOptionPane.showMessageDialog(null, " Press F3 Key On Company Name Column ", "Message", JOptionPane.INFORMATION_MESSAGE);
					}
					} catch (Exception ex) {
					ex.printStackTrace();
					}
					}
            }    

            public void keyTyped(KeyEvent ke)
            {
				    System.out.println("keyTyped");
					int iCol = tabreport.ReportTable.getSelectedColumn();
					int iRow = tabreport.ReportTable.getSelectedRow();
					if (ke.getKeyCode() == KeyEvent.VK_F5) {
					try {

					if (iCol == 6) {
						System.out.println("iCol-->"+iCol);
					if(iRow>=0)
					{
						System.out.println("iRow-->"+iRow);
						String sHsnCode = common.parseNull((String)tabreport.ReportTable.getValueAt(iRow, 6));
						tabreport.ReportTable.setValueAt(sHsnCode,iRow+1, 6);
						tabreport.ReportTable.setValueAt(new java.lang.Boolean(true),iRow+1,7);
					}
					} else {
					JOptionPane.showMessageDialog(null, " Press F3 Key On Company Name Column ", "Message", JOptionPane.INFORMATION_MESSAGE);
					}
					} catch (Exception ex) {
					ex.printStackTrace();
					}
					}
            }
    }
*/
