package GST;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import javax.swing.table.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class MaterialHsnCodeUpdation extends JInternalFrame
{
      JLayeredPane  Layer;
      Object        RowData[][];

      String        ColumnData[] = {"Code","Item Name","UoM","Stock Group","Drawing","Catl","HSN Code","Click for Modify"};
      String        ColumnType[] = {"S"   ,"S"        ,"S"  ,"S"          ,"S"      ,"S"   , "E"      , "B"              };

      Vector        VMCode,VMName,VMDept,VMDraw,VMCatl,VMUoM,VHsnCode,VHsnStatus;

      JTabbedPane   JTP;
      JPanel        ListPanel;
      JButton       BOk;
      Common        common = new Common();
      FindPanel     findPanel;
      AlterTabReport     tabreport;
      String        SFind="";
      int           iMillCode,iAuthCode;
      String        SItemTable;

     Connection theConnection = null;
     boolean bAutoCommitFlag = true;

      public MaterialHsnCodeUpdation(JLayeredPane Layer,int iMillCode,int iAuthCode,String SItemTable)
      {
            this.Layer        = Layer;
            this.iMillCode    = iMillCode;
            this.iAuthCode    = iAuthCode;
            this.SItemTable   = SItemTable;
			try{
            findPanel         = new FindPanel();
            JTP               = new JTabbedPane();
            ListPanel         = new JPanel(true);
            BOk               = new JButton("Update");
            JTP.addTab("Update HSN Code",ListPanel);
            setLayouts();
			}
			catch(Exception ex){ex.printStackTrace();}
      }
         
      public void setLayouts()
      {
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,650,500);
            setTitle("Material For HSN Code Updation");

            ListPanel.setLayout(new BorderLayout());
            ListPanel.add("North",findPanel);
             
            if(iAuthCode>1)
            {
               ListPanel.add("South",BOk);
            }
            
			setVector();
	        setRowData();
		    tabreport = new AlterTabReport(RowData,ColumnData,ColumnType,VHsnStatus);

            ListPanel.add("Center",tabreport);
            findPanel.BApply.addActionListener(new ActList());
            findPanel.TFind.addKeyListener(new KeyList());
            findPanel.TFind1.addKeyListener(new KeyList1());
	        tabreport.ReportTable.addKeyListener(new funKeyList());

            BOk.addActionListener(new UpdtList());

            getContentPane().add("Center",JTP);
      }


      public class ActList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  findPanel.TFind.setEditable(true);
                  findPanel.TFind1.setEditable(true);
                  BOk.setEnabled(true);

                  setVector();
                  setRowData();
                  try
                  {
                       ListPanel.remove(tabreport); 
                  }
                  catch(Exception ex)
                  {
                  }
                  try
                  {
                     tabreport = new AlterTabReport(RowData,ColumnData,ColumnType,VHsnStatus);
                     ListPanel.add(tabreport,BorderLayout.CENTER);
                     Layer.repaint();
                     Layer.updateUI();
                  }
                  catch(Exception ex)
                  {
                     System.out.println("Err2"+ex);
                  }
            }
      }

      public class UpdtList implements ActionListener
      {
         public void actionPerformed(ActionEvent ae)
         {
             //BOk.setEnabled(false);
				if(checkValidHsn()==0){
             		if(updateMaterials())
			 		{
						JOptionPane.showMessageDialog(null, " Data Saved ", "Error", JOptionPane.INFORMATION_MESSAGE);
				    	setVector();
		            	setRowData();
                       removeHelpFrame();
			 		}
				}
			 	try
                    {
                         if(bAutoCommitFlag)
                         {
                              theConnection  . commit();
							  System.out.println("Commit Completed");
                              theConnection  . setAutoCommit(true);
                         }
                         else
                         {
                              theConnection  . rollback();
                              theConnection  . setAutoCommit(true);
                         }
                    }catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
	             
         }
      }
	  public class funKeyList extends KeyAdapter
      {

          public void keyPressed(KeyEvent ke)
          {

			  try{

                  int iCol = tabreport.ReportTable.getSelectedColumn();
				  int iRow = tabreport.ReportTable.getSelectedRow();
				  if (ke.getKeyCode() == KeyEvent.VK_F3) {	
					try {
					if (iCol == 6) {

						if(iRow>=1)
						{
							System.out.println("iRow-->"+iRow);
							String sHsnCode = (common.parseNull((String)tabreport.ReportTable.getValueAt(iRow-1, 6))).trim();
							Boolean bValue  = (Boolean)tabreport.ReportTable.getValueAt(iRow-1, 7);

						    if(bValue.booleanValue() && sHsnCode.length()>0)
						    {

								tabreport.ReportTable.setValueAt(sHsnCode,iRow, 6);
								tabreport.ReportTable.setValueAt(new java.lang.Boolean(true),iRow,7);
						    }
						}
					} else {
						JOptionPane.showMessageDialog(null, " Press F3 Key On HSN Code Column ", "Message", JOptionPane.INFORMATION_MESSAGE);
					}
					} catch (Exception ex) {
					ex.printStackTrace();
					}
				  }
			  }	
			  catch(Exception ex){ex.printStackTrace();}
          }
 
            /*public void keyPressed(KeyEvent ke)
            {

					int iCol = tabreport.ReportTable.getSelectedColumn();
					int iRow = tabreport.ReportTable.getSelectedRow();
					if (ke.getKeyCode() == KeyEvent.VK_F3) {
					try {
					if (iCol == 6) {

					if(iRow>=0)
					{

					String sHsnCode = common.parseNull((String)tabreport.ReportTable.getValueAt(iRow, 6));
					tabreport.ReportTable.setValueAt(sHsnCode,iRow+1, 6);
					tabreport.ReportTable.setValueAt(new java.lang.Boolean(true),iRow+1,7);
					}
					} else {
					JOptionPane.showMessageDialog(null, " Press F3 Key On HSN Code Column ", "Message", JOptionPane.INFORMATION_MESSAGE);
					}
					} catch (Exception ex) {
					ex.printStackTrace();
					}
					}
            }    

            public void keyTyped(KeyEvent ke)
            {

					int iCol = tabreport.ReportTable.getSelectedColumn();
					int iRow = tabreport.ReportTable.getSelectedRow();
					if (ke.getKeyCode() == KeyEvent.VK_F3) {
					try {

					if (iCol == 6) {

					if(iRow>=0)
					{
						
						String sHsnCode = common.parseNull((String)tabreport.ReportTable.getValueAt(iRow, 6));
						tabreport.ReportTable.setValueAt(sHsnCode,iRow+1, 6);
						tabreport.ReportTable.setValueAt(new java.lang.Boolean(true),iRow+1,7);
					}
					} else {
					JOptionPane.showMessageDialog(null, " Press F3 Key On HSN Code Column ", "Message", JOptionPane.INFORMATION_MESSAGE);
					}
					} catch (Exception ex) {
					ex.printStackTrace();
					}
					}
            }*/
      }


      public class KeyList extends KeyAdapter
      {
          public void keyReleased(KeyEvent ke)
          {
               String SFind = findPanel.TFind.getText();
               int i=indexOf(SFind);
               if (i>-1)
               {
                    tabreport.ReportTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect = tabreport.ReportTable.getCellRect(i,0,true);
                    tabreport.ReportTable.scrollRectToVisible(cellRect);
               }
          }
      }

      public class KeyList1 extends KeyAdapter
      {
           public void keyReleased(KeyEvent ke)
           {
                   String SFind = findPanel.TFind1.getText();
                   int i=codeIndexOf(SFind);
                   if (i>-1)
                   {
                           tabreport.ReportTable.setRowSelectionInterval(i,i);
                           Rectangle cellRect = tabreport.ReportTable.getCellRect(i,0,true);
                           tabreport.ReportTable.scrollRectToVisible(cellRect);
                   }
           }
      }
	

      public int indexOf(String SFind)
      {
           SFind = SFind.toUpperCase();
           for(int i=0;i<VMName.size();i++)
           {
                   String str = (String)VMName.elementAt(i);
                   str=str.toUpperCase();
                   if(str.startsWith(SFind))
                           return i;
           }
           return -1;
      }

      public int codeIndexOf(String SFind)
      {
           for(int i=0;i<VMCode.size();i++)
           {
                   String str = (String)VMCode.elementAt(i);
                   if(str.startsWith(SFind))
                           return i;
           }
           return -1;
      }

      public int catlIndexOf(String SFind)
      {
           for(int i=0;i<VMCatl.size();i++)
           {
                   String str = (String)VMCatl.elementAt(i);
                   if(str.startsWith(SFind))
                           return i;
           }
           return -1;
      }

      public int drawIndexOf(String SFind)
      {
           for(int i=0;i<VMDraw.size();i++)
           {
                   String str = (String)VMDraw.elementAt(i);
                   if(str.startsWith(SFind))
                           return i;
           }
           return -1;
      }

      public void setVector()
      {
            VMCode       = new Vector();
            VMName       = new Vector();
            VMDept       = new Vector();
            VMDraw       = new Vector();
            VMCatl       = new Vector();
            VMUoM        = new Vector();
            VHsnCode     = new Vector();

           VHsnStatus  = new Vector();
            try
            {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
                  ResultSet res  = theStatement.executeQuery(getQString());
                  while(res.next())
                  {
                        VMCode     .addElement(res.getString(1));
                        VMName     .addElement(res.getString(2));
                        VMDept     .addElement(res.getString(3));
                        VMDraw     .addElement(common.parseNull(res.getString(4)));
                        VMCatl     .addElement(common.parseNull(res.getString(5)));
                        VMUoM      .addElement(res.getString(6));
                        VHsnCode   .addElement(common.parseNull(res.getString(7)));
                        String SHSNCODE ="";
                        String sHsnCode=common.parseNull(res.getString(7));

                        if(common.toInt(sHsnCode) > 0)
                        {
                           SHSNCODE="1";
                        } else
						 {
							SHSNCODE = "0";
						 }
						 VHsnStatus.addElement(SHSNCODE);
                   
                  }
                  res.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  System.out.println("Err3"+ex);
            }
      }

      public void setRowData()
      {
            RowData = new Object[VMCode.size()][ColumnData.length];
            for(int i=0;i<VMCode.size();i++)
            {
                        RowData[i][0] = common.parseNull((String)VMCode.elementAt(i));      
                        RowData[i][1] = common.parseNull((String)VMName.elementAt(i));
                        RowData[i][2] = common.parseNull((String)VMUoM.elementAt(i));
                        RowData[i][3] = common.parseNull((String)VMDept.elementAt(i));
                        RowData[i][4] = common.parseNull((String)VMDraw.elementAt(i));
                        RowData[i][5] = common.parseNull((String)VMCatl.elementAt(i));
						RowData[i][6] = common.parseNull((String)VHsnCode.elementAt(i));
						//if(common.parseNull((String)VHsnCode.elementAt(i)).length()>0)
                        //RowData[i][6] = common.parseNull((String)VHsnCode.elementAt(i));
                        RowData[i][7] = new Boolean(false);
            }
      }
      public boolean updateMaterials()
      {
           try
           {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

			   theConnection   .	setAutoCommit(true);
			   if(theConnection.	getAutoCommit())
			   theConnection   .	setAutoCommit(false);

               Statement theStatement    = theConnection.createStatement();
				
                for(int i=0;i<VMCode.size();i++)
                {
                     Boolean Bselected = (Boolean)RowData[i][7];
                     //int iStatus       = common.toInt((String)RowData[i][6]);
					 String sCode        = common.parseNull((String)RowData[i][6]).trim().toUpperCase();

                     if(Bselected.booleanValue())
                     {

                         //String QString = " Update "+SItemTable+" Set ";
						 String QString = " Update InvItems Set ";
                         QString = QString+" HSNCODE = '"+sCode+"'";
                         QString = QString+" Where Item_Code = '"+(String)RowData[i][0]+"' And HsnType = "+findPanel.JCHSNType.getSelectedIndex()+" ";

                         theStatement.execute(QString);
                    }
                }
                theStatement.close();
           }
           catch(Exception ex)
           {
                 System.out.println("Error In Updation "+ ex);
			     bAutoCommitFlag     = false;
           }
			return true;
      }

      public String getQString()
      {

          if(iMillCode == 0)
          {
               String QString = " Select distinct InvItems.Item_Code,InvItems.Item_Name,"+
                                " StockGroup.GroupName,InvItems.Draw,"+
                                " InvItems.Catl,"+
                                " UoM.UomName,InvItems.HsnCode"+
                                " From (InvItems Inner Join StockGroup On StockGroup.GroupCode = InvItems.StkGroupCode) ";
								if (findPanel.JCPeriod.getSelectedIndex()==1){
										QString = QString+" Inner Join purchaseorder On purchaseorder.Item_code = InvItems.Item_code "+
						             	" And purchaseorder.OrderDate>=20160101 And purchaseorder.OrderDate <= to_char(sysdate,'yyyymmdd')  "; 
								}
                                if (findPanel.JCPeriod.getSelectedIndex()==2){
										QString = QString+" Inner Join purchaseorder On purchaseorder.Item_code = InvItems.Item_code "+
						             	"and purchaseorder.qty>0 and purchaseorder.qty>purchaseorder.invqty  "; 
								}
                                 if (findPanel.JCPeriod.getSelectedIndex()==3){
										QString = QString+" Inner Join mrs On mrs.Item_code = InvItems.Item_code "+
						             	"and mrs.qty>0 and mrs.orderno=0  "; 
								}
                                QString = QString+" Inner Join Uom On Uom.UomCode = InvItems.UomCode "+
								" Where InvItems.HsnType="+findPanel.JCHSNType.getSelectedIndex()+"";
     
							   if (findPanel.JRSeleDept.isSelected())
							   {
								    QString = QString+" And InvItems.StkGroupCode = '"+(String)findPanel.VDeptCode.elementAt(findPanel.JCDept.getSelectedIndex())+"'";
							   }
                               if(!findPanel.JRAll.isSelected())
                               {
		                           if(findPanel.JRUpdate.isSelected())
                                   {
		                                QString =QString+" and invitems.hsncode <>'0' and invitems.hsncode is not null";   
		                           } 
                                    if(findPanel.JRNotUpdate.isSelected())
                                   {
		                               QString =QString+" and (invitems.hsncode <=0 or invitems .hsncode is null)";   
		                           }
                               }
							   if(((findPanel.TSort.getText()).trim()).length()==0)
								       QString = QString+" Order By 2";
							   else
								       QString = QString+" Order By "+findPanel.TSort.getText();

							   	System.out.println(QString);

							   return QString;

          }

          else
          {
               String QString = " Select "+SItemTable+".Item_Code,InvItems.Item_Name,"+
                                " StockGroup.GroupName,InvItems.Draw,"+
                                " InvItems.Catl,"+
								" UoM.UomName,InvItems.HsnCode"+
                                //" UoM.UomName,"+SItemTable+".HsnCode"+
                                " From (("+SItemTable+" Inner Join InvItems On "+
                                " "+SItemTable+".Item_Code = InvItems.Item_Code) ";
								if (findPanel.JCPeriod.getSelectedIndex()==1){
										QString = QString+" Inner Join purchaseorder On purchaseorder.Item_code = InvItems.Item_code "+
							             " And purchaseorder.OrderDate>=20160101 And purchaseorder.OrderDate <= to_char(sysdate,'yyyymmdd')  "; 
								}
                                 if (findPanel.JCPeriod.getSelectedIndex()==2){
										QString = QString+" Inner Join purchaseorder On purchaseorder.Item_code = InvItems.Item_code "+
						             	"and purchaseorder.qty>0 and purchaseorder.qty>purchaseorder.invqty  "; 
								}
                                 if (findPanel.JCPeriod.getSelectedIndex()==3){
										QString = QString+" Inner Join mrs On mrs.Item_code = InvItems.Item_code "+
						             	"and mrs.qty>0 and mrs.orderno=0  "; 
								}
                                QString = QString+" Left Join StockGroup On StockGroup.GroupCode = "+SItemTable+".StkGroupCode) "+
                                " Inner Join Uom On Uom.UomCode = InvItems.UomCode "+
								" Where InvItems.HsnType="+findPanel.JCHSNType.getSelectedIndex()+"  ";	
								
							   if (findPanel.JRSeleDept.isSelected())
							   {
							QString = QString+" And "+SItemTable+".StkGroupCode = '"+(String)findPanel.VDeptCode.elementAt(findPanel.JCDept.getSelectedIndex())+"'";
							   }
							  if(!findPanel.JRAll.isSelected())
                               {
		                           if(findPanel.JRUpdate.isSelected())
                                   {
		                                QString =QString+" and invitems.hsncode <>'0' and invitems.hsncode is not null";   
		                           } 
                                  else  if(findPanel.JRNotUpdate.isSelected())
                                   {
		                               QString =QString+" and (invitems.hsncode <=0 or invitems .hsncode is null)";   
		                           }
                               }
					    	   if(((findPanel.TSort.getText()).trim()).length()==0)
									   QString = QString+" Order By 2";
							   else
									   QString = QString+" Order By "+findPanel.TSort.getText();

							   return QString;
				
          }
 
			
      }
	 private int checkValidHsn()
    {
		int icnt = 0;
        for(int index=0;index<tabreport.ReportTable.getRowCount();index++)
        {
            Boolean isSave = (Boolean)tabreport.ReportTable.getValueAt(index,7);
            if(isSave.booleanValue())
            {
				String sHsnCode  = common.parseNull((String) tabreport.ReportTable.getValueAt(index, 6));

				if(sHsnCode.length()>8  || sHsnCode.length()<8){
					
				    	JOptionPane.showMessageDialog(null, "Only 8 Characters Allowed In HSN Code  ", "Error Information", JOptionPane.ERROR_MESSAGE);
						icnt++;
				}			
    		}
	 	}
		return icnt;
	}
     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println("Err5"+ex);
          }
     }
}



/*

private class funKeyList extends KeyAdapter {
		    public void keyReleased(KeyEvent ke)
            {
					System.out.println("keyReleased");
					int iCol = tabreport.ReportTable.getSelectedColumn();
					int iRow = tabreport.ReportTable.getSelectedRow();
					if (ke.getKeyCode() == KeyEvent.VK_F5) {
					try {
					if (iCol == 6) {

						if(iRow>=0)
						{
							System.out.println("iRow-->"+iRow);
							String sHsnCode = common.parseNull((String)tabreport.ReportTable.getValueAt(iRow, 6));
							tabreport.ReportTable.setValueAt(sHsnCode,iRow+1, 6);
							tabreport.ReportTable.setValueAt(new java.lang.Boolean(true),iRow+1,7);
						}
					} else {
						JOptionPane.showMessageDialog(null, " Press F3 Key On Company Name Column ", "Message", JOptionPane.INFORMATION_MESSAGE);
					}
					} catch (Exception ex) {
					ex.printStackTrace();
					}
					}    
            }

            public void keyPressed(KeyEvent ke)
            {
					System.out.println("keyPressed");
					int iCol = tabreport.ReportTable.getSelectedColumn();
					int iRow = tabreport.ReportTable.getSelectedRow();
					if (ke.getKeyCode() == KeyEvent.VK_F5) {
					try {
					if (iCol == 6) {
					System.out.println("iCol-->"+iCol);
					if(iRow>=0)
					{
					System.out.println("iRow-->"+iRow);
					String sHsnCode = common.parseNull((String)tabreport.ReportTable.getValueAt(iRow, 6));
					tabreport.ReportTable.setValueAt(sHsnCode,iRow+1, 6);
					tabreport.ReportTable.setValueAt(new java.lang.Boolean(true),iRow+1,7);
					}
					} else {
					JOptionPane.showMessageDialog(null, " Press F3 Key On Company Name Column ", "Message", JOptionPane.INFORMATION_MESSAGE);
					}
					} catch (Exception ex) {
					ex.printStackTrace();
					}
					}
            }    

            public void keyTyped(KeyEvent ke)
            {
				    System.out.println("keyTyped");
					int iCol = tabreport.ReportTable.getSelectedColumn();
					int iRow = tabreport.ReportTable.getSelectedRow();
					if (ke.getKeyCode() == KeyEvent.VK_F5) {
					try {

					if (iCol == 6) {
						System.out.println("iCol-->"+iCol);
					if(iRow>=0)
					{
						System.out.println("iRow-->"+iRow);
						String sHsnCode = common.parseNull((String)tabreport.ReportTable.getValueAt(iRow, 6));
						tabreport.ReportTable.setValueAt(sHsnCode,iRow+1, 6);
						tabreport.ReportTable.setValueAt(new java.lang.Boolean(true),iRow+1,7);
					}
					} else {
					JOptionPane.showMessageDialog(null, " Press F3 Key On Company Name Column ", "Message", JOptionPane.INFORMATION_MESSAGE);
					}
					} catch (Exception ex) {
					ex.printStackTrace();
					}
					}
            }
    }
*/
