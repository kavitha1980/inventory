package GST;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import java.util.ArrayList;
import java.net.InetAddress;

import util.*;
import java.util.HashMap;
import jdbc.*;

public class MotorHistoryData {
    
    Connection                  theConnection           =   null;
    Common                      common                  =   new Common();
    Vector                      VItemCode,VItemName;
    String                      sSysName = "";
    ArrayList                   AItemDetails, AMotorDetails;
    
    
    public MotorHistoryData(){
     
        try{
          sSysName               =   InetAddress.getLocalHost().getHostName();
          setItems();
       }
     catch(Exception e){
          e.printStackTrace();
       }
    }
    
    public void setItems(){
    try{
            VItemCode               = new Vector();
            VItemName               = new Vector();
           
        StringBuffer sb = new StringBuffer();
 
        sb.append(" SELECT ITEM_CODE,ITEM_NAME FROM INVITEMS");
        sb.append(" WHERE STKGROUPCODE='A26'");
        sb.append(" ORDER BY 2");

       // System.out.println("Qry-->"+sb.toString());
        
        if (theConnection==null)
              {
                  ORAConnection connect	=	ORAConnection.getORAConnection();
                  theConnection      	=  connect.getConnection();
              }
              PreparedStatement ps   =  theConnection.prepareStatement(sb.toString());
              ResultSet         rst  =  ps.executeQuery();
              while (rst.next()){
                          VItemCode  .  addElement(rst.getString(1));
                          VItemName  .  addElement(rst.getString(2));
                      }
             rst.close();
             ps.close();
             rst=null;
             ps=null;   
             theConnection=null;
     }
     catch(Exception e)
     {
         System.out.println("set Items "+e);
      }
    }
    
       public  String getItemCode(String SItemName){
         String SItemCode="";
          try{
               SItemCode   = (String)VItemCode.elementAt(VItemName.indexOf(SItemName));
               return SItemCode;
          }
          catch(Exception ex){
              ex.printStackTrace();
              System.out.println("ItemCode"+ex);
              return "";
          }
    }
       
    public  String getItemName(String SItemCode){
         String SItemName="";
          try{
               SItemName   = (String)VItemName.elementAt(VItemCode.indexOf(SItemCode));
               return SItemName;
          }
          catch(Exception ex){
              ex.printStackTrace();
              System.out.println("ItemName"+ex);
              return "";
          }
          
    }
    
    public int updateData (String sItemName,String sMake,String sHP,String sKW,String sRPM,String sFrame,String sSlNo,String sDE,String sNDE,String sGear,int iUserCode){
        int iSaveValue      = -1;
        String sItemCode    = "";
        try{
            sItemCode       =  getItemCode(sItemName);
            
            StringBuffer   sb  = new StringBuffer();
            
                sb.append(" UPDATE INVITEMS SET MOTOR_MAKE = '"+sMake+"', MOTOR_HP = '"+sHP+"', MOTOR_RPM = '"+sRPM+"', MOTOR_KW = '"+sKW+"',");
                sb.append(" MOTOR_FRAME = '"+sFrame+"', MOTOR_SLNO = '"+sSlNo+"', MOTOR_DE = '"+sDE+"',MOTOR_NDE = '"+sNDE+"',");
                sb.append(" MOTOR_GEAR = '"+sGear+"',MOTOR_USERCODE = "+iUserCode+" , MOTOR_SYSNAME = '"+sSysName +"',");
                sb.append(" MOTOR_DATETIME = to_Char(sysdate,'DD-MON-YYYY:hh:mi:ss')");
                sb.append(" WHERE ITEM_CODE = '"+sItemCode.trim()+"'");
                
        //to_Char(sysdate,'DD-MON-YYYY:hh:mi:ss')
                System.out.println("UpdateData-->"+sb.toString());
               if (theConnection==null)
                {
                  ORAConnection connect		 =	ORAConnection.getORAConnection();
                  theConnection              =  connect.getConnection();
                }
                 PreparedStatement    stmt      =   theConnection.prepareStatement(sb.toString());
                                      stmt      .   execute();
                                      stmt      .   close();
                                      iSaveValue =  0;
                                      setMotorDetails(sItemCode);
        }
        catch(Exception ex){
            ex.printStackTrace();
            System.out.println("Update Motor Data-->"+ex);
            iSaveValue = 1;
        }
      //  System.out.println("SaveValue-->"+iSaveValue);
        return iSaveValue;
    }
    
    public void setMotorDetails(String sItemCode){
        AMotorDetails               =   new ArrayList();
    try{
        StringBuffer sb             =   new StringBuffer();

                 sb.append(" SELECT ITEM_CODE,ITEM_NAME,MOTOR_MAKE,MOTOR_HP, MOTOR_RPM, MOTOR_FRAME,");
                 sb.append(" MOTOR_SLNO, MOTOR_DE,MOTOR_NDE, MOTOR_GEAR,MOTOR_KW FROM INVITEMS");
                 sb.append(" WHERE ITEM_CODE='"+sItemCode.trim()+"'");
                 
            //    System.out.println("Select Qry-->"+sb.toString());
        
        if (theConnection==null){
                 ORAConnection 	connect		=  ORAConnection.getORAConnection();
                 theConnection              =  connect.getConnection();
        }
        PreparedStatement ps          = theConnection.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();

       while (rst.next()){
           HashMap          theMap      = new HashMap();

                            theMap      . put("ITEMCODE",rst.getString(1));
                            theMap      . put("ITEMNAME",rst.getString(2));
                            theMap      . put("MAKE",rst.getString(3));
                            theMap      . put("HP",rst.getString(4));
                            theMap      . put("RPM",rst.getString(5));
                            theMap      . put("FRAME",rst.getString(6));
                            theMap      . put("SLNO",rst.getString(7));
                            theMap      . put("DE",rst.getString(8));
                            theMap      . put("NDE",rst.getString(9));
                            theMap      . put("GEAR",rst.getString(10));
                            theMap      . put("KW",rst.getString(11));
                            
                            AMotorDetails.add(theMap);
         }
            rst.close();
            ps.close();
            ps=null;
            rst=null;
      }
      catch(Exception e){
        e.printStackTrace();
     }
    }
    
 
 
    
}
