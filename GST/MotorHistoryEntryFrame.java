package GST;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import util.Common;
import javax.swing.*;
import java.util.HashMap;
import java.util.Vector;
import java.util.ArrayList;
import java.net.InetAddress;

import java.awt.Component;

public class MotorHistoryEntryFrame extends JInternalFrame {
    JFrame                                  frmMain;
    JPanel                                  pnlMain,pnlTitle,pnlEntry,pnlData,pnlButton,pnlRadio;
    JTextField                              txtMake,txtKW,txtHP,txtRPM,txtFrame,txtSlNo,txtDE,txtNDE;
    JButton                                 btnSave,btnUpdate,btnClose;
    JRadioButton                            radioWithGear,radioWithOutGear;
    JComboBox                               cmbItemName;
    JTable                                  theTable;
    MotorHistoryEntryModel                  theModel;
    MotorHistoryData                        data = new MotorHistoryData();
	JLayeredPane  							Layer;
	int iUserCode							=0;
    //String      sUserCode="386";
    
 public MotorHistoryEntryFrame(JLayeredPane Layer,int iMillCode,int iUserCode){
		this.Layer        = Layer;
		this.iUserCode    = iUserCode;
		
        data.setItems();
        createComponent();
        setLayout();
        addListener();
        addComponent();        
 }
 private void createComponent(){
    frmMain                             =   new JFrame();
    pnlMain                             =   new JPanel();
    pnlTitle                            =   new JPanel();
    pnlEntry                            =   new JPanel();
    pnlData                             =   new JPanel();
    pnlButton                           =   new JPanel();
    pnlRadio                            =   new JPanel();
    
    txtMake                             =   new JTextField(10);
    txtKW                               =   new JTextField(10);
    txtHP                               =   new JTextField(10);
    txtRPM                              =   new JTextField(10);
    txtFrame                            =   new JTextField(10);
    txtSlNo                             =   new JTextField(10);
    txtDE                               =   new JTextField(10);
    txtNDE                              =   new JTextField(10);
    
    btnSave                             =   new JButton("Save");
    btnUpdate                           =   new JButton("Update");
    btnClose                            =   new JButton("Close");
    
    cmbItemName                         =   new JComboBox(data.VItemName);
    
    radioWithGear                       =   new JRadioButton("With Gear");
    radioWithGear                       .   setSelected(true);
    
    radioWithOutGear                    =   new JRadioButton("Without Gear");
    radioWithOutGear                    .   setSelected(true);
    
    theModel                            =   new MotorHistoryEntryModel();
    theTable                            =   new JTable(theModel);
    
 }
 private void setLayout(){
    pnlMain                             .   setBorder(new TitledBorder(""));
    pnlMain                             .   setLayout(new BorderLayout());
    
    pnlEntry                            .   setBorder(new TitledBorder("History Entry"));
    pnlEntry                            .   setLayout(new GridLayout(10,2));
    pnlData                             .   setBorder(new TitledBorder("Data"));
    pnlData                             .   setLayout(new BorderLayout());
    
    pnlButton                           .   setBorder(new TitledBorder("Control"));
    pnlButton                           .   setLayout(new FlowLayout(FlowLayout.CENTER));
    
    pnlRadio                            .   setBorder(new TitledBorder("With / Without Gear"));
    pnlRadio                            .   setLayout(new GridLayout(1,2));
        
    pnlMain                             .   setLayout(new BorderLayout());
    frmMain                             .   setLayout(new BorderLayout());
        
    this . setSize(520,650);
	this . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
    this . setTitle(" MOTOR HISTORY DETAILS ENTRY FRAME");
    this . setClosable(true);
    this . setMaximizable(true);
    this . setResizable(true);
    
 }
 private void addListener(){
        btnSave                              .  addActionListener(new ActList());
        btnClose                             .  addActionListener(new ExtList());
    
 }
 private void addComponent(){
    
     ButtonGroup group                  =  new ButtonGroup();
     group                              .  add(radioWithGear);
     group                              .  add(radioWithOutGear);
     pnlRadio                           .  add(radioWithGear);
     pnlRadio                           .  add(radioWithOutGear);
     
     
     pnlEntry                           .   add(new JLabel(" Item Name"));
     pnlEntry                           .   add(cmbItemName);
     pnlEntry                           .   add(new JLabel(" Make"));
     pnlEntry                           .   add(txtMake);
     pnlEntry                           .   add(new JLabel(" KW"));
     pnlEntry                           .   add(txtKW);
     pnlEntry                           .   add(new JLabel(" HP"));
     pnlEntry                           .   add(txtHP);
     pnlEntry                           .   add(new JLabel(" RPM"));
     pnlEntry                           .   add(txtRPM);
     pnlEntry                           .   add(new JLabel(" Frame"));
     pnlEntry                           .   add(txtFrame);
     pnlEntry                           .   add(new JLabel(" SlNo"));
     pnlEntry                           .   add(txtSlNo);
     pnlEntry                           .   add(new JLabel(" Bearing DE"));
     pnlEntry                           .   add(txtDE);
     pnlEntry                           .   add(new JLabel(" Bearing NDE"));
     pnlEntry                           .   add(txtNDE);
     pnlEntry                           .   add(new JLabel(" Gear"));
     pnlEntry                           .   add(pnlRadio);
     
     pnlButton                          .   add(btnSave);
     pnlButton                          .   add(btnClose);
     
     pnlData                            .   add(new JScrollPane(theTable));
     
     pnlMain                            .   add(pnlEntry,BorderLayout.NORTH);
     pnlMain                            .   add(pnlData,BorderLayout.CENTER);
     pnlMain                            .   add(pnlButton,BorderLayout.SOUTH);
	 
	// frmMain                            .   add(pnlMain);
     
    this                            .   add(pnlMain);
     
 }
 private class ActList implements ActionListener{
            int iSaveValue=-1, iUpdateValue=-1;  
        public void actionPerformed(ActionEvent e){
          if(e.getSource()== btnSave){
            int iValue=-1;
                iValue      =   getConfirmation();
            if(iValue==0){
                iSaveValue  =   getSave();
            if(iSaveValue==0){
                    JOptionPane.showMessageDialog(null,"Data Saved","Information",JOptionPane.INFORMATION_MESSAGE);   
                    clearValues();
                    setTableData();
                }
            else if(iSaveValue==1) {
                      JOptionPane.showMessageDialog(null,"Data Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    //  setTableData();
              }
            }
          }
        }
 }
 private class ExtList implements ActionListener{
    public void actionPerformed(ActionEvent e){
         if(e.getSource()==btnClose)
         System.exit(0);                       
    }
 }



 private int getConfirmation(){
        int value=0;
        value=JOptionPane.showConfirmDialog(null, "Do you want to save this data","Confirmation",0,3);
        return value;
 }

 private void clearValues(){
            txtMake                         .   setText("");
            txtKW                           .   setText("");
            txtHP                           .   setText("");
            txtRPM                          .   setText("");
            txtFrame                        .   setText("");
            txtSlNo                         .   setText("");
            txtDE                           .   setText("");
            txtNDE                          .   setText("");

            cmbItemName                     .   setSelectedIndex(0);
  }

 private int getSave(){
        int iSaveValue=-1;
        
                    String sItemName                     =   cmbItemName.getSelectedItem().toString();
                    String sMake                         =   txtMake.getText();
                    String sHP                           =   txtHP.getText();
                    String sKW                           =   txtKW.getText();
                    String sRPM                          =   txtRPM.getText();
                    String sFrame                        =   txtFrame.getText();
                    String sSlNo                         =   txtSlNo.getText();
                    String sDE                           =   txtDE.getText();
                    String sNDE                          =   txtNDE.getText();
                    String sGear                         =   "";
                    if(radioWithGear.isSelected())
                        sGear                            =   "1";
                    else
                        sGear                            =   "2";
                    
                    iSaveValue                           =   data.updateData(sItemName,sMake,sHP,sKW,sRPM,sFrame,sSlNo,sDE,sNDE,sGear,iUserCode);
     return iSaveValue; 
    }
 private void setTableData(){
      try{
          theModel.setNumRows(0);
          System.out.println("list size-->"+data.AMotorDetails.size());
          for(int i=0;i<data.AMotorDetails.size();i++) {
              HashMap           theMap              =      (HashMap)data.AMotorDetails.get(i);
              Vector            theVect             =      new Vector();
              
                                theVect             .      addElement(theMap.get("ITEMCODE"));
                                theVect             .      addElement(theMap.get("ITEMNAME"));
                                theVect             .      addElement(theMap.get("MAKE"));
                                theVect             .      addElement(theMap.get("KW"));
                                theVect             .      addElement(theMap.get("HP"));
                                theVect             .      addElement(theMap.get("RPM"));
                                theVect             .      addElement(theMap.get("FRAME"));
                                theVect             .      addElement(theMap.get("SLNO"));
                                theVect             .      addElement(theMap.get("GEAR"));
                                theVect             .      addElement(theMap.get("DE"));
                                theVect             .      addElement(theMap.get("NDE"));
                                
                                theModel            .      appendRow(theVect);
                                
          }
      }
      catch(Exception ex){
          ex.printStackTrace();
          System.out.println("setTableData-->"+ex);
      }
 }
 /*
 public static void main(String args[]){
    MotorHistoryEntryFrame          frm     = new MotorHistoryEntryFrame();
 }
   */ 
}
