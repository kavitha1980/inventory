package GST;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

public class MotorHistoryEntryModel extends DefaultTableModel{
    String ColumnName[] ={"ITEMCODE","ITEMNAME","MAKE","KW","HP","RPM","FRAME","SLNO","GEAR","DE","NDE"};
    String  ColumnType[]={"S","S","S","S","S","S","S","S","S","S","S"};
    int iColumnWidth[]  ={30,30,50,50,30,30,30,30,30,30,30};
    
    public MotorHistoryEntryModel()
    {
       setDataVector(getRowData(),ColumnName);
    }     
     public Class getColumnClass(int iCol)
     {
       return getValueAt(0,iCol).getClass();
     }

    public boolean isCellEditable(int iRow,int iCol)
    {
        if (ColumnType[iCol]=="E"||ColumnType[iCol]=="B")
            
            return true;
            return false;
    }
    private Object[][]getRowData()
    {
        Object RowData[][]=new Object[1][ColumnName.length];
        
        for(int i=0;i<ColumnName.length;i++)
            RowData[0][i]="";
        return RowData;
    }
    
    public void appendRow(Vector theVect)
    {
        insertRow(getRows(),theVect);
    }
    public int getRows()
    {
        return super.dataVector.size();
    }
    
}
