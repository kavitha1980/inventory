package GateNew;

import java.io.*;
import java.util.*;
import util.*;
import guiutil.*;



//pdf



import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;

public class DocPrint2
{
      Vector VBody,VHead,VTitle,VSum;
      String SFile,SMillName;

      int ipctr=0,ilctr=100,iLen=0;

      FileWriter FW;
    
      Common common = new Common();


      public DocPrint2(Vector VBody,Vector VHead,Vector VSum,Vector VTitle,String SFile,String SMillName)
      {
            this.VBody     = VBody;
            this.VHead     = VHead;
            this.VSum      = VSum;
            this.VTitle    = VTitle;
            this.SFile     = SFile;
            this.SMillName = SMillName;

            if((SFile.trim()).length()==0)
               SFile = "1.prn";

            try
            {
                  iLen = ((String)VHead.elementAt(0)).length();
                  FW = new FileWriter(common.getPrintPath()+SFile);
                  for(int i=0;i<VBody.size();i++)
                  {
                        setHead();
                        String strl = (String)VBody.elementAt(i);
                        FW.write(strl);
                        ilctr++;
                  }
                  setSum();

                  FW.close();
            }
            catch(Exception ex)
            {
                  System.out.println("From DocPrint"+ex);
            }
      }

  

      public void setHead()
      {
            if(ilctr < 63)
                  return;
            if(ipctr > 0)
                  setFoot();
            ipctr++;
            String str1 = "Company : "+SMillName+"\n";
            //String str2 = "Document : "+STitle+"\n";
            String str4 = "Page     : "+ipctr+"\n";
            String str3 = common.Replicate("-",iLen)+"\n";
            try
            {
                  FW.write(str1);
                  for(int i=0;i<VTitle.size();i++)
                  {
                        if(i==0)
                        {
                             String str = "Document : "+(String)VTitle.elementAt(i)+"\n";
                             FW.write(str);
                        }
                        else
                        {
                             String str = "           "+(String)VTitle.elementAt(i)+"\n";
                             FW.write(str);
                        }
                  }
                  FW.write(str4);
                  FW.write(str3);
                  ilctr = VTitle.size()+3;

                  for(int i=0;i<VHead.size();i++)
                        FW.write((String)VHead.elementAt(i));
                  FW.write(str3);
                  ilctr = ilctr+VHead.size()+1;
            }
            catch(Exception ex){}
      }
      public void setFoot()
      {
            try
            {
                  String str1 = common.Replicate("-",iLen)+"\n";
                  FW.write(str1);
            }
            catch(Exception ex){}
      }
      public void setSum()
      {
            try
            {
                  if(VSum.size()>0)
                  {
                       String str1 = common.Replicate("-",iLen)+"\n";
                       FW.write(str1);
                       for(int i=0;i<VSum.size();i++)
                             FW.write((String)VSum.elementAt(i));
                  }
                  String SDateTime = common.getServerDateTime2();

                  String str2 = common.Replicate("-",iLen)+"\n";
                  String str3 = "Report Taken on "+SDateTime+"\n";

                  FW.write(str2);
                  FW.write(str3);
            }
            catch(Exception ex){}
      }




}
