package GateNew;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import util.*;
import guiutil.*;
import jdbc.*;

public class GIAuthenticationFrame extends JInternalFrame
{
     JPanel    TopPanel;
     JPanel    BottomPanel;

     String    SStDate,SEnDate;
     JComboBox JCOrder,JCFilter;
     JButton   BApply,BExit,BSave;
    
     DateField TStDate;
     DateField TEnDate;
    
     Object RowData[][];
     String ColumnData[] = {"Authanticate","G.I. No","Date","Supplier","Invoice No","Inv Date","DC No","DC Date","Inward Type","Inward Mode","Category"};
     String ColumnType[] = {"B"           ,"N"      ,"S"   ,"S"       ,"S"         ,"S"       ,"S"    ,"S"      ,"S"          ,"S"          ,"S"};
                                 
     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;
     int iUserCode,iAuthCode,iMillCode;
     String SYearCode;
     String SItemTable,SSupTable;

     Common common = new Common();

     int iKeyCount=0;

     Vector VGINo,VGIDate,VSupName,VInvNo,VInvDate,VDCNo,VDCDate,VInwType,VInwMode,VCategory;

     TabReport tabreport;

     GIAuthenticationFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iAuthCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable)
     {
         super("Stores Gate Inward Pending for Authentication During a Period");

         this.DeskTop    = DeskTop;
         this.SPanel     = SPanel;
         this.VCode      = VCode;
         this.VName      = VName;
         this.iUserCode  = iUserCode;
         this.iAuthCode  = iAuthCode;
         this.iMillCode  = iMillCode;
         this.SYearCode  = SYearCode;
         this.SItemTable = SItemTable;
         this.SSupTable  = SSupTable;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     public void createComponents()
     {
         TopPanel    = new JPanel();
         BottomPanel = new JPanel();

         TStDate  = new DateField();
         TEnDate  = new DateField();
         BApply   = new JButton("Apply");
         BSave    = new JButton("Authenticate");
         BExit    = new JButton("Exit");

         JCOrder  = new JComboBox();
         JCFilter = new JComboBox();

         TStDate.setTodayDate();
         TEnDate.setTodayDate();

         BExit.setMnemonic('X');
         BApply.setMnemonic('A');
         BSave.setMnemonic('U');
         BSave.setEnabled(false);
         BExit.setEnabled(false);
     }

     public void setLayouts()
     {
         TopPanel.setLayout(new GridLayout(1,8));

         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,790,500);
     }

     public void addComponents()
     {
         JCOrder.addItem("G.I. No");
         JCOrder.addItem("Supplierwise");
         JCOrder.addItem("Inward Typewise");
         JCOrder.addItem("Inward Modewise");

         JCFilter.addItem("All");
         JCFilter.addItem("With Purchase Order");
         JCFilter.addItem("Without Purchase Order");

         TopPanel.add(new JLabel("Sorted On"));
         TopPanel.add(JCOrder);

         TopPanel.add(new JLabel("List Only"));
         TopPanel.add(JCFilter);

         TopPanel.add(new JLabel("Period"));
         TopPanel.add(TStDate);
         TopPanel.add(TEnDate);
         TopPanel.add(BApply);

         BottomPanel.add(BSave);
         BottomPanel.add(BExit);

         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
         BApply.addActionListener(new ApplyList(this));
         BExit.addActionListener(new exitList());
         BSave.addActionListener(new SaveList(this));
     }
     private class exitList implements ActionListener
     {
         public void actionPerformed(ActionEvent ae)
         {
               setVisible(false);
         }

     }
     private class SaveList implements ActionListener
     {
         GIAuthenticationFrame giauthframe;
         public SaveList(GIAuthenticationFrame giauthframe)
         {
               this.giauthframe = giauthframe;
         }

         public void actionPerformed(ActionEvent ae)
         {
               if(!isSelect())
               {
                     JOptionPane.showMessageDialog(null,"No Row Selected","Error",JOptionPane.ERROR_MESSAGE);
                     tabreport.ReportTable.requestFocus();
                     return;
               }
               BSave.setEnabled(false);
               persist();
               setTabReport(giauthframe);
               BSave.setEnabled(true);
         }

     }

     private boolean isSelect()
     {
         for(int i=0;i<RowData.length;i++)
         {
               Boolean bValue = (Boolean)tabreport.ReportTable.getValueAt(i,0);
               if(!bValue.booleanValue())
                     continue;
               return true;
         }
         return false;
     }
     private void persist()
     {
         try
         {

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       theStatement  =  theConnection.createStatement();

               for(int i=0;i<RowData.length;i++)
               {
                     Boolean bValue = (Boolean)tabreport.ReportTable.getValueAt(i,0);
 
                     if(!bValue.booleanValue())
                           continue;
 
                     String SGINo = (String)tabreport.ReportTable.getValueAt(i,1);
 
                     String QS = " Update GateInward set Authentication=1 where ModiStatus=0 and MillCode="+iMillCode+" and GINo="+SGINo;

                     theStatement.executeUpdate(QS);
               }
               theStatement.close();
         }
         catch(Exception ex){}

     }

     public class ApplyList implements ActionListener
     {
          GIAuthenticationFrame giauthframe;
          public ApplyList(GIAuthenticationFrame giauthframe)
          {
                this.giauthframe = giauthframe;
          }
          public void actionPerformed(ActionEvent ae)
          {
               setTabReport(giauthframe);
               if(RowData.length>0)
               {
                    BSave.setEnabled(true);
               }
               BExit.setEnabled(true);
               tabreport.ReportTable.requestFocus();
          }
     }
     public void setTabReport(GIAuthenticationFrame giauthframe)
     {
          setDataIntoVector();
          setRowData();
          try
          {
             getContentPane().remove(tabreport);
          }
          catch(Exception ex){}

          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);

             tabreport.ReportTable.addKeyListener(new KeyList(giauthframe));
             if((iUserCode==7) || (iUserCode==9))
             {
                    tabreport.ReportTable.addKeyListener(new KeyList(giauthframe));
             }
             getContentPane().add(tabreport,BorderLayout.CENTER);
             setSelected(true);
             DeskTop.repaint();
             DeskTop.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }

     }
     public class KeyList extends KeyAdapter
     {
           GIAuthenticationFrame giauthframe;
           public KeyList(GIAuthenticationFrame giauthframe)
           {
                 this.giauthframe = giauthframe;

                 iKeyCount=0;
           }
           public void keyPressed(KeyEvent ke)
           {
                 if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                 {
                       try
                       {
                             iKeyCount++;

                             if(iKeyCount==2)
                             {
                                  boolean bflag = false;
     
                                  int iRow = tabreport.ReportTable.getSelectedRow();
     
                                  String SGINo     = (String)tabreport.ReportTable.getValueAt(iRow,1);
                                  String SCategory = (String)tabreport.ReportTable.getValueAt(iRow,10);
                                  if(SCategory.equals("With P.O."))
                                       bflag = true;
     
                                  GIModiFrame gimodiframe = new GIModiFrame(DeskTop,VCode,VName,SPanel,iUserCode,iAuthCode,iMillCode,giauthframe,SGINo,bflag,SSupTable,SItemTable);
     
                                  try
                                  {
                                       DeskTop.add(gimodiframe);
                                       DeskTop.repaint();
                                       gimodiframe.setSelected(true);
                                       DeskTop.updateUI();
                                       gimodiframe.show();
                                  }
                                  catch(Exception e)
                                  {
                                       e.printStackTrace();     
                                  }
                                  iKeyCount=0;
                             }
                       }
                       catch(Exception ex)
                       {
                             System.out.println(ex);
                       }
                 }
           }
     }

     public void setDataIntoVector()
     {

           VGINo     = new Vector();
           VGIDate   = new Vector();
           VSupName  = new Vector();
           VInvNo    = new Vector();
           VInvDate  = new Vector();
           VDCNo     = new Vector();
           VDCDate   = new Vector();
           VInwType  = new Vector();
           VInwMode  = new Vector();
           VCategory = new Vector();

           SStDate = TStDate.toString();
           SEnDate = TEnDate.toString();
           String StDate  = TStDate.toNormal();
           String EnDate  = TEnDate.toNormal();
           try
           {

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       theStatement  =  theConnection.createStatement();
              String QString = getQString(StDate,EnDate);
              ResultSet res  = theStatement.executeQuery(QString);
              while (res.next())
              {

                 String SCategory="";
                 String str1 = common.parseNull(res.getString(10));
                 if(str1.equals("0"))
                 {
                     SCategory = "Without P.O.";
                 }
                 else
                 {
                     SCategory = "With P.O.";
                 }

                 if(JCFilter.getSelectedIndex()==1)
                     if(str1.equals("0"))
                        continue;

                 if(JCFilter.getSelectedIndex()==2)
                     if(str1.equals("1"))
                        continue;

                 VGINo    .addElement(common.parseNull(res.getString(1)));
                 VGIDate  .addElement(common.parseDate(res.getString(2)));
                 VSupName .addElement(common.parseNull(res.getString(3)));
                 VInvNo   .addElement(common.parseNull(res.getString(4)));
                 VInvDate .addElement(common.parseDate(res.getString(5)));
                 VDCNo    .addElement(common.parseNull(res.getString(6)));
                 VDCDate  .addElement(common.parseDate(res.getString(7)));
                 VInwType .addElement(common.parseNull(res.getString(8)));
                 VInwMode .addElement(common.parseNull(res.getString(9)));
                 VCategory.addElement(SCategory);
              }
              res.close();
              theStatement.close();
           }
           catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
         RowData     = new Object[VGIDate.size()][ColumnData.length];
         for(int i=0;i<VGIDate.size();i++)
         {
               RowData[i][0]  = new Boolean(false);
               RowData[i][1]  = (String)VGINo      .elementAt(i);
               RowData[i][2]  = (String)VGIDate    .elementAt(i);
               RowData[i][3]  = (String)VSupName   .elementAt(i);
               RowData[i][4]  = (String)VInvNo     .elementAt(i);
               RowData[i][5]  = (String)VInvDate   .elementAt(i);
               RowData[i][6]  = (String)VDCNo      .elementAt(i);
               RowData[i][7]  = (String)VDCDate    .elementAt(i);
               RowData[i][8]  = (String)VInwType   .elementAt(i);
               RowData[i][9]  = (String)VInwMode   .elementAt(i);
               RowData[i][10] = (String)VCategory  .elementAt(i);
        }  
     }

     public String getQString(String StDate,String EnDate)
     {
          // Category -    0 - without P.O.    1 - with P.O.

          String QString = " SELECT GateInward.GINo, GateInward.GIDate, "+SSupTable+".Name, GateInward.InvNo, GateInward.InvDate, GateInward.DcNo, GateInward.DcDate, InwType.InwName, InwardMode.Name, nvl(GateInward.EntryStatus,0) as Category "+
                           " FROM ((GateInward INNER JOIN "+SSupTable+" ON GateInward.Sup_Code = "+SSupTable+".Ac_Code) INNER JOIN InwType ON GateInward.InwNo = InwType.InwNo) INNER JOIN InwardMode ON GateInward.ModeCode = InwardMode.Code "+
                           " Group by GateInward.GINo, GateInward.GIDate, "+SSupTable+".Name, GateInward.InvNo, GateInward.InvDate, GateInward.DcNo, GateInward.DcDate, InwType.InwName, InwardMode.Name,nvl(GateInward.EntryStatus,0),GateInward.Status,GateInward.GrnNo,GateInward.ModiStatus,GateInward.Authentication,GateInward.MillCode "+
                           " Having GateInward.GIDate >= '"+StDate+"' and GateInward.GIDate <='"+EnDate+"' and GateInward.MillCode="+iMillCode+
                           " and GateInward.ModiStatus=0 and GateInward.Status=0 and GateInward.GrnNo=0 and GateInward.Authentication=0 ";

          if(JCOrder.getSelectedIndex() == 0)      
               QString = QString+" Order By GINo,GIDate";
          if(JCOrder.getSelectedIndex() == 1)
               QString = QString+" Order By Name,GIDate";
          if(JCOrder.getSelectedIndex() == 2)
               QString = QString+" Order By InwName,GIDate";
          if(JCOrder.getSelectedIndex() == 3)
               QString = QString+" Order By Name,GIDate";

          return QString;
     }
}
