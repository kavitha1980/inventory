package GateNew;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import util.*;
import guiutil.*;
import jdbc.*;

public class GICritPanel extends JPanel
{
    JPanel TopPanel,BottomPanel;
    JPanel PartyPanel;
    JPanel TypePanel;
    JPanel ModePanel;
    JPanel NamePanel;
    JPanel InvoicePanel;
    JPanel DCPanel;
    JPanel CategoryPanel;
    JPanel StatusPanel;
    JPanel SortPanel;
    JPanel BasePanel;
    JPanel ControlPanel;
    JPanel ApplyPanel;

    Common common = new Common();
    Vector VParty,VPartyCode,VType,VTypeCode,VMode,VModeCode;
    MyComboBox JCParty,JCType,JCMode,JCName,JCInvoice,JCDC,JCCategory,JCStatus;

    MyTextField TSort;

    JRadioButton JRAllParty,JRSeleParty;
    JRadioButton JRAllType,JRSeleType;
    JRadioButton JRAllMode,JRSeleMode;
    JRadioButton JRAllName,JRSeleName;
    JRadioButton JRAllInv,JRSeleInv;
    JRadioButton JRAllDC,JRSeleDC;
    JRadioButton JRAllCat,JRSeleCat;
    JRadioButton JRAllStat,JRSeleStat;
    JRadioButton JRDate,JRGateNo;

    JButton      BApply;
    MyTextField  TStNo,TEnNo;
    DateField    TStDate,TEnDate;

    boolean bsig;

    Vector VCode,VName;
    int iMillCode;
    String SSupTable;


    GICritPanel(Vector VCode,Vector VName,int iMillCode,String SSupTable)
    {
        this.VCode     = VCode;
        this.VName     = VName;
        this.iMillCode = iMillCode;
        this.SSupTable = SSupTable;

        getData();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
    }
    public void createComponents()
    {
        TopPanel      = new JPanel();
        BottomPanel   = new JPanel();

        PartyPanel    = new JPanel();
        TypePanel     = new JPanel();
        ModePanel     = new JPanel();
        NamePanel     = new JPanel();
        InvoicePanel  = new JPanel();
        DCPanel       = new JPanel();
        CategoryPanel = new JPanel();
        StatusPanel   = new JPanel();
        SortPanel     = new JPanel();
        BasePanel     = new JPanel();
        ControlPanel  = new JPanel();
        ApplyPanel    = new JPanel();

        JRAllParty  = new JRadioButton("All",true);
        JRSeleParty = new JRadioButton("Selected");
        JCParty     = new MyComboBox(VParty);
        JCParty.setEnabled(false);

        JRAllType   = new JRadioButton("All",true);
        JRSeleType  = new JRadioButton("Selected");
        JCType      = new MyComboBox(VType);
        JCType.setEnabled(false);

        JRAllMode   = new JRadioButton("All",true);
        JRSeleMode  = new JRadioButton("Selected");
        JCMode      = new MyComboBox(VMode);
        JCMode.setEnabled(false);

        JRAllName   = new JRadioButton("All",true);
        JRSeleName  = new JRadioButton("Selected");
        JCName      = new MyComboBox(VName);
        JCName.setEnabled(false);

        JRAllInv    = new JRadioButton("All",true);
        JRSeleInv   = new JRadioButton("Selected");
        JCInvoice   = new MyComboBox();
        JCInvoice.setEnabled(false);

        JRAllDC     = new JRadioButton("All",true);
        JRSeleDC    = new JRadioButton("Selected");
        JCDC        = new MyComboBox();
        JCDC.setEnabled(false);

        JRAllCat    = new JRadioButton("All",true);
        JRSeleCat   = new JRadioButton("Selected");
        JCCategory  = new MyComboBox();
        JCCategory.setEnabled(false);

        JRAllStat   = new JRadioButton("All",true);
        JRSeleStat  = new JRadioButton("Selected");
        JCStatus    = new MyComboBox();
        JCStatus.setEnabled(false);

        JRDate      = new JRadioButton("Date",true);
        JRGateNo    = new JRadioButton("G.I. No");
        bsig = true;

        TSort       = new MyTextField();
        TStNo       = new MyTextField();
        TEnNo       = new MyTextField();

        TStDate     = new DateField();
        TEnDate     = new DateField();

        BApply      = new JButton("Apply");

        TStDate.setTodayDate();
        TEnDate.setTodayDate();

    }
    public void setLayouts()
    {
        setLayout(new GridLayout(2,1));
        TopPanel.setLayout(new GridLayout(1,5));
        BottomPanel.setLayout(new GridLayout(1,6));

        PartyPanel   .setLayout(new GridLayout(3,1));
        TypePanel    .setLayout(new GridLayout(3,1));
        ModePanel    .setLayout(new GridLayout(3,1));
        NamePanel    .setLayout(new GridLayout(3,1));
        InvoicePanel .setLayout(new GridLayout(3,1));   
        DCPanel      .setLayout(new GridLayout(3,1));
        CategoryPanel.setLayout(new GridLayout(3,1));
        StatusPanel  .setLayout(new GridLayout(3,1));
        SortPanel    .setLayout(new GridLayout(3,1));
        BasePanel    .setLayout(new GridLayout(2,1));
        ControlPanel .setLayout(new GridLayout(3,1));
        ApplyPanel   .setLayout(new GridLayout(3,1));

        PartyPanel.setBorder(new TitledBorder("Party Name"));
        TypePanel.setBorder(new TitledBorder("Inward Type"));
        ModePanel.setBorder(new TitledBorder("Inward Mode"));
        NamePanel.setBorder(new TitledBorder("Material"));
        InvoicePanel.setBorder(new TitledBorder("Invoice"));
        DCPanel.setBorder(new TitledBorder("D.C."));
        CategoryPanel.setBorder(new TitledBorder("Category"));
        StatusPanel.setBorder(new TitledBorder("Status"));
        SortPanel.setBorder(new TitledBorder("Sort on"));
        BasePanel.setBorder(new TitledBorder("Based on"));
        ControlPanel.setBorder(new TitledBorder("Control"));
        ApplyPanel.setBorder(new TitledBorder("Apply"));
    }
    public void addComponents()
    {

        JCInvoice.addItem("With Invoice");
        JCInvoice.addItem("Without Invoice");

        JCDC.addItem("With D.C.");
        JCDC.addItem("Without D.C.");

        JCCategory.addItem("With P.O.");
        JCCategory.addItem("Without P.O.");

        JCStatus.addItem("Authenticated");
        JCStatus.addItem("Not Authenticated");

        add(TopPanel);
        add(BottomPanel);

        TopPanel.add(PartyPanel);
        TopPanel.add(TypePanel);
        TopPanel.add(ModePanel);
        TopPanel.add(NamePanel);
        TopPanel.add(InvoicePanel);
        BottomPanel.add(DCPanel);
        //BottomPanel.add(CategoryPanel);
        BottomPanel.add(StatusPanel);
        BottomPanel.add(SortPanel);
        BottomPanel.add(BasePanel);
        BottomPanel.add(ControlPanel);
        BottomPanel.add(ApplyPanel);

        PartyPanel.add(JRAllParty);
        PartyPanel.add(JRSeleParty);
        PartyPanel.add(JCParty);

        TypePanel.add(JRAllType);
        TypePanel.add(JRSeleType);
        TypePanel.add(JCType);

        ModePanel.add(JRAllMode);
        ModePanel.add(JRSeleMode);
        ModePanel.add(JCMode);

        NamePanel.add(JRAllName);
        NamePanel.add(JRSeleName); 
        NamePanel.add(JCName);     

        InvoicePanel.add(JRAllInv);
        InvoicePanel.add(JRSeleInv);
        InvoicePanel.add(JCInvoice);    

        DCPanel.add(JRAllDC);
        DCPanel.add(JRSeleDC);
        DCPanel.add(JCDC);

        CategoryPanel.add(JRAllCat);
        CategoryPanel.add(JRSeleCat);
        CategoryPanel.add(JCCategory);

        StatusPanel.add(JRAllStat);
        StatusPanel.add(JRSeleStat);
        StatusPanel.add(JCStatus);

        SortPanel.add(TSort);
        SortPanel.add(new JLabel("eg 1,2,3"));
        SortPanel.add(new JLabel(" "));

        BasePanel.add(JRDate);
        BasePanel.add(JRGateNo);

        addControlPanel();

        ApplyPanel.add(new JLabel(" "));
        ApplyPanel.add(BApply);
        ApplyPanel.add(new JLabel(" "));
    }
    public void addControlPanel()
    {
        ControlPanel.removeAll();
        if(bsig)
        {
            ControlPanel.add(TStDate);
            ControlPanel.add(TEnDate);
            ControlPanel.add(new JLabel(" "));
        }
        else
        {
            ControlPanel.add(TStNo);
            ControlPanel.add(TEnNo);
            ControlPanel.add(new JLabel(" "));

        }
        ControlPanel.updateUI();
    }
    public void addListeners()
    {
        JRAllParty.addActionListener(new PartyList());
        JRSeleParty.addActionListener(new PartyList());

        JRAllType.addActionListener(new TypeList());
        JRSeleType.addActionListener(new TypeList());

        JRAllMode.addActionListener(new ModeList());
        JRSeleMode.addActionListener(new ModeList());

        JRAllName.addActionListener(new NameList());
        JRSeleName.addActionListener(new NameList());

        JRAllInv.addActionListener(new InvList());
        JRSeleInv.addActionListener(new InvList());

        JRAllDC.addActionListener(new DCList());
        JRSeleDC.addActionListener(new DCList());

        JRAllCat.addActionListener(new CategoryList());
        JRSeleCat.addActionListener(new CategoryList());

        JRAllStat.addActionListener(new StatusList());
        JRSeleStat.addActionListener(new StatusList());

        JRDate.addActionListener(new BaseList());
        JRGateNo.addActionListener(new BaseList());
    }

    public class PartyList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllParty)
            {
                JRAllParty.setSelected(true);
                JRSeleParty.setSelected(false);
                JCParty.setEnabled(false);

            }
            if(ae.getSource()==JRSeleParty)
            {
                JRAllParty.setSelected(false);
                JRSeleParty.setSelected(true);
                JCParty.setEnabled(true);
            }
        }
    }
    public class TypeList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllType)
            {
                JRAllType.setSelected(true);
                JRSeleType.setSelected(false);
                JCType.setEnabled(false);
            }
            if(ae.getSource()==JRSeleType)
            {
                JRAllType.setSelected(false);
                JRSeleType.setSelected(true);
                JCType.setEnabled(true);
            }
        }
    }
    public class ModeList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllMode)
            {
                JRAllMode.setSelected(true);
                JRSeleMode.setSelected(false);
                JCMode.setEnabled(false);
            }
            if(ae.getSource()==JRSeleMode)
            {
                JRAllMode.setSelected(false);
                JRSeleMode.setSelected(true);
                JCMode.setEnabled(true);
            }
        }
    }
    public class NameList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllName)
            {
                JRAllName.setSelected(true);
                JRSeleName.setSelected(false);
                JCName.setEnabled(false);
            }
            if(ae.getSource()==JRSeleName)
            {
                JRAllName.setSelected(false);
                JRSeleName.setSelected(true);
                JCName.setEnabled(true);
            }
        }
    }
    public class InvList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllInv)
            {
                JRAllInv.setSelected(true);
                JRSeleInv.setSelected(false);
                JCInvoice.setEnabled(false);
            }
            if(ae.getSource()==JRSeleInv)
            {
                JRAllInv.setSelected(false);
                JRSeleInv.setSelected(true);
                JCInvoice.setEnabled(true);
            }
        }
    }
    public class DCList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllDC)
            {
                JRAllDC.setSelected(true);
                JRSeleDC.setSelected(false);
                JCDC.setEnabled(false);
            }
            if(ae.getSource()==JRSeleDC)
            {
                JRAllDC.setSelected(false);
                JRSeleDC.setSelected(true);
                JCDC.setEnabled(true);
            }
        }
    }
    public class CategoryList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllCat)
            {
                JRAllCat.setSelected(true);
                JRSeleCat.setSelected(false);
                JCCategory.setEnabled(false);
            }
            if(ae.getSource()==JRSeleCat)
            {
                JRAllCat.setSelected(false);
                JRSeleCat.setSelected(true);
                JCCategory.setEnabled(true);
            }
        }
    }
    public class StatusList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllStat)
            {
                JRAllStat.setSelected(true);
                JRSeleStat.setSelected(false);
                JCStatus.setEnabled(false);
            }
            if(ae.getSource()==JRSeleStat)
            {
                JRAllStat.setSelected(false);
                JRSeleStat.setSelected(true);
                JCStatus.setEnabled(true);
            }
        }
    }

    public class BaseList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRDate)
            {
                bsig = true;
                JRDate.setSelected(true);
                JRGateNo.setSelected(false);
                addControlPanel();
            }
            if(ae.getSource()==JRGateNo)
            {
                bsig = false;
                JRGateNo.setSelected(true);
                JRDate.setSelected(false);
                addControlPanel();
            }
        }
    }

    public void getData()
    {
        VParty         = new Vector();
        VPartyCode     = new Vector();
                   
        VType          = new Vector();
        VTypeCode      = new Vector();

        VMode          = new Vector();
        VModeCode      = new Vector();

        String QS1="";
        String QS2="";
        String QS3="";

        try
        {
             QS1 = "Select Ac_Code,Name from "+SSupTable+" order by Name";
             QS2 = "Select InwNo,InwName from InwType order by InwName";
             QS3 = "Select Code,Name from InwardMode order by Name";


               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();

             ResultSet res1  = theStatement.executeQuery(QS1);
             while(res1.next())
             {
                  VPartyCode.addElement(common.parseNull(res1.getString(1)));
                  VParty.addElement(common.parseNull(res1.getString(2)));
             }
             res1.close();
             ResultSet res2  = theStatement.executeQuery(QS2);
             while(res2.next())
             {
                  VTypeCode.addElement(common.parseNull(res2.getString(1)));
                  VType.addElement((common.parseNull(res2.getString(2))).toUpperCase());
             }
             res2.close();
             ResultSet res3  = theStatement.executeQuery(QS3);
             while(res3.next())
             {
                  VModeCode.addElement(common.parseNull(res3.getString(1)));
                  VMode.addElement(common.parseNull(res3.getString(2)));
             }
             res3.close();
             theStatement.close();
        }
        catch(Exception ex)
        {
             ex.printStackTrace();
        }

    }

}
