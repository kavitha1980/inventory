package GateNew;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import util.*;
import guiutil.*;
import jdbc.*;

public class GIDeleteListFrame extends JInternalFrame
{
     JPanel    TopPanel;
     JPanel    BottomPanel;

     String    SStDate,SEnDate;
     JButton   BApply,BExit,BPrint;
     MyTextField TFile;
    
     DateField TStDate;
     DateField TEnDate;
    
     Object RowData[][];
     String ColumnData[] = {"G.I. No","G.I. Date","Supplier","Date of Deletion"};
     String ColumnType[] = {"N"      ,"S"        ,"S"       ,"S"               };
                                 
     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;
     int iUserCode,iAuthCode,iMillCode;
     String SYearCode;
     String SItemTable,SSupTable,SMillName;

     Common common = new Common();
     File                     file;

     Vector VGINo,VGIDate,VSupName,VDelDate;

     TabReport tabreport;

     String SInt = "  ";
     String Strline = "";
     int iLen=0;

     GIDeleteListFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iAuthCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable,String SMillName)
     {
         super("Stores Gate Inward Deleted List During the Period");

         this.DeskTop    = DeskTop;
         this.SPanel     = SPanel;
         this.VCode      = VCode;
         this.VName      = VName;
         this.iUserCode  = iUserCode;
         this.iAuthCode  = iAuthCode;
         this.iMillCode  = iMillCode;
         this.SYearCode  = SYearCode;
         this.SItemTable = SItemTable;
         this.SSupTable  = SSupTable;
         this.SMillName  = SMillName;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     public void createComponents()
     {
         TopPanel    = new JPanel();
         BottomPanel = new JPanel();

         TStDate  = new DateField();
         TEnDate  = new DateField();
         BApply   = new JButton("Apply");
         BPrint   = new JButton("Print");
         BExit    = new JButton("Exit");

         TFile       = new MyTextField(25);

         TFile.setText("1.prn");

         TStDate.setTodayDate();
         TEnDate.setTodayDate();

         BExit.setMnemonic('X');
         BApply.setMnemonic('A');
         BPrint.setMnemonic('P');
         BPrint.setEnabled(false);
         BExit.setEnabled(false);
         TFile.setEditable(false);
     }

     public void setLayouts()
     {
         TopPanel.setLayout(new GridLayout(1,4,10,10));

         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,650,500);
     }

     public void addComponents()
     {
         TopPanel.add(new JLabel("Period"));
         TopPanel.add(TStDate);
         TopPanel.add(TEnDate);
         TopPanel.add(BApply);

         BottomPanel.add(BPrint);
         BottomPanel.add(BExit);
         BottomPanel.add(TFile);

         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
         BApply.addActionListener(new ApplyList());
         BExit.addActionListener(new exitList());
         BPrint.addActionListener(new PrintList());
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BPrint.setEnabled(false);
               String STitle = "GateInward Deleted List For the Period From "+SStDate+" To "+SEnDate; 
               Vector VHead  = getHead();
               iLen = ((String)VHead.elementAt(0)).length();
               Strline = common.Replicate("-",iLen)+"\n";
               Vector VBody  = getBody();
               String SFile  = TFile.getText();
               if(VBody.size()>0)
                     new DocPrint(VBody,VHead,STitle,SFile,iMillCode,SMillName);
               BExit.requestFocus();
          }
     }

     public Vector getHead()
     {
           Vector vect = new Vector();

           //                     0           1            2            3          
           String Head1[] = {"G.I. Date","G.I. Number","Supplier","Date Of Deletion"};

           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();

           Sha1  = common.Pad(Sha1,10)+SInt;
           Sha2  = common.Rad(Sha2,12)+SInt;
           Sha3  = common.Pad(Sha3,35)+SInt;
           Sha4  = common.Pad(Sha4,16);

           String Strh1 = Sha1+Sha2+Sha3+Sha4+"\n";
           vect.add(Strh1);
           return vect;
     }
     public Vector getBody()
     {
           Vector vect = new Vector();
           for(int i=0;i<VGINo.size();i++)
           {
                 String Sda1  = (String)VGIDate.elementAt(i);
                 String Sda2  = (String)VGINo.elementAt(i);
                 String Sda3  = (String)VSupName.elementAt(i);
                 String Sda4  = (String)VDelDate.elementAt(i);

                 Sda1  = common.Pad(Sda1,10)+SInt;
                 Sda2  = common.Rad(Sda2,12)+SInt;
                 Sda3  = common.Pad(Sda3,35)+SInt;
                 Sda4  = common.Cad(Sda4,16);

                 String Strd = Sda1+Sda2+Sda3+Sda4+"\n";
                 vect.add(Strd);
           }
           return vect;
     }


     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     private class exitList implements ActionListener
     {
         public void actionPerformed(ActionEvent ae)
         {
               setVisible(false);
         }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setTabReport();
               if(RowData.length>0)
               {
                    BPrint.setEnabled(true);
               }
               BExit.setEnabled(true);
               TFile.setEditable(true);
          }
     }
     public void setTabReport()
     {
          setDataIntoVector();
          setRowData();
          try
          {
             getContentPane().remove(tabreport);
          }
          catch(Exception ex){}

          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
             getContentPane().add(tabreport,BorderLayout.CENTER);
             setSelected(true);
             DeskTop.repaint();
             DeskTop.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }

     }

     public void setDataIntoVector()
     {

          VGINo        = new Vector();
          VGIDate      = new Vector();
          VSupName     = new Vector();
          VDelDate     = new Vector();

          SStDate = TStDate.toString();
          SEnDate = TEnDate.toString();
          String StDate  = TStDate.toNormal();
          String EnDate  = TEnDate.toNormal();
          try
          {

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
 
               String QString = getQString(StDate,EnDate);
               ResultSet res  = theStatement.executeQuery(QString);
               while (res.next())
               {
                    VGINo       .addElement(common.parseNull(res.getString(1)));
                    VGIDate     .addElement(common.parseDate(res.getString(2)));
                    VSupName    .addElement(common.parseNull(res.getString(3)));
                    VDelDate    .addElement(common.parseDate(res.getString(4)));
               }
               res.close();
               theStatement.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VGIDate.size()][ColumnData.length];
          for(int i=0;i<VGIDate.size();i++)
          {
               RowData[i][0]  = (String)VGINo       .elementAt(i);
               RowData[i][1]  = (String)VGIDate     .elementAt(i);
               RowData[i][2]  = (String)VSupName    .elementAt(i);
               RowData[i][3]  = (String)VDelDate    .elementAt(i);
          }  
     }    

     public String getQString(String StDate,String EnDate)
     {
          String QString = " SELECT GINo, GIDate, "+SSupTable+".Name, DeletedDate "+
                           " FROM DeletedGateInward INNER JOIN "+SSupTable+" ON DeletedGateInward.Sup_Code = "+SSupTable+".Ac_Code "+
                           " Where GIDate >= '"+StDate+"' and GIDate <='"+EnDate+"' and MillCode="+iMillCode+
                           " Order by 2,1 ";

          return QString;
     }

}
