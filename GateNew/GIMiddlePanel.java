
package GateNew;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import util.*;

public class GIMiddlePanel extends JPanel 
{
         JScrollPane TabScroll;
         InvMiddlePanel MiddlePanel;

         Object RowData[][];
         String ColumnData[] = {"Code","Name","DC/Inv Qty Mentioned By Supplier","Qty Measured @ Gate","Unmeasureable No of packs"};
         String ColumnType[] = {"S"   ,"S"   ,"B"                               ,"B"                  ,"B"                        };

         JLayeredPane Layer;
         Vector VCode,VName;
         StatusPanel SPanel;

         Common common = new Common();
         public GIMiddlePanel(JLayeredPane Layer,Vector VCode,Vector VName,StatusPanel SPanel)
         {
              this.Layer  = Layer;
              this.VCode  = VCode;
              this.VName  = VName;
              this.SPanel = SPanel;

              setLayout(new BorderLayout());
         }
         public void createComponents()
         {
               try
               {
                  MiddlePanel           = new InvMiddlePanel(Layer,VCode,VName,RowData,ColumnData,ColumnType);
                  JScrollPane TabScroll = new JScrollPane(MiddlePanel);
                  add(TabScroll,BorderLayout.CENTER);
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
               }
         }
         public void setRowData(Vector VSelectedCode,Vector VSelectedName)
         {
            RowData     = new Object[VSelectedCode.size()][ColumnData.length];
            for(int i=0;i<VSelectedCode.size();i++)
            {
                  RowData[i][0] = (String)VSelectedCode.elementAt(i);
                  RowData[i][1] = (String)VSelectedName.elementAt(i);
                  RowData[i][2] = "";
                  RowData[i][3] = "";
                  RowData[i][4] = "";
            }
         }
}         
