package GateNew;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.io.*;
import java.sql.*;
import util.*;
import guiutil.*;
import jdbc.*;

public class GIModiFrame extends JInternalFrame
{
     JButton         BOk,BCancel,BAdd,BCut,BPending;
     MyLabel         LGateNo;
     MyComboBox      JCInward,JCSupplier;
     DateField       TGateDate,TInvDate,TDCDate;
     MyTextField     TInvNo,TDCNo;
     InwardModeModel InwMode;
     MyTextField     TRemarks;
     JPanel          TopPanel,MiddlePanel,BottomPanel;

     Vector VInward,VInwardCode,VSupName,VSupCode;
     Vector VPendName,VPendCode;

     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel  SPanel;
     int iUserCode,iAuthCode,iMillCode;
     GIAuthenticationFrame giauthframe;
     String SGINo;
     String SSupTable,SItemTable;
     boolean bflag;   // for differentiate With P.O. and Without P.O.
                      // If true = With P.O., If false = Without P.O.

     int iCtr=0;     // used in ItemListener to control loop

     Object RowData[][];
     String ColumnData[] = {"Code","Name","DC/Inv Qty Mentioned By Supplier","Qty Measured @ Gate","Unmeasureable No of packs"};
     String ColumnType[] = {"S"   ,"E"   ,"B"                               ,"B"                  ,"B"                        };

     GateTableModel GateModel;
     JTable         ReportTable;

     Common common   = new Common();
     Control control = new Control();
          
     GIModiFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iAuthCode,int iMillCode,GIAuthenticationFrame giauthframe,String SGINo,boolean bflag,String SSupTable,String SItemTable)
     {
          this.DeskTop     = DeskTop;
          this.VCode       = VCode;
          this.VName       = VName;
          this.SPanel      = SPanel;
          this.iUserCode   = iUserCode;
          this.iAuthCode   = iAuthCode;
          this.iMillCode   = iMillCode;
          this.giauthframe = giauthframe;
          this.SGINo       = SGINo;
          this.bflag       = bflag;
          this.SSupTable   = SSupTable;
          this.SItemTable  = SItemTable;

          setDataIntoVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
          setData(SGINo);
     }
     public void createComponents()
     {
          BOk        = new JButton("Update");
          BCancel    = new JButton("Exit");
          BAdd       = new JButton("Add");
          BCut       = new JButton("Cut");
          BPending   = new JButton("Pending");

          TGateDate   = new DateField();
          TInvDate    = new DateField();
          TDCDate     = new DateField();
          LGateNo     = new MyLabel("");
          TInvNo      = new MyTextField(15);
          TDCNo       = new MyTextField(15);
          JCSupplier  = new MyComboBox(VSupName);
          JCInward    = new MyComboBox(VInward);
          InwMode     = new InwardModeModel(iUserCode,iAuthCode,iMillCode);
          TRemarks    = new MyTextField(50);
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();
          MiddlePanel = new JPanel();

          BOk.setMnemonic('U');
          BCancel.setMnemonic('E');
          BAdd.setMnemonic('A');
          BCut.setMnemonic('C');
          BPending.setMnemonic('P');
          InwMode.setMnemonic('M');
          BPending.setEnabled(false);
          TGateDate.setEditable(false);
     }
     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
          if(bflag)
          {
               setTitle("Gate Inward of Stores & Spares Materials with P.O.");
          }
          if(!bflag)
          {
               setTitle("Gate Inward of Stores & Spares Materials without P.O.");
          }

          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(6,4,5,5));
          MiddlePanel         .setLayout(new GridLayout(1,1,5,5));
          BottomPanel         .setLayout(new FlowLayout());

          BPending.setBorder(new javax.swing.border.BevelBorder(0));
          BPending.setBackground(new Color(128,128,255));
          BPending.setForeground(Color.white);
     }
     public void addComponents()
     {
          TopPanel.add(new JLabel("Supplier"));
          TopPanel.add(JCSupplier);

          TopPanel.add(new JLabel("Inward Type"));
          TopPanel.add(JCInward);

          TopPanel.add(new JLabel("Gate Inward No"));
          TopPanel.add(LGateNo);

          TopPanel.add(new JLabel("Gate Inward Date"));
          TopPanel.add(TGateDate);

          TopPanel.add(new JLabel("Invoice No"));
          TopPanel.add(TInvNo);

          TopPanel.add(new JLabel("Invoice Date"));
          TopPanel.add(TInvDate);

          TopPanel.add(new JLabel("DC No"));
          TopPanel.add(TDCNo);

          TopPanel.add(new JLabel("DC Date"));
          TopPanel.add(TDCDate);

          TopPanel.add(new JLabel("Select Inward Mode"));
          TopPanel.add(InwMode);

          TopPanel.add(new JLabel("Remarks"));
          TopPanel.add(TRemarks);

          TopPanel.add(new JLabel("Pending Materials"));
          TopPanel.add(BPending);

          BottomPanel.add(new JLabel("F3 - Select Item"));
          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);
          BottomPanel.add(BAdd);
          BottomPanel.add(BCut);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
           BAdd.addActionListener(new ActList());
           BCut.addActionListener(new ActList());
           BOk.addActionListener(new ActList());
           BCancel.addActionListener(new ActList());
           BPending.addActionListener(new PendingList(DeskTop,VSupCode,JCSupplier,this,true,iMillCode));
     }
     public class ActList implements ActionListener
     {
           public void actionPerformed(ActionEvent ae)
           {
                  if(ae.getSource()==BAdd)
                        doNew();
                  if(ae.getSource()==BCut)
                        doCut();
                  if(ae.getSource()==BOk)
                        doSave();
                  if(ae.getSource()==BCancel)
                        doCancel();
           }

     }
     public class ItemList implements ItemListener
     {
           GIModiFrame gimodiframe;

           public ItemList(GIModiFrame gimodiframe)
           {
                  this.gimodiframe = gimodiframe;
           }

           public void itemStateChanged(ItemEvent ie)
           {
                  iCtr++;

                  if(iCtr==2)
                  {

                       try
                       {
                            MiddlePanel.removeAll();
                       }
                       catch(Exception ex){}
                       updateUI();
                       BPending.setEnabled(true);
                  }
                  if(iCtr>1)
                       iCtr=0;
           }
     }

     public void doNew()
     {
            Vector VEmptyData = new Vector();
            for(int i=0;i<ColumnData.length;i++)
                  VEmptyData.addElement("");
            GateModel.insertRow(GateModel.getRows(),VEmptyData);
	    GateModel.setColumnData();
            ReportTable.updateUI();
            ReportTable.requestFocus();
     }
     public void doCut()
     {
            try
            {
                int iRows = GateModel.getRows();
                if(iRows>1)
                {
                     int i = ReportTable.getSelectedRow();
                     GateModel.removeRow(i);
	    	     GateModel.setColumnData();
                     ReportTable.updateUI();
                }
            }
            catch(Exception ex){}
     }
     public void doSave()
     {
            try
            {
                 BOk.setEnabled(false);
                 boolean bSig = false;
                 try
                 {
                       bSig = allOk();
                 }
                 catch(Exception ex)
                 {
                       System.out.println(ex);
                 }
                 if(bSig)
                 {
                      updateOldGIDetails();
                      insertGIDSDetails();
                      removeHelpFrame();
          
                      giauthframe.setSelected(true);
                      giauthframe.setTabReport(giauthframe);
                 }
                 else
                 {
                       BOk.setEnabled(true);
                 }
            }
            catch(Exception ex)
            {
                 System.out.println(ex);
            }
     }
     public void doCancel()
     {
            removeHelpFrame();
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
     public void updateOldGIDetails()
     {
          
          try
          {
               String QS = " Update GateInward Set ModiStatus=1 where MillCode="+iMillCode+" and GINo="+SGINo;

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       theStatement  =  theConnection.createStatement();
               theStatement.execute(QS);
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex.getMessage());
          }
     }

     public void insertGIDSDetails()
     {
          Object FinalData[][] = GateModel.getFromVector();
          String STime = common.getServerDateTime();
          String SRemarks = ((TRemarks.getText()).trim()).toUpperCase();

          String QS="";
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       theStatement  =  theConnection.createStatement();
               for(int i=0;i<FinalData.length;i++)
               {
                    {
                         QS = "Insert Into GateInward (GINo,GIDate,InwNo,Sup_Code,InvNo,InvDate,DcNo,DcDate,ModeCode,Remarks,Item_Code,Item_Name,SupQty,GateQty,UnmeasuredQty,UserCode,UserTime,BaseCode,MillCode,Id) Values(";
                         QS = QS+"0"+LGateNo.getText()+",";
                         QS = QS+"'"+TGateDate.TYear.getText()+TGateDate.TMonth.getText()+TGateDate.TDay.getText()+"',";
                         QS = QS+"0"+(String)VInwardCode.elementAt(JCInward.getSelectedIndex())+",";
                         QS = QS+"'"+(String)VSupCode.elementAt(JCSupplier.getSelectedIndex())+"',";
                         QS = QS+"'"+TInvNo.getText().toUpperCase()+"',";
                         QS = QS+"'"+TInvDate.TYear.getText()+TInvDate.TMonth.getText()+TInvDate.TDay.getText()+"',";
                         QS = QS+"'"+TDCNo.getText().toUpperCase()+"',";
                         QS = QS+"'"+TDCDate.TYear.getText()+TDCDate.TMonth.getText()+TDCDate.TDay.getText()+"',";
                         QS = QS+"0"+InwMode.TCode.getText()+",";
                         QS = QS+"'"+SRemarks+"',";
                         QS = QS+"'"+common.parseNull((String)FinalData[i][0])+"',";
                         QS = QS+"'"+((String)FinalData[i][1]).toUpperCase()+"',";
                         QS = QS+"0"+common.getRound(((String)FinalData[i][2]).trim(),3)+",";
                         QS = QS+"0"+common.getRound(((String)FinalData[i][3]).trim(),3)+",";
                         QS = QS+"0"+common.getRound(((String)FinalData[i][4]).trim(),3)+",";
                         QS = QS+"0"+String.valueOf(iUserCode)+",";
                         QS = QS+"'"+STime+"',";
                         QS = QS+"0"+String.valueOf(iAuthCode)+",";
                         QS = QS+"0"+String.valueOf(iMillCode)+",gateInward_seq.nextval)";
                    }
                    theStatement.execute(QS);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex.getMessage());
          }
     }
     public boolean allOk()
     {
          String SInwMode     = common.parseNull(InwMode.getText().trim());
          String SInvNo       = common.parseNull(TInvNo.getText().trim());
          String SInvDate     = TInvDate.toNormal();
          String SDCNo        = common.parseNull(TDCNo.getText().trim());
          String SDCDate      = TDCDate.toNormal();
          String SCurDate     = common.getServerDate();

          int iRows=0;

          if(!SDCNo.equals(""))
          {
               if(common.toInt(SDCDate) ==0)
               {
                    JOptionPane.showMessageDialog(null,"Enter Dc Date ","Proplem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(common.toInt(SDCDate)>0)
          {
               if(SDCNo.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Enter Dc No ","Proplem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(!SInvNo.equals(""))
          {
               if(common.toInt(SInvDate) ==0)
               {
                    JOptionPane.showMessageDialog(null,"Enter Invoice Date ","Proplem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(common.toInt(SInvDate)>0)
          {
               if(SInvNo.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Enter Invoice No ","Proplem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(SDCNo.equals("") && SInvNo.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Select DC No Or Invoice No","Proplem in Save Data",JOptionPane.ERROR_MESSAGE);
               return false;

          }
          if((SInwMode.equals("Mode")) || (SInwMode.equals("")))
          {
              JOptionPane.showMessageDialog(null,"Inward Mode Must be selected ","Error",JOptionPane.ERROR_MESSAGE);
              InwMode.requestFocus();
              return false;
          }

          try
          {
               iRows    = GateModel.getRows();
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"No Materials Selected","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          Object FinalData[][] = GateModel.getFromVector();
          for(int i=0;i<FinalData.length;i++)
          {
               String S1 = ((String)FinalData[i][0]).trim();
               String S2 = ((String)FinalData[i][1]).trim();
               String S3 = ((String)FinalData[i][2]).trim();
               String S4 = ((String)FinalData[i][3]).trim();
               String S5 = ((String)FinalData[i][4]).trim();

               String SValue1 = common.getRound(S3,3);
               String SValue2 = common.getRound(S4,3);
               String SValue3 = common.getRound(S5,3);

               if((S2.length()==0) || (S3.length()==0) || (S4.length()==0) || (S5.length()==0))
               {
                    JOptionPane.showMessageDialog(null,"All Fields Must be Filled","Error",JOptionPane.ERROR_MESSAGE);
                    ReportTable.requestFocus();
                    return false;
               }
               if(S2.length()>60)
               {
                    JOptionPane.showMessageDialog(null,"Name Must be Below 60 Characters","Error",JOptionPane.ERROR_MESSAGE);
                    ReportTable.requestFocus();
                    return false;
               }

               try
               {
                    double d1=Double.parseDouble(S3);
                    double d2=Double.parseDouble(S4);
                    double d3=Double.parseDouble(S5);                    
               }
               catch(Exception ex)
               {
                    JOptionPane.showMessageDialog(null,"Invalid Number","Error",JOptionPane.ERROR_MESSAGE);
                    ReportTable.requestFocus();
                    return false;
               }
               if((SValue1.length()>12) || (SValue2.length()>12) || (SValue3.length()>12))
               {
                    JOptionPane.showMessageDialog(null,"Invalid Quantity","Error",JOptionPane.ERROR_MESSAGE);
                    ReportTable.requestFocus();
                    return false;
               }
          }
          return true;
     }
     public void setData(String SGINo)
     {
          String SGIDate      = "";
          String SSupName     = "";
          String SSupCode     = "";
          String SInwType     = "";
          String SInvNo       = "";
          String SInvDate     = "";
          String SDCNo        = "";
          String SDCDate      = "";
          String SInwMode     = "";
          String SInwModeCode = "";
          String SRemarks     = "";

          Vector VMCode       = new Vector();
          Vector VMName       = new Vector();
          Vector VInvQty      = new Vector();
          Vector VGateQty     = new Vector();
          Vector VUnmeasure   = new Vector();

          String QS = "";

          QS = " Select GateInward.GIDate,"+SSupTable+".Name,GateInward.Sup_Code, "+
               " InwType.InwName,GateInward.InvNo,GateInward.InvDate, "+
               " GateInward.DcNo,GateInward.DcDate,InwardMode.Name, "+
               " GateInward.ModeCode,GateInward.Remarks,GateInward.Item_Code, "+
               " decode(trim(GateInward.Item_Code),'',GateInward.Item_Name,InvItems.Item_Name) as Item_Name,GateInward.SupQty,GateInward.GateQty, "+
               " GateInward.UnmeasuredQty "+
               " From GateInward "+
               " Inner Join "+SSupTable+" on GateInward.Sup_Code = "+SSupTable+".Ac_Code "+
               " Inner Join InwType on GateInward.InwNo = InwType.InwNo "+
               " Inner Join InwardMode on GateInward.ModeCode = InwardMode.Code "+
               " Left Join InvItems on GateInward.Item_Code = InvItems.Item_Code "+
               " Where GateInward.ModiStatus=0 and GateInward.MillCode="+iMillCode+" and GateInward.GINo="+SGINo;

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat          =  theConnection.createStatement();
               ResultSet res    = stat.executeQuery(QS);
               while(res.next())
               {
                    SGIDate      = common.parseDate(res.getString(1));
                    SSupName     = common.parseNull(res.getString(2));
                    SSupCode     = common.parseNull(res.getString(3));
                    SInwType     = common.parseNull(res.getString(4)).toUpperCase();
                    SInvNo       = common.parseNull(res.getString(5));
                    SInvDate     = common.parseDate(res.getString(6));
                    SDCNo        = common.parseNull(res.getString(7));
                    SDCDate      = common.parseDate(res.getString(8));
                    SInwMode     = common.parseNull(res.getString(9));
                    SInwModeCode = common.parseNull(res.getString(10));
                    SRemarks     = common.parseNull(res.getString(11));

                    VMCode.addElement(common.parseNull(res.getString(12)));
                    VMName.addElement(common.parseNull(res.getString(13)));
                    VInvQty.addElement(common.parseNull(res.getString(14)));
                    VGateQty.addElement(common.parseNull(res.getString(15)));
                    VUnmeasure.addElement(common.parseNull(res.getString(16)));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

          LGateNo.setText(SGINo);
          TGateDate.fromString(SGIDate);
          JCSupplier.setSelectedItem(SSupName);
          JCInward.setSelectedItem(SInwType);
          TInvNo.setText(SInvNo);
          TInvDate.fromString(SInvDate);
          TDCNo.setText(SDCNo);
          TDCDate.fromString(SDCDate);
          InwMode.setText(SInwMode);
          InwMode.TCode.setText(SInwModeCode);
          TRemarks.setText(SRemarks);

          setMiddlePanel(VMCode,VMName,VInvQty,VGateQty,VUnmeasure);
     }

     public void setMiddlePanel(Vector VMCode,Vector VMName,Vector VInvQty,Vector VGateQty,Vector VUnmeasure)
     {
            try
            {
                 MiddlePanel.removeAll();
            }
            catch(Exception ex){}

            RowData     = new Object[VMCode.size()][ColumnData.length];
            for(int i=0;i<RowData.length;i++)
            {
                  RowData[i][0] = (String)VMCode.elementAt(i);
                  RowData[i][1] = (String)VMName.elementAt(i);
                  RowData[i][2] = (String)VInvQty.elementAt(i);
                  RowData[i][3] = (String)VGateQty.elementAt(i);
                  RowData[i][4] = (String)VUnmeasure.elementAt(i);
            }

            GateModel   = new GateTableModel(RowData,ColumnData,ColumnType);

            ReportTable = new JTable(GateModel);

            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
            cellRenderer.setHorizontalAlignment(JLabel.RIGHT);
   
            for (int col=0;col<ReportTable.getColumnCount();col++)
            {
                  if(ColumnType[col]=="N" || ColumnType[col]=="B")
                       ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
            }

            MiddlePanel.add(new JScrollPane(ReportTable));
            getContentPane().add("Center",MiddlePanel);
            if(bflag)
            {
                 JCSupplier.addItemListener(new ItemList(this));
                 ReportTable.addMouseListener(new MouseList());
            }
            ReportTable.addKeyListener(new KeyList());
     }

     public void setMiddlePanel(Vector VSelectedCode,Vector VSelectedName)
     {
            try
            {
                 MiddlePanel.removeAll();
            }
            catch(Exception ex){}

            RowData     = new Object[VSelectedCode.size()][ColumnData.length];

            for(int i=0;i<RowData.length;i++)
            {
                  RowData[i][0] = (String)VSelectedCode.elementAt(i);
                  RowData[i][1] = (String)VSelectedName.elementAt(i);
                  RowData[i][2] = "";
                  RowData[i][3] = "";
                  RowData[i][4] = "";
            }
            GateModel   = new GateTableModel(RowData,ColumnData,ColumnType);
            ReportTable = new JTable(GateModel);

            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
            cellRenderer.setHorizontalAlignment(JLabel.RIGHT);
   
            for (int col=0;col<ReportTable.getColumnCount();col++)
            {
                  if(ColumnType[col]=="N" || ColumnType[col]=="B")
                       ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
            }

            MiddlePanel.add(new JScrollPane(ReportTable));
            getContentPane().add("Center",MiddlePanel);

            if(bflag)
            {
                 ReportTable.addMouseListener(new MouseList());
            }
            ReportTable.addKeyListener(new KeyList());
     }

     public class KeyList extends KeyAdapter
     {
               public void keyPressed(KeyEvent ke)
               {
                    if (ke.getKeyCode()==KeyEvent.VK_F3)
                    {
			 int iSeleRow = ReportTable.getSelectedRow();
			 ItemSearchList itemsearchlist = new ItemSearchList(iSeleRow,ReportTable,GateModel,iMillCode,SItemTable);	
                    }
               }
     }

     public class MouseList extends MouseAdapter
     {
               public void mouseClicked(MouseEvent me)
               {
                    if (me.getClickCount()==2)
                    {
                         String SSupCode = (String)VSupCode.elementAt(JCSupplier.getSelectedIndex());
                         getPendingMaterials(SSupCode);
                         if(VPendName.size()==0)
                         {
                                JOptionPane.showMessageDialog(null,"No Materials are Pending","Information",JOptionPane.INFORMATION_MESSAGE);
                                return;
                         }
                         PendingMaterialPicker MP = new PendingMaterialPicker(DeskTop,VPendCode,VPendName,ReportTable,GateModel);
                         try
                         {
                              DeskTop.add(MP);
                              DeskTop.repaint();
                              MP.setSelected(true);
                              DeskTop.updateUI();
                              MP.show();
                              MP.BrowList.requestFocus();
                         }
                         catch(java.beans.PropertyVetoException ex){}
                    }
               }
     }
     public void getPendingMaterials(String SSupCode)
     {
          VPendName = new Vector();
          VPendCode = new Vector();

          String QS = "Select PurchaseOrder.Item_Code,InvItems.Item_Name,sum(PurchaseOrder.Qty-PurchaseOrder.InvQty) as Pending "+
                      "From PurchaseOrder Inner Join InvItems On InvItems.Item_Code = PurchaseOrder.Item_Code "+
                      "Group By PurchaseOrder.Item_Code,InvItems.Item_Name,PurchaseOrder.Sup_Code,PurchaseOrder.MillCode "+
                      "Having PurchaseOrder.Sup_Code = '"+SSupCode+"' and PurchaseOrder.MillCode="+iMillCode+
                      "Order By 2";

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       theStatement  =  theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    double dPending = common.toDouble(theResult.getString(3));
                    if(dPending > 0)
                    {
                         VPendCode.addElement(theResult.getString(1));
                         VPendName.addElement(theResult.getString(2));
                    }
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setDataIntoVector()
     {
          VInward     = new Vector();
          VInwardCode = new Vector();
          VSupName    = new Vector();
          VSupCode    = new Vector();

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       stat          =  theConnection.createStatement();
               ResultSet result1 = stat.executeQuery("Select InwName,InwNo From InwType Order By 1");
               while(result1.next())
               {
                    VInward.addElement((common.parseNull(result1.getString(1))).toUpperCase());
                    VInwardCode.addElement(common.parseNull(result1.getString(2)));
               }
               result1.close();
               ResultSet result2 = stat.executeQuery("Select Name,Ac_code From "+SSupTable+" Order By 1");
               while(result2.next())
               {
                    VSupName.addElement(common.parseNull(result2.getString(1)));
                    VSupCode.addElement(common.parseNull(result2.getString(2)));
               }
               result2.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }


}
