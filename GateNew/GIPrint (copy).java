package GateNew;
import util.*;
import guiutil.*;
import java.io.*;
import java.util.*;
import java.sql.*;
import jdbc.*;

public class GIPrint
{
     FileWriter FW;
     String SGINo,SGIDate,SSupName,SInwType,SInvNo,SInvDate,SDCNo,SDCDate,SInwMode,SRemarks,SCategory,STime;
     int iMillCode;
     String SMillName;

     Vector VMCode,VMName,VInvQty,VGateQty,VUnmeasure;

     Common common = new Common();
     int Lctr      = 100;
     int Pctr      = 0;

     GIPrint(FileWriter FW,String SGINo,String SGIDate,String SSupName,String SInwType,String SInvNo,String SInvDate,String SDCNo,String SDCDate,String SInwMode,String SRemarks,String SCategory,String STime,int iMillCode,String SMillName)
     {
          this.FW         = FW;
          this.SGINo      = SGINo;
          this.SGIDate    = SGIDate;
          this.SSupName   = SSupName;
          this.SInwType   = SInwType;
          this.SInvNo     = SInvNo;
          this.SInvDate   = SInvDate;
          this.SDCNo      = SDCNo;
          this.SDCDate    = SDCDate;
          this.SInwMode   = SInwMode;
          this.SRemarks   = SRemarks;
          this.SCategory  = SCategory;
          this.STime      = STime;
          this.iMillCode  = iMillCode;
          this.SMillName  = SMillName;

          setDataIntoVector();
          setAdvHead();
          setAdvBody();
          setAdvFoot(0);
     }
   
     public void setAdvHead()
     {
          if(Lctr < 50)
            return;

          if(Pctr > 0)
            setAdvFoot(1);

          Pctr++;

               String Strl1   = "     "+SMillName+"";
               String Strl1a  = " ";
               String Strl2   = "|---------------------------------------------------------------------------------------------------|";
               String Strl2a  = "|                                                                                                   |";
               String Strl3   = "|                                  GATE INWARD OF STORES AND SPARES                                 |";
               String Strl3a  = "|                                                                                                   |";
               String Strl4   = "|---------------------------------------------------------------------------------------------------|";
               String Strl4a  = "|                                                         |                                         |";
               String Strl5   = "| Party Name   : "+common.Pad(SSupName,40)+" | Gate Inward Number : "+common.Pad(SGINo,18)+" |";
               String Strl6   = "|                                                         |             Date   : "+common.Pad(SGIDate,18)+" |";
               String Strl7   = "|                                                         |             Time   : "+common.Pad(STime,18)+" |";
               String Strl7a  = "|                                                         |                                         |";
               String Strl8   = "|---------------------------------------------------------------------------------------------------|";
               String Strl8a  = "|                                                         |                                         |";
               String Strl9   = "| Invoice Number : "+common.Pad(SInvNo,38)+" | D.C. Number : "+common.Pad(SDCNo,25)+" |";
               String Strl10  = "| Invoice Date   : "+common.Pad(SInvDate,38)+" | D.C. Date   : "+common.Pad(SDCDate,25)+" |";
               String Strl10a = "|                                                         |                                         |";
               String Strl11  = "|---------------------------------------------------------------------------------------------------|";
               String Strl11a = "|                                                         |                                         |";
               String Strl12  = "| Inward Type    : "+common.Pad(SInwType,38)+" | Inward Mode : "+common.Pad(SInwMode,25)+" |";
               String Strl12a = "|                                                         |                                         |";
               String Strl13  = "|---------------------------------------------------------------------------------------------------|";
               String Strl13a = "|                                                                                                   |";
               String Strl14  = "| Category       : "+common.Pad(SCategory,80)+" |";
               String Strl14a = "|                                                                                                   |";
               String Strl15  = "|---------------------------------------------------------------------------------------------------|";
               String Strl15a = "|     |           |                                          |            |            |            |";
               String Strl16  = "| Sl. | Material  |             Material Name                |  Invoice   |  Measured  | Unmeasured |";
               String Strl17  = "| No. |   Code    |                                          |  Quantity  |  Qty @Gate |  Quantity  |";
               String Strl17a = "|     |           |                                          |            |            |            |";
               String Strl18  = "|---------------------------------------------------------------------------------------------------|";
               
                                                                               
               try
               {
                    FW.write(Strl1+"\n");
                    FW.write(Strl1a+"\n");
                    FW.write(Strl2+"\n");
                    FW.write(Strl2a+"\n");
                    FW.write(Strl3+"\n");
                    FW.write(Strl3a+"\n");
                    FW.write(Strl4+"\n");
                    FW.write(Strl4a+"\n");
                    FW.write(Strl5+"\n");
                    FW.write(Strl6+"\n");
                    FW.write(Strl7+"\n");
                    FW.write(Strl7a+"\n");
                    FW.write(Strl8+"\n");
                    FW.write(Strl8a+"\n");
                    FW.write(Strl9+"\n");
                    FW.write(Strl10+"\n");
                    FW.write(Strl10a+"\n");
                    FW.write(Strl11+"\n");
                    FW.write(Strl11a+"\n");
                    FW.write(Strl12+"\n");
                    FW.write(Strl12a+"\n");
                    FW.write(Strl13+"\n");
                    FW.write(Strl13a+"\n");
                    FW.write(Strl14+"\n");
                    FW.write(Strl14a+"\n");
                    FW.write(Strl15+"\n");
                    FW.write(Strl15a+"\n");
                    FW.write(Strl16+"\n");
                    FW.write(Strl17+"\n");
                    FW.write(Strl17a+"\n");
                    FW.write(Strl18+"\n");
                    Lctr = 31;
               }
               catch(Exception ex)
               {
               }
     }

     public void setAdvBody()
     {
          int ctr=0;
          for(int i=0;i<VMCode.size();i++)
          {
               setAdvHead();
               String SMCode     = common.Pad((String)VMCode.elementAt(i),9);
               String SMName     = common.Pad((String)VMName.elementAt(i),40);
               String SInvQty    = common.Rad((String)VInvQty.elementAt(i),10);
               String SGateQty   = common.Rad((String)VGateQty.elementAt(i),10);
               String SUnmeasure = common.Rad((String)VUnmeasure.elementAt(i),10);

               try
               {
                    String Strl = "| "+common.Pad(String.valueOf(ctr+1),3)+" | "+
                                  SMCode+" | "+SMName+" | "+
                                  SInvQty+" | "+SGateQty+" | "+SUnmeasure+" |";

                    FW.write(Strl+"\n");
                    Lctr = Lctr++;
                    ctr++;
               }
               catch(Exception ex){}
          }
     }
     public void setAdvFoot(int iSig)
     {

          String Strl1  = "|---------------------------------------------------------------------------------------------------|";
          String Strl2  = "|                                                                                                   |";
          String Strl3  = "| Remarks : "+common.Pad(SRemarks,87)+" |";
          String Strl4  = "|                                                                                                   |";
          String Strl5  = "|                                                                                                   |";
          String Strl6  = "|                                                                                                   |";
          String Strl7  = "|                                                                                                   |";
          String Strl8  = "|                      Security                                    Security Officer                 |";
          String Strl9  = "|                                                                                                   |";
          String Strl10 = "----------------------------------------------------------------------------------------------------|";


          try
          {
               FW.write( Strl1+"\n");       
               FW.write( Strl2+"\n");       
               FW.write( Strl3+"\n");
               FW.write( Strl4+"\n");       
               FW.write( Strl5+"\n");       
               FW.write( Strl6+"\n");       
               FW.write( Strl7+"\n");       
               FW.write( Strl8+"\n");       
               FW.write( Strl9+"\n");       
               FW.write( Strl10+"\n");       
          }
          catch(Exception ex){}


          String Strl11 = "";
          String Strl12 = "";

          if(iSig==0)
          {
             String SDateTime = common.getServerDateTime2();
             Strl11= "< Report Taken on "+SDateTime+" >";
             Strl12= "< End of Report >";
             try
             {
                  FW.write( Strl11+"\n");       
                  FW.write( Strl12+"\n");       
             }
             catch(Exception ex){}

          }
          else
          {
             Strl11= "(Continued on Next Page) ";
             try
             {
                  FW.write( Strl11+"\n");       
             }
             catch(Exception ex){}
          }
     }

     public void setDataIntoVector()
     {

          VMCode       = new Vector();
          VMName       = new Vector();
          VInvQty      = new Vector();
          VGateQty     = new Vector();
          VUnmeasure   = new Vector();

          String QS = "";

          if(SCategory.equals("With P.O."))
          {
               QS = " Select GateInward.Item_Code,InvItems.Item_Name,"+
                    " GateInward.SupQty,GateInward.GateQty,GateInward.UnmeasuredQty "+
                    " From GateInward "+
                    " Inner Join InvItems on GateInward.Item_Code = InvItems.Item_Code "+
                    " Where GateInward.ModiStatus=0 and GateInward.MillCode="+iMillCode+" and GateInward.GINo="+SGINo;
          }
          else
          {
               QS = " Select GateInward.Item_Code,GateInward.Item_Name,"+
                    " GateInward.SupQty,GateInward.GateQty,GateInward.UnmeasuredQty "+
                    " From GateInward "+
                    " Where GateInward.ModiStatus=0 and GateInward.MillCode="+iMillCode+" and GateInward.GINo="+SGINo;
          }

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       theStatement  =  theConnection.createStatement();
               ResultSet res    = theStatement.executeQuery(QS);
               while(res.next())
               {
                    VMCode.addElement(common.parseNull(res.getString(1)));
                    VMName.addElement(common.parseNull(res.getString(2)));
                    VInvQty.addElement(common.parseNull(res.getString(3)));
                    VGateQty.addElement(common.parseNull(res.getString(4)));
                    VUnmeasure.addElement(common.parseNull(res.getString(5)));
               }
               res.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }


     }

}
