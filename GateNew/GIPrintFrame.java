package GateNew;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import util.*;
import guiutil.*;
import jdbc.*;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;

public class GIPrintFrame extends JInternalFrame
{
     JPanel    TopPanel;
     JPanel    BottomPanel;

     String    SStDate,SEnDate;
     JComboBox JCOrder,JCFilter;
     JButton   BApply,BExit,BPrint,BCreatePDF;
     MyTextField TFile;
    
     DateField TStDate;
     DateField TEnDate;
    
     Object RowData[][];
     String ColumnData[] = {"Print","G.I. No","Date","Supplier","Invoice No","Inv Date","DC No","DC Date","Inward Type","Inward Mode","Category","Remarks","EntryStatus"};
     String ColumnType[] = {"B"    ,"N"      ,"S"   ,"S"       ,"S"         ,"S"       ,"S"    ,"S"      ,"S"          ,"S"          ,"S"       ,"S"      ,"S"          };
                                 
     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;
     int iUserCode,iAuthCode,iMillCode;
     String SYearCode;
     String SItemTable,SSupTable,SMillName;

     Common common = new Common();
     PrinterComm              Printer;
     File                     file;


     Vector VGINo,VGIDate,VSupName,VInvNo,VInvDate,VDCNo,VDCDate,VInwType,VInwMode,VCategory,VRemarks,VTime,VEntryStatus;

     TabReport tabreport;
     Document document;
      String PDFFile = common.getPrintPath()+"/GIGate.pdf";
	  //String PDFFile = "d:GIGate.pdf";

     GIPrintFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iAuthCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable,String SMillName)
     {
         super("Stores Gate Inward List During a Period");

         this.DeskTop    = DeskTop;
         this.SPanel     = SPanel;
         this.VCode      = VCode;
         this.VName      = VName;
         this.iUserCode  = iUserCode;
         this.iAuthCode  = iAuthCode;
         this.iMillCode  = iMillCode;
         this.SYearCode  = SYearCode;
         this.SItemTable = SItemTable;
         this.SSupTable  = SSupTable;
         this.SMillName  = SMillName;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     public void createComponents()
     {
         TopPanel    = new JPanel();
         BottomPanel = new JPanel();

         TStDate  = new DateField();
         TEnDate  = new DateField();
         BApply   = new JButton("Apply");
         BPrint   = new JButton("Print");
         BExit    = new JButton("Exit");
         BCreatePDF=new JButton("Create PDF");

         JCOrder  = new JComboBox();
         JCFilter = new JComboBox();

         TFile       = new MyTextField(25);

         TFile.setText("1.prn");

         TStDate.setTodayDate();
         TEnDate.setTodayDate();

         BExit.setMnemonic('X');
         BApply.setMnemonic('A');
         BPrint.setMnemonic('P');
         BPrint.setEnabled(false);
         BExit.setEnabled(false);
         TFile.setEditable(false);
         Printer             = new PrinterComm();
     }

     public void setLayouts()
     {
         TopPanel.setLayout(new GridLayout(1,8));

         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,790,500);
     }

     public void addComponents()
     {
         JCOrder.addItem("G.I. No");
         JCOrder.addItem("Supplierwise");
         JCOrder.addItem("Inward Typewise");
         JCOrder.addItem("Inward Modewise");

         JCFilter.addItem("All");
         JCFilter.addItem("With Purchase Order");
         JCFilter.addItem("Without Purchase Order");

         TopPanel.add(new JLabel("Sorted On"));
         TopPanel.add(JCOrder);

         TopPanel.add(new JLabel("List Only"));
         TopPanel.add(JCFilter);

         TopPanel.add(new JLabel("Period"));
         TopPanel.add(TStDate);
         TopPanel.add(TEnDate);
         TopPanel.add(BApply);

         BottomPanel.add(BPrint);
         BottomPanel.add(BExit);
         BottomPanel.add(TFile);
         BottomPanel.add(BCreatePDF);

         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
         BApply.addActionListener(new ApplyList());
         BExit.addActionListener(new exitList());
         BPrint.addActionListener(new PrintList());
         BCreatePDF.addActionListener(new ActList());
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BCreatePDF)
               {
                    
                    createPDFFile();
                    
                    try{
                File theFile   = new File(PDFFile);
                Desktop        . getDesktop() . open(theFile);
                  

                  
               }catch(Exception ex){}
               }
          }
    }

   private void createPDFFile() {
      Boolean bFlag; 

            try{
               document = new Document(PageSize.A4);
               PdfWriter.getInstance(document, new FileOutputStream(PDFFile));
               document.open();
                    for(int i=0;i<RowData.length;i++)
                    {
                         Boolean BValue = (Boolean)RowData[i][0];
                         if(BValue.booleanValue())
                         {
                                   String SGINo     = (String)VGINo.elementAt(i);
                                   String SGIDate   = (String)VGIDate.elementAt(i);
                                   String SSupName  = (String)VSupName.elementAt(i);
                                   String SInwType  = (String)VInwType.elementAt(i);
                                   String SInvNo    = (String)VInvNo.elementAt(i);
                                   String SInvDate  = (String)VInvDate.elementAt(i);
                                   String SDCNo     = (String)VDCNo.elementAt(i);
                                   String SDCDate   = (String)VDCDate.elementAt(i);
                                   String SInwMode  = (String)VInwMode.elementAt(i);
                                   String SRemarks  = (String)VRemarks.elementAt(i);
                                   String SCategory = (String)VCategory.elementAt(i);
                                   String STime     = (String)VTime.elementAt(i);

                                   new GIPrint(document,SGINo,SGIDate,SSupName,SInwType,SInvNo,SInvDate,SDCNo,SDCDate,SInwMode,SRemarks,SCategory,STime,iMillCode,SMillName);
                         }
                     
                    } document.close();

       }catch(Exception ex){}

   }
     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    if(!isSelect())
                    {
                          JOptionPane.showMessageDialog(null,"No Row Selected","Error",JOptionPane.ERROR_MESSAGE);
                          tabreport.ReportTable.requestFocus();
                          return;
                    }

                    BPrint.setEnabled(false);

                    String SFile  = TFile.getText();
                    if((SFile.trim()).length()==0)
                       SFile = "1.prn";

                    file           = new File(common.getPrintPath()+SFile);
                    FileWriter FW = new FileWriter(file);
                    for(int i=0;i<RowData.length;i++)
                    {
                         Boolean BValue = (Boolean)RowData[i][0];
                         if(BValue.booleanValue())
                         {
                                   String SGINo     = (String)VGINo.elementAt(i);
                                   String SGIDate   = (String)VGIDate.elementAt(i);
                                   String SSupName  = (String)VSupName.elementAt(i);
                                   String SInwType  = (String)VInwType.elementAt(i);
                                   String SInvNo    = (String)VInvNo.elementAt(i);
                                   String SInvDate  = (String)VInvDate.elementAt(i);
                                   String SDCNo     = (String)VDCNo.elementAt(i);
                                   String SDCDate   = (String)VDCDate.elementAt(i);
                                   String SInwMode  = (String)VInwMode.elementAt(i);
                                   String SRemarks  = (String)VRemarks.elementAt(i);
                                   String SCategory = (String)VCategory.elementAt(i);
                                   String STime     = (String)VTime.elementAt(i);

                                   new GIPrint(FW,SGINo,SGIDate,SSupName,SInwType,SInvNo,SInvDate,SDCNo,SDCDate,SInwMode,SRemarks,SCategory,STime,iMillCode,SMillName);
                         }
                    }
                    FW.close();
                    removeHelpFrame();
                    Printer.CallPrinter(file);
               }
               catch(Exception ex){};
          }
     }
     private boolean isSelect()
     {

           for(int i=0;i<RowData.length;i++)
           {
                 Boolean bValue = (Boolean)RowData[i][0];
                 if(!bValue.booleanValue())
                       continue;
                 return true;
           }
           return false;
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     private class exitList implements ActionListener
     {
         public void actionPerformed(ActionEvent ae)
         {
               setVisible(false);
         }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setTabReport();
               if(RowData.length>0)
               {
                    BPrint.setEnabled(true);
               }
               BExit.setEnabled(true);
               TFile.setEditable(true);
               tabreport.ReportTable.requestFocus();
          }
     }
     public void setTabReport()
     {
          setDataIntoVector();
          setRowData();
          try
          {
             getContentPane().remove(tabreport);
          }
          catch(Exception ex){}

          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             getContentPane().add(tabreport,BorderLayout.CENTER);
             setSelected(true);
             DeskTop.repaint();
             DeskTop.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }

     }

     public void setDataIntoVector()
     {

          VGINo        = new Vector();
          VGIDate      = new Vector();
          VSupName     = new Vector();
          VInvNo       = new Vector();
          VInvDate     = new Vector();
          VDCNo        = new Vector();
          VDCDate      = new Vector();
          VInwType     = new Vector();
          VInwMode     = new Vector();
          VCategory    = new Vector();
          VRemarks     = new Vector();
          VTime        = new Vector();
          VEntryStatus = new Vector();

          SStDate = TStDate.toString();
          SEnDate = TEnDate.toString();
          String StDate  = TStDate.toNormal();
          String EnDate  = TEnDate.toNormal();
          try
          {

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
 
               String QString = getQString(StDate,EnDate);
               ResultSet res  = theStatement.executeQuery(QString);
               while (res.next())
               {
  
                    String SCategory="";
                    String str1 = common.parseNull(res.getString(10));
                    if(str1.equals("0"))
                    {
                        SCategory = "Without P.O.";
                    }
                    else
                    {
                        SCategory = "With P.O.";
                    }
    
                    String STime = "";
                    String str2 = common.parseNull(res.getString(12));
                    STime = str2.substring(11,19);
    
                    if(JCFilter.getSelectedIndex()==1)
                        if(str1.equals("0"))
                           continue;
    
                    if(JCFilter.getSelectedIndex()==2)
                        if(str1.equals("1"))
                           continue;
    
                    VGINo       .addElement(common.parseNull(res.getString(1)));
                    VGIDate     .addElement(common.parseDate(res.getString(2)));
                    VSupName    .addElement(common.parseNull(res.getString(3)));
                    VInvNo      .addElement(common.parseNull(res.getString(4)));
                    VInvDate    .addElement(common.parseDate(res.getString(5)));
                    VDCNo       .addElement(common.parseNull(res.getString(6)));
                    VDCDate     .addElement(common.parseDate(res.getString(7)));
                    VInwType    .addElement(common.parseNull(res.getString(8)));
                    VInwMode    .addElement(common.parseNull(res.getString(9)));
                    VCategory   .addElement(SCategory);
                    VRemarks    .addElement(common.parseNull(res.getString(11)));
                    VTime       .addElement(STime);
                    VEntryStatus.addElement(common.parseNull(res.getString(13)));
               }
               res.close();
               theStatement.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VGIDate.size()][ColumnData.length];
          for(int i=0;i<VGIDate.size();i++)
          {
               RowData[i][0]  = new Boolean(false);
               RowData[i][1]  = (String)VGINo       .elementAt(i);
               RowData[i][2]  = (String)VGIDate     .elementAt(i);
               RowData[i][3]  = (String)VSupName    .elementAt(i);
               RowData[i][4]  = (String)VInvNo      .elementAt(i);
               RowData[i][5]  = (String)VInvDate    .elementAt(i);
               RowData[i][6]  = (String)VDCNo       .elementAt(i);
               RowData[i][7]  = (String)VDCDate     .elementAt(i);
               RowData[i][8]  = (String)VInwType    .elementAt(i);
               RowData[i][9]  = (String)VInwMode    .elementAt(i);
               RowData[i][10] = (String)VCategory   .elementAt(i);
               RowData[i][11] = (String)VRemarks    .elementAt(i);
               RowData[i][12] = (String)VEntryStatus.elementAt(i);
          }  
     }    

     public String getQString(String StDate,String EnDate)
     {
          // Category -    0 - without P.O.    1 - with P.O.

          String QString = " SELECT GateInward.GINo, GateInward.GIDate, "+SSupTable+".Name, GateInward.InvNo, GateInward.InvDate, GateInward.DcNo, GateInward.DcDate, InwType.InwName, InwardMode.Name, nvl(GateInward.EntryStatus,0) as Category, GateInward.Remarks, GateInward.UserTime,decode(GateInward.EntryStatus,'1','Direct GRN','') as EntryStatus "+
                           " FROM ((GateInward INNER JOIN "+SSupTable+" ON GateInward.Sup_Code = "+SSupTable+".Ac_Code) INNER JOIN InwType ON GateInward.InwNo = InwType.InwNo) INNER JOIN InwardMode ON GateInward.ModeCode = InwardMode.Code "+
                           " Group by GateInward.GINo, GateInward.GIDate, "+SSupTable+".Name, GateInward.InvNo, GateInward.InvDate, GateInward.DcNo, GateInward.DcDate, InwType.InwName, InwardMode.Name,nvl(GateInward.EntryStatus,0),GateInward.Status,GateInward.GrnNo,GateInward.ModiStatus,GateInward.Authentication,GateInward.Remarks,GateInward.UserTime,GateInward.MillCode,decode(GateInward.EntryStatus,'1','Direct GRN','') "+
                           " Having GateInward.GIDate >= '"+StDate+"' and GateInward.GIDate <='"+EnDate+"' and GateInward.MillCode="+iMillCode+
                           " and GateInward.ModiStatus=0 and GateInward.Status=0 and GateInward.Authentication=1 ";

          if(JCOrder.getSelectedIndex() == 0)
               QString = QString+" Order By GINo,GIDate";
          if(JCOrder.getSelectedIndex() == 1)
               QString = QString+" Order By Name,GIDate";
          if(JCOrder.getSelectedIndex() == 2)
               QString = QString+" Order By InwName,GIDate";
          if(JCOrder.getSelectedIndex() == 3)
               QString = QString+" Order By Name,GIDate";


          return QString;
     }

}
