package GateNew;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import util.*;
import guiutil.*;
import jdbc.*;

public class GISFrame extends JInternalFrame
{
     JButton         BOk,BCancel,BPending,BSupplier;
     MyComboBox      JCInward;
     MyLabel         LGateNo;
     DateField       TGateDate;//,TInvDate,TDCDate;
	 AttDateField	          TInvDate, TDCDate;
     MyTextField     TInvNo,TDCNo;
     JTextField      TSupCode;
     GIMiddlePanel   MiddlePanel;
     JPanel          TopPanel,BottomPanel;
     InwardModeModel InwMode;
     MyTextField     TRemarks;

     Vector VInward,VInwardCode;

     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel  SPanel;
     int iUserCode,iAuthCode,iMillCode;
     String SYearCode;
     String SItemTable,SSupTable;

     String SGateNo="";

     Common common   = new Common();

     int iInvGINo,iCottonGINo,iAgnGINo,iFibreGINo,iYarnGINo,iPurcGINo;
     int iWebGINo;

     Connection               theMConnection = null;

     boolean                  bComflag       = true;
          
     GISFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iAuthCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable)
     {
          super("Gate Inward of Stores & Spares Materials");

          this.DeskTop    = DeskTop;
          this.VCode      = VCode;
          this.VName      = VName;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iAuthCode  = iAuthCode;
          this.iMillCode  = iMillCode;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;

          ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                         theMConnection = oraConnection.getConnection();

          setDataIntoVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }
     public void createComponents()
     {
          BOk        = new JButton("Save");
          BCancel    = new JButton("Exit");
          BPending   = new JButton("Pending Materials");
          BSupplier  = new JButton("Supplier");

          TGateDate   = new DateField();
          //TInvDate    = new DateField();
         // TDCDate     = new DateField();
		 
		  TInvDate    = new AttDateField(10);
          TDCDate     = new AttDateField(10);
          LGateNo     = new MyLabel("");
          TInvNo      = new MyTextField(25);
          TDCNo       = new MyTextField(15);
          TSupCode    = new JTextField();
          JCInward    = new MyComboBox(VInward);
          MiddlePanel = new GIMiddlePanel(DeskTop,VCode,VName,SPanel);
          InwMode     = new InwardModeModel(iUserCode,iAuthCode,iMillCode);
          TRemarks    = new MyTextField(50);
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          TGateDate.setTodayDate();
          TGateDate.setEditable(false);
          JCInward.setSelectedIndex(0);
          JCInward.setEnabled(false);

          LGateNo.setText("To be determined");

          BOk.setMnemonic('S');
          BCancel.setMnemonic('E');
          BPending.setMnemonic('P');
          BSupplier.setMnemonic('U');
          InwMode.setMnemonic('M');
     }
     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);

          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(6,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());

          BPending.setBorder(new javax.swing.border.BevelBorder(0));
          BPending.setBackground(new Color(128,128,255));
          BPending.setForeground(Color.RED);

          BSupplier.setBorder(new javax.swing.border.BevelBorder(0));
          BSupplier.setBackground(new Color(128,128,255));
          BSupplier.setForeground(Color.RED);

     }
     public void addComponents()
     {

          TopPanel.add(new JLabel("Supplier"));
          TopPanel.add(BSupplier);

          TopPanel.add(new JLabel("Inward Type"));
          TopPanel.add(JCInward);

          TopPanel.add(new JLabel("Gate Inward No"));
          TopPanel.add(LGateNo);

          TopPanel.add(new JLabel("Gate Inward Date"));
          TopPanel.add(TGateDate);

          TopPanel.add(new JLabel("Invoice No"));
          TopPanel.add(TInvNo);

          TopPanel.add(new JLabel("Invoice Date"));
          TopPanel.add(TInvDate);

          TopPanel.add(new JLabel("DC No"));
          TopPanel.add(TDCNo);

          TopPanel.add(new JLabel("DC Date"));
          TopPanel.add(TDCDate);

          TopPanel.add(new JLabel("Select Inward Mode"));
          TopPanel.add(InwMode);

          TopPanel.add(new JLabel("Remarks"));
          TopPanel.add(TRemarks);

          TopPanel.add(new JLabel("Fetch Expected Materials"));
          TopPanel.add(BPending);

          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
           BOk.addActionListener(new ActList());
           BCancel.addActionListener(new ActList());
		   TInvDate.theDate.addFocusListener(new FocusList());
		   TDCDate.theDate.addFocusListener(new FocusList());
           BPending.addActionListener(new PendingList(DeskTop,MiddlePanel,TSupCode,BSupplier,false,iMillCode));
           BSupplier.addActionListener(new SupplierSearch(DeskTop,TSupCode,SSupTable));
     }
	 
	 
	  private class FocusList extends FocusAdapter
    {

        public void focusLost(FocusEvent fe)
        {
            
			String SMsg = getValidDaysCount();
			if(SMsg.length() > 0)
			{
				JOptionPane.showMessageDialog(null,SMsg,"Dear user,",JOptionPane.ERROR_MESSAGE);
				BOk.setEnabled(false);
			}
			else	
			BOk.setEnabled(true);
        }
    }

	
	private String getValidDaysCount()
	{
		String SMsg="";
		try
		{
			String SPartyCode = TSupCode.getText();

			int iGRNDate 		= common.toInt(TGateDate.toNormal());
			int iDcDate 		= common.toInt(TDCDate.toNormal());
			int iInvoiceDate 	= common.toInt(TInvDate.toNormal());
			SMsg		 		= common.getInvoiceValidDays(iGRNDate,iDcDate,iInvoiceDate,SPartyCode);

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			System.out.println("ex:"+ex);
		}
		return SMsg;
	}
	 
     public class ActList implements ActionListener
     {
            public void actionPerformed(ActionEvent ae)
            {
                    if(ae.getSource()==BOk)
                    {
                        BOk.setEnabled(false);
                        boolean bSig = false;
                        try
                        {
                              bSig = allOk();
                        }
                        catch(Exception ex)
                        {
                              System.out.println(ex);
                        }
                        if(bSig)
                        {
                             setGateNo();
                             insertGISDetails();
					    getACommit();
                        }
                        else
                        {
                              BOk.setEnabled(true);
                        }
                    }     
                    if(ae.getSource()==BCancel)
                    {
                        removeHelpFrame();
                    }     
          }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
     public void insertGISDetails()
     {
          
          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
          String STime = common.getServerDateTime();
          String SRemarks = ((TRemarks.getText()).trim()).toUpperCase();

          try
          {
               Statement       theStatement  =  theMConnection.createStatement();
			   
			   String SDCDate = TDCDate.toNormal();
			   String SINVDate = TInvDate.toNormal();
			   
               for(int i=0;i<FinalData.length;i++)
               {
                    String QS = "Insert Into GateInward (GINo,GIDate,InwNo,Sup_Code,InvNo,InvDate,DcNo,DcDate,ModeCode,Remarks,Item_Code,SupQty,GateQty,UnmeasuredQty,UserCode,UserTime,BaseCode,MillCode,id) Values(";
                    QS = QS+"0"+LGateNo.getText()+",";
                    QS = QS+"'"+TGateDate.TYear.getText()+TGateDate.TMonth.getText()+TGateDate.TDay.getText()+"',";
                    QS = QS+"0"+(String)VInwardCode.elementAt(JCInward.getSelectedIndex())+",";
                    QS = QS+"'"+TSupCode.getText()+"',";
                    QS = QS+"'"+TInvNo.getText().toUpperCase()+"',";
                   // QS = QS+"'"+TInvDate.TYear.getText()+TInvDate.TMonth.getText()+TInvDate.TDay.getText()+"',";
				    QS  = QS +SINVDate+",";
                    QS = QS+"'"+TDCNo.getText().toUpperCase()+"',";
                  //  QS = QS+"'"+TDCDate.TYear.getText()+TDCDate.TMonth.getText()+TDCDate.TDay.getText()+"',";
				    QS  = QS +SDCDate+",";
                    QS = QS+"0"+InwMode.TCode.getText()+",";
                    QS = QS+"'"+SRemarks+"',";
                    QS = QS+"'"+(String)FinalData[i][0]+"',";
                    QS = QS+"0"+common.getRound(((String)FinalData[i][2]).trim(),3)+",";
                    QS = QS+"0"+common.getRound(((String)FinalData[i][3]).trim(),3)+",";
                    QS = QS+"0"+common.getRound(((String)FinalData[i][4]).trim(),3)+",";
                    QS = QS+"0"+String.valueOf(iUserCode)+",";
                    QS = QS+"'"+STime+"',";
                    QS = QS+"0"+String.valueOf(iAuthCode)+",";
                    QS = QS+"0"+String.valueOf(iMillCode)+", gateinward_seq.nextval )";

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    theStatement.execute(QS);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex.getMessage());
               bComflag  = false;
          }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theMConnection . commit();
                    JOptionPane    . showMessageDialog(null,"The Entered Data is Saved with GI No - "+LGateNo.getText(),"Information",JOptionPane.INFORMATION_MESSAGE);
               }
               else
               {
                    theMConnection . rollback();
                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
               }
               theMConnection   . setAutoCommit(true);

               if(bComflag)
               {
                    removeHelpFrame();
               }
               else
               {
                    BOk.setEnabled(true);
               }

          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

/*     public void UpdateGINo()
     {
          String QS1          = "";

          QS1 = " Update config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1  where Id = 3";

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();

               stat      . executeUpdate(QS1);
               stat      . close();
          }catch(Exception Ex)
          {
               System.out.println("UpdateGINo method"+Ex);
               Ex.printStackTrace();
          }
     }
*/

     public boolean allOk()
     {
          String SDate        = TGateDate.toNormal();
          String SSupCode     = TSupCode.getText();
          String SInwMode     = common.parseNull(InwMode.getText().trim());
          String SCurDate     = common.getServerDate();
          int    iDateDiff    = common.toInt(common.getDateDiff(common.parseDate(SDate),SCurDate));
          String SInvNo       = common.parseNull(TInvNo.getText().trim());
          String SInvDate     = TInvDate.toNormal();
          String SDCNo        = common.parseNull(TDCNo.getText().trim());
          String SDCDate      = TDCDate.toNormal();
          int iRows=0;
		  
		  String SMsg = getValidDaysCount();
			if(SMsg.length() > 0)
			{
				JOptionPane.showMessageDialog(null,SMsg,"Dear user,",JOptionPane.ERROR_MESSAGE);
				BOk.setEnabled(false);
				return false;
			}


          if(SSupCode.length() <= 0)
          {
               JOptionPane.showMessageDialog(null,"Supplier Is Not Select","Proplem in Save Data",JOptionPane.ERROR_MESSAGE);
               return false;

          }
          if(SInwMode.equals("Mode"))
          {
               JOptionPane.showMessageDialog(null,"Mode Is Not Select","Proplem in Save Data",JOptionPane.ERROR_MESSAGE);
               return false;

          }
          if(!SDCNo.equals(""))
          {
               if(common.toInt(SDCDate) ==0)
               {
                    JOptionPane.showMessageDialog(null,"Enter Dc Date ","Proplem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(common.toInt(SDCDate)>0)
          {
               if(SDCNo.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Enter Dc No ","Proplem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(!SInvNo.equals(""))
          {
               if(common.toInt(SInvDate) ==0)
               {
                    JOptionPane.showMessageDialog(null,"Enter Invoice Date ","Proplem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(common.toInt(SInvDate)>0)
          {
               if(SInvNo.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Enter Invoice No ","Proplem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(SDCNo.equals("") && SInvNo.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Select DC No Or Invoice No","Proplem in Save Data",JOptionPane.ERROR_MESSAGE);
               return false;

          }
          if(SDate.length()== 0)
          {
               JOptionPane.showMessageDialog(null,"Enter Date ","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          try
          {
               iRows    = MiddlePanel.MiddlePanel.getRows();
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"No Materials Selected","Error",JOptionPane.ERROR_MESSAGE);
               BPending.setEnabled(true);
               BPending.requestFocus();
               return false;
          }

          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<FinalData.length;i++)
          {
               String S1 = ((String)FinalData[i][2]).trim();
               String S2 = ((String)FinalData[i][3]).trim();
               String S3 = ((String)FinalData[i][4]).trim();

               String SValue1 = common.getRound(S1,3);
               String SValue2 = common.getRound(S2,3);
               String SValue3 = common.getRound(S3,3);

               if((S1.length()==0) || (S2.length()==0) || (S3.length()==0))
               {
                    JOptionPane.showMessageDialog(null,"Quantity Fields Must be Filled","Error",JOptionPane.ERROR_MESSAGE);
                    MiddlePanel.requestFocus();
                    return false;
               }
               try
               {
                    double d1=Double.parseDouble(S1);
                    double d2=Double.parseDouble(S2);
                    double d3=Double.parseDouble(S3);
               }
               catch(Exception ex)
               {
                    JOptionPane.showMessageDialog(null,"Invalid Number","Error",JOptionPane.ERROR_MESSAGE);
                    MiddlePanel.requestFocus();
                    return false;
               }
               if((SValue1.length()>12) || (SValue2.length()>12) || (SValue3.length()>12))
               {
                    JOptionPane.showMessageDialog(null,"Invalid Quantity","Error",JOptionPane.ERROR_MESSAGE);
                    MiddlePanel.requestFocus();
                    return false;
               }

          }

          return true;
     }

/*     public void setGateNo()
     {
          String SGINo = "";

          String QS = " Select maxno from config"+iMillCode+""+SYearCode+" where id = 3";

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();
               ResultSet       result        =  stat.executeQuery(QS);
               result    . next();
               SGINo     = common.parseNull((String)result.getString(1));
               result    . close();
               stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
          SGINo  = String.valueOf(common.toInt(SGINo.trim())+1);
          LGateNo.setText(SGINo);
     }
*/

     private void setGateNo()
     {
          SGateNo="";

          String QS ="";
          try
          {
               QS = " Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 3 for update of MaxNo noWait";

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               Statement       stat =  theMConnection.createStatement();

               PreparedStatement thePrepare = theMConnection.prepareStatement(" Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 3"); 
               
               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    SGateNo = res.getString(1);    
               }
               res.close();

               thePrepare.setInt(1,common.toInt(SGateNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println("E3"+ex );
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    setGateNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
          LGateNo.setText(SGateNo);
     }


     /*public void setGateNo()
     {
          int iGINo=0;

          iInvGINo=0;
          iCottonGINo=0;
          iAgnGINo=0;
          iFibreGINo=0;
          iYarnGINo=0;
          iPurcGINo=0;
          iWebGINo=0;

          int iMaxNo=0;

          getInventoryGINo();

          if(iMillCode==1)
          {
               getWebGINo();
     
               if(iInvGINo>iWebGINo)
               {
                    iGINo = iInvGINo+1;
               }
               else
               {
                    iGINo = iWebGINo+1;
               }
          }
          else
          {
               getOtherGINo();
     
               if(iInvGINo>iMaxNo)
               {
                    iMaxNo = iInvGINo;
               }
               if(iCottonGINo>iMaxNo)
               {
                    iMaxNo = iCottonGINo;     
               }
               if(iAgnGINo>iMaxNo)
               {
                    iMaxNo = iAgnGINo;     
               }
               if(iFibreGINo>iMaxNo)
               {
                    iMaxNo = iFibreGINo;     
               }
               if(iYarnGINo>iMaxNo)
               {
                    iMaxNo = iYarnGINo;     
               }
               if(iPurcGINo>iMaxNo)
               {
                    iMaxNo = iPurcGINo;     
               }

               iGINo = iMaxNo+1;
          }

          SGINo     = String.valueOf(iGINo);
          LGateNo.setText(SGINo);
     }
     private void getInventoryGINo()
     {
          try
          {
               String QS = " Select max(GINo) from GateInward where gidate >= '20070401' and MillCode="+iMillCode;

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    iInvGINo = common.toInt(result.getString(1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
     private void getWebGINo()
     {
          try
          {
               String QS = " Select nvl(max(GINo),0) from WebGI where GiDate >= 20051201 ";

               DORAConnection  doraConnection = DORAConnection.getORAConnection();
               Connection      theConnection  = doraConnection.getConnection();               
               Statement       theStatement   = theConnection.createStatement();
               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    iWebGINo = common.toInt(result.getString(1));
               }
               result.close();
               theStatement.close();
          }      
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
     private void getOtherGINo()
     {

          try
          {
               String QS1 = " Select nvl(max(GINo),0) from SCM.CottonGI where GiDate >= 20070401 ";
               String QS2 = " Select nvl(max(GINo),0) from SCM.Agn where GiDate >= 20070401 ";
               String QS3 = " Select nvl(max(GINo),0) from SCM.GIFibreDetails ";
               String QS4 = " Select nvl(max(GINo),0) from SCM.YarnGI where GiDate >= 20070401 ";
               String QS5 = " Select nvl(max(GINo),0) from SCM.Purchase where GiDate >= 20070401 ";

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
               ResultSet result = theStatement.executeQuery(QS1);
               while(result.next())
               {
                    iCottonGINo = common.toInt(result.getString(1));
               }
               result.close();

               result = theStatement.executeQuery(QS2);
               while(result.next())
               {
                    iAgnGINo = common.toInt(result.getString(1));
               }
               result.close();

               result = theStatement.executeQuery(QS3);
               while(result.next())
               {
                    iFibreGINo = common.toInt(result.getString(1));
               }
               result.close();

               result = theStatement.executeQuery(QS4);
               while(result.next())
               {
                    iYarnGINo = common.toInt(result.getString(1));
               }
               result.close();

               result = theStatement.executeQuery(QS5);
               while(result.next())
               {
                    iPurcGINo = common.toInt(result.getString(1));
               }
               result.close();
               theStatement.close();
          }      
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }*/

     public void setDataIntoVector()
     {
          VInward     = new Vector();
          VInwardCode = new Vector();

          try
          {
               Statement       theStatement  =  theMConnection.createStatement();
               ResultSet result1 = theStatement.executeQuery("Select InwName,InwNo From InwType Order By 2");
               while(result1.next())
               {
                    VInward.addElement((common.parseNull(result1.getString(1))).toUpperCase());
                    VInwardCode.addElement(common.parseNull(result1.getString(2)));
               }
               result1.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

}
