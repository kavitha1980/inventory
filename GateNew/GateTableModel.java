
package GateNew;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import guiutil.*;
import util.*;
public class GateTableModel extends DefaultTableModel
{
      
        Object    RowData[][],ColumnNames[],ColumnType[],ColumnDatas[][];
        Common common = new Common();
        public GateTableModel(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType)
        {
            super(RowData,ColumnNames);
            this.RowData     = RowData;
            this.ColumnNames = ColumnNames;
            this.ColumnType  = ColumnType;

	    setColumnData();
        }
       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }            
       public boolean isCellEditable(int row,int col)
       {
	       if(col==1)
	       {
       	            if(ColumnDatas[row][col]=="X")
			    return true;

		    return false;
	       }
	       else
	       {	
                    if(ColumnType[col]=="B" || ColumnType[col]=="E")
                       return true;

		    return false;
	       }
       }
       public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column));
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }
    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
                FinalData[i][j] = (String)curVector.elementAt(j);
        }
        return FinalData;
    }
    public Vector getCurVector(int i)
    {
        Vector curVector = (Vector)super.dataVector.elementAt(i);
        return curVector;
    }
    public int getRows()
    {
        return super.dataVector.size();
    }

    public void setColumnData()
    {
	ColumnDatas = new Object[super.dataVector.size()][ColumnType.length];

	setColType();

	for(int i=0;i<super.dataVector.size();i++)
	{
	     Vector curVector   = (Vector)super.dataVector.elementAt(i);
	     setColType(curVector,i);
	}
    }

     public void setColType()
     {
	     ColumnType[1] = "S";
     }

     public void setColType(Vector RowVector,int iRow)
     {
             String SItemCode = ((String)RowVector.elementAt(0)).trim();
	     if(SItemCode.equals(""))
	     {
	          ColumnDatas[iRow][1] = "X";
	     }
	     else
	     {
	          ColumnDatas[iRow][1] = "S";
	     }
     }


}
