/*

     The Inward Mode Object is meant for
     allowing the User to select a Mode of the Inward Stores Materials
     belongs to.

     If the Mode in ? is not available a provision
     to insert On line the new Mode.

*/

package GateNew;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;
import java.util.*;
import java.io.*;
import java.sql.*;
import util.*;
import jdbc.*;

public class InwardModeModel extends JButton implements ActionListener
{

      JTextField     TCode;
      Vector         VCode,VName;

      int iUserCode,iAuthCode,iMillCode;

      Common common = new Common();

      Connection theConnection=null;

      public InwardModeModel(int iUserCode,int iAuthCode,int iMillCode)
      {
            this.iUserCode = iUserCode;
            this.iAuthCode = iAuthCode;
            this.iMillCode = iMillCode;

            TCode = new JTextField();
            setText("Mode");
            setBorder(new javax.swing.border.BevelBorder(0));
            setBackground(new Color(128,128,255));
            setForeground(Color.RED);

            setDataIntoVector();
           
            addActionListener(this);
      }
      public void actionPerformed(ActionEvent ae)
      {

            Frame dummy           = new Frame();
            JDialog theDialog     = new JDialog(dummy,"Mode Selector",true); 
            InwModeSearchPanel SP = new InwModeSearchPanel(this,TCode,VCode,VName,theDialog);

            theDialog.getContentPane().add(SP);
            theDialog.setBounds(190,90,400,350);
            theDialog.setVisible(true);
      }
          
      public void setDetails()
      {
            String SName = getText();
            String SCode = TCode.getText();

            int index = VCode.indexOf(SCode);

            if((index == -1) && (SName.trim().length()>0))
                  setData();
            else
            {
                  TCode.setText(SCode);
            }   
      }

      private void setData()
      {

            try
            {
                  String SCode="";
                  String SName="";

                  String STime = common.getServerDateTime();

                  String QS1= " Select Max(Code) from InwardMode ";

                    ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                    Connection      theConnection =  oraConnection.getConnection();               
                    Statement       theStatement  =  theConnection.createStatement();
                  ResultSet result         = theStatement.executeQuery(QS1);
                  while(result.next())
                  {
                       SCode = common.parseNull(result.getString(1));
                  }
                  result.close();


                  int iCode          = common.toInt(SCode)+1;
                  SCode              = String.valueOf(iCode);
                  SName              = getText().toUpperCase();

                  String QS = " Insert into InwardMode(Id,Code,Name,UserCode,UserTime,BaseCode,MillCode) values("+
                              "InwardMode_Seq.nextval"+
                              ","+SCode+
                              ",'"+SName+
                              "',"+String.valueOf(iUserCode)+
                              ",'"+STime+
                              "',"+String.valueOf(iAuthCode)+
                              ","+String.valueOf(iMillCode)+")";

                  theStatement.execute(QS);
                  theStatement.close();
                  setDataIntoVector();
                  TCode.setText(SCode);

            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }

      }

      public void setDataIntoVector()
      {
            VCode      = new Vector();
            VName      = new Vector();

            try
            {

                  String QS = " Select Code,Name from InwardMode order by 2 ";

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
                  ResultSet result         = theStatement.executeQuery(QS);
                  while(result.next())
                  {
                       VCode.addElement(common.parseNull(result.getString(1)));
                       VName.addElement(common.parseNull(result.getString(2)));
                  }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
                  
            }
      }

      public String getCode()
      {
            return TCode.getText();
      }
     
}
