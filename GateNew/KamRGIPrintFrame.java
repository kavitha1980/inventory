package GateNew;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.io.*;
import java.sql.*;
import guiutil.*;
import util.*;
import jdbc.*;

public class KamRGIPrintFrame extends JInternalFrame 
{
     protected                JLayeredPane   Layer;
     int                      iMillCode;

     JPanel                   TopPanel,MiddlePanel,BottomPanel;

     JComboBox                JCSort,JCList;
     DateField                TFromDate,TToDate;

     JButton                  BApply,BPrint,BExit;

     JTable                   theTable;
     RGIPrintModel            theModel;

     Vector                   VRawMaterial;
     Common                   common = new Common();

     String                   SError = "";

     PrinterComm              Printer;
     File                     file;

     public KamRGIPrintFrame(JLayeredPane Layer,int iMillCode)
     {
          this.Layer     = Layer;
          this.iMillCode = iMillCode;

          try
          {
               createComponents();
               setLayouts();
               addComponents();
               addListeners();
               theModel.setNumRows(0);
          }
          catch(Exception ex){}
     }

     private void createComponents()
     {
          TopPanel            = new JPanel();
          MiddlePanel         = new JPanel();
          BottomPanel         = new JPanel();

          JCSort              = new JComboBox();
          JCSort              . addItem("G.I. No");
          JCSort              . addItem("Supplier Wise");

          JCList              = new JComboBox();
          JCList              . addItem("All");
          JCList              . addItem("Cotton");
          JCList              . addItem("Dyed Fibre");
          JCList              . addItem("Purchased Fibre");

          TFromDate           = new DateField();
          TToDate             = new DateField();

          BApply              = new JButton("Apply");
          BPrint              = new JButton("Print");
          BExit               = new JButton("Exit");

          theModel            = new RGIPrintModel();
          theTable            = new JTable(theModel);

          TableColumnModel TC = theTable.getColumnModel();
          for(int i=0;i<theModel.ColumnName.length;i++)
          {
               TC.getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
          }    

          VRawMaterial        = new Vector();
          Printer             = new PrinterComm();

          JCList.setSelectedIndex(0);
          JCList.setEnabled(false);
     }

     private void setLayouts()
     {
          setTitle("Raw Material GateInward Print");
          setBounds(0,0,800,500);
          setIconifiable(true);
          setClosable(true);
          setResizable(true);
          setMaximizable(true);

          TopPanel       . setLayout(new GridLayout(1,9));
          MiddlePanel    . setLayout(new BorderLayout());
          BottomPanel    . setLayout(new FlowLayout());

          MiddlePanel    . setBorder(new TitledBorder("Details"));
          BottomPanel    . setBorder(new TitledBorder("Controls"));
     }

     private void addComponents()
     {
          TopPanel            . add(new JLabel("Sorted by"));
          TopPanel            . add(JCSort);
          TopPanel            . add(new JLabel("Listed by"));
          TopPanel            . add(JCList);
          TopPanel            . add(new JLabel("From"));
          TopPanel            . add(TFromDate);
          TopPanel            . add(new JLabel("To"));
          TopPanel            . add(TToDate);
          TopPanel            . add(BApply);

          MiddlePanel         . add(new JScrollPane(theTable));

          BottomPanel         . add(BPrint);
          BottomPanel         . add(BExit);

          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("Center",MiddlePanel);
          getContentPane()    . add("South",BottomPanel);
     }

     private void addListeners()
     {
          BApply    . addActionListener(new ActList());
          BPrint    . addActionListener(new ActList());
          BExit     . addActionListener(new ActList());
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
                    if(CheckFields())
                    {
                         setTableReport();
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,SError,"Error",JOptionPane.ERROR_MESSAGE);
                    }
               }

               if(ae.getSource()==BPrint)
               {
                    if(CheckClick())
                    {
                         toGenerateGIPrn();
                         Printer.CallPrinter(file);
                         theModel.setNumRows(0);
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Please Select Atleast One Check Box to Print","ERROR",JOptionPane.WARNING_MESSAGE);
                    } 
               }

               if(ae.getSource()==BExit)
               {
                    removeFrame();
               }
          }
     }

     private void setTableReport()
     {
          theModel  . setNumRows(0);

          Vector    theVector;

          try
          {
               String SFromDate    = TFromDate.toNormal();
               String SToDate      = TToDate.toNormal();
               String SSort        = (String)JCSort.getSelectedItem();
               String SList        = (String)JCList.getSelectedItem();
               
               VRawMaterial        = getRawMaterialPrintingDetails(SFromDate,SToDate,SSort,SList);

               for(int i=0;i<VRawMaterial.size();i++)
               {
                    HashMap theMap = (HashMap)VRawMaterial.elementAt(i);

                    theVector      = new Vector();

                    theVector      . addElement(new Boolean(false));
                    theVector      . addElement(common.parseNull((String)theMap.get("GINO")));
                    theVector      . addElement(common.parseDate(common.parseNull((String)theMap.get("GIDATE"))));
                    theVector      . addElement(common.parseNull((String)theMap.get("NAME")));
                    theVector      . addElement(common.parseNull((String)theMap.get("INVOICENO")));
                    theVector      . addElement(common.parseDate(common.parseNull((String)theMap.get("INVOICEDATE"))));
                    theVector      . addElement(common.parseNull((String)theMap.get("DCNO")));
                    theVector      . addElement(common.parseDate(common.parseNull((String)theMap.get("DCDATE"))));
                    theVector      . addElement((String)theMap.get("INWARDTYPE"));
                    theVector      . addElement((String)theMap.get("TRUCKNO"));
                    theVector      . addElement("");
                    theVector      . addElement("");

                    theModel       . appendRow(theVector);
               }                    
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public Vector getRawMaterialPrintingDetails(String SFromDate,String SToDate,String SSort,String SList)
     {
          Vector theVector = new Vector();

          try
          {
               KORAConnection  oraConnection =  KORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();

               Statement theStatement   = theConnection.createStatement();
               ResultSet rs             = theStatement.executeQuery(getRawMaterialQuery(SFromDate,SToDate,SSort,SList).toString());
               ResultSetMetaData rsmd   = rs.getMetaData();

               while(rs.next())
               {
                    HashMap theMap      = new HashMap();

                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         theMap.put(rsmd.getColumnName(i+1),rs.getString(i+1));
                    }

                    theVector.addElement(theMap);
               }
		rs.close();
		theStatement.close();
          }        
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          return theVector;
     }

     private StringBuffer getRawMaterialQuery(String SFromDate,String SToDate,String SSort,String SList)
     {
          StringBuffer SB  = new StringBuffer();

          SB.append(" Select GiNo,GIDate,Supplier.Name,InvNo as InvoiceNo,InvDate as InvoiceDate,DCNo,DCDate,'RawMaterial' as InwardType,TruckNo,TimeIn,FibreType.FibreTypeName as RMType,Bales,Weight,Narration from CottonGI ");
          SB.append(" Inner Join scm.Supplier on Supplier.AcCode = CottonGI.AcCode ");
          SB.append(" Inner Join FibreType on CottonGI.FibreTypeCode = FibreType.FibreTypeCode ");
          SB.append(" where GiDate>="+SFromDate+" and GiDate<="+SToDate+"  and PrintStatus=0");


          if(SSort.equals("G.I. No"))
          {
               SB.append(" Order by 1,2 ");
          }
          else
          {
               SB.append(" Order by 3,2 ");
          }

          return SB;
     }

     private boolean CheckClick()
     {
          for(int i=0;i<theModel.getRowCount();i++)
          {
               Boolean bValue = (Boolean)theModel.getValueAt(i,0);

               if(bValue.booleanValue())
                    return true;
          }
          return false; 
     }   


     private void toGenerateGIPrn()
     {
          try
          {
               file           = new File("D://GIPrint.prn");
               FileWriter FW  = new FileWriter(file);

               for(int i = 0;i<theModel.getRowCount();i++)
               {
                    Boolean bValue = (Boolean)theModel.getValueAt(i,0);

                    if(bValue.booleanValue())
                    {
                         HashMap theMap      = (HashMap)VRawMaterial.elementAt(i);

                         String SSupName     = common.parseNull((String)theMap.get("NAME"));
                         String SGINo        = common.parseNull((String)theMap.get("GINO"));
                         String SGIDate      = common.parseDate(common.parseNull((String)theMap.get("GIDATE")));
                         String STimeIn      = common.parseNull((String)theMap.get("TIMEIN"));
                         String SInvoiceNo   = common.parseNull((String)theMap.get("INVOICENO"));
                         String SInvoiceDate = common.parseDate(common.parseNull((String)theMap.get("INVOICEDATE")));
                         String SDCNo        = common.parseNull((String)theMap.get("DCNO"));
                         String SDCDate      = common.parseDate(common.parseNull((String)theMap.get("DCDATE")));
                         String SInwardType  = common.parseNull((String)theMap.get("INWARDTYPE"));
                         String STruckNo     = common.parseNull((String)theMap.get("TRUCKNO"));
                         String SRMType      = common.parseNull((String)theMap.get("RMTYPE"));
                         String SBales       = common.parseNull((String)theMap.get("BALES"));
                         String SWeight      = common.parseNull((String)theMap.get("WEIGHT"));
                         String SRemarks     = common.parseNull((String)theMap.get("NARRATION"));

                         new RGIPrintClass(FW,SSupName,SGINo,SGIDate,STimeIn,SInvoiceNo,SInvoiceDate,SDCNo,SDCDate,SInwardType,STruckNo,SRMType,SBales,SWeight,SRemarks,iMillCode);

                         updatePrintStatus(SGINo);
                    }
               }

               FW.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void updatePrintStatus(String SGINo)
     {
          try
          {
               String QS = " Update CottonGI set PrintStatus=1 Where GINo="+SGINo;

               KORAConnection  oraConnection =  KORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();

               theStatement  . executeUpdate(QS);

               theStatement.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private boolean CheckFields()
     {
                    SError    = "";
          boolean   bFlag     = true;
          int       iCount    = 0;

          String SFromDate    = TFromDate.toString();
          String SToDate      = TToDate.toString();

                    SError    = "<html><font color='red'>";

          if(SFromDate.length() <= 9)
          {
               iCount++;
               SError   += iCount+"). From Date Must be Filled <br>";
               bFlag     = false;
          }

          if(SToDate.length() <= 9)
          {
               iCount++;
               SError   += iCount+"). To Date Must be Filled <br>";
               bFlag     = false;
          }

          SError        += "</font></html>";

          return bFlag;
     }


     private void removeFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
