package GateNew;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.io.*;
import java.sql.*;
import guiutil.*;
import util.*;
import jdbc.*;

public class MelGateInwardFrame extends JInternalFrame
{
     protected JLayeredPane Layer;
     StatusPanel SPanel;
     int iUserCode,iAuthCode,iMillCode;
     String SMillName;
     String SYearCode;

     RowSet    rowSet;

     JPanel MiddlePanel,MidTop,MidBot,BottomPanel;

     
     DateField        TGIDate,TDCDate,TInvoiceDate;
     WholeNumberField TGINo,TSecurityCode,TBales,TWeight;
     DocNoField       TDCNo,TInvoiceNo;
     NoNumberField    TTruckNo;
     JTextField       TLotNo;
     ClockField       TTimeIn;
     MyComboBox       JCSupplier,JCSecurity,JCFibreType;
     JTextArea        TA;
     TimeField        theTime;
     String SGINo="";

     
     MyButton        BOkay;
     MyLabel         LGateNo;

     JButton          BExit;
     int iMaterialType = 1;
     String STitle;


     Vector VPartyCode;
     Vector VPartyName;
     Vector VSecurityCode;
     Vector VSecurityName;
     Vector VFibreTypeCode;
     Vector VFibreTypeName;

     int iInvGINo,iCottonGINo,iAgnGINo,iFibreGINo,iYarnGINo,iPurcGINo;

     Common common = new Common();

     public MelGateInwardFrame(JLayeredPane Layer,StatusPanel SPanel,int iUserCode,int iAuthCode,int iMillCode,String SMillName,String SYearCode)
     {
          this.Layer     = Layer;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iAuthCode = iAuthCode;
          this.iMillCode = iMillCode;
          this.SMillName = SMillName;
          this.SYearCode = SYearCode;

          setVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          try
          {
               MiddlePanel     = new JPanel();
               MidTop          = new JPanel();
               MidBot          = new JPanel();
               BottomPanel     = new JPanel();
     
               TGIDate         = new DateField();
               TGIDate         . setTodayDate();
               TGIDate         . setEditable(false);    

               TGINo           = new WholeNumberField(10);
               LGateNo         = new MyLabel("");


               JCSupplier      = new MyComboBox(VPartyName);

               JCSecurity      = new MyComboBox(VSecurityName);

               JCFibreType     = new MyComboBox(VFibreTypeName);

               JCFibreType.setSelectedIndex(10);

               TDCNo           = new DocNoField(20);
               TDCDate         = new DateField();

               TTruckNo        = new NoNumberField(15);
               TTimeIn         = new ClockField();
               TSecurityCode   = new WholeNumberField(6);

               TInvoiceNo      = new DocNoField(20);
               TInvoiceDate    = new DateField();

               TBales          = new WholeNumberField(6);
               TWeight          = new WholeNumberField(8);

               TLotNo          = new JTextField();
               theTime         = new TimeField();
     
               TA              = new JTextArea(10,20);
               setGateNo();

               BOkay           = new MyButton("Okay");
               BExit           = new JButton("Exit");       
          }
          catch(Exception ex)
          {
          }
     }

     private void setLayouts()
     {
          setTitle(STitle);
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,800,500);

          MidTop.setLayout(new GridLayout(14,3));
          MidBot.setLayout(new BorderLayout());
          MiddlePanel.setLayout(new BorderLayout());

          MidTop.setBorder(new TitledBorder("Gate Inward Details"));
          MidBot.setBorder(new TitledBorder("Description & Narration"));

          BottomPanel.setBorder(new TitledBorder(""));
          TTimeIn.setEditable(false);
          BOkay.setMnemonic('O');
          BExit.setMnemonic('E');
     }

     private void addComponents()
     {
          MidTop.add(new JLabel("Fibre Type"));
          MidTop.add(JCFibreType);
          MidTop.add(new JLabel(" "));

          MidTop.add(new JLabel("Inward Date"));
          MidTop.add(TGIDate);
          MidTop.add(new JLabel(" "));

          MidTop.add(new JLabel("Inward No"));
          MidTop.add(LGateNo);
          MidTop.add(new JLabel(" "));

          MidTop.add(new JLabel("Supplier Name"));
          MidTop.add(JCSupplier);
          MidTop.add(new JLabel(" "));

          MidTop.add(new JLabel("DC No"));
          MidTop.add(TDCNo);
          MidTop.add(new JLabel(" "));

          MidTop.add(new JLabel("DC Date"));
          MidTop.add(TDCDate);
          MidTop.add(new JLabel(" "));

          MidTop.add(new JLabel("Invoice No"));
          MidTop.add(TInvoiceNo);
          MidTop.add(new JLabel(" "));

          MidTop.add(new JLabel("Invoice Date"));
          MidTop.add(TInvoiceDate);
          MidTop.add(new JLabel(" "));

          MidTop.add(new JLabel("Truck No"));
          MidTop.add(TTruckNo);
          MidTop.add(new JLabel(" "));

          MidTop.add(new JLabel("Time In"));
          MidTop.add(TTimeIn);
          MidTop.add(new JLabel(" "));

          MidTop.add(new JLabel("Security Name"));
          MidTop.add(JCSecurity);
          MidTop.add(new JLabel(" "));

          MidTop.add(new JLabel("Party Lot No"));
          MidTop.add(TLotNo);
          MidTop.add(new JLabel(" "));

          MidTop.add(new JLabel("No of Bales"));
          MidTop.add(TBales);
          MidTop.add(new JLabel(" "));

          MidTop.add(new JLabel("Weight"));
          MidTop.add(TWeight);
          MidTop.add(new JLabel(" "));


          MidBot.add("Center",new JScrollPane(TA));

          MiddlePanel.add("North",MidTop);
          MiddlePanel.add("Center",MidBot);

          BottomPanel.add(BOkay);
          BottomPanel.add(BExit);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
     }

     private void setVector()
     {
         VPartyCode     = new Vector();
         VPartyName     = new Vector();
         VSecurityCode  = new Vector();
         VSecurityName  = new Vector();
         VFibreTypeCode = new Vector();
         VFibreTypeName = new Vector();

         try
         {
              String QS1 = "Select Code,Name from SecurityInfo order by 2 ";
              String QS2 = "Select AcCode,Name from scm.Supplier where length(rtrim(accode))>1 order by 2";
              String QS3 = "Select FibreTypeCode,FibreTypeName from FibreType Order by FibreTypeName";

               CMORAConnection oraConnection = CMORAConnection.getORAConnection();
               Connection      theConnection = oraConnection.getConnection();               
               Statement       stat  =  theConnection.createStatement();
          
               ResultSet result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VSecurityCode.addElement(result.getString(1));
                    VSecurityName.addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VPartyCode.addElement(result.getString(1));
                    VPartyName.addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery(QS3);
               while(result.next())
               {
                    VFibreTypeCode.addElement(result.getString(1));
                    VFibreTypeName.addElement(result.getString(2));
               }
               result.close();

               stat.close();

         }
         catch(Exception ex)
         {
            System.out.println(ex);
         }

    }

     private void addListeners()
     {
          BOkay.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOkay)
               {
                    if(isValidData())
                    {
                         setGateNo();
                         saveData();
                    }
               }
               if(ae.getSource()==BExit)
               {
                    showConfirmMsg();
               }
          }
     }
     public void setGateNo()
     {
          int iGINo=0;

          int iMaxNo=0;

          iInvGINo = 0;

          getInventoryGINo();
     
          iMaxNo = iInvGINo;

          iGINo = iMaxNo+1;

          SGINo     = String.valueOf(iGINo);
          LGateNo.setText(SGINo);
     }
     private void getInventoryGINo()
     {
          try
          {
               String QS = " Select maxno from Config"+iMillCode+""+SYearCode+" where id = 3";

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    iInvGINo = common.toInt(result.getString(1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
     private void saveData()
     {
          persist();
          updateConfig();
          refresh();
          removeHelpFrame();
     }

     private void refresh()
     {
           LGateNo.setText("");
           TDCNo.setText("");
           TInvoiceNo.setText("");
           TTruckNo.setText("");
           TSecurityCode.setText("");
           TBales.setText("");
           TWeight.setText("");

           TLotNo.setText("");
           TA.setText("");
           TGIDate.requestFocus();
     }


     private void persist()
     {

          try
          {
               String QS = "Insert Into CottonGI (ID,GINo,GIDate,ACCode,InvNo,InvDate,DCNo,DCDate,TruckNo,TimeIn,SecCode,PartyLotNo,Bales,Weight,Narration,FibreTypeCode) Values(";
               QS = QS+" GateInward_seq.nextval, ";
               QS = QS+"0"+LGateNo.getText()+",";
               QS = QS+"0"+String.valueOf(TGIDate.toNormal())+",";
               QS = QS+"'"+(String)VPartyCode.elementAt(JCSupplier.getSelectedIndex())+"',";
               QS = QS+"'"+TInvoiceNo.getText()+"',";
               QS = QS+"0"+String.valueOf(TInvoiceDate.toNormal())+",";
               QS = QS+"'"+TDCNo.getText()+"',";
               QS = QS+"0"+String.valueOf(TDCDate.toNormal())+",";
               QS = QS+"'"+(TTruckNo.getText()).toUpperCase()+"',";
               QS = QS+"'"+TTimeIn.getText()+"',";
               QS = QS+"0"+(String)VSecurityCode.elementAt(JCSecurity.getSelectedIndex())+",";
               QS = QS+"'"+TLotNo.getText()+"',";
               QS = QS+"0"+TBales.getText()+",";
               QS = QS+"0"+TWeight.getText()+",";
               QS = QS+"'"+TA.getText().trim()+"',";
               QS = QS+"0"+(String)VFibreTypeCode.elementAt(JCFibreType.getSelectedIndex())+")";


               CMORAConnection oraConnection =  CMORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();

               theStatement  . executeUpdate(QS);

               theStatement.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private boolean isValidData()
     {
          
          if(TGIDate.toNormal().equals(""))
          {
               JOptionPane.showMessageDialog(null,"Inward Date not Filled","Error",JOptionPane.ERROR_MESSAGE);
               TGIDate.requestFocus();
               return false;
          }

          if(TDCNo.getText().equals(""))
          {
               JOptionPane.showMessageDialog(null,"DC No Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TDCNo.requestFocus();
               return false;
          }

          if(TInvoiceNo.getText().equals(""))
          {
               JOptionPane.showMessageDialog(null,"Invoice No Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TInvoiceNo.requestFocus();
               return false;
          }

          if(common.toInt(TDCDate.toNormal())==0)
          {
               JOptionPane.showMessageDialog(null,"DC Date Should Be Filled","Dear user,",JOptionPane.ERROR_MESSAGE);
               TDCDate.requestFocus();
               return false;
          }

          if(common.toInt(TInvoiceDate.toNormal())==0)
          {
               JOptionPane.showMessageDialog(null,"Invoice Date Should Be Filled","Dear user,",JOptionPane.ERROR_MESSAGE);
               TInvoiceDate.requestFocus();
               return false;
          }

          if(common.toInt(TDCDate.toNormal()) > common.toInt(TGIDate.toNormal()))
          {
               JOptionPane.showMessageDialog(null,"DC Date & GI Date Mis-Matches","Dear user,",JOptionPane.ERROR_MESSAGE);
               TDCDate.requestFocus();
               return false;  
          }

          if(common.toInt(TInvoiceDate.toNormal()) > common.toInt(TGIDate.toNormal()))
          {
               JOptionPane.showMessageDialog(null,"Invoice Date & GI Date Mis-Matches","Dear user,",JOptionPane.ERROR_MESSAGE);
               TInvoiceDate.requestFocus();
               return false;  
          }

          if(TTruckNo.getText().equals(""))
          {
               JOptionPane.showMessageDialog(null,"Truck No Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TTruckNo.requestFocus();
               return false;
          }


          if(TTimeIn.getText().equals(""))
          {
               JOptionPane.showMessageDialog(null,"Time In Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TTimeIn.requestFocus();
               return false;
          }

          if(common.toInt(TBales.getText())<=0)
          {
               JOptionPane.showMessageDialog(null,"Invalid No Bales","Dear user,",JOptionPane.ERROR_MESSAGE);
               TBales.requestFocus();
               return false;
          }

          if(TLotNo.getText().equals(""))
          {
               JOptionPane.showMessageDialog(null,"Enter Party Lot No ","Dear user,",JOptionPane.ERROR_MESSAGE);
               TLotNo.requestFocus();
               return false;
          }


          return true;
     } 

     private String getWarningMessage()
     {
          String str = "<html><body>";
          str = str + "<b>Problem in persisting the Data.</b>"+"<br>";
          str = str + "It may be either due to a poor network<br>";
          str = str + "or<br>";          
          str = str + "violation of data management principles.<br>";
          str = str + "Contact your system administrator.<br>";          
          str = str + "</body></html>";
          return str;
     }
     
     private String getGIMessage(String SId)
     {
          String str = "<html><body>";
          str = str + "<b>Id For this Transaction : "+SId+"</b><br>";
          str = str + "A New Gate Inward Transaction was<br>";
          str = str + "successfully registered<br>";          
          str = str + "</body></html>";
          return str;
     }
     private void showGIMessage(Vector VId)
     {
          if(VId.size()==0)
               return;
          String SId = (String)VId.elementAt(0);
          JOptionPane.showMessageDialog(null,getGIMessage(SId),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }
     private String getCurrentTime()
     {
           return theTime.getTimeNow();
          
     }

     private void updateConfig()
     {
          try
          {
               String QS = " Update Config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1  where Id = 3";

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();

               theStatement  . executeUpdate(QS);

               theStatement.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void showConfirmMsg()
     {
          if((JOptionPane.showConfirmDialog(null,"Exit without saving? ","Warning",JOptionPane.YES_NO_OPTION)) == 0)
          {
               removeHelpFrame();
          }               
          else
          {
               return;
          }               
     }
     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }




}
