package GateNew;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class OutPassAuthFrame extends JInternalFrame
{
     JPanel     TopPanel;
     JPanel     BottomPanel;
     JPanel     MiddlePanel;

     String     SDate;
     JButton    BApply,BAuth;
    
     DateField TDate;
    
     Object RowData[][];
     String ColumnData[] = {"Material Type","OutPass Date","OutPasss No","Created By","Click for Auth"}; 
     String ColumnType[] = {"S"            ,"S"           ,"S"          ,"S"         ,"B"             }; 

     JLayeredPane DeskTop;
     StatusPanel SPanel;
     int iUserCode,iAuthCode,iMillCode;
     String SMillName;

     TabReport tabreport;
     ORAConnection connect;
     Connection theconnect;
     Common common = new Common();

     Vector VDocType,VOutPassDate,VOutPassNo,VUserName;

     OutPassAuthFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iAuthCode,int iMillCode,String SMillName)
     {
          super("List of OutPass Pending As On today");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iAuthCode = iAuthCode;
          this.iMillCode = iMillCode;
          this.SMillName = SMillName;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          TopPanel    = new JPanel(true);
          BottomPanel = new JPanel(true);
          MiddlePanel = new JPanel(true);

          TDate       = new DateField();
          BApply      = new JButton("Apply");
          BAuth       = new JButton("Authenticate");

          TDate.setTodayDate();
          TDate.setEditable(false);

          BApply.setMnemonic('A');
          BAuth.setMnemonic('U');
          BAuth.setEnabled(false);
     }

     private void setLayouts()
     {
          TopPanel.setLayout(new FlowLayout());
          MiddlePanel.setLayout(new BorderLayout());

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
     }

     private void addComponents()
     {
          TopPanel.add(new JLabel("As On"));
          TopPanel.add(TDate);
          TopPanel.add(BApply);

          BottomPanel.add(BAuth);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
     }

     private void addListeners()
     {
          BApply.addActionListener(new ActList());
          BAuth.addActionListener(new AuthList());
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setTabReport();
               printData();
          }
     }
     private class AuthList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               updateOutPass();
               removeHelpFrame();
          }
     }

     public void setTabReport()
     {
               boolean bflag = setDataIntoVector();
               if(!bflag)
                    return;
               setVectorIntoRowData();
               BAuth.setEnabled(true);
               BApply.setEnabled(false);

               try
               {
                    MiddlePanel.remove(tabreport);
                    MiddlePanel.updateUI();
               }
               catch(Exception ex){}
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
               MiddlePanel.add("Center",tabreport);
               MiddlePanel.updateUI();
     }
     private boolean setDataIntoVector()
     {
          boolean bflag = false;
          VDocType      = new Vector();
          VOutPassDate  = new Vector();
          VOutPassNo    = new Vector();
          VUserName     = new Vector();

          String SDate = TDate.toNormal();

          String QS = " Select MaterialType,OutPassDate,OutPassNo,InvLogin.UserName "+
                      " From OutPass "+
                      " Left Join InvLogin on OutPass.UserCode=InvLogin.Id "+
                      " Where OutPassDate<='"+SDate+"' And GateAuth=0 "+
                      " And OutPass.MillCode="+iMillCode+
                      " Order By 2,3 ";
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res  = stat.executeQuery(QS);
               while(res.next())
               {
                    int iDocType = res.getInt(1);

                    if(iDocType==0)
                    {
                         VDocType.addElement("Stores Material");
                    }
                    else
                    {
                         VDocType.addElement("Other Material");
                    }

                    VOutPassDate  .addElement(""+res.getString(2));
                    VOutPassNo    .addElement(""+res.getString(3));
                    VUserName     .addElement(""+res.getString(4));
               }
               res.close();
               stat.close();
               bflag = true;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bflag = false;
          }
          return bflag;
     }

     private void setVectorIntoRowData()
     {
          RowData = new Object[VOutPassNo.size()][ColumnData.length];

          for(int i=0;i<VOutPassDate.size();i++)
          {
               RowData[i][0]  = (String)VDocType  .elementAt(i);
               RowData[i][1]  = common.parseDate((String)VOutPassDate.elementAt(i));
               RowData[i][2]  = (String)VOutPassNo.elementAt(i);
               RowData[i][3]  = common.parseNull((String)VUserName.elementAt(i));
               RowData[i][4]  = new Boolean(false);
          }
     }

     private void updateOutPass()
     {
          try
          {
               String SDateTime = common.getServerDate();

               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean bValue = (Boolean)tabreport.ReportTable.getValueAt(i,4);
                    if(!bValue.booleanValue())
                          continue;

                    String SOutPassNo   = (String)VOutPassNo.elementAt(i);
                    String SOutPassDate = (String)VOutPassDate.elementAt(i);

                    String QS = " Update OutPass Set GateAuth=1,AuthUserCode="+iUserCode+","+
                                " GateAuthTime='"+SDateTime+"' Where OutPassNo="+SOutPassNo+
                                " And OutPassDate="+SOutPassDate+" And MillCode="+iMillCode;

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void printData()
     {
            String STitle = "List of OutPass Pending As On "+TDate.toString();

            String SFile  = "OutPassPend.prn";
            new DocPrint(getPendingBody(),getPendingHead(),STitle,SFile,iMillCode,SMillName);
     }

     public Vector getPendingBody()
     {
        Vector vect = new Vector();

        for(int i=0;i<VDocType.size();i++)
        {
              String strl="";

              String SDocType     = (String)VDocType.elementAt(i);
              String SOutpassNo   = (String)VOutPassNo.elementAt(i);
              String SOutpassDate = common.parseDate((String)VOutPassDate.elementAt(i));
              String SUserName    = common.parseNull((String)VUserName.elementAt(i));

              strl = strl + common.Cad(String.valueOf(i+1),5)+common.Space(3);
              strl = strl + common.Pad(SDocType,20)+common.Space(3);
              strl = strl + common.Cad(SOutpassNo,15)+common.Space(3);
              strl = strl + common.Cad(SOutpassDate,15)+common.Space(3);
              strl = strl + common.Cad(SUserName,15)+common.Space(3);

              strl = strl+"\n";
              vect.addElement(strl);
        }
        return vect;
     }

     public Vector getPendingHead()
     {
           Vector vect = new Vector();

           String strl1 = "";

           strl1 = strl1 + common.Cad("S.No.",5)+common.Space(3);
           strl1 = strl1 + common.Cad("Material Type",20)+common.Space(3);
           strl1 = strl1 + common.Cad("Outpass Number",15)+common.Space(3);
           strl1 = strl1 + common.Cad("Outpass Date",15)+common.Space(3);
           strl1 = strl1 + common.Cad("Created By",15)+common.Space(3);

           strl1 = strl1+"\n";

           vect.addElement(strl1);

           return vect;
     }


}
