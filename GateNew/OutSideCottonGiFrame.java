package GateNew;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

import java.util.*;
import java.sql.*;
import util.*;
import guiutil.*;

import jdbc.*;


public class OutSideCottonGiFrame extends JInternalFrame
{
     protected JLayeredPane Layer;
     protected int iPK   = -1;
     private   JPanel    MiddlePanel,BottomPanel,TopPanel;
     private   MyButton  bOkay,bExit;
     private   OutSideCottonGiModel                         theModel;
     private   JTable                                       jTable;
     private   Connection theConnection                     = null;
     private   Vector     theVector;
     Common common = new Common();


     public OutSideCottonGiFrame(JLayeredPane Layer){

          this.Layer = Layer;

          createComponents    ();
          setLayouts          ();
          addComponents       ();
          addActionListeners  ();
          setTableData        ();
     }

     private void createComponents()  {

          try   {

               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();

               bOkay          = new MyButton("Okay");
               bExit          = new MyButton("Exit");

               theModel    = new OutSideCottonGiModel();
               jTable      = new JTable(theModel);
     
               TableColumnModel TC  = jTable.getColumnModel();
     
               for(int i=0;i<theModel.ColumnName.length;i++)   {
     
                 TC .getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
               }

          } catch(Exception ex) {

               ex.printStackTrace();
          }
     }

     private void setLayouts()
     {
          setTitle("OutSide Godown Cotton Receipt");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(30,5,695,425);

          TopPanel      . setLayout(new java.awt.GridLayout(1,1));
          MiddlePanel   . setLayout(new java.awt.BorderLayout());
          BottomPanel   . setLayout(new java.awt.GridLayout(1,5));

          TopPanel      . setBorder(new TitledBorder("Title"));
          MiddlePanel   . setBorder(new TitledBorder("Data"));
          BottomPanel   . setBorder(new TitledBorder("Controls"));
     }

     private void addComponents()  {


          JLabel   ltitle   = null;
     
                   ltitle   = new JLabel("OUTSIDE GODOWN COTTON RECEIPT",
                                           javax.swing.JLabel.CENTER);
                   ltitle   . setForeground(new java.awt.Color(51,50,250,250));
                   ltitle   . setFont(new java.awt.Font("TimesRoman",
                                           java.awt.Font.BOLD, 14));

          TopPanel            . add (ltitle);
          MiddlePanel         . add (new javax.swing.JScrollPane(jTable));

          BottomPanel         . add (new javax.swing.JLabel(""));
          BottomPanel         . add (bOkay);
          BottomPanel         . add (new javax.swing.JLabel(""));
          BottomPanel         . add (bExit);
          BottomPanel         . add (new javax.swing.JLabel(""));

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
     }

     public void addActionListeners() {

          bExit . addActionListener  (

          new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent event)  {

               removeHelpFrame();
            }
          }
          );
          bOkay . addActionListener  (

               new java.awt.event.ActionListener() {

               public void actionPerformed(java.awt.event.ActionEvent event)  {

                    if(checkValid()) {

                         saveData();
                    }

                    setTableData();
               }
           }
          );
     }

     private void setTableData(){

          try  {
                           
               theVector = new Vector();

               setData();

               theModel  . setNumRows(0);

               for(int i=0;i<theVector.size();i++) {

                    Vector   V          = new Vector();

                    HashMap theMap      = (HashMap)theVector.get(i);


                    V  . addElement(common.parseDate((String)theMap.get("ISSUEDATE")));
                    V  . addElement(common.parseNull((String)theMap.get("LOTNO")));
                    V  . addElement(common.parseNull((String)theMap.get("VARNAME")));
                    V  . addElement(common.parseNull((String)theMap.get("PARTYNAME")));
                    V  . addElement(common.parseNull((String)theMap.get("BALES")));
                    V  . addElement(common.parseNull((String)theMap.get("QTY")));
                    V  . addElement("");
                    V  . addElement(new Boolean(false));

                    theModel.appendRow(V);
               }

          } catch(Exception ex) {

               ex.printStackTrace();
          }
     }
     private void removeHelpFrame()   {

          try   {

               Layer     . remove(this);
               Layer     . updateUI();
          }
          catch(Exception ex)  {
          }
     }

     private void setData() {


          try {

               String QS = "";


               QS = " select Distinct IssueDate,OutSidegodownIssue.LotNo,cottonVariety.VarName,Sum(Issuewt) as Qty,PartyMaster.PartyName, "+
                    " outsidegodownissue.varietycode,partymaster.partycode,count(*) as Bales "+
                    " from OutsidegodownIssue "+
                    " Inner join CottonVariety on CottonVariety.VarCode=OutSidegodownIssue.VarietyCode "+
                    " inner join PartyMaster on PartyMaster.Partycode = OutsidegodownIssue.GodownCode "+
                    " where FibreTypecode=-1  and OutSidegodownIssue.ReceiptStatus=0 Group by IssueDate,LotNo,VarName,outsidegodownissue.varietycode, "+
                    " PartyName,partycode Order by 1 ";

                    System.out.println(QS);

               if(theConnection == null)
               {
                    CMORAConnection jdbc = CMORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet result         = theStatement.executeQuery(QS);
               ResultSetMetaData rsmd   = result.getMetaData();

               while(result.next())
               {
                    HashMap row = new HashMap();

                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }                         
                    theVector.addElement(row);
               }
               result         . close();
               theStatement   . close();
               theConnection  = null;


          } catch(Exception ex) {

               ex.printStackTrace();
          }
     }

     private boolean checkValid() {


          for(int i=0;i<theModel.getRowCount();i++) {

               java.lang.Boolean bValue      = (java.lang.Boolean)theModel.getValueAt(i,7);

               if(bValue.booleanValue()) {

                    String STruck = common.parseNull((String)theModel.getValueAt(i,6));

                    if(!STruck.equals("")) {

                         return true;
                    }else {

                         JOptionPane.showMessageDialog(null,"Fill Truck No for Selected Row","Information",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }
               }
          }
          JOptionPane.showMessageDialog(null,"Please Select Any one Check Box for Authentication","Information",JOptionPane.ERROR_MESSAGE);
          return false;
     }

     private void saveData() {


          try {


               boolean bSave = false;
     
               for(int i=0;i<theModel.getRowCount();i++) {
     
                    java.lang.Boolean bValue      = (java.lang.Boolean)theModel.getValueAt(i,7);
     
                    HashMap theMap                = (HashMap)theVector.get(i);
     
                    if(bValue.booleanValue()) {
     
                         int iMaxNo=0;
                         iMaxNo    = getInventoryGINo();
                         iMaxNo    = iMaxNo+1;
     
                         String SGiNo   = String.valueOf(iMaxNo);
                         String SGiDt   = common.getServerPureDate();
                         String SACode  = common.parseNull((String)theMap.get("PARTYCODE"));
                         String SInvDt  = common.parseNull((String)theMap.get("ISSUEDATE"));
                         String SDcDt   = common.parseNull((String)theMap.get("ISSUEDATE"));
                         String STruck  = common.parseNull((String)theModel.getValueAt(i,6));
                         String STime   = common.getTime();
                         String SBales  = common.parseNull((String)theMap.get("BALES"));
                         String SLot    = common.parseNull((String)theMap.get("LOTNO"));
                         String SVariety= common.parseNull((String)theMap.get("VARIETYCODE"));
     
                         bSave = insertData(SGiNo,SGiDt,SACode,SInvDt,SDcDt,STruck,STime,SBales,SLot);
     
                         if(bSave==true) {

     
                              updateGiNo(SGiNo);
     
                              updateReceiptStatus(SACode,SInvDt,SLot,SVariety,SGiNo);
                         }else {
     
                              JOptionPane.showMessageDialog(null,"Data Not Saved ","Dear user,",JOptionPane.ERROR_MESSAGE);
                         }
     
                    }
               }
     
               if(bSave==true) {
     
                    JOptionPane.showMessageDialog(null,"Data Saved Sucessfully","Dear user,",JOptionPane.INFORMATION_MESSAGE);
               }

          } catch(Exception ex) {

               ex.printStackTrace();
          }
     }
     private void updateReceiptStatus(String SACode,String SInvDt,String SLot,String SVariety,String SGiNo) {


          try {

               String SQry = " Update OutSideGodownIssue set ReceiptStatus=?, GiNo=? where LotNo=? and VarietyCode=? and IssueDate=? and GodownCode=?   and FibreTypeCode=-1   ";
               

               if(theConnection == null)
               {
                    CMORAConnection jdbc = CMORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               PreparedStatement ps = theConnection.prepareStatement(SQry);

               ps                   . setInt      (1,1);
               ps                   . setString   (2,SGiNo);
               ps                   . setString   (3,SLot);
               ps                   . setString   (4,SVariety);
               ps                   . setString   (5,SInvDt);
               ps                   . setString   (6,SACode);
               ps                   . executeUpdate();
               ps                   . close();
               theConnection        = null;


          } catch(Exception ex) {

               ex.printStackTrace();
          }
     }

     private boolean insertData(String SGiNo,String SGiDt,String SACode,String SInvDt,String SDcDt,String STruck,String STime,String SBales,String SLot) {


          try {

               String SQry = "";

               SQry = " Insert into CottonGi (Id,GiNo,GiDate,ACCode,InvDate,DcDate,TruckNo,TimeIn,Bales, "+
                      " PartyLotNo,Status,AgnDate,AuthDate,DcjDate,seccode) Values ( GateInward_seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,? ) ";

               System.out.println("Qury--->"+SQry);

               if(theConnection == null)
               {
                    CMORAConnection jdbc = CMORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               System.out.println("Gi No-->"+SGiNo);
               System.out.println(" Dt --> "+SGiDt);
               System.out.println(" Accode "+SACode);
               System.out.println(" Dt -->"+SInvDt);
               System.out.println(" Dt -->"+SDcDt);
               System.out.println(" Truck -->"+STruck);
               System.out.println(" Time --> "+STime);
               System.out.println(" Bales --> "+SBales);
               System.out.println(" Lot --> "+SLot);

               PreparedStatement ps = theConnection.prepareStatement(SQry);

               ps                   . setString (1,SGiNo);
               ps                   . setString (2,SGiDt);
               ps                   . setString (3,SACode);
               ps                   . setString (4,SInvDt);
               ps                   . setString (5,SDcDt);
               ps                   . setString (6,STruck);
               ps                   . setString (7,STime);
               ps                   . setString (8,SBales);
               ps                   . setString (9,SLot);
               ps                   . setInt    (10,0);
               ps                   . setInt    (11,0);
               ps                   . setInt    (12,0);
               ps                   . setInt    (13,0);
               ps                   . setInt    (14,0);


               ps                   . executeUpdate();
               ps                   . close();
               theConnection        = null;


          } catch(Exception ex) {

               ex.printStackTrace();
               return false;
          }
          return true;
     }


     private int getInventoryGINo()
     {
          int iInvGINo =0;

          try
          {

               String QS = " Select maxno from Config06 where id=3 ";

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    iInvGINo = common.toInt(result.getString(1));
               }
               result         .close();
               theStatement   .close();
               theConnection  = null;


          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
          return iInvGINo;
     }
     private void updateGiNo(String SGiNo)
     {
          try
          {
               String QS = " Update Config06 set MaxNo = MaxNo+1  where Id = 3";

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();

               theStatement   . executeUpdate(QS);
               theStatement   . close();
               theConnection  = null;

          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

}

