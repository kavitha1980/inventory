package GateNew;

import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

public class OutSideCottonGiModel extends DefaultTableModel
{
     String    ColumnName[]   = {"IssueDate"      , "Lot No"     ,"VarietyName"      ,"Godown"                ,"Bales"  ,"Qty"              ,"Truck"  ,"Click"};
     String    ColumnType[]   = {"N"              , "N"          ,"S"                ,"S"                     ,"N"      ,"N"                ,"E"      ,"B"};
     int      iColumnWidth[]  = {70               , 55           ,90                 ,165                     ,50       ,50                 ,60       ,40};

     public OutSideCottonGiModel()
     {
          setDataVector(getRowData(),ColumnName);
     }
     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";

          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}
