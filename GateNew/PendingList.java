package GateNew;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import guiutil.*;
import util.*;
import jdbc.*;

public class PendingList implements ActionListener
{
     JLayeredPane  Layer;
     GIMiddlePanel MiddlePanel;
     JTextField    TSupCode;
     JButton       BSupplier;
     int           iMillCode;

     Vector        VSupCode;
     MyComboBox    JCSupplier;
     GIModiFrame   gimodiframe;
     boolean       bflag;          // true  - for Modification &
                                   // false - for Entry

     JList          BrowList,SelectedList;
     JScrollPane    BrowScroll,SelectedScroll;
     JTextField     TIndicator;
     JButton        BOk;
     JPanel         LeftPanel,RightPanel;
     JInternalFrame MaterialFrame;
     JPanel         MFMPanel,MFBPanel;

     String SSupCode = "";

     Vector VPendCode,VPendName;
     Vector VSelectedName,VSelectedCode;
     String str="";
     int iMFSig=0;
     ActionEvent ae;
     Common common = new Common();

     // Contructor for Entry Frame
     PendingList(JLayeredPane Layer,GIMiddlePanel MiddlePanel,JTextField TSupCode,JButton BSupplier,boolean bflag,int iMillCode)
     {
          this.Layer         = Layer;
          this.MiddlePanel   = MiddlePanel;
          this.TSupCode      = TSupCode;
          this.BSupplier     = BSupplier;
          this.bflag         = bflag;
          this.iMillCode     = iMillCode;
     }

     // Constructor for Modification Frame
     PendingList(JLayeredPane Layer,Vector VSupCode,MyComboBox JCSupplier,GIModiFrame gimodiframe,boolean bflag,int iMillCode)
     {
          this.Layer         = Layer;
          this.VSupCode      = VSupCode;
          this.JCSupplier    = JCSupplier;
          this.gimodiframe   = gimodiframe;
          this.bflag         = bflag;
          this.iMillCode     = iMillCode;
     }

     public void createComponents()
     {
          
          VPendCode     = new Vector();
          VPendName     = new Vector();

          VSelectedCode = new Vector();
          VSelectedName = new Vector();

          BrowList      = new JList(getPendName());
          SelectedList  = new JList();
          BrowScroll    = new JScrollPane(BrowList);
          SelectedScroll= new JScrollPane(SelectedList);
          LeftPanel     = new JPanel(true);
          RightPanel    = new JPanel(true);
          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator.setEditable(false);
          MFMPanel      = new JPanel(true);
          MFBPanel      = new JPanel(true);
          MaterialFrame = new JInternalFrame("Materials Expected From this Supplier");
          MaterialFrame.show();
          MaterialFrame.setBounds(80,100,550,350);
          MaterialFrame.setClosable(true);
          MaterialFrame.setResizable(true);
          BrowList.addKeyListener(new KeyList());
          BrowList.requestFocus();
          BOk.setMnemonic('O');
          //iMFSig=0;
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent e)
          {
               if(VSelectedCode.size()==0)
               {
                    JOptionPane.showMessageDialog(null,"No Material is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                    BrowList.requestFocus();
                    return;
               }
               BOk.setEnabled(false);
               if(!bflag)
               {
                    setMiddlePanel();
                    BSupplier.setEnabled(false);
               }
               else
               {
                    gimodiframe.setMiddlePanel(VSelectedCode,VSelectedName);
                    JCSupplier.setEnabled(false);
               }
               removeHelpFrame();
               ((JButton)ae.getSource()).setEnabled(false);
               if(!bflag)
               {
                    MiddlePanel.MiddlePanel.ReportTable.requestFocus();
               }
               if(bflag)
               {
                    gimodiframe.ReportTable.requestFocus();
               }
               
               //((JButton)ae.getSource()).requestFocus();
               str="";
          }
     }

     public void actionPerformed(ActionEvent ae)
     {
          this.ae = ae;
          JButton source = (JButton)ae.getSource();
          createComponents();

          if(VPendName.size()==0)
          {
               JOptionPane.showMessageDialog(null,"No Materials are Pending","Information",JOptionPane.INFORMATION_MESSAGE);
               if(!bflag)
               {
                    BSupplier.requestFocus();
               }
               else
               {
                    JCSupplier.requestFocus();
               }
               return;
          }

          TIndicator.setText(str);
          BOk.setEnabled(true);
          source.setEnabled(false);

          if(iMFSig==0)
          {
               MFMPanel.setLayout(new GridLayout(1,2));
               MFBPanel.setLayout(new GridLayout(1,2));
               MFMPanel.add(BrowScroll);
               MFMPanel.add(SelectedScroll);
               MFBPanel.add(TIndicator);
               MFBPanel.add(BOk);
               BOk.addActionListener(new ActList());
               MaterialFrame.getContentPane().add("Center",MFMPanel);
               MaterialFrame.getContentPane().add("South",MFBPanel);
               iMFSig=1;
          }
          removeHelpFrame();
          try
          {
               Layer.add(MaterialFrame);
               MaterialFrame.moveToFront();
               MaterialFrame.setSelected(true);
               MaterialFrame.show();
               BrowList.requestFocus();
               Layer.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);

               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='&') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }
          
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SMatName = (String)VPendName.elementAt(index);
                    String SMatCode = (String)VPendCode.elementAt(index);
                    addMatDet(SMatName,SMatCode);
                    str="";
                    TIndicator.setText(str);
               }
               if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
               {
                    if(VSelectedCode.size()==0)
                    {
                         JOptionPane.showMessageDialog(null,"No Material is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                         BrowList.requestFocus();
                         return;
                    }
                    if(!bflag)
                    {
                         setMiddlePanel();
                         BSupplier.setEnabled(false);
                    }
                    else
                    {
                         gimodiframe.setMiddlePanel(VSelectedCode,VSelectedName);
                         JCSupplier.setEnabled(false);
                    }
                    removeHelpFrame();
                    ((JButton)ae.getSource()).setEnabled(false);
                    str="";
               }
          }
     }

     public void setCursor()
     {
          int index=0;
          TIndicator.setText(str);

          for(index=0;index<VPendName.size();index++)
          {
               String str1 = ((String)VPendName.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedValue(str1,true);   
                    break;
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(MaterialFrame);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

     public boolean addMatDet(String SMatName,String SMatCode)
     {
          int iIndex=VSelectedCode.indexOf(SMatCode);
          if (iIndex==-1)
          {
               VSelectedName.addElement(SMatName);
               VSelectedCode.addElement(SMatCode);
          }
          else
          {
               VSelectedName.removeElementAt(iIndex);
               VSelectedCode.removeElementAt(iIndex);
          }
          SelectedList.setListData(VSelectedName);
          return true;
     }

     public void setMiddlePanel()
     {
          MiddlePanel.setRowData(VSelectedCode,VSelectedName);
          MiddlePanel.createComponents();
     }

     public Vector getPendName()
     {
          VPendName.removeAllElements();
          VPendCode.removeAllElements();

          if(!bflag)
          {
               SSupCode = TSupCode.getText();
          }
          else
          {
               SSupCode = (String)VSupCode.elementAt(JCSupplier.getSelectedIndex());
          }

          String QS = "Select PurchaseOrder.Item_Code,InvItems.Item_Name,sum(PurchaseOrder.Qty-PurchaseOrder.InvQty) as Pending "+
                      "From PurchaseOrder Inner Join InvItems On InvItems.Item_Code = PurchaseOrder.Item_Code and PurchaseOrder.MillCode="+iMillCode+
                      "Group By PurchaseOrder.Item_Code,InvItems.Item_Name,PurchaseOrder.Sup_Code,PurchaseOrder.Qty,PurchaseOrder.InvQty "+
                      "Having (PurchaseOrder.InvQty<PurchaseOrder.Qty) and PurchaseOrder.Sup_Code = '"+SSupCode+"'"+
                      "Order By 2";

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    double dPending = common.toDouble(theResult.getString(3));
                    if(dPending > 0)
                    {
                         VPendCode.addElement(theResult.getString(1));
                         VPendName.addElement(theResult.getString(2));
                    }
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return VPendName;
     }

}
