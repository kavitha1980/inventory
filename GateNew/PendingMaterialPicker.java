package GateNew;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;
import util.*;
import guiutil.*;
public class PendingMaterialPicker extends JInternalFrame
{
      JTextField     TMatCode,TMatName;
      JTextField     TIndicator;
      JButton        BOk,BCancel;
                  
      JList          BrowList;
      JScrollPane    BrowScroll;
      JPanel         LeftPanel;
      JPanel         LeftCenterPanel,LeftBottomPanel;

      JLayeredPane   Layer;
      Vector         VPendName,VPendCode;
      JTable         ReportTable;
      GateTableModel GateModel;

      Vector         VNameCode;
      String         SMName,SMCode;

      String str="";

      int iLastIndex = 0;
      Common common = new Common();
      PendingMaterialPicker(JLayeredPane Layer,Vector VPendCode,Vector VPendName,JTable ReportTable,GateTableModel GateModel)
      {
          this.Layer       = Layer;
          this.VPendCode   = VPendCode;
          this.VPendName   = VPendName;
          this.ReportTable = ReportTable;
          this.GateModel   = GateModel;
          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
      }
      public void createComponents()
      {
          VNameCode     = new Vector();
          for (int i=0;i<VPendCode.size();i++)
          {
               SMName = (String)VPendName.elementAt(i);
               SMCode = (String)VPendCode.elementAt(i);
               VNameCode.addElement(SMName+" (Code : "+SMCode+")");
          }
          BrowList      = new JList(VNameCode);
          BrowScroll    = new JScrollPane();
          TMatCode      = new JTextField();
          TMatName      = new JTextField();
          TIndicator    = new JTextField();
          TIndicator.setEditable(false);

          BOk           = new JButton("Okay");
          BCancel       = new JButton("Cancel");

          LeftPanel        = new JPanel(true);
          LeftCenterPanel  = new JPanel(true);
          LeftBottomPanel  = new JPanel(true);

          TMatCode.setEditable(false);
          TMatName.setEditable(false);
          BOk.setMnemonic('O');
          BCancel.setMnemonic('C');
      }
      public void setLayouts()
      {
          setBounds(80,50,500,300);
          setResizable(true);
          setClosable(true);
          setTitle("Material Modification");
          getContentPane().setLayout(new BorderLayout());
          LeftPanel.setLayout(new BorderLayout());
          LeftCenterPanel.setLayout(new BorderLayout());
          LeftBottomPanel.setLayout(new GridLayout(3,2));
      }
      public void addComponents()
      {
          getContentPane().add("Center",LeftPanel);

          LeftPanel.add("Center",LeftCenterPanel);
          LeftPanel.add("South",LeftBottomPanel);

          LeftCenterPanel.add("Center",BrowScroll);
          LeftCenterPanel.add("South",TIndicator);
          LeftBottomPanel.add(new JLabel("Material Name"));
          LeftBottomPanel.add(TMatName);
          LeftBottomPanel.add(new JLabel("Material Code"));
          LeftBottomPanel.add(TMatCode);
          LeftBottomPanel.add(BOk);
          LeftBottomPanel.add(BCancel);
      }
      public void setPresets()
      {
          int i = ReportTable.getSelectedRow();
          String SNameCode="";
          String SCode   = (String)ReportTable.getModel().getValueAt(i,0);
          String SName   = (String)ReportTable.getModel().getValueAt(i,1);
          int iindex     = common.indexOf(VPendCode,SCode);
          if(iindex > -1)
                 SNameCode = (String)VNameCode.elementAt(iindex);

          BrowList.setAutoscrolls(true);
          BrowScroll.getViewport().setView(BrowList);
          TMatCode.setText(SCode);
          TMatName.setText(SName);
          show();
          ensureIndexIsVisible(SNameCode);
          LeftCenterPanel.updateUI();
      }
      public void addListeners()
      {
          BrowList.addKeyListener(new KeyList());
          BOk.addActionListener(new ActList());
          BCancel.addActionListener(new ActList());
          addMouseListener(new MouseList());
      }
      public class MouseList extends MouseAdapter
      {
          public void mouseEntered(MouseEvent me)
          {
               BrowList.ensureIndexIsVisible(iLastIndex);               
          }
      }
      public class ActList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    int i = ReportTable.getSelectedRow();
                    ReportTable.getModel().setValueAt(TMatCode.getText(),i,0);
                    ReportTable.getModel().setValueAt(TMatName.getText(),i,1);

	    	    GateModel.setColumnData();
               }
               removeHelpFrame();
               ReportTable.requestFocus();
          }
      }
      public class KeyList extends KeyAdapter
      {
             public void keyReleased(KeyEvent ke)
             {
                  char lastchar=ke.getKeyChar();
                  lastchar=Character.toUpperCase(lastchar);
                  try
                  {
                     if(ke.getKeyCode()==8)
                     {
                        str=str.substring(0,(str.length()-1));
                        setCursor();
                     }
                     else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                     {
                        str=str+lastchar;
                        setCursor();
                     }
                  }
                  catch(Exception ex){}
             }
             public void keyPressed(KeyEvent ke)
             {
                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                     int index = BrowList.getSelectedIndex();
                     String SMatName = (String)VPendName.elementAt(index);
                     String SMatCode = (String)VPendCode.elementAt(index);
                     String SMatNameCode = (String)VNameCode.elementAt(index);
                     addMatDet(SMatName,SMatCode,SMatNameCode,index);
                     str="";
                  }
             }
         }
         public void setCursor()
         {
            TIndicator.setText(str);            
            int index=0;
            for(index=0;index<VNameCode.size();index++)
            {
                 String str1 = ((String)VNameCode.elementAt(index)).toUpperCase();
                 if(str1.startsWith(str))
                 {
                      //BrowList.setSelectedValue(str1,true);
                      BrowList.setSelectedIndex(index);
                      BrowList.ensureIndexIsVisible(index);
                      break;
                 }
            }
         }
         public void removeHelpFrame()
         {
            try
            {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
            }
            catch(Exception ex) { }
         }
         public boolean addMatDet(String SMatName,String SMatCode,String SMatNameCode,int index)
         {
               TMatCode.setText(SMatCode);
               TMatName.setText(SMatName);
               return true;    
         }
         public boolean ensureIndexIsVisible(String SNameCode)
         {
               SNameCode = SNameCode.trim();
               int i=0;
               for(i=0;i<VNameCode.size();i++)
               {
                   String SCurName = (String)VNameCode.elementAt(i);
                   SCurName = SCurName.trim();
                   if(SCurName.startsWith(SNameCode))
                        break;
               }
               if(i==VNameCode.size())
               {
                    return false;
               }
               iLastIndex=i;
               BrowList.setSelectedIndex(i);
               BrowList.ensureIndexIsVisible(i);
               BrowList.requestFocus();
               BrowList.updateUI();
               return true;
         }

}
