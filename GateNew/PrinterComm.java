/*
    This Class Is Used to Print Data Directlly to Printer
*/

package GateNew;

import javax.comm.*;

import java.util.*;
import java.io.*;

import util.*;
import guiutil.*;

public class PrinterComm 
{
     String SPort = "LPT1";

     PrinterComm()
     {
     }

     public void CallPrinter(File fileName)
     {

          try
          {
               ParallelPort parallelPort = getParallelPort();
               OutputStream output = parallelPort.getOutputStream();
               FileInputStream fis= new FileInputStream(fileName);
               byte b[] = new byte[fis.available()];
               fis.read(b,0,fis.available());
               output.write(b);
               parallelPort.close();
          }
          catch(Exception ex)
          {
               System.out.println("doPrint : "+ex);
               ex.printStackTrace();
          }
     }

     private ParallelPort getParallelPort() 
     {
          ParallelPort parallelPort= null;
          try
          {

               Enumeration portList = CommPortIdentifier.getPortIdentifiers();
               System.out.println(portList.hasMoreElements()+","+CommPortIdentifier.getPortIdentifiers());

               while(portList.hasMoreElements())
               {
                    CommPortIdentifier portId = (CommPortIdentifier)portList.nextElement();

                    System.out.println(portId.getPortType()+","+portId.getName());
                    if (portId.getPortType() == CommPortIdentifier.PORT_PARALLEL && portId.getName().equals(SPort))
                    {
                         parallelPort = (ParallelPort)portId.open("Par",2000);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return parallelPort;
     }
}
