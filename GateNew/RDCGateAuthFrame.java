package GateNew;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCGateAuthFrame extends JInternalFrame
{
     JPanel     TopPanel;
     JPanel     BottomPanel;
     JPanel     MiddlePanel;

     JButton    BOk;
    
     Object RowData[][];
     String ColumnData[] = {"Doc Type","RDC Date","RDC No","Supplier","Descript","Qty"}; 
     String ColumnType[] = {"S"       ,"S"       ,"S"     ,"S"       ,"S"       ,"N"  }; 

     JLayeredPane DeskTop;
     StatusPanel SPanel;
     int iUserCode,iAuthCode,iMillCode;
     String SRDCNo;
     RDCPendingListFrame rdcpendinglistframe;
     String SSupTable;

     TabReport tabreport;
     ORAConnection connect;
     Connection theconnect;
     Common common = new Common();

     Vector VDocType,VRDCDate,VRDCNo,VSupName,VAcCode,VDesc,VQty;

     RDCGateAuthFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iAuthCode,int iMillCode,String SRDCNo,RDCPendingListFrame rdcpendinglistframe,String SSupTable)
     {
          super("List of RDC Materials for Authentication");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iAuthCode = iAuthCode;
          this.iMillCode = iMillCode;
          this.SRDCNo    = SRDCNo;
          this.rdcpendinglistframe = rdcpendinglistframe;
          this.SSupTable = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setTabReport();
     }

     private void createComponents()
     {
          TopPanel    = new JPanel(true);
          BottomPanel = new JPanel(true);
          MiddlePanel = new JPanel(true);

          BOk         = new JButton("Authenticate");

          BOk.setMnemonic('A');
     }

     private void setLayouts()
     {
          TopPanel.setLayout(new FlowLayout());
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
     }

     private void addComponents()
     {
          BottomPanel.add(BOk);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

     }

     private void addListeners()
     {
          BOk.addActionListener(new ActList());
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               saveData();
               rdcpendinglistframe.setTabReport();
               removeHelpFrame();
          }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     public void saveData()
     {
         try
         {
               String SDateTime = common.getServerDate();

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       theStatement  =  theConnection.createStatement();

               String QS = " Update RDC set GateAuth=1,GateAuthTime='"+SDateTime+"' Where RDCNo="+SRDCNo+" and MillCode="+iMillCode;

               theStatement.execute(QS);
               theStatement.close();
         }
         catch(Exception ex){}
     }
     public void setTabReport()
     {
               boolean bflag = setDataIntoVector();
               if(!bflag)
                    return;
               setVectorIntoRowData();
               try
               {
                    MiddlePanel.remove(tabreport);
                    MiddlePanel.updateUI();
               }
               catch(Exception ex){}
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
               MiddlePanel.add("Center",tabreport);
               MiddlePanel.updateUI();
     }

     private boolean setDataIntoVector()
     {
          boolean bflag = false;
          VDocType  = new Vector();
          VRDCDate  = new Vector();
          VRDCNo    = new Vector();
          VSupName  = new Vector();
          VAcCode   = new Vector();
          VDesc     = new Vector();
          VQty      = new Vector();

          String QS = " Select RDC.DocType,RDC.RDCDate,RDC.RDCNo,"+SSupTable+".Name, "+
                      " RDC.Sup_Code,RDC.Descript,RDC.Qty "+
                      " From RDC "+
                      " Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = RDC.Sup_Code "+
                      " Where RDC.RDCNo="+SRDCNo+
                      " And RDC.GateAuth=0 And RDC.MillCode="+iMillCode+
                      " Order By 2,3 ";
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res  = stat.executeQuery(QS);
               while(res.next())
               {
                    int iDocType = common.toInt(res.getString(1));

                    if(iDocType==0)
                    {
                         VDocType.addElement("RDC");
                    }
                    else
                    {
                         VDocType.addElement("NRDC");
                    }

                    VRDCDate  .addElement(""+res.getString(2));
                    VRDCNo    .addElement(""+res.getString(3));
                    VSupName  .addElement(""+res.getString(4));
                    VAcCode   .addElement(""+res.getString(5));
                    VDesc     .addElement(""+res.getString(6));
                    VQty      .addElement(""+res.getString(7));
               }
               res.close();
               stat.close();
               bflag = true;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bflag = false;
          }
          return bflag;
     }

     private void setVectorIntoRowData()
     {
          RowData = new Object[VRDCNo.size()][ColumnData.length];

          for(int i=0;i<VRDCDate.size();i++)
          {
               RowData[i][0]  = (String)VDocType  .elementAt(i);
               RowData[i][1]  = common.parseDate((String)VRDCDate  .elementAt(i));
               RowData[i][2]  = (String)VRDCNo    .elementAt(i);
               RowData[i][3]  = (String)VSupName  .elementAt(i);
               RowData[i][4]  = (String)VDesc     .elementAt(i);
               RowData[i][5]  = (String)VQty      .elementAt(i);
          }
     }
}
