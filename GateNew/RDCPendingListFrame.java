package GateNew;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCPendingListFrame extends JInternalFrame
{
     JPanel     TopPanel;
     JPanel     BottomPanel;
     JPanel     MiddlePanel;

     String     SDate;
     JButton    BApply;
    
     DateField TDate;
    
     Object RowData[][];
     String ColumnData[] = {"Doc Type","RDC Date","RDC No","Supplier"}; 
     String ColumnType[] = {"S"       ,"S"       ,"S"     ,"S"       }; 

     JLayeredPane DeskTop;
     StatusPanel SPanel;
     int iUserCode,iAuthCode,iMillCode;
     String SSupTable,SMillName;

     TabReport tabreport;
     ORAConnection connect;
     Connection theconnect;
     Common common = new Common();

     Vector VDocType,VRDCDate,VRDCNo,VSupName,VAcCode,VOutPassStatus;

     RDCPendingListFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iAuthCode,int iMillCode,String SSupTable,String SMillName)
     {
          super("List of RDC Not Sent As On today");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iAuthCode = iAuthCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SMillName = SMillName;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          TopPanel    = new JPanel(true);
          BottomPanel = new JPanel(true);
          MiddlePanel = new JPanel(true);

          TDate       = new DateField();
          BApply      = new JButton("Apply");

          TDate.setTodayDate();
          TDate.setEditable(false);

          BApply.setMnemonic('A');
     }

     private void setLayouts()
     {
          TopPanel.setLayout(new FlowLayout());
          MiddlePanel.setLayout(new BorderLayout());

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
     }

     private void addComponents()
     {
          TopPanel.add(new JLabel("As On"));
          TopPanel.add(TDate);
          TopPanel.add(BApply);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

     }

     private void addListeners()
     {
          BApply.addActionListener(new ActList());
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setTabReport();
               printData();
          }
     }
     public void setTabReport()
     {
               boolean bflag = setDataIntoVector();
               if(!bflag)
                    return;
               setVectorIntoRowData();
               try
               {
                    MiddlePanel.remove(tabreport);
                    MiddlePanel.updateUI();
               }
               catch(Exception ex){}
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
               tabreport.ReportTable.addKeyListener(new KeyList(this));
               MiddlePanel.add("Center",tabreport);
               MiddlePanel.updateUI();
     }
     public class KeyList extends KeyAdapter
     {
          RDCPendingListFrame rdcpendinglistframe;
          public KeyList(RDCPendingListFrame rdcpendinglistframe)
          {
               this.rdcpendinglistframe = rdcpendinglistframe;
          }
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    try
                    {
                         int iRow = tabreport.ReportTable.getSelectedRow();
                         String SRDCNo = (String)tabreport.ReportTable.getValueAt(iRow,2);
                         String SOutPassStatus = (String)VOutPassStatus.elementAt(iRow);

                         if(SOutPassStatus.equals("0"))
                         {
                              JOptionPane.showMessageDialog(null,"OutPass Not Created for this RDC","Error",JOptionPane.ERROR_MESSAGE);
                              tabreport.ReportTable.requestFocus();
                              return;
                         }
                         else
                         {
                              RDCGateAuthFrame rdcgateauthframe = new RDCGateAuthFrame(DeskTop,SPanel,iUserCode,iAuthCode,iMillCode,SRDCNo,rdcpendinglistframe,SSupTable);
     
                              try
                              {
                                   DeskTop.add(rdcgateauthframe);
                                   DeskTop.repaint();
                                   rdcgateauthframe.setSelected(true);
                                   DeskTop.updateUI();
                                   rdcgateauthframe.show();
                              }
                              catch(Exception e)
                              {
                                   e.printStackTrace();     
                              }
                         }
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
          }
     }

     private boolean setDataIntoVector()
     {
          boolean bflag  = false;
          VDocType       = new Vector();
          VRDCDate       = new Vector();
          VRDCNo         = new Vector();
          VSupName       = new Vector();
          VAcCode        = new Vector();
          VOutPassStatus = new Vector();

          String SDate = TDate.toNormal();

          String QS = " Select RDC.DocType,RDC.RDCDate,RDC.RDCNo,"+SSupTable+".Name, "+
                      " RDC.Sup_Code,RDC.OutPassStatus "+
                      " From RDC "+
                      " Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = RDC.Sup_Code "+
                      " Where RDC.RDCDate<='"+SDate+"' And RDC.AssetFlag=0 "+
                      " And RDC.GateAuth=0 And RDC.MillCode="+iMillCode+
                      " Group by RDC.DocType,RDC.RDCDate,RDC.RDCNo,"+SSupTable+".Name,RDC.Sup_Code,RDC.OutPassStatus "+
                      " Order By 2,3 ";
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res  = stat.executeQuery(QS);
               while(res.next())
               {
                    int iDocType = common.toInt(res.getString(1));

                    if(iDocType==0)
                    {
                         VDocType.addElement("RDC");
                    }
                    else
                    {
                         VDocType.addElement("NRDC");
                    }

                    VRDCDate  .addElement(""+res.getString(2));
                    VRDCNo    .addElement(""+res.getString(3));
                    VSupName  .addElement(""+res.getString(4));
                    VAcCode   .addElement(""+res.getString(5));
                    VOutPassStatus.addElement(""+res.getString(6));
               }
               res.close();
               stat.close();
               bflag = true;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bflag = false;
          }
          return bflag;
     }

     private void setVectorIntoRowData()
     {
          RowData = new Object[VRDCNo.size()][ColumnData.length];

          for(int i=0;i<VRDCDate.size();i++)
          {
               RowData[i][0]  = (String)VDocType  .elementAt(i);
               RowData[i][1]  = common.parseDate((String)VRDCDate  .elementAt(i));
               RowData[i][2]  = (String)VRDCNo    .elementAt(i);
               RowData[i][3]  = (String)VSupName  .elementAt(i);
          }
     }

     public void printData()
     {
            String STitle = "List of RDC Not Sent As On "+TDate.toString();

            String SFile  = "RdcNotSent.prn";
            new DocPrint(getPendingBody(),getPendingHead(),STitle,SFile,iMillCode,SMillName);
     }

     public Vector getPendingBody()
     {
        Vector vect = new Vector();

        for(int i=0;i<VDocType.size();i++)
        {
              String strl="";

              String SDocType = (String)VDocType.elementAt(i);
              String SRDCNo   = (String)VRDCNo.elementAt(i);
              String SRDCDate = common.parseDate((String)VRDCDate.elementAt(i));
              String SSupName = (String)VSupName.elementAt(i);

              strl = strl + common.Cad(String.valueOf(i+1),5)+common.Space(3);
              strl = strl + common.Pad(SDocType,8)+common.Space(3);
              strl = strl + common.Rad(SRDCNo,10)+common.Space(3);
              strl = strl + common.Pad(SRDCDate,10)+common.Space(3);
              strl = strl + common.Pad(SSupName,30)+common.Space(3);

              strl = strl+"\n";
              vect.addElement(strl);
        }
        return vect;
     }

     public Vector getPendingHead()
     {
           Vector vect = new Vector();

           String strl1 = "";

           strl1 = strl1 + common.Cad("S.No.",5)+common.Space(3);
           strl1 = strl1 + common.Cad("RDC Type",8)+common.Space(3);
           strl1 = strl1 + common.Cad("RDC Number",10)+common.Space(3);
           strl1 = strl1 + common.Cad("RDC Date",10)+common.Space(3);
           strl1 = strl1 + common.Cad("Party Name",30)+common.Space(3);

           strl1 = strl1+"\n";

           vect.addElement(strl1);

           return vect;
     }

}
