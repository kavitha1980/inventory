package GateNew;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.io.*;
import java.sql.*;
import guiutil.*;
import util.*;
import jdbc.*;

public class RGIPrintClass
{
     FileWriter     FW; 
     String         SSupName,SGINo,SGIDate,STimeIn,SInvoiceNo,SInvoiceDate,SDCNo,SDCDate,SInwardType,STruckNo,SRMType,SBales,SWeight,SRemarks;
     int            iMillCode;
     String         SCategory = "";

     Common         common    = new Common();

     public RGIPrintClass(FileWriter FW,String SSupName,String SGINo,String SGIDate,String STimeIn,String SInvoiceNo,String SInvoiceDate,String SDCNo,String SDCDate,String SInwardType,String STruckNo,String SRMType,String SBales,String SWeight,String SRemarks,int iMillCode)
     {
          this.FW             = FW;
          this.SSupName       = SSupName;
          this.SGINo          = SGINo;
          this.SGIDate        = SGIDate;
          this.STimeIn        = STimeIn;
          this.SInvoiceNo     = SInvoiceNo;
          this.SInvoiceDate   = SInvoiceDate;
          this.SDCNo          = SDCNo;
          this.SDCDate        = SDCDate;
          this.SInwardType    = SInwardType;
          this.STruckNo       = STruckNo;
          this.SRMType        = SRMType;
          this.SBales         = SBales;
          this.SWeight        = SWeight;
          this.SRemarks       = SRemarks;
          this.iMillCode      = iMillCode;

          setPrn();
     }

     private void setPrn()
     {
          try
          {
               FW.write(toPrnHead().toString());

               FW.write("|     |           |                                          |            |            |            |\n");
               FW.write("|   1 |           | "+common.Pad(SRMType,41)+"|"+common.Rad(SBales,11)+" |            |"+common.Rad(common.getRound(SWeight,3),11)+" |\n");
               FW.write("|     |           |                                          |            |            |            |\n");

               FW.write(toPrnFoot().toString());
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private StringBuffer toPrnHead()
     {
          StringBuffer SB = new StringBuffer();

          SB.append("\n");

          if(iMillCode==3)
          {
               SB.append("M Amarjothi Colour Melange Spinning Mills Limited, NambiyurM\n");
          }
          if(iMillCode==4)
          {
               SB.append("M Sri Kamadhenu Textiles., NambiyurM\n");
          }

          SB.append("\n");
          SB.append("|---------------------------------------------------------------------------------------------------|\n");
          SB.append("|                                                                                                   |\n");
          SB.append("|                                  GATE INWARD OF RAWMATERIAL GOODS                                 |\n");
          SB.append("|                                                                                                   |\n");
          SB.append("|---------------------------------------------------------------------------------------------------|\n");
          SB.append("|                                                         |                                         |\n");
          SB.append("| Party Name   : "+common.Pad(SSupName,40)+" | Gate Inward Number : "+common.Pad(SGINo,18)+" |\n");
          SB.append("|                                                         |             Date   : "+common.Pad(SGIDate,18)+" |\n");
          SB.append("|                                                         |             Time   : "+common.Pad(STimeIn,18)+" |\n");
          SB.append("|                                                         |                                         |\n");
          SB.append("|---------------------------------------------------------------------------------------------------|\n");
          SB.append("|                                                         |                                         |\n");
          SB.append("| Invoice Number : "+common.Pad(SInvoiceNo,38)+" | D.C. Number : "+common.Pad(SDCNo,25)+" |\n");
          SB.append("| Invoice Date   : "+common.Pad(SInvoiceDate,38)+" | D.C. Date   : "+common.Pad(SDCDate,25)+" |\n");
          SB.append("|                                                         |                                         |\n");
          SB.append("|---------------------------------------------------------------------------------------------------|\n");
          SB.append("|                                                         |                                         |\n");
          SB.append("| Inward Type    : "+common.Pad(SInwardType,38)+" | Inward Mode : "+common.Pad(STruckNo,25)+" |\n");
          SB.append("|                                                         |                                         |\n");
          SB.append("|---------------------------------------------------------------------------------------------------|\n");
          SB.append("|                                                                                                   |\n");
          SB.append("| Category       : "+common.Pad(SCategory,80)+" |\n");
          SB.append("|                                                                                                   |\n");
          SB.append("|---------------------------------------------------------------------------------------------------|\n");
          SB.append("|     |           |                                          |            |            |            |\n");
          SB.append("| Sl. | Material  |             Material Name                | Number of  |  Measured  | Unmeasured |\n");
          SB.append("| No. |   Code    |                                          |   Bales    |  Qty @Gate |  Quantity  |\n");
          SB.append("|     |           |                                          |            |            |            |\n");
          SB.append("|---------------------------------------------------------------------------------------------------|\n");

          return SB;
     }

     private StringBuffer toPrnFoot()
     {
          String SDateTime    = common.getServerDate();
          String SDate        = common.parseDate(SDateTime.substring(0,8));
          String STime        = SDateTime.substring(9,SDateTime.length());

          SDateTime           = "";
          SDateTime           = SDate+" "+STime;

          StringBuffer SB = new StringBuffer();

          SB.append("|---------------------------------------------------------------------------------------------------|\n");
          SB.append("|                                                                                                   |\n");
          SB.append("| Remarks : "+common.Pad(SRemarks,87)+" |\n");
          SB.append("|                                                                                                   |\n");
          SB.append("|                                                                                                   |\n");
          SB.append("|                                                                                                   |\n");
          SB.append("|                                                                                                   |\n");
          SB.append("|                      Security                                    Security Officer                 |\n");
          SB.append("|                                                                                                   |\n");
          SB.append("----------------------------------------------------------------------------------------------------|\n");
          SB.append("< Report Taken on "+SDateTime+" >\n");
          SB.append("< End of Report >\n");
          SB.append("\n");

          return SB;
     }
}
