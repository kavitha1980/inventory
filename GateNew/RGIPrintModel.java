package GateNew;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.io.*;
import java.sql.*;
import guiutil.*;
import util.*;
import jdbc.*;

public class RGIPrintModel extends DefaultTableModel
{
     String ColumnName[]      = {"Click", "G.I.No.", "Date", "Supplier", "InvoiceNo", "InvDate", "DC No", "DC Date", "Inward Type", "Inward Mode", "Category", "Remarks" };
     String ColumnType[]      = {"B"    , "S"      , "S"   ,"S"        , "S"        , "S"      , "S"    , "S"      , "S"          , "S"          , "S"       , "S"       };
     int  iColumnWidth[]      = {20     , 40       , 50    , 100       , 50         , 50       , 50     , 50       , 60           , 60           , 30        , 30        };

     public RGIPrintModel()
     {
          setDataVector(getRowData(),ColumnName);
     }

     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol]=="B" || ColumnType[iCol]=="E")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
          {
               RowData[0][i] = "";
          }

          return RowData;
     }

     public void appendRow(Vector theVector)
     {
          insertRow(getRows(),theVector);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}
