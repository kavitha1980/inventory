package GateNew;
import util.*;
import guiutil.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import util.*;
import guiutil.*;
import jdbc.*;

public class GIListFrame extends JInternalFrame
{

     GICritPanel TopPanel;
     JPanel MiddlePanel,BottomPanel;

     JButton BExit,BPrint;
     MyTextField TFile;

     String SInt = "  ";
     String Strline = "";
     int iLen=0;

     TabReport   tabreport;

     Object RowData[][];
     String ColumnData[] = {"G.I. No","G.I.Date","Supplier","Inward Type","Mat Code","Mat Name","Inv Qty","Gate Qty","Unmeasure Qty","Invoice No","Inv Date","DC No","DC Date","Inward Mode","Category","Status","Remarks"};
     String ColumnType[] = {"N"      ,"S"       ,"S"       ,"S"          ,"S"       ,"S"       ,"N"      ,"N"       ,"N"            ,"S"         ,"S"       ,"S"    ,"S"      ,"S"          ,"S"       ,"S"     ,"S"};

     Vector VTitle;

     // Vectors for TabReport
     Vector VGINo,VGIDate,VSupName,VInwType,VMCode,VMName,VInvQty,VGateQty,VUnmeasure,VInvNo,VInvDate,VDCNo,VDCDate,VInwMode,VCategory,VStatus,VRemarks;

     Common common = new Common();

     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;
     int iUserCode,iAuthCode,iMillCode;

     GIListFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iAuthCode,int iMillCode)
     {
         super("Gate Inward Details with Constraints");

         this.DeskTop   = DeskTop;
         this.SPanel    = SPanel;
         this.VCode     = VCode;
         this.VName     = VName;
         this.iUserCode = iUserCode;
         this.iAuthCode = iAuthCode;
         this.iMillCode = iMillCode;

         setTempGate();
         createComponents();
         setLayouts();
         addComponents();
         addListeners();

     }
     public void createComponents()
     {
         TopPanel    = new GICritPanel(VCode,VName,iMillCode);
         MiddlePanel = new JPanel(true);
         BottomPanel = new JPanel(true);
         BExit       = new JButton("Exit");
         BPrint      = new JButton("Print");
         TFile       = new MyTextField(25);

         TFile.setText("1.prn");

         BPrint.setEnabled(false);
         TFile.setEditable(false);
         
     }

     public void setLayouts()
     {
         BottomPanel.setBorder(new TitledBorder(""));
         MiddlePanel.setLayout(new BorderLayout());
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,790,500);
     }

     public void addComponents()
     {
         BottomPanel.add(BExit);
         BottomPanel.add(BPrint);
         BottomPanel.add(TFile);

         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);

     }
     public void addListeners()
     {
         TopPanel.BApply.addActionListener(new ApplyList());
         TopPanel.BApply.setMnemonic('A');
         BExit.setMnemonic('X');
         BPrint.setMnemonic('P');
         BExit.addActionListener(new ExitList());
         BPrint.addActionListener(new PrintList());
     }

     public class ExitList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setVisible(false);
          }
     }
     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BPrint.setEnabled(false);
               Vector VHead  = getHead();
               iLen = ((String)VHead.elementAt(0)).length();
               Strline = common.Replicate("-",iLen)+"\n";
               Vector VBody  = getBody();
               Vector VSum   = getSum();
               String SFile  = TFile.getText();
               if(VBody.size()>0)
                     new DocPrint2(VBody,VHead,VSum,VTitle,SFile);
               //TFile.setText("");
               BExit.requestFocus();
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setTempGate();
               setTabReport();
               if(RowData.length>0)
               {
                    BPrint.setEnabled(true);
               }
               BExit.setEnabled(true);
               TFile.setEditable(true);
          }
     }
     public Vector getTitle(GICritPanel TopPanel)
     {

          Vector VTitle = new Vector();

          String SHead="Gate Inward List ";
          String str   = "";

          if(TopPanel.bsig==true)
          {
                if(SHead.length()>150)
                {
                    VTitle.addElement(SHead);
                    SHead="";
                }
                String SStart = TopPanel.TStDate.toString();
                String SEnd   = TopPanel.TEnDate.toString();

                str   = "For the Period From "+SStart+" To "+SEnd+" ";
                SHead = SHead+str;
          }
          else
          {
                if(SHead.length()>150)
                {
                    VTitle.addElement(SHead);
                    SHead="";
                }
                String SStart = TopPanel.TStNo.getText();
                String SEnd   = TopPanel.TEnNo.getText();

                str   = "For G.I. Number From "+SStart+" To "+SEnd+" ";
                SHead = SHead+str;
          }
          if(TopPanel.JRSeleParty.isSelected())
          {
               if(SHead.length()>150)
               {
                   VTitle.addElement(SHead);
                   SHead="";
               }
               str = "Party : "+(String)TopPanel.JCParty.getSelectedItem();
               SHead=SHead+" and "+str;
          }
          if(TopPanel.JRSeleType.isSelected())
          {
               if(SHead.length()>150)
               {
                   VTitle.addElement(SHead);
                   SHead="";
               }
               str = "Type : "+(String)TopPanel.JCType.getSelectedItem();
               SHead=SHead+" and "+str;
          }
          if(TopPanel.JRSeleMode.isSelected())
          {
               if(SHead.length()>150)
               {
                   VTitle.addElement(SHead);
                   SHead="";
               }
               str = "Inw.Mode : "+(String)TopPanel.JCMode.getSelectedItem();
               SHead=SHead+" and "+str;
          }
          if(TopPanel.JRSeleName.isSelected())
          {
               if(SHead.length()>150)
               {
                   VTitle.addElement(SHead);
                   SHead="";
               }
               str = "Mat. Name : "+(String)TopPanel.JCName.getSelectedItem();
               SHead=SHead+" and "+str;
          }
          if(TopPanel.JRSeleInv.isSelected())
          {
               if(SHead.length()>150)
               {
                   VTitle.addElement(SHead);
                   SHead="";
               }
               int iSelect = TopPanel.JCInvoice.getSelectedIndex();
               if (iSelect==0)
               {
                   str = "Invoice : With Invoice";
               }
               else 
               {
                   str = "Invoice : Without Invoice";
               }
               SHead=SHead+" and "+str;
          }
          if(TopPanel.JRSeleDC.isSelected())
          {
               if(SHead.length()>150)
               {
                   VTitle.addElement(SHead);
                   SHead="";
               }
               int iSelect = TopPanel.JCDC.getSelectedIndex();
               if (iSelect==0)
               {
                   str = "DC : With D.C.";
               }
               else 
               {
                   str = "DC : Without D.C.";
               }
               SHead=SHead+" and "+str;
          }
          if(TopPanel.JRSeleCat.isSelected())
          {
               if(SHead.length()>150)
               {
                   VTitle.addElement(SHead);
                   SHead="";
               }
               int iSelect = TopPanel.JCCategory.getSelectedIndex();
               if (iSelect==0)
               {
                   str = "Category : With P.O.";
               }
               else 
               {
                   str = "Category : Without P.O.";
               }
               SHead=SHead+" and "+str;
          }
          if(TopPanel.JRSeleStat.isSelected())
          {
               if(SHead.length()>150)
               {
                   VTitle.addElement(SHead);
                   SHead="";
               }
               int iSelect = TopPanel.JCStatus.getSelectedIndex();
               if (iSelect==0)
               {
                   str = "Status : Authenticated";
               }
               else 
               {
                   str = "Status : Not Authenticated";
               }
               SHead=SHead+" and "+str;
          }
          VTitle.addElement(SHead);
          return VTitle;
     }

     public Vector getHead()
     {
           Vector vect = new Vector();

           //                  0       1         2          3          4          5          6          7           8           9           10        11       12        13          14        15       16
           String Head1[] = {"Date","Number","Supplier","Inw.Type","Mat Code","Mat Name","Inv Qty","Gate Qty","Unmeas. Qty","Invoice No","Inv Date","DC No","DC Date","Inw.Mode","Category","Status","Remarks"};

           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=((String)Head1[5]).trim();
           String Sha7=((String)Head1[6]).trim();
           String Sha8=((String)Head1[7]).trim();
           String Sha9=((String)Head1[8]).trim();
           String Sha10=((String)Head1[9]).trim();
           String Sha11=((String)Head1[10]).trim();
           String Sha12=((String)Head1[11]).trim();
           String Sha13=((String)Head1[12]).trim();
           String Sha14=((String)Head1[13]).trim();
           String Sha15=((String)Head1[14]).trim();
           String Sha16=((String)Head1[15]).trim();
           String Sha17=((String)Head1[16]).trim();

           Sha1  = common.Space(3)+common.Pad(Sha1,7)+SInt;
           Sha2  = common.Rad(Sha2,8)+SInt;
           Sha3  = common.Pad(Sha3,30)+SInt;
           Sha4  = common.Pad(Sha4,15)+SInt;
           Sha5  = common.Pad(Sha5,9)+SInt;
           Sha6  = common.Pad(Sha6,40)+SInt;
           Sha7  = common.Rad(Sha7,12)+SInt;
           Sha8  = common.Rad(Sha8,12)+SInt;
           Sha9  = common.Rad(Sha9,12)+SInt;
           Sha10 = common.Pad(Sha10,15)+SInt;
           Sha11 = common.Pad(Sha11,10)+SInt;
           Sha12 = common.Pad(Sha12,15)+SInt;
           Sha13 = common.Pad(Sha13,10)+SInt;
           Sha14 = common.Pad(Sha14,15)+SInt;
           Sha15 = common.Pad(Sha15,12)+SInt;
           Sha16 = common.Pad(Sha16,20)+SInt;
           Sha17 = common.Pad(Sha17,20);                        
                                                                 

           String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+Sha16+Sha17+"\n";
           vect.add(Strh1);
           return vect;
     }
     public Vector getBody()
     {
           Vector vect = new Vector();
           for(int i=0;i<VGINo.size();i++)
           {
                 String Sda1  = (String)VGIDate.elementAt(i);
                 String Sda2  = (String)VGINo.elementAt(i);
                 String Sda3  = (String)VSupName.elementAt(i);
                 String Sda4  = (String)VInwType.elementAt(i);
                 String Sda5  = (String)VMCode.elementAt(i);
                 String Sda6  = (String)VMName.elementAt(i);
                 String Sda7  = common.getRound((String)VInvQty.elementAt(i),3);
                 String Sda8  = common.getRound((String)VGateQty.elementAt(i),3);
                 String Sda9  = common.getRound((String)VUnmeasure.elementAt(i),3);
                 String Sda10 = (String)VInvNo.elementAt(i);
                 String Sda11 = (String)VInvDate.elementAt(i);
                 String Sda12 = (String)VDCNo.elementAt(i);
                 String Sda13 = (String)VDCDate.elementAt(i);
                 String Sda14 = (String)VInwMode.elementAt(i);
                 String Sda15 = (String)VCategory.elementAt(i);
                 String Sda16 = (String)VStatus.elementAt(i);
                 String Sda17 = (String)VRemarks.elementAt(i);

                 Sda1  = common.Pad(Sda1,10)+SInt;
                 Sda2  = common.Rad(Sda2,8)+SInt;
                 Sda3  = common.Pad(Sda3,30)+SInt;
                 Sda4  = common.Pad(Sda4,15)+SInt;
                 Sda5  = common.Pad(Sda5,9)+SInt;
                 Sda6  = common.Pad(Sda6,40)+SInt;
                 Sda7  = common.Rad(Sda7,12)+SInt;
                 Sda8  = common.Rad(Sda8,12)+SInt;
                 Sda9  = common.Rad(Sda9,12)+SInt;
                 Sda10 = common.Pad(Sda10,15)+SInt;
                 Sda11 = common.Pad(Sda11,10)+SInt;
                 Sda12 = common.Pad(Sda12,15)+SInt;
                 Sda13 = common.Pad(Sda13,10)+SInt;
                 Sda14 = common.Pad(Sda14,15)+SInt;
                 Sda15 = common.Pad(Sda15,12)+SInt;
                 Sda16 = common.Pad(Sda16,20)+SInt;
                 Sda17 = common.Pad(Sda17,20);

                 String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+Sda16+Sda17+"\n";
                 vect.add(Strd);
           }
           return vect;
     }
     public Vector getSum()
     {
           Vector vect = new Vector();

           return vect;
     }

     private void setTabReport()
     {

          setDataIntoVector();
          setRowData();
          try
          {
             getContentPane().remove(tabreport);
          }
          catch(Exception ex){}

          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             getContentPane().add(tabreport,BorderLayout.CENTER);
             setSelected(true);
             DeskTop.repaint();
             DeskTop.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }

     }
     public void setDataIntoVector()
     {

           VGINo      = new Vector();
           VGIDate    = new Vector();
           VSupName   = new Vector();
           VInwType   = new Vector();
           VMCode     = new Vector();
           VMName     = new Vector();
           VInvQty    = new Vector();
           VGateQty   = new Vector();
           VUnmeasure = new Vector();
           VInvNo     = new Vector();
           VInvDate   = new Vector();
           VDCNo      = new Vector();
           VDCDate    = new Vector();
           VInwMode   = new Vector();
           VCategory  = new Vector();
           VStatus    = new Vector();
           VRemarks   = new Vector();

           String SStart="";
           String SEnd="";

           if(TopPanel.bsig==true)
           {
               SStart = TopPanel.TStDate.toNormal();
               SEnd   = TopPanel.TEnDate.toNormal();
           }
           else
           {
               SStart  = TopPanel.TStNo.getText();
               SEnd    = TopPanel.TEnNo.getText();
           }

           VTitle = new Vector();
           VTitle = getTitle(TopPanel);
           try
           {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       theStatement  =  theConnection.createStatement();

              String QS = getQString(SStart,SEnd);

              ResultSet res  = theStatement.executeQuery(QS);
              while (res.next())
              {

                 String SCategory="";
                 String str1 = common.parseNull(res.getString(15));
                 if(str1.equals("0"))
                 {
                     SCategory = "Without P.O.";
                 }
                 else
                 {
                     SCategory = "With P.O.";
                 }

                 String SStatus="";
                 String str2 = common.parseNull(res.getString(16));
                 if(str2.equals("0"))
                 {
                     SStatus = "Not Authenticated";
                 }
                 if(str2.equals("1"))
                 {
                     SStatus = "Authenticated";
                 }


                 VGINo     .addElement(common.parseNull(res.getString(1)));
                 VGIDate   .addElement(common.parseDate(res.getString(2)));
                 VSupName  .addElement(common.parseNull(res.getString(3)));
                 VInwType  .addElement(common.parseNull(res.getString(4)));
                 VMCode    .addElement(common.parseNull(res.getString(5)));
                 VMName    .addElement(common.parseNull(res.getString(6)));
                 VInvQty   .addElement(common.parseNull(res.getString(7)));
                 VGateQty  .addElement(common.parseNull(res.getString(8)));
                 VUnmeasure.addElement(common.parseNull(res.getString(9)));
                 VInvNo    .addElement(common.parseNull(res.getString(10)));
                 VInvDate  .addElement(common.parseDate(res.getString(11)));
                 VDCNo     .addElement(common.parseNull(res.getString(12)));
                 VDCDate   .addElement(common.parseDate(res.getString(13)));
                 VInwMode  .addElement(common.parseNull(res.getString(14)));
                 VCategory .addElement(SCategory);
                 VStatus   .addElement(SStatus);
                 VRemarks  .addElement(common.parseNull(res.getString(17)));
              }
              res.close();
              theStatement.close();
           }
           catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
         RowData     = new Object[VGINo.size()][ColumnData.length];
         for(int i=0;i<VGINo.size();i++)
         {
               RowData[i][0]  = (String)VGINo      .elementAt(i);
               RowData[i][1]  = (String)VGIDate    .elementAt(i);
               RowData[i][2]  = (String)VSupName   .elementAt(i);
               RowData[i][3]  = (String)VInwType   .elementAt(i);
               RowData[i][4]  = (String)VMCode     .elementAt(i);
               RowData[i][5]  = (String)VMName     .elementAt(i);
               RowData[i][6]  = (String)VInvQty    .elementAt(i);
               RowData[i][7]  = (String)VGateQty   .elementAt(i);
               RowData[i][8]  = (String)VUnmeasure .elementAt(i);
               RowData[i][9]  = (String)VInvNo     .elementAt(i);
               RowData[i][10] = (String)VInvDate   .elementAt(i);
               RowData[i][11] = (String)VDCNo      .elementAt(i);
               RowData[i][12] = (String)VDCDate    .elementAt(i);
               RowData[i][13] = (String)VInwMode   .elementAt(i);
               RowData[i][14] = (String)VCategory  .elementAt(i);
               RowData[i][15] = (String)VStatus    .elementAt(i);
               RowData[i][16] = (String)VRemarks   .elementAt(i);
        }  
     }

     public String getQString(String SStart,String SEnd)
     {
          // Category -    0 - without P.O.    1 - with P.O.

          String QS = " SELECT TempGate.GINo, TempGate.GIDate, PartyMaster.PartyName, InwType.InwName, TempGate.Item_Code, TempGate.Item_Name, TempGate.SupQty, TempGate.GateQty, TempGate.UnmeasuredQty, TempGate.InvNo, TempGate.InvDate, TempGate.DcNo, TempGate.DcDate, InwardMode.Name, TempGate.Category, TempGate.Authentication, TempGate.Remarks "+
                      " FROM ((TempGate "+
                      " Left JOIN PartyMaster ON TempGate.Sup_Code = PartyMaster.PartyCode) "+
                      " Left JOIN InwType ON TempGate.InwNo = InwType.InwNo) "+
                      " Left JOIN InwardMode ON TempGate.ModeCode = InwardMode.Code ";

          String SHave = "";
          String str   = "";
          if(TopPanel.bsig==true)
          {
                str   = "TempGate.GIDate >='"+SStart+"' and TempGate.GIDate <='"+SEnd+"' ";
                SHave = SHave+" "+str;
          }
          else
          {
                str   = "TempGate.GINo >= "+SStart+" and TempGate.GINo <="+SEnd+" ";
                SHave = SHave+" "+str;
          }
          if(TopPanel.JRSeleParty.isSelected())
          {
               str = "TempGate.Sup_Code = '"+(String)TopPanel.VPartyCode.elementAt(TopPanel.JCParty.getSelectedIndex())+"'";
               SHave=SHave+" and "+str;
          }
          if(TopPanel.JRSeleType.isSelected())
          {
               str = "TempGate.InwNo = "+(String)TopPanel.VTypeCode.elementAt(TopPanel.JCType.getSelectedIndex());
               SHave=SHave+" and "+str;
          }
          if(TopPanel.JRSeleMode.isSelected())
          {
               str = "TempGate.ModeCode = "+(String)TopPanel.VModeCode.elementAt(TopPanel.JCMode.getSelectedIndex());
               SHave=SHave+" and "+str;
          }
          if(TopPanel.JRSeleName.isSelected())
          {
               str = "TempGate.Item_Code = '"+(String)VCode.elementAt(TopPanel.JCName.getSelectedIndex())+"'";
               SHave=SHave+" and "+str;
          }
          if(TopPanel.JRSeleInv.isSelected())
          {
               int iSelect = TopPanel.JCInvoice.getSelectedIndex();
               if (iSelect==0)
               {
                   str = "trim(TempGate.InvNo) is not null ";
               }
               else 
               {
                   str = "trim(TempGate.InvNo) is null ";
               }
               SHave=SHave+" and "+str;
          }
          if(TopPanel.JRSeleDC.isSelected())
          {
               int iSelect = TopPanel.JCDC.getSelectedIndex();
               if (iSelect==0)
               {
                   str = "trim(TempGate.DcNo) is not null ";
               }
               else 
               {
                   str = "trim(TempGate.DcNo) is null ";
               }
               SHave=SHave+" and "+str;
          }
          if(TopPanel.JRSeleCat.isSelected())
          {
               int iSelect = TopPanel.JCCategory.getSelectedIndex();
               if (iSelect==0)
               {
                   str = "TempGate.Category = '1'";
               }
               else 
               {
                   str = "TempGate.Category = '0'";
               }
               SHave=SHave+" and "+str;
          }
          if(TopPanel.JRSeleStat.isSelected())
          {
               int iSelect = TopPanel.JCStatus.getSelectedIndex();
               if (iSelect==0)
               {
                   str = "TempGate.Authentication = 1";
               }
               else 
               {
                   str = "TempGate.Authentication = 0";
               }
               SHave=SHave+" and "+str;
          }
         
          QS = QS+" Where "+SHave;

          String SOrder=(TopPanel.TSort.getText()).trim();
          if(SOrder.length()>0)
                QS = QS+" Order By "+TopPanel.TSort.getText()+",1";
          else
                QS = QS+" Order By 1 ";

                System.out.println(QS);


          return QS;
     }

     public void setTempGate()
     {

           try
           {

               String SStart = TopPanel.TStDate.toNormal();
               String SEnd   = TopPanel.TEnDate.toNormal();

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
               Statement       theStatement  =  theConnection.createStatement();

               try{theStatement.execute("Drop Table TempGate");}catch(Exception ex){}

              String QS1 = " create table  TempGate as  "+
                           " (SELECT GateInward.GINo, GateInward.GIDate, GateInward.Sup_Code, GateInward.InwNo, GateInward.Item_Code, InvItems.Item_Name, GateInward.SupQty, GateInward.GateQty, GateInward.UnmeasuredQty, GateInward.InvNo, GateInward.InvDate, GateInward.DcNo, GateInward.DcDate, GateInward.ModeCode, '1' as Category, GateInward.Authentication, GateInward.Remarks "+
                           " FROM GateInward "+
                           " INNER JOIN InvItems ON GateInward.Item_Code = InvItems.Item_Code "+
                           " Where GateInward.Item_Name Is Null and GateInward.ModiStatus=0 and GateInward.MillCode="+iMillCode+
                           " Union All "+
                           " SELECT GateInward.GINo, GateInward.GIDate, GateInward.Sup_Code, GateInward.InwNo, ' ' as Item_Code, GateInward.Item_Name, GateInward.SupQty, GateInward.GateQty, GateInward.UnmeasuredQty, GateInward.InvNo, GateInward.InvDate, GateInward.DcNo, GateInward.DcDate, GateInward.ModeCode, '0' as Category, GateInward.Authentication, GateInward.Remarks "+
                           " FROM GateInward "+
                           " Where GateInward.Item_Code Is Null and GateInward.ModiStatus=0 and GateInward.MillCode="+iMillCode+
                           " Union All "+
                           " Select CottonGi.GiNo,to_char(CottonGi.GiDate),CottonGi.Accode as Sup_Code, 0 as InwNo, ' ' as Item_Code, 'Cotton' as Item_Name, Bales as SupQty, Bales as GateQty, 0 as UnmeasuredQty, CottonGi.InvNo, to_char(CottonGi.InvDate), CottonGi.DcNo,to_char(CottonGi.DcDate), 0 as ModeCode, '0' as Category, 1 as Authentication, '' as Remarks "+
                           " FROM CottonGi "+
                           " Where  CottonGi.GiDate>='"+SStart+"' and CottonGi.GIDate <='"+SEnd+"' "+
                           "  ) ";

              theStatement.execute(QS1);
              theStatement.close();
                           System.out.println(QS1);

           }
           catch(Exception ex){System.out.println(ex);}
     }



}
