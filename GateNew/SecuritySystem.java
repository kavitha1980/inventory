package GateNew;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.sql.*;
import java.util.*;
import java.io.*;
import util.*;
import jdbc.*;

public class SecuritySystem
{
     JMenuBar       theMenuBar;
     JFrame         theFrame;
     Container      Container;
     JDesktopPane   DeskTop;
     StatusPanel    SPanel;
     JPanel         MenuPanel;

     JMenu GIMenu,RDCMenu,RGMenu;

     JMenuItem mGIStores,mGIDStores,mGIModi,mGIPrint,mGISList,mGIDelList;
     JMenuItem mRdcOut,mOutPassAuth;
     JMenuItem mKRGGate,mKRGIPrint,mCMRGGate,mCMRGIPrint,mCMOutGate;

     JWindow   wSplash;

     Vector         VCode,VName;
     boolean        bflag;
     Common         common;
     Control        control;

    // Possible Look & Feels
    String mac      = "com.sun.java.swing.plaf.mac.MacLookAndFeel";
    String metal    = "javax.swing.plaf.metal.MetalLookAndFeel";
    String motif    = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
    String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

    // The current Look & Feel
    String currentLookAndFeel = metal;

    int    iUserCode,iAuthCode,iMillCode;
    String SFinYear,SStDate,SEnDate,SYearCode,SItemTable,SSupTable,SMillName;

    Vector VNameCode,VCat,VDraw,VUomName;

    public SecuritySystem(int iUserCode,int iAuthCode,int iMillCode,String SFinYear,String SStDate,String SEnDate,String SYearCode,String SItemTable,String SSupTable,String SMillName) 
    {
          this.iUserCode  = iUserCode;
          this.iAuthCode  = iAuthCode;
          this.iMillCode  = iMillCode;
          this.SFinYear   = SFinYear;
          this.SStDate    = SStDate;
          this.SEnDate    = SEnDate;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;

          updateLookAndFeel();
          CreateComponents();
          setLayouts();
          AddComponents();
          AddListeners();
          theFrame.setVisible(true);
          setDataIntoVector();
    }
    public void CreateComponents()
    {
          theFrame        = new JFrame("Security Monitoring System - "+SMillName+" - Year - "+SFinYear);
          Container       = theFrame.getContentPane();
          DeskTop         = new JDesktopPane();
          SPanel          = new StatusPanel();
          MenuPanel       = new JPanel();
          theMenuBar      = new JMenuBar();

          if(iMillCode==0)
          {
               Container . setBackground(new Color(250,200,200));
               theFrame  . setBackground(new Color(250,200,200));
               DeskTop   . setBackground(new Color(250,200,200));
               theMenuBar. setBackground(new Color(250,200,200));
               MenuPanel . setBackground(new Color(250,200,200));
          }
          if(iMillCode==1)
          {
               Container . setBackground(new Color(220,250,250));
               theFrame  . setBackground(new Color(220,250,250));
               DeskTop   . setBackground(new Color(220,250,250));
               theMenuBar. setBackground(new Color(220,250,250));
               MenuPanel . setBackground(new Color(220,250,250));
          }
          if(iMillCode==3)
          {
               Container . setBackground(new Color(200,250,200));
               theFrame  . setBackground(new Color(200,250,200));
               DeskTop   . setBackground(new Color(200,250,200));
               theMenuBar. setBackground(new Color(200,250,200));
               MenuPanel . setBackground(new Color(200,250,200));
          }
          if(iMillCode==4)
          {
               Container . setBackground(new Color(200,200,250));
               theFrame  . setBackground(new Color(200,200,250));
               DeskTop   . setBackground(new Color(200,200,250));
               theMenuBar. setBackground(new Color(200,200,250));
               MenuPanel . setBackground(new Color(200,200,250));
          }

          GIMenu       = new JMenu("Gate Inward");
          RDCMenu      = new JMenu("RDC");

          if(iMillCode>1)
          {
               RGMenu       = new JMenu("RawMaterial");
          }


          mGIStores    = new JMenuItem("Stores & Spares");
          mGIDStores   = new JMenuItem("Stores & Spares without P.O.");
          mGIModi      = new JMenuItem("Authentication");
          mGIPrint     = new JMenuItem("Stores Inward Printing");

          mGISList     = new JMenuItem("Stores Inward List");
          mGIDelList   = new JMenuItem("GI Deleted List");

          mRdcOut      = new JMenuItem("RDC Outward");
          mOutPassAuth = new JMenuItem("OutPass Authentication");

          if(iMillCode==3)
          {
               mCMRGGate     = new JMenuItem("GateInward");
               mCMOutGate    = new JMenuItem("OutSide Godown GateInward");
               mCMRGIPrint   = new JMenuItem("GIPrint");
          }

          if(iMillCode==4)
          {
               mKRGGate      = new JMenuItem("GateInward");
               mKRGIPrint    = new JMenuItem("GIPrint");
          }


          VCode          = new Vector();                           
          VName          = new Vector();
          common         = new Common();
          control        = new Control();

     }
     public void setLayouts()
     {
          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          theFrame.setSize(screenSize);
          MenuPanel.setLayout(new BorderLayout());          
     }
     public void AddComponents()
     {
          Container.add("North",MenuPanel);
          Container.add("Center",DeskTop);
          Container.add("South",SPanel);

          GIMenu.setMnemonic('I');
          RDCMenu.setMnemonic('R');

          if(iMillCode>1)
          {
               RGMenu.setMnemonic('G');
          }


          theMenuBar.add(GIMenu);
          theMenuBar.add(RDCMenu);

          if(iMillCode>1)
          {
               theMenuBar.add(RGMenu);
          }

          GIMenu.add(mGIStores);
          GIMenu.add(mGIDStores);
          GIMenu.add(mGIModi);
          GIMenu.addSeparator();
          GIMenu.add(mGIPrint);
          GIMenu.add(mGISList);
          GIMenu.add(mGIDelList);

          RDCMenu.add(mRdcOut);
          RDCMenu.add(mOutPassAuth);

          if(iMillCode==3)
          {
               RGMenu.add(mCMRGGate);
               RGMenu.add(mCMOutGate);
               RGMenu.add(mCMRGIPrint);
          }

          if(iMillCode==4)
          {
               RGMenu.add(mKRGGate);
               RGMenu.add(mKRGIPrint);
          }
          MenuPanel.add("West",theMenuBar);

     }
     public void AddListeners()
     {
          theFrame.addWindowListener(new WinList());
          if(iAuthCode>0)
          {
               if((iUserCode==7) || (iUserCode==9))
               {
                    mGIStores.addActionListener(new ActList());
                    mGIDStores.addActionListener(new ActList());
                    mGIModi.addActionListener(new ActList());

                    mRdcOut.addActionListener(new ActList());
                    mOutPassAuth.addActionListener(new ActList());

                    if(iMillCode==3)
                    {
                         mCMRGGate  .addActionListener(new ActList());
                         mCMRGIPrint.addActionListener(new ActList());
                         mCMOutGate .addActionListener(new ActList());
                    }

                    if(iMillCode==4)
                    {
                         mKRGGate.addActionListener(new ActList());
                         mKRGIPrint.addActionListener(new ActList());
                    }

               }
          }
          mGIPrint.addActionListener(new ActList());

          mGISList.addActionListener(new ActList());
          mGIDelList.addActionListener(new ActList());
     }    
     public class WinList extends WindowAdapter
     {
         public void windowClosing(WindowEvent we)
         {
            System.exit(0);
         }
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {

               if(ae.getSource()==mGIStores)
               {
                    GISFrame gisframe = new GISFrame(DeskTop,VCode,VName,SPanel,iUserCode,iAuthCode,iMillCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(gisframe);
                         DeskTop.repaint();
                         gisframe.setSelected(true);
                         DeskTop.updateUI();
                         gisframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mGIDStores)
               {
                    GIDSFrame gidsframe = new GIDSFrame(DeskTop,VCode,VName,SPanel,iUserCode,iAuthCode,iMillCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(gidsframe);
                         DeskTop.repaint();
                         gidsframe.setSelected(true);
                         DeskTop.updateUI();
                         gidsframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mGIModi)
               {
                    GIAuthenticationFrame gimodi = new GIAuthenticationFrame(DeskTop,VCode,VName,SPanel,iUserCode,iAuthCode,iMillCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(gimodi);
                         DeskTop.repaint();
                         gimodi.setSelected(true);
                         DeskTop.updateUI();
                         gimodi.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mGIPrint)
               {
                    GIPrintFrame giprint = new GIPrintFrame(DeskTop,VCode,VName,SPanel,iUserCode,iAuthCode,iMillCode,SYearCode,SItemTable,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(giprint);
                         DeskTop.repaint();
                         giprint.setSelected(true);
                         DeskTop.updateUI();
                         giprint.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mGISList)
               {
                    GIListFrame gilist = new GIListFrame(DeskTop,VCode,VName,SPanel,iUserCode,iAuthCode,iMillCode,SYearCode,SItemTable,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(gilist);
                         DeskTop.repaint();
                         gilist.setSelected(true);
                         DeskTop.updateUI();
                         gilist.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mGIDelList)
               {
                    GIDeleteListFrame gidellist = new GIDeleteListFrame(DeskTop,VCode,VName,SPanel,iUserCode,iAuthCode,iMillCode,SYearCode,SItemTable,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(gidellist);
                         DeskTop.repaint();
                         gidellist.setSelected(true);
                         DeskTop.updateUI();
                         gidellist.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mRdcOut)
               {
                    RDCPendingListFrame rdcpendinglist = new RDCPendingListFrame(DeskTop,SPanel,iUserCode,iAuthCode,iMillCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(rdcpendinglist);
                         DeskTop.repaint();
                         rdcpendinglist.setSelected(true);
                         DeskTop.updateUI();
                         rdcpendinglist.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mOutPassAuth)
               {
                    OutPassAuthFrame outpassauthframe = new OutPassAuthFrame(DeskTop,SPanel,iUserCode,iAuthCode,iMillCode,SMillName);
                    try
                    {
                         DeskTop.add(outpassauthframe);
                         DeskTop.repaint();
                         outpassauthframe.setSelected(true);
                         DeskTop.updateUI();
                         outpassauthframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mCMRGGate)
               {
                    MelGateInwardFrame gateinwardframe = new MelGateInwardFrame(DeskTop,SPanel,iUserCode,iAuthCode,iMillCode,SMillName,SYearCode);
                    try
                    {
                         DeskTop.add(gateinwardframe);
                         DeskTop.repaint();
                         gateinwardframe.setSelected(true);
                         DeskTop.updateUI();
                         gateinwardframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mCMRGIPrint)
               {
                    MelRGIPrintFrame rgiprintframe = new MelRGIPrintFrame(DeskTop,iMillCode);
                    try
                    {
                         DeskTop.add(rgiprintframe);
                         DeskTop.repaint();
                         rgiprintframe.setSelected(true);
                         DeskTop.updateUI();
                         rgiprintframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }


               if(ae.getSource()==mCMOutGate)
               {
                    OutSideCottonGiFrame outsideframe = new OutSideCottonGiFrame(DeskTop);
                    try
                    {
                         DeskTop.add(outsideframe);
                         DeskTop.repaint();
                         outsideframe.setSelected(true);
                         DeskTop.updateUI();
                         outsideframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }      


               if(ae.getSource()==mKRGGate)
               {
                    KamGateInwardFrame gateinwardframe = new KamGateInwardFrame(DeskTop,SPanel,iUserCode,iAuthCode,iMillCode,SMillName,SYearCode);
                    try
                    {
                         DeskTop.add(gateinwardframe);
                         DeskTop.repaint();
                         gateinwardframe.setSelected(true);
                         DeskTop.updateUI();
                         gateinwardframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mKRGIPrint)
               {
                    KamRGIPrintFrame rgiprintframe = new KamRGIPrintFrame(DeskTop,iMillCode);
                    try
                    {
                         DeskTop.add(rgiprintframe);
                         DeskTop.repaint();
                         rgiprintframe.setSelected(true);
                         DeskTop.updateUI();
                         rgiprintframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }      
               

          }

     }
     public void setDataIntoVector()
     {
          ResultSet result = null;

          VNameCode = new Vector();
          VCat      = new Vector();
          VDraw     = new Vector();
          VUomName  = new Vector();
          VCode     = new Vector();
          VName     = new Vector();



          String QS = "";

          if(iMillCode==0)
          {
               QS = " Select Item_Name,Item_Code,InvItems.UoMCode,Catl,Draw,UomName From InvItems"+
                    " inner join Uom on Uom.UomCode = invItems.UomCode Order By Item_Name";
          }
          else
          {
               QS = " Select Item_Name,"+SItemTable+".Item_Code,InvItems.UoMCode,Catl,Draw,UomName From "+SItemTable+" "+
                    " Inner Join InvItems On "+
                    " InvItems.Item_Code = "+SItemTable+".Item_Code "+
                    " inner join Uom on Uom.UomCode = invItems.UomCode Order By Item_Name ";
          }

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
               result                        =  theStatement.executeQuery(QS);

               int ctr=0;
               while(result.next())
               {
                    ctr++;
                    String str1 = result.getString(1);
                    String str2 = result.getString(2);
                    String str3 = result.getString(3);
                    String str4 = result.getString(4);
                    String str5 = result.getString(5);
                    VName.addElement(common.parseNull(str1+"-"+str3));
                    VCode.addElement(str2);
                    VNameCode.addElement(str1+" - "+str3+" (Code : "+str2+")"+" (Catl : "+str4+")"+" (Draw : "+str5+")");

                    VCat    .addElement(common.parseNull((String)result.getString(4)));
                    VDraw   .addElement(common.parseNull((String)result.getString(5)));
                    VUomName.addElement(common.parseNull((String)result.getString(6)));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void updateLookAndFeel()
     {
          try
          {
               UIManager.setLookAndFeel(currentLookAndFeel);
          }
          catch (Exception ex)
          {
               System.out.println("Failed loading L&F: " + currentLookAndFeel);
               System.out.println(ex);
          }
     }

     public int getUserCode()
     {
          return iUserCode;
     }

}
