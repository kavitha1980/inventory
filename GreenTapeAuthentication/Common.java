package GreenTapeAuthentication;

import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.util.*;
import java.sql.*;
import java.net.*;

import jdbc.*;
import guiutil.*;

public class Common
{
     String SInvDSN;
     public String SPeriod,SUser,SFinSt,SFinEn;

     String S0To9[]   = {"Zero ","One ","Two ","Three ","Four ","Five ","Six ","Seven ","Eight ","Nine "};
     String S10To19[] = {"Ten ","Eleven ","Twelve ","Thirteen ","Fourteen ","Fifteen ","Sixteen ","Seventeen ","Eightteen ","Nineteen "};
     String TensArr[] = {"Twenty ","Thirty ","Forty ","Fifty ","Sixty ","Seventy ","Eighty ","Ninety "};

     public Common()
     {
          setDSN();
     }
     
     public String getRound(double dAmount,int iScale)
     {
          String str="0";
          try
          {
               java.math.BigDecimal bd1 = new java.math.BigDecimal(String.valueOf(dAmount));
               java.math.BigDecimal bd2 = bd1.setScale(iScale,4);
               str=bd2.toString();
          }
          catch(Exception ex)
          {
          }
          return str;
      }

     public String getRound(String SAmount,int iScale)
     {
          String str="0";
          try
          {
               java.math.BigDecimal bd1 = new java.math.BigDecimal(SAmount);
               java.math.BigDecimal bd2 = bd1.setScale(iScale,4);
               str=bd2.toString();
          }
          catch(Exception ex)
          {
          }
          return str;
     }

     public String Pad(String Str,int Len)
     {
          String RetStr="";
          Str=parseNull(Str);
          if(Str.length() > Len)
          {
               RetStr = Str.substring(0,Len);
          }
          else
          {
               RetStr = Str+Space(Len-Str.length());
          }
          return RetStr;
     }

     public String Rad(String Str,int Len)
     {
          Str=parseNull(Str);
          String RetStr="";
          if(Str.length() > Len)
          {
               RetStr = Str.substring(0,Len);
          }
          else
          {
               RetStr = Space(Len-Str.length())+Str;
          }
          return RetStr;
     }

     public String Space(int Len)
     {
          String RetStr="";
          for(int index=0;index<Len;index++)
          {
               RetStr=RetStr+" ";
          }
          return RetStr;
     }

     public String Replicate(String Str,int Len)
     {
          String RetStr="";
          for(int index=0;index<Len;index++)
          {
               RetStr = RetStr+Str;
          }
          return RetStr;
     }

     public String parseNull(String str)
     {
          str = ""+str;
          if(str.startsWith("null"))
               return "";
          return str;
     }

     public String Cad(String str,int Len)
     {
          if(Len < str.length())
          {
               return Pad(str,Len);
          }
          String RetStr="";
          int FirstLen = (Len-str.length())/2;
          int SecondLen = Len-str.length()-FirstLen;
          RetStr=Pad(Space(FirstLen)+str+Space(SecondLen),Len);
          return RetStr;
     }

     public String parseDate(String str)
     {
         String SDate="";
         str=parseNull(str);
         str=str.trim();
         if(str.length()!=8)
            return str;
         try
         {
            SDate = str.substring(6,8)+"."+str.substring(4,6)+"."+str.substring(0,4);
         }
         catch(Exception ex)
         {
         }
         return SDate;
     }

     public String pureDate(String str)
     {
         String SDate="";
         str=str.trim();
         if(str.length()!=10)
            return str;
         try
         {
            str=str.trim();
            SDate = str.substring(6,10)+str.substring(3,5)+str.substring(0,2);
         }
         catch(Exception ex)
         {
         }
         return SDate;
     }

     public String parseZero(String str)
     {
          String SZero   = "";
          double  dZero  = 0;
          try
          {
               dZero   = Double.parseDouble(str);
          }catch(Exception ex){dZero=0;}
          SZero = String.valueOf(dZero);
          return SZero;
     }

     public double toDouble(String str)
     {
          double d=0;

          try
          {
               d = Double.parseDouble(str.trim());       
          }
          catch(Exception ex)
          {
               d=0;
          }
          return d;
     }

     public int toInt(String str)
     {
          int d=0;

          try
          {
               d = Integer.parseInt(str.trim());       
          }
          catch(Exception ex)
          {
               d=0;
          }
          return d;
     }

     public float toFloat(String str)
     {
          float d=0;

          try
          {
               d = Float.parseFloat(str.trim());       
          }
          catch(Exception ex)
          {
               d=0;
          }
          return d;
     }

     public String getDateDiff(String str1,String str2)
     {
          String SDays="";

          String SQry="select to_Date('"+str1+"','DD.MM.YYYY')-to_Date('"+str2+"','DD.MM.YYYY') from dual";

          try
          {
              ORAConnection connect    = ORAConnection.getORAConnection();
              Connection theConnection = connect.getConnection();
              Statement theStatement   = theConnection.createStatement();
              ResultSet theResult      = theStatement.executeQuery(SQry);

              if(theResult.next())
                    SDays= theResult.getString(1);
              theResult.close();
              theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               return "";
          }
          return SDays;

     }

     public String getTime()
     {
          java.util.Date dt        = new java.util.Date();

          int iHrs       = dt.getHours();
          int iMin       = dt.getMinutes();

          String sHrs    =(iHrs<10?"0"+iHrs:""+iHrs);
          String sMin    =(iMin<10?"0"+iMin:""+iMin);

          return sHrs+":"+sMin;
     }

     public String getDate(String str1,long days,int Sig)
     {

            String SDate="";

            String SQry="";
            if(Sig==-1)
                SQry= "Select to_char(to_Date('"+str1+"','DD.MM.YYYY')+(-1)*"+days+",'DD.MM.YYYY')  from dual";
            else
                SQry= "Select to_char(to_Date('"+str1+"','DD.MM.YYYY')+"+days+",'DD.MM.YYYY')  from dual";

          try
          {
			ORAConnection connect = ORAConnection.getORAConnection();
			Connection theConnection = connect.getConnection();
			Statement theStatement = theConnection.createStatement();
			ResultSet theResult = theStatement.executeQuery(SQry);
               if(theResult.next())
                    SDate= theResult.getString(1);
            theResult.close();
            theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               return "";
          }
          return SDate;


     }


     //date in pureform
     public String getMonth(String SDate)
     {
           String Month[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};
           int iIndex = toInt(SDate.substring(4,6));
           return Month[iIndex-1];
     }

     public String getMonth(int iIndex)
     {
           String Month[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};
           return Month[iIndex-1];
     }

     public void warn(JLayeredPane DeskTop,StatusPanel SPanel)
     {
          String SFileName  = getPrintPath()+"Error.prn";
          JInternalFrame jf = new JInternalFrame(SFileName);
          Notepad        np = new Notepad(new File(SFileName),SPanel);
          jf.getContentPane().add("Center",np);
          jf.show();
          jf.setBounds(80,100,550,350);
          jf.setClosable(true);
          jf.setMaximizable(true);
          jf.setTitle("Omissions");
          try
          {
               DeskTop.add(jf);
               jf.moveToFront();
               jf.setSelected(true);
               np.editor.setForeground(Color.white);
               np.editor.setBackground(Color.red);
               np.editor.setFont(new Font("monospaced", Font.PLAIN, 18));
               np.editor.setEditable(false);
               DeskTop.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
     }

     public int indexOf(Vector vect,String str)
     {
           int iindex=-1;
           str = str.trim();
           for(int i=0;i<vect.size();i++)
           {
                  String str1 = (String)vect.elementAt(i);
                  str1 = str1.trim();
                  if(str1.startsWith(str))
                        return i;
           }
           return iindex;
     }
     public int exactIndexOf(Vector vect,String str)
     {
           int iindex=-1;
           str = str.trim();

           for(int i=0;i<vect.size();i++)
           {
                  String str1 = (String)vect.elementAt(i);
                  str1 = str1.trim();
                  if(str1.equals(str))
                        return i;
           }
           return iindex;
     }

     // SOp - operation : "S" - SUM; "A" - Average
     public String getSum(Vector Vx,String SOp)
     {
          double dAmount=0.0;   
          for(int i=0;i<Vx.size();i++)
          {
               dAmount = dAmount+toDouble((String)Vx.elementAt(i));
          }
          if(SOp.equals("A"))
          {
               dAmount = dAmount/Vx.size();
          }
          return getRound(dAmount,3);
      }
     // SOp - operation : "S" - SUM; "A" - Average; iScale - Round off Digits
     public String getSum(Vector Vx,String SOp,int iScale)
     {
          
          double dAmount=0.0;   
          for(int i=0;i<Vx.size();i++)
          {
               dAmount = dAmount+toDouble((String)Vx.elementAt(i));
          }
          if(SOp.equals("A"))
          {
               dAmount = dAmount/Vx.size();
          }
          return getRound(dAmount,iScale);
     }

     public Vector VectorFromTokenString(String STokenable)
     {
           Vector VVector = new Vector();
           StringTokenizer ST = new StringTokenizer(STokenable,",");
           while(ST.hasMoreTokens())
           {
                   VVector.addElement(ST.nextToken());
           }
           return VVector;
     }

     public double getProjection(Vector VY)
     {
          Vector VXY    = new Vector();
          Vector VX     = new Vector();
          Vector VXSq   = new Vector();

          double dSigXY  = 0,dNSigXY  = 0;
          double dSigX   = 0,dSigXSigX= 0,dSigY    = 0;
          double dSigXSq = 0,dNSigXSq = 0;
          double dbn     = 0,dbd      = 0;
          double db      = 0,da       = 0;
          double dMeanX  = 0,dMeanY   = 0;
          double dNextY  = 0;

          for(int i=0;i<VY.size();i++)
          {
               int     iX = i+1;
               double  dY = toDouble((String)VY.elementAt(i));
               VXY.addElement(getRound(iX*dY,2));
               VX.addElement(String.valueOf(iX));
               VXSq.addElement(String.valueOf(iX*iX));
          }
          for(int i=0;i<VXY.size();i++)
          {
               dSigXY   = dSigXY  +toDouble((String)VXY.elementAt(i));
               dSigX    = dSigX   +toDouble((String)VX.elementAt(i));
               dSigY    = dSigY   +toDouble((String)VY.elementAt(i));
               dSigXSq  = dSigXSq +toDouble((String)VXSq.elementAt(i));
           }
           dNSigXY   = VX.size()*dSigXY;
           dNSigXSq  = VX.size()*dSigXSq;
           dSigXSigX = dSigX*dSigX;
           dbn       = dNSigXY-(dSigX*dSigY);
           dbd       = dNSigXSq-dSigXSigX;
           db        = dbn/dbd;
           dMeanX    = dSigX/VX.size();
           dMeanY    = dSigY/VX.size();
           da        = dMeanY-(db*dMeanX);
           dNextY    = da+(db*(VX.size()+1));
           return dNextY;
     }

     // Inventory Data Source
     public String getDSN()
     {
          return SInvDSN;
     }

     // setting the DSNs()
     private void setDSN()
     {
          Vector vect = new Vector();
          String str  = getDSNString();
          StringTokenizer ST = new StringTokenizer(str,"\n");
          while(ST.hasMoreElements())
               vect.addElement(ST.nextToken());
          for(int i=0;i<vect.size();i++)
               organizeProperty((String)vect.elementAt(i));
     }

     private void organizeProperty(String str)
     {
          Vector vect = new Vector();
          if(str.startsWith("#"))
               return;
          StringTokenizer ST = new StringTokenizer(str,"|");
          while(ST.hasMoreElements())
          {
               String xtr = ST.nextToken();
               vect.addElement(xtr.substring(0,xtr.length()-1));
          }
          if(vect.size() != 2)
               return;
          String SId     = (String)vect.elementAt(0);
          String SSource = (String)vect.elementAt(1);
          if(SId.startsWith("Inventory"))
               SInvDSN = SSource.trim();
          else if(SId.startsWith("Period"))
               SPeriod = SSource.trim();
          else if(SId.startsWith("User"))
               SUser = SSource.trim();
          else if(SId.startsWith("Start"))
               SFinSt = SSource.trim();
          else if(SId.startsWith("End"))
               SFinEn = SSource.trim();

              /* System.out.println(SFinSt);
               System.out.println(SFinEn);*/
     }

     // Reading the Finance.properties File
     private String  getDSNString()
     {
          String str="";
          try
          {
               Reader in = new FileReader("Inventory.properties");
               char[] buff = new char[4096];
               int nch;
               while ((nch = in.read(buff, 0, buff.length)) != -1)
                   str = str+new String(buff, 0, nch);
          }
          catch(Exception ex)
          {
          }
          return str;
     }

     // Returing the Finance Period

     public String getFinancePeriod()
     {
          return SPeriod;
     }

     public String getFinanceStart()
     {
          return SFinSt;
     }
     public String getFinanceEnd()
     {
          return SFinEn;
     }
     // Returing the Previlaged User

     public String getUser()
     {
          return SUser;
     }

     // Returning the PYDSN()

     public String getPYDSN()
     {
            return "jdbc:odbc:Inventory";
     }

     public Vector getLines(String str)
     {
          Vector              VLines    = new Vector();
          String              SLine     = "";
          StringTokenizer     ST        = new StringTokenizer(str," ");

          while(ST.hasMoreElements())
          {
               String SWord = (ST.nextToken()).trim();
               if((SLine+" "+SWord).length() < 30)
               {
                    SLine = SLine+" "+SWord;
               }
               else
               {
                    VLines    . addElement(SLine.trim());
                    SLine     = SWord;
               }
          }
          if(SLine.length() > 0)
               VLines.addElement(SLine.trim());
          return VLines;
     }

     public int getMissing(Vector vect)
     {
          if (vect.size()==0)
               return 0;
          int iRetVal = 0;
          int iPValue = toInt((String)vect.elementAt(0));
          for(int i=0;i<vect.size();i++)
          {
               int iValue = toInt((String)vect.elementAt(i));
               if(iValue-iPValue > 1)
               {
                    iRetVal = iPValue+1;
                    break;
               }
               iPValue = iValue;
          }
          iRetVal = iPValue+1;
          return iRetVal;
     }

     public String stuff(String str)
     {
          str=str.trim();
          if(str.length()==0)
               return "000000";
          if(str.length()==1)
               return "00000"+str;
          if(str.length()==2)
               return "0000"+str;
          if(str.length()==3)
               return "000"+str;
          if(str.length()==4)
               return "00"+str;
          if(str.length()==5)
               return "0"+str;
          return str;
     }


     public String getServerDateTime2()
     {
          String SDate="";

          String QS = "Select to_Char(Sysdate,'DD/MM/YYYY HH24:MI:SS') From Dual";

          try
          {
               ORAConnection connect    = ORAConnection.getORAConnection();
               Connection theConnection = connect.getConnection();
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);

               if(theResult.next())
                    SDate = theResult.getString(1);

               theResult.close();
               theStatement.close();     
          }
          catch(Exception ex)
          {
               return "";
          }
          return SDate;
     }


     public String getServerDateTime()
     {
          String SDate= " " ;
                         
          String QS = "Select to_Char(Sysdate,'MM/DD/YYYY HH24:MI:SS') From Dual";

          try
          {
               ORAConnection connect = ORAConnection.getORAConnection();
               Connection theConnection = connect.getConnection();
               Statement theStatement = theConnection.createStatement();
               ResultSet theResult = theStatement.executeQuery(QS);
               if(theResult.next())
                    SDate = theResult.getString(1);
               theResult.close();
               theStatement.close();     
          }
          catch(Exception ex)
          {
               return "";
          }
          return SDate ;
     }

     public String getServerDate()
     {
          String SDate = " " ;
          String SQry="select to_Char(sysdate,'yyyymmdd hh24:mi:ss') from dual";
          try
          {
			ORAConnection connect = ORAConnection.getORAConnection();
			Connection theConnection = connect.getConnection();
			Statement theStatement = theConnection.createStatement();
			ResultSet theResult = theStatement.executeQuery(SQry);
               if(theResult.next())
               SDate = theResult.getString(1);

               theResult.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               return "";
          }
          return SDate ;
     }

     public String getServerPureDate()
     {
	  String SDate = "";
          String SQry="select to_Char(sysdate,'yyyymmdd') from dual";
          try
          {
			ORAConnection connect = ORAConnection.getORAConnection();
			Connection theConnection = connect.getConnection();
			Statement theStatement = theConnection.createStatement();
			ResultSet theResult = theStatement.executeQuery(SQry);
               if(theResult.next())
               SDate = theResult.getString(1);

               theResult.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               return "";
          }
          return SDate ;
     }

     public  int getConfigNo(int iType)
     {
          int iValue=0;

          String QS = "select  MaxNo from Config Where id="+iType+"  for update of MaxNo nowait ";

          try
          {
               ORAConnection connect = ORAConnection.getORAConnection();
               Connection theConnection = connect.getConnection();
               theConnection.setAutoCommit(false);
               Statement theStatement          = theConnection.createStatement();
               PreparedStatement  thePrepare   = theConnection.prepareStatement("Update Config set MaxNo=? where id=?" );
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    iValue               = result.getInt(1)+1;
               }

               thePrepare.setInt(1,iValue);
               thePrepare.setInt(2,iType);
               thePrepare.execute();
	       result.close();
               theStatement.close();
               thePrepare.close();
               theConnection.setAutoCommit(true);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return iValue;
     }
     public String getConcernName(int iMillCode)
     {
          String SQry="Select MillName from Mill Where MillCode="+iMillCode;
          try
          {
               ORAConnection connect = ORAConnection.getORAConnection();
               Connection theConnection = connect.getConnection();
               Statement theStatement = theConnection.createStatement();
               ResultSet theResult = theStatement.executeQuery(SQry);
               if(theResult.next())
                    return theResult.getString(1);
		theResult.close();
               theStatement.close();     
          }
          catch(Exception ex)
          {
               return "";
          }
          return "";
     }
     public String getID(String QueryString)
	{
          String ID = "";

          try
          {
               ORAConnection connect    = ORAConnection.getORAConnection();
               Connection theConnection = connect.getConnection();
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QueryString);

               while(theResult.next())
               {
                   ID = theResult.getString(1);
               }
               theResult.close();
               theStatement.close();
         }
         catch(SQLException SQLE)
         {
               System.out.println(SQLE);
               SQLE.printStackTrace();
         }
         return ID;
	}

     public String getLastNetRate(String SItemCode,int iMillCode)
     {
          String SRate="0";

          String QString  =   " Select nvl(Round(Sum(Net/Qty),4),0) as NetRate from purchaseorder "+
                              " where Rate > 0 And Qty > 0 and MillCode="+iMillCode+
                              " and Item_Code = '"+SItemCode+"' and "+
                              " id = (select max(id) from purchaseorder"+
                              " where orderdate = (select max(orderdate) from purchaseorder"+
                              " where MillCode="+iMillCode+" and item_code = '"+SItemCode+"')"+
                              " and MillCode="+iMillCode+" and  item_code = '"+SItemCode+"')";

          String QStrPYOrd=   " Select nvl(Round(Sum(Net/Qty),4),0) as NetRate from pyorder"+
                              " where Rate > 0 and Qty > 0 and MillCode="+iMillCode+
                              " and Item_Code = '"+SItemCode+"' and "+
                              " id = (select max(id) from pyorder"+
                              " where orderdate = (select max(orderdate) from pyorder"+
                              " where MillCode="+iMillCode+" and item_code = '"+SItemCode+"')"+
                              " and MillCode="+iMillCode+" and item_code = '"+SItemCode+"')";

          try
          {
               ORAConnection  connect        = ORAConnection.getORAConnection();
               Connection     theConnection  = connect.getConnection();
               Statement      stat           = theConnection.createStatement();
               ResultSet      result         = stat.executeQuery(QString);

               while(result.next())
                    SRate     =    result.getString(1);

               result    . close();

                    /*add in 04.04.2008 for nill stock use pyorder to get rate*/

               if(toDouble(SRate.trim())==0)
               {
                    result = stat.executeQuery(QStrPYOrd);
                    while(result.next())
                         SRate     =    result.getString(1);
     
                    result.close();
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

          return SRate;
     }

     public int isLeap1(int iYear)
     {
          double dYear = iYear;
          if(Math.IEEEremainder(dYear,100.0)==0)
               return 1;
          else if(Math.IEEEremainder(dYear,4.0)==0)
               return 1;
          else
               return 0;
     }

     public String getMonthEndDate(String SDate)
     {
            int    iMonth = toInt(SDate.substring(4,6));
            int    iYear  = toInt(SDate.substring(0,4));
            String SDay;
            switch (iMonth)
            {
                  case 1:
                        SDay="31";
                        break;   
                  case 2:
                        if(isLeap1(iYear)==1)
                           SDay="29";
                        else
                           SDay="28";
                        break;   
                  case 3:
                        SDay="31";
                        break;   
                  case 4:
                        SDay="30";
                        break;   
                  case 5:
                        SDay="31";
                        break;   
                  case 6:
                        SDay="30";
                        break;   
                  case 7:
                        SDay="31";
                        break;   
                  case 8:
                        SDay="31";
                        break;   
                  case 9:
                        SDay="30";
                        break;   
                  case 10:
                        SDay="31";
                        break;   
                  case 11:
                        SDay="30";
                        break;   
                  case 12:
                        SDay="31";
                        break;
                  default:
                        SDay="30";
                        break;
            }
            return SDay;
     }

     public String getNarration(String Str)
     {
          String SNarr="";

          StringTokenizer ST = new StringTokenizer(Str,"'");
          while(ST.hasMoreElements())
               SNarr =SNarr+ST.nextToken()+"''";

          if(SNarr.length()==0)
               return "";

          return SNarr.substring(0,SNarr.length()-2);
     }

     public String getShortName(int iMillCode)
     {
          String SQry="Select ShortName from Mill Where MillCode="+iMillCode;
          try
          {
               ORAConnection connect = ORAConnection.getORAConnection();
               Connection theConnection = connect.getConnection();
               Statement theStatement = theConnection.createStatement();
               ResultSet theResult = theStatement.executeQuery(SQry);
               if(theResult.next())
                    return theResult.getString(1);
		theResult.close();
               theStatement.close();     
          }
          catch(Exception ex)
          {
               return "";
          }
          return "";
     }
     public String getLastNetRateAll(String SItemCode,int iMillCode)
     {
          String SRate="0";

          String QString  =   " Select nvl(Round(Sum(Net/Qty),4),0) as NetRate from purchaseorder "+
                              " where Rate > 0 And Qty > 0 "+ //  and MillCode="+iMillCode+
                              " and Item_Code = '"+SItemCode+"' and "+
                              " id = (select max(id) from purchaseorder"+
                              " where orderdate = (select max(orderdate) from purchaseorder"+
                              " where  item_code = '"+SItemCode+"')"+ //MillCode="+iMillCode+" and
                              "  and  item_code = '"+SItemCode+"')"; //and MillCode="+iMillCode+"

          String QStrPYOrd=   " Select nvl(Round(Sum(Net/Qty),4),0) as NetRate from pyorder"+
                              " where Rate > 0 and Qty > 0  "+  // and MillCode="+iMillCode+
                              " and Item_Code = '"+SItemCode+"' and "+
                              " id = (select max(id) from pyorder"+
                              " where orderdate = (select max(orderdate) from pyorder"+
                              " where item_code = '"+SItemCode+"')"+ // MillCode="+iMillCode+" and
                              " and  item_code = '"+SItemCode+"')"; // MillCode="+iMillCode+" and

          try
          {
               ORAConnection  connect        = ORAConnection.getORAConnection();
               Connection     theConnection  = connect.getConnection();
               Statement      stat           = theConnection.createStatement();
               ResultSet      result         = stat.executeQuery(QString);

               while(result.next())
                    SRate     =    result.getString(1);

               result    . close();

                    /*add in 04.04.2008 for nill stock use pyorder to get rate*/

               if(toDouble(SRate.trim())==0)
               {
                    result = stat.executeQuery(QStrPYOrd);
                    while(result.next())
                         SRate     =    result.getString(1);
     
                    result.close();
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

          return SRate;
     }

     public String getLocalHostName()
     {
          String         SComName="";
          try
          {
               InetAddress    host      =    InetAddress    . getLocalHost();
               SComName                 =    host           . getHostName();
          }catch(Exception ex)
          {
               System.out.println(ex);
          }

          return SComName;
     }

     public int getNextDate(int int1)
     {
            String str1 = parseDate(String.valueOf(int1));
            String SDay="",SMonth="",SYear="";
            int iYear1=0,iMonth1=0,iDay1=0;
            try
            {
               iYear1  = toInt(str1.substring(6,10))-1900;
               iMonth1 = toInt(str1.substring(3,5))-1;
               iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
               return 0;
            }
            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);

            long num1 = dt1.getTime();
            long num2 = (24*60*60*1000);
             dt1.setTime(num1+num2);

            int iDay   = dt1.getDate();
            int iMonth = dt1.getMonth()+1;
            int iYear  = dt1.getYear()+1900;

            SDay   = (iDay<10 ?"0"+iDay:""+iDay);
            SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
            SYear  = ""+iYear;
            return toInt(SYear+SMonth+SDay);
     }

     public String getRupee(String str)
     {
          str = getRound(str,2);
          int  lRupee=0;
          int  iPaise=0;
          java.util.StringTokenizer ST = new java.util.StringTokenizer(str,".");
          while(ST.hasMoreTokens())
          {
               lRupee=toInt(ST.nextToken());
               iPaise=toInt(ST.nextToken());
          }
          String SRs = (lRupee > 0 ? getCrores(lRupee):"Zero");
          String SPs = (iPaise > 0 ? " and paise "+getTens(iPaise):"");
          return "Rupees "+SRs+SPs+"Only";
     }

     private String getCrores(int i)
     {
          if(i<10000000)
               return getLacs(i);
          String str="";
          int j=0,k=0;
          if(i<=99999999)
          {
               str   = String.valueOf(i);
               j        = toInt(str.substring(0,1));
               k        = toInt(str.substring(1,8));
               return getSingle(j)+"Crore "+getLacs(k);
          }
          str   = String.valueOf(i);
          j        = toInt(str.substring(0,2));
          k        = toInt(str.substring(2,9));
          return getTens(j)+"Crore "+getLacs(k);
     }

     private String getLacs(int i)
     {
          if(i<100000)
               return getThousands(i);

          String str="";
          int j=0,k=0;
          if(i<=999999)
          {
               str   = String.valueOf(i);
               j        = toInt(str.substring(0,1));
               k        = toInt(str.substring(1,6));
               return getSingle(j)+"Lac "+getThousands(k);
          }
          str   = String.valueOf(i);
          j        = toInt(str.substring(0,2));
          k        = toInt(str.substring(2,7));
          return getTens(j)+"Lac "+getThousands(k);
     }

     private String getThousands(int i)
     {
          if(i<1000)
               return getHundreds(i);

          String str="";
          int j=0,k=0;
          if(i<=9999)
          {
               str   = String.valueOf(i);
               j        = toInt(str.substring(0,1));
               k        = toInt(str.substring(1,4));
               return getSingle(j)+"Thousand "+getHundreds(k);
          }
          str   = String.valueOf(i);
          j        = toInt(str.substring(0,2));
          k        = toInt(str.substring(2,5));
          return getTens(j)+"Thousand "+getHundreds(k);
     }

     private String getHundreds(int i)
     {
          if(i<100)
               return getTens(i);

          String str="";
          int j=0,k=0;
          str   = String.valueOf(i);
          j        = toInt(str.substring(0,1));
          k        = toInt(str.substring(1,3));
          return getSingle(j)+"Hundred "+getTens(k);
     }

     private String getTens(int i)
     {
          if(i==0)
               return "";
          if(i >= 0 && i <= 9)
               return getSingle(i);
          if(i >= 10 && i <= 19)
               return S10To19[i-10];

          String str   = String.valueOf(i);
          int j        = toInt(str.substring(0,1))-2;
          int k        = toInt(str.substring(1,2));
          return TensArr[j]+(k>0?getSingle(k):"");
     }

     private String getSingle(int i)
     {
          return S0To9[i];          
     }

     public String getCapitalWordString(String str)
     {
          char[] stringArray = str.toLowerCase().toCharArray();

          boolean found = false;

          for(int i=0;i<stringArray.length;i++)
          {
               if(!found && Character.isLetter(stringArray[i]))
               {
                    stringArray[i] = Character.toUpperCase(stringArray[i]);
                    found = true;
               }
               else
               if (Character.isWhitespace(stringArray[i]))
               {
                    found = false;
               }
          }

          return String.valueOf(stringArray);
     }

     public String getMonthName(int iMonth)
     {
                String SArr[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
                String SMonth = null;
                try
                {
                    SMonth = String.valueOf(iMonth);
                    iMonth = toInt(SMonth.substring(4,6))-1;
                    int iYear  = toInt(SMonth.substring(0,4));  
                    SMonth = SArr[iMonth]+" "+iYear;
                }
                catch(Exception ex)
                {
                    if(SMonth.length()==4)
                         return "EL'"+SMonth;
     
                    return "";
                }
                return SMonth;
     }

     public int getNextMonth(int iMonth)
     {
          iMonth = toInt(String.valueOf(iMonth).substring(0,6));
          String SMonth = String.valueOf(iMonth);
          String STmp = "";
          int iYear = toInt(SMonth.substring(0,4));
          int iNextMonth = iMonth+1;
          if(String.valueOf(iNextMonth).substring(4,6).equals("13"))
               STmp=String.valueOf(iYear+1)+"01";
          else
               STmp=String.valueOf(iNextMonth);

          return toInt(STmp);
     }     

	public String getPrintPath()
	{
		String SFilePath="";
		String workingdir = System.getProperty("user.dir");
		String sfile = "test.txt";
		File fileName = new File(workingdir,sfile);
		File[] root = fileName.listRoots();
		String OSName = System.getProperty("os.name");
		String SActFile="";
		for (int i=0;i<root.length ;i++ )
		{
			String SCurFile = root[i].getAbsolutePath();
			if(OSName.startsWith("Lin"))
			{
				//SActFile = SCurFile;
				SActFile = SCurFile+"software"+System.getProperty("file.separator");
				break;
			}
			else
			{
				if(SCurFile.startsWith("D"))
				{
					SActFile = SCurFile;
					break;
				}
			}
		}

          SFilePath = SActFile+"reports"+System.getProperty("file.separator");

		return SFilePath;
	}

	public String getMailPath()
	{
		String SFilePath="";
		String workingdir = System.getProperty("user.dir");
		String sfile = "test.txt";
		File fileName = new File(workingdir,sfile);
		File[] root = fileName.listRoots();
		String OSName = System.getProperty("os.name");
		String SActFile="";
		for (int i=0;i<root.length ;i++ )
		{
			String SCurFile = root[i].getAbsolutePath();
			if(OSName.startsWith("Lin"))
			{
				//SActFile = SCurFile;
				SActFile = SCurFile+"software"+System.getProperty("file.separator");
				break;
			}
			else
			{
				if(SCurFile.startsWith("X"))
				{
					SActFile = SCurFile;
					break;
				}
			}
		}

		SFilePath = SActFile+"reports"+System.getProperty("file.separator");

		return SFilePath;
	}

     public int getPreviousMonth(int iMonth)
     {
		String SMonth = null;
		SMonth = String.valueOf(iMonth);
		int iYear = toInt(SMonth.substring(0,4));
		String SPreMonth = String.valueOf(iMonth-1);
		if(SPreMonth.substring(4,6).equals("00"))
			SPreMonth=String.valueOf(iYear-1)+"12";
		return toInt(SPreMonth);
     }
     public String getCurrentDate()
     {
          Connection theConnection = null;
          String SServerDate       = "";

          try
          {

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
     
               Statement stat   = theConnection.createStatement();
               ResultSet result = stat.executeQuery(getDateQS());
               if(result.next())
                    SServerDate = result.getString(1);
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return SServerDate;
     }
     private String getDateQS()
     {
          String QS = "Select to_Char(sysdate,'yyyymmdd') from Dual";
          return QS;
     }


}
