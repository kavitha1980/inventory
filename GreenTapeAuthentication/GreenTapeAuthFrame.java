package GreenTapeAuthentication;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.net.InetAddress;
import java.io.*;

//import util.*;
import jdbc.*;
import guiutil.*;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.SheetSettings;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.format.PageOrientation;
import jxl.format.PaperSize;


import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;

import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class GreenTapeAuthFrame extends JInternalFrame
{
    JPanel       pnlTop,pnlMiddle,pnlBottom,pnlPayment,pnlRate,pnlControls,pnlNotes;
    JTable       theTable=null;
    GreenTapeAuthModel theModel=null;
    JButton      btnApply,btnSave,btnExit,btnPrint,btnAttachment;
    JComboBox    cmbContractor;
    JLayeredPane Layer;
    AttDateField txtFromDate,txtToDate;
    Vector       VData,VContractor,VContractorCode,VAuthData,VContractorEmpCode;
    int          iFromDate,iToDate;
    double       dRatePerTape=0.0;
    Common       common;
    String       SSystemName,SContractor;
    int          iRow,iUserCode,iAuthCode,iMillCode,iFlowCode,iAuthUserCode;
    JLabel       lblTotalAmount,lblTotalTapes,lblTotalBundles,lblRate,lblCompletedTapes,lblAmount,lblOpening,lblRoundedNet;
    WritableSheet    sheet;
    WritableWorkbook workbook;


    Document document;
    PdfPTable table;

    //set Model Index
    int sno=0,contractor=1,bundles=2,tapes=3,totalamount=4,opening=5,roundednet=6; 
   
    public GreenTapeAuthFrame(JLayeredPane Layer,int iUserCode,int iAuthCode,int iMillCode,int iAuthUserCode)
    {
        this . Layer     = Layer;
        this . iUserCode = iUserCode;
        this . iAuthCode = iAuthCode;
        this . iMillCode = iMillCode;
        this . iAuthUserCode = iAuthUserCode;    

        common = new Common();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
       if(iAuthUserCode!=7225)
       {
        setAuthenticationDate();
       }
        System.out.println(" AuthUserCode "+iAuthUserCode);
        try
        {
            SSystemName = InetAddress.getLocalHost().getHostName();
        }
        catch(Exception ex)
        {
           System.out.println(ex);
        }
    }

    private void createComponents()
    {
        pnlTop      = new JPanel();
        pnlMiddle   = new JPanel();
        pnlBottom   = new JPanel();
        pnlPayment  = new JPanel();
        pnlControls = new JPanel();
        pnlNotes    = new JPanel();
        pnlRate     = new JPanel();
        
        txtFromDate = new AttDateField(10);
        txtToDate   = new AttDateField(10);
        
        btnApply   = new JButton(" Apply ");
        btnSave    = new JButton(" Authenticate ");
        btnExit    = new JButton(" Exit ");
        btnPrint   = new JButton(" Voucher Print ");
        btnAttachment = new JButton("Green Tape Rate Approval Attachment");
        
        lblTotalAmount  = new JLabel("");
        lblTotalBundles = new JLabel("");
        lblTotalTapes   = new JLabel("");
        lblRate         = new JLabel("");
        lblCompletedTapes = new JLabel("");
        lblAmount         = new JLabel("");
        lblOpening        = new JLabel("");
        lblRoundedNet     = new JLabel("");
        
        lblRate           . setForeground(Color.red);
        lblCompletedTapes . setForeground(Color.red);
        lblAmount         . setForeground(Color.red);
        
        setContractor();
        cmbContractor  = new JComboBox(VContractor);
        theModel   = new GreenTapeAuthModel();
        theTable   = new JTable(theModel);
    }
    private void setLayouts()
    {
        pnlTop    . setLayout(new GridLayout(1,6));
        pnlMiddle . setLayout(new BorderLayout());
        pnlBottom . setLayout(new GridLayout(2,2,3,3));
        pnlPayment. setLayout(new GridLayout(6,2,3,3));
        pnlControls . setLayout(new GridLayout(2,2,3,3));
        pnlNotes    . setLayout(new GridLayout(2,1,3,3));
        pnlRate     . setLayout(new GridLayout(3,1,3,3));
        
        pnlTop       . setBorder(new TitledBorder("Title"));
        pnlMiddle    . setBorder(new TitledBorder("Data"));
        pnlBottom    . setBorder(new TitledBorder("Controls"));
        pnlPayment   . setBorder(new TitledBorder(""));
        pnlControls  . setBorder(new TitledBorder(""));
        pnlNotes     . setBorder(new TitledBorder(""));
        //pnlRate      . setBorder(new TitledBorder(""));
        
        this . setSize(750,600);
        this . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this . setTitle(" Green Tape Authentication Frame");
        this . setClosable(true);
        this . setMaximizable(true);
        this . setResizable(true);
    }
    private void addComponents()
    {
        pnlTop . add(new JLabel(" From Date"));
        pnlTop . add(txtFromDate);
        pnlTop . add(new JLabel(" To Date"));
        pnlTop . add(txtToDate);
        pnlTop . add(new JLabel(" "));
        pnlTop . add(btnApply);
        
        pnlMiddle . add(new JScrollPane(theTable));
        
        pnlControls . add(btnSave);
//        pnlControls . add(btnPrint);
        pnlControls . add(btnExit);

        pnlControls . add(new JLabel(""));
        pnlControls . add(new JLabel(""));
        //pnlControls . add(new JLabel(""));

        pnlPayment  . add(new JLabel("Approved Value/Tape"));
        pnlPayment  . add(lblRate);
        pnlPayment  . add(new JLabel("Total No Of Tapes Completed"));
        pnlPayment  . add(lblCompletedTapes);
        pnlPayment  . add(new JLabel("Total Payment to Issue"));
        pnlPayment  . add(lblAmount);
        pnlPayment  . add(new JLabel("Total Opening"));
        pnlPayment  . add(lblOpening);
        pnlPayment  . add(new JLabel("Total Rounded Net"));
        pnlPayment  . add(lblRoundedNet);

        
        
        pnlNotes . add(new JLabel("1.Press F3--> Datewise Green Tape Received Details  "));
        pnlNotes . add(new JLabel("2.Press F2--> Green Tape Inward Details   "));
        
        //pnlRate   . add(btnAttachment);
        pnlRate   . add(new JLabel(""));
        pnlRate   . add(new JLabel(""));
        
        pnlBottom . add(pnlPayment);
        pnlBottom . add(pnlControls);
        pnlBottom . add(pnlNotes);
        pnlBottom . add(pnlRate);
        
        this . add(pnlTop,BorderLayout.NORTH);
        this . add(pnlMiddle,BorderLayout.CENTER);
        this . add(pnlBottom,BorderLayout.SOUTH);
    }
    private void addListeners()
    {
        btnApply . addActionListener(new ActList());
        btnSave  . addActionListener(new ActList());
        btnExit  . addActionListener(new ActList());
        btnPrint . addActionListener(new ActList());
        btnAttachment . addActionListener(new ActList());
        theTable . addKeyListener(new KeyList());
    }
    private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==btnApply)
            {
                iFromDate = common.toInt(txtFromDate.toNormal());
                iToDate   = common.toInt(txtToDate  .toNormal());
               
                setRate();
                setTableData();
            }
            if(ae.getSource()==btnSave)
            {

                int iSaveVal=-1;
               
                iSaveVal = insertAuthData();   
               
                if(iSaveVal==1)
                 JOptionPane .showMessageDialog(null,"Data Saved Successfully","Info",JOptionPane.INFORMATION_MESSAGE);
                else if(iSaveVal==0)
                 JOptionPane .showMessageDialog(null,"Problem in Save Data","Error",JOptionPane.ERROR_MESSAGE);  
                else if(iSaveVal==3)               
                 JOptionPane.showMessageDialog(null," Please check the Previous Authorization", "Info",JOptionPane.WARNING_MESSAGE);
               
               setTableData();
            }  
            if(ae.getSource()==btnExit)
            {
                dispose();
            }
            if(ae.getSource()==btnAttachment)
            {
                GreenTapeRateApprovalView theFrame = new GreenTapeRateApprovalView();
                theFrame . setVisible(true);
            }
            if(ae.getSource()==btnPrint)
            {
                
              try
              {
                
                initExcelFile();
              //  String SPdfFile          = "D:/GreenTapePaymentVoucher.pdf";
                String SPdfFile          = common.getPrintPath()+"GreenTapePaymentVoucher.pdf";
                String SServerDate       = common.getServerPureDate();
                int iRow                 = 0;
                
                String SPurpose          = "Green Tape Fixing Work From "+common.parseDate(String.valueOf(iFromDate))+" To "+common.parseDate(String.valueOf(iToDate));

                document = new Document(PageSize.A4);
                PdfWriter.getInstance(document, new FileOutputStream(SPdfFile));
                document.open();
                for(int i=0;i<theModel.getRowCount();i++)
                {
                 if(i!=0)
                     document.newPage();

                 String SVocNo         = setVocNo();
                 HashMap theMap        = (HashMap)VAuthData.get(i);
                 int iContractorCode   = common.toInt((String)theMap.get("ContractorCode"));
                 String STransactionId = getTransactionId(iContractorCode);
                // double dAmount = common.toDouble((String)theModel.getValueAt(i, totalamount));
                  double dAmount       = common.toDouble((String)theModel.getValueAt(i, roundednet));
                 String SContractor    = (String)theModel.getValueAt(i,contractor);

                 VoucherPrintingExcel theExcel = new VoucherPrintingExcel(sheet,SContractor,SVocNo,common.parseDate(SServerDate),iRow,dAmount,SPurpose,VData,dRatePerTape);
                 iRow                  = theExcel.getRowNo();

                 
                 VoucherPDF thePdf     = new VoucherPDF(document,table,SContractor,SVocNo,common.parseDate(SServerDate),iRow,dAmount,SPurpose,VData,dRatePerTape,STransactionId);
                 
                }
                document.close();
                closeExcelFile();
                JOptionPane.showMessageDialog(null,"Printing file Created "+common.getPrintPath()+"GreenTapePayment.xls \n Voucher Pdf Created in "+SPdfFile,"Info",JOptionPane.INFORMATION_MESSAGE);
                 try
                 {
                     Process p = Runtime.getRuntime()
                     .exec("rundll32 url.dll,FileProtocolHandler  "+SPdfFile);
                     p.waitFor(); 
                 }
	         catch(Exception ex)
	         {
		   System.out.println(" viewPDF "+ex);
	         }
              }
              catch(Exception e)
              {
                  System.out.println(" Print "+e);
              }
            }
        }
    }
    private class KeyList extends KeyAdapter
    {
        public void keyPressed(KeyEvent ke)
        {
            if(ke.getKeyCode()==KeyEvent.VK_F3)
            {
               int iRow           = theTable.getSelectedRow();
               String SContractor = (String)theModel.getValueAt(iRow,contractor);
               HashMap theMap     = (HashMap)VAuthData.get(iRow);
               int iContractorCode            = common.toInt((String)theMap.get("ContractorCode"));
               int iContractorEmpCode         = common.toInt((String)theMap.get("ContractorEmpCode"));
               GreenTapeDetailsFrame theFrame = new GreenTapeDetailsFrame(iContractorCode,SContractor,iFromDate,iToDate,dRatePerTape);
               theFrame .setVisible(true); 
            }
            if(ke.getKeyCode()==KeyEvent.VK_F2)
            {
               int iRow = theTable.getSelectedRow();
               String SContractor = (String)theModel.getValueAt(iRow,contractor);
               HashMap theMap     = (HashMap)VAuthData.get(iRow);
               int iContractorCode                  = common.toInt((String)theMap.get("ContractorCode"));
               int iContractorEmpCode               = common.toInt((String)theMap.get("ContractorEmpCode"));
               GreenTapeInwardDetailsFrame theFrame = new GreenTapeInwardDetailsFrame(iContractorCode,iFromDate,iToDate);
               theFrame .setVisible(true); 
            }
        }
    }
     private void setTableData()
    {
        theModel . setNumRows(0);
        
        double dOpening=0.0,dCarriedOver=0.0,dRoundedNet=0.0;
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        theTable.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
        theTable.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
        theTable.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
        theTable.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
        theTable.getColumnModel().getColumn(6).setCellRenderer(rightRenderer);
        setDataVector();
        for(int i=0;i<VAuthData.size();i++)
        {
            HashMap theMap = (HashMap)VAuthData.get(i);


            Vector theVect = new Vector();
            theVect . addElement(String.valueOf(i+1));
            theVect . addElement((String)theMap.get("ContractorName"));
            theVect . addElement(String.valueOf(theMap.get("NoOfBundles")));
            theVect . addElement((String)theMap.get("NoOfTapes"));
            theVect . addElement((String)theMap.get("TotalAmount"));
            theVect . addElement((String)theMap.get("Opening"));
            theVect . addElement((String)theMap.get("RoundedNet"));
            theModel . appendRow(theVect);
        }
      
            lblTotalBundles. setText(String.valueOf(getNoOfBundles()));
            lblTotalTapes  . setText(String.valueOf(getNoOfTapes()));
            lblTotalAmount . setText(String.valueOf(getTotalAmount()));
            lblOpening     . setText(String.valueOf(getTotalOpening()));
            lblRoundedNet  . setText(String.valueOf(getTotalRoundedNet()));
            
            lblRate           . setText(String.valueOf(dRatePerTape));
            lblCompletedTapes . setText(String.valueOf(getNoOfTapes()));
            lblAmount         . setText(String.valueOf(getTotalAmount()));
            
    }
   
    private boolean isAuthExists(int iContractorCode,int iContractorEmpCode)
    {
        boolean bExist=true;
        int iCount=0;
        StringBuffer sb = new StringBuffer();
        sb.append(" Select count(1) from GreenTapeAuthentication ");
        sb.append(" where FromDate>="+iFromDate+" and ToDate<="+iToDate+" and ContractorCode="+iContractorCode+" and AuthUserCode="+iAuthUserCode);
        sb.append(" and ContractorEmpCode="+iContractorEmpCode);
        try
        {
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection    theConnection = connect.getConnection();
            
            Statement theStatement = theConnection.createStatement();
            ResultSet theResult    = theStatement.executeQuery(sb.toString());
            while(theResult.next())
            {
               iCount = theResult.getInt(1);
            }
            if(iCount>0)
                bExist=true;
            else
                bExist=false;
            theResult.close();
            theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println(" isAuthExists "+ex);
        }
        return bExist;
    }
    private void setContractor()
    {
        VContractor     = new Vector();
        VContractorCode = new Vector();
        VContractorEmpCode = new Vector();
        StringBuffer sb = new StringBuffer();
        sb.append(" Select ContractorCode,ContractorName,ContractorEmpCode from GreenTapeContractor order by 1");
        try
        {
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection    theConnection = connect.getConnection();
            
            Statement theStatement = theConnection.createStatement();
            ResultSet theResult    = theStatement.executeQuery(sb.toString());
            while(theResult.next())
            {
               VContractorCode . addElement(theResult.getString(1));
               VContractor     . addElement(theResult.getString(2));
               VContractorEmpCode . addElement(theResult.getString(3));
            }
        }
        catch(Exception ex)
        {
            System.out.println(" setDataVector "+ex);
        }
    }
   
    private void setDataVector()
    {
        VAuthData = new Vector();
        StringBuffer sb = new StringBuffer();
        sb.append(" select GreenTapeContractor.ContractorName,sum(NoOfTapes),GreenTapeDailyAuthentication.ContractorCode,nvl(GreenTapeDailyAuthentication.ContractorEmpCode,0) from GreenTapeDailyAuthentication");
        sb.append(" Inner join GreenTapeContractor on GreenTapeContractor.ContractorCode=GreenTapeDailyAuthentication.ContractorCode");
        sb.append(" where (GreenTapeDailyAuthentication.FinalAuthStatus=0  or GreenTapeDailyAuthentication.FinalAuthStatus is null) ");
        sb.append(" and GreenTapeDailyAuthentication.ReceivedDate>="+iFromDate+" and  GreenTapeDailyAuthentication.ReceivedDate<="+iToDate);
        //and GreenTapeAuthentication.ContractorCode="+iContractorCode
        sb.append(" group by GreenTapeContractor.ContractorName,GreenTapeDailyAuthentication.ContractorCode,nvl(GreenTapeDailyAuthentication.ContractorEmpCode,0)  order by 1");
        System.out.println("set DATA QS "+sb.toString());
        try
        {
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection    theConnection = connect.getConnection();

            Statement theStatement = theConnection.createStatement();
            ResultSet theResult    = theStatement.executeQuery(sb.toString());
            while(theResult.next())
            {
                HashMap theMap = new HashMap();
                int iContCode   = theResult.getInt(3);
                int iNoOfTapes  = theResult.getInt(2);
                double dTotalAmount  = iNoOfTapes*dRatePerTape;
                double dOpening      = getOpeningFromStaffWage(iContCode);    
                double dRoundedNet   = dTotalAmount+dOpening; 
              
                theMap . put("ContractorName",theResult.getString(1));
                theMap . put("NoOfTapes",theResult.getString(2));
                theMap . put("NoOfBundles",theResult.getInt(2)/50);
                theMap . put("TotalAmount",common.getRound(theResult.getInt(2)*dRatePerTape,2));
                theMap . put("ContractorCode",theResult.getString(3));
                theMap . put("ContractorEmpCode",theResult.getString(4));
                theMap . put("Opening",common.getRound(dOpening,2));
                theMap . put("RoundedNet",common.getRound(dRoundedNet,2));
                VAuthData  . addElement(theMap);
            }
        }
        catch(Exception ex)
        {
            System.out.println(" setDataVector "+ex);
        }
    }
    private void setRate()
    {
        StringBuffer sb = new StringBuffer();
        sb.append(" Select RatePerTape from GreenTapeRate ");
        sb.append(" where "+iFromDate+">=WithEffectFrom and "+iToDate+"<=WithEffectTo");
       
        try
        {
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection    theConnection = connect.getConnection();
            
            Statement theStatement = theConnection.createStatement();
            ResultSet theResult    = theStatement.executeQuery(sb.toString());
            while(theResult.next())
            {
               dRatePerTape = theResult.getDouble(1);
            }
            theResult.close();
            theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println(" setDataVector "+ex);
        }
    }
   
   
   private int insertAuthData()
    {
        int iAuthenticate =-1,iAlreadyExist=0;
        int iFinalAuthStatus=0;
        if(iAuthUserCode==1980)
            iFinalAuthStatus=1;
        for(int i=0;i<theModel.getRowCount();i++)
        {
          int iNoOfTapes       = common.toInt((String)theModel.getValueAt(i,tapes));
          int iContractorCode  = getContractorCode(i);
          int iContractorEmpCode  = getContractorEmpCode(i);
          double dAmount          = common.toDouble((String)theModel.getValueAt(i, totalamount));
          double dOpening         = common.toDouble((String)theModel.getValueAt(i, opening));
          double dCarriedOver     = 0;
          double dRoundedNet      = common.toDouble((String)theModel.getValueAt(i, roundednet));
          if(!isAuthExists(iContractorCode,iContractorEmpCode))   
          {
           if(AuthenticationFlow(iContractorCode,iContractorEmpCode)==1)
           {

           StringBuffer sb = new StringBuffer();
           sb.append(" insert into GreenTapeAuthentication(FromDate,ToDate,NoOfTapes,AuthDate,AuthDateTime,AuthStatus,AuthUserCode,AuthSystemName,ContractorCode,FinalAuthStatus,TotalAmount,ContractorEmpCode,Id,Opening,CarriedOver,RoundedNet)");
           sb.append(" values(?,?,?,to_Char(sysdate,'yyyymmdd'),to_Char(sysdate,'dd-mm-yyyy hh24:mi:ss'),?,?,?,?,?,?,?,GreenTapeAuth_Seq.nextVal,?,?,?)");
           try
           {  
                ORAConnection connect    = ORAConnection.getORAConnection();
                Connection theConnection = connect.getConnection();

                PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());

                theStatement . setInt(1,iFromDate);
                theStatement . setInt(2,iToDate);
                theStatement . setInt(3,iNoOfTapes);
                theStatement . setInt(4,1);
                theStatement . setInt(5,iAuthUserCode);
                theStatement . setString(6,SSystemName);
                theStatement . setInt(7,iContractorCode);
                theStatement . setInt(8,iFinalAuthStatus);
                theStatement . setDouble(9,dAmount);
                theStatement . setInt(10,iContractorEmpCode);
                theStatement . setDouble(11,dOpening);
                theStatement . setDouble(12,dCarriedOver);
                theStatement . setDouble(13,dRoundedNet);
                theStatement . executeUpdate();
                iAuthenticate=1;
                theStatement.close();
            }
            catch(Exception ex)
            {
                iAuthenticate=0;
                System.out.println(" authenticate Data "+ex);
            }
            }
            else
            {
              iAuthenticate=3;
            }
           }
           else
           {
               iAlreadyExist = iAlreadyExist+1;
           }
          } 
      
        if(iAlreadyExist==theModel.getRowCount())
          JOptionPane.showMessageDialog(null,"Data Already Authenticated ","Info",JOptionPane.INFORMATION_MESSAGE);
           
        return iAuthenticate;
    } 
 private int AuthenticationFlow(int iContractorCode,int iContractorEmpCode)
    {
       int iFlow=0;
        if(iAuthUserCode==7225)
           iFlow=1;
       try
       {
        
        StringBuffer sb=new StringBuffer();
        sb.append("select FromDate,ToDate,AuthUserCode from GreenTapeAuthentication where FromDate="+iFromDate+" and ToDate="+iToDate+" and ContractorCode="+iContractorCode+" and ContractorEmpCode = "+iContractorEmpCode);
        
        ORAConnection connect = ORAConnection.getORAConnection();
        Connection theConnection = connect.getConnection();
        
              PreparedStatement ps=theConnection.prepareStatement(sb.toString());
              
              ResultSet rst=ps.executeQuery();
              while (rst.next())
              {
                 int    iFromdate = rst.getInt(1); 
                 int    iTodate   = rst.getInt(2); 
                 int    iAuthUsercode = rst.getInt(3);

                 if(iAuthUserCode==1987)
                 {
                      if(iAuthUsercode==1526)
                      iFlow=1;
                 }
              }
             rst.close();
             ps.close();
             ps=null;
     }
     catch(Exception e)
     {   
         e.printStackTrace();
         System.out.println("AuthenticationFlow "+e);
      }
    
  return iFlow;
}
 private void setAuthenticationDate()
    {
        StringBuffer sb = new StringBuffer();
        sb.append(" select Min(FromDate),Min(ToDate) from GreenTapeAuthentication ");
        sb.append(" where (FinalAuthStatus=0 or FinalAuthStatus is null)");
        try
        {
            ORAConnection connect    = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();
            
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();
            while(theResult.next())
            {
               String SFromDate = theResult.getString(1);
               String SToDate   = theResult.getString(2);
               txtFromDate .fromString(common.parseDate(SFromDate));
               txtToDate   .fromString(common.parseDate(SToDate));
                 
            }
        }
        catch(Exception e)
        {
            System.out.println(" set Payment Date "+e);
        }
    }

   private String setVocNo()
   {
      String SVocNo="";
      StringBuffer sb = new StringBuffer();
      sb.append(" Select GreenTapePay_Seq.nextVal from dual");
      try
      {
          ORAConnection connect = ORAConnection.getORAConnection();
          Connection    theConnection = connect.getConnection();
            
            Statement theStatement = theConnection.createStatement();
            ResultSet theResult    = theStatement.executeQuery(sb.toString());
            while(theResult.next())
            {
               SVocNo = theResult.getString(1);

            }
      }
      catch(Exception ex)
      {
          System.out.println(" setVocNo "+ex);
      }
      return SVocNo;
  }
  private int getNoOfBundles()
  {
      int iBundles=0;  
      for(int i=0;i<VAuthData.size();i++)
      {
          HashMap theMap = (HashMap)VAuthData.get(i);
          iBundles  = iBundles+common.toInt(String.valueOf(theMap.get("NoOfBundles")));
      }
      return iBundles;
    }
    private int getNoOfTapes()
    {
      int iTapes=0;  
      for(int i=0;i<VAuthData.size();i++)
      {
          HashMap theMap = (HashMap)VAuthData.get(i);
          iTapes  = iTapes+common.toInt((String)theMap.get("NoOfTapes"));
         
      }
      return iTapes;
    }
    private double getTotalAmount()
    {
      double dAmount=0;  
      for(int i=0;i<VAuthData.size();i++)
      {
          HashMap theMap = (HashMap)VAuthData.get(i);
          dAmount  = dAmount+common.toDouble((String)theMap.get("TotalAmount"));
      }
      return dAmount;
    }
    private double getTotalOpening()
    {
      double dOpening=0;  
      for(int i=0;i<VAuthData.size();i++)
      {
          HashMap theMap = (HashMap)VAuthData.get(i);
          dOpening  = dOpening+common.toDouble((String)theMap.get("Opening"));
      }
      return dOpening;
    }
    private double getTotalRoundedNet()
    {
      double dRoundedNet = 0;  
      for(int i=0;i<VAuthData.size();i++)
      {
          HashMap theMap = (HashMap)VAuthData.get(i);
          dRoundedNet  = dRoundedNet+common.toDouble((String)theMap.get("RoundedNet"));
      }
      return dRoundedNet;
    }

    private int getContractorCode(int iRow)
    {
       int iContractorCode=-1;
       HashMap theMap  =(HashMap)VAuthData.get(iRow);
       iContractorCode = common.toInt((String)theMap.get("ContractorCode"));
       return iContractorCode;      
    }
    private int getContractorEmpCode(int iRow)
    {
       int iContractorEmpCode=-1;
       HashMap theMap  =(HashMap)VAuthData.get(iRow);
       iContractorEmpCode = common.toInt((String)theMap.get("ContractorEmpCode"));
       return iContractorEmpCode;      
    }
     private void initExcelFile()
     {
          try
          {
              // workbook  = Workbook.createWorkbook(new File("d://GreenTapePayment.xls"));
               workbook  = Workbook.createWorkbook(new File(common.getPrintPath()+"GreenTapePayment.xls"));
               sheet     = workbook.createSheet("GreenTape",0);

              // sheet     . getSettings().setPaperSize(PaperSize.LEGAL);
              // sheet     . getSettings().setOrientation(PageOrientation.LANDSCAPE);
               sheet     . getSettings().setPaperSize(PaperSize.A4);
               sheet     . getSettings().setOrientation(PageOrientation.PORTRAIT);
               sheet     . getSettings().setTopMargin(0.5);
               sheet     . getSettings().setBottomMargin(0.5);
               sheet     . getSettings().setLeftMargin(0.5);
               sheet     . getSettings().setRightMargin(0.5);
               sheet     . getSettings().setProtected(false);
               sheet     . getSettings().setPassword("Print");
               sheet     . getSettings().setFitToPages(true);
               sheet     . getSettings().setDefaultColumnWidth(7);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
     private void closeExcelFile()
     {
          try
          {
               workbook.write();
               workbook.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
  public String getTransactionId(int iContractorCode)
  {
   String STransactionId="";

   try
   {
       StringBuffer sb = new StringBuffer();
       sb.append(" select Id from GreenTapeAuthentication where FromDate="+iFromDate+" and ToDate="+iToDate+" and ContractorCode="+iContractorCode+" and AuthUserCode=7225");
         
       System.out.println(" getTransaction id "+sb.toString());
       
          ORAConnection connect    = ORAConnection.getORAConnection();
          Connection theConnection = connect.getConnection();
          
          PreparedStatement ps=theConnection.prepareStatement(sb.toString());
          ResultSet rst=ps.executeQuery();
          while (rst.next())
          { 
             STransactionId = rst.getString(1);         
          }
          rst.close();
          ps.close();
   }
   catch(Exception ex)
   {
        System.out.println(" getTransactionId "+ex);
   }
   return STransactionId;
 }
 private double getOpeningFromStaffWage(int iContCode)
 {
    int iSalMonth        = common.toInt(String.valueOf(iToDate).substring(0,6));    
    int iPrevSalMonth    = common.getPreviousMonth(iSalMonth);
    String SPrevFromDate = String.valueOf(iPrevSalMonth)+"01";
    String SPrevToDate   = String.valueOf(iPrevSalMonth)+common.getMonthEndDate(String.valueOf(iPrevSalMonth));  


    double dStaffWage  = 0.0,dRoundedNet=0.0,dOpening = 0.0;
    StringBuffer sb  = new StringBuffer();
    StringBuffer sb1 = new StringBuffer();
    sb.append(" select sum(NetPay+Others) from StaffWage ");
//    sb.append(" inner join staff on staff.empcode = StaffWage.EmpCode and StaffWage.SalaryMonth="+iPrevSalMonth);
	sb.append(" where StaffWage.SalaryMonth="+iPrevSalMonth);
    if(iContCode==1)
        sb.append(" and Staffwage.ContractType=10");
    else if(iContCode==2)
        sb.append(" and Staffwage.ContractType=2");
 
    sb1.append("select sum(RoundedNet) from GreenTapePayment where ContractorCode= "+iContCode+" and FromDate>="+SPrevFromDate+" and ToDate <= "+SPrevToDate); 


    System.out.println(" STAFFWAGE "+sb.toString());
    System.out.println(" GREPAY "+sb1.toString());
    try
    {
          Class.forName("oracle.jdbc.OracleDriver");
          Connection theConnect   = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","hrdnew","hrdnew");
          
          PreparedStatement theStatement = theConnect.prepareStatement(sb.toString());
          ResultSet         theResult    = theStatement.executeQuery();
          while (theResult.next())
          { 
             dStaffWage   = theResult.getDouble(1);
          }
          theStatement . close();
          theResult    . close();


          ORAConnection connect    = ORAConnection.getORAConnection();
          Connection theConnection = connect.getConnection();
          
          PreparedStatement ps     = theConnection.prepareStatement(sb1.toString());
          ResultSet         rst    = ps.executeQuery();
          while (rst.next())
          { 
             dRoundedNet  = rst.getDouble(1);
          }
          rst.close();
          ps.close();

         
         dOpening  =  dRoundedNet - dStaffWage;
         System.out.println("STAFF WAGE "+dStaffWage+" ROUNDED  "+dRoundedNet+" OPENING "+dOpening);
    }
    catch(Exception ex)
    {
      System.out.println(" getOpeningFromStaffWage  "+ex);
    }  
    return dOpening;    
 }   
}
