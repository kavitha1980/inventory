
package GreenTapeAuthentication;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GreenTapeDetailsFrame extends JDialog
{
    JPanel    pnlTop,pnlMiddle,pnlBottom;
    JTable    theTable=null;
    GreenTapeDetailsModel theModel=null;
    JButton   btnOk;
    int       iFromDate,iToDate,iContractorCode;
    double    dRatePerTape;
    String    SContractorName;
    ArrayList theDataList = null;
    Common    common = null;
    public GreenTapeDetailsFrame(int iContractorCode,String SContractorName,int iFromDate,int iToDate,double dRatePerTape)
    {
        this . iContractorCode = iContractorCode;
        this . SContractorName = SContractorName;        
        this . iFromDate       = iFromDate;
        this . iToDate         = iToDate;
        this . dRatePerTape    = dRatePerTape;
        common = new Common();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
        setTableData();
    }
    private  void createComponents()
    {
       pnlTop    = new JPanel();
       pnlMiddle = new JPanel();
       pnlBottom = new JPanel();
       
       btnOk     = new JButton("  OK  ");
       
       theModel  = new GreenTapeDetailsModel();
       theTable  = new JTable(theModel);
       
       
       DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
       rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
       theTable.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
       theTable.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
       theTable.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
    }
    private void setLayouts()
    {
        pnlTop    . setLayout(new FlowLayout(FlowLayout.CENTER));
        pnlMiddle . setLayout(new BorderLayout());
        pnlBottom . setLayout(new FlowLayout(FlowLayout.CENTER));
     
        pnlTop    . setBorder(new TitledBorder("Title"));
        pnlMiddle . setBorder(new TitledBorder("Data"));
        pnlBottom . setBorder(new TitledBorder("Controls"));
        
        this . setTitle(" Green Tape Received Details");
        this . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this . setSize(500,500);
        this . setModal(true);
        
    }
    private void addComponents()
    {
        pnlTop . add(new JLabel(" Green Tape Details for "+SContractorName));
        pnlMiddle . add(new JScrollPane(theTable));
        pnlBottom . add(btnOk);
        
        this . add(pnlTop,BorderLayout.NORTH);
        this . add(pnlMiddle,BorderLayout.CENTER);
        this . add(pnlBottom,BorderLayout.SOUTH);
    }
    private void addListeners()
    {
        btnOk.addActionListener(new ActList());
    }
    private void setTableData()
    {
        theModel.setNumRows(0);
        setDetails();
        for(int i=0;i<theDataList.size();i++)
        {
            HashMap theMap   = (HashMap)theDataList.get(i);
            double dNoOfTapes = common.toDouble(String.valueOf(theMap.get("NoOfTapes")));
            Vector theVect = new Vector();
            theVect . addElement(String.valueOf(i+1));
            theVect . addElement(common.parseDate((String)theMap.get("ReceivedDate")));
            theVect . addElement((String)theMap.get("ContractorName"));
            theVect . addElement((String)theMap.get("NoOfBundles"));
            theVect . addElement((String)theMap.get("NoOfTapes"));
            theVect . addElement(common.getRound(dNoOfTapes*dRatePerTape,2));
            theModel. appendRow(theVect); 
        }
    }
    private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            dispose();
        }
    }
    private void setDetails()
    {
        theDataList = new ArrayList();
        StringBuffer sb = new StringBuffer();
        sb.append(" select ReceivedDate,GreenTapeDailyAuthentication.ContractorCode,");
        sb.append(" GreenTapeContractor.ContractorName,(GreenTapeDailyAuthentication.NoOfTapes/50) as NoOfBundles,");
        sb.append(" GreenTapeDailyAuthentication.NoOfTapes from GreenTapeDailyAuthentication");
        sb.append(" Inner join GreenTapeContractor on GreenTapeContractor.ContractorCode = GreenTapeDailyAuthentication.ContractorCode");
//        sb.append(" where GreenTapeDailyAuthentication.FROMDATE="+iFromDate+" and GreenTapeDailyAuthentication.ToDate="+iToDate);
        sb.append(" where GreenTapeDailyAuthentication.ReceivedDate>="+iFromDate+" and GreenTapeDailyAuthentication.ReceivedDate<="+iToDate);
        sb.append(" and GreenTapeDailyAuthentication.AuthStatus=1 and GreenTapeDailyAuthentication.ContractorCode="+iContractorCode+" order by 2,1");

        System.out.println(" Green Tape Details "+sb.toString());
        try
        {
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection    theConnection = connect.getConnection();
                       
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet         theResult    = theStatement.executeQuery();
            while(theResult.next())
            {
                HashMap theMap = new HashMap();
                theMap . put("ReceivedDate",theResult.getString(1));
                theMap . put("ContractorCode",theResult.getString(2));
                theMap . put("ContractorName",theResult.getString(3));
                theMap . put("NoOfBundles",theResult.getString(4));
                theMap . put("NoOfTapes",theResult.getString(5));
                theDataList . add(theMap);
            }
        }
        catch(Exception e)
        {
            System.out.println(" set Details "+e);
        }
    }
    public static void main(String[] args) 
    {
      
    }
    private class GreenTapeDetailsModel extends DefaultTableModel
   {
    String ColumnName[] ={"S.NO","DATE","CONTRACTOR","NO OF BUNDLES","NO OF TAPES","TOTAL AMOUNT"};
    String  ColumnType[]={"N","S","S","N","N","N"};
    int iColumnWidth[]  ={30,30,30,50,50,30};
    public GreenTapeDetailsModel()
    {
       setDataVector(getRowData(),ColumnName);
    }     
     public Class getColumnClass(int iCol)
     {
       return getValueAt(0,iCol).getClass();
     }
    public boolean isCellEditable(int iRow,int iCol)
    {
        if (ColumnType[iCol]=="E"||ColumnType[iCol]=="B")
            return true;
            return false;
    }
    
    private Object[][]getRowData()
    {
        Object RowData[][]=new Object[1][ColumnName.length];
        for(int i=0;i<ColumnName.length;i++)
            RowData[0][i]="";
        return RowData;
    }
    
    public void appendRow(Vector theVect)
    {
        insertRow(getRows(),theVect);
    }
    public int getRows()
    {
        return super.dataVector.size();
    }
 }
}
