
package GreenTapeAuthentication;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GreenTapeInwardDetailsFrame extends JDialog
{
    JPanel    pnlTop,pnlMiddle,pnlBottom;
    JTable    theTable=null;
    GreenTapeDetailsModel theModel=null;
    JButton   btnOk;
    int       iFromDate,iToDate,iContractorCode;
    ArrayList theDataList = null;
    Common    common = null;
    public GreenTapeInwardDetailsFrame(int iContractorCode,int iFromDate,int iToDate)
    {
        this . iContractorCode = iContractorCode;
        this . iFromDate       = iFromDate;
        this . iToDate         = iToDate;
        common = new Common();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
        setTableData();
    }
    private  void createComponents()
    {
       pnlTop    = new JPanel();
       pnlMiddle = new JPanel();
       pnlBottom = new JPanel();
       
       btnOk     = new JButton("  OK  ");
       
       theModel  = new GreenTapeDetailsModel();
       theTable  = new JTable(theModel);
    }
    private void setLayouts()
    {
        pnlTop    . setLayout(new FlowLayout(FlowLayout.CENTER));
        pnlMiddle . setLayout(new BorderLayout());
        pnlBottom . setLayout(new FlowLayout(FlowLayout.CENTER));
     
        pnlTop    . setBorder(new TitledBorder("Title"));
        pnlMiddle . setBorder(new TitledBorder("Data"));
        pnlBottom . setBorder(new TitledBorder("Controls"));
        
        this . setTitle(" Green Tape Inward Details");
        this . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this . setSize(500,500);
        this . setModal(true);
        
    }
    private void addComponents()
    {
        pnlTop . add(new JLabel(" Green Tape Inward Details"));
        pnlMiddle . add(new JScrollPane(theTable));
        pnlBottom . add(btnOk);
        
        this . add(pnlTop,BorderLayout.NORTH);
        this . add(pnlMiddle,BorderLayout.CENTER);
        this . add(pnlBottom,BorderLayout.SOUTH);
    }
    private void addListeners()
    {
        btnOk.addActionListener(new ActList());
    }
    private void setTableData()
    {
        theModel.setNumRows(0);
        setInwardDetails();
        for(int i=0;i<theDataList.size();i++)
        {
            HashMap theMap   = (HashMap)theDataList.get(i);
          
            Vector theVect = new Vector();
            theVect . addElement(String.valueOf(i+1));
            theVect . addElement(common.parseDate((String)theMap.get("GrnDate")));
            theVect . addElement((String)theMap.get("GrnNo"));
            theVect . addElement((String)theMap.get("ItemName"));
            theVect . addElement((String)theMap.get("SupplierName"));
            theVect . addElement((String)theMap.get("GrnQty"));
            theVect . addElement((String)theMap.get("Uom"));
            theModel. appendRow(theVect); 
        }
    }
    private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            dispose();
        }
    }
    private void setInwardDetails()
    {
        theDataList = new ArrayList();
        StringBuffer sb = new StringBuffer();
        
        sb.append(" select Grn.GrnNo,Grn.GrnDate,InvItems.Item_Name,Supplier.Name,Grn.GrnQty,Uom.UomName from Grn");
        sb.append(" Inner join Supplier on Supplier.AC_CODE = Grn.Sup_code");
        sb.append(" Inner join InvItems on InvItems.Item_Code =Grn.Code");
        sb.append(" Inner join Uom on Uom.UomCode = InvItems.UomCode");
        sb.append(" where Grn.GrnDate>="+iFromDate+" and Grn.GrnDate<="+iToDate);
        sb.append(" and InvItems.Item_code='A08000325'");
        
       
        System.out.println(" GRN Details "+sb.toString());
        try
        {
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection    theConnection = connect.getConnection();
                       
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet         theResult    = theStatement.executeQuery();
            while(theResult.next())
            {
                HashMap theMap = new HashMap();
                theMap . put("GrnNo",theResult.getString(1));
                theMap . put("GrnDate",theResult.getString(2));
                theMap . put("ItemName",theResult.getString(3));
                theMap . put("SupplierName",theResult.getString(4));
                theMap . put("GrnQty",theResult.getString(5));
                theMap . put("Uom",theResult.getString(6));
                theDataList . add(theMap);
            }
        }
        catch(Exception e)
        {
            System.out.println(" set Details "+e);
        }
    }
    public static void main(String[] args) 
    {
      
    }
    private class GreenTapeDetailsModel extends DefaultTableModel
   {
    String ColumnName[] ={"SNo","Date","GrnNo","ItemName","SupplierName","GrnQty","Uom"};
    String  ColumnType[]={"N","S","S","S","S","N","S"};
    int iColumnWidth[]  ={30,30,30,50,50,30,30};
    public GreenTapeDetailsModel()
    {
       setDataVector(getRowData(),ColumnName);
    }     
     public Class getColumnClass(int iCol)
     {
       return getValueAt(0,iCol).getClass();
     }
    public boolean isCellEditable(int iRow,int iCol)
    {
        if (ColumnType[iCol]=="E"||ColumnType[iCol]=="B")
            return true;
            return false;
    }
    
    private Object[][]getRowData()
    {
        Object RowData[][]=new Object[1][ColumnName.length];
        for(int i=0;i<ColumnName.length;i++)
            RowData[0][i]="";
        return RowData;
    }
    
    public void appendRow(Vector theVect)
    {
        insertRow(getRows(),theVect);
    }
    public int getRows()
    {
        return super.dataVector.size();
    }
 }
}
