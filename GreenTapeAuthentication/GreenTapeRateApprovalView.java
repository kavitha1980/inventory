package GreenTapeAuthentication;
import javax.swing.*;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.net.*;
import oracle.jdbc.*;
import oracle.sql.*;
import java.awt.*;
import java.awt.event.*;
import guiutil.*;
import javax.swing.border.TitledBorder;
import java.awt.event.ActionListener;

import util.*;
import jdbc.*;
import guiutil.*;

public class GreenTapeRateApprovalView extends JFrame
{
 Connection                     theConnection       = null;   
 
 JPanel                         pnlMain,pnlFrame,pnlControl;
 JButton                        btnExit;
 PaintPanel                     p =null;
 Common             common      = new Common();
 
 public GreenTapeRateApprovalView()
 {
    createComponent();
    setLayout();
    addListener();
    addComponent();
    setImage(); 
 }
 private void createComponent()
 {
    pnlMain                         = new JPanel();
    pnlControl                      = new JPanel();     
    pnlFrame                        = new JPanel(true);     
    btnExit                         = new JButton("Exit");
}

private void setLayout()
{
    
    pnlMain                         . setLayout(new BorderLayout());
    pnlFrame                        . setBorder(new TitledBorder("Rate Approval Attachment"));
    pnlFrame                        . setLayout(new GridLayout(1,2));
    pnlControl                      . setBorder(new TitledBorder("Control"));
    pnlControl                      . setLayout(new FlowLayout());
    
    this.setSize(800,700);
   // this.setVisible(true);
    this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    this.setTitle("GREEN TAPE PAYMENT RATE APPROVAL ATTACHMENT");
  //  this.setClosable(true);
   // this.setMaximizable(true);
}

private void addListener()
{
     btnExit                        . addActionListener(new ExtList());
}


private void addComponent()
{
    
    pnlFrame                        . add(new JLabel(""));
    pnlControl                      . add(btnExit);
    
    pnlMain                         . add(pnlFrame,BorderLayout.CENTER);
    pnlMain                         . add(pnlControl,BorderLayout.SOUTH);
    
    this.add(pnlMain);
}


private class ExtList implements ActionListener
{
    public void actionPerformed(ActionEvent ae)
    {
        if(ae.getSource()== btnExit)
        {
            dispose();
        }
    }
}

  private void setImage()
     {
       
         String QS1 = " Select OfficeNoteCopy from YarnBagPayOfficeNote"+
                      " Where FromDate<=to_Char(sysdate,'YYYYMMDD') and ToDate>= to_Char(sysdate,'YYYYMMDD') " ;

         System.out.println("View Qry-->"+QS1);
          try
          {
			ORAConnection3 connect    = ORAConnection3.getORAConnection();
			theConnection = connect.getConnection();

               Statement theStatement = theConnection.createStatement();
               ResultSet theResult = theStatement.executeQuery(QS1);
               byte[] imgbytes=null;
               
               if(theResult.next())
               {
                
                    imgbytes  =  getOracleBlob(theResult,"OFFICENOTECOPY");
                
               }
               Image   image=null;
          
               try
               {
                    if(imgbytes!=null)
                    {
                         image = Toolkit.getDefaultToolkit().createImage(imgbytes);
                    }
                    else
                    {
                         image = Toolkit.getDefaultToolkit().createImage("");
                    }
               }
               catch(Exception ex1)
               {
                    System.out.println(ex1);
                    ex1.printStackTrace();
               }
               setPhotoPanel(image);
              
               theStatement.close();

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

private byte[] getOracleBlob(ResultSet result, String columnName) throws SQLException
 {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        byte[] bytes = null;
        
        try {    
            oracle.sql.BLOB blob = ((oracle.jdbc.OracleResultSet)result).getBLOB(columnName);
            if(blob!=null)
            {
                 inputStream = blob.getBinaryStream();        
                 int bytesRead = 0;        
                 
                 while((bytesRead = inputStream.read()) != -1) {        
                     outputStream.write(bytesRead);    
                 }
                 
                 bytes = outputStream.toByteArray();
            
             }
        } catch(IOException e) {
            throw new SQLException(e.getMessage());
        } finally 
        {
        }
        return bytes;
     }

      
 class PaintPanel extends JPanel
     {
          private Image image;
          public PaintPanel(Image image)
          {
               this.image = image;
               setSize(800,700);
               setLayout(new BorderLayout());
          }
          
  public void paintComponent(Graphics g)
          {
               super.paintComponent(g);
               //g.drawImage(image, 0, 0, this);
               g.drawImage(image, 0, 0, this.getWidth(),this.getHeight(),null);
          }
     }
private void setPhotoPanel(Image image)
 {
          try
          {
              
               pnlFrame.updateUI();
               if(p!=null)
               {
                    pnlFrame.remove(p);
               }
               if(image!=null)
               {
                   
                    p =  new PaintPanel(image);
                    pnlFrame.add(p);
               }
          }
       catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
   }

 
}
