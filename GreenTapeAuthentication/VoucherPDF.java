package GreenTapeAuthentication;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.JOptionPane;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javax.swing.JTable;

import util.*;
 public class VoucherPDF{
    String SFile="",SAdvanceMonth;
    Common common = new Common();
    String SAmount="0";
     int            Lctr      = 100;
     int            Pctr      = 0;
     String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
     int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font tinyBold   = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
     private static Font tinyNormal = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

     private static Font underBold  = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
     
  
     String SHead1[] = {"","","","","",""};
     int    iWidth1[]= {30,20,20,20,20,30};

     Document document;
     PdfPTable table;
     Connection theConnection = null;
     String SContractorName,SVocNo,SServerDate,SPurpose,STransactionId;
     int iRow;
     double dAmount;
     Vector VData;
     double dRatePerTape;
     

 public VoucherPDF (Document document,PdfPTable table,String SContractorName,String SVocNo,String SServerDate,int iRow,double dAmount,String SPurpose,Vector VData,double dRatePerTape,String STransactionId)
 {
    this . SContractorName   = SContractorName;
    this . SVocNo            = SVocNo;
    this . dAmount           = dAmount;
    this . SServerDate       = SServerDate;
    this . iRow              = iRow;
    this . SPurpose          = SPurpose;
    this . VData             = VData;
    this . dRatePerTape      = dRatePerTape;
    this . document          = document;
    this . table             = table;
    this . STransactionId    = STransactionId;

    createPDFFile();
      
 }
     
  public void createPDFFile()
   {
      try
      {
        addBody(document,table);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }
  
   public void addBody(Document document,PdfPTable table) throws BadElementException
   {
       String SVocHead = " GREENTAPE PAYMENT";
       try
       {

       table = new PdfPTable(6);       
       table.flushContent();
       table.setWidths(iWidth1);
       table.setWidthPercentage(100);
       table.setSpacingAfter(50);


          String    sPreviousFromDate="", sPreviousToDate="",sWorkType="";
          double    dTotalBags=0.00, dTotalAmount=0.00,dCount=0.00, dAmountperBag;
          double    dPrevTotalBags=0.00, dPrevTotalAmount=0.00,dPrevCount=0.00,dPrevAmount=0.00;
                   
          
          Paragraph paragraph;
          PdfPCell c1;
          int iNo=0, iTotalNo=0;
				
	  SAmount		= String.valueOf(dAmount);
				
             c1 = new PdfPCell(new Phrase(String.valueOf("AMARJOTHI SPINNING MILLS LIMITED"),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_CENTER);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(6);
             table.addCell(c1); 

             c1 = new PdfPCell(new Phrase(String.valueOf("PUDUSURIPALAYAM - NAMBIYUR- 638 458"),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_CENTER);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(6);
             table.addCell(c1); 


             c1 = new PdfPCell(new Phrase(String.valueOf("TRANSACTION ID : "),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_LEFT);
             c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
             c1.setColspan(1);
             table.addCell(c1); 

             c1 = new PdfPCell(new Phrase(STransactionId,bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_LEFT);
             c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(1);
             table.addCell(c1); 


             c1 = new PdfPCell(new Phrase(String.valueOf("CASH PAYMENT VOUCHER "),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_CENTER);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(2);
             table.addCell(c1);


             c1 = new PdfPCell(new Phrase(String.valueOf("DATE :"),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_LEFT);
             c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
             c1.setColspan(1);
             table.addCell(c1);

             String sDate =common.parseDate(SServerDate);
             c1 = new PdfPCell(new Phrase(String.valueOf(sDate),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_LEFT);
             c1.setBorder(Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(1);
             table.addCell(c1);


             c1 = new PdfPCell(new Phrase(String.valueOf("ACCOUNT HEAD :"),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_LEFT);
             c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
             c1.setColspan(2);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(SVocHead),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_LEFT);
             c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(4);
             table.addCell(c1);


             c1 = new PdfPCell(new Phrase(String.valueOf("NAME OF THE PERSON"),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_LEFT);
             c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
             c1.setColspan(2);
             c1.setFixedHeight(35f);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(SContractorName),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_LEFT);
             c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(4);
             c1.setFixedHeight(35f);
             table.addCell(c1);


             c1 = new PdfPCell(new Phrase(String.valueOf("PURPOSE"),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_LEFT);
             c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
             c1.setColspan(2);
             c1.setFixedHeight(35f);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(SPurpose),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_LEFT);
             c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(4);
             c1.setFixedHeight(35f);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf("AMOUNT IN WORDS :"),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_LEFT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(2);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(common.getRupee(SAmount),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_LEFT);
             c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(4);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf("COST CENTER NO."),mediumNormal));
             c1.setHorizontalAlignment(Element.ALIGN_CENTER);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(3);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf("CODE NO."),mediumNormal));
             c1.setHorizontalAlignment(Element.ALIGN_CENTER);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(2);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf("AMOUNT RS"),mediumNormal));
             c1.setHorizontalAlignment(Element.ALIGN_CENTER);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(3);
             table.addCell(c1);


             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(2);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(dAmount),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(3);
             table.addCell(c1);


             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setColspan(1);
             table.addCell(c1);
          for(int i=0;i<23;i++)
          {
             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
             c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
             c1.setColspan(1);
             table.addCell(c1);

        }

             c1 = new PdfPCell(new Phrase(String.valueOf("DEPT HEAD"),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_CENTER);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
             c1.setColspan(1);
             table.addCell(c1);


             c1 = new PdfPCell(new Phrase(String.valueOf("CM/JMD"),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_CENTER);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf("J.O(AC)"),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_CENTER);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
             c1.setColspan(1);
             table.addCell(c1);


             c1 = new PdfPCell(new Phrase(String.valueOf("IA"),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_CENTER);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf("CASHIER"),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_CENTER);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
             c1.setColspan(1);
             table.addCell(c1);

             c1 = new PdfPCell(new Phrase(String.valueOf("RECEIVEDBY"),bigNormal));
             c1.setHorizontalAlignment(Element.ALIGN_CENTER);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
             c1.setColspan(1);
             table.addCell(c1);

             document.add(table);
         }
       catch(Exception ex)
       {
           
       }
   }



    public void addBody1(Document document,PdfPTable table,String SVocNo,String sVochName,String  sACType,double dAmount,String SPurpose,String SServerDate,String sTransactionID) throws BadElementException
   {
	
       try
       {

       table = new PdfPTable(6);       
       table.flushContent();
       table.setWidths(iWidth1);
       table.setWidthPercentage(100);
       table.setSpacingAfter(50);


          String    sPreviousFromDate="", sPreviousToDate="",sWorkType="";
          double    dTotalBags=0.00, dTotalAmount=0.00,dCount=0.00, dAmountperBag;
          double    dPrevTotalBags=0.00, dPrevTotalAmount=0.00,dPrevCount=0.00,dPrevAmount=0.00;
                   
          
          Paragraph paragraph;
          PdfPCell c1;
          int iNo=0, iTotalNo=0;
				
				SAmount		= String.valueOf(dAmount);
				
                         c1 = new PdfPCell(new Phrase(String.valueOf("AMARJOTHI SPINNING MILLS LIMITED"),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(6);
                         table.addCell(c1); 

                         c1 = new PdfPCell(new Phrase(String.valueOf("PUDUSURIPALAYAM - NAMBIYUR- 638 458"),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(6);
                         table.addCell(c1); 


                         c1 = new PdfPCell(new Phrase(String.valueOf("TRANSACTION ID : "),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
				 c1.setColspan(1);
                         table.addCell(c1); 

                         c1 = new PdfPCell(new Phrase(String.valueOf(sTransactionID),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(1);
                         table.addCell(c1); 


                         c1 = new PdfPCell(new Phrase(String.valueOf("CASH PAYMENT VOUCHER "),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(2);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf("DATE :"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
				 c1.setColspan(1);
                         table.addCell(c1);
	  			 
				 String sDate =common.parseDate(SServerDate);
                         c1 = new PdfPCell(new Phrase(String.valueOf(sDate),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(1);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf("ACCOUND HEAD :"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
				 c1.setColspan(2);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(sACType),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(4);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf("NAME OF THE PERSON"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
				 c1.setColspan(2);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(sVochName),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(4);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf("PURPOSE"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
				 c1.setColspan(2);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(SPurpose),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(4);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("AMOUNT IN WORDS :"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(2);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.getRupee(SAmount),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(4);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("COST CENTER NO."),mediumNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(3);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("CODE NO."),mediumNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(2);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("AMOUNT RS"),mediumNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(3);
                         table.addCell(c1);
				

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(2);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(dAmount),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(3);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(1);
                         table.addCell(c1);
            for(int i=0;i<10;i++){

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
				 c1.setColspan(1);
                         table.addCell(c1);

		}



                         c1 = new PdfPCell(new Phrase(String.valueOf("DEPT HEAD"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				 c1.setColspan(1);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf("CM/JMD"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("J.O(AC)"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				 c1.setColspan(1);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf("IA"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("CASHIER"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("RECEIVEDBY"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				 c1.setColspan(1);
                         table.addCell(c1);

                         document.add(table);
         }
       catch(Exception ex)
       {
           
       }
   }


    public void addBody2(Document document,PdfPTable table,String SVocNo,String sVochName,String  sACType,double dAmount,String SPurpose,String SServerDate,String sTransactionID) throws BadElementException
   {
	
       try
       {

       table = new PdfPTable(6);       
       table.flushContent();
       table.setWidths(iWidth1);
       table.setWidthPercentage(100);
       table.setSpacingAfter(50);


          String    sPreviousFromDate="", sPreviousToDate="",sWorkType="";
          double    dTotalBags=0.00, dTotalAmount=0.00,dCount=0.00, dAmountperBag;
          double    dPrevTotalBags=0.00, dPrevTotalAmount=0.00,dPrevCount=0.00,dPrevAmount=0.00;
                   
          
          Paragraph paragraph;
          PdfPCell c1;
          int iNo=0, iTotalNo=0;
				
				SAmount		= String.valueOf(dAmount);
				
                         c1 = new PdfPCell(new Phrase(String.valueOf("AMARJOTHI SPINNING MILLS LIMITED"),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(6);
                         table.addCell(c1); 

                         c1 = new PdfPCell(new Phrase(String.valueOf("PUDUSURIPALAYAM - NAMBIYUR- 638 458"),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(6);
                         table.addCell(c1); 


                         c1 = new PdfPCell(new Phrase(String.valueOf("TRANSACTION ID : "),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
				 c1.setColspan(1);
                         table.addCell(c1); 

                         c1 = new PdfPCell(new Phrase(String.valueOf(sTransactionID),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(1);
                         table.addCell(c1); 


                         c1 = new PdfPCell(new Phrase(String.valueOf("CASH PAYMENT VOUCHER "),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
				 c1.setColspan(2);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf("DATE :"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
			 c1.setColspan(1);
                         table.addCell(c1);
	  			 
				 String sDate =common.parseDate(SServerDate);
                         c1 = new PdfPCell(new Phrase(String.valueOf(sDate),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(1);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf("ACCOUND HEAD :"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
			 c1.setColspan(2);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(sACType),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(4);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf("NAME OF THE PERSON"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
			 c1.setColspan(2);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(sVochName),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(4);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf("PURPOSE"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
			 c1.setColspan(2);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(SPurpose),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(4);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("AMOUNT IN WORDS :"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(2);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.getRupee(SAmount),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder( Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(4);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("COST CENTER NO."),mediumNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(3);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("CODE NO."),mediumNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(2);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("AMOUNT RS"),mediumNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(3);
                         table.addCell(c1);
				

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(2);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(dAmount),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(3);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
			 c1.setColspan(1);
                         table.addCell(c1);
            for(int i=0;i<10;i++)
            {

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf(""),bigBold));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
				 c1.setColspan(1);
                         table.addCell(c1);

		}



                         c1 = new PdfPCell(new Phrase(String.valueOf("DEPT HEAD"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				 c1.setColspan(1);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf("CM/JMD"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("J.O(AC)"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				 c1.setColspan(1);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase(String.valueOf("IA"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("CASHIER"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				 c1.setColspan(1);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(String.valueOf("RECEIVEDBY"),bigNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				 c1.setColspan(1);
                         table.addCell(c1);

                         document.add(table);
         }
       catch(Exception ex)
       {
           
       }
   }




  
}


