package GreenTapeAuthentication;
import jxl.CellReferenceHelper;
import jxl.CellView;
import jxl.HeaderFooter;
import jxl.Range;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.SheetSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.Orientation;
import jxl.format.PageOrientation;
import jxl.format.PaperSize;
import jxl.format.ScriptStyle;
import jxl.format.UnderlineStyle;
import jxl.format.CellFormat;
import jxl.write.Blank;
import jxl.write.Boolean;
import jxl.write.DateFormat;
import jxl.write.DateFormats;
import jxl.write.DateTime;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormat;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFeatures;
import jxl.write.WritableCellFormat;
import jxl.write.WritableCell;
import jxl.write.WritableFont;
import jxl.write.WritableHyperlink;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import java.util.*;
import util.*;
import jdbc.*;
import java.sql.*;
import jxl.format.VerticalAlignment;

import util.*;
import jdbc.*;
import guiutil.*;

public class VoucherPrintingExcel
{
     private   WritableSheet       sheet;
     private   Label               label;
     private   double              dRatePerTape        =0.0;
     private   String              SVocNo,SEmpName,STicketNo,SAmount,SServerDate,SPurpose;
     private   String              SContractorName;
     private   Common              common;
     private   double              dAmount;
     private   Vector              VData;
     public    int                 iRow;
  
     Connection theConnection = null;
     //public VoucherPrintingExcel(WritableSheet sheet,String SVocNo,String SEmpName,String STicketNo,String SAmount,String SServerDate,int iRow,String SPurpose)
     public VoucherPrintingExcel(WritableSheet sheet,String SContractorName,String SVocNo,String SServerDate,int iRow,double dAmount,String SPurpose,Vector VData,double dRatePerTape)
     {
          this . sheet             = sheet;
          this . SContractorName   = SContractorName;
          this . SVocNo            = SVocNo;
          this . dAmount           = dAmount;
          this . SServerDate       = SServerDate;
          this . iRow              = iRow;
          this . SPurpose          = SPurpose;
          this . VData             = VData;
          this . dRatePerTape      = dRatePerTape;
          common                   = new Common();
          printData();
     }

     private void printData()
     {
          String SVocHead = " GREENTAPE PAYMENT";
          try
          {
   
               WritableFont   wfHeadBold               = new WritableFont(WritableFont.TIMES,12,WritableFont.BOLD);
               WritableFont   wfNonBold                = new WritableFont(WritableFont.TIMES,10);
               WritableFont   wfBold                  = new WritableFont(WritableFont.TIMES,10,WritableFont.BOLD);
               
               WritableCellFormat LeftRightTopCenter   = new WritableCellFormat(wfHeadBold);
               LeftRightTopCenter                      . setBorder(Border.LEFT, BorderLineStyle.THIN);
               LeftRightTopCenter                      . setBorder(Border.RIGHT, BorderLineStyle.THIN);
               LeftRightTopCenter                      . setBorder(Border.TOP, BorderLineStyle.THIN);
               LeftRightTopCenter                      . setAlignment(Alignment.CENTRE);

               WritableCellFormat LeftRightOnlyCenter  = new WritableCellFormat(wfHeadBold);
               LeftRightOnlyCenter                     . setBorder(Border.LEFT,BorderLineStyle.THIN);
               LeftRightOnlyCenter                     . setBorder(Border.RIGHT,BorderLineStyle.THIN);
               LeftRightOnlyCenter                     . setAlignment(Alignment.CENTRE);

               WritableCellFormat LeftRightOnly        = new WritableCellFormat(wfNonBold);
               LeftRightOnly                           . setBorder(Border.LEFT,BorderLineStyle.THIN);
               LeftRightOnly                           . setBorder(Border.RIGHT,BorderLineStyle.THIN);

               WritableCellFormat LeftRightCenter      = new WritableCellFormat(wfNonBold);
               LeftRightCenter                         . setBorder(Border.LEFT,BorderLineStyle.THIN);
               LeftRightCenter                         . setBorder(Border.RIGHT,BorderLineStyle.THIN);
               LeftRightCenter                         . setAlignment(Alignment.CENTRE);

               WritableCellFormat TopBottomOnly        = new WritableCellFormat(wfNonBold);
               TopBottomOnly                           . setBorder(Border.TOP, BorderLineStyle.THIN);
               TopBottomOnly                           . setBorder(Border.BOTTOM, BorderLineStyle.THIN);
               TopBottomOnly                           . setAlignment(Alignment.CENTRE);

               WritableCellFormat LeftRightBottom      = new WritableCellFormat(wfNonBold);
               LeftRightBottom                         . setBorder(Border.LEFT, BorderLineStyle.THIN);
               LeftRightBottom                         . setBorder(Border.RIGHT, BorderLineStyle.THIN);
               LeftRightBottom                         . setBorder(Border.BOTTOM, BorderLineStyle.THIN);

               WritableCellFormat LeftRightBottomCenter= new WritableCellFormat(wfNonBold);
               LeftRightBottomCenter                   . setBorder(Border.LEFT, BorderLineStyle.THIN);
               LeftRightBottomCenter                   . setBorder(Border.RIGHT, BorderLineStyle.THIN);
               LeftRightBottomCenter                   . setBorder(Border.BOTTOM, BorderLineStyle.THIN);
               LeftRightBottomCenter                   . setAlignment(Alignment.CENTRE);

               WritableCellFormat LeftRightBottomBold  = new WritableCellFormat(wfBold);
               LeftRightBottomBold                     . setBorder(Border.LEFT, BorderLineStyle.THIN);
               LeftRightBottomBold                     . setBorder(Border.RIGHT, BorderLineStyle.THIN);
               LeftRightBottomBold                     . setBorder(Border.BOTTOM, BorderLineStyle.THIN);
               LeftRightBottomBold                     . setAlignment(Alignment.CENTRE);

               label     = new Label(0,iRow,"AMARJOTHI SPINNING MILLS LIMITED",LeftRightTopCenter);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,11,iRow);
               iRow     += 1;

               label     = new Label(0,iRow,"PUDUSURIPALAYAM - NAMBIYUR - 638 458",LeftRightOnlyCenter);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,11,iRow);
               iRow     += 1;

               label     = new Label(0,iRow,"",LeftRightOnly);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,11,iRow);
               iRow     += 1;

               label     = new Label(0,iRow," VOUCHER NO. : "+SVocNo,LeftRightOnly);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,2,iRow);

               label     = new Label(3,iRow,"CASH PAYMENT VOUCHER",TopBottomOnly);
               sheet     . addCell(label);
               sheet     . mergeCells(3,iRow,7,iRow);

               label     = new Label(8,iRow,"                DATE : "+SServerDate,LeftRightOnly);
               sheet     . addCell(label);
               sheet     . mergeCells(8,iRow,11,iRow);
               iRow     += 1;

               label     = new Label(0,iRow,"",LeftRightOnly);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,11,iRow);
               iRow     += 1;
               
               label     = new Label(0,iRow,"ACCOUNT HEAD : "+SVocHead,LeftRightBottom);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,11,iRow);
               iRow     += 1;
               
               label     = new Label(0,iRow,"NAME OF THE PERSON : "+SContractorName,LeftRightBottom);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,11,iRow+1);
               iRow     += 2;
               
               label     = new Label(0,iRow,"PURPOSE : ",LeftRightOnly);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,11,iRow);
               iRow     += 1;
               
               label     = new Label(0,iRow,"",LeftRightOnly);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,11,iRow);
               iRow     += 1;
               
               label     = new Label(0,iRow,SPurpose,LeftRightOnly);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,11,iRow);
               iRow     += 1;

               label     = new Label(0,iRow,"",LeftRightBottom);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,11,iRow);
               iRow     += 1;

               label     = new Label(0,iRow,"",LeftRightBottom);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,11,iRow);
               iRow     += 1;

               label     = new Label(0,iRow,"AMOUNT IN WORDS : "+common.getRupee(String.valueOf(dAmount)),LeftRightBottom);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,11,iRow);
               iRow     += 1;

               label     = new Label(0,iRow,"COST CENTER NO.",LeftRightCenter);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,4,iRow);

               label     = new Label(5,iRow,"CODE NO.",LeftRightCenter);
               sheet     . addCell(label);
               sheet     . mergeCells(5,iRow,7,iRow);

               label     = new Label(8,iRow,"AMOUNT Rs."+String.valueOf(dAmount),LeftRightCenter);
               sheet     . addCell(label);
               sheet     . mergeCells(8,iRow,11,iRow);
               iRow     += 1;

               label     = new Label(0,iRow,"",LeftRightBottomCenter);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,4,iRow);

               label     = new Label(5,iRow,"",LeftRightBottomCenter);
               sheet     . addCell(label);
               sheet     . mergeCells(5,iRow,7,iRow);
               label     = new Label(8,iRow,SAmount,LeftRightBottomBold);
               sheet     . addCell(label);
               sheet     . mergeCells(8,iRow,11,iRow);
               iRow     += 1;

               for(int i=0;i<5;i++)
               {
                    label     = new Label(0,iRow,"",LeftRightOnly);
                    sheet     . addCell(label);
                    sheet     . mergeCells(0,iRow,1,iRow);

                    label     = new Label(2,iRow,"",LeftRightOnly);
                    sheet     . addCell(label);
                    sheet     . mergeCells(2,iRow,3,iRow);


                    label     = new Label(4,iRow,"",LeftRightOnly);
                    sheet     . addCell(label);
                    sheet     . mergeCells(4,iRow,5,iRow);


                    label     = new Label(6,iRow,"",LeftRightOnly);
                    sheet     . addCell(label);
                    sheet     . mergeCells(6,iRow,6,iRow);

                    label     = new Label(7,iRow,"",LeftRightOnly);
                    sheet     . addCell(label);
                    sheet     . mergeCells(7,iRow,8,iRow);

                    label     = new Label(9,iRow,"",LeftRightOnly);
                    sheet     . addCell(label);
                    sheet     . mergeCells(9,iRow,11,iRow);
                    iRow     += 1;
               }

               label     = new Label(0,iRow,"DEPT. HEAD",LeftRightBottomCenter);
               sheet     . addCell(label);
               sheet     . mergeCells(0,iRow,1,iRow);

               label     = new Label(2,iRow,"CM/JMD",LeftRightBottomCenter);
               sheet     . addCell(label);
               sheet     . mergeCells(2,iRow,3,iRow);


               label     = new Label(4,iRow,"J.O.(A/c)",LeftRightBottomCenter);
               sheet     . addCell(label);
               sheet     . mergeCells(4,iRow,5,iRow);


               label     = new Label(6,iRow,"I.A",LeftRightBottomCenter);
               sheet     . addCell(label);
               sheet     . mergeCells(6,iRow,6,iRow);

               label     = new Label(7,iRow,"CASHIER",LeftRightBottomCenter);
               sheet     . addCell(label);
               sheet     . mergeCells(7,iRow,8,iRow);

               label     = new Label(9,iRow,"RECEIVED BY",LeftRightBottomCenter);
               sheet     . addCell(label);
               sheet     . mergeCells(9,iRow,11,iRow);


               iRow     += 2;

               sheet     . addRowPageBreak(iRow);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public int getRowNo()
     {
          return iRow;
     }

}
