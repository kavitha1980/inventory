
package GreenTapePayment;
import javax.swing.table.*;
import java.util.*;

import util.*;
import guiutil.*;
import jdbc.*;
public class GreenTapeDailyAuthModel extends DefaultTableModel
{
    String ColumnName[] ={"S.NO","DATE","CONTRACTOR","NO OF BUNDLES","NO OF TAPES","TOTAL AMOUNT","SELECT"};
    String  ColumnType[]={"N","S","S","N","N","N","E"};
    int iColumnWidth[]  ={30,30,30,50,50,30,30};
    public GreenTapeDailyAuthModel()
    {
       setDataVector(getRowData(),ColumnName);
    }     
     public Class getColumnClass(int iCol)
     {
       return getValueAt(0,iCol).getClass();
     }
    public boolean isCellEditable(int iRow,int iCol)
    {
        if (ColumnType[iCol]=="E"||ColumnType[iCol]=="B")
            return true;
            return false;
    }
    
    private Object[][]getRowData()
    {
        Object RowData[][]=new Object[1][ColumnName.length];
        
        for(int i=0;i<ColumnName.length;i++)
            RowData[0][i]="";
        return RowData;
    }
    
    public void appendRow(Vector theVect)
    {
        insertRow(getRows(),theVect);
    }
    public int getRows()
    {
        return super.dataVector.size();
    }

}
