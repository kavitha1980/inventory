package GreenTapePayment;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.net.InetAddress;
import java.io.*;


import util.*;
import guiutil.*;
import jdbc.*;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.SheetSettings;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.format.PageOrientation;
import jxl.format.PaperSize;

//public class GreenTapeDailyAuthentication extends JFrame
public class GreenTapeDailyAuthentication extends JInternalFrame
{
    JPanel       pnlTop,pnlMiddle,pnlBottom;
    JTable       theTable=null;
    GreenTapeDailyAuthModel theModel=null;
    JButton      btnApply,btnSave,btnExit,btnPrint;
    JComboBox    cmbContractor;
    JLayeredPane Layer;
    AttDateField txtFromDate,txtToDate;
    Vector       VData,VContractor,VContractorCode,VContractorEmpCode,VAuthData;
    int          iFromDate,iToDate,iContractorCode,iContractorEmpCode;
    double       dRatePerTape=0.0;
    Common       common;
    String       SSystemName,SContractor;
    int          iRow,iUserCode,iAuthCode,iMillCode;
    JLabel       lblTotalAmount,lblTotalTapes,lblTotalBundles;
    WritableSheet    sheet;
    WritableWorkbook workbook;

    //set Model Index
    int sno=0,receiveddate=1,contractor=2,bundles=3,tapes=4,totalamount=5,select=6; 

    public GreenTapeDailyAuthentication(JLayeredPane Layer,int iUserCode,int iAuthCode,int iMillCode)
    {
        this . Layer     = Layer;
        this . iUserCode = iUserCode;
        this . iAuthCode = iAuthCode;
        this . iMillCode = iMillCode;

        System.out.println(" UserCode,AuthCode,MillCode "+iUserCode+","+iAuthCode+","+iMillCode);
        common = new Common();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();

        try
        {
            SSystemName = InetAddress.getLocalHost().getHostName();
        }
        catch(Exception ex)
        {
           System.out.println(ex);
        }
    }
    private void createComponents()
    {
        pnlTop      = new JPanel();
        pnlMiddle   = new JPanel();
        pnlBottom   = new JPanel();
        
        txtFromDate = new AttDateField(10);
        txtToDate   = new AttDateField(10);
        
        btnApply   = new JButton(" Apply ");
        btnSave    = new JButton(" Authenticate ");
        btnExit    = new JButton(" Exit ");
        btnPrint   = new JButton(" Print ");
        
        lblTotalAmount  = new JLabel("");
        lblTotalBundles = new JLabel("");
        lblTotalTapes   = new JLabel("");
        
        setContractor();
        cmbContractor  = new JComboBox(VContractor);
        theModel   = new GreenTapeDailyAuthModel();
        theTable   = new JTable(theModel);
    }
    private void setLayouts()
    {
        pnlTop    . setLayout(new GridLayout(2,4));
        pnlMiddle . setLayout(new BorderLayout());
        pnlBottom . setLayout(new FlowLayout(FlowLayout.CENTER));

        pnlTop       . setBorder(new TitledBorder("Title"));
        pnlMiddle    . setBorder(new TitledBorder("Data"));
        pnlBottom    . setBorder(new TitledBorder("Controls"));
        
        this . setSize(650,600);
        this . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this . setTitle(" Green Tape Authentication Frame");
        this . setMaximizable(true);
        this . setClosable(true);
        this . setResizable(true);
    }
    private void addComponents()
    {
        pnlTop . add(new JLabel(" From Date"));
        pnlTop . add(txtFromDate);
        pnlTop . add(new JLabel(" To Date"));
        pnlTop . add(txtToDate);
        pnlTop . add(new JLabel(" Contractor "));
        pnlTop . add(cmbContractor);
        pnlTop . add(new JLabel(" "));
        pnlTop . add(btnApply);
        
        pnlMiddle . add(new JScrollPane(theTable));
        
        pnlBottom . add(btnSave);
       // pnlBottom . add(btnPrint);
        pnlBottom . add(btnExit);
        pnlBottom . add(new JLabel(" Total Amount "));
        pnlBottom . add(lblTotalAmount);
        
        this . add(pnlTop,BorderLayout.NORTH);
        this . add(pnlMiddle,BorderLayout.CENTER);
        this . add(pnlBottom,BorderLayout.SOUTH);
    }
    private void addListeners()
    {
        btnApply . addActionListener(new ActList());
        btnSave  . addActionListener(new ActList());
        btnExit  . addActionListener(new ActList());
        btnPrint . addActionListener(new ActList());
    }
    private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==btnApply)
            {
                iFromDate = common.toInt(txtFromDate.toNormal());
                iToDate   = common.toInt(txtToDate  .toNormal());
                iContractorCode    = common.toInt((String)VContractorCode.get(cmbContractor.getSelectedIndex()));
                iContractorEmpCode = common.toInt((String)VContractorEmpCode.get(cmbContractor.getSelectedIndex()));
                SContractor     = (String)cmbContractor.getSelectedItem();
                setRate();
                setTableData();
            }
            if(ae.getSource()==btnSave)
            {
               int iSaveVal = authenticateData(); 
               
               if(iSaveVal==1)
                 JOptionPane .showMessageDialog(null,"Data Saved Successfully","Info",JOptionPane.INFORMATION_MESSAGE);
               else if(iSaveVal==0)
                 JOptionPane .showMessageDialog(null,"Problem in Save Data","Error",JOptionPane.ERROR_MESSAGE);  
               else if(iSaveVal==3)               
                 JOptionPane.showMessageDialog(null," Please check the Previous Authorization", "Info",JOptionPane.WARNING_MESSAGE);

                setTableData();
            }  
            if(ae.getSource()==btnExit)
            {
                dispose();
            }
            
        }
    }
    private void setTableData()
    {
        theModel . setNumRows(0);
        
        double dOpening=0.0,dCarriedOver=0.0,dRoundedNet=0.0;
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        theTable.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
        theTable.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
        theTable.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
        setDataVector();
        for(int i=0;i<VData.size();i++)
        {
            HashMap theMap = (HashMap)VData.get(i);
            Vector theVect = new Vector();
            theVect . addElement(String.valueOf(i+1));
            theVect . addElement(common.parseDate((String)theMap.get("ReceivedDate")));
            theVect . addElement((String)theMap.get("ContractorName"));
            theVect . addElement((String)theMap.get("NoOfBundles"));
            theVect . addElement((String)theMap.get("NoOfTapes"));
            theVect . addElement((String)theMap.get("TotalAmount"));
            theVect . addElement(new Boolean(false));
            theModel . appendRow(theVect);
        }
          
            lblTotalBundles. setText(String.valueOf(getNoOfBundles()));
            lblTotalTapes  . setText(String.valueOf(getNoOfTapes()));
            lblTotalAmount . setText(String.valueOf(getTotalAmount()));
    }
    
    private void setContractor()
    {
        VContractor     = new Vector();
        VContractorCode = new Vector();
        VContractorEmpCode = new Vector();
        StringBuffer sb = new StringBuffer();
        sb.append(" Select ContractorCode,ContractorName,ContractorEmpCode from GreenTapeContractor order by 1");
        try
        {
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection    theConnection = connect.getConnection();
            
            Statement theStatement = theConnection.createStatement();
            ResultSet theResult    = theStatement.executeQuery(sb.toString());
            while(theResult.next())
            {
               VContractorCode . addElement(theResult.getString(1));
               VContractor     . addElement(theResult.getString(2));
               VContractorEmpCode . addElement(theResult.getString(3));
            }
        }
        catch(Exception ex)
        {
            System.out.println(" setDataVector "+ex);
        }
    }
    private void setDataVector()
    {
        VData = new Vector();
        StringBuffer sb = new StringBuffer();

        sb.append(" Select ReceivedDate,GreenTapeDetails.ContractorCode,GreenTapeContractor.ContractorName,sum(NoOfBundles),sum(NoOfTapes),GreenTapeDetails.ContractorEmpCode from GreenTapeDetails");
        sb.append(" Inner join GreenTapeContractor on GreenTapeContractor.ContractorCode = GreenTapeDetails.ContractorCode and GreenTapeContractor.ContractorEmpCode=GreenTapeDetails.ContractorEmpCode");
        sb.append(" where GreenTapeDetails.ReceivedDate>="+iFromDate);
        sb.append(" and GreenTapeDetails.ReceivedDate<="+iToDate);
        if(!((String)cmbContractor.getSelectedItem()).equalsIgnoreCase("All"))
           sb.append(" and GreenTapeDetails.ContractorCode="+iContractorCode+" and GreenTapeDetails.ContractorEmpCode="+iContractorEmpCode);

        sb.append(" group by ReceivedDate,GreenTapeDetails.ContractorCode,GreenTapeContractor.ContractorName,GreenTapeDetails.ContractorEmpCode ");        
        sb.append(" order by 2,1");

        System.out.println(" setData Vector "+sb.toString());
        try
        {
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection    theConnection = connect.getConnection();

            Statement theStatement = theConnection.createStatement();
            ResultSet theResult    = theStatement.executeQuery(sb.toString());
            while(theResult.next())
            {
                HashMap theMap = new HashMap();
                theMap . put("ReceivedDate",theResult.getString(1));
                theMap . put("ContractorCode",theResult.getString(2));
                theMap . put("ContractorName",theResult.getString(3));
                theMap . put("NoOfBundles",theResult.getString(4));
                theMap . put("NoOfTapes",theResult.getString(5));
                theMap . put("TotalAmount",common.getRound(theResult.getInt(5)*dRatePerTape,2));
                theMap . put("ContractorEmpCode",theResult.getString(6));
                VData  . addElement(theMap);
            }
        }
        catch(Exception ex)
        {
            System.out.println(" setDataVector "+ex);
        }
    }
    
    private void setRate()
    {
        StringBuffer sb = new StringBuffer();
        sb.append(" Select RatePerTape from GreenTapeRate ");
        sb.append(" where "+iFromDate+">=WithEffectFrom and "+iToDate+"<=WithEffectTo");

        try
        {
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection    theConnection = connect.getConnection();
            
            Statement theStatement = theConnection.createStatement();
            ResultSet theResult    = theStatement.executeQuery(sb.toString());
            while(theResult.next())
            {
               dRatePerTape = theResult.getDouble(1);
            }
            theResult.close();
            theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println(" setDataVector "+ex);
        }
    }
    private int authenticateData()
    {
        int iAuthenticate =-1,iAlreadyExist=0;
        for(int i=0;i<theModel.getRowCount();i++)
        {
          if(((Boolean)theModel.getValueAt(i,select)).booleanValue())
          {
           String SDate   = common.pureDate((String)theModel.getValueAt(i,receiveddate));
           if(!isAlreadyExists(SDate))   
           {
           int iNoOfTapes = common.toInt((String)theModel.getValueAt(i,tapes));

           StringBuffer sb = new StringBuffer();
           sb.append(" insert into GreenTapeDailyAuthentication(ReceivedDate,NoOfTapes,AuthDate,AuthDateTime,AuthStatus,AuthUserCode,AuthSystemName,ContractorCode,FromDate,ToDate,ContractorEmpCode)");
           sb.append(" values(?,?,to_Char(sysdate,'yyyymmdd'),to_Char(sysdate,'dd-mm-yyyy hh24:mi:ss'),?,?,?,?,?,?,?)");
           try
           {
                ORAConnection connect = ORAConnection.getORAConnection();
                Connection theConnection = connect.getConnection();
               
                PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());

                theStatement . setString(1,SDate);
                theStatement . setInt(2,iNoOfTapes);
                theStatement . setInt(3,1);
                theStatement . setInt(4,iUserCode);
                //theStatement . setInt(4,iUserCode);
                theStatement . setString(5,SSystemName);
                theStatement . setInt(6,iContractorCode);
                theStatement . setInt(7,iFromDate);
                theStatement . setInt(8,iToDate);
                theStatement . setInt(9,iContractorEmpCode);
                theStatement . executeUpdate();
                iAuthenticate=1;
                
                if(iUserCode==1980)
                {
                 StringBuffer sb1 = new StringBuffer();
                 sb1.append(" Update GreenTapeDetails set AuthStatus=1 ");
                 sb1.append(" where ReceivedDate="+SDate+" and  ContractorCode="+iContractorCode+" and ContractorEmpCode="+iContractorEmpCode);
                
                 theStatement = theConnection.prepareStatement(sb1.toString());
                 theStatement . executeUpdate();
                }
                theStatement.close();
            }
            catch(Exception ex)
            {
                iAuthenticate=0;
                System.out.println(" authenticate Data "+ex);
            }
           }
           else
           {
               iAlreadyExist = iAlreadyExist+1;

           }
          } 
        } 
        if(iAlreadyExist==theModel.getRowCount())
          JOptionPane.showMessageDialog(null,"Data Already Authenticated ","Info",JOptionPane.INFORMATION_MESSAGE);           
        return iAuthenticate;
    }
   
  private int getNoOfBundles()
  {
      int iBundles=0;  
      for(int i=0;i<VData.size();i++)
      {
          HashMap theMap = (HashMap)VData.get(i);
          iBundles  = iBundles+common.toInt((String)theMap.get("NoOfBundles"));
          
      }
      return iBundles;
    }
    private int getNoOfTapes()
    {
      int iTapes=0;  
      for(int i=0;i<VData.size();i++)
      {
          HashMap theMap = (HashMap)VData.get(i);
          iTapes  = iTapes+common.toInt((String)theMap.get("NoOfTapes"));
          
      }
      return iTapes;
    }
    private double getTotalAmount()
    {
      double dAmount=0;  
      for(int i=0;i<VData.size();i++)
      {
          HashMap theMap = (HashMap)VData.get(i);
          dAmount  = dAmount+common.toDouble((String)theMap.get("TotalAmount"));
      }
      return dAmount;
    }
    private void initExcelFile()
    {
          try
          {
               workbook  = Workbook.createWorkbook(new File("d://GreenTapePayment.xls"));
               sheet     = workbook.createSheet("GreenTape",0);

               sheet     . getSettings().setPaperSize(PaperSize.A4);
               sheet     . getSettings().setOrientation(PageOrientation.PORTRAIT);
               sheet     . getSettings().setTopMargin(0.5);
               sheet     . getSettings().setBottomMargin(0.5);
               sheet     . getSettings().setLeftMargin(0.5);
               sheet     . getSettings().setRightMargin(0.5);
               sheet     . getSettings().setProtected(false);
               sheet     . getSettings().setPassword("Print");
               sheet     . getSettings().setFitToPages(true);
               sheet     . getSettings().setDefaultColumnWidth(7);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void closeExcelFile()
     {
          try
          {
               workbook.write();
               workbook.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
    private boolean isAlreadyExists(String SReceivedDate)
    {
        int iCount=0;
        StringBuffer sb = new StringBuffer();
        sb.append(" select Count(1) from GreenTapeDailyAuthentication where  ReceivedDate=? and ContractorCode=? and  ContractorEmpCode=?");

        try
        {
           ORAConnection connect = ORAConnection.getORAConnection();
           Connection    theConnection = connect.getConnection();
           
           PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
           theStatement . setString(1,SReceivedDate);
           theStatement . setInt(2,iContractorCode);
//           theStatement .setInt(3, iUserCode);
           theStatement .setInt(3, iContractorEmpCode);
           ResultSet         theResult    = theStatement.executeQuery();
           while(theResult.next())
           {
               iCount = theResult.getInt(1);
           }
           if(iCount>0)
               return true;
           else 
               return false;
        }
        catch(Exception ex)
        {
            System.out.println(" isAlreadyExists  "+ex);
            return true;
        }
 }
}
