package Indent;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.sql.*;
import java.util.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

import Indent.UserIndent.*;
import Indent.IndentFiles.*;
import Indent.IndentFiles.Transfer.*;
import Indent.IndentFiles.Modify.*;
import Indent.IndentFiles.INDENTMODIFY.*;
import Indent.IndentFiles.PartyTransfer.*;
import Indent.IndentFiles.GreenTapePayment.*;
import Indent.IndentFiles.NonStockReceipt.*;


public class Indent
{
     // Possible Look & Feels
     String mac      = "com.sun.java.swing.plaf.mac.MacLookAndFeel";
     String metal    = "javax.swing.plaf.metal.MetalLookAndFeel";
     String motif    = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
     String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
     
     // The current Look & Feel
     String currentLookAndFeel = metal;
     
     JMenuBar       theMenuBar;
     JFrame         theFrame;
     Container      Container;
     JDesktopPane   DeskTop;
     StatusPanel    SPanel;
     JPanel         MenuPanel;
     
     JMenu          IndentMenu;
     JMenu          TransferMenu;
     JMenu          AssignMenu;
     JMenu          HelpMenu;
     
     JMenuItem      mIndent,mIssueList,mIssueDetail,mIssueAuth,mIssueDelete,mIssueDeleteNew,mUserIndent,mIndentList,mIndentIssue, mIndentIssueNew,mIndentAuth,mIssueReturn,mrdcIssue,mrdcReceiversList,mservicestock,mservicestocklist,mmaterialinstallation, mServiceStockIssueDeletionFrame, mNonStockIssueDeletionFrame;
     JMenuItem      mTransfer,mConcTranList,mConcTranInvoice,mConcInvoicePrint,mPartyTransfer,mJobWorkIssue,mGreenTapeReceive,mGreenTapeIssue;
     JMenuItem      mGreenTapeReceipts,mStockReceipt,mStockReceiptList/*,mMaterialReceipt*/,mnonvaluelist,mtestmreceipt,mscrabreceiptframe,mScrabPrint,mBarcodeprint,/*mDirectScrab,mTesting,mTesting2,*/mScrabReport,mReceiptReport/*,mScrabReceipt*/,mGRNVerification,mGrnVerifiedList,mtestingpunch,mstockbarcode;
	 JMenuItem		mNewScrabReport;
	 JMenuItem		mNewScrabIssueFrame,mNewScrabIssueReport,mNewDirectScrabIssueFrame;
	 JMenuItem 		mIssuePunching,mservicestocknew;
	 
	 JMenuItem		mSeconSalesReceipt,mSeconSalesReport,mSeconSalesToYard,mSeconSalesToYardReport;
	 
	 JMenuItem      mScrapPreReceipt,mScrabIssueReport;
	 
	 
     
     Vector         VCode,VName,VUomName,VNameCode,VStkGpCode;
     Vector         VIndentTypeCode,VIndentTypeName;
     JTable         ReportTable;
     boolean        bflag;
     util.Common         common;
     Control        control = new Control();
     
     int    iUserCode,iAuthCode,iMillCode,iOwnerCode;
     String SFinYear,SStDate,SEnDate,SYearCode,SItemTable,SSupTable,SMillName,SAuthUserCode,SUnit,SDept;

     IndentFrame              indentFrame              =    null;
     IssueListFrame           issuelistframe           =    null;
     IssueListDetailsFrame    issuelistdetailsframe    =    null;
     IssueAuthenticationFrame issueauthframe           =    null;
     IssueDeletionFrame       issuedeletionframe       =    null;
     IndentIssueFrame         indentissueframe         =    null;
     IndentIssueFrameN        indentissueframeN        =    null;
     IssueReturnFrame         issueReturnFrame         =    null;
     
     UserIndentFrame          userIndentFrame          =    null;
     IndentAuthFrame          indentAuthFrame          =    null;

     RDCIssueFrame            rdcissueframe            =    null;
     RDCReceiversList         rdcReceiversFrame         =    null;
     /*IndentListFrame          indentListFrame          =    null;

     UserIndentFrame          userIndentFrame          =    null;*/

	MaterialInstallationAuthenticationNew    materialinstallationauthenticationnew  = null;
     StockTransferFrame       stocktransferframe       =    null;
	ServiceStockTransfer      servicestocktransfer      = null;
	ServiceStockTransferList		servicestocktransferlist = null;
     ConcernTransferListFrame concernTransferListFrame = null;
     ConcernTransferInvoiceFrame concernTransferInvoiceFrame = null;
     ConcernTransferInvoicePrintFrame concernTransferInvoicePrintFrame = null;

     PartyStockTransferFrame       partystocktransferframe       =    null;
     JobWorkIssueFrame             jobworkissueframe             =    null;
     GreenTapeReceiveFrame         greentapereceiveframe         =    null;
     GreenTapeIssueFrame           greentapeissueframe           =    null;  
     GreenTapeReceiptDetails       greentapereceipts             =    null;
     ItemStockViewFrame       	itemstockviewframe             =    null;
     StockReceiptListFrame       	stockreceiptlistframe             =    null;
	MaterialReceiptDetailsFrame    materialreceiptdetailsframe    = null;
	NonValueListFrame			nonvaluelistframe    = null;
	ScrabReceiptReport             scrabreceiptreport               = null;
	MaterialReceiptReportDetails   materialreceiptreportdetails   = null;
	ScrabDetailsReport              scrabdetailsreport           = null;
	TestMaterialReceipt             testmaterialreceipt            = null;
	ScrabReceiptFrame 			scrabreceiptframe          = null;	
	NewBarcodePrint                newbarcodeprint               =null;
	TestingFrame      testingframe = null;
	ClosingStockBarcodePrint    closingstockbarcodeprint = null;
	
     GRNVerificationFrame          grnverificationframe          =    null;
     GRNVerifiedListFrame          grnverifiedlistframe          =    null; 
	 
	ServiceStockIssueDeletionFrame servicestockissuedeletionframe; 
	NonStockIssueDeletionFrame nonstockissuedeletionframe;
	
	IssuePunchAuthentication issuePunchAuthentication;
	
	SeconSalesTestMaterialReceipt seconSalesTestMaterialReceipt;
	SecondSalesReceiptReport  secondSalesReceiptReport;
	SecondSalesToYardFrame    secondSalesToYardFrame;
	
	SecondSalesToYardDetailsReport secondSalesToYardDetailsReport; 
	
	ServiceStockTransferNew serviceStockTransferNew;
	 
     ArrayList theTemplateList,theMaterialIssueTemplateList;
     public Indent(int iUserCode,int iAuthCode,int iMillCode,String SFinYear,String SStDate,String SEnDate,String SYearCode,String SItemTable,String SSupTable,String SMillName)
     {
          this.iUserCode  = iUserCode;
          this.iAuthCode  = iAuthCode;
          this.iMillCode  = iMillCode;
          this.SFinYear   = SFinYear;
          this.SStDate    = SStDate;
          this.SEnDate    = SEnDate;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;
          
          updateLookAndFeel();
          CreateComponents();
          setLayouts();
          AddComponents();
          AddListeners();
          theFrame.setVisible(true);
          getIndentCode();
          setDataIntoVector();
     }
     public Indent(int iUserCode,int iAuthCode,int iMillCode,String SFinYear,String SStDate,String SEnDate,String SYearCode,String SItemTable,String SSupTable,String SMillName,ArrayList theTemplateList,ArrayList theMaterialIssueTemplateList)
     {
          this.iUserCode  = iUserCode;
          this.iAuthCode  = iAuthCode;
          this.iMillCode  = iMillCode;
          this.SFinYear   = SFinYear;
          this.SStDate    = SStDate;
          this.SEnDate    = SEnDate;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;

          this.theTemplateList              = theTemplateList;
          this.theMaterialIssueTemplateList = theMaterialIssueTemplateList;


          updateLookAndFeel();
          CreateComponents();
          setLayouts();
          AddComponents();
          AddListeners();
          theFrame.setVisible(true);
          getIndentCode();
          setDataIntoVector();
     }
               
     public void CreateComponents()
     {
          theFrame       = new JFrame("Issue Monitoring System - "+SMillName+" - Year - "+SFinYear);
          Container      = theFrame.getContentPane();
          DeskTop        = new JDesktopPane();
          SPanel         = new StatusPanel();
          MenuPanel      = new JPanel();
          theMenuBar     = new JMenuBar();

          IndentMenu     = new JMenu("Indent");
          TransferMenu   = new JMenu("Transfer");
          AssignMenu     = new JMenu("Assignments");
          HelpMenu       = new JMenu("Help");

          mTransfer      = new JMenuItem("Stock Transfer");

          if(iMillCode!=1)
          {
               mConcTranList     = new JMenuItem("Concern Transfer List");
               mConcTranInvoice  = new JMenuItem("Concern Transfer Invoice");
               mConcInvoicePrint = new JMenuItem("Concern Transfer Invoice Print");
          }

          mPartyTransfer = new JMenuItem("Return to Party");
          mJobWorkIssue  = new JMenuItem("Issue for JobWork");

          VCode          = new Vector();
          VName          = new Vector();
          VUomName       = new Vector();
          VNameCode      = new Vector();
          VStkGpCode     = new Vector();
          common         = new util.Common();

          if(iMillCode==0)
          {
               Container . setBackground(new Color(250,200,200));
               theFrame  . setBackground(new Color(250,200,200));
               DeskTop   . setBackground(new Color(250,200,200));
               theMenuBar. setBackground(new Color(250,200,200));
               MenuPanel . setBackground(new Color(250,200,200));
          }
          if(iMillCode==1)
          {
               Container . setBackground(new Color(220,250,250));
               theFrame  . setBackground(new Color(220,250,250));
               DeskTop   . setBackground(new Color(220,250,250));
               theMenuBar. setBackground(new Color(220,250,250));
               MenuPanel . setBackground(new Color(220,250,250));
          }
          if(iMillCode==3)
          {
               Container . setBackground(new Color(200,250,200));
               theFrame  . setBackground(new Color(200,250,200));
               DeskTop   . setBackground(new Color(200,250,200));
               theMenuBar. setBackground(new Color(200,250,200));
               MenuPanel . setBackground(new Color(200,250,200));
          }
          if(iMillCode==4)
          {
               Container . setBackground(new Color(200,200,250));
               theFrame  . setBackground(new Color(200,200,250));
               DeskTop   . setBackground(new Color(200,200,250));
               theMenuBar. setBackground(new Color(200,200,250));
               MenuPanel . setBackground(new Color(200,200,250));
          }

     }

     public void setLayouts()
     {
          Dimension screenSize     = Toolkit.getDefaultToolkit().getScreenSize();
                    theFrame       . setSize(screenSize);
                    MenuPanel      . setLayout(new BorderLayout());          
     }

     public void AddComponents()
     {
          Container      .add("North",MenuPanel);
          Container      .add("Center",DeskTop);
          Container      .add("South",SPanel);

          IndentMenu     .setMnemonic('I');
          TransferMenu   .setMnemonic('T');
          AssignMenu     .setMnemonic('A');
          HelpMenu       .setMnemonic('H');

          theMenuBar     .add(IndentMenu);
          theMenuBar     .add(TransferMenu);
          theMenuBar     .add(AssignMenu);
          theMenuBar     .add(HelpMenu);

          if(iAuthCode>0)
          {
               //IndentMenu      .add(mIndent       = new JMenuItem("Direct Issue"));
          }
          IndentMenu      .add(mIssueList    = new JMenuItem("Issue List"));
          IndentMenu      .add(mIssueDetail  = new JMenuItem("List of Issues - Detailed"));


          mIssueAuth   = new JMenuItem("Issue Authentication");
          mIssueDelete = new JMenuItem("Issue Deletion");
		  mIssueDeleteNew = new JMenuItem("Issue Deletion (New Under Process)");
		mServiceStockIssueDeletionFrame = new JMenuItem("Service Stock Issue Deletion");
		mNonStockIssueDeletionFrame = new JMenuItem("Non Stock Issue Deletion");

//          IndentMenu      .add(mIndentList    = new JMenuItem("Indent List"));

          if(iAuthCode>0)
          {
               IndentMenu.add(mIssueAuth);
               if(iAuthCode==3)
               {
                    IndentMenu.add(new JSeparator());
                    IndentMenu.add(mIssueDelete);
                    IndentMenu.add(mServiceStockIssueDeletionFrame);
                    IndentMenu.add(mNonStockIssueDeletionFrame);
                    IndentMenu.add(new JSeparator());
               }
               TransferMenu.add(mTransfer);

               if(iMillCode!=1)
               {
                    TransferMenu.add(mConcTranList);
                    TransferMenu.add(mConcTranInvoice);
                    TransferMenu.add(mConcInvoicePrint);
               }

               TransferMenu.addSeparator();
               //TransferMenu.add(mPartyTransfer);
               TransferMenu.add(mJobWorkIssue);
          }


//          IndentMenu      .add(mUserIndent   = new JMenuItem("User Indent"));
//          IndentMenu      .add(mIndentAuth   = new JMenuItem("Indent Authentication"));


          mIndentIssue    = new JMenuItem("Issue Against Indent - Old ");
          //IndentMenu      . add(mIndentIssue);
          IndentMenu      . add(mIndentIssueNew= new JMenuItem("Issue Against Indent"));
		  
		  IndentMenu      . add(mIssuePunching= new JMenuItem("Mobile Issued Material Receiver Punching"));
		  

          if(iUserCode==16)
          {
               IndentMenu      .add(mIssueReturn= new JMenuItem("Issue Return"));
          }

          IndentMenu      .add(mrdcIssue   = new JMenuItem("RDC Issue"));
          IndentMenu      .add(mrdcReceiversList = new JMenuItem("RDC Receivers List "));
          IndentMenu      .add(mGRNVerification   = new JMenuItem("GRN Verification"));          
          IndentMenu      .add(mGrnVerifiedList   = new JMenuItem("Grn Verified List"));
          IndentMenu      .add(mGreenTapeReceive   = new JMenuItem(" Green Tape Receive "));

         // IndentMenu      .add(mGreenTapeIssue   = new JMenuItem("Green Tape Issue"));
          IndentMenu      .add(mGreenTapeReceipts= new JMenuItem("Green Tape Receipts List"));
		IndentMenu                            . add(new JSeparator());

          IndentMenu      .add(mStockReceipt= new JMenuItem("Non Stock Receipt"));
          IndentMenu      .add(mStockReceiptList= new JMenuItem("Non Stock Receipt List"));
          IndentMenu      . add(new JSeparator());
		  //IndentMenu      .add(mservicestock= new JMenuItem("Service Stock Receipt Frame"));
		  IndentMenu      .add(mservicestocknew= new JMenuItem("Service Stock Receipt Frame"));
		mservicestocknew.setToolTipText("This frame will take Data from ServiceStockTransfer Table. This frame is used to put receipt for items in stores");
		IndentMenu      .add(mservicestocklist= new JMenuItem("Service Stock list Frame"));
		mservicestocklist.setToolTipText("This frame will take Data from ServiceStockTransfer Table. This frame is used to view the items receipts in the  stores");
	//	IndentMenu      .add(mMaterialReceipt= new JMenuItem("Scrab Receipt frame"));
	//	mMaterialReceipt.setToolTipText("This frame will take Data from MaterialIndentDetails Table");
		IndentMenu      . add(new JSeparator());
		IndentMenu      .add(mnonvaluelist= new JMenuItem("Value entry frame for Non Value Items"));
		mnonvaluelist.setToolTipText("This frame is used to entry the value for non value items");
		IndentMenu      .add(mtestmreceipt= new JMenuItem("Scrap Receipt frame"));
		mtestmreceipt.setToolTipText("This frame will take Data from MaterialIndentDetails Table. This frame is used to put receipt for items in stores");
		IndentMenu	 .add(mScrabReport = new JMenuItem("Scrap Receipt Report"));
		mScrabReport.setToolTipText("This frame will take Data From MaterialIndentDetails Table ");
          //IndentMenu      .add(mReceiptReport = new JMenuItem("Scrab Issue To Scrab Yard "));
		//mReceiptReport.setToolTipText("This frame will take Data from MaterialReturnDetails Table.  This Screen is used to transfer materials from STORES to SCRAP YARD");
          //IndentMenu      .add(mScrabPrint = new JMenuItem("Scrab Issue To Scrab Yard Report"));
		//mScrabPrint.setToolTipText("This frame will take Data From MaterialScrabDetails Table ");
		 
		 
		 
		 //IndentMenu      .add(mNewScrabIssueFrame = new JMenuItem("Scrab Issue To Scrab Yard (Testing)"));
		 //IndentMenu      .add(mNewScrabIssueReport= new JMenuItem("Scrab Issue To Scrab Yard Report(Testing)"));
		 
		  IndentMenu      .add(mNewScrabIssueFrame = new JMenuItem("Scrap Issue To Scrap Yard "));
		 IndentMenu      .add(mNewScrabIssueReport= new JMenuItem("Scrap Issue To Scrap Yard Report"));
	/*	IndentMenu      .add(mScrabReceipt = new JMenuItem("Material Received To Scrap"));
		mScrabReceipt.setToolTipText(" THIS SCREEN IS FOR TO PUT RECEIPT IN SCRAP YARD  "); */
		IndentMenu      . add(new JSeparator());
		
		IndentMenu      .add(mSeconSalesReceipt = new JMenuItem("Second Sales Receipt"));
		IndentMenu      .add(mSeconSalesReport  = new JMenuItem("Second Sales Receipt Report"));
		IndentMenu      .add(mSeconSalesToYard  = new JMenuItem("Second Sales Issue To Scrap Yard"));
		IndentMenu      .add(mSeconSalesToYardReport  = new JMenuItem("Second Sales Issue To Scrap Yard Report "));
		
		
		
		IndentMenu      .add(new JSeparator());
		
		
		
		IndentMenu      .add(mBarcodeprint = new JMenuItem("Barcode Print"));
		mBarcodeprint.setToolTipText("This frame is used to take print of barcode for items ");
		IndentMenu      .add(mscrabreceiptframe= new JMenuItem("Testing Frame (Dont use this frame)"));
		mscrabreceiptframe.setToolTipText("This frame will take Data from MaterialIndentDetails Table. This frame is used to put receipt for items in stores");
		IndentMenu      .add(mmaterialinstallation= new JMenuItem(" Material Issued Without Return Material Frame"));
		mmaterialinstallation.setToolTipText("This frame will take Data from Materialindentreturndetails and materialreturndetails Table. This frame is used to put receipt for items in stores");
	/*	IndentMenu      .add(mtestingpunch= new JMenuItem(" Under Working"));
		mtestingpunch.setTolTipText("This frame will take Data from Materialindentreturndetails and materialreturndetails Table. This frame is used to put receipt for items in stores");*/
		IndentMenu      .add(mstockbarcode= new JMenuItem(" Closing Stock Barcode frame"));
		mstockbarcode.setToolTipText("This frame is used to take a barcode print for closing stocks");
		
		IndentMenu      . add(new JSeparator());
		
		
			IndentMenu      .add(mScrapPreReceipt = new JMenuItem("Scrap Receipt - Stores Item Not Issued"));
			IndentMenu      .add(mNewScrabReport = new JMenuItem("Scrap Receipt Report - Stores Item Not Issued" ));
		//	IndentMenu      .add(mNewDirectScrabIssueFrame= new JMenuItem("Scrap Issue - Stores Item Not Issued"));
		  //  IndentMenu      .add(mScrabIssueReport = new JMenuItem("Scrap Issue Report -Stores Item Not Issued "));
			
		
		//IndentMenu      .add(mservicestocknew= new JMenuItem(" Service Stock New Testing frame"));
		
		

          MenuPanel.add("West",theMenuBar);
     }

     public void AddListeners()
     {
          theFrame            . addWindowListener(new WinList());
		  
          if(iAuthCode>0)
          {
               //mIndent             . addActionListener(new ActList());
          }
          mIssueList          . addActionListener(new ActList());
          mIssueDetail        . addActionListener(new ActList());
          mIssueAuth          . addActionListener(new ActList());
          mIssueDelete        . addActionListener(new ActList());
		mServiceStockIssueDeletionFrame. addActionListener(new ActList());
		mNonStockIssueDeletionFrame . addActionListener(new ActList());
          mrdcIssue           . addActionListener(new ActList());
          mrdcReceiversList   . addActionListener(new ActList());
         // mGreenTapeIssue     . addActionListener(new ActList());
          mGreenTapeReceive   . addActionListener(new ActList());
          mGreenTapeReceipts  . addActionListener(new ActList());
          mStockReceipt  . addActionListener(new ActList());
          mStockReceiptList  . addActionListener(new ActList());
     //     mMaterialReceipt  . addActionListener(new ActList());
		mnonvaluelist  . addActionListener(new ActList());
		mtestmreceipt  . addActionListener(new ActList());
		
			mScrapPreReceipt. addActionListener(new ActList());
			//mNewDirectScrabIssueFrame. addActionListener(new ActList());
			//mScrabIssueReport. addActionListener(new ActList());
		
		
		mSeconSalesReceipt		. addActionListener(new ActList());
		mSeconSalesReport 		. addActionListener(new ActList());  
		mSeconSalesToYard		. addActionListener(new ActList());  
		mSeconSalesToYardReport	. addActionListener(new ActList());  
		
		
		mNewScrabIssueFrame. addActionListener(new ActList());
		
		mNewScrabIssueReport. addActionListener(new ActList());
		mscrabreceiptframe  . addActionListener(new ActList());
		mmaterialinstallation  . addActionListener(new ActList());
	//	mtestingpunch  . addActionListener(new ActList());
		mstockbarcode  . addActionListener(new ActList());
		
		//mservicestock  . addActionListener(new ActList());
		
		mservicestocknew  . addActionListener(new ActList());
		
		mservicestocklist  . addActionListener(new ActList());
          mScrabReport  . addActionListener(new ActList());
		  mNewScrabReport. addActionListener(new ActList());
        //  mReceiptReport  . addActionListener(new ActList());
          //mScrabPrint  . addActionListener(new ActList());
    //      mScrabReceipt  . addActionListener(new ActList());
          mBarcodeprint  . addActionListener(new ActList());
		
		
          mGRNVerification    . addActionListener(new ActList());
          mGrnVerifiedList    .  addActionListener(new ActList());
//         mUserIndent         . addActionListener(new ActList());
//         mIndentAuth         . addActionListener(new ActList());
          //mIndentList         . addActionListener(new ActList());

          mIndentIssue        . addActionListener(new ActList());
          mIndentIssueNew     . addActionListener(new ActList());
		  mIssuePunching. addActionListener(new ActList());

          if(iUserCode==16)
          {
               mIssueReturn        . addActionListener(new ActList());
          } 

          mTransfer           . addActionListener(new ActList());

          if(iMillCode!=1)
          {
               mConcTranList       . addActionListener(new ActList());
               mConcTranInvoice    . addActionListener(new ActList());
               mConcInvoicePrint   . addActionListener(new ActList());
          }

          //mPartyTransfer      . addActionListener(new ActList());
          mJobWorkIssue      . addActionListener(new ActList());

     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               /*if(ae.getSource()==mIndent)
               {
                    try
                    {
                         DeskTop.remove(indentFrame);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         indentFrame = new IndentFrame(DeskTop,VCode,VName,VNameCode,VUomName,VStkGpCode,VIndentTypeCode,VIndentTypeName,SPanel,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(indentFrame);
                         DeskTop.repaint();
                         indentFrame.setSelected(true);
                         DeskTop.updateUI();
                         indentFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }*/

               if(ae.getSource()==mIssueList)
               {
                    try
                    {
                         DeskTop.remove(issuelistframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         issuelistframe = new IssueListFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,iAuthCode,SItemTable,SSupTable);
                         DeskTop.add(issuelistframe);
                         DeskTop.repaint();
                         issuelistframe.setSelected(true);
                         DeskTop.updateUI();
                         issuelistframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mIssueDetail)
               {
                    try
                    {
                         DeskTop.remove(issuelistdetailsframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         issuelistdetailsframe = new IssueListDetailsFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,SMillName);
                         DeskTop.add(issuelistdetailsframe);
                         DeskTop.repaint();
                         issuelistdetailsframe.setSelected(true);
                         DeskTop.updateUI();
                         issuelistdetailsframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mIssueAuth)
               {
                    try
                    {
                         DeskTop.remove(issueauthframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         issueauthframe = new IssueAuthenticationFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode);
                         DeskTop.add(issueauthframe);
                         DeskTop.repaint();
                         issueauthframe.setSelected(true);
                         DeskTop.updateUI();
                         issueauthframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               if(ae.getSource()==mIndentIssue)
               {
                    try
                    {
                         DeskTop.remove(indentissueframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         indentissueframe = new IndentIssueFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,SItemTable,SSupTable,SYearCode,theTemplateList,theMaterialIssueTemplateList);
                         DeskTop.add(indentissueframe);
                         DeskTop.repaint();
                         indentissueframe.setSelected(true);
                         DeskTop.updateUI();
                         indentissueframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mIndentIssueNew)
               {
                    try
                    {
                         DeskTop.remove(indentissueframeN);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         indentissueframeN = new IndentIssueFrameN(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,SItemTable,SSupTable,SYearCode,theTemplateList,theMaterialIssueTemplateList);
                         DeskTop.add(indentissueframeN);
                         DeskTop.repaint();
                         indentissueframeN.setSelected(true);
                         DeskTop.updateUI();
                         indentissueframeN.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			   
               if(ae.getSource()==mIssuePunching)
               {
                    try
                    {
                         DeskTop.remove(issuePunchAuthentication);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         issuePunchAuthentication = new IssuePunchAuthentication(DeskTop,iMillCode,SItemTable,SSupTable,SYearCode,theTemplateList,theMaterialIssueTemplateList);
                         DeskTop.add(issuePunchAuthentication);
                         DeskTop.repaint();
                         issuePunchAuthentication.setSelected(true);
                         DeskTop.updateUI();
                         issuePunchAuthentication.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

			   
			   

               if(ae.getSource()==mIssueReturn)
               {
                    try
                    {
                         DeskTop.remove(issueReturnFrame);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         issueReturnFrame = new IssueReturnFrame(DeskTop,VCode,VName,VNameCode,VUomName,VStkGpCode,VIndentTypeCode,VIndentTypeName,SPanel,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(issueReturnFrame);
                         DeskTop.repaint();
                         issueReturnFrame.setSelected(true);
                         DeskTop.updateUI();
                         issueReturnFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mIssueDelete)
               {
                    try
                    {
                         DeskTop.remove(issuedeletionframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         issuedeletionframe = new IssueDeletionFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,SItemTable);
                         DeskTop.add(issuedeletionframe);
                         DeskTop.repaint();
                         issuedeletionframe.setSelected(true);
                         DeskTop.updateUI();
                         issuedeletionframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			
               if(ae.getSource() == mServiceStockIssueDeletionFrame)
               {
                    try
                    {
                         DeskTop.remove(servicestockissuedeletionframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         servicestockissuedeletionframe = new ServiceStockIssueDeletionFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,SItemTable);
                         DeskTop.add(servicestockissuedeletionframe);
                         DeskTop.repaint();
                         servicestockissuedeletionframe.setSelected(true);
                         DeskTop.updateUI();
                         servicestockissuedeletionframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			
               if(ae.getSource() == mNonStockIssueDeletionFrame)
               {
                    try
                    {
                         DeskTop.remove(nonstockissuedeletionframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         nonstockissuedeletionframe = new NonStockIssueDeletionFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,SItemTable);
                         DeskTop.add(nonstockissuedeletionframe);
                         DeskTop.repaint();
                         nonstockissuedeletionframe.setSelected(true);
                         DeskTop.updateUI();
                         nonstockissuedeletionframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			
               if(ae.getSource()==mrdcIssue)
               {
                    try
                    {
                         DeskTop.remove(rdcissueframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         rdcissueframe = new RDCIssueFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,SItemTable,SSupTable,SYearCode,theTemplateList,theMaterialIssueTemplateList);

                         DeskTop.add(rdcissueframe);
                         DeskTop.repaint();
                         rdcissueframe.setSelected(true);
                         DeskTop.updateUI();
                         rdcissueframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               if(ae.getSource()==mGRNVerification)
               {
                    try
                    {
                         DeskTop.remove(grnverificationframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         grnverificationframe = new GRNVerificationFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,SYearCode,theTemplateList,theMaterialIssueTemplateList);

                         DeskTop.add(grnverificationframe);
                         DeskTop.repaint();
                         grnverificationframe.setSelected(true);
                         DeskTop.updateUI();
                         grnverificationframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mrdcReceiversList)
               {
                    try
                    {
                         DeskTop.remove(rdcReceiversFrame);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         rdcReceiversFrame = new RDCReceiversList(DeskTop,iUserCode,iMillCode,SYearCode);

                         DeskTop.add(rdcReceiversFrame);
                         DeskTop.repaint();
                         rdcReceiversFrame.setSelected(true);
                         DeskTop.updateUI();
                         rdcReceiversFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               if(ae.getSource()==mGrnVerifiedList)
               {
                    try
                    {
                         DeskTop.remove(grnverifiedlistframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         grnverifiedlistframe = new GRNVerifiedListFrame(DeskTop);
                         DeskTop.add(grnverifiedlistframe);
                         DeskTop.repaint();
                         grnverifiedlistframe.setSelected(true);
                         DeskTop.updateUI();
                         grnverifiedlistframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource() == mGreenTapeReceive)
               {
                    int iAmend = 0;

                    try
                    {
                         DeskTop.remove(greentapereceiveframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         greentapereceiveframe = new GreenTapeReceiveFrame(DeskTop,iUserCode,iAuthCode,iMillCode);
                         DeskTop.add(greentapereceiveframe);
                         DeskTop.repaint();
                         greentapereceiveframe.setSelected(true);
                         DeskTop.updateUI();
                         greentapereceiveframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

              /* if(ae.getSource()==mGreenTapeIssue)
               {
                    try
                    {
                         DeskTop.remove(greentapeissueframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                        // greentapeissueframe = new GreenTapeIssueFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,SItemTable,SSupTable,SYearCode,theTemplateList,theMaterialIssueTemplateList);
                         greentapeissueframe = new GreenTapeIssueFrame(DeskTop,iUserCode,iAuthCode,iMillCode);
                         DeskTop.add(greentapeissueframe);
                         DeskTop.repaint();
                         greentapeissueframe.setSelected(true);
                         DeskTop.updateUI();
                         greentapeissueframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }*/
               if(ae.getSource()==mGreenTapeReceipts)
               {
                    try
                    {
                         DeskTop.remove(greentapereceipts);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         greentapereceipts = new GreenTapeReceiptDetails(DeskTop,iUserCode,iAuthCode,iMillCode,SStDate,SEnDate);
                         DeskTop.add(greentapereceipts);
                         DeskTop.repaint();
                         greentapereceipts.setSelected(true);
                         DeskTop.updateUI();
                         greentapereceipts.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			
			if(ae.getSource()==mStockReceipt)
               {
                    try
                    {
                         DeskTop.remove(itemstockviewframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         itemstockviewframe = new ItemStockViewFrame(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(itemstockviewframe);
                         DeskTop.repaint();
                         itemstockviewframe.setSelected(true);
                         DeskTop.updateUI();
                         itemstockviewframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			
			if(ae.getSource()==mStockReceiptList)
               {
                    try
                    {
                         DeskTop.remove(stockreceiptlistframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         stockreceiptlistframe = new StockReceiptListFrame(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(stockreceiptlistframe);
                         DeskTop.repaint();
                         stockreceiptlistframe.setSelected(true);
                         DeskTop.updateUI();
                         stockreceiptlistframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			
	/*		if(ae.getSource()==mMaterialReceipt)
               {
                    try
                    {
                         DeskTop.remove(materialreceiptdetailsframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         materialreceiptdetailsframe = new MaterialReceiptDetailsFrame(DeskTop,iUserCode,iMillCode,iOwnerCode);
                         DeskTop.add(materialreceiptdetailsframe);
                         DeskTop.repaint();
                         materialreceiptdetailsframe.setSelected(true);
                         DeskTop.updateUI();
                         materialreceiptdetailsframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }  */
			
			if(ae.getSource()==mnonvaluelist)
               {
                    try
                    {
                         DeskTop.remove(nonvaluelistframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         nonvaluelistframe = new NonValueListFrame(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(nonvaluelistframe);
                         DeskTop.repaint();
                         nonvaluelistframe.setSelected(true);
                         DeskTop.updateUI();
                         nonvaluelistframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			   
			   
				if(ae.getSource()==mSeconSalesReceipt)
				{
						try
						{
							 DeskTop.remove(seconSalesTestMaterialReceipt);
							 DeskTop.repaint();
							 DeskTop.updateUI();
						}
						catch(Exception ex){}
						try
						{
							 seconSalesTestMaterialReceipt 	= 	new SeconSalesTestMaterialReceipt(DeskTop,iUserCode,iMillCode,iOwnerCode,theTemplateList,theMaterialIssueTemplateList);
							 DeskTop						.	add(seconSalesTestMaterialReceipt);
							 DeskTop						.	repaint();
							 seconSalesTestMaterialReceipt	.	setSelected(true);
							 DeskTop						.	updateUI();
							 seconSalesTestMaterialReceipt	.	show();
						}
						catch(Exception ex)
						{
							 System.out.println(ex);
						}
				}
			   
			   
			   
			   
			
			if(ae.getSource()==mSeconSalesReport)
               {
                    try
                    {
                         DeskTop.remove(secondSalesReceiptReport);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         secondSalesReceiptReport = new SecondSalesReceiptReport(DeskTop,iUserCode,iMillCode,iOwnerCode);
                         DeskTop.add(secondSalesReceiptReport);
                         DeskTop.repaint();
                         secondSalesReceiptReport.setSelected(true);
                         DeskTop.updateUI();
                         secondSalesReceiptReport.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			   
			   
			   
			if(ae.getSource()==mSeconSalesToYardReport)
               {
                    try
                    {
                         DeskTop.remove(secondSalesToYardDetailsReport);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
						
						
                       
                         secondSalesToYardDetailsReport = new SecondSalesToYardDetailsReport(DeskTop,iUserCode,iMillCode,iOwnerCode);
                         DeskTop.add(secondSalesToYardDetailsReport);
                         DeskTop.repaint();
                         secondSalesToYardDetailsReport.setSelected(true);
                         DeskTop.updateUI();
                         secondSalesToYardDetailsReport.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			   
			   
			   
			if(ae.getSource()==mSeconSalesToYard)
               {
                    try
                    {
                         DeskTop.remove(secondSalesToYardFrame);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         secondSalesToYardFrame = new SecondSalesToYardFrame(DeskTop,iUserCode,iMillCode,iOwnerCode);
                         DeskTop.add(secondSalesToYardFrame);
                         DeskTop.repaint();
                         secondSalesToYardFrame.setSelected(true);
                         DeskTop.updateUI();
                         secondSalesToYardFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			   
			   
			   
			if(ae.getSource()==mservicestocknew)
               {
                    try
                    {
                         DeskTop.remove(serviceStockTransferNew);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
						 serviceStockTransferNew = new ServiceStockTransferNew(DeskTop,iUserCode,iMillCode,theTemplateList,theMaterialIssueTemplateList);
						 
						 
                         DeskTop.add(serviceStockTransferNew);
                         DeskTop.repaint();
                         serviceStockTransferNew.setSelected(true);
                         DeskTop.updateUI();
                         serviceStockTransferNew.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

			
			
			
			
			if(ae.getSource()==mtestmreceipt)
               {
                    try
                    {
                         DeskTop.remove(testmaterialreceipt);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         //testmaterialreceipt = new TestMaterialReceipt(DeskTop,iUserCode,iMillCode,iOwnerCode);
						 testmaterialreceipt = new TestMaterialReceipt(DeskTop,iUserCode,iMillCode,iOwnerCode,theTemplateList,theMaterialIssueTemplateList);
						 
						 
                         DeskTop.add(testmaterialreceipt);
                         DeskTop.repaint();
                         testmaterialreceipt.setSelected(true);
                         DeskTop.updateUI();
                         testmaterialreceipt.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			   
			   
			   if(ae.getSource()==mScrapPreReceipt)
               {
                   
                    try
                    {
                       
						ScrapMaterialReceipt theFrame  = new ScrapMaterialReceipt(DeskTop,iUserCode,iMillCode,iOwnerCode,theTemplateList,theMaterialIssueTemplateList);
						 
						 
                         DeskTop.add(theFrame);
                         DeskTop.repaint();
                         theFrame.setSelected(true);
                         DeskTop.updateUI();
                         theFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			   
			   
			   if(ae.getSource()==mNewScrabIssueFrame)
               {
                   
                    try
                    {
                       
						 NewScrapIssueDetails newscrapissuedetails = new NewScrapIssueDetails(DeskTop,iUserCode,iMillCode,iOwnerCode);//,theTemplateList,theMaterialIssueTemplateList);
						 
                         DeskTop.add(newscrapissuedetails);
                         DeskTop.repaint();
                         newscrapissuedetails.setSelected(true);
                         DeskTop.updateUI();
                         newscrapissuedetails.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			/*   if(ae.getSource()==mNewDirectScrabIssueFrame)
               {
                   
                    try
                    {
                       
						 NewDirectScrapIssueDetails theFrame = new NewDirectScrapIssueDetails(DeskTop,iUserCode,iMillCode,iOwnerCode);
						 
                         DeskTop.add(theFrame);
                         DeskTop.repaint();
                         theFrame.setSelected(true);
                         DeskTop.updateUI();
                         theFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }*/
			   
			   if(ae.getSource()==mNewScrabIssueReport)
               {
                   
                    try
                    {
                       
						 NewScrabIssueDetailsReport newscrapissuedetailsrep = new NewScrabIssueDetailsReport(DeskTop,iUserCode,iMillCode,iOwnerCode);//,theTemplateList,theMaterialIssueTemplateList);
						 
                         DeskTop.add(newscrapissuedetailsrep);
                         DeskTop.repaint();
                         newscrapissuedetailsrep.setSelected(true);
                         DeskTop.updateUI();
                         newscrapissuedetailsrep.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			   
			   /*if(ae.getSource()==mScrabIssueReport)
               {
                   
                    try
                    {
                       
						 NewScrabIssueReport theFrame = new NewScrabIssueReport(DeskTop,iUserCode,iMillCode,iOwnerCode);//,theTemplateList,theMaterialIssueTemplateList);
						 
                         DeskTop.add(theFrame);
                         DeskTop.repaint();
                         theFrame.setSelected(true);
                         DeskTop.updateUI();
                         theFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }*/
			
			
			
		/*	if(ae.getSource()==mtestingpunch)
               {
                    try
                    {
                         DeskTop.remove(testingframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         testingframe = new TestingFrame(DeskTop,iUserCode,iMillCode,iOwnerCode,theTemplateList,theMaterialIssueTemplateList,SAuthUserCode,SDept,SUnit);
                         DeskTop.add(testingframe);
                         DeskTop.repaint();
                         testingframe.setSelected(true);
                         DeskTop.updateUI();
                         testingframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			*/
			if(ae.getSource()==mstockbarcode)
               {
                    try
                    {
                         DeskTop.remove(closingstockbarcodeprint);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         closingstockbarcodeprint = new ClosingStockBarcodePrint(DeskTop,iUserCode,iMillCode,SItemTable);
                         DeskTop.add(closingstockbarcodeprint);
                         DeskTop.repaint();
                         closingstockbarcodeprint.setSelected(true);
                         DeskTop.updateUI();
                         closingstockbarcodeprint.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			
			if(ae.getSource()==mscrabreceiptframe)
               {
                    try
                    {
                         DeskTop.remove(scrabreceiptframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         scrabreceiptframe = new ScrabReceiptFrame(DeskTop,iUserCode,iMillCode,iOwnerCode);
                         DeskTop.add(scrabreceiptframe);
                         DeskTop.repaint();
                         scrabreceiptframe.setSelected(true);
                         DeskTop.updateUI();
                         scrabreceiptframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			
		
		if(ae.getSource()==mmaterialinstallation)
         	{
               try
               {
                    DeskTop.remove(materialinstallationauthenticationnew);
                    DeskTop.updateUI();
               }
               catch(Exception ex){}

               try
               {
                    materialinstallationauthenticationnew 	= new MaterialInstallationAuthenticationNew(DeskTop, iUserCode,iMillCode,iOwnerCode,SItemTable);
                    DeskTop         					. add(materialinstallationauthenticationnew);
                    DeskTop         					. repaint();
                    materialinstallationauthenticationnew 	. setSelected(true);
                    DeskTop         					. updateUI();
                    materialinstallationauthenticationnew 	. show();
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
			   
			   
               
            }
			
			
			if(ae.getSource()==mScrabReport)
               {
                    try
                    {
                         DeskTop.remove(scrabreceiptreport);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         scrabreceiptreport = new ScrabReceiptReport(DeskTop,iUserCode,iMillCode,iOwnerCode);
                         DeskTop.add(scrabreceiptreport);
                         DeskTop.repaint();
                         scrabreceiptreport.setSelected(true);
                         DeskTop.updateUI();
                         scrabreceiptreport.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			
			
			if(ae.getSource()==mNewScrabReport)
               {
                   try{
                       
                        NewScrabReceiptReport theFrame = new NewScrabReceiptReport(DeskTop,iUserCode,iMillCode,iOwnerCode);
                         DeskTop.add(theFrame);
                         DeskTop.repaint();
                         theFrame.setSelected(true);
                         DeskTop.updateUI();
                         theFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			
			
			
			/*if(ae.getSource()==mReceiptReport)
               {
                    try
                    {
                         DeskTop.remove(materialreceiptreportdetails);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         materialreceiptreportdetails = new MaterialReceiptReportDetails(DeskTop,iUserCode,iMillCode,iOwnerCode);
                         DeskTop.add(materialreceiptreportdetails);
                         DeskTop.repaint();
                         materialreceiptreportdetails.setSelected(true);
                         DeskTop.updateUI();
                         materialreceiptreportdetails.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }*/
			
			
			/*if(ae.getSource()==mScrabPrint)
               {
                    try
                    {
                         DeskTop.remove(scrabdetailsreport);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         scrabdetailsreport = new ScrabDetailsReport(DeskTop,iUserCode,iMillCode,iOwnerCode);
                         DeskTop.add(scrabdetailsreport);
                         DeskTop.repaint();
                         scrabdetailsreport.setSelected(true);
                         DeskTop.updateUI();
                         scrabdetailsreport.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }*/
			
			if(ae.getSource()==mBarcodeprint)
               {
                    try
                    {
                         DeskTop.remove(newbarcodeprint);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         newbarcodeprint = new NewBarcodePrint(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(newbarcodeprint);
                         DeskTop.repaint();
                         newbarcodeprint.setSelected(true);
                         DeskTop.updateUI();
                         newbarcodeprint.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			
			
		/*	if(ae.getSource()==mScrabReceipt)
               {
                    try
                    {
                         DeskTop.remove(scrabreceiptstatusframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         scrabreceiptstatusframe = new ScrabReceiptStatusFrame(DeskTop,iUserCode,iMillCode,iOwnerCode);
                         DeskTop.add(scrabreceiptstatusframe);
                         DeskTop.repaint();
                         scrabreceiptstatusframe.setSelected(true);
                         DeskTop.updateUI();
                         scrabreceiptstatusframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

			*/
			
			
               if(ae.getSource()==mTransfer)
               {
                   /* try
                    {
                         DeskTop.remove(stocktransferframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         stocktransferframe = new StockTransferFrame(DeskTop,VCode,VName,VUomName,VNameCode,SPanel,iUserCode,iMillCode,SItemTable,SSupTable,SYearCode,SMillName,SStDate);
                         DeskTop.add(stocktransferframe);
                         DeskTop.repaint();
                         stocktransferframe.setSelected(true);
                         DeskTop.updateUI();
                         stocktransferframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }*/
               }
			
			/*if(ae.getSource()==mservicestock)
               {
                    try old service stock screen...15.09.2020 blocked..
                    {
                         DeskTop.remove(servicestocktransfer);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         servicestocktransfer = new ServiceStockTransfer(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(servicestocktransfer);
                         DeskTop.repaint();
                         servicestocktransfer.setSelected(true);
                         DeskTop.updateUI();
                         servicestocktransfer.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }*/
			
			
			if(ae.getSource()==mservicestocklist)
               {
                    try
                    {
                         DeskTop.remove(servicestocktransferlist);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                       
                         servicestocktransferlist = new ServiceStockTransferList(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(servicestocktransferlist);
                         DeskTop.repaint();
                         servicestocktransferlist.setSelected(true);
                         DeskTop.updateUI();
                         servicestocktransferlist.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mConcTranList)
               {
                    try
                    {
                         DeskTop.remove(concernTransferListFrame);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         concernTransferListFrame = new ConcernTransferListFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,iAuthCode,SItemTable,SSupTable,SMillName);
                         DeskTop.add(concernTransferListFrame);
                         DeskTop.repaint();
                         concernTransferListFrame.setSelected(true);
                         DeskTop.updateUI();
                         concernTransferListFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mConcTranInvoice)
               {
                    try
                    {
                         DeskTop.remove(concernTransferInvoiceFrame);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         concernTransferInvoiceFrame = new ConcernTransferInvoiceFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,iAuthCode,SItemTable,SSupTable,SMillName);
                         DeskTop.add(concernTransferInvoiceFrame);
                         DeskTop.repaint();
                         concernTransferInvoiceFrame.setSelected(true);
                         DeskTop.updateUI();
                         concernTransferInvoiceFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mConcInvoicePrint)
               {
                    try
                    {
                         DeskTop.remove(concernTransferInvoicePrintFrame);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         concernTransferInvoicePrintFrame = new ConcernTransferInvoicePrintFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,iAuthCode,SItemTable,SSupTable,SMillName);
                         DeskTop.add(concernTransferInvoicePrintFrame);
                         DeskTop.repaint();
                         concernTransferInvoicePrintFrame.setSelected(true);
                         DeskTop.updateUI();
                         concernTransferInvoicePrintFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               /*if(ae.getSource()==mPartyTransfer)
               {
                    try
                    {
                         DeskTop.remove(partystocktransferframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         partystocktransferframe = new PartyStockTransferFrame(DeskTop,VCode,VName,VNameCode,SPanel,iUserCode,iMillCode,SItemTable,SSupTable,SYearCode,SMillName,SStDate);
                         DeskTop.add(partystocktransferframe);
                         DeskTop.repaint();
                         partystocktransferframe.setSelected(true);
                         DeskTop.updateUI();
                         partystocktransferframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }*/

               if(ae.getSource()==mJobWorkIssue)
               {
                    try
                    {
                         DeskTop.remove(jobworkissueframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         jobworkissueframe = new JobWorkIssueFrame(DeskTop,VCode,VName,VNameCode,SPanel,iUserCode,iMillCode,SItemTable,SSupTable,SYearCode,SMillName,SStDate);
                         DeskTop.add(jobworkissueframe);
                         DeskTop.repaint();
                         jobworkissueframe.setSelected(true);
                         DeskTop.updateUI();
                         jobworkissueframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mUserIndent)
               {
                    try
                    {
                         DeskTop.remove(userIndentFrame);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         userIndentFrame = new UserIndentFrame(DeskTop,iUserCode,iMillCode,SItemTable);
                         DeskTop.add(userIndentFrame);
                         DeskTop.repaint();
                         userIndentFrame.setSelected(true);
                         DeskTop.updateUI();
                         userIndentFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mIndentAuth)
               {
                    try
                    {
                         DeskTop.remove(indentAuthFrame);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         indentAuthFrame = new IndentAuthFrame(DeskTop,iUserCode,iMillCode,SItemTable);
                         DeskTop.add(indentAuthFrame);
                         DeskTop.repaint();
                         indentAuthFrame.setSelected(true);
                         DeskTop.updateUI();
                         indentAuthFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

/*               if(ae.getSource()==mIndentList)
               {
                    try
                    {
                         DeskTop.remove(indentListFrame);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         indentListFrame = new IndentListFrame(DeskTop,VCode,VName,VUomName,SPanel,iUserCode,iMillCode,iAuthCode);
                         DeskTop.add(indentListFrame);
                         DeskTop.repaint();
                         indentListFrame.setSelected(true);
                         DeskTop.updateUI();
                         indentListFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }*/

          }
     }

     public void getIndentCode()
     {
          VIndentTypeCode     = new Vector();
          VIndentTypeName     = new Vector();

          String QS      = "Select code,name from indenttype where (millcode="+iMillCode +" or millcode = 2) and  issueflag = 0";

          try  {
                    ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                    Connection     theConnection  = oraConnection.getConnection();   
                    Statement      stat           = theConnection.createStatement();
     
                    ResultSet result = stat.executeQuery(QS);


                    while(result.next())
                    {
                         VIndentTypeCode     .addElement(common.parseNull(result.getString(1)));
                         VIndentTypeName     .addElement(common.parseNull(result.getString(2)));
                    }
                    result    .close();
                    stat      .close();
          } catch(Exception Ex)
          {
               System.out.println("getIndentCode from Indent "+Ex);
          }
     }

     public void setDataIntoVector()
     {
          String QS = "";

          VName     .removeAllElements();
          VUomName  .removeAllElements();
          VCode     .removeAllElements();
               
          theMenuBar.setEnabled(false);
          int iMax = common.toInt(control.getID("Select count(*) From "+SItemTable));


          JProgressBar   progress  = new JProgressBar();
                         progress  . setMinimum(0);
                         progress  . setMaximum(iMax);
                         SPanel    . Panel2.add(progress);
                         SPanel    . Panel2.updateUI();

          if(iMillCode==0)
          {
               QS = "Select Item_Name,Item_Code,UoMName,StkGroupCode From InvItems inner join UOM on UOM.UOMCode = InvItems.UOMCode Order By Item_Name";
          }
          else
          {
               QS = "Select Item_Name,"+SItemTable+".Item_Code,UoMName,"+SItemTable+".StkGroupCode From "+SItemTable+" inner join invitems on invitems.item_code = "+SItemTable+".item_code inner join UOM on UOM.UOMCode = InvItems.UOMCode Order By Item_Name";
          }
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();   
               Statement      stat           = theConnection.createStatement();

               ResultSet result1 = stat.executeQuery(QS);

               int ctr=0;
               while(result1.next())
               {
                    ctr++;
                    VName     .addElement(common.parseNull(result1.getString(1)));
                    VCode     .addElement(common.parseNull(result1.getString(2)));
                    VUomName  .addElement(common.parseNull(result1.getString(3)));
                    VNameCode .addElement(common.parseNull(result1.getString(1))+" (Code : "+common.parseNull(result1.getString(2))+")");
                    VStkGpCode.addElement(common.parseNull(result1.getString(4)));
                    progress  .setValue(ctr);
               }
               result1.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               JOptionPane.showMessageDialog(null,"Unable to Establish a Connection with Server","Information",JOptionPane.INFORMATION_MESSAGE);
               System.exit(0);
          }
          SPanel.Panel2.removeAll();
          SPanel.Panel2.updateUI();
          theMenuBar.setEnabled(true);
     }

     public class WinList extends WindowAdapter
     {
          public void windowClosing(WindowEvent we)
          {
               System.exit(0);
          }
     }

     public void updateLookAndFeel()
     {
          try
          {
               UIManager.setLookAndFeel(currentLookAndFeel);
          }
          catch (Exception ex)
          {
               System.out.println("Failed loading L&F: " + currentLookAndFeel);
               System.out.println(ex);
          }
     }
}
