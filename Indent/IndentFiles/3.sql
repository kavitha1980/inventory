select IndentNo,AuthDate,RawUser.UserName as DeptName,Unit.Unit_Name,
sum(Qty),sum(issueQty),
Indent.AuthUserCode,indentType.Name, Indent.UserCode from Indent
Inner join RawUser on RawUser.UserCode=Indent.AuthUserCode
Inner join IndentType on IndentType.Code=Indent.IndentType
Inner join Unit on unit.Unit_Code=Indent.Unit_Code
Where issueStatus=0 and Indent.MillCode=iMillCode+" and Indent.Status=1 and Indent.CancelStatus=0
group by AuthDate,IndentNo,RawUser.UserName,Unit.Unit_Name,Indent.AuthUsercode,IndentType.Name, Indent.UserCode 
having sum(Qty)-sum(issueQty)>0
order by 1

