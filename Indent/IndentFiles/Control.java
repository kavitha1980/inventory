package Indent.IndentFiles;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;

import jdbc.*;
import util.*;
import guiutil.*;

public class Control 
{
     String ID="";
     Common common = new Common();
     public Control()
     {
     
     }
     
     public String getID(String QueryString)
     {
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();   
               Statement      theStatement   = theConnection.createStatement();
               ResultSet      theResult      = theStatement.executeQuery(QueryString);


               while(theResult.next())
               {
                    ID = theResult.getString(1);
               }
               theResult      .close();
               theStatement   .close();
          }
          catch(SQLException SQLE)
          {
               System.out.println(SQLE);
               SQLE.printStackTrace();
          }
          return ID;
     } 

     public void setID(String AutoNo,String Column)
     {
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();   
               Statement      theStatement   = theConnection.createStatement();

               String ControlString      = "Update Config set "+Column+"="+AutoNo; 

               theStatement.executeUpdate(ControlString);

               theStatement   .close();
          }
          catch(SQLException SQLE)
          {
               System.out.println("Error in Control.java on setID()  :"+SQLE);
               JOptionPane.showMessageDialog(null,"Error in Control.java","Information",JOptionPane.INFORMATION_MESSAGE);
               SQLE.printStackTrace();

          }
     }
}


