package Indent.IndentFiles;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;


import com.digitalpersona.onetouch.*;
import com.digitalpersona.onetouch.capture.*;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.*;
import com.digitalpersona.onetouch.verification.*;

public class GRNVerificationFrame extends JInternalFrame
{
    JPanel                    pnlTop,pnlMiddle,pnlBottom;
    JButton                   btnRefresh,btnPunch,btnCancel;
    JTable                    theTable   = null;
    JLabel                    LStatus;    
    JComboBox                 cmbUnit;
    JLayeredPane              Layer;
    ArrayList                 theGRNList = null;
    GRNVerificationListModel  theModel = null;
    
    ArrayList      theTemplateList,theMaterialIssueTemplateList;
    Vector         VCode,VName,VUomName,VUnitCode,VUnitName;
    StatusPanel    SPanel;
    String         SYearCode;
    Common         common ;
   
    int            iMillCode = 0,iUserCode,iUnitCode=0;
    
    private        DPFPCapture capturer = DPFPGlobal.getCaptureFactory().createCapture();

    private        DPFPVerification verificator = DPFPGlobal.getVerificationFactory().createVerification();

    DPFPTemplate   template;
    
    private JTextField   prompt = new JTextField();
    private JTextArea    log    = new JTextArea();
    private JTextField   status = new JTextField("[status line]");
    
    int iPunchStatus = 0;

    // set Model Index
    int SNo=0,ItemCode=1,ItemName=2,GRNNo=3,GRNDate=4,GRNQty=5,UnitName=6,MRSUser=7,Select=8;
    public GRNVerificationFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUomName,StatusPanel SPanel,int iUserCode,int iMillCode,String SYearCode,ArrayList theTemplateList,ArrayList theMaterialIssueTemplateList)
    {
        this . Layer     = Layer;
        this . VCode     = VCode;
        this . VName     = VName;
        this . VUomName  = VUomName;
        this . SPanel    = SPanel;
        this . iUserCode = iUserCode;
        this . iMillCode = iMillCode;
        this . SYearCode = SYearCode;
        this . theTemplateList              = theTemplateList;
        this . theMaterialIssueTemplateList = theMaterialIssueTemplateList;
        
        common = new Common();
        setUnitVector();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
        setTableData();
    }
    private void createComponents()
    {
       pnlTop     = new JPanel();
       pnlMiddle  = new JPanel();
       pnlBottom  = new JPanel();
       
       LStatus    = new JLabel("");
 
       cmbUnit    = new JComboBox(VUnitName);       

       btnRefresh = new JButton("Refresh");
       btnPunch   = new JButton("Punch");
       btnCancel  = new JButton("Cancel");
       
       theModel   = new GRNVerificationListModel();
       theTable   = new JTable(theModel);
       
       theTable    . getColumn("S.No")     . setPreferredWidth(20);
       theTable    . getColumn("Item Name"). setPreferredWidth(100);
       
       TableColumn tc = theTable.getColumnModel().getColumn(Select);
       tc.setCellEditor(theTable.getDefaultEditor(Boolean.class));
       tc.setCellRenderer(theTable.getDefaultRenderer(Boolean.class));
       tc.setHeaderRenderer(new CheckBoxHeader(new MyItemListener()));
    }
    class MyItemListener implements ItemListener
    {
      public void itemStateChanged(ItemEvent e) 
      {
        //Object source = e.getSource();
        //if(source instanceof AbstractButton == false) return;

        boolean checked = e.getStateChange() == ItemEvent.SELECTED;
        for(int x = 0; x < theTable.getRowCount(); x++)
        {
          theTable.setValueAt(new Boolean(checked),x,Select);
        }
    }
  }
    private void setLayouts()
    {
        pnlTop     . setLayout(new FlowLayout(FlowLayout.CENTER));
        pnlMiddle  . setLayout(new BorderLayout());
        pnlBottom  . setLayout(new FlowLayout(FlowLayout.CENTER));
        
        this       . setSize(900,400);
        this       . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this       . setTitle("GRN List For Verification");
        this       . setMaximizable(true);
        this       . setClosable(true);
    }
    private void addComponents()
    {
      // pnlTop     . add(new JLabel("GRN List For Verification"));

       pnlTop     . add(new JLabel("Unit Name "));
       pnlTop     . add(cmbUnit);

       pnlMiddle  . add(new JScrollPane(theTable));

       pnlBottom  . add(btnRefresh); 
       pnlBottom  . add(btnPunch);
       pnlBottom  . add(btnCancel);
       pnlBottom  . add(LStatus);
       
       this       . add(pnlTop,BorderLayout.NORTH);
       this       . add(pnlMiddle,BorderLayout.CENTER);
       this       . add(pnlBottom,BorderLayout.SOUTH);
    }
    private void addListeners()
    {
        btnRefresh  . addActionListener(new ActList());
        btnPunch    . addActionListener(new ActList());
        btnCancel   . addActionListener(new ActList());
        cmbUnit     . addItemListener(new ItemList());
    }
    private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==btnRefresh)
            {
               setTableData();
            }

            if(ae.getSource()==btnPunch)
            {
                init();
                start();
                btnPunch.setEnabled(false);
                iPunchStatus=1;
            }
            if(ae.getSource()==btnCancel)
            {
                dispose();
            }
        }
    }
    private class ItemList implements ItemListener
    {
        public void itemStateChanged(ItemEvent ie)
        {
            if(ie.getSource()==cmbUnit && ie.getStateChange()==1)
            {
                
                setTableData();
            }
        }
    }
    private void setTableData()
    {
        iUnitCode = common.toInt((String)VUnitCode.elementAt(VUnitName.indexOf((String)cmbUnit.getSelectedItem())));
        setDataVector();
        theModel . setNumRows(0);
        for(int i=0;i<theGRNList.size();i++)
        {
            HashMap theMap = (HashMap)theGRNList.get(i);
            
            Vector theVect = new Vector();
            theVect . addElement(String.valueOf(i+1));
            theVect . addElement((String)theMap.get("ItemCode"));
            theVect . addElement((String)theMap.get("ItemName"));            
            theVect . addElement((String)theMap.get("GrnNo"));
            theVect . addElement(common.parseDate((String)theMap.get("GrnDate")));
            theVect . addElement((String)theMap.get("GrnQty"));            
            theVect . addElement((String)theMap.get("UnitName"));            
            theVect . addElement((String)theMap.get("UserName"));            
            theVect . addElement(new Boolean(false));
            theModel . appendRow(theVect);
        }
    }
    private void setDataVector()
    {
        theGRNList = new ArrayList();
        StringBuffer sb = new StringBuffer();
        try
        {
            sb.append(" select Grn.GRNNo,Grn.GrnDate,Grn.GrnQty,RawUser.UserName,Grn.Code,InvItems.Item_Name,Unit.Unit_Name  from Grn");
            sb.append(" Inner join RawUser on RawUser.UserCode    = Grn.MrsAuthUserCode");
            sb.append(" Inner join InvItems on InvItems.Item_Code = Grn.Code");
            sb.append(" Inner join Unit on Unit.Unit_Code = Grn.Unit_Code and Grn.Unit_Code="+iUnitCode);
            sb.append(" Left join GRNInspectionDetails on GrnInspectionDetails.GRNNo = GRN.GRNNo and GrnInspectionDetails.ItemCode=Grn.Code");
            sb.append(" where  GRN.MillCode="+iMillCode+" and GRN.GrnDate>=20161101");   
            sb.append(" and GRN.GRNAuthStatus=0");
            sb.append(" and GRN.MRSAuthUserCode=1988 ");
            sb.append(" and GrnInspectionDetails.GrnNo is null ");
            sb.append(" order by Unit.Unit_Name,Grn.GrnDate");
            
            ORAConnection connect     = ORAConnection.getORAConnection();
            Connection theConnection  = connect.getConnection();
            
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet         theResult    = theStatement.executeQuery();
            
            while(theResult.next())
            {
                HashMap theMap = new HashMap();
                theMap . put("GrnNo",theResult.getString(1));
                theMap . put("GrnDate",theResult.getString(2));
                theMap . put("GrnQty",theResult.getString(3));
                theMap . put("UserName",theResult.getString(4));
                theMap . put("ItemCode",theResult.getString(5));
                theMap . put("ItemName",theResult.getString(6));
                theMap . put("UnitName",theResult.getString(7));
                theGRNList . add(theMap);
            }
            
        }
        catch(Exception ex)
        {
            System.out.println(" setData Vector "+ex);
        }
    }
    private void setUnitVector()
    {
        VUnitCode = new Vector();
        VUnitName = new Vector();
        StringBuffer sb = new StringBuffer(" select Unit_Code,Unit_Name from Unit order by 2");
        try
        {
            ORAConnection connect     = ORAConnection.getORAConnection();
            Connection theConnection  = connect.getConnection();
            
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet         theResult    = theStatement.executeQuery();
            
            while(theResult.next())
            {
                VUnitCode . addElement(theResult.getString(1));
                VUnitName . addElement(theResult.getString(2));
            }   
            theResult    . close();
            theStatement . close();
        }
        catch(Exception ex)
        {
            System.out.println(" set Unit Vector "+ex);
        }
        
    }
    private boolean saveData(String SCode,String SName)
    {
      boolean bSave = false;
      try
      {
         if(checkSelected())
         {
            for(int i=0;i<theModel.getRowCount();i++)   
            {
            if(((Boolean)theModel.getValueAt(i,Select)).booleanValue())
            {
                String SGRNNo    = (String)theModel.getValueAt(i,GRNNo);
                String SGRNDate  = common.pureDate((String)theModel.getValueAt(i,GRNDate));
                double dGRNQty   = common.toDouble(String.valueOf(theModel.getValueAt(i,GRNQty)));
                String SItemCode = String.valueOf(theModel.getValueAt(i,ItemCode));
                String SUnitName = String.valueOf(theModel.getValueAt(i,UnitName));
                String SUnitCode = getUnitCode(SUnitName);
                if(!isAlreadyExists(SGRNNo,SGRNDate,dGRNQty,SItemCode,SUnitCode))   
                {
                    bSave = insertGRNInspectionData(SGRNNo,SGRNDate,dGRNQty,SCode,iMillCode,SItemCode,SUnitCode);
                    btnPunch.setEnabled(true);
                }
              }
            }
        }
        else
        {
          JOptionPane .showMessageDialog(null,"No Rows Selected For Save ","Info",JOptionPane.INFORMATION_MESSAGE);
        }
     }
         catch(Exception ex)
         {
             bSave = false;
             ex.printStackTrace();
         }
         return bSave;
     }
     private String getUnitCode(String SUnitName)
     {
        String SUnitCode = "";
        for(int i=0;i<VUnitName.size();i++)
        {
           String SUName = (String)VUnitName.elementAt(i);
           if(SUnitName.equals(SUName))
           {
              SUnitCode =  (String)VUnitCode.elementAt(i);
              break;
           }
        }
        return SUnitCode;
     }
     public boolean checkSelected()
     {
         boolean bSelect =false;
         for(int i=0;i<theModel.getRowCount();i++)
         {
             boolean bVal = ((Boolean)theModel.getValueAt(i, Select)).booleanValue();
             if(bVal)
             {
                 bSelect = true;
                 break;
             }
         }
         
         return bSelect;
     }
     private boolean isAlreadyExists(String SGRNNo,String SGRNDate,double dGRNQty,String SItemCode,String SUnitCode)
     {
         int iCount=0;
         try
         {
           StringBuffer sb = new StringBuffer();  
           sb.append(" Select count(*) from GRNInspectionDetails where GRNNo="+SGRNNo);
           sb.append(" and GRNDate = "+SGRNDate+" and GRNQty = "+dGRNQty+" and ItemCode='"+SItemCode+"' and UnitCode="+SUnitCode);
           
          Connection theConnection=null;

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(sb.toString());
               while(result.next())
               {
                   iCount =result.getInt(1);
               }
               result.close();
               theStatement.close();
          
      }
      catch(Exception ex)
      {
          ex.printStackTrace();
          return true;
      }
       if(iCount>0)
           return true;
       else
           return false;
   }
   private boolean insertGRNInspectionData(String SGRNNo,String SGRNDate,double dGRNQty,String SEmpCode,int iMillCode,String SItemCode,String SUnitCode)
   {
        ORAConnection connect    =  ORAConnection.getORAConnection();
        Connection theConnection =  connect.getConnection();               

         boolean bSave;
         try
         {
          StringBuffer sb = new StringBuffer();
          
           sb.append(" Insert into GRNInspectionDetails(Id,GRNNo,GRNDate,GrnQty,EmpCode,MillCode,EntryUserCode,ItemCode,EntryDateTime,UnitCode)");
           sb.append(" values(GrnInspectionDet_Seq.NextVal,?,?,?,?,?,?,?,to_Date(to_Char(sysdate,'dd-mm-yyyy hh24:mi:ss'),'dd-mm-yyyy hh24:mi:ss'),?)");
           
           
           PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
           theStatement  . setString(1,SGRNNo);
           theStatement  . setString(2,SGRNDate);
           theStatement  . setDouble(3,dGRNQty);
           theStatement  . setString(4,SEmpCode);
           theStatement  . setInt(5,iMillCode);
           theStatement  . setInt(6,iUserCode);
           theStatement  . setString(7,SItemCode);
           theStatement  . setString(8,SUnitCode);
           theStatement  . executeUpdate(); 
           bSave = true;
           theConnection.commit();
           theStatement.close();
         }
         catch(Exception ex)
         {
             bSave =false;
             try
             {    
                theConnection.rollback();
             }
             catch(Exception e)
             {
                 e.printStackTrace();
             }
             ex.printStackTrace();
         }
           return bSave;
     }
    
    // Punching Process
    
     protected void init()        
     {
        capturer = DPFPGlobal.getCaptureFactory().createCapture();
        capturer.addDataListener(new DPFPDataAdapter() 
        {
            public void dataAcquired(final DPFPDataEvent e) 
            {
                    LStatus.setText(" Please wait Under processing.................");

				SwingUtilities.invokeLater(new Runnable()
                                {
                                        public void run()
                                        {
                                    
                                                LStatus.setText("The fingerprint sample was captured....");
                                                process(e.getSample());
                                                
                                        }});
			}
		});
               
		capturer.addReaderStatusListener(new DPFPReaderStatusAdapter() {
                public void readerConnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
                                    
		 			makeReport("The fingerprint reader was connected.");
				}});
			}
                public void readerDisconnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was disconnected.");
				}});
			}
		});
		capturer.addSensorListener(new DPFPSensorAdapter() {
                public void fingerTouched(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was touched.");
				}});
			}
                public void fingerGone(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The finger was removed from the fingerprint reader.");
				}});
			}
		});
		capturer.addImageQualityListener(new DPFPImageQualityAdapter() {
                public void onImageQuality(final DPFPImageQualityEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					if (e.getFeedback().equals(DPFPCaptureFeedback.CAPTURE_FEEDBACK_GOOD))
						makeReport("The quality of the fingerprint sample is good.");
					else
						makeReport("The quality of the fingerprint sample is poor.");
				}});
			}
		});
	}

	protected void process(DPFPSample sample)
	{
		// Draw fingerprint sample image.
		
		DPFPFeatureSet features = extractFeatures(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

		// Check quality of the sample and start verification if it's good
		int iStatus=0;
   	        String SName  = "";
		String SCode  = "";
                LStatus.setText(" Under processing.................");

		if (features != null)
		{
			// Compare the feature set with our template
               for(int i=0; i<theMaterialIssueTemplateList.size(); i++)
               {
                    HashMap theMap = (HashMap) theMaterialIssueTemplateList.get(i);

                    SCode    = (String)theMap.get("EMPCODE");
                    SName    = (String)theMap.get("DISPLAYNAME");
                    
                    DPFPTemplate template   = (DPFPTemplate)theMap.get("TEMPLATE");

                    DPFPVerification matcher= DPFPGlobal.getVerificationFactory().createVerification();
                    matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);

                    DPFPVerificationResult result = 
                    matcher.verify(features,template);

                    if (result.isVerified())
                    {
                         LStatus.setText(""+SName);       
                         iStatus=1;
                         break;
                    }
               }


               if(iStatus==0)
               {

                    for(int i=0; i<theTemplateList.size(); i++)
                    {
     
                         HashMap theMap = (HashMap) theTemplateList.get(i);
     
                         SCode    = (String)theMap.get("EMPCODE");
                         SName    = (String)theMap.get("DISPLAYNAME");
                         
                         DPFPTemplate template   = (DPFPTemplate)theMap.get("TEMPLATE");
     
                         DPFPVerification matcher= DPFPGlobal.getVerificationFactory().createVerification();
                         matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);
     
                         DPFPVerificationResult result = 
                         matcher.verify(features,template);
     
                         if (result.isVerified())
                         {
                                LStatus.setText(""+SName);       

                              iStatus=1;
                              break;
                         }
                    }
               }

                  if(iStatus==0)
                  {
                         LStatus.setText("No Match Found");       
                         JOptionPane.showMessageDialog(null," No Match Found Punch Again");
                         stop();
                         iPunchStatus=0;
                         btnPunch.setEnabled(true);
                  }
                  else
                  {
                      if(saveData(SCode,SName))
                      {
                            JOptionPane.showMessageDialog(null," Grn Verified ");
                           // removeHelpFrame();
                            
                           // LStatus.setText(" Punch And Take Issue");
                           
                            stop();
                            setTableData();
                            iPunchStatus=0;
                            btnPunch.setEnabled(true);
                       }
                       else
                       {
                            JOptionPane.showMessageDialog(null," Problem in Storing Data");
                            stop();
                            iPunchStatus=0;
                            btnPunch.setEnabled(true);
                       }

                }
          }
	}
	protected void start()
	{
            capturer.startCapture();
	}

	protected void stop()
	{
             capturer.stopCapture();
	}
	public void setPrompt(String string) {
	}
	public void makeReport(String string) {
            System.out.println(string);
	}
	
	protected Image convertSampleToBitmap(DPFPSample sample) {
		return DPFPGlobal.getSampleConversionFactory().createImage(sample);
	}

	protected DPFPFeatureSet extractFeatures(DPFPSample sample, DPFPDataPurpose purpose)
	{
		DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
		try {
			return extractor.createFeatureSet(sample, purpose);
		} catch (DPFPImageQualityException e)
        {
            System.out.println(" Returning Null");

        	return null;
		}
	}

class CheckBoxHeader extends JCheckBox
    implements TableCellRenderer, MouseListener {
  protected CheckBoxHeader rendererComponent;
  protected int column;
  protected boolean mousePressed = false;
  public CheckBoxHeader(ItemListener itemListener) {
    rendererComponent = this;
    rendererComponent.addItemListener(itemListener);
  }
  public Component getTableCellRendererComponent(
      JTable table, Object value,
      boolean isSelected, boolean hasFocus, int row, int column) {
    if (table != null) {
      JTableHeader header = table.getTableHeader();
      if (header != null) {
        rendererComponent.setForeground(header.getForeground());
        rendererComponent.setBackground(header.getBackground());
        rendererComponent.setFont(header.getFont());
        header.addMouseListener(rendererComponent);
      }
    }
    setColumn(column);
    rendererComponent.setText("Check All");
    setBorder(UIManager.getBorder("TableHeader.cellBorder"));
    return rendererComponent;
  }
  protected void setColumn(int column) {
    this.column = column;
  }
  public int getColumn() {
    return column;
  }
  protected void handleClickEvent(MouseEvent e) {
    if (mousePressed) {
      mousePressed=false;
      JTableHeader header = (JTableHeader)(e.getSource());
      JTable tableView = header.getTable();
      TableColumnModel columnModel = tableView.getColumnModel();
      int viewColumn = columnModel.getColumnIndexAtX(e.getX());
      int column = tableView.convertColumnIndexToModel(viewColumn);
  
      if (viewColumn == this.column && e.getClickCount() == 1 && column != -1) {
        doClick();
      }
    }
  }
  public void mouseClicked(MouseEvent e) {
    handleClickEvent(e);
    ((JTableHeader)e.getSource()).repaint();
  }
  public void mousePressed(MouseEvent e) {
    mousePressed = true;
  }
  public void mouseReleased(MouseEvent e) {
  }
  public void mouseEntered(MouseEvent e) {
  }
  public void mouseExited(MouseEvent e) {
  }
}
}
