package Indent.IndentFiles;

import java.awt.*;
import java.awt.event.*;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import util.*;

public class GRNVerificationListModel extends DefaultTableModel
{
     String ColumnName[] ={"S.No","Item Code","Item Name","GRN No","GRN Date","GRN Qty","Unit","MRSUser","Select"};
     String ColumnType[] ={"S"   ,"S"        ,"S"        ,"S"     ,"S"       ,"S"      ,"S"   ,"S"      ,"B"     };

     Vector theVector;
     Common common ;
     public GRNVerificationListModel()
     {
          common         = new Common();
          setDataVector(getRowData(),ColumnName);
     }
     public Object[][] getRowData()
     {
          Object RowData[][] = new Object[0][ColumnName.length];

          return RowData;
     }
     public boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="B" || ColumnType[col]=="E")
               return true;
          return false;
     }

     public Class getColumnClass(int col)
     { 
         return getValueAt(0,col).getClass(); 
     }

     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
          }

          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }
     public int getRows()
     {
         return super.dataVector.size(); 
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

}

