package Indent.IndentFiles;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class GRNVerifiedListFrame extends JInternalFrame
{
    JPanel           pnlTop,pnlMiddle,pnlBottom;
    AttDateField     txtFromDate,txtToDate,txtAsOnDate;
    JRadioButton     btnAsOnDate;
    JButton          btnApply,btnClose;
    JTable           theTable = null;
    JLayeredPane     Layer;
    GRNVerifiedListModel theModel = null;
    ArrayList            theGrnVerifiedList = null;
    Common               common ; 
    public GRNVerifiedListFrame(JLayeredPane Layer)
    {
       this . Layer = Layer;
       common = new Common();
       createComponents();
       setLayouts();
       addComponents();
       addListeners();
    }
    private void createComponents()
    {
        pnlTop    = new JPanel();
        pnlMiddle = new JPanel();
        pnlBottom = new JPanel();
        
        txtFromDate = new AttDateField();
        txtToDate   = new AttDateField();
        txtAsOnDate = new AttDateField();
        txtAsOnDate . setTodayDate();
        
        btnAsOnDate = new JRadioButton("As On Date");
        btnAsOnDate . setSelected(true);
        txtFromDate . setEditable(false);
        txtToDate   . setEditable(false);
        
        
        btnApply    = new JButton("Apply");
        btnClose    = new JButton("Close");
        
        theModel    = new GRNVerifiedListModel();
        theTable    = new JTable(theModel);
        
    }
    private void setLayouts()
    {
        pnlTop    . setLayout(new GridLayout(5,2));
        pnlMiddle . setLayout(new BorderLayout());
        pnlBottom . setLayout(new FlowLayout(FlowLayout.CENTER));
        
        pnlTop    . setBorder(new TitledBorder("Title"));
        pnlMiddle . setBorder(new TitledBorder("Verified List"));
        pnlBottom . setBorder(new TitledBorder("Controls"));
        
        this . setSize(600,400);
        this . setTitle("Grn Verified List");
        this . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this . setMaximizable(true);
        this . setClosable(true);

    }
    private void addComponents()
    {
        pnlTop .  add(new JLabel(""));
        pnlTop .  add(btnAsOnDate);
        pnlTop .  add(new JLabel(" As On Date"));
        pnlTop .  add(txtAsOnDate);
        pnlTop .  add(new JLabel(" From Date"));
        pnlTop .  add(txtFromDate);
        pnlTop .  add(new JLabel(" To Date"));
        pnlTop .  add(txtToDate);
        pnlTop .  add(new JLabel(""));
        pnlTop .  add(btnApply);
        
        pnlMiddle . add(new JScrollPane(theTable));
        
        pnlBottom . add(btnClose);
        
        this . add(pnlTop,BorderLayout.NORTH);
        this . add(pnlMiddle,BorderLayout.CENTER);
        this . add(pnlBottom,BorderLayout.SOUTH);
    }
    private void addListeners()
    {
        btnApply    . addActionListener(new ActList());
        btnClose    . addActionListener(new ActList());
        btnAsOnDate . addItemListener(new ItemList());
    }
    private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==btnApply)
            {
                setTableData();
            }
            if(ae.getSource()==btnClose)
            {
                dispose();
            }
        }
    }
    private class ItemList implements ItemListener
    {
        public void itemStateChanged(ItemEvent ie)
        {
            if(ie.getSource() == btnAsOnDate)
            {
                if(btnAsOnDate.isSelected())
                {
                    txtAsOnDate . setEditable(true);
                    txtFromDate . setEditable(false);
                    txtToDate   . setEditable(false);
                }
                else 
                {
                    txtAsOnDate . setEditable(false);
                    txtFromDate . setEditable(true);
                    txtToDate   . setEditable(true);
                }
            }
        }
    }
    private void setTableData()
    {
        theModel . setNumRows(0);
        setDataVector();
        for(int i=0;i<theGrnVerifiedList.size();i++)
        {
            HashMap theMap = (HashMap)theGrnVerifiedList.get(i);
            Vector theVect = new Vector();
            theVect . addElement(String.valueOf(i+1));
            theVect . addElement((String)theMap.get("UnitName"));
            theVect . addElement((String)theMap.get("GRNNo"));
            theVect . addElement(common.parseDate((String)theMap.get("GRNDate")));
            theVect . addElement((String)theMap.get("GrnQty"));
            theVect . addElement((String)theMap.get("ItemCode"));
            theVect . addElement((String)theMap.get("ItemName"));
            theVect . addElement((String)theMap.get("EmpName"));
            theVect . addElement((String)theMap.get("VerifiedDate"));
            theModel . appendRow(theVect);
        }
    }
    private void setDataVector()
    {
        String SAsOnDate = txtAsOnDate.toNormal();
        String SFromDate = txtFromDate.toNormal();
        String SToDate   = txtToDate.toNormal(); 
        theGrnVerifiedList = new ArrayList();
        StringBuffer sb = new StringBuffer();
        sb.append(" select GRNNo,GRNDate,ItemCode,Item_Name,EmpName,Unit_Name,EntryDateTime,GrnQty from (");
        sb.append(" select GrnInspectionDetails.GRNNo,GrnInspectionDetails.GrnDate,GrnInspectionDetails.ItemCode,");
        sb.append(" InvItems.Item_Name,Staff.EmpName,Unit.Unit_Name,GrnInspectionDetails.EntryDateTime,GrnInspectiondetails.GrnQty from GrnInspectionDetails");
        sb.append(" Inner join InvItems on InvITems.Item_Code = GrnInspectionDetails.ItemCode");
        sb.append(" Inner join Staff on Staff.EmpCode = GrnInspectionDetails.EmpCode");
        sb.append(" Inner join Unit on Unit.Unit_Code = GrnInspectionDetails.UnitCode");
        if(btnAsOnDate.isSelected())
            sb.append(" where to_Number(to_Char(to_Date(GrnInspectionDetails.EntryDateTime),'yyyymmdd'),99999999)="+SAsOnDate);
        else
        {
            sb.append(" where to_Number(to_Char(to_Date(GrnInspectionDetails.EntryDateTime),'yyyymmdd'),99999999)>="+SFromDate);
            sb.append(" and to_Number(to_Char(to_Date(GrnInspectionDetails.EntryDateTime),'yyyymmdd'),99999999)<="+SToDate);
        }
        sb.append(" Union All");
        sb.append(" select GrnInspectionDetails.GRNNo,GrnInspectionDetails.GrnDate,GrnInspectionDetails.ItemCode,");
        sb.append(" InvItems.Item_Name,SchemeApprentice.EmpName,Unit.Unit_Name,GrnInspectionDetails.EntryDateTime,GrnInspectiondetails.GrnQty from GrnInspectionDetails");
        sb.append(" Inner join InvItems on InvITems.Item_Code = GrnInspectionDetails.ItemCode");
        sb.append(" Inner join SchemeApprentice on SchemeApprentice.EmpCode = GrnInspectionDetails.EmpCode");
        sb.append(" Inner join Unit on Unit.Unit_Code = GrnInspectionDetails.UnitCode");        
        if(btnAsOnDate.isSelected())
            sb.append(" where to_Number(to_Char(to_Date(GrnInspectionDetails.EntryDateTime),'yyyymmdd'),99999999)="+SAsOnDate);
        else
        {
            sb.append(" where to_Number(to_Char(to_Date(GrnInspectionDetails.EntryDateTime),'yyyymmdd'),99999999)>="+SFromDate);
            sb.append(" and to_Number(to_Char(to_Date(GrnInspectionDetails.EntryDateTime),'yyyymmdd'),99999999)<="+SToDate);
        }
        sb.append(" Union All");
        sb.append(" select GrnInspectionDetails.GRNNo,GrnInspectionDetails.GrnDate,GrnInspectionDetails.ItemCode,");
        sb.append(" InvItems.Item_Name,ContractApprentice.EmpName,Unit.Unit_Name,GrnInspectionDetails.EntryDateTime,GrnInspectionDetails.GrnQty from GrnInspectionDetails");
        sb.append(" Inner join InvItems on InvITems.Item_Code = GrnInspectionDetails.ItemCode");
        sb.append(" Inner join ContractApprentice on ContractApprentice.EmpCode = GrnInspectionDetails.EmpCode");
        sb.append(" Inner join Unit on Unit.Unit_Code = GrnInspectionDetails.UnitCode");          
        if(btnAsOnDate.isSelected())
            sb.append(" where to_Number(to_Char(to_Date(GrnInspectionDetails.EntryDateTime),'yyyymmdd'),99999999)="+SAsOnDate);
        else
        {
            sb.append(" where to_Number(to_Char(to_Date(GrnInspectionDetails.EntryDateTime),'yyyymmdd'),99999999)>="+SFromDate);
            sb.append(" and to_Number(to_Char(to_Date(GrnInspectionDetails.EntryDateTime),'yyyymmdd'),99999999)<="+SToDate);
        }
        sb.append(" Union All");
        sb.append(" select GrnInspectionDetails.GRNNo,GrnInspectionDetails.GrnDate,GrnInspectionDetails.ItemCode,");
        sb.append(" InvItems.Item_Name,ReliveEmp.EmpName,Unit.Unit_Name,GrnInspectionDetails.EntryDateTime,GrnInspectionDetails.GrnQty from GrnInspectionDetails");
        sb.append(" Inner join InvItems on InvITems.Item_Code = GrnInspectionDetails.ItemCode");
        sb.append(" Inner join ReliveEmp on ReliveEmp.EmpCode = GrnInspectionDetails.EmpCode");
        sb.append(" Inner join Unit on Unit.Unit_Code = GrnInspectionDetails.UnitCode");          
        if(btnAsOnDate.isSelected())
            sb.append(" where to_Number(to_Char(to_Date(GrnInspectionDetails.EntryDateTime),'yyyymmdd'),99999999)="+SAsOnDate);
        else
        {
            sb.append(" where to_Number(to_Char(to_Date(GrnInspectionDetails.EntryDateTime),'yyyymmdd'),99999999)>="+SFromDate);
            sb.append(" and to_Number(to_Char(to_Date(GrnInspectionDetails.EntryDateTime),'yyyymmdd'),99999999)<="+SToDate);
        }
        sb.append(" ) order by Unit_Name,to_Number(to_Char(to_Date(EntryDateTime),'yyyymmdd'),99999999)");
        try
        {
            ORAConnection connect     = ORAConnection . getORAConnection();
            Connection theConnection  = connect.getConnection();
            
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet         theResult    = theStatement.executeQuery();
            
            while(theResult.next())
            {
                HashMap theMap = new HashMap();
                theMap . put("GRNNo",theResult.getString(1));
                theMap . put("GRNDate",theResult.getString(2));
                theMap . put("ItemCode",theResult.getString(3));
                theMap . put("ItemName",theResult.getString(4));
                theMap . put("EmpName",theResult.getString(5));
                theMap . put("UnitName",theResult.getString(6));
                theMap . put("VerifiedDate",theResult.getString(7));
                theMap . put("GrnQty",theResult.getString(8));                
                theGrnVerifiedList . add(theMap);
            }
        }
        catch(Exception ex)
        {
            System.out.println(" setData Vector "+ex);
        }

    }
   
}
