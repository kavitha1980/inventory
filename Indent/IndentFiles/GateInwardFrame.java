
package Indent.IndentFiles;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import util.*;
public class GateInwardFrame extends JDialog
{
    JPanel   pnlTop,pnlMiddle,pnlBottom;
    JButton  btnOK;
    JTable   theTable =null;
    PrevGateInwardModel theModel = null;
    Common   common = new Common();
    Vector   VGINo,VGIDate,VRecQty,VReceivedBy;
    String   SDescription;
    int      iRDCNo;
    public GateInwardFrame(int iRDCNo,String SDescription,Vector VGINo,Vector VGIDate,Vector VRecQty,Vector VReceivedBy)
    {
        this . iRDCNo       = iRDCNo;
        this . SDescription = SDescription;
        this . VGINo  = VGINo;
        this . VGIDate= VGIDate;
        this . VRecQty= VRecQty; 
        this . VReceivedBy = VReceivedBy;
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
        setTableData();
    }
    private void createComponents()
    {
        pnlTop    = new JPanel();
        pnlMiddle = new JPanel();
        pnlBottom = new JPanel();
        btnOK     = new JButton("OK");
        
        theModel = new PrevGateInwardModel();
        theTable = new JTable(theModel);
    }
    private void setLayouts()
    {
        pnlTop    . setLayout(new FlowLayout(FlowLayout.CENTER));
        pnlMiddle . setLayout(new BorderLayout()) ;
        pnlBottom . setLayout(new FlowLayout(FlowLayout.CENTER));
        
        this . setLayout(new BorderLayout());
        this . setSize(600,600);
        this . setModal(true);
        this . setTitle(" PREVIOUS GATE INWARDS FOR RDCNO - "+iRDCNo+" ( "+SDescription+")");
    }
    private void addComponents()
    {
        pnlTop    .  add(new JLabel(" PREVIOUS GATE INWARD FOR RDC "));
        pnlMiddle .  add(new JScrollPane(theTable));
        pnlBottom .  add(btnOK);
        
        this . add(pnlTop,BorderLayout.NORTH);
        this . add(pnlMiddle,BorderLayout.CENTER);
        this . add(pnlBottom,BorderLayout.SOUTH);
    }
    private void addListeners()
    {
        btnOK . addActionListener(new ActList());
    }
    private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==btnOK)
            {
                dispose();
            }
        }
    }
    private void setTableData()
    {
        theModel.setNumRows(0);
        for(int i=0;i<VGINo.size();i++)
        {
             Vector theVect = new Vector();
             theVect . addElement(String.valueOf(i+1));
             theVect . addElement(VGINo.elementAt(i));
             theVect . addElement(common.parseDate((String)VGIDate.elementAt(i)));
             theVect . addElement(VRecQty.elementAt(i));
             theVect . addElement(VReceivedBy.elementAt(i));
             
             theModel.appendRow(theVect);
        }
    }
}
