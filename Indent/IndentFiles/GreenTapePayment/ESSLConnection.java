package Indent.IndentFiles.GreenTapePayment;

import java.sql.*;

class ESSLConnection
{
	static ESSLConnection connect = null;
	Connection theConnection = null;

	private ESSLConnection()
	{
		try
		{
		  Class.forName("oracle.jdbc.OracleDriver");
			theConnection = 
                   //DriverManager.getConnection("jdbc:oracle:thin:@192.168.252.39:1521:amardye2","essl","essl");
                                DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","essl","essl");
                  
		}
		catch(Exception e)
		{
			System.out.println("ESSLConnection : "+e);
		}
	}

	public static ESSLConnection getORAConnection()
	{
		if (connect == null)
		{
			connect = new ESSLConnection();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}

