
package Indent.IndentFiles.GreenTapePayment;
import java.util.*;
public class GreenTapeIssueClass 
{
    String SContractorName,SContractorCode,SItemCode,SContractorEmpCode;
    ArrayList theIssueList;
   public GreenTapeIssueClass(String SContractorName,String SContractorCode,String SItemCode,String SContractorEmpCode)
   {
       this . SContractorName = SContractorName;
       this . SContractorCode = SContractorCode;
       this . SItemCode       = SItemCode;
       this . SContractorEmpCode = SContractorEmpCode;
       theIssueList = new ArrayList();
   }
   public void appendIssueDetails(String SReceiveId,String SStockQty,String SIssuedQty,String SBalanceQty,String SItemCode,String SReceivedDate)
   {
       HashMap theMap = new HashMap();
       theMap . put("ReceiveId",SReceiveId);
       theMap . put("StockQty",SStockQty);
       theMap . put("IssuedQty",SIssuedQty);
       theMap . put("BalanceQty",SBalanceQty);
       theMap . put("ItemCode",SItemCode);
       theMap . put("ReceiveDate",SReceivedDate);
       theIssueList.add(theMap);
       
   }
}
