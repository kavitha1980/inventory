
package Indent.IndentFiles.GreenTapePayment;
import javax.swing.table.*;
import java.util.*;
public class GreenTapeIssueDetModel extends DefaultTableModel
{
    String ColumnName[] ={"S.No","IssueDate","Unit","Department","IssueQty"};
    String  ColumnType[]={"N","S","S","S","N"};
    int iColumnWidth[]  ={30,30,50,50,30};
    public GreenTapeIssueDetModel()
    {
       setDataVector(getRowData(),ColumnName);
    }     
     public Class getColumnClass(int iCol)
     {
       return getValueAt(0,iCol).getClass();
     }
    public boolean isCellEditable(int iRow,int iCol)
    {
        if (ColumnType[iCol]=="E"||ColumnType[iCol]=="B")
            return true;
            return false;
    }
    
    private Object[][]getRowData()
    {
        Object RowData[][]=new Object[1][ColumnName.length];
        
        for(int i=0;i<ColumnName.length;i++)
            RowData[0][i]="";
        return RowData;
    }
    
    public void appendRow(Vector theVect)
    {
        insertRow(getRows(),theVect);
    }
    public int getRows()
    {
        return super.dataVector.size();
    }

}
