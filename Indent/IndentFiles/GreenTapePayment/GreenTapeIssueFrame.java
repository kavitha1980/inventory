package Indent.IndentFiles.GreenTapePayment;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.sql.*;
import java.net.InetAddress;
import javax.swing.table.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class GreenTapeIssueFrame extends JInternalFrame
{
   
    JPanel    pnlTop,pnlMiddle,pnlBottom;
    JButton   btnSave,btnExit,btnApply;
    JTable    theTable=null;
    JLayeredPane Layer;
    JComboBox    cmbUnit;
    GreenTapeIssueModel theModel = null;
    ArrayList theIndentList=null,theIssueList=null; 
    AttDateField txtIssueDate;
    Common    common;
    String    SSystemName,SIssueDate="";
    int       iUnitCode,iDeptCode,iHodCode,iMillCode,iUserCode;
    Vector    VDeptName,VDeptCode,VUnitName,VUnitCode; 
    Vector    VReceivedId,VIssueQty,VIssueStatus;
    // set Model Index
    
    int SNo=0,Contractor=1,Unit=2,Department=3,StockQty=4,IssuedQty=5,IssueQty=6,BalanceQty=7,Select=8;

    public GreenTapeIssueFrame(JLayeredPane Layer,int iUserCode,int iHodCode,int iMillCode)
    {
        this . Layer = Layer;
        this . iUserCode = iUserCode;
        this . iHodCode  = iHodCode;
        this . iMillCode = iMillCode;
        common = new Common();
        setVector();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
        
        try
        {
            SSystemName = InetAddress.getLocalHost().getHostName();
        }
        catch(Exception ex)
        {
            System.out.println(" Systemname "+ex);
        }
    }
    private void createComponents()
    {
       pnlTop    = new JPanel();
       pnlMiddle = new JPanel();
       pnlBottom = new JPanel();
       
       btnSave   = new JButton(" SAVE ");
       btnExit   = new JButton(" EXIT ");
       btnApply  = new JButton(" Apply "); 

       txtIssueDate = new AttDateField(10);    
       txtIssueDate . fromString(common.parseDate(common.getServerPureDate()));
          

       theModel  = new GreenTapeIssueModel();
       theTable  = new JTable(theModel);
       
        
        cmbUnit       = new JComboBox(VUnitName);       

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        theTable.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
        theTable.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
        theTable.getColumnModel().getColumn(6).setCellRenderer(rightRenderer);
        theTable.getColumnModel().getColumn(7).setCellRenderer(rightRenderer);
        
        TableColumn unitColumn  = theTable.getColumn("UNIT");
        unitColumn              . setCellEditor(new DefaultCellEditor(cmbUnit));

    }
    private void setLayouts()
    {
        //pnlTop    . setLayout(new FlowLayout(FlowLayout.CENTER));

        pnlTop    . setLayout(new GridLayout(1,4,3,3));
        pnlMiddle . setLayout(new BorderLayout()) ;
        pnlBottom . setLayout(new FlowLayout(FlowLayout.CENTER));

        pnlTop    . setBorder(new TitledBorder("Title"));
        pnlMiddle . setBorder(new TitledBorder("Data"));
        pnlBottom . setBorder(new TitledBorder("Controls"));
        
        this      . setSize(800,600);
        this      . setTitle(" Green Tape Issue ");
        this      . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this      . setMaximizable(true);
        this      . setClosable(true);
        this      . setResizable(true);
    }
    private void addComponents()
    {
//        pnlTop    . add(new JLabel(" GREEN TAPE ISSUE FRAME "));
         
        pnlTop    . add(new JLabel("Issue Date"));
        pnlTop    . add(txtIssueDate);
        pnlTop    . add(new JLabel(""));
        pnlTop    . add(btnApply);


        pnlMiddle . add(new JScrollPane(theTable));
        
        pnlBottom . add(btnSave);
        pnlBottom . add(btnExit);
        //pnlBottom . add(new JLabel("Press F3 in Unit or Department -->For Unit,Department"));

        this      . add(pnlTop,BorderLayout.NORTH);
        this      . add(pnlMiddle,BorderLayout.CENTER);
        this      . add(pnlBottom,BorderLayout.SOUTH);
    }
    private void addListeners()
    {
        btnSave  . addActionListener(new ActList());
        btnExit  . addActionListener(new ActList());
        btnApply . addActionListener(new ActList());
        theTable . addKeyListener(new TableKeyList());
        txtIssueDate.theDate. addKeyListener(new KeyList());
//        theTable . addMouseListener(new MouseList());


    }    
     private void setTableData()
    {
        setIssueList();
        theModel.setNumRows(0);
       
        for(int i=0;i<theIssueList.size();i++)
        {
            GreenTapeIssueClass theClass =(GreenTapeIssueClass)theIssueList.get(i);
            Vector theVect      = new Vector();
            double dStockQty  = getStock(theClass.SContractorCode);    
            double dIssuedQty = getIssuedQty(theClass.SContractorCode);    
            double dBalanceQty= dStockQty-dIssuedQty;   
            theVect . addElement(String.valueOf(i+1));
            theVect . addElement(theClass.SContractorName);
            theVect . addElement(String.valueOf(""));
            //theVect . addElement("");
            theVect . addElement(VDeptName.get(0));
            theVect . addElement(common.getRound(dStockQty,0));
            theVect . addElement(common.getRound(dIssuedQty,0));
            theVect . addElement("");
            theVect . addElement(String.valueOf(dBalanceQty));
            theVect . addElement(new Boolean(false));
            theModel. appendRow(theVect);
        }
    }
    private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
           if(ae.getSource()==btnApply)
           {
              SIssueDate = txtIssueDate.toNormal();
              setTableData();
           }
          
           if(ae.getSource()==btnSave)  
           {
              if(checkValidData())
              {
               insertIssue();   
               JOptionPane.showMessageDialog(null,"Data Saved Sucessfully ","Info",JOptionPane.INFORMATION_MESSAGE);
               setTableData();
              }
           }
           if(ae.getSource()==btnExit)
           {
               dispose();
           }
        }
    }
    private class TableKeyList extends KeyAdapter
    {
       public void keyReleased(KeyEvent ke)        
       {
           if(ke.getKeyCode()==KeyEvent.VK_TAB)
           {
               int iRow = theTable.getSelectedRow();
               double dStockQty  = common.toDouble((String)theModel.getValueAt(iRow,StockQty));
               double dIssuedQty = common.toDouble((String)theModel.getValueAt(iRow,IssuedQty));
               double dIssueQty  = common.toDouble((String)theModel.getValueAt(iRow,IssueQty));
               double dBalanceQty = dStockQty - (dIssuedQty+dIssueQty);
               theModel . setValueAt(String.valueOf(dBalanceQty),iRow,BalanceQty);
           }
          if(ke.getSource()==theTable)
          {
            int iCol = theTable.getSelectedColumn();
            if(iCol==IssueQty)
            {
               int iRow = theTable.getSelectedRow();
               double dStockQty  = common.toDouble((String)theModel.getValueAt(iRow,StockQty));
               double dIssuedQty = common.toDouble((String)theModel.getValueAt(iRow,IssuedQty));
               double dIssueQty  = common.toDouble((String)theModel.getValueAt(iRow,IssueQty));
               double dBalanceQty = dStockQty - (dIssuedQty+dIssueQty);
               theModel . setValueAt(String.valueOf(dBalanceQty),iRow,BalanceQty);

            }
          }
       }
    }
    private class KeyList extends KeyAdapter
    {
       public void keyReleased(KeyEvent ke)
       {
         // if(ke.getSource()==txtIssueDate)
          //{
            
            SIssueDate = txtIssueDate.toNormal();
            if(SIssueDate.length()==8)
              setTableData();

          //}
          
       }
    }
    private class MouseList extends MouseAdapter
    {
       public void mouseExited(MouseEvent me)
       { 
          
       }
    }
      
    private void setIssueList()
    {
        GreenTapeIssueClass theIssueClass=null;
        theIssueList = new ArrayList();
        StringBuffer sb = new StringBuffer();
        
        sb.append(" Select GreenTapeReceived.Id,GreenTapeReceived.NoOfBundles,decode(GreenTapeReceived.IssuedBundles,'',0,GreenTapeReceived.IssuedBundles),GreenTapeReceived.ContractorCode,GreenTapeContractor.ContractorName,");
        sb.append(" GreenTapeReceived.ItemCode,GreenTapeReceived.ReceivedDate,GreenTapeReceived.ContractorEmpCode from GreenTapeReceived  ");
        sb.append(" Inner join GreenTapeContractor on GreenTapeContractor.ContractorCode=GreenTapeReceived.ContractorCode ");
        sb.append(" and GreenTapeContractor.ContractorEmpCode = GreenTapeReceived.ContractorEmpCode");
        sb.append(" where (GreenTapeReceived.IssueStatus=0 or GreenTapeReceived.IssueStatus is null) and GreenTapeReceived.ReceivedDate<="+SIssueDate+" order by GreenTapeReceived.ReceivedDate,GreenTapeReceived.Id");
        // System.out.println(" setIssue List "+sb.toString());
        try
        {
            ORAConnection connect    = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();
            
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet         theResult    = theStatement.executeQuery();
            while(theResult.next())
            {
                
                String SReceiveId = theResult.getString(1);
                String SStockQty  = theResult.getString(2);
                String SIssuedQty = theResult.getString(3);
                String SContractorCode = theResult.getString(4);
                String SContractorName = theResult.getString(5);
                String SItemCode       = theResult.getString(6);
                String SReceivedDate   = theResult.getString(7);
                double dStockQty       = theResult.getDouble(2);
                double dIssuedQty      = theResult.getDouble(3);
                String SBalanceQty     = common.getRound(dStockQty-dIssuedQty,0);
                String SContractorEmpCode = theResult.getString(8);
                
                int iIndex=getIndexOf(SContractorName,SContractorCode);
                if(iIndex==-1)
                {
                   theIssueClass = new GreenTapeIssueClass(SContractorName,SContractorCode,SItemCode,SContractorEmpCode);
                   theIssueList.add(theIssueClass);
                   iIndex=theIssueList.size()-1;
                }
                theIssueClass = (GreenTapeIssueClass)theIssueList.get(iIndex);
                theIssueClass . appendIssueDetails(SReceiveId,SStockQty, SIssuedQty,SBalanceQty,SItemCode,SReceivedDate);

             
            }
        }
        catch(Exception ex)
        {
            System.out.println(" setIssueList "+ex);
        }
    }
    
    public int getIndexOf(String SContractorName,String SContractorCode){
     int iIndex=-1;
     for(int i=0;i<theIssueList.size();i++){
         GreenTapeIssueClass theIssueClass=(GreenTapeIssueClass)theIssueList.get(i);
         if(theIssueClass.SContractorName.equals(SContractorName)&&(theIssueClass.SContractorCode.equals(SContractorCode))){
             iIndex=i;
             break;
         }
     }
     return iIndex;
 }
 private double getStock(String SContractorCode)
    {
       double dStock = 0.0;
       for(int i=0;i<theIssueList.size();i++)
       {
           GreenTapeIssueClass theClass = (GreenTapeIssueClass)theIssueList.get(i);
           if(theClass.SContractorCode.equals(SContractorCode))
           {
               for(int j=0;j<theClass.theIssueList.size();j++)
               {
                   HashMap theMap = (HashMap)theClass.theIssueList.get(j);
                   dStock = dStock+common.toDouble((String)theMap.get("StockQty"));
               }
           }
       }
       return dStock;
    }
    private double getIssuedQty(String SContractorCode)
    {
       double dIssuedQty = 0.0;
       for(int i=0;i<theIssueList.size();i++)
       {
           GreenTapeIssueClass theClass = (GreenTapeIssueClass)theIssueList.get(i);
           if(theClass.SContractorCode.equals(SContractorCode))
           {
               for(int j=0;j<theClass.theIssueList.size();j++)
               {
                   HashMap theMap = (HashMap)theClass.theIssueList.get(j);
                   dIssuedQty = dIssuedQty+common.toDouble((String)theMap.get("IssuedQty"));
               }
           }
       }
       return dIssuedQty;
    }
    private void insertIssue()
    {
        
        ORAConnection connect         = ORAConnection.getORAConnection();
        Connection    theConnection   = connect.getConnection();
      
        for(int i=0;i<theModel.getRowCount();i++)
        {
         double dIssueQty  = common.toDouble((String)theModel.getValueAt(i,IssueQty));
         GreenTapeIssueClass theClass = (GreenTapeIssueClass)theIssueList.get(i);
         int iContractorCode    = common.toInt(theClass.SContractorCode);
         int iContractorEmpCode = common.toInt(theClass.SContractorEmpCode);
         String SItemCode       = theClass.SItemCode;
         if(((Boolean)theModel  . getValueAt(i,Select)).booleanValue())
         {
           int    iIssueNo     = getIssueNo();

           String SDeptName    = (String)theModel.getValueAt(i, Department);
           String SUnitName    = (String)theModel.getValueAt(i, Unit);
           int    iDepartmentCode = getDeptCode(SDeptName);
           int    iUnitcode       = getUnitCode(SUnitName);

           StringBuffer sb = new StringBuffer();
           sb.append(" Insert into GreenTapeIssue(ID,ISSUENO,ISSUEDATE,ISSUEQTY,ITEMCODE,");
           sb.append(" SystemName,ContractorCode,UnitCode,Dept_Code,ContractorEmpCode)");
           sb.append(" values(GreenTapeIssue_Seq.NextVal,?,?,?,?,?,?,?,?,?)");
        try
        {
           
           if(theConnection.getAutoCommit())
             theConnection.setAutoCommit(false);
           
            PreparedStatement theStatement= theConnection.prepareStatement(sb.toString());
            theStatement . setInt(1,iIssueNo);
            theStatement . setString(2,SIssueDate);
            theStatement . setDouble(3,dIssueQty);
            theStatement . setString(4,SItemCode);
            theStatement . setString(5,SSystemName);
            theStatement . setInt(6,iContractorCode);
            theStatement . setInt(7,iUnitcode);
            theStatement . setInt(8,iDepartmentCode);
            theStatement . setInt(9,iContractorEmpCode);
            theStatement . executeUpdate();

            getReceivedIdVector(dIssueQty,iContractorCode);
            for(int k=0;k<VReceivedId.size();k++)
            {
            StringBuffer sb1 = new StringBuffer();
            sb1.append(" Update GreenTapeReceived set IssuedBundles=decode(IssuedBundles,'',0,IssuedBundles)+?,IssueDate=?");
            sb1.append(" ,IssueStatus=?");
            sb1.append(" where ID=?");
         
            theStatement= theConnection.prepareStatement(sb1.toString());
            //theStatement . setDouble(1,dIssueQty);
            //theStatement . setString(2,SIssueDate);
            //theStatement . setInt(3,iReceiveId);
            theStatement . setDouble(1,common.toDouble(String.valueOf(VIssueQty.elementAt(k))));
            theStatement . setString(2,SIssueDate);
            theStatement . setInt(3,common.toInt(String.valueOf(VIssueStatus.elementAt(k))));
            theStatement . setInt(4,common.toInt(String.valueOf(VReceivedId.elementAt(k))));

            theStatement . executeUpdate();
            theStatement .close();
            theConnection.commit();
            theConnection.setAutoCommit(true);
            }
         }
        catch(Exception ex)
        {
            try
            {
              theConnection.rollback();
            }
            catch(Exception e)
            {
                System.out.println(" Rollback");
            }
            System.out.println(" insert Issue "+ex);
            ex.printStackTrace();
        }
       }
     }
    }
    private void getReceivedIdVector(double dIssueQty,int iContractorCode)
    {
        VReceivedId  = new Vector();
        VIssueQty    = new Vector();
        VIssueStatus = new Vector();
        String SContCode = String.valueOf(iContractorCode);
        for(int i=0;i<theIssueList.size();i++)
        {
            GreenTapeIssueClass theClass = (GreenTapeIssueClass)theIssueList.get(i);
            if(theClass.SContractorCode.equals(SContCode))
            {
             for(int j=0;j<theClass.theIssueList.size();j++)
             {
               HashMap theMap = (HashMap)theClass.theIssueList.get(j);
               String SReceivedId = (String)theMap.get("ReceiveId");
               double dBalanceQty = common.toDouble((String)theMap.get("BalanceQty"));
               if(dIssueQty==dBalanceQty || dIssueQty>dBalanceQty)
               {
                 VReceivedId.addElement(SReceivedId);  
                 VIssueStatus.addElement(1);
                 VIssueQty. addElement(dBalanceQty);
                 dIssueQty = dIssueQty-dBalanceQty;
                 if(dIssueQty==0)
                     break;
               }
               else if(dIssueQty<dBalanceQty)
               {
                   VReceivedId.addElement(SReceivedId);
                   VIssueStatus.addElement(0);
                   VIssueQty .addElement(dIssueQty);
                   break;
               }
               
             }
            }
        }
    }

   /* private void insertIssue()
    {
        
        ORAConnection connect         = ORAConnection.getORAConnection();
        Connection    theConnection   = connect.getConnection();
      
        for(int i=0;i<theModel.getRowCount();i++)
        {
         double dStockQty  = common.toDouble((String)theModel.getValueAt(i,StockQty));
         double dIssuedQty = common.toDouble((String)theModel.getValueAt(i,IssuedQty));
         double dIssueQty  = common.toDouble((String)theModel.getValueAt(i,IssueQty));
         int    iReceiveId = common.toInt((String)theModel.getValueAt(i,ReceiveId));
         
         HashMap theMap = (HashMap)theIssueList.get(i);
         int    iContractorCode = common.toInt((String)theMap.get("ContractorCode"));
         String SItemCode       = (String)theMap.get("ItemCode");
         if(((Boolean)theModel  . getValueAt(i,Select)).booleanValue())
         {
           int    iIssueNo     = getIssueNo();

           String SDeptName    = (String)theModel.getValueAt(i, Department);
           String SUnitName    = (String)theModel.getValueAt(i, Unit);
           int    iDepartmentCode = getDeptCode(SDeptName);
           int    iUnitcode       = getUnitCode(SUnitName);

           StringBuffer sb = new StringBuffer();
           sb.append(" Insert into GreenTapeIssue(ID,ISSUENO,ISSUEDATE,ISSUEQTY,ITEMCODE,");
           sb.append(" SystemName,ContractorCode,UnitCode,Dept_Code)");
           sb.append(" values(GreenTapeIssue_Seq.NextVal,?,?,?,?,?,?,?,?)");
        try
        {
           
           if(theConnection.getAutoCommit())
             theConnection.setAutoCommit(false);
           
            PreparedStatement theStatement= theConnection.prepareStatement(sb.toString());
            theStatement . setInt(1,iIssueNo);
            theStatement . setString(2,SIssueDate);
            theStatement . setDouble(3,dIssueQty);
            theStatement . setString(4,SItemCode);
            theStatement . setString(5,SSystemName);
            theStatement . setInt(6,iContractorCode);
            theStatement . setInt(7,iUnitcode);
            theStatement . setInt(8,iDepartmentCode);
            theStatement . executeUpdate();

            
            StringBuffer sb1 = new StringBuffer();
            sb1.append(" Update GreenTapeReceived set IssuedBundles=decode(IssuedBundles,'',0,IssuedBundles)+?,IssueDate=?");
            if(dStockQty==(dIssuedQty+dIssueQty))
                sb1.append(" ,IssueStatus=1");
            sb1.append(" where ID=?");
         
            theStatement= theConnection.prepareStatement(sb1.toString());
            theStatement . setDouble(1,dIssueQty);
            theStatement . setString(2,SIssueDate);
            theStatement . setInt(3,iReceiveId);

            theStatement . executeUpdate();
            theStatement .close();
            theConnection.commit();
            theConnection.setAutoCommit(true);
            
         }
        catch(Exception ex)
        {
            try
            {
              theConnection.rollback();
            }
            catch(Exception e)
            {
                System.out.println(" Rollback");
            }
            System.out.println(" insert Issue "+ex);
            ex.printStackTrace();
        }
       }
     }
    }*/
    private int getIssueNo()
    {
        int iIssueNo=0;
        StringBuffer sb = new StringBuffer();
        sb.append(" select Max(IssueNo) from GreenTapeIssue ");
        try
        {
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet         theResult    = theStatement.executeQuery();
            while(theResult.next())
            {
                iIssueNo = theResult.getInt(1)+1;
            }
        }
        catch(Exception e)
        {
            System.out.println(" getIssue No "+e);
        }
        return iIssueNo;
    }
   
    private boolean checkValidData()
    {
        int iRow = theTable.getSelectedRow();
        double dStockQty  = common.toDouble((String)theModel.getValueAt(iRow,StockQty));
        double dIssuedQty = common.toDouble((String)theModel.getValueAt(iRow,IssuedQty));
        double dIssueQty  = common.toDouble((String)theModel.getValueAt(iRow,IssueQty));

        String SDepartment = (String)theModel.getValueAt(iRow,Department);
        String SUnit       = (String)theModel.getValueAt(iRow,Unit); 

        if(dIssueQty>(dStockQty-dIssuedQty))
        {
            JOptionPane . showMessageDialog(null,"Required Qty is greater than Stock Qty","Info",JOptionPane.INFORMATION_MESSAGE);
            theModel.setValueAt("",iRow,IssueQty);
            theModel.setValueAt("",iRow,BalanceQty);
            return false;
        }
        if(SDepartment.length()<=0 || SUnit.length()<=0)
        {
           JOptionPane . showMessageDialog(null,"Unit And Department Must be Filled","Info",JOptionPane.INFORMATION_MESSAGE);
           return false;
        }
        return true;
    }
   public void setVector()  
     {
          VUnitName     = new Vector();
          VUnitCode     = new Vector();  
          VDeptName     = new Vector();
          VDeptCode     = new Vector();
        
        
          String QS1="";
          String QS2= "";
          String QS3= "";

          if(iMillCode!=1)
               QS1 = " Select Unit_Name,Unit_Code From Unit where Unit_Code<=5 Order By Unit_Name";
          else
               QS1 = "Select Unit_Name,Unit_Code From Unit where (MillCode=1 OR MillCode = 2) Order By Unit_Name";

         /* if(iMillCode!=1)
               QS2 = "Select Dept_Name,Dept_Code From Dept where (MillCode=0 OR MillCode = 2) Order By Dept_Name";
          else
               QS2 = "Select Dept_Name,Dept_Code From Dept where (MillCode=1 OR MillCode = 2) Order By Dept_Name";*/

          QS2 = "Select Dept_Name,Dept_Code From Dept where Dept_Code=50 Order By Dept_Name";



          QS3 = " Select Group_Name,Group_Code,Signal From Cata "+
                " Inner join UserCataList on UserCataList.CataGroupcode=cata.Group_Code "+
                " where UserCataList.UserCode="+iUserCode;


//              " Where MillCode=2 or MillCode="+iMillCode+" Order By 1";


          try
          {
               Connection theConnection=null;

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS1);

               while(result.next())
               {
                    VUnitName.addElement(result.getString(1));
                    VUnitCode.addElement(result.getString(2));
               }
               result.close();
               result          = theStatement.executeQuery(QS2);
               while(result.next())
               {
                    VDeptName.addElement(result.getString(1));
                    VDeptCode.addElement(result.getString(2));
               }
               result.close();

            

               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
    private String getUnitName(String SUnitCode)
     {
          int iIndex=-1;

          iIndex = VUnitCode.indexOf(SUnitCode);


          if(iIndex!=-1)
               return (String)VUnitName.elementAt(iIndex);
          else
               return "";
     }

     private int getUnitCode(String SUnitName)
     {
          int iIndex=-1;

          iIndex = VUnitName.indexOf(SUnitName);
          if(iIndex!=-1)
               return common.toInt((String)VUnitCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getDeptCode(String SDeptName)
     {
          int iIndex=-1;

          iIndex = VDeptName.indexOf(SDeptName);
          if(iIndex!=-1)
               return common.toInt((String)VDeptCode.elementAt(iIndex));
          else
               return 0;
     }
     private String getDeptName(String SDeptCode)
     {
          int iIndex=-1;

          iIndex = VDeptCode.indexOf(SDeptCode);
          if(iIndex!=-1)
               return (String)VDeptName.elementAt(iIndex);
          else
               return "";
     }
}
