package Indent.IndentFiles.GreenTapePayment;
import javax.swing.table.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;

import util.*;

public class GreenTapeIssueModel extends DefaultTableModel
{
    Common common = new Common();
    String  ColumnName[] ={"S.NO","CONTRACTOR","UNIT","DEPARTMENT","STOCK QTY","ISSUED QTY","ISSUE QTY","BALANCE QTY","SELECT"};
    String  ColumnType[] ={"N","S","E","S","N","N","E","N","E"};
    int iColumnWidth[]   ={30,30,30,30,30,30,30,30,50};

    public GreenTapeIssueModel()
    {
       setDataVector(getRowData(),ColumnName);
    }     
    public Class getColumnClass(int iCol)
    {
       return getValueAt(0,iCol).getClass();
    }
    public boolean isCellEditable(int iRow,int iCol)
    {
        if (ColumnType[iCol]=="E"||ColumnType[iCol]=="B")
            return true;
            return false;
    }
    
    private Object[][]getRowData()
    {
        Object RowData[][]=new Object[1][ColumnName.length];
        
        for(int i=0;i<ColumnName.length;i++)
            RowData[0][i]="";
        return RowData;
    }
    
    public void appendRow(Vector theVect)
    {
        insertRow(getRows(),theVect);
    }
    public int getRows()
    {
        return super.dataVector.size();
    }
    public void setValueAt(Object aValue, int row, int column)
     {
          try
          {

               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
               if(column==6)
               {
                    setTotalValue(row,column);
               }
          }
          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }
     public void setTotalValue(int iRow,int iColumn)
     {
          double dStock     = common.toDouble((String)getValueAt(iRow,4)); 
          double dIssuedQty = common.toDouble((String)getValueAt(iRow,5)); 
          double dIssueQty  =  common.toDouble((String)getValueAt(iRow,6));
           
          setValueAt(common.getRound(dStock-(dIssuedQty+dIssueQty),0),iRow,7);

     }

}
