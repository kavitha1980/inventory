
package Indent.IndentFiles.GreenTapePayment;
import javax.swing.table.*;
import java.util.*;

import util.*;
import guiutil.*;
import jdbc.*;
public class GreenTapeLedgerModel extends DefaultTableModel
{
    String ColumnName[] ={"Date","Doc.Id","Doc.No","Type","Unit","Supplier","Dept","Class","Rec Qty","Rec Value","Iss Qty","Iss Value","Stock Qty","Stock Value","Net Rate"};
    String  ColumnType[]={"S","S","S","S","S","S","S","S","N","N","N","N","N","N","N"};
//    int iColumnWidth[]  ={30,30,50};
    public GreenTapeLedgerModel()
    {
       setDataVector(getRowData(),ColumnName);
    }     
     public Class getColumnClass(int iCol)
     {
       return getValueAt(0,iCol).getClass();
     }
    public boolean isCellEditable(int iRow,int iCol)
    {
        if (ColumnType[iCol]=="E"||ColumnType[iCol]=="B")
            return true;
            return false;
    }
    
    private Object[][]getRowData()
    {
        Object RowData[][]=new Object[1][ColumnName.length];
        
        for(int i=0;i<ColumnName.length;i++)
            RowData[0][i]="";
        return RowData;
    }
    
    public void appendRow(Vector theVect)
    {
        insertRow(getRows(),theVect);
    }
    public int getRows()
    {
        return super.dataVector.size();
    }

}
