
package Indent.IndentFiles.GreenTapePayment;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;
//public class GreenTapeReceiptDetails extends JFrame
public class GreenTapeReceiptDetails extends JInternalFrame
{
    JPanel    pnlTop,pnlBottom,pnlMiddle,pnlMiddleTop,pnlMiddleBottom,pnlStock,pnlIssue,pnlStockIssue,pnlLedger,pnlLedgerBottom;
    JButton   btnApply,btnPrint,btnExit;
    JComboBox cmbContractor,cmbUnit;
    JTable    theTable = null,theIssueTable=null,theLedgerTable=null;
    JLabel    lblTotBundles,lblTotTapes,lblRatePerTape,lblPayableAmount,lblStock; 
    JLabel    lblReceipt,lblIssue,lblClosingStock,lblOpeningStock;
    JLayeredPane Layer;
    JTabbedPane tabPane;
    GreenTapeReceiptModel   theModel =null;
    GreenTapeIssueDetModel  theIssueDetModel=null;
    GreenTapeLedgerModel    theLedgerModel=null;
    AttDateField txtFromDate,txtToDate;
    String       SFromDate,SToDate,SContractor="",SStDate,SEnDate;
    int          iContractorCode,iContractorEmpCode;
    double       dRatePerTape=0.0,dOpeningQty=0.0,dOpeningValue=0.0,dTotalReceipt=0.0,dTotalIssue=0.0,dOpeningStock=0.0,dClosingStock=0.0;
    Vector       VData,VContractorCode,VContractorName,VContractorEmpCode,VIssueData,VLedgerData;
    Common       common;
    public GreenTapeReceiptDetails(JLayeredPane Layer,int iUserCode,int iAuthCode,int iMillCode,String SStDate,String SEnDate)
    {
        this . Layer = Layer;
        this . SStDate = SStDate;
        this . SEnDate = SEnDate;
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
    }
    private void createComponents()
    {
        common = new Common();
        
        pnlTop    = new JPanel();
        pnlMiddle = new JPanel();
        pnlBottom = new JPanel();
        pnlMiddleTop    = new JPanel();
        pnlMiddleBottom = new JPanel();
        pnlStock        = new JPanel();
        pnlIssue        = new JPanel();
        pnlStockIssue   = new JPanel();
        pnlLedger       = new JPanel();
        pnlLedgerBottom = new JPanel();

        
        btnApply  = new JButton("Apply");
        btnPrint  = new JButton("Print");
        btnExit   = new JButton("Exit");
        
        txtFromDate      = new AttDateField(10);
        txtToDate        = new AttDateField(10);

        lblTotBundles    = new JLabel("");
        lblTotTapes      = new JLabel("");
        lblRatePerTape   = new JLabel("");
        lblPayableAmount = new JLabel("");
        lblStock         = new JLabel("");
        lblReceipt       = new JLabel("");
        lblIssue         = new JLabel("");
        lblClosingStock  = new JLabel("");
        lblOpeningStock  = new JLabel("");        

        
        setContractor();
        cmbContractor = new JComboBox(VContractorName);

        theModel  = new GreenTapeReceiptModel();
        theTable  = new JTable(theModel);

        theIssueDetModel = new GreenTapeIssueDetModel();
        theIssueTable    = new JTable(theIssueDetModel);
        
        
        theLedgerModel   = new GreenTapeLedgerModel();
        theLedgerTable   = new JTable(theLedgerModel);

        tabPane   = new JTabbedPane();
        
        tabPane.add("Receipts",new JScrollPane(theTable));
        tabPane.add("Issue",pnlStockIssue);
        tabPane.add("Ledger",pnlLedger);
        //tabPane.add("Issues",new JScrollPane(theIssueTable));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        theTable.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
        
    }
    private void setLayouts()
    {
        pnlTop    . setLayout(new GridLayout(2,4,3,3));
        //pnlMiddle . setLayout(new BorderLayout());
        pnlMiddle . setLayout(new BorderLayout());
        pnlBottom . setLayout(new FlowLayout(FlowLayout.CENTER));
        pnlMiddleTop    . setLayout(new BorderLayout());
        pnlMiddleBottom . setLayout(new GridLayout(2,4,3,3));
        pnlStockIssue   . setLayout(new BorderLayout());
        pnlStock        . setLayout(new GridLayout(1,2,3,3));
        pnlIssue        . setLayout(new BorderLayout());
        pnlLedger       . setLayout(new BorderLayout());
        pnlLedgerBottom . setLayout(new FlowLayout(FlowLayout.LEFT));

        
        pnlTop    . setBorder(new TitledBorder("Filter"));
        pnlMiddle . setBorder(new TitledBorder("Data"));
        pnlBottom . setBorder(new TitledBorder("Controls"));
        pnlMiddleBottom . setBorder(new TitledBorder("Payment Details"));
        pnlStock        . setBorder(new TitledBorder("Stock"));
        pnlIssue        . setBorder(new TitledBorder("Issue"));
        pnlLedgerBottom . setBorder(new TitledBorder("Stock Details"));
        
        this . setTitle("Green Tape Receipts And Issues");
        this . setSize(550,600);
        this . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this . setMaximizable(true);
        this . setClosable(true);
        this . setResizable(true);        
    }
    private void addComponents()
    {
        pnlTop . add(new JLabel("FromDate"));
        pnlTop . add(txtFromDate);
        pnlTop . add(new JLabel("ToDate"));
        pnlTop . add(txtToDate);
        pnlTop . add(new JLabel("Contractor"));
        pnlTop . add(cmbContractor);
        pnlTop . add(new JLabel("")) ;
        pnlTop . add(btnApply);
        
        //pnlMiddle . add(new JScrollPane(theTable));
        //pnlMiddleTop . add(new JScrollPane(theTable));
         pnlMiddleTop . add(tabPane);
        
        pnlMiddleBottom . add(new JLabel(" Total No Of Bundles "));
        pnlMiddleBottom . add(lblTotBundles);
        pnlMiddleBottom . add(new JLabel(" Total No Of Tapes "));
        pnlMiddleBottom . add(lblTotTapes);
        pnlMiddleBottom . add(new JLabel(" Rate Per Tape "));
        pnlMiddleBottom . add(lblRatePerTape);
        pnlMiddleBottom . add(new JLabel(" Payable Amount "));
        pnlMiddleBottom . add(lblPayableAmount);
        
        
        pnlMiddle . add(pnlMiddleTop,BorderLayout.CENTER);
        pnlMiddle . add(pnlMiddleBottom,BorderLayout.SOUTH);

        pnlStock        . add(new JLabel("   STOCK"));
        pnlStock        . add(lblStock);
        lblStock        . setForeground(Color.red);
        
        pnlIssue        . add(new JScrollPane(theIssueTable));
        
        pnlStockIssue   . add(pnlStock,BorderLayout.NORTH);
        pnlStockIssue   . add(pnlIssue,BorderLayout.CENTER);
        
        pnlLedgerBottom . add(new JLabel(""));
        pnlLedgerBottom . add(new JLabel("Opening"));
        pnlLedgerBottom . add(lblOpeningStock);
        pnlLedgerBottom . add(new JLabel(""));
        pnlLedgerBottom . add(new JLabel(""));
        pnlLedgerBottom . add(new JLabel(""));
        pnlLedgerBottom . add(new JLabel(""));
        pnlLedgerBottom . add(new JLabel("Receipt"));
        pnlLedgerBottom . add(lblReceipt);
        pnlLedgerBottom . add(new JLabel(""));
        pnlLedgerBottom . add(new JLabel(""));
        pnlLedgerBottom . add(new JLabel(""));
        pnlLedgerBottom . add(new JLabel(""));
        pnlLedgerBottom . add(new JLabel("Issue"));
        pnlLedgerBottom . add(lblIssue);
        pnlLedgerBottom . add(new JLabel(""));
        pnlLedgerBottom . add(new JLabel(""));
        pnlLedgerBottom . add(new JLabel(""));
        pnlLedgerBottom . add(new JLabel(""));
        pnlLedgerBottom . add(new JLabel("Stock"));
        pnlLedgerBottom . add(lblClosingStock);

        pnlLedger       . add(new JScrollPane(theLedgerTable),BorderLayout.CENTER);
        pnlLedger       . add(pnlLedgerBottom,BorderLayout.SOUTH);


        pnlBottom . add(btnPrint);
        pnlBottom . add(btnExit);
        
        this . add(pnlTop,BorderLayout.NORTH);
        this . add(pnlMiddle,BorderLayout.CENTER);
        this . add(pnlBottom,BorderLayout.SOUTH);
    }
    private void addListeners()
    {
        btnApply . addActionListener(new ActList());
        btnPrint . addActionListener(new ActList());
        btnExit  . addActionListener(new ActList());
    }
    private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==btnApply)
            {
                SFromDate = txtFromDate.toNormal();
                SToDate   = txtToDate.toNormal();
                SContractor = (String)cmbContractor.getSelectedItem();
                iContractorCode = common.toInt((String)VContractorCode.get(cmbContractor.getSelectedIndex()));
                iContractorEmpCode = common.toInt((String)VContractorEmpCode.get(cmbContractor.getSelectedIndex()));
                setRate();
                setStock();
                setTableData();
                setIssueTableData();
                setLedgerTableData();
            }
            if(ae.getSource()==btnPrint)
            {
                if(iContractorCode==999)
                {
                   JOptionPane.showMessageDialog(null,"Plz Select Any One Contractor");
                   return;
                }
   
                String SFile = "D:/GreenTapeReceipts.pdf";
                String STitle = " Green Tape Received Details For the Period Of "+common.parseDate(SFromDate)+"-"+common.parseDate(SToDate);
                String SSubTitle = " Contractor Name - "+SContractor;
               
                GreenTapeReceiptsPDF createPdf = new GreenTapeReceiptsPDF(SFile,STitle,SSubTitle,theModel,SFromDate,SToDate,SContractor,dRatePerTape);
                JOptionPane .showMessageDialog(null,"PDF File Created in D:/GreenTapeReceipts.pdf ","Info",JOptionPane.INFORMATION_MESSAGE);
            }
            if(ae.getSource()==btnExit)
            {
                dispose();
            }
        }
    }
    private void setTableData()
    {
        clearData();
        setDataVector();
        theModel.setNumRows(0);
        double dTotalTapes=0.0,dTotalBundles=0.0;
        for(int i=0;i<VData.size();i++)
        {
            HashMap theMap = (HashMap)VData.get(i);
            Vector theVect = new Vector();

            theVect . addElement(String.valueOf(i+1));
            theVect . addElement(common.parseDate((String)theMap.get("Date")));
            theVect . addElement((String)theMap.get("Receipts"));
            dTotalBundles = dTotalBundles + common.toDouble((String)theMap.get("Receipts"));
            theModel.appendRow(theVect);
        }
        Vector theTotalVect = new Vector();
        theTotalVect . addElement("Total");
        theTotalVect . addElement("");
        theTotalVect . addElement(dTotalBundles);
        theModel     . appendRow(theTotalVect);

        dTotalTapes = dTotalBundles*50;
        lblTotBundles  . setText(common.getRound(dTotalBundles,0));
        lblTotTapes    . setText(common.getRound(dTotalTapes,0));
        lblRatePerTape . setText(common.getRound(dRatePerTape, 2));
        lblPayableAmount . setText(common.getRound(dTotalTapes*dRatePerTape,2));
    }
    private void setIssueTableData()
    {
        setIssueDetails();
        theIssueDetModel.setNumRows(0);
        double dTotalIssue=0.0;
        for(int i=0;i<VIssueData.size();i++)
        {
            HashMap theMap  = (HashMap)VIssueData.get(i);
            Vector theVect = new Vector();
            theVect . addElement(String.valueOf(i+1));
            theVect . addElement(common.parseDate((String)theMap.get("IssueDate")));
            theVect . addElement((String)theMap.get("Unit"));
            theVect . addElement((String)theMap.get("Department"));
            theVect . addElement((String)theMap.get("IssueQty"));
            double dIssueQty = common.toDouble((String)theMap.get("IssueQty"));
           // theVect . addElement(common.getRound(dIssueQty*VContractorCode.size(),0));
            
            dTotalIssue = dTotalIssue+dIssueQty;
            theIssueDetModel.appendRow(theVect);
        }
        Vector theVect = new Vector();
        theVect . addElement("Total");
        theVect . addElement("");
        theVect . addElement("");
        theVect . addElement("");
        theVect . addElement(common.getRound(dTotalIssue,0));
        //theVect . addElement(common.getRound(dTotalIssue*VContractorCode.size(),0));
        theIssueDetModel.appendRow(theVect);
    }
    private void setLedgerTableData()
    {
      dOpeningStock = 0.0;
      dTotalReceipt = 0.0;
      dTotalIssue   = 0.0;
      dClosingStock = 0.0;
      
      setOpening();
      setLedgerData();
      theLedgerModel . setNumRows(0);

      Vector vect   = new Vector();
      vect          . add("Opening");
      for(int k=0;k<11;k++)
        vect          . add("");
      vect            . add(String.valueOf(dOpeningQty));
      vect            . add(String.valueOf(dOpeningValue));
      vect            . add("");
      theLedgerModel  . appendRow(vect);     

      for(int i=0;i<VLedgerData.size();i++)
      {
        HashMap theMap = (HashMap)VLedgerData.elementAt(i);
        Vector theVect = new Vector();
        theVect . add(common.parseDate((String)theMap.get("DocDate")));
        theVect . add((String)theMap.get("DocNo2"));
        theVect . add((String)theMap.get("DocNo"));
        theVect . add((String)theMap.get("DocType"));
        theVect . add((String)theMap.get("UnitName"));
        theVect . add((String)theMap.get("Supplier"));
        theVect . add((String)theMap.get("DeptName"));
        theVect . add((String)theMap.get("GroupName"));
        if(common.toInt((String)theMap.get("DocTypeCode"))==2)
        {
		theVect . add("");
		theVect . add("");
		theVect . add((String)theMap.get("DocQty"));
		theVect . add((String)theMap.get("DocValue"));
                double dStockQty = common.toDouble(String.valueOf(theLedgerModel.getValueAt(i,12)))-common.toDouble(String.valueOf(theMap.get("DocQty")));
                theVect . add(String.valueOf(dStockQty));
                dTotalIssue += common.toDouble((String)theMap.get("DocQty"));
                dClosingStock = dStockQty;
               
        }
        else if(common.toInt((String)theMap.get("DocTypeCode"))==1)
        {
		theVect . add((String)theMap.get("DocQty"));
		theVect . add((String)theMap.get("DocValue"));
		theVect . add("");
		theVect . add("");
                double dStockQty = common.toDouble(String.valueOf(theLedgerModel.getValueAt(i,12)))+common.toDouble(String.valueOf(theMap.get("DocQty")));
                theVect . add(String.valueOf(dStockQty));
                dTotalReceipt += common.toDouble((String)theMap.get("DocQty"));
                dClosingStock  = dStockQty;
        }

        //theVect . add("");
        theVect . add("");
        theVect . add("");
        theLedgerModel . appendRow(theVect);
      } 
        System.out.println(" OPG "+dOpeningQty);
        dOpeningStock    =  dOpeningQty;
        lblOpeningStock  .  setText(String.valueOf(dOpeningStock));
        lblIssue         .  setText(String.valueOf(dTotalIssue));
        lblReceipt       .  setText(String.valueOf(dTotalReceipt));
        lblClosingStock  .  setText(String.valueOf(dClosingStock));
    }

    private void setRate()
    {
        StringBuffer sb = new StringBuffer();
        sb.append(" select RatePerTape from GreentapeRate ");
        sb.append(" where ("+SFromDate+">=WithEffectFrom and "+SFromDate+"<=WithEffectTo) ");
        sb.append(" and ("+SToDate+">=WithEffectFrom and "+SToDate+"<=WithEffectTo)");
         try
        {
            ORAConnection connect    = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();
            
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();
            while(theResult.next())
            {
              dRatePerTape = theResult.getDouble(1);
            }
            theResult.close();
            theStatement.close();
        }
        catch(Exception e)
        {
           System.out.println(" set GreenTape Rate "+e);
        }
        
    }
    private void setStock()
    {
        String SItemStock="";
        StringBuffer sb = new StringBuffer();
        sb.append(" select Stock from ItemStock where ItemCode='A08100130'");
        try
        {
           ORAConnection connect    =  ORAConnection.getORAConnection();
           Connection theConnection =  connect.getConnection();
           
           PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
           ResultSet         theResult    = theStatement.executeQuery();
           
           while(theResult.next())
           {
               SItemStock = theResult.getString(1);
           }
           lblStock.setText(SItemStock);

           theResult.close();
           theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println(" setStock "+ex);
        }
    }
    private void setDataVector()
    {
        VData = new Vector();
        StringBuffer sb = new StringBuffer();
        sb.append(" select GreenTapeReceived.ReceivedDate,GreenTapeReceived.OpeningStock,GreenTapeReceived.NoOfBundles,");
        sb.append(" GreenTapeReceived.ClosingStock,GreenTapeContractor.ContractorName from GreenTapeReceived ");

        //sb.append(" decode(GreenTapeIssue.IssueQty,'',0,GreenTapeIssue.IssueQty) as Issues,GreenTapeReceived.ClosingStock,GreenTapeContractor.ContractorName from GreenTapeReceived ");
        //sb.append(" Left join GreenTapeIssue on GreenTapeIssue.IssueDate=GreenTapeReceived.ReceivedDate");
        //sb.append(" and GreenTapeIssue.ContractorCode = GreenTapeReceived.ContractorCode");

        sb.append(" Inner join GreenTapeContractor on GreenTapeContractor.ContractorCode=GreenTapeReceived.ContractorCode and GreenTapeContractor.ContractorEmpCode=GreenTapeReceived.ContractorEmpCode");
        sb.append(" where GreenTapeReceived.ReceivedDate>="+SFromDate+" and GreenTapeReceived.ReceivedDate<="+SToDate);
        if(iContractorCode!=999)
           sb.append(" and GreenTapeReceived.ContractorCode="+iContractorCode+" and GreenTapeReceived.ContractorEmpCode="+iContractorEmpCode);
        sb.append(" Order by 1");
        
      //  System.out.println(" setData Vector "+sb.toString());
        try
        {
            ORAConnection connect    = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();
            
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();
            while(theResult.next())
            {
                HashMap theMap = new HashMap();
                theMap .put("Date",theResult.getString(1) );
                theMap .put("OpeningStock", theResult.getString(2));
                theMap .put("Receipts",theResult.getString(3));
                //theMap .put("Issues", theResult.getString(4));
                theMap .put("ClosingStock", theResult.getString(4));
                theMap .put("ContractorName", theResult.getString(5));
                VData . add(theMap);
            }
            theResult.close();
            theStatement.close();
        }
        catch(Exception e)
        {
           System.out.println(" setData Vector GreenTape Receipts Issues "+e);
        }
    }   
    private void setIssueDetails()
    {
       VIssueData = new Vector();
       StringBuffer sb = new StringBuffer();
   /*   sb.append(" Select IssueDate,sum(IssueQty),Unit.Unit_Name,Dept.Dept_Name,Contractorcode,ContractorEmpCode from GreenTapeIssue");
       sb.append(" Inner join Dept on Dept.Dept_Code=GreenTapeIssue.Dept_Code");
       sb.append(" Inner join Unit on Unit.Unit_Code=GreenTapeIssue.UnitCode");
       sb.append(" where IssueDate>="+SFromDate+" and IssueDate<="+SToDate);
       sb.append(" and ContractorCode="+iContractorCode+" and ContractorEmpCode="+iContractorEmpCode);
       sb.append(" group by IssueDate,Unit.Unit_Name,Dept.Dept_Name,ContractorCode,ContractorEmpCode");
       sb.append(" Order by 1");*/

       sb.append(" Select IssueDate,sum(Qty),Unit.Unit_Name,Dept.Dept_Name from NonStockIssue");
       sb.append(" Inner join Dept on Dept.Dept_Code=NonStockIssue.Dept_Code");
       sb.append(" Inner join Unit on Unit.Unit_Code=NonStockIssue.Unit_Code");
       sb.append(" where IssueDate>="+SFromDate+" and IssueDate<="+SToDate);
       sb.append(" and NonStockIssue.Code='A08100130'");
       //sb.append(" and ContractorCode="+iContractorCode);
       sb.append(" group by IssueDate,Unit.Unit_Name,Dept.Dept_Name");
       sb.append(" Order by 1");
       System.out.println(" set IssueDetails "+sb.toString());
       try
       {
           ORAConnection connect    = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();
            
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();
            while(theResult.next())
            {
                HashMap theMap = new HashMap();
                theMap .put("IssueDate",theResult.getString(1) );
                theMap .put("IssueQty", theResult.getString(2));
                theMap .put("Unit",theResult.getString(3));
                theMap .put("Department", theResult.getString(4));
                
                VIssueData . add(theMap);
            }
            theResult.close();
            theStatement.close();
       }
       catch(Exception ex)
       {
           System.out.println(" Issue Details "+ex);
       }
    }    
    private void setOpening()
    {
        dOpeningQty     = 0.0;dOpeningValue=0.0;
        StringBuffer sb = new StringBuffer();
        sb.append(" Select OpgQty,OpgVal From InvItems");
        sb.append(" inner join uom on uom.uomcode = invitems.uomcode and Item_Code='A08100130'");
        try
        {
           ORAConnection connect    = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();
            
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();
            while(theResult.next())
            {
               dOpeningQty   = theResult.getDouble(1);
               dOpeningValue = theResult.getDouble(2);
            }
            theResult.close();
            theStatement.close();
        }
        catch(Exception ex)
        {
           System.out.println(" set Opening "+ex);
        }


    }
    private void setLedgerData()
    {
       VLedgerData = new Vector();
       StringBuffer sb = new StringBuffer();
       sb.append(" select DocDate,DocNo,Block,DocQty,DocValue,Supplier,Unit_Name,Dept_Name,Group_Name,DocType,GrnType,DocNo2,DocTypecode from(");
       sb.append(" Select NonStockIssue.IssueDate as DocDate,NonStockIssue.UIRefNo as DocNo,' ' as Block,   NonStockIssue.Qty as DocQty,");
       sb.append(" NonStockIssue.IssueValue as DocValue,");
       sb.append(" ' ' as Supplier, Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'Issue' as DocType,'1' as GrnType,  ");
       sb.append(" NonStockIssue.IssueNo as DocNo2,'2' as DocTypeCode  From NonStockIssue ");
       sb.append(" Inner Join Dept On NonStockIssue.Dept_Code = Dept.Dept_Code  and NonStockIssue.Code = 'A08100130'  ");
       sb.append(" and NonStockIssue.IssueDate >= '"+SStDate+"' and NonStockIssue.IssueDate <= '"+SEnDate+"'  ");
       sb.append(" and NonStockIssue.MillCode=0");
       sb.append(" Inner Join Cata On Cata.Group_Code = NonStockIssue.Group_Code  ");
       sb.append(" Inner Join Unit On NonStockIssue.Unit_Code = Unit.Unit_Code  ");
       sb.append(" Union All");
       sb.append(" Select to_char(GreenTapeDetails.ReceivedDate) As DocDate,0 as DocNo,'' as Block, sum(GreenTapeDetails.NoOfBundles) as DocQty,");
       sb.append(" 0 as DocValue,'' as Supplier, '' as Unit_Name,'' as Dept_Name,'' as Group_Name,'Receipt' as DocType,'0' as GRNType, ");
       sb.append(" 0 as DocNo2,'1' as DocTypeCode  From GreenTapeDetails where ReceivedDate>='"+SStDate+"' and ReceivedDate<= '"+SEnDate+"'");      
       sb.append(" group by to_char(GreenTapeDetails.ReceivedDate)");
       sb.append(" )");
       sb.append(" Order By 1,13,2");

       System.out.println(" setLedgerData "+sb.toString());
       try
       {
            ORAConnection connect    = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();
            
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();
            while(theResult.next())
            {
                HashMap theMap = new HashMap();
                theMap .put("DocDate",theResult.getString(1) );
                theMap .put("DocNo", theResult.getString(2));
                theMap .put("Block",theResult.getString(3));
                theMap .put("DocQty", theResult.getString(4));
                theMap .put("DocValue", theResult.getString(5));
                theMap .put("Supplier", theResult.getString(6));
                theMap .put("UnitName", theResult.getString(7));
                theMap .put("DeptName", theResult.getString(8));
                theMap .put("GroupName", theResult.getString(9));
                theMap .put("DocType", theResult.getString(10));
                theMap .put("GrnType", theResult.getString(11));
                theMap .put("DocNo2", theResult.getString(12));
                theMap .put("DocTypeCode", theResult.getString(13));
                VLedgerData . add(theMap);
            }
            theResult.close();
            theStatement.close();
       }
       catch(Exception ex)
       {
           System.out.println(" Ledger Details "+ex);
       }

    }

    private void setContractor()
    {
        VContractorCode = new Vector();
        VContractorName = new Vector();
        VContractorEmpCode = new Vector();

        VContractorCode    . addElement("999");
        VContractorName    . addElement("All");
        VContractorEmpCode . addElement("999");

        try
        {
            String QS = " Select ContractorCode,ContractorName,ContractorEmpCode from GreenTapeContractor order by 1";
          
            ORAConnection connect    = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();  
          
            PreparedStatement theStatement = theConnection.prepareStatement(QS);
            
            ResultSet         theResult    = theStatement.executeQuery();
            while(theResult.next())
            {
                VContractorCode . add(theResult.getString(1));
                VContractorName . add(theResult.getString(2));
                VContractorEmpCode . add(theResult.getString(3));
            }
            theResult . close();
            theConnection = null;
          
        }
        catch(Exception ex)
        {
           System.out.println(ex);
        }
    }     
    private void clearData()
    {
        lblTotBundles . setText("");
        lblTotTapes   . setText("");
        lblRatePerTape. setText("");
        lblPayableAmount . setText("");
    }
    
}
