
package Indent.IndentFiles.GreenTapePayment;

import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
public class GreenTapeReceiptsPDF 
{
     int            Lctr      = 100;
     int            Pctr      = 0;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font tinyBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font tinyNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font underBold  = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
   
     Document   document;
     PdfPTable  table,notesTable,empTable;
     Connection theConnection = null;
     
     GreenTapeReceiptModel theModel;
     String     SFile,STitle,SSubTitle;
     String     SFromDate,SToDate,SContractor;
     int[]      iWidth;
     String[]   SHead;
     Common     common ;
     double     dPackingKgs,dWashingWaste,dBleaching,dActualKgs,dMarginKgs,dEligibleKgs,
                dNotAvailPersonsAmount,dAbsentPersonsAmount,dRoundingOff,dRoundedNet;
     double dRatePerTape;
     int    iMaxLineCount=42,iMonth;
     //Model Index 
     int SNo=0,ReceivedDate=1,NoOfBundles=2;
     
     
     public GreenTapeReceiptsPDF(String SFile,String STitle,String SSubTitle,GreenTapeReceiptModel theModel,String SFromDate,String SToDate,String SContractor,double dRatePerTape)
     {
          this . SFile      = SFile;
          this . STitle     = STitle;
          this . SSubTitle  = SSubTitle;
          this . theModel   = theModel;
          this . SFromDate  = SFromDate;
          this . SToDate    = SToDate;
          this . SContractor = SContractor;
          this . dRatePerTape= dRatePerTape;
          SHead  = new String[]{"S.No","Received Date ","No Of Bundles"};
          iWidth = new int[]{13,30,40};
           
          common  = new Common();
          createPDFFile(); 

     }
   private void createPDFFile()
    {
     try
     {
       document = new Document(PageSize.A4);
      
       PdfWriter.getInstance(document, new FileOutputStream(SFile));
       document.open();

       table      = new PdfPTable(3);
       table      . setWidthPercentage(100);
       addHead(document,table);
       addBody(document,table);
       addFoot(document);
       document.close();
              // JOptionPane.showMessageDialog(null, "PDF File Created in "+SFile,"Info",JOptionPane.INFORMATION_MESSAGE);
     }
     catch (Exception e)
     {
        e.printStackTrace();
     }
   }
   private void addHead(Document document,PdfPTable table) throws BadElementException
   {
       try
          {
               if(Lctr < 42)
                    return;

               if(Pctr > 0)
                    addFoot(document,table);

               Pctr++;

               document.newPage();

               Paragraph paragraph;
               paragraph = new Paragraph(STitle,bigBold);
               paragraph.setAlignment(Element.ALIGN_CENTER);
               paragraph.setSpacingAfter(10);
               document.add(paragraph);

               paragraph = new Paragraph(SSubTitle,smallBold);
               paragraph.setAlignment(Element.ALIGN_CENTER);
               paragraph.setSpacingAfter(10);
               document.add(paragraph);
    
               
          int iRowCount = 2;
          PdfPCell c1;

           for(int i=0;i<SHead.length;i++)
           {
             c1 = new PdfPCell(new Phrase(SHead[i] ,mediumBold));
             c1.setHorizontalAlignment(Element.ALIGN_LEFT);
             c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
             c1.setFixedHeight(30f);
             table.addCell(c1);
             iRowCount = iRowCount+1;
           }
          
          }
       catch(Exception ex)
       {
           ex.printStackTrace();
       }
   }
   private void addBody(Document document,PdfPTable empTable) throws BadElementException
   {
       try
       {

           PdfPCell c1;
           double dTotalBundles =0.0,dTotalTapes=0.0;
           for(int i=0;i<theModel.getRowCount();i++)
           {
          
            String SlNo          = String.valueOf(theModel.getValueAt(i,SNo));
            String SReceivedDate = String.valueOf(theModel.getValueAt(i,ReceivedDate));
            String SNoOfBundles  = String.valueOf(theModel.getValueAt(i,NoOfBundles));
            
            double dNoOfBundles  = common.toDouble(String.valueOf(theModel.getValueAt(i,NoOfBundles)));
			if(i<theModel.getRowCount()-1)
	            dTotalBundles = dTotalBundles+dNoOfBundles;
            
             addDataIntoTable(table,SlNo,mediumBold,Element.ALIGN_LEFT,30f);
             addDataIntoTable(table,SReceivedDate,mediumBold,Element.ALIGN_LEFT,30f);
             addDataIntoTable(table,SNoOfBundles,mediumBold,Element.ALIGN_RIGHT,30f);
             
           }
           table.setSpacingAfter(30);
           document.add(table);
           dTotalTapes           = dTotalBundles*50;

           double dPayableAmount = dTotalTapes*dRatePerTape;
           addDataIntoParagraph(document,"Total No of Bundles : "+common.getRound(dTotalBundles,0),mediumBold,Element.ALIGN_LEFT);
           addDataIntoParagraph(document,"Total No Of Tapes   : "+common.getRound(dTotalTapes,0),mediumBold,Element.ALIGN_LEFT);
           addDataIntoParagraph(document,"Rate Per Tape       : "+common.getRound(dRatePerTape,2),mediumBold,Element.ALIGN_LEFT);
           addDataIntoParagraph(document,"PayableAmount       : "+common.getRound(dPayableAmount,2),mediumBold,Element.ALIGN_LEFT);
           

       }
       catch(Exception ex)
       {
           ex.printStackTrace();
       }
   }
   private void addFoot(Document document,PdfPTable table)
   {
 

   }

   private void addFoot(Document document)
   {
       try
           {
               Paragraph paragraph;

               paragraph = new Paragraph("Store Keeper "+common.Space(32)+" SO "+common.Space(32)+" SMB "+common.Space(32)+" IA/Cash "+common.Space(32)+" Accounts ",smallBold);
               paragraph.setAlignment(Element.ALIGN_LEFT);
               paragraph.setSpacingBefore(30);
               paragraph.setSpacingAfter(10);
               document.add(paragraph);
           }
           catch(Exception e)
           {
               System.out.println(" addData Para "+e);
           }

   }
   private void addDataIntoTable(PdfPTable table,String SData,Font FontType,int align,float dHeight)
   {
               PdfPCell c1;
               c1 = new PdfPCell(new Phrase(SData ,FontType));
               c1.setHorizontalAlignment(align);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               c1.setFixedHeight(dHeight);
               table.addCell(c1);
   
   }
   private void addDataIntoParagraph(Document document,String SData,Font FontType,int align)
   {
           try
           {
               Paragraph paragraph;
               paragraph = new Paragraph(SData,FontType);
               paragraph.setAlignment(align);
               paragraph.setSpacingAfter(10);
               document.add(paragraph);
           }
           catch(Exception e)
           {
               System.out.println(" addData Para "+e);
           }
   
   }
   private void addDataIntoTable(PdfPTable table,String SData,Font FontType,int halign,int valign,int iColSpan,float dHeight)
   {
               PdfPCell c1;
               c1 = new PdfPCell(new Phrase(SData ,FontType));
               c1.setHorizontalAlignment(halign);
               c1.setColspan(iColSpan);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
               c1.setFixedHeight(dHeight);
               table.addCell(c1);
   
   }

 
   private void addEmptyRow(int iColumnSize,PdfPTable table)
   {
       PdfPCell c1;
       for(int i=0;i<7;i++)
       {
           c1 = new PdfPCell(new Phrase("" ,smallBold));
           c1.setHorizontalAlignment(Element.ALIGN_LEFT);
           c1.setBorderColor(BaseColor.WHITE);
           table.addCell(c1);
       }
       
   }
}
