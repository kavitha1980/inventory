package Indent.IndentFiles.GreenTapePayment;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.awt.*;
import java.sql.*;
import java.util.*;
import java.net.InetAddress;
import javax.swing.table.*;
import java.io.*;
import guiutil.*;
import util.*;
import jdbc.*;

/*import com.digitalpersona.onetouch.*;
import com.digitalpersona.onetouch.capture.*;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.*;
import com.digitalpersona.onetouch.verification.*;*/
//public class GreenTapeReceiveFrame  extends JFrame
public class GreenTapeReceiveFrame  extends JInternalFrame
{
    JPanel                pnlTop,pnlMiddle,pnlBottom;
    JButton               BSave,BExit;
    //AttDateField          TDate;
    JTextField            TDate;
    JLabel                lblTapes,LStatus;
    JLabel                lblReceived,lblItemStock;
    JComboBox             cmbContractor;
    JTable                theTable = null;
    GreenTapeReceiveModel theModel = null;
    WholeNumberField      txtReceived;
    Vector                VContractorCode,VContractorName,VContractorEmpCode,VData;
    Connection            theConnection = null;
    Common                common = new Common();
    double                dOpeningStock=0.0,dClosingStock=0.0,dIssue=0.0;
    double                dIndividualOpeningStock=0.0,dIndividualClStock=0.0,dIndividualIssue=0.0;
    int                   iTotalReceived=0,iMaxOfReceivedDate=0;
    String                SSystemName;
    JLayeredPane          Layer;
    int                   iHodCode,iMillCode,iUserCode;
    ArrayList             theContractorList;
    GreenTapeReceiveFrame greentapeReceiveFrame;
    String  SItemCode   ="";
// A08000325
    int    iContractorCode,iContractorEmpCode;
    public GreenTapeReceiveFrame(JLayeredPane Layer,int iUserCode,int iHodCode,int iMillCode)
    {
        this . Layer    = Layer;
        this . iUserCode= iUserCode;
        this . iHodCode = iHodCode;
        this . iMillCode= iMillCode;
        setItemCode();
        setContractor();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
        setStockDetails();
        setItemStock();
        setTableData();
       // setContractorList();
        iContractorCode = getContractorCode((String)cmbContractor.getSelectedItem());
        iContractorEmpCode = getContractorEmpCode((String)cmbContractor.getSelectedItem());
        greentapeReceiveFrame=this;
        try
        {
            SSystemName = InetAddress.getLocalHost().getHostName();
        }
        catch(Exception e)
        {
            System.out.println(" SystemName  "+e);
        }
    }
    private void createComponents()
    {
        pnlTop    = new JPanel();
        pnlMiddle = new JPanel();
        pnlBottom = new JPanel();
        BSave     = new JButton(" Save ");
        BExit     = new JButton(" Exit ");
      
        TDate     = new JTextField(10);
      
        try
        {
          TDate     .setText(common.parseDate(common.getServerPureDate()));
        }
        catch(Exception e){}
      
     
        lblTapes  = new JLabel("");
        LStatus   = new JLabel("");
             
        lblReceived          = new JLabel("");
        lblItemStock         = new JLabel("");

        txtReceived   = new WholeNumberField(6);
        cmbContractor = new JComboBox(VContractorName);
        theModel      = new GreenTapeReceiveModel();
        theTable      = new JTable(theModel);
        
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        
        theTable.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
        theTable.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
        
        DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
        leftRenderer.setHorizontalAlignment(SwingConstants.LEFT);
        theTable.getColumnModel().getColumn(0).setCellRenderer(leftRenderer);
        theTable.getColumnModel().getColumn(1).setCellRenderer(leftRenderer);
    }
    private void setLayouts()
    {
     
        pnlTop    . setLayout(new GridLayout(5,2,3,3));
        pnlMiddle . setLayout(new BorderLayout());
        pnlBottom . setLayout(new GridLayout(4,2,3,3));

        pnlMiddle  . setBorder(new TitledBorder("Data"));
        pnlBottom  . setBorder(new TitledBorder("Save"));
        pnlTop     . setBorder(new TitledBorder("Receive"));
        
        this . setSize(630,520);
        this . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this . setTitle("Green Tape Receive Frame");
        this . setMaximizable(true);
        this . setClosable(true);
        this . setResizable(true);
    }
    private void addComponents()
    {

        pnlTop . add(new JLabel("Date of Receiving"));
        pnlTop . add(TDate);
        pnlTop . add(new JLabel("Contractor Name"));
        pnlTop . add(cmbContractor);
        pnlTop . add(new JLabel(" No of Tape Bundles Received ")) ;
        pnlTop . add(txtReceived);
        int iNoOfTapes = common.toInt(txtReceived.getText())*50;
        lblTapes  . setText(String.valueOf(iNoOfTapes));
        pnlTop . add(new JLabel(" No of Tapes Received ")) ;
        pnlTop . add(lblTapes);
        pnlTop . add(new JLabel("Item Stock")) ;
        pnlTop . add(lblItemStock);
        lblItemStock   . setForeground(Color.red);
        pnlMiddle . add(new JScrollPane(theTable));
       
        pnlBottom . add(LStatus);
        pnlBottom . add(BSave);
        pnlBottom . add(BExit);
        
        this . add(pnlTop,BorderLayout.NORTH);
        this . add(pnlMiddle,BorderLayout.CENTER);
        this . add(pnlBottom,BorderLayout.SOUTH);
    }
    private void addListeners()
    {
        BSave . addActionListener(new ActList());
        BExit . addActionListener(new ActList());
        txtReceived  . addFocusListener(new FocusList());
        TDate        . addFocusListener(new FocusList());
        cmbContractor. addItemListener(new ItemList());
    }
    private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==BSave)
            {
              if(validData())
              {
               if(JOptionPane.showConfirmDialog(null,"Do U want to Save Data? ","Confirmation",JOptionPane.YES_NO_OPTION)==0)   
               {
                iContractorCode = getContractorCode((String)cmbContractor.getSelectedItem());
                iContractorEmpCode = getContractorEmpCode((String)cmbContractor.getSelectedItem());
                saveData("","");
                JOptionPane.showMessageDialog(null,"Data Saved Successfully ","Info",JOptionPane.INFORMATION_MESSAGE);
               
                clearData();
                iContractorCode = getContractorCode((String)cmbContractor.getSelectedItem());
                iContractorEmpCode = getContractorEmpCode((String)cmbContractor.getSelectedItem());
                setStockDetails();
                setItemStock();
                setTableData();
               }
              }
            }
            if(ae.getSource()==BExit)
            {
                 dispose();
            }
        }
    }
    private class FocusList extends FocusAdapter
    {
        public void focusLost(FocusEvent fe)
        {
           if(fe.getSource()== txtReceived) 
           {
             iContractorCode = getContractorCode((String)cmbContractor.getSelectedItem());
             iContractorEmpCode = getContractorEmpCode((String)cmbContractor.getSelectedItem());
             int iNoOfTapes = common.toInt(txtReceived.getText())*50;
             lblTapes  . setText(String.valueOf(iNoOfTapes));   
           }
           if(fe.getSource()==TDate)
           {
               clearData();
               iContractorCode = getContractorCode((String)cmbContractor.getSelectedItem());
               iContractorEmpCode = getContractorEmpCode((String)cmbContractor.getSelectedItem());
               setStockDetails();
               setTableData();
           }
        }
    }
    private class ItemList implements ItemListener
    {
        public void itemStateChanged(ItemEvent ie)
        {
            clearData();
            iContractorCode = getContractorCode((String)cmbContractor.getSelectedItem());
            iContractorEmpCode = getContractorEmpCode((String)cmbContractor.getSelectedItem());
            setStockDetails();
            setTableData();
        }
    }
    private void setStockDetails()
    {
       setReceivedData();
    }
    public void setTableData()
    {
        setData();
        theModel . setNumRows(0);
        for(int i=0;i<VData.size();i++)
        {
            HashMap theMap = (HashMap)VData.get(i);
            Vector theVect = new Vector();
            theVect . addElement(String.valueOf(i+1));
            theVect . addElement(theMap.get("Date"));
            theVect . addElement(theMap.get("ContractorName"));
            theVect . addElement(theMap.get("NoOfBundles"));
            theVect . addElement(theMap.get("NoOfTapes"));
            theModel. appendRow(theVect);
        }	
    }
    public int saveData(String SEmpCode,String SEmpName)
    {
           int iSave=0;
          
           ORAConnection connect = ORAConnection.getORAConnection();
           theConnection         = connect.getConnection();  
           try
           {
            if(theConnection.getAutoCommit())
               theConnection.setAutoCommit(false);    
        
           String SDate = common.pureDate(TDate.getText());
           iContractorCode = getContractorCode((String)cmbContractor.getSelectedItem());
           iContractorEmpCode = getContractorEmpCode((String)cmbContractor.getSelectedItem());
           if(isExists(iContractorCode,SDate,SEmpCode,iContractorEmpCode))
           {
              iSave = updateData(SEmpCode);
           }
           else
           {
             iSave  = insertData(SEmpCode);
           }
           updateItemStock();
           getSave();
           clearData();
           setStockDetails();
           setTableData();
          
           }
           catch(Exception e){}
        return iSave;
    }
   
    private void setData()
    {
        VData        = new Vector();
        String SDate = common.pureDate(TDate.getText());
        StringBuffer sb = new StringBuffer();
        sb.append(" Select ReceivedDate,NoOfBundles,NoOfTapes,ContractorName from GreenTapeDetails");
        sb.append(" Inner join GreenTapeContractor on GreenTapeContractor.ContractorCode=GreenTapeDetails.ContractorCode and GreenTapeContractor.ContractorEmpCode=GreenTapeDetails.ContractorEmpCode");
        sb.append(" where ReceivedDate="+SDate);
        try
        {
            ORAConnection connect = ORAConnection.getORAConnection();
            theConnection         = connect.getConnection();
            
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet         theResult    = theStatement.executeQuery();
            while(theResult.next())       
            {
                HashMap theMap = new HashMap();
                theMap . put("Date",common.parseDate(theResult.getString(1)));
                theMap . put("NoOfBundles",theResult.getString(2));
                theMap . put("NoOfTapes",theResult.getString(3));
                theMap . put("ContractorName",theResult.getString(4));
                VData  . add(theMap);
            }
            theResult.close();
            theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println(" set Data "+ex);
        }
    }
    private void setReceivedData()
    {
        iTotalReceived=0;
        String SDate = common.pureDate(TDate.getText());
        StringBuffer sb = new StringBuffer();
        sb.append(" Select sum(NoOfBundles),sum(NoOfTapes) from GreenTapeDetails");
        sb.append(" where ReceivedDate="+SDate+" and ContractorCode="+iContractorCode+" and ContractorEmpCode="+iContractorEmpCode);

        System.out.println("setReceivedData :"+sb.toString());
        try
        {
            ORAConnection connect = ORAConnection.getORAConnection();
            theConnection         = connect.getConnection();
            
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet         theResult    = theStatement.executeQuery();
            double dReceivedTapes=0.0;
            while(theResult.next())       
            {
               iTotalReceived = theResult.getInt(1);
               dReceivedTapes = theResult.getDouble(2);
            }
            theResult.close();
            theStatement.close();
     
              lblReceived.setText(String.valueOf(iTotalReceived));
 
        }
        catch(Exception ex)
        {
            System.out.println(" set Data "+ex);
        }
    }
   
     public int insertData(String SEmpCode)
    {

        int iSave=0;
        String SDate = common.pureDate(TDate.getText());
        
        iContractorCode    = getContractorCode((String)cmbContractor.getSelectedItem());
        iContractorEmpCode = getContractorEmpCode((String)cmbContractor.getSelectedItem()); 

        int    iReceivedTapes = common.toInt(lblTapes.getText());
        int    iReceivedBundles = common.toInt(txtReceived.getText());
        StringBuffer sb = new StringBuffer();
        sb.append(" Insert into GreenTapeReceived(Id,ReceivedDate,OpeningStock,ReceivedTapes,ClosingStock,EntryDate,SystemName,TapeReceivedFrom,ItemCode,ContractorCode,NoOfBundles,ContractorEmpCode)");
        sb.append(" values(GreenTapeReceived_Seq.NextVal,?,?,?,?,to_Char(sysdate,'yyyymmdd'),?,?,?,?,?,?)");
        try
        {
                PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
                theStatement.setString(1,SDate);
                theStatement.setDouble(2,dIndividualOpeningStock);
                theStatement.setInt(3,iReceivedTapes);
                theStatement.setDouble(4,(dIndividualClStock+iReceivedBundles));
                theStatement.setString(5,SSystemName);
                theStatement.setString(6,SEmpCode);
                theStatement.setString(7,SItemCode);
                theStatement.setInt(8,iContractorCode);
                theStatement.setInt(9,iReceivedBundles);
                theStatement.setInt(10,iContractorEmpCode);
                theStatement.executeUpdate();
                iSave=1;

                theStatement.close();
        }
        catch(Exception ex)
        {
           System.out.println(" Save Data "+ex);
        }
        return iSave;
    }
    public int updateData(String SEmpCode)
    {

        int iSave=0;
        String SDate = common.pureDate(TDate.getText());
        iContractorCode = getContractorCode((String)cmbContractor.getSelectedItem());
        iContractorEmpCode = getContractorEmpCode((String)cmbContractor.getSelectedItem());
       
        int    iReceivedTapes    = common.toInt(lblTapes.getText());
        int    iReceivedBundles  = common.toInt(txtReceived.getText());
        StringBuffer sb = new StringBuffer();
        sb.append(" Update GreenTapeReceived set ReceivedTapes=ReceivedTapes+?,ClosingStock=ClosingStock+?,NoOFBundles=NoOfBundles+? where ReceivedDate=? and ContractorCode=? and ContractorEmpCode=?");
       
        try
        {
                PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
                theStatement.setInt(1,iReceivedTapes);
                theStatement.setDouble(2,iReceivedBundles);
                theStatement.setDouble(3,iReceivedBundles);
                theStatement.setString(4, SDate);
                theStatement.setInt(5, iContractorCode);
                theStatement.setInt(6, iContractorEmpCode);
                theStatement.executeUpdate();
                iSave = 1;
     
                theStatement.close();
        }
        catch(Exception ex)
        {
           System.out.println(" Update Data "+ex);
        }
        return iSave;
    }

    private void setItemStock()
    {
        String SItemStock="";
        StringBuffer sb = new StringBuffer();
        sb.append(" select Stock from ItemStock where ItemCode='A08100130'");
        try
        {
           ORAConnection connect    =  ORAConnection.getORAConnection();
           Connection theConnection =  connect.getConnection();
           
           PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
           ResultSet         theResult    = theStatement.executeQuery();
           
           while(theResult.next())
           {
               SItemStock = theResult.getString(1);
           }
           lblItemStock.setText(SItemStock);

           theResult.close();
           theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println(" setItemStock "+ex);
        }
    }

    private void getSave()
    {

        String SDate = common.pureDate(TDate.getText());
        iContractorCode    = getContractorCode((String)cmbContractor.getSelectedItem());
        iContractorEmpCode = getContractorEmpCode((String)cmbContractor.getSelectedItem());
        int    iReceivedBundles= txtReceived.getValue();
        int    iTapes          = common.toInt(lblTapes.getText());
        try
        {
              String     QS = " Insert into GreenTapeDetails(Id,ReceivedDate,ContractorCode,NoOfBundles,NoOfTapes,EntryDate,SystemName,ContractorEmpCode)"+
                              " values(GreenTape_Seq.nextVal,?,?,?,?,to_Char(sysdate,'yyyymmdd'),?,?)";   
              
               ORAConnection connect = ORAConnection.getORAConnection();
               theConnection         = connect.getConnection();  
  
                PreparedStatement theStatement = theConnection.prepareStatement(QS);
                theStatement.setString(1,SDate);
                theStatement.setInt(2,iContractorCode);
                theStatement.setInt(3,iReceivedBundles);
                theStatement.setInt(4,iTapes);
                theStatement.setString(5, SSystemName);
                theStatement.setInt(6, iContractorEmpCode);
                theStatement.executeUpdate();
                
                theConnection.commit();
                //JOptionPane.showMessageDialog(null,"Data Saved Successfully ","Info",JOptionPane.INFORMATION_MESSAGE);

                theStatement.close();
                theConnection = null;


        }
        catch(Exception ex)
        {
           System.out.println(" Save "+ex);
        }
    }
    private void updateItemStock()
    {
//        int iReceivedTapes = common.toInt(lblTapes.getText());
        StringBuffer sb = new StringBuffer();

        int    iReceivedBundles  = common.toInt(txtReceived.getText());
        sb.append(" update ItemStock set Stock=Stock+"+iReceivedBundles+" where ItemCode='A08100130'");
        try
        {
            ORAConnection connect     = ORAConnection.getORAConnection();
            Connection theConnect     = connect.getConnection();
            
            PreparedStatement theStatement = theConnect.prepareStatement(sb.toString());
            theStatement  . executeUpdate();
            theStatement  . close();
        }
        catch(Exception ex)
        {
            System.out.println(" update  itemstock "+ex);
        }
    }
    private boolean isExists(int iContractorCode,String SDate,String SEmpCode,int iContractorEmpCode)
    {
        boolean bExist = false;
        try
        {
            StringBuffer sb = new StringBuffer();
            sb.append(" select count(*) from GreenTapeReceived ");
            sb.append(" where ReceivedDate="+SDate+" and ContractorCode="+iContractorCode+" and ContractorEmpCode ="+iContractorEmpCode);
            if(SEmpCode.length()>0)
              sb.append(" and TapeReceivedFrom="+SEmpCode);

            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet         theResult    = theStatement.executeQuery();
            while(theResult.next())
            {
                if(theResult.getInt(1)>0)
                  bExist = true;                       
            }
            theResult.close();
            theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println(" is Exists "+ex);
            return bExist;
        }
        return bExist;
    }
    
    private int getContractorCode(String SContractorName)
    {
       return common.toInt((String)VContractorCode .get(VContractorName.indexOf(SContractorName)));
    }
    private int getContractorEmpCode(String SContractorName)
    {
       return common.toInt((String)VContractorEmpCode .get(VContractorName.indexOf(SContractorName)));
    }
    private void setContractor()
    {
        VContractorCode = new Vector();
        VContractorName = new Vector();
        VContractorEmpCode = new Vector();
        try
        {
            String QS = " Select ContractorCode,ContractorName,ContractorEmpCode from GreenTapeContractor order by 1";
            if(theConnection==null)
            {
              ORAConnection connect = ORAConnection.getORAConnection();
              theConnection         = connect.getConnection();  
            }
          
            PreparedStatement theStatement = theConnection.prepareStatement(QS);
            
            ResultSet         theResult    = theStatement.executeQuery();
            while(theResult.next())
            {
                VContractorCode . add(theResult.getString(1));
                VContractorName . add(theResult.getString(2));
                VContractorEmpCode . add(theResult.getString(3));
            }
            theResult.close();
            theStatement.close();
            theConnection = null;
          
        }
        catch(Exception ex)
        {
           System.out.println(ex);
        }
    }
   
    private void setItemCode()
    {
        try
        {
            String QS = " Select Item_Code,Item_Name from InvItems where Item_Name like 'NYLON TAPE GREEN FOR BALE PACKING' order by 1";
            if(theConnection==null)
            {
              ORAConnection connect = ORAConnection.getORAConnection();
              theConnection         = connect.getConnection();  
            }
          
            PreparedStatement theStatement = theConnection.prepareStatement(QS);
            
            ResultSet         theResult    = theStatement.executeQuery();
            while(theResult.next())
            {
               SItemCode=theResult.getString(1);
            }
            theResult     . close();
            theStatement  . close();
            theConnection = null;
            
        }
        catch(Exception e)
        {
            System.out.println(" setItem Code "+e);
        }
    }
    private void setMaxDate()
    {
       iMaxOfReceivedDate=0;
       try
        {
            String QS = " Select Max(ReceivedDate) from GreenTapeReceived where ContractorCode="+iContractorCode+" and ContractorEmpCode="+iContractorEmpCode;
            if(theConnection==null)
            {
              ORAConnection connect = ORAConnection.getORAConnection();
              theConnection         = connect.getConnection();  
            }
          
            PreparedStatement theStatement = theConnection.prepareStatement(QS);
            
            ResultSet         theResult    = theStatement.executeQuery();
            while(theResult.next())
            {
               iMaxOfReceivedDate = theResult.getInt(1);
            }
            theResult     . close();
            theStatement  . close();
            theConnection = null;
            
        }
        catch(Exception e)
        {
            System.out.println(" setMax Date "+e);
        }
    }
     private byte[] getOracleBlob(ResultSet result, String columnName) throws SQLException
    {   
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        byte[] bytes = null;
        
        try {    
            oracle.sql.BLOB blob = ((oracle.jdbc.OracleResultSet)result).getBLOB(columnName);        
            inputStream = blob.getBinaryStream();        
            int bytesRead = 0;        
            
            while((bytesRead = inputStream.read()) != -1) {        
                outputStream.write(bytesRead);    
            }
            
            bytes = outputStream.toByteArray();
            
        } 
        
        catch(IOException e) {
            throw new SQLException(e.getMessage());
        } 
        finally 
        {
        }
    
        return bytes;
    }
    private void clearData()
    {
      
        txtReceived.setText("");
        lblTapes.setText("");
 
    }
    private boolean validData()
    {
        String SDate = common.pureDate(TDate.getText());
        int    iDate = common.toInt(SDate);
         setMaxDate();
        int    iReceivedTapes = common.toInt(lblTapes.getText());
        int    iReceivedBundles = common.toInt(txtReceived.getText());
        if(iReceivedTapes<=0 && iReceivedBundles<=0)
        {
          return false;
        }
         System.out.println(" Valid Data "+iDate+" ,"+iMaxOfReceivedDate);
        if(iDate<iMaxOfReceivedDate)
        {
           JOptionPane.showMessageDialog(null,"Invalid Date");
           return false;
        }
        return true;
    }
   
}
