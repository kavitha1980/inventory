package Indent.IndentFiles.INDENTMODIFY;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import Indent.IndentFiles.*;

public class IndentListFrame extends JInternalFrame
{
     JComboBox      JCOrder,JCFilter;
     JButton        BApply;
     JPanel         TopPanel,BottomPanel;
     JPanel         DatePanel,FilterPanel,SortPanel,BasisPanel,ApplyPanel;
     
     TabReport      tabreport;
     DateField      TStDate;
     DateField      TEnDate;
     
     JRadioButton   JRPeriod,JRNo;
     NextField      TStNo,TEnNo;
     
     Object         RowData[][];
     
     String         ColumnData[] = {"Indent Id","Date","Indent No","Code","Name","Indent Qty","Unit","Status"};
     String         ColumnType[] = {"N"        ,"S"   ,"N"        ,"S"   ,"S"   ,"N"        ,"S"    ,"S"     };

     String         SStDate,SEnDate;
     Common         common = new Common();

     Vector         VIndentNo,VIndentDate,VRefNo,VIndentCode,VIndentName,VIndentQty,VIndentUnit,VStatus,VId;
     Vector         VCode,VName,VUom;
     
     JLayeredPane   Layer;
     StatusPanel    SPanel;
     int            iUserCode,iMillCode,iAuthCode;

     public IndentListFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUom,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode)
     {
          super("Issue List During a Period");
          this.Layer     = Layer;
          this.VCode     = VCode;
          this.VName     = VName;
          this.VUom      = VUom;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.iAuthCode = iAuthCode;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     
     public void createComponents()
     {
          TStDate        = new DateField();
          TEnDate        = new DateField();
          TStNo          = new NextField();
          TEnNo          = new NextField();
          BApply         = new JButton("Apply");
          
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          
          JCOrder        = new JComboBox();
          JCFilter       = new JComboBox();
          
          JRPeriod       = new JRadioButton("Periodical",true);
          JRNo           = new JRadioButton("Indent No");
          
          DatePanel      = new JPanel();
          FilterPanel    = new JPanel();
          SortPanel      = new JPanel();
          BasisPanel     = new JPanel();
          ApplyPanel     = new JPanel();
          
          TStDate        . setTodayDate();
          TEnDate        . setTodayDate();
     }
     
     public void setLayouts()
     {
          TopPanel       . setLayout(new GridLayout(1,5));
          
          DatePanel      . setLayout(new GridLayout(3,1));
          SortPanel      . setLayout(new BorderLayout());
          FilterPanel    . setLayout(new BorderLayout());
          BasisPanel     . setLayout(new GridLayout(2,1));
          ApplyPanel     . setLayout(new BorderLayout());
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,610,500);
     }
     
     public void addComponents()
     {
          JCOrder        . addItem("Indent Id");
          JCOrder        . addItem("Indent No");
          JCOrder        . addItem("Materialwise");
          JCOrder        . addItem("Unitwise");
          
          JCFilter       . addItem("All");
          JCFilter       . addItem("Authenticated");
          JCFilter       . addItem("Not Authenticated");
          
          SortPanel      . add("Center",JCOrder);
          FilterPanel    . add("Center",JCFilter);
          
          DatePanel      . add(TStDate);
          DatePanel      . add(TEnDate);
          
          BasisPanel     . add(JRPeriod);
          BasisPanel     . add(JRNo);
          
          ApplyPanel     . add("Center",BApply);
                           
          TopPanel       . add(SortPanel);
          TopPanel       . add(FilterPanel);
          TopPanel       . add(BasisPanel);
          TopPanel       . add(DatePanel);
          TopPanel       . add(ApplyPanel);
          
          SortPanel      . setBorder(new TitledBorder("Sorting"));
          FilterPanel    . setBorder(new TitledBorder("List Only"));
          DatePanel      . setBorder(new TitledBorder("Period"));
          BasisPanel     . setBorder(new TitledBorder("Basis"));
          ApplyPanel     . setBorder(new TitledBorder("Control"));
          
          getContentPane(). add(TopPanel,BorderLayout.NORTH);
          getContentPane(). add(BottomPanel,BorderLayout.SOUTH);
     }
     
     public void addListeners()
     {
          BApply   . addActionListener(new ApplyList());
          JRPeriod . addActionListener(new JRList());
          JRNo     . addActionListener(new JRList());
     }
     
     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    DatePanel .setBorder(new TitledBorder("Periodical"));
                    DatePanel .removeAll();
                    DatePanel .add(TStDate);
                    DatePanel .add(TEnDate);
                    DatePanel .updateUI();
                    JRNo      .setSelected(false);
               }
               else
               {
                    DatePanel .setBorder(new TitledBorder("Numbered"));
                    DatePanel .removeAll();
                    DatePanel .add(TStNo);
                    DatePanel .add(TEnNo);
                    DatePanel .updateUI();
                    JRPeriod  .setSelected(false);
               }
          }
     }
     
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    getContentPane()    .add(tabreport,BorderLayout.CENTER);
                    
                    if(iAuthCode>1)
                    {
                         tabreport.ReportTable.addKeyListener(new KeyList());
                    }
                    setSelected(true);
                    Layer.repaint();
                    Layer.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }
     
     public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
          } 
     }
     
     public void setDataIntoVector()
     {
          VIndentNo      = new Vector();
          VIndentDate    = new Vector();
          VRefNo         = new Vector();
          VIndentCode    = new Vector();
          VIndentName    = new Vector();
          VIndentQty     = new Vector();
          VIndentUnit    = new Vector();
          VStatus        = new Vector();
          VId            = new Vector();
          
          String StDate  = TStDate.toNormal();
          String EnDate  = TEnDate.toNormal();
          
          String QString = getQString(StDate,EnDate);
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();

               ResultSet result  = stat.executeQuery(QString);

               while (result.next())
               {
                    String SStatus = "";

                    String SAuth  = common.parseNull(result.getString(8));
                    
                    if(SAuth.equals("1"))
                    {
                         SStatus = "Authenticated";    
                    }
                    else
                    {
                         SStatus = "Not Authenticated";    
                    }
                    VIndentNo      . addElement(common.parseNull(result.getString(1)));
                    VIndentDate    . addElement(common.parseDate(result.getString(2)));
                    VRefNo         . addElement(common.parseNull(result.getString(3)));
                    VIndentCode    . addElement(common.parseNull(result.getString(4)));
                    VIndentName    . addElement(common.parseNull(result.getString(5)));
                    VIndentQty     . addElement(common.parseNull(result.getString(6)));
                    VIndentUnit    . addElement(common.parseNull(result.getString(7)));
                    VStatus        . addElement(SStatus);
                    VId            . addElement(common.parseNull(result.getString(9)));
               }
          }
          catch(Exception ex){System.out.println(ex);}
     }
     
     public void setRowData()
     {
          RowData     = new Object[VIndentNo.size()][ColumnData.length];

          for(int i=0;i<VIndentNo.size();i++)
          {
               RowData[i][0]  = (String)VIndentNo.elementAt(i);
               RowData[i][1]  = (String)VIndentDate.elementAt(i);
               RowData[i][2]  = (String)VRefNo.elementAt(i);
               RowData[i][3]  = (String)VIndentCode.elementAt(i);
               RowData[i][4]  = (String)VIndentName.elementAt(i);
               RowData[i][5]  = (String)VIndentQty.elementAt(i);
               RowData[i][6]  = (String)VIndentUnit.elementAt(i);
               RowData[i][7]  = (String)VStatus.elementAt(i);
          }
     }
     
     public String getQString(String StDate,String EnDate)
     {
          String QString  = "";

          if(iMillCode == 0)
          {
               if(JRPeriod.isSelected())
               {
                    QString  =     " select Indent.indentno,Indent.indentdate,Indent.uirefno,Indent.code,"+
                                   " Invitems.Item_Name,Indent.qty,Unit.unit_Name,Indent.authentication,Indent.Id"+
                                   " from indent"+
                                   " inner join invitems on invitems.item_code = indent.code"+
                                   " INNER JOIN Unit ON Indent.Unit_Code = Unit.Unit_Code "+
                                   " Where Indent.IndentDate >= '"+StDate+"' and Indent.IndentDate <='"+EnDate+"' "+
                                   " And (Indent.MillCode is Null Or Indent.MillCode = 0) ";
               }
               else
               {
                    QString  =     " select Indent.indentno,Indent.indentdate,Indent.uirefno,Indent.code,"+
                                   " Invitems.Item_Name,Indent.qty,Unit.unit_Name,Indent.authentication,Indent.Id"+
                                   " from indent"+
                                   " inner join invitems on invitems.item_code = indent.code"+
                                   " INNER JOIN Unit ON Indent.Unit_Code = Unit.Unit_Code "+
                                   " Where Indent.UIRefNo >="+TStNo.getText()+" and Indent.UIRefNo <= "+TEnNo.getText() +
                                   " And (Indent.MillCode is Null Or Indent.MillCode = 0) ";
               }
          }
          else
          {
               if(JRPeriod.isSelected())
               {
                    QString  =     " select Indent.indentno,Indent.indentdate,Indent.uirefno,Indent.code,"+
                                   " Invitems.Item_Name,Indent.qty,Unit.unit_Name,Indent.authentication,Indent.Id"+
                                   " from indent"+
                                   " inner join invitems on invitems.item_code = indent.code"+
                                   " INNER JOIN Unit ON Indent.Unit_Code = Unit.Unit_Code "+
                                   " Where Indent.IssueDate >= '"+StDate+"' and Indent.IssueDate <='"+EnDate+"' "+
                                   " And Indent.MillCode = 1 ";
               }
               else
               {
                    QString  =     " select Indent.indentno,Indent.indentdate,Indent.uirefno,Indent.code,"+
                                   " Invitems.Item_Name,Indent.qty,Unit.unit_Name,Indent.authentication,Indent.Id"+
                                   " from indent"+
                                   " inner join invitems on invitems.item_code = indent.code"+
                                   " INNER JOIN Unit ON Indent.Unit_Code = Unit.Unit_Code "+
                                   " Where Indent.UIRefNo >="+TStNo.getText()+" and Indent.UIRefNo <= "+TEnNo.getText() +
                                   " And Indent.MillCode = 1 ";
               }
          }
          if(JCFilter.getSelectedIndex() == 1)
               QString = QString+" And Indent.Authentication = 1 ";
          if(JCFilter.getSelectedIndex() == 2)
               QString = QString+" And Indent.Authentication = 0 ";
          
          if(JCOrder.getSelectedIndex() == 0)
               QString = QString+" Order By Indent.IndentNo,Indent.IndentDate";
          if(JCOrder.getSelectedIndex() == 1)
               QString = QString+" Order By Indent.UIRefNo,Indent.IndentDate";
          if(JCOrder.getSelectedIndex() == 2)
               QString = QString+" Order By InvItems.Item_Name,Indent.IndentDate";
          if(JCOrder.getSelectedIndex() == 3)
               QString = QString+" Order By Unit.Unit_Name,Indent.IndentDate";

          return QString;
     }
}
