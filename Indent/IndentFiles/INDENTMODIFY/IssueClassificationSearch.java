package Indent.IndentFiles.Modify;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class IssueClassificationSearch
{
     JLayeredPane        Layer;
     Vector              VClass,VClassCode;
     JTable              theTable;
     JList               BrowList;
     JScrollPane         BrowScroll;
     JPanel              LeftPanel;
     JInternalFrame      ClassFrame;
     JTextField          TIndicator;
     int                 iSelectedRow,iRow;
     String              str="";
     Common              common = new Common();
     IssueMiddlePanel    IMP;
     MouseEvent          me;
     
     IssueClassificationSearch(JLayeredPane Layer,IssueMiddlePanel  IMP)
     {
          this.Layer          = Layer;
          this.IMP            = IMP;
          BrowList            = new JList(IMP.dataUse.getClassName());
          BrowScroll          = new JScrollPane(BrowList);
          LeftPanel           = new JPanel(true);
          TIndicator          = new JTextField();
          TIndicator          . setEditable(false);
          ClassFrame          = new JInternalFrame("Classification Selector");
          ClassFrame          . show();
          ClassFrame          . setBounds(80,100,550,350);
          ClassFrame          . setClosable(true);
          ClassFrame          . setResizable(true);
          BrowList            . addKeyListener(new KeyList());
          ClassFrame          . getContentPane().setLayout(new BorderLayout());
          ClassFrame          . getContentPane().add("South",TIndicator);
          ClassFrame          . getContentPane().add("Center",BrowScroll);
          ClassFrame          . show();
     }
     
     public void showClassificationFrame(MouseEvent me)
     {
          this.me = me; 
          removeHelpFrame();
          try
          {
               Layer     .add(ClassFrame);
               ClassFrame.moveToFront();
               ClassFrame.setSelected(true);
               ClassFrame.show();
               BrowList  .requestFocus();
          }
          catch(Exception ex){}
     }  
     
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SClassName = (String)IMP.dataUse.setPatClaseName(index);
                    String SClassCode = (String)IMP.dataUse.setPatClassCode(index);
                    addClassDet(SClassName);
                    str="";
                    removeHelpFrame();
               }
          }
     }
     
     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<IMP.dataUse.getClassListSize();index++)
          {
               String str1 = ((String)IMP.dataUse.setPatClaseName(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedValue(str1,true);
                    BrowList.ensureIndexIsVisible(index+5);
                    break;
               }
          }
     }
     
     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(ClassFrame);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
          }
     public void addClassDet(String SClassName)
     {
          try
          {     
               for(int i=0;i<IMP.tabreport.RowData.length;i++)
               {
                    String SCode = ((String)IMP.tabreport.RowData[i][0]).trim();
                    if(SCode.length() == 0)
                         continue;
                    IMP.tabreport.RowData[i][4] = SClassName;
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     } 
}
