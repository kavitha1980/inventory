package Indent.IndentFiles.Modify;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class IssueDepartmentSearch
{
     JLayeredPane        Layer;
     Vector              VDept,VDeptCode;
     JTable              theTable;
     JList               BrowList;
     JScrollPane         BrowScroll;
     JPanel              LeftPanel;
     JInternalFrame      DeptFrame;
     JTextField          TIndicator;
     int                 iSelectedRow,iRow;
     String              str="";
     Common              common = new Common();
     IssueMiddlePanel    IMP;
     MouseEvent          me;
     
     IssueDepartmentSearch(JLayeredPane Layer,IssueMiddlePanel  IMP)
     {
          this.Layer     = Layer;
          this.IMP       = IMP;
          BrowList       = new JList(IMP.dataUse.getDeptName());
          BrowScroll     = new JScrollPane(BrowList);
          LeftPanel      = new JPanel(true);
          TIndicator     = new JTextField();
          TIndicator     .setEditable(false);
          DeptFrame      = new JInternalFrame("Department Selector");
          DeptFrame      .show();
          DeptFrame      .setBounds(80,100,550,350);
          DeptFrame      .setClosable(true);
          DeptFrame      .setResizable(true);
          BrowList       .addKeyListener(new KeyList());
          DeptFrame      .getContentPane().setLayout(new BorderLayout());
          DeptFrame      .getContentPane().add("South",TIndicator);
          DeptFrame      .getContentPane().add("Center",BrowScroll);
          DeptFrame      .show();
     }
     
     public void showDepartmentFrame(MouseEvent me)
     {
          this.me = me; 
          removeHelpFrame();
          try
          {
               Layer     .add(DeptFrame);
               DeptFrame .moveToFront();
               DeptFrame .setSelected(true);
               DeptFrame .show();
               BrowList  .requestFocus();
          }
          catch(Exception ex){}
     }  

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SDeptName = (String)IMP.dataUse.setPatDepName(index);
                    String SDeptCode = (String)IMP.dataUse.setPatDepCode(index);
                    addDeptDet(SDeptName);
                    str="";
                    removeHelpFrame();
               }
          }
     }
     
     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<IMP.dataUse.getDeptListSize();index++)
          {
               String str1 = ((String)IMP.dataUse.setPatDepName(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedValue(str1,true);
                    BrowList.ensureIndexIsVisible(index+5);
                    break;
               }
          }
     }
     
     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(DeptFrame);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }
     
     public void addDeptDet(String SDeptName)
     {
          try
          {     
               for(int i=0;i<IMP.tabreport.RowData.length;i++)
               {
                    String SCode = ((String)IMP.tabreport.RowData[i][0]).trim();
                    if(SCode.length() == 0)
                         continue;
                    IMP.tabreport.RowData[i][3] = SDeptName;
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     } 
}
