package Indent.IndentFiles.Modify;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class  IssueMiddlePanel extends JPanel
{
     IssueTabReport           tabreport;
     Object                   RowData[][],IdData[];
     String                   ColumnData[] = {"Code","Name","UOM","Department","Classification","Stock","Quantity","Rate","Value"};
     String                   ColumnType[] = {"S","S","S","B","B","N","E","N","N"};
     
     JLayeredPane             Layer;
     JComboBox                JCUnit,JCCata,JCDept;
     int                      iUserCode,iMillCode;

     IssueMaterialSearch      MS;
     DataUse                  dataUse;
     Common                   common = new Common();
     
     IssueMiddlePanel(JLayeredPane Layer,JComboBox JCUnit,JComboBox JCCata,int iUserCode,int iMillCode)
     {
          this.Layer     = Layer;
          this.JCUnit    = JCUnit;
          this.JCCata    = JCCata;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          
          setDataintoVectors();
          JCDept       = new JComboBox(dataUse.getDeptName());
          setLayout(new BorderLayout());
     }
     
     public void setDataintoVectors()
     {
          dataUse = new DataUse(iMillCode);
          dataUse . getItemData();
          dataUse . getOtherData();
     }

     public void addListeners()
     {
          try
          {
                         tabreport . ReportTable.addKeyListener(new KeyList());
          JTableHeader   th        = tabreport.ReportTable.getTableHeader();
                         th        . addMouseListener(new IssueTableHeaderHandle(this));
          }catch (Exception ex)
          {
               System.out.println("addLis");
               ex.printStackTrace();
          }
     }
     
     private class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    showMaterialSelectionFrame();
               }
          }
     }
     
     private void showMaterialSelectionFrame()
     {
          try
          {
               Layer.remove(MS);
               Layer.updateUI();
          }
          catch(Exception ex){}
          
          try
          {
               MS        = new IssueMaterialSearch(Layer,this,tabreport.ReportTable.getSelectedRow(),iMillCode);
               MS        .setSelected(true);
               Layer     .add(MS);
               Layer     .repaint();
               MS        .moveToFront();
               Layer     .updateUI();
          }
          catch(Exception ex){}
     }
     
     public void setData(IssueRecords issuerecords)
     {
          RowData    = new Object[issuerecords.VIssueCode.size()+4][ColumnData.length];            
          IdData     = new Object[issuerecords.VIssueCode.size()];
          String CData[] = {"Code","Name","UOM","Department","Classification","Stock","Quantity","Rate","Value"};
          String CType[] = {"S","S","S","B","B","N","E","N","N"};
          
          for(int i=0;i<issuerecords.VIssueCode.size();i++)
          {
               double dIssueQty    = common.toDouble(common.parseNull(((String)issuerecords.VIssueQty    .elementAt(i)).trim()).trim());
               if(dIssueQty<0)
                    dIssueQty      = (-1)*dIssueQty;

               double dIssueValuse = common.toDouble(common.parseNull(((String)issuerecords.VIssueValue  .elementAt(i)).trim()).trim());
               if(dIssueValuse<0)
                    dIssueValuse   = (-1)*dIssueValuse;

               RowData[i][0]  = (String)issuerecords.VIssueCode   .elementAt(i);
               RowData[i][1]  = (String)issuerecords.VIssueName   .elementAt(i);
               RowData[i][2]  = (String)issuerecords.VIssueUom    .elementAt(i);
               RowData[i][3]  = (String)issuerecords.VIssueDept   .elementAt(i);
               RowData[i][4]  = (String)issuerecords.VIssueGroup  .elementAt(i);
               RowData[i][5]  = (String)issuerecords.VStock       .elementAt(i);
               RowData[i][6]  = String.valueOf(dIssueQty);
               RowData[i][7]  = (String)issuerecords.VIssueRate   .elementAt(i);
               RowData[i][8]  = String.valueOf(dIssueValuse);
               IdData[i]      = (String)issuerecords.VId          .elementAt(i);
          }
          
          int j = issuerecords.VIssueCode.size();
          
          for(int i=0;i<4;i++)
          {
               for(int k=0;k<CData.length;k++)
               {
                    RowData[j+i][k] = "";
               }
          }
          try
          {
               tabreport   = new IssueTabReport(RowData,CData,CType,JCCata,JCDept,this,issuerecords);
               add(tabreport,BorderLayout.CENTER);
               addListeners();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}
