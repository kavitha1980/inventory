package Indent.IndentFiles.Modify;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;
import Indent.IndentFiles.*;

public class IssueModiFrame extends JInternalFrame
{
     DateField           TDate;
     NextField           TIndIssNo,TRefNo;
     IssueMiddlePanel    MiddlePanel;
     Common              common = new Common();
     Control             control;
     DataUse             dataUse;
     StatusPanel         SPanel;

     Connection          theConnection  =    null;
     IssueRecords        issuerecords   =    null;

     JLayeredPane        Layer;
     JComboBox           JCUDept,JCUnit,JCCata,JCType;
     JButton             BOk,BCancel;
     JLabel              LIndIss;
     JPanel              TopPanel,BottomPanel;
     int                 iUserCode,iMillCode;
     String              SIssueNo,SIssueDate,SIndentNo;

     Vector              VIndentTypeCode,VIndentTypeName;
     boolean             isReturn       =    false;
     boolean             bComflag       =    true;

     public IssueModiFrame(JLayeredPane Layer,StatusPanel SPanel,int iUserCode,int iMillCode,String SIssueNo,String SIssueDate,String SIndentNo)
     {
          this.Layer     = Layer;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SIssueNo  = SIssueNo;
          this.SIssueDate= SIssueDate;
          this.SIndentNo = SIndentNo;
          
          getDBConnection();
          setDataintoVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setPresets();
     }

     public void getDBConnection()
     {
          try
          {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnection  =    oraConnection.getConnection();
                              theConnection  .    setAutoCommit(false);

          }catch(SQLException SQLE)
          {
               System.out.println("IndentFrame getDBConnections SQLException"+ SQLE);
               SQLE.printStackTrace();
          }
     }

     public void createComponents()
     {
          BOk                 = new JButton("Update");
          BCancel             = new JButton("Cancel");

          dataUse             = new DataUse(iMillCode);
          dataUse             .     getOtherData();

          JCUDept             = new JComboBox(dataUse.getHodName());
          JCUnit              = new JComboBox(dataUse.getUnitName());
          JCCata              = new JComboBox(dataUse.getClassName());
          JCType              = new JComboBox(dataUse.getTypeName());

          JCUnit              . setEnabled(false);
          JCUDept             . setEnabled(false);
          JCType              . setEnabled(false);

          TIndIssNo           = new NextField();
          TRefNo              = new NextField();
          TDate               = new DateField();
          LIndIss             = new JLabel("  Indent No");

          TopPanel            = new JPanel();
          MiddlePanel         = new IssueMiddlePanel(Layer,JCUnit,JCCata,iUserCode,iMillCode);
          BottomPanel         = new JPanel();
          
          control             = new Control();
          TRefNo              . setEditable(false);
          TIndIssNo           . setEditable(false);
          TIndIssNo           . setText(SIssueNo);

          TDate               . setEditable(false);
          BOk                 . setEnabled(false);
     }

     public void setLayouts()
     {
          setTitle("Issue Modification");
          setMaximizable(true);
          setClosable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,795,495);
          
          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(3,2));
          BottomPanel         .setLayout(new FlowLayout());
          TopPanel            .setBorder(new TitledBorder("Control Block"));
     }

     public void addComponents()
     {
          TopPanel         . add(new JLabel(" Date"));
          TopPanel         . add(TDate);
          
          TopPanel         . add(new JLabel("  User Indent No"));
          TopPanel         . add(TRefNo);
          
          TopPanel         . add(new JLabel(" Unit"));
          TopPanel         . add(JCUnit);

          TopPanel         . add(new JLabel("  Indent Type"));
          TopPanel         . add(JCType);

          TopPanel         . add(new JLabel(" User Department"));
          TopPanel         . add(JCUDept);

          TopPanel         . add(LIndIss);
          TopPanel         . add(TIndIssNo);
          
          BottomPanel      . add(BOk);
          
          getContentPane() . add("North",TopPanel);
          getContentPane() . add("Center",MiddlePanel);
          getContentPane() . add("South",BottomPanel);
     }

     public void setPresets()
     {
          issuerecords   = new IssueRecords(SIssueNo,SIssueDate,SIndentNo,iMillCode);
          
          BOk            . setEnabled(true);
          TIndIssNo      . setText(SIssueNo);

          TDate          . fromString(common.parseDate(SIssueDate));
          TRefNo         . setText(SIndentNo);

          JCUnit         . setSelectedItem(issuerecords.SUnit);
          JCUDept        . setSelectedItem(issuerecords.SHodName);

          if(issuerecords.isReturn)
               JCType   . setSelectedIndex(1);
          else
               JCType   . setSelectedIndex(0);
          
          MiddlePanel    . setData(issuerecords);
     }

          /*   Event Related  */

     public void addListeners()
     {
          BOk  .addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(setData())
               {
                    setComOrRol();
                    removeHelpFrame();
               }
               else
               {
                    JOptionPane    .showMessageDialog(null,"The Entry data was not Saved ","InforMation",JOptionPane.INFORMATION_MESSAGE);
                    BOk            .setEnabled(true);
               }
          }
     }

     public void setComOrRol()
     {
          if(bComflag)
          {
               try  {
                         theConnection .commit();
                         JOptionPane.showMessageDialog(null,"Data Saved","InforMation",JOptionPane.INFORMATION_MESSAGE);
               }    catch(Exception ex)
               {    System.out.println("Indent frame actionPerformed ->"+ex);ex.printStackTrace();  }
          }
          else
          {
               try  {
                         theConnection  .rollback();
                         JOptionPane    .showMessageDialog(null,"The Given Data is not Saved Please Enter Correct data","InforMation",JOptionPane.INFORMATION_MESSAGE);
                         BOk            .setEnabled(true);
               }    catch(Exception ex)
               {    System.out.println("Indent frame actionPerformed ->"+ex);ex.printStackTrace();  }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer     .remove(this);
               Layer     .repaint();
               Layer     .updateUI();
          }
          catch(Exception ex) { }
     }

        /*      DataBase Related         */

     private int getIssueID()
     {
          Statement stat      = null;
          ResultSet result    = null;

          int iId=0;
          try
          {
               stat           =  theConnection    .createStatement();
               result         =  stat             .executeQuery("Select Issue_Seq.nextVal from Dual");

               while(result.next())
               {
                    iId  =    result.getInt(1);
               }
               result    .close();
               stat      .close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
               bComflag       =    true;
          }
          return iId;
     }

     public void insertIssueDetails()
     {
          Statement stat = null;
          String    str  = "";

          try
          {
               stat           =  theConnection    .createStatement();
               int j          =  issuerecords     .VIssueCode.size();
               int iSlNo      =  issuerecords     .iMaxSlNo;

               String SHCode  =  String.valueOf(dataUse.getHodCode(((String)JCUDept.getSelectedItem()).trim()));
               String SUCode  =  String.valueOf(dataUse.getUnitCode(((String)JCUnit .getSelectedItem()).trim()));

               for(int i=j;i<MiddlePanel.tabreport.RowData.length;i++)
               {
                    if(!isEligible(i))
                         continue;

                    iSlNo++;
                                      /*Use getValueAt in department because its vary*/

                    double dQty         = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,6));
                    double dRate        = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,7));
                    String SItemCode    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,0));
                    String SItemName    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,1));
                    String SDept        = common.parseNull(((String)MiddlePanel.tabreport.RowData[i][3]).trim());
                    String SCata        = common.parseNull(((String)MiddlePanel.tabreport.RowData[i][4]).trim());


                    if(issuerecords.isReturn)
                    {
                         dQty=dQty*(-1);
                    }
                    str = "";

                    str = "Insert Into Issue(id,IssueNo,IssueDate,Code,HodCode,Qty,IssRate,UIRefNo,Dept_Code,Group_Code,Unit_Code,UserCode,CreationDate,issueValue,SlNo,indenttype,MillCode) Values (";
                    str = str+ getIssueID()+",";
                    str = str+ TIndIssNo.getText()+",";
                    str = str+"'"+TDate.toNormal()+"',";
                    str = str+"'"+SItemCode.trim()+"',";
                    str = str+SHCode+",";
                    str = str+common.getRound(dQty,3)+",";
                    str = str+common.getRound(dRate,4)+",";
                    str = str+"0"+TRefNo.getText()+",";
                    str = str+dataUse.getDeptCode(SDept)+",";
                    str = str+dataUse.getClassCode(SCata)+",";
                    str = str+SUCode+",";
                    str = str+String.valueOf(iUserCode)+",";
                    str = str+"'"+common.getServerDate()+"',";
                    str = str+common.getRound(dQty*dRate,2)+",";
                    str = str+String.valueOf(iSlNo)+",";
                    str = str+getIndentCode(((String)JCType.getSelectedItem()).trim())+",";
                    str = str+String.valueOf(iMillCode)+")";

                    stat.execute(str);
               }
               stat.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
               bComflag       =    false;
          }
     }

     public void updateIssueDetails()
     {
          Statement stat = null;
          String    str  = "";

          try
          {
               stat           =  theConnection    .createStatement();
               int j          =  issuerecords     .VIssueCode.size();

               String SHCode  =  String.valueOf(dataUse.getHodCode(((String)JCUDept.getSelectedItem()).trim()));
               String SUCode  =  String.valueOf(dataUse.getUnitCode(((String)JCUnit .getSelectedItem()).trim()));

               for(int i=0;i<j;i++)
               {
                    if(!isEligible(i))
                         continue;
                                      /*Use getValueAt in department because its vary*/

                    double dQty         = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,6));
                    double dRate        = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,7));
                    String SItemCode    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,0));
                    String SItemName    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,1));
                    String SDept        = common.parseNull(((String)MiddlePanel.tabreport.RowData[i][3]).trim());
                    String SCata        = common.parseNull(((String)MiddlePanel.tabreport.RowData[i][4]).trim());

                    String SSlNo       =  (String)issuerecords     .VIssueSlNo.elementAt(i);

                    if(issuerecords.isReturn)
                    {
                         dQty=dQty*(-1);
                    }
                    str = "";
                    str = " Update issue set ";
                    str = str+"Code          = '"+SItemCode.trim()+"',";
                    str = str+"Qty           = "+common.getRound(dQty,3)+",";
                    str = str+"IssRate       = "+common.getRound(dRate,4)+",";
                    str = str+"Dept_Code     = "+dataUse.getDeptCode(SDept)+",";
                    str = str+"Group_Code    = "+dataUse.getClassCode(SCata)+",";
                    str = str+"UserCode      = "+String.valueOf(iUserCode)+",";
                    str = str+"CreationDate  = '"+common.getServerDate()+"',";
                    str = str+"issueValue    = "+common.getRound(dQty*dRate,2)+" ";
                    str = str+"where issueno = "+SIssueNo;
                    str = str+" and Issue.SlNo ="+SSlNo.trim();

                    stat.execute(str);
               }
               stat.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
               bComflag       =    false;
          }
     }

     private void updateInvitemsInsertData()
     {
          ResultSet result    = null;
          try
          {
               int j          =  issuerecords     .VIssueCode.size();
               Statement stat =  theConnection    .createStatement();

               for(int i=j;i<MiddlePanel.tabreport.RowData.length;i++)
               {     
                    if(!isEligible(i))
                         continue;

                    String SItemName    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,1));
                    String SItemCode    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,0));
                    double dIssueQty    = common.toDouble(common.parseNull(((String)MiddlePanel.tabreport.RowData[i][6]).trim()));
                    double dRate        = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,7));
                    double dIssueVal    = common.toDouble(common.getRound(String.valueOf(dRate*dIssueQty),2));

                    int iCount=0;
                    
                    result = stat.executeQuery("Select count(*) from InvItems Where Item_Code='"+SItemCode+"'");
                    while(result.next())
                    {
                         iCount   =    result.getInt(1);
                    }
                    result.close();
                    
                    if(iCount==0)
                         continue;

                    String QString = "";

                    if(iMillCode==1)
                         QString =   " Update DyeingInvItems";
                    else
                         QString =   " Update InvItems";

                    if(issuerecords.isReturn)
                    {
                         dIssueQty =   (-1)*dIssueQty;
                         dIssueVal =   (-1)*dIssueVal;
                    }

                    QString = QString+" Set IssQty=IssQty+"+dIssueQty+",IssVal=IssVal+"+dIssueVal+" ";
                    QString = QString+" Where Item_Code = '"+SItemCode+"'";

                    stat.execute(QString);
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame updateInvitemsData() ->"+e);
               e.printStackTrace();
               bComflag  = false;
          }
     }

     private void updateInvitemsUpdateData()
     {
          double dIssueQty      = 0;
          double dIssueVal      = 0;
          double dOldIssueQty   = 0;
          double dOldIssueVal   = 0;
          double dNewIssueQty   = 0;
          double dNewIssueVal   = 0;

          String QString        = ""; 
          String QStringNew     = "";

          try
          {
               int j          =  issuerecords     .VIssueCode.size();

               Statement stat      =  theConnection.createStatement();

               for(int i=0;i<j;i++)
               {     
                    QString        = ""; 
                    QStringNew     = ""; 

                    dIssueQty      = 0;
                    dIssueVal      = 0;
                    dOldIssueQty   = 0;
                    dOldIssueVal   = 0;
                    dNewIssueQty   = 0;
                    dNewIssueVal   = 0;

                    if(!isEligible(i))
                         continue;

                    String SOldCode     = (String)issuerecords.VIssueCode.elementAt(i);
                    double dOldQty      = common.toDouble((String)issuerecords.VIssueQty.elementAt(i));
                    double dOldVal      = common.toDouble((String)issuerecords.VIssueValue.elementAt(i));
                    double dOldRate     = common.toDouble((String)issuerecords.VIssueRate.elementAt(i));

                    String SNewCode     = (String)MiddlePanel.tabreport.ReportTable.getValueAt(i,0);
                    double dNewQty      = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,6));
                    double dNewVal      = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,8));
                    double dNewRate     = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,7));

                    if(iMillCode==1)
                    {
                         QString        =   " Update DyeingInvItems";
                         QStringNew     =   " Update DyeingInvItems";
                    }
                    else
                    {
                         QString        =   " Update InvItems";
                         QStringNew     =   " Update InvItems";
                    }

                    if(!issuerecords.isReturn)
                    {
                         if((SNewCode.trim()).equals(SOldCode.trim()))
                         {
                              dIssueQty = common.toDouble(common.getRound(dNewQty - dOldQty,3));
                              dIssueVal = common.toDouble(common.getRound(dIssueQty*dOldRate,2));

                              QString = QString+" Set IssQty=IssQty+"+dIssueQty+",IssVal=IssVal+"+dIssueVal+" ";
                              QString = QString+" Where Item_Code = '"+SOldCode+"'";

                              stat.execute(QString);
                         }
                         else
                         {
                              dOldIssueQty   =    dOldQty;
                              dOldIssueVal   =    dOldQty*dOldRate;
                              
                              dNewIssueQty   =    dNewQty;
                              dNewIssueVal   =    dNewQty*dNewRate;
                              
                              QString        = QString+" Set IssQty=IssQty-"+dOldIssueQty+",IssVal=IssVal-"+dOldIssueVal+" ";
                              QString        = QString+" Where Item_Code = '"+SOldCode+"'";

                              QStringNew     = QStringNew+" Set IssQty=IssQty+"+dNewIssueQty+",IssVal=IssVal+"+dNewIssueVal+" ";
                              QStringNew     = QStringNew+" Where Item_Code = '"+SNewCode+"'";

                              stat .execute(QString);
                              stat.execute(QStringNew);
                         }
                    }
                    else
                    {
                         if((SNewCode.trim()).equals(SOldCode.trim()))
                         {
                              dIssueQty = dNewQty + dOldQty;
                              dIssueVal = dIssueQty*dOldRate;

                              QString = QString+" Set IssQty=IssQty+"+dIssueQty+",IssVal=IssVal+"+dIssueVal+" ";
                              QString = QString+" Where Item_Code = '"+SOldCode+"'";

                              stat.execute(QString);
                         }
                         else
                         {
                              dOldIssueQty   =    dOldQty;
                              dOldIssueVal   =    dOldQty*dOldRate;
                              
                              if(dOldIssueQty<0)
                              {
                                   dOldIssueQty   =    (-1)*dOldIssueQty;
                                   dOldIssueVal   =    (-1)*dOldIssueVal;
                              }

                              dNewIssueQty   =    dNewQty;
                              dNewIssueVal   =    dNewQty*dNewRate;

                              QString        = QString+" Set IssQty=IssQty+"+dOldIssueQty+",IssVal=IssVal+"+dOldIssueVal+" ";
                              QString        = QString+" Where Item_Code = '"+SOldCode+"'";

                              QStringNew     = QStringNew+" Set IssQty=IssQty-"+dNewIssueQty+",IssVal=IssVal-"+dNewIssueVal+" ";
                              QStringNew     = QStringNew+" Where Item_Code = '"+SNewCode+"'";

                              stat .execute(QString);
                              stat.execute(QStringNew);
                         }
                    }
               }
                    stat .close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame updateInvitemsData() ->"+e);
               e.printStackTrace();
               bComflag  = false;
          }
     }

     public boolean setData()
     {
          if(!isStockValid())
          {
               return false;
          }
          
          if(!isDataValid())
          {
               return false;
          }

          try
          {
               insertIssueDetails();
               updateIssueDetails();
               updateInvitemsInsertData();
               updateInvitemsUpdateData();
          }
          catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
          return bComflag;
     }

          /*        Data  Validation          */

     private boolean isStockValid()
     {
          boolean bflag = true;
          
          if(((String)JCType.getSelectedItem()).equals("Return"))
          {
               bflag = true;
          }
          else
          {
               for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
               {
                    String Smsg="";
                    String SCode        = ((String)MiddlePanel.tabreport.RowData[i][0]).trim();
                    double dIssueQty    = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][6]).trim());

                    String SEnDate      = TDate.toNormal();
                    
                    if(SCode.length() == 0)
                         continue;
                    
                    /*Item IC = new Item(SCode,SEnDate,iMillCode);
                    double dStock = common.toDouble(IC.getClStock());

                    if((dStock-dIssueQty)<0)
                         Smsg = Smsg + " Negative Stock for the Date,"; */

                    double dStock = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][5]).trim());

                    if(dStock<0)
                         Smsg = Smsg + " Negative Stock for the Date,"; 

                    if(Smsg.length()>0)
                    {
                         Smsg = Smsg.substring(0,(Smsg.length()-1));
                         Smsg = Smsg + " in S.No - " + (i+1);
                         JOptionPane.showMessageDialog(null,Smsg,"Invalid Quantity",JOptionPane.INFORMATION_MESSAGE);
                         bflag=false;
                    }
               }
          }
          return bflag;
     }

     private boolean isDataValid()
     {
          boolean bflag = true;
          
          for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
          {
               String Smsg="";

               String SCode   = ((String)MiddlePanel.tabreport.RowData[i][0]).trim();
               String SDept   = ((String)MiddlePanel.tabreport.RowData[i][3]).trim();
               String SCata   = ((String)MiddlePanel.tabreport.RowData[i][4]).trim();

               if(i==0)
               {
                    if(SCode.length()==0)
                    {
                         JOptionPane.showMessageDialog(null,"No Data in the Table","Missing",JOptionPane.INFORMATION_MESSAGE);
                         return false;
                    }
               }

               if(SCode.length() == 0)
                    continue;
               
               if(dataUse.getDeptCode(SDept)==0)
                    Smsg = Smsg + "Department,";

               if(dataUse.getClassCode(SCata)==0)
                    Smsg = Smsg + " Classification,";

               if(!isEligible(i))
                    Smsg = Smsg + " Quantity,";

               if(Smsg.length()>0)
               {
                    Smsg = Smsg.substring(0,(Smsg.length()-1));
                    Smsg = Smsg + "  Field(s) Missing in S.No - " + (i+1);
                    JOptionPane.showMessageDialog(null,Smsg,"Missing Fields",JOptionPane.INFORMATION_MESSAGE);
                    bflag=false;
               }
          }
          return bflag;
     }

     private boolean isEligible(int iRowCount)
     {
          double dQty            = common.toDouble(common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(iRowCount,6)));
          String SItemCode    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(iRowCount,0));

          if(dQty==0)
               return false;
          if(SItemCode.length()<3)
               return false;

          return true;
     }

     public int getIndentCode(String SIndentName)
     {
          int iIndex    = VIndentTypeName.indexOf(SIndentName.trim());

          return common.toInt(((String)VIndentTypeCode.elementAt(iIndex)).trim());
     }

     public void setDataintoVector()
     {
          VIndentTypeCode     = new Vector();
          VIndentTypeName     = new Vector();

          String QS      = "Select code,name from indenttype where (millcode="+iMillCode +" or millcode = 2) and  issueflag = 0";

          try  {
                    ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                    Connection     theConnection  = oraConnection.getConnection();   
                    Statement      stat           = theConnection.createStatement();
     
                    ResultSet result = stat.executeQuery(QS);


                    while(result.next())
                    {
                         VIndentTypeCode     .addElement(common.parseNull(result.getString(1)));
                         VIndentTypeName     .addElement(common.parseNull(result.getString(2)));
                    }
                    result    .close();
                    stat      .close();
          } catch(Exception Ex)
          {
               System.out.println("getIndentCode from Indent "+Ex);
          }
     }
}
