package Indent.IndentFiles.Modify;

import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class IssueRecords
{
     String SRefNo = "",SUnit = "";
     Vector VIssueCode,VIssueName,VIssueUom,VIssueDept,VIssueGroup;
     Vector VStock,VIssueQty,VIssueRate,VIssueValue,VId,VIssueSlNo;
     int iMaxSlNo = 0;
     boolean isReturn = false;
     
     Common common = new Common();
     String SIssueNo,SIssueDate,SIndentNo,SHodName;
     int iMillCode;
     
     IssueRecords(String SIssueNo,String SIssueDate,String SIndentNo,int iMillCode)
     {
          this.SIssueNo       = SIssueNo;
          this.SIssueDate     = SIssueDate;
          this.SIndentNo      = SIndentNo;
          this.iMillCode      = iMillCode;
          
          VIssueCode          = new Vector();
          VIssueName          = new Vector();
          VIssueUom           = new Vector();
          VIssueDept          = new Vector();
          VIssueGroup         = new Vector();
          VStock              = new Vector();
          VIssueQty           = new Vector();
          VIssueRate          = new Vector();
          VIssueValue         = new Vector();
          VId                 = new Vector();
          VIssueSlNo          = new Vector();
          
          ResultSet result    = null;
          String QS           = "";
          String SQuery       = "";

          if(iMillCode == 0)
          {
               SQuery =  " SELECT max(Issue.SlNo) From "+
                         " Issue   "+
                         " Where Issue.IssueNo = "+SIssueNo+
                         " And Issue.IssueDate='"+SIssueDate+"'"+
                         " And Issue.UIRefNo="+SIndentNo+
                         " And (Issue.MillCode = 0 Or Issue.MillCode is Null) ";
          }
          else
          {
               SQuery =  " SELECT max(Issue.SlNo) From "+
                         " Issue   "+
                         " Where Issue.IssueNo = "+SIssueNo+
                         " And Issue.IssueDate='"+SIssueDate+"'"+
                         " And Issue.UIRefNo="+SIndentNo+
                         " And Issue.MillCode = 1 ";
          }

          if(iMillCode == 0)
          {
               QS =      " SELECT Issue.IssueDate, Issue.UIRefNo, Unit.Unit_Name, "+
                         " Issue.Code, InvItems.Item_Name, UOM.UoMName, "+
                         " Dept.Dept_Name, Cata.Group_Name, Issue.Qty, "+
                         " Issue.IssRate, Issue.Id,Hod.HodName,issue.SlNo "+
                         " From (((((Issue "+
                         " INNER JOIN Hod ON Issue.HodCode=Hod.HodCode "+
                         " And Issue.IssueNo = "+SIssueNo+
                         " And Issue.IssueDate='"+SIssueDate+"'"+
                         " And Issue.UIRefNo="+SIndentNo+
                         " And (Issue.MillCode = 0 Or Issue.MillCode is Null)) "+
                         " INNER JOIN Unit ON Issue.Unit_Code=Unit.Unit_Code) "+
                         " INNER JOIN InvItems ON Issue.Code=InvItems.Item_Code) "+
                         " INNER JOIN UOM ON UOM.UOMCode=InvItems.UOMCode) "+
                         " INNER JOIN Dept ON Issue.Dept_Code=Dept.Dept_code) "+
                         " INNER JOIN Cata ON Issue.Group_Code=Cata.Group_Code ";
          }
          else
          {
               QS =      " SELECT Issue.IssueDate, Issue.UIRefNo, Unit.Unit_Name, "+
                         " Issue.Code, InvItems.Item_Name, UOM.UoMName, "+
                         " Dept.Dept_Name, Cata.Group_Name, Issue.Qty, "+
                         " Issue.IssRate, Issue.Id,Hod.HodName,Issue.SlNo "+
                         " From (((((Issue "+
                         " INNER JOIN Hod ON Issue.HodCode=Hod.HodCode "+
                         " And Issue.IssueNo = "+SIssueNo+
                         " And Issue.IssueDate='"+SIssueDate+"'"+
                         " And Issue.UIRefNo="+SIndentNo+
                         " And Issue.MillCode = 1) "+
                         " INNER JOIN Unit ON Issue.Unit_Code=Unit.Unit_Code) "+
                         " INNER JOIN InvItems ON Issue.Code=InvItems.Item_Code) "+
                         " INNER JOIN UOM ON UOM.UOMCode=InvItems.UOMCode) "+
                         " INNER JOIN Dept ON Issue.Dept_Code=Dept.Dept_code) "+
                         " INNER JOIN Cata ON Issue.Group_Code=Cata.Group_Code ";
          }

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();

               result  = stat.executeQuery(QS);

               while (result.next())
               {
                    SIssueDate          = common.parseDate(result.getString(1));
                    SRefNo              = common.parseNull(result.getString(2));
                    SUnit               = common.parseNull(result.getString(3));
                    SHodName            = common.parseNull(result.getString(12));

                    double dQty         = common.toDouble(result.getString(9));
                    double dRate        = common.toDouble(result.getString(10));
                    
                    String SItemCode    = result.getString(4);
                    Item      IC        = new Item(SItemCode,iMillCode);
                    
                    double dStock       = common.toDouble(IC.getClStock());
                    String SStock       = common.getRound(dStock,3);
                    double dValue       = dQty*dRate;
                    String SValue       = common.getRound(dValue,2);
                    
                    VIssueCode          . addElement(SItemCode);
                    VIssueName          . addElement(common.parseNull(result.getString(5)));
                    VIssueUom           . addElement(common.parseNull(result.getString(6)));
                    VIssueDept          . addElement(common.parseNull(result.getString(7)));
                    VIssueGroup         . addElement(common.parseNull(result.getString(8)));
                    VStock              . addElement(SStock);
                    VIssueQty           . addElement(""+dQty);
                    VIssueRate          . addElement(""+dRate);
                    VIssueValue         . addElement(SValue);
                    VId                 . addElement(common.parseNull(result.getString(11)));
                    VIssueSlNo          . addElement(common.parseNull(result.getString(13)));
                    if(dQty<0)
                         isReturn = true;
               }
               result.close();
               result  = stat.executeQuery(SQuery);
               result.next();
               iMaxSlNo  =    common.toInt(((String)result.getString(1)).trim());
               result.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}
