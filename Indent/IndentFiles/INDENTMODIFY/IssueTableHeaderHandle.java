package Indent.IndentFiles.Modify;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class IssueTableHeaderHandle extends MouseAdapter
{
     IssueDepartmentSearch     deptSearch; 
     IssueClassificationSearch classSearch;
     IssueMiddlePanel IMP;
     MouseEvent me;
     
     public IssueTableHeaderHandle(IssueMiddlePanel IMP)
     {
          this.IMP      =  IMP;

          deptSearch  = new IssueDepartmentSearch(IMP.Layer,IMP);
          classSearch = new IssueClassificationSearch(IMP.Layer,IMP);
     }
     
     public void mouseClicked(MouseEvent me)
     {
          this.me  = me;
          String Scol = findColumnClicked(me);
          if(Scol.equals("Department"))
               deptSearch.showDepartmentFrame(me); 
          else
          if(Scol.equals("Classification"))
               classSearch.showClassificationFrame(me); 
     }
     
     private String findColumnClicked(MouseEvent me)
     {
          String Scol="";
          TableColumnModel colModel = IMP.tabreport.ReportTable.getColumnModel();
          int iCol = colModel.getColumnIndexAtX(me.getX());
          Scol = IMP.ColumnData[iCol];
          return Scol;
     }
}
