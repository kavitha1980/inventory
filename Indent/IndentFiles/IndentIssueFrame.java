package Indent.IndentFiles;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;


public class IndentIssueFrame extends JInternalFrame
{
     Connection          theConnection  =    null;
     String    SDate;
     JPanel    TopPanel,BottomPanel,MiddlePanel;
     
     DateField TDate;
     
     boolean bComflag = true;
     
     Common common = new Common();

     JTable          theTable;
     JButton         BRefresh;
     IndentListModel theModel;

     
     JLayeredPane   Layer;
     StatusPanel    SPanel;
     Vector         VCode,VName,VUomName;
     int            iUserCode,iMillCode;
     IssueFrame issueframe=null;
     String     SItemTable,SSupTable,SYearCode;
     Vector     VAuthUserCode;

    ArrayList theTemplateList,theMaterialIssueTemplateList;

     public IndentIssueFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUomName,StatusPanel SPanel,int iUserCode,int iMillCode,String SItemTable,String SSupTable,String SYearCode,ArrayList theTemplateList,ArrayList theMaterialIssueTemplateList)
     {
          super("Issue Authentication Utility");
          this.Layer     = Layer;
          this.VCode     = VCode;
          this.VName     = VName;
          this.VUomName  = VUomName;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SItemTable= SItemTable;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;

          this.theTemplateList              = theTemplateList;
          this.theMaterialIssueTemplateList = theMaterialIssueTemplateList;


          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          setData();
     }
     public void getDBConnection()
     {
          try
          {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnection  =    oraConnection.getConnection();
                              theConnection  .    setAutoCommit(false);

          }catch(SQLException SQLE)
          {
               System.out.println("IndentFrame getDBConnections SQLException"+ SQLE);
               SQLE.printStackTrace();
          }
     }

     public void createComponents()
     {
          
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          MiddlePanel    = new JPanel();



          theModel       = new IndentListModel();
          theTable       = new JTable(theModel);


          BRefresh       = new JButton(" Refresh ");
     }
     
     public void setLayouts()
     {
          TopPanel       .setLayout(new FlowLayout());
          BottomPanel    .setLayout(new FlowLayout());
          MiddlePanel    .setLayout(new BorderLayout());


          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);

     }
     
     public void addComponents()
     {
          MiddlePanel.add(new JScrollPane(theTable));
          BottomPanel.add(BRefresh);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add("South",BottomPanel);
     }

     public void addListeners()
     {
          theTable.addKeyListener(new KeyList(this));
          BRefresh.addActionListener(new ActList());
     }

     private class KeyList extends KeyAdapter
     {
          IndentIssueFrame indentissueframe;
          public KeyList(IndentIssueFrame indentissueframe)
          {
               this.indentissueframe = indentissueframe;
          }

          public void keyReleased(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    int iRow = theTable.getSelectedRow();

                    if(iRow<0)
                         return;


                    int iIndentNo       = common.toInt((String)theModel.getValueAt(iRow,1));
                    String  SIndentDate = (String)theModel.getValueAt(iRow,2);
                    String  SDept       = (String)theModel.getValueAt(iRow,3);
                    String  SUnit       = (String)theModel.getValueAt(iRow,4);
                    String  SIndentType = (String)theModel.getValueAt(iRow,8);   

                    String  SAuthUserCode = (String)VAuthUserCode.elementAt(iRow);


                    if(checkControlTime(0))
                    {
                         JOptionPane.showMessageDialog(null," Cannot Issue Material Time Expired " );
                         return;
                    }
                    try
                    {
                         Layer.remove(issueframe);
                    }
                    catch(Exception ex)
                    {
                    }

                    try
                    {
                         issueframe = new IssueFrame(Layer,iIndentNo,iMillCode,SDept,SUnit,SIndentDate,SItemTable,SSupTable,SYearCode,SAuthUserCode,SIndentType,indentissueframe,iUserCode,theTemplateList,theMaterialIssueTemplateList);
                         Layer.add(issueframe);
                         Layer.repaint();
                         issueframe.setSelected(true);
                         Layer.updateUI();
                         issueframe.show();
                    }catch(Exception e)
                    {
                        System.out.println(e);
                    }
               }


          }
     }



     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
               Layer.repaint();
          }
          catch(Exception ex){}
     }
     public void setData()
     {
          theModel.setNumRows(0);

          VAuthUserCode = new Vector();
          try
          {
               Connection theConnection=null;

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(getIndentQuery());
               int SlNo=1;

               while(result.next())
               {
				   
				    System.out.println("comming issue frame");
				   
                    Vector theVect = new Vector();

                    String SIndentNo    = common.parseNull(result.getString(1));
                    String SIndentDate  = common.parseDate(result.getString(2));
                    String SDept        = common.parseNull(result.getString(3));
                    String SUnit        = common.parseNull(result.getString(4));
                    double dIndentQty   = result.getDouble(5);
                    double dIssueQty    = result.getDouble(6);
                    double dBalance     = dIndentQty-dIssueQty;
                    VAuthUserCode.addElement(result.getString(7));
                    String SIndentType  = result.getString(8);

                    theVect.addElement(String.valueOf(SlNo));
                    theVect.addElement(SIndentNo);
                    theVect.addElement(SIndentDate);
                    theVect.addElement(common.parseNull(SDept));
                    theVect.addElement(SUnit);
                    theVect.addElement(String.valueOf(dIndentQty));
                    theVect.addElement(String.valueOf(dIssueQty));
                    theVect.addElement(common.getRound(dBalance,3));
                    theVect.addElement(SIndentType);

                    theModel.appendRow(theVect);
                    SlNo++;
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private String getIndentQuery()
     {
          String QS =" select IndentNo,AuthDate,RawUser.UserName as DeptName,Unit.Unit_Name,"+
                     " sum(Qty),sum(issueQty),"+
                     " Indent.AuthUserCode,indentType.Name from Indent"+
                     " Inner join RawUser on RawUser.UserCode=Indent.AuthUserCode"+
                     " Inner join IndentType on IndentType.Code=Indent.IndentType"+
                     " Inner join Unit on unit.Unit_Code=Indent.Unit_Code"+
                     " Where issueStatus=0 and Indent.MillCode="+iMillCode+" and Indent.Status=1 and Indent.CancelStatus=0"+
                     " group by AuthDate,IndentNo,RawUser.UserName,Unit.Unit_Name,Indent.AuthUsercode,IndentType.Name"+
                     " having sum(Qty)-sum(issueQty)>0"+
                     " order by 1";

          return QS;
     }
     private class ActList implements ActionListener
     {
        public void actionPerformed(ActionEvent ae)
        {
            setData();
        }
     }
     public boolean checkControlTime(int iTypeCode)
     {
          int iValue =0;
          boolean bFlag = false;

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(getControlTimeQS(iTypeCode));

               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result.close();
               theStatement.close();

               if(iValue<0)
                  return true;

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               return true;
          }

          return false;
     }

     private String getControlTimeQS(int iTypeCode)
     {

          String QS=" Select (to_date(controlTime,'hh24:mi')-(to_date(to_Char(sysdate,'hh24:mi'),'hh24:mi')))*(24*60) as Diff from"+
                    " TimeControl"+
                    " Where TypeCode="+iTypeCode;


          return QS;
     }



}
