package Indent.IndentFiles;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class IssueAuthenticationFrame extends JInternalFrame
{
     Connection          theConnection  =    null;
     String    SDate;
     JButton   BApply,BOk;
     JPanel    TopPanel,BottomPanel;
     
     TabReport tabreport;
     DateField TDate;
     
     Object RowData[][];
     
     //  Issue Id = Issue No    Issue No = User Indent No

     boolean bComflag = true;
     
     String ColumnData[] = {"Authentication","Issue Id","Date","Issue No","Unit"};
     String ColumnType[] = {"B","N","S","N","S"};
     Common common = new Common();
     Vector VIssueNo,VIssueDate,VRefNo,VIssueUnit;
     
     JLayeredPane   Layer;
     StatusPanel    SPanel;
     Vector         VCode,VName,VUomName;
     int            iUserCode,iMillCode;
     
     public IssueAuthenticationFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUomName,StatusPanel SPanel,int iUserCode,int iMillCode)
     {
          super("Issue Authentication Utility");
          this.Layer     = Layer;
          this.VCode     = VCode;
          this.VName     = VName;
          this.VUomName  = VUomName;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          
          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void getDBConnection()
     {
          try
          {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnection  =    oraConnection.getConnection();
                              theConnection  .    setAutoCommit(false);

          }catch(SQLException SQLE)
          {
               System.out.println("IndentFrame getDBConnections SQLException"+ SQLE);
               SQLE.printStackTrace();
          }
     }

     public void createComponents()
     {
          TDate          = new DateField();
          BApply         = new JButton("Apply");
          BOk            = new JButton("Authenticate");
          
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          
          TDate          .setTodayDate();
          BOk            .setEnabled(false);
     }
     
     public void setLayouts()
     {
          TopPanel       .setLayout(new FlowLayout());
          BottomPanel    .setLayout(new FlowLayout());
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          TopPanel       .add(new JLabel("As On "));
          TopPanel       .add(TDate);
          TopPanel       .add(BApply);
          BottomPanel    .add(BOk);
          
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply   .addActionListener(new ApplyList());
          BOk      .addActionListener(new ActList());
     }
     
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    getContentPane()    .add(tabreport,BorderLayout.CENTER);
                    tabreport           .ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
                                        setSelected(true);
                    Layer               .repaint();
                    Layer               .updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
               BOk.setEnabled(true);
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk  .setEnabled(false);
               authenticateMarkedRecords();
               getCommitStatus();
               removeHelpFrame();
          }
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
               Layer.repaint();
          }
          catch(Exception ex){}
     }

     private void authenticateMarkedRecords()
     {
          String QS="";
          try
          {
               Statement      stat          =  theConnection.createStatement();
               for(int i=0;i<RowData.length;i++)
               {
                    Boolean bValue = (Boolean)RowData[i][0];
                    if(!bValue.booleanValue())
                         continue;
                    
                    String SIssueNo = (String)VIssueNo.elementAt(i);

                    QS = " Update Issue Set Authentication=1 Where IssueNo="+SIssueNo+
                         " And MillCode = "+iMillCode;

                    try
                    {
                         stat.execute(QS);
                    }
                    catch(Exception ex)
                    {
                         JOptionPane.showMessageDialog(null,"Problem in Authentication","Attention",JOptionPane.INFORMATION_MESSAGE);
                         return;
                    }
               }
               stat .close();
          }
          catch(Exception ex)
          {
             bComflag    = false;
             System.out.println(ex);
          }
     }

     public void getCommitStatus()
     {
          if(bComflag)
          {
               try  {
                         theConnection .commit();
                         JOptionPane.showMessageDialog(null,"Data Saved","InforMation",JOptionPane.INFORMATION_MESSAGE);
               }    catch(Exception ex)
               {    System.out.println("Indent frame actionPerformed ->"+ex);ex.printStackTrace();  }
          }
          else
          {
               try  {
                         theConnection  .rollback();
                         JOptionPane    .showMessageDialog(null,"The Given Data is not Saved","InforMation",JOptionPane.INFORMATION_MESSAGE);
                         BOk            .setEnabled(true);
               }    catch(Exception ex)
               {    System.out.println("Indent frame actionPerformed ->"+ex);ex.printStackTrace();  }
          }
     }
     
     public void setDataIntoVector()
     {
          VIssueNo     = new Vector();
          VIssueDate   = new Vector();
          VRefNo       = new Vector();
          VIssueUnit   = new Vector();
          
          String SDate = TDate.toNormal();
          
          String QString = getQString(SDate);
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();   
               Statement      stat          =  theConnection.createStatement();
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    String str1  = res.getString(1);  
                    String str2  = res.getString(2);
                    String str3  = res.getString(3);
                    String str4  = res.getString(4);
                    
                    VIssueNo     .addElement(str1);
                    VIssueDate   .addElement(common.parseDate(str2));
                    VRefNo       .addElement(str3);
                    VIssueUnit   .addElement(common.parseNull(str4));
               }
               res       .close();
               stat      .close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VIssueNo.size()][ColumnData.length];
          for(int i=0;i<VIssueNo.size();i++)
          {
               RowData[i][0]  = new Boolean(true);
               RowData[i][1]  = (String)VIssueNo.elementAt(i);
               RowData[i][2]  = (String)VIssueDate.elementAt(i);
               RowData[i][3]  = (String)VRefNo.elementAt(i);
               RowData[i][4]  = (String)VIssueUnit.elementAt(i);
          }
     }

     public String getQString(String SDate)
     {
          String QString  = "";
          
          QString  =     " SELECT Issue.IssueNo, Issue.IssueDate, Issue.UIRefNo,Unit.Unit_Name"+
                         " FROM Issue "+
                         " INNER JOIN Unit ON Issue.Unit_Code = Unit.Unit_Code "+
                         " Where Issue.IssueDate <='"+SDate+"' "+
                         " And Issue.Authentication = 0 "+
                         " And Issue.MillCode="+iMillCode+
                         " Group by Issue.IssueNo, Issue.IssueDate, Issue.UIRefNo,Unit.Unit_Name "+
                         " Order by Issue.IssueDate,Issue.IssueNo ";

          return QString;
     }
}
