package Indent.IndentFiles;

import javax.swing.table.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import com.digitalpersona.onetouch.*;
import com.digitalpersona.onetouch.capture.*;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.*;
import com.digitalpersona.onetouch.verification.*;

public class IssueFrame extends JInternalFrame
{
     int                 i;

     JComboBox           JCType,JCCata;

     MyLabel             LDept,LUnit,LDate,LStatus;


     NextField           TIndIssNo,TRefNo;
     JTextField          TValue;
     Vector              VUDept,VUDeptCode,VUnit,VUnitCode,VCata,VCataCode;
     JButton             BOk,BCancel,BPunch     ;
     JPanel              TopPanel,BottomPanel,MiddlePanel;

     JTable              theTable;
     IssueListModel      theModel;
     IndentIssueFrame    indentissueframe;

     
     JLayeredPane        Layer;
     Common              common = new Common();
     Control             control;
     MyLabel             LPerson;

     String              SIndNo = "",SIssueNo = "";
     Vector              VName,VCode,VUOMName,VStkGpCode,VNameCode,VStockUserCode;
     Vector              VIssueTypeCode,VIssueTypeName;
     Vector              VIndentTypeCode,VIndentTypeName;

     StatusPanel         SPanel;
     int                 iUserCode,iMillCode;
     String              SYearCode,SItemTable;

     Connection          theMConnection =    null;
     Connection          theDConnection =    null;
     Connection          theConnection1 =    null;

     boolean             isReturn       =    false;
     boolean             bComflag       =    true;
     int                 iIndentNo;
     String              SDept,SUnit,SIndentDate,SSupTable,SAuthUserCode,SIndentType;

     ArrayList theTemplateList,theMaterialIssueTemplateList;
     private DPFPCapture capturer = DPFPGlobal.getCaptureFactory().createCapture();

//     private DPFPVerification verificator = DPFPGlobal.getVerificationFactory().createVerification();

     DPFPTemplate template;
     private JTextField prompt = new JTextField();
     private JTextArea log = new JTextArea();
     private JTextField status = new JTextField("[status line]");
     int iPunchStatus=0;
     int iProcessStatus=0;
     public IssueFrame(JLayeredPane Layer,int iIndentNo,int iMillCode,String SDept,String SUnit,String SIndentDate,String SItemTable,String SSupTable,String SYearCode,String SAuthUserCode,String SIndentType ,IndentIssueFrame indentissueframe)
     {
          this.Layer          = Layer;
          this.iIndentNo      = iIndentNo;
          this.iMillCode      = iMillCode;
          this.SDept          = SDept;
          this.SUnit          = SUnit;
          this.SIndentDate    = SIndentDate;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;
          this.SYearCode      = SYearCode;
          this.SAuthUserCode  = SAuthUserCode;
          this.SIndentType    = SIndentType;
          this.indentissueframe= indentissueframe;



          try
          {

               getDBConnections();
               getUsers(theMConnection);
               setIndentCode();
               createComponents();
               setLayouts();
               addComponents();    
               addListeners();
               setIssueModel();

          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     public IssueFrame(JLayeredPane Layer,int iIndentNo,int iMillCode,String SDept,String SUnit,String SIndentDate,String SItemTable,String SSupTable,String SYearCode,String SAuthUserCode,String SIndentType ,IndentIssueFrame indentissueframe,int iUserCode,ArrayList theTemplateList,ArrayList theMaterialIssueTemplateList)
     {
          this.Layer          = Layer;
          this.iIndentNo      = iIndentNo;
          this.iMillCode      = iMillCode;
          this.SDept          = SDept;
          this.SUnit          = SUnit;
          this.SIndentDate    = SIndentDate;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;
          this.SYearCode      = SYearCode;
          this.SAuthUserCode  = SAuthUserCode;
          this.SIndentType    = SIndentType;
          this.indentissueframe= indentissueframe;
          this.iUserCode       = iUserCode;




          this.theTemplateList              = theTemplateList;
          this.theMaterialIssueTemplateList = theMaterialIssueTemplateList;
          try
          {

               getDBConnections();
               getUsers(theMConnection);
               setIndentCode();
               createComponents();
               setLayouts();
               addComponents();    
               addListeners();
               setIssueModel();
          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void getDBConnections()
     {
          ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                         theMConnection = oraConnection.getConnection();

          if(iMillCode==1)
          {
               DORAConnection DoraConnection = DORAConnection.getORAConnection();
                              theDConnection = DoraConnection.getConnection();
          }
     }

     public void createComponents()
     {
          try
          {
               BOk            = new JButton("Okay");
               BPunch         = new JButton("Punch");
               BCancel        = new JButton("Exit");

               LDept          = new MyLabel("");
               LUnit          = new MyLabel("");
               LDate          = new MyLabel("");
               LStatus        = new MyLabel("");

               JCType         = new JComboBox(VIndentTypeName);
               JCCata         = new JComboBox();
               TIndIssNo      = new NextField();
               TRefNo         = new NextField();
               TValue         = new JTextField(10);

               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();         // IndentMiddlePanel(Layer,VCode,VName,VUOMName,VNameCode,JCUnit,VUnitCode,JCCata,TValue,VCata,VCataCode,iUserCode,iMillCode,SItemTable,SSupTable);
               BottomPanel    = new JPanel();
               control        = new Control();


               TRefNo    . setEditable(false);
               TRefNo    .setText(""+iIndentNo);


               theModel       = new IssueListModel();
               theTable       = new JTable(theModel)
               {
                 public Component prepareRenderer
                  (TableCellRenderer renderer, int index_row, int index_col){
                 Component comp = super.prepareRenderer(renderer, index_row, index_col);


                 if(index_col == 10){              //  && !isCellSelected(index_row, index_col)
                 comp.setBackground(Color.lightGray);
                 comp.setForeground(Color.black);

                 }
                 if(index_col == 15){              //  && !isCellSelected(index_row, index_col)
                 comp.setBackground(Color.green);
                 comp.setForeground(Color.black);
                 }

                 if(index_col!=10 && index_col!=15){
                 comp.setBackground(Color.white);
                 comp.setForeground(Color.black);

                 }
                 return comp;
                 }
                 };

               for(int i=0;i<theModel.ColumnName.length;i++)
               {
                    (theTable.getColumnModel()).getColumn(i).setPreferredWidth(theModel.ColumnWidth[i]);
               }


               LDate.setText(SIndentDate);
               LDept.setText(SDept);
               LUnit.setText(SUnit);
               LStatus.setText(" Punch And Take Issue");
               LStatus.setFont(new Font("courier New",Font.BOLD,19));
               LStatus.setForeground(Color.RED);


               TValue    . setEditable(false);
               TValue    . setHorizontalAlignment(JTextField.RIGHT);
               TValue    . setBorder(new TitledBorder("Net Value of this Document"));

               JCType.setSelectedItem(SIndentType);
               JCType.setEnabled(false);
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setLayouts()
     {
          try
          {
               setTitle("Issue Against Indent");
               setMaximizable(true);
               setClosable(false);
               setIconifiable(true);
               setResizable(true);
               setBounds(0,0,895,595);

               getContentPane()    .setLayout(new BorderLayout());
               TopPanel            .setLayout(new GridLayout(3,2));
               BottomPanel         .setLayout(new GridLayout(1,2));
               TopPanel            .setBorder(new TitledBorder("Control Block"));

               MiddlePanel         .setLayout(new BorderLayout());
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void addComponents()
     {
          TopPanel         . add(new JLabel(" Date"));
          TopPanel         . add(LDate);

          TopPanel         . add(new JLabel(" Indent No"));
          TopPanel         . add(TRefNo);

          TopPanel         . add(new JLabel(" Unit"));
          TopPanel         . add(LUnit);

          TopPanel         . add(new JLabel("  Indent Type"));
          TopPanel         . add(JCType);

          TopPanel         . add(new JLabel(" User "));
          TopPanel         . add(LDept);

          TopPanel         . add(BPunch);
          TopPanel         . add(LStatus);


          MiddlePanel      . add(new JScrollPane(theTable));


          BottomPanel      . add(TValue);
          BottomPanel      . add(BCancel);

          getContentPane() . add("North",TopPanel);
          getContentPane() . add("Center",MiddlePanel);
          getContentPane() . add("South",BottomPanel);
     }

     public void addListeners()
     {
          BOk       . addActionListener(new ActList());
          BPunch    . addActionListener(new ActList());
          BCancel   . addActionListener(new ActList());

          theTable  . addMouseListener(new MouseList());
     }

     public class MouseList extends MouseAdapter
     {
          public void mouseClicked(MouseEvent me)
          {
               if(me.getClickCount()==2)
               {
                    int iRow = theTable.getSelectedRow();
                    int iCol = theTable.getSelectedColumn();

                    
                    if(iCol==5)
                    {
                         new SearchList(Layer,VUDept,VUDeptCode,theModel,iRow,iCol,"Department");

                    }
                    if(iCol==6)
                    {

                         new SearchList(Layer,VCata,VCataCode,theModel,iRow,iCol,"Classification");
                    }
               }
          }

     }

     public void removeHelpFrame()
     {
          try
          {
               Layer. remove(this);
               Layer. repaint();
               Layer. updateUI();

          }
          catch(Exception ex)
          {

               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {

               if(ae.getSource()==BPunch)
               {
                       if(checkControlTime(iMillCode))
                       {
                            JOptionPane.showMessageDialog(null," Cannot Issue Material Time Expired " );
                            return;
                       }
                       init();
                       start();
                       BPunch.setEnabled(false);
                       iPunchStatus=1;

               }
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
                    stop();

               }
  /*             if(checkControlTime(0))
               {
                    JOptionPane.showMessageDialog(null," Cannot Issue Material Time Expired " );
                    return;
               }
               if(setData())
               {
                    JOptionPane.showMessageDialog(null," Item Issued Sucessfully");
                    setIssueModel();
                    removeHelpFrame();
                    indentissueframe.setData();
               }
               else
               {

                    JOptionPane.showMessageDialog(null," Problem in Storing Data");
               } */
          }
     }

     public void getUsers(Connection theConnection)
     {
          ResultSet result = null;

          VUDept         = new Vector();
          VUDeptCode     = new Vector();

          VUnit          = new Vector();
          VUnitCode      = new Vector();

          VCata          = new Vector();
          VCataCode      = new Vector();
          
          try
          {
               Statement      stat   = theConnection.createStatement();

                String SDeptQS = "";
          if(iMillCode!=1)
               SDeptQS = "Select Dept_Name,Dept_Code From Dept where (MillCode=0 OR MillCode = 2) Order By Dept_Name";
          else
               SDeptQS = "Select Dept_Name,Dept_Code From Dept where (MillCode=1 OR MillCode = 2) Order By Dept_Name";


                      result = stat.executeQuery(SDeptQS);

               while(result.next())
               {
                    VUDept         . addElement(result.getString(1));
                    VUDeptCode     . addElement(result.getString(2));
               }
               result.close();
               
               String QS1 = "";
               String QS2 = "";
               QS1 = " Select Unit_Name,Unit_Code From Unit  Order By Unit_Name"; // Where MillCode=2 or MillCode="+iMillCode+"
               QS2 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               
               result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VUnit     . addElement(result.getString(1));
                    VUnitCode . addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VCata     . addElement(result.getString(1));
                    VCataCode . addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("Indent XyX :"+ex);
          }
     }

     private void setIssueModel()
     {

          VStockUserCode = new Vector();

          try
          {
               theModel.setNumRows(0);

               Connection theConnection=null;

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(getIndentList());
               int iSlNo=1;

               while(result.next())
               {
                    Vector theVect = new Vector();

                    String SItemCode = result.getString(1);
                    String SIndQty   = result.getString(4);
                    String SIssuedQty= result.getString(5);
                    String SStock    = result.getString(6);
                    String SRate     = result.getString(7);
                    String STotalStock= result.getString(12);

                    String SIndBalanceQty= "";

                    if(common.toDouble(SIssuedQty)==0)
                         SIndBalanceQty= SIndQty;
                    else
                         SIndBalanceQty= String.valueOf((common.toDouble(SIndQty)-common.toDouble(SIssuedQty)));

     
                    String SValue     = common.getRound(common.toDouble(SIndQty)*common.toDouble(SRate),2);

                    String SBalanceQty= String.valueOf((common.toDouble(SStock)-common.toDouble(SIndBalanceQty)));

                    String STotBalanceQty= String.valueOf((common.toDouble(STotalStock)-common.toDouble(SIndBalanceQty)));


                    theVect.addElement(String.valueOf(iSlNo));
                    theVect.addElement(SItemCode);
                    theVect.addElement(result.getString(2));
                    theVect.addElement(result.getString(3));
                    theVect.addElement(common.parseNull(result.getString(10)));
                    theVect.addElement(result.getString(8));
                    theVect.addElement(result.getString(9));
                    theVect.addElement(SStock);
                    theVect.addElement(SIndQty);
                    theVect.addElement(result.getString(5));
                    theVect.addElement(SIndBalanceQty);
                    theVect.addElement(result.getString(7));
                    theVect.addElement(SValue);
                    theVect.addElement(new Boolean(false));
                    theVect.addElement(SBalanceQty);
                    theVect.addElement(STotBalanceQty);

                    VStockUserCode.addElement(result.getString(11));

                    theModel.appendRow(theVect);

                    iSlNo++;
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private String getIndentList()
     {
            String QS= "   select Code,InvItems.Item_Name,Uom.UomName,Indent.Qty as IndentQty,"+
               "  IssueQty as IssuedQty,decode(sum(ItemStock.Stock),null,0,sum(ItemStock.Stock)),StockValue as Rate"+
               "  ,Dept.Dept_Name,Cata.Group_Name,InvItems.LocName,Indent.UserCode,getTotalStock(Indent.Code,"+iMillCode+")"+
               "  from Indent"+
               "  Inner join invItems on invitems.item_Code=Indent.Code"+
               "  and IndentNo="+iIndentNo+" and Unit_Code="+getUnitCode(SUnit)+
               "  Inner join ItemStock on ItemStock.ItemCode=indent.Code"+
               "  and ItemStock.HodCode=Indent.UserCode"+
               "  Inner join uom on uom.uomcode=Invitems.UOMCode"+
               "  Inner join Dept on Dept.Dept_Code=Indent.DeptCode"+
               "  Inner join Cata on Cata.Group_Code=Indent.Group_Code"+
               "  where ItemStock.MillCode="+iMillCode+
               "  and indent.IssueQty<Indent.Qty and Indent.CancelStatus=0"+
               "  Group by Code,InvItems.Item_Name,Uom.UomName,Indent.Qty,"+
               "  IssueQty,StockValue"+
               "  ,Dept.Dept_Name,Cata.Group_Name,InvItems.LocName,Indent.UserCode";


          return QS;
     }

	private class WindowList extends WindowAdapter
	{
		public void windowClosing(WindowEvent we)
		{
			stop();		
		}	
	}

                         /*   INSERT AND UPDATIONS*/

     public boolean setData(String SCode,String SName)
     {
          stop();
          


//        String    SUDeptCode     = getDeptCode(LDept.getText());
          int       iType          = JCType.getSelectedIndex();

          String    str            = "";
          int       iMSlNo         = 0;

          if(!isDataValid())
          {
               return false;
          }    

          try
          {
               if(theMConnection.getAutoCommit())
                    theMConnection . setAutoCommit(false);

               getIssueNo();

               Statement      stat          =  theMConnection.createStatement();
               PreparedStatement thePrepare = null;


               for(int i=0;i<theModel.getRows();i++)
               {
                    
                    Boolean Bselected = (Boolean)theModel.getValueAt(i,13);   
                    if(!Bselected.booleanValue())
                         continue;


                    iMSlNo++;

                    double dValue       = 0;

                    int    iDeptCode    = common.toInt(getDeptCode((String)theModel.getValueAt(i,5)));
                    int    iCataCode    = common.toInt(getCataCode((String)theModel.getValueAt(i,6)));


                    double dCurStock    = common.toDouble((String)theModel.getValueAt(i,7));
                    double dIndentQty   = common.toDouble((String)theModel.getValueAt(i,8));
                    double dIssuedQty   = common.toDouble((String)theModel.getValueAt(i,9));
                    double dQty         = common.toDouble((String)theModel.getValueAt(i,10));

                    double dRate        = common.toDouble((String)theModel.getValueAt(i,11));
                    String SItemName    = common.parseNull((String)theModel.getValueAt(i,2));
                    String SItemCode    = common.parseNull((String)theModel.getValueAt(i,1));

                    if(iType == 1)
                    {
                         dQty = dQty*(-1);
                    }

                    if(iType != 1)
                    {
                         if(dCurStock==0)
                         {
                              dValue       = 0;     // common.toDouble((String)MiddlePanel.VMidIssueValue.elementAt(i));
                         }
                         else 
                              dValue       = dQty*dRate;
                    }
                    else
                    {
                              dValue       = dQty*dRate;
                    }
                    str = "";

                    Item item = new Item(SItemCode,iMillCode,SItemTable,SSupTable);

                    if((common.toDouble(item.getClStock())-dQty)<=0)
                         dValue = common.toDouble(item.getClValue());


                    int iStockUserCode = common.toInt((String)VStockUserCode.elementAt(i));

                    if(iStockUserCode!=6125)
                    {
                         if(iType==2)
                         {
                              str = "Insert Into Issue(id,IssueNo,IssueDate,Code,HodCode,Qty,IssRate,UIRefNo,Dept_Code,Group_Code,Unit_Code,UserCode,CreationDate,MillCode,issueValue,SlNo,IndentType,Authentication,MrsAuthUserCode,IndentUserCode,ReceiverEmpCode) Values (";
                              str = str+ getIssueID()+",";
                              str = str+ SIssueNo+",";
                              str = str+"'"+common.getServerPureDate()+"',";
                              str = str+"'"+SItemCode+"',";
                              str = str+"0"+",";
                              str = str+common.getRound(dQty,3)+",";
                              str = str+common.getRound(dRate,4)+",";
                              str = str+"0"+TRefNo.getText()+",";
                              str = str+iDeptCode+",";        
                              str = str+iCataCode+",";
                              str = str+getUnitCode(LUnit.getText())+",";        
                              str = str+String.valueOf(iUserCode)+",";
                              str = str+"'"+common.getServerDate()+"',";
                              str = str+String.valueOf(iMillCode)+",";
                              str = str+common.getRound(dValue,2)+",";
                              str = str+String.valueOf(iMSlNo)+",";
                              str = str+getIndentCode(((String)JCType.getSelectedItem()).trim())+",";
                              str = str+"1"+",";
                              str = str+iStockUserCode+",";        
                              str = str+SAuthUserCode+","+SCode+")";        //SAuthUserCode
     
                         }
                         else
                         {
                              str = "Insert Into Issue(id,IssueNo,IssueDate,Code,HodCode,Qty,IssRate,UIRefNo,Dept_Code,Group_Code,Unit_Code,UserCode,CreationDate,issueValue,SlNo,IndentType,MillCode,MrsAuthUserCode,IndentUserCode,ReceiverEmpCode) Values (";
                              str = str+ getIssueID()+",";
                              str = str+ SIssueNo+",";
                              str = str+"'"+common.getServerPureDate()+"',";
                              str = str+"'"+SItemCode+"',";
                              str = str+"0"+",";
                              str = str+common.getRound(dQty,3)+",";
                              str = str+common.getRound(dRate,4)+",";
                              str = str+"0"+TRefNo.getText()+",";
                              str = str+iDeptCode+",";
                              str = str+iCataCode+",";
                              str = str+getUnitCode(LUnit.getText())+",";
                              str = str+String.valueOf(iUserCode)+",";
                              str = str+"'"+common.getServerDate()+"',";
                              str = str+common.getRound(dValue,2)+",";
                              str = str+String.valueOf(iMSlNo)+",";
                              str = str+getIndentCode(((String)JCType.getSelectedItem()).trim())+",";
                              str = str+String.valueOf(iMillCode)+",";
                              str = str+iStockUserCode+",";        
                              str = str+SAuthUserCode+","+SCode+")";        //SAuthUserCode
                         }
                         stat.execute(str);

                    }
                    else
                    {
                         if(iType==2)
                         {
                              str = "Insert Into NonStockIssue(id,IssueNo,IssueDate,Code,HodCode,Qty,IssRate,UIRefNo,Dept_Code,Group_Code,Unit_Code,UserCode,CreationDate,MillCode,issueValue,SlNo,IndentType,Authentication,MrsAuthUserCode,IndentUserCode,ReceiverEmpCode) Values (";
                              str = str+ getNonStockIssueID()+",";
                              str = str+ SIssueNo+",";
                              str = str+"'"+common.getServerPureDate()+"',";
                              str = str+"'"+SItemCode+"',";
                              str = str+"0"+",";
                              str = str+common.getRound(dQty,3)+",";
                              str = str+common.getRound(dRate,4)+",";
                              str = str+"0"+TRefNo.getText()+",";
                              str = str+iDeptCode+",";        
                              str = str+iCataCode+",";
                              str = str+getUnitCode(LUnit.getText())+",";        
                              str = str+String.valueOf(iUserCode)+",";
                              str = str+"'"+common.getServerDate()+"',";
                              str = str+String.valueOf(iMillCode)+",";
                              str = str+common.getRound(dValue,2)+",";
                              str = str+String.valueOf(iMSlNo)+",";
                              str = str+getIndentCode(((String)JCType.getSelectedItem()).trim())+",";
                              str = str+"1"+",";
                              str = str+iStockUserCode+",";        
                              str = str+SAuthUserCode+","+SCode+")";        //SAuthUserCode
     
                         }
                         else
                         {
                              str = "Insert Into NonStockIssue(id,IssueNo,IssueDate,Code,HodCode,Qty,IssRate,UIRefNo,Dept_Code,Group_Code,Unit_Code,UserCode,CreationDate,issueValue,SlNo,IndentType,MillCode,MrsAuthUserCode,IndentUserCode,ReceiverEmpCode) Values (";
                              str = str+ getNonStockIssueID()+",";
                              str = str+ SIssueNo+",";
                              str = str+"'"+common.getServerPureDate()+"',";
                              str = str+"'"+SItemCode+"',";
                              str = str+"0"+",";
                              str = str+common.getRound(dQty,3)+",";
                              str = str+common.getRound(dRate,4)+",";
                              str = str+"0"+TRefNo.getText()+",";
                              str = str+iDeptCode+",";
                              str = str+iCataCode+",";
                              str = str+getUnitCode(LUnit.getText())+",";
                              str = str+String.valueOf(iUserCode)+",";
                              str = str+"'"+common.getServerDate()+"',";
                              str = str+common.getRound(dValue,2)+",";
                              str = str+String.valueOf(iMSlNo)+",";
                              str = str+getIndentCode(((String)JCType.getSelectedItem()).trim())+",";
                              str = str+String.valueOf(iMillCode)+",";
                              str = str+iStockUserCode+",";        
                              str = str+SAuthUserCode+","+SCode+")";        //SAuthUserCode
                         }
                         stat.execute(str);




                    }
                    String str1 ="";
                    String SUpIndQs="";
                    double dBalanceQty=0;

                    str1       = " Update Indent set IssueQty=IssueQty+?,IssueStatus=? where IndentNo=? and Code=? and AuthUserCode=? and MillCode=? and UserCode=? and Unit_Code=?";

                    thePrepare =  theMConnection.prepareStatement(str1);

                    thePrepare.setDouble(1,dQty);
                    if((dIssuedQty+dQty)==dIndentQty)
                        thePrepare.setInt(2,1);
                    else
                        thePrepare.setInt(2,0);
                    thePrepare.setString(3,TRefNo.getText());
                    thePrepare.setString(4,SItemCode);
                    thePrepare.setString(5,SAuthUserCode);
                    thePrepare.setInt(6,iMillCode);
                    thePrepare.setInt(7,iStockUserCode);
                    thePrepare.setString(8,getUnitCode(SUnit));


                    thePrepare.executeUpdate();
                    thePrepare.close();

                    thePrepare  = theMConnection.prepareStatement(" Insert into IndentReceiverList(Id,IndentNo,EmpCode,EmpName,ReceiveQty) values(IndentReceiver_Seq.nextval,?,?,?,?)");

                    thePrepare.setString(1,TRefNo.getText());
                    thePrepare.setString(2,SCode);
                    thePrepare.setString(3,SName);
                    thePrepare.setDouble(4,dQty);

                
                    thePrepare.executeUpdate();
                    thePrepare.close();

                    SUpIndQs   = " Update ItemStock set Stock=Stock-"+dQty+",IndentQty=IndentQty-"+dQty+" where ItemCode='"+SItemCode+"' and Hodcode="+iStockUserCode+" and MillCode="+iMillCode;     //


                    stat.execute(SUpIndQs);
                    


               }

               stat .close();

               if(iType==2)
               {
                    insertIntoSubStoreReceipt();
               }

               if(iMillCode==1)
               {
                    updateSubStoreMasterData();
               }
               updateInvitemsData();
               UpdatedIssueNo();

          }
          catch(Exception ex)
          {
               try
               {
                    theMConnection.rollback();
               }
               catch(Exception e)
               {
               }
               ex        .printStackTrace();

               bComflag  = false;
          }
          setComOrRol();

          return bComflag;
     }

     public void setComOrRol()
     {
          if(bComflag)
          {
               try
               {
                    theMConnection . commit();

                    if(iMillCode==1)
                         theDConnection . commit();
               } catch(Exception ex)
               {
                    System.out.println("Indent frame actionPerformed ->"+ex);
                    ex.printStackTrace();
               }
          }
          else
          {
               try
               {
                    theMConnection . rollback();

                    if(iMillCode==1)
                         theDConnection . rollback();

                    BOk            . setEnabled(true);
               }catch(Exception ex)
               {
                    System.out.println("Indent frame actionPerformed ->"+ex);
                    ex.printStackTrace();
               }
          }
          try
          {
               theMConnection . setAutoCommit(true);

               if(iMillCode==1)
                    theDConnection . setAutoCommit(true);
          }catch(Exception ex)
          {
               System.out.println("Indent frame actionPerformed ->"+ex);
               ex.printStackTrace();
          }
     }

     private void insertIntoSubStoreReceipt()
     {
          try
          {
               PreparedStatement psmt   =    theDConnection.prepareStatement(getInsertQS());

               Statement stat =  theDConnection.createStatement();

               for(int i=0;i<theModel.getRows();i++)
               {
                    Boolean Bselected = (Boolean)theModel.getValueAt(i,13);   
                    if(!Bselected.booleanValue())
                         continue;

                    if(!isEligible(i))
                         continue;

                    String SCode        = common.parseNull(((String)theModel.getValueAt(i,1)).trim());
                    double dIssueQty    = common.toDouble(common.parseNull(((String)theModel.getValueAt(i,10)).trim()));
                    double dRate        = common.toDouble((String)theModel.getValueAt(i,11));
                    double dIssueVal    = common.toDouble(common.getRound(String.valueOf(dRate*dIssueQty),2));

                    psmt.setInt(1,getID());
                    psmt.setString(2,SIssueNo);    //common.toInt((String)TIndIssNo.getText())
                    psmt.setInt(3,common.toInt(common.getServerPureDate()));
                    psmt.setInt(4,common.toInt((String)TRefNo.getText()));
                    psmt.setString(5,SCode.trim());
                    psmt.setDouble(6,dIssueQty);
                    psmt.setDouble(7,common.toDouble((String)theModel.getValueAt(i,11)));
                    psmt.setString(8,"1");

               if(theDConnection.getAutoCommit())
                    theDConnection . setAutoCommit(false);

                    psmt.execute();

                    String QS = "";

                    QS = " Update InvItems Set ";
                    QS = QS+" CSStock=nvl(CSStock,0)+"+dIssueQty+",";
                    QS = QS+" CSValue=nvl(CSValue,0)+"+dIssueVal+" ";
                    QS = QS+" Where Item_Code = '"+SCode+"'";

                    if(theDConnection.getAutoCommit())
                         theDConnection . setAutoCommit(false);

                    stat.execute(QS);
               }
               psmt.close();
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame insertIntoSubStoreReceipt() ->"+e);
               System.out.println("\n\n\n\nError in insertIntoSubStoreReceipt()\n\n\n");
               e.printStackTrace();
               bComflag  = false;
          }
     }
     
     private String getInsertQS()
     {
          String QS ="";
          QS   =    "Insert into SubStoreReceipt(ID,GrnNo,GrnDate,IndentSlipNo,RawCode,GrnQty,GrnRate,ShiftCode) "+
                    "values(?,?,?,?,?,?,?,?) ";
          return QS;
     }

     private void updateSubStoreMasterData()
     {
          int       iType     = common.toInt(SIndentType);

          try
          {
               Statement stat =  theDConnection.createStatement();

               for(int i=0;i<theModel.getRows();i++)
               {     
                    Boolean Bselected = (Boolean)theModel.getValueAt(i,13);   
                    if(!Bselected.booleanValue())
                         continue;

                    if(!isEligible(i))
                         continue;

                    String SItemName    = common.parseNull((String)theModel.getValueAt(i,2));
                    String SItemCode    = common.parseNull((String)theModel.getValueAt(i,1));
                    double dIssueQty    = common.toDouble(common.parseNull(((String)theModel.getValueAt(i,10)).trim()));

                    int iCount=0;
                    
                    ResultSet res = stat.executeQuery("Select count(*) from InvItems Where Item_Code='"+SItemCode+"'");
                    while(res.next())
                    {
                         iCount   =    res.getInt(1);
                    }
                    res.close();

                    if(iCount==0)
                         continue;

                    String QS = "";

                    if(iType == 1)
                         dIssueQty =   (-1)*dIssueQty;

                    QS = " Update InvItems Set ";
                    QS = QS+" MSIssQty=nvl(MSIssQty,0)+"+dIssueQty+",";
                    QS = QS+" MSStock=nvl(MSStock,0)-"+dIssueQty+" ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";

                    if(theDConnection.getAutoCommit())
                         theDConnection . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame updateSubStoreMasterData() ->"+e);
               e.printStackTrace();
               bComflag  = false;
               System.out.println("\n\n\n\nError in updateSubStoreMasterData()\n\n\n");
          }
     }


     private void updateInvitemsData()
     {
          ResultSet result    = null;
          int       iType     = JCType.getSelectedIndex();
          try
          {
               Statement stat =  theMConnection.createStatement();

               for(int i=0;i<theModel.getRows();i++)
               {     

                    Boolean Bselected = (Boolean)theModel.getValueAt(i,13);   
                    if(!Bselected.booleanValue())
                         continue;
                    int iStockUserCode = common.toInt((String)VStockUserCode.elementAt(i));
                    if(iStockUserCode==6125)
                         continue;


                    String SItemName    = common.parseNull((String)theModel.getValueAt(i,2));
                    String SItemCode    = common.parseNull((String)theModel.getValueAt(i,1));
                    double dIssueQty    = common.toDouble((String)theModel.getValueAt(i,10));
                    double dRate        = common.toDouble((String)theModel.getValueAt(i,11));
                    double dIssueVal    = common.toDouble(common.getRound(String.valueOf(dRate*dIssueQty),2));

                    String QString = "";

                    QString =   " Update "+SItemTable+" ";

                    if(iType == 1)
                    {
                         dIssueQty =   (-1)*dIssueQty;
                         dIssueVal =   (-1)*dIssueVal;
                    }

                    QString = QString+" Set IssQty=nvl(IssQty,0)+"+dIssueQty+",IssVal=nvl(IssVal,0)+"+dIssueVal+" ";
                    QString = QString+" Where Item_Code = '"+SItemCode+"'";

                    if(theMConnection.getAutoCommit())
                         theMConnection . setAutoCommit(false);

                    stat.execute(QString);
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame updateInvitemsData() ->"+e);
               e.printStackTrace();
               bComflag  = false;
          }
     }

     public void UpdatedIssueNo()
     {
          String SUString = "";

          Statement stat      = null;
          ResultSet result    = null;

          int       iId       = 0;
          try
          {
               stat           =  theMConnection.createStatement();

               if(theMConnection.getAutoCommit())
                    theMConnection . setAutoCommit(false);

               SUString = " Update config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1 where Id = 5";

               stat . executeUpdate(SUString);
               stat . close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame UpdatedIssueNo() ->"+e);
               bComflag  = false;
               e.printStackTrace();
          }
     }
     public void getIssueNo()
     {
          try
          {
               SIssueNo  = String.valueOf(common.toInt(control.getID("Select maxno From Config"+iMillCode+""+SYearCode+" where Id = 5 for update of maxno nowait"))+1);
          }
          catch(Exception ex)
          {
               System.out.println(" getIssueNo ->"+ex);
               ex.printStackTrace();
          }
     }
     private boolean isDataValid()
     {
          boolean bflag = true;

          for(int i=0;i<theModel.getRows();i++)
          {

               double dCurStock    = common.toDouble((String)theModel.getValueAt(i,7));
               double dIndentQty   = common.toDouble((String)theModel.getValueAt(i,8));
               double dIssuedQty   = common.toDouble((String)theModel.getValueAt(i,9));
               double dQty         = common.toDouble((String)theModel.getValueAt(i,10));

               Boolean Bselected = (Boolean)theModel.getValueAt(i,13);   
               if(!Bselected.booleanValue())
                    continue;

               if((dQty+dIssuedQty)>dIndentQty)
               {

                    JOptionPane.showMessageDialog(null," Issue Qty is More than indent Qty");
                    bflag=false;
               }
          }

          int iCount=0;
          for(int i=0;i<theModel.getRows();i++)
          {

               Boolean Bselected = (Boolean)theModel.getValueAt(i,13);

               if(Bselected.booleanValue())
               {
                    iCount++;
               }
          }

          for(int i=0;i<theModel.getRows();i++)
          {
               Boolean Bselected = (Boolean)theModel.getValueAt(i,13);   
               if(Bselected.booleanValue())
               {
                    String SItemCode    = common.parseNull((String)theModel.getValueAt(i,1));

                    double dQty         = common.toDouble((String)theModel.getValueAt(i,10));
                    if(dQty<=0)
                    {
                         JOptionPane.showMessageDialog(null," Invalid Issue Qty For the Selected Item -->"+SItemCode );
                         bflag=false;
                    }
               }
          }

          for(int i=0;i<theModel.getRows();i++)
          {
               Boolean Bselected = (Boolean)theModel.getValueAt(i,13);   
               if(Bselected.booleanValue())
               {

                    int    iDeptCode    = common.toInt(getDeptCode((String)theModel.getValueAt(i,5)));
                    int    iCataCode    = common.toInt(getCataCode((String)theModel.getValueAt(i,6)));

                    if(iDeptCode==0 || iCataCode==0)
                    {
                         JOptionPane.showMessageDialog(null," Department or Classification is UnClassifed in the Row"+(i+1));
                         bflag= false;
                    }
               }

          }
          if(iCount==0)
          {
               JOptionPane.showMessageDialog(null," No Item is Selected For Issue");
               return false;
          }
          int       iType          = JCType.getSelectedIndex();

          if(iMillCode==1 && iType==2)
          {
               for(int i=0;i<theModel.getRows();i++)
               {
                    Boolean Bselected = (Boolean)theModel.getValueAt(i,13);

                    if(Bselected.booleanValue())
                    {
                         String SItemCode    = common.parseNull((String)theModel.getValueAt(i,1));
                         String SItemName    = common.parseNull((String)theModel.getValueAt(i,2));

                         if(!isDyeRawItemAvailable(SItemCode))
                         {
                              JOptionPane.showMessageDialog(null," Item Not Available In SubStore "+SItemName);

                              bflag=false;
                         }
                    }
               }
          }
          return bflag;
     }
     private int getIssueID()
     {
          Statement stat      = null;
          ResultSet result    = null;

          int iId=0;
          try
          {
               stat      =    theMConnection.createStatement();
               result    =    stat.executeQuery("Select Issue_Seq.nextVal from Dual");

               while(result.next())
               {
                    iId  =    result.getInt(1);
               }
               result    .close();
               stat      .close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return iId;
     }
     private int getNonStockIssueID()
     {
          Statement stat      = null;
          ResultSet result    = null;

          int iId=0;
          try
          {
               stat      =    theMConnection.createStatement();
               result    =    stat.executeQuery("Select NonStockIssue_Seq.nextVal from Dual");

               while(result.next())
               {
                    iId  =    result.getInt(1);
               }
               result    .close();
               stat      .close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return iId;
     }

     private String getDeptCode(String SDept)
     {
          int iIndex = VUDept.indexOf(SDept);

          return (String)VUDeptCode.elementAt(iIndex);
     }
     private String getUnitCode(String SDept)
     {
          int iIndex = VUnit.indexOf(SDept);

          return (String)VUnitCode.elementAt(iIndex);
     }
     private String getCataCode(String SDept)
     {
          int iIndex = VCata.indexOf(SDept);

          return (String)VCataCode.elementAt(iIndex);
     }
     public int getIndentCode(String SIndentName)
     {
          int iIndex    = VIndentTypeName.indexOf(SIndentName.trim());

          return common.toInt(((String)VIndentTypeCode.elementAt(iIndex)).trim());
     }
     public void setIndentCode()
     {
          VIndentTypeCode     = new Vector();
          VIndentTypeName     = new Vector();

          String QS      = " Select code,name from indenttype where (millcode="+iMillCode +" or millcode = 2) and  issueflag = 0 ";

          try  {
                    ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                    Connection     theConnection  = oraConnection.getConnection();   
                    Statement      stat           = theConnection.createStatement();
     
                    ResultSet result = stat.executeQuery(QS);


                    while(result.next())
                    {
                         VIndentTypeCode     .addElement(common.parseNull(result.getString(1)));
                         VIndentTypeName     .addElement(common.parseNull(result.getString(2)));
                    }
                    result    .close();
                    stat      .close();
          } catch(Exception Ex)
          {
               System.out.println("getIndentCode from Indent "+Ex);
          }
     }
     public  boolean isDyeRawItemAvailable(String SItemCode)
     {
          int iCount=0;
          String QS      = " Select count(InvItems.Item_code) from InvItems where Item_Code='"+SItemCode+"'";

          try  {

               if(theDConnection==null)
               {
                    DORAConnection DoraConnection = DORAConnection.getORAConnection();
                                   theDConnection = DoraConnection.getConnection();
               }
               Statement      stat           = theDConnection.createStatement();
     
               ResultSet result = stat.executeQuery(QS);
               if(result.next())
               {
                    iCount = result.getInt(1);
               }
               result    .close();
               stat      .close();
          } catch(Exception Ex)
          {
               System.out.println("Check Item from Indent "+Ex);
          }
          if(iCount==0)
               return false;
          return true;
     }

     private int getIndentTypeCode(String SIndentType)
     {
          int iIndex = VIndentTypeName.indexOf(SIndentType);

          return common.toInt((String)VIndentTypeCode.elementAt(iIndex));
     }
     private boolean isEligible(int iRowCount)
     {
          double iQty         = common.toDouble(common.parseNull((String)theModel.getValueAt(iRowCount,10)));
          String SItemCode    = common.parseNull((String)theModel.getValueAt(iRowCount,1));

          if(iQty==0)
               return false;
          if(SItemCode.length()<3)
               return false;

          return true;
     }
     private int getID()
     {
          Statement stat      = null;
          ResultSet result    = null;

          int iId=0;
          try
          {
               stat      = theDConnection.createStatement();
               result    = stat.executeQuery("Select SubStoreReceipt_Seq.nextVal from Dual");

               while(result.next())
               {
                    iId  =    result.getInt(1);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return iId;
     }

     public boolean checkControlTime(int iTypeCode)
     {
          int iValue =0;
          boolean bFlag = false;

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(getControlTimeQS(iTypeCode));

               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result.close();
               theStatement.close();

               if(iValue<0)
                  return true;

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               return true;
          }

          return false;
     }
     public boolean   checkEligiblePerson(String SEmpCode)
     {
          int iValue =0;
          boolean bFlag = false;


          String QS ="";


          if(iMillCode==1)
          {
               QS= " select count(1) from DyeingIssueReceiverList Where UserCode in(select AuthUsercode from MrsUserAuthentication where userCode="+SAuthUserCode+")"+
                           " and DyeingIssueReceiverList.EmpCode="+SEmpCode;


          }

          else
          {
               QS= " select count(1) from IssueReceiverList Where UserCode in(select AuthUsercode from MrsUserAuthentication where userCode="+SAuthUserCode+")"+
                           " and IssueReceiverList.EmpCode="+SEmpCode;

          }



          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);

               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result.close();
               theStatement.close();


               if(iValue>0)
                  bFlag=true;

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               return bFlag;
          }

                      
          return bFlag;
     }

     private String getControlTimeQS(int iTypeCode)
     {

          String QS=" Select (to_date(controlTime,'hh24:mi')-(to_date(to_Char(sysdate,'hh24:mi'),'hh24:mi')))*(24*60) as Diff from"+
                    " TimeControl"+
                    " Where TypeCode="+iTypeCode;


          return QS;
     }

    protected void init()        
    {

		capturer.addDataListener(new DPFPDataAdapter() {
                public void dataAcquired(final DPFPDataEvent e) {
                    LStatus.setText(" Please wait Under processing.................");

				SwingUtilities.invokeLater(new Runnable()
                                {
                                        public void run()
                                        {
                                                LStatus.setText("The fingerprint sample was captured....");
                                                process(e.getSample());
                                        }});
			}
		});
		capturer.addReaderStatusListener(new DPFPReaderStatusAdapter() {
                public void readerConnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
		 			makeReport("The fingerprint reader was connected.");
				}});
			}
                public void readerDisconnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was disconnected.");
				}});
			}
		});
		capturer.addSensorListener(new DPFPSensorAdapter() {
                public void fingerTouched(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was touched.");
				}});
			}
                public void fingerGone(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The finger was removed from the fingerprint reader.");
				}});
			}
		});
		capturer.addImageQualityListener(new DPFPImageQualityAdapter() {
                public void onImageQuality(final DPFPImageQualityEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					if (e.getFeedback().equals(DPFPCaptureFeedback.CAPTURE_FEEDBACK_GOOD))
						makeReport("The quality of the fingerprint sample is good.");
					else
						makeReport("The quality of the fingerprint sample is poor.");
				}});
			}
		});
	}

	protected void process(DPFPSample sample)
	{
		// Draw fingerprint sample image.
		
		DPFPFeatureSet features = extractFeatures(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

		// Check quality of the sample and start verification if it's good
		int iStatus=0;
   	    String SName  = "";
		String SCode  = "";
          stop();
          System.out.println(iIndentNo+" Process Status Comming "+iProcessStatus);

          if(iProcessStatus==1)
               return;

          System.out.println(iIndentNo+" Process Status Comming "+iProcessStatus);


          LStatus.setText(" Under processing.................");
          iProcessStatus=1;


		if (features != null)
		{
			// Compare the feature set with our template



               for(int i=0; i<theMaterialIssueTemplateList.size(); i++)
               {
                    HashMap theMap = (HashMap) theMaterialIssueTemplateList.get(i);

                    SCode    = (String)theMap.get("EMPCODE");
                    SName    = (String)theMap.get("DISPLAYNAME");
                    
                    DPFPTemplate template   = (DPFPTemplate)theMap.get("TEMPLATE");

                    DPFPVerification matcher= DPFPGlobal.getVerificationFactory().createVerification();
                    matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);

                    DPFPVerificationResult result = 
                    matcher.verify(features,template);

                    if (result.isVerified())
                    {
                         LStatus.setText(""+SName);       
                         iStatus=1;
                         break;
                    }

               }

                  if(iStatus==0)
                  {
                         LStatus.setText("No Match Found");       
                         JOptionPane.showMessageDialog(null," No Match Found Punch Again");
                         stop();
                         iPunchStatus=0;
                         BPunch.setEnabled(true);
                         iProcessStatus=0;

                  }
                  else
                  {
                       if(!checkEligiblePerson(SCode))
                       {
                            JOptionPane.showMessageDialog(null," Person Not Elgible for Taking this Material"+SCode);
                            iProcessStatus=0;

                            return;
                       }



                       if(setData(SCode,SName))
                       {
                            JOptionPane.showMessageDialog(null," Item Issued Sucessfully");
                            setIssueModel();
                            removeHelpFrame();
                            indentissueframe.setData();
                            LStatus.setText(" Punch And Take Issue");
                            stop();
                            iPunchStatus=0;
                            BPunch.setEnabled(true);
                            iProcessStatus=0;

                       }
                       else
                       {
        
                            JOptionPane.showMessageDialog(null," Problem in Storing Data");
                            stop();
                            iPunchStatus=0;
                            BPunch.setEnabled(true);
                            iProcessStatus=0;

                       }
                  }
          }
	}
	protected void start()
	{
		capturer.startCapture();
	}

	protected void stop()
	{
		capturer.stopCapture();
	}
	public void setPrompt(String string) {
	}
	public void makeReport(String string) {
	}
	
	protected Image convertSampleToBitmap(DPFPSample sample) {
		return DPFPGlobal.getSampleConversionFactory().createImage(sample);
	}

	protected DPFPFeatureSet extractFeatures(DPFPSample sample, DPFPDataPurpose purpose)
	{
		DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
		try {
			return extractor.createFeatureSet(sample, purpose);
		} catch (DPFPImageQualityException e)
        {
            System.out.println(" Returning Null");

        	return null;
		}
	}
	private void updateStatus(int FAR)
	{
          System.out.println(FAR);
	}
}




