package Indent.IndentFiles;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class IssueListDetailsFrame extends JInternalFrame
{    
     String         SStDate,SEnDate;
     String         unit           = "";
     String         dept           = "";
     String         group          = "";
     String         authen         = "";
     String         notauthen      = "";
     String         SUnit          = "";
     String         SDept          = ""; 
     String         SGroup         = "";
     String         SAuthen        = ""; 
     String         SNotAuthen     = ""; 

     JComboBox      JCOrder,JCFilter;
     JButton        BPrint;
     JTextField     TPrint;
     JPanel         BottomPanel;
     FileWriter     FW;
     int            len = 0;
     JTextField     TFileName;
     
     JPanel         DatePanel,FilterPanel,SortPanel,BasisPanel,ApplyPanel;
     IssueCritPanel TopPanel;

     TabReport      tabreport;
     Common         common = new Common();

     DateField      TStDate;
     DateField      TEnDate;

     JRadioButton   JRPeriod,JRNo;
     NextField      TStNo,TEnNo;

     Object         RowData[][];

     String         ColumnData[] = {"Issue Id","Date","Issue No","Code","Name","Issue Qty","Issue Rate","Issue Value","Unit","Department","Classification","Status","User","Issue Type"};
     String         ColumnType[] = {   "N"    ,  "S" ,    "N"   ,  "S" ,  "S" ,   "N"     ,"N"         ,"N"          ,"S"   ,"S"         ,"S"             ,"S"     ,"S"   ,"S"};
     Vector         VIssueNo,VIssueDate,VRefNo,VIssueCode,VIssueName,VIssueValue,VIssueQty,VIssueRate,VIssueUnit,VIssueDept,VIssueCata,VStatus,VId,VUser,VIssueType;

     JLayeredPane   Layer;
     StatusPanel    SPanel;
     Vector         VCode,VName,VUom;
     int            iUserCode,iMillCode;
     String         SMillName;
     
     int            TLine = 0;
     int            iPage = 1;

     String         Head1 = "",SHead1   = "",
                    SHead2= "",SEnd     = "",SCen = "",SPitch    = "";
     String         STitle= "",SHead    = "",SAsOn= "",Head4     = "",SPage     =    "";
     int            iTEnd    = 0;
     int            iDEnd    = 0;
     int            iELinCon = 0;
     
     public IssueListDetailsFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUom,StatusPanel SPanel,int iUserCode,int iMillCode,String SMillName)
     {
          super("Issue List Details Frame");
          this.Layer     = Layer;
          this.VCode     = VCode;
          this.VName     = VName;
          this.VUom      = VUom;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SMillName = SMillName;
          
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TStDate        = new DateField();
          TEnDate        = new DateField();
          TStNo          = new NextField();
          TEnNo          = new NextField();
          BPrint         = new JButton("Print");
          TPrint         = new JTextField(25);
          TPrint         . setText("IssueListDetails.prn");
          TopPanel       = new IssueCritPanel(iMillCode);
          
          BottomPanel    = new JPanel();
          
          JCOrder        = new JComboBox();
          JCFilter       = new JComboBox();
          
          JRPeriod       = new JRadioButton("Periodical",true);
          JRNo           = new JRadioButton("Issue No");
          
          DatePanel      = new JPanel();
          FilterPanel    = new JPanel();
          SortPanel      = new JPanel();
          BasisPanel     = new JPanel();
          ApplyPanel     = new JPanel();
          
          TStDate        .setTodayDate();
          TEnDate        .setTodayDate();
     }

     public void setLayouts()
     {
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }

     public void addComponents()
     {
          BPrint              .setEnabled(false);
          BottomPanel         .add(BPrint);
          BottomPanel         .add(TPrint);
          getContentPane()    .add(TopPanel,BorderLayout.NORTH);
          getContentPane()    .add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          TopPanel  .BApply.addActionListener(new ApplyList());
          BPrint    .addActionListener(new PrintList());
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               BPrint.setEnabled(true);
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport = new TabReport(RowData,ColumnData,ColumnType);
                    getContentPane().add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    Layer.repaint();
                    Layer.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }
     
     public class PrintList implements  ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               PrnFileWrite();
               JOptionPane.showMessageDialog(null,getInfo(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VIssueNo.size()][ColumnData.length];
          for(int i=0;i<VIssueNo.size();i++)
          {
               RowData[i][0]  = (String)VIssueNo       .elementAt(i);
               RowData[i][1]  = (String)VIssueDate     .elementAt(i);
               RowData[i][2]  = (String)VRefNo         .elementAt(i);
               RowData[i][3]  = (String)VIssueCode     .elementAt(i);
               RowData[i][4]  = (String)VIssueName     .elementAt(i);
               RowData[i][5]  = common.getRound((String)VIssueQty      .elementAt(i),3);
               RowData[i][6]  = common.getRound((String)VIssueRate     .elementAt(i),4);
               RowData[i][7]  = common.getRound((String)VIssueValue    .elementAt(i),2);
               RowData[i][8]  = (String)VIssueUnit     .elementAt(i);
               RowData[i][9]  = (String)VIssueDept     .elementAt(i);
               RowData[i][10] = (String)VIssueCata     .elementAt(i);
               RowData[i][11] = (String)VStatus        .elementAt(i);
               RowData[i][12] = (String)VUser          .elementAt(i);
			   
			   RowData[i][13] = (String)VIssueType          .elementAt(i);

          }
     }

     private String getInfo()
     {
          String str = "<html><body>"; 
          str = str+"<h4><font color='green'> PRN File Successfully Created</font></h4>";
          str = str+"</body></html>";
          
          return str;
     }

     public void setDataIntoVector()
     {
          VIssueNo     = new Vector();
          VIssueDate   = new Vector();
          VRefNo       = new Vector();
          VIssueCode   = new Vector();
          VIssueName   = new Vector();
          VIssueQty    = new Vector();
          VIssueRate   = new Vector();
          VIssueUnit   = new Vector();
          VIssueDept   = new Vector();
          VIssueCata   = new Vector();
          VStatus      = new Vector();
          VId          = new Vector();
          VIssueValue  = new Vector();
          VUser        = new Vector();
		  VIssueType   = new Vector();
		  


          String StDate = TopPanel.TStDate.toNormal();
          String EnDate = TopPanel.TEnDate.toNormal();
          
          //String QString = getQString(StDate,EnDate);
		  
		  String QString = getQStringNew(StDate,EnDate);
		  
		  
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    String SStatus = "";
                    
                    String str1  = common.parseNull((String)res.getString(1));  
                    String str2  = common.parseNull((String)res.getString(2));
                    String str3  = common.parseNull((String)res.getString(3));
                    String str4  = common.parseNull((String)res.getString(4));
                    String str5  = common.parseNull((String)res.getString(5));
                    String str6  = common.parseNull((String)res.getString(6));
                    String str7  = common.parseNull((String)res.getString(7));
                    String str8  = common.parseNull((String)res.getString(8));
                    String str9  = common.parseNull((String)res.getString(9));
                    String str10 = common.parseNull((String)res.getString(10));
                    String str11 = common.parseNull((String)res.getString(11));
                    String str12 = common.parseNull((String)res.getString(12));
                    String str13 = common.parseNull((String)res.getString(13));
                    String str14 = common.parseNull((String)res.getString(14));
					
					String str15 = common.parseNull((String)res.getString(15));

                    
                    if(str11.equals("1"))
                    {
                         SStatus = "Authenticated";    
                    }
                    else
                    {
                         SStatus = "Not Authenticated";    
                    }   
                    
                    VIssueNo     .addElement(str1);
                    VIssueDate   .addElement(common.parseDate(str2));
                    VRefNo       .addElement(str3);
                    VIssueCode   .addElement(str4);
                    VIssueName   .addElement(str5);
                    VIssueQty    .addElement(str6);
                    VIssueRate   .addElement(common.parseNull(str7));
                    VIssueUnit   .addElement(common.parseNull(str8));
                    VIssueDept   .addElement(common.parseNull(str9));
                    VIssueCata   .addElement(common.parseNull(str10));
                    VStatus      .addElement(SStatus);
                    VId          .addElement(str12);
                    VIssueValue  .addElement(str13);
                    VUser        .addElement(str14);
					VIssueType	 .addElement(str15);
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     
     public String getQString(String StDate,String EnDate)
     {
          String QString ="";
          
          if(TopPanel.JRDate.isSelected())
          {
               QString  =     " SELECT Issue.IssueNo, Issue.IssueDate, Issue.UIRefNo,"+
                              " Issue.Code, InvItems.Item_Name, Issue.Qty, Issue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " Issue.Authentication, Issue.Id,Issue.IssueValue,RawUser.UserName "+
                              " FROM (((Issue "+
                              " INNER JOIN InvItems ON Issue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON Issue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON Issue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON Issue.Group_Code = Cata.Group_Code "+
                              " INNER JOIN RawUser on RawUser.UserCode = Issue.IndentUserCode "+
                              " Where Issue.IssueDate >= '"+StDate+"' and Issue.IssueDate <='"+EnDate+"' and  Issue.millcode="+iMillCode ;
          }
          else
          {
               QString  =     " SELECT Issue.IssueNo, Issue.IssueDate, Issue.UIRefNo,"+
                              " Issue.Code, InvItems.Item_Name, Issue.Qty, Issue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " Issue.Authentication, Issue.Id,Issue.IssueValue,RawUser.UserName "+
                              " FROM (((Issue "+
                              " INNER JOIN InvItems ON Issue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON Issue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON Issue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON Issue.Group_Code = Cata.Group_Code "+
                              " INNER JOIN RawUser on RawUser.UserCode = Issue.IndentUserCode "+
                              " Where Issue.UIRefNo >="+TopPanel.TStNo.getText()+" and Issue.UIRefNo <= "+TopPanel.TEnNo.getText()+" and Issue.millcode="+iMillCode ;
          }

          if(TopPanel.JCList.getSelectedIndex() == 0  && TopPanel.JRSeleList.isSelected())
          {
               QString        =    QString+" And Issue.Authentication = 1 ";
               SAuthen        =    "Authentication";
          }

          if(TopPanel.JCList.getSelectedIndex() == 1 && TopPanel.JRSeleList.isSelected())
          {
               QString        =    QString+" And Issue.Authentication = 0 ";
               SNotAuthen     =    "NotAuthentication";
          }

          if(TopPanel.JRSeleUnit.isSelected())
          {
               SUnit          =    "Unit.Unit_name = "+(String)TopPanel.JCUnit.getSelectedItem();
               QString        =    QString+" And Unit.unit_name = '"+(String)TopPanel.JCUnit.getSelectedItem()+"' ";
          }

          if(TopPanel.JRSeleDept.isSelected())
          {
               SDept          =    "Dept.Dept_name = "+(String)TopPanel.JCDept.getSelectedItem();
               QString        =    QString+" And Dept.Dept_name = '"+(String)TopPanel.JCDept.getSelectedItem()+"' ";
          }

          if(TopPanel.JRSeleGroup.isSelected())
          {
               SGroup         =    "Cata.Group_name = "+(String)TopPanel.JCGroup.getSelectedItem();
               QString        =    QString+" And Cata.Group_name = '"+(String)TopPanel.JCGroup.getSelectedItem()+"' ";
          }
          
          String SOrder = (TopPanel.TSort.getText()).trim();

          if(SOrder.length()>0)
               QString        =    QString+" Order By "+TopPanel.TSort.getText()+",10";
          else
               QString        =    QString+" Order By 1,2,5";

          return QString;
     }



    public String getQStringNew(String StDate,String EnDate)
     {
          String QString ="";
		  
		  QString  =  " SELECT IssueNo, IssueDate, UIRefNo,"+
                              " Code, Item_Name, Qty, IssRate,"+
                              " Unit_Name, Dept_Name, Group_Name, "+
                              " Authentication, Id,IssueValue,UserName,ISsueType from ( ";
		  
		  
          
          if(TopPanel.JRDate.isSelected())
          {
               QString  =   QString +  " SELECT Issue.IssueNo, Issue.IssueDate, Issue.UIRefNo,"+
                              " Issue.Code, InvItems.Item_Name, Issue.Qty, Issue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " Issue.Authentication, Issue.Id,Issue.IssueValue,RawUser.UserName,'Regular Issue' as ISsueType "+
                              " FROM (((Issue "+
                              " INNER JOIN InvItems ON Issue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON Issue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON Issue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON Issue.Group_Code = Cata.Group_Code "+
                              " INNER JOIN RawUser on RawUser.UserCode = Issue.IndentUserCode "+
                              " Where Issue.IssueDate >= '"+StDate+"' and Issue.IssueDate <='"+EnDate+"' and  Issue.millcode="+iMillCode ;
          }
          else
          {
               QString  =   QString + " SELECT Issue.IssueNo, Issue.IssueDate, Issue.UIRefNo,"+
                              " Issue.Code, InvItems.Item_Name, Issue.Qty, Issue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " Issue.Authentication, Issue.Id,Issue.IssueValue,RawUser.UserName,'Regular Issue' "+
                              " FROM (((Issue "+
                              " INNER JOIN InvItems ON Issue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON Issue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON Issue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON Issue.Group_Code = Cata.Group_Code "+
                              " INNER JOIN RawUser on RawUser.UserCode = Issue.IndentUserCode "+
                              " Where Issue.UIRefNo >="+TopPanel.TStNo.getText()+" and Issue.UIRefNo <= "+TopPanel.TEnNo.getText()+" and Issue.millcode="+iMillCode ;
          }

          if(TopPanel.JCList.getSelectedIndex() == 0  && TopPanel.JRSeleList.isSelected())
          {
               QString        =    QString+" And Issue.Authentication = 1 ";
               SAuthen        =    "Authentication";
          }

          if(TopPanel.JCList.getSelectedIndex() == 1 && TopPanel.JRSeleList.isSelected())
          {
               QString        =    QString+" And Issue.Authentication = 0 ";
               SNotAuthen     =    "NotAuthentication";
          }

          if(TopPanel.JRSeleUnit.isSelected())
          {
               SUnit          =    "Unit.Unit_name = "+(String)TopPanel.JCUnit.getSelectedItem();
               QString        =    QString+" And Unit.unit_name = '"+(String)TopPanel.JCUnit.getSelectedItem()+"' ";
          }

          if(TopPanel.JRSeleDept.isSelected())
          {
               SDept          =    "Dept.Dept_name = "+(String)TopPanel.JCDept.getSelectedItem();
               QString        =    QString+" And Dept.Dept_name = '"+(String)TopPanel.JCDept.getSelectedItem()+"' ";
          }

          if(TopPanel.JRSeleGroup.isSelected())
          {
               SGroup         =    "Cata.Group_name = "+(String)TopPanel.JCGroup.getSelectedItem();
               QString        =    QString+" And Cata.Group_name = '"+(String)TopPanel.JCGroup.getSelectedItem()+"' ";
          }
		  
		  
          if(JRPeriod.isSelected())
          {
			  
			  
			  
			  QString        =    QString + "Union All " +
                              " SELECT NonStockIssue.IssueNo, NonStockIssue.IssueDate, NonStockIssue.UIRefNo,"+
                              " NonStockIssue.Code, InvItems.Item_Name, NonStockIssue.Qty, NonStockIssue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " NonStockIssue.Authentication, NonStockIssue.Id,0 as IssueValue,getName(NonStockIssue.ReceiverEmpcode,"+iMillCode+") as UserName,'Non Stock Issue' "+
                              " FROM (((NonStockIssue "+
                              " INNER JOIN InvItems ON NonStockIssue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON NonStockIssue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON NonStockIssue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON NonStockIssue.Group_Code = Cata.Group_Code "+
                              " Where NonStockIssue.IssueDate >= '"+StDate+"' and NonStockIssue.IssueDate <='"+EnDate+"' "+
                              " And NonStockIssue.MillCode="+iMillCode;
          }
          else
          {
			  			  QString        =    QString + "Union All " +
							" SELECT NonStockIssue.IssueNo, NonStockIssue.IssueDate, NonStockIssue.UIRefNo,"+
                              " NonStockIssue.Code, InvItems.Item_Name, NonStockIssue.Qty, NonStockIssue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " NonStockIssue.Authentication, NonStockIssue.Id,,0 as IssueValue,getName(NonStockIssue.ReceiverEmpcode,"+iMillCode+") as UserName,'Non Stock Issue'  "+
                              " FROM (((NonStockIssue "+
                              " INNER JOIN InvItems ON NonStockIssue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON NonStockIssue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON NonStockIssue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON NonStockIssue.Group_Code = Cata.Group_Code "+
							  " Where NonStockIssue.UIRefNo >="+TopPanel.TStNo.getText()+" and NonStockIssue.UIRefNo <= "+TopPanel.TEnNo.getText()+" and NonStockIssue.millcode="+iMillCode ;
                              //" And NonStockIssue.MillCode="+iMillCode;
          }
		  
		  if(TopPanel.JCList.getSelectedIndex() == 0  && TopPanel.JRSeleList.isSelected())
          {
               QString        =    QString+" And NonStockIssue.Authentication = 1 ";
               SAuthen        =    "Authentication";
          }

          if(TopPanel.JCList.getSelectedIndex() == 1 && TopPanel.JRSeleList.isSelected())
          {
               QString        =    QString+" And NonStockIssue.Authentication = 0 ";
               SNotAuthen     =    "NotAuthentication";
          }

          if(TopPanel.JRSeleUnit.isSelected())
          {
               SUnit          =    "Unit.Unit_name = "+(String)TopPanel.JCUnit.getSelectedItem();
               QString        =    QString+" And Unit.unit_name = '"+(String)TopPanel.JCUnit.getSelectedItem()+"' ";
          }

          if(TopPanel.JRSeleDept.isSelected())
          {
               SDept          =    "Dept.Dept_name = "+(String)TopPanel.JCDept.getSelectedItem();
               QString        =    QString+" And Dept.Dept_name = '"+(String)TopPanel.JCDept.getSelectedItem()+"' ";
          }

          if(TopPanel.JRSeleGroup.isSelected())
          {
               SGroup         =    "Cata.Group_name = "+(String)TopPanel.JCGroup.getSelectedItem();
               QString        =    QString+" And Cata.Group_name = '"+(String)TopPanel.JCGroup.getSelectedItem()+"' ";
          }
		  

          if(JRPeriod.isSelected())
          {
			  
			  
			  
			  QString        =    QString + "Union All " +
                              " SELECT ServiceStockISsue.IssueNo, ServiceStockISsue.IssueDate, ServiceStockISsue.UIRefNo,"+
                              " ServiceStockISsue.Code, InvItems.Item_Name, ServiceStockISsue.Qty, ServiceStockISsue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " ServiceStockISsue.Authentication, ServiceStockISsue.Id,0 as IssueValue,getName(ServiceStockISsue.ReceiverEmpcode,"+iMillCode+") as UserName,'Service Stock Issue' "+
                              " FROM (((ServiceStockISsue "+
                              " INNER JOIN InvItems ON ServiceStockISsue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON ServiceStockISsue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON ServiceStockISsue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON ServiceStockISsue.Group_Code = Cata.Group_Code "+
                              " Where ServiceStockISsue.IssueDate >= '"+StDate+"' and ServiceStockISsue.IssueDate <='"+EnDate+"' "+
                              " And ServiceStockISsue.MillCode="+iMillCode;
          }
          else
          {
			  			  QString        =    QString + "Union All " +
							" SELECT ServiceStockISsue.IssueNo, ServiceStockISsue.IssueDate, ServiceStockISsue.UIRefNo,"+
                              " ServiceStockISsue.Code, InvItems.Item_Name, ServiceStockISsue.Qty, ServiceStockISsue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " ServiceStockISsue.Authentication, ServiceStockISsue.Id,,0 as IssueValue,getName(ServiceStockISsue.ReceiverEmpcode,"+iMillCode+") as UserName,'Service Stock Issue'  "+
                              " FROM (((ServiceStockISsue "+
                              " INNER JOIN InvItems ON ServiceStockISsue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON ServiceStockISsue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON ServiceStockISsue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON ServiceStockISsue.Group_Code = Cata.Group_Code "+
							  " Where ServiceStockISsue.UIRefNo >="+TopPanel.TStNo.getText()+" and ServiceStockISsue.UIRefNo <= "+TopPanel.TEnNo.getText()+" and NonStockIssue.millcode="+iMillCode ;
                              //" And NonStockIssue.MillCode="+iMillCode;
          }
		  
		  if(TopPanel.JCList.getSelectedIndex() == 0  && TopPanel.JRSeleList.isSelected())
          {
               QString        =    QString+" And ServiceStockISsue.Authentication = 1 ";
               SAuthen        =    "Authentication";
          }

          if(TopPanel.JCList.getSelectedIndex() == 1 && TopPanel.JRSeleList.isSelected())
          {
               QString        =    QString+" And ServiceStockISsue.Authentication = 0 ";
               SNotAuthen     =    "NotAuthentication";
          }

          if(TopPanel.JRSeleUnit.isSelected())
          {
               SUnit          =    "Unit.Unit_name = "+(String)TopPanel.JCUnit.getSelectedItem();
               QString        =    QString+" And Unit.unit_name = '"+(String)TopPanel.JCUnit.getSelectedItem()+"' ";
          }

          if(TopPanel.JRSeleDept.isSelected())
          {
               SDept          =    "Dept.Dept_name = "+(String)TopPanel.JCDept.getSelectedItem();
               QString        =    QString+" And Dept.Dept_name = '"+(String)TopPanel.JCDept.getSelectedItem()+"' ";
          }

          if(TopPanel.JRSeleGroup.isSelected())
          {
               SGroup         =    "Cata.Group_name = "+(String)TopPanel.JCGroup.getSelectedItem();
               QString        =    QString+" And Cata.Group_name = '"+(String)TopPanel.JCGroup.getSelectedItem()+"' ";
          }
		  
		  
		  
          
          String SOrder = (TopPanel.TSort.getText()).trim();

          if(SOrder.length()>0)
               QString        =    QString+" ) Order By "+TopPanel.TSort.getText()+",10";
          else
               QString        =    QString+" ) Order By 1,2,5 ";

          return QString;
     }

	 

     public void PrnFileWrite()
     {
          try
          {
               String SFile =TPrint.getText();

               if((SFile.trim()).length()==0)
                  SFile = "1.prn";

               FW = new FileWriter(common.getPrintPath()+SFile);

               initValue();
               PrnHead();
               PrnBody();
               FW.write("Report Taken Time : E"+common.getServerDateTime2()+"F");
               FW.write("");
               FW.close();
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

     private void initValue()
     {
          TLine     = 0;
          iPage     = 1;
          iTEnd     = 0;
          iDEnd     = 0;
          iELinCon  = 0;
     }

     private void PrnHead()
     {
          String SReport = "";

          unit      = "";
          dept      = "";
          group     = "";
          authen    = "";
          notauthen = "";

          initValue();
          try
          {

               Head1     = "ME"+SMillName+"F"+"\n";

               SAsOn     = "IssueList Details From E"  +common.parseDate(TopPanel.TStDate.toNormal())+  "F  TO E"  +common.parseDate(TopPanel.TEnDate.toNormal())+"F";
               SReport   = "Report Taken By ";
               Head4     = "Page No          : "+iPage;

     
               if(TopPanel.JRSeleUnit.isSelected())
               {
                    unit      =    (String)TopPanel.JCUnit.getSelectedItem();
                    SReport   = SReport +" Unit : E"+unit+"F";
               }
               else
                    SReport   = SReport +" Unit : EAllF";

               if(TopPanel.JRSeleDept.isSelected())
               {
                    dept      =    (String)TopPanel.JCDept.getSelectedItem();
                    SReport   =    SReport +" Department : E"+ dept+"F";
               }
               else
                    SReport   = SReport +" Department : EAllF";

               if(TopPanel.JRSeleGroup.isSelected())
               {
                    group     =    (String)TopPanel.JCGroup.getSelectedItem();
                    SReport   =    SReport +" Group : E"+ group+"F";
               }
               else
                    SReport   = SReport +" Group : EAllF";

               if(TopPanel.JRSeleList.isSelected())
               {
                    if((((String)TopPanel.JCList.getSelectedItem()).trim()).equals("Authenticated"))
                    {
                         authen    = "Authentication";
                         SReport   = SReport +" and E"+ authen+"F";
                    }
                    
                    if((((String)TopPanel.JCList.getSelectedItem()).trim()).equals("Not-Authenticated"))
                    {
                         notauthen = "Not-Authenticated";
                         SReport   = SReport +" and E"+ notauthen+"F";
                    }
               }


               STitle    = Head1+SAsOn;
               SPage     = Head4;
     
               SHead1    = "|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";
               SHead2    = "|ESlNoF|E Issue Id F| E  DateF   | E Issue NoF  | E   Code F   |E           Name                      F|E  Issue QtyF |E  Issue RateF  |E  Issue ValueF   |E   UnitF   |E   DepartmentF  |E        Classification      F  |E      Status      F |E  UserName  F|\n";
               SCen      = "|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
               SEnd      = "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";

               SPitch    = "P";

               SHead     = SHead1+SHead2+SCen;
     
               prnWriter(STitle);
               prnWriter(SReport);
               prnWriter(SPage+SPitch);
               prnWriter(SHead);

               TLine=TLine+6;
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void PrnBody()
     {
          int       iSlNo = 1;
          int       iPCon = VIssueDate.size()-1;

          for(int i=0; i<VIssueDate.size(); i++)
          {
               String Str0    = Integer.toString(i+1);
               String Str1    = (String)VIssueNo       .elementAt(i);
               String Str2    = (String)VIssueDate     .elementAt(i);
               String Str3    = (String)VRefNo         .elementAt(i);
               String Str4    = (String)VIssueCode     .elementAt(i);
               String Str5    = (String)VIssueName     .elementAt(i);
               String Str6    = (String)VIssueQty      .elementAt(i);
               String Str7    = common.getRound((String)VIssueRate.elementAt(i),3);
               String Str8    = (String)VIssueUnit     .elementAt(i);
               String Str9    = (String)VIssueDept     .elementAt(i);
               String Str10   = (String)VIssueCata     .elementAt(i);
               String Str11   = (String)VStatus        .elementAt(i);
               String Str12   = (String)VId            .elementAt(i);
               String Str13   = (String)VIssueValue    .elementAt(i);
               String Str14   = (String)VUser          .elementAt(i);
               


               String SData =    "|"+common.Rad(" "+Str0,4)+"|"+
                                     common.Pad(" "+Str1,10)+"|"+
                                     common.Cad(    Str2,10)+"|"+
                                     common.Pad(" "+Str3,12)+"|"+
                                     common.Pad(" "+Str4,12)+"|"+
                                     common.Pad(" "+Str5,37)+"|"+
                                     common.Rad(common.getRound(Str6,3)+" ",12)+"|"+
                                     common.Rad(common.getRound(Str7,4)+" ",14)+"|"+
                                     common.Rad(common.getRound(Str13,2)+" ",16)+"|"+
                                     common.Pad(" "+Str8,10)+"|"+
                                     common.Pad(" "+Str9,15)+"|"+
                                     common.Pad(" "+Str10,30)+"|"+
                                     common.Pad(" "+Str11,19)+"|"+
                                     common.Pad(" "+Str14,12)+"|";


               if(iPCon==i)
                    iTEnd  = 1;
               prnWriter(SData);
               iSlNo++;
          }
          prnWriter(SEnd);
     }

     private void prnWriter(String Str)
     {
          try
          {
               TLine =TLine+1;
               if(TLine<62)
               {
                    FW.write(Str+"\n");
               }
               else
               {
                    if(!Str.substring(0,1).equals("|") )
                    {
                         FW.write(Str+"\n");
                    }
                    if(iTEnd!=1)
                    {
                         FW.write(SEnd);
                         iPage=iPage+1;
                         TLine=0;
                         FW.write(""+"\n");
                         PrnHead();
                    }
                 }
          }
          catch(Exception e)
          {
            System.out.println(e);
          }
     }
}
