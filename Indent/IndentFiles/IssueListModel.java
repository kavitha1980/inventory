package Indent.IndentFiles;

import java.awt.*;
import java.awt.event.*;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import util.*;


public class IssueListModel extends DefaultTableModel
{

                    //         0       1      2     3       4         5             6          7           8         9           10       11        12      13       14       15

     String   ColumnName[] = {"SNo","Code","Name","UOM"," BIN ","Department","Classification","Stock","IndentQty","IssuedQty","IssueQty","Rate","Value","Select","Balance","TotalBal"};
     String   ColumnType[] = {"N","S","S","S","S","S","S","N","N","N","E","N","N","B","S","S"};
     int      ColumnWidth[]= {10,10,170,10,20,10,20,20,15,15,15,15,15,15,20,15};

     Vector theVector;
     Common common ;
     public IssueListModel()
     {
          common         = new Common();
          setDataVector(getRowData(),ColumnName);
     }
     public Object[][] getRowData()
     {
          Object RowData[][] = new Object[0][ColumnName.length];

          return RowData;
     }
     public boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="B" || ColumnType[col]=="E")
               return true;
          return false;
     }

     public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }

     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {

               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
               if(column==10)
               {
                    setTotalValue(row,column);
               }
          }
          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }
     public int getRows()
     {
         return super.dataVector.size(); 
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public void setTotalValue(int iRow,int iColumn)
     {
          double dStock= common.toDouble((String)getValueAt(iRow,7)); 
          double dQty =  common.toDouble((String)getValueAt(iRow,10));
          double dRate = common.toDouble((String)getValueAt(iRow,11));
          double dTotStock= common.toDouble((String)getValueAt(iRow,15)); 


          setValueAt(common.getRound((dQty*dRate),2),iRow,12);
//          setValueAt(common.getRound((dStock-dQty),2),iRow,14);
//          setValueAt(common.getRound((dTotStock-dQty),2),iRow,15);

     }
}

