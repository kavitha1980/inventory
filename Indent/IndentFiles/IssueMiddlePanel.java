package Indent.IndentFiles;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class  IssueMiddlePanel extends JPanel
{
     IssueTabReport tabreport;
     Object RowData[][],IdData[];
     String ColumnData[] = {"Code","Name","UOM","Department","Classification","Stock","Quantity","Rate","Value"};
     String ColumnType[] = {"S","S","S","B","B","N","E","N","N"};
     
     JLayeredPane Layer;
     Vector VCode,VName,VUom;
     Vector VUnitCode;
     
     JComboBox JCUnit,JCCata,JCDept;
     
     IssueMaterialSearch    MS;
     Vector VDeptCode,VDept;
     Vector VCata,VCataCode;
     int iUserCode,iMillCode;
     
     Common common = new Common();
     
     IssueMiddlePanel(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUom,JComboBox JCUnit,Vector VUnitCode,JComboBox JCCata,Vector VCata,Vector VCataCode,int iUserCode,int iMillCode)
     {
          this.Layer     = Layer;
          this.VCode     = VCode;
          this.VName     = VName;
          this.VUom      = VUom; 
          this.JCUnit    = JCUnit;
          this.VUnitCode = VUnitCode;
          this.JCCata    = JCCata;
          this.VCata     = VCata;
          this.VCataCode = VCataCode;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          
          setDeptVectors();
          JCDept       = new JComboBox(VDept);
          setLayout(new BorderLayout());
     }
     
     public void addListeners()
     {
                         tabreport .ReportTable.addKeyListener(new KeyList());
          JTableHeader   th        = tabreport.ReportTable.getTableHeader();
                         th        .addMouseListener(new IssueTableHeaderHandle(this));
     }
     
     private class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    showMaterialSelectionFrame();
               }
          }
     }
     
     private void showMaterialSelectionFrame()
     {
          try
          {
               Layer.remove(MS);
               Layer.updateUI();
          }
          catch(Exception ex){}
          
          try
          {
               MS   = new IssueMaterialSearch(Layer,VCode,VName,VUom,this,tabreport.ReportTable.getSelectedRow(),iMillCode);
               MS   .setSelected(true);
               Layer.add(MS);
               Layer.repaint();
               MS   .moveToFront();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }
     
     public int getDepartmentCode(int iRow)
     {
          String SDept = ((String)tabreport.RowData[iRow][3]).trim();
          if(SDept.length() == 0)
               return 0;  
          int iDept       = VDept.indexOf(SDept);
          return Integer.parseInt((String)VDeptCode.elementAt(iDept));
     }
     
     
     private void setDeptVectors()
     {
          VDept      = new Vector();
          VDeptCode  = new Vector();
          
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();
               
               String QS1 = "";
               if(iMillCode==0)
               {
                    QS1 = "Select Dept_Name,Dept_Code From Dept Where MillCode is Null or MillCode <> 1 Order By Dept_Name";
               }
               else
               {
                    QS1 = "Select Dept_Name,Dept_Code From Dept Where MillCode <> 0 Order By Dept_Name";
               }
               
               ResultSet result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VDept     .addElement(result.getString(1));
                    VDeptCode .addElement(result.getString(2));
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);   
          }
     }
     
     public void setData(IssueRecords issuerecords)
     {
          RowData    = new Object[issuerecords.VIssueCode.size()+4][ColumnData.length];            
          IdData     = new Object[issuerecords.VIssueCode.size()];
          String CData[] = {"Code","Name","UOM","Department","Classification","Stock","Quantity","Rate","Value"};
          String CType[] = {"S","S","S","B","B","N","E","N","N"};
          
          for(int i=0;i<issuerecords.VIssueCode.size();i++)
          {
               RowData[i][0] = (String)issuerecords.VIssueCode.elementAt(i);
               RowData[i][1] = (String)issuerecords.VIssueName.elementAt(i);
               RowData[i][2] = (String)issuerecords.VIssueUom.elementAt(i);
               RowData[i][3] = (String)issuerecords.VIssueDept.elementAt(i);
               RowData[i][4] = (String)issuerecords.VIssueGroup.elementAt(i);
               RowData[i][5] = (String)issuerecords.VStock.elementAt(i);
               RowData[i][6] = (String)issuerecords.VIssueQty.elementAt(i);
               RowData[i][7] = (String)issuerecords.VIssueRate.elementAt(i);
               RowData[i][8] = (String)issuerecords.VIssueValue.elementAt(i);
               IdData[i]     = (String)issuerecords.VId.elementAt(i);
          }
          
          int j = issuerecords.VIssueCode.size();
          
          for(int i=0;i<4;i++)
          {
               for(int k=0;k<CData.length;k++)
               {
                    RowData[j+i][k] = "";
               }
          }
          try
          {
               tabreport   = new IssueTabReport(RowData,CData,CType,JCCata,JCDept,this,issuerecords);
               add(tabreport,BorderLayout.CENTER);
               addListeners();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}
