package Indent.IndentFiles;

import javax.swing.table.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import com.digitalpersona.onetouch.*;
import com.digitalpersona.onetouch.capture.*;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.*;
import com.digitalpersona.onetouch.verification.*;

public class IssuePunchAuthentication extends JInternalFrame
{
     int                 i;

     JComboBox           JCType;

     JButton             BOk,BCancel,BPunch,BRefresh     ;
     JPanel              TopPanel,BottomPanel,MiddlePanel;
	 


     JTable              theTable;
     IssuePunchAuthModel     theModel;

     
     JLayeredPane        Layer;
     Common              common = new Common();
     Control             control;
     MyLabel             LPerson;

     StatusPanel         SPanel;
     int                 iUserCode,iMillCode;
     String              SYearCode,SItemTable,SAuthUserCode,SSupTable;

     Connection          theMConnection =    null;
     Connection          theDConnection =    null;
     Connection          theConnection1 =    null;

     boolean             isReturn       =    false;
     boolean             bComflag       =    true;

     ArrayList theTemplateList,theMaterialIssueTemplateList;
     private DPFPCapture capturer = DPFPGlobal.getCaptureFactory().createCapture();
	 
	 

     private DPFPVerification verificator = DPFPGlobal.getVerificationFactory().createVerification();

     DPFPTemplate template;
     private JTextField prompt = new JTextField();
     private JTextArea log = new JTextArea();
     private JTextField status = new JTextField("[status line]");
     int iPunchStatus=0;
     int iProcessStatus=0;
	 
	 MyLabel             LStatus;
	 
	 Vector vDataVect,VIndentTypeName;
	 
	 
     public IssuePunchAuthentication(JLayeredPane Layer,int iMillCode,String SItemTable,String SSupTable,String SYearCode,ArrayList theTemplateList,ArrayList theMaterialIssueTemplateList)
     {
          this.Layer          = Layer;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;
          this.SYearCode      = SYearCode;
		  this.theTemplateList              = theTemplateList;
		  this.theMaterialIssueTemplateList=theMaterialIssueTemplateList;



          try
          {

               getDBConnections();
               //getUsers(theMConnection);
               setIndentCode();
               createComponents();
               setLayouts();
               addComponents();    
               addListeners();
               setIssueModel();

          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void getDBConnections()
     {
          ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                         theMConnection = oraConnection.getConnection();

          if(iMillCode==1)
          {
               DORAConnection DoraConnection = DORAConnection.getORAConnection();
                              theDConnection = DoraConnection.getConnection();
          }
     }

     public void createComponents()
     {
          try
          {
               BOk            = new JButton("Okay");
               BPunch         = new JButton("Punch");
               BCancel        = new JButton("Exit");

               JCType         = new JComboBox(VIndentTypeName);

               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();         // IndentMiddlePanel(Layer,VCode,VName,VUOMName,VNameCode,JCUnit,VUnitCode,JCCata,TValue,VCata,VCataCode,iUserCode,iMillCode,SItemTable,SSupTable);
               BottomPanel    = new JPanel();
               control        = new Control();
			   
			   LStatus        = new MyLabel("");			   


               theModel       = new IssuePunchAuthModel();
               theTable       = new JTable(theModel)
               {
                 public Component prepareRenderer
                  (TableCellRenderer renderer, int index_row, int index_col){
                 Component comp = super.prepareRenderer(renderer, index_row, index_col);


                 if(index_col == 10){              //  && !isCellSelected(index_row, index_col)
                 //comp.setBackground(Color.lightGray);
                 //comp.setForeground(Color.black);

                 }
                 if(index_col == 15){              //  && !isCellSelected(index_row, index_col)
                 //comp.setBackground(Color.green);
                 //comp.setForeground(Color.black);
                 }

                 if(index_col!=10 && index_col!=15){
                 //comp.setBackground(Color.white);
                 //comp.setForeground(Color.black);

                 }
                 return comp;
                 }
                 };

               for(int i=0;i<theModel.ColumnName.length;i++)
               {
                    (theTable.getColumnModel()).getColumn(i).setPreferredWidth(theModel.ColumnWidth[i]);
               }


               LStatus.setText(" Punch And Take Issue");
               LStatus.setFont(new Font("courier New",Font.BOLD,16));
               LStatus.setForeground(Color.RED);
			   
	           BRefresh       = new JButton(" Refresh ");
			   

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setLayouts()
     {
          try
          {
               setTitle("Issued Material Recevier Punching");
               setMaximizable(true);
               setClosable(false);
               setIconifiable(true);
               setResizable(true);
               setBounds(0,0,940,465);

               getContentPane()    .setLayout(new BorderLayout());
               TopPanel            .setLayout(new GridLayout(1,4));
               BottomPanel         .setLayout(new GridLayout(1,2));
               TopPanel            .setBorder(new TitledBorder("Control Block"));

               MiddlePanel         .setLayout(new BorderLayout());
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void addComponents()
     {
          TopPanel         . add(new JLabel("  Indent No"));
          TopPanel         . add(JCType);


          TopPanel         . add(BPunch);
          TopPanel         . add(LStatus);


          MiddlePanel      . add(new JScrollPane(theTable));


          BottomPanel      . add(BRefresh);
          BottomPanel      . add(BCancel);

          getContentPane() . add("North",TopPanel);
          getContentPane() . add("Center",MiddlePanel);
          getContentPane() . add("South",BottomPanel);
     }

     public void addListeners()
     {
          BOk       . addActionListener(new ActList());
          BPunch    . addActionListener(new ActList());
          BCancel   . addActionListener(new ActList());
		  BRefresh	. addActionListener(new ActList());
			JCType.addItemListener(new ItemList());


          //theTable  . addMouseListener(new MouseList());
     }
	 
	private class ItemList implements ItemListener
    {
    	public void itemStateChanged(ItemEvent ie)
        {
        	try
            {
				setIssueModel();
			}
            catch(Exception ex)
            {}
		}
	}
	 


     public void removeHelpFrame()
     {
          try
          {
               Layer. remove(this);
               Layer. repaint();
               Layer. updateUI();

          }
          catch(Exception ex)
          {

               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {

               if(ae.getSource()==BPunch)
               {

                       int iSelected = 0;
                       String SErrMsg = "";
					   init();
                       start();
                       BPunch.setEnabled(false);
                       iPunchStatus=1;

               }
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
                    stop();

               }
               if(ae.getSource()==BRefresh)
               {
					setIssueModel();                
               }
			   
			   
			   
               
          }
     }


     private void setIssueModel()
     {

          try
          {
			  SAuthUserCode="";
			  
			   vDataVect = new Vector();
			  
               theModel.setNumRows(0);

               Connection theConnection=null;

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

			   
				PreparedStatement   ps = theConnection.prepareStatement(getIndentList());
				ResultSet           rs = ps.executeQuery();
				ResultSetMetaData rsmd = rs.getMetaData();
				while(rs.next()) 
				{
					HashMap theMap = new HashMap();

					for(int i=0;i<rsmd.getColumnCount();i++)
					{
						theMap.put(rsmd.getColumnName(i+1),rs.getString(i+1));
					}
					vDataVect.addElement(theMap);
				}
				rs . close();
				ps . close();
				
				String QS = " Select AuthUserCode from Indent where IndentNo='"+(String)JCType.getSelectedItem()+"' ";
				
				
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);

               if(result.next())
               {
                    SAuthUserCode = result.getString(1);
               }
               result.close();
               theStatement.close();
				
				
               int iSlNo=1;
				
				
				for(int i=0;i<vDataVect.size();i++) {
					
					
					HashMap	theMap 	= (HashMap)vDataVect.get(i);	
					
					Vector theVect = new Vector();

                    theVect.addElement(String.valueOf(iSlNo));
                    theVect.addElement(common.parseNull((String)theMap.get("CODE")));
					theVect.addElement(common.parseNull((String)theMap.get("ITEM_NAME")));
                    theVect.addElement(common.parseNull((String)theMap.get("UNIT_NAME")));
					theVect.addElement(common.parseNull((String)theMap.get("DEPT_NAME")));
					theVect.addElement(common.parseNull((String)theMap.get("INDENTNO")));
					theVect.addElement(common.parseNull((String)theMap.get("QTY")));
					theVect.addElement(common.parseNull((String)theMap.get("STOCKTYPE")));
					
                    theModel.appendRow(theVect);

                    iSlNo++;
					
				}
					
				
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private String getIndentList()
     {
			
		 
            String QS= "";
			
			
				QS = 
				" SELECT IssueNo, IssueDate,UIRefNo as Indentno, "+
				" Code, Item_Name, Qty, IssRate, "+
				" Unit_Name, Dept_Name, Group_Name,  "+
				" Authentication, Id,IssueValue,stocktype,TblName from ( "+
				" SELECT Issue.IssueNo, Issue.IssueDate, Issue.UIRefNo, "+
				" Issue.Code, InvItems.Item_Name, Issue.Qty, Issue.IssRate, "+
				" Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name,  "+
				" Issue.Authentication, Issue.Id,Issue.IssueValue,'Regular ISsue' as stocktype,'ISSUE' as TblName "+
				" FROM (((Issue  "+
				" INNER JOIN InvItems ON Issue.Code = InvItems.Item_Code)  "+
				" INNER JOIN Unit ON Issue.Unit_Code = Unit.Unit_Code)  "+
				" INNER JOIN Dept ON Issue.Dept_Code = Dept.Dept_code)  "+
				" INNER JOIN Cata ON Issue.Group_Code = Cata.Group_Code  "+
				" Where "+
				" ISsue.RECEIVEREMPCODE=0 "+
				" and  Issue.millcode="+iMillCode+" "+
				" and issue.RECEIVEREMPCODE is not null and Issue.UIRefNo="+common.toInt((String)JCType.getSelectedItem())+" "+ 
				"  union all "+
				" SELECT NonStockIssue.IssueNo, NonStockIssue.IssueDate, NonStockIssue.UIRefNo, "+
				" NonStockIssue.Code, InvItems.Item_Name, NonStockIssue.Qty, NonStockIssue.IssRate, "+
				"  Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name,  "+
				" NonStockIssue.Authentication, NonStockIssue.Id,0 as IssueValue,'Non Stock Issue' ,'NONSTOCKISSUE' as TblName "+
				" FROM (((NonStockIssue  "+
				" INNER JOIN InvItems ON NonStockIssue.Code = InvItems.Item_Code)  "+
				" INNER JOIN Unit ON NonStockIssue.Unit_Code = Unit.Unit_Code)  "+
				" INNER JOIN Dept ON NonStockIssue.Dept_Code = Dept.Dept_code)  "+
				" INNER JOIN Cata ON NonStockIssue.Group_Code = Cata.Group_Code  "+
				" Where NonStockIssue.RECEIVEREMPCODE=0 "+
				" And NonStockIssue.MillCode="+iMillCode+" and NonStockIssue.UIRefNo="+common.toInt((String)JCType.getSelectedItem())+"  "+
				"  union all "+
				" SELECT ServiceStockISsue.IssueNo, ServiceStockISsue.IssueDate, ServiceStockISsue.UIRefNo, "+
				" ServiceStockISsue.Code, InvItems.Item_Name, ServiceStockISsue.Qty, ServiceStockISsue.IssRate, "+
				" Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name,  "+
				" ServiceStockISsue.Authentication, ServiceStockISsue.Id,0 as IssueValue,'Service Stock Issue' ,'SERVICESTOCKISSUE' "+
				" FROM (((ServiceStockISsue  "+
				" INNER JOIN InvItems ON ServiceStockISsue.Code = InvItems.Item_Code)  "+
				" INNER JOIN Unit ON ServiceStockISsue.Unit_Code = Unit.Unit_Code)  "+
				" INNER JOIN Dept ON ServiceStockISsue.Dept_Code = Dept.Dept_code)  "+
				" INNER JOIN Cata ON ServiceStockISsue.Group_Code = Cata.Group_Code  "+
				" Where ServiceStockISsue.RECEIVEREMPCODE=0 "+
				" And ServiceStockISsue.MillCode="+iMillCode+"  and ServiceStockISsue.UIRefNo="+common.toInt((String)JCType.getSelectedItem())+"  "+
				" ) ";
				
			
			   
			   
			   //System.out.println("Issue Location Qry"+QS);


          return QS;
     }

	private class WindowList extends WindowAdapter
	{
		public void windowClosing(WindowEvent we)
		{
			stop();		
		}	
	}

                         /*   INSERT AND UPDATIONS*/

     public boolean setData(String SCode,String SName)
     {
          stop();
          
          int       iIndentNo          = common.toInt((String)JCType.getSelectedItem());

          try
          {
               if(theMConnection.getAutoCommit())
                    theMConnection . setAutoCommit(false);


               Statement      stat          =  theMConnection.createStatement();
               PreparedStatement thePrepare = null;


               for(int i=0;i<theModel.getRows();i++)
               {
				   
					HashMap	theMap 	= (HashMap)vDataVect.get(i);	
                    
                    String SItemCode    = common.parseNull((String)theMap.get("CODE"));
					String SIndentId    = common.parseNull((String)theMap.get("ID"));
					
                    String STableName   = common.parseNull((String)theMap.get("TBLNAME"));
					
					System.out.println(STableName);
					System.out.println(SItemCode);
					System.out.println(SIndentId);
					System.out.println(SCode);
					

                    //String str1       = " Update "+STableName+" set ReceiverEmpCode= ? where IndentNo=? and Code=? and MillCode=? and Id=? and ReceiverEmpCode=0  ";
					
					String str1       = " Update "+STableName+" set ReceiverEmpCode="+SCode+" where UIREFNO="+iIndentNo+"	and Code='"+SItemCode+"' and MillCode="+iMillCode+" and Id="+SIndentId+" and ReceiverEmpCode=0  ";
					
					System.out.println(str1)	;

                    thePrepare =  theMConnection.prepareStatement(str1);
					
                    /*thePrepare.setString(1,SCode);
					thePrepare.setInt(2,iIndentNo);					
					thePrepare.setString(3,SItemCode);
					thePrepare.setInt(4,iMillCode);
                    thePrepare.setString(5,SIndentId);*/
                    thePrepare.executeUpdate();
                    thePrepare.close();

               }



          }
          catch(Exception ex)
          {
               try
               {
                    theMConnection.rollback();
               }
               catch(Exception e)
               {
               }
               ex        .printStackTrace();

               bComflag  = false;
          }
          setComOrRol();

          return bComflag;
     }

     public void setComOrRol()
     {
          if(bComflag)
          {
               try
               {
                    theMConnection . commit();

                    if(iMillCode==1)
                         theDConnection . commit();
               } catch(Exception ex)
               {
                    System.out.println("Indent frame actionPerformed ->"+ex);
                    ex.printStackTrace();
               }
          }
          else
          {
               try
               {
                    theMConnection . rollback();

                    if(iMillCode==1)
                         theDConnection . rollback();

                    BOk            . setEnabled(true);
               }catch(Exception ex)
               {
                    System.out.println("Indent frame actionPerformed ->"+ex);
                    ex.printStackTrace();
               }
          }
          try
          {
               theMConnection . setAutoCommit(true);

               if(iMillCode==1)
                    theDConnection . setAutoCommit(true);
          }catch(Exception ex)
          {
               System.out.println("Indent frame actionPerformed ->"+ex);
               ex.printStackTrace();
          }
     }



     public void setIndentCode()
     {
          VIndentTypeName     = new Vector();

		  
		  
				String QS = " select distinct IndentNo from ( "+
				" SELECT IssueNo, IssueDate,UIRefNo as Indentno, "+
				" Code, Item_Name, Qty, IssRate, "+
				" Unit_Name, Dept_Name, Group_Name,  "+
				" Authentication, Id,IssueValue,stocktype,TblName from ( "+
				" SELECT Issue.IssueNo, Issue.IssueDate, Issue.UIRefNo, "+
				" Issue.Code, InvItems.Item_Name, Issue.Qty, Issue.IssRate, "+
				" Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name,  "+
				" Issue.Authentication, Issue.Id,Issue.IssueValue,'Regular ISsue' as stocktype,'ISSUE' as TblName "+
				" FROM (((Issue  "+
				" INNER JOIN InvItems ON Issue.Code = InvItems.Item_Code)  "+
				" INNER JOIN Unit ON Issue.Unit_Code = Unit.Unit_Code)  "+
				" INNER JOIN Dept ON Issue.Dept_Code = Dept.Dept_code)  "+
				" INNER JOIN Cata ON Issue.Group_Code = Cata.Group_Code  "+
				" Where "+
				"  "+
				"  Issue.millcode="+iMillCode+" "+
				" and issue. RECEIVEREMPCODE=0 and issue.Issuedate>=20200619 "+
				"  union all "+
				" SELECT NonStockIssue.IssueNo, NonStockIssue.IssueDate, NonStockIssue.UIRefNo, "+
				" NonStockIssue.Code, InvItems.Item_Name, NonStockIssue.Qty, NonStockIssue.IssRate, "+
				"  Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name,  "+
				" NonStockIssue.Authentication, NonStockIssue.Id,0 as IssueValue,'Non Stock Issue' ,'NONSTOCKISSUE' as TblName "+
				" FROM (((NonStockIssue  "+
				" INNER JOIN InvItems ON NonStockIssue.Code = InvItems.Item_Code)  "+
				" INNER JOIN Unit ON NonStockIssue.Unit_Code = Unit.Unit_Code)  "+
				" INNER JOIN Dept ON NonStockIssue.Dept_Code = Dept.Dept_code)  "+
				" INNER JOIN Cata ON NonStockIssue.Group_Code = Cata.Group_Code  "+
				" Where NonStockIssue.RECEIVEREMPCODE=0 "+
				" And NonStockIssue.MillCode="+iMillCode+"  and NonStockIssue.Issuedate>=20200619  "+
				"  union all "+
				" SELECT ServiceStockISsue.IssueNo, ServiceStockISsue.IssueDate, ServiceStockISsue.UIRefNo, "+
				" ServiceStockISsue.Code, InvItems.Item_Name, ServiceStockISsue.Qty, ServiceStockISsue.IssRate, "+
				" Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name,  "+
				" ServiceStockISsue.Authentication, ServiceStockISsue.Id,0 as IssueValue,'Service Stock Issue' ,'SERVICESTOCKISSUE' "+
				" FROM (((ServiceStockISsue  "+
				" INNER JOIN InvItems ON ServiceStockISsue.Code = InvItems.Item_Code)  "+
				" INNER JOIN Unit ON ServiceStockISsue.Unit_Code = Unit.Unit_Code)  "+
				" INNER JOIN Dept ON ServiceStockISsue.Dept_Code = Dept.Dept_code)  "+
				" INNER JOIN Cata ON ServiceStockISsue.Group_Code = Cata.Group_Code  "+
				" Where ServiceStockISsue.RECEIVEREMPCODE=0 "+
				" And ServiceStockISsue.MillCode="+iMillCode+" and ServiceStockISsue.Issuedate>=20200619   "+
				" ) "+
				" ) Order by 1 ";

		  
		  

          try  {
                    ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                    Connection     theConnection  = oraConnection.getConnection();   
                    Statement      stat           = theConnection.createStatement();
     
                    ResultSet result = stat.executeQuery(QS);


                    while(result.next())
                    {
                         VIndentTypeName     .addElement(common.parseNull(result.getString(1)));
                    }
                    result    .close();
                    stat      .close();
					
					
          } catch(Exception Ex)
          {
               System.out.println("getIndentCode from Indent "+Ex);
          }
     }
     public boolean   checkEligiblePerson(String SEmpCode)
     {
          int iValue =0;
          boolean bFlag = false;


          String QS ="";


          if(iMillCode==1)
          {
               QS= " select count(1) from DyeingIssueReceiverList Where UserCode in(select AuthUsercode from MrsUserAuthentication where userCode="+SAuthUserCode+")"+
                           " and DyeingIssueReceiverList.EmpCode="+SEmpCode;


          }

          else
          {
               QS= " select count(1) from IssueReceiverList Where UserCode in(select AuthUsercode from MrsUserAuthentication where userCode="+SAuthUserCode+")"+
                           " and IssueReceiverList.EmpCode="+SEmpCode;

          }



          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);

               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result.close();
               theStatement.close();


               if(iValue>0)
                  bFlag=true;

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               return bFlag;
          }

                      
          return bFlag;
     }

     private String getControlTimeQS(int iTypeCode)
     {

          String QS=" Select (to_date(controlTime,'hh24:mi')-(to_date(to_Char(sysdate,'hh24:mi'),'hh24:mi')))*(24*60) as Diff from"+
                    " TimeControl"+
                    " Where TypeCode="+iTypeCode;


          return QS;
     }

    protected void init()        
    {

		capturer.addDataListener(new DPFPDataAdapter() {
                public void dataAcquired(final DPFPDataEvent e) {
                    LStatus.setText(" Please wait Under processing.................");

				SwingUtilities.invokeLater(new Runnable()
                                {
                                        public void run()
                                        {
                                                LStatus.setText("The fingerprint sample was captured....");
                                                process(e.getSample());
                                        }});
			}
		});
		capturer.addReaderStatusListener(new DPFPReaderStatusAdapter() {
                public void readerConnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
		 			makeReport("The fingerprint reader was connected.");
				}});
			}
                public void readerDisconnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was disconnected.");
				}});
			}
		});
		capturer.addSensorListener(new DPFPSensorAdapter() {
                public void fingerTouched(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was touched.");
				}});
			}
                public void fingerGone(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The finger was removed from the fingerprint reader.");
				}});
			}
		});
		capturer.addImageQualityListener(new DPFPImageQualityAdapter() {
                public void onImageQuality(final DPFPImageQualityEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					if (e.getFeedback().equals(DPFPCaptureFeedback.CAPTURE_FEEDBACK_GOOD))
						makeReport("The quality of the fingerprint sample is good.");
					else
						makeReport("The quality of the fingerprint sample is poor.");
				}});
			}
		});
	}

	protected void process(DPFPSample sample)
	{
		// Draw fingerprint sample image.
		
		DPFPFeatureSet features = extractFeatures(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

		// Check quality of the sample and start verification if it's good
		int iStatus=0;
   	    String SName  = "";
		String SCode  = "";
          stop();
          System.out.println("Process Status Comming "+iProcessStatus);

          if(iProcessStatus==1)
               return;

          //System.out.println(iIndentNo+" Process Status Comming "+iProcessStatus);


          LStatus.setText(" Under processing.................");
          iProcessStatus=1;


		if (features != null)
		{
			// Compare the feature set with our template



               for(int i=0; i<theMaterialIssueTemplateList.size(); i++)
               {
                    HashMap theMap = (HashMap) theMaterialIssueTemplateList.get(i);

                    SCode    = (String)theMap.get("EMPCODE");
                    SName    = (String)theMap.get("DISPLAYNAME");
                    
                    DPFPTemplate template   = (DPFPTemplate)theMap.get("TEMPLATE");

                    DPFPVerification matcher= DPFPGlobal.getVerificationFactory().createVerification();
                    matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);

                    DPFPVerificationResult result = 
                    matcher.verify(features,template);

                    if (result.isVerified())
                    {
                         LStatus.setText(""+SName);       
                         iStatus=1;
                         break;
                    }

               }

                  if(iStatus==0)
                  {
                         LStatus.setText("No Match Found");       
                         JOptionPane.showMessageDialog(null," No Match Found Punch Again");
                         stop();
                         iPunchStatus=0;
                         BPunch.setEnabled(true);
                         iProcessStatus=0;

                  }
                  else
                  {
                       if(!checkEligiblePerson(SCode))
                       {
                            JOptionPane.showMessageDialog(null," Person Not Elgible for Taking this Material"+SCode);
                            iProcessStatus=0;
							 stop();
							 iPunchStatus=0;
							 BPunch.setEnabled(true);

                            return;
                       }


                       int iSelected = 0;

                       String SErrMsg = "";

                       if(setData(SCode,SName))
                       {
                            JOptionPane.showMessageDialog(null," Punching Regestered Sucessfully");
                            setIssueModel();
                            removeHelpFrame();
                            LStatus.setText(" Punch And Take Issue");
                            stop();
                            iPunchStatus=0;
                            BPunch.setEnabled(true);
                            iProcessStatus=0;

                       }
                       else
                       {
        
                            JOptionPane.showMessageDialog(null," Problem in Storing Data");
                            stop();
                            iPunchStatus=0;
                             BPunch.setEnabled(true);
                            iProcessStatus=0;

                       }
                  }
          }
	}
	protected void start()
	{
		capturer.startCapture();
	}

	protected void stop()
	{
		capturer.stopCapture();
	}
	public void setPrompt(String string) {
	}
	public void makeReport(String string) {
	}
	
	protected Image convertSampleToBitmap(DPFPSample sample) {
		return DPFPGlobal.getSampleConversionFactory().createImage(sample);
	}

	protected DPFPFeatureSet extractFeatures(DPFPSample sample, DPFPDataPurpose purpose)
	{
		DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
		try {
			return extractor.createFeatureSet(sample, purpose);
		} catch (DPFPImageQualityException e)
        {
            System.out.println(" Returning Null");

        	return null;
		}
	}
	private void updateStatus(int FAR)
	{
          System.out.println(FAR);
	}
	
	
	
	
}




