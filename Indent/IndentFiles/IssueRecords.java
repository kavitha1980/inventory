package Indent.IndentFiles;

import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class IssueRecords
{
     String SRefNo = "",SUnit = "";
     Vector VIssueCode,VIssueName,VIssueUom,VIssueDept,VIssueGroup;
     Vector VStock,VIssueQty,VIssueRate,VIssueValue,VId;
     int iMaxSlNo = 0;
     boolean isReturn = false;
     
     Common common = new Common();
     String SIssueNo,SIssueDate,SIndentNo;
     int iMillCode;
     String SItemTable,SSupTable;
     
     IssueRecords(String SIssueNo,String SIssueDate,String SIndentNo,int iMillCode,String SItemTable,String SSupTable)
     {
          this.SIssueNo   = SIssueNo;
          this.SIssueDate = SIssueDate;
          this.SIndentNo  = SIndentNo;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          
          VIssueCode  = new Vector();
          VIssueName  = new Vector();
          VIssueUom   = new Vector();
          VIssueDept  = new Vector();
          VIssueGroup = new Vector();
          VStock      = new Vector();
          VIssueQty   = new Vector();
          VIssueRate  = new Vector();
          VIssueValue = new Vector();
          VId         = new Vector();
          
          ResultSet result    = null;
          String QS           = "";
          String SQuery       = "";

          SQuery =  " SELECT max(Issue.SlNo) From "+
                    " ( "+
                    " (((Issue   "+
                    " INNER JOIN Unit ON Issue.Unit_Code=Unit.Unit_Code)  "+
                    " INNER JOIN InvItems ON Issue.Code=InvItems.Item_Code)   "+
                    " INNER JOIN UOM ON UOM.UOMCode=InvItems.UOMCode)   "+
                    " INNER JOIN Dept ON Issue.Dept_Code=Dept.Dept_code)   "+
                    " INNER JOIN Cata ON Issue.Group_Code=Cata.Group_Code   "+
                    " Where Issue.IssueNo = "+SIssueNo+
                    " And Issue.IssueDate='"+SIssueDate+"'"+
                    " And Issue.UIRefNo="+SIndentNo+
                    " And Issue.MillCode = "+iMillCode;


          QS =      " SELECT Issue.IssueDate, Issue.UIRefNo, Unit.Unit_Name, "+
                    " Issue.Code, InvItems.Item_Name, UOM.UoMName, "+
                    " Dept.Dept_Name, Cata.Group_Name, Issue.Qty, "+
                    " Issue.IssRate, Issue.Id "+
                    " From ((((Issue "+
                    " INNER JOIN Unit ON Issue.Unit_Code=Unit.Unit_Code) "+
                    " INNER JOIN InvItems ON Issue.Code=InvItems.Item_Code) "+
                    " INNER JOIN UOM ON UOM.UOMCode=InvItems.UOMCode) "+
                    " INNER JOIN Dept ON Issue.Dept_Code=Dept.Dept_code) "+
                    " INNER JOIN Cata ON Issue.Group_Code=Cata.Group_Code "+
                    " Where Issue.IssueNo = "+SIssueNo+
                    " And Issue.IssueDate='"+SIssueDate+"'"+
                    " And Issue.UIRefNo="+SIndentNo+
                    " And Issue.MillCode = "+iMillCode;

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();

               result  = stat.executeQuery(QS);
               while (result.next())
               {
                    SIssueDate   = common.parseDate(result.getString(1));
                    SRefNo       = result.getString(2);
                    SUnit        = result.getString(3);
                    double dQty  = common.toDouble(result.getString(9));
                    double dRate = common.toDouble(result.getString(10));
                    
                    String SItemCode    = result.getString(4);
                    Item      IC        = new Item(SItemCode,iMillCode,SItemTable,SSupTable);
                    
                    double dStock  = common.toDouble(IC.getClStock());
                    String SStock  = common.getRound(dStock,3);
                    double dValue  = dQty*dRate;
                    String SValue  = common.getRound(dValue,2);
                    
                    VIssueCode  . addElement(SItemCode);
                    VIssueName  . addElement(result.getString(5));
                    VIssueUom   . addElement(result.getString(6));
                    VIssueDept  . addElement(result.getString(7));
                    VIssueGroup . addElement(result.getString(8));
                    VStock      . addElement(SStock);
                    VIssueQty   . addElement(""+dQty);
                    VIssueRate  . addElement(""+dRate);
                    VIssueValue . addElement(SValue);
                    VId         . addElement(result.getString(11));
                    
                    if(dQty<0)
                         isReturn = true;
               }
               result.close();
               result  = stat.executeQuery(SQuery);
               result.next();
               iMaxSlNo  =    common.toInt(((String)result.getString(1)).trim());
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}
