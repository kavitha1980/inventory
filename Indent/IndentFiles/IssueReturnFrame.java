package Indent.IndentFiles;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class IssueReturnFrame extends JInternalFrame
{
     DateField              TDate;
     int                    i;
     JComboBox              JCUDept,JCUnit,JCType,JCCata;
     MyComboBox             JCUser;
     NextField              TIndIssNo,TRefNo;
     JTextField             TValue;
     Vector                 VUDept,VUDeptCode,VUnit,VUnitCode,VCata,VCataCode;
     Vector                 VIUserCode,VIUserName;
     Vector                 VGUserCode;
     Vector                 VSeleCode,VSeleStock;
     JButton                BApply,BOk,BCancel;
     JPanel                 TopPanel,BottomPanel;
     JPanel                 Top1Panel,Top2Panel;
     IssueReturnMiddlePanel MiddlePanel;
     JLayeredPane           Layer;
     Common                 common = new Common();
     Control                control;

     String                 SIndNo = "",SIssueNo = "";
     Vector                 VName,VCode,VUOMName,VStkGpCode,VNameCode;
     Vector                 VIndentTypeCode,VIndentTypeName;
     JLabel                 LIndIss;

     StatusPanel            SPanel;
     int                    iUserCode,iMillCode;
     String                 SYearCode,SItemTable,SSupTable;

     Connection             theMConnection =    null;
     Connection             theDConnection =    null;
                     
     boolean                isReturn       =    false;
     boolean                bComflag       =    true;

     public IssueReturnFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VNameCode,Vector VUOMName,Vector VStkGpCode,Vector VIndentTypeCode,Vector VIndentTypeName,StatusPanel SPanel,int iUserCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable)
     {
          this.Layer          = Layer;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.VUOMName       = VUOMName;
          this.VStkGpCode     = VStkGpCode;
          this.VIndentTypeCode= VIndentTypeCode;
          this.VIndentTypeName= VIndentTypeName;
          this.SPanel         = SPanel;
          this.iUserCode      = iUserCode;
          this.iMillCode      = iMillCode;
          this.SYearCode      = SYearCode;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;

          getDBConnections();
          getUsers(theMConnection);
          createComponents();
          setLayouts();
          addComponents();    
          addListeners();
     }

     public void getDBConnections()
     {
          ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                         theMConnection = oraConnection.getConnection();

          if(iMillCode==1)
          {
               DORAConnection DoraConnection = DORAConnection.getORAConnection();
                              theDConnection = DoraConnection.getConnection();
          }
     }

     public void createComponents()
     {
          try
          {
               BApply         = new JButton("Apply");
               BOk            = new JButton("Okay");
               BCancel        = new JButton("Cancel");

               JCUser         = new MyComboBox(VIUserName);
               JCUDept        = new JComboBox(VUDept);
               JCUnit         = new JComboBox(VUnit);
               JCType         = new JComboBox(VIndentTypeName);
               JCCata         = new JComboBox(VCata);
               TIndIssNo      = new NextField();
               TRefNo         = new NextField();
               TDate          = new DateField();
               TValue         = new JTextField(10);
               LIndIss        = new JLabel("  Indent No");

               TopPanel       = new JPanel();
               Top1Panel      = new JPanel();
               Top2Panel      = new JPanel();
               BottomPanel    = new JPanel();
               control        = new Control();

               getIssueNo();
               getIndentNo();

               TRefNo    . setEditable(false);
               TDate     . setTodayDate();
               TDate     . setEditable(false);
               TIndIssNo . setEditable(false);
               TValue    . setEditable(false);
               TValue    . setHorizontalAlignment(JTextField.RIGHT);
               TValue    . setBorder(new TitledBorder("Net Value of this Document"));
               
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setLayouts()
     {
          try
          {
               setTitle("Direct Issue");
               setMaximizable(true);
               setClosable(true);
               setIconifiable(true);
               setResizable(true);
               setBounds(0,0,795,495);

               getContentPane()    .setLayout(new BorderLayout());
               TopPanel            .setLayout(new BorderLayout());
               Top1Panel           .setLayout(new FlowLayout());
               Top2Panel           .setLayout(new GridLayout(3,2));
               BottomPanel         .setLayout(new GridLayout(1,2));
               TopPanel            .setBorder(new TitledBorder("Control Block"));
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void addComponents()
     {
          Top1Panel.add(new JLabel("Select User"));
          Top1Panel.add(JCUser);
          Top1Panel.add(new JLabel(""));
          Top1Panel.add(BApply);

          TopPanel.add("North",Top1Panel);

          getContentPane() . add("North",TopPanel);
     }


     public void addListeners()
     {
          BApply    . addActionListener(new ActList());
          BOk       . addActionListener(new ActList());
          JCType    . addItemListener(new ItemTypeList());
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer. remove(this);
               Layer. repaint();
               Layer. updateUI();
          }
          catch(Exception ex) { }
     }
     
     public class ItemTypeList implements ItemListener
     {
          public void itemStateChanged(ItemEvent e)
          {  
               setIndentIssueNo();
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
                    BApply.setEnabled(false);
                    JCUser.setEnabled(false);
                    String SAuthUserCode = (String)VIUserCode.elementAt(JCUser.getSelectedIndex());
                    //getGroupUserCode();
                    MiddlePanel    = new IssueReturnMiddlePanel(Layer,VCode,VName,VUOMName,VNameCode,JCUnit,VUnitCode,JCCata,TValue,VCata,VCataCode,iUserCode,iMillCode,SItemTable,SSupTable,SAuthUserCode);
                    JCType.setSelectedIndex(1);
                    JCType.setEnabled(false);
                    setIndentIssueNo();
                    setIssueData();
               }
               if(ae.getSource()==BOk)
               {
                    BOk  .setEnabled(false);
                    if(setData())
                    {
                         removeHelpFrame();
                    }
                    else
                    {
                         BOk  . setEnabled(true);
                    }
               }
          }
     }

     public void getGroupUserCode()
     {
          VGUserCode = new Vector();

          String SAuthUserCode = (String)VIUserCode.elementAt(JCUser.getSelectedIndex());

          String SGroupUserCode = "";

          VGUserCode.addElement("1");
          
          try
          {
               Statement      stat   = theMConnection.createStatement();
               ResultSet      result = stat.executeQuery("Select GroupUserCode from UserGroup Where AuthUserCode="+SAuthUserCode+" and MillCode="+iMillCode);

               while(result.next())
               {
                    SGroupUserCode = common.parseNull(result.getString(1));
               }
               result.close();
               
               if(SGroupUserCode.equals(""))
               {
                    VGUserCode.addElement(SAuthUserCode);
               }
               else
               {
                    result = stat.executeQuery("Select AuthUserCode from UserGroup Where GroupUserCode="+SGroupUserCode+" and MillCode="+iMillCode+" Order by AuthUserCode");
     
                    while(result.next())
                    {
                         VGUserCode.addElement(common.parseNull(result.getString(1)));
                    }
                    result.close();
               }
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("GroupUser :"+ex);
          }
     }

     public void setIssueData()
     {

          Top2Panel        . add(new JLabel(" User Indent No"));
          Top2Panel        . add(TRefNo);

          Top2Panel        . add(new JLabel("  Date"));
          Top2Panel        . add(TDate);

          Top2Panel        . add(new JLabel(" Unit"));
          Top2Panel        . add(JCUnit);

          Top2Panel        . add(new JLabel("  Indent Type"));
          Top2Panel        . add(JCType);

          Top2Panel        . add(new JLabel(" User Department"));
          Top2Panel        . add(JCUDept);

          Top2Panel        . add(LIndIss);
          Top2Panel        . add(TIndIssNo);

          BottomPanel      . add(TValue);
          BottomPanel      . add(BOk);

          TopPanel         . add("Center",Top2Panel);

          getContentPane() . add("Center",MiddlePanel);
          getContentPane() . add("South",BottomPanel);

          Layer. repaint();
          Layer. updateUI();
     }


     private int getCategoryCode(int iRow)
     {
          String SCata = ((String)MiddlePanel.tabreport.RowData[iRow][5]).trim();
          if(SCata.length() == 0)
               return 0;  
          int iCata       = VCata.indexOf(SCata);
          return Integer.parseInt((String)VCataCode.elementAt(iCata));
     }

     private String getItemCode(String SItemName)
     {
          SItemName = SItemName.trim();

          for(int i = 0;i<VName.size();i++)
          {
               String SItName   = ((String)VName.elementAt(i)).trim();
               if(SItName.equals(SItemName))
               {
                    return ((String)VCode.elementAt(i));
               }
          }
          return "";
     }

     private void setIndentIssueNo()
     {
          if(JCType.getSelectedIndex() == 0 )
          {
               TIndIssNo.setText(SIssueNo);
               setTitle("Issue Slip - Placement");
               LIndIss.setText("  Issue No");
               isReturn = false;
               MiddlePanel.tabreport.isReturn = false;
          }
          else
          if(JCType.getSelectedIndex() == 2 )
          {
               TIndIssNo.setText(SIssueNo);
               setTitle("Issue Slip for SubStore - Placement");
               LIndIss.setText("  Issue No");
               isReturn = false;
               MiddlePanel.tabreport.isReturn = false;
          }
          else
          {     
               TIndIssNo.setText(SIssueNo);
               TRefNo.setText(SIndNo);
               setTitle("Return - Placement");
               LIndIss.setText("  ");
               isReturn = true;
               MiddlePanel.tabreport.isReturn = true;
          }
     }
                    /*   DATA FROM SERVER */

     public int checkIndentNo(String SURefNo)
     {
          int iCount=0;

          ResultSet result    = null;

          String SQuery    = " Select Count(*) from issue where UIRefNo="+SURefNo+" and millcode ="+iMillCode+" order by 1" ;

          try  {
                    ORAConnection  oraConnection  = ORAConnection. getORAConnection();
                    Connection     theConnect     = oraConnection. getConnection();
                    Statement      stat           = theConnect   . createStatement();
                                   result         = stat         . executeQuery(SQuery);
                    while(result.next())
                    {
                         iCount = common.toInt((String)result.getString(1));
                    }
                    result.close();
                    stat.close();
          }catch(Exception ex)
          {
               System.out.println("getmaterials() from indentframe  ->"+ex);
               return 0;
          }
          return iCount;
     }

     public void getIssueNo()
     {
          try
          {
               SIssueNo  = String.valueOf(common.toInt(control.getID("Select maxno From Config"+iMillCode+""+SYearCode+" where Id = 5 "))+1);
          }
          catch(Exception ex)
          {
               System.out.println(" getIssueNo ->"+ex);
               ex.printStackTrace();
          }
     }

     public void getIndentNo()
     {
          try
          {
               SIndNo = String.valueOf(common.toInt(control.getID("Select maxno From ConfigAll where Id = 2 "))+1);
          }
          catch(Exception ex)
          {
               System.out.println(" getIndentNo ->"+ex);
               ex.printStackTrace();
          }
     }

     public void getUsers(Connection theConnection)
     {
          ResultSet result = null;

          VUDept         = new Vector();
          VUDeptCode     = new Vector();
          VUnit          = new Vector();
          VUnitCode      = new Vector();
          VCata          = new Vector();
          VCataCode      = new Vector();
          VIUserCode     = new Vector();
          VIUserName     = new Vector();

          try
          {
               Statement      stat   = theConnection.createStatement();
                              result = stat.executeQuery("Select HodName,HodCode From Hod Order By HodName");

               while(result.next())
               {
                    VUDept         . addElement(result.getString(1));
                    VUDeptCode     . addElement(result.getString(2));
               }
               result.close();
               
               String QS1 = "";
               String QS2 = "";
               String QS3 = "";
               QS1 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";
               QS2 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               QS3 = " Select UserCode,UserName from RawUser Where UserCode in (Select Distinct(AuthUserCode) from MrsUserAuthentication Where AuthUserCode>1 and authusercode<>6125) Order by UserName ";
               
               result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VUnit     . addElement(result.getString(1));
                    VUnitCode . addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VCata     . addElement(result.getString(1));
                    VCataCode . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery(QS3);
               while(result.next())
               {
                    VIUserCode. addElement(result.getString(1));
                    VIUserName. addElement(result.getString(2));
               }
               result    . close();
               stat      . close();

               if(VIUserCode.size()<=0)
               {
                    VIUserCode.addElement("1");
                    VIUserName.addElement("ENTRY LEVEL");
               }
          }
          catch(Exception ex)
          {
               System.out.println("Indent XyX :"+ex);
          }
     }

     private int getID()
     {
          Statement stat      = null;
          ResultSet result    = null;

          int iId=0;
          try
          {
               stat      = theDConnection.createStatement();
               result    = stat.executeQuery("Select SubStoreReceipt_Seq.nextVal from Dual");

               while(result.next())
               {
                    iId  =    result.getInt(1);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return iId;
     }

     private int getIssueID()
     {
          Statement stat      = null;
          ResultSet result    = null;

          int iId=0;
          try
          {
               stat      =    theMConnection.createStatement();
               result    =    stat.executeQuery("Select Issue_Seq.nextVal from Dual");

               while(result.next())
               {
                    iId  =    result.getInt(1);
               }
               result    .close();
               stat      .close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return iId;
     }

     public int getIndentCode(String SIndentName)
     {
          int iIndex    = VIndentTypeName.indexOf(SIndentName.trim());

          return common.toInt(((String)VIndentTypeCode.elementAt(iIndex)).trim());
     }

                         /*   DATA VALIDATION*/

     private boolean isStockValid()
     {
          boolean bflag = true;
          
          if(((String)JCType.getSelectedItem()).equals("Return"))
          {
               bflag = true;
          }
          else
          {
               for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
               {
                    String Smsg="";
                    String SCode        = ((String)MiddlePanel.tabreport.RowData[i][1]).trim();
                    double dIssueQty    = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][7]).trim());
                    String SEnDate      = TDate.toNormal();
                    
                    if(SCode.length() == 0)
                         continue;
                    
                    Item IC = new Item(SCode,SEnDate,iMillCode,SItemTable,SSupTable,VGUserCode);
                    
                    double dStock = common.toDouble(IC.getClStock());
                    
                    if((dStock-dIssueQty)<0)
                         Smsg = Smsg + " Negative Stock for the Date,"; 

                    if(Smsg.length()>0)
                    {
                         Smsg = Smsg.substring(0,(Smsg.length()-1));
                         Smsg = Smsg + " in S.No - " + (i+1);
                         JOptionPane.showMessageDialog(null,Smsg,"Invalid Quantity",JOptionPane.INFORMATION_MESSAGE);
                         bflag=false;
                    }
               }
          }
          return bflag;
     }
     
     private boolean isDataValid()
     {
          boolean bflag = true;

          for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
          {
               String Smsg="";
               String SCode         = ((String)MiddlePanel.tabreport.RowData[i][1]).trim();

               if(i==0)
               {
                    if(SCode.length()==0)
                    {
                         JOptionPane.showMessageDialog(null,"No Data in the Table","Missing",JOptionPane.INFORMATION_MESSAGE);
                         return false;
                    }
               }

               if(SCode.length() == 0)
                    continue;

               if(MiddlePanel.getDepartmentCode(i)==0)
                    Smsg = Smsg + "Department,";
               if(getCategoryCode(i)==0)
                    Smsg = Smsg + " Classification,";

               if(!isEligible(i))
                    Smsg = Smsg + " Quantity,";

               if(Smsg.length()>0)
               {
                    Smsg = Smsg.substring(0,(Smsg.length()-1));
                    Smsg = Smsg + "  Field(s) Missing in S.No - " + (i+1);
                    JOptionPane.showMessageDialog(null,Smsg,"Missing Fields",JOptionPane.INFORMATION_MESSAGE);
                    bflag=false;
               }
          }
          return bflag;
     }

     private boolean isEligible(int iRowCount)
     {
          double iQty         = common.toDouble(common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(iRowCount,7)));
          String SItemCode    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(iRowCount,1));

          if(iQty==0)
               return false;
          if(SItemCode.length()<3)
               return false;

          return true;
     }
                         /*   INSERT AND UPDATIONS*/
     public boolean setData()
     {
          String    SUDeptCode      = (String)VUDeptCode.elementAt(JCUDept.getSelectedIndex());
          int       iType           = JCType.getSelectedIndex();
          int       iCata           = JCCata.getSelectedIndex();
          String    SIndentUserCode = (String)VIUserCode.elementAt(JCUser.getSelectedIndex());

          String    str            = "";
          int       iMSlNo         = 0;

          if(!isStockValid())
          {
               return false;
          }
          
          if(!isDataValid())
          {
               return false;
          }

          getIssueNo();
          getIndentNo();
          setIndentIssueNo();
          try
          {
               Statement      stat          =  theMConnection.createStatement();

               for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
               {
                    if(!isEligible(i))
                         continue;


                    double dValue       = 0;

                    double dCurStock    = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,6));
                    double dQty         = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,7));
                    double dRate        = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,8));
                    String SItemName    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,2));
                    String SItemCode    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,1));

                    if(iType == 1)
                    {
                         dQty   = dQty*(-1);
                         dValue = dQty*dRate;
                    }

                    str = "";


                    int iCount = checkUserItemStock(stat,SItemCode,SIndentUserCode);

                    iMSlNo++;

                    str = "Insert Into Issue(id,IssueNo,IssueDate,Code,HodCode,Qty,IssRate,UIRefNo,Dept_Code,Group_Code,Unit_Code,UserCode,CreationDate,issueValue,SlNo,IndentType,MrsAuthUserCode,IndentUserCode,MillCode) Values (";
                    str = str+ getIssueID()+",";
                    str = str+ TIndIssNo.getText()+",";
                    str = str+"'"+TDate.toNormal()+"',";
                    str = str+"'"+SItemCode+"',";
                    str = str+(String)VUDeptCode.elementAt(JCUDept.getSelectedIndex())+",";
                    str = str+common.getRound(dQty,3)+",";
                    str = str+common.getRound(dRate,4)+",";
                    str = str+"0"+TRefNo.getText()+",";
                    str = str+MiddlePanel.getDepartmentCode(i)+",";
                    str = str+getCategoryCode(i)+",";
                    str = str+(String)VUnitCode.elementAt(JCUnit.getSelectedIndex())+",";
                    str = str+String.valueOf(iUserCode)+",";
                    str = str+"'"+common.getServerDate()+"',";
                    str = str+common.getRound(dValue,2)+",";
                    str = str+String.valueOf(iMSlNo)+",";
                    str = str+getIndentCode(((String)JCType.getSelectedItem()).trim())+",";
                    str = str+SIndentUserCode+",";
                    str = str+SIndentUserCode+",";
                    str = str+String.valueOf(iMillCode)+")";

                    String QS1 = "";

                    if(iCount>0)
                    {

                         QS1 = " Update ItemStock set Stock=nvl(Stock,0)-("+dQty+")"+
                               " Where ItemCode='"+SItemCode+"' and HodCode="+SIndentUserCode+" and MillCode="+iMillCode;
                    }
                    else
                    {
                         QS1 = " Insert into ItemStock(MillCode,HodCode,ItemCode,Stock) Values ("+
                               "0"+iMillCode+","+SIndentUserCode+",'"+SItemCode+"',"+"((-1)*"+dQty+"))";
                    }


                    String QS2 = " Update ItemStock set StockValue="+common.getRound(dRate,4)+
                                 " Where ItemCode='"+SItemCode+"' and MillCode="+iMillCode;


                    if(theMConnection.getAutoCommit())
                         theMConnection . setAutoCommit(false);

                    stat.execute(str);
                    stat.execute(QS1);
                    stat.execute(QS2);
               }

               stat .close();

               if(iMillCode==1)
               {
                    updateSubStoreMasterData();
               }

               updateInvitemsData();
               UpdatedIssueNo();
               UpdateIndentNo();
          }
          catch(Exception ex)
          {
               ex        .printStackTrace();
               bComflag  = false;
          }
          setComOrRol();

          return bComflag;
     }

     public void setComOrRol()
     {
          if(bComflag)
          {
               try
               {
                    theMConnection . commit();

                    if(iMillCode==1)
                         theDConnection . commit();
               } catch(Exception ex)
               {
                    System.out.println("Indent frame actionPerformed ->"+ex);
                    ex.printStackTrace();
               }
          }
          else
          {
               try
               {
                    theMConnection . rollback();

                    if(iMillCode==1)
                         theDConnection . rollback();

                    BOk            . setEnabled(true);
               }catch(Exception ex)
               {
                    System.out.println("Indent frame actionPerformed ->"+ex);
                    ex.printStackTrace();
               }
          }
          try
          {
               theMConnection . setAutoCommit(true);

               if(iMillCode==1)
                    theDConnection . setAutoCommit(true);
          }catch(Exception ex)
          {
               System.out.println("Indent frame actionPerformed ->"+ex);
               ex.printStackTrace();
          }
     }

     public int checkUserItemStock(Statement stat,String SItemCode,String SAuthUserCode)
     {
          int iCount=0;

          try
          {
               String QS = " Select Count(*) from ItemStock Where ItemCode='"+SItemCode+"' and MillCode="+iMillCode+" and HodCode="+SAuthUserCode;
               
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    iCount = res.getInt(1);
               }
               res.close();

          }
          catch(Exception e)
          {
               System.out.println("E7"+e);
               bComflag  = false;
          }
          return iCount;
     }

     private void getUserItemStock(Statement stat,String SItemCode,double dIssueQty,int iType)
     {
          try
          {
               VSeleCode  = new Vector();
               VSeleStock = new Vector();

               Vector VHCode     = new Vector();
               Vector VHStock    = new Vector();

               String QS1 = " Select HodCode,Stock from ItemStock Where ItemCode='"+SItemCode+"' and MillCode="+iMillCode+" Order By HodCode ";
               
               ResultSet res = stat.executeQuery(QS1);
               while(res.next())
               {
                    VHCode.addElement(res.getString(1));
                    VHStock.addElement(res.getString(2));
               }
               res.close();

               String QS = "";

               for(i=0;i<VGUserCode.size();i++)
               {
                    String SGUserCode = (String)VGUserCode.elementAt(i);

                    int iIndex = VHCode.indexOf(SGUserCode);

                    if(iIndex>=0)
                    {
                         if(iType==1)
                         {
                              VSeleCode.addElement(SGUserCode);
                              VSeleStock.addElement(""+dIssueQty);

                              break;
                         }
                         else
                         {
                              double dUserStock    = common.toDouble((String)VHStock.elementAt(iIndex));

                              if(dUserStock<=0)
                                   continue;


                              if(dIssueQty<=dUserStock)
                              {
                                   VSeleCode.addElement(SGUserCode);
                                   VSeleStock.addElement(""+dIssueQty);
     
                                   break;
                              }
                              else
                              {
                                   VSeleCode.addElement(SGUserCode);
                                   VSeleStock.addElement(""+dUserStock);

                                   dIssueQty = dIssueQty - dUserStock;
                              }
                         }
                    }
                    else
                    {
                         continue;
                    }

               }
               if(dIssueQty>0 || VSeleCode.size()<=0)
               {
                    JOptionPane.showMessageDialog(null,"Check User ","Problem in User Stock ",JOptionPane.ERROR_MESSAGE);
                    bComflag=false;
               }
          }
          catch(Exception e)
          {
               System.out.println("E7"+e);
               bComflag  = false;
          }
     }


     private void updateSubStoreMasterData()
     {
          int       iType     = JCType.getSelectedIndex();

          try
          {
               Statement stat =  theDConnection.createStatement();

               for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
               {     
                    if(!isEligible(i))
                         continue;

                    String SItemName    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,2));
                    String SItemCode    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,1));
                    double dIssueQty    = common.toDouble(common.parseNull(((String)MiddlePanel.tabreport.RowData[i][7]).trim()));

                    int iCount=0;
                    
                    ResultSet res = stat.executeQuery("Select count(*) from InvItems Where Item_Code='"+SItemCode+"'");
                    while(res.next())
                    {
                         iCount   =    res.getInt(1);
                    }
                    res.close();

                    if(iCount==0)
                         continue;

                    String QS = "";

                    if(iType == 1)
                         dIssueQty =   (-1)*dIssueQty;

                    QS = " Update InvItems Set ";
                    QS = QS+" MSIssQty=nvl(MSIssQty,0)+("+dIssueQty+"),";
                    QS = QS+" MSStock=nvl(MSStock,0)-("+dIssueQty+") ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";

                    if(theDConnection.getAutoCommit())
                         theDConnection . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame updateSubStoreMasterData() ->"+e);
               e.printStackTrace();
               bComflag  = false;
               System.out.println("\n\n\n\nError in updateSubStoreMasterData()\n\n\n");
          }
     }

     private void updateInvitemsData()
     {
          ResultSet result    = null;
          int       iType     = JCType.getSelectedIndex();
          try
          {
               Statement stat =  theMConnection.createStatement();

               for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
               {     
                    if(!isEligible(i))
                         continue;

                    String SItemName    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,2));
                    String SItemCode    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,1));
                    double dIssueQty    = common.toDouble(common.parseNull(((String)MiddlePanel.tabreport.RowData[i][7]).trim()));
                    double dRate        = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,8));
                    double dIssueVal    = common.toDouble(common.getRound(String.valueOf(dRate*dIssueQty),2));

                    String QString = "";

                    QString =   " Update "+SItemTable+" ";

                    if(iType == 1)
                    {
                         dIssueQty =   (-1)*dIssueQty;
                         dIssueVal =   (-1)*dIssueVal;
                    }

                    QString = QString+" Set IssQty=nvl(IssQty,0)+("+dIssueQty+"),IssVal=nvl(IssVal,0)+("+dIssueVal+") ";
                    QString = QString+" Where Item_Code = '"+SItemCode+"'";

                    if(theMConnection.getAutoCommit())
                         theMConnection . setAutoCommit(false);

                    stat.execute(QString);
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame updateInvitemsData() ->"+e);
               e.printStackTrace();
               bComflag  = false;
          }
     }

     public void UpdatedIssueNo()
     {
          String SUString = "";

          Statement stat      = null;
          ResultSet result    = null;

          try
          {
               stat           =  theMConnection.createStatement();

               if(theMConnection.getAutoCommit())
                    theMConnection . setAutoCommit(false);

               SUString = " Update config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1 where Id = 5";

               stat . executeUpdate(SUString);
               stat . close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame UpdatedIssueNo() ->"+e);
               bComflag  = false;
               e.printStackTrace();
          }
     }

     public void UpdateIndentNo()
     {
          String SUString = "";

          Statement stat      = null;
          ResultSet result    = null;

          try
          {
               stat           =  theMConnection.createStatement();

               if(theMConnection.getAutoCommit())
                    theMConnection . setAutoCommit(false);

               SUString = " Update ConfigAll set MaxNo = MaxNo+1 where Id = 2";

               stat . executeUpdate(SUString);
               stat . close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame UpdateIndenNo() ->"+e);
               bComflag  = false;
               e.printStackTrace();
          }
     }

}
