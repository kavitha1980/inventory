package Indent.IndentFiles;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class IssueReturnMiddlePanel extends JPanel
{
     ReturnTabReport      tabreport;
     Object               RowData[][];
     String               ColumnData[] = {"SNo","Code","Name","UOM","Department","Classification","Stock","Quantity","Rate","Value"};
     String               ColumnType[] = {"N","S","S","S","B","B","N","E","N","N"};
     
     JLayeredPane         Layer;
     Vector               VCode,VName,VUomName;
     Vector               VUnitCode;
     JComboBox            JCUnit,JCCata,JCDept;
     JTextField           TValue;
     ReturnMaterialSearch MS;
     Vector               VDeptCode,VDept,VNameCode;
     Vector               VCata,VCataCode;
     int                  iUserCode,iMillCode;
     String               SItemTable,SSupTable,SAuthUserCode;
     boolean              isReturn  = false;
     Common               common    = new Common();
     Vector               VMidIssueValue;
     
     IssueReturnMiddlePanel(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUomName,Vector VNameCode,JComboBox JCUnit,Vector VUnitCode,JComboBox JCCata,JTextField TValue,Vector VCata,Vector VCataCode,int iUserCode,int iMillCode,String SItemTable,String SSupTable,String SAuthUserCode)
     {
          this.Layer          = Layer;
          this.VCode          = VCode;
          this.VNameCode      = VNameCode;
          this.VName          = VName;
          this.VUomName       = VUomName;
          this.JCUnit         = JCUnit;
          this.VUnitCode      = VUnitCode;
          this.TValue         = TValue;
          this.JCCata         = JCCata;
          this.VCata          = VCata;
          this.VCataCode      = VCataCode;
          this.iUserCode      = iUserCode;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;
          this.SAuthUserCode  = SAuthUserCode;
          
          setDeptVectors();
          createComponents(10);
          addComponents();
          addListeners();
     }
     
     public void createComponents(int irows)
     {
          JCDept              = new JComboBox(VDept);
          RowData             = new Object[irows][ColumnData.length];
          VMidIssueValue      = new Vector();
          for(int i=0;i<irows;i++)
          {
               RowData[i][0]  = String.valueOf(i+1);
               RowData[i][1]  = " ";
               RowData[i][2]  = " ";
               RowData[i][3]  = " ";
               RowData[i][4]  = " ";
               RowData[i][5]  = " ";
               RowData[i][6]  = " ";
               RowData[i][7]  = " ";
               RowData[i][8]  = "0";
               RowData[i][9]  = "0";
               VMidIssueValue .addElement("");
          }
          tabreport = new ReturnTabReport(RowData,ColumnData,ColumnType,TValue,JCCata,JCDept,this);
     }
     
     public void addComponents()
     {
          setLayout(new GridLayout(1,1));
          add(tabreport);
          updateUI();
     }
     
     public void addListeners()
     {
                         tabreport . ReportTable.addKeyListener(new KeyList());
          JTableHeader   th        = tabreport.ReportTable.getTableHeader();
                         th        . addMouseListener(new ReturnTableHeaderHandle(this));
     }
     
     private class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    showMaterialSelectionFrame();
               }
          }
     }

     private void showMaterialSelectionFrame()
     {
          try
          {
               Layer     . remove(MS);
               Layer     . updateUI();
          }
          catch(Exception ex){}
          
          try
          {
               MS        = new ReturnMaterialSearch(Layer,VCode,VName,VUomName,VNameCode,this,tabreport.ReportTable.getSelectedRow(),iMillCode,SItemTable,SSupTable,SAuthUserCode);
               MS        . setSelected(true);
               Layer     . add(MS);
               Layer     . repaint();
               MS        . moveToFront();
               Layer     . updateUI();
          }
          catch(Exception ex){}
     }
     
     public int getDepartmentCode(int iRow)
     {
          String SDept = ((String)tabreport.RowData[iRow][4]).trim();
          if(SDept.length() == 0)
               return 0;  
          int iDept       = VDept.indexOf(SDept);
          return Integer.parseInt((String)VDeptCode.elementAt(iDept));
     }
     
     private void setDeptVectors()
     {
          ResultSet result    = null;

          VDept      = new Vector();
          VDeptCode  = new Vector();
          
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();   
               Statement      stat          =  theConnection.createStatement();
               
               String QS1 = "";
               QS1 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";

               result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VDept     .addElement(result.getString(1));
                    VDeptCode .addElement(result.getString(2));
               }
               result    .close();
               stat      .close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);   
          }
     }
}
