package Indent.IndentFiles;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;


public class ItemSearchList extends JInternalFrame
{

     JLayeredPane theLayer;
     protected int iRow,iCol;
     protected String STitle;

     JDialog   dialog;
     JPanel    thePanel,TopPanel;
     JList     BrowList;
     JLabel    LIndicator;
     JTextField TStartText,TMiddleText,TEndText;

     String str="";

     Common common = new Common();

     Connection theConnection=null;
     Vector VCode,VName,VStkGroupCode,VStock;

     String SItemTable,SCatl,SDraw;
     int iMillCode,iUserCode;

     JTextField     TMatCode;
     JTextField     TMatCatl;
     JTextField     TMatDraw;
     JTextField     TMatName;
     JButton        BMatName;
     NextField      TQty,TRate;
     JLabel         LValue;
     String         SWhere="MRS";
     public ItemSearchList(JLayeredPane theLayer,int iMillCode,int iUserCode,String SItemTable)
     {
          this.theLayer = theLayer;
          this.iMillCode= iMillCode;
          this.iUserCode= iUserCode;
          this.SItemTable=SItemTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          try
          {
               ORAConnection jdbc   = ORAConnection.getORAConnection();
               theConnection        = jdbc.getConnection();


          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
          
     private void createComponents()
     {
          TopPanel   = new JPanel();
          thePanel   = new JPanel(true);
          BrowList   = new JList();

          TStartText  = new JTextField(30);
          TMiddleText = new JTextField(30);
          TEndText    = new JTextField(30);

     }
               
     private void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);

          TopPanel.setLayout(new GridLayout(2,6));
          thePanel.setLayout(new  BorderLayout());
          BrowList.setFont(new Font("monospaced", Font.PLAIN, 11));
     }

     private void addComponents()
     {
          TopPanel.add(new JLabel(" Starting Text "));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(" Middle Text "));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(" End Text "));
          TopPanel.add(new JLabel(""));


          TopPanel.add(TStartText);
          TopPanel.add(new JLabel(""));
          TopPanel.add(TMiddleText);
          TopPanel.add(new JLabel(""));
          TopPanel.add(TEndText);
          TopPanel.add(new JLabel(""));


          thePanel.add("Center",new JScrollPane(BrowList));

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(thePanel,BorderLayout.CENTER);

     }

     private void addListeners()
     {
          TStartText.addKeyListener(new KeyList());
          TMiddleText.addKeyListener(new KeyList1());
          TEndText.addKeyListener(new KeyList2());

//          BrowList.addKeyListener(new KeyList3());
     }
     

     private class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setItemList(0);
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setItemList(0);
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         str="";
                    }

               }
               catch(Exception ex)
               {
               }
          }

     }

     private class KeyList1 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setItemList(1);
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setItemList(1);
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         str="";
                    }

               }
               catch(Exception ex)
               {
               }
          }


     }
     private class KeyList2 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setItemList(2);
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setItemList(2);
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         str="";
                    }
               }
               catch(Exception ex)
               {
               }
          }

     }

     private void setItemList(int iStatus)
     {
          VCode = new Vector();
          VName = new Vector();
          VStkGroupCode= new Vector();
          VStock       = new Vector();  

          try
          {
               String QS="";

               if(!str.equals(""))
               {
                    if(iStatus==0)
                    {
                        QS ="  Select Item_Code,rpad(Item_Name,50),0 as Stock,0 as StkGroupCode from InvItems"+
                            "  where Item_Name like  '"+str+"%'";
                    }
                    if(iStatus==1)
                    {
                        QS ="  Select Item_Code,rpad(Item_Name,50),0 as Stock,0 as StkGroupCode from InvItems"+
                            "  where Item_Name like  '%"+str+"%'";
                    }
                    if(iStatus==2)
                    {
                        QS ="  Select Item_Code,rpad(Item_Name,50),0 as Stock,0 as StkGroupCode from InvItems"+
                            "  where Item_Name like  '%"+str+"'";
                    }
                    PreparedStatement thePrepare = theConnection.prepareStatement(QS);
     
                    ResultSet theResult = thePrepare.executeQuery();
                    while(theResult.next())
                    {

                         String SItemCode = theResult.getString(1);
                         String SItemName = theResult.getString(2)+"   "+SItemCode;
                         VCode.addElement(SItemCode);
                         VName.addElement(SItemName);
                         VStock.addElement(theResult.getString(3));
                         VStkGroupCode.addElement(theResult.getString(4));
                    }
                    theResult.close();
                    thePrepare.close();
               }
               BrowList.setListData(VName);
               BrowList.setSelectedIndex(0);


          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

     }
      public void setCDM(String SMatCode)
      {
           try
           {
                   if(theConnection == null)
                   {
                         ORAConnection jdbc   = ORAConnection.getORAConnection();
                         theConnection        = jdbc.getConnection();
                   }
                   Statement stat   = theConnection.createStatement();
                   ResultSet res1 = stat.executeQuery("Select Catl,Draw From InvItems Where Item_Code='"+SMatCode+"'");
                   while(res1.next())
                   {
                         SCatl = common.parseNull(res1.getString(1));
                         SDraw = common.parseNull(res1.getString(2));
                   }
           }
           catch(Exception ex)
           {
                   System.out.println("@ setCDM");
                   System.out.println(ex);
           }
       }
}
