package Indent.IndentFiles;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class MaterialSearch extends JInternalFrame
{
     JLayeredPane        Layer;
     Vector              VName,VCode,VUomName,VNameCode;
     IndentMiddlePanel   MiddlePanel;
     int                 iRow;
     int                 iMillCode;
     String              SItemTable,SSupTable;
     Vector              VGUserCode;
     
     JTextField          TIndicator;          
     JList               BrowList;
     JScrollPane         BrowScroll;
     JPanel              BottomPanel;
     
     String              SName     = "",SCode     = "";
     String              str       = "";
     Common              common    = new Common();
     
     
     MaterialSearch(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUomName,Vector VNameCode,IndentMiddlePanel MiddlePanel,int iRow,int iMillCode,String SItemTable,String SSupTable,Vector VGUserCode)
     {
          this.Layer          = Layer;
          this.VName          = VName;
          this.VCode          = VCode;
          this.VUomName       = VUomName;
          this.VNameCode      = VNameCode;
          this.MiddlePanel    = MiddlePanel;
          this.iRow           = iRow;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;
          this.VGUserCode     = VGUserCode;

          BrowList            = new JList(VNameCode);
          BrowList            . setFont(new Font("monospaced", Font.PLAIN, 11));
          
          BrowScroll          = new JScrollPane(BrowList);
          TIndicator          = new JTextField();
          TIndicator          . setEditable(false);
          
          setBounds(80,100,550,350);
          setClosable(true);
          setResizable(true);
          setTitle("Select Material");
          BrowList       . addKeyListener(new KeyList());
          
          BottomPanel    = new JPanel(true);
          BottomPanel    . setLayout(new GridLayout(1,2));
          BottomPanel    . add(TIndicator);
          
          getContentPane(). setLayout(new BorderLayout());
          getContentPane(). add("Center",BrowScroll);
          getContentPane(). add("South",BottomPanel);
          setPresets();
          show();
     }
     
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar  = ke.getKeyChar();
                    lastchar  = Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str  = str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str  = str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }
     
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int       index          = BrowList.getSelectedIndex();
                    String    SMatNameCode   = (String)VNameCode.elementAt(index);
                    String    SMatName       = (String)VName.elementAt(index);
                    String    SMatCode       = (String)VCode.elementAt(index);
                    String    SMatUom        = (String)VUomName.elementAt(index);
                    boolean bFlag = checkRowData(SMatCode);
                    if(bFlag)
                    {
                         JOptionPane.showMessageDialog(null,"This Item already selected","Duplicate",JOptionPane.INFORMATION_MESSAGE);
                    }
                    else
                    {
                         setMiddlePanel(SMatName,SMatCode,SMatUom);
                         str            = "";
                         removeHelpFrame();
                    }
               }
          }
     }

     private boolean checkRowData(String SMatCode)
     {
          boolean bFlag = false;

          for(int i=0;i<MiddlePanel.RowData.length;i++)
          {
               String SCode = (String)MiddlePanel.RowData[i][1];

               if(SCode.equals(SMatCode))
               {
                    return true;
               }
               else
               {
                    continue;
               }
          }
          return bFlag;
     }

     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<VName.size();index++)
          {
               String str1 = ((String)VName.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }
     
     public void setCursor(String xtr)
     {
          int index=0;
          for(index=0;index<VName.size();index++)
          {
               String str1 = ((String)VName.elementAt(index)).toUpperCase();
               if(str1.startsWith(xtr))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }
     
     private void setPresets()
     {
          String SName = (String)MiddlePanel.RowData[iRow][2];
          if(SName.length() > 0 )
               setCursor(SName.toUpperCase());
     }
     
     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
               MiddlePanel.tabreport.ReportTable.requestFocus();
          }
          catch(Exception ex) { }
     }
     
     public void setMiddlePanel(String SMatName,String SMatCode,String SMatUom)
     {
          SMatCode  = SMatCode.trim();
          SMatName  = SMatName.trim();
          
          Item IC   = new Item(SMatCode,iMillCode,SItemTable,SSupTable,VGUserCode);

          double         dStock = common.toDouble(IC.getClStock());


          Item IC2  = new Item(SMatCode,iMillCode,SItemTable,SSupTable);

          double         dAllStock = common.toDouble(IC2.getClStock());
          double         dAllValue = common.toDouble(IC2.getClValue());
          double         dRate  = 0;

          try
          {
               dRate = dAllValue/dAllStock;
          }
          catch(Exception ex)
          {
               dRate=0;
          }

          if(dAllStock==0)
          {
               dRate = common.toDouble(IC2.SRate);
          }

          double dValue = dStock * dRate;


          if(dStock>dAllStock || dStock<dAllStock)
          {
               MiddlePanel . VMidIssueValue    . setElementAt(common.getRound(dValue,2),iRow);
          }
          else
          {
               MiddlePanel . VMidIssueValue    . setElementAt(common.getRound(dAllValue,2),iRow);
          }


          String    SStock    = common.getRound(dStock,3);
                    SStock    = SStock.trim();

          MiddlePanel.RowData[iRow][1] = SMatCode;
          MiddlePanel.RowData[iRow][2] = SMatName;
          MiddlePanel.RowData[iRow][3] = SMatUom;
          MiddlePanel.RowData[iRow][4] = "";
          MiddlePanel.RowData[iRow][5] = "";
          MiddlePanel.RowData[iRow][6] = SStock;
          MiddlePanel.RowData[iRow][7] = "";
          MiddlePanel.RowData[iRow][8] = common.getRound(dRate,4);
          MiddlePanel.RowData[iRow][9] = "0";
     }

}
