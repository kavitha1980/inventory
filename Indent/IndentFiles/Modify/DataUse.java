package Indent.IndentFiles.Modify;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;
import Indent.IndentFiles.*;

public class DataUse extends JInternalFrame
{
     Connection theConnection      = null;

     private   Vector    VName     = null,VUomName     = null,VCode        = null,
                         VUnit     = null,VUnitCode    = null,VClass       = null,
                         VClassCode= null,VUDept       = null,VUDeptCode   = null,
                         VType     = null,VNameCode    = null,VDeptCode    = null,
                         VDept     = null,VTypeCode    = null;

     private   int       iMillCode;
     String SItemTable;

     public DataUse(int iMillCode,String SItemTable)
     {
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;

          getOraConnection();
     }
     
     public void getOraConnection()
     {
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
                              theConnection =  oraConnection.getConnection();               
          }catch(Exception Ex)
          {
               System.out.println(  " DataUse "+ Ex);
               Ex.printStackTrace();
          }
     }

     public void getItemData()
     {
          Common    common    = new Common();
          Statement stat      = null;
          ResultSet result    = null;

          String QS =    "";

          VName     = new Vector();
          VUomName  = new Vector();
          VCode     = new Vector();
          VNameCode = new Vector();

          if(iMillCode==0)
          {
               QS = "Select Item_Name,Item_Code,UoMName From InvItems inner join UOM on UOM.UOMCode = InvItems.UOMCode Order By Item_Name";
          }
          else
          {
               QS = "Select Item_Name,"+SItemTable+".Item_Code,UoMName From "+SItemTable+" inner join invitems on invitems.item_code = "+SItemTable+".item_code inner join UOM on UOM.UOMCode = InvItems.UOMCode Order By Item_Name";
          }

          try
          {
               stat      = theConnection.createStatement();
               result    = stat.executeQuery(QS);

               while(result.next())
               {
                    
                    VName     .addElement(common.parseNull(result.getString(1)));
                    VCode     .addElement(common.parseNull(result.getString(2)));
                    VUomName  .addElement(common.parseNull(result.getString(3)));
                    VNameCode .addElement(((String)result.getString(1)).trim()+" (Code : "+((String)result.getString(2)).trim()+")");
               }
               result    .close();
               stat      .close();
          }
          catch(Exception ex)
          {
               System.out.println("Data Use insetDataIntoVector() ->"+ ex);
               ex.printStackTrace();
          }
     }

     public void getOtherData()
     {
          Statement stat      = null;
          ResultSet result    = null;
          Common    common    = new Common();

          VUDept         = new Vector();
          VUDeptCode     = new Vector();

          VUnit          = new Vector();
          VUnitCode      = new Vector();

          VClass         = new Vector();
          VClassCode     = new Vector();

          VDept          = new Vector();
          VDeptCode      = new Vector();

          VType          = new Vector();
          VTypeCode      = new Vector();

                    
          try
          {
               stat      =  theConnection.createStatement();
               result    = stat.executeQuery("Select HodName,HodCode From Hod Order By HodName");

               while(result.next())
               {
                    VUDept         .addElement(result.getString(1));
                    VUDeptCode     .addElement(result.getString(2));
               }
               result.close();

               String QS1 = "";
               String QS2 = "";

               QS1 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";
               QS2 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";

               result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VUnit     .addElement(result.getString(1));
                    VUnitCode .addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VClass    .addElement(result.getString(1));
                    VClassCode.addElement(result.getString(2));
               }
               result.close();


               String QString1 = "";

               QString1 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";

               result = stat.executeQuery(QString1);
               while(result.next())
               {
                    VDept     .addElement(result.getString(1));
                    VDeptCode .addElement(result.getString(2));
               }
               result    .close();

               String QString2 = "";

               QString2 = "Select name,code from indenttype where (millcode="+iMillCode +" or millcode = 2) and  issueflag = 0 order by code ";

               result = stat.executeQuery(QString2);
               while(result.next())
               {
                    VType     .addElement(result.getString(1));
                    VTypeCode .addElement(result.getString(2));
               }
               result    .close();
               stat      .close();
          }
          catch(Exception ex)
          {
               System.out.println("Indent XyX :"+ex);
          }
     }

          /*        Return a Vector To Fill the Lists Like (Combo Box and List*/

     public Vector getItemNameCode()
     {
          return VNameCode;
     }

     public Vector getItemName()
     {
          return VName;
     }
     public Vector getHodName()
     {
          return VUDept;
     }

     public Vector getDeptName()
     {
          return VDept;
     }

     public Vector getClassName()
     {
          return VClass;
     }

     public Vector getTypeName()
     {
          return VType;
     }

     public Vector getUnitName()
     {
          return VUnit;
     }

          /*        Return a String  which has the given Index No */


     public String setPatItemNameCode(int i)
     {
          return ((String)VNameCode.elementAt(i));
     }

     public String setPatItemName(int i)
     {
          return ((String)VName      .elementAt(i));
     }

     public String setPatItemCode(int i)
     {
          return ((String)VCode      .elementAt(i));
     }

     public String setPatUomName(int i)
     {
          return ((String)VUomName   .elementAt(i));
     }

     public String setPatUnitName(int i)
     {
          return ((String)VUnit      .elementAt(i));
     }

     public String setPatUnitCode(int i)
     {
          return ((String)VUnitCode  .elementAt(i));
     }

     public String setPatClaseName(int i)
     {
          return ((String)VClass     .elementAt(i));
     }

     public String setPatClassCode(int i)
     {
          return ((String)VClassCode .elementAt(i));
     }

     public String setPatHodName(int i)
     {
          return ((String)VUDept     .elementAt(i));
     }

     public String setPatHodCode(int i)
     {
          return ((String)VUDeptCode .elementAt(i));
     }

     public String setPatDepName(int i)
     {
          return ((String)VDept  .elementAt(i));
     }

     public String setPatDepCode(int i)
     {
          return ((String)VDeptCode      .elementAt(i));
     }

          /*        Return the size of the vector which is Used in Loops*/


     public int getItemListSize()
     {
          return VName.size();
     }

     public int getDeptListSize()
     {
          return VDept.size();
     }

     public int getClassListSize()
     {
          return VClass.size();
     }

          /*  Return an Integer which is the code of Particular Field, It is used to insert  in Database  */

     public int getDeptCode(String SDept)
     {
          SDept     = SDept.trim();

          if(SDept.length() == 0)
               return 0;  
          int iDept       = VDept.indexOf(SDept);
          return Integer.parseInt((String)VDeptCode.elementAt(iDept));
     }

     public int getHodCode(String SHod)
     {
          SHod     = SHod.trim();

          if(SHod.length() == 0)
               return 0;  
          int iHod       = VUDept.indexOf(SHod);
          return Integer.parseInt((String)VUDeptCode.elementAt(iHod));
     }

     public int getClassCode(String SClass)
     {
          SClass     = SClass.trim();

          if(SClass.length() == 0)
               return 0;  
          int iClass       = VClass.indexOf(SClass);
          return Integer.parseInt((String)VClassCode.elementAt(iClass));

     }

     public int getUnitCode(String SUnit)
     {
          SUnit     = SUnit.trim();

          if(SUnit.length() == 0)
               return 0;
          int iUnit       = VUnit.indexOf(SUnit);
          return Integer.parseInt((String)VUnitCode.elementAt(iUnit));
     }

}

