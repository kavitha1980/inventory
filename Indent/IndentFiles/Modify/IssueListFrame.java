package Indent.IndentFiles.Modify;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;
import Indent.IndentFiles.*;

public class IssueListFrame extends JInternalFrame
{
     String         SStDate,SEnDate;
     JComboBox      JCOrder,JCFilter;
     JButton        BApply;
     JPanel         TopPanel,BottomPanel;
     JPanel         DatePanel,FilterPanel,SortPanel,BasisPanel,ApplyPanel;
     
     TabReport      tabreport,tabreport1,tabreport2;
     DateField      TStDate;
     DateField      TEnDate;
     
     JRadioButton   JRPeriod,JRNo;
     NextField      TStNo,TEnNo;
     JTabbedPane    theTab;
     
     Object         RowData[][],RowData1[][],RowData2[][];
     
     String         ColumnData[] = {"Issue Id","Date","Issue No","Code","Name","Issue Qty","Issue Rate","Unit","Department","Classification","Status","Received   By"};
     String         ColumnType[] = {"N","S","N","S","S","N","N","S","S","S","S","S"};
     Common         common = new Common();
     Vector         VIssueNo,VIssueDate,VRefNo,VIssueCode,VIssueName,VIssueQty,VIssueRate,VIssueUnit,VIssueDept,VIssueCata,VStatus,VId,VIUserCode,VIUserName,VIssueType,VReceiver;
     
     JLayeredPane   Layer;
     StatusPanel    SPanel;
     Vector         VCode,VName,VUom;
     int            iUserCode,iMillCode,iAuthCode;
     String         SItemTable,SSupTable;
     
     public IssueListFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUom,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode,String SItemTable,String SSupTable)
     {
          super("Issue List During a Period");
          this.Layer      = Layer;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VUom       = VUom;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.iAuthCode  = iAuthCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
		  
		  
		  try {
			  
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
			  
		  } catch(Exception ex) {
			  ex.printStackTrace();
		  }

     }
     
     public void createComponents()
     {
          TStDate        = new DateField();
          TEnDate        = new DateField();
          TStNo          = new NextField();
          TEnNo          = new NextField();
          BApply         = new JButton("Apply");
          
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          
          JCOrder        = new JComboBox();
          JCFilter       = new JComboBox();
          
          JRPeriod       = new JRadioButton("Periodical",true);
          JRNo           = new JRadioButton("Issue No");
          
          DatePanel      = new JPanel();
          FilterPanel    = new JPanel();
          SortPanel      = new JPanel();
          BasisPanel     = new JPanel();
          ApplyPanel     = new JPanel();
          
          TStDate        . setTodayDate();
          TEnDate        . setTodayDate();
     }
     
     public void setLayouts()
     {
          TopPanel       . setLayout(new GridLayout(1,5));
          
          DatePanel      . setLayout(new GridLayout(3,1));
          SortPanel      . setLayout(new BorderLayout());
          FilterPanel    . setLayout(new BorderLayout());
          BasisPanel     . setLayout(new GridLayout(2,1));
          ApplyPanel     . setLayout(new BorderLayout());
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          JCOrder        . addItem("Issue Id");
          JCOrder        . addItem("Issue No");
          JCOrder        . addItem("Materialwise");
          JCOrder        . addItem("Departmentwise");
          JCOrder        . addItem("Groupwise");
          JCOrder        . addItem("Unitwise");
          
          JCFilter       . addItem("All");
          JCFilter       . addItem("Authenticated");
          JCFilter       . addItem("Not Authenticated");
          
          SortPanel      . add("Center",JCOrder);
          FilterPanel    . add("Center",JCFilter);
          
          DatePanel      . add(TStDate);
          DatePanel      . add(TEnDate);
          
          BasisPanel     . add(JRPeriod);
          BasisPanel     . add(JRNo);
          
          ApplyPanel     . add("Center",BApply);
                           
          TopPanel       . add(SortPanel);
          TopPanel       . add(FilterPanel);
          TopPanel       . add(BasisPanel);
          TopPanel       . add(DatePanel);
          TopPanel       . add(ApplyPanel);
          
          SortPanel      . setBorder(new TitledBorder("Sorting"));
          FilterPanel    . setBorder(new TitledBorder("List Only"));
          DatePanel      . setBorder(new TitledBorder("Period"));
          BasisPanel     . setBorder(new TitledBorder("Basis"));
          ApplyPanel     . setBorder(new TitledBorder("Control"));
          
          getContentPane(). add(TopPanel,BorderLayout.NORTH);
          getContentPane(). add(BottomPanel,BorderLayout.SOUTH);
     }
     
     public void addListeners()
     {
          BApply   .addActionListener(new ApplyList());
          JRPeriod .addActionListener(new JRList());
          JRNo     .addActionListener(new JRList());
     }
     
     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    DatePanel .setBorder(new TitledBorder("Periodical"));
                    DatePanel .removeAll();
                    DatePanel .add(TStDate);
                    DatePanel .add(TEnDate);
                    DatePanel .updateUI();
                    JRNo      .setSelected(false);
               }
               else
               {
                    DatePanel .setBorder(new TitledBorder("Numbered"));
                    DatePanel .removeAll();
                    DatePanel .add(TStNo);
                    DatePanel .add(TEnNo);
                    DatePanel .updateUI();
                    JRPeriod  .setSelected(false);
               }
          }
     }
     
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {

               setDataIntoVector();
               setRowData();
			   
				   
                    getContentPane().remove(theTab);
               }
               catch(Exception ex){ex.printStackTrace();}
               try
               {
                    theTab      = new JTabbedPane();

                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    tabreport.setBorder(new TitledBorder("Regular List"));

                    tabreport1 = new TabReport(RowData1,ColumnData,ColumnType);
                    tabreport1.setBorder(new TitledBorder("NonStock List"));
					
                    tabreport2 = new TabReport(RowData2,ColumnData,ColumnType);
                    tabreport2.setBorder(new TitledBorder("Service Stock  List"));
					
					
                    theTab.add(tabreport," Regular Issue");
                    theTab.add(tabreport1," Non Stock Issue");
					theTab.add(tabreport2," Service Stock Issue");
     
                    getContentPane().add(theTab,BorderLayout.CENTER);
                    
                    if(iAuthCode>1)
                    {
                         //tabreport.ReportTable.addKeyListener(new KeyList());
                    }
                    
                    setSelected(true);
                    Layer.repaint();
                    Layer.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
					ex.printStackTrace();
               }
          }
     }
     
     public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if (ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                   /* int i = tabreport.ReportTable.getSelectedRow();
                    String SAuth = (String)RowData[i][10];
                    if(SAuth.equals("Authenticated"))
                    {
                         JOptionPane.showMessageDialog(null,"This Issue No is Authenticated","Error Message",JOptionPane.INFORMATION_MESSAGE);
                         tabreport.ReportTable.requestFocus();
                         return;
                    }
                    double dIssQty      = common.toDouble((String)RowData[i][5]);

                    if(dIssQty<0)
                    {
                         JOptionPane.showMessageDialog(null,"Issue Return Cannot be Modified","Error Message",JOptionPane.INFORMATION_MESSAGE);
                         tabreport.ReportTable.requestFocus();
                         return;
                    }

                    String SIssueNo     = (String)RowData[i][0];
                    String SIssueDate   = common.pureDate((String)RowData[i][1]);
                    String SIndentNo    = (String)RowData[i][2];
                    String SIUserCode   = (String)VIUserCode.elementAt(i);
                    String SIUserName   = (String)VIUserName.elementAt(i);
					
                    IssueModiFrame      issuemodiframe = new IssueModiFrame(Layer,SPanel,iUserCode,iMillCode,SIssueNo,SIssueDate,SIndentNo,SItemTable,SSupTable,SIUserCode,SIUserName);
                    Layer               .add(issuemodiframe);
                    issuemodiframe      .show();
                    issuemodiframe      .moveToFront();
                    Layer               .repaint();
                    Layer               .updateUI();*/
               }
          } 
     }
     
     public void setDataIntoVector()
     {
          VIssueNo     = new Vector();
          VIssueDate   = new Vector();
          VRefNo       = new Vector();
          VIssueCode   = new Vector();
          VIssueName   = new Vector();
          VIssueQty    = new Vector();
          VIssueRate   = new Vector();
          VIssueUnit   = new Vector();
          VIssueDept   = new Vector();
          VIssueCata   = new Vector();
          VStatus      = new Vector();
          VId          = new Vector();
          VIUserCode   = new Vector();
          VIUserName   = new Vector();
          VIssueType   = new Vector();
          VReceiver    = new Vector();
          String StDate = TStDate.toNormal();
          String EnDate = TEnDate.toNormal();
          
          String QString  = getQString(StDate,EnDate);
          String QString1 = getNonStockQS(StDate,EnDate);
		  
		  String QString2 = getServiceStockQS(StDate,EnDate);
		  
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();

               ResultSet result  = stat.executeQuery(QString);

               while (result.next())
               {
                    String SStatus = "";
                    
                    String str1  = result.getString(1);  
                    String str2  = result.getString(2);
                    String str3  = result.getString(3);
                    String str4  = result.getString(4);
                    String str5  = result.getString(5);
                    String str6  = result.getString(6);
                    String str7  = result.getString(7);
                    String str8  = result.getString(8);
                    String str9  = result.getString(9);
                    String str10 = result.getString(10);
                    String str11 = result.getString(11);
                    String str12 = result.getString(12);
                    String str13 = result.getString(13);
                    String str14 = result.getString(14);
                    String str15 = result.getString(15);
                    String str16 = result.getString(16);
                    
                    if(str11.equals("1"))
                    {
                         SStatus = "Authenticated";    
                    }
                    else
                    {
                         SStatus = "Not Authenticated";    
                    }
                    
                    VIssueNo     .addElement(str1);
                    VIssueDate   .addElement(common.parseDate(str2));
                    VRefNo       .addElement(str3);
                    VIssueCode   .addElement(str4);
                    VIssueName   .addElement(str5);
                    VIssueQty    .addElement(str6);
                    VIssueRate   .addElement(common.parseNull(str7));
                    VIssueDept   .addElement(common.parseNull(str8));
                    VIssueCata   .addElement(common.parseNull(str9));
                    VIssueUnit   .addElement(common.parseNull(str10));
                    VStatus      .addElement(SStatus);
                    VId          .addElement(str12);
                    VIUserCode   .addElement(str13);
                    VIUserName   .addElement(str14);
                    VIssueType   .addElement(str15);
                    VReceiver    .addElement(str16);
               }
               result.close();

               result  = stat.executeQuery(QString1);

               while (result.next())
               {
                    String SStatus = "";
                    
                    String str1  = result.getString(1);  
                    String str2  = result.getString(2);
                    String str3  = result.getString(3);
                    String str4  = result.getString(4);
                    String str5  = result.getString(5);
                    String str6  = result.getString(6);
                    String str7  = result.getString(7);
                    String str8  = result.getString(8);
                    String str9  = result.getString(9);
                    String str10 = result.getString(10);
                    String str11 = result.getString(11);
                    String str12 = result.getString(12);
                    String str13 = result.getString(13);
                    String str14 = result.getString(14);
                    String str15 = result.getString(15);
                    String str16 = result.getString(16);
                    
                    if(str11.equals("1"))
                    {
                         SStatus = "Authenticated";    
                    }
                    else
                    {
                         SStatus = "Not Authenticated";    
                    }
                    
                    VIssueNo     .addElement(str1);
                    VIssueDate   .addElement(common.parseDate(str2));
                    VRefNo       .addElement(str3);
                    VIssueCode   .addElement(str4);
                    VIssueName   .addElement(str5);
                    VIssueQty    .addElement(str6);
                    VIssueRate   .addElement(common.parseNull(str7));
                    VIssueDept   .addElement(common.parseNull(str8));
                    VIssueCata   .addElement(common.parseNull(str9));
                    VIssueUnit   .addElement(common.parseNull(str10));
                    VStatus      .addElement(SStatus);
                    VId          .addElement(str12);
                    VIUserCode   .addElement(str13);
                    VIUserName   .addElement(str14);
                    VIssueType   .addElement(str15);
                    VReceiver    .addElement(str16);

               }
               result.close();
			   
               result  = stat.executeQuery(QString2);

               while (result.next())
               {
                    String SStatus = "";
                    
                    String str1  = common.parseNull(result.getString(1));  
                    String str2  = common.parseNull(result.getString(2));
                    String str3  = common.parseNull(result.getString(3));
                    String str4  = common.parseNull(result.getString(4));
                    String str5  = common.parseNull(result.getString(5));
                    String str6  = common.parseNull(result.getString(6));
                    String str7  = common.parseNull(result.getString(7));
                    String str8  = common.parseNull(result.getString(8));
                    String str9  = common.parseNull(result.getString(9));
                    String str10 = common.parseNull(result.getString(10));
                    String str11 = common.parseNull(result.getString(11));
                    String str12 = common.parseNull(result.getString(12));
                    String str13 = common.parseNull(result.getString(13));
                    String str14 = common.parseNull(result.getString(14));
                    String str15 = common.parseNull(result.getString(15));
                    String str16 = common.parseNull(result.getString(16));
                    
                    if(str11.equals("1"))
                    {
                         SStatus = "Authenticated";    
                    }
                    else
                    {
                         SStatus = "Not Authenticated";    
                    }
                    
                    VIssueNo     .addElement(str1);
                    VIssueDate   .addElement(common.parseDate(str2));
                    VRefNo       .addElement(str3);
                    VIssueCode   .addElement(str4);
                    VIssueName   .addElement(str5);
                    VIssueQty    .addElement(str6);
                    VIssueRate   .addElement(common.parseNull(str7));
                    VIssueDept   .addElement(common.parseNull(str8));
                    VIssueCata   .addElement(common.parseNull(str9));
                    VIssueUnit   .addElement(common.parseNull(str10));
                    VStatus      .addElement(SStatus);
                    VId          .addElement(str12);
                    VIUserCode   .addElement(str13);
                    VIUserName   .addElement(str14);
                    VIssueType   .addElement(str15);
                    VReceiver    .addElement(str16);

               }
               result.close();

			   

               stat.close();
          }
          catch(Exception ex){
			  System.out.println(ex);
			  ex.printStackTrace();
			  }
     }
     
     public void setRowData()
     {
		 
		try {
			
		 
          int iSize    = getStockSize();
          int iNonSize = getNonStockSize();
		  
          int iServSize = getServStockSize();
		  

          RowData     = new Object[iSize][ColumnData.length];
          RowData1    = new Object[iNonSize][ColumnData.length];
		  
          RowData2    = new Object[iServSize][ColumnData.length];
		  
		  System.out.println("iServSize"+iServSize);
		  

          int j=0,k=0,l=0;

          for(int i=0;i<VIssueNo.size();i++)
          {
               String SIssueType = (String)VIssueType.elementAt(i);

               if(SIssueType.equals("0"))
               {
                    double dIssueQty    = common.toDouble(common.parseNull(((String)VIssueQty.elementAt(i)).trim()).trim());
     
                    RowData[j][0]  = (String)VIssueNo.elementAt(i);
                    RowData[j][1]  = (String)VIssueDate.elementAt(i);
                    RowData[j][2]  = (String)VRefNo.elementAt(i);
                    RowData[j][3]  = (String)VIssueCode.elementAt(i);
                    RowData[j][4]  = (String)VIssueName.elementAt(i);
                    RowData[j][5]  = String.valueOf(dIssueQty);
                    RowData[j][6]  = (String)VIssueRate.elementAt(i);
                    RowData[j][7]  = (String)VIssueDept.elementAt(i);
                    RowData[j][8]  = (String)VIssueCata.elementAt(i);
                    RowData[j][9]  = (String)VIssueUnit.elementAt(i);
                    RowData[j][10] = (String)VStatus.elementAt(i);
                    RowData[j][11] = (String)VReceiver.elementAt(i);

                    j++;
               }
			   else if(SIssueType.equals("2"))
               {
				   
                    double dIssueQty    = common.toDouble(common.parseNull(((String)VIssueQty.elementAt(i)).trim()).trim());
     
                    RowData2[l][0]  = common.parseNull((String)VIssueNo.elementAt(i));
                    RowData2[l][1]  = common.parseNull((String)VIssueDate.elementAt(i));
                    RowData2[l][2]  = common.parseNull((String)VRefNo.elementAt(i));
                    RowData2[l][3]  = common.parseNull((String)VIssueCode.elementAt(i));
                    RowData2[l][4]  = common.parseNull((String)VIssueName.elementAt(i));
                    RowData2[l][5]  = String.valueOf(dIssueQty);
                    RowData2[l][6]  = common.parseNull((String)VIssueRate.elementAt(i));
                    RowData2[l][7]  = common.parseNull((String)VIssueDept.elementAt(i));
                    RowData2[l][8]  = common.parseNull((String)VIssueCata.elementAt(i));
                    RowData2[l][9]  = common.parseNull((String)VIssueUnit.elementAt(i));
                    RowData2[l][10] = common.parseNull((String)VStatus.elementAt(i));
                    RowData2[l][11] = common.parseNull((String)VReceiver.elementAt(i));


                    l++;
				   
				   
				   
			   }	   
               else
               {
                    double dIssueQty    = common.toDouble(common.parseNull(((String)VIssueQty.elementAt(i)).trim()).trim());
     
                    RowData1[k][0]  = (String)VIssueNo.elementAt(i);
                    RowData1[k][1]  = (String)VIssueDate.elementAt(i);
                    RowData1[k][2]  = (String)VRefNo.elementAt(i);
                    RowData1[k][3]  = (String)VIssueCode.elementAt(i);
                    RowData1[k][4]  = (String)VIssueName.elementAt(i);
                    RowData1[k][5]  = String.valueOf(dIssueQty);
                    RowData1[k][6]  = (String)VIssueRate.elementAt(i);
                    RowData1[k][7]  = (String)VIssueDept.elementAt(i);
                    RowData1[k][8]  = (String)VIssueCata.elementAt(i);
                    RowData1[k][9]  = (String)VIssueUnit.elementAt(i);
                    RowData1[k][10] = (String)VStatus.elementAt(i);
                    RowData1[k][11] = (String)VReceiver.elementAt(i);


                    k++;
               }
          }
		  
		} catch(Exception ex)  {
			ex.printStackTrace();
		}
     }

     private int getStockSize()
     {
          int iSize=0;
          for(int i=0;i<VIssueNo.size();i++)
          {
               if(((String)VIssueType.elementAt(i)).equals("0"))
               {
                   iSize++;
               }
          }
          return iSize;
     }
     private int getNonStockSize()
     {
          int iSize=0;
          for(int i=0;i<VIssueNo.size();i++)
          {
               if(((String)VIssueType.elementAt(i)).equals("1"))
               {
                   iSize++;
               }
          }
          return iSize;
     }
	 
	 private int getServStockSize() {
		 
          int iSize=0;
          for(int i=0;i<VIssueNo.size();i++)
          {
               if(((String)VIssueType.elementAt(i)).equals("2"))
               {
                   iSize++;
               }
          }
          return iSize;
		 
		 
	 }

     public String getQString(String StDate,String EnDate)
     {
          String QString  = "";
          
          if(JRPeriod.isSelected())
          {
               QString  =     " SELECT Issue.IssueNo, Issue.IssueDate, Issue.UIRefNo,"+
                              " Issue.Code, InvItems.Item_Name, Issue.Qty, Issue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " Issue.Authentication, Issue.Id,Issue.IndentUserCode,RawUser.UserName,0 as IssueType,getName(Issue.ReceiverEmpcode,"+iMillCode+") "+
                              " FROM (((Issue "+
                              " INNER JOIN InvItems ON Issue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON Issue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON Issue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON Issue.Group_Code = Cata.Group_Code "+
                              " INNER JOIN RawUser ON Issue.IndentUserCode = RawUser.UserCode "+
                              " Where Issue.IssueDate >= '"+StDate+"' and Issue.IssueDate <='"+EnDate+"' "+
                              " And Issue.MillCode="+iMillCode;
          }
          else
          {
               QString  =     " SELECT Issue.IssueNo, Issue.IssueDate, Issue.UIRefNo,"+
                              " Issue.Code, InvItems.Item_Name, Issue.Qty, Issue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " Issue.Authentication, Issue.Id,Issue.IndentUserCode,RawUser.UserName,0 as IssueType,getName(Issue.ReceiverEmpcode,"+iMillCode+") "+
                              " FROM (((Issue "+
                              " INNER JOIN InvItems ON Issue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON Issue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON Issue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON Issue.Group_Code = Cata.Group_Code "+
                              " INNER JOIN RawUser ON Issue.IndentUserCode = RawUser.UserCode "+
                              " Where Issue.UIRefNo >="+TStNo.getText()+" and Issue.UIRefNo <= "+TEnNo.getText() +
                              " And Issue.MillCode="+iMillCode;
          }
          
          if(JCFilter.getSelectedIndex() == 1)
               QString = QString+" And Issue.Authentication = 1 ";
          if(JCFilter.getSelectedIndex() == 2)
               QString = QString+" And Issue.Authentication = 0 ";
          
          if(JCOrder.getSelectedIndex() == 0)
               QString = QString+" Order By Issue.IssueNo,Issue.IssueDate";
          if(JCOrder.getSelectedIndex() == 1)
               QString = QString+" Order By Issue.UIRefNo,Issue.IssueDate";
          if(JCOrder.getSelectedIndex() == 2)
               QString = QString+" Order By InvItems.Item_Name,Issue.IssueDate";
          if(JCOrder.getSelectedIndex() == 3)
               QString = QString+" Order By Dept.Dept_Name,Issue.IssueDate";
          if(JCOrder.getSelectedIndex() == 4)
               QString = QString+" Order By Cata.Group_Name,Issue.IssueDate";
          if(JCOrder.getSelectedIndex() == 5)
               QString = QString+" Order By Unit.Unit_Name,Issue.IssueDate";

          return QString;
     }

     public String getNonStockQS(String StDate,String EnDate)
     {
          String QString  = "";
          
          if(JRPeriod.isSelected())
          {
               QString  =     " SELECT NonStockIssue.IssueNo, NonStockIssue.IssueDate, NonStockIssue.UIRefNo,"+
                              " NonStockIssue.Code, InvItems.Item_Name, NonStockIssue.Qty, NonStockIssue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " NonStockIssue.Authentication, NonStockIssue.Id,'' as IndentUserCode,'' as UserName,1 as IssueType,getName(NonStockIssue.ReceiverEmpcode,"+iMillCode+") "+
                              " FROM (((NonStockIssue "+
                              " INNER JOIN InvItems ON NonStockIssue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON NonStockIssue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON NonStockIssue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON NonStockIssue.Group_Code = Cata.Group_Code "+
                              " Where NonStockIssue.IssueDate >= '"+StDate+"' and NonStockIssue.IssueDate <='"+EnDate+"' "+
                              " And NonStockIssue.MillCode="+iMillCode;
          }
          else
          {
               QString  =     " SELECT NonStockIssue.IssueNo, NonStockIssue.IssueDate, NonStockIssue.UIRefNo,"+
                              " NonStockIssue.Code, InvItems.Item_Name, NonStockIssue.Qty, NonStockIssue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " NonStockIssue.Authentication, NonStockIssue.Id,'' as IndentUserCode,'' as UserName,1 as IssueType,getName(NonStockIssue.ReceiverEmpcode,"+iMillCode+")  "+
                              " FROM (((NonStockIssue "+
                              " INNER JOIN InvItems ON NonStockIssue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON NonStockIssue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON NonStockIssue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON NonStockIssue.Group_Code = Cata.Group_Code "+
                              " Where NonStockIssue.UIRefNo >="+TStNo.getText()+" and NonStockIssue.UIRefNo <= "+TEnNo.getText() +
                              " And NonStockIssue.MillCode="+iMillCode;
          }
          
          if(JCFilter.getSelectedIndex() == 1)
               QString = QString+" And NonStockIssue.Authentication = 1 ";
          if(JCFilter.getSelectedIndex() == 2)
               QString = QString+" And NonStockIssue.Authentication = 0 ";
          
          if(JCOrder.getSelectedIndex() == 0)
               QString = QString+" Order By NonStockIssue.IssueNo,NonStockIssue.IssueDate";
          if(JCOrder.getSelectedIndex() == 1)
               QString = QString+" Order By NonStockIssue.UIRefNo,NonStockIssue.IssueDate";
          if(JCOrder.getSelectedIndex() == 2)
               QString = QString+" Order By InvItems.Item_Name,NonStockIssue.IssueDate";
          if(JCOrder.getSelectedIndex() == 3)
               QString = QString+" Order By Dept.Dept_Name,NonStockIssue.IssueDate";
          if(JCOrder.getSelectedIndex() == 4)
               QString = QString+" Order By Cata.Group_Name,NonStockIssue.IssueDate";
          if(JCOrder.getSelectedIndex() == 5)
               QString = QString+" Order By Unit.Unit_Name,NonStockIssue.IssueDate";

          return QString;
     }
	 
     public String getServiceStockQS(String StDate,String EnDate)
     {
          String QString  = "";
          
          if(JRPeriod.isSelected())
          {
               QString  =     " SELECT ServiceStockISsue.IssueNo, ServiceStockISsue.IssueDate, ServiceStockISsue.UIRefNo,"+
                              " ServiceStockISsue.Code, InvItems.Item_Name, ServiceStockISsue.Qty, ServiceStockISsue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " ServiceStockISsue.Authentication, ServiceStockISsue.Id,'' as IndentUserCode,'' as UserName,2 as IssueType,'' "+
                              " FROM (((ServiceStockISsue "+
                              " INNER JOIN InvItems ON ServiceStockISsue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON ServiceStockISsue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON ServiceStockISsue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON ServiceStockISsue.Group_Code = Cata.Group_Code "+
                              " Where ServiceStockISsue.IssueDate >= '"+StDate+"' and ServiceStockISsue.IssueDate <='"+EnDate+"' "+
                              " And ServiceStockISsue.MillCode="+iMillCode;
          }
          else
          {
			  
               QString  =     " SELECT ServiceStockISsue.IssueNo, ServiceStockISsue.IssueDate, ServiceStockISsue.UIRefNo,"+
                              " ServiceStockISsue.Code, InvItems.Item_Name, ServiceStockISsue.Qty, ServiceStockISsue.IssRate,"+
                              " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, "+
                              " ServiceStockISsue.Authentication, ServiceStockISsue.Id,'' as IndentUserCode,'' as UserName,2 as IssueType,'' "+
                              " FROM (((ServiceStockISsue "+
                              " INNER JOIN InvItems ON ServiceStockISsue.Code = InvItems.Item_Code) "+
                              " INNER JOIN Unit ON ServiceStockISsue.Unit_Code = Unit.Unit_Code) "+
                              " INNER JOIN Dept ON ServiceStockISsue.Dept_Code = Dept.Dept_code) "+
                              " INNER JOIN Cata ON ServiceStockISsue.Group_Code = Cata.Group_Code "+
							  " Where NonStockIssue.UIRefNo >="+TStNo.getText()+" and NonStockIssue.UIRefNo <= "+TEnNo.getText() +
                              " And ServiceStockISsue.MillCode="+iMillCode;
			  
          }
          
          if(JCFilter.getSelectedIndex() == 1)
               QString = QString+" And ServiceStockISsue.Authentication = 1 ";
          if(JCFilter.getSelectedIndex() == 2)
               QString = QString+" And ServiceStockISsue.Authentication = 0 ";
          
          if(JCOrder.getSelectedIndex() == 0)
               QString = QString+" Order By ServiceStockISsue.IssueNo,ServiceStockISsue.IssueDate";
          if(JCOrder.getSelectedIndex() == 1)
               QString = QString+" Order By ServiceStockISsue.UIRefNo,ServiceStockISsue.IssueDate";
          if(JCOrder.getSelectedIndex() == 2)
               QString = QString+" Order By InvItems.Item_Name,ServiceStockISsue.IssueDate";
          if(JCOrder.getSelectedIndex() == 3)
               QString = QString+" Order By Dept.Dept_Name,ServiceStockISsue.IssueDate";
          if(JCOrder.getSelectedIndex() == 4)
               QString = QString+" Order By Cata.Group_Name,ServiceStockISsue.IssueDate";
          if(JCOrder.getSelectedIndex() == 5)
               QString = QString+" Order By Unit.Unit_Name,ServiceStockISsue.IssueDate";
		   
		   
		   System.out.println(QString);

          return QString;
     }
	 
	 
	 
	 

}
