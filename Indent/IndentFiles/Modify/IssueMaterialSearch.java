package Indent.IndentFiles.Modify;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class IssueMaterialSearch extends JInternalFrame
{
     JLayeredPane     Layer;
     IssueMiddlePanel MiddlePanel;
     int iRow;
     int iMillCode;
     String SItemTable,SSupTable;
     Vector VGUserCode;
     
     JTextField     TIndicator;          
     JList          BrowList;
     JScrollPane    BrowScroll;
     JPanel         BottomPanel;

     String         SName     = "",SCode     = "";
     String         str       = "";
     Common         common    = new Common();
     
     IssueMaterialSearch(JLayeredPane Layer,IssueMiddlePanel MiddlePanel,int iRow,int iMillCode,String SItemTable,String SSupTable,Vector VGUserCode)
     {
          this.Layer       = Layer;
          this.MiddlePanel = MiddlePanel;
          this.iRow        = iRow;
          this.iMillCode   = iMillCode;
          this.SItemTable  = SItemTable;
          this.SSupTable   = SSupTable;
          this.VGUserCode  = VGUserCode;

          createComponents();
          setLayouts();
          addComponents();
     }

     public void createComponents()
     {
          BrowList       = new JList(MiddlePanel.dataUse.getItemNameCode());
          BrowList       . setFont(new Font("monospaced", Font.PLAIN, 11));
          
          BrowScroll     = new JScrollPane(BrowList);
          TIndicator     = new JTextField();
          TIndicator     . setEditable(false);
     }

     public void setLayouts()
     {
          setBounds(80,100,550,350);
          setClosable(true);
          setResizable(true);
          setTitle("Select Material");
     }

     public void addComponents()
     {

          BrowList            . addKeyListener(new KeyList());
          
          BottomPanel         = new JPanel(true);
          BottomPanel         . setLayout(new GridLayout(1,2));
          BottomPanel         . add(TIndicator);
          
          getContentPane()    .setLayout(new BorderLayout());
          getContentPane()    .add("Center",BrowScroll);
          getContentPane()    .add("South",BottomPanel);
          setPresets();
          show();
     }

     
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }
     
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();

                    String SMatNameCode =  MiddlePanel.dataUse.setPatItemNameCode(index);
                    String SMatName     =  MiddlePanel.dataUse.setPatItemName(index);
                    String SMatCode     =  MiddlePanel.dataUse.setPatItemCode(index);
                    String SMatUom      =  MiddlePanel.dataUse.setPatUomName(index);
                    boolean bFlag = checkRowData(SMatCode);
                    if(bFlag)
                    {
                         JOptionPane.showMessageDialog(null,"This Item already selected","Duplicate",JOptionPane.INFORMATION_MESSAGE);
                    }
                    else
                    {
                         setMiddlePanel(SMatName,SMatCode,SMatUom);
                         str            = "";
                         removeHelpFrame();
                    }
               }
          }
     }

     private boolean checkRowData(String SMatCode)
     {
          boolean bFlag = false;

          for(int i=0;i<MiddlePanel.RowData.length;i++)
          {
               String SCode = (String)MiddlePanel.RowData[i][0];

               if(SCode.equals(SMatCode))
               {
                    return true;
               }
               else
               {
                    continue;
               }
          }
          return bFlag;
     }
     
     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<MiddlePanel.dataUse.getItemListSize();index++)
          {
               String str1 = ((String)MiddlePanel.dataUse.setPatItemName(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }
     
     public void setCursor(String xtr)
     {
          int index=0;
          for(index=0;index<MiddlePanel.dataUse.getItemListSize();index++)
          {
               String str1 = ((String)MiddlePanel.dataUse.setPatItemName(index)).toUpperCase();
               if(str1.startsWith(xtr))
               {
                    BrowList  .setSelectedIndex(index);
                    BrowList  .ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }
     
     private void setPresets()
     {
          String SName = (String)MiddlePanel.RowData[iRow][1];
          if(SName.length() > 0 )
               setCursor(SName.toUpperCase());
     }
     
     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
               MiddlePanel.tabreport.ReportTable.requestFocus();
          }
          catch(Exception ex) { }
     }
     
     public void setMiddlePanel(String SMatName,String SMatCode,String SMatUom)
     {
          Item      IC   = new Item(SMatCode,iMillCode,SItemTable,SSupTable,VGUserCode);
          double dStock  = common.toDouble(IC.getClStock());

          Item IC2  = new Item(SMatCode,iMillCode,SItemTable,SSupTable);

          double         dAllStock = common.toDouble(IC2.getClStock());
          double         dAllValue = common.toDouble(IC2.getClValue());

          double dRate   = 0;
          try
          {
               dRate = dAllValue/dAllStock;
          }
          catch(Exception ex)
          {
               dRate=0;
          }

          if(dAllStock==0)
          {
               dRate = common.toDouble(IC2.SRate);
          }

          String SStock  = common.getRound(dStock,3);
          
          MiddlePanel.RowData[iRow][0] = SMatCode;
          MiddlePanel.RowData[iRow][1] = SMatName;
          MiddlePanel.RowData[iRow][2] = SMatUom;
          MiddlePanel.RowData[iRow][3] = "";
          MiddlePanel.RowData[iRow][4] = "";
          MiddlePanel.RowData[iRow][5] = SStock;
          MiddlePanel.RowData[iRow][6] = "";
          MiddlePanel.RowData[iRow][7] = common.getRound(dRate,4);
          MiddlePanel.RowData[iRow][8] = "0";
     }
}
