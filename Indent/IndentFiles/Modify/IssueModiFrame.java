package Indent.IndentFiles.Modify;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;
import Indent.IndentFiles.*;

public class IssueModiFrame extends JInternalFrame
{
     DateField           TDate;
     NextField           TIndIssNo,TRefNo;
     IssueMiddlePanel    MiddlePanel;
     Common              common = new Common();
     Control             control;
     DataUse             dataUse;
     StatusPanel         SPanel;

     Connection          theConnection  =    null;
     IssueRecords        issuerecords   =    null;

     JLayeredPane        Layer;
     JComboBox           JCUDept,JCUnit,JCCata,JCType;
     JButton             BOk,BCancel;
     JLabel              LIndIss;
     MyLabel             LIUser;
     JPanel              TopPanel,BottomPanel;
     int                 iUserCode,iMillCode;
     String              SIssueNo,SIssueDate,SIndentNo;
     String              SItemTable,SSupTable;
     String              SIUserCode,SIUserName;

     Vector              VIndentTypeCode,VIndentTypeName;
     Vector              VGUserCode;
     Vector              VSeleCode,VSeleStock;
     Vector              VRemCode,VRemStock,VRemSlNo,VRemStatus;
     boolean             bComflag       =    true;

     int iMSlNo=0;

     public IssueModiFrame(JLayeredPane Layer,StatusPanel SPanel,int iUserCode,int iMillCode,String SIssueNo,String SIssueDate,String SIndentNo,String SItemTable,String SSupTable,String SIUserCode,String SIUserName)
     {
          this.Layer      = Layer;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.SIssueNo   = SIssueNo;
          this.SIssueDate = SIssueDate;
          this.SIndentNo  = SIndentNo;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SIUserCode = SIUserCode;
          this.SIUserName = SIUserName;

          getDBConnection();
          getGroupUserCode();
          setDataintoVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setPresets();
     }

     public void getDBConnection()
     {
          try
          {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnection  =    oraConnection.getConnection();
                              theConnection  .    setAutoCommit(false);

          }catch(SQLException SQLE)
          {
               System.out.println("IndentFrame getDBConnections SQLException"+ SQLE);
               SQLE.printStackTrace();
          }
     }

     public void createComponents()
     {
          BOk                 = new JButton("Update");
          BCancel             = new JButton("Cancel");

          dataUse             = new DataUse(iMillCode,SItemTable);
          dataUse             .     getOtherData();

          JCUDept             = new JComboBox(dataUse.getHodName());
          JCUnit              = new JComboBox(dataUse.getUnitName());
          JCCata              = new JComboBox(dataUse.getClassName());
          JCType              = new JComboBox(dataUse.getTypeName());

          JCUnit              . setEnabled(false);
          JCUDept             . setEnabled(false);
          JCType              . setEnabled(false);

          TIndIssNo           = new NextField();
          TRefNo              = new NextField();
          TDate               = new DateField();
          LIndIss             = new JLabel("  Indent No");
          LIUser              = new MyLabel("");

          TopPanel            = new JPanel();
          MiddlePanel         = new IssueMiddlePanel(Layer,JCUnit,JCCata,iUserCode,iMillCode,SItemTable,SSupTable,VGUserCode);
          BottomPanel         = new JPanel();
          
          control             = new Control();
          TRefNo              . setEditable(false);
          TIndIssNo           . setEditable(false);
          TIndIssNo           . setText(SIssueNo);

          TDate               . setEditable(false);
          BOk                 . setEnabled(false);
     }

     public void setLayouts()
     {
          setTitle("Issue Modification");
          setMaximizable(true);
          setClosable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,795,495);
          
          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(4,4,10,10));
          BottomPanel         .setLayout(new FlowLayout());
          TopPanel            .setBorder(new TitledBorder("Control Block"));
     }

     public void addComponents()
     {
          TopPanel         . add(new JLabel(""));
          TopPanel         . add(new MyLabel(" User"));
          TopPanel         . add(LIUser);
          TopPanel         . add(new JLabel(""));

          TopPanel         . add(new JLabel(" Date"));
          TopPanel         . add(TDate);
          TopPanel         . add(new JLabel("  User Indent No"));
          TopPanel         . add(TRefNo);
          
          TopPanel         . add(new JLabel(" Unit"));
          TopPanel         . add(JCUnit);
          TopPanel         . add(new JLabel("  Indent Type"));
          TopPanel         . add(JCType);

          TopPanel         . add(new JLabel(" User Department"));
          TopPanel         . add(JCUDept);
          TopPanel         . add(LIndIss);
          TopPanel         . add(TIndIssNo);
          
          BottomPanel      . add(BOk);
          
          getContentPane() . add("North",TopPanel);
          getContentPane() . add("Center",MiddlePanel);
          getContentPane() . add("South",BottomPanel);
     }

     public void setPresets()
     {
          issuerecords   = new IssueRecords(SIssueNo,SIssueDate,SIndentNo,iMillCode,SItemTable,SSupTable,VGUserCode);
          
          BOk            . setEnabled(true);
          TIndIssNo      . setText(SIssueNo);

          TDate          . fromString(common.parseDate(SIssueDate));
          TRefNo         . setText(SIndentNo);

          JCUnit         . setSelectedItem(issuerecords.SUnit);
          JCUDept        . setSelectedItem(issuerecords.SHodName);

          LIUser         . setText(SIUserName);

          JCType   . setSelectedIndex(0);
          
          MiddlePanel    . setData(issuerecords);
     }

          /*   Event Related  */

     public void addListeners()
     {
          BOk  .addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(setData())
               {
                    setComOrRol();
                    removeHelpFrame();
               }
               else
               {
                    JOptionPane    .showMessageDialog(null,"The Entry data was not Saved ","InforMation",JOptionPane.INFORMATION_MESSAGE);
                    BOk            .setEnabled(true);
               }
          }
     }

     public void setComOrRol()
     {
          if(bComflag)
          {
               try  {
                         theConnection .commit();
                         JOptionPane.showMessageDialog(null,"Data Saved","InforMation",JOptionPane.INFORMATION_MESSAGE);
               }    catch(Exception ex)
               {    System.out.println("Indent frame actionPerformed ->"+ex);ex.printStackTrace();  }
          }
          else
          {
               try  {
                         theConnection  .rollback();
                         JOptionPane    .showMessageDialog(null,"The Given Data is not Saved Please Enter Correct data","InforMation",JOptionPane.INFORMATION_MESSAGE);
                         BOk            .setEnabled(true);
               }    catch(Exception ex)
               {    System.out.println("Indent frame actionPerformed ->"+ex);ex.printStackTrace();  }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer     .remove(this);
               Layer     .repaint();
               Layer     .updateUI();
          }
          catch(Exception ex) { }
     }

        /*      DataBase Related         */

     private int getIssueID()
     {
          Statement stat      = null;
          ResultSet result    = null;

          int iId=0;
          try
          {
               stat           =  theConnection    .createStatement();
               result         =  stat             .executeQuery("Select Issue_Seq.nextVal from Dual");

               while(result.next())
               {
                    iId  =    result.getInt(1);
               }
               result    .close();
               stat      .close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
               bComflag       =    true;
          }
          return iId;
     }

     public boolean setData()
     {
          if(!isStockValid())
          {
               return false;
          }
          
          if(!isDataValid())
          {
               return false;
          }

          try
          {
               updatePrevData();
               updateNewData();
          }
          catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
          return bComflag;
     }

     private void updatePrevData()
     {
          try
          {
               int j          =  issuerecords     .VIssueCode.size();

               iMSlNo         =  issuerecords     .iMaxSlNo;
               String SHCode  =  String.valueOf(dataUse.getHodCode(((String)JCUDept.getSelectedItem()).trim()));
               String SUCode  =  String.valueOf(dataUse.getUnitCode(((String)JCUnit .getSelectedItem()).trim()));

               Statement stat      =  theConnection.createStatement();

               for(int i=0;i<j;i++)
               {     
                    if(!isEligible(i))
                         continue;

                    String SOldCode     = (String)issuerecords.VIssueCode.elementAt(i);
                    double dOldQty      = common.toDouble((String)issuerecords.VIssueQty.elementAt(i));
                    double dOldVal      = common.toDouble((String)issuerecords.VIssueValue.elementAt(i));
                    double dOldRate     = common.toDouble((String)issuerecords.VIssueRate.elementAt(i));

                    String SNewCode     = (String)MiddlePanel.tabreport.ReportTable.getValueAt(i,0);
                    double dNewQty      = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,6));
                    double dNewVal      = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,8));
                    double dNewRate     = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,7));

                    String SDept        = common.parseNull(((String)MiddlePanel.tabreport.RowData[i][3]).trim());
                    String SCata        = common.parseNull(((String)MiddlePanel.tabreport.RowData[i][4]).trim());


                    if((SNewCode.trim()).equals(SOldCode.trim()))
                    {
                         if(dNewQty>dOldQty)
                         {
                              double dExcessQty = dNewQty - dOldQty;
                              double dExcessVal = dExcessQty * dNewRate;

                              getUserExcessStock(stat,SOldCode,dExcessQty);

                              for(int k=0;k<VSeleCode.size();k++)
                              {
                                   iMSlNo++;
          
                                   String SAuthUserCode = (String)VSeleCode.elementAt(k);
                                   double dIssQty       = common.toDouble((String)VSeleStock.elementAt(k));
                                   double dIssVal       = (dIssQty * dExcessVal) / dExcessQty;
          
                                   String str = "";

                                   str = "Insert Into Issue(id,IssueNo,IssueDate,Code,HodCode,Qty,IssRate,UIRefNo,Dept_Code,Group_Code,Unit_Code,UserCode,CreationDate,issueValue,SlNo,IndentType,MrsAuthUserCode,IndentUserCode,MillCode) Values (";
                                   str = str+ getIssueID()+",";
                                   str = str+ TIndIssNo.getText()+",";
                                   str = str+"'"+TDate.toNormal()+"',";
                                   str = str+"'"+SOldCode+"',";
                                   str = str+SHCode+",";
                                   str = str+common.getRound(dIssQty,3)+",";
                                   str = str+common.getRound(dNewRate,4)+",";
                                   str = str+"0"+TRefNo.getText()+",";
                                   str = str+dataUse.getDeptCode(SDept)+",";
                                   str = str+dataUse.getClassCode(SCata)+",";
                                   str = str+SUCode+",";
                                   str = str+String.valueOf(iUserCode)+",";
                                   str = str+"'"+common.getServerDate()+"',";
                                   str = str+common.getRound(dIssVal,2)+",";
                                   str = str+String.valueOf(iMSlNo)+",";
                                   str = str+getIndentCode(((String)JCType.getSelectedItem()).trim())+",";
                                   str = str+SAuthUserCode+",";
                                   str = str+SIUserCode+",";
                                   str = str+String.valueOf(iMillCode)+")";
               
                                   String QS1 = " Update ItemStock set Stock=nvl(Stock,0)-("+dIssQty+")"+
                                                " Where ItemCode='"+SOldCode+"' and HodCode="+SAuthUserCode+" and MillCode="+iMillCode;
          
          
                                   String QS2 = " Update ItemStock set StockValue="+common.getRound(dNewRate,4)+
                                                " Where ItemCode='"+SOldCode+"' and MillCode="+iMillCode;
          
          
                                   if(theConnection.getAutoCommit())
                                        theConnection . setAutoCommit(false);
               
                                   stat.execute(str);
                                   stat.execute(QS1);
                                   stat.execute(QS2);
                              }

                              String QS3 = " Update "+SItemTable+" ";
                              QS3 = QS3 + " Set IssQty=nvl(IssQty,0)+"+dExcessQty+",IssVal=nvl(IssVal,0)+"+dExcessVal+" ";
                              QS3 = QS3 + " Where Item_Code = '"+SOldCode+"'";

                              stat.execute(QS3);
                         }
                         else
                         if(dNewQty<dOldQty)
                         {
                              double dShortQty = dOldQty - dNewQty;
                              double dShortVal = dShortQty * dOldRate;

                              getUserShortStock(stat,SOldCode,dShortQty);

                              for(int k=0;k<VRemCode.size();k++)
                              {
                                   String SAuthUserCode = (String)VRemCode.elementAt(k);
                                   String SSlNo         = (String)VRemSlNo.elementAt(k);
                                   String SRemStatus    = (String)VRemStatus.elementAt(k);
                                   double dRemQty       = common.toDouble((String)VRemStock.elementAt(k));
                                   double dRemVal       = (dRemQty * dShortVal) / dShortQty;

                                   String QS1 = "";
                                   String QS2 = "";

                                   if(SRemStatus.equals("1"))
                                   {
                                        QS1 = " Delete from Issue Where IssueNo="+SIssueNo+" and UIRefNo="+SIndentNo+
                                              " and Code='"+SOldCode+"' and SlNo="+SSlNo+" and MrsAuthUserCode="+SAuthUserCode;

                                        QS2 = " Update ItemStock set Stock=nvl(Stock,0)+("+dRemQty+")"+
                                              " Where ItemCode='"+SOldCode+"' and HodCode="+SAuthUserCode+" and MillCode="+iMillCode;

                                   }
                                   else
                                   {
                                        QS1 = " Update Issue Set Qty=Qty-("+dRemQty+"),IssueValue=IssueValue-("+dRemVal+") Where IssueNo="+SIssueNo+" and UIRefNo="+SIndentNo+
                                              " and Code='"+SOldCode+"' and SlNo="+SSlNo+" and MrsAuthUserCode="+SAuthUserCode;

                                        QS2 = " Update ItemStock set Stock=nvl(Stock,0)+("+dRemQty+")"+
                                              " Where ItemCode='"+SOldCode+"' and HodCode="+SAuthUserCode+" and MillCode="+iMillCode;
                                   }
                                   
                                   if(theConnection.getAutoCommit())
                                        theConnection . setAutoCommit(false);
               
                                   stat.execute(QS1);
                                   stat.execute(QS2);
                              }

                              String QS3 = " Update "+SItemTable+" ";
                              QS3 = QS3 + " Set IssQty=nvl(IssQty,0)-("+dShortQty+"),IssVal=nvl(IssVal,0)-("+dShortVal+") ";
                              QS3 = QS3 + " Where Item_Code = '"+SOldCode+"'";

                              stat.execute(QS3);
                         }
                         else
                         {
                              continue;
                         }

                    }
                    else
                    {
                         double dShortQty = dOldQty;
                         double dShortVal = dOldVal;

                         getUserShortStock(stat,SOldCode,dShortQty);

                         for(int k=0;k<VRemCode.size();k++)
                         {
                              String SAuthUserCode = (String)VRemCode.elementAt(k);
                              String SSlNo         = (String)VRemSlNo.elementAt(k);
                              String SRemStatus    = (String)VRemStatus.elementAt(k);
                              double dRemQty       = common.toDouble((String)VRemStock.elementAt(k));
                              double dRemVal       = (dRemQty * dShortVal) / dShortQty;

                              String QS1 = "";
                              String QS2 = "";

                              if(SRemStatus.equals("1"))
                              {
                                   QS1 = " Delete from Issue Where IssueNo="+SIssueNo+" and UIRefNo="+SIndentNo+
                                         " and Code='"+SOldCode+"' and SlNo="+SSlNo+" and MrsAuthUserCode="+SAuthUserCode;

                                   QS2 = " Update ItemStock set Stock=nvl(Stock,0)+("+dRemQty+")"+
                                         " Where ItemCode='"+SOldCode+"' and HodCode="+SAuthUserCode+" and MillCode="+iMillCode;

                              }
                              else
                              {
                                   QS1 = " Update Issue Set Qty=Qty-("+dRemQty+"),IssueValue=IssueValue-("+dRemVal+") Where IssueNo="+SIssueNo+" and UIRefNo="+SIndentNo+
                                         " and Code='"+SOldCode+"' and SlNo="+SSlNo+" and MrsAuthUserCode="+SAuthUserCode;

                                   QS2 = " Update ItemStock set Stock=nvl(Stock,0)+("+dRemQty+")"+
                                         " Where ItemCode='"+SOldCode+"' and HodCode="+SAuthUserCode+" and MillCode="+iMillCode;
                              }
                              
                              if(theConnection.getAutoCommit())
                                   theConnection . setAutoCommit(false);
          
                              stat.execute(QS1);
                              stat.execute(QS2);
                         }

                         String QS3 = " Update "+SItemTable+" ";
                         QS3 = QS3 + " Set IssQty=nvl(IssQty,0)-("+dShortQty+"),IssVal=nvl(IssVal,0)-("+dShortVal+") ";
                         QS3 = QS3 + " Where Item_Code = '"+SOldCode+"'";

                         stat.execute(QS3);


                         double dExcessQty = dNewQty;
                         double dExcessVal = dNewVal;

                         getUserExcessStock(stat,SNewCode,dExcessQty);

                         for(int k=0;k<VSeleCode.size();k++)
                         {
                              iMSlNo++;
     
                              String SAuthUserCode = (String)VSeleCode.elementAt(k);
                              double dIssQty       = common.toDouble((String)VSeleStock.elementAt(k));
                              double dIssVal       = (dIssQty * dExcessVal) / dExcessQty;
     
                              String str = "";

                              str = "Insert Into Issue(id,IssueNo,IssueDate,Code,HodCode,Qty,IssRate,UIRefNo,Dept_Code,Group_Code,Unit_Code,UserCode,CreationDate,issueValue,SlNo,IndentType,MrsAuthUserCode,IndentUserCode,MillCode) Values (";
                              str = str+ getIssueID()+",";
                              str = str+ TIndIssNo.getText()+",";
                              str = str+"'"+TDate.toNormal()+"',";
                              str = str+"'"+SNewCode+"',";
                              str = str+SHCode+",";
                              str = str+common.getRound(dIssQty,3)+",";
                              str = str+common.getRound(dNewRate,4)+",";
                              str = str+"0"+TRefNo.getText()+",";
                              str = str+dataUse.getDeptCode(SDept)+",";
                              str = str+dataUse.getClassCode(SCata)+",";
                              str = str+SUCode+",";
                              str = str+String.valueOf(iUserCode)+",";
                              str = str+"'"+common.getServerDate()+"',";
                              str = str+common.getRound(dIssVal,2)+",";
                              str = str+String.valueOf(iMSlNo)+",";
                              str = str+getIndentCode(((String)JCType.getSelectedItem()).trim())+",";
                              str = str+SAuthUserCode+",";
                              str = str+SIUserCode+",";
                              str = str+String.valueOf(iMillCode)+")";
          
                              String QS1 = " Update ItemStock set Stock=nvl(Stock,0)-("+dIssQty+")"+
                                           " Where ItemCode='"+SNewCode+"' and HodCode="+SAuthUserCode+" and MillCode="+iMillCode;
     
     
                              String QS2 = " Update ItemStock set StockValue="+common.getRound(dNewRate,4)+
                                           " Where ItemCode='"+SNewCode+"' and MillCode="+iMillCode;
     
     
                              if(theConnection.getAutoCommit())
                                   theConnection . setAutoCommit(false);
          
                              stat.execute(str);
                              stat.execute(QS1);
                              stat.execute(QS2);
                         }

                         String QS4 = " Update "+SItemTable+" ";
                         QS4 = QS4 + " Set IssQty=nvl(IssQty,0)+"+dExcessQty+",IssVal=nvl(IssVal,0)+"+dExcessVal+" ";
                         QS4 = QS4 + " Where Item_Code = '"+SNewCode+"'";

                         stat.execute(QS4);
                    }
               }
               stat .close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame updatePrevData() ->"+e);
               e.printStackTrace();
               bComflag  = false;
          }
     }

     private void updateNewData()
     {
          try
          {
               int j          =  issuerecords     .VIssueCode.size();

               String SHCode  =  String.valueOf(dataUse.getHodCode(((String)JCUDept.getSelectedItem()).trim()));
               String SUCode  =  String.valueOf(dataUse.getUnitCode(((String)JCUnit .getSelectedItem()).trim()));

               Statement stat      =  theConnection.createStatement();

               for(int i=j;i<MiddlePanel.tabreport.RowData.length;i++)
               {     
                    if(!isEligible(i))
                         continue;

                    String SNewCode     = (String)MiddlePanel.tabreport.ReportTable.getValueAt(i,0);
                    double dNewQty      = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,6));
                    double dNewVal      = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,8));
                    double dNewRate     = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,7));

                    String SDept        = common.parseNull(((String)MiddlePanel.tabreport.RowData[i][3]).trim());
                    String SCata        = common.parseNull(((String)MiddlePanel.tabreport.RowData[i][4]).trim());

                    double dExcessQty = dNewQty;
                    double dExcessVal = dNewVal;

                    getUserExcessStock(stat,SNewCode,dExcessQty);

                    for(int k=0;k<VSeleCode.size();k++)
                    {
                         iMSlNo++;

                         String SAuthUserCode = (String)VSeleCode.elementAt(k);
                         double dIssQty       = common.toDouble((String)VSeleStock.elementAt(k));
                         double dIssVal       = (dIssQty * dExcessVal) / dExcessQty;

                         String str = "";

                         str = "Insert Into Issue(id,IssueNo,IssueDate,Code,HodCode,Qty,IssRate,UIRefNo,Dept_Code,Group_Code,Unit_Code,UserCode,CreationDate,issueValue,SlNo,IndentType,MrsAuthUserCode,IndentUserCode,MillCode) Values (";
                         str = str+ getIssueID()+",";
                         str = str+ TIndIssNo.getText()+",";
                         str = str+"'"+TDate.toNormal()+"',";
                         str = str+"'"+SNewCode+"',";
                         str = str+SHCode+",";
                         str = str+common.getRound(dIssQty,3)+",";
                         str = str+common.getRound(dNewRate,4)+",";
                         str = str+"0"+TRefNo.getText()+",";
                         str = str+dataUse.getDeptCode(SDept)+",";
                         str = str+dataUse.getClassCode(SCata)+",";
                         str = str+SUCode+",";
                         str = str+String.valueOf(iUserCode)+",";
                         str = str+"'"+common.getServerDate()+"',";
                         str = str+common.getRound(dIssVal,2)+",";
                         str = str+String.valueOf(iMSlNo)+",";
                         str = str+getIndentCode(((String)JCType.getSelectedItem()).trim())+",";
                         str = str+SAuthUserCode+",";
                         str = str+SIUserCode+",";
                         str = str+String.valueOf(iMillCode)+")";
     
                         String QS1 = " Update ItemStock set Stock=nvl(Stock,0)-("+dIssQty+")"+
                                      " Where ItemCode='"+SNewCode+"' and HodCode="+SAuthUserCode+" and MillCode="+iMillCode;


                         String QS2 = " Update ItemStock set StockValue="+common.getRound(dNewRate,4)+
                                      " Where ItemCode='"+SNewCode+"' and MillCode="+iMillCode;


                         if(theConnection.getAutoCommit())
                              theConnection . setAutoCommit(false);
     
                         stat.execute(str);
                         stat.execute(QS1);
                         stat.execute(QS2);
                    }

                    String QS3 = " Update "+SItemTable+" ";
                    QS3 = QS3 + " Set IssQty=nvl(IssQty,0)+"+dExcessQty+",IssVal=nvl(IssVal,0)+"+dExcessVal+" ";
                    QS3 = QS3 + " Where Item_Code = '"+SNewCode+"'";

                    stat.execute(QS3);
               }
               stat .close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame updateNewData() ->"+e);
               e.printStackTrace();
               bComflag  = false;
          }
     }

     private void getUserExcessStock(Statement stat,String SItemCode,double dIssueQty)
     {
          try
          {
               VSeleCode  = new Vector();
               VSeleStock = new Vector();

               Vector VHCode     = new Vector();
               Vector VHStock    = new Vector();

               String QS1 = " Select HodCode,Stock from ItemStock Where ItemCode='"+SItemCode+"' and MillCode="+iMillCode+" Order By HodCode ";
               
               ResultSet res = stat.executeQuery(QS1);
               while(res.next())
               {
                    VHCode.addElement(res.getString(1));
                    VHStock.addElement(res.getString(2));
               }
               res.close();

               String QS = "";

               for(int i=0;i<VGUserCode.size();i++)
               {
                    String SGUserCode = (String)VGUserCode.elementAt(i);

                    int iIndex = VHCode.indexOf(SGUserCode);

                    if(iIndex>=0)
                    {
                         double dUserStock    = common.toDouble((String)VHStock.elementAt(iIndex));

                         if(dUserStock<=0)
                              continue;


                         if(dIssueQty<=dUserStock)
                         {
                              VSeleCode.addElement(SGUserCode);
                              VSeleStock.addElement(""+dIssueQty);

                              break;
                         }
                         else
                         {
                              VSeleCode.addElement(SGUserCode);
                              VSeleStock.addElement(""+dUserStock);

                              dIssueQty = dIssueQty - dUserStock;
                         }
                    }
                    else
                    {
                         continue;
                    }

               }
          }
          catch(Exception e)
          {
               System.out.println("E7"+e);
               bComflag  = false;
          }
     }

     private void getUserShortStock(Statement stat,String SItemCode,double dIssueQty)
     {
          try
          {
               VRemCode   = new Vector();
               VRemStock  = new Vector();
               VRemSlNo   = new Vector();
               VRemStatus = new Vector();

               Vector VHCode     = new Vector();
               Vector VHStock    = new Vector();
               Vector VHSlNo     = new Vector();

               String QS1 = " Select MrsAuthUserCode,Qty,SlNo from Issue Where Code='"+SItemCode+"' and IssueNo="+SIssueNo+" and UIRefNo="+SIndentNo+" and MillCode="+iMillCode+" Order By MrsAuthUserCode desc ";
               
               ResultSet res = stat.executeQuery(QS1);
               while(res.next())
               {
                    VHCode.addElement(res.getString(1));
                    VHStock.addElement(res.getString(2));
                    VHSlNo.addElement(res.getString(3));
               }
               res.close();

               String QS = "";

               for(int i=0;i<VHCode.size();i++)
               {
                    String SGUserCode = (String)VHCode.elementAt(i);
                    String SSlNo      = (String)VHSlNo.elementAt(i);

                    double dUserStock = common.toDouble((String)VHStock.elementAt(i));

                    if(dUserStock<=0)
                         continue;


                    if(dIssueQty<=dUserStock)
                    {
                         VRemCode.addElement(SGUserCode);
                         VRemStock.addElement(""+dIssueQty);
                         VRemSlNo.addElement(SSlNo);
                         if(dIssueQty<dUserStock)
                         {
                              VRemStatus.addElement("0");
                         }
                         else
                         {
                              VRemStatus.addElement("1");
                         }

                         break;
                    }
                    else
                    {
                         VRemCode.addElement(SGUserCode);
                         VRemStock.addElement(""+dUserStock);
                         VRemSlNo.addElement(SSlNo);
                         VRemStatus.addElement("1");

                         dIssueQty = dIssueQty - dUserStock;
                    }

               }
          }
          catch(Exception e)
          {
               System.out.println("E7"+e);
               bComflag  = false;
          }
     }


          /*        Data  Validation          */

     private boolean isStockValid()
     {
          boolean bflag = true;
          
          if(((String)JCType.getSelectedItem()).equals("Return"))
          {
               bflag = true;
          }
          else
          {
               for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
               {
                    String Smsg="";
                    String SCode        = ((String)MiddlePanel.tabreport.RowData[i][0]).trim();
                    double dIssueQty    = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][6]).trim());

                    String SEnDate      = TDate.toNormal();
                    
                    if(SCode.length() == 0)
                         continue;
                    
                    double dStock = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][5]).trim());

                    if(dStock<0)
                         Smsg = Smsg + " Negative Stock for the Date,"; 

                    if(Smsg.length()>0)
                    {
                         Smsg = Smsg.substring(0,(Smsg.length()-1));
                         Smsg = Smsg + " in S.No - " + (i+1);
                         JOptionPane.showMessageDialog(null,Smsg,"Invalid Quantity",JOptionPane.INFORMATION_MESSAGE);
                         bflag=false;
                    }
               }
          }
          return bflag;
     }

     private boolean isDataValid()
     {
          boolean bflag = true;
          
          for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
          {
               String Smsg="";

               String SCode   = ((String)MiddlePanel.tabreport.RowData[i][0]).trim();
               String SDept   = ((String)MiddlePanel.tabreport.RowData[i][3]).trim();
               String SCata   = ((String)MiddlePanel.tabreport.RowData[i][4]).trim();

               if(i==0)
               {
                    if(SCode.length()==0)
                    {
                         JOptionPane.showMessageDialog(null,"No Data in the Table","Missing",JOptionPane.INFORMATION_MESSAGE);
                         return false;
                    }
               }

               if(SCode.length() == 0)
                    continue;
               
               if(dataUse.getDeptCode(SDept)==0)
                    Smsg = Smsg + "Department,";

               if(dataUse.getClassCode(SCata)==0)
                    Smsg = Smsg + " Classification,";

               if(!isEligible(i))
                    Smsg = Smsg + " Quantity,";

               if(Smsg.length()>0)
               {
                    Smsg = Smsg.substring(0,(Smsg.length()-1));
                    Smsg = Smsg + "  Field(s) Missing in S.No - " + (i+1);
                    JOptionPane.showMessageDialog(null,Smsg,"Missing Fields",JOptionPane.INFORMATION_MESSAGE);
                    bflag=false;
               }
          }
          return bflag;
     }

     private boolean isEligible(int iRowCount)
     {
          double dQty            = common.toDouble(common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(iRowCount,6)));
          String SItemCode    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(iRowCount,0));

          if(dQty==0)
               return false;
          if(SItemCode.length()<3)
               return false;

          return true;
     }

     public int getIndentCode(String SIndentName)
     {
          int iIndex    = VIndentTypeName.indexOf(SIndentName.trim());

          return common.toInt(((String)VIndentTypeCode.elementAt(iIndex)).trim());
     }

     public void setDataintoVector()
     {
          VIndentTypeCode     = new Vector();
          VIndentTypeName     = new Vector();

          String QS      = "Select code,name from indenttype where (millcode="+iMillCode +" or millcode = 2) and  issueflag = 0";

          try  {
                    ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                    Connection     theConnection  = oraConnection.getConnection();   
                    Statement      stat           = theConnection.createStatement();
     
                    ResultSet result = stat.executeQuery(QS);

                    while(result.next())
                    {
                         VIndentTypeCode     .addElement(common.parseNull(result.getString(1)));
                         VIndentTypeName     .addElement(common.parseNull(result.getString(2)));
                    }
                    result    .close();
                    stat      .close();
          } catch(Exception Ex)
          {
               System.out.println("getIndentCode from Indent "+Ex);
          }
     }

     public void getGroupUserCode()
     {
          VGUserCode = new Vector();

          String SGroupUserCode = "";

          VGUserCode.addElement("1");
          
          try
          {
               ORAConnection   oraConnection = ORAConnection.getORAConnection();
               Connection      theConnection = oraConnection.getConnection();               

               Statement       stat          = theConnection.createStatement();
               ResultSet       result        = stat.executeQuery("Select GroupUserCode from UserGroup Where AuthUserCode="+SIUserCode+" and MillCode="+iMillCode);

               while(result.next())
               {
                    SGroupUserCode = common.parseNull(result.getString(1));
               }
               result.close();
               
               if(SGroupUserCode.equals(""))
               {
                    VGUserCode.addElement(SIUserCode);
               }
               else
               {
                    result = stat.executeQuery("Select AuthUserCode from UserGroup Where GroupUserCode="+SGroupUserCode+" and MillCode="+iMillCode+" Order by AuthUserCode");
     
                    while(result.next())
                    {
                         VGUserCode.addElement(common.parseNull(result.getString(1)));
                    }
                    result.close();
               }
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("GroupUser :"+ex);
          }
     }

}
