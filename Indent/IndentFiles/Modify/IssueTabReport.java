package Indent.IndentFiles.Modify;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class IssueTabReport extends JPanel
{
     JTable         ReportTable;
     Object         RowData[][];
     String         ColumnData[],ColumnType[];
     JPanel         thePanel;
     JComboBox      JCCata,JCDept;
     Common common = new Common();

     IssueMiddlePanel IMP;
     IssueRecords     issuerecords;
     Vector           VGUserCode;
     AbstractTableModel dataModel;
     
     IssueTabReport(Object RowData[][],String ColumnData[],String ColumnType[],JComboBox JCCata,JComboBox JCDept,IssueMiddlePanel IMP,IssueRecords issuerecords,Vector VGUserCode)
     {
          this.RowData       = RowData;
          this.ColumnData    = ColumnData;
          this.ColumnType    = ColumnType;
          this.JCCata        = JCCata;
          this.JCDept        = JCDept;
          this.IMP           = IMP;
          this.issuerecords  = issuerecords;
          this.VGUserCode    = VGUserCode;
          
          thePanel           = new JPanel();
          setReportTable();
          setBorder(new TitledBorder("List Of Items"));
     }
     
     public void setReportTable()
     {
          dataModel = new AbstractTableModel()
          {
               public int getColumnCount(){return ColumnData.length;}
               public int getRowCount(){return RowData.length;}
               public Object getValueAt(int row,int col){return RowData[row][col];}
               public String getColumnName(int col){ return ColumnData[col];}
               public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }
               public boolean isCellEditable(int row,int col)
               {
                    if(ColumnType[col]=="B" || ColumnType[col]=="E")
                         return true;
                    return false;
               }
               public void setValueAt(Object element,int row,int col)
               {
                    if(col!=6)
                         RowData[row][col]=element;
                    if(col > 6)
                         return;
                    if(col == 6)
                    {
                         if(isValidQty((String)element))
                         {
                              RowData[row][col]=element; 
                              setTotal();

                              double dQty   = Double.parseDouble((String)element);
                              String SCode   = (String)ReportTable.getValueAt(row,0);
                              Item IC        = new Item(SCode,IMP.iMillCode,IMP.SItemTable,IMP.SSupTable,VGUserCode);
                              double dStock  = common.toDouble(IC.getClStock());

                              if(row<issuerecords.VIssueCode.size())
                              {
                                   double dIssQty      = 0;

                                   String SNewCode     = (String)ReportTable.getValueAt(row,0);
                                   String SOldCode     = (String)issuerecords.VIssueCode.elementAt(row);
                                   if(SNewCode.equals(SOldCode))
                                   {
                                        dIssQty = common.toDouble((String)issuerecords.VIssueQty.elementAt(row));

                                        dStock = dStock + dIssQty - dQty;
                                   }
                                   else
                                   {
                                        dStock = getQtyStock(element,row,col);
                                   }
                              }
                              else
                              {
                                   dStock = getQtyStock(element,row,col);
                              }
                              ReportTable.setValueAt(common.getRound(dStock,3),row,col-1);
                    }
                    else
                        JOptionPane.showMessageDialog(null,"Invaid Quantity","Error Message",JOptionPane.INFORMATION_MESSAGE);
                    }                    
               }
          };
          ReportTable  = new JTable(dataModel);
          ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
          DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
          cellRenderer.setHorizontalAlignment(JLabel.RIGHT);
          
          for (int col=0;col<ReportTable.getColumnCount();col++)
          {
               if(ColumnType[col]=="N" || ColumnType[col]=="E") 
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
          }
          TableColumn CataColumn = ReportTable.getColumn("Classification");
          CataColumn     .setCellEditor(new DefaultCellEditor(JCCata));
          TableColumn    DeptColumn = ReportTable.getColumn("Department");
          DeptColumn     .setCellEditor(new DefaultCellEditor(JCDept));
          setQtyHeader();
          formatColumns();
          setLayout(new BorderLayout());
          thePanel.setLayout(new BorderLayout());
          thePanel.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
          thePanel.add(new JScrollPane(ReportTable),BorderLayout.CENTER);
          add("Center",thePanel);
     }
     
     private void setQtyHeader()
     {
          DefaultTableCellRenderer cellRenderer   =    new DefaultTableCellRenderer();
                                   cellRenderer   .    setHorizontalAlignment(JLabel.CENTER);
                                   cellRenderer   .    setBackground(new Color(100,200,250));
                                   cellRenderer   .    setIcon(new ImageIcon("prodbig.gif"));
                                   cellRenderer   .    setToolTipText("Quantity");
                                   (ReportTable   .    getTableHeader()).setBorder(new BevelBorder(BevelBorder.RAISED));
                                   cellRenderer   .    setBorder(new SoftBevelBorder(0));
                                   (ReportTable   .    getColumn("Quantity")).setHeaderRenderer(cellRenderer);
     }

     private double getQtyStock(Object element,int row,int col)
     {
          if(isValidQty((String)element)) 
          {
               RowData[row][col]=element; 
               setTotal();
               double dQty    = Double.parseDouble((String)element);
               String SCode   = (String)ReportTable.getValueAt(row,0);
               Item IC        = new Item(SCode,IMP.iMillCode,IMP.SItemTable,IMP.SSupTable,VGUserCode);
               double dStock  = common.toDouble(IC.getClStock());
               
               dStock = dStock - dQty;
               ReportTable.setValueAt(common.getRound(dStock,3),row,col-1);

               return dStock;
          }
          else
               JOptionPane.showMessageDialog(null,"Invaid Quantity","Error Message",JOptionPane.INFORMATION_MESSAGE);

           return 0;
     }

     private boolean isValidQty(String str)
     {
          boolean bFlag=true;
          try
          {
               double dvalue=Double.parseDouble(str);
               bFlag = true;
          }
          catch(Exception ex)
          {
               bFlag=false;
          }
          return bFlag;
     }

     private void formatColumns()
     {
          TableColumn codeColumn  = ReportTable.getColumn(ReportTable.getColumnName(0));
          TableColumn nameColumn  = ReportTable.getColumn(ReportTable.getColumnName(1));
          TableColumn uomColumn   = ReportTable.getColumn(ReportTable.getColumnName(2));
          TableColumn deptColumn  = ReportTable.getColumn(ReportTable.getColumnName(3));
          TableColumn CataColumn  = ReportTable.getColumn(ReportTable.getColumnName(4));
          TableColumn stockColumn = ReportTable.getColumn(ReportTable.getColumnName(5));
          TableColumn qtyColumn   = ReportTable.getColumn(ReportTable.getColumnName(6));
          TableColumn rateColumn  = ReportTable.getColumn(ReportTable.getColumnName(7));
          TableColumn valueColumn = ReportTable.getColumn(ReportTable.getColumnName(8));
          
          codeColumn     .setPreferredWidth(75);
          nameColumn     .setPreferredWidth(140);
          uomColumn      .setPreferredWidth(50);
          deptColumn     .setPreferredWidth(90);
          CataColumn     .setPreferredWidth(90);
          stockColumn    .setPreferredWidth(75);
          qtyColumn      .setPreferredWidth(80);
          rateColumn     .setPreferredWidth(70);
          valueColumn    .setPreferredWidth(60);
     }
     
     private void setTotal()
     {
          double dValue=0,dTValue=0;
          for(int i=0;i<RowData.length;i++)
          {
               double    dQty      = common.toDouble((String)ReportTable.getValueAt(i,6));    
               double    dRate     = common.toDouble((String)ReportTable.getValueAt(i,7));
                         dValue    = dQty*dRate;
                         dTValue   = dTValue+dValue;
               ReportTable.setValueAt(common.getRound(dValue,2),i,8);
          }
     }
}
