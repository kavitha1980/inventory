package Indent.IndentFiles;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class NonStockIssueDeletionFrame extends JInternalFrame
{
     String         SStDate,SEnDate;
     JComboBox      JCOrder;
     JButton        BApply,BDelete;
     JPanel         TopPanel,BottomPanel;
     JPanel         DatePanel,SortPanel,BasisPanel,ApplyPanel;
     
     TabReport      tabreport;
     DateField      TStDate;
     DateField      TEnDate;
     
     JRadioButton   JRPeriod,JRNo;
     NextField      TStNo,TEnNo;
     
     Object         RowData[][];
     
     //  Issue Id = Issue No    Issue No = User Indent No
     
     String         ColumnData[] = {"Delete Mark","Issue Id","Date","Issue No","Code","Name","Issue Qty","Issue Rate","Unit","Department","Classification"};
     String         ColumnType[] = {"B","N","S","N","S","S","N","N","S","S","S"};
     Common         common = new Common();
     Vector         VIssueNo,VIssueDate,VRefNo,VIssueCode,VIssueName,VIssueQty,VIssueRate,VIssueUnit,VIssueDept,VIssueCata,VId,VIssueUserCode,VIndentType,VRdcNo;
     
     JLayeredPane   Layer;
     StatusPanel    SPanel;
     Vector         VCode,VName,VUom;
     Vector         VDICode,VDIName,VDIssQty,VDIssValue,VDIUserCode,VDIndentNo,VDIssueNo,VDIndentType,VDRdcNo;

     int            iUserCode,iMillCode;
     String         SItemTable; 

     Connection     theConnection  = null;
     Connection     theDConnection = null;

     boolean        bComflag = true;

     public NonStockIssueDeletionFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUom,StatusPanel SPanel,int iUserCode,int iMillCode,String SItemTable)
     {
          super("Non Stock Issue Deletion Utility");
		
          this.Layer      = Layer;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VUom       = VUom;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
     
          getConn();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void getConn()
     {
          try
          {
               ORAConnection  oraConnection = ORAConnection.getORAConnection();
                              theConnection = oraConnection.getConnection();
                              theConnection . setAutoCommit(false);

	          if(iMillCode==1)
	          {
	               DORAConnection jdbc           = DORAConnection.getORAConnection();
	                              theDConnection = jdbc.getConnection();
							theDConnection . setAutoCommit(false);
	          }
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void createComponents()
     {
          TStDate        = new DateField();
          TEnDate        = new DateField();
          TStNo          = new NextField();
          TEnNo          = new NextField();
          BApply         = new JButton("Apply");
          BDelete        = new JButton("Delete Marked Issues");
          
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          DatePanel      = new JPanel();
          SortPanel      = new JPanel();
          BasisPanel     = new JPanel();
          ApplyPanel     = new JPanel();

          JCOrder        = new JComboBox();

          JRPeriod       = new JRadioButton("Periodical",true);
          JRNo           = new JRadioButton("Issue No");
     
          TStDate        .setTodayDate();
          TEnDate        .setTodayDate();
          BDelete        .setEnabled(false);
     }

     public void setLayouts()
     {
          TopPanel       .setLayout(new GridLayout(1,4));
          BottomPanel    .setLayout(new FlowLayout());
          
          DatePanel      .setLayout(new GridLayout(3,1));
          SortPanel      .setLayout(new BorderLayout());
          BasisPanel     .setLayout(new GridLayout(2,1));
          ApplyPanel     .setLayout(new BorderLayout());
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }

     public void addComponents()
     {
          JCOrder        .addItem("Issue Id");
          JCOrder        .addItem("Issue No");
          JCOrder        .addItem("Materialwise");
          JCOrder        .addItem("Departmentwise");
          JCOrder        .addItem("Groupwise");
          JCOrder        .addItem("Unitwise");
          
          SortPanel      .add("Center",JCOrder);
          
          DatePanel      .add(TStDate);
          DatePanel      .add(TEnDate);
          
          BasisPanel     .add(JRPeriod);
          BasisPanel     .add(JRNo);
          
          ApplyPanel     .add("Center",BApply);
          
          TopPanel       .add(SortPanel);
          TopPanel       .add(BasisPanel);
          TopPanel       .add(DatePanel);
          TopPanel       .add(ApplyPanel);
          
          SortPanel      .setBorder(new TitledBorder("Sorting"));
          DatePanel      .setBorder(new TitledBorder("Period"));
          BasisPanel     .setBorder(new TitledBorder("Basis"));
          ApplyPanel     .setBorder(new TitledBorder("Control"));
          
          BottomPanel    .add(BDelete);
          
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply   .addActionListener(new ApplyList());
          BDelete  .addActionListener(new DeleteList());
          
          JRPeriod .addActionListener(new JRList());
          JRNo     .addActionListener(new JRList());
     }

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    DatePanel.setBorder(new TitledBorder("Periodical"));
                    DatePanel.removeAll();
                    DatePanel.add(TStDate);
                    DatePanel.add(TEnDate);
                    DatePanel.updateUI();
                    JRNo.setSelected(false);
               }
               else
               {
                    DatePanel.setBorder(new TitledBorder("Numbered"));
                    DatePanel.removeAll();
                    DatePanel.add(TStNo);
                    DatePanel.add(TEnNo);
                    DatePanel.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    getContentPane()    . add(tabreport,BorderLayout.CENTER);
                                          setSelected(true);
                    Layer               . repaint();
                    Layer               . updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
               BDelete   . setEnabled(true);
          }
     }
     
     private class DeleteList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validStock())
               {
				if(JOptionPane.showConfirmDialog(null, "Confirm Delete the Data?", "Confirmation!!", JOptionPane.YES_NO_OPTION) == 0) 
				{
					BDelete   . setEnabled(false);
					deleteMarkedRecords();
					getAcommit();
					removeHelpFrame();
				}
               }
          }
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
               Layer.repaint();
          }
          catch(Exception ex){}
     }

     public boolean validStock()
     {
          try
          {
               Statement      stat           =  theConnection.createStatement();

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean bValue = (Boolean)RowData[i][0];
                    if(!bValue.booleanValue())
                         continue;

                    int iCount     = 0;

                    String SIndentType  = (String)VIndentType.elementAt(i);

				if(SIndentType.equals("1"))
					return false;

                    String SItemCode  = (String)VIssueCode.elementAt(i);
                    String SIUserCode = (String)VIssueUserCode.elementAt(i);
                    String SIssueDate = (String)VIssueDate.elementAt(i);
                    String SCurDate   = common.parseDate(common.getServerPureDate());
                    double dIssueQty  = common.toDouble(common.getRound((String)VIssueQty.elementAt(i),3));

                    String QS = " Select Count(*) from ItemStock Where MillCode="+iMillCode+" and HodCode="+SIUserCode+" and ItemCode='"+SItemCode+"' ";
                    
                    ResultSet result = stat.executeQuery(QS);

                    while(result.next())
                    {
                         iCount   =    result.getInt(1);
                    }
                    result.close();
                    
                    if(iCount<=0)
                    {
                         JOptionPane.showMessageDialog(null,"Problem in ItemStock - "+SItemCode+" Contact EDP ","Attention",JOptionPane.INFORMATION_MESSAGE);
                         return false;
                    }

                    int iDateDiff = common.toInt(common.getDateDiff(SCurDate,SIssueDate));

                    if(iUserCode!=16)
                    {
                         if(iDateDiff>2)
                         {
                              JOptionPane.showMessageDialog(null,"Issue Date Limit Exceed - "+SItemCode+" Contact EDP ","Attention",JOptionPane.INFORMATION_MESSAGE);
                              return false;
                         }
                    }

				if(SIndentType.equals("2"))
				{
					if(iMillCode==1)
					{
						int iDyeCount = checkDyeingItemMaster(SItemCode);

		                    if(iDyeCount<=0)
		                    {
		                         JOptionPane.showMessageDialog(null,"This Item Not Available in Dyeing","Attention",JOptionPane.INFORMATION_MESSAGE);
		                         return false;
		                    }
						else
						{
							/*
								double dAvailStock = getAvailableDyeingStock(SItemCode);

								if(dAvailStock<dIssueQty)
								{
									JOptionPane.showMessageDialog(null,"Problem in DyeingStock - "+SItemCode+" Contact EDP ","Attention",JOptionPane.INFORMATION_MESSAGE);
									return false;
								}
							*/
						}
					}
					else
					{
						return false;
					}
				}

				if(SIndentType.equals("4"))
				{
                    	String SRdcNo  = (String)VRdcNo.elementAt(i);

					int iRdcStatus = checkRDCOutStatus(SRdcNo);

	                    if(iRdcStatus > 0 && common.toInt(SRdcNo) > 0)
	                    {
	                         JOptionPane.showMessageDialog(null,"This Item Already Sent Out for JobWork","Attention",JOptionPane.INFORMATION_MESSAGE);
	                         return false;
	                    }
				}
               }
               stat.close();
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"Problem in Deletion","Attention",JOptionPane.INFORMATION_MESSAGE);
               System.out.println(ex);
               bComflag = false;
          }

          return true;
     }

     private int checkDyeingItemMaster(String SItemCode)
     {
          int iCount=0;

          try
          {
               Statement stat   =    theDConnection.createStatement();
               
               ResultSet res = stat.executeQuery("Select count(*) from InvItems Where Item_Code='"+SItemCode+"'");
               while(res.next())
               {
                    iCount   =    res.getInt(1);
               }
               res.close();
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("checkDye"+e);
          }
		return iCount;
     }

     private double getAvailableDyeingStock(String SItemCode)
     {
          double dQty=0;

          try
          {
               Statement stat   =    theDConnection.createStatement();
               
               ResultSet res = stat.executeQuery("Select nvl(CSStock,0) as Stock from InvItems Where Item_Code='"+SItemCode+"'");
               while(res.next())
               {
                    dQty   =    common.toDouble(common.getRound(res.getDouble(1),3));
               }
               res.close();
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("getAvailDyeStock"+e);
          }
		return dQty;
     }

     private int checkRDCOutStatus(String SRdcNo)
     {
          int iStatus=0;

          try
          {
               Statement stat   =    theConnection.createStatement();
               
               ResultSet res = stat.executeQuery("Select nvl(GateStatus,0) as GateStatus from RDC Where RDCNo="+SRdcNo+" and MillCode="+iMillCode);
               while(res.next())
               {
                    iStatus   =    res.getInt(1);
               }
               res.close();
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("checkRdcStatus"+e);
          }
		return iStatus;
     }

     private void deleteMarkedRecords()
     {
          String QS="";
          try
          {
               Statement      stat           =  theConnection.createStatement();

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean bValue = (Boolean)RowData[i][0];
                    if(!bValue.booleanValue())
                         continue;

                    QS = "Update NonStockIssue Set DM = 9 Where Id="+(String)VId.elementAt(i);
                    
                    try
                    {
                         if(theConnection    . getAutoCommit())
                              theConnection  . setAutoCommit(false);

                         stat.execute(QS);
                    }
                    catch(Exception ex)
                    {
                         JOptionPane.showMessageDialog(null,"Problem in Deletion","Attention",JOptionPane.INFORMATION_MESSAGE);
                         bComflag = false;
                         return;
                    }
               }

               try
               {
                    QS = "Insert Into DeletedNonStockIssue Select * From NonStockIssue Where DM=9 and MillCode="+iMillCode;

                    if(theConnection    . getAutoCommit())
                         theConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               catch(Exception ex)
               {
                    JOptionPane.showMessageDialog(null,"Problem in Deletion","Attention",JOptionPane.INFORMATION_MESSAGE);
                    ex.printStackTrace();
                    bComflag = false;
                    return;
               }

               IssueDeletedDetails();
			updateOtherData();
               updateInvitemsData();
               updateItemStockData();

               try
               {
                    QS = "Delete From NonStockIssue Where DM=9";

                    if(theConnection    . getAutoCommit())
                         theConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               catch(Exception ex)
               {
                    JOptionPane.showMessageDialog(null,"Problem in Deletion","Attention",JOptionPane.INFORMATION_MESSAGE);
                    ex.printStackTrace();
                    bComflag = false;
                    return;
               }
               stat.close();
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"Problem in Deletion","Attention",JOptionPane.INFORMATION_MESSAGE);
               System.out.println(ex);
               bComflag = false;
          }
     }

     public void IssueDeletedDetails()
     {
          VDICode        = new Vector();
          VDIName        = new Vector();
          VDIssQty       = new Vector();
          VDIssValue     = new Vector();
          VDIUserCode    = new Vector();
          VDIndentNo     = new Vector();
          VDIssueNo      = new Vector();
		VDIndentType   = new Vector();
		VDRdcNo		= new Vector();

          String    QS   = " Select NonStockIssue.Code,InvItems.Item_Name,NonStockIssue.Qty,NonStockIssue.IssueValue,NonStockIssue.MrsAuthUserCode,"+
				       " NonStockIssue.UIRefNo,NonStockIssue.IssueNo,nvl(NonStockIssue.IndentType,0) as IndentType, 0 as RdcNo "+
				       " From NonStockIssue "+
					  " Inner Join InvItems on NonStockIssue.Code = InvItems.Item_Code "+
					  " and NonStockIssue.DM=9 and NonStockIssue.MillCode="+iMillCode;

          try
          {
               Statement stat      = theConnection.createStatement();
               ResultSet result    = stat.executeQuery(QS);

               while(result.next())
               {
                    VDICode     . addElement(common.parseNull((String)result.getString(1)));
                    VDIName     . addElement(common.parseNull((String)result.getString(2)));
                    VDIssQty    . addElement(common.parseNull((String)result.getString(3)));
                    VDIssValue  . addElement(common.parseNull((String)result.getString(4)));
                    VDIUserCode . addElement(common.parseNull((String)result.getString(5)));
                    VDIndentNo  . addElement(common.parseNull((String)result.getString(6)));
                    VDIssueNo   . addElement(common.parseNull((String)result.getString(7)));
                    VDIndentType. addElement(common.parseNull((String)result.getString(8)));
                    VDRdcNo     . addElement(common.parseNull((String)result.getString(9)));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               ex.printStackTrace();
          }
     }

     private void updateOtherData()
     {
          ResultSet result = null;
		Statement dyeStat=null;
          try
          {
               Statement stat    =  theConnection.createStatement();

			if(iMillCode==1)
			{
               	dyeStat = theDConnection.createStatement();
			}

               for(int i=0;i<VDICode.size();i++)
               {     
                    String SIndentType   = common.parseNull((String)VDIndentType.elementAt(i));
                    String SItemCode     = common.parseNull((String)VDICode.elementAt(i));
                    String SItemName     = common.parseNull((String)VDIName.elementAt(i));
                    double dIssueQty     = common.toDouble(common.getRound((String)VDIssQty.elementAt(i),3));
                    double dIssueVal     = common.toDouble(common.getRound((String)VDIssValue.elementAt(i),2));
                    String SIndentNo     = common.parseNull((String)VDIndentNo.elementAt(i));
                    String SIssueNo      = common.parseNull((String)VDIssueNo.elementAt(i));
                    String SRdcNo        = common.parseNull((String)VDRdcNo.elementAt(i));

				if(SIndentType.equals("2"))
				{
					if(iMillCode == 1)
					{
		                    String QS1 = " Delete from SubStoreReceipt Where Id=(select min(id) from substorereceipt "+
								   " Where GrnNo="+SIssueNo+" and IndentSlipNo="+SIndentNo+
								   " and RawCode='"+SItemCode+"' and GrnQty="+dIssueQty+")";

						/*
							String QS2 = "";
							
							QS2 = "Update InvItems Set ";
							QS2 = QS2+" CSStock=nvl(CSStock,0)-"+dIssueQty+",";
							QS2 = QS2+" CSValue=nvl(CSValue,0)-"+dIssueVal+",";
							QS2 = QS2+" MSStock=nvl(MSStock,0)+"+dIssueQty+",";
							QS2 = QS2+" MSIssQty=nvl(MSIssQty,0)-"+dIssueQty+" ";
							QS2 = QS2+" Where Item_Code = '"+SItemCode+"'";
						*/

		                    if(theDConnection  . getAutoCommit())
		                         theDConnection  . setAutoCommit(false);

		                    dyeStat.execute(QS1);
						
						/*
							dyeStat.execute(QS2);
						*/
					}
				}

				if(SIndentType.equals("4") && common.toInt(SRdcNo) > 0)
				{
	                    String QS1 = " Delete from RDC Where Id=(select min(id) from RDC "+
							   " Where RDCNo="+SRdcNo+" and MillCode="+iMillCode+
							   " and Descript='"+SItemName+"' and Qty="+dIssueQty+")";

	                    if(theConnection  . getAutoCommit())
	                         theConnection  . setAutoCommit(false);

	                    stat.execute(QS1);

					if(iMillCode==1)
					{
						int iCount = 0;

			               result    = dyeStat.executeQuery("Select count(*) from InvItems Where Item_Code='"+SItemCode+"'");

			               while(result.next())
			               {
			                    iCount   =    result.getInt(1);
			               }
			               result.close();
			               
			               if(iCount<=0)
			                    continue;

						/*
							String QS2 = "";
							
							QS2 = "Update InvItems Set ";
							QS2 = QS2+" MSStock=nvl(MSStock,0)+"+dIssueQty+",";
							QS2 = QS2+" MSIssQty=nvl(MSIssQty,0)-"+dIssueQty+" ";
							QS2 = QS2+" Where Item_Code = '"+SItemCode+"'";

							if(theDConnection  . getAutoCommit())
								theDConnection  . setAutoCommit(false);

							dyeStat.execute(QS2);
						*/
					}
				}

				if(SIndentType.equals("0"))
				{
					if(iMillCode==1)
					{
						int iCount = 0;

			               result    = dyeStat.executeQuery("Select count(*) from InvItems Where Item_Code='"+SItemCode+"'");

			               while(result.next())
			               {
			                    iCount   =    result.getInt(1);
			               }
			               result.close();
			               
			               if(iCount<=0)
			                    continue;


						/*
							String QS2 = "";
							
							QS2 = "Update InvItems Set ";
							QS2 = QS2+" MSStock=nvl(MSStock,0)+"+dIssueQty+",";
							QS2 = QS2+" MSIssQty=nvl(MSIssQty,0)-"+dIssueQty+" ";
							QS2 = QS2+" Where Item_Code = '"+SItemCode+"'";

							if(theDConnection  . getAutoCommit())
								theDConnection  . setAutoCommit(false);

							dyeStat.execute(QS2);
						*/
					}
				}
               }
               stat.close();

			if(iMillCode==1)
			{
               	dyeStat.close();
			}
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame updateOtherData() ->"+e);
               e.printStackTrace();
               bComflag  = false;
          }
     }

     private void updateInvitemsData()
     {
          ResultSet result = null;
          try
          {
               Statement stat =  theConnection.createStatement();

               for(int i=0;i<VDICode.size();i++)
               {     
                    String SItemCode     = common.parseNull((String)VDICode.elementAt(i));
                    double dIssueQty     = common.toDouble(common.getRound((String)VDIssQty.elementAt(i),3));
                    double dIssueVal     = common.toDouble(common.getRound((String)VDIssValue.elementAt(i),2));

                    int iCount     = 0;

                    result    = stat.executeQuery("Select count(*) from "+SItemTable+" Where Item_Code='"+SItemCode+"'");

                    while(result.next())
                    {
                         iCount   =    result.getInt(1);
                    }
                    result.close();
                    
                    if(iCount==0)
                         continue;

				/*
					String QString = "";

					QString =   " Update "+SItemTable;

					dIssueQty =   (-1)*dIssueQty;
					dIssueVal =   (-1)*dIssueVal;

					QString = QString+" Set IssQty=nvl(IssQty,0)+"+dIssueQty+",IssVal=nvl(IssVal,0)+"+dIssueVal+" ";
					QString = QString+" Where Item_Code = '"+SItemCode+"'";

					if(theConnection.getAutoCommit())
						theConnection . setAutoCommit(false);

					stat.execute(QString);
				*/
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame updateInvitemsData() ->"+e);
               e.printStackTrace();
               bComflag  = false;
          }
     }

     private void updateItemStockData()
     {
          ResultSet result = null;
          try
          {
               Statement stat =  theConnection.createStatement();

               for(int i=0;i<VDICode.size();i++)
               {     
                    String SItemCode     = common.parseNull((String)VDICode.elementAt(i));
                    double dIssueQty     = common.toDouble(common.getRound((String)VDIssQty.elementAt(i),3));
                    String SAuthUserCode = common.parseNull((String)VDIUserCode.elementAt(i));
                    String SIndentNo     = common.parseNull((String)VDIndentNo.elementAt(i));

                    int iCount     = 0;

                    result    = stat.executeQuery("Select count(*) from Indent Where IndentNo="+SIndentNo+" and MillCode="+iMillCode+" and UserCode="+SAuthUserCode+" and Code='"+SItemCode+"'");

                    while(result.next())
                    {
                         iCount   =    result.getInt(1);
                    }
                    result.close();
                    

                    if(theConnection.getAutoCommit())
                         theConnection . setAutoCommit(false);

                    String QString = "";

                    if(iCount>0)
                    {
                         QString = " Update ItemStock ";
                         QString = QString+" Set Stock=nvl(Stock,0)+"+dIssueQty+",IndentQty=nvl(IndentQty,0)+"+dIssueQty+" ";
                         QString = QString+" Where MillCode="+iMillCode+" and HodCode="+SAuthUserCode+" and ItemCode = '"+SItemCode+"'";

                         String QS1 = " Update Indent set IssueStatus=0,IssueQty=nvl(IssueQty,0)-"+dIssueQty+" "+
                                      " Where IndentNo="+SIndentNo+" and MillCode="+iMillCode+" and UserCode="+SAuthUserCode+" and Code = '"+SItemCode+"'";

                         stat.execute(QString);
                         stat.execute(QS1);
                    }
                    else
                    {
                         QString = " Update ItemStock ";
                         QString = QString+" Set Stock=nvl(Stock,0)+"+dIssueQty+" ";
                         QString = QString+" Where MillCode="+iMillCode+" and HodCode="+SAuthUserCode+" and ItemCode = '"+SItemCode+"'";

                         stat.execute(QString);
                    }
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame updateItemStockData() ->"+e);
               e.printStackTrace();
               bComflag  = false;
          }
     }

     private void getAcommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnection  . commit();

                    if(iMillCode==1)
                         theDConnection . commit();

                    System         . out.println("Commit");
                    theConnection  . setAutoCommit(true);

	               if(iMillCode==1)
	                    theDConnection   . setAutoCommit(true);
               }
               else
               {
                    theConnection  . rollback();

                    if(iMillCode==1)
	                   theDConnection . rollback();

                    System         . out.println("RollBack");
                    theConnection  . setAutoCommit(true);

	               if(iMillCode==1)
	                    theDConnection   . setAutoCommit(true);
               }
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
     
     public void setDataIntoVector()
     {
          ResultSet      result    = null;

          VIssueNo       = new Vector();
          VIssueDate     = new Vector();
          VRefNo         = new Vector();
          VIssueCode     = new Vector();
          VIssueName     = new Vector();
          VIssueQty      = new Vector();
          VIssueRate     = new Vector();
          VIssueUnit     = new Vector();
          VIssueDept     = new Vector();
          VIssueCata     = new Vector();
          VId            = new Vector();
          VIssueUserCode = new Vector();
		VIndentType    = new Vector();
		VRdcNo		= new Vector();
          
          String StDate  = TStDate.toNormal();
          String EnDate  = TEnDate.toNormal();
          
          String QString = getQString(StDate,EnDate);
          try
          {
               Statement      stat          =  theConnection.createStatement();
                              result        =  stat.executeQuery(QString);

               while (result.next())
               {
                    String str1  = result.getString(1);  
                    String str2  = result.getString(2);
                    String str3  = result.getString(3);
                    String str4  = result.getString(4);
                    String str5  = result.getString(5);
                    String str6  = result.getString(6);
                    String str7  = result.getString(7);
                    String str8  = result.getString(8);
                    String str9  = result.getString(9);
                    String str10 = result.getString(10);
                    String str11 = result.getString(11);
                    String str12 = result.getString(12);
                    String str13 = result.getString(13);
                    String str14 = result.getString(14);
                    
                    VIssueNo      .addElement(str1);
                    VIssueDate    .addElement(common.parseDate(str2));
                    VRefNo        .addElement(str3);
                    VIssueCode    .addElement(str4);
                    VIssueName    .addElement(str5);
                    VIssueQty     .addElement(str6);
                    VIssueRate    .addElement(common.parseDate(str7));
                    VIssueDept    .addElement(common.parseNull(str8));
                    VIssueCata    .addElement(common.parseNull(str9));
                    VIssueUnit    .addElement(common.parseNull(str10));
                    VId           .addElement(str11);
                    VIssueUserCode.addElement(str12);
                    VIndentType.addElement(str13);
                    VRdcNo.addElement(str14);
               }
               result    .close();
               stat      .close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VIssueNo.size()][ColumnData.length];
          for(int i=0;i<VIssueNo.size();i++)
          {
               RowData[i][0]  = new Boolean(false);
               RowData[i][1]  = (String)VIssueNo  .elementAt(i);
               RowData[i][2]  = (String)VIssueDate.elementAt(i);
               RowData[i][3]  = (String)VRefNo    .elementAt(i);
               RowData[i][4]  = (String)VIssueCode.elementAt(i);
               RowData[i][5]  = (String)VIssueName.elementAt(i);
               RowData[i][6]  = (String)VIssueQty .elementAt(i);
               RowData[i][7]  = (String)VIssueRate.elementAt(i);
               RowData[i][8]  = (String)VIssueDept.elementAt(i);
               RowData[i][9]  = (String)VIssueCata.elementAt(i);
               RowData[i][10] = (String)VIssueUnit.elementAt(i);
          }
     }

     public String getQString(String StDate,String EnDate)
     {
          String QString  = "";
          
          if(JRPeriod.isSelected())
          {
               QString = " SELECT NonStockIssue.IssueNo, NonStockIssue.IssueDate, NonStockIssue.UIRefNo,"+
                         " NonStockIssue.Code, InvItems.Item_Name, NonStockIssue.Qty, NonStockIssue.IssRate,"+
                         " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, NonStockIssue.Id,"+
					" NonStockIssue.MrsAuthUserCode, nvl(NonStockIssue.IndentType,0) as IndentType, 0 as RdcNo "+
                         " FROM (((NonStockIssue "+
                         " INNER JOIN InvItems ON NonStockIssue.Code = InvItems.Item_Code) "+
                         " INNER JOIN Unit ON NonStockIssue.Unit_Code = Unit.Unit_Code) "+
                         " INNER JOIN Dept ON NonStockIssue.Dept_Code = Dept.Dept_code) "+
                         " INNER JOIN Cata ON NonStockIssue.Group_Code = Cata.Group_Code "+
                         " Where NonStockIssue.IssueDate >= '"+StDate+"' and NonStockIssue.IssueDate <='"+EnDate+"' "+
                         " And nvl(NonStockIssue.Authentication, 0) = 0 "+
                         " And NonStockIssue.MillCode="+iMillCode;
          }
          else
          {
               QString = " SELECT NonStockIssue.IssueNo, NonStockIssue.IssueDate, NonStockIssue.UIRefNo,"+
                         " NonStockIssue.Code, InvItems.Item_Name, NonStockIssue.Qty, NonStockIssue.IssRate,"+
                         " Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, NonStockIssue.Id,"+
					" NonStockIssue.MrsAuthUserCode, nvl(NonStockIssue.IndentType,0) as IndentType, 0 as RdcNo "+
                         " FROM (((NonStockIssue "+
                         " INNER JOIN InvItems ON NonStockIssue.Code = InvItems.Item_Code) "+
                         " INNER JOIN Unit ON NonStockIssue.Unit_Code = Unit.Unit_Code) "+
                         " INNER JOIN Dept ON NonStockIssue.Dept_Code = Dept.Dept_code) "+
                         " INNER JOIN Cata ON NonStockIssue.Group_Code = Cata.Group_Code "+
                         " Where NonStockIssue.UIRefNo >="+TStNo.getText()+" and NonStockIssue.UIRefNo <= "+TEnNo.getText() +
                         " And nvl(NonStockIssue.Authentication, 0) = 0 "+
                         " And NonStockIssue.MillCode="+iMillCode;
          }
          
          if(JCOrder.getSelectedIndex() == 0)
               QString = QString+" Order By NonStockIssue.IssueNo,NonStockIssue.IssueDate";
          if(JCOrder.getSelectedIndex() == 1)
               QString = QString+" Order By NonStockIssue.UIRefNo,NonStockIssue.IssueDate";
          if(JCOrder.getSelectedIndex() == 2)
               QString = QString+" Order By InvItems.Item_Name,NonStockIssue.IssueDate";
          if(JCOrder.getSelectedIndex() == 3)
               QString = QString+" Order By Dept.Dept_Name,NonStockIssue.IssueDate";
          if(JCOrder.getSelectedIndex() == 4)
               QString = QString+" Order By Cata.Group_Name,NonStockIssue.IssueDate";
          if(JCOrder.getSelectedIndex() == 5)
               QString = QString+" Order By Unit.Unit_Name,NonStockIssue.IssueDate";
          return QString;
     }

}
