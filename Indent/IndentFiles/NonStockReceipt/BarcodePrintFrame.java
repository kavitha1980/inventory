package Indent.IndentFiles.NonStockReceipt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;


//import java.io.*;
import javax.comm.*;

import javax.print.*;
import javax.print.attribute.*;
import javax.print.attribute.standard.*;
import javax.print.event.*;





public class BarcodePrintFrame extends JInternalFrame 
{

     protected     JLayeredPane Layer;
	
	private   int            iUserCode;
	private   int            iMillCode;

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BExit,BSave,BPrint;   
     Common        common = new Common();
     Connection     theConnection;
	boolean errorflag = true;
	
     int PRINT=15;
	int iPreviousStock = 0;
     
	String SItemName ;

	

     ArrayList theList = new ArrayList();

     BarcodePrintModel   theModel;

     JTable             theTable;

     
     FileWriter FW;
     File file;

        String       SPort = "COM1";

	String str = "";
		String substr = "";

     public BarcodePrintFrame(JLayeredPane Layer,int iUserCode,int iMillCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
		this.iMillCode = iMillCode;
		
		createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }

	private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();
			
               theModel       = new BarcodePrintModel();
               theTable       = new JTable(theModel);
			
		
               BExit          = new JButton("Exit");
               BSave          = new JButton("Save");
               BPrint         = new JButton("BARCODE");
			
          }
          catch(Exception e)
          {
               System.out.println(e);
			e.printStackTrace();
    }
     }

     private void setLayouts()
     {
          setTitle("Non Stock Transfer ");
          setClosable(true);                    
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,1100,600);

          TopPanel.setLayout(new GridLayout(4,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder("Info."));
          MiddlePanel.setBorder(new TitledBorder("Details"));
          BottomPanel.setBorder(new TitledBorder("Controls"));
    
		TopPanel            . setBackground(new Color(213,234,255));
		MiddlePanel         .setBackground(new Color(213,234,255));
		BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

          TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));

         
	//	theTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		theTable.getColumnModel().getColumn(0).setPreferredWidth(10);
		theTable.getColumnModel().getColumn(1).setPreferredWidth(10);
		theTable.getColumnModel().getColumn(2).setPreferredWidth(50);
		theTable.getColumnModel().getColumn(3).setPreferredWidth(30);
		theTable.getColumnModel().getColumn(4).setPreferredWidth(90);
		theTable.getColumnModel().getColumn(5).setPreferredWidth(65);
		
		theTable.getColumnModel().getColumn(6).setPreferredWidth(27);
		theTable.getColumnModel().getColumn(7).setPreferredWidth(30);
		theTable.getColumnModel().getColumn(8).setPreferredWidth(40);
		theTable.getColumnModel().getColumn(9).setPreferredWidth(40);
		theTable.getColumnModel().getColumn(10).setPreferredWidth(40);
		theTable.getColumnModel().getColumn(11).setPreferredWidth(25);
		theTable.getColumnModel().getColumn(12).setPreferredWidth(25);
		theTable.getColumnModel().getColumn(13).setPreferredWidth(50);
		theTable.getColumnModel().getColumn(14).setPreferredWidth(50);
		theTable.getColumnModel().getColumn(15).setPreferredWidth(10);



	    MiddlePanel.add(new JScrollPane(theTable));

          BottomPanel.add(BExit);
		BottomPanel.add(BSave);
		BottomPanel.add(BPrint);


          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);


          BExit.setMnemonic('E');
     }

	 private void addListeners()
     {
          BExit.addActionListener(new ActList());
		BPrint.addActionListener(new PrintList());
		BSave.addActionListener(new ActList());
		theTable.addMouseListener(new MouseList());


     }                 

	 public void setTabReport()
     {
          try
          {
             	theModel.setNumRows(0);	
                
			 ArrayList VDetails = getMaterialIndent(iMillCode);

                    for(int i=0;i<VDetails.size();i++)
               {
                    HashMap  row =(HashMap)VDetails.get(i);                         

                     Vector VTheVect = new Vector();
				  
					VTheVect.addElement(String.valueOf(i+1));
					VTheVect.addElement(common.parseNull((String)row.get("ID")));				 
					VTheVect.addElement(common.parseNull((String)row.get("MILLNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("HODCODE")));
					VTheVect.addElement(common.parseNull((String)row.get("ITEM_NAME")));
					VTheVect.addElement(common.parseNull((String)row.get("ITEM_CODE")));

					VTheVect.addElement(common.parseNull((String)row.get("STOCK")));
					VTheVect.addElement(common.parseNull((String)row.get("S.VALUE")));
					VTheVect.addElement(common.parseNull((String)row.get("CATL")));
					VTheVect.addElement(common.parseNull((String)row.get("DRAW")));
					VTheVect.addElement(common.parseNull((String)row.get("LOCNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("S.STOCK")));
					VTheVect.addElement(common.parseNull((String)row.get("T.STOCK")));

					VTheVect.addElement(common.parseNull((String)row.get("USERNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("ENTRYDATE")));

					VTheVect.addElement(new Boolean(false));
			
                     theModel.appendRow(VTheVect);
               }
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               
			
			
			if(ae.getSource()==BSave)
			{
				if(JOptionPane.showConfirmDialog(null, "Are you sure you want to Save the Details?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
				{ 		
                         validations();
					showMessage(getACommit());
					theModel.setNumRows(0);
					setTabReport();
				}
			}
					
			if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
			
			
		}
     }

	private class MouseList extends MouseAdapter
	{
		public void mouseClicked(MouseEvent me)
		{
		}
	}
	
	private class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               System.out.println("comming before do print check");

               doPrint();

               System.out.println("comming after do print check");

          }
     }


 private void showMessage(String smsg)
     {
          JOptionPane.showMessageDialog(null,getMessage(smsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage(String smsg)
     {
          String str = "<html><body>";
          str = str + smsg+ "<br>";
          str = str + "</body></html>";
          return str;
     }
	
	private void validations()
	{
			try
			{
				for(int i=0; i<theModel.getRowCount(); i++)
				{
					Boolean bValue = (Boolean)theModel.getValueAt(i, 15);
					if(bValue.booleanValue())
					{	
						int iCount=0;
						//int iPreviousStock = 0; // Stock already available with user NON-STOCK  and division MILL 
						String SItemCode   = common.parseNull((String)theModel.getValueAt(i, 5));				

						String sId           = (String)theModel.getValueAt(i, 1);
						String SItemCodee  = common.parseNull((String)theModel.getValueAt(i, 5));				
						int iHODCode  = 6125;
						int iTransferStock  = common.toInt((String)theModel.getValueAt(i,6));
					
						String SItem   = common.parseNull((String)theModel.getValueAt(i, 5));	

						int iStock  = common.toInt((String)theModel.getValueAt(i,6));
						int iIdd           = common.toInt((String)theModel.getValueAt(i, 1)); 
						
						String SLocationName  = common.parseNull((String)theModel.getValueAt(i, 10));
		
						iCount = alredyInserted(SItemCodee);
						
						System.out.println("iCount-->"+iCount);
						
						if(iCount>0){
							
							insertItemStockHistory(SItemCodee,iHODCode);
							double dTotalStock = iPreviousStock + iTransferStock;

							updateItemStock(SItemCodee,iHODCode,dTotalStock,sId);
							updateStatus(SItemCodee,sId,iHODCode);
							updateLocation(SItemCodee,SLocationName);
						}
						else
						{
							insertItemstock(SItemCodee,iHODCode,iTransferStock,sId);
							updateStatus(SItemCodee,sId,iHODCode);
							updateLocation(SItemCodee,SLocationName);
						}
					}
				}
			}
			catch(Exception e){
				e.printStackTrace(); 
			}
	}
	private int alredyInserted(String SItemCode)
	{
		int iCount = 0;
		iPreviousStock = 0;

		StringBuffer sb = new StringBuffer();
		sb.append(" Select Count(1),sum(stock) from ItemStock ");
		sb.append(" where ItemCode ='"+SItemCode+"' and HODCode = 6125 and MillCode = "+iMillCode );	
		
		try{

			if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
               }

			PreparedStatement ps     = theConnection.prepareStatement(sb.toString());
			ResultSet	result1	 	= ps.executeQuery();
			while(result1.next())
			{ 
				iCount = result1.getInt(1);
				iPreviousStock = result1.getInt(2);		
			}
			result1 . close();
			ps    . close();
		}
		catch(Exception ex){
			errorflag = false;
			ex.printStackTrace();
		}
		return iCount;
	}
	private void insertItemStockHistory (String SItemCode,int iHODCode)
	{
		try{
			if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
               }
			theConnection.setAutoCommit(false);
			String QS15 = "Insert into ItemStock_History (select MillCode, HODCode, ItemCode, Stock, StockValue,IndentQty,TransferQty,ReservedQty,ItemStockTransferId,Id,Sysdate, ItemStock_History_Seq.nextVal from ItemStock Where ItemCode = '"+SItemCode+"' and HODCode = "+iHODCode+" and MillCode = "+iMillCode+" ) ";
			PreparedStatement ps16 = theConnection.prepareStatement(QS15);
			ps16.executeQuery();	
			ps16.close();
		}
		catch(Exception ex){
			errorflag = false;
			ex.printStackTrace();
		}
	}
	private void insertItemstock(String SItemCode,int iHODCode,int iTransferStock,String sId)
	{
		try{
			String QS4 = " Insert into Itemstock(MillCode,HODCode,ItemCode,Stock,StockValue,IndentQty,TransferQty,ReservedQty,ItemStockTransferId,Id,EntryDateTime ) " +
					   "	Values( "+iMillCode+","+iHODCode+",'"+SItemCode+"',"+iTransferStock+", "+
					   " 0,0,0,0,"+sId+",ItemStock_seq.nextVal,SysDate  )";

				if(theConnection ==null) {

					ORAConnection jdbc   = ORAConnection.getORAConnection();
					theConnection        = jdbc.getConnection();
				}
				theConnection.setAutoCommit(false);
				
				PreparedStatement ps6 = theConnection.prepareStatement(QS4);
				ps6.executeQuery();
				ps6.close();
		}
		catch(Exception ex){
			errorflag = false;
			ex.printStackTrace();
		}
	}
	private void updateItemStock(String SItemCode,int iHODCode,double dTotalStock,String sId){

		try{

			String QS3 = "Update ItemStock set Stock = "+dTotalStock+", ITEMSTOCKTRANSFERID="+sId+",ENTRYDATETIME = SysDate Where ItemCode = '"+SItemCode+"' and HODCode = "+iHODCode+" and MillCode = "+iMillCode+" ";

			if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
               }

			theConnection.setAutoCommit(false);
			PreparedStatement ps1 = theConnection.prepareStatement(QS3);
			ps1.executeQuery();	
			ps1.close();
		}
		catch(Exception ex){
			errorflag = false;
			ex.printStackTrace();
		}
	}
	private void updateStatus(String SItemCode,String SId,int iHODCode)
	{
		try
		{
		     if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
               }
				theConnection.setAutoCommit(false);
				String QS = " Update ItemStock_Transfer	set TranferStatus = 1 , TransferDate = SysDate Where ItemCode = '"+SItemCode+"' and Id = "+SId+" and MillCode = "+iMillCode+" and HODCode = "+iHODCode+" ";

				PreparedStatement ps = theConnection.prepareStatement(QS);
				ps.executeUpdate();	
				ps.close();
		}
		catch(Exception ex)
		{
			errorflag = false;
			ex.printStackTrace();		
		}
	}
	private void updateLocation(String SItemCode,String sLocName)
	{
		try
		{
		     if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
               }
				theConnection.setAutoCommit(false);
				String QS = " Update InvItems set LOCNAME = '"+sLocName+"' where ITEM_CODE = '"+SItemCode+"' ";

				PreparedStatement ps = theConnection.prepareStatement(QS);
				ps.executeUpdate();	
				ps.close();
		}
		catch(Exception ex)
		{
			errorflag = false;
			ex.printStackTrace();		
		}
	}

  private void removeHelpFrame()
  {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
  }
  public ArrayList getMaterialIndent( int iMillCode)
  {
          theList = new ArrayList();
		StringBuffer sb = new StringBuffer();

				sb.append(" select ItemStock_Transfer.Id,Mill.MillName,ItemStock_Transfer.HodCode,InvItems.Item_Name,InvItems.Item_Code,ItemStock_Transfer.Stock,ItemStock_Transfer.StockValue, ");
				sb.append(" InvItems.Catl,InvItems.Draw,InvItems.LocName, ");
				sb.append(" rawuser.UserName,to_char(ItemStock_Transfer.EntryDateTime,'dd.mm.yyyy') as EntryDate from ItemStock_Transfer ");
				sb.append(" Inner join Mill on Mill.MillCode = ItemStock_Transfer.MillCode ");
				sb.append(" Inner join InvItems on InvItems.Item_Code = ItemStock_Transfer.ItemCode");
				sb.append(" Inner join rawuser on rawuser.UserCode = ItemStock_Transfer.EntryUserCode");
				sb.append(" Where ItemStock_Transfer.TranferStatus = 0 and Mill.MillCode = "+iMillCode+"  ");
				sb.append(" group by ItemStock_Transfer.Id,Mill.MillName,ItemStock_Transfer.HodCode,InvItems.Item_Name,InvItems.Item_Code, ");
				sb.append(" ItemStock_Transfer.Stock,ItemStock_Transfer.StockValue,");
				sb.append(" InvItems.Catl,InvItems.Draw,InvItems.LocName,rawuser.UserName,to_char(ItemStock_Transfer.EntryDateTime,'dd.mm.yyyy') ");
				sb.append(" Order by Id ");
          try
          {

               Class                   . forName("oracle.jdbc.OracleDriver");
		     Connection theConnection           = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "Inventory", "Stores");
			
			Statement theStatement     = theConnection.createStatement();
               ResultSet  result          = theStatement.executeQuery(sb.toString());
               ResultSetMetaData rsmd    = result.getMetaData();

               while(result.next())
               {
                    HashMap row = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }                         
                    theList.add(row);
               }
               result.close();
              theStatement.close();
		    theConnection.close();
		}
          catch(Exception ex)
          {
			ex.printStackTrace();
		}

          return theList;

     }
	public String getACommit()
     {
          String sReturn = "";
		
		System.out.println("In getACommit");
		System.out.println("errorflag-->"+errorflag);
		
          try
          {
               if(errorflag)
               {
                    theConnection    . commit();
                    sReturn = " Data Saved... ";
               }
               else
               {
                    theConnection    . rollback();
                    sReturn = "Error In Inserting ..";
               }
               theConnection    . setAutoCommit(true);
          }catch(Exception ex)
          {
               ex.printStackTrace();
               sReturn = "Error In Inserting ..";
          }
          return sReturn;
     }


	// From this it is for only barcode print

	 private void doPrint()
     {
          try
          {
			
			  
               SerialPort serialPort    = getSerialPort();
               serialPort               . setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
               OutputStream output      = serialPort.getOutputStream();
			  
			   
			
			
			   System.out.println("commin final "+serialPort);
			
			for(int i=0; i<theModel.getRowCount(); i++)
				{
					Boolean bValue = (Boolean)theModel.getValueAt(i, 15);
					if(bValue.booleanValue())
					{	
						int iCount=0;

						String SItemCod   = common.parseNull((String)theModel.getValueAt(i, 5));				
						String SItemNamee = common.parseNull((String)theModel.getValueAt(i, 4));
						String SLoc = common.parseNull((String)theModel.getValueAt(i, 10));
						String SCatl = common.parseNull((String)theModel.getValueAt(i, 8));
						String SDraw = common.parseNull((String)theModel.getValueAt(i, 9));
						
						String SLocDraw = SCatl  +"  "+  SDraw;
						
               
					//	String SLocDraw = SCatl  +"      "+  SDraw;
					//	String SLocQty = SLoc  +"      "+  SQty;
						
						String str = "";
						String substr = "";
						String substr2 = "";
						
						 substr = SItemNamee.substring(0, 28);
						 substr2 = SItemNamee.substring(28,SItemNamee.length());

	
				String Str2 = "191100601600020"+substr+"\r";     //191100501400070    191100300800070
				String Str4 = "191100601400055"+substr2+"\r";     //191100501400070    191100300800070
				String Str3 = "191100601100020"+SLocDraw+"\r";
                    String Str5 = "191100600800020"+SLoc+"\r";
                    String Str7 = "1e6205000200090"+SItemCod+"\r";
				output.write("n".getBytes());
                    output.write("f285".getBytes());
                    output.write("L".getBytes());
                    output.write("H10".getBytes());
                    output.write("D11".getBytes());
                    output.write(Str3.getBytes());
                    output.write(Str4.getBytes());
                    output.write(Str5.getBytes());
                    output.write(Str7.getBytes());
				output.write(Str2.getBytes()); 
				
				
                    String str1 = "191100501400080Amarjothi Spinning Mills Ltd\r";
                    String xtr1 = "1e6205000900090GOKUL\r";
                    String xtr2 = "191100500600080Bale Id:11111111\r";
                    String str2 = "191100400400080"+SItemNamee+"\r";
                    String str3 = "191100300200080Date: "+SItemNamee+"\r";
                    String str4 = "191100400400080"+common.parseNull(SItemCod)+"\r";

                     





                    


                    output.write("n".getBytes());
                    output.write("f285".getBytes());
                    output.write("L".getBytes());
                    output.write("H10".getBytes());
                    output.write("D11".getBytes());

                    output.write(str4.getBytes());
                    output.write(str3.getBytes());
                    output.write(xtr2.getBytes());
                    output.write(xtr1.getBytes());
                    output.write(str1.getBytes());
                    output.write("E\r".getBytes());
            
			   
			   
							  
							  			   System.out.println("commin all print final");

					}  // this is for loop
					}  // this is for loop
					
								                  serialPort.close();
 
		  }catch(Exception ex)	    {
			  ex.printStackTrace();
		  }
	 }	  

     private SerialPort getSerialPort() 
     {
          SerialPort serialPort         = null;
     
          try
          {
               Enumeration portList     = CommPortIdentifier.getPortIdentifiers();
     
               while(portList.hasMoreElements())
               {
                    CommPortIdentifier portId = (CommPortIdentifier)portList.nextElement();

                    if(portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort     = (SerialPort)portId.open("comapp", 2000);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               
          }
     
          return serialPort;
     }






















}