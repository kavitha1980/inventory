package Indent.IndentFiles.NonStockReceipt;


import javax.swing.table.*;
import java.util.*;

public class BarcodePrintModel extends DefaultTableModel
{

     String ColumnName[]   = {"SL.NO","ID","MILLNAME","HODCODE","ITEMNAME","ITEMCODE","STOCK","S.VALUE","CATL","DRAW","LOCNAME","S.STOCK","T.STOCK","USERNAME","ENTRYDATE","CLICK"};
     String ColumnType[]   = {"S","S","S","S","S","S","S","S","S","S","E","S","S","S","S","E"};
     int  iColumnWidth[] = {10,10,30,40,30,30,30,30,30,30,30,30,30,30,30,5};


     public BarcodePrintModel()
     {
          setDataVector(getRowData(),ColumnName);
     }

     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";
		
     			
          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}
