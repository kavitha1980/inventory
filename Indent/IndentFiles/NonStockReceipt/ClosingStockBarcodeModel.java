package Indent.IndentFiles.NonStockReceipt;


import javax.swing.table.*;
import java.util.*;

public class ClosingStockBarcodeModel extends DefaultTableModel
{

     String ColumnName[]   = {"SL.NO","DEPARTMENT","ITEMCODE","ITEMNAME","UOM","BINNAME","QUANTITY","VALUE","RATE/UNIT","NONSTOCK","TOATLSTOCK","COUNT","CLICK"};
     String ColumnType[]   = {"S","S","S","S","E","E","E","E","E","E","E","E","E"};
     int  iColumnWidth[] = {10,10,30,40,30,30,30,30,30,30,30,10,5};


     public ClosingStockBarcodeModel()
     {
          setDataVector(getRowData(),ColumnName);
     }

     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";
		
     			
          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}
