package Indent.IndentFiles.NonStockReceipt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;



//import java.io.*;
import javax.comm.*;

import javax.print.*;
import javax.print.attribute.*;
import javax.print.attribute.standard.*;
import javax.print.event.*;





public class ClosingStockBarcodePrint extends JInternalFrame 
{

		protected     JLayeredPane Layer;

		private   int            iUserCode;
		private   int            iMillCode;

		JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
		JTabbedPane   thePane;
		JButton       BExit,BPrint,BApply;  
		JTextField    searchField;		
		JComboBox     CStockGroup;
		Common        common = new Common();
		Connection     theConnection;
		boolean errorflag = true;

		int PRINT=15;
		int iPreviousStock = 0;

		String SItemName,SDraw,SCatl ,SIName,SICod;

		ArrayList theList = new ArrayList();

		ClosingStockBarcodeModel   theModel;

		JTable             theTable;

		DateField      TStDate;
		DateField      TEnDate;
		

		String SDept = "";
		String SCode = "";
		String SName = "";
		double dSStock = 0;
		double dSValue = 0;
		double dRRate = 0;
		double dNNonStock = 0;
		double dToStock = 0;
		String SUom = "";
		String SLoc = "";	

		String       SPort = "COM1";

		String str = "";
		String substr = "";
		
		Vector V1,V2,V3,V4,V5,V6,V7,V8,V9,V10,V11,V12,V13;
		Vector VGroupName,VGroupCode,VCatl,VDraw,VItemCode,VItemName;
		
		String SItemTable;
		String SMillName;
		String SMillCode;

     public ClosingStockBarcodePrint(JLayeredPane Layer,int iUserCode,int iMillCode,String SItemTable)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
		this.iMillCode = iMillCode;
		this.SItemTable = SItemTable;
		
		setData3();
		setData2();
	//	setVector();
		createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }

	private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();
			
               theModel       = new ClosingStockBarcodeModel();
               theTable       = new JTable(theModel);
			
			TStDate      = new DateField();
			TEnDate        = new DateField();
			TStDate.setTodayDate();
			TEnDate.setTodayDate();
			
			 searchField    = new JTextField();
			
			CStockGroup       = new JComboBox(new Vector(VGroupName));
			
			
			BApply        = new JButton (" Click Me ");
               BExit          = new JButton("Exit");
               BPrint         = new JButton("BARCODE");
			
          }
          catch(Exception e)
          {
               System.out.println(e);
			e.printStackTrace();
    }
     }

     private void setLayouts()
     {
          setTitle(" Closing Stock Barcode Print Frame ");
          setClosable(true);                    
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,1100,600);

          TopPanel.setLayout(new GridLayout(3,6));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder("Info."));
          MiddlePanel.setBorder(new TitledBorder("Details"));
          BottomPanel.setBorder(new TitledBorder("Controls"));
    
		TopPanel            . setBackground(new Color(213,234,255));
		MiddlePanel         .setBackground(new Color(213,234,255));
		BottomPanel         . setBackground(new Color(213,234,255));
    }
    
    private void setData2()
	{
		VGroupName			= null;
		VGroupCode			= null;
		
		
		
		VGroupName			= new Vector();
		VGroupCode			= new Vector();	
			
		VGroupName			. clear();
		VGroupCode			. clear();
		
		VGroupName			. add("All");
		VGroupCode			. add("999999");
		
		
		try
		{
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
				Statement theStatement = theConnection.createStatement();
			
				String QSS = " Select GroupCode,GroupName From StockGroup Order By 2 ";
				
			
			
				ResultSet re = theStatement.executeQuery(QSS);

			while (re.next())
			{
				VGroupName		. add(common.parseNull(re.getString(2)));
				VGroupCode		. add(common.parseNull(re.getString(1)));
				
			}
				re.close();
			
				theStatement 		.close();
			
		}
			catch(Exception ex)
			{
				System.out.println("From StockGroup "+ex);
			}
		
		
	}

	  private void setData3()
	{
		VCatl			= null;
		VDraw			= null;
		VItemCode = null;
		VItemName = null;


		VCatl			= new Vector();
		VDraw		= new Vector();
		VItemCode		= new Vector();
		VItemName		= new Vector();


		VCatl			. clear();
		VDraw		. clear();
		VItemCode		. clear();
		VItemName		. clear();

		
		
		
		
		try
		{
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
				Statement theStatement = theConnection.createStatement();
			
				String QSS = " select item_code,item_name,draw,catl from invitems order by 1 ";
				
			
			
				ResultSet re = theStatement.executeQuery(QSS);

			while (re.next())
			{
				VCatl		. add(common.parseNull(re.getString(4)));
				VDraw		. add(common.parseNull(re.getString(3)));
				VItemCode		. add(common.parseNull(re.getString(1)));
				VItemName		. add(common.parseNull(re.getString(2)));
			}
				re.close();
			
				theStatement 		.close();
			
		}
			catch(Exception ex)
			{
				System.out.println("From invitems "+ex);
			}
		
		
	}



     private void addComponents()
     {
		
		
		TopPanel            . add(new JLabel(""));
		TopPanel            . add(new JLabel("  As On "));
	     TopPanel            . add(TEnDate);
		TopPanel            . add(new JLabel("StockGroup"));
	     TopPanel            . add(CStockGroup);
		TopPanel            . add(new JLabel(""));
		
		TopPanel            . add(new JLabel(""));
		TopPanel            . add(new JLabel(""));
	     TopPanel			.add(new JLabel(""));
		TopPanel            . add(new JLabel(""));
	     TopPanel			.add(new JLabel(""));
		TopPanel            . add(new JLabel(""));
		
		TopPanel            . add(new JLabel(""));
		TopPanel            . add(new JLabel(""));
		TopPanel            . add(searchField);
		TopPanel            . add(new JLabel(""));
		TopPanel            . add(BApply);
		TopPanel            . add(new JLabel(""));
	   
	     		MiddlePanel.add(new JScrollPane(theTable));

         

          BottomPanel.add(BExit);
		BottomPanel.add(BPrint);


          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);


          BExit.setMnemonic('E');
     }

	 private void addListeners()
     {
          BExit.addActionListener(new ActList());
		BPrint.addActionListener(new PrintList());
		theTable.addMouseListener(new MouseList());
		BApply.addActionListener(new ActList());
		searchField. addKeyListener(new SearchKeyList());



     }                 

	 public void setTabReport()
     {
          try
          {
             	theModel.setNumRows(0);	
			
			  String SStDate = common.pureDate((String)TStDate.toNormal()); 
                String SEnDate = common.pureDate((String)TEnDate.toNormal());
			String SGrnFilter = "";
			String SIssueFilter = "";

                
			 ArrayList theList = setDetails( SEnDate, SGrnFilter, SIssueFilter, iMillCode);

             
		
				 
				
				
				
		/*		String SDept = common.parseNull((String)V1.elementAt(i));
					 String SCode = common.parseNull((String)V2.elementAt(i));
					 String SName = common.parseNull((String)V3.elementAt(i));
					 double dSStock = common.toDouble((String)V5.elementAt(i));
					 double dSValue = common.toDouble((String)V6.elementAt(i));   
					 double dRRate = common.toDouble((String)V7.elementAt(i));
					 double dNonnStock =  common.toDouble((String)V11.elementAt(i));
					 double dToStock = common.toDouble((String)V12.elementAt(i));
					 String SUom = common.parseNull((String)V4.elementAt(i));
					 String SLoc = common.parseNull((String)V10.elementAt(i));
				*/
				
			for(int i=0;i<V1.size();i++)
               {

                     Vector VTheVect = new Vector();
				 
				 

				VTheVect.addElement(String.valueOf(i+1));
				VTheVect.addElement(common.parseNull((String)V1.elementAt(i)));
				VTheVect.addElement(common.parseNull((String)V2.elementAt(i)));
				VTheVect.addElement(common.parseNull((String)V3.elementAt(i)));
				VTheVect.addElement(common.parseNull((String)V4.elementAt(i)));
				VTheVect.addElement(common.parseNull((String)V8.elementAt(i)));
				VTheVect.addElement(common.parseNull((String)V5.elementAt(i)));
				VTheVect.addElement(common.parseNull((String)V6.elementAt(i)));
				VTheVect.addElement(common.parseNull((String)V7.elementAt(i)));
				VTheVect.addElement(common.parseNull((String)V11.elementAt(i)));
				VTheVect.addElement(common.parseNull((String)V12.elementAt(i)));
				VTheVect.addElement(String.valueOf(""));
				VTheVect.addElement(new Boolean(false));
				 theModel.appendRow(VTheVect);           // 9095411244
				 
			}
				 
				 
		  } 
          
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               
			
			
			if(ae.getSource()==BApply)
			{
				
					theModel.setNumRows(0);
					setTabReport();
				
			} 
					
			if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
			
			
		}
     } 

	private class MouseList extends MouseAdapter
	{
		public void mouseClicked(MouseEvent me)
		{
		}
	}
	
	private class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               System.out.println("comming before do print check");

               doPrint();

               System.out.println("comming after do print check");

          }
     }


	 private class SearchKeyList extends KeyAdapter
        {
            public void keyReleased(KeyEvent ke)
            {
                searchItems();     
            }

            public void keyPressed(KeyEvent ke)
            {
                searchItems();
            }    

            public void keyTyped(KeyEvent ke)
            {
                searchItems();
            }
        }

 private void showMessage(String smsg)
     {
          JOptionPane.showMessageDialog(null,getMessage(smsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage(String smsg)
     {
          String str = "<html><body>";
          str = str + smsg+ "<br>";
          str = str + "</body></html>";
          return str;
     }
	

  private void removeHelpFrame()
  {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
  }
  
   public ArrayList setDetails(String SEnDate,String SGrnFilter,String SIssueFilter,int iMillCode)
     {
          
		theList = new ArrayList();
             
		 V1  = new Vector();
            V2  = new Vector();
            V3  = new Vector();
            V4  = new Vector();
            V5  = new Vector();
            V6  = new Vector();
            V7  = new Vector();
            V8  = new Vector();
            V9  = new Vector();
            V10 = new Vector();
            V11 = new Vector();
            V12 = new Vector();


		   SEnDate = common.pureDate((String)TEnDate.toNormal());
			 
			//String  SFilter = "",SGrnFilter="",SIssueFilter="";
			  
			   if(iMillCode!=99)
            {
                if(iMillCode==0)
                {
                    //SFilter        = " Where (StockGroup.MillCode is Null OR StockGroup.MillCode="+common.toInt(SMillCode)+")";
                    SGrnFilter     = " And (Grn.MillCode is Null OR GRN.MillCode = "+iMillCode+") ";
                    SIssueFilter   = " And (Issue.MillCode is Null OR Issue.MillCode = "+iMillCode+")";
                }
                else
                {
                    //SFilter  =    " Where StockGroup.MillCode="+common.toInt(SMillCode) ;
                    SGrnFilter     = " And  GRN.MillCode = "+iMillCode;
                    SIssueFilter   = " And  Issue.MillCode = "+iMillCode;
                }
            }
			 
		
		  
		 String QS1=   " SELECT InvItems.Item_Code as Code, InvItems.Item_Name, "+
					" Sum(nvl(InvItems.OpgQty,0)) as OpgQty, Sum(nvl(InvItems.OpgVal,0)) as OpgVal,  "+
					" Uom.UomName, StockGroup.GroupName,InvItems.LocName,   "+
					"  InvItems.StkGroupCode,InvItems.ExClass  FROM ((InvItems  "+
					" Inner Join StockGroup on  InvItems.StkGroupCode=StockGroup.GroupCode and InvItems.hsntype=0  )  "+
					" Inner Join Uom on InvItems.UomCode=Uom.UomCode )   "+
					" Group by InvItems.Item_Code, InvItems.Item_Name,  Uom.UomName,  "+
					" StockGroup.GroupName,  invitems.locname,InvItems.StkGroupCode,InvItems.ExClass  ";

           
            String QS2 = " Select Code,sum(GrnQty) as GrnQty,sum(GrnVal) as GrnVal from "+
                         " (SELECT GRN.Code, Sum(GRN.GrnQty) AS GrnQty, "+
                         " Sum(GRN.GrnValue) as GrnVal "+
                         " FROM GRN "+
                         " WHERE Grn.RejFlag=0 and GRN.GrnDate <='"+SEnDate+"' "+SGrnFilter+" "+
                         " GROUP BY GRN.Code "+
                         " Union All "+
                         " SELECT GRN.Code, Sum(GRN.GrnQty) AS GrnQty, "+
                         " Sum(GRN.GrnValue) as GrnVal "+
                         " FROM GRN "+
                         " WHERE Grn.RejFlag=1 and GRN.RejDate <='"+SEnDate+"' "+SGrnFilter+" "+
                         " GROUP BY GRN.Code) "+
                         " Group by Code ";

            String QS2a =" Select Code,sum(GrnQty) as GrnQty,sum(GrnVal) as GrnVal from "+
                         " (SELECT GRN.Code, Sum(GRN.GrnQty) AS GrnQty, Sum(GRN.GrnValue) as GrnVal "+
                         " FROM GRN "+
                         " WHERE Grn.RejFlag=0 and (GrnType<>2 and GrnBlock>1) And GRN.GrnDate <='"+SEnDate+"' "+SGrnFilter+" "+
                         " GROUP BY GRN.Code"+
                         " Union All "+
                         " SELECT GRN.Code, Sum(GRN.GrnQty) AS GrnQty, Sum(GRN.GrnValue) as GrnVal "+
                         " FROM GRN "+
                         " WHERE Grn.RejFlag=1 and (GrnType<>2 and GrnBlock>1) And GRN.RejDate <='"+SEnDate+"' "+SGrnFilter+" "+
                         " GROUP BY GRN.Code) "+
                         " Group by Code ";

            String QS3 = " SELECT Issue.Code, Sum(Issue.Qty) AS IssQty, "+
                         " Sum(Issue.IssueValue) as IssVal "+
                         " FROM Issue "+
                         " WHERE Issue.IssueDate <='"+SEnDate+"' "+SIssueFilter+" "+
                         " GROUP BY Issue.Code ";

            String QS4 = " Select Code,Sum(Stock+IssQty) as Stock from "+
                         " (Select ItemCode as Code,Sum(Stock) as Stock,0 as IssQty "+
                         " From ItemStock "+
                         " Where HodCode=6125 and MillCode="+iMillCode +
                         " Group by ItemCode "+
                         " Union All "+
                         " Select Code,0 as Stock,Sum(Qty) as IssQty "+
                         " From NonStockIssue "+
                         " Where MillCode="+iMillCode+" and IssueDate>'"+SEnDate+"'"+
                         " Group by Code) "+
                         " Group by Code having Sum(Stock+IssQty)>0 ";

            String QS6 = " SELECT c1.GroupName, c1.code, c1.Item_Name, "+
                         " c1.UOMName, c1.OpgQty, c1.OpgVal, "+
                         " c2.GrnQty, c2.GrnVal, c3.IssQty, "+
                         " c3.IssVal, c2a.GrnQty, c2a.GrnVal, "+
                         " c1.LocName, c1.StkGroupCode,c1.ExClass,c4.Stock "+
                         " From ("+QS1+") c1 "+
                         " Left JOIN ("+QS2+") c2 ON c1.code = c2.Code "+
                         " Left JOIN ("+QS2a+") c2a ON c1.code = c2a.code "+
                         " Left JOIN ("+QS3+") c3 ON c1.code = c3.code "+
                         " Left JOIN ("+QS4+") c4 ON c1.code = c4.code"+
                         " Order by 1,3 ";

            try
            {
                     if(theConnection==null)
                     {
                          ORAConnection jdbc = ORAConnection.getORAConnection();
                          theConnection       = jdbc.getConnection();
                     }
                     Statement stat           = theConnection.createStatement();

                     ResultSet res = stat.executeQuery(QS6);
				  ResultSetMetaData rsmd     = res.getMetaData();
                     double dTGsk=0;                           
                     while(res.next())
                     {
						
						
						
                          double dOpg   = common.toDouble(res.getString(5));
                          double dOVal  = common.toDouble(res.getString(6));
                          double dRec   = common.toDouble(res.getString(7));
                          double dRVal  = common.toDouble(res.getString(8));
                          double dIss   = common.toDouble(res.getString(9))+common.toDouble(res.getString(11));
                          double dIVal  = common.toDouble(res.getString(10))+common.toDouble(res.getString(12));
                          double dStock = dOpg+dRec-dIss;

                          double dValue = dOVal+dRVal-dIVal;
                          double dRate  = dValue / dStock;

                          double dNonStock = common.toDouble(res.getString(16));
                          double dTStock   = dStock+dNonStock;

                          if(common.getRound(dTStock,3).equals("0.000") && common.getRound(dValue,2).equals("0.00"))
                               continue;

                         

                          V1.addElement(res.getString(1));
                          V2.addElement(res.getString(2));
                          V3.addElement(res.getString(3));
                          V4.addElement(res.getString(4));
                          V5.addElement(common.getRound(dStock,3));
                          V6.addElement(common.getRound(dValue,2));
                          V7.addElement(common.getRound(dRate,3));
                          V8.addElement(common.parseNull(res.getString(13)));
                          V9.addElement(common.parseNull(res.getString(14)));
                          V10.addElement(res.getString(15));
                          V11.addElement(common.getRound(dNonStock,3));
                          V12.addElement(common.getRound(dTStock,3));
                          dTGsk = dTGsk+dValue;
					 
				 
				 
			/*	   for(int i=0;i<V1.size();i++)
                     {
				 
					String SDept = common.parseNull((String)V1.elementAt(i));
					 String SCode = common.parseNull((String)V2.elementAt(i));
					 String SName = common.parseNull((String)V3.elementAt(i));
					 double dSStock = common.toDouble((String)V5.elementAt(i));
					 double dSValue = common.toDouble((String)V6.elementAt(i));   
					 double dRRate = common.toDouble((String)V7.elementAt(i));
					 double dNonnStock =  common.toDouble((String)V11.elementAt(i));
					 double dToStock = common.toDouble((String)V12.elementAt(i));
					 String SUom = common.parseNull((String)V4.elementAt(i));
					 String SLoc = common.parseNull((String)V10.elementAt(i));
				 
				 }
				 */
				 // for(int i=0;i<theList.size();i++)
                   //  {
					 
					
                          
					 
					
					
                  //   }
				 
				 
				 }
                     res.close();
				 

                     double dTotVal=0;
                     int iUpCount=0;
                     int iTotCount=0;
                     for(int i=0;i<V1.size();i++)
                     {
					double dStock = common.toDouble((String)V5.elementAt(i));
					
					
					if(dStock>0 || dStock<0)
					continue;


					double dValue = common.toDouble((String)V6.elementAt(i));

					if(dValue>0 || dValue<0)
					{
						iTotCount++;
						dTotVal = dTotVal + dValue;

						String SCode = (String)V2.elementAt(i);
					}
				 
				  
				 
				 }
				
				 
					System.out.println("Location===>"+SLoc);                    

                     stat.close();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
                  ex.printStackTrace();
            }
     		 return theList;

	}
	
	private void searchItems()
     {
          String SSearchStr        = common.parseNull(searchField.getText()).trim();

          int iIndex               = -1;

          for(int i=0; i<theModel.getRowCount(); i++)
          {
               String SItemName    = common.parseNull((String)theModel.getValueAt(i, 3));

               if(SItemName.startsWith(SSearchStr.toUpperCase()))
               {
                    iIndex         = i;
                    break;
               }
          }
          if(iIndex != -1)
          {
                 theTable       . setRowSelectionInterval(iIndex, iIndex);
                 Rectangle theRect   = theTable.getCellRect(iIndex, 1, true);
                 theTable       . scrollRectToVisible(theRect);
                 theTable       . setSelectionBackground(Color.red);			   
          }
     }
 
	public String getACommit()
     {
          String sReturn = "";
		
		System.out.println("In getACommit");
		System.out.println("errorflag-->"+errorflag);
		
          try
          {
               if(errorflag)
               {
                    theConnection    . commit();
                    sReturn = " Data Saved... ";
               }
               else
               {
                    theConnection    . rollback();
                    sReturn = "Error In Inserting ..";
               }
               theConnection    . setAutoCommit(true);
          }catch(Exception ex)
          {
               ex.printStackTrace();
               sReturn = "Error In Inserting ..";
          }
          return sReturn;
     }



	private void doPrint()
	{
		try
		{


			SerialPort serialPort    = getSerialPort();
			serialPort               . setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
			OutputStream output      = serialPort.getOutputStream();




			System.out.println("commin final "+serialPort);

			for(int i=0; i<theModel.getRowCount(); i++)
			{
				Boolean bValue = (Boolean)theModel.getValueAt(i, 12);
				int iCount = common.toInt((String)theModel.getValueAt(i, 11));

				if(bValue.booleanValue() && iCount >0)
				{	

					for(int j=0 ; j<iCount;j++)
					{
						System.out.println("Count--->"+iCount);

						String SDept = common.parseNull((String)theModel.getValueAt(i,1));
						String SItemCod   = common.parseNull((String)theModel.getValueAt(i, 2));				
						String SItemCodd   = "CODE: "+common.parseNull((String)theModel.getValueAt(i, 2));				
						String SIName = common.parseNull((String)theModel.getValueAt(i, 3));
						String SItemNamee = "ITEM : "+common.parseNull((String)theModel.getValueAt(i, 3));
						String SLoc = "LOC: "+common.parseNull((String)theModel.getValueAt(i, 5));
						String SQty ="QTY: "+common.parseNull((String)theModel.getValueAt(i, 10));
						String SDraw = "DRAW: "+common.parseNull((String)VDraw.get(VItemName.indexOf(SIName)));
						String SCatl = "CATL: "+common.parseNull((String)VCatl.get(VItemName.indexOf(SIName)));
					//	String SQty = "None";	
						String SICod   = common.parseNull((String)VItemCode.get(VItemName.indexOf(SIName)));
							
						System.out.println("draw--->"+SDraw);
						System.out.println("catl--->"+SCatl);
						

						String str = "";
						String substr = "";
						String substr2 = "";

						if(SItemNamee.length()>28) 
						{
							substr = SItemNamee.substring(0, 28);
							substr2 = SItemNamee.substring(28,SItemNamee.length());
						}else
						{
							substr = SItemNamee;	
						}	



							String Str2 = "191100301600065"+substr+"\r";     
							String Str4 = "191100301400100"+substr2+"\r";     
							String Str3 = "191100301200065"+SItemCodd+"\r";
							String Str15 = "191100301000065"+SDraw+"\r";
							String Str13 = "191100300800065"+SCatl+"\r";
							String Str7 = "191100300600065"+SLoc+"\r";
							String Str11 = "191100300400065"+SQty+"\r";
							String Str8 = "1e6205000000185"+SICod+"\r";
							
							/*
							String Str2 = "191100301600020"+substr+"\r";     
							String Str4 = "191100301400055"+substr2+"\r";     
							String Str3 = "191100301200020"+SItemCodd+"\r";
							String Str15 = "191100301000020"+SDraw+"\r";
							String Str13 = "191100300800020"+SCatl+"\r";
							String Str7 = "191100300600020"+SLoc+"\r";
							String Str11 = "191100300400020"+SQty+"\r";
							String Str8 = "1e6205000000140"+SICod+"\r";
							
							*/
							
							

							output.write("n".getBytes());
							output.write("f285".getBytes());
							output.write("L".getBytes());
							output.write("H10".getBytes());
							output.write("D11".getBytes());

							output.write(Str2.getBytes()); 
							output.write(Str4.getBytes());
							output.write(Str3.getBytes());
							output.write(Str15.getBytes());
							output.write(Str13.getBytes());
							output.write(Str7.getBytes());
							output.write(Str11.getBytes()); 
							output.write(Str8.getBytes());
							output.write("E\r".getBytes());  

							System.out.println("commin all print final");

					}  
				}  
			}
				serialPort.close();

		}
			catch(Exception ex)	    
			{
				ex.printStackTrace();
			}
	}	  

	private SerialPort getSerialPort() 
	{
		SerialPort serialPort         = null;

		try
		{
			Enumeration portList     = CommPortIdentifier.getPortIdentifiers();

			while(portList.hasMoreElements())
			{
				CommPortIdentifier portId = (CommPortIdentifier)portList.nextElement();

				if(portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
				{
					serialPort     = (SerialPort)portId.open("comapp", 2000);
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);

		}

		return serialPort;
	}


}