package Indent.IndentFiles.NonStockReceipt;

import java.util.Vector;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.io.*;

import guiutil.*;
import util.*;
import jdbc.*;
import javax.swing.border.*;



import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

public class MaterialInstallationAuthenticationNew extends JInternalFrame {

    private JLayeredPane 							theLayer;
			int 									iUserCode;
			int 									iMillCode;

			java.util.List							returnlist;

			JPanel 									JTopPanel,JMiddlePanel,JBottomPanel;
			JTable									theTable;
			MaterialInstallationAuthenticationNewModel theModel;

			JButton									bSave,bExit,bApply,bSelect,bPdf;
			Common									common;
			String SItemTable;
			Vector VUserCode,VUserName,VMillCode,VMillName,VShortName,VDisplayName;
			int  iOwnerCode,iDeptCode,iGroupCode,iUnitCode;
			Vector VCode,VName,VReturnType,VReturnName,VReturnCode,VRCode,VRName,VNRCode,VNRName;
			JComboBox JMatrialName,JReturnType;
			DateField      TStDate;
			DateField      TEnDate;
			
			int iTotalColumns = 12;
			int iWidth[] = {10,35,35,35,35,35,35,35,35,35,35,35}; 
			
			   FileWriter FW;
				File file;
			
			String   SFileName;
			PdfPTable table;

			Document document;
			
			private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 11, Font.BOLD);
			private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
			private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 9, Font.NORMAL);
			private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
			private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 5, Font.BOLD);

	public MaterialInstallationAuthenticationNew(JLayeredPane theLayer,int iUserCode,int iMillCode,int iOwnerCode,String SItemTable){

		this.theLayer  		= theLayer;
        this.iUserCode 		= iUserCode;
        this.iMillCode 		= iMillCode;
        this.SItemTable		= SItemTable;

        this.VUserCode 		= VUserCode;
        this.VUserName 		= VUserName;
        this.VMillCode 		= VMillCode;
        this.VMillName 		= VMillName;
        this.VShortName		= VShortName;
        this.VDisplayName	= VDisplayName;
        this.iUnitCode   	= iUnitCode;
        this.iDeptCode 		= iDeptCode;
		this.iGroupCode 	= iGroupCode;
		this.iOwnerCode 	= iOwnerCode;

		System.out.println(iUserCode+"<===>"+iMillCode+"<===>"+iUnitCode+"<===>"+iDeptCode);

		common = new Common();

		setMatrialType();
	//	setMatrialType1();
	//	setMatrialType2();
	//	setReturnType();
		createComponents();
        setLayouts();
        addComponents();
        addListeners();
		getScreenDetails();
	}

	public void	createComponents(){
		
		JTopPanel		= new JPanel();
		JMiddlePanel	= new JPanel();
		JBottomPanel	= new JPanel();
		
		bSave			= new JButton("Save");
		bSave.setEnabled(false);
		bExit			= new JButton("Exit");
		bApply			= new JButton("Apply");
		bSelect			= new JButton("Select All");
		bPdf			= new JButton("PDF");
		
		TStDate      = new DateField();
			TEnDate        = new DateField();
			TStDate.setTodayDate();
			TEnDate.setTodayDate();
		
		
		JMatrialName    = new JComboBox(VName);
		
		JReturnType    = new JComboBox();
		JReturnType.addItem("All");
		JReturnType.addItem("Return");
		JReturnType.addItem("NonReturn");
		
		theModel		= new MaterialInstallationAuthenticationNewModel();
		theTable				= new JTable(theModel);
	}

    public void setLayouts(){
		
		JTopPanel		. setLayout(new GridLayout(6,4));
		JMiddlePanel	. setLayout(new BorderLayout());
		JBottomPanel	. setLayout(new FlowLayout());

		setTitle("Material Installation Authentication-Dept Hod");
        setClosable(true);
        setMaximizable(true);
		setResizable(true);
		setIconifiable(true);
        //setBounds(0,0,400,200);
		setSize(900,650);
       
	}

    public void addComponents(){
		
		JTopPanel            . add(new JLabel("FromDate"));
	     JTopPanel            . add(TStDate);
		JTopPanel.add(new JLabel("Return Type"));
		JTopPanel.add(JMatrialName);
		
		JTopPanel            . add(new JLabel("ToDate"));
	     JTopPanel            . add(TEnDate);
		JTopPanel.add(new JLabel("Return/NonReturn"));
		JTopPanel.add(JReturnType);
		
		
		JTopPanel.add(new JLabel(""));
		JTopPanel.add(new JLabel(""));
		
		
		
		JTopPanel.add(new JLabel(""));
		JTopPanel.add(new JLabel(""));
		
		JTopPanel.add(new JLabel(""));
		JTopPanel.add(new JLabel(""));
		
		JTopPanel.add(new JLabel(""));
		JTopPanel		. add(bApply);
		
		
		JTopPanel.add(new JLabel(""));
		JTopPanel.add(new JLabel(""));
		
	//	JTopPanel.add(new JLabel(""));
	//	JTopPanel.add(new JLabel(""));
		
		
		
		JTopPanel.add(new JLabel(""));
		JTopPanel.add(bSelect);
		
		JTopPanel.add(new JLabel(""));
		JTopPanel.add(new JLabel(""));
				
		
		JMiddlePanel	. add(new JScrollPane(theTable));
		JBottomPanel	. add(bSave);
		JBottomPanel	. add(bPdf);
		JBottomPanel	. add(bExit);
		
		JTopPanel .setBorder(new TitledBorder("Material Selection"));
		JMiddlePanel .setBorder(new TitledBorder("Clik to View"));
		JBottomPanel .setBorder(new TitledBorder("Controls"));

		getContentPane().add("North",JTopPanel);
		getContentPane().add("Center",JMiddlePanel);
		getContentPane().add("South",JBottomPanel);
	}

    public void addListeners(){
		JMatrialName .addActionListener(new ActList());
		bApply 	. addActionListener(new ActList());
		bExit	. addActionListener(new ActList());
		bSave	. addActionListener(new ActList());
		bSelect	. addActionListener(new ActList());
		bPdf	. addActionListener(new ActList());
	}
	
	private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
		{
			
			
			if(ae.getSource()==JMatrialName)
            {
				theModel.setNumRows(0);
            }
			if(ae.getSource()==bApply)
            {
				getScreenDetails();
            }
			if(ae.getSource()==bSave)
            {
				updateData();
            }
            if(ae.getSource()==bExit)
            {
				removeHelpFrame();
				
            }
			
			if(ae.getSource()==bSelect)
            {
				getSelection();
				
            }
		  
		 if(ae.getSource()==bPdf) 
			{
			 try
			 {
			//	 SFilePath = "d:\\ MaterialIssuedWithoutReturnMaterial";
				// SExtension = ".pdf";
				//DateFormat df = new SimpleDateFormat("yyyyMMddhhmmss");
				//SFilename = SFilePath + df.format(new Date()) + "." + SExtension;
				String SDateTime = getServerDateTime().replace(" ", "");
				SFileName = "d:\\MaterialIssuedWithoutReturnMaterial"+SDateTime+".pdf"; 
				System.out.println("FILENAME====>"+SFileName);
				createPDFFile();
				File theFile   = new File(SFileName);
				Desktop        . getDesktop() . open(theFile);
			 }
			  catch(Exception ex){
            ex.printStackTrace();
			}
			 
			}
        }
    }


 public String getServerDate()
     {
          String SDate = " " ;
          String SQry="select to_Char(sysdate,'yyyymmdd hh24:mi:ss') from dual";
          try
          {
			ORAConnection connect = ORAConnection.getORAConnection();
			Connection theConnection = connect.getConnection();
			Statement theStatement = theConnection.createStatement();
			ResultSet theResult = theStatement.executeQuery(SQry);
               if(theResult.next())
               SDate = theResult.getString(1);

               theResult.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               return "";
          }
          return SDate ;
     }

     public String getServerPureDate()
     {
	  String SDate = "";
          String SQry="select to_Char(sysdate,'yyyymmdd') from dual";
          try
          {
			ORAConnection connect = ORAConnection.getORAConnection();
			Connection theConnection = connect.getConnection();
			Statement theStatement = theConnection.createStatement();
			ResultSet theResult = theStatement.executeQuery(SQry);
               if(theResult.next())
               SDate = theResult.getString(1);

               theResult.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               return "";
          }
          return SDate ;
     }




 public String getServerDateTime()
     {
          String SDate= " " ;
                         
          String QS = " Select to_Char(Sysdate,'DD.MM.YYYY.HH24.MI.SS') From Dual ";


          try
          {
               ORAConnection connect = ORAConnection.getORAConnection();
               Connection theConnection = connect.getConnection();
               Statement theStatement = theConnection.createStatement();
               ResultSet theResult = theStatement.executeQuery(QS);
               if(theResult.next())
                    SDate = theResult.getString(1);
               theResult.close();
               theStatement.close();     
          }
          catch(Exception ex)
          {
               return "";
          }
          return SDate ;
     }



/*private static String FILE_PATH = "D:\Report";
private static String FILE_EXTENSION = ".pdf";
DateFormat df = new SimpleDateFormat("yyyyMMddhhmmss"); // add S if you need milliseconds
String filename = FILE_PATH + df.format(new Date()) + "." + FILE_EXTENSION;*/




  private void setMatrialType()
  {
	  VCode = new Vector();
	  VName = new Vector();
	  
	  VCode .addElement("0");
	 VName .addElement("All");
	  
	  try
	  {
		  String QS = " select code,name from materialindentreturntype ";
		  
		Connection theConnection=null;

          if(theConnection == null)
		  {
			  ORAConnection jdbc   = ORAConnection.getORAConnection();
              theConnection        = jdbc.getConnection();
           }
            Statement      stat        	=  theConnection.createStatement();
            ResultSet      theResult   =  stat.executeQuery(QS);
            while(theResult.next())
			{
				VCode.addElement(theResult.getString(1));
				VName.addElement(theResult.getString(2));
			}
			theResult.close();
			stat.close();
		  
	  }
	  catch(Exception ex){}
  }
 /* 
 private void setMatrialType1()
  {
	  VRCode = new Vector();
	  VRName = new Vector();
	  
	  VRCode .addElement("0");
	 VRName .addElement("All");
	  
	  try
	  {
		  String QS1 = " select * from materialindentreturndetails  where returntypecode in (1,6,10,11)  ";
		  
		  Connection theConnection=null;

          if(theConnection == null)
		  {
			  ORAConnection jdbc   = ORAConnection.getORAConnection();
              theConnection        = jdbc.getConnection();
           }
            Statement      stat1       	=  theConnection.createStatement();
            ResultSet      theResult1   =  stat1.executeQuery(QS1);
            while(theResult1.next())
			{
				VRCode.addElement(theResult1.getString(1));
				VRName.addElement(theResult1.getString(2));
			}
			theResult1.close();
			stat1.close();
		  
	  }
	  catch(Exception ex){}
  }
  
  private void setMatrialType2()
  {
	  VNRCode = new Vector();
	  VNRName = new Vector();
	  
	  VNRCode .addElement("0");
	 VNRName .addElement("All");
	  
	  try
	  {
		  String QS2 = " select * from materialindentreturndetails where returntypecode not in (1,6,10,11) ";
		  
		  Connection theConnection=null;

          if(theConnection == null)
		  {
			  ORAConnection jdbc   = ORAConnection.getORAConnection();
              theConnection        = jdbc.getConnection();
           }
            Statement      stat2       	=  theConnection.createStatement();
            ResultSet      theResult2   =  stat2.executeQuery(QS2);
            while(theResult2.next())
			{
				VNRCode.addElement(theResult2.getString(1));
				VNRName.addElement(theResult2.getString(2));
			}
			theResult2.close();
			stat2.close();
		  
	  }
	  catch(Exception ex){}
  }
*/
private void getSelection()
	{
    	int iCol=theModel.getColumnCount();
  
		for (int rowIndex = 0; rowIndex < theModel.getRowCount(); rowIndex++) 
		{
        	if(((Boolean)theModel.getValueAt(rowIndex,13)).booleanValue())
             theModel.setValueAt(new Boolean(false), rowIndex,13);
            else
              theModel.setValueAt(new Boolean(true), rowIndex,13);
		}
    
    }  
	
	public void getScreenDetails(){
		
		returnlist = new java.util.ArrayList();
		
		
		String SName  = (String)JMatrialName.getSelectedItem();
		
		System.out.println("SName-->"+SName);
		int iIndex 		 = VName.indexOf(SName);
		System.out.println("iIndex-->"+iIndex);
		String SCode  = (String)VCode.elementAt(iIndex);
		
		String SReturnType = common.parseNull((String)JReturnType.getSelectedItem());
	//	int iRIndex 		 = VRName.indexOf(SReturnType);
	//	String SRCode  = (String)VRCode.elementAt(iRIndex);
	//	int iNRIndex 		 = VNRName.indexOf(SReturnType);
	//	String SNRCode  = (String)VNRCode.elementAt(iNRIndex);

		System.out.println("SCode-->"+SCode);
		String SStDate = common.pureDate((String)TStDate.toNormal()); 
			String SEnDate = common.pureDate((String)TEnDate.toNormal()); 
	
		StringBuffer 	sb = new StringBuffer(); //"+VRCode+","+VNRCode+"
						
 						sb . append(" Select distinct MaterialIndentreturnDetails.ItemCode,MaterialIndentreturnDetails.ItemName,MaterialIndentreturnDetails.RequiredQty,MaterialIndentreturnDetails.IndentNo,");
 						sb . append(" MaterialIndentreturnDetails.Unit_Code, MaterialIndentreturnDetails.DeptCode, MaterialIndentreturnDetails.MachineCode,nvl(Machine.Mach_Name,'General')  as MACHINENAME,");
 						sb . append(" MaterialIndentreturnDetails.usercode, Rawuser.UserName, process_units.UnitName,Department.DeptName,to_char(MaterialIndentreturnDetails.EntryDate,'dd.mm.yyyy') as EntryDate,materialindentreturntype.name,MaterialIndentreturnDetails.Id, ");
 						sb . append(" Issue.IssueDate as IssueDate,  ");
						sb . append(" to_Date(to_Char(SysDate, 'YYYYMMDD'), 'YYYYMMDD') - to_Date(IssueDate, 'YYYYMMDD') as DelayDays ");
						sb . append(" From MaterialIndentreturnDetails ");
 						sb . append(" Left Join Machine On Machine.Mach_Code = MaterialIndentreturnDetails.MachineCode  ");
 						sb . append(" Inner Join process_units On process_units.UnitCode = MaterialIndentreturnDetails.Unit_Code ");
 						sb . append(" Inner Join HrdNew.Department On Department.DeptCode = MaterialIndentreturnDetails.DeptCode ");
						sb . append(" Inner join materialindentreturntype on materialindentreturntype.code = MaterialIndentreturnDetails.Returntypecode ");
						sb . append(" Inner join Scm.Rawuser on Rawuser.usercode = MaterialIndentreturnDetails.usercode ");//and  MaterialIndentreturnDetails.ownercode="+iOwnerCode);
						sb.append(" Inner join issue on issue.uirefno = MaterialIndentreturnDetails.indentno  ");
						sb.append(" and Issue.Code = MaterialIndentreturnDetails.ItemCode ");
						sb . append(" Where MaterialIndentreturnDetails.DEPTHODINSTSTATUS =0");
						sb . append("and to_char(MaterialIndentreturnDetails.entrydate,'yyyymmdd') >= "+SStDate+" ");
						sb . append("and to_char(MaterialIndentreturnDetails.entrydate,'yyyymmdd') <= "+SEnDate+" ");
					if(!SCode.equals("0"))
						{
							sb . append(" and MaterialIndentreturnDetails.returntypecode="+SCode+" ");  
						}
						if(SReturnType.equals("All"))
						{
						sb . append(" and MaterialIndentreturnDetails.returntypecode in (1,2,3,4,5,6,7,8,9,10,11) ");
						}
						else if(SReturnType.equals("Return"))
						{
							sb . append(" and MaterialIndentreturnDetails.returntypecode in (1,6,10,11)  ");
						}
 						else{
						sb . append(" and MaterialIndentreturnDetails.returntypecode not in (1,6,10,11) ");
						} 
						sb . append(" Order by 17 desc, process_units.UnitName,Department.DeptName,rawuser.username,materialindentreturntype.name desc ");

						
						System.out.println(sb.toString());
		try{
			Connection theConnection=null;

            if(theConnection == null){
            
                ORAConnection jdbc   = ORAConnection.getORAConnection();
                theConnection        = jdbc.getConnection();
            }
            Statement      stat        	=  theConnection.createStatement();
            ResultSet      theResult1   =  stat.executeQuery(sb.toString());
            while(theResult1.next()){
				java.util.HashMap 	hm 			= new java.util.HashMap();
									hm			. put("ITEMCODE",theResult1.getString(1));
									hm			. put("ITEMNAME",theResult1.getString(2));
									hm			. put("REQUIREDQTY",theResult1.getString(3));
									hm			. put("INDENTNO",theResult1.getString(4));
									hm			. put("UNIT_CODE",theResult1.getString(5));
									hm			. put("DEPTCODE",theResult1.getString(6));
									hm			. put("MACHINECODE",theResult1.getString(7));
									hm			. put("MACHINENAME",theResult1.getString(8));
									hm			. put("USERCODE",theResult1.getString(9));
									hm			. put("USERNAME",theResult1.getString(10));
									hm			. put("UNITNAME",theResult1.getString(11));
									hm			. put("DEPTNAME",theResult1.getString(12));
									hm			. put("ENTRYDATE",theResult1.getString(13));
									hm			. put("RETURNTYPE",theResult1.getString(14));
									hm			. put("ID",theResult1.getString(15));
									hm			. put("ISSUEDATE",common.parseDate(theResult1.getString(16)));
									hm			. put("DELAYDAYS",theResult1.getString(17));
									returnlist	. add(hm);
			}
			theResult1.close();
			stat.close();
			theModel.setNumRows(0);
			int isl  = 0;
			for(int i=0;i<returnlist . size();i++){

				isl++;
				java.util.HashMap hm = (java.util.HashMap)returnlist.get(i);

				Vector 	vec = new Vector();
					    vec . add(String.valueOf(isl));
						vec . add(common.parseNull((String)hm			. get("ITEMNAME")));
						vec . add(common.parseNull((String)hm			. get("ITEMCODE")));
						vec . add(common.parseNull((String)hm			. get("INDENTNO")));
						vec . add(common.parseNull((String)hm			. get("ENTRYDATE")));
						vec . add(common.parseNull((String)hm			. get("REQUIREDQTY")));
						vec . add(common.parseNull((String)hm			. get("MACHINENAME")));
						vec . add(common.parseNull((String)hm			. get("DEPTNAME")));
						vec . add(common.parseNull((String)hm			. get("UNITNAME")));
						vec . add(common.parseNull((String)hm			. get("USERNAME")));
						vec . add(common.parseNull((String)hm			. get("RETURNTYPE")));
						vec . add(common.parseNull((String)hm			. get("ISSUEDATE")));
						vec . add(common.parseNull((String)hm			. get("DELAYDAYS")));
						vec . add(new Boolean(false));
				theModel	. appendRow(vec);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private void removeHelpFrame()
    {
        try
        {
			this . setVisible(false);
        }
        catch(Exception ex){}
    }

	public void updateData()
	{

		 Connection theConnection=null;
		 
		try{
           

            if(theConnection == null)
            {
                ORAConnection jdbc   = ORAConnection.getORAConnection();
                theConnection        = jdbc.getConnection();
            }

			String str = "update MaterialIndentreturnDetails set DEPTHODINSTSTATUS =1,DEPTHODINSTDate = sysdate,DEPTHODINSTSYSTEMNAME='"+common.getLocalHostName()+"'  where ITEMCODE=? AND INDENTNO=? AND MACHINECODE=? AND DEPTCODE=? AND UNIT_CODE=? AND ID=?";

			String str2 = "update MaterialIndentDetails set DEPTHODINSTSTATUS =1,DEPTHODINSTSTATUSDate = sysdate where ITEMCODE=? AND INDENTNO=? AND MACHINECODE=? AND DEPTCODE=? AND UNIT_CODE=? ";


			theConnection.setAutoCommit(false);
			
			PreparedStatement pts	= theConnection.prepareStatement(str);
			PreparedStatement ptss	= theConnection.prepareStatement(str2);

			for(int i=0;i<returnlist.size();i++){

				java.util.HashMap hm = (java.util.HashMap)returnlist.get(i);

				boolean bvl = ((Boolean)theModel.getValueAt(i,13)).booleanValue();
				if(bvl){
					String itemcode = common.parseNull((String)hm			. get("ITEMCODE"));
					String indentno = common.parseNull((String)hm			. get("INDENTNO"));
					String machcode = common.parseNull((String)hm			. get("MACHINECODE"));
					String deptcode = common.parseNull((String)hm			. get("DEPTCODE"));
					String unitcode = common.parseNull((String)hm			. get("UNIT_CODE"));
					String id 		= common.parseNull((String)hm			. get("ID"));
					
			//		System.out.println(str+"="+itemcode+"="+indentno+"="+machcode+"="+deptcode+"="+unitcode+"="+id);
					
					pts	. setString(1,itemcode);
					pts	. setString(2,indentno);
					pts	. setString(3,machcode);
					pts	. setString(4,deptcode);
					pts	. setString(5,unitcode);
					pts	. setString(6,id);
					pts.executeUpdate();
					
			//		System.out.println(str2+"="+itemcode+"="+indentno+"="+machcode+"="+deptcode+"="+unitcode);
					ptss	. setString(1,itemcode);
					ptss	. setString(2,indentno);
					ptss	. setString(3,machcode);
					ptss	. setString(4,deptcode);
					ptss	. setString(5,unitcode);
					ptss.executeUpdate();
					
					
					
				}
			}
			
			theConnection.commit();
			theConnection.setAutoCommit(true);
			
			JOptionPane.showMessageDialog(null,"Data Updated","Information",JOptionPane.INFORMATION_MESSAGE);
			getScreenDetails();
		} 
		catch(Exception ex)
        {
           try
           {
            theConnection.rollback();  
            theConnection.setAutoCommit(true);
            System.out.println(" ERRor "+ex); 
            JOptionPane.showMessageDialog(null, "Data Not Saved "+ex, "Information", JOptionPane.INFORMATION_MESSAGE);
           }
           catch(Exception ex1)
           {
              System.out.println(" rollback "+ex1);  
           }
        }
	}


	private void createPDFFile()
	{
        try 
        {
			document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(SFileName));
            document.setPageSize(PageSize.LEGAL.rotate());
            document.open();
                             
            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            table.setHeaderRows(5);
			pdfHead();
			pdfBody();
           
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }

	}  
	
	private void pdfHead()
	{
	String SReturnType = common.parseNull((String)JReturnType.getSelectedItem());
	
	
	   AddCellIntoTable("AMARJOTHI SPINNING MILLS LIMITED", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
       // AddCellIntoTable(" MATERIAL ISSUED WITHOUT RETURN MATERIAL : " +TStDate.toString()+ "---" +TEnDate.toString() , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
	AddCellIntoTable(" NON-RETURN MATERIAL LIST AGAINST STORE ISSUES : " +TStDate.toString()+ "---" +TEnDate.toString() , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
	
	//	AddCellIntoTable(" TYPE : " +JMatrialName.getSelectedItem(), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
		if(SReturnType.equals("Return"))
		{		 
			AddCellIntoTable(" INDENT PLACED WITH OLD MATERIAL RETURNED " , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
		}
		else if (SReturnType.equals("NonReturn"))
		{
			AddCellIntoTable("  INDENT PLACED WITHOUT OLD MATERIAL RETURNED " , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold); 
		}
		else
		{
			AddCellIntoTable("  " , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold); 
		}
	 
		AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);

          AddCellIntoTable("SL NO" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
     	AddCellIntoTable("ITEM CODE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("ITEM NAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable(" INDENT NO" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("INDENT DATE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("UNIT" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("DEPARTMENT" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("MACHINE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("ISSUE QTY" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("ISSUE DATE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("ISSUE TYPE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("USERNAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
     
        
		
		
	}

	private void pdfBody()
	{
	try
	{
		int colcount=theModel.getColumnCount();
        int rowcount=theModel.getRowCount();
		  
		  for(int K=0;K<theModel.getRowCount();K++)
            {
	

       AddCellIntoTable(String.valueOf(theModel.getValueAt(K,0)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);

        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,2)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,1)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,3)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,4)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,8)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,7)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,6)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,5)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,11)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,10)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
	//	AddCellIntoTable(String.valueOf(theModel.getValueAt(K,0)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(String.valueOf(theModel.getValueAt(K,9)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		
	
	}
		
		
		document.add(table);
		document.close();
	
	}
	catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }
	}  

	public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str ));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
   }  

}
