package Indent.IndentFiles.NonStockReceipt;

import java.util.Vector;
import javax.swing.table.DefaultTableModel;

public class MaterialInstallationAuthenticationNewModel  extends DefaultTableModel {

   public String   ColumnName[]   =  { "SI No","ITEMNAME","ITEMCODE","INDENTNO","INDENTDATE","REQUIREDQTY","MACHINENAME","DEPTNAME","UNITNAME","USERNAME","RETURN TYPE","ISSUEDATE","DELAYDAYS","SELECT"};
   public String   ColumnType[]   =  { "N"    ,"S"       ,"S"       ,"S"  ,"S"     ,"S"          ,"S"          ,"S"       ,"S"     ,"S",       "S"  ,"S"	   ,"S"    ,"B"};
   public int      iColumnWidth[] =  {  5    ,30   	 ,50        ,100   ,100    ,50           ,70           ,100       ,20        ,30,30,30		,50,30 };

   public MaterialInstallationAuthenticationNewModel(){
       setDataVector(getRowData(),ColumnName);
   }

   public Class getColumnClass(int iCol)  {
      return getValueAt(0,iCol).getClass();
   }

   public boolean isCellEditable(int iRow,int iCol)   {
      if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
         return true;

      return false;
   }

   private Object[][] getRowData()    {
       Object 	RowData[][] = new Object[1][ColumnName.length];

       for(int i=0;i<ColumnName.length;i++)
            RowData[0][i] = "";

       return RowData;
   }

   public void appendRow(Vector theVect)   {
      insertRow(getRows(),theVect);
   }

   public int getRows()   {
      return super.dataVector.size();
   }
}

