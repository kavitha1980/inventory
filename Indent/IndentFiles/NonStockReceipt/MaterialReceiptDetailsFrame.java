package Indent.IndentFiles.NonStockReceipt;

import guiutil.FinDateField;
import jdbc.ORAConnection;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;
import java.net.InetAddress;
import util.*;
import javax.swing.table.TableColumnModel;

public class MaterialReceiptDetailsFrame extends JInternalFrame 
{

     protected     JLayeredPane Layer;

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BApply;
     JButton       BExit;
     JButton       BSave;
     JTextField    searchField;
     Common        common = new Common();
     Connection     theConnection;
	
     int           iUserCode=0,iMillCode,iOwnerCode;
     int PRINT=11;
     
     FinDateField TFromDate,TToDate;

     ArrayList theList = new ArrayList();
     ArrayList theReturnList = new ArrayList();

     MaterialReceiptDetailsModel   theModel;

     JTable             theTable;
	InetAddress ip;
     
     public MaterialReceiptDetailsFrame(JLayeredPane Layer,int iUserCode,int iMillCode,int iOwnerCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
		  this.iOwnerCode = iOwnerCode;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }

     private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();

               theModel       = new MaterialReceiptDetailsModel();
               theTable       = new JTable(theModel);
               
               
               TableColumnModel TC  = theTable.getColumnModel();

                for(int i=0;i<theModel.ColumnName.length;i++)   {

                    TC .getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
                }

               TFromDate      = new FinDateField();
               TToDate        = new FinDateField();

               TFromDate      .   setTodayDate();
               TToDate        .   setTodayDate();
               
               BExit          = new JButton("Exit");
               BApply         = new JButton("Apply");
               BSave          = new JButton("Save");
               
               searchField    = new JTextField();
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }
     }

     private void setLayouts()
     {
            setTitle("Material Receipt Against Return ");
            setClosable(true);                    
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,775,600);

            TopPanel.setLayout(new GridLayout(4,2));
            MiddlePanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());

            TopPanel.setBorder(new TitledBorder("Info."));
            MiddlePanel.setBorder(new TitledBorder("Details"));
            BottomPanel.setBorder(new TitledBorder("Controls"));

            TopPanel            . setBackground(new Color(213,234,255));
            MiddlePanel         .setBackground(new Color(213,234,255));
            BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

        /*TopPanel            . add(new JLabel("FromDate"));
        TopPanel            . add(TFromDate);
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel("ToDate"));
        TopPanel            . add(TToDate);
        
        TopPanel	    . add(BApply);*/
         
        TopPanel            . add(new JLabel("Search"));
        TopPanel            . add(searchField);
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));

        MiddlePanel.add(new JScrollPane(theTable));

        BottomPanel.add(BExit);
        BottomPanel.add(BSave);

        getContentPane().add("North",TopPanel);
        getContentPane().add("Center",MiddlePanel);
        getContentPane().add("South",BottomPanel);
        BExit.setMnemonic('E');
        BSave.setMnemonic('S');

     }

    private void addListeners()
    {
          BExit.addActionListener(new ActList());
          BApply.addActionListener(new ActList());
          BSave.addActionListener(new ActList());
          searchField. addKeyListener(new SearchKeyList());
          //theTable.addKeyListener(new KeyList ());
    }

    public void setTabReport()
    {
          try
          {

                String SFromDate = common.pureDate((String)TFromDate.toNormal()); 
                String SToDate = common.pureDate((String)TToDate.toNormal()); 

                getMaterialIndentReturn(SFromDate,SToDate);

                ArrayList ADetails = getMaterialIndent(SFromDate,SToDate);

                for(int i=0;i<ADetails.size();i++)
                {
                     HashMap  row =(HashMap)ADetails.get(i);

                     Vector VTheVect = new Vector();
                     
                     String sItemCode = common.parseNull((String)row.get("ITEMCODE"));
                     String sIndentNo = common.parseNull((String)row.get("INDENTNO"));
                     String sUnitCode = common.parseNull((String)row.get("UNIT_CODE"));
                     String sDeptCode= common.parseNull((String)row.get("DEPTCODE"));
                     String sMachCode = common.parseNull((String)row.get("MACHINECODE"));

                     VTheVect.addElement(common.parseNull((String)row.get("ITEMCODE")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEMNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("REQUIREDQTY")));
                     VTheVect.addElement(common.parseNull((String)row.get("INDENTNO")));
                     VTheVect.addElement(common.parseNull((String)row.get("UNITNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("DEPTNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("MACHINENAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("USERNAME")));
                     VTheVect.addElement(common.parseNull(common.parseDate((String)row.get("ENTRYDATE"))));
                     VTheVect.addElement(String.valueOf(getRetAlreadyReturned(sItemCode,sIndentNo,sUnitCode,sDeptCode,sMachCode)));
                     VTheVect.addElement(common.parseNull((String)row.get("DELAYDAYS")));
                     VTheVect.addElement("");

                     theModel.appendRow(VTheVect);
               }
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
              
               if(ae.getSource()==BSave)
               {
                    if(insertReturn()==0){
                        showMessage("Return Quantity Updated..");
				    theModel.setNumRows(0);
                        setTabReport();
                    }
                   else{
                           
                   }
               }

               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }

               if(ae.getSource()==BApply)
               {
                    theModel.setNumRows(0);
                    setTabReport();
               }
          }
     }
     private class KeyList extends KeyAdapter  {

          public void keyPressed(KeyEvent ke)
          {
               int iRow = theTable.getSelectedRow       ();
               int iCol = theTable.getSelectedColumn    ();

               System.out.println("keyPressed");

               if( iCol==11) {

			checkValue(iRow);
               }
           }

          public void keyReleased(KeyEvent ke)   {
             try   {

            int    iRow    = theTable.getSelectedRow();
            int    iCol    = theTable.getSelectedColumn();
            
            System.out.println("keyReleased");

            if( iCol==11) {              
			checkValue(iRow);
            }

         }catch(Exception e)  {
   
            e.printStackTrace();
         }
      }
   }
     private class SearchKeyList extends KeyAdapter
        {
            public void keyReleased(KeyEvent ke)
            {
                searchItems();     
            }

            public void keyPressed(KeyEvent ke)
            {
                searchItems();
            }    

            public void keyTyped(KeyEvent ke)
            {
                searchItems();
            }
        }

    private void showMessage(String sMsg)
    {
          JOptionPane.showMessageDialog(null,getMessage(sMsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
    }
    private String getMessage(String sMsg)
    {
          String str = "<html><body>";
          str = str + sMsg;
          str = str + "</body></html>";
          return str;
    }
	
    private void removeHelpFrame()
    {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public ArrayList getMaterialIndent(String SFromDate, String SToDate)
     {
            theList = new ArrayList();
            StringBuffer sb = new StringBuffer();

            sb.append(" Select MaterialIndentDetails.ItemCode,MaterialIndentDetails.ItemName,MaterialIndentDetails.RequiredQty,MaterialIndentDetails.IndentNo, ");
            sb.append(" process_units.UnitName,Department.DeptName,nvl(Machine.Mach_Name,'General')  as MACHINENAME,Rawuser.UserName, ");
            sb.append(" to_char(MaterialIndentDetails.EntryDate,'yyyymmdd') as EntryDate,MaterialIndentDetails.Unit_code, ");
            sb.append(" MaterialIndentDetails.DeptCode,MaterialIndentDetails.MachineCode,Round(SysDate - MaterialIndentDetails.EntryDate) as DelayDays,MaterialIndentDetails.RETURNQTY,MaterialIndentDetails.ID ");
            sb.append(" From MaterialIndentDetails ");
            sb.append(" Left Join Machine On Machine.Mach_Code = MaterialIndentDetails.MachineCode  ");
            sb.append(" Inner Join process_units On process_units.UnitCode = MaterialIndentDetails.Unit_Code ");
            sb.append(" Inner Join HrdNew.Department On Department.DeptCode = MaterialIndentDetails.DeptCode ");
            sb.append(" Inner join Scm.Rawuser on Rawuser.usercode = MaterialIndentDetails.usercode ");
            //sb.append(" Where to_char(MaterialIndentDetails.EntryDate,'yyyymmdd') >= "+SFromDate+"  ");
            sb.append(" Where to_char(MaterialIndentDetails.EntryDate,'yyyymmdd') <=to_char(sysdate,'yyyymmdd') and MaterialIndentDetails.RETURNSTATUS=1 ");
		  sb.append(" and MaterialIndentDetails.STORERECEIPTSTATUS=0 ") ;
            sb.append(" Order by to_char(MaterialIndentDetails.EntryDate,'yyyymmdd'),MaterialIndentDetails.IndentNo ");

	//	System.out.println(sb.toString());

            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(sb.toString());
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theList;
     }
     public ArrayList getMaterialIndentReturn(String SFromDate, String SToDate)
     {
            theReturnList = new ArrayList();

            StringBuffer sb = new StringBuffer();

            sb.append(" select ITEMCODE,INDENTNO,UNIT_CODE,DEPTCODE,MACHINECODE,REQUIREDQTY,RETURNQTY ");
            sb.append(" From MaterialReturnDetails ");
            sb.append(" Where nvl(RETURNQTY,0)<REQUIREDQTY ");
            sb.append(" and to_char(MaterialReturnDetails.EntryDate,'yyyymmdd') <="+SToDate+" and ReturnType=1 ");
		  

            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(sb.toString());
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theReturnList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theReturnList;
     }
     private void checkValue(int iRow){


         double dalreadyRet = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,9)));

         double dretQty = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,11)));
         
         double dReqQty = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,2)));
         
         double dNeedToRetrun = dReqQty-dalreadyRet;

         if(dretQty>dNeedToRetrun){

              showMessage("Return Quantiy is Must be equal or Less Than Required Qty");
          }

    }
     private int insertReturn(){
         
         int iError = 0;
         int icheck = 0;
         StringBuffer sb = new StringBuffer();
         StringBuffer sbupdate = new StringBuffer();
         PreparedStatement preparedStat = null;
         PreparedStatement preparedSt= null;
         java.util.HashMap theMap = new java.util.HashMap();
	    String iip = "";

        try{
		   
		  iip = InetAddress.getLocalHost().getHostAddress(); 
		   
            sb.append(" Insert into MaterialReturnDetails ( ID,ITEMCODE,ITEMNAME,REQUIREDQTY,INDENTNO,UNIT_CODE,DEPTCODE,MACHINECODE,USERCODE,ENTRYDATE,RETURNQTY,MACHINENAME,RETURNTYPE,OWNERCODE,STORERECEIPTDATE,SYSTEMNAME ) ");
            sb.append(" Values(MaterialReturnDetails_Seq.nextVal,?,?,?,?,?,?,?,?,sysdate,?,?,1,?,sysdate,'"+iip+"') ");
            
            sbupdate.append(" Update MaterialIndentDetails set STORERECEIPTSTATUS=1 , StoreReceiptDate = SysDate  ") ;  
            sbupdate.append(" where ITEMCODE=? and INDENTNO=? and UNIT_CODE=? and DEPTCODE=? and MACHINECODE=? and ID=? ") ;

            if(theConnection ==null) {

                ORAConnection jdbc   = ORAConnection.getORAConnection();
                theConnection        = jdbc.getConnection();
            }
            preparedStat = theConnection.prepareStatement(sb.toString());
            preparedSt = theConnection.prepareStatement(sbupdate.toString());
            
            for (int index = 0; index < theModel.getRows(); index++) {

                theMap = (java.util.HashMap) theList.get(index);

                double dretQty = common.toDouble(common.parseNull((String)theModel.getValueAt(index,11)));
                

                    if(dretQty>0){
                        
                        double dalreadyRet = common.toDouble(common.parseNull((String)theModel.getValueAt(index,9)));
                        
                        double dReqQty = common.toDouble(common.parseNull((String) theMap.get("REQUIREDQTY")));

                        double dNeedToRetrun = dReqQty-dalreadyRet;

                     //   if(dretQty>dNeedToRetrun){
                     //       icheck += 1;
                      //  }
                      //  else{
                            //preparedStat = theConnection.prepareStatement(sb.toString());

                            preparedStat.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
                            preparedStat.setString(2,common.parseNull((String) theMap.get("ITEMNAME")));
                            preparedStat.setString(3,common.parseNull((String) theMap.get("REQUIREDQTY")));
                            preparedStat.setString(4,common.parseNull((String) theMap.get("INDENTNO")));
                            preparedStat.setString(5,common.parseNull((String) theMap.get("UNIT_CODE")));
                            preparedStat.setString(6,common.parseNull((String) theMap.get("DEPTCODE")));
                            preparedStat.setString(7,common.parseNull((String) theMap.get("MACHINECODE")));
                            preparedStat.setInt(8,iUserCode);
                            preparedStat.setString(9,common.parseNull((String)theModel.getValueAt(index,11)));
                            preparedStat.setString(10,common.parseNull((String) theMap.get("MACHINENAME")));
					   preparedStat.setInt(11,iOwnerCode);
                            preparedStat.executeUpdate();
                            
                            double dtotal = (dalreadyRet+dretQty);
                            
							System.out.println("dalreadyRet=="+dalreadyRet);
							System.out.println("dReciptQty=="+dretQty);
							
                            System.out.println("dtotal=="+dtotal);
                            

                            if(dtotal==dReqQty){

                                preparedSt.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
                                preparedSt.setString(2,common.parseNull((String) theMap.get("INDENTNO")));
                                preparedSt.setString(3,common.parseNull((String) theMap.get("UNIT_CODE")));
                                preparedSt.setString(4,common.parseNull((String) theMap.get("DEPTCODE")));
                                preparedSt.setString(5,common.parseNull((String) theMap.get("MACHINECODE")));
                                preparedSt.setString(6,common.parseNull((String) theMap.get("ID")));
                                preparedSt.executeUpdate();

                            }
                       // }
                    }
            }
            if(icheck>0)
            {
                showMessage(" Return Quantiy is Must be equal or Less Than Required Qty");
                //iError = 1;
			 iError = 0;
            }

            if(preparedStat.equals("null"))
            preparedStat.close();
         }

         catch(Exception ex){
             iError = 1;
             ex.printStackTrace();
         }
        return iError;
     }

     private double getRetAlreadyReturned(String sItemCode,String sIndnetNo,String sUnitCode,String sDeptCode,String sMachCode){

         double dalreadyRet = 0;
         java.util.HashMap theMap = new java.util.HashMap();
         
         for (int index = 0; index < theReturnList.size(); index++) {
             
             theMap = (java.util.HashMap) theReturnList.get(index);
             
            String sItem =  common.parseNull((String) theMap.get("ITEMCODE"));
            String sIndnent =  common.parseNull((String) theMap.get("INDENTNO"));
            String sUnit =  common.parseNull((String) theMap.get("UNIT_CODE"));
            String sDept =  common.parseNull((String) theMap.get("DEPTCODE"));
            String sMach =  common.parseNull((String) theMap.get("MACHINECODE"));
            
            if(sItemCode.equals(sItem) && sIndnetNo.equals(sIndnent) && sUnitCode.equals(sUnit) && sDeptCode.equals(sDept) && sMachCode.equals(sMach)){

                    dalreadyRet = common.toDouble(common.parseNull((String) theMap.get("RETURNQTY")));

                    //break;
            }
        }
         return dalreadyRet;
     }
     private void searchItems()
     {
          String SSearchStr        = common.parseNull(searchField.getText()).trim();

          int iIndex               = -1;

          for(int i=0; i<theModel.getRowCount(); i++)
          {
               String SItemName    = common.parseNull((String)theModel.getValueAt(i, 1));

               if(SItemName.startsWith(SSearchStr.toUpperCase()))
               {
                    iIndex         = i;
                    break;
               }
          }
          if(iIndex != -1)
          {
                 theTable       . setRowSelectionInterval(iIndex, iIndex);
                 Rectangle theRect   = theTable.getCellRect(iIndex, 0, true);
                 theTable       . scrollRectToVisible(theRect);
                 theTable       . setSelectionBackground(Color.green);			   
          }
     }
}

