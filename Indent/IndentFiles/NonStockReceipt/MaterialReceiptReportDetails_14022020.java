package Indent.IndentFiles.NonStockReceipt;

import guiutil.FinDateField;
import jdbc.ORAConnection;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;

import util.*;
import javax.swing.table.TableColumnModel;

public class MaterialReceiptReportDetails extends JInternalFrame 
{

     protected     JLayeredPane Layer;

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BApply;
     JButton       BExit;
     JButton       BSave;
     JTextField    searchField;
     Common        common = new Common();
     Connection     theConnection;
	
     int           iUserCode=0,iMillCode,iOwnerCode,iid;
     int PRINT=9;
     
     FinDateField TFromDate,TToDate;

     ArrayList theList = new ArrayList();
     ArrayList theReturnList = new ArrayList();

     MaterialReceiptReportModel   theModel;

     JTable             theTable;
	 Boolean bselect;
     
     public MaterialReceiptReportDetails(JLayeredPane Layer,int iUserCode,int iMillCode,int iOwnerCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
		  this.iOwnerCode = iOwnerCode;

          createComponents();
          setLayouts();
          addComponents();
		  addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }

     private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();

               theModel       = new MaterialReceiptReportModel();
               theTable       = new JTable(theModel);
               
               
               TableColumnModel TC  = theTable.getColumnModel();

                for(int i=0;i<theModel.ColumnName.length;i++)   {

                    TC .getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
                }

               TFromDate      = new FinDateField();
               TToDate        = new FinDateField();

               TFromDate      .   setTodayDate();
               TToDate        .   setTodayDate();
               
               BExit          = new JButton("Exit");
               BApply         = new JButton("Apply");
               BSave          = new JButton("Save");
               
               searchField    = new JTextField();
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }
     }

     private void setLayouts()
     {
            setTitle("Material Scrab Issue Against Return ");
            setClosable(true);                    
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,775,600);

            TopPanel.setLayout(new GridLayout(2,2));
            MiddlePanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());

            TopPanel.setBorder(new TitledBorder("Info."));
            MiddlePanel.setBorder(new TitledBorder("Details"));
            BottomPanel.setBorder(new TitledBorder("Controls"));

            TopPanel            . setBackground(new Color(213,234,255));
            MiddlePanel         .setBackground(new Color(213,234,255));
            BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

        
		TopPanel            . add(new JLabel(""));
		TopPanel            . add(new JLabel(""));
		TopPanel            . add(new JLabel("Search"));
        TopPanel            . add(searchField);
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));

        MiddlePanel.add(new JScrollPane(theTable));

        BottomPanel.add(BExit);
        BottomPanel.add(BSave);

        getContentPane().add("North",TopPanel);
        getContentPane().add("Center",MiddlePanel);
        getContentPane().add("South",BottomPanel);
        BExit.setMnemonic('E');
        BSave.setMnemonic('S');

     }

    private void addListeners()
    {
          BExit.addActionListener(new ActList());
         // BApply.addActionListener(new ActList());
          BSave.addActionListener(new ActList());
          searchField. addKeyListener(new SearchKeyList());
          //theTable.addKeyListener(new KeyList ());
    }

    public void setTabReport()
    {
          try
          {

                String SFromDate = common.pureDate((String)TFromDate.toNormal()); 
                String SToDate = common.pureDate((String)TToDate.toNormal()); 
				

                getMaterialIndentReturn(SFromDate,SToDate);

                ArrayList ADetails = getMaterialIndent(SFromDate,SToDate);

                for(int i=0;i<ADetails.size();i++)
                {
					
                     HashMap  row =(HashMap)ADetails.get(i);

                     Vector VTheVect = new Vector();
                     
                     String sItemCode = common.parseNull((String)row.get("ITEMCODE"));
                     String sIndentNo = common.parseNull((String)row.get("INDENTNO"));
					 
					VTheVect.addElement(common.parseNull((String)row.get("ID")));
					VTheVect.addElement(common.parseNull((String)row.get("ITEMCODE")));
					VTheVect.addElement(common.parseNull((String)row.get("ITEMNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("RETURNQTY")));
					VTheVect.addElement(common.parseNull((String)row.get("INDENTNO")));
					VTheVect.addElement(String.valueOf(getRetAlreadyReturned(sItemCode,sIndentNo)));//,sUnitCode,sDeptCode,sMachCode)));
					VTheVect.addElement(common.parseNull((String)row.get("RETURNQTY")));
					VTheVect.addElement(new Boolean(true));

					theModel.appendRow(VTheVect); 
					 
					 
               }
			   
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
              
               if(ae.getSource()==BSave)
               {
                    if(insertReturn()==0)
					{
					updateStatus();
                        showMessage("Return Quantity Updated..");
					theModel.setNumRows(0);
                        setTabReport();
				    
					}
					
				
				}
                   
               

               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }

               /*if(ae.getSource()==BApply)
               {
                    
					theModel.setNumRows(0);
					setTabReport();
					
               }*/
          }
     }
    
     private class SearchKeyList extends KeyAdapter
        {
            public void keyReleased(KeyEvent ke)
            {
                searchItems();     
            }

            public void keyPressed(KeyEvent ke)
            {
                searchItems();
            }    

            public void keyTyped(KeyEvent ke)
            {
                searchItems();
            }
        }

    private void showMessage(String sMsg)
    {
          JOptionPane.showMessageDialog(null,getMessage(sMsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
    }
    private String getMessage(String sMsg)
    {
          String str = "<html><body>";
          str = str + sMsg;
          str = str + "</body></html>";
          return str;
    }
	
    private void removeHelpFrame()
    {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public ArrayList getMaterialIndent(String SFromDate, String SToDate)
     {
            theList = new ArrayList();
            StringBuffer sb = new StringBuffer();

							 
				sb.append(" select sum(MaterialReturnDetails.RETURNQTY) as RETURNQTY ,   ");
				sb.append(" Materialreturndetails.id,  ");
				sb.append(" MaterialReturnDetails.itemcode,MaterialReturnDetails.itemname   ");
				sb.append(" ,MaterialReturnDetails.IndentNo,UserName,   ");
				sb.append(" MaterialReturnDetails.RequiredQty,   ");
				sb.append(" to_char(MaterialReturnDetails.EntryDate,'yyyymmdd') as EntryDate   ");
				sb.append(" From MaterialReturnDetails   ");
				sb.append(" Inner join Scm.Rawuser on Rawuser.usercode = MaterialReturnDetails.usercode  ");
				sb.append(" Where to_char(MaterialReturnDetails.EntryDate,'yyyymmdd')   ");
				sb.append(" <=to_char(sysdate,'yyyymmdd')  and MaterialReturnDetails.RETURNTYPE=1   ");
				sb.append(" and MaterialReturnDetails.closingstatus = 0   ");
				sb.append(" group by materialreturndetails.id,MaterialReturnDetails.itemcode,MaterialReturnDetails.itemname,  ");
				sb.append(" MaterialReturnDetails.IndentNo,UserName,MaterialReturnDetails.RequiredQty,to_char(MaterialReturnDetails.EntryDate,'yyyymmdd')   ");
				sb.append(" order by 2   ");
			

            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(sb.toString());
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theList;
     }
     public ArrayList getMaterialIndentReturn(String SFromDate, String SToDate)
     {
            theReturnList = new ArrayList();

            StringBuffer sb = new StringBuffer();

          
		  
			sb.append(" select ITEMCODE,INDENTNO,SCRABQTY,ReceiptQty ");
            sb.append(" From MaterialScrabDetails ");
            sb.append(" Where nvl(ReceiptQty,0)<SCRABQTY ");
            sb.append(" and  to_char(MaterialScrabDetails.EntryDate,'yyyymmdd') <="+SToDate );
			
			

            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
				 
                 ResultSet  result          = theStatement.executeQuery(sb.toString());
				
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theReturnList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theReturnList;
     }
    
     private int insertReturn(){
         
         int iError = 0;
         int icheck = 0;
	    
	   StringBuffer sb = new StringBuffer();
         PreparedStatement preparedStat = null;
         java.util.HashMap theMap = new java.util.HashMap();
		 
       try{
		  
		   if(theConnection ==null) {

                ORAConnection jdbc   = ORAConnection.getORAConnection();
                theConnection        = jdbc.getConnection();
            }
			
			
				
			sb.append(" Insert into MaterialScrabDetails ( ID,ITEMCODE,ITEMNAME,RECEIPTQTY,INDENTNO,ENTRYDATE,SCRABQTY,USERCODE,FROMSTORE,SYSTEMNAME,uomcode ) ");
            	sb.append(" Values(MaterialScrabDetails_Seq.nextVal,?,?,?,?,sysdate,?,?,1,'"+common.getLocalHostName()+"',0) ");
            
	
           
		 preparedStat = theConnection.prepareStatement(sb.toString());
		 
            for (int index = 0; index < theModel.getRows(); index++) {

                theMap = (java.util.HashMap) theList.get(index);

                double dretQty = common.toDouble(common.parseNull((String)theModel.getValueAt(index,6)));       // here changed 6 to 3
				
                bselect = (Boolean)(theModel.getValueAt(index,7)); 

                    
					if(bselect.booleanValue())
					{
						
					if(dretQty>0){
						
                        double dalreadyRet = common.toDouble(common.parseNull((String)theModel.getValueAt(index,5)));
                        
						double dReqQty = common.toDouble(common.parseNull((String) theMap.get("RETURNQTY")));

                        double dNeedToRetrun = dReqQty-dalreadyRet;

                        if(dretQty>dNeedToRetrun){
							
                            icheck += 1;
							
                        }
                        else{
						
				//	  preparedStat.setString(1,common.parseNull((String) theMap.get("ID")));		
                            preparedStat.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
                            preparedStat.setString(2,common.parseNull((String) theMap.get("ITEMNAME")));
                            preparedStat.setString(3,common.parseNull((String) theMap.get("RETURNQTY")));
                            preparedStat.setString(4,common.parseNull((String) theMap.get("INDENTNO")));
					  preparedStat.setString(5,common.parseNull((String)theModel.getValueAt(index,6)));
							preparedStat.setInt(6,iUserCode);
                           
                            preparedStat.executeUpdate();
                            
                            double dtotal = (dalreadyRet+dretQty);
                            
                            
                        }
                    }
					}
            }
            if(icheck>0)
            {
               
				showMessage(" Return Quantiy is Must be equal or Less Than Required Qty");
                iError = 1;
            }

            if(preparedStat.equals("null"))
            preparedStat.close();
			
       }
         catch(Exception ex){
             iError = 1;
             ex.printStackTrace();
         }
        return iError;
     }
	
	

	
	private void updateStatus()
	{   
			try
      		{
				 if(theConnection ==null)
					{
					ORAConnection jdbc   = ORAConnection.getORAConnection();
                         theConnection        = jdbc.getConnection();
					}	

			 for(int j=0; j<theModel.getRowCount(); j++)
				{
					Boolean bValue = (Boolean)theModel.getValueAt(j, 7);
					if(bValue.booleanValue())
					{


				int iId			     = common.toInt((String)theModel.getValueAt(j, 0));
			
		     
				StringBuffer SB          = null;
				SB                 		= new StringBuffer();
			
				
				SB.append(" Update MaterialReturnDetails set ClosingStatus = 1 Where Id = "+iId+" " );
				
				System.out.println(SB.toString());
				
				PreparedStatement thePS  = theConnection.prepareStatement(SB.toString());
				thePS				.executeUpdate();
				thePS                    . close();
			}
				}
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
	}
	
	
     private double getRetAlreadyReturned(String sItemCode,String sIndnetNo)//,String sUnitCode,String sDeptCode,String sMachCode)
	 {
		  
         double dalreadyRet = 0;
         java.util.HashMap theMap = new java.util.HashMap();
		 
         for (int index = 0; index < theReturnList.size(); index++) {
             
			theMap = (java.util.HashMap) theReturnList.get(index);
             
            String sItem =  common.parseNull((String) theMap.get("ITEMCODE"));
            String sIndnent =  common.parseNull((String) theMap.get("INDENTNO"));
            
            if(sItemCode.equals(sItem) && sIndnetNo.equals(sIndnent) )
			{ 
                    
			dalreadyRet		+= common.toDouble(common.parseNull((String) theMap.get("SCRABQTY")));
			    
            }
			
        }
         return dalreadyRet;
		 
     }
     private void searchItems()
     {
          String SSearchStr        = common.parseNull(searchField.getText()).trim();

          int iIndex               = -1;

          for(int i=0; i<theModel.getRowCount(); i++)
          {
               String SItemName    = common.parseNull((String)theModel.getValueAt(i, 2));

               if(SItemName.startsWith(SSearchStr.toUpperCase()))
               {
                    iIndex         = i;
                    break;
               }
          }
          if(iIndex != -1)
          {
                 theTable       . setRowSelectionInterval(iIndex, iIndex);
                 Rectangle theRect   = theTable.getCellRect(iIndex, 0, true);
                 theTable       . scrollRectToVisible(theRect);
                 theTable       . setSelectionBackground(Color.green);			   
          }
     } 
}

