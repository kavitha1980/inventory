package Indent.IndentFiles.NonStockReceipt;

import javax.swing.table.*;
import java.util.*;

public class MaterialReceiptReportModel extends DefaultTableModel
{

     String ColumnName[]   = {"Id","ItemCode","ItemName","Return Qty","IndentNo","ScrabQty","Click"};
     String ColumnType[]   = {"N","S","S","S","S","S","B"};
     int  iColumnWidth[] = {8,8,80,5,5,5,3};


     public MaterialReceiptReportModel()
     {
          setDataVector(getRowData(),ColumnName);
     }

     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";
		
     			
          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}
