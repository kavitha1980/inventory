package Indent.IndentFiles.NonStockReceipt;

import guiutil.FinDateField;
import jdbc.ORAConnection;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;

import util.*;
import javax.swing.table.TableColumnModel;

public class NewDirectScrapIssueDetails extends JInternalFrame 
{

     protected     JLayeredPane Layer;

     JPanel        TopPanel,MiddlePanel,BottomPanel,ScrapPanel,PreReceiptScrapPanel;
     JButton       BApply;
     JButton       BExit;
     JButton       BSave,BSelect;
     JTextField    searchField;
     Common        common = new Common();
     Connection     theConnection;
	
     int           iUserCode=0,iMillCode,iOwnerCode,iid;
     int PRINT=9;
     
     FinDateField TFromDate,TToDate;

     ArrayList theList = new ArrayList();
     ArrayList theReturnList = new ArrayList();

     NewScrapIssueDetailsModel   theModel;
	 DirectScrapIssueDetailsModel	theModel1;

     JTable             theTable,theTable1;
	 Font 		  font;
	 Boolean bselect;
	 ArrayList ADataList;
     
     public NewDirectScrapIssueDetails(JLayeredPane Layer,int iUserCode,int iMillCode,int iOwnerCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
		  this.iOwnerCode = iOwnerCode;

          createComponents();
          setLayouts();
          addComponents();
		  addListeners();
		  setPrevTableData();
     }

     private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               
              
			   theModel1       = new DirectScrapIssueDetailsModel();
               theTable1        = new JTable(theModel1);
               
               
               TableColumnModel TC1  = theTable1.getColumnModel();

                for(int i=0;i<theModel1.ColumnName.length;i++)   {

                    TC1 .getColumn(i).setPreferredWidth(theModel1.iColumnWidth[i]);
                }

               TFromDate      = new FinDateField();
               TToDate        = new FinDateField();

               TFromDate      .   setTodayDate();
               TToDate        .   setTodayDate();
               
               BExit          = new JButton("Exit");
               BApply         = new JButton("Apply");
               BSave          = new JButton("Save");
			   BSelect		  = new JButton("Select All");
               
               searchField    = new JTextField();
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }
     }

     private void setLayouts()
     {
            setTitle("Material Scrab Issue Against Return ");
            setClosable(true);                    
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,775,600);

            TopPanel.setLayout(new GridLayout(2,2));
            MiddlePanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());

            TopPanel.setBorder(new TitledBorder("Info."));
            MiddlePanel.setBorder(new TitledBorder("Details"));
            BottomPanel.setBorder(new TitledBorder("Controls"));

            TopPanel            . setBackground(new Color(213,234,255));
            MiddlePanel         .setBackground(new Color(213,234,255));
            BottomPanel         . setBackground(new Color(213,234,255));
			
    }

     private void addComponents()
     {

        
		TopPanel            . add(new JLabel(""));
		TopPanel            . add(new JLabel(""));
		TopPanel            . add(new JLabel("Search"));
        TopPanel            . add(searchField);
        
        TopPanel            . add(new JLabel("Select"));
        TopPanel            . add(BSelect);
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));

        MiddlePanel.add(new JScrollPane(theTable1));
		
        BottomPanel.add(BExit);
        BottomPanel.add(BSave);

        getContentPane().add("North",TopPanel);
        getContentPane().add("Center",MiddlePanel);
        getContentPane().add("South",BottomPanel);
        BExit.setMnemonic('E');
        BSave.setMnemonic('S');

     }

    private void addListeners()
    {
          BExit.addActionListener(new ActList());
          BSave.addActionListener(new ActList());
		  BSelect . addActionListener(new ActList());
          searchField. addKeyListener(new SearchKeyList());
    }

    
	  public void setPrevTableData()
    {
          try
          {
			  
			theModel1.setNumRows(0);  
			setDataVector();
			
				for(int i=0;i<ADataList.size();i++)
                {
                     HashMap  theMap =(HashMap)ADataList.get(i);
					 String SScrapType  = common.parseNull((String)theMap.get("SECONDSALESSTATUS"));

                      ArrayList theList;
               
					   theList                     = new ArrayList();
					   theList                     . clear();
					   theList                     . add(i+1);
					   theList                     . add(common.parseNull((String)theMap.get("ID")));
					   theList                     . add((String)theMap.get("ITEMCODE"));
					   theList                     . add((String)theMap.get("ITEMNAME"));
					   theList                     . add((String)theMap.get("DEPTNAME"));
					   theList                     . add((String)theMap.get("UNITNAME"));
					   theList                     . add((String)theMap.get("MACHINENAME"));
					   theList                     . add(common.parseNull((String)theMap.get("USERNAME")));
					   theList                     . add(common.parseDate((String)theMap.get("REQUIREDQTY")));
					   theList                    . add(common.parseDate((String)theMap.get("RETURNQTY")));
					   theList                     . add(common.parseNull((String)theMap.get("NAME")));
					  
					   if(SScrapType.equals("0"))
					   {
						  theList                     . add("SCRAP"); 
					   }
					   else
					   {
						   theList                     . add("SECOND SALES"); 
					   }
					   theList.add(new Boolean(true));
					   
					   theModel1                      . appendRow(new Vector(theList));
					   theList                         = null;

                    
               }
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
	 
 private void setDataVector()
 {
		 ADataList = new ArrayList();
		 
		 try
		 {
			  if(theConnection ==null) 
			  {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
			  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(getQS());
				 ResultSetMetaData rsmd     = result.getMetaData();
				 
			while(result.next())
			{
				 HashMap theMap = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           theMap.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      ADataList.add(theMap);
                 
            }
			result.close();
			theStatement.close();
		 }
            
		catch(Exception ex)
		{
			  ex.printStackTrace();
		}
	}				
		
	 
	 private String getQS()
	 {
		 String QS = " select ReturnQty,    PreReceiptScrapTransfer.id,   itemcode, itemname   , UserName,  Entryusercode ,  RequiredQty,"+    
					 " EntryDate, materialindentreturntype.name, returntype ,DeptName,UnitName , nvl(Machine.Mach_Name,'General')  as MACHINENAME,UnitCode,"+   
					 " DeptCode,MachineCode,SecondSalesStatus From PreReceiptScrapTransfer    "+ 
					 " Inner join materialindentreturntype on materialindentreturntype.code = PreReceiptScrapTransfer.returntype "+ 
					 " Inner join Scm.Rawuser on Rawuser.usercode = PreReceiptScrapTransfer.Entryusercode   "+ 
					 " Inner Join process_units On process_units.UnitCode = PreReceiptScrapTransfer.UnitCode    "+ 
					 " Inner Join HrdNew.Department On Department.DeptCode = PreReceiptScrapTransfer.DeptCode  "+ 
					 " Left Join Machine On Machine.Mach_Code = PreReceiptScrapTransfer.MachineCode  "+    
					 "  Where PreReceiptScrapTransfer.EntryDate  "+  
					 " <=to_char(sysdate,'yyyymmdd')  and "+  
					 " PreReceiptScrapTransfer.TransferStatus=0  "+   
					 " and PreReceiptScrapTransfer.StoreReceiptStatus = 1  "+ 
					 " order by 2   ";
					 
		return QS;			 
	 }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
              
               if(ae.getSource()==BSave)
               {
					if((JOptionPane.showConfirmDialog(null,"Do U want to Save? ","Information",JOptionPane.YES_NO_OPTION))== 0)
					{
						
						insertPreReturn();
						
					}
					
					
				}
             
               if(ae.getSource()==BExit)
               {
                    if((JOptionPane.showConfirmDialog(null,"Do U want to Exit? ","Warning",JOptionPane.YES_NO_OPTION))== 0)
					{
						removeHelpFrame();
					}	
					else
					{
						return;
					}
               }
			   if(ae.getSource()==BSelect)
			  {
					getSelection();
			  }

          }
     }
    
     private class SearchKeyList extends KeyAdapter
        {
            public void keyReleased(KeyEvent ke)
            {
                searchItems();     
            }

            public void keyPressed(KeyEvent ke)
            {
                searchItems();
            }    

            public void keyTyped(KeyEvent ke)
            {
                searchItems();
            }
        }

    private void showMessage(String sMsg)
    {
          JOptionPane.showMessageDialog(null,getMessage(sMsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
    }
    private String getMessage(String sMsg)
    {
          String str = "<html><body>";
          str = str + sMsg;
          str = str + "</body></html>";
          return str;
    }
	
    private void removeHelpFrame()
    {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     
	private void insertPreReturn(){
         
		 Connection theCommitConnection = null;
		 int iCount =0;
		 
       
	   try
	   {
		  
			Class.forName("oracle.jdbc.OracleDriver");
			theCommitConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");   
		
			theCommitConnection.setAutoCommit(false);
			
			 for(int i=0; i<theModel1.getRowCount(); i++)
			 {
					HashMap theMap = (HashMap)ADataList.get(i);
					Boolean bValue = (Boolean)theModel1.getValueAt(i, 12);
					
					if(bValue.booleanValue())
					{

						String SItemCode 		=   common.parseNull((String)theModel1.getValueAt(i, 2));
						String SItemName 		=   common.parseNull((String)theModel1.getValueAt(i, 3));
						int iReceiptQty 		=   common.toInt((String)theModel1.getValueAt(i, 8));
						int iScrabQty 			=   common.toInt((String)theModel1.getValueAt(i, 9));
						int iReturnTypeCode 	=   common.toInt((String)theMap.get("RETURNTYPE"));
						int iId			        =   common.toInt((String)theModel1.getValueAt(i, 1));
						int iDeptCode			=   common.toInt((String)theMap.get("DEPTCODE"));
						int iUnitCode			=   common.toInt((String)theMap.get("UNITCODE"));
						int iMachineCode		=   common.toInt((String)theMap.get("MACHINECODE"));
						String SMachineName	    =   common.parseNull((String)theModel1.getValueAt(i, 6));
						int iScrapTypeCode		=   common.toInt((String)theMap.get("SECONDSALESSTATUS"));
						int iUCode				=   common.toInt((String)theMap.get("ENTRYUSERCODE")); 
						System.out.println("iUCode-->"+iUCode);
			
						StringBuffer Sb          = null;
						Sb                 		 = new StringBuffer();
						
						StringBuffer SB          = null;
						SB                 		 = new StringBuffer();
					    
						
						
						Sb.append(" Insert into ScrapIssueDetails ( ID,ITEMCODE,ITEMNAME,RECEIPTQTY,UNITCODE,DEPTCODE,MACHINECODE,USERCODE,ENTRYDATE,SCRABQTY,MACHINENAME,FROMSTORE,SYSTEMNAME,CLOSINGSTATUS,SECONDSALESSTATUS,StoreUserCode ) ");
						Sb.append(" Values(SCRAPISSUEDETAILS_SEQ.nextVal,'"+SItemCode+"','"+SItemName+"',"+iReceiptQty+","+iUnitCode+","+iDeptCode+","+iMachineCode+","+iUCode+",sysdate,"+iScrabQty+",'"+SMachineName+"',1,'"+common.getLocalHostName()+"',1,"+iScrapTypeCode+" ,"+iUserCode+") ");
						
						
						System.out.println("QS-->"+Sb.toString());
						
						SB.append(" Update PreReceiptScrapTransfer set TransferStatus = 1 Where Id = "+iId+" " );
				
							
							PreparedStatement thePSS  = theCommitConnection.prepareStatement(Sb.toString());
							thePSS					  .executeUpdate();
							thePSS                    . close();
							
							PreparedStatement thePS  = theConnection.prepareStatement(SB.toString());
							thePS					 .executeUpdate();
							thePS                    . close();
							
							iCount++;
					}
			 }
			 setPrevTableData();
						
				theCommitConnection.commit();
				theCommitConnection.setAutoCommit(true);
				
				if(iCount>0)
				{
					JOptionPane.showMessageDialog(null, "Data Updated Successfully!!", "Information", JOptionPane.INFORMATION_MESSAGE);
				}
				
				
				
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			
				   try
				   {
					theCommitConnection.rollback();  
					theCommitConnection.setAutoCommit(true);
					
					JOptionPane.showMessageDialog(null, "Data Not Saved.. Error :"+ex, "Information", JOptionPane.INFORMATION_MESSAGE);
				   }
				   catch(Exception ex1)
				   {}
			}
			
	
	}
	
   
     private void searchItems()
     {
          String SSearchStr        = common.parseNull(searchField.getText()).trim();

          int iIndex               = -1;

          for(int i=0; i<theModel1.getRowCount(); i++)
          {
               String SItemName    = common.parseNull((String)theModel1.getValueAt(i, 3));

               if(SItemName.startsWith(SSearchStr.toUpperCase()))
               {
                    iIndex         = i;
                    break;
               }
          }
          if(iIndex != -1)
          {
                 theTable1       . setRowSelectionInterval(iIndex, iIndex);
                 Rectangle theRect   = theTable1.getCellRect(iIndex, 0, true);
                 theTable1       . scrollRectToVisible(theRect);
                 theTable1      . setSelectionBackground(Color.green);			   
          }
     } 
	 
	 
private void getSelection()
	{
    	
		for (int i = 0; i < theModel1.getRowCount(); i++) 
		{
        	if(((Boolean)theModel1.getValueAt(i,12)))
			{
				 theModel1.setValueAt(new Boolean(false), i,12);
			}
            else
			{
              theModel1.setValueAt(new Boolean(true), i,12);
			}
		  
		}
    
    }  	
}

