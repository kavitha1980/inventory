package Indent.IndentFiles.NonStockReceipt;

import guiutil.FinDateField;
import jdbc.ORAConnection;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;

import util.*;
import javax.swing.table.TableColumnModel;

public class NewScrapIssueDetails extends JInternalFrame 
{

     protected     JLayeredPane Layer;

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BApply;
     JButton       BExit;
     JButton       BSave,BSelect;
     JTextField    searchField;
     Common        common = new Common();
     Connection     theConnection;
	
     int           iUserCode=0,iMillCode,iOwnerCode,iid;
     int PRINT=9;
     
     FinDateField TFromDate,TToDate;

     ArrayList theList = new ArrayList();
     ArrayList theReturnList = new ArrayList();

     NewScrapIssueDetailsModel   theModel;

     JTable             theTable;
	 Boolean bselect;
     
     public NewScrapIssueDetails(JLayeredPane Layer,int iUserCode,int iMillCode,int iOwnerCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
		  this.iOwnerCode = iOwnerCode;

          createComponents();
          setLayouts();
          addComponents();
		  addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }

     private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();

               theModel       = new NewScrapIssueDetailsModel();
               theTable       = new JTable(theModel);
               
               
               TableColumnModel TC  = theTable.getColumnModel();

                for(int i=0;i<theModel.ColumnName.length;i++)   {

                    TC .getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
                }

               TFromDate      = new FinDateField();
               TToDate        = new FinDateField();

               TFromDate      .   setTodayDate();
               TToDate        .   setTodayDate();
               
               BExit          = new JButton("Exit");
               BApply         = new JButton("Apply");
               BSave          = new JButton("Save");
			   BSelect		  = new JButton("Select All");
               
               searchField    = new JTextField();
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }
     }

     private void setLayouts()
     {
            setTitle("Material Scrab Issue Against Return ");
            setClosable(true);                    
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,775,600);

            TopPanel.setLayout(new GridLayout(2,2));
            MiddlePanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());

            TopPanel.setBorder(new TitledBorder("Info."));
            MiddlePanel.setBorder(new TitledBorder("Details"));
            BottomPanel.setBorder(new TitledBorder("Controls"));

            TopPanel            . setBackground(new Color(213,234,255));
            MiddlePanel         .setBackground(new Color(213,234,255));
            BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

        
		TopPanel            . add(new JLabel(""));
		TopPanel            . add(new JLabel(""));
		TopPanel            . add(new JLabel("Search"));
        TopPanel            . add(searchField);
        
        TopPanel            . add(new JLabel("Select"));
        TopPanel            . add(BSelect);
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));

        MiddlePanel.add(new JScrollPane(theTable));

        BottomPanel.add(BExit);
        BottomPanel.add(BSave);

        getContentPane().add("North",TopPanel);
        getContentPane().add("Center",MiddlePanel);
        getContentPane().add("South",BottomPanel);
        BExit.setMnemonic('E');
        BSave.setMnemonic('S');

     }

    private void addListeners()
    {
          BExit.addActionListener(new ActList());
          BSave.addActionListener(new ActList());
		  BSelect . addActionListener(new ActList());
          searchField. addKeyListener(new SearchKeyList());
    }

    public void setTabReport()
    {
          try
          {

                String SFromDate = common.pureDate((String)TFromDate.toNormal()); 
                String SToDate = common.pureDate((String)TToDate.toNormal()); 
			

                ArrayList ADetails = getMaterialIndent(SFromDate,SToDate);
				System.out.println("ADetails.size():"+ADetails.size());

                for(int i=0;i<ADetails.size();i++)
                {
					
                     HashMap  row =(HashMap)ADetails.get(i);

                     Vector VTheVect = new Vector();
                     
                     String sItemCode = common.parseNull((String)row.get("ITEMCODE"));
                     String sIndentNo = common.parseNull((String)row.get("INDENTNO"));
					 
					VTheVect.addElement(i+1); 
					VTheVect.addElement(common.parseNull((String)row.get("ID")));
					VTheVect.addElement(common.parseNull((String)row.get("ITEMCODE")));
					VTheVect.addElement(common.parseNull((String)row.get("ITEMNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("DEPTNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("UNITNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("MACHINENAME")));
					VTheVect.addElement(common.parseNull((String)row.get("USERNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("RETURNQTY")));
					VTheVect.addElement(common.parseNull((String)row.get("INDENTNO")));
					VTheVect.addElement(common.parseNull((String)row.get("RETURNQTY")));
					VTheVect.addElement(common.parseNull((String)row.get("NAME")));
					VTheVect.addElement(new Boolean(true));

					theModel.appendRow(VTheVect); 
					 
					 
               }
			   
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
              
               if(ae.getSource()==BSave)
               {
					if((JOptionPane.showConfirmDialog(null,"Do U want to Save? ","Information",JOptionPane.YES_NO_OPTION))== 0)
					{
						insertReturn();
						theModel.setNumRows(0);
						setTabReport();
						System.out.println("Call Tab Report");
					}
					
					
				}
             
               if(ae.getSource()==BExit)
               {
                    if((JOptionPane.showConfirmDialog(null,"Do U want to Exit? ","Warning",JOptionPane.YES_NO_OPTION))== 0)
					{
						removeHelpFrame();
					}	
					else
					{
						return;
					}
               }
			   if(ae.getSource()==BSelect)
			  {
					getSelection();
			  }

          }
     }
    
     private class SearchKeyList extends KeyAdapter
        {
            public void keyReleased(KeyEvent ke)
            {
                searchItems();     
            }

            public void keyPressed(KeyEvent ke)
            {
                searchItems();
            }    

            public void keyTyped(KeyEvent ke)
            {
                searchItems();
            }
        }

    private void showMessage(String sMsg)
    {
          JOptionPane.showMessageDialog(null,getMessage(sMsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
    }
    private String getMessage(String sMsg)
    {
          String str = "<html><body>";
          str = str + sMsg;
          str = str + "</body></html>";
          return str;
    }
	
    private void removeHelpFrame()
    {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public ArrayList getMaterialIndent(String SFromDate, String SToDate)
     {
            theList = new ArrayList();
            StringBuffer sb = new StringBuffer();

				//commented By Ananthi 08.10.2020 for add Username,DeptName.UnitName,MachineName			 
				
				/*sb.append(" select sum(MaterialReturnDetails.RETURNQTY) as RETURNQTY ,   ");
				sb.append(" Materialreturndetails.id,  ");
				sb.append(" MaterialReturnDetails.itemcode,MaterialReturnDetails.itemname   ");
				sb.append(" ,MaterialReturnDetails.IndentNo,UserName,   ");
				sb.append(" MaterialReturnDetails.RequiredQty,   ");
				sb.append(" to_char(MaterialReturnDetails.EntryDate,'yyyymmdd') as EntryDate,materialindentreturntype.name,MaterialReturnDetails.returntypecode   ");
				sb.append(" From MaterialReturnDetails   ");
				sb.append(" Inner join Scm.Rawuser on Rawuser.usercode = MaterialReturnDetails.usercode  ");
				sb.append(" Inner join materialindentreturntype on materialindentreturntype.code = MaterialReturnDetails.returntypecode ");
				sb.append(" Where to_char(MaterialReturnDetails.EntryDate,'yyyymmdd')   ");
				sb.append(" <=to_char(sysdate,'yyyymmdd')  and MaterialReturnDetails.RETURNTYPE=1   ");
				sb.append(" and MaterialReturnDetails.closingstatus = 0 and scrapstatus = 0 and returntypecode not in (12,13,14)  ");//and returntypecode not in (12,13,14)  ");
				sb.append(" group by materialreturndetails.id,MaterialReturnDetails.itemcode,MaterialReturnDetails.itemname,  ");
				sb.append(" MaterialReturnDetails.IndentNo,UserName,MaterialReturnDetails.RequiredQty,to_char(MaterialReturnDetails.EntryDate,'yyyymmdd')   ");
				sb.append(" ,materialindentreturntype.name,MaterialReturnDetails.returntypecode order by 2 ");*/
				
				
				//Added By Ananthi 08.10.2020 for add Username,DeptName.UnitName,MachineName	

				sb.append(" select sum(MaterialReturnDetails.RETURNQTY) as RETURNQTY ,  ");  
				sb.append(" Materialreturndetails.id,   ");
				sb.append(" MaterialReturnDetails.itemcode, ");
				sb.append(" MaterialReturnDetails.itemname   "); 
				sb.append(" ,MaterialReturnDetails.IndentNo, ");
				sb.append(" UserName,  ");
				sb.append(" materialindentdetails.usercode ,  ");
				sb.append(" MaterialReturnDetails.RequiredQty, ");   
				sb.append(" to_char(MaterialReturnDetails.EntryDate,'yyyymmdd') as EntryDate, ");
				sb.append(" materialindentreturntype.name, ");
				sb.append(" MaterialReturnDetails.returntypecode ,DeptName,UnitName , ");
				sb.append(" nvl(Machine.Mach_Name,'General')  as MACHINENAME,MaterialIndentDetails.Unit_Code,  ");
				sb.append(" MaterialIndentDetails.DeptCode,MaterialIndentDetails.MachineCode ");
				sb.append(" From MaterialReturnDetails    ");
				sb.append(" Inner join materialindentreturntype on materialindentreturntype.code = MaterialReturnDetails.returntypecode "); 
				sb.append(" Inner join materialindentdetails on materialindentdetails.itemcode = MaterialReturnDetails.Itemcode ");
				sb.append(" and materialindentdetails.IndentNo=MaterialReturnDetails.IndentNo ");
				sb.append(" and materialindentdetails.Unit_code=MaterialReturnDetails.Unit_code ");
				sb.append(" and materialindentdetails.Deptcode=MaterialReturnDetails.Deptcode ");
				sb.append(" and materialindentdetails.MACHINECODE = MaterialReturnDetails.MACHINECODE ");
				sb.append(" and materialindentdetails.RequiredQty = MaterialReturnDetails.RequiredQty ");
				sb.append(" Inner join Scm.Rawuser on Rawuser.usercode = materialindentdetails.usercode  "); 
				sb.append(" Inner Join process_units On process_units.UnitCode = MaterialIndentDetails.Unit_Code  ");  
				sb.append(" Inner Join HrdNew.Department On Department.DeptCode = MaterialIndentDetails.DeptCode  ");
				sb.append(" Left Join Machine On Machine.Mach_Code = MaterialIndentDetails.MachineCode  ");   
				sb.append(" Where to_char(MaterialReturnDetails.EntryDate,'yyyymmdd')    ");
				sb.append(" <=to_char(sysdate,'yyyymmdd')  and  ");
				sb.append(" MaterialReturnDetails.RETURNTYPE=1    ");
				sb.append(" and MaterialReturnDetails.closingstatus = 0  ");
				sb.append(" and MaterialReturnDetails.scrapstatus = 0 and MaterialReturnDetails.returntypecode not in (12,13,14)  ");//and MaterialReturnDetails.itemcode='A09015032' ");
				sb.append(" group by materialreturndetails.id,MaterialReturnDetails.itemcode,MaterialReturnDetails.itemname,   ");
				sb.append(" MaterialReturnDetails.IndentNo,UserName,MaterialReturnDetails.RequiredQty,to_char(MaterialReturnDetails.EntryDate,'yyyymmdd') ");   
				sb.append(" ,materialindentreturntype.name,MaterialReturnDetails.returntypecode,materialindentdetails.usercode ");
				sb.append(" ,DeptName,UnitName,Machine.Mach_Name,MaterialIndentDetails.Unit_Code,MaterialIndentDetails.DeptCode,MaterialIndentDetails.MachineCode   order by 2  ");
			

            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(sb.toString());
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theList;
     }
   
    
   private void insertReturn(){
         
		 Connection theCommitConnection = null;
       
	   try
	   {
		  
			Class.forName("oracle.jdbc.OracleDriver");
			theCommitConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");   
		
			theCommitConnection.setAutoCommit(false);
			
			 for(int k=0; k<theModel.getRowCount(); k++)
			 {
					HashMap theMap = (HashMap)theList.get(k);
					Boolean bValue = (Boolean)theModel.getValueAt(k, 12);
					
					if(bValue.booleanValue())
					{

						String SItemCode 		=   common.parseNull((String)theModel.getValueAt(k, 2));
						String SItemName 		=   common.parseNull((String)theModel.getValueAt(k, 3));
						int iReceiptQty 		=   common.toInt((String)theModel.getValueAt(k, 8));
						int iIndentNo 			=   common.toInt((String)theModel.getValueAt(k, 9));
						int iScrabQty 			=   common.toInt((String)theModel.getValueAt(k, 10));
						int iReturnTypeCode 	=   common.toInt((String)theMap.get("RETURNTYPECODE"));
						int iId			        =   common.toInt((String)theModel.getValueAt(k, 1));
						int iDeptCode			=   common.toInt((String)theMap.get("DEPTCODE"));
						int iUnitCode			=   common.toInt((String)theMap.get("UNIT_CODE"));
						int iMachineCode		=   common.toInt((String)theMap.get("MACHINECODE"));
						int iUCode				=   common.toInt((String)theMap.get("USERCODE"));
						System.out.println("iReturnTypeCode-->"+iReturnTypeCode);
			
						StringBuffer Sb          = null;
						Sb                 		 = new StringBuffer();
						
						StringBuffer SB          = null;
						SB                 		 = new StringBuffer();
					    
						//String SDate 			= "2020-07-31 10:18:21";
						//	System.out.println("SDate-->"+SDate);
						
						SItemName =  SItemName.replace("'","");
						
						if(iReturnTypeCode==12 || iReturnTypeCode==13 || iReturnTypeCode==14)
						{
							Sb.append(" Insert into MaterialScrabDetails ( ID,ITEMCODE,ITEMNAME,RECEIPTQTY,INDENTNO,ENTRYDATE,SCRABQTY,USERCODE,FROMSTORE,SYSTEMNAME,uomcode,SecondSalesStatus,Unit_Code,DeptCode,MachineCode ) ");
							Sb.append(" Values(MaterialScrabDetails_Seq.nextVal,'"+SItemCode+"','"+SItemName+"',"+iReceiptQty+","+iIndentNo+",sysdate,"+iScrabQty+","+iUCode+",1,'"+common.getLocalHostName()+"',0,1,"+iUnitCode+","+iDeptCode+","+iMachineCode+") ");
						}
						else
						{
							Sb.append(" Insert into MaterialScrabDetails ( ID,ITEMCODE,ITEMNAME,RECEIPTQTY,INDENTNO,ENTRYDATE,SCRABQTY,USERCODE,FROMSTORE,SYSTEMNAME,uomcode,Unit_Code,DeptCode,MachineCode ) ");
							Sb.append(" Values(MaterialScrabDetails_Seq.nextVal,'"+SItemCode+"','"+SItemName+"',"+iReceiptQty+","+iIndentNo+",sysdate,"+iScrabQty+","+iUCode+",1,'"+common.getLocalHostName()+"',0,"+iUnitCode+","+iDeptCode+","+iMachineCode+") ");
						}
						
						SB.append(" Update MaterialReturnDetails set ClosingStatus = 1 Where Id = "+iId+" " );
				
							//System.out.println(SB.toString());
							System.out.println(Sb.toString());
							
							PreparedStatement thePSS  = theCommitConnection.prepareStatement(Sb.toString());
							thePSS					  .executeUpdate();
							thePSS                    . close();
							
							PreparedStatement thePS  = theConnection.prepareStatement(SB.toString());
							thePS					 .executeUpdate();
							thePS                    . close();
					}
			 }
				theCommitConnection.commit();
				theCommitConnection.setAutoCommit(true);
				
				JOptionPane.showMessageDialog(null, "Data Updated Successfully!!", "Information", JOptionPane.INFORMATION_MESSAGE);
				
				
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			
				   try
				   {
					theCommitConnection.rollback();  
					theCommitConnection.setAutoCommit(true);
					
					JOptionPane.showMessageDialog(null, "Data Not Saved.. Error :"+ex, "Information", JOptionPane.INFORMATION_MESSAGE);
				   }
				   catch(Exception ex1)
				   {}
			}
			
	
	}
	
   
     private void searchItems()
     {
          String SSearchStr        = common.parseNull(searchField.getText()).trim();

          int iIndex               = -1;

          for(int i=0; i<theModel.getRowCount(); i++)
          {
               String SItemName    = common.parseNull((String)theModel.getValueAt(i, 3));

               if(SItemName.startsWith(SSearchStr.toUpperCase()))
               {
                    iIndex         = i;
                    break;
               }
          }
          if(iIndex != -1)
          {
                 theTable       . setRowSelectionInterval(iIndex, iIndex);
                 Rectangle theRect   = theTable.getCellRect(iIndex, 0, true);
                 theTable       . scrollRectToVisible(theRect);
                 theTable       . setSelectionBackground(Color.green);			   
          }
     } 
	 
	 
private void getSelection()
	{
    	int iCol=theModel.getColumnCount();
  
		for (int rowIndex = 0; rowIndex < theModel.getRowCount(); rowIndex++) 
		{
        	if(((Boolean)theModel.getValueAt(rowIndex,12)))
			{
				 theModel.setValueAt(new Boolean(false), rowIndex,12);
			}
            else
			{
              theModel.setValueAt(new Boolean(true), rowIndex,12);
			}
		  
		}
    
    }  	
}

