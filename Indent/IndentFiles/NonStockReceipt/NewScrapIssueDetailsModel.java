package Indent.IndentFiles.NonStockReceipt;

import javax.swing.table.*;
import java.util.*;

public class NewScrapIssueDetailsModel extends DefaultTableModel
{

     String ColumnName[]   = {"S.No","Id","ItemCode","ItemName","DeptName","UnitName","MachineName","UserName","Return Qty","IndentNo","ScrabQty","ReturnType","Click"};
     String ColumnType[]   = {"N","N","S","S","S","S","S","S","S","S","S","S","B"};
     int  iColumnWidth[] = {8,8,20,50,20,10,10,20,10,10,10,10,3};


     public NewScrapIssueDetailsModel()
     {
          setDataVector(getRowData(),ColumnName);
     }

     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";
		
     			
          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}
