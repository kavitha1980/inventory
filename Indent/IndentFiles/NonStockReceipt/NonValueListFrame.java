package Indent.IndentFiles.NonStockReceipt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;



public class NonValueListFrame extends JInternalFrame 
{

     protected     JLayeredPane Layer;
	 

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BExit,BApply,BSave;
	DateField      TStDate;
     DateField      TEnDate; //TFromDate,TToDate;
	
	

	JComboBox     CGroup,CItem;
     Common        common = new Common();
     Connection     theConnection;
	ArrayList   AGroupName,AGroupCode,AItemName,AItemCode;
	
	int           iUserCode=0;
		int           iMillCode=0;

     int PRINT=13;
	

     
	String SItemName ;
	
	String SItemCode;
	
	String SItemCod;

     ArrayList theList = new ArrayList();

     NonValueListModel   theModel;

     JTable             theTable;

 
    public NonValueListFrame(JLayeredPane Layer,int iUserCode,int iMillCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
		          this.iMillCode = iMillCode;


		setData();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }


	private void setData()
	{
		AGroupName			= null;
		AGroupCode			= null;
		AItemName				= null;
		AItemCode				= null;
		
		AGroupName			= new ArrayList();
		AGroupCode			= new ArrayList();
		AItemName			= new ArrayList();
		AItemCode			= new ArrayList();
		
		AGroupName			. clear();
		AGroupCode			. clear();
		AItemName			. clear();
		AItemCode			. clear();
		
		AGroupName			. add("All");
		AGroupCode			. add("9999999");
		AItemName			. add("All");
		AItemCode			. add("9999999");

		try
		{
			if(theConnection ==null) {

				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
			Statement theStatement = theConnection.createStatement();
			
			String QS = " Select groupname,groupcode from stockgroup order by 1 ";
			ResultSet rs = theStatement.executeQuery(QS);

			while (rs.next())
			{
				AGroupName		. add(common.parseNull(rs.getString(1)));
				AGroupCode		. add(common.parseNull(rs.getString(2)));
				
			}
			rs.close();
			
			String QS33 = " Select item_name,item_code from invitems  order by 1 ";
			ResultSet res = theStatement.executeQuery(QS33);

			while (res.next())
			{
				AItemName		. add(common.parseNull(res.getString(1)));
				AItemCode		. add(common.parseNull(res.getString(2)));
				
			}
			res.close();
			
			theStatement 		.close();
		}
		catch(Exception ex)
			{
			System.out.println("From StockReceiptListFrame "+ex);
			}
	}
	
	
	
	private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();
			
               theModel       = new NonValueListModel();
               theTable       = new JTable(theModel);

			TStDate      = new DateField();
			TEnDate        = new DateField();
			TStDate.setTodayDate();
			TEnDate.setTodayDate();
			
			CGroup   	= new JComboBox(new Vector(AGroupName));	
			CItem   	= new JComboBox(new Vector(AItemName));	

               BExit          = new JButton("Exit");
			BApply          = new JButton("Apply");
			BSave          = new JButton("Save");

			
          }
          catch(Exception e)
          {
               System.out.println(e);
			e.printStackTrace();
		}
     }

     private void setLayouts()
     {
          setTitle("Non Value items ");
          setClosable(true);                    
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,1100,600);

          TopPanel.setLayout(new GridLayout(8,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder("Info."));
          MiddlePanel.setBorder(new TitledBorder("Details"));
          BottomPanel.setBorder(new TitledBorder("Controls"));
    
		TopPanel            . setBackground(new Color(213,234,255));
		MiddlePanel         .setBackground(new Color(213,234,255));
		BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

          TopPanel            . add(new JLabel("FromDate"));
	     TopPanel            . add(TStDate);
		TopPanel            . add(new JLabel("ToDate"));
	     TopPanel            . add(TEnDate);
		
		TopPanel            . add(new JLabel("Group"));
	     TopPanel            . add(CGroup);
		TopPanel            . add(new JLabel("Item"));
	     TopPanel            . add(CItem);
		TopPanel            . add(new JLabel(""));
	     TopPanel            . add(BApply);
	     TopPanel            . add(new JLabel(""));
	     TopPanel            . add(new JLabel(""));
	    	TopPanel            . add(new JLabel(""));
	     TopPanel            . add(new JLabel(""));
	     TopPanel            . add(new JLabel(""));
	     TopPanel			.add(new JLabel(""));
	     
		
		MiddlePanel.add(new JScrollPane(theTable));

          BottomPanel.add(BExit);
          BottomPanel.add(BSave);


          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);


          BExit.setMnemonic('E');
     }

	 private void addListeners()
     {
          BExit.addActionListener(new ActList());
          BApply.addActionListener(new ActList());
		BSave.addActionListener(new ActList());


     }                 

	 public void setTabReport()
     {
          try
          {
		
			String SStDate = common.pureDate((String)TStDate.toNormal()); 
			String SEnDate = common.pureDate((String)TEnDate.toNormal()); 
			
			
			
			String SGroup = common.parseNull((String)CGroup.getSelectedItem());
			
			System.out.println(SGroup);
			String SGroupCode = "";
			if (!SGroupCode.equals("All"))
				SGroupCode= common.parseNull((String)AGroupCode.get(AGroupName.indexOf(SGroup)));
			
			String SItem = common.parseNull((String)CItem.getSelectedItem());
			
			System.out.println(SItem);
			String SItemCod = "";
			if (!SItemCod.equals("All"))
				SItemCod= common.parseNull((String)AItemCode.get(AItemName.indexOf(SItem)));
			
			


		theModel.setNumRows(0);	
                
			 ArrayList VDetails = getMaterialIndent(SItemCod,SGroupCode,SStDate,SEnDate,iMillCode);

                    for(int i=0;i<VDetails.size();i++)
               {
                    HashMap  row =(HashMap)VDetails.get(i);  

					String SItemCode = common.parseNull((String)row.get("ITEM_CODE"));
					
					

                     Vector VTheVect = new Vector();
				 VTheVect.addElement(String.valueOf(i+1));
				 VTheVect.addElement(common.parseNull((String)row.get("ID")));
				 VTheVect.addElement(common.parseNull((String)row.get("MILLNAME")));
    				 VTheVect.addElement(common.parseNull((String)row.get("HODCODE")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEMCODE")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEM_NAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("STOCK")));
                    VTheVect.addElement(common.parseNull((String)row.get("UNITRATE")));
                     VTheVect.addElement(common.parseNull((String)row.get("STOCKVALUE")));
				 VTheVect.addElement(common.parseNull((String)row.get("CATL")));
                     VTheVect.addElement(common.parseNull((String)row.get("LOCNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("DRAW")));
                     VTheVect.addElement(common.parseNull((String)row.get("GROUPNAME")));
				 VTheVect.addElement(common.parseNull((String)row.get("USERNAME")));
				 VTheVect.addElement(common.parseNull((String)row.get("ENTRYDATE")));
				 VTheVect.addElement(new Boolean(false));
				 
			
                     theModel.appendRow(VTheVect);
               }
        //  }
		}
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }
	
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }             

			if(ae.getSource()==BApply)
               {    
                    theModel.setNumRows(0);
                    setTabReport();
               }	
			
		if(ae.getSource()==BSave) 
			{
			 updateStatus();
			 theModel.setNumRows(0);
                 setTabReport();
			}
		}
     }
	
	


 private void showMessage(String sMsg)
     {
          JOptionPane.showMessageDialog(null,getMessage(sMsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage(String sMsg)
     {
          String str = "<html><body>";
          str = str + sMsg;      
          str = str + "</body></html>";
          return str;
     }
	
	
  private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public ArrayList getMaterialIndent(String SItemCod,String SGroupCode,String SStDate, String SEnDate,int iMillCode)
     {

          theList = new ArrayList();
		
				
		String QS22 =	" select itemstock_transfer.id,mill.millname,itemstock_transfer.hodcode,itemstock_transfer.itemcode, "+
					" invitems.item_name,itemstock_transfer.stock,round((itemstock_Transfer.stockvalue / itemstock_transfer.Stock),2) as UnitRate, "+
					 "itemstock_transfer.stockvalue,invitems.catl,invitems.locname,invitems.draw, "+
					" stockgroup.groupname,rawuser.username,to_char(ItemStock_Transfer.TransferDate,'dd.mm.yyyy') as entrydate from itemstock_transfer "+
					" inner join invitems on invitems.item_code = itemstock_transfer.itemcode "+
					" inner join mill on mill.millcode = itemstock_transfer.millcode "+
					" inner join rawuser on rawuser.usercode  = itemstock_transfer.entryusercode "+
					" inner join stockgroup on stockgroup.groupcode = invitems.stkgroupcode "+
					" where  itemstock_transfer.millcode = "+iMillCode+" "+
					" and to_char(itemstock_transfer.TransferDate,'yyyymmdd')>= '"+SStDate+"' "+
					" and to_char(itemstock_transfer.TransferDate,'yyyymmdd')<= '"+SEnDate+"' ";
					if (!SItemCod.equals("9999999"))  
									QS22 = QS22 + " and InvItems.Item_Code = '"+SItemCod+"' "; 
					if (!SGroupCode.equals("9999999"))  
									QS22 = QS22 + " and StockGroup.GroupCode = '"+SGroupCode+"' "; 
					QS22 = QS22 + " Order by itemstock_transfer.id asc";			


		System.out.println(QS22);


		try
          {
			
		     
               Class                   . forName("oracle.jdbc.OracleDriver");
		     Connection theConnection           = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "Inventory", "Stores");
			
			Statement theStatement     = theConnection.createStatement();
               ResultSet  result          = theStatement.executeQuery(QS22);
               ResultSetMetaData rsmd    = result.getMetaData();

               while(result.next())
               {
                    HashMap row = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }                         
                    theList.add(row);
               }
               result.close();
              theStatement.close();
		    theConnection.close();
		}
          catch(Exception ex)
          {
               System.out.println(ex);
           ex.printStackTrace();
		}

          return theList;

     }
	
	private void updateStatus()
	{   
			try
      		{
				 if(theConnection ==null)
					{
					ORAConnection jdbc   = ORAConnection.getORAConnection();
                         theConnection        = jdbc.getConnection();
					}	

			 for(int j=0; j<theModel.getRowCount(); j++)
				{
					Boolean bValue = (Boolean)theModel.getValueAt(j, 15);
					if(bValue.booleanValue())
					{


				int iId			     = common.toInt((String)theModel.getValueAt(j, 1));
				String SCode             = common.parseNull((String)theModel.getValueAt(j,4));
				int iStock               = common.toInt((String)theModel.getValueAt(j,6));
				double dVal              = common.toDouble((String)theModel.getValueAt(j,7));
				
				double dStockVal  = iStock * dVal;
		     
				StringBuffer SB          = null;
				SB                 		= new StringBuffer();
			
				
				SB.append(" Update itemstock_transfer set StockValue = "+dStockVal+" Where Id = "+iId+"  and itemcode = '"+SCode+"' and millcode = "+iMillCode+" ");
				
				System.out.println(SB.toString());
				
				PreparedStatement thePS  = theConnection.prepareStatement(SB.toString());
				thePS				.executeUpdate();
				thePS                    . close();
			
			}
			
			theConnection			. commit();
			theConnection			. setAutoCommit(true);
			}
			JOptionPane.showMessageDialog(null, "Data Saved Successfully!!", "Info!", JOptionPane.INFORMATION_MESSAGE);
			}
			catch(Exception e)
			{
			try
			{
				theConnection		. rollback();
				theConnection		. setAutoCommit(true);
			}
			
			
			catch(Exception ex){
				ex.printStackTrace();
			}
	}
	
	
	}
	
	
	
}