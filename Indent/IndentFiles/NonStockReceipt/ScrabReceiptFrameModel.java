package Indent.IndentFiles.NonStockReceipt;

import javax.swing.table.*;
import java.util.*;

public class ScrabReceiptFrameModel extends DefaultTableModel
{

     String ColumnName[]   = {"ItemCode","ItemName","Return Qty","IndentNo","UnitName","Deptname","Machine","User","EntryDate","Already Ret","Delay Days","Issue Date","Issue Status","Receipt Qty"};
     String ColumnType[]   = {"S","S","N","N","S","S","S","S","S","S","N","S","S","E"};
     int  iColumnWidth[] = {20,60,20,20,30,30,30,30,15,30,30,30,30,30};


     public ScrabReceiptFrameModel()
     {
          setDataVector(getRowData(),ColumnName);
     }

     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";
		
     			
          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}
