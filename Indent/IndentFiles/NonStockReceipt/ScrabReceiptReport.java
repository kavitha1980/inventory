package Indent.IndentFiles.NonStockReceipt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;


public class ScrabReceiptReport extends JInternalFrame 
{

     protected     JLayeredPane Layer;

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BApply,BProcess;
     JButton       BExit;
	DateField      TStDate;
     DateField      TEnDate; 
     JTextField    searchField;
     Common        common = new Common();
     Connection     theConnection;
	JComboBox     CType; 
		Vector  VTypeCode,VTypeName;
	
     int           iUserCode=0,iMillCode,iOwnerCode,iCode;
     int PRINT=8;
	
	int iTotalColumns = 12;
     int iWidth[] = {20,50,150,30,30,30,50,50,50,50,50,50}; //,50,50,50};
     
     FinDateField TFromDate,TToDate;

     ArrayList theList = new ArrayList();
     ArrayList theReturnList = new ArrayList();

     ScrabReceiptModel   theModel;

     JTable             theTable;
	
	double dTotal = 0;
	double dURate = 0;
	double dNVal = 0;
	
	 FileWriter FW;
	 File file;

	String   SFileName;
	PdfPTable table;

	Document document;
	
	private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 11, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 9, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 5, Font.BOLD);
     
     public ScrabReceiptReport(JLayeredPane Layer,int iUserCode,int iMillCode,int iOwnerCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
		  this.iOwnerCode = iOwnerCode;

          setData2();
		createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }

     private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();

               theModel       = new ScrabReceiptModel();
               theTable       = new JTable(theModel);
			
			TStDate      = new DateField();
			TEnDate        = new DateField();
			TStDate.setTodayDate();
			TEnDate.setTodayDate();
			
			//CType       = new JComboBox(new Vector(VTypeName));
			CType = new JComboBox();
			CType.addItem("ALL");
			CType.addItem("SCRAP");
			CType.addItem("SECOND SALES");
               
               
              // TableColumnModel TC  = theTable.getColumnModel();

               // for(int i=0;i<theModel.ColumnName.length;i++)   {

               //     TC .getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
              //  }

               

               
               BExit          = new JButton("Exit");
               BApply         = new JButton("Apply");
               BProcess          = new JButton("PDF");

               searchField    = new JTextField();
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }
     }

     private void setLayouts()
     {
            setTitle(" SCRAB RECEIPT REPORT");
            setClosable(true);                    
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,775,600);

            TopPanel.setLayout(new GridLayout(8,2));
            MiddlePanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());

            TopPanel.setBorder(new TitledBorder("Info."));
            MiddlePanel.setBorder(new TitledBorder("Details"));
            BottomPanel.setBorder(new TitledBorder("Controls"));

            TopPanel            . setBackground(new Color(213,234,255));
            MiddlePanel         .setBackground(new Color(213,234,255));
            BottomPanel         . setBackground(new Color(213,234,255));
    }


	private void setData2()
	{
		VTypeName			= null;
		VTypeCode			= null;
		
		
		
		VTypeName			= new Vector();
		VTypeCode			= new Vector();	
			
		VTypeName			. clear();
		VTypeCode			. clear();
		
		VTypeName			. add("All");
		VTypeCode			. add("999999");
		
		
		try
		{
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
				Statement theStatement = theConnection.createStatement();
			
				String QSS = " Select Code, Name from MaterialIndentReturnType "+ 
							" where ReturnStatus = 1 "+
							" and DisableStatus = 0 "+
							" Order by SortNo, 2 ";
				
			
			
				ResultSet re = theStatement.executeQuery(QSS);

			while (re.next())
			{
				VTypeName		. add(common.parseNull(re.getString(2)));
				VTypeCode		. add(common.parseNull(re.getString(1)));
				
			}
				re.close();
			
				theStatement 		.close();
			
		}
			catch(Exception ex)
			{
				System.out.println("From MaterialIndentDetails "+ex);
			}
		
		
	}





     private void addComponents()
     {

       
        
        TopPanel            . add(new JLabel("FromDate"));
	   TopPanel            . add(TStDate);
	   TopPanel            . add(new JLabel("ToDate"));
	   TopPanel            . add(TEnDate);
	   
        TopPanel            . add(new JLabel("Return Type"));
	   TopPanel	    . add(CType);
        
	   
	   TopPanel            . add(new JLabel(""));
	   TopPanel	    . add(BApply);
         
     //   TopPanel            . add(new JLabel("Search"));
     //   TopPanel            . add(searchField);
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));

        MiddlePanel.add(new JScrollPane(theTable));

        BottomPanel.add(BExit);
	   BottomPanel.add(BProcess);

        getContentPane().add("North",TopPanel);
        getContentPane().add("Center",MiddlePanel);
        getContentPane().add("South",BottomPanel);
        BExit.setMnemonic('E');

     }

    private void addListeners()
    {
          BExit.addActionListener(new ActList());
          BApply.addActionListener(new ActList());
          searchField. addKeyListener(new SearchKeyList());
          //theTable.addKeyListener(new KeyList ());
		BProcess.addActionListener(new ActList());
    }

    public void setTabReport()
    {
          try
          {

                String SFromDate = common.pureDate((String)TStDate.toNormal()); 
                String SToDate = common.pureDate((String)TEnDate.toNormal());

			String SType  = common.parseNull((String)CType.getSelectedItem());
		//	 if (!SType.equals("All"))
		//	  iIndentNoo = common.toInt((String)VTypeCode.get(VTypeName.indexOf(SType)));
			 
			 
  

         //       getMaterialIndentReturn(SItemCodee,iIndentNoo);

                ArrayList ADetails = getMaterialIndent(SFromDate,SToDate,SType,iCode);
				System.out.println("ADetails.size()-->"+ADetails.size());

                for(int i=0;i<ADetails.size();i++)
                {
                     HashMap  row =(HashMap)ADetails.get(i);

                     Vector VTheVect = new Vector();
                     
                     String sItemCode = common.parseNull((String)row.get("ITEMCODE"));
                     String sIndentNo = common.parseNull((String)row.get("INDENTNO"));
                     String sUnitCode = common.parseNull((String)row.get("UNIT_CODE"));
                     String sDeptCode= common.parseNull((String)row.get("DEPTCODE"));
                     String sMachCode = common.parseNull((String)row.get("MACHINECODE"));

                     VTheVect.addElement(String.valueOf(i+1));
				 VTheVect.addElement(common.parseNull((String)row.get("ITEMCODE")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEMNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("RETURNQTY")));
				 VTheVect.addElement(common.parseNull((String)row.get("UNITRATE")));
				 VTheVect.addElement(common.parseNull((String)row.get("NETVALUE")));
                     VTheVect.addElement(common.parseNull((String)row.get("INDENTNO")));
                     VTheVect.addElement(common.parseNull((String)row.get("UNITNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("DEPTNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("MACHINENAME")));
				 VTheVect.addElement(common.parseNull((String)row.get("RETURNTYPE")));
                     VTheVect.addElement(common.parseNull((String)row.get("USERNAME")));
               

                     theModel.appendRow(VTheVect);
				 
				// dTotal +=  common.toDouble((String)row.get("NETVALUE"));
               }
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
              
               

               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }

               if(ae.getSource()==BApply)
               {
                    theModel.setNumRows(0);
                    setTabReport();
               }
         
			if(ae.getSource()==BProcess) 
			{
				String SType =  common.parseNull((String)CType.getSelectedItem());
				String SDateTime = getServerDateTime().replace(" ", "");
				
			 try
			 {
			 if(SType.equals("SCRAP")){
				SFileName = "d:\\SCRABITEMRECEIPTREPORT-"+SDateTime+".pdf"; 
			 }
			 else if(SType.equals("SECOND SALES")){
				 SFileName = "d:\\SECONDSALERECEIPTREPORT-"+SDateTime+".pdf"; 
			 }
			 else{
				 SFileName = "d:\\SCRABRECEIPTREPORT-"+SDateTime+".pdf"; 
			 }
				createPDFFile();
				File theFile   = new File(SFileName);
				Desktop        . getDesktop() . open(theFile);
			 }
			  catch(Exception ex){
			  ex.printStackTrace();
			}
			 
			}



	    }
     }
     private class KeyList extends KeyAdapter  {

          public void keyPressed(KeyEvent ke)
          {
               int iRow = theTable.getSelectedRow       ();
               int iCol = theTable.getSelectedColumn    ();

               System.out.println("keyPressed");

               if( iCol==10) {
                   
			checkValue(iRow);
               }
           }

          public void keyReleased(KeyEvent ke)   {
             try   {

            int    iRow    = theTable.getSelectedRow();
            int    iCol    = theTable.getSelectedColumn();
            
            System.out.println("keyReleased");

            if( iCol==10) {              
			checkValue(iRow);
            }

         }catch(Exception e)  {
   
            e.printStackTrace();
         }
      }
   }
     private class SearchKeyList extends KeyAdapter
        {
            public void keyReleased(KeyEvent ke)
            {
                searchItems();     
            }

            public void keyPressed(KeyEvent ke)
            {
                searchItems();
            }    

            public void keyTyped(KeyEvent ke)
            {
                searchItems();
            }
        }

    private void showMessage(String sMsg)
    {
          JOptionPane.showMessageDialog(null,getMessage(sMsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
    }
    private String getMessage(String sMsg)
    {
          String str = "<html><body>";
          str = str + sMsg;
          str = str + "</body></html>";
          return str;
    }
	
    private void removeHelpFrame()
    {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public ArrayList getMaterialIndent(String SFromDate, String SToDate,String SType,int iCode)
     {
            theList = new ArrayList();
            StringBuffer sb = new StringBuffer();

            					
					
			sb.append(" Select t1.ItemCode, t1.ItemName, t1.returnqty, "); 
			sb.append(" round((GRN.invamount / GRN.invqty), 2) as unitrate, ");       
			sb.append(" round((t1.returnqty * ((GRN.invamount/GRN.invqty))), 2) as netvalue,  ");     
			sb.append(" t1.indentno,t1.unitname,t1.deptname,t1.machinename,t1.returntype,t1.username,t1.storereceiptdate,t1.maxgrnid ");
			sb.append(" from      ");
			sb.append(" (     ");  
			sb.append(" Select  t.ItemCode, t.ItemName, t.returnqty, t.indentno,   ");  
			sb.append(" t.unitname, t.deptname,t.machinename,t.returntype,t.username,t.storereceiptdate, nvl(Max(Grn.Id), 0) as MaxGRNId  ");
			sb.append(" from  ");
			sb.append(" ( ");
			
			
			
			sb.append(" Select MaterialIndentDetails.ItemCode,MaterialIndentDetails.ItemName,materialreturndetails.Returnqty  ");
			sb.append(" ,MaterialIndentDetails.IndentNo,   ");
			sb.append(" process_units.UnitName,Department.DeptName,nvl(Machine.Mach_Name,'General')  as MACHINENAME,  ");
			sb.append(" materialindentreturntype.name as returntype,Rawuser.UserName,      ");
			sb.append(" to_char(materialreturndetails.Entrydate,'yyyymmdd') as StoreReceiptDate  ");
			sb.append(" From MaterialIndentDetails   ");
			sb.append(" Left Join Machine On Machine.Mach_Code = MaterialIndentDetails.MachineCode  ");
			sb.append(" left join issue on issue.code = materialindentdetails.itemcode   ");
			sb.append(" and issue.uirefno = materialindentdetails.indentno   ");
			sb.append(" Inner Join process_units On process_units.UnitCode = MaterialIndentDetails.Unit_Code   ");
			sb.append(" Inner Join HrdNew.Department On Department.DeptCode = MaterialIndentDetails.DeptCode   ");
			sb.append(" Inner join Scm.Rawuser on Rawuser.usercode = MaterialIndentDetails.usercode  ");
			sb.append(" inner join materialreturndetails on materialreturndetails.itemcode = MaterialIndentDetails.itemcode   ");
			sb.append(" and MaterialIndentDetails.IndentNo = materialreturndetails.IndentNo   ");
			sb.append(" and materialreturndetails.RequiredQty = MaterialIndentDetails.RequiredQty    ");
			sb.append(" and materialreturndetails.MACHINECODE = MaterialIndentDetails.MACHINECODE ");
			sb.append(" and materialreturndetails.UNIT_CODE = MaterialIndentDetails.UNIT_CODE ");
			sb.append(" Inner join materialindentreturntype on materialindentreturntype.code = materialreturndetails.returntypecode   ");
			sb.append(" Where to_char(materialreturndetails.Entrydate,'yyyymmdd') >= '"+SFromDate+"'  ");
			sb.append(" and to_char(materialreturndetails.Entrydate,'yyyymmdd') <= '"+SToDate+"'  ");
			sb.append(" and materialreturndetails.RETURNFROMINDENT=0 ");
			sb.append(" and Materialindentreturntype.Code not in (12,13,14)  ");
			//sb.append(" Order by 4,2 ");
			
			
			/*sb.append(" Select MaterialIndentDetails.ItemCode,MaterialIndentDetails.ItemName,materialreturndetails.Returnqty ");
			sb.append(" ,MaterialIndentDetails.IndentNo,  ");
			sb.append(" process_units.UnitName,Department.DeptName,nvl(Machine.Mach_Name,'General')  as MACHINENAME, ");
			sb.append(" materialindentreturntype.name as returntype,Rawuser.UserName,     ");
			sb.append(" to_char(MaterialIndentDetails.StoreReceiptDate,'yyyymmdd') as StoreReceiptDate ");
			sb.append(" From MaterialIndentDetails  ");
			sb.append(" Left Join Machine On Machine.Mach_Code = MaterialIndentDetails.MachineCode "); 
			sb.append(" left join issue on issue.code = materialindentdetails.itemcode  ");
			sb.append(" and issue.uirefno = materialindentdetails.indentno  ");
			sb.append(" Inner Join process_units On process_units.UnitCode = MaterialIndentDetails.Unit_Code  ");
			sb.append(" Inner Join HrdNew.Department On Department.DeptCode = MaterialIndentDetails.DeptCode  ");
			sb.append(" Inner join Scm.Rawuser on Rawuser.usercode = MaterialIndentDetails.usercode  ");
			sb.append(" inner join materialreturndetails on materialreturndetails.itemcode = MaterialIndentDetails.itemcode  ");
			sb.append(" and MaterialIndentDetails.IndentNo = materialreturndetails.IndentNo   ");
			sb.append(" and materialreturndetails.RequiredQty = MaterialIndentDetails.RequiredQty   ");
			sb.append(" Inner join materialindentreturntype on materialindentreturntype.code = materialreturndetails.returntypecode  ");
			sb.append(" Where to_char(MaterialIndentDetails.StoreReceiptDate,'yyyymmdd') >= '"+SFromDate+"' ");
			sb.append(" and to_char(MaterialIndentDetails.StoreReceiptDate,'yyyymmdd') <= '"+SToDate+"' ");
			sb.append(" and MaterialIndentDetails.STORERECEIPTSTATUS=1 and MaterialIndentDetails.RETURNSTATUS=1   ");*/
			
			
			if (SType.equals("ALL")){
			sb.append(" and Materialindentreturntype.Code in (1,6,10,11,12,13,14)  ");
			}
			else if (SType.equals("SCRAP")){
			sb.append(" and Materialindentreturntype.Code in (1,6,10,11)  ");
			}
			else{
			sb.append(" and Materialindentreturntype.Code in (12,13,14)  ");
			}
			sb.append(" )t ");
			sb.append(" left Join GRN on GRN.Code = t.ItemCode   "); 
			sb.append(" Group by t.ItemCode, t.ItemName, t.returnqty, t.indentno,  ");
			sb.append(" t.unitname, t.deptname,t.machinename,t.returntype,t.username,t.storereceiptdate ");
			sb.append(" )t1     ");
			sb.append(" left Join GRN on GRN.Id = t1.MaxGrnId  ");     
			sb.append(" Order by 12     ");
			
			System.out.println("sb.toString()-->"+sb.toString());
			
            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(sb.toString());
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theList;
     }
     public ArrayList getMaterialIndentReturn(String SItemCodee, int iIndentNoo)
     {
            theReturnList = new ArrayList();

            StringBuffer sb = new StringBuffer();

            
		sb.append(" select issue.code,materialindentdetails.requiredqty,issue.issrate as unitrate,(materialindentdetails.requiredqty*issue.issrate) as netvalue ");
		sb.append(" from issue  ");
		sb.append(" inner join materialindentdetails on materialindentdetails.ITEMCODE = issue.code ");
		sb.append(" and materialindentdetails.INDENTNO = issue.uirefno ");
		sb.append(" where code ='"+SItemCodee+"'  and uirefno = "+iIndentNoo+" ");
		sb.append(" union all ");
		sb.append(" select nonstockissue.code,materialindentdetails.requiredqty,nonstockissue.issrate as unitrate,(materialindentdetails.requiredqty*nonstockissue.issrate) as netvalue  ");
		sb.append(" from nonstockissue  ");
		sb.append(" inner join materialindentdetails on materialindentdetails.ITEMCODE = nonstockissue.code ");
		sb.append(" and materialindentdetails.INDENTNO = nonstockissue.uirefno ");
		sb.append(" where code = '"+SItemCodee+"' and uirefno = "+iIndentNoo+" ");


            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(sb.toString());
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theReturnList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theReturnList;
     }
     private void checkValue(int iRow){


         double dalreadyRet = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,9)));

         double dretQty = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,10)));
         
         double dReqQty = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,2)));
         
         double dNeedToRetrun = dReqQty-dalreadyRet;

         if(dretQty>dNeedToRetrun){

              showMessage("Return Quantiy is Must be equal or Less Than Required Qty");
          }

    }
    
     private double getRetAlreadyReturned(String sItemCode,String sIndnetNo,String sUnitCode,String sDeptCode,String sMachCode){

         double dalreadyRet = 0;
         java.util.HashMap theMap = new java.util.HashMap();
         
         for (int index = 0; index < theReturnList.size(); index++) {
             
             theMap = (java.util.HashMap) theReturnList.get(index);
             
            String sItem =  common.parseNull((String) theMap.get("ITEMCODE"));
            String sIndnent =  common.parseNull((String) theMap.get("INDENTNO"));
            String sUnit =  common.parseNull((String) theMap.get("UNIT_CODE"));
            String sDept =  common.parseNull((String) theMap.get("DEPTCODE"));
            String sMach =  common.parseNull((String) theMap.get("MACHINECODE"));
            
            if(sItemCode.equals(sItem) && sIndnetNo.equals(sIndnent) && sUnitCode.equals(sUnit) && sDeptCode.equals(sDept) && sMachCode.equals(sMach)){

                    dalreadyRet += common.toDouble(common.parseNull((String) theMap.get("RETURNQTY")));

                    //break;
            }
        }
         return dalreadyRet;
     }
     private void searchItems()
     {
          String SSearchStr        = common.parseNull(searchField.getText()).trim();

          int iIndex               = -1;

          for(int i=0; i<theModel.getRowCount(); i++)
          {
               String SItemName    = common.parseNull((String)theModel.getValueAt(i, 2));

               if(SItemName.startsWith(SSearchStr.toUpperCase()))
               {
                    iIndex         = i;
                    break;
               }
          }
          if(iIndex != -1)
          {
                 theTable       . setRowSelectionInterval(iIndex, iIndex);
            //     Rectangle theRect   = theTable.getCellRect(iIndex, 0, true);
              //   theTable       . scrollRectToVisible(theRect);
                 theTable       . setSelectionBackground(Color.green);			   
          }
     }


 public String getServerDateTime()
     {
          String SDate= " " ;
                         
          String QS = " Select to_Char(Sysdate,'DD.MM.YYYY.HH24.MI.SS') From Dual ";


          try
          {
               ORAConnection connect = ORAConnection.getORAConnection();
               Connection theConnection = connect.getConnection();
               Statement theStatement = theConnection.createStatement();
               ResultSet theResult = theStatement.executeQuery(QS);
               if(theResult.next())
                    SDate = theResult.getString(1);
               theResult.close();
               theStatement.close();     
          }
          catch(Exception ex)
          {
               return "";
          }
          return SDate ;
     }


	private void createPDFFile()
	{
        try 
        {
			document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(SFileName));
            document.setPageSize(PageSize.LEGAL.rotate());
            document.open();
                             
            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            table.setHeaderRows(5);
			pdfHead();
			pdfBody();
           
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }

    }  

	private void pdfHead()
	{
		
			String SType = common.parseNull((String)CType.getSelectedItem());

	
	   AddCellIntoTable("AMARJOTHI SPINNING MILLS LIMITED", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
//        AddCellIntoTable(" SCRAB RECEIPT REPORT : "  +TStDate.toString()+ "---" +TEnDate.toString(), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
    if(SType.equals("SCRAP"))
		{		 
			//AddCellIntoTable(" SCRAP MATERIALS " , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
			AddCellIntoTable(" SCRAB RECEIPT REPORT : "  +TStDate.toString()+ "---" +TEnDate.toString(), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
		}
		else if (SType.equals("SECOND SALES"))
		{
			//AddCellIntoTable("  SECOND SALE MATERIALS " , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold); 
			AddCellIntoTable(" SECOND SALES REPORT : "  +TStDate.toString()+ "---" +TEnDate.toString(), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
		}
		else
		{
			AddCellIntoTable(" SCRAB AND SECOND SALE MATERIALS " , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold); 
		}
	    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
     
		AddCellIntoTable("SL NO" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
     		AddCellIntoTable("ITEM CODE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);

		AddCellIntoTable("ITEM NAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("RECEIPT QTY" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("UNIT RATE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("NET VALUE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
        AddCellIntoTable("INDENT NO" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
	    AddCellIntoTable("UNIT NAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
        AddCellIntoTable("DEPT NAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
        AddCellIntoTable("MACHINE NAME " , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
	   AddCellIntoTable("RETURN TYPE " , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
        AddCellIntoTable("USERNAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);

     
        
		
		
	}

	private void pdfBody()
	{
	try
	{
		dURate = 0;
		dNVal = 0;
		dTotal = 0;
		
		int colcount=theModel.getColumnCount();
        int rowcount=theModel.getRowCount();
		
		  
		for(int K=0;K<theModel.getRowCount();K++)
          {
		
		   
			
				dURate = common.toDouble((String)theModel.getValueAt(K,4));
				dNVal = common.toDouble((String)theModel.getValueAt(K,5));
			 
				dTotal += dNVal;

		   AddCellIntoTable(String.valueOf(theModel.getValueAt(K,0)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);

		   AddCellIntoTable(String.valueOf(theModel.getValueAt(K,1)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		   AddCellIntoTable(String.valueOf(theModel.getValueAt(K,2)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		   AddCellIntoTable(String.valueOf(theModel.getValueAt(K,3)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		   AddCellIntoTable( common.getRound(dURate,2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		   AddCellIntoTable( common.getRound(dNVal,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		   AddCellIntoTable(String.valueOf(theModel.getValueAt(K,6)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		   AddCellIntoTable(String.valueOf(theModel.getValueAt(K,7)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		   AddCellIntoTable(String.valueOf(theModel.getValueAt(K,8)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		   AddCellIntoTable(String.valueOf(theModel.getValueAt(K,9)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		   AddCellIntoTable(String.valueOf(theModel.getValueAt(K,10)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		    AddCellIntoTable(String.valueOf(theModel.getValueAt(K,11)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
			
		}	
		
			
		

			  AddCellIntoTable("TOTAL" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 5,25, 4, 1, 8, 2, mediumbold);
			   AddCellIntoTable(common.getRound(dTotal,2) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, mediumbold);
			   AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 6,25, 4, 1, 8, 2, smallnormal);
		
	
	
	
		document.add(table);

		document.close();
	
	}
	catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }
	}  

	public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str ));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
   }  


}

