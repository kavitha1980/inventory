package Indent.IndentFiles.NonStockReceipt;

import guiutil.FinDateField;
import jdbc.ORAConnection;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;
import java.net.InetAddress;
import util.*;
import javax.swing.table.TableColumnModel;

import com.digitalpersona.onetouch.*;
import com.digitalpersona.onetouch.capture.*;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.*;
import com.digitalpersona.onetouch.verification.*;
import guiutil.*;

public class ScrapMaterialReceipt extends JInternalFrame 
{

     protected     JLayeredPane Layer;

     JPanel        TopPanel,MiddlePanel,BottomPanel,PreReceiptScrapPanel;
     JTabbedPane   thePane;
     JButton       BApply;
     JButton       BExit;
     JButton       BSave;

	Vector        VDeptName,VDeptCode,VUserName,VUserCode,VItemName,VItemCode,VTypeCode,VTypeName;
     JTextField    searchField;
	JComboBox     CDept,CUser,CItem,CType;
     Common        common = new Common();
     Connection    theConnection;
	String        SUser,SDept,SItem;
	int           iRECEIPTQTY;
	JTabbedPane   jTabPane;
	
     int           iUserCode=0,iMillCode,iOwnerCode,iDeptCode,iCode;
     int           PRINT=11;
     
	FinDateField TFromDate,TToDate;
	Font 		  font;
	Boolean bselect;

     ArrayList theList = new ArrayList();
     ArrayList theReturnList = new ArrayList();
	 PreReceiptScarpModel		theModel1;

     JTable             theTable1;
	 ArrayList ADataList ;
	InetAddress ip;
	
    private DPFPCapture capturer 			= DPFPGlobal.getCaptureFactory()		.createCapture();
    private DPFPVerification verificator 	= DPFPGlobal.getVerificationFactory()	.createVerification();

    DPFPTemplate template;
	int iIssueDays=0;
	
	ArrayList theTemplateList,theMaterialIssueTemplateList;
	
	int iPunchStatus=0;
	int iSaveCount=0;
	MyLabel      LStatus;
	
     
     public ScrapMaterialReceipt(JLayeredPane Layer,int iUserCode,int iMillCode,int iOwnerCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
		  this.iOwnerCode = iOwnerCode;

			getIssueDays();
			setData			();
			setData2		();
			createComponents();
			setLayouts		();
			addComponents	();
			addListeners	();
			setPreReceiptTableData();
     }
	 
public ScrapMaterialReceipt(JLayeredPane Layer,int iUserCode,int iMillCode,int iOwnerCode,ArrayList theTemplateList,ArrayList theMaterialIssueTemplateList)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
		  this.iOwnerCode = iOwnerCode;
          this.theTemplateList                = theTemplateList;
          this.theMaterialIssueTemplateList = theMaterialIssueTemplateList;

			getIssueDays();
			setData();
			setData2();
			createComponents();
			setLayouts();
			addComponents();
			addListeners();
			setPreReceiptTableData();
     }	 
	
 private void setData()
 {
		VDeptName			= null;
		VDeptCode			= null;
		VUserName			= null;
		VUserCode			= null;
		VItemName			= null;
		VItemCode			= null;
		
		
		VDeptName			= new Vector();
		VDeptCode			= new Vector();	
		VUserName			= new Vector();
		VUserCode			= new Vector();
		VItemName			= new Vector();
		VItemCode			= new Vector();
			
		VDeptName			. clear();
		VDeptCode			. clear();
		VUserName			. clear();
		VUserCode			. clear();
		VItemName           . clear();
		VItemCode           . clear();
		
		VDeptName			. add("All");
		VDeptCode			. add("999999");
		
		VUserName			. add("All");
		VUserCode			. add("999999");
		
		VItemName			. add("All");
		VItemCode			. add("999999");
			
		try
		{
			 if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

			Statement theStatement = theConnection.createStatement();
			
			String QSS = " Select DeptName,DeptCode from Hrdnew.Department order by 1";
			ResultSet re = theStatement.executeQuery(QSS);

			while (re.next())
			{
				VDeptName		. add(common.parseNull(re.getString(1)));
				VDeptCode		. add(common.parseNull(re.getString(2)));
				
			}
			re.close();
			
			String QS46 = " Select UserName,UserCode from Rawuser order by 1";
			ResultSet rss = theStatement.executeQuery(QS46);

			while (rss.next())
			{
				VUserName		. add(common.parseNull(rss.getString(1)));
				VUserCode		. add(common.parseNull(rss.getString(2)));
				
			}
			rss.close();
			
			
			String QS66 = " Select distinct ItemName,Itemcode from Materialindentdetails where storereceiptstatus = 0 order by 1";
			ResultSet rssset = theStatement.executeQuery(QS66);

			while (rssset.next())
			{
				VItemName		. add(common.parseNull(rssset.getString(1)));
				VItemCode		. add(common.parseNull(rssset.getString(2)));
				
			}
			rssset.close();
			
			theStatement 		.close();
			
		}
		catch(Exception ex)
			{
			System.out.println("From MaterialIndentDetails "+ex);
			}
		}

	private int getIssueDays()
	{
		
		iIssueDays =0;
		
		try
		{
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
			
			//String QS =  " select round(sysdate-max(entrydate)) from MaterialScrabDetails";
			
			String QS =  " select round(sysdate-max(entrydate)) from ScrapIssueDetails";
			
			Statement theStatement = theConnection.createStatement();
			ResultSet result = theStatement.executeQuery(QS);
			
			while (result.next())
			{
				iIssueDays = result.getInt(1);
				
			}
			result.close();
			theStatement.close();
		}
		catch(Exception ex)
		{
			System.out.println("ex: "+ex);
		}
		return iIssueDays;
		
	}
	
	private void setData2()
	{
		VTypeName			= null;
		VTypeCode			= null;
		
		
		
		VTypeName			= new Vector();
		VTypeCode			= new Vector();	
			
		VTypeName			. clear();
		VTypeCode			. clear();
		
		VTypeName			. add("All");
		VTypeCode			. add("999999");
		
		
		try
		{
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
				Statement theStatement = theConnection.createStatement();
			
				String QSS = " Select Code, Name from MaterialIndentReturnType "+ 
							" where ReturnStatus = 1 "+
							" and DisableStatus = 0 "+
							" Order by SortNo, 2 ";
				
			
			
				ResultSet re = theStatement.executeQuery(QSS);

			while (re.next())
			{
				VTypeName		. add(common.parseNull(re.getString(2)));
				VTypeCode		. add(common.parseNull(re.getString(1)));
				
			}
				re.close();
			
				theStatement 		.close();
			
		}
			catch(Exception ex)
			{
				System.out.println("From MaterialIndentDetails "+ex);
			}
		
		
	}


     private void createComponents()
     {
          try
          {
               TopPanel       		= new JPanel();
               MiddlePanel    		= new JPanel();
               BottomPanel    		= new JPanel();

				theModel1       = new PreReceiptScarpModel();
                theTable1       = new JTable(theModel1);
			   
			   TableColumnModel TC1  = theTable1.getColumnModel();
               
                for(int i=0;i<theModel1.ColumnName.length;i++)   {

                    TC1.getColumn(i).setPreferredWidth(theModel1.iColumnWidth[i]);
                }

               TFromDate      = new FinDateField();
               TToDate        = new FinDateField();

               TFromDate      .   setTodayDate();
               TToDate        .   setTodayDate();
			   
				font           = new Font("Arial",Font.BOLD,11);
				jTabPane       = new JTabbedPane();
				jTabPane.setFont(font);
			
			
			CDept       = new JComboBox(new Vector(VDeptName));
		
			CUser       = new JComboBox(new Vector(VUserName));
			
			CItem       = new JComboBox(new Vector(VItemName));
			
			CType       = new JComboBox(new Vector(VTypeName));
          
			
               BExit          = new JButton("Exit");
               BApply         = new JButton("Apply");
               //BSave          = new JButton("Save");
			   BSave          = new JButton("Punch");
			   
			   System.out.println("iIssueDays-->"+iIssueDays);
			   
			 /*  if(iIssueDays>3)
			   {
				   BSave.setEnabled(false);
				   JOptionPane.showMessageDialog(null, "The SecondSales Issue  is Pending.. You Can't Receipt the SecondSales!!", "Information", JOptionPane.INFORMATION_MESSAGE);
			   }
			   else
			   {
				   BSave.setEnabled(true);
			   }*/
			   
			    BSave.setEnabled(true);
               
			   LStatus        = new MyLabel();
               searchField    = new JTextField();
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }
     }

     private void setLayouts()
     {
            setTitle("Scrap Receipt Not Store Materials Issue ");
            setClosable(false);                    
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,775,600);

            TopPanel.setLayout(new GridLayout(8,2));
            MiddlePanel.setLayout(new BorderLayout());
			//PreReceiptScrapPanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());

            TopPanel.setBorder(new TitledBorder("Info."));
            MiddlePanel.setBorder(new TitledBorder("Details"));
            BottomPanel.setBorder(new TitledBorder("Controls"));

            TopPanel            . setBackground(new Color(213,234,255));
            MiddlePanel         .setBackground(new Color(213,234,255));
            BottomPanel         . setBackground(new Color(213,234,255));
			
    }

     private void addComponents()
     {

	    TopPanel            . add(new JLabel("Department"));
        TopPanel            . add(CDept);

        TopPanel            . add(new JLabel("User"));
        TopPanel            . add(CUser);
        
	    TopPanel            . add(new JLabel("Item"));
        TopPanel            . add(CItem);
	   
	    TopPanel            . add(new JLabel("Return Type"));
		TopPanel            . add(CType);
	   
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));
	    TopPanel	    	. add(BApply);
         
        TopPanel            . add(new JLabel("Search"));
        TopPanel            . add(searchField);
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
		MiddlePanel        . add(new JScrollPane(theTable1));

        BottomPanel.add(BExit);
        BottomPanel.add(BSave);
		BottomPanel.add(LStatus);
		        

        getContentPane().add("North",TopPanel);
        getContentPane().add("Center",MiddlePanel);
        getContentPane().add("South",BottomPanel);
        BExit.setMnemonic('E');
        BSave.setMnemonic('S');

     }

    private void addListeners()
    {
          BExit.addActionListener(new ActList());
          BApply.addActionListener(new ActList());
          BSave.addActionListener(new ActList());
          searchField. addKeyListener(new SearchKeyList());
    }

   
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
			if(ae.getSource()==BSave)
			{
				moveCursorToOtherCell(0, 0);
				
				if(validations())
				{  
					
						if(common.parseNull(LStatus.getText()).length()>0) {
							stop();
							LStatus.setText("");
						}
						
						init();
						start();
						BSave.setEnabled(false);
						iPunchStatus=0;    
						
				}
			}
			
               if(ae.getSource()==BExit)
               {
				    stop();
                    removeHelpFrame();
               }

               if(ae.getSource()==BApply)
               {
                    
					theModel1.setNumRows(0);
                    setPreReceiptTableData();
					BSave.setEnabled(true);
               }
          }
     }
  
     private class SearchKeyList extends KeyAdapter
        {
            public void keyReleased(KeyEvent ke)
            {
                searchItems();     
            }

            public void keyPressed(KeyEvent ke)
            {
                searchItems();
            }    

            public void keyTyped(KeyEvent ke)
            {
                searchItems();
            }
        }
	   
	private void moveCursorToOtherCell(int iRow, int iCol) {
		theTable1       . setRowSelectionInterval(iRow, iCol);
		Rectangle cell = theTable1.getCellRect(iRow, 0, true);
		theTable1       . scrollRectToVisible(cell);
		theTable1		. editCellAt(iRow, iCol);
	}

    private void showMessage(String sMsg)
    {
          JOptionPane.showMessageDialog(null,getMessage(sMsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
    }
    private String getMessage(String sMsg)
    {
          String str = "<html><body>";
          str = str + sMsg;
          str = str + "</body></html>";
          return str;
    }
	
    private void removeHelpFrame()
    {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     
	 public void setPreReceiptTableData()
    {
          try
          {
			  
			theModel1.setNumRows(0);  
			setDataVector();
			
				for(int i=0;i<ADataList.size();i++)
                {
                     HashMap  theMap =(HashMap)ADataList.get(i);
					 String SScrapType  = common.parseNull((String)theMap.get("SECONDSALESSTATUS"));

                      ArrayList theList;
               
					   theList                     = new ArrayList();
					   theList                     . clear();
					   theList                     . add(i+1);
					   theList                     . add((String)theMap.get("ITEMCODE"));
					   theList                     . add((String)theMap.get("ITEMNAME"));
					   theList                     . add((String)theMap.get("REQUIREDQTY"));
					   theList                     . add("0");
					   theList                     . add((String)theMap.get("UNITNAME"));
					   theList                     . add((String)theMap.get("DEPTNAME"));
					   theList                     . add((String)theMap.get("MACHINENAME"));
					   theList                     . add(common.parseNull((String)theMap.get("USERNAME")));
					   theList                     . add(common.parseDate((String)theMap.get("ENTRYDATE")));
					   theList                     . add("0.0");
					   theList                     . add(common.parseNull((String)theMap.get("DELAYDAYS")));
					   theList                     . add(common.parseNull((String)theMap.get("RETURNTYPE")));
					   if(SScrapType.equals("0"))
					   {
						  theList                     . add("SCRAP"); 
					   }
					   else
					   {
						   theList                     . add("SECOND SALES"); 
					   }
					   theList.add(new Boolean(false));
					   
					   theModel1                      . appendRow(new Vector(theList));
					   theList                         = null;

                    
               }
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
	 
	 private void setDataVector()
     {
            ADataList = new ArrayList();
			int iUserCode = 0;
			int iDeptCode =0;
			String SItemCode = "";
			
			String SDept  = common.parseNull((String)CDept.getSelectedItem());
			if (!SDept.equals("All"))
				iDeptCode = common.toInt((String)VDeptCode.get(VDeptName.indexOf(SDept)));
			String SUser  = common.parseNull((String)CUser.getSelectedItem());
			if (!SUser.equals("All"))
				iUserCode = common.toInt((String)VUserCode.get(VUserName.indexOf(SUser)));
			String SItem  = common.parseNull((String)CItem.getSelectedItem());
			if (!SItem.equals("All"))
				SItemCode = common.parseNull((String)VItemCode.get(VItemName.indexOf(SItem)));

			String SType  = common.parseNull((String)CType.getSelectedItem());
			if (!SType.equals("All"))
				iCode = common.toInt((String)VTypeCode.get(VTypeName.indexOf(SType)));
           
					
		String QS =		" Select distinct PreReceiptScrapTransfer.ItemCode,PreReceiptScrapTransfer.ItemName,  "+
						" PreReceiptScrapTransfer.RequiredQty ,  "+
						" process_units.UnitName,hrdnew.department.DeptName,nvl(Machine.Mach_Name,'General')  as MACHINENAME,Rawuser.UserName,    "+
						" EntryDate,PreReceiptScrapTransfer.Unitcode,    "+
						" PreReceiptScrapTransfer.DeptCode,PreReceiptScrapTransfer.MachineCode,Round(SysDate - PreReceiptScrapTransfer.EntryDatetime) as DelayDays,materialindentreturntype.name as returntype,  "+
						" PreReceiptScrapTransfer.RETURNQTY , PreReceiptScrapTransfer.Id ,PreReceiptScrapTransfer.secondSalesStatus "+
						" From PreReceiptScrapTransfer    "+
						" Left Join Machine On Machine.Mach_Code = PreReceiptScrapTransfer.MachineCode   "+  
						" Inner Join process_units On process_units.UnitCode = PreReceiptScrapTransfer.UnitCode   "+ 
						" Inner Join hrdnew.department On hrdnew.department.DeptCode = PreReceiptScrapTransfer.DeptCode    "+
						" Inner join Scm.Rawuser on Rawuser.usercode = PreReceiptScrapTransfer.OwnerCode   "+
						" Inner join materialindentreturntype on materialindentreturntype.code = PreReceiptScrapTransfer.returntype  "+
						" Where PreReceiptScrapTransfer.EntryDate <=to_char(sysdate,'yyyymmdd')   "+
						" and PreReceiptScrapTransfer.TransferStatus=0  "+
						" and PreReceiptScrapTransfer.STORERECEIPTSTATUS=0 ";
						if (!SDept.equals("All"))
						QS = QS +" and PreReceiptScrapTransfer.DeptCode="+iDeptCode+"  ";
						if (!SUser.equals("All"))
							QS = QS +" and PreReceiptScrapTransfer.OwnerCode="+iUserCode+"  ";
						if (!SItem.equals("All"))
						QS = QS +" and PreReceiptScrapTransfer.ItemCode='"+SItemCode+"'  ";
						if (!SType.equals("All"))
						QS = QS +" and PreReceiptScrapTransfer.returntype="+iCode+"  ";
						
						System.out.println("QS-->"+QS);
					
		
            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(QS);
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      ADataList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
       
     }
	 
	 private boolean InsertData(String sEmpCode)
	{
         int iError = 0;
         int icheck = 0;
		 String SSystemName  = common.getLocalHostName();
		 int iCount =0;
         
		 PreparedStatement theStatement= null;
         
		try
		{
			
			System.out.println("comming insert"+iSaveCount);
			
		   
			if(theConnection ==null) 
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
			
		  
			theConnection			. setAutoCommit(false);
		  
			// PRev Receipt Scrap
			
			for (int i = 0; i < theModel1.getRows(); i++) 
			{
				
				HashMap theMap1	= (HashMap) ADataList.get(i);
					
				 bselect = (Boolean)(theModel1.getValueAt(i,14));

				if(bselect.booleanValue())
                {
                    int iID = common.toInt((String) theMap1.get("ID"));
					
					String PreScrapQS = " Update PreReceiptScrapTransfer set PunchEmpCode="+sEmpCode+" ,STORERECEIPTSTATUS=1 , StoreSystemName='"+SSystemName+"' ,StoreEntryDateTime = sysdate,StoreUserCode="+iUserCode+" "+  
										" where ID ="+iID+" ";
										
										System.out.println("PreScrapQS-->"+PreScrapQS);
		
		
					theStatement  = theConnection.prepareStatement(PreScrapQS);
                    theStatement . executeUpdate();
					
					iCount++;
						
				}
			}
			
			theStatement.close();

			theConnection			. commit();
			theConnection			. setAutoCommit(true);
			
			iSaveCount++;
			
			System.out.println("iCount-->"+iCount);
			
			if(iCount>0)
				showMessage("Return Quantity Updated..");
			theModel1.setNumRows(0);
			setPreReceiptTableData();
		}
		catch(Exception e)
		{
			try
			{
				theConnection		. rollback();
				theConnection		. setAutoCommit(true);
			}
			catch(Exception ex){}
			iError = 1;
			e.printStackTrace();
				showMessage("Return Quantity Not Updated..");
			return false;
			
		
		}
		return true;
     }

     private void searchItems()
     {
          String SSearchStr        = common.parseNull(searchField.getText()).trim();

          int iIndex               = -1;

          for(int i=0; i<theModel1.getRowCount(); i++)
          {
               String SItemName    = common.parseNull((String)theModel1.getValueAt(i, 2));

               if(SItemName.startsWith(SSearchStr.toUpperCase()))
               {
                    iIndex         = i;
                    break;
               }
          }
          if(iIndex != -1)
          {
                 theTable1       . setRowSelectionInterval(iIndex, iIndex);
                 Rectangle theRect   = theTable1.getCellRect(iIndex, 1, true);
                 theTable1       . scrollRectToVisible(theRect);
                 theTable1       . setSelectionBackground(Color.red);			   
          }
     }

	private boolean validations()
	{
		
		try
		{
				
			if ( ((String)CUser.getSelectedItem()).equals("All"))
			{
			
				JOptionPane.showMessageDialog(null, "User Selection All Cant Allowed For Save..Select Department User.."				, "Error!", JOptionPane.ERROR_MESSAGE);
				return false;
			}
			
			return true;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			
			JOptionPane.showMessageDialog(null, "Problem while Checking Data..", "Error!", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}
	
	
     protected void init()        
     {
            capturer = DPFPGlobal.getCaptureFactory().createCapture();
        
                
		capturer.addDataListener(new DPFPDataAdapter() {
                   
                public void dataAcquired(final DPFPDataEvent e) {
                  
                  
                    LStatus.setText(" Please wait Under processing.................");

				SwingUtilities.invokeLater(new Runnable()
                                {
                                        public void run()
                                        {
                                    
                                                LStatus.setText("The fingerprint sample was captured....");
                                                process(e.getSample());
                                                
                                        }});
			}
		});
               
		capturer.addReaderStatusListener(new DPFPReaderStatusAdapter() {
                public void readerConnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
                                    
		 			makeReport("The fingerprint reader was connected.");
				}});
			}
                public void readerDisconnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was disconnected.");
				}});
			}
		});
		capturer.addSensorListener(new DPFPSensorAdapter() {
                public void fingerTouched(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was touched.");
				}});
			}
                public void fingerGone(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The finger was removed from the fingerprint reader.");
				}});
			}
		});
		capturer.addImageQualityListener(new DPFPImageQualityAdapter() {
                public void onImageQuality(final DPFPImageQualityEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					if (e.getFeedback().equals(DPFPCaptureFeedback.CAPTURE_FEEDBACK_GOOD))
						makeReport("The quality of the fingerprint sample is good.");
					else
						makeReport("The quality of the fingerprint sample is poor.");
				}});
			}
		});
	}

	protected void process(DPFPSample sample)
	{
		// Draw fingerprint sample image.
		
		DPFPFeatureSet features = extractFeatures(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

		// Check quality of the sample and start verification if it's good
		int iStatus=0;
   	        String SName  = "";
		String SCode  = "";
                LStatus.setText(" Under processing.................");
				
				

		if (features != null)
		{
			// Compare the feature set with our template



               for(int i=0; i<theMaterialIssueTemplateList.size(); i++)
               {
                    HashMap theMap = (HashMap) theMaterialIssueTemplateList.get(i);

                    SCode    = (String)theMap.get("EMPCODE");
                    SName    = (String)theMap.get("DISPLAYNAME");
                    
                    DPFPTemplate template   = (DPFPTemplate)theMap.get("TEMPLATE");

                    DPFPVerification matcher= DPFPGlobal.getVerificationFactory().createVerification();
                    matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);

                    DPFPVerificationResult result = 
                    matcher.verify(features,template);

                    if (result.isVerified())
                    {
                         LStatus.setText(""+SName);       
                         iStatus=1;
                         break;
                    }
               }


               if(iStatus==0)
               {

                    for(int i=0; i<theTemplateList.size(); i++)
                    {
     
                         HashMap theMap = (HashMap) theTemplateList.get(i);
     
                         SCode    = (String)theMap.get("EMPCODE");
                         SName    = (String)theMap.get("DISPLAYNAME");
                         
                         DPFPTemplate template   = (DPFPTemplate)theMap.get("TEMPLATE");
     
                         DPFPVerification matcher= DPFPGlobal.getVerificationFactory().createVerification();
                         matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);
     
                         DPFPVerificationResult result = 
                         matcher.verify(features,template);
     
                         if (result.isVerified())
                         {
                                LStatus.setText(""+SName);       

                              iStatus=1;
                              break;
                         }
                    }
               }

                  if(iStatus==0)
                  {
                         LStatus		.	setText("No Match Found");       
                         JOptionPane	.	showMessageDialog(null," No Match Found Punch Again");
                         //stop();
                         iPunchStatus=0;
                         //BSave.setEnabled(true);


                  }
                  else if(iSaveCount==0)
                  {
					  
                    if(isReceiverEligible(SCode))
                    {
					    if(InsertData(SCode))
						{
							stop();
							
							setData();
							iPunchStatus=0;
							BSave.setEnabled(true);
							iSaveCount=0;
							theModel1.setNumRows(0);
							setPreReceiptTableData();
							LStatus.setText("");
						}
						
						else
                       {
                            JOptionPane.showMessageDialog(null," Problem in Storing Data");
                            stop();
                            iPunchStatus=0;
                            BSave.setEnabled(true);
							
                       }
                   }
                   else
                   {
                      JOptionPane.showMessageDialog(null," You are not eligible to Receive this Material");
                      //27.08.2020 stop();
                      setData();
                      BSave.setEnabled(false);
                   }

                }
          }
	}
	
     private boolean isReceiverEligible(String SEmployeeCode)
     {
          boolean bEligible=false;
          //int iRDCUserCode = getRDCUserCode();
          StringBuffer sb  = new StringBuffer();
          //sb.append(" Select EmpCode from IssueReceiverList where UserCode ="+iRDCUserCode);
		  
		  int iRawUser=0;
		  
			 
			
				String SUser=(String)CUser.getSelectedItem();
			 
			  iRawUser = common.toInt((String)VUserCode.get(VUserName.indexOf(SUser)));
		  
		  
		  sb.append(" Select EmpCode from IssueReceiverList where UserCode ="+iRawUser+" and EmpCode="+SEmployeeCode);
		  
		  		  System.out.println("semp valid"+sb.toString());

          try
          {
              ORAConnection   oraConnection =  ORAConnection.getORAConnection();
              Connection      theConnection =  oraConnection.getConnection();               
			  
			  
              Statement theStatement    = theConnection.createStatement();
              ResultSet result          = theStatement.executeQuery(sb.toString());
              while(result.next())
              {
                 String SEmpCode = result.getString(1);
				 
				System.out.println("result empcode"+SEmpCode);

				 
                 if(SEmpCode.equals(SEmployeeCode))
                   bEligible = true; 
              }
              result.close();
			  
          }
          catch(Exception ex)
          {
             System.out.println(" isReceiver Eligible"+ex);
			 ex.printStackTrace();
          }
		  
		  //bEligible=false;
		  
        return bEligible;
     }
	
	
	protected void start()
	{
            capturer.startCapture();
	}

	protected void stop()
	{
		System.out.println("comm into stop");
             capturer.stopCapture();
	}
	public void setPrompt(String string) {
	}
	public void makeReport(String string) {
            System.out.println(string);
	}
	
	protected Image convertSampleToBitmap(DPFPSample sample) {
		return DPFPGlobal.getSampleConversionFactory().createImage(sample);
	}

	protected DPFPFeatureSet extractFeatures(DPFPSample sample, DPFPDataPurpose purpose)
	{
		DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
		try {
			return extractor.createFeatureSet(sample, purpose);
		} catch (DPFPImageQualityException e)
        {
            System.out.println(" Returning Null");

        	return null;
		}
	}
	


}
