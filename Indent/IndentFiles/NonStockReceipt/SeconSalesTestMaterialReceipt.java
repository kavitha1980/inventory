package Indent.IndentFiles.NonStockReceipt;

import guiutil.FinDateField;
import jdbc.ORAConnection;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;
import java.net.InetAddress;
import util.*;
import javax.swing.table.TableColumnModel;

import com.digitalpersona.onetouch.*;
import com.digitalpersona.onetouch.capture.*;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.*;
import com.digitalpersona.onetouch.verification.*;
import guiutil.*;

public class SeconSalesTestMaterialReceipt extends JInternalFrame 
{

     protected     JLayeredPane Layer;

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BApply;
     JButton       BExit;
     JButton       BSave;

	Vector        VDeptName,VDeptCode,VUserName,VUserCode,VItemName,VItemCode,VTypeCode,VTypeName;
     JTextField    searchField;
	JComboBox     CDept,CUser,CItem,CType;
     Common        common = new Common();
     Connection    theConnection;
	String        SUser,SDept,SItem;
	int           iRECEIPTQTY;
	
     int           iUserCode=0,iMillCode,iOwnerCode,iDeptCode,iCode;
     int           PRINT=11;
     
   FinDateField TFromDate,TToDate;

     ArrayList theList = new ArrayList();
     ArrayList theReturnList = new ArrayList();

     TestMaterialReceiptModel   theModel;

     JTable             theTable;
	InetAddress ip;
	
    private DPFPCapture capturer 			= DPFPGlobal.getCaptureFactory()		.createCapture();
    private DPFPVerification verificator 	= DPFPGlobal.getVerificationFactory()	.createVerification();

    DPFPTemplate template;
	
	ArrayList theTemplateList,theMaterialIssueTemplateList;
	
	int iPunchStatus=0;
	int iSaveCount=0;
	MyLabel      LStatus;
	int iIssueDays=0;
	
     
     public SeconSalesTestMaterialReceipt(JLayeredPane Layer,int iUserCode,int iMillCode,int iOwnerCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
		  this.iOwnerCode = iOwnerCode;

          getIssueDays();
		  setData();
		setData2();
		createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }
	 
public SeconSalesTestMaterialReceipt(JLayeredPane Layer,int iUserCode,int iMillCode,int iOwnerCode,ArrayList theTemplateList,ArrayList theMaterialIssueTemplateList)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
		  this.iOwnerCode = iOwnerCode;
          this.theTemplateList                = theTemplateList;
          this.theMaterialIssueTemplateList = theMaterialIssueTemplateList;

          getIssueDays();
		  setData();
		setData2();
		createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }	 
	
 private void setData()
 {
		VDeptName			= null;
		VDeptCode			= null;
		VUserName			= null;
		VUserCode			= null;
		VItemName			= null;
		VItemCode			= null;
		
		
		VDeptName			= new Vector();
		VDeptCode			= new Vector();	
		VUserName			= new Vector();
		VUserCode			= new Vector();
		VItemName			= new Vector();
		VItemCode			= new Vector();
			
		VDeptName			. clear();
		VDeptCode			. clear();
		VUserName			. clear();
		VUserCode			. clear();
		VItemName           . clear();
		VItemCode           . clear();
		
		VDeptName			. add("All");
		VDeptCode			. add("999999");
		
		VUserName			. add("All");
		VUserCode			. add("999999");
		
		VItemName			. add("All");
		VItemCode			. add("999999");
			
		try
		{
			 if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

			Statement theStatement = theConnection.createStatement();
			
			String QSS = " Select DeptName,DeptCode from Hrdnew.Department order by 1";
			ResultSet re = theStatement.executeQuery(QSS);

			while (re.next())
			{
				VDeptName		. add(common.parseNull(re.getString(1)));
				VDeptCode		. add(common.parseNull(re.getString(2)));
				
			}
			re.close();
			
			String QS46 = " Select UserName,UserCode from Rawuser order by 1";
			ResultSet rss = theStatement.executeQuery(QS46);

			while (rss.next())
			{
				VUserName		. add(common.parseNull(rss.getString(1)));
				VUserCode		. add(common.parseNull(rss.getString(2)));
				
			}
			rss.close();
			
			
			String QS66 = " Select distinct ItemName,Itemcode from Materialindentdetails where storereceiptstatus = 0 order by 1";
			ResultSet rssset = theStatement.executeQuery(QS66);

			while (rssset.next())
			{
				VItemName		. add(common.parseNull(rssset.getString(1)));
				VItemCode		. add(common.parseNull(rssset.getString(2)));
				
			}
			rssset.close();
			
			theStatement 		.close();
			
		}
		catch(Exception ex)
			{
			System.out.println("From MaterialIndentDetails "+ex);
			}
		}	
	
	private void setData2()
	{
		VTypeName			= null;
		VTypeCode			= null;
		
		
		
		VTypeName			= new Vector();
		VTypeCode			= new Vector();	
			
		VTypeName			. clear();
		VTypeCode			. clear();
		
		VTypeName			. add("All");
		VTypeCode			. add("999999");
		
		
		try
		{
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
				Statement theStatement = theConnection.createStatement();
			
				/*String QSS = " Select Code, Name from MaterialIndentReturnType "+ 
							" where ReturnStatus = 1 "+
							" and DisableStatus = 0 "+
							" Order by SortNo, 2 ";*/
							
				String QSS = " Select Code, Name from MaterialIndentReturnType "+ 							
							 " where ReturnStatus = 1  "+
							 " and DisableStatus = 0  "+
							 " and name like'%SECOND%' "+
							 " Order by code  desc  ";
			
			
				ResultSet re = theStatement.executeQuery(QSS);

			while (re.next())
			{
				VTypeName		. add(common.parseNull(re.getString(2)));
				VTypeCode		. add(common.parseNull(re.getString(1)));
				
			}
				re.close();
			
				theStatement 		.close();
			
		}
			catch(Exception ex)
			{
				System.out.println("From MaterialIndentDetails "+ex);
			}
		
		
	}


     private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();

               theModel       = new TestMaterialReceiptModel();
               theTable       = new JTable(theModel);
               
               
               TableColumnModel TC  = theTable.getColumnModel();

                for(int i=0;i<theModel.ColumnName.length;i++)   {

                    TC .getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
                }

               TFromDate      = new FinDateField();
               TToDate        = new FinDateField();

               TFromDate      .   setTodayDate();
               TToDate        .   setTodayDate();
			
			
			CDept       = new JComboBox(new Vector(VDeptName));
		
			CUser       = new JComboBox(new Vector(VUserName));
			
			CItem       = new JComboBox(new Vector(VItemName));
			
			CType       = new JComboBox(new Vector(VTypeName));
			
			//CType		.setSelectedIndex(1);
          
			
               BExit          = new JButton("Exit");
               BApply         = new JButton("Apply");
               //BSave          = new JButton("Save");
			   BSave          = new JButton("Punch");
			   
			   if(iIssueDays>3)
			   {
				   
				   BSave.setEnabled(false);
				   JOptionPane.showMessageDialog(null, "The SecondSales Issue  is Pending.. You Can't Receipt the SecondSales!!", "Information", JOptionPane.INFORMATION_MESSAGE);
			   }
			   else
			   {
				   BSave.setEnabled(true);
			   }
               
			   LStatus        = new MyLabel();
               searchField    = new JTextField();
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }
     }

     private void setLayouts()
     {
            setTitle("Material Receipt Against Return ");
            setClosable(false);                    
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,775,600);

            TopPanel.setLayout(new GridLayout(8,2));
            MiddlePanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());

            TopPanel.setBorder(new TitledBorder("Info."));
            MiddlePanel.setBorder(new TitledBorder("Details"));
            BottomPanel.setBorder(new TitledBorder("Controls"));

            TopPanel            . setBackground(new Color(213,234,255));
            MiddlePanel         .setBackground(new Color(213,234,255));
            BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

   

	   TopPanel            . add(new JLabel("Department"));
        TopPanel            . add(CDept);


        TopPanel            . add(new JLabel("User"));
        TopPanel            . add(CUser);
        
	    TopPanel            . add(new JLabel("Item"));
        TopPanel            . add(CItem);
	   
	   TopPanel            . add(new JLabel("Return Type"));
		TopPanel            . add(CType);
	   
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));
	    TopPanel	    	. add(BApply);
         
        TopPanel            . add(new JLabel("Search"));
        TopPanel            . add(searchField);
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
      

        MiddlePanel.add(new JScrollPane(theTable));

        BottomPanel.add(BExit);
        BottomPanel.add(BSave);
		BottomPanel.add(LStatus);
		        

        getContentPane().add("North",TopPanel);
        getContentPane().add("Center",MiddlePanel);
        getContentPane().add("South",BottomPanel);
        BExit.setMnemonic('E');
        BSave.setMnemonic('S');

     }

    private void addListeners()
    {
          BExit.addActionListener(new ActList());
          BApply.addActionListener(new ActList());
          BSave.addActionListener(new ActList());
          searchField. addKeyListener(new SearchKeyList());
    }
	
	private int getIssueDays()
	{
		
		iIssueDays =0;
		
		try
		{
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
			
			String QS =  " select round(sysdate-max(entrydate)) from MaterialScrabDetails";
			
			//String QS =  " select round(sysdate-max(entrydate)) from ScrapIssueDetails";
			
			Statement theStatement = theConnection.createStatement();
			ResultSet result = theStatement.executeQuery(QS);
			
			while (result.next())
			{
				iIssueDays = result.getInt(1);
				
			}
			result.close();
			theStatement.close();
		}
		catch(Exception ex)
		{
			System.out.println("ex: "+ex);
		}
		return iIssueDays;
		
	}

    public void setTabReport()
    {
          try
          {
			int iUserCode = 0;
			int iDeptCode =0;
			String SItemCode = "";
			
                String SFromDate = common.pureDate((String)TFromDate.toNormal()); 
                String SToDate = common.pureDate((String)TToDate.toNormal()); 
			 
			 String SDept  = common.parseNull((String)CDept.getSelectedItem());
			 if (!SDept.equals("All"))
			  iDeptCode = common.toInt((String)VDeptCode.get(VDeptName.indexOf(SDept)));
			 String SUser  = common.parseNull((String)CUser.getSelectedItem());
			 if (!SUser.equals("All"))
			  iUserCode = common.toInt((String)VUserCode.get(VUserName.indexOf(SUser)));
		   String SItem  = common.parseNull((String)CItem.getSelectedItem());
			 if (!SItem.equals("All"))
			  SItemCode = common.parseNull((String)VItemCode.get(VItemName.indexOf(SItem)));
		  
		  String SType  = common.parseNull((String)CType.getSelectedItem());
			 if (!SType.equals("All"))
			  iCode = common.toInt((String)VTypeCode.get(VTypeName.indexOf(SType)));
		  
		  
		  
		      


                getMaterialIndentReturn(SFromDate,SToDate);

                ArrayList ADetails = getMaterialIndent(SFromDate,SToDate,iDeptCode,iUserCode,SUser,SDept,SItem,SItemCode,SType,iCode);

                for(int i=0;i<ADetails.size();i++)
                {
                     HashMap  row =(HashMap)ADetails.get(i);

                     Vector VTheVect = new Vector();
                     
                     String sItemCode = common.parseNull((String)row.get("ITEMCODE"));
                     String sIndentNo = common.parseNull((String)row.get("INDENTNO"));
                     String sUnitCode = common.parseNull((String)row.get("UNIT_CODE"));
                     String sDeptCode= common.parseNull((String)row.get("DEPTCODE"));
                     String sMachCode = common.parseNull((String)row.get("MACHINECODE"));

					 VTheVect.addElement(String.valueOf(i+1));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEMCODE")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEMNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("RETURNQTY")));
                     VTheVect.addElement(common.parseNull((String)row.get("INDENTNO")));
                     VTheVect.addElement(common.parseNull((String)row.get("UNITNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("DEPTNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("MACHINENAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("USERNAME")));
                     VTheVect.addElement(common.parseNull(common.parseDate((String)row.get("ENTRYDATE"))));
					 
					 if(common.toInt((String)row.get("INDENTNO"))==0) {
						VTheVect.addElement("0");
					 }else {
						VTheVect.addElement(String.valueOf(getRetAlreadyReturned(sItemCode,sIndentNo,sUnitCode,sDeptCode,sMachCode)));
					 }	 
                     
                     VTheVect.addElement(common.parseNull((String)row.get("DELAYDAYS")));
				      VTheVect.addElement(common.parseNull((String)row.get("RETURNTYPE")));
					
					
					if(common.toInt((String)row.get("INDENTNO"))==0) {
						
						                     //VTheVect.addElement(common.parseNull((String)row.get("RETURNQTY")));
											 VTheVect.addElement("");

					}else {
						                     VTheVect.addElement("");

					}
					
					

                     theModel.appendRow(VTheVect);
               }
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
			if(ae.getSource()==BSave)
			{
				moveCursorToOtherCell(0, 0);
				
				if(validations())
				{  
					//if(JOptionPane.showConfirmDialog(null, "Are you sure you want to Save the Details?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
					//if(JOptionPane.showConfirmDialog(null, "Punch to Save the Details..", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
					//{
						
						if(common.parseNull(LStatus.getText()).length()>0) {
							stop();
							LStatus.setText("");
						}
						
						init();
						start();
						BSave.setEnabled(false);
						//BSave.setEnabled(true);
						iPunchStatus=0;    
						
						
						
						//insertReturn();
					//}
				}
			}
			
               if(ae.getSource()==BExit)
               {
				   stop();
                    removeHelpFrame();
               }

               if(ae.getSource()==BApply)
               {
                    theModel.setNumRows(0);
                    setTabReport();
					BSave.setEnabled(true);
               }
          }
     }
     private class KeyList extends KeyAdapter  {

          public void keyPressed(KeyEvent ke)
          {
               int iRow = theTable.getSelectedRow       ();
               int iCol = theTable.getSelectedColumn    ();

               System.out.println("keyPressed");

               if( iCol==13) {

			checkValue(iRow);
               }
           }

          public void keyReleased(KeyEvent ke)   {
             try   {

            int    iRow    = theTable.getSelectedRow();
            int    iCol    = theTable.getSelectedColumn();
            
            System.out.println("keyReleased");

            if( iCol==13) {              
			checkValue(iRow);
            }

         }catch(Exception e)  {
   
            e.printStackTrace();
         }
      }
   }
     private class SearchKeyList extends KeyAdapter
        {
            public void keyReleased(KeyEvent ke)
            {
                searchItems();     
            }

            public void keyPressed(KeyEvent ke)
            {
                searchItems();
            }    

            public void keyTyped(KeyEvent ke)
            {
                searchItems();
            }
        }
	   
	private void moveCursorToOtherCell(int iRow, int iCol) {
		theTable       . setRowSelectionInterval(iRow, iCol);
		Rectangle cell = theTable.getCellRect(iRow, 0, true);
		theTable       . scrollRectToVisible(cell);
		
		theTable		. editCellAt(iRow, iCol);
	}

    private void showMessage(String sMsg)
    {
          JOptionPane.showMessageDialog(null,getMessage(sMsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
    }
    private String getMessage(String sMsg)
    {
          String str = "<html><body>";
          str = str + sMsg;
          str = str + "</body></html>";
          return str;
    }
	
    private void removeHelpFrame()
    {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public ArrayList getMaterialIndent(String SFromDate, String SToDate, int iDeptCode,int iUserCode,String SUser,String SDept, String SItem,String SItemCode,String SType,int iCode)
     {
            theList = new ArrayList();
            StringBuffer sb = new StringBuffer();

	String QS92=		" Select distinct MaterialIndentDetails.ItemCode,MaterialIndentDetails.ItemName,  "+
					//" MaterialReturnDetails.RequiredQty as returnqty,MaterialIndentDetails.IndentNo,   "+
					" MaterialReturnDetails.RETURNQTY as returnqty,MaterialIndentDetails.IndentNo,   "+
					" process_units.UnitName,hrdnew.department.DeptName,nvl(Machine.Mach_Name,'General')  as MACHINENAME,Rawuser.UserName,    "+
					" to_char(MaterialIndentDetails.EntryDate,'yyyymmdd') as EntryDate,MaterialIndentDetails.Unit_code,    "+
					" MaterialIndentDetails.DeptCode,MaterialIndentDetails.MachineCode,Round(SysDate - MaterialIndentDetails.EntryDate) as DelayDays,materialindentreturntype.name as returntype,  "+
					" MaterialIndentDetails.RETURNQTY as rQty, MaterialIndentDetails.Id,MaterialIndentDetails.requiredqty  "+
					//" ,MaterialReturnDetails.Id as returnid "+
					" From MaterialIndentDetails    "+
					" Left Join Machine On Machine.Mach_Code = MaterialIndentDetails.MachineCode   "+  
					" Inner Join process_units On process_units.UnitCode = MaterialIndentDetails.Unit_Code   "+ 
					" Inner Join hrdnew.department On hrdnew.department.DeptCode = MaterialIndentDetails.DeptCode    "+
					//" Inner join Scm.Rawuser on Rawuser.usercode = MaterialIndentDetails.usercode   "+
					" Inner join Scm.Rawuser on Rawuser.usercode = MaterialIndentDetails.OwnerCode   "+
					" inner join MaterialReturnDetails on MaterialReturnDetails.itemcode = MaterialIndentDetails.itemcode  "+
					" and MaterialIndentDetails.IndentNo = MaterialReturnDetails.IndentNo  "+
					" and MaterialReturnDetails.RequiredQty = MaterialIndentDetails.RequiredQty  "+
					" and materialreturndetails.RETURNTYPECODE not in(2,5) "+
					" inner join materialindentreturntype on materialindentreturntype.code = MaterialReturnDetails.returntypecode  "+
					" Where to_char(MaterialIndentDetails.EntryDate,'yyyymmdd') <=to_char(sysdate,'yyyymmdd')   "+
					" and MaterialIndentDetails.RETURNSTATUS=1  "+
					" and MaterialIndentDetails.STORERECEIPTSTATUS=0 ";
					if (!SDept.equals("All"))
					QS92 = QS92 +" and MaterialIndentDetails.DeptCode="+iDeptCode+"  ";
					if (!SUser.equals("All"))
						QS92 = QS92 +" and MaterialIndentDetails.OwnerCode="+iUserCode+"  ";
					//QS92 = QS92 +" and MaterialIndentDetails.UserCode="+iUserCode+"  ";
					if (!SItem.equals("All"))
					QS92 = QS92 +" and MaterialIndentDetails.ItemCode='"+SItemCode+"'  ";
					if (!SType.equals("All"))
					QS92 = QS92 +" and Materialindentreturntype.Code="+iCode+"  ";
					if (SType.equals("All"))
					QS92 = QS92 +" and Materialindentreturntype.Code in(12,13,14)  ";
				
				
	/*QS92 = QS92 + " Union all "+
	
				 " Select distinct ScrapRdc.ItemCode,invitems.Item_Name,   "+
				 "  ScrapRdc.ScrapQty as returnqty,0 as IndentNo,     "+
				  " '' as UnitName,'' as DeptName,'' as MACHINENAME,rawuser.userName,      "+
				  " to_char(ScrapRdc.EntryDate,'yyyymmdd') as EntryDate,0 as Unit_code,      "+
				  " 0 as DeptCode,0 as MachineCode,0 as DelayDays,'RDC' as returntype,    "+
				  " ScrapRdc.ScrapQty	 as rQty, ScrapRdc.Id,0 as requiredqty  "+
   			      " from   "+
				  " ScrapRdc  "+
				  " inner join invitems on invitems.item_code= ScrapRdc.itemcode  "+
				  " inner join rawuser on rawuser.usercode= scraprdc.entryuser "+
				  " Where  scraprdc.StoreREceiptStatus=0 ";

				if (!SUser.equals("All"))
						QS92 = QS92 +" and scraprdc.ENTRYUSER="+iUserCode+"  "; */
					
					

				  
				/*" Select distinct ScrapRdc.ItemCode,invitems.Item_Name,  "+
				" ScrapRdc.ScrapQty as returnqty,0 as IndentNo,    "+
				" '' as UnitName,'' as DeptName,'' as MACHINENAME,Hod.HodName,     "+
				" to_char(ScrapRdc.EntryDate,'yyyymmdd') as EntryDate,0 as Unit_code,     "+
				" 0 as DeptCode,0 as MachineCode,0 as DelayDays,'RDC' as returntype,   "+
				" ScrapRdc.ScrapQty	 as rQty, ScrapRdc.Id,0 as requiredqty "+
				//,0   "+
				" from  "+
				" ScrapRdc "+ 
				" inner join GateInward on ScrapRdc.gino = GateInward .gino "+
				" inner join invitems on invitems.item_code= ScrapRdc.itemcode "+
				" inner join supplier on supplier.ac_code = gateInward.Sup_Code "+
				" inner join rdc on rdc.rdcno = ScrapRdc .rdcno "+
				" inner join  RDCMemo on RDCMemo. memono=rdc.memono "+
				" Left Join Hod on RDCMemo.HodCode=Hod.HodCode  "+
				" Where ScrapRdc.AuthStatus=1 and scraprdc.StoreREceiptStatus=2222 and KEEPINSCRAP=1 ";*/
				
				
				
				
				
				//	QS92 = QS92 +" Order by hrdnew.department.DeptName  ";  
					
					
					
					


            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(QS92);
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theList;
     }
     public ArrayList getMaterialIndentReturn(String SFromDate, String SToDate)
     {
            theReturnList = new ArrayList();

            StringBuffer sb = new StringBuffer();

            sb.append(" select ITEMCODE,INDENTNO,UNIT_CODE,DEPTCODE,MACHINECODE,REQUIREDQTY,RETURNQTY ");
            sb.append(" From MaterialReturnDetails ");
            sb.append(" Where nvl(RETURNQTY,0)<REQUIREDQTY ");
            sb.append(" and to_char(MaterialReturnDetails.EntryDate,'yyyymmdd') <="+SToDate+" and ReturnType=1 ");
		  

            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(sb.toString());
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theReturnList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theReturnList;
     }
     private void checkValue(int iRow){


         double dalreadyRet = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,10)));

         double dretQty = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,13)));
         
         double dReqQty = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,3)));
         
         double dNeedToRetrun = dReqQty-dalreadyRet;

         if(dretQty>dNeedToRetrun){

              showMessage("Return Quantiy is Must be equal or Less Than Required Qty");
          }

    }
	
  /*   private void insertReturn()
	{
         int iError = 0;
         int icheck = 0;
         StringBuffer sb = new StringBuffer();
         StringBuffer sbupdate = new StringBuffer();
         PreparedStatement preparedStat = null;
         PreparedStatement preparedSt= null;
		 
		 PreparedStatement preparedUpdate= null;
         java.util.HashMap theMap = new java.util.HashMap();
	    String iip = "";
		
		
         StringBuffer sbupdate1 = new StringBuffer();
		
	    
		try
		{
		   
			iip = InetAddress.getLocalHost().getHostAddress(); 
		   
			sb.append(" Insert into MaterialReturnDetails ( ID,ITEMCODE,ITEMNAME,REQUIREDQTY,INDENTNO,UNIT_CODE,DEPTCODE,MACHINECODE,USERCODE,ENTRYDATE,RETURNQTY,MACHINENAME,RETURNTYPE,OWNERCODE,STORERECEIPTDATE,SYSTEMNAME,MACHINETABLECODE,RETURNTYPECODE,RdcId ) ");
			sb.append(" Values(MaterialReturnDetails_Seq.nextVal,?,?,?,?,?,?,?,?,sysdate,?,?,1,?,sysdate,'"+iip+"',0,?,?) ");
            
			sbupdate.append(" Update MaterialIndentDetails set STORERECEIPTSTATUS=1 , StoreReceiptDate = SysDate  ") ;  
			sbupdate.append(" where ITEMCODE=? and INDENTNO=? and UNIT_CODE=? and DEPTCODE=? and MACHINECODE=?  "); //and ID = ? 
			
			sbupdate1.append(" Update ScrapRdc set STORERECEIPTSTATUS=1 , StoreReceiptDate = SysDate  ") ;  
			sbupdate1.append(" where Id=? "); //and ID = ? 
			

			if(theConnection ==null) 
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
			
			
			
			
		  
			theConnection			. setAutoCommit(false);
		  
			preparedStat 	= theConnection.prepareStatement(sb.toString());
			preparedSt 		= theConnection.prepareStatement(sbupdate.toString());
			preparedUpdate	= theConnection.prepareStatement(sbupdate1.toString());
            
			for (int index = 0; index < theModel.getRows(); index++) 
			{
				
				if(common.toInt((String)theModel.getValueAt(index,4))>0) {
					
					
					theMap				= (java.util.HashMap) theList.get(index);
					String SITEMCODE  	= common.parseNull((String)theModel.getValueAt(index, 1));
					int iINDENTNO  		= common.toInt((String)theModel.getValueAt(index, 4));

					String SType 		= common.parseNull((String)theModel.getValueAt(index,12));
					iCode 				= common.toInt((String)VTypeCode.get(VTypeName.indexOf(SType)));

					double dretQty 		= common.toDouble(common.parseNull((String)theModel.getValueAt(index,13)));
					
					
					//int iRetId =  common.toInt((String)theMap.get("RETURNID"));
					
			
					if(dretQty > 0)
					{
						double dReqQty 		= 0;
						
						StringBuffer sb11	= null;
						sb11				= new StringBuffer();
							

						sb11.append(" Select Sum(ReturnQty) from MaterialReturnDetails where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' "); 
						sb11.append(" and ReturnFromIndent = 1 ");

						//and id="+iRetId+" ");
						
						
						System.out.println("sb11"+sb11.toString());

						PreparedStatement thePS	= theConnection.prepareStatement(sb11.toString());
						ResultSet	result	 	= thePS.executeQuery();
				
						while(result.next())
						{ 
							dReqQty 			= common.toDouble(result.getString(1));
						}
						thePS                    	. close();
						result						. close();
						
						
						double dalreadyRet 	= 0;
						
						StringBuffer sb1	= null;
						sb1					= new StringBuffer();
						
						sb1.append(" Select Sum(ReturnQty) from MaterialReturnDetails where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' ");
						sb1.append(" and ReturnFromIndent = 0 ");
						sb1.append(" and ReturnType = 1  ");
						//and id="+iRetId+"   ");

						thePS				= theConnection.prepareStatement(sb1.toString());
						result	 			= thePS.executeQuery();
				
						while(result.next())
						{ 
							dalreadyRet		= common.toDouble(result.getString(1));
						}

						thePS                   . close();
						result					. close();
						
						preparedStat.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
						preparedStat.setString(2,common.parseNull((String) theMap.get("ITEMNAME")));
						preparedStat.setString(3,common.parseNull((String) theMap.get("REQUIREDQTY")));
						preparedStat.setString(4,common.parseNull((String) theMap.get("INDENTNO")));
						preparedStat.setString(5,common.parseNull((String) theMap.get("UNIT_CODE")));
						preparedStat.setString(6,common.parseNull((String) theMap.get("DEPTCODE")));
						preparedStat.setString(7,common.parseNull((String) theMap.get("MACHINECODE")));
						preparedStat.setInt(8,iUserCode);
						preparedStat.setString(9,common.parseNull((String)theModel.getValueAt(index,13)));
						preparedStat.setString(10,common.parseNull((String) theMap.get("MACHINENAME")));
						preparedStat.setInt(11,iOwnerCode);
						preparedStat.setInt(12,iCode);
						preparedStat.setInt(13,0);
						preparedStat.executeUpdate();

						double dtotal = (dalreadyRet+dretQty);

						System.out.println("dalreadyRet=="+dalreadyRet);
						System.out.println("dReciptQty=="+dretQty);

						System.out.println("dtotal=="+dtotal);
						
						if(dtotal == dReqQty)
						{
							preparedSt.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
							preparedSt.setString(2,common.parseNull((String) theMap.get("INDENTNO")));
							preparedSt.setString(3,common.parseNull((String) theMap.get("UNIT_CODE")));
							preparedSt.setString(4,common.parseNull((String) theMap.get("DEPTCODE")));
							preparedSt.setString(5,common.parseNull((String) theMap.get("MACHINECODE")));
							preparedSt.executeUpdate();
						}
					}
					
				}else {
					
					theMap				= (java.util.HashMap) theList.get(index);
					String SITEMCODE  	= common.parseNull((String)theModel.getValueAt(index, 1));
					int iINDENTNO  		= common.toInt((String)theModel.getValueAt(index, 4));

					//String SType 		= common.parseNull((String)theModel.getValueAt(index,12));
					iCode 				= 15;

					double dretQty 		= common.toDouble(common.parseNull((String)theModel.getValueAt(index,3)));
					
			
					if(dretQty > 0)
					{
					
						preparedStat.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
						preparedStat.setString(2,common.parseNull((String) theMap.get("ITEMNAME")));
						preparedStat.setString(3,common.parseNull((String) theMap.get("REQUIREDQTY")));
						preparedStat.setString(4,common.parseNull((String) theMap.get("INDENTNO")));
						preparedStat.setString(5,common.parseNull((String) theMap.get("UNIT_CODE")));
						preparedStat.setString(6,common.parseNull((String) theMap.get("DEPTCODE")));
						preparedStat.setString(7,common.parseNull((String) theMap.get("MACHINECODE")));
						preparedStat.setInt(8,iUserCode);
						preparedStat.setString(9,common.parseNull((String)theModel.getValueAt(index,13)));
						preparedStat.setString(10,common.parseNull((String) theMap.get("MACHINENAME")));
						preparedStat.setInt(11,iOwnerCode);
						preparedStat.setInt(12,iCode);
						preparedStat.setInt(13,common.toInt((String) theMap.get("ID")));
						preparedStat.executeUpdate();
						
						preparedUpdate.setString(1,common.parseNull((String) theMap.get("ID")));
						preparedUpdate.executeUpdate();
						
					
					}	
					
					
				}
				
				
				
			}
			
			preparedSt.close();
			preparedStat.close();
			preparedUpdate.close();

			theConnection			. commit();
			theConnection			. setAutoCommit(true);
			
			showMessage("Return Quantity Updated..");
		}
		catch(Exception e)
		{
			try
			{
				theConnection		. rollback();
				theConnection		. setAutoCommit(true);
			}
			catch(Exception ex){}
			iError = 1;
			e.printStackTrace();
			
			showMessage("Return Quantity Not Updated..");
		}
     }
	

*/
     private boolean insertReturn(String sEmpCode,String sName)
	{
         int iError = 0;
         int icheck = 0;
         StringBuffer sb = new StringBuffer();
         StringBuffer sbupdate = new StringBuffer();
         PreparedStatement preparedStat = null;
         PreparedStatement preparedSt= null;
		 
		 PreparedStatement preparedUpdate= null;
         java.util.HashMap theMap = new java.util.HashMap();
	    String iip = "";
		
		
         StringBuffer sbupdate1 = new StringBuffer();
		
	    
		try
		{
			
			System.out.println("comming insert"+iSaveCount);
			
		   
			iip 		= InetAddress.getLocalHost().getHostAddress(); 
		   
			sb.append(" Insert into MaterialReturnDetails ( ID,ITEMCODE,ITEMNAME,REQUIREDQTY,INDENTNO,UNIT_CODE,DEPTCODE,MACHINECODE,USERCODE,ENTRYDATE,RETURNQTY,MACHINENAME,RETURNTYPE,OWNERCODE,STORERECEIPTDATE,SYSTEMNAME,MACHINETABLECODE,RETURNTYPECODE,RdcId,PunchEmpCode,ScrapInspection ) ");
			sb.append(" Values(MaterialReturnDetails_Seq.nextVal,?,?,?,?,?,?,?,?,sysdate,?,?,1,?,sysdate,'"+iip+"',0,?,?,?,?) ");
            
			sbupdate.append(" Update MaterialIndentDetails set STORERECEIPTSTATUS=1 , StoreReceiptDate = SysDate  ") ;  
			sbupdate.append(" where ITEMCODE=? and INDENTNO=? and UNIT_CODE=? and DEPTCODE=? and MACHINECODE=?  "); //and ID = ? 
			
			sbupdate1.append(" Update ScrapRdc set STORERECEIPTSTATUS=1 , StoreReceiptDate = SysDate  ") ;  
			sbupdate1.append(" where Id=? "); //and ID = ? 
			

			if(theConnection ==null) 
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
			
		  
			theConnection			. setAutoCommit(false);
		  
			preparedStat 	= theConnection.prepareStatement(sb.toString());
			preparedSt 		= theConnection.prepareStatement(sbupdate.toString());
			preparedUpdate	= theConnection.prepareStatement(sbupdate1.toString());
            
			for (int index = 0; index < theModel.getRows(); index++) 
			{
				
				if(common.toInt((String)theModel.getValueAt(index,4))>0) {
					
					
					theMap				= (java.util.HashMap) theList.get(index);
					
					String SITEMCODE  	= common.parseNull((String)theModel.getValueAt(index, 1));
					int iINDENTNO  		= common.toInt((String)theModel.getValueAt(index, 4));

					String SType 		= common.parseNull((String)theModel.getValueAt(index,12));
					iCode 				= common.toInt((String)VTypeCode.get(VTypeName.indexOf(SType)));

					double dretQty 		= common.toDouble(common.parseNull((String)theModel.getValueAt(index,13)));
					//int iRetId =  common.toInt((String)theMap.get("RETURNID"));

					String sMach =  common.parseNull((String) theMap.get("MACHINECODE"));
					
				    String sUnitCode = common.parseNull((String)theMap.get("UNIT_CODE"));


			
					if(dretQty > 0)
					{
						double dReqQty 		= 0;
						
						StringBuffer sb11	= null;
						sb11				= new StringBuffer();
							
						sb11.append(" Select Sum(ReturnQty) from MaterialReturnDetails where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' "); 
						sb11.append(" and ReturnFromIndent = 1 ");
						sb11.append(" and MACHINECODE = '"+sMach+"' ");
						sb11.append(" and UNIT_CODE = '"+sUnitCode+"'  and RETURNTYPECODE not in(2,5)  ");
						
						//" and id="+iRetId+" ");

						PreparedStatement thePS	= theConnection.prepareStatement(sb11.toString());
						ResultSet	result	 	= thePS.executeQuery();
				
						while(result.next())
						{ 
							dReqQty 			= common.toDouble(result.getString(1));
						}
						thePS                    	. close();
						result						. close();
						
						
						System.out.println("sb1 req qry "+sb11.toString());
						
						
						double dalreadyRet 	= 0;
						
						StringBuffer sb1	= null;
						sb1					= new StringBuffer();
						
						sb1.append(" Select Sum(ReturnQty) from MaterialReturnDetails where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' ");
						sb1.append(" and ReturnFromIndent = 0 ");
						sb1.append(" and ReturnType = 1 ");
						sb1.append(" and MACHINECODE = '"+sMach+"' ");
						sb1.append(" and UNIT_CODE = '"+sUnitCode+"' and RETURNTYPECODE not in(2,5) ");

						
						//and id="+iRetId+"  ");

						thePS				= theConnection.prepareStatement(sb1.toString());
						result	 			= thePS.executeQuery();
				
						while(result.next())
						{ 
							dalreadyRet		= common.toDouble(result.getString(1));
						}

						thePS                   . close();
						result					. close();
						
						preparedStat.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
						preparedStat.setString(2,common.parseNull((String) theMap.get("ITEMNAME")));
						preparedStat.setString(3,common.parseNull((String) theMap.get("REQUIREDQTY")));
						preparedStat.setString(4,common.parseNull((String) theMap.get("INDENTNO")));
						preparedStat.setString(5,common.parseNull((String) theMap.get("UNIT_CODE")));
						preparedStat.setString(6,common.parseNull((String) theMap.get("DEPTCODE")));
						preparedStat.setString(7,common.parseNull((String) theMap.get("MACHINECODE")) );
						preparedStat.setInt(8,iUserCode);
						preparedStat.setString(9,common.parseNull((String)theModel.getValueAt(index,13)));
						preparedStat.setString(10,common.parseNull((String) theMap.get("MACHINENAME")));
						preparedStat.setInt(11,iOwnerCode);
						preparedStat.setInt(12,iCode);
						preparedStat.setInt(13,0);
						
						preparedStat.setString(14,sEmpCode);
						preparedStat.setInt(15,0);
						preparedStat.executeUpdate();

						double dtotal = (dalreadyRet+dretQty);

						System.out.println("dalreadyRet=="+dalreadyRet);
						System.out.println("dReciptQty=="+dretQty);

						System.out.println("dtotal=="+dtotal);
						
						System.out.println("dReqQty=="+dReqQty);
						
						
						if(dtotal == dReqQty)
						{
							preparedSt.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
							preparedSt.setString(2,common.parseNull((String) theMap.get("INDENTNO")));
							preparedSt.setString(3,common.parseNull((String) theMap.get("UNIT_CODE")));
							preparedSt.setString(4,common.parseNull((String) theMap.get("DEPTCODE")));
							preparedSt.setString(5,common.parseNull((String) theMap.get("MACHINECODE")));
							preparedSt.executeUpdate();
						}
					}
					
				}else {
					
					theMap				= (java.util.HashMap) theList.get(index);
					String SITEMCODE  	= common.parseNull((String)theModel.getValueAt(index, 1));
					int iINDENTNO  		= common.toInt((String)theModel.getValueAt(index, 4));

					//String SType 		= common.parseNull((String)theModel.getValueAt(index,12));
					iCode 				= 15;

					//double dretQty 		= common.toDouble(common.parseNull((String)theModel.getValueAt(index,3)));
					
					double dretQty 		= common.toDouble(common.parseNull((String)theModel.getValueAt(index,13)));
					
			
					if(dretQty > 0)
					{
					
						preparedStat.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
						preparedStat.setString(2,common.parseNull((String) theMap.get("ITEMNAME")));
						preparedStat.setString(3,common.parseNull((String) theMap.get("REQUIREDQTY")));
						preparedStat.setString(4,common.parseNull((String) theMap.get("INDENTNO")));
						preparedStat.setString(5,common.parseNull((String) theMap.get("UNIT_CODE")));
						preparedStat.setString(6,common.parseNull((String) theMap.get("DEPTCODE")));
						preparedStat.setString(7,common.parseNull((String) theMap.get("MACHINECODE")));
						preparedStat.setInt(8,iUserCode);
						preparedStat.setString(9,common.parseNull((String)theModel.getValueAt(index,13)));
						preparedStat.setString(10,common.parseNull((String) theMap.get("MACHINENAME")));
						preparedStat.setInt(11,iOwnerCode);
						preparedStat.setInt(12,iCode);
						preparedStat.setInt(13,common.toInt((String) theMap.get("ID")));
						preparedStat.setString(14,sEmpCode);
						preparedStat.setInt(15,0);
						preparedStat.executeUpdate();
						
						preparedUpdate.setString(1,common.parseNull((String) theMap.get("ID")));
						preparedUpdate.executeUpdate();
						
					
					}	
					
					
				}
				
				
				
			}
			
			preparedSt.close();
			preparedStat.close();
			preparedUpdate.close();

			theConnection			. commit();
			theConnection			. setAutoCommit(true);
			
			iSaveCount++;
			
			showMessage("Return Quantity Updated..");
			theModel.setNumRows(0);
			setTabReport();
		}
		catch(Exception e)
		{
			try
			{
				theConnection		. rollback();
				theConnection		. setAutoCommit(true);
			}
			catch(Exception ex){}
			iError = 1;
			e.printStackTrace();
				showMessage("Return Quantity Not Updated..");
			return false;
			
		
		}
		return true;
     }
	
	
    /* private void insertReturn()
	{
         int iError = 0;
         int icheck = 0;
         StringBuffer sb = new StringBuffer();
         StringBuffer sbupdate = new StringBuffer();
         PreparedStatement preparedStat = null;
         PreparedStatement preparedSt= null;
         java.util.HashMap theMap = new java.util.HashMap();
	    String iip = "";
	    
		try
		{
		   
			iip = InetAddress.getLocalHost().getHostAddress(); 
		   
			sb.append(" Insert into MaterialReturnDetails ( ID,ITEMCODE,ITEMNAME,REQUIREDQTY,INDENTNO,UNIT_CODE,DEPTCODE,MACHINECODE,USERCODE,ENTRYDATE,RETURNQTY,MACHINENAME,RETURNTYPE,OWNERCODE,STORERECEIPTDATE,SYSTEMNAME,MACHINETABLECODE,RETURNTYPECODE ) ");
			sb.append(" Values(MaterialReturnDetails_Seq.nextVal,?,?,?,?,?,?,?,?,sysdate,?,?,1,?,sysdate,'"+iip+"',0,?) ");
            
			sbupdate.append(" Update MaterialIndentDetails set STORERECEIPTSTATUS=1 , StoreReceiptDate = SysDate  ") ;  
			sbupdate.append(" where ITEMCODE=? and INDENTNO=? and UNIT_CODE=? and DEPTCODE=? and MACHINECODE=?  "); //and ID = ? 

			if(theConnection ==null) 
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
		  
			theConnection			. setAutoCommit(false);
		  
			preparedStat = theConnection.prepareStatement(sb.toString());
			preparedSt = theConnection.prepareStatement(sbupdate.toString());
            
			for (int index = 0; index < theModel.getRows(); index++) 
			{
				theMap = (java.util.HashMap) theList.get(index);
				
				String SITEMCODE  = common.parseNull((String)theModel.getValueAt(index, 1));
				int iINDENTNO  = common.toInt((String)theModel.getValueAt(index, 4));

				String SType = common.parseNull((String)theModel.getValueAt(index,12));
				iCode = common.toInt((String)VTypeCode.get(VTypeName.indexOf(SType)));


				double dretQty = common.toDouble(common.parseNull((String)theModel.getValueAt(index,13)));
				
		
					if(dretQty > 0)
					{
						double dReqQty = 0;
					
						StringBuffer sb11	= null;
						sb11				= new StringBuffer();

					sb11.append(" Select Sum(ReturnQty) from MaterialReturnDetails where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' "); 
					sb11.append(" and ReturnFromIndent = 1 ");

					PreparedStatement thePS	= theConnection.prepareStatement(sb11.toString());
					ResultSet	result	 	= thePS.executeQuery();
			
					while(result.next())
					{ 
						dReqQty 			= common.toDouble(result.getString(1));
					}
					
					
					thePS                    . close();
					result				. close();
					
					double dalreadyRet = 0;
					
					StringBuffer sb1	= null;
					sb1				= new StringBuffer();
					
					sb1.append(" Select Sum(ReturnQty) from MaterialReturnDetails where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' ");
					sb1.append(" and ReturnFromIndent = 0 ");
					sb1.append(" and ReturnType = 1  ");

					thePS				= theConnection.prepareStatement(sb1.toString());
					result	 			= thePS.executeQuery();
			
					while(result.next())
					{ 
						dalreadyRet		= common.toDouble(result.getString(1));
					}

					thePS                    . close();
					result				. close();
					
					preparedStat.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
					preparedStat.setString(2,common.parseNull((String) theMap.get("ITEMNAME")));
					preparedStat.setString(3,common.parseNull((String) theMap.get("REQUIREDQTY")));
					preparedStat.setString(4,common.parseNull((String) theMap.get("INDENTNO")));
					preparedStat.setString(5,common.parseNull((String) theMap.get("UNIT_CODE")));
					preparedStat.setString(6,common.parseNull((String) theMap.get("DEPTCODE")));
					preparedStat.setString(7,common.parseNull((String) theMap.get("MACHINECODE")));
					preparedStat.setInt(8,iUserCode);
					preparedStat.setString(9,common.parseNull((String)theModel.getValueAt(index,13)));
					preparedStat.setString(10,common.parseNull((String) theMap.get("MACHINENAME")));
					preparedStat.setInt(11,iOwnerCode);
					preparedStat.setInt(12,iCode);
					preparedStat.executeUpdate();

					double dtotal = (dalreadyRet+dretQty);

					System.out.println("dalreadyRet=="+dalreadyRet);
					System.out.println("dReciptQty=="+dretQty);

					System.out.println("dtotal=="+dtotal);
					
					if(dtotal == dReqQty)
					{
						preparedSt.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
						preparedSt.setString(2,common.parseNull((String) theMap.get("INDENTNO")));
						preparedSt.setString(3,common.parseNull((String) theMap.get("UNIT_CODE")));
						preparedSt.setString(4,common.parseNull((String) theMap.get("DEPTCODE")));
						preparedSt.setString(5,common.parseNull((String) theMap.get("MACHINECODE")));
						preparedSt.executeUpdate();
					}
				}
			}
			
			preparedSt.close();
			preparedStat.close();

			theConnection			. commit();
			theConnection			. setAutoCommit(true);
			
			showMessage("Return Quantity Updated..");
			theModel.setNumRows(0);
			setTabReport();
		}
		catch(Exception e)
		{
			try
			{
				theConnection		. rollback();
				theConnection		. setAutoCommit(true);
			}
			catch(Exception ex){}
			iError = 1;
			e.printStackTrace();
			
			showMessage("Return Quantity Not Updated..");
		}
     }*/

     private double getRetAlreadyReturned(String sItemCode,String sIndnetNo,String sUnitCode,String sDeptCode,String sMachCode){

         double dalreadyRet = 0;
         java.util.HashMap theMap = new java.util.HashMap();
         
         for (int index = 0; index < theReturnList.size(); index++) {
             
             theMap = (java.util.HashMap) theReturnList.get(index);
             
            String sItem =  common.parseNull((String) theMap.get("ITEMCODE"));
            String sIndnent =  common.parseNull((String) theMap.get("INDENTNO"));
            String sUnit =  common.parseNull((String) theMap.get("UNIT_CODE"));
            String sDept =  common.parseNull((String) theMap.get("DEPTCODE"));
            String sMach =  common.parseNull((String) theMap.get("MACHINECODE"));
            
            if(sItemCode.equals(sItem) && sIndnetNo.equals(sIndnent) && sUnitCode.equals(sUnit) && sDeptCode.equals(sDept) && sMachCode.equals(sMach)){

                    dalreadyRet = common.toDouble(common.parseNull((String) theMap.get("RETURNQTY")));

            }
        }
         return dalreadyRet;
     }
     private void searchItems()
     {
          String SSearchStr        = common.parseNull(searchField.getText()).trim();

          int iIndex               = -1;

          for(int i=0; i<theModel.getRowCount(); i++)
          {
               String SItemName    = common.parseNull((String)theModel.getValueAt(i, 2));

               if(SItemName.startsWith(SSearchStr.toUpperCase()))
               {
                    iIndex         = i;
                    break;
               }
          }
          if(iIndex != -1)
          {
                 theTable       . setRowSelectionInterval(iIndex, iIndex);
                 Rectangle theRect   = theTable.getCellRect(iIndex, 1, true);
                 theTable       . scrollRectToVisible(theRect);
                 theTable       . setSelectionBackground(Color.red);			   
          }
     }

	private boolean validations()
	{
		

		
		try
		{
				
			if ( ((String)CUser.getSelectedItem()).equals("All")){
			
				JOptionPane.showMessageDialog(null, "User Selection All Cant Allowed For Save..Select Department User.."				, "Error!", JOptionPane.ERROR_MESSAGE);
				return false;
			}
			
			
			
			
			
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}

			for(int i=0; i<theModel.getRowCount(); i++)
			{
				int iRECEIPTQTY  = common.toInt((String)theModel.getValueAt(i, 13));			
				String SITEMCODE  = common.parseNull((String)theModel.getValueAt(i, 1));
				int iINDENTNO  = common.toInt((String)theModel.getValueAt(i, 4));
				double dretQty = common.toDouble(common.parseNull((String)theModel.getValueAt(i,13)));
				
				double dIndentRetQty= common.toDouble(common.parseNull((String)theModel.getValueAt(i,3)));
				
				
				if(common.toInt((String)theModel.getValueAt(i,4))<=0 && dretQty>0) {
					
					if(dIndentRetQty!=dretQty) {
						
						JOptionPane.showMessageDialog(null, "Internal Servic Scrap Item Quantity is Must be equal to Indent Return Qty.. Check Row No. "+(i+1), "Error!", JOptionPane.ERROR_MESSAGE);
						return false;
					}
				}				
				
				
				if(dretQty>dIndentRetQty) {
					
						JOptionPane.showMessageDialog(null, "Return Quantity is Must be equal or Less Than Indent Return Qty.. Check Row No. "+(i+1), "Error!", JOptionPane.ERROR_MESSAGE);
						return false;
				}
				


				HashMap theMap				= (java.util.HashMap) theList.get(i);
				
				String sMach =  common.parseNull((String) theMap.get("MACHINECODE"));
				String sUnitCode=  common.parseNull((String) theMap.get("UNIT_CODE"));
				

				
                 if(dretQty>0 &&  common.toInt((String)theModel.getValueAt(i,4))>0)
				{
					double dReturnQty	= 0, dRecdQty = 0 , dRecptQty = 0;
					
					StringBuffer sb	= null;
					sb				= new StringBuffer();

					sb.append(" Select Sum(ReturnQty) from MaterialReturnDetails where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' "); 
					sb.append(" and ReturnFromIndent = 1 ");
					sb.append(" and MACHINECODE = '"+sMach+"' ");
					sb.append(" and UNIT_CODE = '"+sUnitCode+"' and RETURNTYPECODE not in(2,5)  ");
					
					
					//" and id="+iRetId+"   ");

					PreparedStatement thePS	= theConnection.prepareStatement(sb.toString());
					ResultSet	result	 	= thePS.executeQuery();
			
					while(result.next())
					{ 
						dReturnQty 		= common.toDouble(result.getString(1));
					}
					
					thePS                    . close();
					result				. close();
					
					StringBuffer sb1	= null;
					sb1				= new StringBuffer();
					
					sb1.append(" Select Sum(ReturnQty) from MaterialReturnDetails where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' ");
					sb1.append(" and ReturnFromIndent = 0 ");
					sb1.append(" and ReturnType = 1 ");
					sb1.append(" and MACHINECODE = '"+sMach+"' ");
					sb1.append(" and UNIT_CODE = '"+sUnitCode+"' and RETURNTYPECODE not in(2,5)  ");
					//and id="+iRetId+"    ");

					thePS				= theConnection.prepareStatement(sb1.toString());
					result	 			= thePS.executeQuery();
			
					while(result.next())
					{ 
						dRecdQty 			= common.toDouble(result.getString(1));
					}
					
					dRecptQty				= dRecdQty + dretQty;
					
					thePS                    . close();
					result				. close();
					
					System.out.println("dRecdQty"+dRecdQty);
					System.out.println("dReturnQty"+dReturnQty);
					System.out.println("dRecdQty"+dRecptQty);
					
					if(dRecptQty > dReturnQty)
					{
						JOptionPane.showMessageDialog(null, "Return Quantity is Must be equal or Less Than Indent Return Qty.. Check Row No. "+(i+1), "Error!", JOptionPane.ERROR_MESSAGE);
						return false;
					}
				}
				
				
				
				
				
				else if( dretQty>0 &&  common.toInt((String)theModel.getValueAt(i,4))<=0 ) {
					
				
					if(common.toDouble((String)theModel.getValueAt(i,3))!= dretQty)
					{
						JOptionPane.showMessageDialog(null, "Return Quantity and Receipt Qty is Must be equal For RDC Return.. Check Row No. "+(i+1), "Error!", JOptionPane.ERROR_MESSAGE);
						return false;
					}
					
				}
				
			}
			
			return true;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			
			JOptionPane.showMessageDialog(null, "Problem while Checking Data..", "Error!", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}
	
	
     protected void init()        
     {
            capturer = DPFPGlobal.getCaptureFactory().createCapture();
        
                
		capturer.addDataListener(new DPFPDataAdapter() {
                   
                public void dataAcquired(final DPFPDataEvent e) {
                  
                  
                    LStatus.setText(" Please wait Under processing.................");

				SwingUtilities.invokeLater(new Runnable()
                                {
                                        public void run()
                                        {
                                    
                                                LStatus.setText("The fingerprint sample was captured....");
                                                process(e.getSample());
                                                
                                        }});
			}
		});
               
		capturer.addReaderStatusListener(new DPFPReaderStatusAdapter() {
                public void readerConnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
                                    
		 			makeReport("The fingerprint reader was connected.");
				}});
			}
                public void readerDisconnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was disconnected.");
				}});
			}
		});
		capturer.addSensorListener(new DPFPSensorAdapter() {
                public void fingerTouched(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was touched.");
				}});
			}
                public void fingerGone(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The finger was removed from the fingerprint reader.");
				}});
			}
		});
		capturer.addImageQualityListener(new DPFPImageQualityAdapter() {
                public void onImageQuality(final DPFPImageQualityEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					if (e.getFeedback().equals(DPFPCaptureFeedback.CAPTURE_FEEDBACK_GOOD))
						makeReport("The quality of the fingerprint sample is good.");
					else
						makeReport("The quality of the fingerprint sample is poor.");
				}});
			}
		});
	}

	protected void process(DPFPSample sample)
	{
		// Draw fingerprint sample image.
		
		DPFPFeatureSet features = extractFeatures(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

		// Check quality of the sample and start verification if it's good
		int iStatus=0;
   	        String SName  = "";
		String SCode  = "";
                LStatus.setText(" Under processing.................");
				
				

		if (features != null)
		{
			// Compare the feature set with our template



               for(int i=0; i<theMaterialIssueTemplateList.size(); i++)
               {
                    HashMap theMap = (HashMap) theMaterialIssueTemplateList.get(i);

                    SCode    = (String)theMap.get("EMPCODE");
                    SName    = (String)theMap.get("DISPLAYNAME");
                    
                    DPFPTemplate template   = (DPFPTemplate)theMap.get("TEMPLATE");

                    DPFPVerification matcher= DPFPGlobal.getVerificationFactory().createVerification();
                    matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);

                    DPFPVerificationResult result = 
                    matcher.verify(features,template);

                    if (result.isVerified())
                    {
                         LStatus.setText(""+SName);       
                         iStatus=1;
                         break;
                    }
               }


               if(iStatus==0)
               {

                    for(int i=0; i<theTemplateList.size(); i++)
                    {
     
                         HashMap theMap = (HashMap) theTemplateList.get(i);
     
                         SCode    = (String)theMap.get("EMPCODE");
                         SName    = (String)theMap.get("DISPLAYNAME");
                         
                         DPFPTemplate template   = (DPFPTemplate)theMap.get("TEMPLATE");
     
                         DPFPVerification matcher= DPFPGlobal.getVerificationFactory().createVerification();
                         matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);
     
                         DPFPVerificationResult result = 
                         matcher.verify(features,template);
     
                         if (result.isVerified())
                         {
                                LStatus.setText(""+SName);       

                              iStatus=1;
                              break;
                         }
                    }
               }

                  if(iStatus==0)
                  {
                         LStatus		.	setText("No Match Found");       
                         JOptionPane	.	showMessageDialog(null," No Match Found Punch Again");
                         //stop();
                         iPunchStatus=0;
                         //BSave.setEnabled(true);


                  }
                  else if(iSaveCount==0)
                  {
					  
                    if(isReceiverEligible(SCode))
                    {
                      if(insertReturn(SCode,SName))
                      {
                            //JOptionPane.showMessageDialog(null," Item Received Sucessfully");
                           // removeHelpFrame();
                            
                           // LStatus.setText(" Punch And Take Issue");
						   
						   
						   //if(iSaveCount==0) {

                            stop();
                            setData();
                            iPunchStatus=0;
                            BSave.setEnabled(true);
							iSaveCount=0;
							theModel.setNumRows(0);
							setTabReport();
							LStatus.setText("");
							
							//BPunch.setEnabled(true);
							
						   
						   //}
                           
					   }
                       else
                       {
                            JOptionPane.showMessageDialog(null," Problem in Storing Data");
                            stop();
                            iPunchStatus=0;
                            BSave.setEnabled(true);
							//BPunch.setEnabled(true);
                       }
                   }
                   else
                   {
                      JOptionPane.showMessageDialog(null," You are not eligible to Receive this Material");
                      //27.08.2020 stop();
                      setData();
                      BSave.setEnabled(false);
                   }

                }
          }
	}
	
     private boolean isReceiverEligible(String SEmployeeCode)
     {
          boolean bEligible=false;
          //int iRDCUserCode = getRDCUserCode();
          StringBuffer sb  = new StringBuffer();
          //sb.append(" Select EmpCode from IssueReceiverList where UserCode ="+iRDCUserCode);
		  
		  int iRawUser=0;
		  
			 
			
				String SUser=(String)CUser.getSelectedItem();
			 
			  iRawUser = common.toInt((String)VUserCode.get(VUserName.indexOf(SUser)));
		  
		  
		  sb.append(" Select EmpCode from IssueReceiverList where UserCode ="+iRawUser+" and EmpCode="+SEmployeeCode);
		  
		  		  System.out.println("semp valid"+sb.toString());

          try
          {
              ORAConnection   oraConnection =  ORAConnection.getORAConnection();
              Connection      theConnection =  oraConnection.getConnection();               
			  
			  
			  
			  
              Statement theStatement    = theConnection.createStatement();
              ResultSet result          = theStatement.executeQuery(sb.toString());
              while(result.next())
              {
                 String SEmpCode = result.getString(1);
				 
				System.out.println("result empcode"+SEmpCode);

				 
                 if(SEmpCode.equals(SEmployeeCode))
                   bEligible = true; 
              }
              result.close();
			  
          }
          catch(Exception ex)
          {
             System.out.println(" isReceiver Eligible"+ex);
          }
		  
		  //bEligible=false;
		  
        return bEligible;
     }
	
	
	protected void start()
	{
            capturer.startCapture();
	}

	protected void stop()
	{
		System.out.println("comm into stop");
             capturer.stopCapture();
	}
	public void setPrompt(String string) {
	}
	public void makeReport(String string) {
            System.out.println(string);
	}
	
	protected Image convertSampleToBitmap(DPFPSample sample) {
		return DPFPGlobal.getSampleConversionFactory().createImage(sample);
	}

	protected DPFPFeatureSet extractFeatures(DPFPSample sample, DPFPDataPurpose purpose)
	{
		DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
		try {
			return extractor.createFeatureSet(sample, purpose);
		} catch (DPFPImageQualityException e)
        {
            System.out.println(" Returning Null");

        	return null;
		}
	}
	


}
