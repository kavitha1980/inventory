package Indent.IndentFiles.NonStockReceipt;

import javax.swing.table.*;
import java.util.*;

public class SecondSalesReceiptModel extends DefaultTableModel
{

     String ColumnName[]   = {"SL.NO","ITEMCODE","ITEMNAME","RECEIPTQTY","UNITRATE","NETVALUE","INDENTNO","UNITNAME","DEPTNAME","MACHINENAME","RETURNTYPE","USER","SELECT"};
     String ColumnType[]   = {"S","S","S","N","N","N","N","S","S","S","S","S","B"};
     int  iColumnWidth[] = {20,20,60,20,20,20,20,30,30,30,30,30,10};


     public SecondSalesReceiptModel()
     {
          setDataVector(getRowData(),ColumnName);
     }

     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";
		
     			
          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}
