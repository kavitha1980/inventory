package Indent.IndentFiles.NonStockReceipt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;


public class SecondSalesToYardDetailsReport extends JInternalFrame 
{

	protected     JLayeredPane Layer;

	JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
	JTabbedPane   thePane;
	JButton       BApply,BProcess;
	JButton       BExit;
	DateField      TStDate;
	DateField      TEnDate; 
	JTextField    searchField;
	Common        common = new Common();
	Connection     theConnection;
	JComboBox      JCType;

	int           iUserCode=0,iMillCode,iOwnerCode;
	int PRINT=7;

	int iTotalColumns = 6;
	int iWidth[] = {3,10,35,25,10,10}; //,50,50,50};

	FinDateField TFromDate,TToDate;

	ArrayList theList = new ArrayList();
	ArrayList theReturnList = new ArrayList();

	ScrabDetailsModel   theModel;

	JTable             theTable;


	FileWriter FW;
	File file;

	String   SFileName;
	PdfPTable table;

	Document document;

	private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 11, Font.BOLD);
	private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
	private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 9, Font.NORMAL);
	private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
	private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 5, Font.BOLD);

     public SecondSalesToYardDetailsReport(JLayeredPane Layer,int iUserCode,int iMillCode,int iOwnerCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
		  this.iOwnerCode = iOwnerCode;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }

     private void createComponents()
     {
          try
          {
				TopPanel       = new JPanel();
				MiddlePanel    = new JPanel();
				BottomPanel    = new JPanel();
				thePane        = new JTabbedPane();

				theModel       = new ScrabDetailsModel();
				theTable       = new JTable(theModel);

				TStDate      = new DateField();
				TEnDate        = new DateField();
				
				TStDate.setTodayDate();
				TEnDate.setTodayDate();

				JCType = new JComboBox();
				//JCType.addItem("SCRAP");
				JCType.addItem("SECOND SALES");

				BExit          = new JButton("Exit");
				BApply         = new JButton("Apply");
				BProcess          = new JButton("PDF");

				searchField    = new JTextField();
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }
     }

     private void setLayouts()
     {
			setTitle(" SECOND SALES ISSUE TO SCRAB YARD REPORT");
			setClosable(true);                    
			setMaximizable(true);
			setIconifiable(true);
			setResizable(true);
			setBounds(0,0,775,600);

			TopPanel.setLayout(new GridLayout(6,4,2,2));
			MiddlePanel.setLayout(new BorderLayout());
			BottomPanel.setLayout(new FlowLayout());

			TopPanel.setBorder(new TitledBorder("Info."));
			MiddlePanel.setBorder(new TitledBorder("Details"));
			BottomPanel.setBorder(new TitledBorder("Controls"));

			TopPanel            . setBackground(new Color(213,234,255));
			MiddlePanel         .setBackground(new Color(213,234,255));
			BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

			TopPanel            . add(new JLabel("FromDate"));
			TopPanel            . add(TStDate);
			TopPanel            . add(new JLabel("ToDate"));
			TopPanel            . add(TEnDate);

			TopPanel            . add(new JLabel("Return Type"));
			TopPanel	    	   . add(JCType);
			TopPanel            . add(new JLabel(""));
			TopPanel	    	  . add(BApply);
			

			TopPanel            . add(new JLabel("Search"));
			TopPanel            . add(searchField);

			TopPanel            . add(new JLabel(""));
			TopPanel            . add(new JLabel(""));


			MiddlePanel.add(new JScrollPane(theTable));

			BottomPanel.add(BExit);
			BottomPanel.add(BProcess);

			getContentPane().add("North",TopPanel);
			getContentPane().add("Center",MiddlePanel);
			getContentPane().add("South",BottomPanel);
			BExit.setMnemonic('E');

     }

    private void addListeners()
    {
		BExit.addActionListener(new ActList());
		BApply.addActionListener(new ActList());
		searchField. addKeyListener(new SearchKeyList());
		BProcess.addActionListener(new ActList());
    }

    public void setTabReport()
    {
          try
          {

                String SFromDate = common.pureDate((String)TStDate.toNormal()); 
                String SToDate = common.pureDate((String)TEnDate.toNormal());
				String SType  = common.parseNull((String)JCType.getSelectedItem());					


                ArrayList ADetails = getMaterialIndent(SFromDate,SToDate,SType);
				System.out.println("ADetails.size()-->"+ADetails.size());
                for(int i=0;i<ADetails.size();i++)
                {
					HashMap  row =(HashMap)ADetails.get(i);

					Vector VTheVect = new Vector();

					/*String sItemCode = common.parseNull((String)row.get("ITEMCODE"));
					String sIndentNo = common.parseNull((String)row.get("INDENTNO"));
					String sUnitCode = common.parseNull((String)row.get("UNIT_CODE"));
					String sDeptCode= common.parseNull((String)row.get("DEPTCODE"));
					String sMachCode = common.parseNull((String)row.get("MACHINECODE"));*/

					VTheVect.addElement(String.valueOf(i+1));
					VTheVect.addElement(common.parseNull((String)row.get("ITEMCODE")));
					VTheVect.addElement(common.parseNull((String)row.get("ITEMNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("DEPTNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("RECEIPTQTY")));
					VTheVect.addElement(common.parseNull((String)row.get("INDENTNO")));
					VTheVect.addElement(common.parseNull((String)row.get("SCRABQTY")));
					VTheVect.addElement(common.parseNull(common.parseDate((String)row.get("ENTRYDATE"))));

					theModel.appendRow(VTheVect);
               }
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
            
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }

               if(ae.getSource()==BApply)
               {
                    theModel.setNumRows(0);
                    setTabReport();
               }
         
			if(ae.getSource()==BProcess) 
			{
			 try
			 {
				 String SType  = common.parseNull((String)JCType.getSelectedItem());	
				if (SType.equals("SCRAP"))
				{
					SFileName = "d:\\MaterialReport\\SCRABRECEIPTREPORTFROMSTORE .pdf"; 
				}
				else
				{
					SFileName = "d:\\MaterialReport\\SECONDSALESFROMSTORE .pdf";
				}
				createPDFFile();
				File theFile   = new File(SFileName);
				Desktop        . getDesktop() . open(theFile);
			 }
			  catch(Exception ex){
			  ex.printStackTrace();
			}
			 
			}

	    }
     }
      private class SearchKeyList extends KeyAdapter
        {
            public void keyReleased(KeyEvent ke)
            {
                searchItems();     
            }

            public void keyPressed(KeyEvent ke)
            {
                searchItems();
            }    

            public void keyTyped(KeyEvent ke)
            {
                searchItems();
            }
        }
		
 private void searchItems()
 {
          String SSearchStr        = common.parseNull(searchField.getText()).trim();

          int iIndex               = -1;

          for(int i=0; i<theModel.getRowCount(); i++)
          {
               String SItemName    = common.parseNull((String)theModel.getValueAt(i, 2));

               if(SItemName.startsWith(SSearchStr.toUpperCase()))
               {
                    iIndex         = i;
                    break;
               }
          }
          if(iIndex != -1)
          {
                 theTable       . setRowSelectionInterval(iIndex, iIndex);
				 theTable       . setSelectionBackground(Color.green);			   
          }
     }
  
    private void removeHelpFrame()
    {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public ArrayList getMaterialIndent(String SFromDate, String SToDate,String SType)
     {
            theList = new ArrayList();
            StringBuffer sb = new StringBuffer();

					  
			
			sb.append(" select Materialscrabdetails.itemcode,Materialscrabdetails.itemname,Materialscrabdetails.receiptqty,Materialscrabdetails.indentno, ");
			sb.append(" Materialscrabdetails.scrabqty,");
			sb.append(" to_char(Materialscrabdetails.EntryDate,'dd.mm.yyyy hh24:mi:ss') as entrydate,Hrdnew.department.deptname from Materialscrabdetails");
			sb.append(" left join Hrdnew.department on department.deptcode = Materialscrabdetails.deptcode");
			sb.append(" Where to_char(MaterialScrabDetails.EntryDate,'yyyymmdd') >= "+SFromDate+"  ");
			sb.append(" and to_char(MaterialScrabDetails.EntryDate,'yyyymmdd') <= "+SToDate+" ");
			sb.append(" and MaterialScrabDetails.FromStore = 1 ");
			if (SType.equals("SCRAP")){
			sb.append(" and MaterialScrabDetails.SecondSalesStatus!=1  ");
			}
			else{
			sb.append(" and MaterialScrabDetails.SecondSalesStatus =1  ");
			}
			sb.append(" order by ItemName Asc ");

            try
            {
                  if(theConnection ==null)
				 {
					ORAConnection jdbc   = ORAConnection.getORAConnection();
					theConnection        = jdbc.getConnection();
				}

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(sb.toString());
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theList;
     }
	 
	 

    
     
	private void createPDFFile()
	{
        try 
        {
			document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(SFileName));
            document.setPageSize(PageSize.LEGAL.rotate());
            document.open();
                             
            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            table.setHeaderRows(3);
			pdfHead();
			pdfBody();
           
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }

    }  

	private void pdfHead()
	{
		String SType  = common.parseNull((String)JCType.getSelectedItem());	
		AddCellIntoTable("AMARJOTHI SPINNING MILLS LIMITED", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
		if (SType.equals("SCRAP"))
		{
			AddCellIntoTable(" SCRAP ISSUE FROM STORES TO SCRAP YARD  : "  +TStDate.toString()+ "---" +TEnDate.toString(), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
		}
		else
		{
			AddCellIntoTable(" SECONDSALES FROM DEPARTMENT   : "  +TStDate.toString()+ "---" +TEnDate.toString(), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
		}
		//    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
		AddCellIntoTable("SL NO" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("ITEM CODE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("ITEM NAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("DEPTNAME " , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("SCRABQTY " , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("ENTRYDATE " , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 2, mediumbold);
       
	}

	private void pdfBody()
	{
	try
	{
		
		int colcount=theModel.getColumnCount();
        int rowcount=theModel.getRowCount();
		
		  
		  for(int K=0;K<theModel.getRowCount();K++)
            {


        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,0)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,15, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,1)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,15, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,2)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,15, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(String.valueOf(theModel.getValueAt(K,3)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,15, 4, 1, 8, 2, smallnormal);		
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,6)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,15, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,7)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,15, 4, 1, 8, 2, smallnormal);
    
	}
	
	/*	AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,45, 4, 0, 8, 0, mediumbold);
		AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,45, 4, 0, 8, 0, mediumbold);
		AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,45, 4, 0, 8, 0, mediumbold);
		AddCellIntoTable("STORE DEPARTMENT" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 20f,4, 0, 8, 2, bigbold);
		AddCellIntoTable("SECURITY                    CHECKED BY" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 20f,4, 0, 8, 2, bigbold);
		AddCellIntoTable("SCRAP YARD INCHARGE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,20f, 4,0, 8, 2, bigbold);*/
		
		
		AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, iTotalColumns,25, 0, 0, 0, 0, smallnormal);
		AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, iTotalColumns,25, 0, 0, 0, 0, smallnormal);
		
		AddCellIntoTable("SK" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25, 0, 0, 0, 0, mediumbold);
		AddCellIntoTable("SM(M)" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25, 0, 0, 0, 0, mediumbold);
		AddCellIntoTable("SM(A)" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25, 0, 0, 0, 0, mediumbold);
		AddCellIntoTable("IA" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25, 0, 0, 0, 0, mediumbold);
		AddCellIntoTable("ISO" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25, 0, 0, 0, 0, mediumbold);
		AddCellIntoTable("Sec" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25, 0, 0, 0, 0, mediumbold);
		
		AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, iTotalColumns,25f, 0, 0, 0, 0, mediumbold);
		AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, iTotalColumns,25f, 0, 0, 0, 0, mediumbold);
		AddCellIntoTable("Report Taken On :"+common.getServerDateTime() , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, iTotalColumns,25f, 0, 0, 0, 0, mediumbold);
	
		document.add(table);

		document.close();
	}
	catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }
	}  

	public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str ));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
   }  


}

