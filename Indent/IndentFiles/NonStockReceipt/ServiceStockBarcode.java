package Indent.IndentFiles.NonStockReceipt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import javax.comm.*;

import javax.print.*;
import javax.print.attribute.*;
import javax.print.attribute.standard.*;
import javax.print.event.*;


public class ServiceStockBarcode extends JInternalFrame 
{

     protected     JLayeredPane Layer;
	
	private   int            iUserCode;
	private   int            iMillCode;

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BPrint,BExit,BApply;   
     Common        common = new Common();
     Connection     theConnection;
	boolean errorflag = true;
	
     int PRINT=9;
	int iPreviousStock = 0;
     
	String SItemName ;

     ArrayList theList = new ArrayList();

     NewBarcodePrintModel   theModel;

     JTable             theTable;

      String       SPort = "COM1";
	 
	 String SType;
	
	JComboBox    CType;
	
     FileWriter FW;
     File file;



     public ServiceStockBarcode(JLayeredPane Layer,int iUserCode,int iMillCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
		this.iMillCode = iMillCode;
		
		createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }

	private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();
			
			CType       = new JComboBox();
				CType.addItem("NON STOCK");
				CType.addItem("SERVICE STOCK");
			
               theModel       = new NewBarcodePrintModel();
               theTable       = new JTable(theModel);
			
		
               BExit          = new JButton("Exit");
               BPrint          = new JButton("Print");
			BApply          = new JButton("Apply");
			
          }
          catch(Exception e)
          {
               System.out.println(e);
			e.printStackTrace();
    }
     }

     private void setLayouts()
     {
          setTitle("BARCODE PRINT ");
          setClosable(true);                    
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,1100,600);

          TopPanel.setLayout(new GridLayout(4,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder("Info."));
          MiddlePanel.setBorder(new TitledBorder("Details"));
          BottomPanel.setBorder(new TitledBorder("Controls"));
    
		TopPanel            . setBackground(new Color(213,234,255));
		MiddlePanel         .setBackground(new Color(213,234,255));
		BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

          TopPanel            . add(new JLabel("TYPE"));
	    TopPanel            . add(CType);
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(BApply);
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));

         
	//	theTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		theTable.getColumnModel().getColumn(0).setPreferredWidth(10);
		theTable.getColumnModel().getColumn(1).setPreferredWidth(10);
		theTable.getColumnModel().getColumn(2).setPreferredWidth(50);
		theTable.getColumnModel().getColumn(3).setPreferredWidth(30);
		theTable.getColumnModel().getColumn(4).setPreferredWidth(90);
		theTable.getColumnModel().getColumn(5).setPreferredWidth(65);
		theTable.getColumnModel().getColumn(6).setPreferredWidth(50);
		theTable.getColumnModel().getColumn(7).setPreferredWidth(27);
		


	    MiddlePanel.add(new JScrollPane(theTable));

          BottomPanel.add(BExit);
		BottomPanel.add(BPrint);


          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);


          BExit.setMnemonic('E');
     }

	 private void addListeners()
     {
          BApply.addActionListener(new ActList());
		BExit.addActionListener(new ActList());
		BPrint.addActionListener(new PrintList());
		theTable.addMouseListener(new MouseList());


     }                 

	 public void setTabReport()
     {
          try
          {
             	
			String SType =  common.parseNull((String)CType.getSelectedItem());
			
			theModel.setNumRows(0);	
                
			 ArrayList VDetails = getMaterialIndent(iMillCode,SType);

                    for(int i=0;i<VDetails.size();i++)
               {
                    HashMap  row =(HashMap)VDetails.get(i);                         

                     Vector VTheVect = new Vector();
				  
					VTheVect.addElement(String.valueOf(i+1));
					VTheVect.addElement(common.parseNull((String)row.get("ITEM_NAME")));
					VTheVect.addElement(common.parseNull((String)row.get("ITEM_CODE")));

					VTheVect.addElement(common.parseNull((String)row.get("CATL")));
					VTheVect.addElement(common.parseNull((String)row.get("DRAW")));
					VTheVect.addElement(common.parseNull((String)row.get("LOCNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("BARCODEPRINTEDORNOT")));
					VTheVect.addElement(common.parseNull((String)row.get("QUANTITY")));


					VTheVect.addElement(new Boolean(false));
			
                     theModel.appendRow(VTheVect);
               }
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               
			if(ae.getSource()==BApply)
               {
                    setTabReport();
               }
			
			if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
		}
     }
	
	private class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               System.out.println("comming before do print");

               updatePrintStatus();
			doPrint();
		//	setTabReport();

               System.out.println("comming after do print");

          }
     }


	private class MouseList extends MouseAdapter
	{
		public void mouseClicked(MouseEvent me)
		{
		}
	}

 private void showMessage(String smsg)
     {
          JOptionPane.showMessageDialog(null,getMessage(smsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage(String smsg)
     {
          String str = "<html><body>";
          str = str + smsg+ "<br>";
          str = str + "</body></html>";
          return str;
     }
	

  private void removeHelpFrame()
  {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
  }
  public ArrayList getMaterialIndent( int iMillCode,String SType)
  {
          theList = new ArrayList();
		StringBuffer sb = new StringBuffer();
				StringBuffer sb1 = new StringBuffer();
				
			


				sb.append(" select InvItems.Item_Name,InvItems.Item_Code, ");
				sb.append(" InvItems.Catl,InvItems.Draw,InvItems.LocName,  ");
				sb.append(" deCode(InvItems.BarcodePrintingStatus, 1, 'Yes', 'No') as BARCODEPRINTEDORNOT,ItemStock_Transfer.Stock as Quantity from ItemStock_Transfer ");
				sb.append(" Inner join InvItems on InvItems.Item_Code = ItemStock_Transfer.ItemCode ");
				sb.append(" where itemstock_transfer.tranferStatus = 1 ");
				sb.append(" Order by Item_Name ");
				
								
				sb1.append(" select InvItems.Item_Name,InvItems.Item_Code, "); 
				sb1.append(" InvItems.Catl,InvItems.Draw,ServiceStocktransfer.LocName, "); 
				sb1.append(" deCode(InvItems.BarcodePrintingStatus, 1, 'Yes', 'No') as BARCODEPRINTEDORNOT,ServiceStocktransfer.Stock as Quantity from ServiceStocktransfer  ");
				sb1.append(" Inner join InvItems on InvItems.Item_Code = ServiceStocktransfer.ItemCode  ");
				sb1.append(" where ServiceStocktransfer.transferStatus = 1  ");
				sb1.append(" Order by Item_Name  ");
				
          try
          {

               Class                   . forName("oracle.jdbc.OracleDriver");
		     Connection theConnection           = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "Inventory", "Stores");
			
			Statement theStatement     = theConnection.createStatement();
			ResultSet  result          = null;
		
			
			
			if(SType.equals("NON STOCK"))
			{
			  result          = theStatement.executeQuery(sb.toString());
			}
			else
			{
			  result          = theStatement.executeQuery(sb1.toString());
               }
			
			
			ResultSetMetaData rsmd    = result.getMetaData();

			

               while(result.next())
               {
                    HashMap row = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }                         
                    theList.add(row);
               }
               result.close();
              theStatement.close();
		    theConnection.close();
		}
          catch(Exception ex)
          {
			ex.printStackTrace();
		}

          return theList;

     }
	

	private void doPrint()
     {
          try
          {
			
			  
               SerialPort serialPort    = getSerialPort();
               serialPort               . setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
               OutputStream output      = serialPort.getOutputStream();
			  
			   
			
			
			   System.out.println("commin final "+serialPort);
			
			for(int i=0; i<theModel.getRowCount(); i++)
				{
					Boolean bValue = (Boolean)theModel.getValueAt(i, 8);
					if(bValue.booleanValue())
					{	
						int iCount=0;

						String SItemCod   = common.parseNull((String)theModel.getValueAt(i, 2));				
						String SItemCodd   = "CODE: "+common.parseNull((String)theModel.getValueAt(i, 2));				
						String SItemNamee = "ITEM : "+common.parseNull((String)theModel.getValueAt(i, 1));
						String SLoc = "LOC: "+common.parseNull((String)theModel.getValueAt(i, 5));
						String SCatl = "CATL: "+common.parseNull((String)theModel.getValueAt(i, 3));
						String SDraw = "DRAW: "+common.parseNull((String)theModel.getValueAt(i, 4));
						String SQty = "QNTY: "+common.parseNull((String)theModel.getValueAt(i, 7));

						String SLocDraw = SCatl  +"                 "+  SDraw;
 						
						String SLocQty = SLoc  +"                  "+  SQty;
              
	
              String str = "";
						String substr = "";
						String substr2 = "";
						
						if(SItemNamee.length()>28) {
							
							substr = SItemNamee.substring(0, 28);
							substr2 = SItemNamee.substring(28,SItemNamee.length());
						}else {
							substr = SItemNamee;	
						}	
						 			

	
				String Str2 = "191100301600020"+substr+"\r";     //191100501400070    191100300800070
				String Str4 = "191100301400055"+substr2+"\r";     //191100501400070    191100300800070
				String Str3 = "191100301200020"+SItemCodd+"\r";
                    String Str5 = "191100301000020"+SCatl+"\r";
				String Str10 = "191100300800020"+SDraw+"\r";
                    String Str7 = "191100300600020"+SLoc+"\r";
				String Str11 = "191100300400020"+SQty+"\r";
				String Str8 = "1e6205000000140"+SItemCod+"\r";
				
				output.write("n".getBytes());
                    output.write("f285".getBytes());
                    output.write("L".getBytes());
                    output.write("H10".getBytes());
                    output.write("D11".getBytes());
                    
				output.write(Str3.getBytes());
                    output.write(Str4.getBytes());
                    output.write(Str5.getBytes());
                    output.write(Str7.getBytes());
				output.write(Str2.getBytes()); 
			     output.write(Str8.getBytes()); 
				output.write(Str10.getBytes()); 
				output.write(Str11.getBytes());
				output.write("E\r".getBytes()); 

				
                  /*  String str1 = "191100501400080Amarjothi Spinning Mills Ltd\r";
                    String xtr1 = "1e6205000900090GOKUL\r";
                    String xtr2 = "191100500600080Bale Id:11111111\r";
                    String str2 = "191100400400080"+SItemNamee+"\r";
                    String str3 = "191100300200080Date: "+SItemNamee+"\r";
                    String str4 = "191100400400080"+common.parseNull(SItemCod)+"\r";*/
   
			   
							  
			System.out.println("commin all print final");

					}  // this is for loop
					}  // this is for loop
					
								                  serialPort.close();
 
		  }catch(Exception ex)	    {
			  ex.printStackTrace();
		  }
	 }	  

     private SerialPort getSerialPort() 
     {
          SerialPort serialPort         = null;
     
          try
          {
               Enumeration portList     = CommPortIdentifier.getPortIdentifiers();
     
               while(portList.hasMoreElements())
               {
                    CommPortIdentifier portId = (CommPortIdentifier)portList.nextElement();

                    if(portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort     = (SerialPort)portId.open("comapp", 2000);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               
          }
     
          return serialPort;
     }

private void updatePrintStatus(){
					
		try{
			
			if(theConnection ==null) 
			{

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
               }
			
			for(int i=0; i<theModel.getRowCount(); i++)
				{
					
				Boolean bValue = (Boolean)theModel.getValueAt(i, 8);
				if(bValue.booleanValue())			
				{
			String SCode = common.parseNull((String)theModel.getValueAt(i, 2));				
			
			
			String QS33 = "Update invitems set BarcodePrintingStatus = 1 Where Item_Code = '"+SCode+"'  ";
			
			System.out.println(QS33);
			
			

			theConnection.setAutoCommit(false);
			PreparedStatement ps1 = theConnection.prepareStatement(QS33);
			ps1.executeQuery();	
			ps1.close();
			theConnection			. commit();
			theConnection			. setAutoCommit(true);		
			}
				}
		}
			catch(Exception e)
			{
			try
			{
				theConnection		. rollback();
				theConnection		. setAutoCommit(true);
			}
			catch(Exception ex)
			{
			ex.printStackTrace();
			}
			}

}
	


}