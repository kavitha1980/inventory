package Indent.IndentFiles.NonStockReceipt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import javax.comm.*;

import javax.print.*;
import javax.print.attribute.*;
import javax.print.attribute.standard.*;
import javax.print.event.*;



public class ServiceStockTransfer extends JInternalFrame 
{

		protected     JLayeredPane Layer;
		private   int            iUserCode;
		private   int            iMillCode;

		JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
		JTabbedPane   thePane;
		JButton       BExit,BSave;   
		Common        common = new Common();
		Connection     theConnection;
		boolean errorflag = true;

		int PRINT=15;
		int iPreviousStock = 0;

		String SItemName ,SDept,SDeptCode;
		ArrayList theList = new ArrayList();
		ServiceStockModel   theModel;
		JTable             theTable;
		String       SPort = "COM1";
		JComboBox  CDept,CServiceDept;
		JButton BApply;
		Vector  VGroupName,VGroupCode;
		FileWriter FW;
		File file;
		
		Vector  VDeptName,VDeptCode;



     public ServiceStockTransfer(JLayeredPane Layer,int iUserCode,int iMillCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
		this.iMillCode = iMillCode;
		
		setData2();
		createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }

	private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();
			
               theModel       = new ServiceStockModel();
               theTable       = new JTable(theModel);
			
			CDept   = new JComboBox(new Vector(VGroupName));
			
			CServiceDept   = new JComboBox(VDeptName);
			
			
		
               BExit          = new JButton("Exit");
               BSave          = new JButton("Save");
			BApply          = new JButton("Apply");
			
          }
          catch(Exception e)
          {
               System.out.println(e);
			e.printStackTrace();
    }
     }


	 private void setData2()
	{
		VGroupName			= null;
		VGroupCode			= null;
		
		
		
		VGroupName			= new Vector();
		VGroupCode			= new Vector();	
			
		VGroupName			. clear();
		VGroupCode			. clear();
		
		VGroupName			. add("All");
		VGroupCode			. add("999999");
		
		VDeptName			= null;
		VDeptCode			= null;
		
		VDeptName			= new Vector();
		VDeptCode			= new Vector();	
			
		VDeptName			. clear();
		VDeptCode			. clear();
		
		VDeptName			. add("All");
		VDeptCode			. add("999999");
		
		
		
		try
		{
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
				Statement theStatement = theConnection.createStatement();
				String QSS = " Select GroupCode,GroupName From StockGroup Order By 2 ";
				
			
			
				ResultSet re = theStatement.executeQuery(QSS);

			while (re.next())
			{
				VGroupName		. add(common.parseNull(re.getString(2)));
				VGroupCode		. add(common.parseNull(re.getString(1)));
				
			}
				re.close();
				theStatement 		.close();
				
				
				Statement theStatement1 = theConnection.createStatement();
				String QSS1 = " Select DEpt_Code,Dept_Name From Dept Order By 2 ";
				
			
			
				ResultSet re1 = theStatement1.executeQuery(QSS1);

			while (re1.next())
			{
				VDeptName		. add(common.parseNull(re1.getString(2)));
				VDeptCode		. add(common.parseNull(re1.getString(1)));
				
			}
				re1.close();
				theStatement1 		.close();
				
			
		}
			catch(Exception ex)
			{
				System.out.println("From StockGroup "+ex);
			}
		
		
	}
	

     private void setLayouts()
     {
          setTitle(" Service Stock Transfer ");
          setClosable(true);                    
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,1100,600);

          TopPanel.setLayout(new GridLayout(2,4));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder("Info."));
          MiddlePanel.setBorder(new TitledBorder("Details"));
          BottomPanel.setBorder(new TitledBorder("Controls"));
    
		TopPanel            . setBackground(new Color(213,234,255));
		MiddlePanel         .setBackground(new Color(213,234,255));
		BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

          TopPanel            . add(new JLabel("Department"));
	    TopPanel            . add(CDept);
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(BApply);
		
          TopPanel            . add(new JLabel("Internal Service Department"));
	    TopPanel            . add(CServiceDept);
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
		
	    
	    /*TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));*/
	    

         
	//	theTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		theTable.getColumnModel().getColumn(0).setPreferredWidth(10);
		theTable.getColumnModel().getColumn(1).setPreferredWidth(10);
		theTable.getColumnModel().getColumn(2).setPreferredWidth(50);
		theTable.getColumnModel().getColumn(3).setPreferredWidth(30);
		theTable.getColumnModel().getColumn(4).setPreferredWidth(90);
		theTable.getColumnModel().getColumn(5).setPreferredWidth(65);
		
		theTable.getColumnModel().getColumn(6).setPreferredWidth(27);
		theTable.getColumnModel().getColumn(7).setPreferredWidth(30);
		theTable.getColumnModel().getColumn(8).setPreferredWidth(40);
		theTable.getColumnModel().getColumn(9).setPreferredWidth(40);
		theTable.getColumnModel().getColumn(10).setPreferredWidth(40);
		theTable.getColumnModel().getColumn(11).setPreferredWidth(25);
		theTable.getColumnModel().getColumn(12).setPreferredWidth(25);
		theTable.getColumnModel().getColumn(13).setPreferredWidth(50);
		theTable.getColumnModel().getColumn(14).setPreferredWidth(50);
		theTable.getColumnModel().getColumn(15).setPreferredWidth(35);
		theTable.getColumnModel().getColumn(16).setPreferredWidth(10);



	    MiddlePanel.add(new JScrollPane(theTable));

          BottomPanel.add(BExit);
		BottomPanel.add(BSave);


          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);


          BExit.setMnemonic('E');
     }

	 private void addListeners()
     {
          BExit.addActionListener(new ActList());
		BSave.addActionListener(new ActList());
		theTable.addMouseListener(new MouseList());
		BApply.addActionListener(new ActList());



     }                 

	 public void setTabReport()
     {
          try
          {
             	theModel.setNumRows(0);	
                
			  String SDept  = common.parseNull((String)CDept.getSelectedItem());
				if (!SDept.equals("All"))
			  SDeptCode = common.parseNull((String)VGroupCode.get(VGroupName.indexOf(SDept)));


			String SServiceDept  = common.parseNull((String)CServiceDept.getSelectedItem());
			
			String SServiceDeptCode="";
				if (!SServiceDept.equals("All"))
					 SServiceDeptCode = common.parseNull((String)VDeptCode.get(VDeptName.indexOf(SServiceDept)));

		  
		  
			 
			 ArrayList VDetails = getMaterialIndent(iMillCode,SDeptCode,SDept,SServiceDept,SServiceDeptCode);

                    for(int i=0;i<VDetails.size();i++)
               {
                    HashMap  row =(HashMap)VDetails.get(i);                         

                     Vector VTheVect = new Vector();
				  
					VTheVect.addElement(String.valueOf(i+1));
					VTheVect.addElement(common.parseNull((String)row.get("ID")));				 
					VTheVect.addElement(common.parseNull((String)row.get("MILLNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("HODCODE")));
					VTheVect.addElement(common.parseNull((String)row.get("ITEM_NAME")));
					VTheVect.addElement(common.parseNull((String)row.get("ITEM_CODE")));

					VTheVect.addElement(common.parseNull((String)row.get("STOCK")));
					VTheVect.addElement(common.parseNull((String)row.get("S.VALUE")));
					VTheVect.addElement(common.parseNull((String)row.get("CATL")));
					VTheVect.addElement(common.parseNull((String)row.get("DRAW")));
					VTheVect.addElement(common.parseNull((String)row.get("LOCNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("S.STOCK")));
					VTheVect.addElement(common.parseNull((String)row.get("T.STOCK")));

					VTheVect.addElement(common.parseNull((String)row.get("USERNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("ENTRYDATE")));
					VTheVect.addElement(common.parseNull((String)row.get("GROUPNAME")));

					VTheVect.addElement(new Boolean(false));
			
                     theModel.appendRow(VTheVect);
               }
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               
			
			
			if(ae.getSource()==BSave)
			{
				

				if(JOptionPane.showConfirmDialog(null, "Are you sure you want to Save the Details?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
				{ 		
                         validations();
					showMessage(getACommit());
					
					
					updatePrintStatus();
					printLabelNew();
					
					
					theModel.setNumRows(0);
					setTabReport();
				}
				
				/*if(JOptionPane.showConfirmDialog(null, "Are you sure you want to Print the Details?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
				{
					showMessage(getACommit());
					updatePrintStatus();
					printLabelNew();
				
				}*/
			}
			
			if(ae.getSource()==BApply)
			{
				
					theModel.setNumRows(0);
					setTabReport();
				
			} 
					
			if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
		}
     }

	private class MouseList extends MouseAdapter
	{
		public void mouseClicked(MouseEvent me)
		{
		}
	}

 private void showMessage(String smsg)
     {
          JOptionPane.showMessageDialog(null,getMessage(smsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage(String smsg)
     {
          String str = "<html><body>";
          str = str + smsg+ "<br>";
          str = str + "</body></html>";
          return str;
     }
	
	private void validations()
	{
			try
			{
				
				for(int i=0; i<theModel.getRowCount(); i++)
				{
					Boolean bValue = (Boolean)theModel.getValueAt(i, 16);
					if(bValue.booleanValue())
					{	
						int iCount=0;
						//int iPreviousStock = 0; // Stock already available with user NON-STOCK  and division MILL 
						String SItemCode   	= common.parseNull((String)theModel.getValueAt(i, 5));				

						String sId          = (String)theModel.getValueAt(i, 1);
						String SItemCodee  	= common.parseNull((String)theModel.getValueAt(i, 5));				
						int iHODCode  		= 10371;
						int iTransferStock  = common.toInt((String)theModel.getValueAt(i,6));
						String SItem   		= common.parseNull((String)theModel.getValueAt(i, 5));	
						int iStock  		= common.toInt((String)theModel.getValueAt(i,6));
						int iIdd           	= common.toInt((String)theModel.getValueAt(i, 1)); 
						
						String SLocationName= common.parseNull((String)theModel.getValueAt(i, 10));
						
						String SGroup		= common.parseNull((String)theModel.getValueAt(i, 15));
						
						int iInternalService =0;
						
						if(SGroup.equals("INTERNAL SERVICE")) {
							iInternalService=1;
						}
		
						iCount 				= alredyInserted(SItemCodee);
						
						System.out.println("iCount-->"+iCount);
						
						if(iCount>0){
							
							//insertItemStockHistory(SItemCodee,iHODCode) wrong table name..table does not exist..;
							double dTotalStock = iPreviousStock + iTransferStock;

							updateItemStock(SItemCodee,iHODCode,dTotalStock,sId);
							//updateStatus(SItemCodee,sId,iHODCode);
							
							updateStatusNew(SItemCodee,sId,iHODCode,iInternalService,i);  //new method to update internal service return item...
							
							
							
							updateLocation(SItemCodee,SLocationName);
						}
						else
						{
							insertItemstock(SItemCodee,iHODCode,iTransferStock,sId,iInternalService);
							//updateStatus(SItemCodee,sId,iHODCode);
							 updateStatusNew(SItemCodee,sId,iHODCode,iInternalService,i); // new method to update internal service return item...
							updateLocation(SItemCodee,SLocationName);
						}
					}
				}
			}
			catch(Exception e){
				e.printStackTrace(); 
			}
	}
	private int alredyInserted(String SItemCode)
	{
		int iCount 		= 0;
		iPreviousStock 	= 0;
		

		StringBuffer sb = new StringBuffer();
		
		sb.append(" Select Count(1),sum(stock) from itemstock ");
		sb.append(" where ItemCode ='"+SItemCode+"' and HODCode = 10371 and MillCode = "+iMillCode );	
		
		try{

			if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
               }

			PreparedStatement ps     = theConnection.prepareStatement(sb.toString());
			ResultSet	result1	 	= ps.executeQuery();
			while(result1.next())
			{ 
				iCount = result1.getInt(1);
				iPreviousStock = result1.getInt(2);		
			}
			result1 . close();
			ps    . close();
		}
		catch(Exception ex){
			errorflag = false;
			ex.printStackTrace();
		}
		return iCount;
	}
	private void insertItemStockHistory (String SItemCode,int iHODCode)
	{
		try{
			if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
               }
			theConnection.setAutoCommit(false);
			String QS15 = "Insert into itemstock_history (select MillCode, HODCode, ItemCode, Stock, StockValue,IndentQty,TransferQty,ReservedQty,ItemStockTransferId,Id,Sysdate, itemstockhistory_seq.nextVal from itemstock Where ItemCode = '"+SItemCode+"' and HODCode = "+iHODCode+" and MillCode = "+iMillCode+" ) ";
			PreparedStatement ps16 = theConnection.prepareStatement(QS15);
			ps16.executeQuery();	
			ps16.close();
		}
		catch(Exception ex){
			errorflag = false;
			ex.printStackTrace();
		}
	}
	private void insertItemstock(String SItemCode,int iHODCode,int iTransferStock,String sId,int iInternalService)
	{
		try{
			String QS4 = " Insert into itemstock(MillCode,HODCode,ItemCode,Stock,StockValue,IndentQty,TransferQty,ReservedQty,ItemStockTransferId,Id,EntryDateTime,internalservice,transferscreen ) " +
					   "	Values( "+iMillCode+","+iHODCode+",'"+SItemCode+"',"+iTransferStock+", "+
					   " 0,0,0,0,"+sId+",itemstock_seq.nextVal,SysDate,"+iInternalService+",1  )";

				if(theConnection ==null) {

					ORAConnection jdbc   = ORAConnection.getORAConnection();
					theConnection        = jdbc.getConnection();
				}
				theConnection.setAutoCommit(false);
				
				PreparedStatement ps6 = theConnection.prepareStatement(QS4);
				ps6.executeQuery();
				ps6.close();
		}
		catch(Exception ex){
			errorflag = false;
			ex.printStackTrace();
		}
	}
	
	
	
	private void updateLocation(String SItemCode,String SLocation){

		try{

			String QS3 = "Update InvItems set ItemLocName = '"+SLocation+"' Where Item_Code = '"+SItemCode+"' ";

			if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
            }

			theConnection			.	setAutoCommit(false);
			PreparedStatement ps1 	= 	theConnection.prepareStatement(QS3);
			ps1						.	executeQuery();	
			ps1						.	close();
		}
		catch(Exception ex){
			errorflag = false;
			ex.printStackTrace();
		}
	}
	
	
	private void updateItemStock(String SItemCode,int iHODCode,double dTotalStock,String sId){

		try{

//			String QS3 = "Update itemstock set Stock = "+dTotalStock+", ITEMSTOCKTRANSFERID="+sId+",ENTRYDATETIME = SysDate,internalservice="+iInternalservice+" Where ItemCode = '"+SItemCode+"' and HODCode = "+iHODCode+" and MillCode = "+iMillCode+" ";
			
						String QS3 = "Update itemstock set Stock = "+dTotalStock+", ITEMSTOCKTRANSFERID="+sId+",ENTRYDATETIME = SysDate,transferscreen=1 Where ItemCode = '"+SItemCode+"' and HODCode = "+iHODCode+" and MillCode = "+iMillCode+" ";

			if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
               }

			theConnection			.	setAutoCommit(false);
			PreparedStatement ps1 	= 	theConnection.prepareStatement(QS3);
			ps1						.	executeQuery();	
			ps1						.	close();
		}
		catch(Exception ex){
			errorflag = false;
			ex.printStackTrace();
		}
	}
	
	private void updateStatusNew(String SItemCode,String SId,int iHODCode,int iInternalservice,int iRow)
	{
		try
		{
				if(theConnection ==null) {

					  ORAConnection jdbc   = ORAConnection.getORAConnection();
					  theConnection        = jdbc.getConnection();
				}
			
				theConnection.setAutoCommit(false);
				
				Boolean bValue = (Boolean)theModel.getValueAt(iRow, 16);
					
				if(iInternalservice==0) {

					String SLocationName  = common.parseNull((String)theModel.getValueAt(iRow, 10));
				
					String QS = " Update Servicestocktransfer	set TransferStatus = 1 , TransferStatusDate = SysDate , BarcodePrintingStatus = 1,locname = '"+SLocationName+"' Where ItemCode = '"+SItemCode+"' and Id = "+SId+" and MillCode = "+iMillCode+" and HODCode = "+iHODCode+" ";

					PreparedStatement ps = theConnection.prepareStatement(QS);
					ps.executeUpdate();	
					ps.close();
				}
				else {

					String SLocationName  	= common.parseNull((String)theModel.getValueAt(iRow, 10));
					int iStock  			= common.toInt((String)theModel.getValueAt(iRow,6));
					
				
					String QS = " Update IndentMaterial_InternalService	set   StockTransferQty= "+iStock+"   Where ItemCode = '"+SItemCode+"' and Id = "+SId+"  ";

					PreparedStatement ps = theConnection.prepareStatement(QS);
					ps.executeUpdate();	
					ps.close();
				
					
				}
						
		}
		catch(Exception ex)
		{
			errorflag = false;
			ex.printStackTrace();		
		}
	}	
	
	private void updateStatus(String SItemCode,String SId,int iHODCode )
	{
		try
		{
		     if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
               }
			
				theConnection.setAutoCommit(false);
				
				for(int i=0; i<theModel.getRowCount(); i++)
				{
					Boolean bValue = (Boolean)theModel.getValueAt(i, 16);

					
					
					if(bValue.booleanValue())
					{

							String SLocationName  = common.parseNull((String)theModel.getValueAt(i, 10));
						
							String QS = " Update Servicestocktransfer	set TransferStatus = 1 , TransferStatusDate = SysDate , BarcodePrintingStatus = 1,locname = '"+SLocationName+"' Where ItemCode = '"+SItemCode+"' and Id = "+SId+" and MillCode = "+iMillCode+" and HODCode = "+iHODCode+" ";

							PreparedStatement ps = theConnection.prepareStatement(QS);
							ps.executeUpdate();	
							ps.close();
						
					}	
				}

				
		}
		catch(Exception ex)
		{
			errorflag = false;
			ex.printStackTrace();		
		}
	}

  private void removeHelpFrame()
  {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
  }
  public ArrayList getMaterialIndent( int iMillCode,String SDeptCode,String SDept,String SServiceDept,String SServiceDeptCode)
  {
			theList 		= new ArrayList();
			StringBuffer sb = new StringBuffer();

/*				sb.append(" select ItemStock_Transfer.Id,Mill.MillName,ItemStock_Transfer.HodCode,InvItems.Item_Name,InvItems.Item_Code,ItemStock_Transfer.Stock,ItemStock_Transfer.StockValue, ");
				sb.append(" InvItems.Catl,InvItems.Draw,InvItems.LocName, ");
				sb.append(" rawuser.UserName,to_char(ItemStock_Transfer.EntryDateTime,'dd.mm.yyyy') as EntryDate from ItemStock_Transfer ");
				sb.append(" Inner join Mill on Mill.MillCode = ItemStock_Transfer.MillCode ");
				sb.append(" Inner join InvItems on InvItems.Item_Code = ItemStock_Transfer.ItemCode");
				sb.append(" Inner join rawuser on rawuser.UserCode = ItemStock_Transfer.EntryUserCode");
				sb.append(" Where ItemStock_Transfer.TranferStatus = 0 and Mill.MillCode = "+iMillCode+"  ");
				sb.append(" group by ItemStock_Transfer.Id,Mill.MillName,ItemStock_Transfer.HodCode,InvItems.Item_Name,InvItems.Item_Code, ");
				sb.append(" ItemStock_Transfer.Stock,ItemStock_Transfer.StockValue,");
				sb.append(" InvItems.Catl,InvItems.Draw,InvItems.LocName,rawuser.UserName,to_char(ItemStock_Transfer.EntryDateTime,'dd.mm.yyyy') ");
				sb.append(" Order by Item_Name "); */
				
				
				sb.append(" select id,millname,hodcode,item_name,item_code,stock,stockvalue,catl,draw,locname,username,entrydate,groupname from ( ");
								
				sb.append(" select servicestocktransfer.id,mill.millname,servicestocktransfer.hodcode,invitems.item_name,invitems.item_code, ");
				//sb.append(" servicestocktransfer.stock,servicestocktransfer.stockvalue,invitems.catl,invitems.draw,invitems.locname, ");
				sb.append(" servicestocktransfer.stock,servicestocktransfer.stockvalue,invitems.catl,invitems.draw,invitems.ItemLocName as locname, ");
				sb.append(" rawuser.username,to_char(servicestocktransfer.entrydatetime,'dd.mm.yyyy') as entrydate,Stockgroup.groupname from servicestocktransfer ");
				sb.append(" Inner join Mill on Mill.MillCode = servicestocktransfer.MillCode  ");
				sb.append(" Inner join InvItems on InvItems.Item_Code = servicestocktransfer.ItemCode ");
				sb.append(" left join stockgroup on stockgroup.groupCode = servicestocktransfer.deptcode ");
				sb.append(" Inner join rawuser on rawuser.UserCode = servicestocktransfer.EntryUserCode ");
				sb.append(" where servicestocktransfer.transferstatus = 0 and mill.millcode = 0 ");
				
				if (!SDept.equals("All")){
					sb.append(" and servicestocktransfer.DeptCode= '"+SDeptCode+"' ");
				}
				
				if (!SServiceDept.equals("All")){
					sb.append(" and servicestocktransfer.DeptCode= '"+SServiceDeptCode+"' ");
				}
				
				
				/*sb.append(" union all ");
				
				// Internal Stock Return...
				 
				 sb.append(" select IndentMaterial_InternalService.id,'' as millname,'0' as hodcode,invitems.item_name,invitems.item_code, ");
				 sb.append(" to_char(IndentMaterial_InternalService.Req_qty),'0' as stockvalue,invitems.catl,invitems.draw,invitems.locname,  ");
				 sb.append(" rawuser.username,to_char(IndentMaterial_InternalService.entrydatetime,'dd.mm.yyyy') as entrydate, ");
				 sb.append(" 'INTERNAL SERVICE' as groupname from IndentMaterial_InternalService  ");
				 sb.append(" Inner join InvItems on InvItems.Item_Code = IndentMaterial_InternalService.ItemCode  ");
				 sb.append(" Inner join rawuser on rawuser.UserCode = IndentMaterial_InternalService.AuthUserCode ");
				 sb.append(" where nvl(IndentMaterial_InternalService.RETURN_QTY,0)<> nvl(IndentMaterial_InternalService.StockTransferQty,0) and  nvl(IndentMaterial_InternalService.RETURN_QTY,0)>0 ");*/
				 
				sb.append(" ) Order by 4 ");
				
				
				System.out.println("qry"+sb.toString());
				
				
				

          try
          {

               Class                   . forName("oracle.jdbc.OracleDriver");
		     Connection theConnection           = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "Inventory", "Stores");
			
			Statement theStatement     = theConnection.createStatement();
               ResultSet  result          = theStatement.executeQuery(sb.toString());
               ResultSetMetaData rsmd    = result.getMetaData();

               while(result.next())
               {
                    HashMap row = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }                         
                    theList.add(row);
               }
               result.close();
              theStatement.close();
		    theConnection.close();
		}
          catch(Exception ex)
          {
			ex.printStackTrace();
		}

          return theList;

     }
	public String getACommit()
     {
          String sReturn = "";
		
		System.out.println("In getACommit");
		System.out.println("errorflag-->"+errorflag);
		
          try
          {
               if(errorflag)
               {
                    theConnection    . commit();
                    sReturn = " Data Saved... ";
               }
               else
               {
                    theConnection    . rollback();
                    sReturn = "Error In Inserting ..";
               }
               theConnection    . setAutoCommit(true);
          }catch(Exception ex)
          {
               ex.printStackTrace();
               sReturn = "Error In Inserting ..";
          }
          return sReturn;
     }


	private void printLabelNew()
     {
          try
          {
			
			  
               SerialPort serialPort    = getSerialPort();
               serialPort               . setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
               OutputStream output      = serialPort.getOutputStream();
			  
			   
			
			
			   System.out.println("commin final "+serialPort);
			
			for(int i=0; i<theModel.getRowCount(); i++)
				{
					Boolean bValue = (Boolean)theModel.getValueAt(i, 16);
					if(bValue.booleanValue())
					{	
						int iCount=0;

						String SItemCod   = common.parseNull((String)theModel.getValueAt(i, 5));
						String SItemCodd   = "CODE: "+common.parseNull((String)theModel.getValueAt(i, 5));				
						
						String SItemNamee = "ITEM: "+common.parseNull((String)theModel.getValueAt(i, 4));
						String SLoc = "LOC: "+common.parseNull((String)theModel.getValueAt(i, 10));
						String SCatl = "CATL: "+common.parseNull((String)theModel.getValueAt(i, 8));
						String SDraw = "DRAW: "+common.parseNull((String)theModel.getValueAt(i, 9));
						String SQty = "QNTY: "+common.parseNull((String)theModel.getValueAt(i, 6));

						String SLocDraw = SCatl  +"  "+  SDraw;
						String SLocQty = SLoc  +"                 "+  SQty;

	
			

			String str = "";
						String substr = "";
						String substr2 = "";
						
						 if(SItemNamee.length()>28) {
							
							substr = SItemNamee.substring(0, 28);
							substr2 = SItemNamee.substring(28,SItemNamee.length());
						}else {
							substr = SItemNamee;	
						}	

	
				String Str2 = "191100301600065"+substr+"\r";     //191100501400070    191100300800070
				String Str4 = "191100301400100"+substr2+"\r";     //191100501400070    191100300800070
				String Str3 = "191100301200065"+SItemCodd+"\r";
                    String Str5 = "191100301000065"+SCatl+"\r";
				String Str10 = "191100300800065"+SDraw+"\r";
                    String Str7 = "191100300600065"+SLoc+"\r";
				String Str11 = "191100300400065"+SQty+"\r";
				String Str8 = "1e6205000000185"+SItemCod+"\r";
				
				
				/*
				String Str2 = "191100301600020"+substr+"\r";     //191100501400070    191100300800070
				String Str4 = "191100301400055"+substr2+"\r";     //191100501400070    191100300800070
				String Str3 = "191100301200020"+SItemCodd+"\r";
                    String Str5 = "191100301000020"+SCatl+"\r";
				String Str10 = "191100300800020"+SDraw+"\r";
                    String Str7 = "191100300600020"+SLoc+"\r";
				String Str11 = "191100300400020"+SQty+"\r";
				String Str8 = "1e6205000000140"+SItemCod+"\r";
				*/

				output.write("n".getBytes());
                    output.write("f285".getBytes());
                    output.write("L".getBytes());
                    output.write("H10".getBytes());
                    output.write("D11".getBytes());

             

				
                   output.write(Str3.getBytes());
                    output.write(Str4.getBytes());
                    output.write(Str5.getBytes());
                    output.write(Str7.getBytes());
				output.write(Str2.getBytes()); 
			     output.write(Str8.getBytes()); 
				output.write(Str10.getBytes()); 
				output.write(Str11.getBytes());
				output.write("E\r".getBytes()); 
                  
							  
			System.out.println("commin all print final");

					}  // this is for loop
					}  // this is for loop
					
			serialPort.close();
 
		  }catch(Exception ex)	    {
			  ex.printStackTrace();
		  }
	 }	  

     private SerialPort getSerialPort() 
     {
          SerialPort serialPort         = null;
     
          try
          {
               Enumeration portList     = CommPortIdentifier.getPortIdentifiers();
     
               while(portList.hasMoreElements())
               {
                    CommPortIdentifier portId = (CommPortIdentifier)portList.nextElement();

                    if(portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort     = (SerialPort)portId.open("comapp", 2000);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               
          }
     
          return serialPort;
     }

private void updatePrintStatus(){
					
		try{
			
			if(theConnection ==null) 
			{

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
               }
			
			for(int i=0; i<theModel.getRowCount(); i++)
				{
					
				Boolean bValue = (Boolean)theModel.getValueAt(i, 16);
				if(bValue.booleanValue())			
				{
			String SCode = common.parseNull((String)theModel.getValueAt(i, 5));				
			
			
			String QS33 = "Update invitems set BarcodePrintingStatus = 1 Where Item_Code = '"+SCode+"'  ";
			
			System.out.println(QS33);
			
			

			theConnection.setAutoCommit(false);
			PreparedStatement ps1 = theConnection.prepareStatement(QS33);
			ps1.executeQuery();	
			ps1.close();
			theConnection			. commit();
			theConnection			. setAutoCommit(true);		
			}
				}
		}
			catch(Exception e)
			{
			try
			{
				theConnection		. rollback();
				theConnection		. setAutoCommit(true);
			}
			catch(Exception ex)
			{
			ex.printStackTrace();
			}
			}

} 

}


