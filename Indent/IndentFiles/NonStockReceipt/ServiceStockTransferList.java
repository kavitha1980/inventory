package Indent.IndentFiles.NonStockReceipt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

import javax.comm.*;

import javax.print.*;
import javax.print.attribute.*;
import javax.print.attribute.standard.*;
import javax.print.event.*;



public class ServiceStockTransferList extends JInternalFrame 
{

     protected     JLayeredPane Layer;
	 

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BExit,BApply,BProcess,BUpdate,BPrint;
	DateField      TStDate;
     DateField      TEnDate; //TFromDate,TToDate;

	JComboBox     CUser;
     Common        common = new Common();
     Connection     theConnection;
	ArrayList   AUserName,AUserCode;
	
	int           iUserCode=0;
		int           iMillCode=0;

     int PRINT=13;
	
	boolean errorflag = true;
	
	int iTotalColumns = 12;
     int iWidth[] = {15,50,150,50,30,50,50,50,50,50,50,60}; //,50,50,50};
     
	String SItemName ;
	
	String SItemCode,STotalStock;

     ArrayList theList = new ArrayList();

     ServiceStockTransferListModel   theModel;

     JTable             theTable;

     
     FileWriter FW;
     File file;
	
	int iHodcode;

	String   SFileName,SItemCodee;
	PdfPTable table;

	Document document;
	
	private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 11, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 9, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 5, Font.BOLD);

	JComboBox  CDept;
	
	Vector  VGroupName,VGroupCode;
	
	      String       SPort = "COM1";


     public ServiceStockTransferList(JLayeredPane Layer,int iUserCode,int iMillCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
		          this.iMillCode = iMillCode;


		setData();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }


	private void setData()
	{
		AUserName			= null;
		AUserCode			= null;
		
		
		AUserName			= new ArrayList();
		AUserCode			= new ArrayList();
		
		AUserName			. clear();
		AUserCode			. clear();
		
		AUserName			. add("All");
		AUserCode			. add("9999999");
		
		
		VGroupName			= null;
		VGroupCode			= null;
		
		
		
		VGroupName			= new Vector();
		VGroupCode			= new Vector();	
			
		VGroupName			. clear();
		VGroupCode			. clear();
		
		VGroupName			. add("All");
		VGroupCode			. add("999999");
		

		try
		{
			if(theConnection ==null) {

				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
			Statement theStatement = theConnection.createStatement();
			String QS = " Select UserName,UserCode from rawuser order by 1 ";
			ResultSet rs = theStatement.executeQuery(QS);

			while (rs.next())
			{
				AUserName		. add(common.parseNull(rs.getString(1)));
				AUserCode		. add(common.parseNull(rs.getString(2)));
				
			}
			rs.close();
			theStatement 		.close();
			
			
			
				Statement theStatement1 = theConnection.createStatement();
			
				String QSS1 = " Select GroupCode,GroupName From StockGroup Order By 2 ";
				
			
			
				ResultSet re1 = theStatement1.executeQuery(QSS1);

			while (re1.next())
			{
				VGroupName		. add(common.parseNull(re1.getString(2)));
				VGroupCode		. add(common.parseNull(re1.getString(1)));
				
			}
				re1.close();
			
				theStatement1 		.close();
			
		}
		catch(Exception ex)
			{
			System.out.println("From StockReceiptListFrame "+ex);
			}
	}
	
	
	
	private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();
			
               theModel       = new ServiceStockTransferListModel();
               theTable       = new JTable(theModel);

			TStDate      = new DateField();
			TEnDate        = new DateField();
			TStDate.setTodayDate();
			TEnDate.setTodayDate();
			
			CUser   	= new JComboBox(new Vector(AUserName));	

               BExit          = new JButton("Exit");
			BApply          = new JButton("Apply");
			BProcess          = new JButton("PDF");
			BUpdate          = new JButton("UPDATE");
			BPrint			= new JButton("PRINT");

			CDept   = new JComboBox(new Vector(VGroupName));
			
          }
          catch(Exception e)
          {
               System.out.println(e);
			e.printStackTrace();
		}
     }

     private void setLayouts()
     {
          setTitle("Service Stock Transfer List");
          setClosable(true);                    
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,1100,600);

          TopPanel.setLayout(new GridLayout(7,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder("Info."));
          MiddlePanel.setBorder(new TitledBorder("Details"));
          BottomPanel.setBorder(new TitledBorder("Controls"));
    
		TopPanel            . setBackground(new Color(213,234,255));
		MiddlePanel         .setBackground(new Color(213,234,255));
		BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

          TopPanel            . add(new JLabel("FromDate"));
	     TopPanel            . add(TStDate);
		TopPanel            . add(new JLabel("ToDate"));
	     TopPanel            . add(TEnDate);
		
		TopPanel            . add(new JLabel("User"));
	     TopPanel            . add(CUser);
	     TopPanel            . add(new JLabel("department"));
	     TopPanel            . add(CDept);
		 
		 
   
		TopPanel            . add(new JLabel(""));
	     TopPanel            . add(BApply);
	    	TopPanel            . add(new JLabel(""));
	     TopPanel            . add(new JLabel(""));
	     TopPanel            . add(new JLabel(""));
	     TopPanel			.add(new JLabel(""));
	     
		
		MiddlePanel.add(new JScrollPane(theTable));

          BottomPanel.add(BExit);
          BottomPanel.add(BProcess);
		BottomPanel.add(BUpdate);
		BottomPanel.add(BPrint);
		
		


          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);


          BExit.setMnemonic('E');
     }

	 private void addListeners()
     {
          BExit.addActionListener(new ActList());
          BApply.addActionListener(new ActList());
		BProcess.addActionListener(new ActList());
		BUpdate.addActionListener(new ActList());
		BPrint	.addActionListener(new ActList());


     }                 

	 public void setTabReport()
     {
          try
          {
		
			String SStDate = common.pureDate((String)TStDate.toNormal()); 
			String SEnDate = common.pureDate((String)TEnDate.toNormal()); 
			
			
			
			String SUser = common.parseNull((String)CUser.getSelectedItem());
			
			System.out.println(SUser);
			int iUserCode = 9999999;
			if (!SUser.equals("All")){
				iUserCode= common.toInt((String)AUserCode.get(AUserName.indexOf(SUser)));
			}
			
				String SDept  = common.parseNull((String)CDept.getSelectedItem());
				
				String SDeptCode ="9999999";
				
				if (!SDept.equals("All"))
					 SDeptCode = common.parseNull((String)VGroupCode.get(VGroupName.indexOf(SDept)));			

		theModel.setNumRows(0);	
                
			 ArrayList VDetails = getMaterialIndent(SUser,iUserCode,SStDate,SEnDate,iMillCode,SDeptCode,SDept);

                    for(int i=0;i<VDetails.size();i++)
               {
                    HashMap  row =(HashMap)VDetails.get(i);  

					String SItemCode = common.parseNull((String)row.get("ITEM_CODE"));
					
					String STotalStock = getTotalStock(SItemCode);

					Vector VTheVect = new Vector();
					VTheVect.addElement(String.valueOf(i+1));
					VTheVect.addElement(common.parseNull((String)row.get("MILLNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("HODCODE")));
					VTheVect.addElement(common.parseNull((String)row.get("ITEM_NAME")));
					VTheVect.addElement(common.parseNull((String)row.get("ITEM_CODE")));
					

					VTheVect.addElement(common.parseNull((String)row.get("STOCK")));
					VTheVect.addElement(common.parseNull((String)row.get("STOCKVALUE")));
								 
					VTheVect.addElement(common.parseNull((String)row.get("CATL")));
					VTheVect.addElement(common.parseNull((String)row.get("DRAW")));
					VTheVect.addElement(common.parseNull((String)row.get("LOCNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("STORESTOCK")));


					VTheVect.addElement(String.valueOf(STotalStock));

					VTheVect.addElement(common.parseNull((String)row.get("USERNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("DEPTNAME")));
					VTheVect.addElement(common.parseNull((String)row.get("ENTRYDATE")));

					VTheVect.addElement(new Boolean(false));
				 
                     theModel.appendRow(VTheVect);
               }
        //  }
		}
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }
	
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }             

			if(ae.getSource()==BApply)
               {    
                    theModel.setNumRows(0);
                    setTabReport();
               }
			if(ae.getSource()==BPrint)
               {    
					doPrint();
               }

			   
			
			if(ae.getSource()==BUpdate)
               {    
				if(JOptionPane.showConfirmDialog(null, "Are you sure you want to Update the BINNAME?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
				{
				//	showMessage(getACommit());
					updateStatus(); 
					doPrint();
					theModel.setNumRows(0);
					setTabReport();		
				}
			}	
			
		if(ae.getSource()==BProcess) 
			{
			 try
			 {
				SFileName = "d:\\ServiceStockTransferReceiptList.pdf"; 
				createPDFFile();
				File theFile   = new File(SFileName);
				Desktop        . getDesktop() . open(theFile);
			 }
			  catch(Exception ex){
            ex.printStackTrace();
			}
			 
			}
		
		
		}
     }
	
	private void showMessage(String smsg)
     {
          JOptionPane.showMessageDialog(null,getMessage(smsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }
	
	 private String getMessage(String smsg)
     {
          String str = "<html><body>";
          str = str + smsg+ "<br>";
          str = str + "</body></html>";
          return str;
     }
	 
private void doPrint()
     {
          try
          {
			
			  
               SerialPort serialPort    = getSerialPort();
               serialPort               . setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
               OutputStream output      = serialPort.getOutputStream();
			  
			   
			
			
			   System.out.println("commin final "+serialPort);
			
			for(int i=0; i<theModel.getRowCount(); i++)
				{
					Boolean bValue = (Boolean)theModel.getValueAt(i, 15);
					if(bValue.booleanValue())
					{	
						int iCount=0;

						String SItemCod   	= common.parseNull((String)theModel.getValueAt(i, 4));				
						String SItemCodd   	= "CODE: "+common.parseNull((String)theModel.getValueAt(i, 4));				
						
						String SItemNamee = "ITEM : "+common.parseNull((String)theModel.getValueAt(i, 3));
						
						String SLoc 	= "LOC: "+common.parseNull((String)theModel.getValueAt(i, 9));
						String SCatl 	= "CATL: "+common.parseNull((String)theModel.getValueAt(i, 7));
						String SDraw 	= "DRAW: "+common.parseNull((String)theModel.getValueAt(i, 8));
						String SQty 	= "QNTY: "+common.parseNull((String)theModel.getValueAt(i, 11));

						String SLocDraw = SCatl  +"                 "+  SDraw;
						String SLocQty = SLoc  +"                  "+  SQty;
              
	
						String str = "";
						String substr = "";
						String substr2 = "";
						
						if(SItemNamee.length()>28) {
							
							substr = SItemNamee.substring(0, 28);
							substr2 = SItemNamee.substring(28,SItemNamee.length());
						}else {
							substr = SItemNamee;	
						}	
						 			

	
/*				String Str2 = "191100301600020"+substr+"\r";     //191100501400070    191100300800070
				String Str4 = "191100301400055"+substr2+"\r";     //191100501400070    191100300800070
				String Str3 = "191100301200020"+SItemCodd+"\r";
                    String Str5 = "191100301000020"+SCatl+"\r";
				String Str10 = "191100300800020"+SDraw+"\r";
                    String Str7 = "191100300600020"+SLoc+"\r";
				String Str11 = "191100300400020"+SQty+"\r";
				String Str8 = "1e6205000000140"+SItemCod+"\r";*/
				
				
				String Str2 = "191100301600065"+substr+"\r";     //191100501400070    191100300800070
				String Str4 = "191100301400100"+substr2+"\r";     //191100501400070    191100300800070
				String Str3 = "191100301200065"+SItemCodd+"\r";
                    String Str5 = "191100301000065"+SCatl+"\r";
				String Str10 = "191100300800065"+SDraw+"\r";
                    String Str7 = "191100300600065"+SLoc+"\r";
				String Str11 = "191100300400065"+SQty+"\r";
				String Str8 = "1e6205000000185"+SItemCod+"\r";
				
				
				output.write("n".getBytes());
                    output.write("f285".getBytes());
                    output.write("L".getBytes());
                    output.write("H10".getBytes());
                    output.write("D11".getBytes());
                    
				output.write(Str3.getBytes());
                    output.write(Str4.getBytes());
                    output.write(Str5.getBytes());
                    output.write(Str7.getBytes());
				output.write(Str2.getBytes()); 
			     output.write(Str8.getBytes()); 
				output.write(Str10.getBytes()); 
				output.write(Str11.getBytes());
				output.write("E\r".getBytes()); 

				
                  /*  String str1 = "191100501400080Amarjothi Spinning Mills Ltd\r";
                    String xtr1 = "1e6205000900090GOKUL\r";
                    String xtr2 = "191100500600080Bale Id:11111111\r";
                    String str2 = "191100400400080"+SItemNamee+"\r";
                    String str3 = "191100300200080Date: "+SItemNamee+"\r";
                    String str4 = "191100400400080"+common.parseNull(SItemCod)+"\r";*/
   
			   
							  
			System.out.println("commin all print final");

					}  // this is for loop
					}  // this is for loop
					
								                  serialPort.close();
 
		  }catch(Exception ex)	    {
			  ex.printStackTrace();
		  }
	 }	  

     private SerialPort getSerialPort() 
     {
          SerialPort serialPort         = null;
     
          try
          {
               Enumeration portList     = CommPortIdentifier.getPortIdentifiers();
     
               while(portList.hasMoreElements())
               {
                    CommPortIdentifier portId = (CommPortIdentifier)portList.nextElement();

                    if(portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort     = (SerialPort)portId.open("comapp", 2000);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               
          }
     
          return serialPort;
     }
	 
	
	private void updateStatus()
	{
		try
		{
				if(theConnection ==null) {
						ORAConnection jdbc   = ORAConnection.getORAConnection();
						theConnection        = jdbc.getConnection();
				}
			
				theConnection.setAutoCommit(false);
				
				for(int i=0; i<theModel.getRowCount(); i++)
				{
					Boolean bValue = (Boolean)theModel.getValueAt(i, 15);
					
					if(bValue.booleanValue())
					{
				
						String SLocationName  	= common.parseNull((String)theModel.getValueAt(i,9));
						String SItemCodee 		= common.parseNull((String)theModel.getValueAt(i,4));
						String iHodcode 		= common.parseNull((String)theModel.getValueAt(i,2));
						
						String QS = " Update Servicestocktransfer set locname = '"+SLocationName+"' Where ItemCode = '"+SItemCodee+"'  and MillCode = "+iMillCode+" and HODCode = "+iHodcode+" ";

						System.out.println("ser up:"+QS);

						PreparedStatement ps = theConnection.prepareStatement(QS);
						ps.executeUpdate();	
						ps.close();
						
						String QS3 = "Update InvItems set ItemLocName = '"+SLocationName+"' Where Item_Code = '"+SItemCodee+"' ";
						
						System.out.println("item up:"+QS3);

						PreparedStatement ps1 	= 	theConnection.prepareStatement(QS3);
						ps1						.	executeQuery();	
						ps1						.	close();

					}
				}
				
				theConnection			. commit();
				theConnection			. setAutoCommit(true);
				
				showMessage("Binname Updated Successfully..");
				
		}
		catch(Exception e)
		{
			try
			{
				theConnection		. rollback();
				theConnection		. setAutoCommit(true);
			}
			catch(Exception ex){}
			errorflag = false;
			e.printStackTrace();		
		}
	}


	public String getACommit()
     {
          String sReturn = "";
		
		System.out.println("In getACommit");
		System.out.println("errorflag-->"+errorflag);
		
          try
          {
               if(errorflag)
               {
                    theConnection    . commit();
                    sReturn = " Data Saved... ";
               }
               else
               {
                    theConnection    . rollback();
                    sReturn = "Error In Inserting ..";
               }
               theConnection    . setAutoCommit(true);
          }catch(Exception ex)
          {
               ex.printStackTrace();
               sReturn = "Error In Inserting ..";
          }
          return sReturn;
     }


	private String getTotalStock(String SItemCode)
	{
		  String sStock = "0";
            StringBuffer sb = new StringBuffer();

            sb.append(" select sum(Stock) from ItemStock where itemcode ='"+SItemCode+"' and millcode = 0 and hodcode = 10371 ");
		  
		 // System.out.println(sb.toString());
		  
		  try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(sb.toString());
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
				sStock = result.getString(1);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
		  return sStock;
     }


 private void showMessage()
     {
          JOptionPane.showMessageDialog(null,getMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage()
     {
          String str = "<html><body>";
          str = str + "A New Country Name has been<br>";
          str = str + "successfully registered<br>";          
          str = str + "</body></html>";
          return str;
     }
	
	
  private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public ArrayList getMaterialIndent(String SUser,int iUserCode,String SStDate, String SEnDate,int iMillCode,String SDeptCode,String SDept)
     {

          theList = new ArrayList();
		
	String QS1 = " select Mill.MillName,ServiceStockTransfer.HodCode,InvItems.Item_Name,InvItems.Item_Code,Servicestocktransfer.Stock ,  "+
				" ServiceStockTransfer.StockValue, "+
				" InvItems.Catl,InvItems.Draw,Servicestocktransfer.LocName,0 as STORESTOCK, "+
				"  0 as TOTALSTOCK, " +
				" rawuser.UserName,to_char(ServiceStockTransfer.TransferStatusDate,'dd.mm.yyyy') "+
				"  as EntryDate,decode(stockgroup.groupname,null,dept.dept_name,stockgroup.groupname) as DeptName  from servicestocktransfer   "+
				" inner join InvItems on InvItems.Item_Code = servicestocktransfer.ItemCode  and TRANSFERSTATUS = 1  "+
				" inner join Mill on Mill.MillCode = servicestocktransfer.MillCode  "+
				//" inner join stockgroup on stockgroup.groupcode = invitems.stkgroupcode "+
				" left join stockgroup on stockgroup.groupcode = servicestocktransfer.DeptCode "+
				" left join Dept on to_char(dept.dept_code) = servicestocktransfer.DeptCode "+
				" inner join rawuser on rawuser.usercode = servicestocktransfer.EntryUsercode  " ;
			QS1 = QS1 +" where to_char(servicestocktransfer.transferstatusdate,'yyyymmdd') >= "+SStDate+"  " ;
			QS1 = QS1 +" and to_char(servicestocktransfer.transferstatusdate,'yyyymmdd') <="+SEnDate+" ";
			if (!SUser.equals("All"))
			QS1 = QS1 +" and servicestocktransfer.entryusercode = "+iUserCode+" ";

			if (!SDept.equals("All"))
			QS1 = QS1 +" and Stockgroup.groupcode = '"+SDeptCode+"' ";
		
		
		
			QS1 = QS1 +"order by InvItems.Item_Name asc " ;

		


		try
          {
			
		     
               Class                   . forName("oracle.jdbc.OracleDriver");
		     Connection theConnection           = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "Inventory", "Stores");
			
			Statement theStatement     = theConnection.createStatement();
               ResultSet  result          = theStatement.executeQuery(QS1);
               ResultSetMetaData rsmd    = result.getMetaData();

               while(result.next())
               {
                    HashMap row = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }                         
                    theList.add(row);
               }
               result.close();
              theStatement.close();
		    theConnection.close();
		}
          catch(Exception ex)
          {
               System.out.println(ex);
           ex.printStackTrace();
		}

          return theList;

     }
	
	
/*	private double getTotalStock(String sItem){
		double dTotal = 0;
		try{
				 for(int i=0;i<theList.size();i++)
				{
					HashMap  row =(HashMap)theList.get(i);  
					String SItemCode = common.parseNull((String)row.get("ITEM_CODE"));

				    System.out.println("sItem-->"+sItem);
				    System.out.println("SItemCode-->"+SItemCode);

					if(sItem.equals(SItemCode)){
						dTotal += common.toDouble((String)row.get("STOCK"));
					}
				}	
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return dTotal;
	}   */
	
	private void createPDFFile()
    {
        try 
        {
			document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(SFileName));
            document.setPageSize(PageSize.LEGAL.rotate());
            document.open();
                             
            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            table.setHeaderRows(4);
			pdfHead();
			pdfBody();
           
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }

    }  

	private void pdfHead()
	{
	
	   AddCellIntoTable("AMARJOTHI SPINNING MILLS LIMITED", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
        //AddCellIntoTable(" SERVICE STOCK RECEIVED REPORT : "  +TStDate.toString()+ "---" +TEnDate.toString(), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
		
		AddCellIntoTable(" OLD SPARE STOCK RECEIVED REPORT : "  +TStDate.toString()+ "--- Dept:"+(String)CDept.getSelectedItem()+" -- "+TEnDate.toString(), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
		AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);

		AddCellIntoTable("SL NO" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("ITEM CODE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("ITEM NAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("DEPT NAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("TRANSFER QTY" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("TRANSFER VALUE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("CATL" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("DRAW" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("LOCNAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("STORE STOCK" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("TOTALSTOCK" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		//AddCellIntoTable("DEPT NAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("USER NAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
     
        
		
		
	}

	private void pdfBody()
	{
	try
	{
	//	int iTotal = 0;
		int colcount=theModel.getColumnCount();
        int rowcount=theModel.getRowCount();
		double dstockTotal = 0;
		  
		  for(int K=0;K<theModel.getRowCount();K++)
            {
	

        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,0)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);

        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,4)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,3)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		 AddCellIntoTable(String.valueOf(theModel.getValueAt(K,13)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);      
		AddCellIntoTable(String.valueOf(theModel.getValueAt(K,5)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(String.valueOf(theModel.getValueAt(K,6)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(String.valueOf(theModel.getValueAt(K,7)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(String.valueOf(theModel.getValueAt(K,8)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(String.valueOf(theModel.getValueAt(K,9)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(String.valueOf(theModel.getValueAt(K,10)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(String.valueOf(theModel.getValueAt(K,11)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(String.valueOf(theModel.getValueAt(K,12)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		
		
		dstockTotal += common.toDouble((String)theModel.getValueAt(K,5));
	
	}
		
		
	AddCellIntoTable("TOTAL STOCK" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 4,45, 4, 1, 8, 2, mediumbold);
	AddCellIntoTable(common.getRound(dstockTotal,0) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,45, 4, 1, 8, 2, mediumbold);
	AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 7,45, 4, 1, 8, 2, mediumbold);		
		document.add(table);
		document.close();
	
	}
	catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }
	}  

	public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str ));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
   }  

}