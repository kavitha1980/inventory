package Indent.IndentFiles.NonStockReceipt;


import javax.swing.table.*;
import java.util.*;

public class StockListModel extends DefaultTableModel
{

     String ColumnName[]   = {"SL.NO","MILLNAME","HODCODE","ITEMNAME","ITEMCODE","STOCK","STOCKVALUE","CATL","DRAW","LOCNAME","STORESTOCK","TOTALSTOCK","DEPTNAME","USERNAME","ENTRYDATE"};
     String ColumnType[]   = {"S","S","S","S","S","S","S","S","S","S","S","S","S","S","S"};
     int  iColumnWidth[] = {10,30,40,30,30,30,30,30,30,30,30,30,30,30,30};


     public StockListModel()
     {
          setDataVector(getRowData(),ColumnName);
     }

     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";
		
     			
          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}

