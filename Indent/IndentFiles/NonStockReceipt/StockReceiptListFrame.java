package Indent.IndentFiles.NonStockReceipt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;


public class StockReceiptListFrame extends JInternalFrame 
{

     protected     JLayeredPane Layer;
	 

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BExit,BApply,BProcess;
	DateField      TStDate;
     DateField      TEnDate; //TFromDate,TToDate;

	JComboBox     CUser;
     Common        common = new Common();
     Connection     theConnection;
	ArrayList   AUserName,AUserCode;
	
	int           iUserCode=0;
		int           iMillCode=0;

     int PRINT=13;
	
	int iTotalColumns = 12;
     int iWidth[] = {20,50,150,50,50,50,50,50,50,50,50,50}; //,50,50,50};
     
	String SItemName ;
	
	String SItemCode,STotalStock;

     ArrayList theList = new ArrayList();

     StockListModel   theModel;

     JTable             theTable;

     
     FileWriter FW;
     File file;

	String   SFileName;
	PdfPTable table;

	Document document;
	
	private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 11, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 9, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 5, Font.BOLD);



     public StockReceiptListFrame(JLayeredPane Layer,int iUserCode,int iMillCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
		          this.iMillCode = iMillCode;


		setData();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }


	private void setData()
	{
		AUserName			= null;
		AUserCode			= null;
		
		
		AUserName			= new ArrayList();
		AUserCode			= new ArrayList();
		
		AUserName			. clear();
		AUserCode			. clear();
		
		AUserName			. add("All");
		AUserCode			. add("9999999");

		try
		{
			if(theConnection ==null) {

				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
			Statement theStatement = theConnection.createStatement();
			String QS = " Select UserName,UserCode from rawuser order by 1 ";
			ResultSet rs = theStatement.executeQuery(QS);

			while (rs.next())
			{
				AUserName		. add(common.parseNull(rs.getString(1)));
				AUserCode		. add(common.parseNull(rs.getString(2)));
				
			}
			rs.close();
			theStatement 		.close();
		}
		catch(Exception ex)
			{
			System.out.println("From StockReceiptListFrame "+ex);
			}
	}
	
	
	
	private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();
			
               theModel       = new StockListModel();
               theTable       = new JTable(theModel);

			TStDate      = new DateField();
			TEnDate        = new DateField();
			TStDate.setTodayDate();
			TEnDate.setTodayDate();
			
			CUser   	= new JComboBox(new Vector(AUserName));	

               BExit          = new JButton("Exit");
			BApply          = new JButton("Apply");
			BProcess          = new JButton("PDF");

			
          }
          catch(Exception e)
          {
               System.out.println(e);
			e.printStackTrace();
		}
     }

     private void setLayouts()
     {
          setTitle("Non Stock Transfer ");
          setClosable(true);                    
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,1100,600);

          TopPanel.setLayout(new GridLayout(7,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder("Info."));
          MiddlePanel.setBorder(new TitledBorder("Details"));
          BottomPanel.setBorder(new TitledBorder("Controls"));
    
		TopPanel            . setBackground(new Color(213,234,255));
		MiddlePanel         .setBackground(new Color(213,234,255));
		BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

          TopPanel            . add(new JLabel("FromDate"));
	     TopPanel            . add(TStDate);
		TopPanel            . add(new JLabel("ToDate"));
	     TopPanel            . add(TEnDate);
		
		TopPanel            . add(new JLabel("User"));
	     TopPanel            . add(CUser);
		TopPanel            . add(new JLabel(""));
	     TopPanel            . add(BApply);
	     TopPanel            . add(new JLabel(""));
	     TopPanel            . add(new JLabel(""));
	    	TopPanel            . add(new JLabel(""));
	     TopPanel            . add(new JLabel(""));
	     TopPanel            . add(new JLabel(""));
	     TopPanel			.add(new JLabel(""));
	     
		
		MiddlePanel.add(new JScrollPane(theTable));

          BottomPanel.add(BExit);
          BottomPanel.add(BProcess);


          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);


          BExit.setMnemonic('E');
     }

	 private void addListeners()
     {
          BExit.addActionListener(new ActList());
          BApply.addActionListener(new ActList());
		BProcess.addActionListener(new ActList());


     }                 

	 public void setTabReport()
     {
          try
          {
		
			String SStDate = common.pureDate((String)TStDate.toNormal()); 
			String SEnDate = common.pureDate((String)TEnDate.toNormal()); 
			
			
			
			String SUser = common.parseNull((String)CUser.getSelectedItem());
			
			System.out.println(SUser);
			int iUserCode = 0;
			if (!SUser.equals("All")){
				iUserCode= common.toInt((String)AUserCode.get(AUserName.indexOf(SUser)));
			}

		theModel.setNumRows(0);	
                
			 ArrayList VDetails = getMaterialIndent(SUser,iUserCode,SStDate,SEnDate,iMillCode);

                    for(int i=0;i<VDetails.size();i++)
               {
                    HashMap  row =(HashMap)VDetails.get(i);  

					String SItemCode = common.parseNull((String)row.get("ITEM_CODE"));
					
					String STotalStock = getTotalStock(SItemCode);

                     Vector VTheVect = new Vector();
				 VTheVect.addElement(String.valueOf(i+1));
				 VTheVect.addElement(common.parseNull((String)row.get("MILLNAME")));
    				 VTheVect.addElement(common.parseNull((String)row.get("HODCODE")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEM_NAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEM_CODE")));
                     
				 VTheVect.addElement(common.parseNull((String)row.get("STOCK")));
                     VTheVect.addElement(common.parseNull((String)row.get("STOCKVALUE")));
                     				 
                     VTheVect.addElement(common.parseNull((String)row.get("CATL")));
                     VTheVect.addElement(common.parseNull((String)row.get("DRAW")));
                     VTheVect.addElement(common.parseNull((String)row.get("LOCNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("STORESTOCK")));
                     //VTheVect.addElement(common.parseNull((String)row.get("TOTALSTOCK")));
			
			//	 VTheVect.addElement(String.ValueOf(getTotalStock(SItemCode)));

				 VTheVect.addElement(String.valueOf(STotalStock));
				 VTheVect.addElement(common.parseNull((String)row.get("DEPTNAME")));
				 VTheVect.addElement(common.parseNull((String)row.get("USERNAME")));
				 VTheVect.addElement(common.parseNull((String)row.get("ENTRYDATE")));
				 
			
                     theModel.appendRow(VTheVect);
               }
        //  }
		}
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }
	
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }             

			if(ae.getSource()==BApply)
               {    
                    theModel.setNumRows(0);
                    setTabReport();
               }	
			
		if(ae.getSource()==BProcess) 
			{
			 try
			 {
				SFileName = "d:\\MaterialReport\\MaterialReceivedReceipt.pdf"; 
				createPDFFile();
				File theFile   = new File(SFileName);
				Desktop        . getDesktop() . open(theFile);
			 }
			  catch(Exception ex){
         //   System.out.println("PrnCreation"+ex);    ask gm to call 169
            ex.printStackTrace();
			}
			 
			}
		
		
		}
     }
	
	
	private String getTotalStock(String SItemCode)
	{
		  String sStock = "0";
            StringBuffer sb = new StringBuffer();

            sb.append(" select sum(Stock) from itemstock where itemcode ='"+SItemCode+"' and millcode = 0 and hodcode = 6125 ");
		  
		 // System.out.println(sb.toString());
		  
		  try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(sb.toString());
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
				sStock = result.getString(1);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
		  return sStock;
     }


 private void showMessage()
     {
          JOptionPane.showMessageDialog(null,getMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage()
     {
          String str = "<html><body>";
          str = str + "A New Country Name has been<br>";
          str = str + "successfully registered<br>";          
          str = str + "</body></html>";
          return str;
     }
	
	
  private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public ArrayList getMaterialIndent(String SUser,int iUserCode,String SStDate, String SEnDate,int iMillCode)
     {

          theList = new ArrayList();
		
		
		
	String QS1 = " select Mill.MillName,ItemStock_Transfer.HodCode,InvItems.Item_Name,InvItems.Item_Code,ItemStock_transfer.Stock ,  "+
				" ItemStock_Transfer.StockValue, "+
				" InvItems.Catl,InvItems.Draw,InvItems.LocName,0 as STORESTOCK, "+
				"  0 as TOTALSTOCK, " +
				" rawuser.UserName,to_char(ItemStock_Transfer.TransferDate,'dd.mm.yyyy') "+
				"  as EntryDate,Stockgroup.groupname from itemstock_transfer   "+
				" inner join InvItems on InvItems.Item_Code = ItemStock_Transfer.ItemCode  and TRANFERSTATUS = 1  "+
				" inner join Mill on Mill.MillCode = ItemStock_Transfer.MillCode  "+
				" inner join rawuser on rawuser.usercode = ItemStock_Transfer.EntryUsercode  " +
				" left join Stockgroup on Stockgroup.groupcode = ItemStock_Transfer.deptcode";
			QS1 = QS1 +" Where to_char(ItemStock_Transfer.transferdate,'yyyymmdd') >= "+SStDate+"  " ;
			QS1 = QS1 +" and to_char(ItemStock_Transfer.transferdate,'yyyymmdd') <="+SEnDate+" ";
				if (!SUser.equals("All"))
				QS1 = QS1 +" and itemstock_transfer.entryusercode = "+iUserCode+" ";
				QS1 = QS1 +" 	order by InvItems.Item_Name asc " ;

		System.out.println(QS1);
		


		try
          {
			
		     
               Class                   . forName("oracle.jdbc.OracleDriver");
		     Connection theConnection           = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "Inventory", "Stores");
			
			Statement theStatement     = theConnection.createStatement();
               ResultSet  result          = theStatement.executeQuery(QS1);
               ResultSetMetaData rsmd    = result.getMetaData();

               while(result.next())
               {
                    HashMap row = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }                         
                    theList.add(row);
               }
               result.close();
              theStatement.close();
		    theConnection.close();
		}
          catch(Exception ex)
          {
               System.out.println(ex);
           ex.printStackTrace();
		}

          return theList;

     }
	

	
	private void createPDFFile()
    {
        try 
        {
			document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(SFileName));
            document.setPageSize(PageSize.LEGAL.rotate());
            document.open();
                             
            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            table.setHeaderRows(3);
			pdfHead();
			pdfBody();
           
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }

    }  

	private void pdfHead()
	{
	
	   AddCellIntoTable("AMARJOTHI SPINNING MILLS LIMITED", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);
        AddCellIntoTable(" NONSTOCK MATERIAL RECEIVED REPORT : "  +TStDate.toString()+ "---" +TEnDate.toString(), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25,0,0,0,0, bigbold);

          AddCellIntoTable("SL NO" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
     		AddCellIntoTable("ITEM CODE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);

		AddCellIntoTable("ITEM NAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("DEPT NAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("TRANSFER QTY" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
        AddCellIntoTable("TRANSFER VALUE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
	    AddCellIntoTable("CATL" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
        AddCellIntoTable("DRAW" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
        AddCellIntoTable("LOCNAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
        AddCellIntoTable("STORE STOCK" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
        AddCellIntoTable("TOTALSTOCK" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("USER NAME" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
     
        
		
		
	}

	private void pdfBody()
	{
	try
	{
	//	int iTotal = 0;
		int colcount=theModel.getColumnCount();
        int rowcount=theModel.getRowCount();
		double dstockTotal = 0;
		  
		  for(int K=0;K<theModel.getRowCount();K++)
            {
	

        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,0)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);

        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,4)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,3)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
         AddCellIntoTable(String.valueOf(theModel.getValueAt(K,12)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(String.valueOf(theModel.getValueAt(K,5)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,6)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,7)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,8)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,9)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,10)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
        AddCellIntoTable(String.valueOf(theModel.getValueAt(K,11)) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
       
		AddCellIntoTable(String.valueOf(theModel.getValueAt(K,13)) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25, 4, 1, 8, 2, smallnormal);
		
		dstockTotal += common.toDouble((String)theModel.getValueAt(K,5));
	
	}
		
		
	AddCellIntoTable("TOTAL STOCK" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 4,45, 4, 1, 8, 2, mediumbold);
	AddCellIntoTable(common.getRound(dstockTotal,0) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,45, 4, 1, 8, 2, mediumbold);
	AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 7,45, 4, 1, 8, 2, mediumbold);		
		document.add(table);
		document.close();
	
	}
	catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }
	}  

	public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str ));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
   }  

}