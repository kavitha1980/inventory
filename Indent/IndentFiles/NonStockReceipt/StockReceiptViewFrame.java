package Indent.IndentFiles.NonStockReceipt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;
//import domain.jdbc.*;
//import Indent.ItemStockViewFrame;

public class StockReceiptViewFrame extends JInternalFrame 
{

     protected     JLayeredPane Layer;

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BExit;
     Common        common = new Common();
     Connection     theConnection;
	
	int           iUserCode=0;
     int PRINT=11;
     
	String SItemName ;

     ArrayList theList = new ArrayList();

     StockReceiptModel   theModel;

     JTable             theTable;

     
     FileWriter FW;
     File file;



     public StockReceiptViewFrame(JLayeredPane Layer,int iUserCode,int iMillCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }

	private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();
			
               theModel       = new StockReceiptModel();
               theTable       = new JTable(theModel);

			
               
               BExit          = new JButton("Exit");
			
          }
          catch(Exception e)
          {
               System.out.println(e);
			e.printStackTrace();
    }
     }

     private void setLayouts()
     {
          setTitle("Non Stock Transfer ");
          setClosable(true);                    
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,1100,600);

          TopPanel.setLayout(new GridLayout(4,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder("Info."));
          MiddlePanel.setBorder(new TitledBorder("Details"));
          BottomPanel.setBorder(new TitledBorder("Controls"));
    
		TopPanel            . setBackground(new Color(213,234,255));
		MiddlePanel         .setBackground(new Color(213,234,255));
		BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

          TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));

          MiddlePanel.add(new JScrollPane(theTable));

          BottomPanel.add(BExit);


          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);


          BExit.setMnemonic('E');
     }

	 private void addListeners()
     {
          BExit.addActionListener(new ActList());

     }                 

	 public void setTabReport()
     {
          try
          {
             	theModel.setNumRows(0);	
                
			 ArrayList VDetails = getMaterialIndent();

                    for(int i=0;i<VDetails.size();i++)
               {
                    HashMap  row =(HashMap)VDetails.get(i);                         

                     Vector VTheVect = new Vector();
				 VTheVect.addElement(String.valueOf(i+1));
				 VTheVect.addElement(common.parseNull((String)row.get("ID")));				 
				 VTheVect.addElement(common.parseNull((String)row.get("MILLNAME")));
    				 VTheVect.addElement(common.parseNull((String)row.get("HODCODE")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEM_NAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEM_CODE")));
                     
				 VTheVect.addElement(common.parseNull((String)row.get("STOCK")));
                     VTheVect.addElement(common.parseNull((String)row.get("STOCKVALUE")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEMSTOCKTRANSFERID")));
			
                     theModel.appendRow(VTheVect);
               }
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }             
					
		}
     }
	
	

 private void showMessage()
     {
          JOptionPane.showMessageDialog(null,getMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage()
     {
          String str = "<html><body>";
          str = str + "A New Country Name has been<br>";
          str = str + "successfully registered<br>";          
          str = str + "</body></html>";
          return str;
     }
	
	
  private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public ArrayList getMaterialIndent()
     {

          theList = new ArrayList();
		
		StringBuffer sb = new StringBuffer();
		
				sb.append("select ItemStock.Id,Mill.MillName,ItemStock.HodCode,InvItems.Item_Name,InvItems.Item_Code,ItemStock.Stock,ItemStock.StockValue,ItemStock.IndentQty,  ");
				sb.append(" ItemStock.TransferQty,ItemStock.ReservedQty,ItemStock.ItemStockTransferId from ItemStock  ");
				sb.append(" Inner join Mill on Mill.MillCode = ItemStock.MillCode ");
				sb.append("   Inner join InvItems on InvItems.Item_Code = ItemStock.ItemCode");
				sb.append(" Order by Id ");
				  
          try
          {
			
		     
               Class                   . forName("oracle.jdbc.OracleDriver");
		     Connection theConnection           = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "Inventory", "Stores");
			
			Statement theStatement     = theConnection.createStatement();
               ResultSet  result          = theStatement.executeQuery(sb.toString());
               ResultSetMetaData rsmd    = result.getMetaData();

               while(result.next())
               {
                    HashMap row = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }                         
                    theList.add(row);
               }
               result.close();
              theStatement.close();
		    theConnection.close();
		}
          catch(Exception ex)
          {
               System.out.println(ex);
           ex.printStackTrace();
		}

          return theList;

     }
}