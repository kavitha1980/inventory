package Indent.IndentFiles.NonStockReceipt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;

//import Indent.IndentFiles.NonStockReceipt.TestingReceipt.*;


public class StockTransferViewFrame extends JInternalFrame 
{

     protected     JLayeredPane Layer;
	
	private   int            iUserCode;
	private   int            iMillCode;

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BExit,BSave;   
     Common        common = new Common();
     Connection     theConnection;
	
     int PRINT=15;
     
	String SItemName ;

     ArrayList theList = new ArrayList();

     StockTransferModel   theModel;
	
	TestingModel       theModell;
	

     JTable             theTable;

     
     FileWriter FW;
     File file;



     public StockTransferViewFrame(JLayeredPane Layer,int iUserCode,int iMillCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
		this.iMillCode = iMillCode;
		
        //  setData();
		createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }

	private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();
			
               theModel       = new StockTransferModel();
               theTable       = new JTable(theModel);
				
		//	theModell   = new TestingModel();
		//	theTable       = new JTable(theModell);

		
               BExit          = new JButton("Exit");
               BSave          = new JButton("Save");
			
          }
          catch(Exception e)
          {
               System.out.println(e);
			e.printStackTrace();
    }
     }

     private void setLayouts()
     {
          setTitle("Non Stock Transfer ");
          setClosable(true);                    
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,1100,600);

          TopPanel.setLayout(new GridLayout(4,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder("Info."));
          MiddlePanel.setBorder(new TitledBorder("Details"));
          BottomPanel.setBorder(new TitledBorder("Controls"));
    
		TopPanel            . setBackground(new Color(213,234,255));
		MiddlePanel         .setBackground(new Color(213,234,255));
		BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

          TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));
	    TopPanel            . add(new JLabel(""));

          MiddlePanel.add(new JScrollPane(theTable));

          BottomPanel.add(BExit);
		BottomPanel.add(BSave);


          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);


          BExit.setMnemonic('E');
     }

	 private void addListeners()
     {
          BExit.addActionListener(new ActList());
		BSave.addActionListener(new ActList());
		theTable.addMouseListener(new MouseList());


     }                 

	 public void setTabReport()
     {
          try
          {
             	theModel.setNumRows(0);	
                
			 ArrayList VDetails = getMaterialIndent(iMillCode);

                    for(int i=0;i<VDetails.size();i++)
               {
                    HashMap  row =(HashMap)VDetails.get(i);                         

                     Vector VTheVect = new Vector();
				  
				  VTheVect.addElement(String.valueOf(i+1));
				 VTheVect.addElement(common.parseNull((String)row.get("ID")));				 
				 VTheVect.addElement(common.parseNull((String)row.get("MILLNAME")));
    				 VTheVect.addElement(common.parseNull((String)row.get("HODCODE")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEM_NAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEM_CODE")));
                     
				 VTheVect.addElement(common.parseNull((String)row.get("STOCK")));
                     VTheVect.addElement(common.parseNull((String)row.get("STOCKVALUE")));
                     VTheVect.addElement(common.parseNull((String)row.get("CATL")));
                     VTheVect.addElement(common.parseNull((String)row.get("DRAW")));
                     VTheVect.addElement(common.parseNull((String)row.get("LOCNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("STORESTOCK")));
                     VTheVect.addElement(common.parseNull((String)row.get("TOTALSTOCK")));
                     
				 VTheVect.addElement(common.parseNull((String)row.get("USERNAME")));
				 VTheVect.addElement(common.parseNull((String)row.get("ENTRYDATE")));
				 
				VTheVect.addElement(new Boolean(false));
			
                     theModel.appendRow(VTheVect);
               }
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }             
			
			if(ae.getSource()==BSave)
			{
				 if(validations())
                         {  
                           if(JOptionPane.showConfirmDialog(null, "Are you sure you want to Save the Details?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
					  {
						SaveDetails();
						setTabReport();
					  }
					}
          
		
			}
		}
	}

	private class MouseList extends MouseAdapter
	{
		public void mouseClicked(MouseEvent me)
		{
			
		}
	}

 private void showMessage()
     {
          JOptionPane.showMessageDialog(null,getMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage()
     {
          String str = "<html><body>";
          str = str + "A New Country Name has been<br>";
          str = str + "successfully registered<br>";          
          str = str + "</body></html>";
          return str;
     }
	
	private boolean validations()
			{
				
			int iCount=0;
		
			for(int i=0; i<theModel.getRowCount(); i++)
			{
				Boolean bValue = (Boolean)theModel.getValueAt(i, 15);

				if(bValue.booleanValue())
				{	
					
					
					String SItemCode   = common.parseNull((String)theModel.getValueAt(i, 5));				

					int iId           = common.toInt((String)theModel.getValueAt(i, 1));
					String SItemCodee  = common.parseNull((String)theModel.getValueAt(i, 5));				
					int iHODCode  = 6125;
					int iTransferStock  = common.toInt((String)theModel.getValueAt(i,6));
					
					String SItem   = common.parseNull((String)theModel.getValueAt(i, 5));	
						System.out.println(SItem);
					int iStock  = common.toInt((String)theModel.getValueAt(i,6));
										int iIdd           = common.toInt((String)theModel.getValueAt(i, 1)); 


					
					
			

			String QS10 = "  Select Count(*) from ItemStock_temp "+
                       "  where ItemCode ='"+SItemCode+"' and HODCode = 6125 ";	
				
				   
                try
                  { 
                       if(theConnection==null)
					{
                     	Class                   . forName("oracle.jdbc.OracleDriver");
			          theConnection           = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "Inventory", "Stores");
					}
					theConnection			. commit();
					theConnection			. setAutoCommit(false);	
 
					PreparedStatement ps     = theConnection.prepareStatement(QS10);
					ResultSet	result1	 	= ps.executeQuery();
				  
               		while(result1.next())
               		{ 

                           iCount=result1.getInt(1);

					}
					result1 . close();
					ps    . close();
					
					
          
			if(iCount>0){
			
					/*
					String QS15 = " Insert into Itemstock_History(MillCode,HODCode,ItemCode,Stock,StockValue,IndentQty,TransferQty,ReservedQty,ItemStockTransferId,Id,EntryDateTime ) " +
							   "	Values( "+iMillCode+","+iHODCode+",'"+SItem+"',"+iStock+", "+
							   " 0,0,0,0,"+iId+",ItemStockHistory_seq.nextVal,SysDate  )";	
					*/


				//	String QS15 = "Insert into ItemStock_History (select * from ItemStockTransfer_temp where id =	"+iId+" ) ";			
				
					String QS15 = "Insert into ItemStock_History (select * from ItemStock_Temp Where ItemCode = '"+SItemCode+"' ) ";			
					
					PreparedStatement ps16 = theConnection.prepareStatement(QS15);
					ps16.executeQuery();	
					ps16.close();
					
					
					
					String QS3 = "Update  ItemStock_Temp set Stock = "+iTransferStock+" and  Where ItemCode = '"+SItemCode+"' and HODCode = "+iHODCode+" and MillCode = "+iMillCode+" "; 	

					System.out.println(QS3);

					PreparedStatement ps1 = theConnection.prepareStatement(QS3);
					ps1.executeQuery();	
					ps1.close();
					
					
					
						}
			else
					{
						
					String QS4 = " Insert into Itemstock_Temp(MillCode,HODCode,ItemCode,Stock,StockValue,IndentQty,TransferQty,ReservedQty,ItemStockTransferId,Id,EntryDateTime,SLNO ) " +
							   "	Values( "+iMillCode+","+iHODCode+",'"+SItemCodee+"',"+iTransferStock+", "+
							   " 0,0,0,0,"+iId+",ItemStockT_seq.nextVal,SysDate,ItemStockTemp_t_seq.nextVal  )";					
					
					PreparedStatement ps6 = theConnection.prepareStatement(QS4);
					ps6.executeQuery();	
					ps6.close();	
					}
			  
			  theConnection			. commit();
			theConnection			. setAutoCommit(true);	
			  
			   }
			
			
			catch(Exception e){
			e.printStackTrace(); 
			}
			
			}	// boolean close		

			}	 // For loop close brace
			return true;

			 }  
	
	private  void SaveDetails()
	{
		try
		{
		    Class          . forName("oracle.jdbc.OracleDriver");
			Connection theConnection           = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "Inventory", "Stores");
	
			for(int i=0; i<theModel.getRowCount(); i++)
			{
				Boolean bValue = (Boolean)theModel.getValueAt(i, 15);

				if(bValue.booleanValue())
				{

					int iId           = common.toInt((String)theModel.getValueAt(i, 1));
					String SItemCode   = common.parseNull((String)theModel.getValueAt(i, 5));				
					int iHODCode  = 6125;
					int iTransferStock  = common.toInt((String)theModel.getValueAt(i,6));
					
					
																																													
					String QS = " Update ItemStockTransfer_Temp	set TransferStatus = 1 , TransferDate = SysDate Where ItemCode = '"+SItemCode+"' and Id = "+iId+" ";
					System.out.println(QS);
				
					PreparedStatement ps = theConnection.prepareStatement(QS);
					ps.executeUpdate();	
					ps.close();
					
				
						
					
				}	
				
			}
			
			theConnection			. commit();
			theConnection			. setAutoCommit(true);	
				
			JOptionPane.showMessageDialog(null, "Data Saved Successfully!!", "Info!", JOptionPane.INFORMATION_MESSAGE);
		}
		catch(Exception ex)
		{
			try
			{	
				theConnection		. rollback();
				theConnection		. setAutoCommit(true);
			}
			catch(Exception e){}
			ex.printStackTrace();
			
               JOptionPane.showMessageDialog(null, "Data Not Saved.. Error : "+ex.getMessage(), "Info!", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
  private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public ArrayList getMaterialIndent( int iMillCode)
     {

          theList = new ArrayList();
		
		StringBuffer sb = new StringBuffer();
		
				sb.append(" select ItemStockTransfer_Temp.Id,Mill.MillName,ItemStockTransfer_Temp.HodCode,InvItems.Item_Name,InvItems.Item_Code,ItemStockTransfer_Temp.Stock,ItemStockTransfer_Temp.StockValue, ");
				sb.append(" InvItems.Catl,InvItems.Draw,InvItems.LocName, ");
				sb.append(" rawuser.UserName,to_char(ItemStockTransfer_Temp.EntryDateTime,'dd.mm.yyyy') as EntryDate from ItemStockTransfer_Temp ");
				sb.append(" Inner join Mill on Mill.MillCode = ItemStockTransfer_Temp.MillCode ");
				sb.append("   Inner join InvItems on InvItems.Item_Code = ItemStockTransfer_Temp.ItemCode");
				sb.append("   Inner join rawuser on rawuser.UserCode = ItemStockTransfer_Temp.EntryUserCode");
				sb.append("  Where ItemStockTransfer_Temp.TransferStatus = 0 and Mill.MillCode = "+iMillCode+"  ");
				sb.append("  group by ItemStockTransfer_Temp.Id,Mill.MillName,ItemStockTransfer_Temp.HodCode,InvItems.Item_Name,InvItems.Item_Code, ");
				sb.append(" ItemStockTransfer_Temp.Stock,ItemStockTransfer_Temp.StockValue,");
				sb.append(" InvItems.Catl,InvItems.Draw,InvItems.LocName,rawuser.UserName,to_char(ItemStockTransfer_Temp.EntryDateTime,'dd.mm.yyyy') ");
				sb.append(" Order by Id ");
				  
          try
          {
			
		     
               Class                   . forName("oracle.jdbc.OracleDriver");
		     Connection theConnection           = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "Inventory", "Stores");
			
			Statement theStatement     = theConnection.createStatement();
               ResultSet  result          = theStatement.executeQuery(sb.toString());
               ResultSetMetaData rsmd    = result.getMetaData();

               while(result.next())
               {
                    HashMap row = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }                         
                    theList.add(row);
               }
               result.close();
              theStatement.close();
		    theConnection.close();
		}
          catch(Exception ex)
          {
               System.out.println(ex);
           ex.printStackTrace();
		}

          return theList;

     }
}