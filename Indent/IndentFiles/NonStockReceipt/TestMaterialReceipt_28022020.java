package Indent.IndentFiles.NonStockReceipt;

import guiutil.FinDateField;
import jdbc.ORAConnection;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;
import java.net.InetAddress;
import util.*;
import javax.swing.table.TableColumnModel;

public class TestMaterialReceipt_28022020 extends JInternalFrame 
{

     protected     JLayeredPane Layer;

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BApply;
     JButton       BExit;
     JButton       BSave;
/*	DateField      TStDate;
     DateField      TEnDate; */
	Vector        VDeptName,VDeptCode,VUserName,VUserCode,VItemName,VItemCode;
     JTextField    searchField;
	JComboBox    CDept,CUser,CItem;
     Common        common = new Common();
     Connection     theConnection;
	String        SUser,SDept,SItem,SITEMCODE;
	int iINDENTNO,iRECEIPTQTY;
	
     int           iUserCode=0,iMillCode,iOwnerCode,iDeptCode;
     int PRINT=11;
     
   FinDateField TFromDate,TToDate;

     ArrayList theList = new ArrayList();
     ArrayList theReturnList = new ArrayList();

     TestMaterialReceiptModel   theModel;

     JTable             theTable;
	InetAddress ip;
     
     public TestMaterialReceipt_28022020(JLayeredPane Layer,int iUserCode,int iMillCode,int iOwnerCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
		  this.iOwnerCode = iOwnerCode;

          setData();
		createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel.setNumRows(0);

          setTabReport();
     }
	
 private void setData()
	{
		VDeptName			= null;
		VDeptCode			= null;
		VUserName			= null;
		VUserCode			= null;
		VItemName			= null;
		VItemCode			= null;
		
		
		VDeptName			= new Vector();
		VDeptCode			= new Vector();	
		VUserName			= new Vector();
		VUserCode			= new Vector();
		VItemName			= new Vector();
		VItemCode			= new Vector();
			
		VDeptName			. clear();
		VDeptCode			. clear();
		VUserName			. clear();
		VUserCode			. clear();
		VItemName           . clear();
		VItemCode           . clear();
		
		VDeptName			. add("All");
		VDeptCode			. add("999999");
		
		VUserName			. add("All");
		VUserCode			. add("999999");
		
		VItemName			. add("All");
		VItemCode			. add("999999");
			
		try
		{
			Class          . forName("oracle.jdbc.OracleDriver");
			Connection theConnection           = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "inventory", "stores");
			Statement theStatement = theConnection.createStatement();
			
			String QSS = " Select DeptName,DeptCode from Hrdnew.Department order by 1";
			ResultSet re = theStatement.executeQuery(QSS);

			while (re.next())
			{
				VDeptName		. add(common.parseNull(re.getString(1)));
				VDeptCode		. add(common.parseNull(re.getString(2)));
				
			}
			re.close();
			
			String QS46 = " Select UserName,UserCode from Rawuser order by 1";
			ResultSet rss = theStatement.executeQuery(QS46);

			while (rss.next())
			{
				VUserName		. add(common.parseNull(rss.getString(1)));
				VUserCode		. add(common.parseNull(rss.getString(2)));
				
			}
			rss.close();
			
			
			String QS66 = " Select distinct ItemName,Itemcode from Materialindentdetails order by 1";
			ResultSet rssset = theStatement.executeQuery(QS66);

			while (rssset.next())
			{
				VItemName		. add(common.parseNull(rssset.getString(1)));
				VItemCode		. add(common.parseNull(rssset.getString(2)));
				
			}
			rssset.close();
			
			theStatement 		.close();
			theConnection		.close();
			
		}
		catch(Exception ex)
			{
			System.out.println("From MaterialIndentDetails "+ex);
			}
		}	
	

     private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();

               theModel       = new TestMaterialReceiptModel();
               theTable       = new JTable(theModel);
               
               
               TableColumnModel TC  = theTable.getColumnModel();

                for(int i=0;i<theModel.ColumnName.length;i++)   {

                    TC .getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
                }

               TFromDate      = new FinDateField();
               TToDate        = new FinDateField();

               TFromDate      .   setTodayDate();
               TToDate        .   setTodayDate();
			
			
			CDept       = new JComboBox(new Vector(VDeptName));
		
			CUser       = new JComboBox(new Vector(VUserName));
			
			CItem       = new JComboBox(new Vector(VItemName));
          
			
               BExit          = new JButton("Exit");
               BApply         = new JButton("Apply");
               BSave          = new JButton("Save");
               
               searchField    = new JTextField();
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }
     }

     private void setLayouts()
     {
            setTitle("Material Receipt Against Return ");
            setClosable(true);                    
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,775,600);

            TopPanel.setLayout(new GridLayout(7,2));
            MiddlePanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());

            TopPanel.setBorder(new TitledBorder("Info."));
            MiddlePanel.setBorder(new TitledBorder("Details"));
            BottomPanel.setBorder(new TitledBorder("Controls"));

            TopPanel            . setBackground(new Color(213,234,255));
            MiddlePanel         .setBackground(new Color(213,234,255));
            BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

   /*     TopPanel            . add(new JLabel("FromDate"));
        TopPanel            . add(TFromDate);
         TopPanel            . add(new JLabel("ToDate"));
        TopPanel            . add(TToDate);*/

	   TopPanel            . add(new JLabel("Department"));
        TopPanel            . add(CDept);


        TopPanel            . add(new JLabel("User"));
        TopPanel            . add(CUser);
        
	    TopPanel            . add(new JLabel("Item"));
        TopPanel            . add(CItem);
	   
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
        TopPanel            . add(new JLabel(""));
	   TopPanel	    . add(BApply);
         
        TopPanel            . add(new JLabel("Search"));
        TopPanel            . add(searchField);
        
        TopPanel            . add(new JLabel(""));
        TopPanel            . add(new JLabel(""));
        
      

        MiddlePanel.add(new JScrollPane(theTable));

        BottomPanel.add(BExit);
        BottomPanel.add(BSave);

        getContentPane().add("North",TopPanel);
        getContentPane().add("Center",MiddlePanel);
        getContentPane().add("South",BottomPanel);
        BExit.setMnemonic('E');
        BSave.setMnemonic('S');

     }

    private void addListeners()
    {
          BExit.addActionListener(new ActList());
          BApply.addActionListener(new ActList());
          BSave.addActionListener(new ActList());
          searchField. addKeyListener(new SearchKeyList());
          //theTable.addKeyListener(new KeyList ());
    }

    public void setTabReport()
    {
          try
          {
			int iUserCode = 0;
			int iDeptCode =0;
			String SItemCode = "";
			
                String SFromDate = common.pureDate((String)TFromDate.toNormal()); 
                String SToDate = common.pureDate((String)TToDate.toNormal()); 
			 
			 String SDept  = common.parseNull((String)CDept.getSelectedItem());
			 if (!SDept.equals("All"))
			  iDeptCode = common.toInt((String)VDeptCode.get(VDeptName.indexOf(SDept)));
			 String SUser  = common.parseNull((String)CUser.getSelectedItem());
			 if (!SUser.equals("All"))
			  iUserCode = common.toInt((String)VUserCode.get(VUserName.indexOf(SUser)));
		   String SItem  = common.parseNull((String)CItem.getSelectedItem());
			 if (!SItem.equals("All"))
			  SItemCode = common.parseNull((String)VItemCode.get(VItemName.indexOf(SItem)));

                getMaterialIndentReturn(SFromDate,SToDate);

                ArrayList ADetails = getMaterialIndent(SFromDate,SToDate,iDeptCode,iUserCode,SUser,SDept,SItem,SItemCode);

                for(int i=0;i<ADetails.size();i++)
                {
                     HashMap  row =(HashMap)ADetails.get(i);

                     Vector VTheVect = new Vector();
                     
                     String sItemCode = common.parseNull((String)row.get("ITEMCODE"));
                     String sIndentNo = common.parseNull((String)row.get("INDENTNO"));
                     String sUnitCode = common.parseNull((String)row.get("UNIT_CODE"));
                     String sDeptCode= common.parseNull((String)row.get("DEPTCODE"));
                     String sMachCode = common.parseNull((String)row.get("MACHINECODE"));

                     VTheVect.addElement(common.parseNull((String)row.get("ITEMCODE")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEMNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("REQUIREDQTY")));
                     VTheVect.addElement(common.parseNull((String)row.get("INDENTNO")));
                     VTheVect.addElement(common.parseNull((String)row.get("UNITNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("DEPTNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("MACHINENAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("USERNAME")));
                     VTheVect.addElement(common.parseNull(common.parseDate((String)row.get("ENTRYDATE"))));
                     VTheVect.addElement(String.valueOf(getRetAlreadyReturned(sItemCode,sIndentNo,sUnitCode,sDeptCode,sMachCode)));
                     VTheVect.addElement(common.parseNull((String)row.get("DELAYDAYS")));
                     VTheVect.addElement("");

                     theModel.appendRow(VTheVect);
               }
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
			if(ae.getSource()==BSave)
			{
				moveCursorToOtherCell(0, 0);
				
				if(validations())
				{  
					if(JOptionPane.showConfirmDialog(null, "Are you sure you want to Save the Details?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
					{
						insertReturn();
					}
				}
			}
			
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }

               if(ae.getSource()==BApply)
               {
                    theModel.setNumRows(0);
                    setTabReport();
               }
          }
     }
     private class KeyList extends KeyAdapter  {

          public void keyPressed(KeyEvent ke)
          {
               int iRow = theTable.getSelectedRow       ();
               int iCol = theTable.getSelectedColumn    ();

               System.out.println("keyPressed");

               if( iCol==11) {

			checkValue(iRow);
               }
           }

          public void keyReleased(KeyEvent ke)   {
             try   {

            int    iRow    = theTable.getSelectedRow();
            int    iCol    = theTable.getSelectedColumn();
            
            System.out.println("keyReleased");

            if( iCol==11) {              
			checkValue(iRow);
            }

         }catch(Exception e)  {
   
            e.printStackTrace();
         }
      }
   }
     private class SearchKeyList extends KeyAdapter
        {
            public void keyReleased(KeyEvent ke)
            {
                searchItems();     
            }

            public void keyPressed(KeyEvent ke)
            {
                searchItems();
            }    

            public void keyTyped(KeyEvent ke)
            {
                searchItems();
            }
        }
	   
	private void moveCursorToOtherCell(int iRow, int iCol) {
		theTable       . setRowSelectionInterval(iRow, iCol);
		Rectangle cell = theTable.getCellRect(iRow, 0, true);
		theTable       . scrollRectToVisible(cell);
		
		theTable		. editCellAt(iRow, iCol);
	}

    private void showMessage(String sMsg)
    {
          JOptionPane.showMessageDialog(null,getMessage(sMsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
    }
    private String getMessage(String sMsg)
    {
          String str = "<html><body>";
          str = str + sMsg;
          str = str + "</body></html>";
          return str;
    }
	
    private void removeHelpFrame()
    {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public ArrayList getMaterialIndent(String SFromDate, String SToDate, int iDeptCode,int iUserCode,String SUser,String SDept, String SItem,String SItemCode)
     {
            theList = new ArrayList();
            StringBuffer sb = new StringBuffer();

	String QS92=		" Select MaterialIndentDetails.ItemCode,MaterialIndentDetails.ItemName,MaterialIndentDetails.RequiredQty,MaterialIndentDetails.IndentNo, "+
					" process_units.UnitName,Department.DeptName,nvl(Machine.Mach_Name,'General')  as MACHINENAME,Rawuser.UserName, "+ 
					" to_char(MaterialIndentDetails.EntryDate,'yyyymmdd') as EntryDate,MaterialIndentDetails.Unit_code,  "+
					" MaterialIndentDetails.DeptCode,MaterialIndentDetails.MachineCode,Round(SysDate - MaterialIndentDetails.EntryDate) as DelayDays,MaterialIndentDetails.RETURNQTY,MaterialIndentDetails.ID  "+
					" From MaterialIndentDetails  "+
					" Left Join Machine On Machine.Mach_Code = MaterialIndentDetails.MachineCode   "+
					" Inner Join process_units On process_units.UnitCode = MaterialIndentDetails.Unit_Code  "+
					" Inner Join HrdNew.Department On Department.DeptCode = MaterialIndentDetails.DeptCode  "+
					" Inner join Scm.Rawuser on Rawuser.usercode = MaterialIndentDetails.usercode "+
					" Where to_char(MaterialIndentDetails.EntryDate,'yyyymmdd') <=to_char(sysdate,'yyyymmdd') "+
					" and MaterialIndentDetails.RETURNSTATUS=1  "+
					" and MaterialIndentDetails.STORERECEIPTSTATUS=0  ";
					if (!SDept.equals("All"))
					QS92 = QS92 +" and MaterialIndentDetails.DeptCode="+iDeptCode+"  ";
					if (!SUser.equals("All"))
					QS92 = QS92 +" and MaterialIndentDetails.UserCode="+iUserCode+"  ";
					if (!SItem.equals("All"))
					QS92 = QS92 +" and MaterialIndentDetails.ItemCode='"+SItemCode+"'  ";
					QS92 = QS92 +" Order by Department.DeptName  ";  //to_char(MaterialIndentDetails.EntryDate,'yyyymmdd'),
					

		System.out.println(sb.toString());

            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(QS92);
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theList;
     }
     public ArrayList getMaterialIndentReturn(String SFromDate, String SToDate)
     {
            theReturnList = new ArrayList();

            StringBuffer sb = new StringBuffer();

            sb.append(" select ITEMCODE,INDENTNO,UNIT_CODE,DEPTCODE,MACHINECODE,REQUIREDQTY,RETURNQTY ");
            sb.append(" From MaterialReturnDetails ");
            sb.append(" Where nvl(RETURNQTY,0)<REQUIREDQTY ");
            sb.append(" and to_char(MaterialReturnDetails.EntryDate,'yyyymmdd') <="+SToDate+" and ReturnType=1 ");
		  

            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(sb.toString());
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theReturnList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theReturnList;
     }
     private void checkValue(int iRow){


         double dalreadyRet = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,9)));

         double dretQty = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,11)));
         
         double dReqQty = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,2)));
         
         double dNeedToRetrun = dReqQty-dalreadyRet;

         if(dretQty>dNeedToRetrun){

              showMessage("Return Quantiy is Must be equal or Less Than Required Qty");
          }

    }
     private void insertReturn()
	{
         int iError = 0;
         int icheck = 0;
         StringBuffer sb = new StringBuffer();
         StringBuffer sbupdate = new StringBuffer();
         PreparedStatement preparedStat = null;
         PreparedStatement preparedSt= null;
         java.util.HashMap theMap = new java.util.HashMap();
	    String iip = "";

		try
		{
		   
			iip = InetAddress.getLocalHost().getHostAddress(); 
		   
			sb.append(" Insert into MaterialReturnDetails ( ID,ITEMCODE,ITEMNAME,REQUIREDQTY,INDENTNO,UNIT_CODE,DEPTCODE,MACHINECODE,USERCODE,ENTRYDATE,RETURNQTY,MACHINENAME,RETURNTYPE,OWNERCODE,STORERECEIPTDATE,SYSTEMNAME ) ");
			sb.append(" Values(MaterialReturnDetails_Seq.nextVal,?,?,?,?,?,?,?,?,sysdate,?,?,1,?,sysdate,'"+iip+"') ");
            
			sbupdate.append(" Update MaterialIndentDetails set STORERECEIPTSTATUS=1 , StoreReceiptDate = SysDate  ") ;  
			sbupdate.append(" where ITEMCODE=? and INDENTNO=? and UNIT_CODE=? and DEPTCODE=? and MACHINECODE=? and ID=? ") ;

			if(theConnection ==null) 
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
		  
			theConnection			. setAutoCommit(false);
		  
			preparedStat = theConnection.prepareStatement(sb.toString());
			preparedSt = theConnection.prepareStatement(sbupdate.toString());
            
			for (int index = 0; index < theModel.getRows(); index++) 
			{
				theMap = (java.util.HashMap) theList.get(index);

				double dretQty = common.toDouble(common.parseNull((String)theModel.getValueAt(index,11)));

				if(dretQty > 0)
				{
					double dReqQty = 0;
					
					StringBuffer sb11	= null;
					sb11				= new StringBuffer();

					sb11.append(" Select Sum(ReturnQty) from MaterialReturnDetails where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' "); 
					sb11.append(" and ReturnFromIndent = 1 ");

					PreparedStatement thePS	= theConnection.prepareStatement(sb.toString());
					ResultSet	result	 	= thePS.executeQuery();
			
					while(result.next())
					{ 
						dReqQty 			= common.toDouble(result.getString(1));
					}
					
					thePS                    . close();
					result				. close();
					
					double dalreadyRet = 0;
					
					StringBuffer sb1	= null;
					sb1				= new StringBuffer();
					
					sb1.append(" Select Sum(ReturnQty) from MaterialReturnDetails where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' ");
					sb1.append(" and ReturnFromIndent = 0 ");
					sb1.append(" and ReturnType = 1 ");

					thePS				= theConnection.prepareStatement(sb1.toString());
					result	 			= thePS.executeQuery();
			
					while(result.next())
					{ 
						dalreadyRet		= common.toDouble(result.getString(1));
					}
					
					preparedStat.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
					preparedStat.setString(2,common.parseNull((String) theMap.get("ITEMNAME")));
					preparedStat.setString(3,common.parseNull((String) theMap.get("REQUIREDQTY")));
					preparedStat.setString(4,common.parseNull((String) theMap.get("INDENTNO")));
					preparedStat.setString(5,common.parseNull((String) theMap.get("UNIT_CODE")));
					preparedStat.setString(6,common.parseNull((String) theMap.get("DEPTCODE")));
					preparedStat.setString(7,common.parseNull((String) theMap.get("MACHINECODE")));
					preparedStat.setInt(8,iUserCode);
					preparedStat.setString(9,common.parseNull((String)theModel.getValueAt(index,11)));
					preparedStat.setString(10,common.parseNull((String) theMap.get("MACHINENAME")));
					preparedStat.setInt(11,iOwnerCode);
					preparedStat.executeUpdate();

					double dtotal = (dalreadyRet+dretQty);

					System.out.println("dalreadyRet=="+dalreadyRet);
					System.out.println("dReciptQty=="+dretQty);

					System.out.println("dtotal=="+dtotal);
					
					if(dtotal == dReqQty)
					{
						preparedSt.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
						preparedSt.setString(2,common.parseNull((String) theMap.get("INDENTNO")));
						preparedSt.setString(3,common.parseNull((String) theMap.get("UNIT_CODE")));
						preparedSt.setString(4,common.parseNull((String) theMap.get("DEPTCODE")));
						preparedSt.setString(5,common.parseNull((String) theMap.get("MACHINECODE")));
						preparedSt.setString(6,common.parseNull((String) theMap.get("ID")));
						preparedSt.executeUpdate();
					}
				}
			}
			
			preparedSt.close();
			preparedStat.close();

			theConnection			. commit();
			theConnection			. setAutoCommit(true);
			
			showMessage("Return Quantity Updated..");
			theModel.setNumRows(0);
			setTabReport();
		}
		catch(Exception e)
		{
			try
			{
				theConnection		. rollback();
				theConnection		. setAutoCommit(true);
			}
			catch(Exception ex){}
			iError = 1;
			e.printStackTrace();
			
			showMessage("Return Quantity Not Updated..");
		}
     }

     private double getRetAlreadyReturned(String sItemCode,String sIndnetNo,String sUnitCode,String sDeptCode,String sMachCode){

         double dalreadyRet = 0;
         java.util.HashMap theMap = new java.util.HashMap();
         
         for (int index = 0; index < theReturnList.size(); index++) {
             
             theMap = (java.util.HashMap) theReturnList.get(index);
             
            String sItem =  common.parseNull((String) theMap.get("ITEMCODE"));
            String sIndnent =  common.parseNull((String) theMap.get("INDENTNO"));
            String sUnit =  common.parseNull((String) theMap.get("UNIT_CODE"));
            String sDept =  common.parseNull((String) theMap.get("DEPTCODE"));
            String sMach =  common.parseNull((String) theMap.get("MACHINECODE"));
            
            if(sItemCode.equals(sItem) && sIndnetNo.equals(sIndnent) && sUnitCode.equals(sUnit) && sDeptCode.equals(sDept) && sMachCode.equals(sMach)){

                    dalreadyRet = common.toDouble(common.parseNull((String) theMap.get("RETURNQTY")));

                    //break;
            }
        }
         return dalreadyRet;
     }
     private void searchItems()
     {
          String SSearchStr        = common.parseNull(searchField.getText()).trim();

          int iIndex               = -1;

          for(int i=0; i<theModel.getRowCount(); i++)
          {
               String SItemName    = common.parseNull((String)theModel.getValueAt(i, 1));

               if(SItemName.startsWith(SSearchStr.toUpperCase()))
               {
                    iIndex         = i;
                    break;
               }
          }
          if(iIndex != -1)
          {
                 theTable       . setRowSelectionInterval(iIndex, iIndex);
                 Rectangle theRect   = theTable.getCellRect(iIndex, 0, true);
                 theTable       . scrollRectToVisible(theRect);
                 theTable       . setSelectionBackground(Color.green);			   
          }
     }

	private boolean validations()
	{
		try
		{
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}

			for(int i=0; i<theModel.getRowCount(); i++)
			{
				int iRECEIPTQTY  = common.toInt((String)theModel.getValueAt(i, 11));			
				String SITEMCODE  = common.parseNull((String)theModel.getValueAt(i, 0));
				int iINDENTNO  = common.toInt((String)theModel.getValueAt(i, 3));
				double dretQty = common.toDouble(common.parseNull((String)theModel.getValueAt(i,11)));
                
                    if(dretQty>0)
				{
					double dReturnQty	= 0, dRecdQty = 0;
					
					StringBuffer sb	= null;
					sb				= new StringBuffer();

					sb.append(" Select Sum(ReturnQty) from MaterialReturnDetails where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' "); 
					sb.append(" and ReturnFromIndent = 1 ");

					PreparedStatement thePS	= theConnection.prepareStatement(sb.toString());
					ResultSet	result	 	= thePS.executeQuery();
			
					while(result.next())
					{ 
						dReturnQty 		= common.toDouble(result.getString(1));
					}
					
					thePS                    . close();
					result				. close();
					
					StringBuffer sb1	= null;
					sb1				= new StringBuffer();
					
					sb1.append(" Select Sum(ReturnQty) from MaterialReturnDetails where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' ");
					sb1.append(" and ReturnFromIndent = 0 ");
					sb1.append(" and ReturnType = 1 ");

					thePS				= theConnection.prepareStatement(sb1.toString());
					result	 			= thePS.executeQuery();
			
					while(result.next())
					{ 
						dRecdQty 			= common.toDouble(result.getString(1));
					}
					
					dRecdQty				= dRecdQty + dretQty;
					
					thePS                    . close();
					result				. close();

					if(dRecdQty > dReturnQty)
					{
						JOptionPane.showMessageDialog(null, "Return Quantity is Must be equal or Less Than Indent Return Qty.. Check Row No. "+(i+1), "Error!", JOptionPane.ERROR_MESSAGE);
						return false;
					}
				}
			}
			
			return true;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			
			JOptionPane.showMessageDialog(null, "Problem while Checking Data..", "Error!", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}


}
