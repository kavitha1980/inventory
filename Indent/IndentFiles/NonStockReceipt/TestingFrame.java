package Indent.IndentFiles.NonStockReceipt;

import java.io.*;


import guiutil.FinDateField;
import jdbc.ORAConnection;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;
import java.net.InetAddress;
import util.*;
import javax.swing.table.TableColumnModel;


import com.digitalpersona.onetouch.*;
import com.digitalpersona.onetouch.capture.*;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.*;
import com.digitalpersona.onetouch.verification.*;


public class TestingFrame extends JInternalFrame 
{

     protected     JLayeredPane Layer;

     JPanel        TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopLast;
     JTabbedPane   thePane;
     JButton       BApply;
     JButton       BExit;
     JButton       BSave,BPunch;
/*	DateField      TStDate;
     DateField      TEnDate; */
	Vector        VDeptName,VDeptCode,VUserName,VUserCode,VItemName,VItemCode,VUDept,VUDeptCode,VUnit,VUnitCode,VCata,VCataCode,VTypeName,VTypeCode,VAuthUserCode,VEmpName,VEmpCode;
     JTextField    searchField;
	JComboBox    CDept,CUser,CItem,CType;
     Common        common = new Common();
     Connection     theConnection;
	String        SUser,SDept,SItem,SAuthUserCode,SUnit,SUserNam,SDeptt;
	int iRECEIPTQTY,iIndentNo;
	
     int           iUserCode=0,iMillCode,iOwnerCode,iDeptCode,iCode,iUserCod,ideptcod;
     int PRINT=11;
	
	String SCode = "";
	String SName = "";
	String SUName = "";
	JLabel          LStatus;
	
	 int iPunchStatus=0;
     int iProcessStatus=0;
         ArrayList theTemplateList,theMaterialIssueTemplateList;

     
   FinDateField TFromDate,TToDate;

     ArrayList theList = new ArrayList();
     ArrayList theReturnList = new ArrayList();

     TestingFrameModel   theModel;

     JTable             theTable;
	InetAddress ip;
	
	private DPFPCapture capturer = DPFPGlobal.getCaptureFactory().createCapture();

     private DPFPVerification verificator = DPFPGlobal.getVerificationFactory().createVerification();

     DPFPTemplate template;
	
     
     public TestingFrame(JLayeredPane Layer,int iUserCode,int iMillCode,int iOwnerCode,ArrayList theTemplateList,ArrayList theMaterialIssueTemplateList,String SAuthUserCode,String SDept,String SUnit)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
		this.iOwnerCode = iOwnerCode;
		this.SAuthUserCode = SAuthUserCode;
		 this.SDept          = SDept;
          this.SUnit          = SUnit;
       //   this.SIndentDate    = SIndentDate;
      //    this.SItemTable     = SItemTable;
       //   this.SSupTable      = SSupTable;
       //   this.SYearCode      = SYearCode;
		this.theTemplateList              = theTemplateList;
          this.theMaterialIssueTemplateList = theMaterialIssueTemplateList;  
		  

          setData();
		setData2();
	//	setData22();
		setData3();
          getUsers(theConnection);
		createComponents();
          setLayouts();
          addComponents();
          addListeners();
		

          theModel.setNumRows(0);

          setTabReport();
		
		onLoad();
     }
	
 private void setData()
	{
		VDeptName			= null;
		VDeptCode			= null;
		VUserName			= null;
		VUserCode			= null;
		VItemName			= null;
		VItemCode			= null;
		
		
		VDeptName			= new Vector();
		VDeptCode			= new Vector();	
		VUserName			= new Vector();
		VUserCode			= new Vector();
		VItemName			= new Vector();
		VItemCode			= new Vector();
			
		VDeptName			. clear();
		VDeptCode			. clear();
		VUserName			. clear();
		VUserCode			. clear();
		VItemName           . clear();
		VItemCode           . clear();
		
		VDeptName			. add("All");
		VDeptCode			. add("999999");
		
		VUserName			. add("All");
		VUserCode			. add("999999");
		
		VItemName			. add("All");
		VItemCode			. add("999999");
			
		try
		{
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
			
			Statement theStatement = theConnection.createStatement();
			
			String QSS = " Select DeptName,DeptCode from Hrdnew.Department order by 1";
			ResultSet re = theStatement.executeQuery(QSS);

			while (re.next())
			{
				VDeptName		. add(common.parseNull(re.getString(1)));
				VDeptCode		. add(common.parseNull(re.getString(2)));
				
			}
			re.close();
			
			String QS46 = " Select UserName,UserCode from Rawuser order by 1";
			ResultSet rss = theStatement.executeQuery(QS46);

			while (rss.next())
			{
				VUserName		. add(common.parseNull(rss.getString(1)));
				VUserCode		. add(common.parseNull(rss.getString(2)));
				
			}
			rss.close();
			
			
			String QS66 = " Select distinct ItemName,Itemcode from Materialindentdetails_temp order by 1";
			ResultSet rssset = theStatement.executeQuery(QS66);

			while (rssset.next())
			{
				VItemName		. add(common.parseNull(rssset.getString(1)));
				VItemCode		. add(common.parseNull(rssset.getString(2)));
				
			}
			rssset.close();
			
			theStatement 		.close();
		//	theConnection		.close();
			
		}
		catch(Exception ex)
			{
			System.out.println("From MaterialIndentDetails_temp "+ex);
			}
		}	
	


	private void setData2()
	{
		VTypeName			= null;
		VTypeCode			= null;
		
		
		
		VTypeName			= new Vector();
		VTypeCode			= new Vector();	
			
		VTypeName			. clear();
		VTypeCode			. clear();
		
		VTypeName			. add("All");
		VTypeCode			. add("999999");
		
		
		try
		{
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
				Statement theStatement = theConnection.createStatement();
			
				String QSS = " Select Code, Name from MaterialIndentReturnType "+ 
							" where ReturnStatus = 1 "+
							" and DisableStatus = 0 "+
							" Order by SortNo, 2 ";
				
			
			
				ResultSet re = theStatement.executeQuery(QSS);

			while (re.next())
			{
				VTypeName		. add(common.parseNull(re.getString(2)));
				VTypeCode		. add(common.parseNull(re.getString(1)));
				
			}
				re.close();
			
				theStatement 		.close();
			
		}
			catch(Exception ex)
			{
				System.out.println("From MaterialIndentDetails "+ex);
			}
		
		
	}


private void setData3()
	{
		VEmpName			= null;
		VEmpCode			= null;
		
		
		
		VEmpName			= new Vector();
		VEmpCode			= new Vector();	
			
		VEmpName			. clear();
		VEmpCode			. clear();
		
	
		
		
		try
		{
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
				Statement theStatement = theConnection.createStatement();
			
				String QSS =  
				" select distinct onetouchemployee.displayname,issuereceiverlist.empcode from issuereceiverlist "+
				" inner join onetouchemployee on onetouchemployee.empcode = issuereceiverlist.empcode "+
				" order by 1 ";
				
			
			
				ResultSet re = theStatement.executeQuery(QSS);

			while (re.next())
			{
				VEmpName		. add(common.parseNull(re.getString(1)));
				VEmpCode		. add(common.parseNull(re.getString(2)));
				
			}
				re.close();
			
				theStatement 		.close();
			
		}
			catch(Exception ex)
			{
				System.out.println("From IssueReceiverList "+ex);
			}
		
		
	}



     public void getUsers(Connection theConnection)
     {
          ResultSet result = null;

          VUDept         = new Vector();  
          VUDeptCode     = new Vector();

          VUnit          = new Vector();
          VUnitCode      = new Vector();

          VCata          = new Vector();
          VCataCode      = new Vector();
          
          try
          {
               
			 if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }
			
			Statement      stat   = theConnection.createStatement();

                String SDeptQS = "";
          if(iMillCode!=1)
               SDeptQS = "Select Dept_Name,Dept_Code From Dept where (MillCode=0 OR MillCode = 2) Order By Dept_Name";
          else
               SDeptQS = "Select Dept_Name,Dept_Code From Dept where (MillCode=1 OR MillCode = 2) Order By Dept_Name";


                      result = stat.executeQuery(SDeptQS);

               while(result.next())
               {
                    VUDept         . addElement(result.getString(1));
                    VUDeptCode     . addElement(result.getString(2));
               }
               result.close();
               
               String QS1 = "";
               String QS2 = "";
               QS1 = " Select Unit_Name,Unit_Code From Unit  Order By Unit_Name"; // Where MillCode=2 or MillCode="+iMillCode+"
               QS2 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               
               result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VUnit     . addElement(result.getString(1));  
                    VUnitCode . addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VCata     . addElement(result.getString(1));
                    VCataCode . addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
			//theConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println("Indent XyX :"+ex);
			ex.printStackTrace();
          }
     }




     private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();

               theModel       = new TestingFrameModel();
               theTable       = new JTable(theModel);
               
               
               TableColumnModel TC  = theTable.getColumnModel();

                for(int i=0;i<theModel.ColumnName.length;i++)   {

                    TC .getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
                }

               TFromDate      = new FinDateField();
               TToDate        = new FinDateField();

               TFromDate      .   setTodayDate();
               TToDate        .   setTodayDate();
			
			
			CDept       = new JComboBox(new Vector(VDeptName));
		
			CUser       = new JComboBox(new Vector(VUserName));
			
			CItem       = new JComboBox(new Vector(VItemName));
			
			CType       = new JComboBox(new Vector(VTypeName));
			
			
			LStatus        = new JLabel("");
			LStatus.setText(" Punch And Return ");
               LStatus.setFont(new Font("courier New",Font.BOLD,19));
               LStatus.setForeground(Color.BLUE);
			
               BExit          = new JButton("Exit");
               BApply         = new JButton("Apply");
               BSave          = new JButton("Save");
			BPunch         = new JButton("Punch");
			BSave.setEnabled(false);
               
               searchField    = new JTextField();
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }
     }

     private void setLayouts()
     {
            setTitle("Material Receipt Against Return ");
            setClosable(true);                    
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,775,600);

            TopPanel.setLayout(new GridLayout(9,2));
            MiddlePanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());

            TopPanel.setBorder(new TitledBorder("Info."));
            MiddlePanel.setBorder(new TitledBorder("Details"));
            BottomPanel.setBorder(new TitledBorder("Controls"));

            TopPanel            . setBackground(new Color(213,234,255));
            MiddlePanel         .setBackground(new Color(213,234,255));
            BottomPanel         . setBackground(new Color(213,234,255));
    }

     private void addComponents()
     {

   /*     TopPanel            . add(new JLabel("FromDate"));
        TopPanel            . add(TFromDate);
         TopPanel            . add(new JLabel("ToDate"));
        TopPanel            . add(TToDate);*/

		TopPanel            . add(new JLabel("Department"));
		TopPanel            . add(CDept);
		TopPanel            . add(new JLabel(""));



		TopPanel            . add(new JLabel("User"));
		TopPanel            . add(CUser);
		TopPanel            . add(new JLabel(""));



		TopPanel            . add(new JLabel("Item"));
		TopPanel            . add(CItem);
		TopPanel            . add(new JLabel(""));


		TopPanel            . add(new JLabel("Return Type"));
		TopPanel            . add(CType);
		TopPanel            . add(new JLabel(""));

		TopPanel            . add(new JLabel(""));
		TopPanel	    		. add(BApply);
		TopPanel            . add(new JLabel(""));


		TopPanel            . add(new JLabel(""));
		TopPanel            . add(new JLabel(""));
		TopPanel            . add(new JLabel(""));

		TopPanel            . add(new JLabel(""));
		TopPanel         . add(BPunch);
		TopPanel         . add(LStatus);

		TopPanel            . add(new JLabel(""));
		TopPanel            . add(new JLabel(""));	        
		TopPanel            . add(new JLabel(""));	        


		TopPanel            . add(new JLabel(""));
		TopPanel            . add(searchField);
		TopPanel            . add(new JLabel(""));	        




	   
        
      

        MiddlePanel.add(new JScrollPane(theTable));

        BottomPanel.add(BExit);
        BottomPanel.add(BSave);

        getContentPane().add("North",TopPanel);
        getContentPane().add("Center",MiddlePanel);
        getContentPane().add("South",BottomPanel);
        BExit.setMnemonic('E');
        BSave.setMnemonic('S');

     }

    private void addListeners()
    {
          BExit.addActionListener(new ActList());
          BApply.addActionListener(new ActList());
          BSave.addActionListener(new ActList());
          searchField. addKeyListener(new SearchKeyList());
		          BPunch    . addActionListener(new ActList());

          //theTable.addKeyListener(new KeyList ());
    }

    public void setTabReport()
    {
          try
          {
			int iUserCode = 0;
			int iDeptCode =0;
			String SItemCode = "";
			
                String SFromDate = common.pureDate((String)TFromDate.toNormal()); 
                String SToDate = common.pureDate((String)TToDate.toNormal()); 
			 
			 String SDept  = common.parseNull((String)CDept.getSelectedItem());
			 if (!SDept.equals("All"))
			  iDeptCode = common.toInt((String)VDeptCode.get(VDeptName.indexOf(SDept)));
			 String SUser  = common.parseNull((String)CUser.getSelectedItem());
			 if (!SUser.equals("All"))
			  iUserCode = common.toInt((String)VUserCode.get(VUserName.indexOf(SUser)));
		   String SItem  = common.parseNull((String)CItem.getSelectedItem());
			 if (!SItem.equals("All"))
			  SItemCode = common.parseNull((String)VItemCode.get(VItemName.indexOf(SItem)));
			String SType  = common.parseNull((String)CType.getSelectedItem());
			 if (!SType.equals("All"))
			  iCode = common.toInt((String)VTypeCode.get(VTypeName.indexOf(SType)));


                getMaterialIndentReturn(SFromDate,SToDate);

                ArrayList ADetails = getMaterialIndent(SFromDate,SToDate,iDeptCode,iUserCode,SUser,SDept,SItem,SItemCode,SType,iCode);

                for(int i=0;i<ADetails.size();i++)
                {
                     HashMap  row =(HashMap)ADetails.get(i);

                     Vector VTheVect = new Vector();
                     
                     String sItemCode = common.parseNull((String)row.get("ITEMCODE"));
                     String sIndentNo = common.parseNull((String)row.get("INDENTNO"));
                     String sUnitCode = common.parseNull((String)row.get("UNIT_CODE"));
                     String sDeptCode= common.parseNull((String)row.get("DEPTCODE"));
                     String sMachCode = common.parseNull((String)row.get("MACHINECODE"));

 				 VTheVect.addElement(String.valueOf(i+1));
				 VTheVect.addElement(common.parseNull((String)row.get("ITEMCODE")));
                     VTheVect.addElement(common.parseNull((String)row.get("ITEMNAME")));
				 VTheVect.addElement(common.parseNull((String)row.get("RETURNTYPE")));
                     VTheVect.addElement(common.parseNull((String)row.get("RETURNQTY")));
                     VTheVect.addElement(common.parseNull((String)row.get("INDENTNO")));
                     VTheVect.addElement(common.parseNull((String)row.get("UNITNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("DEPTNAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("MACHINENAME")));
                     VTheVect.addElement(common.parseNull((String)row.get("USERNAME")));
                     VTheVect.addElement(common.parseNull(common.parseDate((String)row.get("ENTRYDATE"))));
                     VTheVect.addElement(String.valueOf(getRetAlreadyReturned(sItemCode,sIndentNo,sUnitCode,sDeptCode,sMachCode)));
                     VTheVect.addElement(common.parseNull((String)row.get("DELAYDAYS")));
                     VTheVect.addElement("");

                     theModel.appendRow(VTheVect);
               }
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
		/*	if(ae.getSource()==BSave)
			{
				moveCursorToOtherCell(0, 0);
				
				if(validations())
				{  
					if(JOptionPane.showConfirmDialog(null, "Are you sure you want to Save the Details?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
					{
						insertReturn();
					}
				}
			}*/
			
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }

               if(ae.getSource()==BApply)
               {
                    theModel.setNumRows(0);
                    setTabReport();
               }
			
			  if(ae.getSource()==BPunch)
               {
                     //  if(checkControlTime(iMillCode))
                     //  {
                      //      JOptionPane.showMessageDialog(null," Cannot Issue Material Time Expired " );
                      //      return;
                      // }

				start();
				init();
				BPunch.setEnabled(false);
				iPunchStatus=1;
			//	stop();

					
			}
			
			
          }
     }
     private class KeyList extends KeyAdapter  {

          public void keyPressed(KeyEvent ke)
          {
               int iRow = theTable.getSelectedRow       ();
               int iCol = theTable.getSelectedColumn    ();

               System.out.println("keyPressed");

               if( iCol==13) {

			checkValue(iRow);
               }
           }

          public void keyReleased(KeyEvent ke)   {
             try   {

            int    iRow    = theTable.getSelectedRow();
            int    iCol    = theTable.getSelectedColumn();
            
            System.out.println("keyReleased");

            if( iCol==13) {              
			checkValue(iRow);
            }

         }catch(Exception e)  {
   
            e.printStackTrace();
         }
      }
   }
     private class SearchKeyList extends KeyAdapter
        {
            public void keyReleased(KeyEvent ke)
            {
                searchItems();     
            }

            public void keyPressed(KeyEvent ke)
            {
                searchItems();
            }    

            public void keyTyped(KeyEvent ke)
            {
                searchItems();
            }
        }
	   
	private void moveCursorToOtherCell(int iRow, int iCol) {
		theTable       . setRowSelectionInterval(iRow, iCol);
		Rectangle cell = theTable.getCellRect(iRow, 0, true);
		theTable       . scrollRectToVisible(cell);
		
		theTable		. editCellAt(iRow, iCol);
	}

    private void showMessage(String sMsg)
    {
          JOptionPane.showMessageDialog(null,getMessage(sMsg),"Dear User",JOptionPane.INFORMATION_MESSAGE);
    }
    private String getMessage(String sMsg)
    {
          String str = "<html><body>";
          str = str + sMsg;
          str = str + "</body></html>";
          return str;
    }
	
    private void removeHelpFrame()
    {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
	
	
	 public boolean checkControlTime(int iTypeCode)
     {
          int iValue =0;
          boolean bFlag = false;

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(getControlTimeQS(iTypeCode));

               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result.close();
               theStatement.close();

               if(iValue<0)
                  return true;

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               return true;
          }

          return false;
     }
	
	 private String getControlTimeQS(int iTypeCode)
     {

          String QS=" Select (to_date(controlTime,'hh24:mi')-(to_date(to_Char(sysdate,'hh24:mi'),'hh24:mi')))*(24*60) as Diff from"+
                    " TimeControl"+
                    " Where TypeCode="+iTypeCode;


          return QS;
     }

	
     public ArrayList getMaterialIndent(String SFromDate, String SToDate, int iDeptCode,int iUserCode,String SUser,String SDept, String SItem,String SItemCode,String SType,int iCode)
     {
            theList = new ArrayList();
            StringBuffer sb = new StringBuffer();

						String QS92=		
						" Select distinct MaterialIndentDetails_temp.ItemCode,MaterialIndentDetails_temp.ItemName,  "+
					" materialindentreturntype.name as returntype,materialreturndetails_temp.ReturnQty as returnqty,MaterialIndentDetails_temp.IndentNo,   "+
					" process_units.UnitName,hrdnew.department.DeptName,nvl(Machine.Mach_Name,'General')  as MACHINENAME,Rawuser.UserName,    "+
					" to_char(MaterialIndentDetails_temp.EntryDate,'yyyymmdd') as EntryDate,MaterialIndentDetails_temp.Unit_code,    "+
					" MaterialIndentDetails_temp.DeptCode,MaterialIndentDetails_temp.MachineCode,Round(SysDate - MaterialIndentDetails_temp.EntryDate) as DelayDays,  "+
					" MaterialIndentDetails_temp.RETURNQTY as rQty, MaterialIndentDetails_temp.Id,Materialindentdetails_temp.requiredqty  "+
					" From MaterialIndentDetails_temp    "+
					" Left Join Machine On Machine.Mach_Code = MaterialIndentDetails_temp.MachineCode   "+  
					" Inner Join process_units On process_units.UnitCode = MaterialIndentDetails_temp.Unit_Code   "+ 
					" Inner Join hrdnew.department On hrdnew.department.DeptCode = MaterialIndentDetails_temp.DeptCode    "+
					" Inner join Scm.Rawuser on Rawuser.usercode = MaterialIndentDetails_temp.usercode   "+
					" inner join materialreturndetails_temp on materialreturndetails_temp.itemcode = MaterialIndentDetails_temp.itemcode  "+
					" and MaterialIndentDetails_temp.IndentNo = materialreturndetails_temp.IndentNo  "+
					" and materialreturndetails_temp.RequiredQty = MaterialIndentDetails_temp.RequiredQty  "+
					" inner join materialindentreturntype on materialindentreturntype.code = materialreturndetails_temp.returntypecode  "+
					" Where to_char(MaterialIndentDetails_temp.EntryDate,'yyyymmdd') <=to_char(sysdate,'yyyymmdd')   "+
					" and MaterialIndentDetails_temp.RETURNSTATUS=1  "+
					" and MaterialIndentDetails_temp.STORERECEIPTSTATUS=0 ";
					if (!SDept.equals("All"))
					QS92 = QS92 +" and MaterialIndentDetails_temp.DeptCode="+iDeptCode+"  ";
					if (!SUser.equals("All"))
					QS92 = QS92 +" and MaterialIndentDetails_temp.UserCode="+iUserCode+"  ";
					if (!SItem.equals("All"))
					QS92 = QS92 +" and MaterialIndentDetails_temp.ItemCode='"+SItemCode+"'  ";
					if (!SType.equals("All"))
					QS92 = QS92 +" and Materialindentreturntype.Code="+iCode+"  ";
					QS92 = QS92 +" Order by hrdnew.department.DeptName  ";  

		System.out.println(sb.toString());

            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(QS92);
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theList;
     }
     public ArrayList getMaterialIndentReturn(String SFromDate, String SToDate)
     {
            theReturnList = new ArrayList();

            StringBuffer sb = new StringBuffer();

            sb.append(" select ITEMCODE,INDENTNO,UNIT_CODE,DEPTCODE,MACHINECODE,REQUIREDQTY,RETURNQTY ");
            sb.append(" From MaterialReturnDetails_temp ");
            sb.append(" Where nvl(RETURNQTY,0)<REQUIREDQTY ");
            sb.append(" and to_char(MaterialReturnDetails_temp.EntryDate,'yyyymmdd') <="+SToDate+" and ReturnType=1 ");
		  

            try
            {
                  if(theConnection ==null) {

                      ORAConnection jdbc   = ORAConnection.getORAConnection();
                      theConnection        = jdbc.getConnection();
                  }

                 Statement theStatement     = theConnection.createStatement();
                 ResultSet  result          = theStatement.executeQuery(sb.toString());
                 ResultSetMetaData rsmd     = result.getMetaData();

                 while(result.next())
                 {
                      HashMap row = new HashMap();
                      for(int i=0;i<rsmd.getColumnCount();i++)
                      {
                           row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                      }
                      theReturnList.add(row);
                 }
                  result.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
         return theReturnList;
     }
     private void checkValue(int iRow){


         double dalreadyRet = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,11)));

         double dretQty = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,13)));
         
         double dReqQty = common.toDouble(common.parseNull((String)theModel.getValueAt(iRow,4)));
         
         double dNeedToRetrun = dReqQty-dalreadyRet;

         if(dretQty>dNeedToRetrun){

              showMessage("Return Quantiy is Must be equal or Less Than Required Qty");
          }

    }
     private void insertReturn()
	{
         int iError = 0;
         int icheck = 0;
         StringBuffer sb = new StringBuffer();
         StringBuffer sbupdate = new StringBuffer();
         PreparedStatement preparedStat = null;
         PreparedStatement preparedSt= null;
         java.util.HashMap theMap = new java.util.HashMap();
	    String iip = "";
	    
	    String SType  = common.parseNull((String)CType.getSelectedItem());
	    iCode = common.toInt((String)VTypeCode.get(VTypeName.indexOf(SType)));
	    
	    String SEmpName =  common.parseNull((String)LStatus.getText());
	    int iEmpCode = common.toInt((String)VEmpCode.get(VEmpName.indexOf(SEmpName)));

		try
		{
		   
			iip = InetAddress.getLocalHost().getHostAddress(); 
		   
			sb.append(" Insert into MaterialReturnDetails_temp ( ID,ITEMCODE,ITEMNAME,REQUIREDQTY,INDENTNO,UNIT_CODE,DEPTCODE,MACHINECODE,USERCODE,ENTRYDATE,RETURNQTY,MACHINENAME,RETURNTYPE,OWNERCODE,STORERECEIPTDATE,SYSTEMNAME,MACHINETABLECODE,RETURNTYPECODE,RETURNEMPCODE ) ");
			sb.append(" Values(MaterialReturnDetailstemp_Seq.nextVal,?,?,?,?,?,?,?,?,sysdate,?,?,1,?,sysdate,'"+iip+"',0,?,"+iEmpCode+") ");
            
			sbupdate.append(" Update MaterialIndentDetails_temp set STORERECEIPTSTATUS=1 , StoreReceiptDate = SysDate  ") ;  
			sbupdate.append(" where ITEMCODE=? and INDENTNO=? and UNIT_CODE=? and DEPTCODE=? and MACHINECODE=?  ") ;

			if(theConnection ==null) 
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
		  
			theConnection			. setAutoCommit(false);
		  
			preparedStat = theConnection.prepareStatement(sb.toString());
			preparedSt = theConnection.prepareStatement(sbupdate.toString());
            
			for (int index = 0; index < theModel.getRows(); index++) 
			{
				theMap = (java.util.HashMap) theList.get(index);
				
				String SITEMCODE  = common.parseNull((String)theModel.getValueAt(index, 1));
				int iINDENTNO  = common.toInt((String)theModel.getValueAt(index, 5));

				double dretQty = common.toDouble(common.parseNull((String)theModel.getValueAt(index,13)));
				
				  SType  = common.parseNull((String)theModel.getValueAt(index, 3));
				iCode = common.toInt((String)VTypeCode.get(VTypeName.indexOf(SType)));
				
		/*	  ArrayList ADetails = getMaterialIndent(SFromDate,SToDate,iDeptCode,iUserCode,SUser,SDept,SItem,SItemCode);
			  
				for(int i=0;i<ADetails.size();i++)
				{
					HashMap  row =(HashMap)ADetails.get(i);
					String SMachineCode = common.parseNull((String)row.get("MACHINECODE"));
				}*/
					if(dretQty > 0)
					{
						double dReqQty = 0;
					
						StringBuffer sb11	= null;
						sb11				= new StringBuffer();

					sb11.append(" Select Sum(ReturnQty) from MaterialReturnDetails_temp where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' "); 
					sb11.append(" and ReturnFromIndent = 1 ");

					PreparedStatement thePS	= theConnection.prepareStatement(sb11.toString());
					ResultSet	result	 	= thePS.executeQuery();
			
					while(result.next())
					{ 
						dReqQty 			= common.toDouble(result.getString(1));
					}
					
					
					thePS                    . close();
					result				. close();
					
					double dalreadyRet = 0;
					
					StringBuffer sb1	= null;
					sb1				= new StringBuffer();
					
					sb1.append(" Select Sum(ReturnQty) from MaterialReturnDetails_temp where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' ");
					sb1.append(" and ReturnFromIndent = 0 ");
					sb1.append(" and ReturnType = 1  ");

					thePS				= theConnection.prepareStatement(sb1.toString());
					result	 			= thePS.executeQuery();
			
					while(result.next())
					{ 
						dalreadyRet		= common.toDouble(result.getString(1));
					}
					
					preparedStat.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
					preparedStat.setString(2,common.parseNull((String) theMap.get("ITEMNAME")));
					preparedStat.setString(3,common.parseNull((String) theMap.get("REQUIREDQTY")));
					preparedStat.setString(4,common.parseNull((String) theMap.get("INDENTNO")));
					preparedStat.setString(5,common.parseNull((String) theMap.get("UNIT_CODE")));
					preparedStat.setString(6,common.parseNull((String) theMap.get("DEPTCODE")));
					preparedStat.setString(7,common.parseNull((String) theMap.get("MACHINECODE")));
					preparedStat.setInt(8,iUserCode);
					preparedStat.setString(9,common.parseNull((String)theModel.getValueAt(index,12)));
					preparedStat.setString(10,common.parseNull((String) theMap.get("MACHINENAME")));
					preparedStat.setInt(11,iOwnerCode);
					preparedStat.setInt(12,iCode);
					preparedStat.executeUpdate();

					double dtotal = (dalreadyRet+dretQty);

					System.out.println("dalreadyRet=="+dalreadyRet);
					System.out.println("dReciptQty=="+dretQty);

					System.out.println("dtotal=="+dtotal);
					
					if(dtotal == dReqQty)
					{
						preparedSt.setString(1,common.parseNull((String) theMap.get("ITEMCODE")));
						preparedSt.setString(2,common.parseNull((String) theMap.get("INDENTNO")));
						preparedSt.setString(3,common.parseNull((String) theMap.get("UNIT_CODE")));
						preparedSt.setString(4,common.parseNull((String) theMap.get("DEPTCODE")));
						preparedSt.setString(5,common.parseNull((String) theMap.get("MACHINECODE")));
					//	preparedSt.setString(6,common.parseNull((String) theMap.get("ID")));
						preparedSt.executeUpdate();
					}
				}
			}
			
			preparedSt.close();
			preparedStat.close();

			theConnection			. commit();
			theConnection			. setAutoCommit(true);
			
			showMessage("Return Quantity Updated..");
			theModel.setNumRows(0);
			setTabReport();
		}
		catch(Exception e)
		{
			try
			{
				theConnection		. rollback();
				theConnection		. setAutoCommit(true);
			}
			catch(Exception ex){}
			iError = 1;
			e.printStackTrace();
			
			showMessage("Return Quantity Not Updated..");
		}
     }

     private double getRetAlreadyReturned(String sItemCode,String sIndnetNo,String sUnitCode,String sDeptCode,String sMachCode){

         double dalreadyRet = 0;
         java.util.HashMap theMap = new java.util.HashMap();
         
         for (int index = 0; index < theReturnList.size(); index++) {
             
             theMap = (java.util.HashMap) theReturnList.get(index);
             
            String sItem =  common.parseNull((String) theMap.get("ITEMCODE"));
            String sIndnent =  common.parseNull((String) theMap.get("INDENTNO"));
            String sUnit =  common.parseNull((String) theMap.get("UNIT_CODE"));
            String sDept =  common.parseNull((String) theMap.get("DEPTCODE"));
            String sMach =  common.parseNull((String) theMap.get("MACHINECODE"));
            
            if(sItemCode.equals(sItem) && sIndnetNo.equals(sIndnent) && sUnitCode.equals(sUnit) && sDeptCode.equals(sDept) && sMachCode.equals(sMach)){

                    dalreadyRet = common.toDouble(common.parseNull((String) theMap.get("RETURNQTY")));

                    //break;
            }
        }
         return dalreadyRet;
     }
     private void searchItems()
     {
          String SSearchStr        = common.parseNull(searchField.getText()).trim();

          int iIndex               = -1;

          for(int i=0; i<theModel.getRowCount(); i++)
          {
               String SItemName    = common.parseNull((String)theModel.getValueAt(i, 2));

               if(SItemName.startsWith(SSearchStr.toUpperCase()))
               {
                    iIndex         = i;
                    break;
               }
          }
          if(iIndex != -1)
          {
                 theTable       . setRowSelectionInterval(iIndex, iIndex);
                 Rectangle theRect   = theTable.getCellRect(iIndex, 1, true);
                 theTable       . scrollRectToVisible(theRect);
                 theTable       . setSelectionBackground(Color.yellow);			   
          }
     }


	private boolean validations()
	{
		try
		{
			if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}

			SName = "";
			SUName = "";
			
			for(int i=0; i<theModel.getRowCount(); i++)
			{
				int iRECEIPTQTY  = common.toInt((String)theModel.getValueAt(i, 13));			
				String SITEMCODE  = common.parseNull((String)theModel.getValueAt(i, 1));
				int iINDENTNO  = common.toInt((String)theModel.getValueAt(i, 5));
				double dretQty = common.toDouble(common.parseNull((String)theModel.getValueAt(i,13)));
		
			 
			 
                    if(dretQty>0)
				{
					double dReturnQty	= 0, dRecdQty = 0;
					
					StringBuffer sb	= null;
					sb				= new StringBuffer();

					sb.append(" Select Sum(ReturnQty) from MaterialReturnDetails_temp where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' "); 
					sb.append(" and ReturnFromIndent = 1    ");

					PreparedStatement thePS	= theConnection.prepareStatement(sb.toString());
					ResultSet	result	 	= thePS.executeQuery();
			
					while(result.next())
					{ 
						dReturnQty 		= common.toDouble(result.getString(1));
					}
					
					thePS                    . close();
					result				. close();
					
					StringBuffer sb1	= null;
					sb1				= new StringBuffer();
					
					sb1.append(" Select Sum(ReturnQty) from MaterialReturnDetails_temp where IndentNo = "+iINDENTNO+" and ItemCode = '"+SITEMCODE+"' ");
					sb1.append(" and ReturnFromIndent = 0 ");
					sb1.append(" and ReturnType = 1 ");

					thePS				= theConnection.prepareStatement(sb1.toString());
					result	 			= thePS.executeQuery();
			
					while(result.next())
					{ 
						dRecdQty 			= common.toDouble(result.getString(1));
					}
					
					dRecdQty				= dRecdQty + dretQty;
					
					thePS                    . close();
					result				. close();

					if(dRecdQty > dReturnQty)
					{
						JOptionPane.showMessageDialog(null, "Return Quantity is Must be equal or Less Than Indent Return Qty.. Check Row No. "+(i+1), "Error!", JOptionPane.ERROR_MESSAGE);
						return false;
					}
					
				
				}
			}
			
			return true;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			
			JOptionPane.showMessageDialog(null, "Problem while Checking Data..", "Error!", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}

 protected void init()        
    {

		capturer.addDataListener(new DPFPDataAdapter() {
                public void dataAcquired(final DPFPDataEvent e) {
                    LStatus.setText(" Please wait Under processing.................");

				SwingUtilities.invokeLater(new Runnable()
                                {
                                        public void run()
                                        {
                                                LStatus.setText("The fingerprint sample was captured....");
                                                process(e.getSample());
                                        }});
			}
		});
		capturer.addReaderStatusListener(new DPFPReaderStatusAdapter() {
                public void readerConnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
		 			makeReport("The fingerprint reader was connected.");
				}});
			}
                public void readerDisconnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was disconnected.");
				}});
			}
		});
		capturer.addSensorListener(new DPFPSensorAdapter() {
                public void fingerTouched(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was touched.");
				}});
			}
                public void fingerGone(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The finger was removed from the fingerprint reader.");
				}});
			}
		});
		capturer.addImageQualityListener(new DPFPImageQualityAdapter() {
                public void onImageQuality(final DPFPImageQualityEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					if (e.getFeedback().equals(DPFPCaptureFeedback.CAPTURE_FEEDBACK_GOOD))
						makeReport("The quality of the fingerprint sample is good.");
					else
						makeReport("The quality of the fingerprint sample is poor.");
				}});
			}
		});
	}


	 private String getDeptCode(String SDept)
     {
          int iIndex = VUDept.indexOf(SDept);

          return (String)VUDeptCode.elementAt(iIndex);
     }
     private String getUnitCode(String SDept)
     {
          int iIndex = VUnit.indexOf(SDept);

          return (String)VUnitCode.elementAt(iIndex);
     }

	
	public boolean   checkEligiblePerson(String SEmpCode)
     {
          int iValue =0;
          boolean bFlag = false;


          String QS ="";


          if(iMillCode==1)
          {
               QS= " select count(1) from DyeingIssueReceiverList Where UserCode in(select usercode from Materialindentdetails where userCode="+SAuthUserCode+")"+
                           " and DyeingIssueReceiverList.EmpCode="+SEmpCode;


          }

          else
          {
               QS= " select count(1) from IssueReceiverList Where UserCode in(select usercode from Materialindentdetails where userCode="+SAuthUserCode+")"+
                           " and IssueReceiverList.EmpCode="+SEmpCode;

          }



          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);

               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result.close();
               theStatement.close();


               if(iValue>0)
                  bFlag=true;

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               return bFlag;
          }

                      
          return bFlag;
     }

  /*   private String getControlTimeQS(int iTypeCode)
     {

          String QS=" Select (to_date(controlTime,'hh24:mi')-(to_date(to_Char(sysdate,'hh24:mi'),'hh24:mi')))*(24*60) as Diff from"+
                    " TimeControl"+
                    " Where TypeCode="+iTypeCode;


          return QS;
     }*/



	protected void process(DPFPSample sample)
	{
		// Draw fingerprint sample image.
		
		DPFPFeatureSet features = extractFeatures(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

		// Check quality of the sample and start verification if it's good
		int iStatus=0;
   	    String SName  = "";
		String SCode  = "";
          stop();
		//start();
          System.out.println("here Process Status Comming "+iProcessStatus);

          if(iProcessStatus==1)
               return;

          System.out.println("There Process Status Comming "+iProcessStatus);


          LStatus.setText(" Please Hold On.................");
          iProcessStatus=1;


		if (features != null)
		{
			// Compare the feature set with our template



               for(int i=0; i<theMaterialIssueTemplateList.size(); i++)
               {
                    HashMap theMap = (HashMap) theMaterialIssueTemplateList.get(i);

                    SCode    = (String)theMap.get("EMPCODE");
                    SName    = (String)theMap.get("DISPLAYNAME");
				SUName   = (String)theMap.get("USERNAME");
                    
                    DPFPTemplate template   = (DPFPTemplate)theMap.get("TEMPLATE");

                    DPFPVerification matcher= DPFPGlobal.getVerificationFactory().createVerification();
                    matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);

                    DPFPVerificationResult result = matcher.verify(features,template);

                    if (result.isVerified())
                    {
                         LStatus.setText(""+SName);       
                         iStatus=1;
                         break;
                    }

               }

			if(iStatus==0)
			{
                         LStatus.setText("No Match Found");       
                         JOptionPane.showMessageDialog(null," No Match Found Punch Again");
                         stop();
                         iPunchStatus=0;
                         BPunch.setEnabled(true);
                         iProcessStatus=0;
			}
			else
			{
                   /*   if(!checkEligiblePerson(SCode))
                       {
                            JOptionPane.showMessageDialog(null," Person Not Elgible To return this Material"+SCode);
                            iProcessStatus=0;

                            return;
                       }



                       if(setData22())
                       {
                            JOptionPane.showMessageDialog(null," Item Returned Sucessfully");
                        //    setIssueModel();
                            removeHelpFrame();
                        //    Validations();
					   insertReturn();
                            LStatus.setText(" Punch And Return");
                            stop();
                            iPunchStatus=0;
                            BPunch.setEnabled(true);
                            iProcessStatus=0;

                       }
                       else
                       {
        
                            JOptionPane.showMessageDialog(null," Problem in Storing Data");
                            stop();
                            iPunchStatus=0;
                            BPunch.setEnabled(true);
                            iProcessStatus=0;

                       }*/
				   
				
				
				LStatus.setText(""+SName);
				
					
				if(validations())
				{ 
					if(JOptionPane.showConfirmDialog(null, "Are you sure you want to Save the Details?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
					{                      
					//	insertReturn();
					}
					stop();
					//LStatus.setText("");	
				//	LStatus.setText(""+SName);
					LStatus.setText("Punch to Return");
					iPunchStatus=0;
					BApply.setEnabled(true);
					BPunch.setEnabled(true);
					iProcessStatus=0;
					
				}
					
               }
          }
	}
	
	protected void start()
	{
		capturer.startCapture();
	}
	
	protected void stop()
	{
		capturer.stopCapture();
	}

	public void setPrompt(String string) {
	}
	public void makeReport(String string) {
	}
	
	protected Image convertSampleToBitmap(DPFPSample sample) {
		return DPFPGlobal.getSampleConversionFactory().createImage(sample);
	}

	protected DPFPFeatureSet extractFeatures(DPFPSample sample, DPFPDataPurpose purpose)
	{
		DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
		try 
		{
			return extractor.createFeatureSet(sample, purpose);
		} 
		catch (DPFPImageQualityException e)
		{
			System.out.println(" Returning Null");

			return null;
		}
	}
	private void updateStatus(int FAR)
	{
          System.out.println(FAR);
	}


  public void onLoad()
     {
            try
            {

					theMaterialIssueTemplateList = new ArrayList();

				

                if(theConnection == null)
			{
				ORAConnection jdbc   = ORAConnection.getORAConnection();
				theConnection        = jdbc.getConnection();
			}
			
			 for(int i=0; i<theMaterialIssueTemplateList.size(); i++)
               {
                    HashMap theMap = (HashMap) theMaterialIssueTemplateList.get(i);

                    SCode    = (String)theMap.get("EMPCODE");
                    SName    = (String)theMap.get("DISPLAYNAME");
				SUName   = (String)theMap.get("USERNAME");
				
			}
			
			String sqlStmt=	" select Onetouchemployee.empcode,onetouchemployee.displayname,onetouchemployee.fingerprint,rawuser.username,onetouchemployee.materialissuestatus from issuereceiverlist "+
			" inner join rawuser on rawuser.usercode = issuereceiverlist.usercode "+
			" inner join onetouchemployee on onetouchemployee.empcode = issuereceiverlist.empcode ";
	
		System.out.println("SQLSTMT=====>"+sqlStmt);
				
				
                Statement st=theConnection.createStatement();
                ResultSet rs = st.executeQuery(sqlStmt);
                while(rs.next())
                {
                      byte[] data =null ;
                      data  = getOracleBlob(rs,"FINGERPRINT");
                      DPFPTemplate t = DPFPGlobal.getTemplateFactory().createTemplate();
                      t.deserialize(data);
                      HashMap theMap = new HashMap();
                    
                      theMap.put("EMPCODE",rs.getString(1));
                      theMap.put("DISPLAYNAME",rs.getString(2));
                      theMap.put("TEMPLATE",t);
				  theMap.put("USERNAME" ,rs.getString(4));
					  
					  theMaterialIssueTemplateList.add(theMap);


                }
				rs.close();
				st.close();
			
			} catch (Exception ex) {

                    System.out.println(ex);
                    ex.printStackTrace();
			}
    } 

    private byte[] getOracleBlob(ResultSet result, String columnName) throws SQLException
    {   
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        byte[] bytes = null;
        
        try {    
            oracle.sql.BLOB blob = ((oracle.jdbc.OracleResultSet)result).getBLOB(columnName);        
            inputStream = blob.getBinaryStream();        
            int bytesRead = 0;        
            
            while((bytesRead = inputStream.read()) != -1) {        
                outputStream.write(bytesRead);    
            }
            
            bytes = outputStream.toByteArray();
            
        } catch(IOException e) {
            throw new SQLException(e.getMessage());
        } finally 
        {
        }
    
        return bytes;
    }

}
