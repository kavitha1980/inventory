package Indent.IndentFiles;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import com.digitalpersona.onetouch.*;
import com.digitalpersona.onetouch.capture.*;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.*;
import com.digitalpersona.onetouch.verification.*;

public class RDCIssueFrame extends JInternalFrame
{
     Connection   theConnection  =    null;
     String       SDate;
     JPanel       TopPanel,BottomPanel,MiddlePanel;
     MyLabel      LStatus;
     
     boolean bComflag = true;
     
     Common common    = new Common();

     JTable          theTable;
     JButton         BRefresh,BPunch;
     RDCListModel    theModel;
     
     JLayeredPane    Layer;
     StatusPanel     SPanel;
     Vector          VCode,VName,VUomName;
     int             iUserCode,iMillCode,iAuthCode;

     String          SItemTable,SSupTable,SYearCode,SSystemName;
     Vector          VAuthUserCode,VRDCSlNo,VRDCUserCode;
     int             iMemoNo,iMemoDate;

    Vector          VGINo,VGIDate,VRecQty,VReceivedBy;
    ArrayList theTemplateList,theMaterialIssueTemplateList;
  
    
    private DPFPCapture capturer = DPFPGlobal.getCaptureFactory().createCapture();

    private DPFPVerification verificator = DPFPGlobal.getVerificationFactory().createVerification();

    DPFPTemplate template;
    
    
     private JTextField prompt = new JTextField();
     private JTextArea  log = new JTextArea();
     private JTextField status = new JTextField("[status line]");
     int iPunchStatus=0;
    //set Model Index
    int iSNo=0,RDCNo=1,RDCDate=2,UserName=3,Unit=4;
    int SupplierName=5,Description=6,RDCQty=7,ReceiveQty=8,DeptReceivedQty= 9,DeptReceiveQty=10,Balance=11,Select=12,GINo=13,GIDate=14,Dept=15;
     public RDCIssueFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUomName,StatusPanel SPanel,int iUserCode,int iMillCode,String SItemTable,String SSupTable,String SYearCode,ArrayList theTemplateList,ArrayList theMaterialIssueTemplateList)
     {
          super("Receive Authentication Utility");
          this.Layer     = Layer;
          this.VCode     = VCode;
          this.VName     = VName;
          this.VUomName  = VUomName;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.iAuthCode = iAuthCode;
          this.SItemTable= SItemTable;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;

          this.theTemplateList                = theTemplateList;
          this.theMaterialIssueTemplateList = theMaterialIssueTemplateList;
          SSystemName = common.getLocalHostName();
          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          setData();
     }

     public void getDBConnection()
     {
          try
          {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnection  =    oraConnection.getConnection();
                              theConnection  .    setAutoCommit(false);

          }catch(SQLException SQLE)
          {
               System.out.println("RDCReceiveFrame getDBConnections SQLException"+ SQLE);
               SQLE.printStackTrace();
          }
     }

     public void createComponents()
     {
        
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          MiddlePanel    = new JPanel();

          theModel       = new RDCListModel();
          theTable       = new JTable(theModel);


          BRefresh       = new JButton("Refresh ");
          BPunch         = new JButton("Punch");
          
          LStatus        = new MyLabel();
          
          SDate = common.getServerPureDate();
     }
     
     public void setLayouts()
     {
          TopPanel       .setLayout(new FlowLayout());
          BottomPanel    .setLayout(new FlowLayout());
          MiddlePanel    .setLayout(new BorderLayout());

          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,1050,500);

     }
     
     public void addComponents()
     {
          MiddlePanel.add(new JScrollPane(theTable));
          BottomPanel.add(BRefresh);
          BottomPanel.add(BPunch);
          BottomPanel.add(LStatus);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add("South",BottomPanel);
          this . add(MiddlePanel,BorderLayout.CENTER);
          this . add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          theTable.addKeyListener(new KeyList(this));
          BRefresh.addActionListener(new ActList());
          BPunch  .addActionListener(new ActList());
     }

    private class KeyList extends KeyAdapter
     {
          RDCIssueFrame rdcreceiveframe;
          public KeyList(RDCIssueFrame rdcreceiveframe)
          {
               this.rdcreceiveframe = rdcreceiveframe;
          }

          public void keyReleased(KeyEvent ke)
          {
                          
               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    int iRow = theTable.getSelectedRow();

                    int     iRDCNo       = common.toInt((String)theModel.getValueAt(iRow,RDCNo));
                    String  SRDCDate     = (String)theModel.getValueAt(iRow,RDCDate);
                    String  SDescription = (String)theModel.getValueAt(iRow,Description);
                    String  SDept       = (String)theModel.getValueAt(iRow,UserName);
                    String  SUnit       = (String)theModel.getValueAt(iRow,Unit);
                    int     iGINo       =  common.toInt((String)theModel.getValueAt(iRow,GINo));
                    String  SGIDate     = (String)theModel.getValueAt(iRow,GIDate);
                    String  SDepartment       = (String)theModel.getValueAt(iRow,Dept);
                    getPreviousGateInwards(iRDCNo,SDescription,iGINo);
                    GateInwardFrame theFrame =new GateInwardFrame(iRDCNo,SDescription,VGINo,VGIDate,VRecQty,VReceivedBy);
                    theFrame .setVisible(true);
               }
          }
     }


     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
               Layer.repaint();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
     public void setData()
     {
        VRDCSlNo = new Vector();
        theModel.setNumRows(0);
        theTable.getColumn("S.No").setPreferredWidth(20);
        theTable.getColumn("SupplierName").setPreferredWidth(150);
        theTable.getColumn("Description").setPreferredWidth(250);
        VAuthUserCode = new Vector();
        VRDCUserCode  = new Vector();
          try
          {
               Connection theConnection=null;

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }	

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(getRDCQuery());
               int SlNo=1;

               while(result.next())
               {
                    Vector theVect = new Vector();

                    String SRDCNo       = common.parseNull(result.getString(1));
                    String SRDCDate     = common.parseDate(result.getString(2));
                   
                    String SSupplierName= common.parseNull(result.getString(3));
                    String SDescription = common.parseNull(result.getString(4));
                    double dRDCQty        = result.getDouble(5);
                    double dGIReceiveQty  = result.getDouble(6);
                    
                    VAuthUserCode.addElement(result.getString(7));

                    String SDept          = common.parseNull(result.getString(8));
                    String SUnit          = common.parseNull(result.getString(9));
                    double dDeptReceivedQty = result.getDouble(10);
                    String SGINo        = common.parseNull(result.getString(11));
                    String SGIDate      = common.parseDate(result.getString(12));
                    String SRDCSlNo     = result.getString(13);
                    VRDCUserCode        . addElement(result.getString(14));
                    String SDepartment          = common.parseNull(result.getString(15));  
                    VRDCSlNo . addElement(SRDCSlNo);
                    //double dBalance       = dRDCQty - dDeptReceivedQty;       
                     double dBalance       = dGIReceiveQty - dDeptReceivedQty;       

                    
                        theVect.addElement(String.valueOf(SlNo));
                        theVect.addElement(SRDCNo);
                        theVect.addElement(SRDCDate);
                        theVect.addElement(common.parseNull(SDept));
                        theVect.addElement(SUnit);
                        theVect.addElement(SSupplierName);
                        theVect.addElement(SDescription);
                        theVect.addElement(String.valueOf(dRDCQty));
                        theVect.addElement(String.valueOf(dGIReceiveQty));
                        theVect.addElement(String.valueOf(dDeptReceivedQty));
                        theVect.addElement("");
                        theVect.addElement(common.getRound(dBalance,3));
                        theVect.addElement(new Boolean(false));
                        theVect.addElement(SGINo);
                        theVect.addElement(SGIDate);
                        theVect.addElement(SDepartment);

                        theModel.appendRow(theVect);
                        
                          SlNo++;
                                    
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private String getRDCQuery()
     {
          
         StringBuffer sb = new StringBuffer();
      /*  sb.append(" Select RDCNo,RDCDate,SupplierName,Descript,RDCQty,RecQty, MemoAuthUserCode,UserName,Unit_Name,DeptRecQty,GiNo,GIDate,RDCSlNo,UserCode from( ");
        sb.append(" Select RDCNo,RDCDate,SupplierName,Descript,RDCQty,RecQty, MemoAuthUserCode, UserName,Unit_Name,sum(DeptRecQty) as DeptRecQty,GINo,GIDate,RDCSlNo,UserCode ");
        sb.append(" From(Select   RDC.RDCNo, RDC.RDCDate,Supplier.Name as SupplierName, RDC.Descript,");
        sb.append(" RDC.Qty as RDCQty,GateInwardRDC.RecQty as RecQty,RDC.MemoAuthUserCode,RawUser.UserName,");
        sb.append(" Unit.Unit_Name, sum(decode(RDCReceiver.ReceivedQty,null,0,RDCReceiver.ReceivedQty)) as DeptRecQty,");
        sb.append(" RDCReceiver.DateAndTime,GateInwardRDC.GINo,GateInwardRDC.GIDate,GateInwardRDC.RDCSlNo as RDCSlNo,RawUser.UserCode  from RDC  ");
        sb.append(" Inner Join GateInwardRDC  on trim(RDC.Descript) = trim(GateInwardRDC.Descript)  and            GateInwardRDC.RDCNo = RDC.RDCNo and  ");
        sb.append(" GateInWardRDC.MillCode = RDC.MillCode and RDC.DocType=0  and  ");
        sb.append(" RDC.MemoAuthUserCode = GateInwardRDC.MemoAuthUserCode  and RDC.SlNo=GateInwardRDC.RDCSlNo");
        sb.append(" Inner Join Supplier on GateInwardRDC.Sup_Code=Supplier.AC_Code   ");
        sb.append(" Inner join Unit on unit.Unit_Code=RDC.Unit_Code  ");
        sb.append(" Inner join RDCMemo on RDCMemo.RDCNo = RDC.RDCNo and RDCMemo.MillCode=RDC.Millcode   ");
        sb.append(" Inner join RawUser on RawUser.UserCode=RDCMemo.AuthUserCode ");
        sb.append(" Left join RDCReceiver on RDCReceiver.RDCNo=RDC.RDCNo and   RDCReceiver.Description=RDC.Descript and GateInwardRDC.GINo=RDCReceiver.GINo  ");
        sb.append(" Where (RDC.RDCAuthStatus=0 or RDC.RDCAuthStatus is null) and RDC.RDCDate<="+SDate+" and ");
        sb.append(" RDC.MillCode="+iMillCode+" Group by RDC.RDCNo,RDC.RDCDate,Supplier.Name,  RDC.Descript,RDC.Qty,");
        sb.append(" GateInwardRDC.RecQty,RDC.MemoAuthUserCode,RawUser.UserName, Unit.Unit_Name,");
        sb.append(" RDCReceiver.ReceivedQty,RDCReceiver.DateAndTime,GateInwardRDC.GINo,GateInwardRDC.GIDate,GateInwardRDC.RDCSlNo,RawUser.UserCode) ");
        sb.append(" Group By RDCNo,RDCDate,SupplierName,Descript, ");
        sb.append(" RDCQty,RecQty,MemoAuthUserCode, UserName,Unit_Name,GINo,GIDate,DeptRecQty,RDCSlNo,UserCode) where DeptRecQty<ReCQty Order by RDCNo");*/


       /* sb.append(" Select RDCNo,RDCDate,SupplierName,Descript,RDCQty,RecQty, MemoAuthUserCode,UserName,Unit_Name,DeptRecQty,GiNo,GIDate,RDCSlNo,UserCode from( ");
        sb.append(" Select RDCNo,RDCDate,SupplierName,Descript,RDCQty,RecQty, MemoAuthUserCode, UserName,Unit_Name,sum(DeptRecQty) as DeptRecQty,GINo,GIDate,RDCSlNo,UserCode ");
        sb.append(" From(Select   RDC.RDCNo, RDC.RDCDate,Supplier.Name as SupplierName, RDC.Descript,");
        sb.append(" RDC.Qty as RDCQty,GateInwardRDC.RecQty as RecQty,RDC.MemoAuthUserCode,RawUser.UserName,");
        sb.append(" Unit.Unit_Name, decode(RDCReceiver.ReceivedQty,null,0,RDCReceiver.ReceivedQty) as DeptRecQty,");
        sb.append(" RDCReceiver.DateAndTime,GateInwardRDC.GINo,GateInwardRDC.GIDate,GateInwardRDC.RDCSlNo as RDCSlNo,RawUser.UserCode  from RDC  ");
        sb.append(" Inner Join GateInwardRDC  on trim(RDC.Descript) = trim(GateInwardRDC.Descript)  and            GateInwardRDC.RDCNo = RDC.RDCNo and  ");
        sb.append(" GateInWardRDC.MillCode = RDC.MillCode and RDC.DocType=0  and  ");
        sb.append(" RDC.MemoAuthUserCode = GateInwardRDC.MemoAuthUserCode  and RDC.SlNo=GateInwardRDC.RDCSlNo");
        sb.append(" Inner Join Supplier on GateInwardRDC.Sup_Code=Supplier.AC_Code   ");
        sb.append(" Inner join Unit on unit.Unit_Code=RDC.Unit_Code  ");
        sb.append(" Inner join RDCMemo on RDCMemo.RDCNo = RDC.RDCNo and RDCMemo.MillCode=RDC.Millcode   ");
        sb.append(" Inner join RawUser on RawUser.UserCode=RDCMemo.AuthUserCode ");
        sb.append(" Left join RDCReceiver on RDCReceiver.RDCNo=RDC.RDCNo and   RDCReceiver.Description=RDC.Descript and GateInwardRDC.GINo=RDCReceiver.GINo  ");
        sb.append(" Where (RDC.RDCAuthStatus=0 or RDC.RDCAuthStatus is null) and RDC.RDCDate<="+SDate+" and ");
        sb.append(" RDC.MillCode="+iMillCode+" Group by RDC.RDCNo,RDC.RDCDate,Supplier.Name,  RDC.Descript,RDC.Qty,");
        sb.append(" GateInwardRDC.RecQty,RDC.MemoAuthUserCode,RawUser.UserName, Unit.Unit_Name,decode(RDCReceiver.ReceivedQty,null,0,RDCReceiver.ReceivedQty),");
        sb.append(" RDCReceiver.ReceivedQty,RDCReceiver.DateAndTime,GateInwardRDC.GINo,GateInwardRDC.GIDate,GateInwardRDC.RDCSlNo,RawUser.UserCode) ");
        sb.append(" Group By RDCNo,RDCDate,SupplierName,Descript, ");
        sb.append(" RDCQty,RecQty,MemoAuthUserCode, UserName,Unit_Name,GINo,GIDate,RDCSlNo,UserCode) where DeptRecQty<ReCQty Order by RDCNo");*/

         
//janaki adding dept on 26.11.2016

	sb.append("    Select RDCNo,RDCDate,SupplierName,Descript,RDCQty,RecQty, MemoAuthUserCode,UserName,Unit_Name,DeptRecQty,GiNo,GIDate,RDCSlNo,UserCode,dept_name,dept_code from(  ");
	sb.append("    Select RDCNo,RDCDate,SupplierName,Descript,RDCQty,RecQty, MemoAuthUserCode, UserName,Unit_Name,sum(DeptRecQty) as DeptRecQty,GINo,GIDate,RDCSlNo,UserCode ,dept_code,dept_name ");
	sb.append("    From(Select   RDC.RDCNo, RDC.RDCDate,Supplier.Name as SupplierName, RDC.Descript, ");
	sb.append("    RDC.Qty as RDCQty,GateInwardRDC.RecQty as RecQty,RDC.MemoAuthUserCode,RawUser.UserName, ");
	sb.append("    Unit.Unit_Name, decode(RDCReceiver.ReceivedQty,null,0,RDCReceiver.ReceivedQty) as DeptRecQty, ");
	sb.append("    RDCReceiver.DateAndTime,GateInwardRDC.GINo,GateInwardRDC.GIDate,GateInwardRDC.RDCSlNo as RDCSlNo,RawUser.UserCode,dept.dept_code,dept.dept_name  from RDC   ");
	sb.append("    Inner Join GateInwardRDC  on trim(RDC.Descript) = trim(GateInwardRDC.Descript)  and            GateInwardRDC.RDCNo = RDC.RDCNo and   ");
	sb.append("    GateInWardRDC.MillCode = RDC.MillCode and RDC.DocType=0  and   ");
	sb.append("    RDC.MemoAuthUserCode = GateInwardRDC.MemoAuthUserCode  and RDC.SlNo=GateInwardRDC.RDCSlNo ");
	sb.append("    Inner Join Supplier on GateInwardRDC.Sup_Code=Supplier.AC_Code    ");
	sb.append("    Inner join Unit on unit.Unit_Code=RDC.Unit_Code   ");
	sb.append("    Inner join dept on dept.dept_Code=RDC.dept_Code     ");
	sb.append("    Inner join RDCMemo on RDCMemo.RDCNo = RDC.RDCNo and RDCMemo.MillCode=RDC.Millcode    ");
//	sb.append("    Inner join RawUser on RawUser.UserCode=RDCMemo.AuthUserCode  ");
	sb.append("    Inner join RawUser on RawUser.UserCode=RDC.MemoAuthUserCode  ");
	sb.append("    Left join RDCReceiver on RDCReceiver.RDCNo=RDC.RDCNo and   RDCReceiver.Description=RDC.Descript and GateInwardRDC.GINo=RDCReceiver.GINo   ");
	sb.append("    Where (RDC.RDCAuthStatus=0 or RDC.RDCAuthStatus is null) and RDC.RDCDate<="+SDate+" and  ");
	sb.append("    RDC.MillCode="+iMillCode+" Group by RDC.RDCNo,RDC.RDCDate,Supplier.Name,  RDC.Descript,RDC.Qty, ");
	sb.append("    GateInwardRDC.RecQty,RDC.MemoAuthUserCode,RawUser.UserName, Unit.Unit_Name,decode(RDCReceiver.ReceivedQty,null,0,RDCReceiver.ReceivedQty), ");
	sb.append("    RDCReceiver.ReceivedQty,RDCReceiver.DateAndTime,GateInwardRDC.GINo,GateInwardRDC.GIDate,GateInwardRDC.RDCSlNo,RawUser.UserCode,dept.dept_name,dept.dept_code)  ");
	sb.append("    Group By RDCNo,RDCDate,SupplierName,Descript,  ");
	sb.append("    RDCQty,RecQty,MemoAuthUserCode, UserName,Unit_Name,GINo,GIDate,RDCSlNo,UserCode,dept_name,dept_code) where DeptRecQty<ReCQty Order by RDCNo ");


 
        //  System.out.println(sb.toString());

          return sb.toString();
     }
     private class ActList implements ActionListener
     {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==BRefresh)
            {
               setData();
            }
            if(ae.getSource()==BPunch)
            {

              init();
              start();
              BPunch.setEnabled(false);
              iPunchStatus=1;    
              //saveDataforcheck("183604","Test");
            }
        }
     }
     private boolean saveData(String SCode,String SName)
     {
         
         boolean bSave = false;
         try
         {
               if(checkSelected())
               {
                    
                  for(int i=0;i<theModel.getRowCount();i++)   
                  {
                    if(((Boolean)theModel.getValueAt(i,Select)).booleanValue())
                    {
                      String SRDCNo    = (String)theModel.getValueAt(i,RDCNo);
                      String SRDCDate  = common.pureDate((String)theModel.getValueAt(i,RDCDate));
                      String SGINo     = (String)theModel.getValueAt(i,GINo);
                      String SGIDate   = common.pureDate((String)theModel.getValueAt(i,GIDate));
                      String SItemName = (String)theModel.getValueAt(i,Description);
                      double dReceivedQty = common.toDouble((String)theModel.getValueAt(i,DeptReceiveQty));
                      getMemoNoAndDate(SRDCDate,SRDCNo,SItemName);
                     
                      int    iRDCSlNo = common.toInt((String)VRDCSlNo.elementAt(i)); 
                     
                      if(isDataValid(i))
                      {
                        if(!isAlreadyExists(SItemName,SRDCNo,SGINo,SGIDate,iRDCSlNo))   
                        {
                            bSave = insertDepartmentReceivedRDC(SRDCNo,SRDCDate,SItemName,dReceivedQty,SCode,SGINo,SGIDate,iRDCSlNo);
                            BPunch.setEnabled(true);
                        }
                      }
                      else
                      {
                          BPunch.setEnabled(true);
                      }
                    }
                  }
              }
         else
         {
             JOptionPane .showMessageDialog(null,"No Rows Selected For Save ","Info",JOptionPane.INFORMATION_MESSAGE);
         }
         }
         catch(Exception ex)
         {
             bSave = false;
             ex.printStackTrace();
         }
         return bSave;
     }
     public boolean checkSelected()
     {
         boolean bSelect =false;
         for(int i=0;i<theModel.getRowCount();i++)
         {
             boolean bVal = ((Boolean)theModel.getValueAt(i, Select)).booleanValue();
             if(bVal)
             {
                 bSelect = true;
                 break;
             }
         }
         
         return bSelect;
     }
     private boolean isDataValid(int i)
     {
         String SRDCNo    = (String)theModel.getValueAt(i,RDCNo);
         String SRDCDate  = (String)theModel.getValueAt(i,RDCDate);
        // String SGINo     = (String)theModel.getValueAt(i,GINo);
        // String SGIDate   = (String)theModel.getValueAt(i,GIDate);
         String SItemName = (String)theModel.getValueAt(i,Description);
         double dReceiveQty = common.toDouble((String)theModel.getValueAt(i,DeptReceiveQty));
         double dReceivedQty = common.toDouble((String)theModel.getValueAt(i,DeptReceivedQty));
         double dGIQty      = common.toDouble((String)theModel.getValueAt(i,ReceiveQty));
         double dBalance    = common.toDouble((String)theModel.getValueAt(i,Balance));
         
         if(dReceiveQty<=0)
         {
             JOptionPane .showMessageDialog(null,"Dept Receive Qty Must Not be 0") ;
             return false;
         }
        else if(dReceiveQty>dBalance)
        {
            JOptionPane .showMessageDialog(null,"Receive Qty is Greater than Balance Qty") ;
             return false;
        }
        else if((dReceiveQty+dReceivedQty)>dGIQty)
        {
             JOptionPane .showMessageDialog(null,"Receive Qty is Greater than GI Qty") ;
             return false;
        }
      
         return true;
     }
     public boolean checkControlTime(int iTypeCode)
     {
          int iValue =0;
          boolean bFlag = false;

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(getControlTimeQS(iTypeCode));

               if(result.next())
               {
                   
                    iValue = result.getInt(1);
                    System.out.println(" Time "+iValue);
               }
               result.close();
               theStatement.close();

               if(iValue<0)
                  return true;

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               return true;
          }

          return false;
     }
 private void getMemoNoAndDate(String SRDCDate,String SRDCNo,String SDescription)
  {
      try
      {
          String QS = " Select MemoNo,MemoDate from RDCMemo where Descript = '"+SDescription+"'"+
                      " and RDCNo = "+SRDCNo;
                  //+" and RDCDate = "+SRDCDate;
          
          Connection theConnection=null;

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                   iMemoNo   = result.getInt(1);
                   iMemoDate = result.getInt(2);
               }
      }
      catch(Exception ex)
      {
          ex.printStackTrace();
      }
  }
     private boolean insertDepartmentReceivedRDC(String SRDCNo,String SRDCDate,String SItemName,double dReceivedQty,String SReceiverEmpCode,String SGINo,String SGIDate,int iRDCSlNo)
     {
         boolean bSave;
         try
         {
         StringBuffer sb = new StringBuffer();
           sb.append(" Insert into RDCReceiver(Id,MemoNo,MemoDate,RDCNo,RDCDate,Description,");
           sb.append(" ReceivedQty,ReceivedBy,SystemName,DateAndTime,UserCode,");
           sb.append(" MillCode,GiNo,GiDate,RDCSlNo) ");
           sb.append(" values(DeptReceivedRDC_Seq.NextVal,?,?,?,?,?,?,?,?,to_Date(to_Char(sysdate,'dd-mm-yyyy hh24:mi:ss'),'dd-mm-yyyy hh24:mi:ss'),?,?,?,?,?)");
           ORAConnection   oraConnection =  ORAConnection.getORAConnection();
           theConnection =  oraConnection.getConnection();               
           PreparedStatement theStatement    = theConnection.prepareStatement(sb.toString());
           theStatement  . setInt(1,iMemoNo);
           theStatement  . setInt(2,iMemoDate);
           theStatement  . setString(3,SRDCNo);
           theStatement  . setString(4,SRDCDate);
           theStatement  . setString(5,SItemName);
           theStatement  . setDouble(6,dReceivedQty);
           theStatement  . setString(7,SReceiverEmpCode);
           theStatement  . setString(8,SSystemName);
           theStatement  . setInt(9,iUserCode);
           theStatement  . setInt(10,iMillCode);
           theStatement  . setString(11,SGINo);
           theStatement  . setString(12,SGIDate);
           theStatement  . setInt(13,iRDCSlNo);
           theStatement.executeUpdate(); 
           bSave = true;
           theConnection.commit();
           theStatement.close();
         }
         catch(Exception ex)
         {
             bSave =false;
             try
             {    
                theConnection.rollback();
             }
             catch(Exception e)
             {
                 e.printStackTrace();
             }
             ex.printStackTrace();
         }
           return bSave;
     }
     private String getControlTimeQS(int iTypeCode)
     {
          String QS=" Select (to_date(controlTime,'hh24:mi')-(to_date(to_Char(sysdate,'hh24:mi'),'hh24:mi')))*(24*60) as Diff from"+
                    " TimeControl"+
                    " Where TypeCode="+iTypeCode;
          return QS;
     }
     private boolean isReceiverEligible(String SEmployeeCode)
     {
          boolean bEligible=false;
          int iRDCUserCode = getRDCUserCode();
          StringBuffer sb  = new StringBuffer();
          sb.append(" Select EmpCode from IssueReceiverList where UserCode ="+iRDCUserCode);
          try
          {
              ORAConnection   oraConnection =  ORAConnection.getORAConnection();
              Connection      theConnection =  oraConnection.getConnection();               
              Statement theStatement    = theConnection.createStatement();
              ResultSet result          = theStatement.executeQuery(sb.toString());
              while(result.next())
              {
                 String SEmpCode = result.getString(1);
                 if(SEmpCode.equals(SEmployeeCode))
                   bEligible = true; 
              }
              result.close();
          }
          catch(Exception ex)
          {
             System.out.println(" isReceiver Eligible"+ex);
          }
        return bEligible;
     }
     private int getRDCUserCode()
     {
       int iRdcUserCode = -1;
       if(checkSelected())
       {
         for(int i=0;i<theModel.getRowCount();i++)   
         {
          if(((Boolean)theModel.getValueAt(i,Select)).booleanValue())
          {
             iRdcUserCode = common.toInt((String)VRDCUserCode.elementAt(i));
          }
         }
       }
       return iRdcUserCode;

     } 

     protected void init()        
     {
            capturer = DPFPGlobal.getCaptureFactory().createCapture();
        
                
		capturer.addDataListener(new DPFPDataAdapter() {
                   
                public void dataAcquired(final DPFPDataEvent e) {
                  
                  
                    LStatus.setText(" Please wait Under processing.................");

				SwingUtilities.invokeLater(new Runnable()
                                {
                                        public void run()
                                        {
                                    
                                                LStatus.setText("The fingerprint sample was captured....");
                                                process(e.getSample());
                                                
                                        }});
			}
		});
               
		capturer.addReaderStatusListener(new DPFPReaderStatusAdapter() {
                public void readerConnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
                                    
		 			makeReport("The fingerprint reader was connected.");
				}});
			}
                public void readerDisconnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was disconnected.");
				}});
			}
		});
		capturer.addSensorListener(new DPFPSensorAdapter() {
                public void fingerTouched(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was touched.");
				}});
			}
                public void fingerGone(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The finger was removed from the fingerprint reader.");
				}});
			}
		});
		capturer.addImageQualityListener(new DPFPImageQualityAdapter() {
                public void onImageQuality(final DPFPImageQualityEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					if (e.getFeedback().equals(DPFPCaptureFeedback.CAPTURE_FEEDBACK_GOOD))
						makeReport("The quality of the fingerprint sample is good.");
					else
						makeReport("The quality of the fingerprint sample is poor.");
				}});
			}
		});
	}

	protected void process(DPFPSample sample)
	{
		// Draw fingerprint sample image.
		
		DPFPFeatureSet features = extractFeatures(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

		// Check quality of the sample and start verification if it's good
		int iStatus=0;
   	        String SName  = "";
		String SCode  = "";
                LStatus.setText(" Under processing.................");

		if (features != null)
		{
			// Compare the feature set with our template



               for(int i=0; i<theMaterialIssueTemplateList.size(); i++)
               {
                    HashMap theMap = (HashMap) theMaterialIssueTemplateList.get(i);

                    SCode    = (String)theMap.get("EMPCODE");
                    SName    = (String)theMap.get("DISPLAYNAME");
                    
                    DPFPTemplate template   = (DPFPTemplate)theMap.get("TEMPLATE");

                    DPFPVerification matcher= DPFPGlobal.getVerificationFactory().createVerification();
                    matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);

                    DPFPVerificationResult result = 
                    matcher.verify(features,template);

                    if (result.isVerified())
                    {
                         LStatus.setText(""+SName);       
                         iStatus=1;
                         break;
                    }
               }


               if(iStatus==0)
               {

                    for(int i=0; i<theTemplateList.size(); i++)
                    {
     
                         HashMap theMap = (HashMap) theTemplateList.get(i);
     
                         SCode    = (String)theMap.get("EMPCODE");
                         SName    = (String)theMap.get("DISPLAYNAME");
                         
                         DPFPTemplate template   = (DPFPTemplate)theMap.get("TEMPLATE");
     
                         DPFPVerification matcher= DPFPGlobal.getVerificationFactory().createVerification();
                         matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);
     
                         DPFPVerificationResult result = 
                         matcher.verify(features,template);
     
                         if (result.isVerified())
                         {
                                LStatus.setText(""+SName);       

                              iStatus=1;
                              break;
                         }
                    }
               }

                  if(iStatus==0)
                  {
                         LStatus.setText("No Match Found");       
                         JOptionPane.showMessageDialog(null," No Match Found Punch Again");
                         stop();
                         iPunchStatus=0;
                         BPunch.setEnabled(true);


                  }
                  else
                  {
                    if(isReceiverEligible(SCode))
                    {
                      if(saveData(SCode,SName))
                      {
                            JOptionPane.showMessageDialog(null," Item Received Sucessfully");
                           // removeHelpFrame();
                            
                           // LStatus.setText(" Punch And Take Issue");
                           
                            stop();
                            setData();
                            iPunchStatus=0;
                            BPunch.setEnabled(true);

                       }
                       else
                       {
                            JOptionPane.showMessageDialog(null," Problem in Storing Data");
                            stop();
                            iPunchStatus=0;
                            BPunch.setEnabled(true);
                       }
                   }
                   else
                   {
                      JOptionPane.showMessageDialog(null," You are not eligible to Receive this Material");
                      stop();
                      setData();
                      BPunch.setEnabled(true);
                   }

                }
          }
	}
	protected void start()
	{
            capturer.startCapture();
	}

	protected void stop()
	{
             capturer.stopCapture();
	}
	public void setPrompt(String string) {
	}
	public void makeReport(String string) {
            System.out.println(string);
	}
	
	protected Image convertSampleToBitmap(DPFPSample sample) {
		return DPFPGlobal.getSampleConversionFactory().createImage(sample);
	}

	protected DPFPFeatureSet extractFeatures(DPFPSample sample, DPFPDataPurpose purpose)
	{
		DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
		try {
			return extractor.createFeatureSet(sample, purpose);
		} catch (DPFPImageQualityException e)
        {
            System.out.println(" Returning Null");

        	return null;
		}
	}

 private void getPreviousGateInwards(int iRDCNo,String SDescription,int iGiNo)
        {
            VGINo   = new Vector();
            VGIDate = new Vector();
            VRecQty = new Vector();
            VReceivedBy = new Vector();
        try
        {
            Connection theConnection=null;
            String QS = " Select GateInwardRDC.RDCNo,GateInwardRDC.Descript,GateInwardRDC.GiNo,GateInwardRDC.GiDate,GateInwardRDC.RecQty,OneTouchEmployee.DisplayName from GateInwardRDC "+
                   " Left Join RDCReceiver on RDCReceiver.GINo=GateInwardRDC.GINo"+
                   " Left join OneTouchEmployee on OneTouchEmployee.EmpCode= RDCReceiver.ReceivedBy";
                   QS = QS+" where GateInwardRDC.RDCNo="+iRDCNo+" and GateInwardRDC.Descript='"+SDescription+"'"+" and GateInwardRDC.GiNo!="+iGiNo;
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                   VGINo   .   addElement(result.getString(3));
                   VGIDate .   addElement(result.getString(4));
                   VRecQty .   addElement(result.getString(5));
                   VReceivedBy . addElement(result.getString(6));
               }
      }
      catch(Exception ex)
      {
          ex.printStackTrace();
      }
      }
      private boolean isAlreadyExists(String SDescription,String SRDCNo,String SGiNo,String SGiDate,int iRDCSlNo)
      {
         int iCount=0;
         try
          {
           String QS = " Select count(*) from RDCReceiver where Description = '"+SDescription+"'"+
                       " and RDCNo = "+SRDCNo+" and GiNo= "+SGiNo+" and GiDate="+SGiDate+" and RDCSlNo="+iRDCSlNo;
           System.out.println(" Already Exists QS "+QS);
          Connection theConnection=null;

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                   iCount =result.getInt(1);
               }
               result.close();
               theStatement.close();
          
      }
      catch(Exception ex)
      {
          ex.printStackTrace();
          return true;
      }
       if(iCount>0)
           return true;
       else
           return false;
   }

}
