package Indent.IndentFiles;

import java.awt.*;
import java.awt.event.*;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import util.*;


public class RDCListModel extends DefaultTableModel
{
                         //   0         1         2         3         4        5          6         7         8         

     String ColumnName[] ={"S.No","RDCNo","RDCDate","User","Unit","SupplierName","Description","RDCQty","GateInwardQty","DeptReceivedQty","DeptReceiveQty","Balance","Select","GINo","GIDate","Department"};
     String ColumnType[] ={"S","S","S","S","S","S","S","S","S","S","E","S","E","S","S","S"};

     Vector theVector;
     Common common ;
     public RDCListModel()
     {
          common         = new Common();
          setDataVector(getRowData(),ColumnName);
     }
     public Object[][] getRowData()
     {
          Object RowData[][] = new Object[0][ColumnName.length];

          return RowData;
     }
     public boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="B" || ColumnType[col]=="E")
               return true;
          return false;
     }

     public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }

     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));


          }

          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }
     public int getRows()
     {
         return super.dataVector.size(); 
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

}

