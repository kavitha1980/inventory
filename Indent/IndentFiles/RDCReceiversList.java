
package Indent.IndentFiles;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import util.*;
import guiutil.*;
//import jdbc.*;
import java.sql.*;
public class RDCReceiversList extends JInternalFrame
{
    JPanel       pnlTop,pnlMiddle,pnlBottom;
    JButton      BApply,BExit;
    MyTextField  txtFromDate,txtToDate;
    JTable       theTable = null;
    RDCReceivedListModel theModel=null;
    Connection    theConnection = null;
    String        SYearCode="";
    int           iMillCode,iUserCode;
    Common        common = new Common();
    Vector        VRDCDate,VRDCNo,VItemName,VUnit,VDepartment,VReceivedQty,VReceiverName,VReceiveDate;
    Vector        VSupplierName;
    JLayeredPane  Layer;
    public RDCReceiversList(JLayeredPane Layer,int iUserCode,int iMillCode,String SYearCode)
    {
        this . Layer     = Layer;
        this . iUserCode = iUserCode;
        this . iMillCode = iMillCode;
        this . SYearCode = SYearCode;
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
        
    }
    private void createComponents()
    {
         pnlTop    = new JPanel();
         pnlMiddle = new JPanel();
         pnlBottom = new JPanel();
         
         txtFromDate = new MyTextField(10);
         txtToDate   = new MyTextField(10);
         
         BApply    = new JButton("Apply");
         BExit     = new JButton("Exit");
         
         theModel  = new RDCReceivedListModel();
         theTable  = new JTable(theModel);
    }
    private void setLayouts()
    {
        pnlTop    . setLayout(new FlowLayout(FlowLayout.CENTER));
        pnlMiddle . setLayout(new BorderLayout());
        pnlBottom . setLayout(new FlowLayout(FlowLayout.CENTER));
        
        pnlTop    . setBorder(new TitledBorder("Filter"));
        pnlMiddle . setBorder(new TitledBorder("Data"));
        pnlBottom . setBorder(new TitledBorder("Controls"));
        
        this . setLayout(new BorderLayout());
        this . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this . setMaximizable(true);
        this . setClosable(true);
        this . setSize(1000,600);
        this . setVisible(true);
        this . setTitle(" RDC Receivers List ");
    }
    private void addComponents()
    {
        pnlTop  . add(new JLabel("From Date"));
        pnlTop  . add(txtFromDate);
        pnlTop  . add(new JLabel("To Date"));
        pnlTop  . add(txtToDate);
        pnlTop  . add(BApply);
        
        pnlMiddle . add(new JScrollPane(theTable));
        
        pnlBottom . add(BExit);
        
        
        this . add(pnlTop,BorderLayout.NORTH);
        this . add(pnlMiddle,BorderLayout.CENTER);
        this . add(pnlBottom,BorderLayout.SOUTH);
        
    }
    private void addListeners()
    {
        BApply . addActionListener(new ActList());
        BExit  . addActionListener(new ActList());
    }
    private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==BApply)
            {
                setTableData();
            }
            if(ae.getSource()==BExit)
            {
                dispose();
            }
        }
    }
   
    private void setTableData()
    {
       theModel . setNumRows(0);
       theTable.getColumn("SlNo").setMaxWidth(50);
       theTable.getColumn("RDCDate").setMaxWidth(100);
       theTable.getColumn("RDCNo").setMaxWidth(80);
       theTable.getColumn("Unit").setMaxWidth(100);
//       theTable.getColumn("ReceivedDate").setMaxWidth(110);

        setRDCReceivedData();
        
        for(int i=0;i<VRDCNo.size();i++)
        {
            Vector theVect = new Vector();
            theVect . addElement(String.valueOf(i+1));
            theVect . addElement(VRDCNo.elementAt(i));
            theVect . addElement(VRDCDate.elementAt(i));
            theVect . addElement(VItemName.elementAt(i));
            theVect . addElement(VSupplierName.elementAt(i));
            theVect . addElement(VUnit.elementAt(i));
            theVect . addElement(VDepartment.elementAt(i));
            theVect . addElement(VReceiveDate.elementAt(i));
            theVect . addElement(VReceivedQty.elementAt(i));
            theVect . addElement(VReceiverName.elementAt(i));
            theModel . appendRow(theVect);
        }
    }
    private void setRDCReceivedData()
    {
        String SFromDate = common.pureDate(txtFromDate.getText());
        String SToDate   = common.pureDate(txtToDate  .getText());
        try
        {
        VRDCDate = new Vector();
        VRDCNo   = new Vector();
        VItemName= new Vector();
        VUnit    = new Vector();
        VDepartment  = new Vector();
        VReceivedQty = new Vector();
        VReceiverName= new Vector();
        VReceiveDate = new Vector();
        VSupplierName= new Vector();
        StringBuffer sb = new StringBuffer();
        sb.append(" Select RDCReceiver.RDCNo,RDCReceiver.RDCDate,RDCReceiver.Description,Dept.Dept_Name ,Unit.Unit_Name,");
        sb.append(" RDCReceiver.ReceivedQty,RDCReceiver.ReceivedBy,OneTouchEmployee.DisplayName,");
        sb.append(" to_Char(dateandtime,'dd-mm-yyyy'),Supplier.Name ");
        sb.append(" from RDCReceiver");
        sb.append(" Inner join RDC on RDC.RDCNo = RDCReceiver.RDCNo and RDC.Descript=RDCReceiver.Description  and RDC.MillCode = RDCReceiver.MillCode");
        sb.append(" Inner join Dept on Dept.Dept_Code = RDC.Dept_Code");
        sb.append(" Inner join Unit on Unit.Unit_Code = RDC.Unit_Code");
        sb.append(" Left join OneTouchEmployee on OneTouchEmployee.EmpCode= RDCReceiver.ReceivedBy");
        sb.append(" Inner join Supplier on Supplier.Ac_Code = RDC.Sup_Code");
        sb.append(" where RDCReceiver.MillCode = "+iMillCode);
        sb.append(" and to_Char(RDCReceiver.DateAndTime,'yyyymmdd')>="+SFromDate);
        sb.append(" and to_Char(RDCReceiver.DateAndTime,'yyyymmdd')<="+SToDate);
        sb.append(" order by 1");
       // sb.append(" and to_Char(RDCReceiver.DateAndTime,'yyyymmdd')>=20141120");
       //sb.append(" and to_Char(RDCReceiver.DateAndTime,'yyyymmdd')<=20141121");
        
         System.out.println(" set RDCReceived Data "+sb.toString());
              
               if(theConnection == null)
               {
                     jdbc.ORAConnection   oraConnection =  jdbc.ORAConnection.getORAConnection();
                     theConnection =  oraConnection.getConnection();   
               }

               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(sb.toString());

               while(theResult.next())
               {
                   VRDCNo     . addElement(theResult.getString(1));
                   VRDCDate   . addElement(common.parseDate(theResult.getString(2)));
                   VItemName  . addElement(theResult.getString(3));
                   VDepartment. addElement(theResult.getString(4));
                   VUnit      . addElement(theResult.getString(5));
                   VReceivedQty  . addElement(theResult.getString(6));
                  // VReceivedBy   . addElement(theResult.getString(7));
                   VReceiverName . addElement(common.parseNull(theResult.getString(8)));
                   VReceiveDate  . addElement(theResult.getString(9));
                   VSupplierName . addElement(theResult.getString(10));
               }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
   

 class RDCReceivedListModel extends DefaultTableModel
{
     Common common ;
     String ColumnName[] ={"SlNo","RDCNo","RDCDate","Description","Supplier Name","Unit","Department","ReceivedDate","ReceivedQty","Received By"};
     String ColumnType[] ={"S","S","S","S","S","S","S","S","S","S"};
     public RDCReceivedListModel()
     {
          common         = new Common();
          setDataVector(getRowData(),ColumnName);
     }
     public Object[][] getRowData()
     {
          Object RowData[][] = new Object[0][ColumnName.length];

          return RowData;
     }
     public boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="B" || ColumnType[col]=="E")
               return true;
          return false;
     }

     public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }

     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));


          }

          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }
     public int getRows()
     {
         return super.dataVector.size(); 
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }
 }
}