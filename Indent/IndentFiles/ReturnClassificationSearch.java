package Indent.IndentFiles;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class ReturnClassificationSearch
{
     JLayeredPane        Layer;
     Vector              VClass,VClassCode;
     JTable              theTable;
     JList               BrowList;
     JScrollPane         BrowScroll;
     JPanel              LeftPanel;
     JInternalFrame      ClassFrame;
     JTextField          TIndicator;
     int                 iSelectedRow,iRow;
     String              str="";
     Common              common = new Common();
     IssueReturnMiddlePanel   IMP;
     MouseEvent          me;
     
     ReturnClassificationSearch(JLayeredPane Layer,Vector VClass,Vector VClassCode, IssueReturnMiddlePanel  IMP)
     {
          this.Layer          = Layer;
          this.VClass         = VClass;
          this.VClassCode     = VClassCode;
          this.IMP            = IMP;
          BrowList            = new JList(VClass);
          BrowScroll          = new JScrollPane(BrowList);
          LeftPanel           = new JPanel(true);
          TIndicator          = new JTextField();
          TIndicator          .setEditable(false);
          ClassFrame          = new JInternalFrame("Classification Selector");
          ClassFrame          .show();
          ClassFrame          .setBounds(80,100,550,350);
          ClassFrame          .setClosable(true);
          ClassFrame          .setResizable(true);
          BrowList            .addKeyListener(new KeyList());
          ClassFrame          .getContentPane().setLayout(new BorderLayout());
          ClassFrame          .getContentPane().add("South",TIndicator);
          ClassFrame          .getContentPane().add("Center",BrowScroll);
          ClassFrame          .show();
     }

     public void showClassificationFrame(MouseEvent me)
     {
          this.me = me; 
          removeHelpFrame();
          try
          {
               Layer     .add(ClassFrame);
               ClassFrame.moveToFront();
               ClassFrame.setSelected(true);
               ClassFrame.show();
               BrowList  .requestFocus();
          }
          catch(Exception ex){}
     }
     
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }
          
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index           = BrowList.getSelectedIndex();
                    String SClassName   = (String)VClass.elementAt(index);
                    String SClassCode   = (String)VClassCode.elementAt(index);
                    addClassDet(SClassName);
                    str  = "";
                    removeHelpFrame();
                    IMP.tabreport.requestFocus();
               }
          }
     }
     
     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<VClass.size();index++)
          {
               String str1 = ((String)VClass.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedValue(str1,true);
                    BrowList.ensureIndexIsVisible(index+5);
                    break;
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(ClassFrame);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

     public void addClassDet(String SClassName)
     {
          try
          {     
               for(int i=0;i<IMP.tabreport.RowData.length;i++)
               {
                    String SCode = ((String)IMP.tabreport.RowData[i][1]).trim();
                    if(SCode.length() == 0)
                         continue;
                    IMP.tabreport.RowData[i][5] = SClassName;
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     } 
}
