package Indent.IndentFiles;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class ReturnMaterialsSearch
{
     JLayeredPane        Layer;
     Vector              VName,VCode,VUomName,VNameCode;
     String              SName,SCode,SUom;
     IssueReturnMiddlePanel   MiddlePanel;
     int                 iMillCode;
     String              SItemTable,SSupTable,SAuthUserCode;
     
     JList          BrowList,SelectedList;
     JScrollPane    BrowScroll,SelectedScroll;
     JTextField     TIndicator;
     JButton        BOk;
     JPanel         LeftPanel,RightPanel;
     JInternalFrame MaterialFrame;
     JPanel         MFMPanel,MFBPanel;
     Vector         VSelectedName,VSelectedCode,VSelectedUom;
     Vector         VSelectedNameCode;
     String         str="";
     int            iMFSig=0;
     MouseEvent     ae;
     Common         common = new Common();

     ReturnMaterialsSearch(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUomName,Vector VNameCode,IssueReturnMiddlePanel MiddlePanel,int iMillCode,String SItemTable,String SSupTable,String SAuthUserCode)
     {
          this.Layer          = Layer;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VUomName       = VUomName;
          this.VNameCode      = VNameCode;
          this.MiddlePanel    = MiddlePanel;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;
          this.SAuthUserCode  = SAuthUserCode;

          createComponents();
     }

     public void createComponents()
     {
          VSelectedName       = new Vector();
          VSelectedCode       = new Vector();
          VSelectedUom        = new Vector();
          VSelectedNameCode   = new Vector();

          BrowList       = new JList(VNameCode);
          BrowList       .setFont(new Font("monospaced", Font.PLAIN, 11));
          SelectedList   = new JList();
          BrowScroll     = new JScrollPane(BrowList);
          SelectedScroll = new JScrollPane(SelectedList);
          LeftPanel      = new JPanel(true);
          RightPanel     = new JPanel(true);
          TIndicator     = new JTextField();
          BOk            = new JButton("Selection Over");
          TIndicator     .setEditable(false);
          MFMPanel       = new JPanel(true);
          MFBPanel       = new JPanel(true);
          MaterialFrame  = new JInternalFrame("Materials Selector");
          MaterialFrame  .show();
          MaterialFrame  .setBounds(50,50,650,350);
          MaterialFrame  .setClosable(true);
          MaterialFrame  .setResizable(true);
          BrowList       .addKeyListener(new KeyList());
          BrowList       .requestFocus();
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent e)
          {
               if(VSelectedCode.size()==0)
               {
                    JOptionPane.showMessageDialog(null,"No Material is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                    BrowList.requestFocus();
                    return;
               }
               BOk.setEnabled(false);
               setMiddlePanel();
               removeHelpFrame();
               str="";
          }
     }
     
     public void showMaterialsFrame(MouseEvent ae)
     {
          this.ae = ae;
          TIndicator.setText(str);
          BOk.setEnabled(true);
          if(iMFSig==0)
          {
               MFMPanel.setLayout(new GridLayout(1,2));
               MFBPanel.setLayout(new GridLayout(1,2));
               MFMPanel.add(BrowScroll);
               MFMPanel.add(SelectedScroll);
               MFBPanel.add(TIndicator);
               MFBPanel.add(BOk);
               BOk.addActionListener(new ActList());
               MaterialFrame.getContentPane().add("Center",MFMPanel);
               MaterialFrame.getContentPane().add("South",MFBPanel);
               iMFSig=1;
          }
          removeHelpFrame();
          try
          {
               Layer.add(MaterialFrame);
               MaterialFrame.moveToFront();
               MaterialFrame.setSelected(true);
               MaterialFrame.show();
               BrowList.requestFocus();
               Layer.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex)
               {
                    Toolkit.getDefaultToolkit().beep();
               }
          }
     
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    try
                    {
                         int index = BrowList.getSelectedIndex();
                         if(index == -1)
                              return;
                         String SMatNameCode      = (String)VNameCode.elementAt(index);
                         String SMatName          = (String)VName.elementAt(index);
                         String SMatCode          = (String)VCode.elementAt(index);
                         String SMatUom           = (String)VUomName.elementAt(index);
                         addMatDet(SMatName,SMatCode,SMatUom,SMatNameCode);
                         str="";
                         TIndicator.setText(str);
                    }catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
               }
               if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
               {
                    try
                    {
                         setMiddlePanel();
                         removeHelpFrame();
                         str="";
                         MiddlePanel.tabreport.requestFocus();
                    }catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
               }
          }
     }

     public void setCursor()
     {
          int index=0;
          TIndicator.setText(str);
          for(index=0;index<VNameCode.size();index++)
          {
               String str1 = ((String)VNameCode.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
          if(index >= VNameCode.size())
          Toolkit.getDefaultToolkit().beep();
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(MaterialFrame);
               Layer.repaint();
               Layer.updateUI();
               MiddlePanel.tabreport.ReportTable.requestFocus();
          }
          catch(Exception ex) { }
     }
     
     public boolean addMatDet(String SMatName,String SMatCode,String SMatUom,String SMatNameCode)
     {
          int iIndex=VSelectedCode.indexOf(SMatCode);
          if (iIndex==-1)
          {
               VSelectedName       .addElement(SMatName);
               VSelectedCode       .addElement(SMatCode);
               VSelectedUom        .addElement(SMatUom);
               VSelectedNameCode   .addElement(SMatNameCode);
          }
          else
          {
               VSelectedName       .removeElementAt(iIndex);
               VSelectedCode       .removeElementAt(iIndex);
               VSelectedUom        .removeElementAt(iIndex);
               VSelectedNameCode   .removeElementAt(iIndex);
          }
          SelectedList.setListData(VSelectedNameCode);
          return true;
     }

     public void setMiddlePanel()
     {
          String SMatCode = "";
          String SMatName = "";
          try
          {

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();

               for(int i=0;i<VSelectedName.size();i++)
               {
                    double dStock=0;
                    double dRate=0;

                    SMatCode = (String)VSelectedCode.elementAt(i);
                    SMatName = (String)VSelectedName.elementAt(i);
                    

                    String QS = " Select Stock,StockValue from ItemStock Where HodCode="+SAuthUserCode+" and ItemCode='"+SMatCode+"' and MillCode="+iMillCode;

                    ResultSet res  = stat.executeQuery(QS);
                    while (res.next())
                    {
                         dStock = common.toDouble(common.parseNull((String)res.getString(1)));
                         dRate  = common.toDouble(common.parseNull((String)res.getString(2)));
                    }
                    res.close();

                    if(dRate<=0)
                    {
                         String QS1 = " Select max(StockValue) from ItemStock Where ItemCode='"+SMatCode+"' and MillCode="+iMillCode;
          
                         res  = stat.executeQuery(QS1);
                         while (res.next())
                         {
                              dRate  = common.toDouble(common.parseNull((String)res.getString(1)));
                         }
                         res.close();
                    }

                    if(dRate<=0)
                    {
                         Item      IC                  = new Item(SMatCode,iMillCode,SItemTable,SSupTable);
                         double    dAllStock           = common.toDouble(IC.getClStock());
                         double    dAllValue           = common.toDouble(IC.getClValue());
     
                         try
                         {
                              dRate = dAllValue/dAllStock;
                         }
                         catch(Exception ex)
                         {
                              dRate=0;
                         }
     
                         if(dAllStock==0)
                         {
                              dRate = common.toDouble(IC.SRate);
                         }
                    }

                    double dValue = dStock * dRate;


                    MiddlePanel . VMidIssueValue    . setElementAt(common.getRound(dValue,2),i);

                    String SStock  = common.getRound(dStock,3);
                    
                    MiddlePanel.RowData[i][1] = SMatCode.trim();
                    MiddlePanel.RowData[i][2] = SMatName.trim();
                    MiddlePanel.RowData[i][3] = (String)VSelectedUom.elementAt(i);
                    MiddlePanel.RowData[i][6] = SStock.trim();
                    MiddlePanel.RowData[i][8] = common.getRound(dRate,4);
               }
          }
          catch(Exception ex)
          {
               System.out.println("Set Middlepanel"+ex);
               ex.printStackTrace();
          }
     }

}
