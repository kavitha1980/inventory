package Indent.IndentFiles;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class SearchListN
{
     JLayeredPane        Layer;
     Vector              VDept,VDeptCode;
     JTable              theTable;
     JList               BrowList;
     JScrollPane         BrowScroll;
     JPanel              LeftPanel;
     JInternalFrame      DeptFrame;
     JTextField          TIndicator;
     int                 iSelectedRow;
     String              str="";
     Common              common = new Common();
     IssueListModelN     theModel;
     MouseEvent          me;
     int                 iRow,iCol;
     String              STitle;
     JDialog   dialog;


     SearchListN(JLayeredPane Layer,Vector VDept,Vector VDeptCode, IssueListModelN theModel,int iRow,int iCol,String STitle)
     {
          this.Layer     = Layer;
          this.VDept     = VDept;
          this.VDeptCode = VDeptCode;
          this.theModel  = theModel;
          this.iRow      = iRow;
          this.iCol      = iCol;
          this.STitle    = STitle;

          BrowList       = new JList(VDept);
          BrowScroll     = new JScrollPane(BrowList);
          LeftPanel      = new JPanel(true);
          TIndicator     = new JTextField();
          TIndicator     .setEditable(false);
          dialog     = new JDialog(new Frame(),STitle,true);

          BrowList.addKeyListener(new KeyList());

          activate();

     }

     public void activate()
     {
          dialog.getContentPane().add("South",TIndicator);
          dialog.getContentPane().add("Center",BrowScroll);
          dialog.setBounds(50,50,500,500);
          dialog.setVisible(true);

          BrowList.requestFocus();
     }


     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
               if(ke.getKeyCode()==8)
               {
                    str=str.substring(0,(str.length()-1));
                    setCursor();
               }
               else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
               {
                    str=str+lastchar;
                    setCursor();
               }
               }
               catch(Exception ex){}
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index           = BrowList.getSelectedIndex();
                    String SDeptName    = (String)VDept.elementAt(index);
                    String SDeptCode    = (String)VDeptCode.elementAt(index);
                    str                 = "";
                    if(iRow==0)
                    {
                         for(int i=0; i<theModel.getRows(); i++)
                         {
                                 theModel.setValueAt(SDeptName,i,iCol);
                         }
                    }
                    dialog.setVisible(false);
               }
          }
     }

     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<VDept.size();index++)
          {
               String str1 = ((String)VDept.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedValue(str1,true);
                    BrowList.ensureIndexIsVisible(index+5);
                    break;
               }
          }
     }


     public void addDeptDet(String SDeptName)
     {
          try
          {     
               theModel.setValueAt(SDeptName,iRow,iCol);
          }
          catch(Exception ex)
          {
          System.out.println(ex);
          }
     } 
}

/*

          DeptFrame      = new JInternalFrame("Department Selector");
          DeptFrame      .show();
          DeptFrame      .setBounds(80,100,550,350);
          DeptFrame      .setClosable(true);
          DeptFrame      .setResizable(true);
          BrowList       .addKeyListener(new KeyList());
          DeptFrame      .getContentPane().setLayout(new BorderLayout());
          DeptFrame      .getContentPane().add("South",TIndicator);
          DeptFrame      .getContentPane().add("Center",BrowScroll);
          DeptFrame      .show();

     public void showDepartmentFrame()
     {
          removeHelpFrame();
          try
          {
               Layer     .add(DeptFrame);
               DeptFrame .moveToFront();
               DeptFrame .setSelected(true);
               DeptFrame .show();
               BrowList  .requestFocus();
          }
          catch(Exception ex){}
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(DeptFrame);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

*/
