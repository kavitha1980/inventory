package Indent.IndentFiles;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class TableHeaderHandle extends MouseAdapter
{
     MaterialsSearch          matSearch;
     DepartmentSearch         deptSearch; 
     ClassificationSearch     classSearch;
     IndentMiddlePanel        IMP;
     MouseEvent               me;

     int iCount=0;
     
     public TableHeaderHandle(IndentMiddlePanel IMP)
     {
          this.IMP       = IMP;

          matSearch      = new MaterialsSearch(IMP.Layer,IMP.VCode,IMP.VName,IMP.VUomName,IMP.VNameCode,IMP,IMP.iMillCode,IMP.SItemTable,IMP.SSupTable,IMP.VGUserCode);
          deptSearch     = new DepartmentSearch(IMP.Layer,IMP.VDept,IMP.VDeptCode,IMP); 
          classSearch    = new ClassificationSearch(IMP.Layer,IMP.VCata,IMP.VCataCode,IMP);
     }
     
     public void mouseClicked(MouseEvent me)
     {
          this.me  = me;
     
          iCount++;

          String Scol = findColumnClicked(me);

          if(iCount<=1)
          {
               if(Scol.equals("Department"))
                    deptSearch.showDepartmentFrame(me); 
               else
               if(Scol.equals("Classification"))
                    classSearch.showClassificationFrame(me); 
               else
                    matSearch.showMaterialsFrame(me);
          }
          else
          {
               if(Scol.equals("Department"))
                    deptSearch.showDepartmentFrame(me); 
               else
               if(Scol.equals("Classification"))
                    classSearch.showClassificationFrame(me); 
          }

     }
     
     private String findColumnClicked(MouseEvent me)
     {
          String Scol="";
          TableColumnModel colModel = IMP.tabreport.ReportTable.getColumnModel();
          int iCol = colModel.getColumnIndexAtX(me.getX());
          Scol = IMP.ColumnData[iCol];
          return Scol;
     }
}
