package Indent.IndentFiles.Transfer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;
import Indent.IndentFiles.*;

public class ConcernTransferInvoiceFrame extends JInternalFrame
{
     String         SStDate,SEnDate;
     JComboBox      JCFilter;
     JButton        BApply,BInvoice;
     JLabel         LNet;
     JPanel         TopPanel,BottomPanel;
     JPanel         DatePanel,FilterPanel,SortPanel,BasisPanel,ApplyPanel;
     
     TabReport      tabreport;
     DateField      TStDate;
     DateField      TEnDate;
     
     JTabbedPane    theTab;
     
     Object         RowData[][];
     
     String         ColumnData[] = {"Trans Id","Trans Date","Code","Name","Qty","Rate","Tax(%)","TotalValue","Select"};
     String         ColumnType[] = {"N"       ,"S"         ,"S"   ,"S"   ,"N"  ,"N"   ,"N"     ,"N"         ,"B"     };
     Common         common = new Common();
     Vector         VAuthUserCode,VAuthUserName,VToMillCode,VToMillName;
     Vector         VTransNo,VTransDate,VTransCode,VTransName,VTransQty,VTransRate,VTransTax,VTransValue,VId;
     
     JLayeredPane   Layer;
     StatusPanel    SPanel;
     Vector         VCode,VName,VUom;
     int            iUserCode,iMillCode,iAuthCode;
     String         SItemTable,SSupTable,SMillName;

     int iInvoiceNo=0;

     Connection theConnection=null;

     boolean        bComflag  = true;
     
     public ConcernTransferInvoiceFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUom,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode,String SItemTable,String SSupTable,String SMillName)
     {
          super("Pending Concern Transfer List for Invoice During a Period");
          this.Layer      = Layer;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VUom       = VUom;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.iAuthCode  = iAuthCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                               theConnection =  oraConnection.getConnection();

               setAuthUsers();
     
               if(VToMillCode.size()>0)
               {
                    createComponents();
                    setLayouts();
                    addComponents();
                    addListeners();
               }
               else
               {
                    JOptionPane.showMessageDialog(null,"No Pending Items for Invoice","Information",JOptionPane.INFORMATION_MESSAGE);
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     
     public void createComponents()
     {
          TStDate        = new DateField();
          TEnDate        = new DateField();
          BApply         = new JButton("Apply");
          BInvoice       = new JButton("Create Invoice");
          
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          
          JCFilter       = new JComboBox(VToMillName);

          DatePanel      = new JPanel();
          FilterPanel    = new JPanel();
          SortPanel      = new JPanel();
          BasisPanel     = new JPanel();
          ApplyPanel     = new JPanel();

          LNet        = new JLabel("");
          LNet.setFont(new Font("monospaced", Font.BOLD, 14));
          
          TStDate        . setTodayDate();
          TEnDate        . setTodayDate();

          BApply.setEnabled(true);
          BInvoice.setEnabled(false);
     }
     
     public void setLayouts()
     {
          TopPanel       . setLayout(new FlowLayout());
          
          DatePanel      . setLayout(new GridLayout(1,3));
          FilterPanel    . setLayout(new BorderLayout());
          ApplyPanel     . setLayout(new BorderLayout());
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,700,500);
     }
     
     public void addComponents()
     {
          FilterPanel    . add("Center",JCFilter);

          DatePanel      . add(TStDate);
          DatePanel      . add(new JLabel(""));
          DatePanel      . add(TEnDate);
          
          ApplyPanel     . add("Center",BApply);
                           
          TopPanel       . add(FilterPanel);
          TopPanel       . add(DatePanel);
          TopPanel       . add(ApplyPanel);

          BottomPanel    . add(BInvoice);
          BottomPanel    . add(new JLabel("     Total Invoice Value     "));
          BottomPanel    . add(LNet);
          
          FilterPanel    . setBorder(new TitledBorder("List Only"));
          DatePanel      . setBorder(new TitledBorder("Period"));
          ApplyPanel     . setBorder(new TitledBorder("Control"));
          
          getContentPane(). add(TopPanel,BorderLayout.NORTH);
          getContentPane(). add(BottomPanel,BorderLayout.SOUTH);
     }
     
     public void addListeners()
     {
          BApply   .addActionListener(new ApplyList());
          BInvoice .addActionListener(new ActList());
          JCFilter .addItemListener(new ItemList());
     }
     
     public class ItemList implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
               BApply.setEnabled(true);
               BInvoice.setEnabled(false);
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    tabreport.setBorder(new TitledBorder("Transfer List"));

                    getContentPane().add(tabreport,BorderLayout.CENTER);
                    
                    setSelected(true);

                    if(VToMillCode.size()>1)
                    {
                         BApply.setEnabled(false);
                    }
                    else
                    {
                         BApply.setEnabled(true);
                    }

                    BInvoice.setEnabled(true);
                    setTotalValue();

                    Layer.repaint();
                    Layer.updateUI();

                    tabreport.ReportTable.addKeyListener(new KeyList1());
                    tabreport.ReportTable.addMouseListener(new MouseList1());
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(isValidData())
               {
                    createInvoice();
               }
          }
     }

     private class KeyList1 extends KeyAdapter
     {
         public void keyPressed(KeyEvent ke)
         {
            if(ke.getKeyCode()==KeyEvent.VK_SPACE)
            {
                setTotalValue();
            }
         }
     }

     public class MouseList1 extends MouseAdapter
     {
          public void mouseReleased(MouseEvent me)
          {
               if(me.getClickCount()==1 || me.getClickCount()==2)
               {
                    setTotalValue();
               }
          }
     }

     private void setTotalValue()
     {
          double dTValue = 0;

          for(int i=0;i<RowData.length;i++)
          {
               Boolean Bselected = (Boolean)RowData[i][8];
               if(Bselected.booleanValue())
               {
                    double dValue = common.toDouble((String)RowData[i][7]);

                    dTValue = dTValue + dValue;
               }
          }
          LNet.setText(common.getRound(dTValue,2));
          LNet.setForeground(Color.red);
     }

     public void setDataIntoVector()
     {
          VTransNo     = new Vector();
          VTransDate   = new Vector();
          VTransCode   = new Vector();
          VTransName   = new Vector();
          VTransQty    = new Vector();
          VTransRate   = new Vector();
          VTransTax    = new Vector();
          VTransValue  = new Vector();
          VId          = new Vector();
          
          String StDate = TStDate.toNormal();
          String EnDate = TEnDate.toNormal();
          
          String QString  = getQString(StDate,EnDate);
          try
          {
               Statement       stat          =  theConnection.createStatement();

               ResultSet result  = stat.executeQuery(QString);

               while (result.next())
               {
                    VTransNo     .addElement(common.parseNull(result.getString(1)));
                    VTransDate   .addElement(common.parseDate(result.getString(2)));
                    VTransCode   .addElement(common.parseNull(result.getString(3)));
                    VTransName   .addElement(common.parseNull(result.getString(4)));
                    VTransQty    .addElement(common.parseNull(result.getString(5)));
                    VTransRate   .addElement(common.parseNull(result.getString(6)));
                    VTransTax    .addElement(common.parseNull(result.getString(7)));
                    VTransValue  .addElement(common.parseNull(result.getString(8)));
                    VId          .addElement(common.parseNull(result.getString(9)));
               }
               result.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }
     
     public void setRowData()
     {
          RowData = new Object[VTransNo.size()][ColumnData.length];

          for(int i=0;i<VTransNo.size();i++)
          {
               RowData[i][0]  = (String)VTransNo.elementAt(i);
               RowData[i][1]  = (String)VTransDate.elementAt(i);
               RowData[i][2]  = (String)VTransCode.elementAt(i);
               RowData[i][3]  = (String)VTransName.elementAt(i);
               RowData[i][4]  = (String)VTransQty.elementAt(i);
               RowData[i][5]  = (String)VTransRate.elementAt(i);
               RowData[i][6]  = (String)VTransTax.elementAt(i);
               RowData[i][7]  = (String)VTransValue.elementAt(i);
               RowData[i][8]  = new Boolean(false);
          }
     }

     public String getQString(String StDate,String EnDate)
     {
          String QString  = "";
          
          int iToMillCode = common.toInt((String)VToMillCode.elementAt(JCFilter.getSelectedIndex()));

          QString  =     " SELECT ConcernTransfer.TranNo,ConcernTransfer.TranDate, "+
                         " ConcernTransfer.Code, InvItems.Item_Name, ConcernTransfer.Qty, ConcernTransfer.Rate,"+
                         " ConcernTransfer.TaxPer,ConcernTransfer.InvAmount,ConcernTransfer.Id "+
                         " FROM ConcernTransfer "+
                         " INNER JOIN InvItems ON ConcernTransfer.Code = InvItems.Item_Code "+
                         " and ConcernTransfer.InvoiceStatus=0 and ConcernTransfer.TranDate >= '"+StDate+"' and ConcernTransfer.TranDate <='"+EnDate+"' "+
                         " And ConcernTransfer.MillCode="+iMillCode+" and ConcernTransfer.ToMillCode="+iToMillCode+
                         " Order By 3,1,2 ";

          return QString;
     }

     public String getUserName(String SUserCode)
     {
          int iIndex = VAuthUserCode.indexOf(SUserCode);
          return (String)VAuthUserName.elementAt(iIndex);
     }

     public void setAuthUsers()
     {
          VAuthUserCode = new Vector();
          VAuthUserName = new Vector();

          VToMillCode   = new Vector();
          VToMillName   = new Vector();
          
          String QS1 = " Select distinct IndentAuthentication.AuthUserCode,RawUser.UserName "+
                       " from IndentAuthentication "+
                       " Inner join RawUser on RawUser.UserCode= IndentAuthentication.AuthUserCode"+
                       " Order by 2 ";

          String QS2 = " Select MillCode,ShortName from Mill Where MillCode in (Select distinct(ToMillCode) from ConcernTransfer Where InvoiceStatus=0 and MillCode="+iMillCode+")  order by 2 ";

          try
          {
               Statement       stat          =  theConnection.createStatement();

               ResultSet result  = stat.executeQuery(QS1);

               while (result.next())
               {
                    VAuthUserCode.addElement(common.parseNull(result.getString(1)));
                    VAuthUserName.addElement(common.parseNull(result.getString(2)));  
               }
               result.close();


               result  = stat.executeQuery(QS2);

               while (result.next())
               {
                    VToMillCode.addElement(common.parseNull(result.getString(1)));
                    VToMillName.addElement(common.parseNull(result.getString(2)));  
               }
               result.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     private boolean isValidData()
     {
          int iCount=0;

          String SPTaxPer="";

          for(int i=0;i<RowData.length;i++)
          {
               Boolean Bselected = (Boolean)RowData[i][8];

               if(!Bselected.booleanValue())
                    continue;



               String STaxPer = (String)RowData[i][6];

               if(!SPTaxPer.equals(STaxPer))
               {
                    if(iCount==0)
                    {
                         SPTaxPer = STaxPer;
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Different in TaxPer","Error",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }
               }

               iCount++;
          }

          if(iCount<=0)
          {
               JOptionPane.showMessageDialog(null,"No Entries Made","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          return true;
     }

     private void createInvoice()
     {
          getInvoiceNo();

          String SShortName = common.getShortName(iMillCode);
          String SInvNo = SShortName+"/ST-"+iInvoiceNo;

          updateInvoiceData(SInvNo);
          getACommit();
          removeHelpFrame();
     }

     private void getInvoiceNo()
     {
          iInvoiceNo=0;
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               
               String QS = " Select TransInvNo from Mill Where MillCode="+iMillCode+" for update of TransInvNo nowait";

               PreparedStatement thePrepare    = theConnection.prepareStatement(QS);

               ResultSet theResult             = thePrepare.executeQuery();
               if(theResult.next())
                  iInvoiceNo = theResult.getInt(1)+1;

               theResult.close();
               thePrepare.close();

               thePrepare = theConnection.prepareStatement(" Update Mill set TransInvNo=? Where MillCode=?");
               thePrepare.setInt(1,iInvoiceNo);
               thePrepare.setInt(2,iMillCode);
               thePrepare.executeUpdate();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);

               String SException = String.valueOf(ex);

               System.out.println("Invoice No: "+ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getInvoiceNo();
                    bComflag = true;
               }
               else
               {
                    bComflag = false;
               }
          }
     }

     private void updateInvoiceData(String SInvNo)
     {
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               
               String SDate = common.getServerPureDate();

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][8];
     
                    if(!Bselected.booleanValue())
                         continue;

                    Statement stat = theConnection.createStatement();

                    String SItemCode   = (String)VTransCode.elementAt(i);
                    String STransNo    = (String)VTransNo.elementAt(i);
                    String SId         = (String)VId.elementAt(i);

                    String QS = " Update ConcernTransfer set InvoiceStatus=1,InvNo='"+SInvNo+"',InvDate="+SDate+
                                " Where Code='"+SItemCode+"' and TranNo="+STransNo+" and Id="+SId+" and MillCode="+iMillCode;

                    stat.executeUpdate(QS);
                    stat.close();
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag = false;
          }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnection  . commit();
                    JOptionPane    . showMessageDialog(null,"Invoice Created Sucessfully");
                    System         . out.println("Commit");
               }
               else
               {
                    theConnection  . rollback();
                    JOptionPane    . showMessageDialog(null,"Sorry,Problem in Invoice Creation","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theConnection   . setAutoCommit(true);
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }


}
