package Indent.IndentFiles.Transfer;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;
import Indent.IndentFiles.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class ConcernTransferInvoicePrintFrame extends JInternalFrame
{
     String         SStDate,SEnDate;
     JComboBox      JCFilter;
     JButton        BApply,BPrint;
     JPanel         TopPanel,BottomPanel;
     JPanel         DatePanel,FilterPanel,SortPanel,BasisPanel,ApplyPanel;
     
     TabReport      tabreport;
     DateField      TStDate;
     DateField      TEnDate;
     
     JTabbedPane    theTab;
     
     Object         RowData[][];
     
     String         ColumnData[] = {"Invoice No","Invoice Date","Invoice Value","Concern","Select"};
     String         ColumnType[] = {"S"         ,"S"           ,"N"            ,"S"      ,"B"     };
     Common         common = new Common();
     Vector         VToMillCode,VToMillName;
     Vector         VInvNo,VInvDate,VInvValue,VConcern,VConcName,VConcCode;
     Vector         VMCode,VMName,VMUom,VMQty,VMRate,VMValue;
     String         STaxPer="0";
     
     JLayeredPane   Layer;
     StatusPanel    SPanel;
     Vector         VCode,VName,VUom;
     int            iUserCode,iMillCode,iAuthCode;
     String         SItemTable,SSupTable,SMillName;

     Connection theConnection=null;

     boolean        bComflag  = true;

     String SFile = common.getPrintPath()+"Invoice.pdf";

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font tinyBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font tinyNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     int    iWidth[] = {5,10,15,10,10,8,12,14,8,8};

     Document document;
     PdfPTable table;

     float fleft   = 16.0f;
     float fheight = 32.0f;

     int            Pctr      = 0;

     int iItemCount=50;

     double dTNet=0;


     public ConcernTransferInvoicePrintFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUom,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode,String SItemTable,String SSupTable,String SMillName)
     {
          super("Concern Transfer Invoice List During a Period");
          this.Layer      = Layer;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VUom       = VUom;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.iAuthCode  = iAuthCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                               theConnection =  oraConnection.getConnection();

               setMillData();
     
               if(VToMillCode.size()>1)
               {
                    createComponents();
                    setLayouts();
                    addComponents();
                    addListeners();
               }
               else
               {
                    JOptionPane.showMessageDialog(null,"No Pending Invoice for Print","Information",JOptionPane.INFORMATION_MESSAGE);
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     
     public void createComponents()
     {
          TStDate        = new DateField();
          TEnDate        = new DateField();
          BApply         = new JButton("Apply");
          BPrint         = new JButton("Print");
          
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          
          JCFilter       = new JComboBox(VToMillName);

          DatePanel      = new JPanel();
          FilterPanel    = new JPanel();
          SortPanel      = new JPanel();
          BasisPanel     = new JPanel();
          ApplyPanel     = new JPanel();

          TStDate        . setTodayDate();
          TEnDate        . setTodayDate();

          BApply.setEnabled(true);
          BPrint.setEnabled(false);

          VMCode   = new Vector();
          VMName   = new Vector();
          VMUom    = new Vector();
          VMQty    = new Vector();
          VMRate   = new Vector();
          VMValue  = new Vector();
     }
     
     public void setLayouts()
     {
          TopPanel       . setLayout(new FlowLayout());
          
          DatePanel      . setLayout(new GridLayout(1,3));
          FilterPanel    . setLayout(new BorderLayout());
          ApplyPanel     . setLayout(new BorderLayout());
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,600,500);
     }
     
     public void addComponents()
     {
          FilterPanel    . add("Center",JCFilter);

          DatePanel      . add(TStDate);
          DatePanel      . add(new JLabel(""));
          DatePanel      . add(TEnDate);
          
          ApplyPanel     . add("Center",BApply);
                           
          TopPanel       . add(FilterPanel);
          TopPanel       . add(DatePanel);
          TopPanel       . add(ApplyPanel);

          BottomPanel    . add(BPrint);
          
          FilterPanel    . setBorder(new TitledBorder("List Only"));
          DatePanel      . setBorder(new TitledBorder("Period"));
          ApplyPanel     . setBorder(new TitledBorder("Control"));
          
          getContentPane(). add(TopPanel,BorderLayout.NORTH);
          getContentPane(). add(BottomPanel,BorderLayout.SOUTH);
     }
     
     public void addListeners()
     {
          BApply   .addActionListener(new ApplyList());
          BPrint   .addActionListener(new ActList());
     }
     
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    tabreport.setBorder(new TitledBorder("Invoice List"));

                    getContentPane().add(tabreport,BorderLayout.CENTER);
                    
                    setSelected(true);

                    tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

                    BPrint.setEnabled(true);

                    Layer.repaint();
                    Layer.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(isValidData())
               {
                    printInvoice();
                    getACommit();
                    removeHelpFrame();
               }
          }
     }

     public void setDataIntoVector()
     {
          VInvNo     = new Vector();
          VInvDate   = new Vector();
          VInvValue  = new Vector();
          VConcern   = new Vector();
          VConcName  = new Vector();
          VConcCode  = new Vector();
          
          String StDate = TStDate.toNormal();
          String EnDate = TEnDate.toNormal();
          
          String QString  = getQString(StDate,EnDate);
          try
          {
               Statement       stat          =  theConnection.createStatement();

               ResultSet result  = stat.executeQuery(QString);

               while (result.next())
               {
                    VInvNo     .addElement(common.parseNull(result.getString(1)));
                    VInvDate   .addElement(common.parseDate(result.getString(2)));
                    VInvValue  .addElement(common.getRound(result.getDouble(3),2));
                    VConcern   .addElement(common.parseNull(result.getString(4)));
                    VConcName  .addElement(common.parseNull(result.getString(5)));
                    VConcCode  .addElement(common.parseNull(result.getString(6)));
               }
               result.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }
     
     public void setRowData()
     {
          RowData = new Object[VInvNo.size()][ColumnData.length];

          for(int i=0;i<VInvNo.size();i++)
          {
               RowData[i][0]  = (String)VInvNo.elementAt(i);
               RowData[i][1]  = (String)VInvDate.elementAt(i);
               RowData[i][2]  = (String)VInvValue.elementAt(i);
               RowData[i][3]  = (String)VConcern.elementAt(i);
               RowData[i][4]  = new Boolean(false);
          }
     }

     public String getQString(String StDate,String EnDate)
     {
          String QString  = "";
          
          int iToMillCode = common.toInt((String)VToMillCode.elementAt(JCFilter.getSelectedIndex()));

          QString  =     " SELECT ConcernTransfer.InvNo, ConcernTransfer.InvDate, "+
                         " sum(ConcernTransfer.InvAmount), Mill.ShortName, Mill.MillName, "+
                         " ConcernTransfer.ToMillCode "+
                         " FROM ConcernTransfer "+
                         " INNER JOIN Mill ON ConcernTransfer.ToMillCode = Mill.MillCode "+
                         " and ConcernTransfer.InvoiceStatus=1 and ConcernTransfer.PrintStatus=0 "+
                         " and ConcernTransfer.InvDate >= '"+StDate+"' and ConcernTransfer.InvDate <='"+EnDate+"' "+
                         " and ConcernTransfer.MillCode="+iMillCode;

          if(JCFilter.getSelectedIndex()>0)
          {
               QString = QString + " and ConcernTransfer.ToMillCode="+iToMillCode;
          }

          QString = QString + " Group by ConcernTransfer.InvNo, ConcernTransfer.InvDate, Mill.ShortName, Mill.MillName, ConcernTransfer.ToMillCode "+
                              " Order By 1 ";

          return QString;
     }

     public void setMillData()
     {
          VToMillCode   = new Vector();
          VToMillName   = new Vector();

          VToMillCode.addElement("99");
          VToMillName.addElement("ALL");
          
          String QS1 = " Select MillCode,ShortName from Mill Where MillCode in (Select distinct(ToMillCode) from ConcernTransfer Where InvoiceStatus=1 and PrintStatus=0 and MillCode="+iMillCode+")  order by 2 ";

          try
          {
               Statement       stat          =  theConnection.createStatement();

               ResultSet result  = stat.executeQuery(QS1);

               while (result.next())
               {
                    VToMillCode.addElement(common.parseNull(result.getString(1)));
                    VToMillName.addElement(common.parseNull(result.getString(2)));  
               }
               result.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     private boolean isValidData()
     {
          int iCount=0;

          for(int i=0;i<RowData.length;i++)
          {
               Boolean Bselected = (Boolean)RowData[i][4];

               if(!Bselected.booleanValue())
                    continue;

               iCount++;
          }

          if(iCount<=0)
          {
               JOptionPane.showMessageDialog(null,"No Row Selected","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          return true;
     }

     private void updateInvoiceData(String SInvNo)
     {
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               

               Statement stat = theConnection.createStatement();

               String QS = " Update ConcernTransfer set PrintStatus=1 Where PrintStatus=0 and InvNo='"+SInvNo+"' and MillCode="+iMillCode;

               stat.executeUpdate(QS);
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag = false;
          }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnection  . commit();
                    JOptionPane    . showMessageDialog(null,"Invoice File Created Sucessfully");
                    System         . out.println("Commit");
               }
               else
               {
                    theConnection  . rollback();
                    JOptionPane    . showMessageDialog(null,"Sorry,Problem in Invoice Printing","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theConnection   . setAutoCommit(true);
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

     public void printInvoice()
     {
          try
          {
               document = new Document(PageSize.A4);
               PdfWriter.getInstance(document, new FileOutputStream(SFile));
               document.open();

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][4];
     
                    if(!Bselected.booleanValue())
                         continue;

                    table = new PdfPTable(10);
                    table.setWidths(iWidth);
                    table.setWidthPercentage(100);
     
                    String SInvNo    = (String)RowData[i][0];
                    String SInvDate  = (String)RowData[i][1];
                    String SConcern  = (String)VConcName.elementAt(i);
                    int    iConcCode = common.toInt((String)VConcCode.elementAt(i));
                    getData(SInvNo);
                    addHead(document,table,SInvNo,SInvDate,SConcern,iConcCode);
                    addBody(document,table,SInvNo,SInvDate,SConcern,iConcCode);
                    addFoot(document,table,0);

                    updateInvoiceData(SInvNo);

                    Pctr       = 0;
                    iItemCount = 50;
               }

               document.close();
          }
          catch (Exception e)
          {
               bComflag = false;
               e.printStackTrace();
          }
     }

     public void getData(String SInvNo)
     {
          VMCode   .removeAllElements();
          VMName   .removeAllElements();
          VMUom    .removeAllElements();
          VMQty    .removeAllElements();
          VMRate   .removeAllElements();
          VMValue  .removeAllElements();

          STaxPer = "0";

          String QS = " Select ConcernTransfer.Code,InvItems.Item_Name,Uom.UomName, "+
                      " ConcernTransfer.Qty,ConcernTransfer.InvRate,ConcernTransfer.TaxPer "+
                      " from ConcernTransfer "+
                      " inner join InvItems on ConcernTransfer.Code = InvItems.Item_Code "+
                      " and ConcernTransfer.InvNo='"+SInvNo+"' and ConcernTransfer.MillCode="+iMillCode+
                      " inner join Uom on InvItems.UomCode = Uom.UomCode "+
                      " Order by 1 ";

          try
          {
               Statement       stat          =  theConnection.createStatement();

               ResultSet result  = stat.executeQuery(QS);

               while (result.next())
               {
                    double dQty   = result.getDouble(4);
                    double dRate  = result.getDouble(5);
                    double dValue = dQty * dRate;

                    VMCode .addElement(common.parseNull(result.getString(1)));
                    VMName .addElement(common.parseDate(result.getString(2)));
                    VMUom  .addElement(common.parseDate(result.getString(3)));
                    VMQty  .addElement(common.getRound(dQty,3));
                    VMRate .addElement(common.getRound(dRate,4));
                    VMValue.addElement(common.getRound(dValue,2));

                    STaxPer = common.parseNull(result.getString(6));
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }


     private void addHead(Document document,PdfPTable table,String SInvNo,String SInvDate,String SConcern,int iConcCode) throws BadElementException
     {
          try
          {
               if(iItemCount<12)
                    return;

               if(Pctr > 0)
               {
                    addFoot(document,table,1);
                    iItemCount=0;
               }
               else
               {
                    iItemCount=0;
               }

               Pctr++;

               document.newPage();

               table.flushContent();

               table = new PdfPTable(10);
               table.setWidths(iWidth);
               table.setWidthPercentage(100);


               PdfPCell c1;

               String Str1  = "";
               String Str2  = "";
               String Str3  = "";
               String Str4  = "";
               String Str5  = "";

               String Str6  = " C.EX.RC.No.";
               String Str7  = " ECC No ";
               String Str8  = " PLA No ";
               String Str9  = " RANGE";
               String Str10 = " DIVISION";
               String Str11 = " COMMISSIONERATE";
               String Str12 = " Tariff Heading & Sub Heading No : 5510 90 90 ";

               String Str13  = "To,";
               String Str14  = "M/s. "+SConcern;
               String Str15  = "Gobi Main Road, Pudusuripalayam,";
               String Str16  = "Nambiyur - 638 458,";
               String Str17  = "Gobi (TK), Erode (Dt)";

               String SConcTinNo = "";

               String SRCNo       = "";
               String SECCNo      = "";
               String SPLANo      = "";
               String SRange      = "";
               String SDivision   = "";
               String SCommission = "";

               if(iMillCode==0)
               {
                    Str1  = SMillName;
                    Str2  = " Mill : Gobi Main Road, Pudusuripalayam, Nambiyur - 638 458, Gobi (TK), Erode (Dt), Tamilnadu, India. ";
                    Str3  = " TEL : 04285 - 267201,267301    FAX : 04285-267565    TIN No : 33632960864    CST No : 440691 dt.24.09.1990 ";
                    Str4  = " Regd. Office : Amarjothi House, 157, Kumaran Road, Tirupur - 641 601, Tamilnadu, India. ";
                    Str5  = " TEL : 0421 - 4311600     FAX : 0421 - 4326694     E-Mail : info@amarjothi.net, www.amarjothi.net ";

                    SRCNo       = "AAFCA7082CXM001";
                    SECCNo      = "AAFCA7082CXM001";
                    SPLANo      = "59/93";
                    SRange      = "47, KVP Plaza, Cutcherry Road, Gobichettipalayam - 638452";
                    SDivision   = "Division-II, 81, Sathy Road, Erode - 638004";
                    SCommission = "Anai Medu, Salem - 636 007";
               }

               if(iMillCode==3)
               {
                    Str1  = SMillName;
                    Str2  = " Mill : Gobi Main Road, Pudusuripalayam, Nambiyur - 638 458, Gobi (TK), Erode (Dt), Tamilnadu, India. ";
                    Str3  = " TEL : 04285 - 267404    FAX : 04285-267575    TIN No : 33712404816    CST No : 1031365 dt. 05.05.2010 ";
                    Str4  = " Regd. Office : Amarjothi House, 157, Kumaran Road, Tirupur - 641 601, Tamilnadu, India. ";
                    Str5  = " TEL : 0421 - 4311600     FAX : 0421 - 4326694     E-Mail : info@amarjothi.net, www.amarjothi.net ";

                    SRCNo       = "AAICA0275AEM001";
                    SECCNo      = "AAICA0275AEM001";
                    SPLANo      = "";
                    SRange      = "47, KVP Plaza, Cutcherry Road, Gobichettipalayam - 638452";
                    SDivision   = "Division-II, 81, Sathy Road, Erode - 638004";
                    SCommission = "Anai Medu, Salem - 636 007";
               }

               if(iMillCode==4)
               {
                    Str1  = SMillName;
                    Str2  = " Mill : Gobi Main Road, Pudusuripalayam, Nambiyur - 638 458, Gobi (TK), Erode (Dt), Tamilnadu, India. ";
                    Str3  = " TEL : 04285 - 267201,267301    FAX : 04285-267565    TIN No : 33612404386    CST No : 972942 dt. 14.12.2008 ";
                    Str4  = " Regd. Office : Amarjothi House, 157, Kumaran Road, Tirupur - 641 601, Tamilnadu, India. ";
                    Str5  = " TEL : 0421 - 4311600     FAX : 0421 - 4326694     E-Mail : info@amarjothi.net, www.amarjothi.net ";

                    SRCNo       = "";
                    SECCNo      = "";
                    SPLANo      = "";
                    SRange      = "47, KVP Plaza, Cutcherry Road, Gobichettipalayam - 638452";
                    SDivision   = "Division-II, 81, Sathy Road, Erode - 638004";
                    SCommission = "Anai Medu, Salem - 636 007";
               }

               if(iConcCode==0)
               {
                    SConcTinNo = "33632960864";
               }

               if(iConcCode==3)
               {
                    SConcTinNo = "33712404816";
               }

               if(iConcCode==4)
               {
                    SConcTinNo = "33612404386";
               }

               c1 = new PdfPCell(new Phrase(Str1,bigBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(10);
               c1.setBorder(Rectangle.LEFT | Rectangle.TOP | Rectangle.RIGHT);
               table.addCell(c1);

               addEmptySpanRow(table,"Left","Right");
               addEmptySpanRow(table,"Left","Right");

               c1 = new PdfPCell(new Phrase(Str2,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(10);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str3,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(10);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               addEmptySpanRow(table,"Left","Right");
               addEmptySpanRow(table,"Left","Right");

               c1 = new PdfPCell(new Phrase(Str4,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(10);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str5,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(10);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               addEmptySpanRow(table,"Left","Right");
               addEmptySpanRow(table,"Left","Right");

               c1 = new PdfPCell(new Phrase(Str6,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(3);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(": "+SRCNo,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(7);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str7,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(3);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(": "+SECCNo,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(7);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str8,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(3);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(": "+SPLANo,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(7);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str9,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(3);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(": "+SRange,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(7);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str10,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(3);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(": "+SDivision,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(7);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str11,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(3);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(": "+SCommission,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(7);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);

               addEmptySpanRow(table,"Left","Right");
               addEmptySpanRow(table,"Left","Right");

               c1 = new PdfPCell(new Phrase(Str12,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(10);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               addEmptySpanRow(table,"Left","Right");


               c1 = new PdfPCell(new Phrase("INVOICE",mediumBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(10);
               c1.setFixedHeight(fheight);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("NO : "+SInvNo,mediumBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(5);
               c1.setFixedHeight(fheight);
               c1.setPaddingLeft(fleft);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("DATE : "+SInvDate,mediumBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(5);
               c1.setPaddingLeft(fleft);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str13,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(7);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(3);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str14,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(7);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("TIN No : "+SConcTinNo,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setColspan(3);
               c1.setPaddingRight(fleft);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str15,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(7);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(3);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str16,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(7);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(3);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str17,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(7);
               c1.setPaddingLeft(fleft);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(3);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase("S.No.",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("ItemCode",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("ItemName",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(3);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("UOM",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("Quantity",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("Rate",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("Total Value",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(2);
               table.addCell(c1);

               document.add(table);
          }    
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     private void addBody(Document document,PdfPTable table,String SInvNo,String SInvDate,String SConcern,int iConcCode) throws BadElementException
     {
          try
          {
               int iCount=0;

               PdfPCell c1;

               dTNet=0;

               for(int i=0;i<VMCode.size();i++)
               {
                    addHead(document,table,SInvNo,SInvDate,SConcern,iConcCode);

                    iItemCount++;

                    String SCode        = (String)VMCode   . elementAt(i);
                    String SUOM         = (String)VMUom    . elementAt(i);
                    String SName        = (String)VMName   . elementAt(i);
                    String SQty         = (String)VMQty    . elementAt(i);
                    String SRate        = (String)VMRate   . elementAt(i);
                    String SValue       = (String)VMValue  . elementAt(i);
     
                    dTNet = dTNet + common.toDouble(SValue);

                    try
                    {
                         c1 = new PdfPCell(new Phrase(common.Pad(String.valueOf(i+1),4),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.Pad(SCode,9),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.Pad(SName,35),tinyNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setVerticalAlignment(Element.ALIGN_BOTTOM);
                         c1.setColspan(3);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.Pad(SUOM,6),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SQty,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SRate,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SValue,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setColspan(2);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);


                         c1 = new PdfPCell(new Phrase("",smallNormal));
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase("",smallNormal));
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase("",tinyNormal));
                         c1.setColspan(3);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase("",smallNormal));
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase("",smallNormal));
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase("",smallNormal));
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase("",smallNormal));
                         c1.setColspan(2);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                         table.addCell(c1);
                    }
                    catch(Exception ex){}
               }
          }    
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     private void addFoot(Document document,PdfPTable table,int iSig) throws BadElementException
     {
          try
          {
               PdfPCell c1;

               if(iSig==1)
               {
                    c1 = new PdfPCell(new Phrase("",smallNormal));
                    c1.setColspan(10);
                    c1.setBorder(Rectangle.TOP);
                    table.addCell(c1);
               }
               else
               {
                    double dTax    = dTNet * common.toDouble(STaxPer) / 100;

                    String SGrand  = common.getRound(dTNet+dTax,2);

                    String SRupees = common.getRupee(SGrand);
                    
                    float f = 20.0f;
     
                    c1 = new PdfPCell(new Phrase("",smallNormal));
                    c1.setFixedHeight(f);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("",smallNormal));
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("",smallNormal));
                    c1.setColspan(3);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("VAT / CST : "+STaxPer+"%",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setColspan(3);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(common.getRound(dTax,2),smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setColspan(2);
                    table.addCell(c1);


                    c1 = new PdfPCell(new Phrase("",smallNormal));
                    c1.setFixedHeight(f);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("",smallNormal));
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("",smallNormal));
                    c1.setColspan(3);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("Grand Total",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setColspan(3);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(SGrand,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setColspan(2);
                    table.addCell(c1);


                    c1 = new PdfPCell(new Phrase(SRupees,smallBold));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setColspan(10);
                    c1.setPaddingLeft(fleft);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    table.addCell(c1);


                    addEmptySpanRow(table,"Left","Right");
                    addEmptySpanRow(table,"Left","Right");

                    c1 = new PdfPCell(new Phrase("E & OE",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setColspan(10);
                    c1.setPaddingLeft(fleft);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                    table.addCell(c1);


                    c1 = new PdfPCell(new Phrase("For "+common.getCapitalWordString(SMillName),smallBold));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setColspan(10);
                    c1.setPaddingRight(fleft);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    table.addCell(c1);

                    addEmptySpanRow(table,"Left","Right");
                    addEmptySpanRow(table,"Left","Right");
                    addEmptySpanRow(table,"Left","Right");
                    addEmptySpanRow(table,"Left","Right");
                    addEmptySpanRow(table,"Left","Right");
                    addEmptySpanRow(table,"Left","Right");

                    c1 = new PdfPCell(new Phrase("Prepared By",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setColspan(2);
                    c1.setPaddingLeft(fleft);
                    c1.setBorder(Rectangle.LEFT);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("Checked By",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setColspan(3);
                    c1.setBorder(Rectangle.NO_BORDER);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("Authorized Signatory",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setColspan(5);
                    c1.setPaddingRight(fleft);
                    c1.setBorder(Rectangle.RIGHT);
                    table.addCell(c1);


                    c1 = new PdfPCell(new Phrase("",smallNormal));
                    c1.setColspan(10);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                    table.addCell(c1);

               }

               document.add(table);


               String Str15="";

               if(iSig==0)
                    Str15 = "<End Of Report>";
               else
                    Str15 = "(Continued on Next Page)";

               Paragraph paragraph = new Paragraph("Page - "+Pctr,smallNormal);
               paragraph.setAlignment(Element.ALIGN_RIGHT);
               document.add(paragraph);


               paragraph = new Paragraph(Str15,smallNormal);
               paragraph.setAlignment(Element.ALIGN_LEFT);
               document.add(paragraph);

          }    
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     private void addEmptyCell(PdfPTable table)
     {
          PdfPCell c1 = new PdfPCell();
          table.addCell(c1);
     }

     private void addEmptyCell(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
          table.addCell(c1);
     }

     private void addEmptyRow(PdfPTable table)
     {
          for(int i=0;i<iWidth.length;i++)
          {
               PdfPCell c1 = new PdfPCell();
               table.addCell(c1);
          }
     }

     private void addEmptyRow(PdfPTable table,String SLeft,String SRight)
     {
          for(int i=0;i<iWidth.length;i++)
          {
               PdfPCell c1 = new PdfPCell();
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);
          }
     }

     private void addEmptySpanRow(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
          c1.setColspan(iWidth.length);
          table.addCell(c1);
     }

     private static void addEmptyLine(Paragraph paragraph, int number)
     {
          for (int i = 0; i < number; i++)
          {
               paragraph.add(new Paragraph(" "));
          }
     }


}
