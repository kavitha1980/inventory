package Indent.IndentFiles.Transfer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;
import Indent.IndentFiles.*;

public class ConcernTransferListFrame extends JInternalFrame
{
     String         SStDate,SEnDate;
     JComboBox      JCFilter;
     JButton        BApply,BPrint;
     JPanel         TopPanel,BottomPanel;
     JPanel         DatePanel,FilterPanel,SortPanel,BasisPanel,ApplyPanel;
     
     TabReport      tabreport;
     DateField      TStDate;
     DateField      TEnDate;
     
     JTabbedPane    theTab;
     
     Object         RowData[][];
     
     String         ColumnData[] = {"Trans Type","Trans Id","Trans Date","Code","Name","Qty","Rate","Concern","From User","To User"};
     String         ColumnType[] = {"S"         ,"N"       ,"S"         ,"S"   ,"S"   ,"N"  ,"N"   ,"S"      ,"S"        ,"S"      };
     Common         common = new Common();
     Vector         VAuthUserCode,VAuthUserName;
     Vector         VTransType,VTransNo,VTransDate,VTransCode,VTransName,VTransQty,VTransRate,VTransConc,VFromUser,VToUser;
     
     JLayeredPane   Layer;
     StatusPanel    SPanel;
     Vector         VCode,VName,VUom;
     int            iUserCode,iMillCode,iAuthCode;
     String         SItemTable,SSupTable,SMillName;
     
     public ConcernTransferListFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUom,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode,String SItemTable,String SSupTable,String SMillName)
     {
          super("Concern Transfer List During a Period");
          this.Layer      = Layer;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VUom       = VUom;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.iAuthCode  = iAuthCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;

          setAuthUsers();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     
     public void createComponents()
     {
          TStDate        = new DateField();
          TEnDate        = new DateField();
          BApply         = new JButton("Apply");
          BPrint         = new JButton("Print");
          
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          
          JCFilter       = new JComboBox();
          
          DatePanel      = new JPanel();
          FilterPanel    = new JPanel();
          SortPanel      = new JPanel();
          BasisPanel     = new JPanel();
          ApplyPanel     = new JPanel();
          
          TStDate        . setTodayDate();
          TEnDate        . setTodayDate();
     }
     
     public void setLayouts()
     {
          TopPanel       . setLayout(new FlowLayout());
          
          DatePanel      . setLayout(new GridLayout(1,3));
          FilterPanel    . setLayout(new BorderLayout());
          ApplyPanel     . setLayout(new BorderLayout());
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          JCFilter       . addItem("All");
          JCFilter       . addItem("Received");
          JCFilter       . addItem("Issued");
          
          FilterPanel    . add("Center",JCFilter);
          
          DatePanel      . add(TStDate);
          DatePanel      . add(new JLabel(""));
          DatePanel      . add(TEnDate);
          
          ApplyPanel     . add("Center",BApply);
                           
          TopPanel       . add(FilterPanel);
          TopPanel       . add(DatePanel);
          TopPanel       . add(ApplyPanel);

          BottomPanel    . add(BPrint);
          
          FilterPanel    . setBorder(new TitledBorder("List Only"));
          DatePanel      . setBorder(new TitledBorder("Period"));
          ApplyPanel     . setBorder(new TitledBorder("Control"));
          
          getContentPane(). add(TopPanel,BorderLayout.NORTH);
          getContentPane(). add(BottomPanel,BorderLayout.SOUTH);
     }
     
     public void addListeners()
     {
          BApply   .addActionListener(new ApplyList());
          BPrint   .addActionListener(new PrintList());
     }
     
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    tabreport.setBorder(new TitledBorder("Transfer List"));

                    getContentPane().add(tabreport,BorderLayout.CENTER);
                    
                    setSelected(true);
                    Layer.repaint();
                    Layer.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }
     
     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               printData();
          }
     }

     public void setDataIntoVector()
     {
          VTransType   = new Vector();
          VTransNo     = new Vector();
          VTransDate   = new Vector();
          VTransCode   = new Vector();
          VTransName   = new Vector();
          VTransQty    = new Vector();
          VTransRate   = new Vector();
          VTransConc   = new Vector();
          VFromUser    = new Vector();
          VToUser      = new Vector();

          
          String StDate = TStDate.toNormal();
          String EnDate = TEnDate.toNormal();
          
          String QString  = getQString(StDate,EnDate);
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();

               ResultSet result  = stat.executeQuery(QString);

               while (result.next())
               {
                    String SFUserCode = result.getString(9);
                    String STUserCode = result.getString(10);

                    String SFUserName = getUserName(SFUserCode);
                    String STUserName = getUserName(STUserCode);
                    
                    VTransType   .addElement(common.parseNull(result.getString(1)));
                    VTransNo     .addElement(common.parseNull(result.getString(2)));
                    VTransDate   .addElement(common.parseDate(result.getString(3)));
                    VTransCode   .addElement(common.parseNull(result.getString(4)));
                    VTransName   .addElement(common.parseNull(result.getString(5)));
                    VTransQty    .addElement(common.parseNull(result.getString(6)));
                    VTransRate   .addElement(common.parseNull(result.getString(7)));
                    VTransConc   .addElement(common.parseNull(result.getString(8)));
                    VFromUser    .addElement(SFUserName);
                    VToUser      .addElement(STUserName);
               }
               result.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }
     
     public void setRowData()
     {
          RowData = new Object[VTransType.size()][ColumnData.length];

          for(int i=0;i<VTransType.size();i++)
          {
               RowData[i][0]  = (String)VTransType.elementAt(i);
               RowData[i][1]  = (String)VTransNo.elementAt(i);
               RowData[i][2]  = (String)VTransDate.elementAt(i);
               RowData[i][3]  = (String)VTransCode.elementAt(i);
               RowData[i][4]  = (String)VTransName.elementAt(i);
               RowData[i][5]  = (String)VTransQty.elementAt(i);
               RowData[i][6]  = (String)VTransRate.elementAt(i);
               RowData[i][7]  = (String)VTransConc.elementAt(i);
               RowData[i][8]  = (String)VFromUser.elementAt(i);
               RowData[i][9]  = (String)VToUser.elementAt(i);
          }
     }

     public String getQString(String StDate,String EnDate)
     {
          String QString  = "";
          
          if(JCFilter.getSelectedIndex() == 0)
          {
               QString  =     " Select TransType,TranNo,TranDate,Code,Item_Name,Qty,Rate,MillName,FromUserCode,ToUserCode from "+
                              " (SELECT 'Receipt' as TransType,ConcernTransfer.TranNo,ConcernTransfer.TranDate, "+
                              " ConcernTransfer.Code, InvItems.Item_Name, ConcernTransfer.Qty, ConcernTransfer.Rate,"+
                              " Mill.ShortName as MillName,ConcernTransfer.AuthUserCode as FromUserCode,ConcernTransfer.ToUserCode "+
                              " FROM ConcernTransfer "+
                              " INNER JOIN InvItems ON ConcernTransfer.Code = InvItems.Item_Code "+
                              " INNER JOIN Mill ON ConcernTransfer.MillCode = Mill.MillCode "+
                              " Where ConcernTransfer.TranDate >= '"+StDate+"' and ConcernTransfer.TranDate <='"+EnDate+"' "+
                              " And ConcernTransfer.ToMillCode="+iMillCode+
                              " Union All "+
                              " SELECT 'Issue' as TransType,ConcernTransfer.TranNo,ConcernTransfer.TranDate, "+
                              " ConcernTransfer.Code, InvItems.Item_Name, ConcernTransfer.Qty, ConcernTransfer.Rate,"+
                              " Mill.ShortName as MillName,ConcernTransfer.AuthUserCode as FromUserCode,ConcernTransfer.ToUserCode "+
                              " FROM ConcernTransfer "+
                              " INNER JOIN InvItems ON ConcernTransfer.Code = InvItems.Item_Code "+
                              " INNER JOIN Mill ON ConcernTransfer.ToMillCode = Mill.MillCode "+
                              " Where ConcernTransfer.TranDate >= '"+StDate+"' and ConcernTransfer.TranDate <='"+EnDate+"' "+
                              " And ConcernTransfer.MillCode="+iMillCode+
                              " )";
          }

          if(JCFilter.getSelectedIndex() == 1)
          {
               QString  =     " SELECT 'Receipt' as TransType,ConcernTransfer.TranNo,ConcernTransfer.TranDate, "+
                              " ConcernTransfer.Code, InvItems.Item_Name, ConcernTransfer.Qty, ConcernTransfer.Rate,"+
                              " Mill.ShortName,ConcernTransfer.AuthUserCode as FromUserCode,ConcernTransfer.ToUserCode "+
                              " FROM ConcernTransfer "+
                              " INNER JOIN InvItems ON ConcernTransfer.Code = InvItems.Item_Code "+
                              " INNER JOIN Mill ON ConcernTransfer.MillCode = Mill.MillCode "+
                              " Where ConcernTransfer.TranDate >= '"+StDate+"' and ConcernTransfer.TranDate <='"+EnDate+"' "+
                              " And ConcernTransfer.ToMillCode="+iMillCode;
          }
          if(JCFilter.getSelectedIndex() == 2)
          {
               QString  =     " SELECT 'Issue' as TransType,ConcernTransfer.TranNo,ConcernTransfer.TranDate, "+
                              " ConcernTransfer.Code, InvItems.Item_Name, ConcernTransfer.Qty, ConcernTransfer.Rate,"+
                              " Mill.ShortName,ConcernTransfer.AuthUserCode as FromUserCode,ConcernTransfer.ToUserCode "+
                              " FROM ConcernTransfer "+
                              " INNER JOIN InvItems ON ConcernTransfer.Code = InvItems.Item_Code "+
                              " INNER JOIN Mill ON ConcernTransfer.ToMillCode = Mill.MillCode "+
                              " Where ConcernTransfer.TranDate >= '"+StDate+"' and ConcernTransfer.TranDate <='"+EnDate+"' "+
                              " And ConcernTransfer.MillCode="+iMillCode;
          }

          QString = QString+" Order By 3,1,2";

          return QString;
     }

     public String getUserName(String SUserCode)
     {
          int iIndex = VAuthUserCode.indexOf(SUserCode);
          return (String)VAuthUserName.elementAt(iIndex);
     }

     public void setAuthUsers()
     {
          VAuthUserCode = new Vector();
          VAuthUserName = new Vector();
          
          String QS = " Select distinct IndentAuthentication.AuthUserCode,RawUser.UserName "+
                      " from IndentAuthentication "+
                      " Inner join RawUser on RawUser.UserCode= IndentAuthentication.AuthUserCode"+
                      " Order by 2 ";

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();

               ResultSet result  = stat.executeQuery(QS);

               while (result.next())
               {
                    VAuthUserCode.addElement(common.parseNull(result.getString(1)));
                    VAuthUserName.addElement(common.parseNull(result.getString(2)));  
               }
               result.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void printData()
     {
          String STitle = "Concern Transfer List From "+TStDate.toString()+" To "+TEnDate.toString()+" Basis : "+(String)JCFilter.getSelectedItem(); 

          String SFile  = "TransList.prn";
          new DocPrint(getPendingBody(),getPendingHead(),STitle,SFile,iMillCode,SMillName);
     }

     public Vector getPendingBody()
     {
        Vector vect = new Vector();

        int iCount=0;


          for(int i=0;i<RowData.length;i++)
          {
              iCount++;

              String strl="";

              String SQty  = common.getRound(common.toDouble((String)VTransQty.elementAt(i)),3);
              String SRate = common.getRound(common.toDouble((String)VTransRate.elementAt(i)),4);

              strl = strl + common.Rad(String.valueOf(iCount),5)+common.Space(3);
              strl = strl + common.Pad((String)VTransType.elementAt(i),10)+common.Space(3);
              strl = strl + common.Pad((String)VTransNo.elementAt(i),10)+common.Space(3);
              strl = strl + common.Pad((String)VTransDate.elementAt(i),10)+common.Space(3);
              strl = strl + common.Pad((String)VTransCode.elementAt(i),12)+common.Space(3);
              strl = strl + common.Pad((String)VTransName.elementAt(i),50)+common.Space(3);
              strl = strl + common.Rad(SQty,15)+common.Space(3);
              strl = strl + common.Rad(SRate,15)+common.Space(3);
              strl = strl + common.Pad((String)VTransConc.elementAt(i),7)+common.Space(3);
              strl = strl + common.Pad((String)VFromUser.elementAt(i),15)+common.Space(3);
              strl = strl + common.Pad((String)VToUser.elementAt(i),15)+common.Space(3);
              
              strl = strl+"\n";
              vect.addElement(strl);
        }
        return vect;
     }

     public Vector getPendingHead()
     {
           Vector vect = new Vector();

           String strl1 = "";

              strl1 = strl1 + common.Cad("S.No.",5)+common.Space(3);
              strl1 = strl1 + common.Cad("Trans Type",10)+common.Space(3);
              strl1 = strl1 + common.Cad("Trans No",10)+common.Space(3);
              strl1 = strl1 + common.Cad("Trans Date",10)+common.Space(3);
              strl1 = strl1 + common.Pad("Code",12)+common.Space(3);
              strl1 = strl1 + common.Pad("Name",50)+common.Space(3);
              strl1 = strl1 + common.Cad("Qty",15)+common.Space(3);
              strl1 = strl1 + common.Cad("Rate",15)+common.Space(3);
              strl1 = strl1 + common.Cad("Concern",7)+common.Space(3);
              strl1 = strl1 + common.Pad("From User",15)+common.Space(3);
              strl1 = strl1 + common.Pad("To User",15)+common.Space(3);

           strl1 = strl1+"\n";

           vect.addElement(strl1);

           return vect;
     } 



}
