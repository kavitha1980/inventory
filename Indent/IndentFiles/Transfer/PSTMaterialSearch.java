package Indent.IndentFiles.Transfer;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class PSTMaterialSearch extends JInternalFrame
{
     JLayeredPane             Layer;
     PartyStockTransferMiddlePanel MiddlePanel;
     int                      iRow;
     int                      iMillCode;
     String                   SItemTable,SSupTable,SAuthUserCode;
     String                   SSupCode;
     Vector                   VNameCode;
     Vector                   VName,VCode,VUom,VStock,VStockRate;
     
     JTextField               TIndicator;          
     JList                    BrowList;
     JScrollPane              BrowScroll;
     JPanel                   BottomPanel;
     

     String                   SName="",SCode="";
     String                   str="";
     Common                   common = new Common();

     PSTMaterialSearch(JLayeredPane Layer,PartyStockTransferMiddlePanel MiddlePanel,int iRow,int iMillCode,String SItemTable,String SSupTable,String SAuthUserCode,String SSupCode,Vector VCode,Vector VName,Vector VUom,Vector VNameCode,Vector VStock,Vector VStockRate)
     {
          this.Layer          = Layer;
          this.MiddlePanel    = MiddlePanel;
          this.iRow           = iRow;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;
          this.SAuthUserCode  = SAuthUserCode;
          this.SSupCode       = SSupCode;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VUom           = VUom;
          this.VNameCode      = VNameCode;
          this.VStock         = VStock;
          this.VStockRate     = VStockRate;

          BrowList            = new JList(VNameCode);
          BrowList            .setFont(new Font("monospaced", Font.PLAIN, 11));
          
          BrowScroll          = new JScrollPane(BrowList);
          
          TIndicator          = new JTextField();
          TIndicator          .setEditable(false);
          
          setBounds(80,100,550,350);
          setClosable(true);
          setResizable(true);
          setTitle("Select Material");
          BrowList            .addKeyListener(new KeyList());
          
          BottomPanel         = new JPanel(true);
          BottomPanel         .setLayout(new GridLayout(1,2));
          BottomPanel         .add(TIndicator);
          
          getContentPane()    .setLayout(new BorderLayout());
          getContentPane()    .add("Center",BrowScroll);
          getContentPane()    .add("South",BottomPanel);
          setPresets();
          show();
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SMatNameCode = (String)VNameCode.elementAt(index);
                    String SMatName     = (String)VName.elementAt(index);
                    String SMatCode     = (String)VCode.elementAt(index);
                    String SMatUom      = (String)VUom.elementAt(index);
                    String SMatStock    = common.getRound(common.toDouble((String)VStock.elementAt(index)),3);
                    String SMatRate     = common.getRound(common.toDouble((String)VStockRate.elementAt(index)),4);
                    boolean bFlag = checkRowData(SMatCode);
                    if(bFlag)
                    {
                         JOptionPane.showMessageDialog(null,"This Item already selected","Duplicate",JOptionPane.INFORMATION_MESSAGE);
                    }
                    else
                    {
                         setMiddlePanel(SMatName,SMatCode,SMatUom,SMatStock,SMatRate);
                         str            = "";
                         removeHelpFrame();
                    }
               }
          }
     }
     private boolean checkRowData(String SMatCode)
     {
          boolean bFlag = false;

          for(int i=0;i<MiddlePanel.RowData.length;i++)
          {
               String SCode = (String)MiddlePanel.RowData[i][1];

               if(SCode.equals(SMatCode))
               {
                    return true;
               }
               else
               {
                    continue;
               }
          }
          return bFlag;
     }

     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<VName.size();index++)
          {
               String str1 = ((String)VName.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList  .setSelectedIndex(index);
                    BrowList  .ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }

     public void setCursor(String xtr)
     {
          int index=0;
          for(index=0;index<VName.size();index++)
          {
               String str1 = ((String)VName.elementAt(index)).toUpperCase();
               if(str1.startsWith(xtr))
               {
                    BrowList  .setSelectedIndex(index);
                    BrowList  .ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }

     private void setPresets()
     {
          String SName = (String)MiddlePanel.RowData[iRow][2];
          if(SName.length() > 0 )
               setCursor(SName.toUpperCase());
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer          .remove(this);
               Layer          .repaint();
               Layer          .updateUI();
               MiddlePanel    .tabreport.ReportTable.requestFocus();
          }
          catch(Exception ex) { }
     }

     public void setMiddlePanel(String SMatName,String SMatCode,String SMatUom,String SMatStock,String SMatRate)
     {
          try
          {
               double dRate=0;

               String QString  =   " Select nvl(Round(Sum(Net/Qty),4),0) as NetRate from purchaseorder "+
                                   " where Rate > 0 And Qty > 0 and MillCode="+iMillCode+
                                   " and Sup_Code='"+SSupCode+"' and Item_Code = '"+SMatCode+"' and "+
                                   " id = (select max(id) from purchaseorder"+
                                   " where orderdate = (select max(orderdate) from purchaseorder"+
                                   " where MillCode="+iMillCode+" and Sup_Code='"+SSupCode+"' and item_code = '"+SMatCode+"')"+
                                   " and MillCode="+iMillCode+" and Sup_Code='"+SSupCode+"' and item_code = '"+SMatCode+"')";
     
               String QStrPYOrd=   " Select nvl(Round(Sum(Net/Qty),4),0) as NetRate from pyorder"+
                                   " where Rate > 0 and Qty > 0 and MillCode="+iMillCode+
                                   " and Sup_Code='"+SSupCode+"' and Item_Code = '"+SMatCode+"' and "+
                                   " id = (select max(id) from pyorder"+
                                   " where orderdate = (select max(orderdate) from pyorder"+
                                   " where MillCode="+iMillCode+" and Sup_Code='"+SSupCode+"' and item_code = '"+SMatCode+"')"+
                                   " and MillCode="+iMillCode+" and Sup_Code='"+SSupCode+"' and item_code = '"+SMatCode+"')";

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();

               ResultSet      result         = stat.executeQuery(QString);

               while(result.next())
                    dRate  = common.toDouble(common.parseNull((String)result.getString(1)));  

               result    . close();

               if(dRate<=0)
               {
                    result = stat.executeQuery(QStrPYOrd);
                    while(result.next())
                         dRate  = common.toDouble(common.parseNull((String)result.getString(1)));  
     
                    result.close();
               }
               stat.close();


               if(dRate<=0)
                    dRate = common.toDouble(SMatRate);


               MiddlePanel.RowData[iRow][1] = SMatCode;
               MiddlePanel.RowData[iRow][2] = SMatName;
               MiddlePanel.RowData[iRow][3] = SMatUom;
               MiddlePanel.RowData[iRow][4] = SMatStock;
               MiddlePanel.RowData[iRow][5] = "";
               MiddlePanel.RowData[iRow][6] = common.getRound(dRate,4);
               MiddlePanel.RowData[iRow][7] = "0";

               MiddlePanel.RateData[iRow]   = SMatRate;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }


}
