package Indent.IndentFiles.Transfer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;
import Indent.IndentFiles.*;

public class PartyStockTransferFrame extends JInternalFrame
{
     DateField                TDate;
     MyTextField              TSentThro,TPurpose;
     JTextField               TSupCode;
     JButton                  BSupplier,BOk,BCancel,BMaterial;
     JPanel                   TopPanel,BottomPanel;

     PartyStockTransferMiddlePanel MiddlePanel;

     Common                   common = new Common();
     
     JLayeredPane             Layer;
     Vector                   VName,VCode,VNameCode;
     Vector                   VUom,VUomCode;
     StatusPanel              SPanel;
     int                      iUserCode,iMillCode;
     String                   SItemTable,SSupTable,SYearCode,SMillName,SStDate;

     String                   SIssueNo  = "";
     Connection               theConnection  = null;
     Connection               theDConnection = null;

     boolean                  bComflag  = true;

     String SAuthUserCode="";

     int               iClicked=0;

     int iIndentNo=0;
     int iIssueNo=0;
     int iRdcNo=0;

     
     public PartyStockTransferFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iUserCode,int iMillCode,String SItemTable,String SSupTable,String SYearCode,String SMillName,String SStDate)
     {
          this.Layer      = Layer;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SYearCode  = SYearCode;
          this.SMillName  = SMillName;
          this.SStDate    = SStDate;
          
          SAuthUserCode="1";

          setOraConnection();
          setParamVectors();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void setOraConnection()
     {
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                              theConnection  =  oraConnection.getConnection();
                              theConnection  .  setAutoCommit(false);

               if(iMillCode==1)
               {
                    DORAConnection DoraConnection = DORAConnection.getORAConnection();
                                   theDConnection = DoraConnection.getConnection();
               }
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void createComponents()
     {
          BSupplier        = new JButton("Select Party");
          BOk              = new JButton("Okay");
          BCancel          = new JButton("Cancel");
          BMaterial        = new JButton("Select Materials");
          
          TDate            = new DateField();
          TSentThro        = new MyTextField(20);
          TPurpose         = new MyTextField(100);
          TSupCode         = new JTextField();

          TopPanel         = new JPanel();
          BottomPanel      = new JPanel();

          MiddlePanel      = new PartyStockTransferMiddlePanel(Layer,iMillCode,SItemTable,SSupTable,SAuthUserCode,TSupCode);

          TDate     .setTodayDate();
          TDate     .setEditable(false);
          BOk       .setEnabled(false);
     }
     
     public void setLayouts()
     {
          setTitle("Stock Transfer to Party");
          setMaximizable(true);
          setClosable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,675,495);
          
          getContentPane()    .setLayout(new BorderLayout());

          TopPanel            .setLayout(new GridLayout(5,2,10,10));
          BottomPanel         .setLayout(new FlowLayout());
          TopPanel            .setBorder(new TitledBorder("Control Block"));
     }
     
     public void addComponents()
     {
          TopPanel         .add(new JLabel("  Name of the Party"));
          TopPanel         .add(BSupplier);

          TopPanel         .add(new JLabel("  Date"));
          TopPanel         .add(TDate);
          
          TopPanel         .add(new MyLabel("Vehicle to Sent Thro"));
          TopPanel         .add(TSentThro);

          TopPanel         .add(new MyLabel("Purpose"));
          TopPanel         .add(TPurpose);

          TopPanel         .add(new JLabel("  Select Materials"));
          TopPanel         .add(BMaterial);

          BottomPanel      .add(BOk);
          
          getContentPane() . add("North",TopPanel);
          getContentPane() . add("Center",MiddlePanel);
          getContentPane() . add("South",BottomPanel);
     }

     public void addListeners()
     {
          BSupplier.addActionListener(new SupplierSearch(Layer,TSupCode,SSupTable));
          BMaterial.addActionListener(new OnClick());
          BOk.addActionListener(new ActList());
     }

     private class OnClick implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               String str = (TSupCode.getText()).trim();
               if(str.length()==0)
               {
                    JOptionPane.showMessageDialog(null,"Please Select Party ","Information",JOptionPane.INFORMATION_MESSAGE);
               }
               else
               {
                    MiddlePanel.setDataIntoVector();
                    if(MiddlePanel.VCode.size()>0)
                    {
                         BSupplier.setEnabled(false);
                         BMaterial.setEnabled(false);
                         MiddlePanel.showMaterialSelectionFrame(iClicked);
                         BOk.setEnabled(true);
                    }
                    else
                    {
                         BSupplier.setEnabled(true);
                         BMaterial.setEnabled(true);
                         BOk.setEnabled(false);
                         JOptionPane    .showMessageDialog(null,"No Material Available for this Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
                    }
               }
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    if(setData())
                    {
                         getIndenNo();
                         getIssueNo();
                         getRdcNo();
                         insertTransferDetails();
                         getACommit();
                         removeHelpFrame();
                    }
                    else
                    {
                         BOk.setEnabled(true);
                    }
               }
          }
     }

     public boolean setData()
     {
          if(!isValidData())
               return false;
          
          if(!isStockValid())
               return false;
          
          if(!isDataValid())
               return false;
          
          return true;
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer     .remove(this);
               Layer     .repaint();
               Layer     .updateUI();
          }
          catch(Exception ex) { }
     }

     private void getIndenNo()
     {
          iIndentNo=0;
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               
               String QS = " Select IndentNo from IndentNoConfig for update of indentNo nowait";

               PreparedStatement thePrepare    = theConnection.prepareStatement(QS);

               ResultSet theResult             = thePrepare.executeQuery();
               if(theResult.next())
                  iIndentNo = theResult.getInt(1)+1;

               theResult.close();
               thePrepare.close();

               thePrepare = theConnection.prepareStatement(" Update IndentNoConfig set indentNo=?");
               thePrepare.setInt(1,iIndentNo);
               thePrepare.executeUpdate();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);

               String SException = String.valueOf(ex);

               System.out.println("Indent indentNo: "+ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getIndenNo();
                    bComflag = true;
               }
               else
               {
                    bComflag = false;
               }
          }
     }

     private void getIssueNo()
     {
          iIssueNo=0;
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               
               String QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" Where Id = 5 for update of MaxNo nowait";

               PreparedStatement thePrepare    = theConnection.prepareStatement(QS);

               ResultSet theResult             = thePrepare.executeQuery();
               if(theResult.next())
                  iIssueNo = theResult.getInt(1)+1;

               theResult.close();
               thePrepare.close();

               thePrepare = theConnection.prepareStatement(" Update Config"+iMillCode+""+SYearCode+" set MaxNo=? Where Id=5 ");
               thePrepare.setInt(1,iIssueNo);
               thePrepare.executeUpdate();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);

               String SException = String.valueOf(ex);

               System.out.println("IssueNo: "+ex);
     
               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getIssueNo();
                    bComflag = true;
               }
               else
               {
                    bComflag = false;
               }
          }
     }

     private void getRdcNo()
     {
          iRdcNo=0;
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               
               String QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" Where Id = 9 for update of MaxNo nowait";

               PreparedStatement thePrepare    = theConnection.prepareStatement(QS);

               ResultSet theResult             = thePrepare.executeQuery();
               if(theResult.next())
                  iRdcNo = theResult.getInt(1)+1;

               theResult.close();
               thePrepare.close();

               thePrepare = theConnection.prepareStatement(" Update Config"+iMillCode+""+SYearCode+" set MaxNo=? Where Id=9 ");
               thePrepare.setInt(1,iRdcNo);
               thePrepare.executeUpdate();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);

               String SException = String.valueOf(ex);

               System.out.println("RdcNo: "+ex);
     
               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getRdcNo();
                    bComflag = true;
               }
               else
               {
                    bComflag = false;
               }
          }
     }

     private void insertTransferDetails()
     {
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               
               String SInsQS1 = " Insert Into Issue(Id,IssueNo,IssueDate,Code,Qty,IssRate,HodCode,UIRefNo,Unit_Code,Dept_Code,Group_Code,UserCode,MillCode,Authentication,SlNo,CreationDate,IssueValue,IndentType,MrsAuthUserCode,IndentUserCode,PartyStatus,PartyCode,RDCNo,InvRate,InvValue) "+
                                " Values (Issue_Seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

               String SInsQS2 = " Insert Into RDC (Id,RdcType,RdcNo,RdcDate,Sup_Code,Descript,Dept_Code,Unit_Code,Remarks,Qty,DueDate,Thro,SlNo,UserCode,ModiDate,MillCode,AssetFlag,DocType,UomCode,GodownAsset,MemoNo,MemoSlNo,MemoAuthUserCode) "+
                                " Values (Rdc_Seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


               int iSlNo=0;

               String SUnitCode  = "0";
               String SDeptCode  = "61";
               String SGroupCode = "194";

               String SSupCode   = (TSupCode.getText()).trim();

               String SSentThro  = (common.parseNull(TSentThro.getText().trim())).toUpperCase();
               String SPurpose   = (common.parseNull(TPurpose.getText().trim())).toUpperCase();


               for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
               {
                    iSlNo++;

                    PreparedStatement thePrepare1 = theConnection.prepareStatement(SInsQS1);
                    PreparedStatement thePrepare2 = theConnection.prepareStatement(SInsQS2);

                    Statement stat = theConnection.createStatement();

                    String SItemCode   = ((String)MiddlePanel.tabreport.RowData[i][1]).trim();
                    String SItemName   = ((String)MiddlePanel.tabreport.RowData[i][2]).trim();
                    String SUomName    = ((String)MiddlePanel.tabreport.RowData[i][3]).trim();
                    int iUomCode       =  getUomCode(SUomName);
                    String SStockRate  = (String)MiddlePanel.RateData[i];
                    double dQty        = common.toDouble((String)MiddlePanel.tabreport.RowData[i][5]);
                    double dStockValue = common.toDouble(common.getRound(dQty * common.toDouble(SStockRate),2));

                    String SLastRate   = (String)MiddlePanel.tabreport.RowData[i][6];

                    double dBasicValue = dQty * common.toDouble(SLastRate);


                    if(SItemCode.length() == 0)
                         continue;


                    thePrepare1.setInt(1,iIssueNo);
                    thePrepare1.setString(2,common.getServerDate().substring(0,8));
                    thePrepare1.setString(3,SItemCode);
                    thePrepare1.setDouble(4,dQty);
                    thePrepare1.setString(5,SStockRate);
                    thePrepare1.setInt(6,0);
                    thePrepare1.setInt(7,iIndentNo);
                    thePrepare1.setString(8,SUnitCode);
                    thePrepare1.setString(9,SDeptCode);
                    thePrepare1.setString(10,SGroupCode);
                    thePrepare1.setInt(11,iUserCode);
                    thePrepare1.setInt(12,iMillCode);
                    thePrepare1.setInt(13,1);
                    thePrepare1.setInt(14,iSlNo);
                    thePrepare1.setString(15,common.getServerDate());
                    thePrepare1.setString(16,common.getRound(dStockValue,2));
                    thePrepare1.setInt(17,0);
                    thePrepare1.setString(18,SAuthUserCode);
                    thePrepare1.setString(19,SAuthUserCode);
                    thePrepare1.setInt(20,1);
                    thePrepare1.setString(21,SSupCode);
                    thePrepare1.setInt(22,iRdcNo);
                    thePrepare1.setString(23,SLastRate);
                    thePrepare1.setString(24,common.getRound(dBasicValue,2));

                    thePrepare1.executeUpdate();
                    thePrepare1.close();


                    thePrepare2.setInt(1,0);
                    thePrepare2.setInt(2,iRdcNo);
                    thePrepare2.setString(3,common.getServerDate().substring(0,8));
                    thePrepare2.setString(4,SSupCode);
                    thePrepare2.setString(5,SItemName);
                    thePrepare2.setString(6,SDeptCode);
                    thePrepare2.setString(7,SUnitCode);
                    thePrepare2.setString(8,SPurpose);
                    thePrepare2.setDouble(9,dQty);
                    thePrepare2.setString(10,common.getServerDate().substring(0,8));
                    thePrepare2.setString(11,SSentThro);
                    thePrepare2.setInt(12,iSlNo);
                    thePrepare2.setInt(13,iUserCode);
                    thePrepare2.setString(14,common.getServerDate());
                    thePrepare2.setInt(15,iMillCode);
                    thePrepare2.setString(16,"0");
                    thePrepare2.setString(17,"1");
                    thePrepare2.setInt(18,iUomCode);
                    thePrepare2.setString(19,"0");
                    thePrepare2.setString(20,"0");
                    thePrepare2.setString(21,"0");
                    thePrepare2.setString(22,SAuthUserCode);

                    thePrepare2.executeUpdate();
                    thePrepare2.close();


                    String QS1 = " Update ItemStock set Stock=nvl(Stock,0)-("+dQty+")"+
                                 " Where ItemCode='"+SItemCode+"' and HodCode="+SAuthUserCode+" and MillCode="+iMillCode;

                    String QS2 = " Update ItemStock set StockValue="+SStockRate+
                                 " Where ItemCode='"+SItemCode+"' and MillCode="+iMillCode;

                    String QS3 = " Update "+SItemTable+" "+
                                 " Set IssQty=nvl(IssQty,0)+"+dQty+",IssVal=nvl(IssVal,0)+"+dStockValue+" "+
                                 " Where Item_Code = '"+SItemCode+"'";


                    stat.executeUpdate(QS1);
                    stat.executeUpdate(QS2);
                    stat.executeUpdate(QS3);

                    stat.close();
               }

               if(iMillCode==1)
               {
                    updateSubStoreMasterData();
               }

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag = false;
          }
     }

     private void updateSubStoreMasterData()
     {
          try
          {
               Statement stat =  theDConnection.createStatement();

               for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
               {     
                    String SItemCode   = ((String)MiddlePanel.tabreport.RowData[i][1]).trim();
                    double dIssueQty   = common.toDouble((String)MiddlePanel.tabreport.RowData[i][5]);


                    if(SItemCode.length() == 0)
                         continue;


                    int iCount=0;
                    
                    ResultSet res = stat.executeQuery("Select count(*) from InvItems Where Item_Code='"+SItemCode+"'");
                    while(res.next())
                    {
                         iCount   =    res.getInt(1);
                    }
                    res.close();

                    if(iCount==0)
                         continue;

                    String QS = "";

                    QS = " Update InvItems Set ";
                    QS = QS+" MSIssQty=nvl(MSIssQty,0)+"+dIssueQty+",";
                    QS = QS+" MSStock=nvl(MSStock,0)-"+dIssueQty+" ";
                    QS = QS+" Where Item_Code = '"+SItemCode+"'";

                    if(theDConnection.getAutoCommit())
                         theDConnection . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("updateSubStoreMasterData() ->"+e);
               e.printStackTrace();
               bComflag  = false;
          }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnection  . commit();

                    if(iMillCode==1)
                         theDConnection . commit();

                    JOptionPane    . showMessageDialog(null,"Data Saved Sucessfully");
                    System         . out.println("Commit");
               }
               else
               {
                    theConnection  . rollback();

                    if(iMillCode==1)
                         theDConnection . rollback();

                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theConnection   . setAutoCommit(true);

               if(iMillCode==1)
                    theDConnection . setAutoCommit(true);

          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private int getUomCode(String SUomName)
     {
          int index= VUom.indexOf(common.parseNull(SUomName));

          if(index==-1)
              return 0;
          else
              return (common.toInt((String)VUomCode.elementAt(index)));
     }

     private boolean isValidData()
     {
          try
          {
               String SSentThro    = common.parseNull(TSentThro.getText().trim());
               String SPurpose     = common.parseNull(TPurpose.getText().trim());

               if(SSentThro.length()==0)
               {
                    JOptionPane.showMessageDialog(null,"Sent Thro Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
                    TSentThro.requestFocus();
                    return false;
               }

               if(SPurpose.length()==0)
               {
                    JOptionPane.showMessageDialog(null,"Purpose Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
                    TPurpose.requestFocus();
                    return false;
               }

               if((TDate.toNormal()).length()!=8)
               {
                    JOptionPane.showMessageDialog(null,"Date Field is Invalid","Invalid Field",JOptionPane.INFORMATION_MESSAGE);
                    return false;
               }     
          }
          catch(Exception ex)
          {
               System.out.println(" isValid in PartyStockTransferFrame ->"+ex);
          }
          return true;
     }

     private boolean isStockValid()
     {
          boolean bflag = true;
          
          for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
          {
               String Smsg="";
               String SCode     = ((String)MiddlePanel.tabreport.RowData[i][1]).trim();
               double dStock    = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][4]).trim());
               
               if(SCode.length() == 0)
                    continue;
               
               if(dStock<0)
                    Smsg = Smsg + " Negative Stock for the Date,"; 
               if(Smsg.length()>0)
               {
                    Smsg = Smsg.substring(0,(Smsg.length()-1));
                    Smsg = Smsg + " in S.No - " + (i+1);
                    JOptionPane.showMessageDialog(null,Smsg,"Invalid Quantity",JOptionPane.INFORMATION_MESSAGE);
                    bflag=false;
               }
          }
          return bflag;
     }

     private boolean isDataValid()
     {
          boolean bflag = true;

          for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
          {
               String Smsg="";
               String SCode         = ((String)MiddlePanel.tabreport.RowData[i][1]).trim();

               if(SCode.length() == 0)
                    continue;
               
               if(!isEligible(i))
                    Smsg = Smsg + " Quantity,";

               if(Smsg.length()>0)
               {
                    Smsg = Smsg.substring(0,(Smsg.length()-1));
                    Smsg = Smsg + "  Field(s) Missing in S.No - " + (i+1);
                    JOptionPane.showMessageDialog(null,Smsg,"Missing Fields",JOptionPane.INFORMATION_MESSAGE);
                    bflag=false;
               }
          }
          return bflag;
     }

     private boolean isEligible(int iRowCount)
     {
          double dQty         = common.toDouble(common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(iRowCount,5)));
          String SItemCode    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(iRowCount,1));

          if(dQty<=0)
               return false;
          if(SItemCode.length()<3)
               return false;

          return true;
     }

     public void setParamVectors()
     {
          VUom      = new Vector();
          VUomCode  = new Vector();

          try
          {
               Statement stat = theConnection.createStatement();
               ResultSet res  = stat.executeQuery("Select UomName,UomCode From Uom Order By UomCode");
               while(res.next())
               {
                    VUom      .addElement(res.getString(1));
                    VUomCode  .addElement(res.getString(2));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

}
