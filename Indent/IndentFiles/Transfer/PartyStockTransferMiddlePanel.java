package Indent.IndentFiles.Transfer;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class PartyStockTransferMiddlePanel extends JPanel
{
     Object              RowData[][];
     String              ColumnData[] = {"SNo","Code","Name","UOM","Stock","Quantity","Rate","Value"};
     String              ColumnType[] = {"N"  ,"S"   ,"S"   ,"S"  ,"N"    ,"E"       ,"N"   ,"N"    };
     
     Object              RateData[];

     JLayeredPane        Layer;
     Vector              VCode,VName,VUom,VNameCode,VStock,VStockRate;
     PSTMaterialSearch   MS;
     PSTMaterialsSearch  SM;
     PSTTabReport        tabreport;
     Common              common = new Common();
     int                 iMillCode;
     String              SItemTable,SSupTable,SAuthUserCode;
     JTextField          TSupCode;

     PartyStockTransferMiddlePanel(JLayeredPane Layer,int iMillCode,String SItemTable,String SSupTable,String SAuthUserCode,JTextField TSupCode)
     {
          this.Layer         = Layer;
          this.iMillCode     = iMillCode;
          this.SItemTable    = SItemTable;
          this.SSupTable     = SSupTable;
          this.SAuthUserCode = SAuthUserCode;
          this.TSupCode      = TSupCode;
     }
     
     public void createComponents(int irows)
     {
          RowData        = new Object[irows][ColumnData.length];
          RateData       = new Object[irows];

          for(int i=0;i<irows;i++)
          {
               RowData[i][0] = String.valueOf(i+1);
               RowData[i][1] = " ";
               RowData[i][2] = " ";
               RowData[i][3] = " ";
               RowData[i][4] = " ";
               RowData[i][5] = " ";
               RowData[i][6] = "0";
               RowData[i][7] = "0";

               RateData[i] = "0";
          }
          try
          {
               setLayout(new BorderLayout());
               tabreport   = new PSTTabReport(RowData,ColumnData,ColumnType,iMillCode,SItemTable,SSupTable,SAuthUserCode);
               add(tabreport,BorderLayout.CENTER);
               updateUI();
               addListeners();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     
     public void addListeners()
     {
          tabreport.ReportTable.addKeyListener(new KeyList());
     }
     
     private class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    String str = (TSupCode.getText()).trim();
                    if(str.length()==0)
                    {
                         JOptionPane.showMessageDialog(null,"Please Select Party ","Information",JOptionPane.INFORMATION_MESSAGE);
                    }
                    else
                    {
                         showMaterialSelectionFrame(1);
                    }
               }
          }
     }
     
     public void showMaterialSelectionFrame(int iClicked)
     {
          try
          {
               Layer.remove(MS);
               Layer.remove(SM);
               Layer.updateUI();
          }
          catch(Exception ex){}
          
          try
          {
               String SSupCode = (TSupCode.getText()).trim();

               if(iClicked==0)
               {
                    SM   = new PSTMaterialsSearch(Layer,this,iMillCode,SItemTable,SSupTable,SAuthUserCode,SSupCode,VCode,VName,VUom,VNameCode,VStock,VStockRate);
                    SM   .setSelected(true);
                    Layer.add(SM);
                    Layer.repaint();
                    SM   .moveToFront();
                    Layer.updateUI();
               }
               else
               {
                    MS   = new PSTMaterialSearch(Layer,this,tabreport.ReportTable.getSelectedRow(),iMillCode,SItemTable,SSupTable,SAuthUserCode,SSupCode,VCode,VName,VUom,VNameCode,VStock,VStockRate);
                    MS   .setSelected(true);
                    Layer.add(MS);
                    Layer.repaint();
                    MS   .moveToFront();
                    Layer.updateUI();
               }
          }
          catch(Exception ex){}
     }

     public void setDataIntoVector()
     {
          VName      = new Vector();
          VCode      = new Vector();
          VUom       = new Vector();
          VNameCode  = new Vector();
          VStock     = new Vector();
          VStockRate = new Vector();

          String SSupCode = (TSupCode.getText()).trim();
               
          String QS = " Select InvItems.Item_Name,t.Code,Uom.UoMName,sum(ItemStock.Stock),ItemStock.StockValue from InvItems "+
                      " inner join (Select distinct(Code) from Grn Where MillCode="+iMillCode+" and Sup_Code='"+SSupCode+"') t on InvItems.Item_Code=t.Code "+
                      " inner join ItemStock on (t.Code=ItemStock.ItemCode and ItemStock.MillCode="+iMillCode+" and ItemStock.HodCode="+SAuthUserCode+") "+
                      " inner join UOM on UOM.UOMCode = InvItems.UOMCode "+
                      " Group by InvItems.Item_Name,t.Code,Uom.UoMName,ItemStock.StockValue "+
                      " having sum(ItemStock.Stock)>0 "+
                      " Order By InvItems.Item_Name";

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();   
               Statement      stat           = theConnection.createStatement();

               ResultSet result1 = stat.executeQuery(QS);

               while(result1.next())
               {
                    VName     .addElement(common.parseNull(result1.getString(1)));
                    VCode     .addElement(common.parseNull(result1.getString(2)));
                    VUom      .addElement(common.parseNull(result1.getString(3)));
                    VNameCode .addElement(common.parseNull(result1.getString(1))+" (Code : "+common.parseNull(result1.getString(2))+")");
                    VStock    .addElement(common.parseNull(result1.getString(4)));
                    VStockRate.addElement(common.parseNull(result1.getString(5)));
               }
               result1.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     
}
