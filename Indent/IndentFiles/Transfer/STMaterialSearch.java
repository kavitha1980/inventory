package Indent.IndentFiles.Transfer;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class STMaterialSearch extends JInternalFrame
{
     JLayeredPane             Layer;
     Vector                   VName,VCode,VUom;
     StockTransferMiddlePanel MiddlePanel;
     int                      iRow;
     int                      iMillCode;
     String                   SItemTable,SSupTable,SAuthUserCode;
     
     JTextField               TIndicator;          
     JList                    BrowList;
     JScrollPane              BrowScroll;
     JPanel                   BottomPanel;
     
     Vector                   VNameCode;
     String                   SName="",SCode="";
     String                   str="";
     Common                   common = new Common();

     STMaterialSearch(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUom,Vector VNameCode,StockTransferMiddlePanel MiddlePanel,int iRow,int iMillCode,String SItemTable,String SSupTable,String SAuthUserCode)
     {
          this.Layer          = Layer;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VUom           = VUom;
          this.VNameCode      = VNameCode;
          this.MiddlePanel    = MiddlePanel;
          this.iRow           = iRow;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;
          this.SAuthUserCode  = SAuthUserCode;

          BrowList            = new JList(VNameCode);
          BrowList            .setFont(new Font("monospaced", Font.PLAIN, 11));
          
          BrowScroll          = new JScrollPane(BrowList);
          
          TIndicator          = new JTextField();
          TIndicator          .setEditable(false);
          
          setBounds(80,100,550,350);
          setClosable(true);
          setResizable(true);
          setTitle("Select Material");
          BrowList            .addKeyListener(new KeyList());
          
          BottomPanel         = new JPanel(true);
          BottomPanel         .setLayout(new GridLayout(1,2));
          BottomPanel         .add(TIndicator);
          
          getContentPane()    .setLayout(new BorderLayout());
          getContentPane()    .add("Center",BrowScroll);
          getContentPane()    .add("South",BottomPanel);
          setPresets();
          show();
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SMatNameCode = (String)VNameCode.elementAt(index);
                    String SMatName     = (String)VName.elementAt(index);
                    String SMatCode     = (String)VCode.elementAt(index);
                    String SMatUom      = (String)VUom.elementAt(index);
                    boolean bFlag = checkRowData(SMatCode);
                    if(bFlag)
                    {
                         JOptionPane.showMessageDialog(null,"This Item already selected","Duplicate",JOptionPane.INFORMATION_MESSAGE);
                    }
                    else
                    {
                         setMiddlePanel(SMatName,SMatCode,SMatUom);
                         str            = "";
                         removeHelpFrame();
                    }
               }
          }
     }
     private boolean checkRowData(String SMatCode)
     {
          boolean bFlag = false;

          for(int i=0;i<MiddlePanel.RowData.length;i++)
          {
               String SCode = (String)MiddlePanel.RowData[i][1];

               if(SCode.equals(SMatCode))
               {
                    return true;
               }
               else
               {
                    continue;
               }
          }
          return bFlag;
     }

     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<VName.size();index++)
          {
               String str1 = ((String)VName.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList  .setSelectedIndex(index);
                    BrowList  .ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }

     public void setCursor(String xtr)
     {
          int index=0;
          for(index=0;index<VName.size();index++)
          {
               String str1 = ((String)VName.elementAt(index)).toUpperCase();
               if(str1.startsWith(xtr))
               {
                    BrowList  .setSelectedIndex(index);
                    BrowList  .ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }

     private void setPresets()
     {
          String SName = (String)MiddlePanel.RowData[iRow][2];
          if(SName.length() > 0 )
               setCursor(SName.toUpperCase());
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer          .remove(this);
               Layer          .repaint();
               Layer          .updateUI();
               MiddlePanel    .tabreport.ReportTable.requestFocus();
          }
          catch(Exception ex) { }
     }

     public void setMiddlePanel(String SMatName,String SMatCode,String SMatUom)
     {
          try
          {
               double dStock=0;
               double dRate=0;

               String QS = " Select Stock,StockValue from ItemStock Where HodCode="+SAuthUserCode+" and ItemCode='"+SMatCode+"' and MillCode="+iMillCode;

               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    dStock = common.toDouble(common.parseNull((String)res.getString(1)));
                    dRate  = common.toDouble(common.parseNull((String)res.getString(2)));  
               }
               res.close();
               stat.close();

               String SStock  = common.getRound(dStock,3);
               
               MiddlePanel.RowData[iRow][1] = SMatCode;
               MiddlePanel.RowData[iRow][2] = SMatName;
               MiddlePanel.RowData[iRow][3] = SMatUom;
               MiddlePanel.RowData[iRow][4] = "";
               MiddlePanel.RowData[iRow][5] = "";
               MiddlePanel.RowData[iRow][6] = SStock;
               MiddlePanel.RowData[iRow][7] = "";
               MiddlePanel.RowData[iRow][8] = common.getRound(dRate,4);
               MiddlePanel.RowData[iRow][9] = "0";
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

     }

}
