package Indent.IndentFiles.Transfer;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class STMaterialsSearch
{
     JLayeredPane                  Layer;
     Vector                        VName,VCode,VUom,VNameCode;
     String                        SName,SCode,SUom;
     StockTransferMiddlePanel      MiddlePanel;
     int                           iMillCode;
     String                        SItemTable,SSupTable,SAuthUserCode;
     
     JList                         BrowList,SelectedList;
     JScrollPane                   BrowScroll,SelectedScroll;
     JTextField                    TIndicator;
     JButton                       BOk;
     JPanel                        LeftPanel,RightPanel;
     JInternalFrame                MaterialFrame;
     JPanel                        MFMPanel,MFBPanel;
     Vector                        VSelectedName,VSelectedCode,VSelectedUom;
     Vector                        VSelectedNameCode;
     String                        SFrameTitle,SWhere;
     String                        str="";
     int                           iMFSig=0;
     MouseEvent                    ae;
     Common                        common = new Common();


     STMaterialsSearch(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUom,Vector VNameCode,StockTransferMiddlePanel MiddlePanel,int iMillCode,String SItemTable,String SSupTable,String SAuthUserCode)
     {
          this.Layer         = Layer;
          this.VCode         = VCode;
          this.VName         = VName;
          this.VUom          = VUom;
          this.VNameCode     = VNameCode;
          this.MiddlePanel   = MiddlePanel;
          this.iMillCode     = iMillCode;
          this.SItemTable    = SItemTable;
          this.SSupTable     = SSupTable;
          this.SAuthUserCode = SAuthUserCode;
          
          createComponents();
     }

     public void createComponents()
     {
          VSelectedName     = new Vector();
          VSelectedCode     = new Vector();
          VSelectedUom      = new Vector();
          VSelectedNameCode = new Vector();

          BrowList       = new JList(VNameCode);
          BrowList       .setFont(new Font("monospaced", Font.PLAIN, 11));
          SelectedList   = new JList();
          BrowScroll     = new JScrollPane(BrowList);
          SelectedScroll = new JScrollPane(SelectedList);
          LeftPanel      = new JPanel(true);
          RightPanel     = new JPanel(true);
          TIndicator     = new JTextField();
          BOk            = new JButton("Selection Over");
          TIndicator     .setEditable(false);
          MFMPanel       = new JPanel(true);
          MFBPanel       = new JPanel(true);
          MaterialFrame  = new JInternalFrame("Materials Selector");
          MaterialFrame  .show();
          MaterialFrame  .setBounds(50,50,650,350);
          MaterialFrame  .setClosable(true);
          MaterialFrame  .setResizable(true);
          BrowList       .addKeyListener(new KeyList());
          BrowList       .requestFocus();
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent e)
          {
               if(VSelectedCode.size()==0)
               {
                    JOptionPane    .showMessageDialog(null,"No Material is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                    BrowList       .requestFocus();
                    return;
               }
               BOk.setEnabled(false);
               setMiddlePanel();
               removeHelpFrame();
               str="";
          }
     }

     public void showMaterialsFrame(MouseEvent ae)
     {
          this.ae = ae;
          TIndicator.setText(str);
          BOk.setEnabled(true);
          if(iMFSig==0)
          {
               MFMPanel       .setLayout(new GridLayout(1,2));
               MFBPanel       .setLayout(new GridLayout(1,2));
               MFMPanel       .add(BrowScroll);
               MFMPanel       .add(SelectedScroll);
               MFBPanel       .add(TIndicator);
               MFBPanel       .add(BOk);
               BOk            .addActionListener(new ActList());
               MaterialFrame  .getContentPane().add("Center",MFMPanel);
               MaterialFrame  .getContentPane().add("South",MFBPanel);
               iMFSig=1;
          }
          removeHelpFrame();
          try
          {
               Layer          .add(MaterialFrame);
               MaterialFrame  .moveToFront();
               MaterialFrame  .setSelected(true);
               MaterialFrame  .show();
               BrowList       .requestFocus();
               Layer          .repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex)
               {
                    Toolkit.getDefaultToolkit().beep();
               }
          }
     
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    if(index == -1)
                         return;
     
                    String SMatNameCode = (String)VNameCode .elementAt(index);
                    String SMatName       = (String)VName   .elementAt(index);
                    String SMatCode       = (String)VCode   .elementAt(index);
                    String SMatUom        = (String)VUom    .elementAt(index);
                    addMatDet(SMatName,SMatCode,SMatUom,SMatNameCode);
                    str="";
                    TIndicator.setText(str);
               }
               if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
               {
                    setMiddlePanel();
                    removeHelpFrame();
                    str="";
               }
          }
     }

     public void setCursor()
     {
          int index=0;
          TIndicator.setText(str);
          for(index=0;index<VNameCode.size();index++)
          {
               String str1 = ((String)VNameCode.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList  .setSelectedIndex(index);
                    BrowList  .ensureIndexIsVisible(index+10);
                    break;
               }
          }
          if(index >= VNameCode.size())
               Toolkit.getDefaultToolkit().beep();
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer          .remove(MaterialFrame);
               Layer          .repaint();
               Layer          .updateUI();
               MiddlePanel    .tabreport.ReportTable.requestFocus();
          }
          catch(Exception ex) { }
     }

     public boolean addMatDet(String SMatName,String SMatCode,String SMatUom,String SMatNameCode)
     {
          int iIndex=VSelectedCode.indexOf(SMatCode);
          if (iIndex==-1)
          {
               VSelectedName       .addElement(SMatName);
               VSelectedCode       .addElement(SMatCode);
               VSelectedUom        .addElement(SMatUom);
               VSelectedNameCode   .addElement(SMatNameCode);
          }
          else
          {
               VSelectedName       .removeElementAt(iIndex);
               VSelectedCode       .removeElementAt(iIndex);
               VSelectedUom        .removeElementAt(iIndex);
               VSelectedNameCode   .removeElementAt(iIndex);
          }
          SelectedList.setListData(VSelectedNameCode);
          return true;
     }

     public void setMiddlePanel()
     {
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();

               for(int i=0;i<VSelectedName.size();i++)
               {
                    String SMatCode = (String)VSelectedCode.elementAt(i);
                    String SMatName = (String)VSelectedName.elementAt(i);

                    double dStock=0;
                    double dRate=0;

                    String QS = " Select Stock,StockValue from ItemStock Where HodCode="+SAuthUserCode+" and ItemCode='"+SMatCode+"' and MillCode="+iMillCode;

                    ResultSet res  = stat.executeQuery(QS);
                    while (res.next())
                    {
                         dStock = common.toDouble(common.parseNull((String)res.getString(1)));
                         dRate  = common.toDouble(common.parseNull((String)res.getString(2)));  
                    }
                    res.close();

                    String SStock  = common.getRound(dStock,3);
                    
                    MiddlePanel.RowData[i][1] = SMatCode;
                    MiddlePanel.RowData[i][2] = SMatName;
                    MiddlePanel.RowData[i][3] = (String)VSelectedUom.elementAt(i);
                    MiddlePanel.RowData[i][6] = SStock;
                    MiddlePanel.RowData[i][8] = common.getRound(dRate,4);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

     }


}
