package Indent.IndentFiles.Transfer;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class STTabReport extends JPanel
{
     JTable         ReportTable;
     Object         RowData[][];
     String         ColumnData[],ColumnType[];
     JComboBox      JCCata,JCDept;
     int            iMillCode;
     String         SItemTable,SSupTable;
     String         SAuthUserCode;

     JPanel         thePanel;
     Common common = new Common();
     AbstractTableModel dataModel;
     
     STTabReport(Object RowData[][],String ColumnData[],String ColumnType[],JComboBox JCCata,JComboBox JCDept,int iMillCode,String SItemTable,String SSupTable,String SAuthUserCode)
     {
          this.RowData        = RowData;
          this.ColumnData     = ColumnData;
          this.ColumnType     = ColumnType;
          this.JCCata         = JCCata;
          this.JCDept         = JCDept;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;
          this.SAuthUserCode  = SAuthUserCode;

          thePanel            = new JPanel();

          setReportTable();
          setBorder(new TitledBorder("List Of Items"));
     }
     
     public void setReportTable()
     {
          dataModel = new AbstractTableModel()
          {
               public int getColumnCount(){return ColumnData.length;}
               public int getRowCount(){return RowData.length;}
               public Object getValueAt(int row,int col){return RowData[row][col];}
               public String getColumnName(int col){ return ColumnData[col];}
               public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }
               public boolean isCellEditable(int row,int col)
               {
                    if(ColumnType[col]=="B" || ColumnType[col]=="E")
                         return true;
                    return false;
               }
          
               public void setValueAt(Object element,int row,int col)
               {
                    if(col!=7)
                         RowData[row][col]=element;
                    if(col > 7)
                         return;

                    if(col == 7)
                    {
                         if(isValidQty((String)element)) 
                         {
                              RowData[row][col]=element; 
                              setTotal();
                              double dQty    = Double.parseDouble((String)element);
                              String SCode   = (String)ReportTable.getValueAt(row,1);
                              double dStock  = getStock(SCode);

                              dStock = dStock - dQty;
                                   
                              ReportTable.setValueAt(common.getRound(dStock,3),row,col-1);
                         }
                         else
                              JOptionPane.showMessageDialog(null,"Invaid Quantity","Error Message",JOptionPane.INFORMATION_MESSAGE);
                    }                    
               }
          };
               
          ReportTable  = new JTable(dataModel);
          ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
          DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
          cellRenderer.setHorizontalAlignment(JLabel.RIGHT);
          
          for (int col=0;col<ReportTable.getColumnCount();col++)
          {
               if(ColumnType[col]=="N" || ColumnType[col]=="E") 
               ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
          }

          TableColumn    CataColumn     = ReportTable.getColumn("Classification");
                         CataColumn     . setCellEditor(new DefaultCellEditor(JCCata));
          TableColumn    DeptColumn     = ReportTable.getColumn("Department");
                         DeptColumn     . setCellEditor(new DefaultCellEditor(JCDept));

          setQtyHeader();
          formatColumns();
          setLayout(new BorderLayout());
          thePanel.setLayout(new BorderLayout());
          thePanel.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
          thePanel.add(new JScrollPane(ReportTable),BorderLayout.CENTER);
          add("Center",thePanel);
     }
     
     private void setQtyHeader()
     {
          DefaultTableCellRenderer cellRenderer   = new DefaultTableCellRenderer();
                                   cellRenderer   .setHorizontalAlignment(JLabel.CENTER);
                                   cellRenderer   .setBackground(new Color(100,200,250));
                                   cellRenderer   .setIcon(new ImageIcon("prodbig.gif"));
                                   cellRenderer   .setToolTipText("Quantity");
                                   (ReportTable   .getTableHeader()).setBorder(new BevelBorder(BevelBorder.RAISED));
                                   cellRenderer   .setBorder(new SoftBevelBorder(0));
                                   (ReportTable   .getColumn("Quantity")).setHeaderRenderer(cellRenderer);
     }
     
     private boolean isValidQty(String str)
     {
          boolean bFlag=true;
          try
          {
               double dvalue=Double.parseDouble(str);
               bFlag = true;
          }
          catch(Exception ex)
          {
               bFlag=false;
          }
          return bFlag;
     }
     
     private void formatColumns()
     {
          TableColumn snoColumn   =  ReportTable.getColumn(ReportTable.getColumnName(0));
          TableColumn codeColumn  =  ReportTable.getColumn(ReportTable.getColumnName(1));
          TableColumn nameColumn  =  ReportTable.getColumn(ReportTable.getColumnName(2));
          TableColumn uomColumn   =  ReportTable.getColumn(ReportTable.getColumnName(3));
          TableColumn deptColumn  =  ReportTable.getColumn(ReportTable.getColumnName(4));
          TableColumn CataColumn  =  ReportTable.getColumn(ReportTable.getColumnName(5));
          TableColumn stockColumn =  ReportTable.getColumn(ReportTable.getColumnName(6));
          TableColumn qtyColumn   =  ReportTable.getColumn(ReportTable.getColumnName(7));
          TableColumn rateColumn  =  ReportTable.getColumn(ReportTable.getColumnName(8));
          TableColumn valueColumn =  ReportTable.getColumn(ReportTable.getColumnName(9));
          
          snoColumn      .setPreferredWidth(30);
          codeColumn     .setPreferredWidth(75);
          nameColumn     .setPreferredWidth(140);
          uomColumn      .setPreferredWidth(50);
          deptColumn     .setPreferredWidth(90);
          CataColumn     .setPreferredWidth(90);
          stockColumn    .setPreferredWidth(75);
          qtyColumn      .setPreferredWidth(80);
          rateColumn     .setPreferredWidth(60);
          valueColumn    .setPreferredWidth(60);
     }
     
     private void setTotal()
     {
          double dValue=0,dTValue=0;
          for(int i=0;i<RowData.length;i++)
          {
               double dQty   = common.toDouble((String)ReportTable.getValueAt(i,7));    
               double dRate  = common.toDouble((String)ReportTable.getValueAt(i,8));
               dValue = dQty*dRate;
               dTValue = dTValue+dValue;
               ReportTable.setValueAt(common.getRound(dValue,2),i,9);
          }
     }

     private double getStock(String SCode)
     {
          double dQty=0;

          try
          {
               String QS= " Select Stock from ItemStock where ItemCode='"+SCode+"' and MillCode="+iMillCode+" and HodCode="+SAuthUserCode;

               ORAConnection   oraConnection = ORAConnection.getORAConnection();
               Connection      theConnection = oraConnection.getConnection();               
               Statement theStatement        = theConnection.createStatement();
               ResultSet theResult           = theStatement.executeQuery(QS);
     
               if(theResult.next())
                    dQty = theResult.getDouble(1);

          }
          catch(Exception ex)
          {
               System.out.println(ex);   
          }
          return dQty;
     }

}
