package Indent.IndentFiles.Transfer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;
import Indent.IndentFiles.*;

public class StockTransferFrame extends JInternalFrame
{
     DateField                TDate,TDCDate;
     JTextField               TDCNo;
     JComboBox                JCUnit,JCCata,JCTo;
     MyComboBox               JCUser;
     NextField                TIssueNo,TRefNo;
     MyLabel                  LMillName;
     Vector                   VUnit,VUnitCode,VCata,VCataCode,VTo,VToCode;
     Vector                   VIUserCode,VIUserName;
     JButton                  BApply,BOk,BCancel;
     JPanel                   TopPanel,BottomPanel;
     JPanel                   Top1Panel,Top2Panel;
     StockTransferMiddlePanel MiddlePanel;
     Common                   common = new Common();
     Control                  control;
     
     JLayeredPane             Layer;
     Vector                   VName,VCode,VUom,VNameCode;
     StatusPanel              SPanel;
     int                      iUserCode,iMillCode;
     String                   SItemTable,SSupTable,SYearCode,SMillName,SStDate;

     String                   SIssueNo  = "";
     Connection               theConnection  = null;

     boolean                  bComflag  = true;

     String SSeleMillCode="";

     String SAuthUserCode="";
     
     public StockTransferFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUom,Vector VNameCode,StatusPanel SPanel,int iUserCode,int iMillCode,String SItemTable,String SSupTable,String SYearCode,String SMillName,String SStDate)
     {
          this.Layer      = Layer;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VUom       = VUom;
          this.VNameCode  = VNameCode;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SYearCode  = SYearCode;
          this.SMillName  = SMillName;
          this.SStDate    = SStDate;
          
          setOraConnection();
          getUsers();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setUnit(0);
     }

     public void setOraConnection()
     {
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                              theConnection  =  oraConnection.getConnection();
                              theConnection  .  setAutoCommit(false);
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void createComponents()
     {
          BApply           = new JButton("Apply");
          BOk              = new JButton("Okay");
          BCancel          = new JButton("Cancel");
          
          JCUser           = new MyComboBox(VIUserName);
          JCUnit           = new JComboBox();
          JCCata           = new JComboBox(VCata);
          JCTo             = new JComboBox(VTo);

          TIssueNo         = new NextField();
          TRefNo           = new NextField();
          TDate            = new DateField();
          TDCDate          = new DateField();
          TDCNo            = new MyTextField(15);

          LMillName        = new MyLabel("");

          TopPanel         = new JPanel();
          Top1Panel        = new JPanel();
          Top2Panel        = new JPanel();

          
          BottomPanel      = new JPanel();
          control          = new Control();

          getIssueNo();

          TIssueNo  .setEditable(false);
          TIssueNo  .setText(SIssueNo);
          TDate     .setTodayDate();
          TDate     .setEditable(false);

          VUnit               = new Vector();
          VUnitCode           = new Vector();

          LMillName.setText(SMillName);
     }
     
     public void setLayouts()
     {
          setTitle("Stock Transfer");
          setMaximizable(true);
          setClosable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,795,495);
          
          getContentPane()    .setLayout(new BorderLayout());

          TopPanel            .setLayout(new BorderLayout());
          Top1Panel           .setLayout(new FlowLayout());
          Top2Panel           .setLayout(new GridLayout(4,4,10,10));
          BottomPanel         .setLayout(new FlowLayout());
          TopPanel            .setBorder(new TitledBorder("Control Block"));
     }
     
     public void addComponents()
     {
          Top1Panel.add(new JLabel("Select User"));
          Top1Panel.add(JCUser);
          Top1Panel.add(new JLabel(""));
          Top1Panel.add(BApply);

          TopPanel.add("North",Top1Panel);

          getContentPane() . add("North",TopPanel);
     }

     public void addListeners()
     {
          BApply.addActionListener(new ActList());
          BOk.addActionListener(new ActList());
          JCTo.addItemListener(new ItemList());
     }
     public class ItemList implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
               int iIndex = JCTo.getSelectedIndex();
               setUnit(iIndex);
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
                    BApply.setEnabled(false);
                    JCUser.setEnabled(false);
                    SAuthUserCode="";
                    SAuthUserCode  = (String)VIUserCode.elementAt(JCUser.getSelectedIndex());
                    MiddlePanel    = new StockTransferMiddlePanel(Layer,VCode,VName,VUom,VNameCode,JCUnit,VUnitCode,JCCata,VCata,VCataCode,iUserCode,iMillCode,SItemTable,SSupTable,SAuthUserCode);
                    setIssueData();
               }
               if(ae.getSource()==BOk)
               {
                    if(setData())
                    {
                         setComOrRol();
                         removeHelpFrame();
                    }
                    else
                    {
                         BOk  .setEnabled(true);
                    }
               }
          }
     }

     public void setIssueData()
     {

          Top2Panel        .add(new JLabel(" From Concern"));
          Top2Panel        .add(LMillName);
          
          Top2Panel        .add(new JLabel("  To Concern"));
          Top2Panel        .add(JCTo);

          Top2Panel        .add(new JLabel(" Unit"));
          Top2Panel        .add(JCUnit);

          Top2Panel        .add(new JLabel("  Date"));
          Top2Panel        .add(TDate);
          
          Top2Panel        .add(new JLabel(" User Indent No"));
          Top2Panel        .add(TRefNo);
          
          Top2Panel        .add(new JLabel("  Issue No"));
          Top2Panel        .add(TIssueNo);
          
          Top2Panel        .add(new JLabel(" D.C. No."));
          Top2Panel        .add(TDCNo);
          
          Top2Panel        .add(new JLabel("  D.C.Date"));
          Top2Panel        .add(TDCDate);
          
          BottomPanel      .add(BOk);
          
          TopPanel         . add("Center",Top2Panel);

          getContentPane() . add("Center",MiddlePanel);
          getContentPane() . add("South",BottomPanel);

          Layer. repaint();
          Layer. updateUI();
     }

     public boolean setData()
     {
          if(!isValidData())
               return false;
          
          if(!isValidItem())
               return false;

          if(!isStockValid())
               return false;
          
          if(!isDataValid())
               return false;
          
          getIssueNo();
          insertIssueDetails();
          insertTransferDetails();
          updateInvitemsData();
          UpdateIssueNo();
          removeHelpFrame();

          return bComflag;
     }

     public void setComOrRol()
     {
          if(bComflag)
          {
               try  {
                         theConnection  .commit();
               }    catch(Exception ex)
               {    System.out.println("Indent frame actionPerformed ->"+ex);ex.printStackTrace();  }
          }
          else
          {
               try  {
                         theConnection  .rollback();
                         BOk            .setEnabled(true);
               }    catch(Exception ex)
               {    System.out.println("Indent frame actionPerformed ->"+ex);ex.printStackTrace();  }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer     .remove(this);
               Layer     .repaint();
               Layer     .updateUI();
          }
          catch(Exception ex) { }
     }

               /*    DML,DDL process in Data Base         */
     public void insertIssueDetails()
     {
          String QString = "Insert Into Issue(id,IssueNo,IssueDate,Code,Qty,IssRate,UIRefNo,Dept_Code,Group_Code,Unit_Code,CreationDate,UserCode,MillCode,HodCode,indenttype,SlNo,IssueValue,MrsAuthUserCode,IndentUserCode,Authentication) Values (";
          
          try
          {
               int  iHodIndent     = 0;
               int  iSlNo          = 0;
               Statement      stat           =  theConnection.createStatement();
               
               for(int i=0;i<MiddlePanel.RowData.length;i++)
               {
                    String SCode  = (String)MiddlePanel.tabreport.ReportTable.getValueAt(i,1);
                    double dQty   = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,7));
                    double dStock = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,6));

                    if((SCode.length() == 0) || (dQty<=0) || (dStock<0))
                         continue;
                    
                    iSlNo++;

                    String SUnitCode    = (String)VUnitCode.elementAt(JCUnit.getSelectedIndex());
                    double dRate        = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,8));
                    double dValue       = dQty*dRate;

                    
                    String    str = QString;
                              str = str+ getIssueID()+",";
                              str = str+SIssueNo+",";
                              str = str+"'"+TDate.toNormal()+"',";
                              str = str+"'"+SCode+"',";
                              str = str+common.getRound(dQty,3)+",";
                              str = str+common.getRound(dRate,4)+",";
                              str = str+"0"+TRefNo.getText()+",";
                              str = str+MiddlePanel.getDepartmentCode(i)+",";
                              str = str+getCategoryCode(i)+",";
                              str = str+SUnitCode+",";
                              str = str+"'"+common.getServerDate()+"',";
                              str = str+String.valueOf(iUserCode)+",";
                              str = str+String.valueOf(iMillCode)+",";
                              str = str+String.valueOf(iHodIndent)+",";
                              str = str+String.valueOf(iHodIndent)+",";
                              str = str+String.valueOf(iSlNo)+",";
                              str = str+common.getRound(dValue,2)+",";
                              str = str+SAuthUserCode+",";
                              str = str+SAuthUserCode+",";
                              str = str+"1"+")";
                    
                    String QS1 = " Update ItemStock set Stock=nvl(Stock,0)-("+dQty+")"+
                                 " Where ItemCode='"+SCode+"' and HodCode="+SAuthUserCode+" and MillCode="+iMillCode;


                    stat.execute(str);
                    stat.execute(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println( " insertIssueDetails()  ->"+ex);
               bComflag       =    false;
          }
     }

     public void insertTransferDetails()
     {
          String QString = "Insert Into StockTransfer(id,TranNo,TranDate,Code,Qty,Rate,UIRefNo,DcNo,DcDate,Dept_Code,Group_Code,Unit_Code,UserCode,CreationDate,SlNo,MillCode,ToMillCode,TransUserCode) Values (";
          
          try
          {
               Statement      stat           =  theConnection.createStatement();

               int iSlNo = 0;

               for(int i=0;i<MiddlePanel.RowData.length;i++)
               {
                    String SCode   = (String)MiddlePanel.tabreport.ReportTable.getValueAt(i,1);
                    double dQty    = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,7));
                    double dStock  = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,6));

                    if((SCode.length() == 0) || (dQty<=0) || (dStock<0))
                         continue;
                    
                    iSlNo++;

                    String SUnitCode = (String)VUnitCode.elementAt(JCUnit.getSelectedIndex());
                    double dRate = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,8));
                    
                    String    str = QString;
                              str = str+getStockTransferId()+",";
                              str = str+SIssueNo+",";
                              str = str+"'"+TDate.toNormal()+"',";
                              str = str+"'"+SCode+"',";
                              str = str+common.getRound(dQty,3)+",";
                              str = str+common.getRound(dRate,4)+",";
                              str = str+"0"+TRefNo.getText()+",";
                              str = str+"'"+TDCNo.getText()+"',";
                              str = str+"'"+TDCDate.toNormal()+"',";
                              str = str+MiddlePanel.getDepartmentCode(i)+",";
                              str = str+getCategoryCode(i)+",";
                              str = str+SUnitCode+",";
                              str = str+String.valueOf(iUserCode)+",";
                              str = str+"'"+common.getServerDate()+"',";
                              str = str+String.valueOf(iSlNo)+",";
                              str = str+String.valueOf(iMillCode)+",";
                              str = str+SSeleMillCode+",";
                              str = str+SAuthUserCode+")";
                    
                    stat.execute(str);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(" insertTransferDetails() StockTransferframe -> "+ ex);
               bComflag       =    false;
          }
     }
     
     public void UpdateIssueNo()
     {
          String SUString     = "";
          Statement stat      = null;

          int iId=0;
          try
          {
               stat           =  theConnection.createStatement();

               SUString  =   " Update config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1 where Id = 5 ";

               stat .executeUpdate(SUString);
               stat .close();
          }
          catch(Exception e)
          {
               System.out.println("StockTransferFrame UpdateIssueNo() ->"+e);
               e.printStackTrace();
               bComflag       =    false;
          }
     }

     public int checkIndentNo(String SURefNo)
     {
          int iCount=0;

          ResultSet result    = null;

          String SQuery    = " Select Count(*) from issue where UIRefNo="+SURefNo+" and millcode ="+iMillCode+" and IssueDate>="+SStDate+" order by 1" ;

          try  {
                    ORAConnection  oraConnection  = ORAConnection. getORAConnection();
                    Connection     theConnect     = oraConnection. getConnection();
                    Statement      stat           = theConnect   . createStatement();
                                   result         = stat         . executeQuery(SQuery);
                    while(result.next())
                    {
                         iCount = common.toInt((String)result.getString(1));
                    }
                    result.close();
                    stat.close();
          }catch(Exception ex)
          {
               System.out.println("getmaterials() from indentframe  ->"+ex);
               return 0;
          }
          return iCount;
     }

     public int checkItem(String SCode)
     {
          int iCount=0;
          String SSeleTable="";

          ResultSet result    = null;

          String QS = " Select Item_Table from Mill Where MillCode="+SSeleMillCode;

          try  {
                    ORAConnection  oraConnection  = ORAConnection. getORAConnection();
                    Connection     theConnect     = oraConnection. getConnection();
                    Statement      stat           = theConnect   . createStatement();
                                   result         = stat         . executeQuery(QS);
                    while(result.next())
                    {
                         SSeleTable = result.getString(1);
                    }
                    result.close();

                    if(SSeleTable.length()>0)
                    {
                         String SQuery    = " Select Count(*) from "+SSeleTable+" where Item_Code='"+SCode+"'";

                         result         = stat         . executeQuery(SQuery);
                         while(result.next())
                         {
                              iCount = common.toInt((String)result.getString(1));
                         }
                         result.close();
                    }
                    stat.close();
          }catch(Exception ex)
          {
               System.out.println("getmaterials() from itemtable  ->"+ex);
               return 0;
          }
          return iCount;
     }

     public void getIssueNo()
     {
          try
          {
               SIssueNo  = String.valueOf(common.toInt(control.getID("Select maxno From Config"+iMillCode+""+SYearCode+" where Id = 5 "))+1);
          }
          catch(Exception ex)
          {
               System.out.println(" getIssueNo ->"+ex);
               ex.printStackTrace();
          }
     }

     public void getUsers()
     {
          ResultSet result    =    null;
          Statement stat      =    null;

          VTo                 = new Vector();
          VToCode             = new Vector();
          VCata               = new Vector();
          VCataCode           = new Vector();
          VIUserCode          = new Vector();
          VIUserName          = new Vector();

          int iDivisionCode=0;

          if(iMillCode==0)
          {
               iDivisionCode = 1;
          }
          else
          if(iMillCode==1)
          {
               iDivisionCode = 2;
          }
          else
          if(iMillCode==3)
          {
               iDivisionCode = 15;
          }
          else
          if(iMillCode==4)
          {
               iDivisionCode = 14;
          }


          try
          {
               stat =  theConnection.createStatement();
               String QS1 = "";
               String QS2 = "";
               String QS3 = "";
               
               QS1 = " Select MillName,MillCode from Mill Where MillCode<>"+iMillCode+" Order by MillName";

               QS2 = " Select Group_Name,Group_Code From Cata Where TranSignal=1 Order By Group_Name";

               QS3 = " Select UserCode,UserName from RawUser Where UserCode in (Select Distinct(AuthUserCode) from MrsUserAuthentication Where AuthUserCode>1 and authusercode<>6125) and (DivisionCode=6 or DivisionCode="+iDivisionCode+") Order by UserName ";

               result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VTo    .addElement(result.getString(1));
                    VToCode.addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VCata     .addElement(result.getString(1));
                    VCataCode .addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery(QS3);
               while(result.next())
               {
                    VIUserCode. addElement(result.getString(1));
                    VIUserName. addElement(result.getString(2));
               }
               result    . close();
               stat.close();

               if(VIUserCode.size()<=0)
               {
                    VIUserCode.addElement("1");
                    VIUserName.addElement("ENTRY LEVEL");
               }
          }
          catch(Exception ex)
          {
               System.out.println("  getUsers STockTransferFRame Indent XyX :"+ex);
          }
     }

     public void setUnit(int iIndex)
     {
          ResultSet result    =    null;
          Statement stat      =    null;

          JCUnit.removeAllItems();

          VUnit    .removeAllElements();
          VUnitCode.removeAllElements();

          SSeleMillCode = (String)VToCode.elementAt(iIndex);
          
          try
          {
               stat =  theConnection.createStatement();
               String QS1 = "";
               
               QS1 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+common.toInt(SSeleMillCode)+" Order By Unit_Name";

               result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VUnit     .addElement(result.getString(1));
                    VUnitCode .addElement(result.getString(2));
               }
               result.close();
               stat.close();

               for(int i=0;i<VUnit.size();i++)
               {
                    JCUnit.addItem((String)VUnit.elementAt(i));
               }
          }
          catch(Exception ex)
          {
               System.out.println("  setUnit :"+ex);
          }
     }

     private int getIssueID()
     {
          Statement stat      = null;
          ResultSet result    = null;

          int iId=0;
          try
          {
               stat           =  theConnection    . createStatement();
               result         =  stat             . executeQuery("Select Issue_Seq.nextVal from Dual");

               while(result.next())
               {
                    iId  =    result.getInt(1);
               }
               result    .close();
               stat      .close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return iId;
     }

     private int getStockTransferId()
     {
          Statement stat      = null;
          ResultSet result    = null;

          int iId=0;
          try
          {
               stat           =  theConnection.createStatement();
               result         =  stat.executeQuery("Select STOCKTRANSFER_SEQ.nextVal from Dual");

               while(result.next())
               {
                    iId  =    result.getInt(1);
               }
               result    .close();
               stat      .close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return iId;
     }

     private int getCategoryCode(int iRow)
     {
          String SCata = ((String)MiddlePanel.tabreport.RowData[iRow][5]).trim();
          if(SCata.length() == 0)
               return 0;

          int iCata   = VCata.indexOf(SCata);

          return Integer.parseInt((String)VCataCode.elementAt(iCata));
     }

               /*         Data Validation                 */

     private boolean isStockValid()
     {
          boolean bflag = true;
          
          for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
          {
               String Smsg="";
               String SCode     = ((String)MiddlePanel.tabreport.RowData[i][1]).trim();
               double dStock    = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][6]).trim());
               
               if(SCode.length() == 0)
                    continue;
               
               if(dStock<0)
                    Smsg = Smsg + " Negative Stock for the Date,"; 
               if(Smsg.length()>0)
               {
                    Smsg = Smsg.substring(0,(Smsg.length()-1));
                    Smsg = Smsg + " in S.No - " + (i+1);
                    JOptionPane.showMessageDialog(null,Smsg,"Invalid Quantity",JOptionPane.INFORMATION_MESSAGE);
                    bflag=false;
               }
          }
          return bflag;
     }

     private boolean isValidItem()
     {
          boolean bflag = true;
          
          for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
          {
               int iCount=0;
               String Smsg="";
               String SCode     = ((String)MiddlePanel.tabreport.RowData[i][1]).trim();
               
               if(SCode.length() == 0)
                    continue;
               
               iCount = checkItem(SCode);

               if(iCount<=0)
               {
                    Smsg = Smsg + " Item Not Available in "+(String)JCTo.getSelectedItem()+","; 
               }

               if(Smsg.length()>0)
               {
                    Smsg = Smsg.substring(0,(Smsg.length()-1));
                    Smsg = Smsg + " in S.No - " + (i+1);
                    JOptionPane.showMessageDialog(null,Smsg,"Invalid Item",JOptionPane.INFORMATION_MESSAGE);
                    bflag=false;
               }
          }
          return bflag;
     }

     private boolean isEligible(int iRowCount)
     {
          double dQty         = common.toDouble(common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(iRowCount,7)));
          String SItemCode    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(iRowCount,1));

          if(dQty<=0)
               return false;
          if(SItemCode.length()<3)
               return false;

          return true;
     }
     
     private boolean isDataValid()
     {
          boolean bflag = true;

          String SURefNo =    ((String)TRefNo.getText()).trim();

          int iCount = checkIndentNo(SURefNo);

          if(iCount>0)
          {
               JOptionPane.showMessageDialog(null,"The Entered User Indent No is already Exists Please Change and Save the Data","Missing",JOptionPane.INFORMATION_MESSAGE);
               return false;
          }

          for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
          {
               String Smsg="";
               String SCode         = ((String)MiddlePanel.tabreport.RowData[i][1]).trim();

               if(i==0)
               {
                    if(SCode.length()==0)
                    {
                         JOptionPane.showMessageDialog(null,"No Data in the Table","Missing",JOptionPane.INFORMATION_MESSAGE);
                         return false;
                    }
               }

               if(SCode.length() == 0)
                    continue;
               
               if(MiddlePanel.getDepartmentCode(i)==0)
                    Smsg = Smsg + "Department,";

               if(getCategoryCode(i)==0)
                    Smsg = Smsg + " Classification,";

               if(!isEligible(i))
                    Smsg = Smsg + " Quantity,";

               if(Smsg.length()>0)
               {
                    Smsg = Smsg.substring(0,(Smsg.length()-1));
                    Smsg = Smsg + "  Field(s) Missing in S.No - " + (i+1);
                    JOptionPane.showMessageDialog(null,Smsg,"Missing Fields",JOptionPane.INFORMATION_MESSAGE);
                    bflag=false;
               }
          }
          if((TDate.toNormal()).length()!=8)
          {
               JOptionPane.showMessageDialog(null,"Date Field is Invalid","Invalid Field",JOptionPane.INFORMATION_MESSAGE);
               bflag=false;
          }     
          if(TRefNo.getText().length()==0)
          {
               JOptionPane.showMessageDialog(null,"Ref No must be filled","Invalid Field",JOptionPane.INFORMATION_MESSAGE);
               bflag=false;
          }     
          return bflag;
     }
     
     private boolean isValidData()
     {
          try
          {
               String SRefNo       = common.parseNull(TRefNo.getText().trim());

               String SDate        = TDate.toNormal();
                      SDate        = SDate.trim();

               String SCurDate     = common.getServerDate();
                      SCurDate     = SCurDate.substring(0,8);

               int    iDateDiff    = common.toInt(common.getDateDiff(common.parseDate(SDate),SCurDate));
               String SDCNo        = common.parseNull(TDCNo.getText().trim());
               String SDCDate      = TDCDate.toNormal();

               int    iDCDateDiff  = common.toInt(common.getDateDiff(common.parseDate(SDCDate),common.parseDate(SCurDate)));

               if((SDate.length() < 8) || (iDateDiff>0))
               {

                    JOptionPane.showMessageDialog(null,"Date is Invalid","Error",JOptionPane.ERROR_MESSAGE);
                    TDate.TDay.requestFocus();
                    return false;
               }
               if(SRefNo.length()==0)
               {
                    JOptionPane.showMessageDialog(null,"Indent No Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
                    TRefNo.requestFocus();
                    return false;
               }
               if(SDCNo.length()==0)
               {
                    JOptionPane.showMessageDialog(null,"D.C. No Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
                    TDCNo.requestFocus();
                    return false;
               }
               if((SDCNo.length()==0) && (SDCDate.length()>0))
               {
                    JOptionPane.showMessageDialog(null,"D.C. No Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
                    TDCNo.requestFocus();
                    return false;
               }
               if((SDCNo.length()>0) && ((SDCDate.length()<8) || (iDCDateDiff>0)))
               {
                    JOptionPane.showMessageDialog(null,"D.C. Date is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
                    TDCDate.TDay.requestFocus();
                    return false;
               }
          }
          catch(Exception ex)
          {
               System.out.println(" isValid in StockTransferFrame ->"+ex);
          }
          return true;
     }

     private void updateInvitemsData()
     {
          ResultSet result    = null;
          int iType           = 0;
          try
          {
               Statement stat =  theConnection.createStatement();

               for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
               {     
                    if(!isEligible(i))
                         continue;

                    String SItemName    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,2));
                    String SItemCode    = common.parseNull((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,1));
                    double dIssueQty    = common.toDouble(common.parseNull(((String)MiddlePanel.tabreport.RowData[i][7]).trim()));
                    double dRate        = common.toDouble((String)MiddlePanel.tabreport.ReportTable.getValueAt(i,8));
                    double dIssueVal    = common.toDouble(common.getRound(String.valueOf(dRate*dIssueQty),2));

                    String QString = "";

                    QString =   " Update "+SItemTable+" ";

                    QString = QString+" Set IssQty=nvl(IssQty,0)+"+dIssueQty+",IssVal=nvl(IssVal,0)+"+dIssueVal+" ";
                    QString = QString+" Where Item_Code = '"+SItemCode+"'";
                    stat.execute(QString);
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("Indent Frame updateInvitemsData() ->"+e);
               e.printStackTrace();
               bComflag  = false;
          }
     }
}
