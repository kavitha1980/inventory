package Indent.IndentFiles.Transfer;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class  StockTransferMiddlePanel extends JPanel
{
     Object              RowData[][];
     String              ColumnData[] = {"SNo","Code","Name","UOM","Department","Classification","Stock","Quantity","Rate","Value"};
     String              ColumnType[] = {"N","S","S","S","B","B","N","E","N","N"};
     
     JLayeredPane        Layer;
     Vector              VCode,VName,VUom,VNameCode,VUnitCode,VDeptCode,VDept,VCata,VCataCode;
     JComboBox           JCUnit,JCCata,JCDept;
     STMaterialSearch    MS;
     STTabReport         tabreport;
     Common              common = new Common();
     int                 iUserCode,iMillCode;
     String              SItemTable,SSupTable,SAuthUserCode;

     StockTransferMiddlePanel(JLayeredPane Layer,Vector VCode,Vector VName,Vector VUom,Vector VNameCode,JComboBox JCUnit,Vector VUnitCode,JComboBox JCCata,Vector VCata,Vector VCataCode,int iUserCode,int iMillCode,String SItemTable,String SSupTable,String SAuthUserCode)
     {
          this.Layer         = Layer;
          this.VCode         = VCode;
          this.VName         = VName;
          this.VUom          = VUom;
          this.VNameCode     = VNameCode;
          this.JCUnit        = JCUnit;
          this.VUnitCode     = VUnitCode;
          this.JCCata        = JCCata;
          this.VCata         = VCata;
          this.VCataCode     = VCataCode;
          this.iUserCode     = iUserCode;
          this.iMillCode     = iMillCode;
          this.SItemTable    = SItemTable;
          this.SSupTable     = SSupTable;
          this.SAuthUserCode = SAuthUserCode;
          
          setDeptVectors();
          createComponents(8);
     }
     
     public void createComponents(int irows)
     {
          JCDept         = new JComboBox(VDept);
          RowData        = new Object[irows][ColumnData.length];

          for(int i=0;i<irows;i++)
          {
               RowData[i][0] = String.valueOf(i+1);
               RowData[i][1] = " ";
               RowData[i][2] = " ";
               RowData[i][3] = " ";
               RowData[i][4] = " ";
               RowData[i][5] = " ";
               RowData[i][6] = " ";
               RowData[i][7] = " ";
               RowData[i][8] = "0";
               RowData[i][9] = "0";
          }
          try
          {
               setLayout(new BorderLayout());
               tabreport   = new STTabReport(RowData,ColumnData,ColumnType,JCCata,JCDept,iMillCode,SItemTable,SSupTable,SAuthUserCode);
               add(tabreport,BorderLayout.CENTER);
               updateUI();
               addListeners();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     
     public void addListeners()
     {
                                   tabreport.ReportTable.addKeyListener(new KeyList());
          JTableHeader   th   =    tabreport.ReportTable.getTableHeader();
                         th   .    addMouseListener(new STTableHeaderHandle(this));
     }
     
     private class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    showMaterialSelectionFrame();
               }
          }
     }
     
     private void showMaterialSelectionFrame()
     {
          try
          {
               Layer.remove(MS);
               Layer.updateUI();
          }
          catch(Exception ex){}
          
          try
          {
               MS   = new STMaterialSearch(Layer,VCode,VName,VUom,VNameCode,this,tabreport.ReportTable.getSelectedRow(),iMillCode,SItemTable,SSupTable,SAuthUserCode);
               MS   .setSelected(true);
               Layer.add(MS);
               Layer.repaint();
               MS   .moveToFront();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }
     
     public int getDepartmentCode(int iRow)
     {
          String SDept = ((String)tabreport.RowData[iRow][4]).trim();
          if(SDept.length() == 0)
               return 0;

          int iDept       = VDept.indexOf(SDept);
          return Integer.parseInt((String)VDeptCode.elementAt(iDept));
     }
     
     private void setDeptVectors()
     {
          ResultSet result    = null;

          VDept      = new Vector();
          VDeptCode  = new Vector();
          
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       stat          =  theConnection.createStatement();
               
               String QS1 = "";

               QS1 = "Select Dept_Name,Dept_Code From Dept Where TranSignal=1 Order By Dept_Name";

               result    = stat.executeQuery(QS1);
               while(result.next())
               {
                    VDept     .addElement(result.getString(1));
                    VDeptCode .addElement(result.getString(2));
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);   
          }
     }
}
