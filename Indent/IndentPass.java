package Indent;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

import com.digitalpersona.onetouch.*;
import com.digitalpersona.onetouch.capture.*;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.*;
import com.digitalpersona.onetouch.verification.*;

public class IndentPass
{
     Accept AD;

     ArrayList theTemplateList,theMaterialIssueTemplateList;
     Connection          theConnection1 =    null;


     public IndentPass()
     {
          try
          {
               onLoad();
               AD.setVisible(false);
          }
          catch(Exception ex){}
          
          try
          {
               AD = new Accept();
               AD.show();
               AD.BAccept.addActionListener(new ADList());
               AD.BDeny.addActionListener(new ADList());
          }          
          catch(Exception ex)
          {
               System.out.println("991 "+ex);
               ex.printStackTrace();
          }
     }
     
     public static void main(String arg[])
     {
          new IndentPass();
     }

     public class ADList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(AD.BDeny==ae.getSource())
               {
                    AD.setVisible(false);
                    System.exit(0);
               }
               if(ae.getSource()==AD.BAccept)
               {
                    startHRD();
               }
          }
     }

     public void startHRD()
     {
          if(AD.isPassValid())
          {
               AD.setVisible(false);
               Indent indent = new Indent(AD.getHRCode(),AD.getAuthCode(),AD.getMillCode(),AD.getYear(),AD.getStDate(),AD.getEnDate(),AD.getYearCode(),AD.getItemTable(),AD.getSupTable(),AD.getMillName(),theTemplateList,theMaterialIssueTemplateList);
          }
          else
          {
               JOptionPane.showMessageDialog(null, "Password is Invalid", "Warning", 
               JOptionPane.ERROR_MESSAGE,new ImageIcon("Warning.gif"));
               AD.TA.setText("");
          }
     }
     public void onLoad()
     {
            try
            {

                theTemplateList    = new ArrayList();
                theMaterialIssueTemplateList = new ArrayList();

                Class.forName("oracle.jdbc.OracleDriver");
                theConnection1 = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","essl","essl");
                String sqlStmt=" Select EmpCode,DisplayName,FingerPrint,MaterialIssueStatus from OneTouchEmployee where MaterialIssueStatus=1";
				
                Statement st=theConnection1.createStatement();
                ResultSet rs = st.executeQuery(sqlStmt);
                while(rs.next())
                {
                      byte[] data =null;
					  
                      data  = getOracleBlob(rs,"FINGERPRINT");
                      DPFPTemplate t = DPFPGlobal.getTemplateFactory().createTemplate();
                      t.deserialize(data);
                      HashMap theMap = new HashMap();
                    
                      theMap.put("EMPCODE",rs.getString(1));
                      theMap.put("DISPLAYNAME",rs.getString(2));
                      theMap.put("TEMPLATE",t);

                      if(Integer.parseInt(rs.getString(4))==1)
                      {
                           theMaterialIssueTemplateList.add(theMap);
                      }
                      else
                      {
                           theTemplateList.add(theMap);
                      }

                }
				rs.close();
				st.close();
				theConnection1.close();
			} catch (Exception ex) {

                    System.out.println(ex);
                    ex.printStackTrace();
                    //JOptionPane.showMessageDialog(this, ex.getLocalizedMessage(), "Fingerprint loading", JOptionPane.ERROR_MESSAGE);
			}
    } 

    private byte[] getOracleBlob(ResultSet result, String columnName) throws SQLException
    {   
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        byte[] bytes = null;
        
        try {    
            oracle.sql.BLOB blob = ((oracle.jdbc.OracleResultSet)result).getBLOB(columnName);        
            inputStream = blob.getBinaryStream();        
            int bytesRead = 0;        
            
            while((bytesRead = inputStream.read()) != -1) {        
                outputStream.write(bytesRead);    
            }
            
            bytes = outputStream.toByteArray();
            
        } catch(IOException e) {
            throw new SQLException(e.getMessage());
        } finally 
        {
        }
    
        return bytes;
    }

}
