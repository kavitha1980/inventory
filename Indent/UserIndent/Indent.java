package Indent.UserIndent;

import java.util.*;
import util.*;
import java.io.*;


public class Indent
{
     int iIndentNo,iDeptCode,iUnitCode;

     ArrayList arList;

     public Indent(int iIndentNo,int iDeptCode,int iUnitCode)
     {
          this.iIndentNo = iIndentNo;
          this.iDeptCode = iDeptCode;
          this.iUnitCode = iUnitCode;

          arList         = new ArrayList();
     }
     public int getDeptCode()
     {
          return iDeptCode;

     }
     public int getUnitCode()
     {
          return iUnitCode;
     }

     public void setIndentData(int iId,String SItemCode,double dQty)
     {
          HashMap theMap = new HashMap();

          theMap.put("Id"               , String.valueOf(iId));
          theMap.put("ItemCode"         , SItemCode);
          theMap.put("Qty"              , String.valueOf(dQty));

          arList.add(theMap);
     }


     public void setIndentData(int iId,String SItemCode,double dQty,double dStock,String SItemName,String SStkGrpCode)
     {
          HashMap theMap = new HashMap();

          theMap.put("Id"               , String.valueOf(iId));
          theMap.put("ItemCode"         , SItemCode);
          theMap.put("Qty"              , String.valueOf(dQty));
          theMap.put("Stock"            , String.valueOf(dStock));
          theMap.put("ItemName"         , SItemName);
          theMap.put("StkGrpCode"       , SStkGrpCode);


          arList.add(theMap);
     }

     public int getIndentNo()
     {
          return iIndentNo;
     }

     public ArrayList getItemList()
     {
          return arList;
     }
     public void remove(String SItemCode)
     {
          for(int i=0; i<arList.size(); i++)
          {
               HashMap theMap = (HashMap)arList.get(i);

               if(((String)theMap.get("ItemCode")).equals(SItemCode))
               {
                    arList.remove(i);
                    break;
               }
          }
     }
     public void setQty(String SItemCode,double dQty)
     {
          for(int i=0; i<arList.size(); i++)
          {
               HashMap theMap = (HashMap)arList.get(i);

               if(((String)theMap.get("ItemCode")).equals(SItemCode))
               {
                    theMap.put("Qty",String.valueOf(dQty));
                    break;
               }
          }
     }
     public String getId(String SItemCode)
     {
          String SId="";
          for(int i=0; i<arList.size(); i++)
          {
               HashMap theMap = (HashMap)arList.get(i);

               if(((String)theMap.get("ItemCode")).equals(SItemCode))
               {
                    SId = (String)theMap.get("Id");
                    break;
               }
          }
          return SId;
     }

}


