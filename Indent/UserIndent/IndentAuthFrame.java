package Indent.UserIndent;

import java.awt.*;
import java.awt.event.*;

import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import util.*;
import javax.swing.border.*;
import jdbc.*;
import java.sql.*;
import java.net.*;

import guiutil.*;

public class IndentAuthFrame extends JInternalFrame
{

     JPanel       TopPanel,MiddlePanel,BottomPanel;
     JLayeredPane theLayer;

     MyComboBox   CIndentNo;
     JButton      BAuthenticate;

     JTable       theTable;

     IndentModel  theModel;

     int iUserCode,iMillCode;
     Common    common = new Common();

     Vector    theIndent;
     Item1      item;
     String    SItemTable;
     public IndentAuthFrame(JLayeredPane theLayer,int iUserCode,int iMillCode,String SItemTable)
     {
          this.theLayer  = theLayer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SItemTable= SItemTable;

          try
          {
               setData();
               createComponents();
               setLayouts();
               addComponents();
               addListeners();
               setModel();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void createComponents()
     {
          TopPanel       = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();

          CIndentNo      = new MyComboBox(getIndentNo());
     

          theModel      = new IndentModel();
          theTable      = new JTable(theModel);

          BAuthenticate         = new JButton(" Authenticate ");
     }

     public void setLayouts()
     {
          TopPanel  . setLayout(new GridLayout(1,6));
          MiddlePanel.setLayout(new BorderLayout());


          MiddlePanel.setBorder(new TitledBorder(" Materia List"));

          setTitle(" Indent  Authentication");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,600,475);
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel(" IndentNo"));
          TopPanel.add(CIndentNo);
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));


          MiddlePanel.add(new JScrollPane(theTable));

          BottomPanel    . add(BAuthenticate);
     
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

     }
     public void addListeners()
     {
          BAuthenticate.addActionListener(new ActList());
          CIndentNo.addItemListener(new ItemsList());
          theTable.addKeyListener(new KeyList());
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               saveData();
          }
     }

     private class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               int iIndentNo = common.toInt((String)CIndentNo.getSelectedItem());
               int iIndex    = getIndexOf(iIndentNo);
               if(iIndex==-1)
                    return;
               Indent indent   = (Indent)theIndent.elementAt(iIndex);



               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    int iRow = theTable.getSelectedRow();
                    String SItemCode= (String)theModel.getValueAt(iRow,0);

                    if(common.toInt(indent.getId(SItemCode))>0)
                         return;

                    ItemList itemlist = new ItemList(theModel,iRow,iMillCode,iUserCode,SItemTable);
               }


               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    theModel.appendEmptyRow();
               }
               if(ke.getKeyCode()==KeyEvent.VK_DELETE)
               {
                    int iRow = theTable.getSelectedRow();
                    String SItemCode= (String)theModel.getValueAt(iRow,0);

                    if(JOptionPane.showConfirmDialog(null,"Confirm To Delete the Row?","Information",JOptionPane.YES_NO_OPTION) == 0)
                    {
                         indent.remove(SItemCode);
                         theModel.deleteRow(iRow);
                         delete(iIndentNo,SItemCode);
                    }
               }
          }
     }

     private class ItemsList implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
               setModel();
          }
     }
     private void saveData()
     {
          int iIndentNo = common.toInt((String)CIndentNo.getSelectedItem());
          int iIndex    = getIndexOf(iIndentNo);

          if(iIndex==-1)
               return;
          Indent indent = (Indent)theIndent.elementAt(iIndex);

          ArrayList arList = indent.getItemList();

          for(int i=0; i<theModel.getRows(); i++)
          {
               
               String SItemCode= (String)theModel.getValueAt(i,0);
               double dQty     = common.toDouble((String)theModel.getValueAt(i,4));
               String SId      = indent.getId(SItemCode);

               if(common.toInt(SId)==0)
               {
                    indent.setIndentData(0,SItemCode,dQty);
               }
               else
               {
                    indent.setQty(SItemCode,dQty);
               }
          }
          try
          {
               Connection theConnection=null;

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               PreparedStatement thePrepare=null;

               String SInsQS=" Insert into Indent(IndentNo,IndentDate,DeptCode,Unit_Code,Code,Qty,MillCode,UserCode,CreationDate,EntrySystemName,Status,AuthSystemName,AuthUserCode)"+
                                           "values(?,?,?,?,?,?,?,?,?,?,?.?,?)";


               String SUpQS= " Update Indent set Qty=? ,Status=?,AuthUserCode=?,AuthSystemName=? where IndentNo=? and Code=? ";

               for(int i=0; i<arList.size(); i++)
               {
                    HashMap theMap = (HashMap)arList.get(i);

                    int  iId       = common.toInt((String)theMap.get("Id"));

                    if(iId==0)
                    {
                         thePrepare     = theConnection.prepareStatement(SInsQS);

                         thePrepare.setInt(1,iIndentNo);
                         thePrepare.setString(2,common.getServerDate().substring(0,8));
                         thePrepare.setInt(3,indent.getDeptCode());
                         thePrepare.setInt(4,indent.getUnitCode());
                         thePrepare.setString(5,(String)theModel.getValueAt(i,0));
                         thePrepare.setDouble(6,common.toDouble((String)theModel.getValueAt(i,4)));
                         thePrepare.setInt(7,iMillCode);
                         thePrepare.setInt(8,iUserCode);
                         thePrepare.setString(9,common.getServerDate());
                         thePrepare.setString(10,InetAddress.getLocalHost().getHostName());
                         thePrepare.setInt(11,1);
                         thePrepare.setString(12,InetAddress.getLocalHost().getHostName());
                         thePrepare.setInt(13,iUserCode);


                         thePrepare.executeUpdate();
                    }
                    else
                    {
                         thePrepare    = theConnection.prepareStatement(SUpQS);

                         thePrepare.setDouble(1,common.toDouble((String)theModel.getValueAt(i,4)));
                         thePrepare.setInt(2,1);
                         thePrepare.setInt(3,iUserCode);
                         thePrepare.setString(4,InetAddress.getLocalHost().getHostName());
                         thePrepare.setInt(5,iIndentNo);
                         thePrepare.setString(6,(String)theModel.getValueAt(i,0));

                         thePrepare.executeUpdate();
                    }
               }
               thePrepare.close();

               JOptionPane.showMessageDialog(null," Indent Was Authenticated Successfully");
               setData();
               CIndentNo.setValues(getIndentNo());

          }

          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void setModel()
     {
          theModel.setNumRows(0);
          for(int i=0; i<theIndent.size(); i++)
          {
               Indent indent = (Indent)theIndent.elementAt(i);

               int iIndentNo = common.toInt((String)CIndentNo.getSelectedItem());


               if(indent.getIndentNo()==iIndentNo)
               {

                    ArrayList itemlist =     indent.getItemList();


                    for(int j=0; j<itemlist.size(); j++)
                    {

                         HashMap theMap   = (HashMap)itemlist.get(j);

                         String SItemCode         = (String)theMap.get("ItemCode");

                         String SItemName         = (String)theMap.get("ItemName");
                         String SStockGrpCode     = (String)theMap.get("StkGrpCode");
                         double dQty              = common.toDouble((String)theMap.get("Qty"));
                         double dStock            = common.toDouble((String)theMap.get("Stock"));     

                         theModel.appendRow(SItemCode,SItemName,SStockGrpCode,dStock,dQty);
                    }
               }
          }
     }
     private void setData()
     {
          theIndent = new Vector();
          try
          {
               Connection theConnection=null;

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               PreparedStatement thePrepare=theConnection.prepareStatement(getIndentQuery());
               thePrepare.setInt(1,iUserCode);

               ResultSet theResult = thePrepare.executeQuery();

               while(theResult.next())
               {
                    Indent indent = null;

                    int       iIndentNo = theResult.getInt(1);
                    int       iId       = theResult.getInt(2);
                    String    SItemCode = theResult.getString(3);
                    double    dQty      = theResult.getDouble(4);
                    int       iUnitCode = theResult.getInt(5);
                    int       iDeptCode = theResult.getInt(6);
                    double    dStock    = theResult.getDouble(7);
                    String    SItemName = theResult.getString(8);
                    String    SStkGrpCode= theResult.getString(9);

                



                    int iIndex = getIndexOf(iIndentNo);

                    if(iIndex==-1)
                    {
                         indent = new Indent(iIndentNo,iDeptCode,iUnitCode);
                         theIndent.addElement(indent);
                         iIndex = theIndent.size()-1;
                    }
                    indent = (Indent)theIndent.elementAt(iIndex);
                    indent.setIndentData(iId,SItemCode,dQty,dStock,SItemName,SStkGrpCode);
               }

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private String getIndentQuery()
     {
          String QS= "  Select Indent.IndentNo,Indent.Id,Indent.Code,Indent.Qty,Unit_code,DeptCode,ItemStock.Stock,InvItems.Item_Name,InvItems.StkGroupcode from (Indent"+
                     " Inner join InvItems on  InvItems.Item_Code=Indent.Code)"+
                     " Inner join ItemStock on ItemStock.Itemcode=Indent.Code"+
                     " and Indent.UserCode=(select AuthUsercode from MrsUserAuthentication where UserCode=?)"+
                     " Where  Indent.Status=0"+
                     " Order by 1";


          return QS;
     }
     private Vector getIndentNo()
     {
          Vector VIndentNo = new Vector();

          for(int i=0; i<theIndent.size(); i++)
          {
               Indent indent = (Indent)theIndent.elementAt(i);

               VIndentNo.addElement(String.valueOf(indent.getIndentNo()));

          }
          return VIndentNo;
     }
     private int getIndexOf(int iIndentNo)
     {
          int iIndex = -1;

          for(int i=0; i<theIndent.size(); i++)
          {
               Indent indent = (Indent)theIndent.elementAt(i);

               if(indent.getIndentNo()==iIndentNo)
               {
                    iIndex = i;
                    break;

               }
          }
          return iIndex;
     }
     private void delete(int iIndentNo,String SItemCode)
     {
          try
          {
               Connection theConnection=null;

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               PreparedStatement thePrepare=theConnection.prepareStatement("Delete from Indent where IndentNo=? and Code=?");
               thePrepare.setInt(1,iIndentNo);
               thePrepare.setString(2,SItemCode);

               thePrepare.executeUpdate();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
}


