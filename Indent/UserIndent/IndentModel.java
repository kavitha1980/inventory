package Indent.UserIndent;

import java.awt.*;
import java.awt.event.*;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import util.*;


public class IndentModel extends DefaultTableModel
{
                         //   0              1         2         3              4               5      6         7

     String ColumnName[] ={"ItemCode","ItemName","StockGroup","Department","Classification","Stock","Req_Qty","Balance"};
     String ColumnType[] ={"S","S","S","E","E","S","E","S"};

     Vector theVector;
     Common common ;
     int iMillCode,iUserCode;
     String SItemTable;
     public IndentModel()
     {
          common         = new Common();
          setDataVector(getRowData(),ColumnName);

     }
     public Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          RowData[0][0] = "";
          RowData[0][1] = "";
          RowData[0][2] = "";
          RowData[0][3] = "";
          RowData[0][4] = "";
          RowData[0][5] = "";
          RowData[0][6] = "";
          RowData[0][7] = "";

          return RowData;
     }
     public boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="B" || ColumnType[col]=="E")
               return true;
          return false;
     }

     public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }

     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
               if(column==6)
               setBalance(row);


          }

          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }
     public int getRows()
     {
         return super.dataVector.size(); 
     }
     public void appendEmptyRow()
     {
          Vector theVect = new Vector();
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");

          insertRow(getRows(),theVect);

     }
     public void deleteRow(int index)
     {
         if(getRows()==1)
         return;
         if(index>=getRows())
         return;
         if(index==-1)
         return;
         removeRow(index);
     }
     public void setBalance(int iRow)
     {
          double dStock  = common.toDouble((String)getValueAt(iRow,5));
          double dReqQty = common.toDouble((String)getValueAt(iRow,6));

          if((dStock-dReqQty)<0)
          {
               JOptionPane.showMessageDialog(null," Invalid Request Qty");
               setValueAt("",iRow,6);
               return;
          }
          setValueAt(String.valueOf((dStock-dReqQty)),iRow,7);
     }    
     public void appendRow(String SItemCode,String SItemName,String SStockGroupCode,double dStock,double dQty)
     {

          Vector theVect = new Vector();

          theVect.addElement(SItemCode);
          theVect.addElement(SItemName);
          theVect.addElement(SStockGroupCode);
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement(String.valueOf(dStock));
          theVect.addElement(String.valueOf(dQty));
          theVect.addElement(String.valueOf(dStock-dQty));

          insertRow(getRows(),theVect);
     }
     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

}

