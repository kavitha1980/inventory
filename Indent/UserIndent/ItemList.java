package Indent.UserIndent;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.io.*;
import java.sql.*;


import util.*;
import guiutil.*;
import jdbc.*;

public class ItemList 
{

     protected int iRow,iCol;
     protected String STitle;

     JDialog   dialog;
     JPanel    thePanel,TopPanel;
     JList     BrowList;
     JLabel    LIndicator;
     JTextField TStartText,TMiddleText,TEndText;

     String str="";

     Common common = new Common();

     Connection theConnection=null;
     Vector VCode,VName,VStkGroupCode,VStock;

     IndentModel theModel;
     String SItemTable;
     int iMillCode,iUserCode;
     public ItemList(IndentModel theModel,int iRow,int iMillCode,int iUserCode,String SItemTable)
     {
          this.theModel = theModel;
          this.iRow     = iRow;
          this.iMillCode= iMillCode;
          this.iUserCode= iUserCode;
          this.SItemTable=SItemTable;



          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          try
          {
               ORAConnection jdbc   = ORAConnection.getORAConnection();
               theConnection        = jdbc.getConnection();


          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
          activate();
     }
          
     private void createComponents()
     {
          TopPanel   = new JPanel();
          thePanel   = new JPanel(true);
          BrowList   = new JList();

          TStartText  = new JTextField(30);
          TMiddleText = new JTextField(30);
          TEndText    = new JTextField(30);

          dialog     = new JDialog(new Frame(),STitle,true);
     }
               
     private void setLayouts()
     {
          TopPanel.setLayout(new GridLayout(2,6));
          thePanel.setLayout(new  BorderLayout());
          BrowList.setFont(new Font("monospaced", Font.PLAIN, 11));
     }

     private void addComponents()
     {
          TopPanel.add(new JLabel(" Starting Text "));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(" Middle Text "));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(" End Text "));
          TopPanel.add(new JLabel(""));


          TopPanel.add(TStartText);
          TopPanel.add(new JLabel(""));
          TopPanel.add(TMiddleText);
          TopPanel.add(new JLabel(""));
          TopPanel.add(TEndText);
          TopPanel.add(new JLabel(""));


          thePanel.add("Center",new JScrollPane(BrowList));
     }

     private void addListeners()
     {
          TStartText.addKeyListener(new KeyList());
          TMiddleText.addKeyListener(new KeyList1());
          TEndText.addKeyListener(new KeyList2());

          BrowList.addKeyListener(new KeyList3());
     }
     
     public void activate()
     {
          dialog.getContentPane().add("North",TopPanel);
          dialog.getContentPane().add("Center",thePanel);
          dialog.setBounds(50,50,500,500);
          dialog.setVisible(true);

          BrowList.requestFocus();
     }
     
     public void deactivate()
     {
          dialog.setVisible(false);
/*        try
          {
               theConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }*/
     }

     private class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setItemList(0);
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setItemList(0);
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         str="";
                    }

               }
               catch(Exception ex)
               {
               }
          }

     }

     private class KeyList1 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setItemList(1);
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setItemList(1);
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         str="";
                    }

               }
               catch(Exception ex)
               {
               }
          }


     }
     private class KeyList2 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setItemList(2);
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setItemList(2);
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         str="";
                    }
               }
               catch(Exception ex)
               {
               }
          }

     }

     private class KeyList3 extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    setDetails(BrowList.getSelectedIndex());
               }
               if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
               {
                    setDetails(-1);
               }
          }
     }
     private void setItemList(int iStatus)
     {
          VCode = new Vector();
          VName = new Vector();
          VStkGroupCode= new Vector();
          VStock       = new Vector();  

          try
          {
               String QS="";

               if(!str.equals(""))
               {
                    if(iStatus==0)
                    {

                        QS ="  Select Item_Code,rpad(Item_Name,50),decode(ItemStock.Stock,null,0,ItemStock.Stock)-decode(ItemStock.IndentQty,null,0,ItemStock.IndentQty),StkGroupCode from InvItems"+
                            "  Left join ItemStock on ItemStock.ItemCode=InvItems.Item_Code"+
                            "  and ItemStock.MillCode="+iMillCode+" and (ItemStock.HodCode in(select AuthUserCode from MrsUserAuthentication where UserCode="+iUserCode+")"+
                            "  )"+ // or ItemStock.HodCode=1
                            "  where Item_Name like  '"+str+"%'"+
                            "  and Stock>0";
                    }
                    if(iStatus==1)
                    {
                        QS ="  Select Item_Code,rpad(Item_Name,50),decode(ItemStock.Stock,null,0,ItemStock.Stock)-decode(ItemStock.IndentQty,null,0,ItemStock.IndentQty),StkGroupCode from InvItems"+
                            "  Left join ItemStock on ItemStock.ItemCode=InvItems.Item_Code"+
                            "  and ItemStock.MillCode="+iMillCode+" and (ItemStock.HodCode in(select AuthUserCode from MrsUserAuthentication where UserCode="+iUserCode+")"+
                            "  )"+  //or ItemStock.HodCode=1
                            "  where Item_Name like  '%"+str+"%'"+
                            "  and Stock>0";
                    }
                    if(iStatus==2)
                    {
                        QS ="  Select Item_Code,rpad(Item_Name,50),decode(ItemStock.Stock,null,0,ItemStock.Stock)-decode(ItemStock.IndentQty,null,0,ItemStock.IndentQty),StkGroupCode from InvItems"+
                            "  Left join ItemStock on ItemStock.ItemCode=InvItems.Item_Code"+
                            "  and ItemStock.MillCode="+iMillCode+" and (ItemStock.HodCode in(select AuthUserCode from MrsUserAuthentication where UserCode="+iUserCode+")"+
                            "  )"+  //or ItemStock.HodCode=1
                            "  where Item_Name like  '%"+str+"'"+
                            "  and Stock>0";
                    }

     
                    PreparedStatement thePrepare = theConnection.prepareStatement(QS);
     
                    ResultSet theResult = thePrepare.executeQuery();
                    while(theResult.next())
                    {

                         String SItemName= theResult.getString(2);
                         String SStock   = theResult.getString(3);
                         String SItemWithStock = SItemName+"   -   "+SStock;

                         VCode.addElement(theResult.getString(1));
                         VName.addElement(SItemWithStock);
                         VStock.addElement(SStock);
                         VStkGroupCode.addElement(theResult.getString(4));
                    }
                    theResult.close();
                    thePrepare.close();
               }
               BrowList.setListData(VName);
               BrowList.setSelectedIndex(0);


          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

     }
     private void setDetails(int index)
     {
          
          if(index > -1)
          {
               String SItemCode = (String)VCode.elementAt(index);
               String SItemName = (String)VName.elementAt(index);
               String SStkGrpCd = (String)VStkGroupCode.elementAt(index);
               String SStock    = (String)VStock.elementAt(index);


               if(isExist(SItemCode))
               {
                    JOptionPane.showMessageDialog(null," Item Already Selected for Indent");
                    return;
               }
               if(common.toDouble(SStock)<=0)
               {
                    JOptionPane.showMessageDialog(null," Stock is Zero");
                    return;
               }


               theModel.setValueAt(SItemCode,iRow,0);
               theModel.setValueAt(SItemName,iRow,1);

               theModel.setValueAt(SStock,iRow,3);
               theModel.setValueAt(SStkGrpCd,iRow,2);

          }
          dialog.setVisible(false);
     }
     private boolean isExist(String SItemCode)
     {
          boolean bFlag = false;
          for(int i=0; i<theModel.getRows(); i++)
          {
               if(((String)theModel.getValueAt(i,0)).equals(SItemCode))
               {
                    bFlag = true;
               }
          }
          return bFlag;
     }


}
