package Indent.UserIndent;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.io.*;
import java.sql.*;


import util.*;
import guiutil.*;
import jdbc.*;

public class ItemsList 
{

     protected int iRow,iCol;
     protected String STitle;

     JDialog   dialog;
     JPanel    thePanel,TopPanel,MidLeft,MidRight,MiddlePanel;
     JList     BrowList,SelectedList;
     JLabel    LIndicator;
     JTextField TStartText,TMiddleText,TEndText;

     JButton   BOk;


     String str="";

     Common common = new Common();

     Connection theConnection=null;
     Vector VCode,VName,VStkGroupCode,VStock,VShowName;

     IndentModel theModel;
     String SItemTable;
     int iMillCode,iUserCode;
     Vector VItemCode,VItemName,VItemStock,VItemStkGrpCode;
     public ItemsList(IndentModel theModel,int iRow,int iMillCode,int iUserCode,String SItemTable)
     {
          this.theModel = theModel;
          this.iRow     = iRow;
          this.iMillCode= iMillCode;
          this.iUserCode= iUserCode;
          this.SItemTable=SItemTable;



          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          try
          {
               ORAConnection jdbc   = ORAConnection.getORAConnection();
               theConnection        = jdbc.getConnection();


          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
          activate();
     }
          
     private void createComponents()
     {

          VItemCode  = new Vector();
          VItemName  = new Vector();
          VItemStock     = new Vector();
          VItemStkGrpCode= new Vector();

          TopPanel   = new JPanel();
          thePanel   = new JPanel(true);
          BrowList   = new JList();
          SelectedList= new JList();

          MidLeft    = new JPanel();
          MidRight   = new JPanel();

          MiddlePanel= new JPanel();

          BOk            = new JButton("Selection Over");

          TStartText  = new JTextField(30);
          TMiddleText = new JTextField(30);
          TEndText    = new JTextField(30);

          dialog     = new JDialog(new Frame(),STitle,true);
     }
               
     private void setLayouts()
     {
          MidLeft .setLayout(new  BorderLayout());
          MidRight.setLayout(new  BorderLayout());


          MiddlePanel . setLayout(new GridLayout(1,2));
          TopPanel    . setLayout(new GridLayout(2,6));
          thePanel    . setLayout(new  BorderLayout());
          BrowList    . setFont(new Font("monospaced", Font.PLAIN, 11));
          SelectedList. setFont(new Font("monospaced", Font.PLAIN, 11));
     }

     private void addComponents()
     {
          TopPanel.add(new JLabel(" Starting Text "));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(" Middle Text "));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(" End Text "));
          TopPanel.add(new JLabel(""));


          TopPanel.add(TStartText);
          TopPanel.add(new JLabel(""));
          TopPanel.add(TMiddleText);
          TopPanel.add(new JLabel(""));
          TopPanel.add(TEndText);
          TopPanel.add(new JLabel(""));


          thePanel.add("Center",new JScrollPane(BrowList));

          MidLeft.add("North",TopPanel);
          MidLeft.add("Center",thePanel);


          MidRight.add("Center",new JScrollPane(SelectedList));
          MidRight.add("South",BOk);

          MiddlePanel.add(MidLeft);
          MiddlePanel.add(MidRight);

          BOk.setEnabled(true);

     }

     private void addListeners()
     {
          TStartText.addKeyListener(new KeyList());
          TMiddleText.addKeyListener(new KeyList1());
          TEndText.addKeyListener(new KeyList2());

          BrowList.addKeyListener(new KeyList3());
          BOk.addActionListener(new ActList());

     }


     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent e)
          {
               if(VItemCode.size()==0)
               {
                    JOptionPane.showMessageDialog(null,"No Material is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                    BrowList.requestFocus();
                    return;
               }
               for(int i=0; i<VItemCode.size(); i++)
               {
                    Vector theVect = new Vector();

                    theVect.addElement(((String)VItemCode.elementAt(i)).trim());
                    theVect.addElement((String)VItemName.elementAt(i));
                    theVect.addElement((String)VItemStkGrpCode.elementAt(i));
                    theVect.addElement("");
                    theVect.addElement("");
                    theVect.addElement((String)VItemStock.elementAt(i));
                    theVect.addElement("");
                    theVect.addElement("");

                    theModel.appendRow(theVect);

               }
               BOk.setEnabled(false);
               dialog.setVisible(false);
               str="";
          }
     }

     public void activate()
     {
          dialog.getContentPane().add("Center",MiddlePanel);
          dialog.setBounds(50,50,850,500);
          dialog.setVisible(true);

          BrowList.requestFocus();
     }
     
     public void deactivate()
     {
          dialog.setVisible(false);
     }

     private class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setItemList(0);
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setItemList(0);
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         str="";
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DOWN)
                    {
                         BrowList.requestFocus();
                         BrowList.setSelectedIndex(1);
                    }


               }
               catch(Exception ex)
               {
               }
          }

     }

     private class KeyList1 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setItemList(1);
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setItemList(1);
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         str="";
                    }

               }
               catch(Exception ex)
               {
               }
          }


     }
     private class KeyList2 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setItemList(2);
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setItemList(2);
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         str="";
                    }
               }
               catch(Exception ex)
               {
               }
          }

     }

     private class KeyList3 extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    setDetails(BrowList.getSelectedIndex());
               }
               if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
               {
                    setDetails(-1);
               }
          }
     }
     private void setItemList(int iStatus)
     {
          VCode = new Vector();
          VName = new Vector();
          VStkGroupCode= new Vector();
          VStock       = new Vector();
          VShowName= new Vector();

          try
          {
               String QS="";

               if(!str.equals(""))
               {
                    if(iStatus==0)
                    {

                        QS ="  Select rpad(Item_Code,10),rpad(Item_Name,50),decode(ItemStock.Stock,null,0,ItemStock.Stock)-decode(ItemStock.IndentQty,null,0,ItemStock.IndentQty),StkGroupCode,rpad(Catl,20),rpad(Draw,20) from InvItems"+
                            "  Left join ItemStock on ItemStock.ItemCode=InvItems.Item_Code"+
                            "  and ItemStock.MillCode="+iMillCode+" and (ItemStock.HodCode in(select AuthUserCode from MrsUserAuthentication where UserCode="+iUserCode+")"+
                            "  )"+ // or ItemStock.HodCode=1
                            "  where Item_Name like  '"+str+"%'"+
                            "  and Stock>0";
                    }
                    if(iStatus==1)
                    {
                        QS ="  Select rpad(Item_Code,10),rpad(Item_Name,50),decode(ItemStock.Stock,null,0,ItemStock.Stock)-decode(ItemStock.IndentQty,null,0,ItemStock.IndentQty),StkGroupCode,rpad(Catl,20),rpad(Draw,20) from InvItems"+
                            "  Left join ItemStock on ItemStock.ItemCode=InvItems.Item_Code"+
                            "  and ItemStock.MillCode="+iMillCode+" and (ItemStock.HodCode in(select AuthUserCode from MrsUserAuthentication where UserCode="+iUserCode+")"+
                            "  )"+  //or ItemStock.HodCode=1
                            "  where Item_Name like  '%"+str+"%'"+
                            "  and Stock>0";
                    }
                    if(iStatus==2)
                    {
                        QS ="  Select rpad(Item_Code,10),rpad(Item_Name,50),decode(ItemStock.Stock,null,0,ItemStock.Stock)-decode(ItemStock.IndentQty,null,0,ItemStock.IndentQty),StkGroupCode,rpad(Catl,20),rpad(Draw,20) from InvItems"+
                            "  Left join ItemStock on ItemStock.ItemCode=InvItems.Item_Code"+                                                                                        
                            "  and ItemStock.MillCode="+iMillCode+" and (ItemStock.HodCode in(select AuthUserCode from MrsUserAuthentication where UserCode="+iUserCode+")"+
                            "  )"+  //or ItemStock.HodCode=1
                            "  where Item_Name like  '%"+str+"'"+
                            "  and Stock>0";
                    }

     
                    PreparedStatement thePrepare = theConnection.prepareStatement(QS);
     
                    ResultSet theResult = thePrepare.executeQuery();
                    while(theResult.next())
                    {

                         String SItemCode= theResult.getString(1);
                         String SItemName= theResult.getString(2);
                         String SStock   = theResult.getString(3);
                         String SCatl    = theResult.getString(4);
                         String SDraw    = theResult.getString(5);

                         String SItemWithStock =SItemCode+"-"+SItemName+" - "+SStock+"  (Catl- "+SCatl+") (Draw- "+SDraw+")";


                         VCode.addElement(SItemCode);
                         VName.addElement(SItemWithStock);
                         VShowName.addElement(SItemName);
                         VStock.addElement(SStock);
                         VStkGroupCode.addElement(theResult.getString(4));
                    }
                    theResult.close();
                    thePrepare.close();
               }
               BrowList.setListData(VName);
               BrowList.setSelectedIndex(0);

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

     }
     private void setDetails(int index)
     {
         
         if(index > -1)
         {
              String SItemCode = (String)VCode.elementAt(index);
              String SItemName = (String)VName.elementAt(index);
              String SItemShowName=(String)VShowName.elementAt(index);
              String SStkGrpCd= (String)VStkGroupCode.elementAt(index);
              String SStock    = (String)VStock.elementAt(index);

              if(isExist(SItemCode))
              {
                   JOptionPane.showMessageDialog(null," Item Already Selected for Indent");
                   return;
              }
              if(common.toDouble(SStock)<=0)
              {
                    JOptionPane.showMessageDialog(null," Stock is Zero");
                   return;
              }
              int iIndex=VItemCode.indexOf(SItemCode);

              if (iIndex==-1)
              {
    
                   VItemCode.addElement(SItemCode);
                   VItemName.addElement(SItemShowName);
                   VItemStock.addElement(SStock);
                   VItemStkGrpCode.addElement(SStkGrpCd);
              }
              else
              {
                   VItemCode        . removeElementAt(iIndex);
                   VItemName        . removeElementAt(iIndex);
                   VItemStock       . removeElementAt(iIndex);
                   VItemStkGrpCode  . removeElementAt(iIndex);
              }
              SelectedList.setListData(VItemName);
          }
     }
     private boolean isExist(String SItemCode)
     {
          boolean bFlag = false;
          for(int i=0; i<theModel.getRows(); i++)
          {
               if(((String)theModel.getValueAt(i,0)).equals(SItemCode))
               {
                    bFlag = true;
               }
          }
          return bFlag;
     }
}
