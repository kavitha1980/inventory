package Indent.UserIndent;

import java.awt.*;
import java.awt.event.*;

import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import util.*;
import javax.swing.border.*;
import jdbc.*;
import java.sql.*;
import java.net.*;

import guiutil.*;

public class UserIndentFrame extends JInternalFrame
{

     JPanel       TopPanel,MiddlePanel,BottomPanel;
     JLayeredPane theLayer;

     MyComboBox   CDept,CUnit,CCata;
     JButton      BSave;

     JTable       theTable;

     IndentModel  theModel;

     int iUserCode,iMillCode;
     Common    common = new Common();
     String SItemTable;

     Vector    VUnitName ,VUnitCode,VDeptName,
          VDeptCode    ,          VGroupName   ,
          VGroupCode   ,          VBlockName   ,
          VBlockCode   ,          VNatureName  ,    VNatureCode  ,
          VCata        ,          VCataCode;

     public UserIndentFrame(JLayeredPane theLayer,int iUserCode,int iMillCode,String SItemTable)
     {
          this.theLayer  = theLayer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SItemTable= SItemTable;

          try
          {
               setVector();
               createComponents();
               setLayouts();
               addComponents();
               addListeners();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void createComponents()
     {
          TopPanel       = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();
     
          CDept          = new MyComboBox(VDeptName);
          CUnit          = new MyComboBox(VUnitName);
          CCata          = new MyComboBox(VCata);

          theModel      = new IndentModel();
          theTable      = new JTable(theModel);

          BSave         = new JButton("Save");

          setColumn();
     }

     public void setLayouts()
     {
          TopPanel  . setLayout(new GridLayout(2,6));
          MiddlePanel.setLayout(new BorderLayout());


          MiddlePanel.setBorder(new TitledBorder(" Materia List"));

          setTitle(" Material Indent ");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,600,475);
          theModel.setNumRows(0);
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel(" Indent No "));
          TopPanel.add(new MyLabel("  UnDetermined "));
          TopPanel.add(new JLabel(" "));


          TopPanel.add(new JLabel(" Indent Date "));
          TopPanel.add(new MyLabel(""+common.parseDate(common.getServerDate().substring(0,8))));
          TopPanel.add(new JLabel(" "));


          TopPanel.add(new JLabel(" Unit "));
          TopPanel.add(CUnit);
          TopPanel.add(new JLabel(" "));

          TopPanel.add(new JLabel(" "));
          TopPanel.add(new JLabel(" "));
          TopPanel.add(new JLabel(" "));


          MiddlePanel.add(new JScrollPane(theTable));

          BottomPanel    . add(BSave);
     
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

     }

     private void setColumn()
     {
          TableColumn    CataColumn     = theTable.getColumn("Classification");
                         CataColumn     . setCellEditor(new DefaultCellEditor(CCata));
          TableColumn    DeptColumn     = theTable.getColumn("Department");
                         DeptColumn     . setCellEditor(new DefaultCellEditor(CDept));

          TableColumn    ItemName       = theTable.getColumn("ItemName");

          theTable.getTableHeader().addMouseListener(new MouseList());

     }
     private class MouseList extends MouseAdapter
     {
          public void mouseClicked(MouseEvent me)
          {
               if(theModel.getRows()==0)
               {
                    ItemsList itemlist = new ItemsList(theModel,0,iMillCode,iUserCode,SItemTable);
               }
          }
     }

     public void addListeners()
     {
          BSave.addActionListener(new ActList());
          theTable.addKeyListener(new KeyList());

     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(checkValidData())
               {
                    saveData();
               }
               
          }
     }
     private boolean checkValidData()
     {
          if(theModel.getRows()==0)
          {
               JOptionPane.showMessageDialog(null,"No Item to Save");
               return false;
          }
          for(int i=0; i<theModel.getRows(); i++)
          {
               System.out.println(common.toDouble((String)theModel.getValueAt(i,5))+","+common.toDouble((String)theModel.getValueAt(i,6)));

               if(common.toDouble((String)theModel.getValueAt(i,5))<=0 || common.toDouble((String)theModel.getValueAt(i,6))<=0)
               {
                    JOptionPane.showMessageDialog(null," Invalid Stock or  Invaild ReqQty");
                    return false;
               }
          }
          return true;
     }

     private void saveData()
     {
          int iIndentNo=0;
          Connection theConnection=null;

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
               
               theConnection.setAutoCommit(false);

               String QS = " Select IndentNo from IndentNoConfig for update of indentNo nowait";

               String SInsQS=" Insert into Indent(IndentNo,IndentDate,DeptCode,Unit_Code,Code,Qty,MillCode,UserCode,CreationDate,EntrySystemName,Group_Code,Status)"+
                                           "values(?,?,?,?,?,?,?,?,?,?,?,?)";

               PreparedStatement thePrepare    = theConnection.prepareStatement(QS);

               ResultSet theResult             = thePrepare.executeQuery();
               if(theResult.next())
                  iIndentNo = theResult.getInt(1)+1;

               theResult.close();
               thePrepare.close();

               for(int i=0; i<theModel.getRows(); i++)
               {

                    thePrepare =    theConnection.prepareStatement(SInsQS);

                    thePrepare.setInt(1,iIndentNo);
                    thePrepare.setString(2,common.getServerDate().substring(0,8));
                    thePrepare.setInt(3,getDeptCode((String)theModel.getValueAt(i,3)));
                    thePrepare.setInt(4,getUnitCode((String)CUnit.getSelectedItem()));
                    thePrepare.setString(5,(String)theModel.getValueAt(i,0));
                    thePrepare.setDouble(6,common.toDouble((String)theModel.getValueAt(i,6)));
                    thePrepare.setInt(7,iMillCode);
                    thePrepare.setInt(8,iUserCode);
                    thePrepare.setString(9,common.getServerDate());
                    thePrepare.setString(10,InetAddress.getLocalHost().getHostName());
                    thePrepare.setInt(11,getGroupCode((String)theModel.getValueAt(i,4)));
                    thePrepare.setInt(12,1);
                            
                    thePrepare.executeUpdate();
                    thePrepare.close();
               }
               thePrepare          = theConnection.prepareStatement(" Update IndentNoConfig set indentNo=?");
               thePrepare.setInt(1,iIndentNo);
               thePrepare.executeUpdate();
               thePrepare.close();

               theConnection.setAutoCommit(true);
               theConnection.close();

               JOptionPane    . showMessageDialog(null,"Data Save Sucessfully With IndentNo : "+iIndentNo);

               theModel.setNumRows(0);

          }
          catch(Exception ex)
          {

               System.out.println(ex);
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               System.out.println("Indent indentNo: "+ex);
               try
               {
                    theConnection.rollback();
     
                    if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
                    {
                         JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                         try
                         {
                         }catch(Exception Ex)
                         {
                             System.out.println(Ex);
                             Ex.printStackTrace();
                         }
                         saveData();
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null," Problemnt in Storing Data Contact Mr.MohanRaj");

                    }
               }
               catch(Exception e)
               {
               }
          }
     }
     private class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {

               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    int iRow = theTable.getSelectedRow();
                    ItemList itemlist = new ItemList(theModel,iRow,iMillCode,iUserCode,SItemTable);

               }


               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    theModel.appendEmptyRow();
               }
               if(ke.getKeyCode()==KeyEvent.VK_DELETE)
               {
                    int iRow = theTable.getSelectedRow();

                    theModel.deleteRow(iRow);
               }
          }
     }
     public void setVector()
     {
          VUnitName     = new Vector();
          VUnitCode     = new Vector();  
          VDeptName     = new Vector();
          VDeptCode     = new Vector();
          VGroupName    = new Vector();
          VGroupCode    = new Vector();
          VBlockName    = new Vector();
          VBlockCode    = new Vector();
          VNatureName   = new Vector();
          VNatureCode   = new Vector();

          VCata         = new Vector();
          VCataCode     = new Vector();
        
          String QS1="";
          String QS2= "";
          String QS3= "";

          if(iMillCode!=1)
               QS1 = "Select Unit_Name,Unit_Code From Unit where (MillCode=0 OR MillCode = 2) Order By Unit_Name";
          else
               QS1 = "Select Unit_Name,Unit_Code From Unit where (MillCode=1 OR MillCode = 2) Order By Unit_Name";

          if(iMillCode!=1)
               QS2 = "Select Dept_Name,Dept_Code From Dept where (MillCode=0 OR MillCode = 2) Order By Dept_Name";
          else
               QS2 = "Select Dept_Name,Dept_Code From Dept where (MillCode=1 OR MillCode = 2) Order By Dept_Name";

            
          QS3 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";


          try
          {
               Connection theConnection=null;

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS1);

               while(result.next())
               {
                    VUnitName.addElement(result.getString(1));
                    VUnitCode.addElement(result.getString(2));
               }
               result.close();
               result          = theStatement.executeQuery(QS2);
               while(result.next())
               {
                    VDeptName.addElement(result.getString(1));
                    VDeptCode.addElement(result.getString(2));
               }
               result.close();

               result          = theStatement.executeQuery(QS3);
               while(result.next())
               {
                    VCata.addElement(result.getString(1));
                    VCataCode.addElement(result.getString(2));
               }
               result.close();

               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private int getUnitCode(String SUnitName)
     {
          int iIndex=-1;

          iIndex = VUnitName.indexOf(SUnitName);
          if(iIndex!=-1)
               return common.toInt((String)VUnitCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getDeptCode(String SDeptName)
     {
          int iIndex=-1;

          iIndex = VDeptName.indexOf(SDeptName);
          if(iIndex!=-1)
               return common.toInt((String)VDeptCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getGroupCode(String SCataName)
     {
          int iIndex=-1;

          iIndex = VCata.indexOf(SCataName);
          if(iIndex!=-1)
               return common.toInt((String)VCataCode.elementAt(iIndex));
          else
               return 0;
     }

}


