package Finance;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class InvItemSearch extends JDialog
{
      JLayeredPane   Layer;
      JTextField     TSupCode;
      String         SSupTable;
      JButton        BName;
      JButton        BOk;
      Vector         VItemName,VItemCode,VHSNCode;
      JTextField     TIndicator;          
      JList          BrowList;
      JScrollPane    BrowScroll;
      JPanel         BottomPanel;
      String str="";
      ActionEvent ae;

      Common common = new Common();

      Connection theConnection   = null;
      
      ReverseChargePurchaseModel theModel = null;
      int               iRow,iCol,iCode;
      // set Model Index   
      int SNo=0,GateInwItem=1,ItemCode=2,ItemName=3,HSNCode=4,Rate=5,Qty=6,Value=7,CGRatePer=8,CGValue=9,SGRatePer=10,SGValue=11,IGRatePer=12,IGValue=13,TotalValue=14,Select=15;
      InvItemSearch(ReverseChargePurchaseModel theModel,int iRow,int iCol,Vector VItemCode,Vector VItemName,Vector VHSNCode)
      {
          this.Layer       = Layer;
          this.theModel    = theModel;
          this.iRow        = iRow;
          this.iCol        = iCol;
          this.iCode       = iCode;
          
          this.VItemName   = VItemName;
          this.VItemCode   = VItemCode;
          this.VHSNCode    = VHSNCode;

          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator    . setEditable(false);

          BrowList      = new JList(VItemName);

          BrowScroll    = new JScrollPane(BrowList);

          BottomPanel   =   new JPanel(true);
          BottomPanel   .   setLayout(new GridLayout(1,2));
          this          .   setTitle("InvItem Selector");
          this          .   setBounds(250,100,550,350);
          BrowList      .   addKeyListener(new KeyList());
          this          .   getContentPane()    .   setLayout(new BorderLayout());
          this          .   getContentPane()    .   add("South",BottomPanel);
          this          .   getContentPane()    .   add("Center",BrowScroll);
          
          BottomPanel   .   add(TIndicator);
          BottomPanel   .   add(BOk);
       //   BOk           .   addActionListener(new ActList());
          this          .   setModal(true);
          this          .   setSize(700,500);

      }

      public void setPreset()
      {
          int index=0;
          for(index=0;index<VItemCode.size();index++)
          {
               String str1 = (String)VItemCode.elementAt(index);
               String str  = ((String)VItemName.elementAt(index)).toUpperCase();
               if(str1.startsWith(TSupCode.getText()))
               {
                    BrowList.setSelectedValue(str,true);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
          BrowList.requestFocus();
          BrowList.updateUI();
      }

    /*  public class ActList implements ActionListener
      {
          public void actionPerformed(ActionEvent e)
          {
               int index = BrowList.getSelectedIndex();
               setItemDetails(index);
               str="";
               removeHelpFrame();
          }
      }
*/

      public class KeyList extends KeyAdapter
      {
             public void keyReleased(KeyEvent ke)
             {
                  char lastchar=ke.getKeyChar();
                  lastchar=Character.toUpperCase(lastchar);
                  try
                  {
                     if(ke.getKeyCode()==8)
                     {
                        str=str.substring(0,(str.length()-1));
                        setCursor();
                     }
                     else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar>='0' && lastchar <= '9'))
                     {
                        str=str+lastchar;
                        setCursor();
                     }
                  }
                  catch(Exception ex){}
             }
             public void keyPressed(KeyEvent ke)
             {
                  if(ke.getKeyCode()==116)    // F5 is pressed
                  {
                         BrowList.setListData(VItemName);
                  }
                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                     int index = BrowList.getSelectedIndex();
                     setItemDetails(index);
                     str="";
                     removeHelpFrame();
                  }
             }
         }
         public void setCursor()
         {
            TIndicator.setText(str);
            int index=0;
            for(index=0;index<VItemName.size();index++)
            {
                 String str1 = ((String)VItemName.elementAt(index)).toUpperCase();
                 if(str1.startsWith(str))
                 {
                      BrowList.setSelectedValue(str1,true);
                      BrowList.ensureIndexIsVisible(index+10);
                      break;
                 }
            }
         }
         public void removeHelpFrame()
         {
            try
            {
               dispose();
            }
            catch(Exception ex) { }
         }
         public boolean setItemDetails(int index)
         {
               theModel .   setValueAt(String.valueOf(VHSNCode.elementAt(index)),iRow,HSNCode);
               theModel .   setValueAt(String.valueOf(VItemCode.elementAt(index)),iRow,ItemCode);
               theModel .   setValueAt(String.valueOf(VItemName.elementAt(index)),iRow,ItemName);
               return true;
         } 
}
