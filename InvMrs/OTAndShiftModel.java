import java.awt.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import util.*;

public class OTAndShiftModel extends DefaultTableModel
{

     String    ColumnName[]   = {"HRD Ticket No", "Worker Name", "Department","Shift", "In Date", "In Time", "Out Date", "Out Time", " Hrs"};
     String    ColumnType[]   = {"S"            , "S"          , "S"         ,"S"  , "S"      , "S"      , "S"       , "S"        ,"S"   };
     int       iLength[]      = {70             , 120          ,100          ,50   , 50       , 20       , 50        ,  20        ,10    };

     Common    common         = new Common();

     Vector    vect = new Vector();
     int       iStatus, iUserCode;

     public OTAndShiftModel(Vector vect,int iStatus, int iUserCode)
     {
            this.vect         = vect;
            this.iStatus      = iStatus;
            this.iUserCode    = iUserCode;

            setDataVector(getRowData(),ColumnName);
     }

     public OTAndShiftModel()
     {

            setDataVector(getRowData(),ColumnName);
     }

     public Object[][] getRowData()
     {
          int iSNo            = 0;

          Object RowData[][]  = new Object[vect.size()][ColumnName.length];

          for(int i=0;i<vect.size();i++)
          {
               OTWorker worker  = (OTWorker)vect.elementAt(i);

               RowData[i][0]  = worker.SHRDCode;
               RowData[i][1]  = worker.SEmpName;
               RowData[i][2]  = worker.SDeptName;
               RowData[i][3]  = String.valueOf(worker.iShift);
               RowData[i][4]  = common.parseDate(worker.SInDate);
               RowData[i][5]  = worker.SInTime;
               RowData[i][6]  = common.parseDate(worker.SOutDate);
               RowData[i][7]  = worker.SOutTime;
               RowData[i][8]  = worker.SWorkedHrs;
          }
          return RowData;
     }

     public boolean isCellEditable(int row,int col)
     {
          if(iUserCode == 4785 || iUserCode == 2003)
          {
               if(ColumnType[col] == "E" || ColumnType[col] == "B")
               {
                    return true;
               }

               return false;
          }
          else
          {
               if(ColumnType[col] == "B")
               {
                    return true;
               }
               else if(ColumnType[col]=="E")
               {
                    if(iStatus == 0)
                    {
                         String STime      = common.parseNull(getValueAt(row,col).toString());
     
                         if(STime.length() == 0)
                         {
                              return true;
                         }
                         else
                         {
                              return false;
                         }
                    }
                    else
                    {
                         return true;
                    }
               }
     
               return false;
          }
     }

     public Class getColumnClass(int col)
     {
          return getValueAt(0,col).getClass();
     }

     public int getRows()
     {
          return super.dataVector.size();
     }

     public void appendRow(OTWorker worker)
     {
          Vector curVector    = new Vector();

          curVector           . addElement(worker.SHRDCode);
          curVector           . addElement(worker.SEmpName);
          curVector           . addElement(worker.SDeptName);
          curVector           . addElement(String.valueOf(worker.iShift));
          curVector           . addElement(common.parseDate(worker.SInDate));
          curVector           . addElement(worker.SInTime);
          curVector           . addElement(common.parseDate(worker.SOutDate));
          curVector           . addElement(worker.SOutTime);
          curVector           . addElement(worker.SWorkedHrs);

          insertRow(getRows(), curVector);
     }

     public void deleteRow(Worker worker)
     {
          for(int i=0;i<(getRows());i++)
          {
               if(worker.SHRDCode.equals((String)getValueAt(i,0)))
               {
                    removeRow(i);
               }
          }
     }
     public boolean isDateAvailable(String SDate)
     {
          boolean bAvail =false;

          for(int i=0;i<(getRows());i++)
          {
               if(SDate.equals((String)getValueAt(i,4)))
               {
                    bAvail = true;
                    break;
               }
          }
          return bAvail;
     }

}
