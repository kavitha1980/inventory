import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class InvPass
{
     Accept AD;

     public InvPass()
     {
          try
          {
               AD.setVisible(false);
          }
          catch(Exception ex){}

          try
          {
               AD = new Accept();
               AD.show();
               AD.BAccept.addActionListener(new ADList());
               AD.BDeny.addActionListener(new ADList());
          }          
          catch(Exception ex)
          {
               System.out.println("991 "+ex);
               ex.printStackTrace();
          }
     }

     public static void main(String arg[])
     {
        new InvPass();
     }

     public class ADList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(AD.BDeny==ae.getSource())
               {
                    AD.setVisible(false);
                    System.exit(0);
               }
               if(ae.getSource()==AD.BAccept)
               {
                    startHRD();
               }
          }
     }
     public void startHRD()
     {
          if(AD.isPassValid())
          {
            AD.setVisible(false);
//            Inventory inv = new Inventory(AD.getHRCode(),AD.getAuthCode(),AD.getMillCode(),AD.getYear(),AD.getStDate(),AD.getEnDate(),AD.getYearCode(),AD.getItemTable(),AD.getSupTable(),AD.getMillName());
			
	  	ItemStockDifferenceFrame itemPendFrame   = new ItemStockDifferenceFrame(AD.getHRCode(),AD.getAuthCode(),AD.getMillCode(),AD.getYear(),AD.getStDate(),AD.getEnDate(),AD.getYearCode(),AD.getItemTable(),AD.getSupTable(),AD.getMillName());
     		
					itemPendFrame.setVisible(true);
			
          }
          else
          {
             JOptionPane.showMessageDialog(null, "Password is Invalid", "Warning", 
             JOptionPane.ERROR_MESSAGE,new ImageIcon("Warning.gif"));
             AD.TA.setText("");
          }
     }
}
