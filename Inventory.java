import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.sql.*;
import java.util.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;
import GST.*;
import vital.*;

import MRS.*;
import MRS.PlanMrs.*;
import enquiry.*;
import Order.*;
import Kardex.*;
import GRN.*;
import Planning.*;
import Issue.*;
import spdlposting.*;
import GreenTapePayment.*;
import GreenTapeAuthentication.*;
import javax.swing.plaf.*;
import Sales.*;
import DyesPriceRevision.*;
import ReverseCharge.*;

public class Inventory
{
     JMenuBar       theMenuBar;
     JFrame         theFrame;
     Container      Container;
     JDesktopPane   DeskTop;
     StatusPanel    SPanel;
     JPanel         MenuPanel;
     JComboBox      JCSupplier;

     Runtime        rt        = null;

     JMenu          VitalMenu,MRSMenu,ENQuiryMenu,OrderMenu,KardexMenu,PlanningMenu,IssueMenu,SpdlPostMenu, DyesMenu, ReverseChargeMenu, VitalMenuGst, SalesMenu;
     JMenu          DyesMenuNew;

     JMenuItem      mDepartment,mGroup,mBookTo,mBookThro,mMRS,mMRSSplit,mMRSList,mMRSListDetails,mMRSListPrint,mMRSPendingList,mMRSMonComp,mMRSClosing,mPlanMRS,mStockGroup,mHod,mMatGroup,mSupplier,mMRSTempList,mMRSMonthPlan,mMRSConvList,mItemDelete,mItemMerge,mProjectList, mRegularPlanViewFrame;
     JMenuItem      mVMaterial,mVMatEntry,mVMatModi,mUom,mMatTransfer,mVSupplier,mMRSItemMatchingFrame,mMRSItemAuthenticateFrame, mMonthlyPlanningItemFrame;
     JMenuItem      mIndustrySector,mIndustrySubSector,mBrand,mTemperature,mSTGroup,mMatRoom,mMatRack,mMatRow,mMatColum,mMatTray;
     JMenuItem	    mMatBox,mRatePeriodical,mQualityPeriodical,mCostingType,mVAbolish;
     JMenuItem      mMRSEnquiry,mDirectEnquiry,mEnquiryList,mEnquiryMail;
     JMenuItem      mMatStock,mMatMRSStock,mMatMRSStockGst,mMatStockGst,mYearlyMRSOrder,mYearlyMRSOrderGst,mRateUpdation,mOrderDeletion,mMRSDeletion,mAdvReqDeletion,mAdvReqGst;
     JMenuItem      mDirectOrder,mOrderList,mOrderListGst,mOrderDetail,mOrderDetailGst,mOrderSplit,mOrderMod,mOrderAbs,mOrderPending,mLastPOComp,mAdvReq,mAdvReqAbs,mOrderMonComp;
     JMenuItem	    mOrderAuth,mAuthPending,mOrderAbsMail,mOrderDetailConv,mYearlyOrderAbs;
     JMenuItem      mtestledger,mKardex,mLedger,mItemSearch,mStockUpdate,mMasterStock,mStockRate,mMelTrans,mEPCGRequestFrame,mEPCGRequestListFrame,mEPCGOrdersListFrame,mRejectionGst;
     JMenuItem      mRegularPlanning,mVMatIso,mVMatQtyAllow,mVMRSPosting,mMonthlyMRSItemRemoval;
     JMenuItem      mMonIssue,mMonAbstract;
     JMenuItem      mSpdlposting;
     JMenuItem      mMonthlyMRS;
     JMenuItem	    mGRNPendingList;
     JMenuItem	    mOthersGRN1, mDirectGRN1;	
     JMenuItem      mOrderPendingProcess;
     JMenuItem      mStoreMaterialUnLoad,mStoreMaterialAuthentication;
     JMenuItem      mPriceRevisionEntry,mPriceRevisionEntry_Old, mDyes_CostingProcess, mShadewiseDyesReport;

     JMenuItem      mPriceRevisionEntryNew, mDyesCostingProcessNew, mShadewiseDyesReportNew, mShadewiseCostingReportNew;
     
     JMenuItem      mReverseChargeFrame;
     JMenuItem      miVMatEntryGst,miVHsnCodeUpdate,miVGstRateUpdate,miVHsnCodeModify,mGSTNo,miMotorHistory;
	 JMenuItem      mSalesFrame,mSalesInvoicePDF,mGICashReceiptCancel;
	
                                                    

     JMenu          GRNMenu;

     JMenuItem      	mNewGRN,mGstGRN, mGRNNew, mDirectGRN, mDirectGRNGst, mOthersGRN,mStockTransPend,mOthersIssue,mIssuedGRN,mInspection,mRejection,mGRNList,mGRNDetail,mGRNAbs,mGRNDelete,mDyeingGRNAbs,mGRNMonComp,mGatePending,mFinPending,mSupplierList,mGRedPendPrin,mGrnRejectionForm,mRejRdcMatch,
mRejGiMatch,mGIComp;
     JMenuItem      mInvNotReceived, mPendingInvReceived,mGreenTapeReceiptDetails,mGreenTapeDailyAuthentication,mGreenTapeAuthentication;
     JMenuItem	    mNewGRN1, MGrnUpdate;

     JWindow        wSplash;

     protected      String ID="";

     Vector         VCode,VName,VNameCode,VCat,VDraw,VUomName;
     Vector 	    VPaperColor,VPaperSets,VPaperSize,VPaperSide,VSlipNo;
     
     JTable         ReportTable;
     boolean        bflag;
     util.Common         common;

     // Possible Look & Feels
     String mac      = "com.sun.java.swing.plaf.mac.MacLookAndFeel";
     String metal    = "javax.swing.plaf.metal.MetalLookAndFeel";
     String motif    = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
     String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

     // The current Look & Feel
     String currentLookAndFeel = metal;

     int       iUserCode,iAuthCode,iMillCode;
     String    SFinYear,SStDate,SEnDate,SYearCode,SItemTable,SSupTable,SMillName;

     MaterialFrame            materialFrame  = null;
     MaterialEntryFrame       materialEntryFrame  = null;
     MaterialModiFrame        materialModiFrame  = null;
     UOMFrame                 uomFrame;

     IndustrySectorFrame mindustrySectorFrame;
     IndustrySubSectorMasterFrame mindustrySubSectorMasterFrame;
     BrandMasterFrame mbrandMasterFrame;
     TemperatureMasterFrame mtemperatureMasterFrame;
     STGroupMasterFrame msTGroupMasterFrame;
     MatRoomMasterFrame mmatRoomMasterFrame;
     MatRackMasterFrame mmatRackMasterFrame;
     MatRowMasterFrame mmatRowMasterFrame;
     MatColumMasterFrame mmatColumMasterFrame;
     MatTrayMasterFrame mmatTrayMasterFrame;
     MatBoxMasterFrame mmatBoxMasterFrame;
     RatePeriodicalMasterFrame mratePerodicalMasterFrame;
     QualityPeriodicalMasterFrame mqualityPeriodicalMasterFrame;
     CostingTypeMasterFrame mcostingTypeMasterFrame;

     MaterialTransferFrame    materialTransferFrame;
     SupplierFrame            supplierFrame;
     MRSPostingFrame          mrspostingframe;

     MRSListFrameTest             mrsListFrame;
	MRS.MRSTempListFrame             mrsTempListFrame;
     ProjectListFrame             projectlistframe;
     MRSConversionPendingListFrame mrsConvListFrame;
	 MonthlyMRSItemRemovalFrame mrsitemremoveframe;

	MRS.MonthPlanMRSFrame    planmrsframe;
     MRSListDetailsFrame      mrsListDetailsFrame;
     MRSListPrint             mrsListPrint;
     MRSPendingList           mrsPendingList;
     MRSCompFrame             mrscompframe;
     MRSPostingAgainstCashGrnFrame mrspostingagainstcashgrnframe;

     EnquiryFrame             enquiryframe;
     DirectEnquiryFrame       directenquiryframe;
     EnquiryListFrame         enquirylistframe;
     EnquiryListForm          enquirylistform;

//     ItemSearchList           itemsearchlist;
     DirectOrderFrame         directorderframe;
     //OrderListFrame           orderlistframe;
     OrderListFrameGst        orderlistframegst;
     OrderDetailsFrame        orderdetailsframe;
     OrderDetailsFrameGst     orderdetailsframegst;
     OrderSplitFrame          ordersplitframe;
     OrderDetailsConvertedFrame        orderdetailsconvertedframe;

     OrderAbsFrame            orderabsframe;
     YearlyOrderAbsFrame      yearlyorderabsframe;
     YearlyOrderAbsFrameGst   yearlyorderabsframegst;
//   OrderAbsMailFrame        orderabsmailframe;
     OrderPendingList         orderPendingList;

     //AdvanceFrame             advanceframe;
     AdvanceGSTFrame          advancegstframe;
     LastPOComparisonFrame    lastpocomparisonframe;
     AdvReqAbsFrame           advreqabsframe;
     

     OrderCompFrame           ordercompframe;

     DepartmentMasterFrame    departmentMasterFrame;
     StockGroupMaster         stockgroupmaster;
     GroupMasterFrame         groupMasterFrame;
     BookToMasterFrame        booktoMasterFrame;
     BookThroMasterFrame      bookthroMasterFrame;
     HodMasterFrame           hodmasterframe;
     MatGroupMaster           matgroupmaster;
     SupplierEntryFrame       supplierentryframe;

     MRSFrame                 mrsFrame;
     MrsSplitFrame            mrsSplitFrame;
     //MatReqFrame              MatReqframe;
     MatReqGstFrame           matReqGstFrame;
     //MatReqMRSFrame           MatReqMRSframe;
     MatReqMRSFrameGst        MatReqMRSframegst;
     //YearlyMrsPendingFrame    YearlyMrsPendingframe;
     YearlyMrsPendingFrameGst YearlyMrsPendingframeGst;
     DeletionListFrame        deletionlistframe;
     AdvReqDeletionFrame      advreqdeletionframe;
     MRSDeletionListFrame     mrsdeletionlistframe;
     OrderRateUpdationFrame   orderrateupdationframe;


     GrnRejectionFormFrame    grnRejectionFormFrame;
     RejectionRDCMatchFrame   rejectionRDCMatchFrame;
     RejectionGIMatchFrame    rejectionGIMatchFrame;
	 
	 TestingLedger testledger;


     KardexFrame              kardexframe;
     LedgerFrame              ledgerframe;
     DeptWiseItemStockUpdationFrame   itemstockupdationframe;
     ItemMasterStockUpdationFrame     itemmasterstockupdationframe;
     ItemStockRateUpdationFrame       itemstockrateupdationframe;
     MelangeStockUpdationFrame        melangestockupdationframe;

     MonAbstractFrame         monAbstractframe;
     MonIssueFrame            monissueframe;

     NewRegularPlanFrame      newRegularPlanFrame;
     PlanMRSFrame             planMRSFrame;
     AuthPendingFrame         authPendingFrame;

     SpdlPostFrame            spdlPostFrame;
     OrderAuthFrame           orderAuthFrame;

     GRejectionPendingPrint   GrejectPendprint;

     GIAllocationPrintFrame   giframe;
     EPCGRequestFrame         epcgRequestFrame;
     EPCGRequestListFrame     epcgRequestListFrame;
     EPCGOrdersListFrame      epcgorderslistframe;
     MRSItemMatchingFrame     mrsitemmatchingframe;
     MRSItemAuthenticateFrame     mrsitemAuthenticateframe;

     MonthlyPlanningItemFrame monthlyplanningitemframe;

     ItemMasterDeletionFrame  itemmasterdeletionframe;
	NewMonthlyMRSNew	      MonthlyMRS;
     ItemMergingFrame         itemmergingframe;
  
     GreenTapeReceiptDetails      greentapeReceiptDetails;      
     GreenTapeDailyAuthentication greentapeDailyAuthentication;
     GreenTapeAuthFrame           greentapeAuthFrame;
     String                   SProjectCount=" 0 ";
	 
	 OrderPendingProcessList  orderPendingprocessList;
	 
    // PriceRevisionEntryFrame		pricerevision ;	
     PriceRevisionEntryFrameNew     priceRevisionEntryFrameNew;

     GstMaterialEntryFrame  gstMaterialEntryFrame =null;
     MaterialHsnCodeUpdation materialHsnCodeUpdation = null;
     MaterialGstRateUpdationFrame materialGstRateUpdationFrame = null;
      MaterialHsnCodeModification materialHsnCodeModification = null;
      GSTNoUpdationFrame			gstnoupdation;
      PriceRevisionCostingProcess		costingProcess;
      ShadeWiseMultipleDyesReport		shadewiseMultipleDyesReport;
      ShadeWiseDyesReport				shadewiseDyesReport;
      ShadeReceipeCostingReport         shadeReceipeCostingReport;

      SalesFrame  salesframe =null;
      SalesInvoicePdfPrint  SalesPdf =null;
	  GICashReceiptCancelFrame	GIcashreceiptcancelframe;
	  
	RegularPlanViewFrame regularplanviewframe;
	
	 MotorHistoryEntryFrame		motorHistoryUpdation ;			

     public Inventory(int iUserCode,int iAuthCode,int iMillCode,String SFinYear,String SStDate,String SEnDate,String SYearCode,String SItemTable,String SSupTable,String SMillName)
     {
          this.iUserCode  = iUserCode;
          this.iAuthCode  = iAuthCode;
          this.iMillCode  = iMillCode;
          this.SFinYear   = SFinYear;
          this.SStDate    = SStDate;
          this.SEnDate    = SEnDate;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;

          rt = Runtime.getRuntime();


          updateLookAndFeel();
          setProjectPendingCount();
          CreateComponents();
          setLayouts();
          AddComponents();
          AddListeners();
          theFrame.setVisible(true);
          setSplashScreen();
          setDataIntoVector();
     }

     private void setSplashScreen()
     {
          wSplash = new JWindow();
          wSplash.getContentPane().add("Center",new JLabel(new ImageIcon("j://inventorynew/inv1.gif")));
          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          double x             = screenSize.getWidth();
          double y             = screenSize.getHeight();
          x=x/2;
          y=y/2;
          wSplash.setSize(700,400);
          wSplash.setLocation(new Double(x-350).intValue(),new Double(y-200).intValue());
          wSplash.show();
     }

     public void CreateComponents()
     {
          theFrame            = new JFrame("Inventory Monitoring System - "+SMillName+" - Year - "+SFinYear);
          Container           = theFrame.getContentPane();
          DeskTop             = new JDesktopPane();
          
          SPanel              = new StatusPanel();
          MenuPanel           = new JPanel();
          theMenuBar          = new JMenuBar();

          common              = new util.Common();

          VCode               = new Vector();
          VName               = new Vector();
          VNameCode           = new Vector();
          VCat                = new Vector();
          VDraw               = new Vector();
          VUomName            = new Vector();

          VPaperColor=new Vector();
          VPaperSets =new Vector();
          VPaperSize =new Vector();
          VPaperSide =new Vector();
          VSlipNo    =new Vector();


          if(iMillCode==0)
          {
               Container . setBackground(new Color(250,200,200));
               theFrame  . setBackground(new Color(250,200,200));
               DeskTop   . setBackground(new Color(250,200,200));
               theMenuBar. setBackground(new Color(250,200,200));
               MenuPanel . setBackground(new Color(250,200,200));
          }
          if(iMillCode==1)
          {
               Container . setBackground(new Color(220,250,250));
               theFrame  . setBackground(new Color(220,250,250));
               DeskTop   . setBackground(new Color(220,250,250));
               theMenuBar. setBackground(new Color(220,250,250));
               MenuPanel . setBackground(new Color(220,250,250));
          }
          if(iMillCode==3)
          {
               Container . setBackground(new Color(200,250,200));
               theFrame  . setBackground(new Color(200,250,200));
               DeskTop   . setBackground(new Color(200,250,200));
               theMenuBar. setBackground(new Color(200,250,200));
               MenuPanel . setBackground(new Color(200,250,200));
          }
          if(iMillCode==4)
          {
               Container . setBackground(new Color(200,200,250));
               theFrame  . setBackground(new Color(200,200,250));
               DeskTop   . setBackground(new Color(200,200,250));
               theMenuBar. setBackground(new Color(200,200,250));
               MenuPanel . setBackground(new Color(200,200,250));
          }

     }

     public void setLayouts()
     {
          Dimension screenSize     = Toolkit.getDefaultToolkit().getScreenSize();
                    theFrame       . setSize(screenSize);
                    theFrame       . setExtendedState(JFrame.MAXIMIZED_BOTH);

                    MenuPanel      . setLayout(new BorderLayout());          
     }

     public void AddComponents()
     {
          Container.add("North",MenuPanel);
          Container.add("Center",DeskTop);
          Container.add("South",SPanel);


          if(iAuthCode>0)
          {
               theMenuBar     .add(VitalMenu        = new JMenu("Vital"));
               //theMenuBar     .add(PlanningMenu     = new JMenu("Planning"));
               theMenuBar.add(VitalMenuGst        = new JMenu("Vital Gst"));
          }
          theMenuBar     .add(MRSMenu          = new JMenu("MRS"));
          if(iAuthCode>0)
          {
               theMenuBar     .add(ENQuiryMenu      = new JMenu("ENQUIRY"));
          }

          theMenuBar     . add(OrderMenu        = new JMenu("Order"));

          if(iAuthCode>0 || iUserCode==8)
          {
               theMenuBar     .add(GRNMenu          = new JMenu("GRN"));
          }

          theMenuBar     .add(KardexMenu       = new JMenu("Kardex"));
          if(iAuthCode>0)
          {
               theMenuBar     .add(IssueMenu        = new JMenu("Issue"));
               theMenuBar     .add(SpdlPostMenu     = new JMenu("Spindle Posting"));
          }
          theMenuBar   	      . add(DyesMenu   	    = new JMenu("Dyes"));
          theMenuBar   	      . add(DyesMenuNew	    = new JMenu("Dyes New"));
          
		  
		  theMenuBar   	      . add(ReverseChargeMenu = new JMenu("GST Cash Purchase"));
		  theMenuBar          . add(SalesMenu       = new JMenu("Sales"));

          mVSupplier          = new JMenuItem("Supplier");
          mVMaterial          = new JMenuItem("Material");
        //  mVMatEntry          = new JMenuItem("Material Entry (New)");  janaki comment 
          mVMatModi           = new JMenuItem("Material Modification");
          mUom                = new JMenuItem("Unit Of Measurements");
          mMatTransfer        = new JMenuItem("Materials Transfer from Mill");

         miVMatEntryGst       = new JMenuItem("Material Entry GST");
	  miVHsnCodeUpdate     = new JMenuItem("Hsn Code Updation");
      miVHsnCodeModify     = new JMenuItem("Hsn Code Modification");  
	  miVGstRateUpdate     = new JMenuItem("GST Rate Updation");
      mGSTNo               = new JMenuItem("Party & GST No Updation"); 
	  miMotorHistory 	   = new JMenuItem("Motor History Updation");

          if(iAuthCode>0)
          {
               VitalMenu      .add(mVSupplier);
               //VitalMenu      .add(mVMaterial);
              // VitalMenu      .add(mVMatEntry);
               VitalMenu      .add(mVMatModi);
               VitalMenu      .add(mUom);
               VitalMenu      .add(mDepartment     = new JMenuItem("Department"));
               VitalMenu      .add(mGroup          = new JMenuItem("Group Or Classification"));
               VitalMenu      .add(mBookTo         = new JMenuItem("Book To"));
               VitalMenu      .add(mBookThro       = new JMenuItem("Book Thro"));
               VitalMenu      .add(mStockGroup     = new JMenuItem("Stock Group"));
               VitalMenu      .add(mHod            = new JMenuItem("HOD"));
               VitalMenu      .add(mMatGroup       = new JMenuItem("Mat Group"));

               VitalMenu          .add(mIndustrySector          = new JMenuItem("Industry Sector "));
               VitalMenu          .add(mIndustrySubSector       = new JMenuItem("Industry Sub Sector "));
               VitalMenu          .add(mBrand                   = new JMenuItem("Brand Names "));
               VitalMenu          .add(mTemperature             = new JMenuItem("Temperature "));
               VitalMenu          .add(mSTGroup                 = new JMenuItem("STGroups "));
               VitalMenu          .add(mMatRoom                 = new JMenuItem("MatRooms "));
               VitalMenu          .add(mMatRack                 = new JMenuItem("MatRack "));
               VitalMenu          .add(mMatRow                  = new JMenuItem("MatRow "));
               VitalMenu          .add(mMatColum                = new JMenuItem("MatColum "));
               VitalMenu          .add(mMatTray                 = new JMenuItem("MatTray "));
               VitalMenu          .add(mMatBox                  = new JMenuItem("MatBox "));
               VitalMenu          .add(mRatePeriodical          = new JMenuItem("Rate Periodical "));
               VitalMenu          .add(mQualityPeriodical       = new JMenuItem("Quality Periodical "));
               VitalMenu          .add(mCostingType             = new JMenuItem("Costing Type "));

	       mVAbolish = new JMenuItem("Material Abolish");

               //VitalMenu    .add(mSupplier       = new JMenuItem("Supplier Creation"));
               VitalMenu      .addSeparator();
               VitalMenu      .add(mMonthlyPlanningItemFrame= new JMenuItem("Monthly Planning Items"));
               if(iMillCode > 0)
               {
                    VitalMenu .add(mMatTransfer);
               }
					VitalMenuGst	.add(miVMatEntryGst);
					VitalMenuGst	.add(miVHsnCodeUpdate);
					VitalMenuGst	.add(miVHsnCodeModify);
					VitalMenuGst    .add(miVGstRateUpdate);
					VitalMenuGst    .add(mGSTNo);
					VitalMenuGst    .add(miMotorHistory);
					
          }

          if(iAuthCode==3 && iMillCode==0)
          {
               //VitalMenu       .add(mItemDelete = new JMenuItem("Item Deletion"));
               VitalMenu       .add(mItemMerge  = new JMenuItem("Item Merging"));
          }

          mPlanMRS        = new JMenuItem("From Planning");
          if(iAuthCode>0)
          {
               MRSMenu        .add(mPlanMRS);
               //MRSMenu        .addSeparator();
               //MRSMenu        .add(mMRS            = new JMenuItem("MRS Frame"));
               MRSMenu        .add(mMRSSplit       = new JMenuItem("MRS Split Frame"));
               MRSMenu        .addSeparator();
          }

          MRSMenu        .add(mMRSList                 = new JMenuItem("MRS ListFrame"));
          MRSMenu        .add(mMRSListDetails          = new JMenuItem("MRS List(Details)"));
          MRSMenu        .add(mMRSListPrint            = new JMenuItem("MRS List(Print)"));
          MRSMenu        .add(mMRSPendingList          = new JMenuItem("MRS Pending List"));

          if(iAuthCode>0)
          {
               MRSMenu        .add(mMRSClosing     = new JMenuItem("MRS Posting Against CashGrn"));
          }
          MRSMenu        .addSeparator();
          MRSMenu        .add(mMRSMonComp     = new JMenuItem("Comparison"));
                          mMRSDeletion    = new JMenuItem("MRS Deletion");
          
		MRSMenu        .addSeparator();
          MRSMenu        .add(mMonthlyMRS          = new JMenuItem("Monthly MRS"));


          MRSMenu        .addSeparator();

          MRSMenu        .add(mMRSItemAuthenticateFrame= new JMenuItem("MRS Items Authentication"));
          MRSMenu        .add(mMRSItemMatchingFrame    = new JMenuItem("MRS Items - Matching"));
          MRSMenu        .addSeparator();

          MRSMenu        .add(mMRSTempList     = new JMenuItem("MRS Authenticaion(For Approval)"));
          MRSMenu        .addSeparator();
          MRSMenu        .add(mMRSConvList     = new JMenuItem("MRS Converted  & Pending List "));
          MRSMenu        .addSeparator();
          MRSMenu        .add(mProjectList     = new JMenuItem("Project Authenticaion(For Approval)"));
	  MRSMenu        .add(mMonthlyMRSItemRemoval     = new JMenuItem("Monthly MRS Item List"));


          if(common.toInt(SProjectCount)>0)
               mProjectList.setForeground(Color.GREEN);
          else
               mProjectList.setForeground(Color.RED);

          if(iAuthCode>0)
          {

          if(getItemMatchingCount()>0)
               mMRSItemAuthenticateFrame.setForeground(Color.GREEN);
          else
               mMRSItemAuthenticateFrame.setForeground(Color.RED);
          }


          MRSMenu        .add(mMRSMonthPlan     		= new JMenuItem("MRS Conversion(from Monthly Planning)"));
          MRSMenu        .add(mRegularPlanViewFrame	= new JMenuItem("Regular Planning View"));
                          

          if(iAuthCode>0)
          {
               ENQuiryMenu    .add(mMRSEnquiry     = new JMenuItem("Enquiry for MRS"));
               ENQuiryMenu    .add(mDirectEnquiry  = new JMenuItem("Direct Enquiry"));
               ENQuiryMenu    .add(mEnquiryList    = new JMenuItem("Enquiry List"));
               ENQuiryMenu    .add(mEnquiryMail    = new JMenuItem("Enquiry Mail"));
               
               //OrderMenu      .add(mMatMRSStock    = new JMenuItem("Order From MRS "));
               OrderMenu      .add(mMatMRSStockGst = new JMenuItem("Order From MRS (GST)"));
               //OrderMenu      .add(mMatStock       = new JMenuItem("Order From MRS Consolidation"));
	       OrderMenu      .add(mMatStockGst    = new JMenuItem("Order From MRS Consolidation (GST)"));
               //OrderMenu      .add(mYearlyMRSOrder = new JMenuItem("Order for Yearly MRS "));
               OrderMenu      .add(mYearlyMRSOrderGst = new JMenuItem("Order for Yearly MRS (GST)"));
               //OrderMenu      .add(mDirectOrder    = new JMenuItem("Direct Order"));
               //OrderMenu      .add(mAdvReq         = new JMenuItem("Request for Advance"));
               OrderMenu      .add(mAdvReqGst      = new JMenuItem("Request for Advance (GST)"));  
          }

          mRateUpdation   = new JMenuItem("Order Rate Updation");
          mOrderDeletion  = new JMenuItem("Order Deletion");
          mAdvReqDeletion = new JMenuItem("AdvRequest Deletion");

          if(iAuthCode>0 || iUserCode==8)
          {
               OrderMenu      .add(mOrderAuth      = new JMenuItem("Order TaxClaim Authentication"));

               OrderMenu      .add(mAuthPending    = new JMenuItem("TaxClaim Authentication Pending"));

               OrderMenu      .addSeparator();
     
               //OrderMenu      .add(mOrderList      = new JMenuItem("Order List"));
               OrderMenu      .add(mOrderListGst   = new JMenuItem("Order List (Gst)"));
               
               //OrderMenu      .add(mOrderDetail    = new JMenuItem("Order List - Detailed"));
               OrderMenu      .add(mOrderDetailGst = new JMenuItem("Order List - Detailed (Gst)"));
               OrderMenu      .add(mOrderSplit     = new JMenuItem("Order Splitting"));

               OrderMenu      .addSeparator();

               OrderMenu      .add(mOrderDetailConv = new JMenuItem(" Order List (Auto Conversion)"));

               OrderMenu      .addSeparator();
          }

		OrderMenu           . add(mOrderPending   = new JMenuItem("Order Pending List"));
		OrderMenu           . add(mOrderPendingProcess   = new JMenuItem("Order Pending Process List"));

          if(iAuthCode>0 || iUserCode==8)
          {
               OrderMenu      .add(mOrderAbs       = new JMenuItem("Order Abstract"));
               OrderMenu      .add(mYearlyOrderAbs = new JMenuItem("Yearly Order Abstract"));

               OrderMenu      .add(mAdvReqAbs      = new JMenuItem("Request for Advance Abstract"));
               OrderMenu      .add(mOrderMonComp   = new JMenuItem("Comparisons"));
               OrderMenu      .add(mLastPOComp     = new JMenuItem("Comparison with Previous PO"));
               //OrderMenu      .add(mOrderAbsMail   = new JMenuItem("Order Abstract With Mail"));
          }

          if(iAuthCode>0)
          {
               OrderMenu      .add(new JSeparator());
              // OrderMenu      .add(mEPCGRequestFrame        = new JMenuItem("EPCG Order Entry"));
              // OrderMenu      .add(mEPCGRequestListFrame    = new JMenuItem("EPCG Order - Authentication"));
               //OrderMenu      .add(new JSeparator());
               //OrderMenu      .add(mEPCGOrdersListFrame     = new JMenuItem("EPCG Orders List"));
          }

          mOrderMod       = new JMenuItem("Order Amendment and Modification Details");
          mVMRSPosting     = new JMenuItem("Pending MRS Posting");
          if (iAuthCode == 3)
          {
               OrderMenu      .add(mOrderMod);
               MRSMenu        .add(mMRSDeletion);
               OrderMenu      .add(mOrderDeletion);
               OrderMenu      .add(mAdvReqDeletion);

               if(iUserCode==16)
               {
                    OrderMenu      .add(mRateUpdation);
               }
          }

          mMonIssue           = new JMenuItem("Monthwise Issues");
          mMonAbstract        = new JMenuItem("Monthwise Abstract");

          if(iAuthCode>0)
          {
               IssueMenu      . add(mMonIssue);
               IssueMenu      . add(mMonAbstract);
          }

          KardexMenu      .add(mKardex     = new JMenuItem("Kardex"));
         // KardexMenu      .add(mLedger     = new JMenuItem("Ledger"));
          KardexMenu      .add(mItemSearch = new JMenuItem("Item Search"));
		  KardexMenu      .add(mtestledger     = new JMenuItem("Ledger"));

		//  if((iUserCode==17)|| (iUserCode==2))
         // {
			KardexMenu      .add(mStoreMaterialUnLoad         = new JMenuItem("StoreMaterial UnLoad"));
			KardexMenu      .add(mStoreMaterialAuthentication = new JMenuItem("StoreMaterial Authentication "));
			DyesMenu        .add(mPriceRevisionEntry_Old	  = new JMenuItem(" Price Revision Entry Frame_Before GST"));
			DyesMenu        .add(mPriceRevisionEntry	  = new JMenuItem(" Price Revision Entry Frame"));
//			DyesMenu        .add(mPriceRevisionEntry	  = new JMenuItem(" Price Revision Entry Frame"));
			DyesMenu        .add(mDyes_CostingProcess	  = new JMenuItem(" Price Revision Costing Process"));
			DyesMenu        .add(mShadewiseDyesReport	  = new JMenuItem(" Shadewise Dyes Report (Consolidate)"));
			
			
			DyesMenuNew     .add(mPriceRevisionEntryNew	  = new JMenuItem(" Price Revision Entry Frame "));
			DyesMenuNew     .add(mDyesCostingProcessNew	  = new JMenuItem(" Price Revision Costing Process "));
			DyesMenuNew     .add(mShadewiseDyesReportNew  = new JMenuItem(" Shadewise Dyes Report (Consolidate) "));
			DyesMenuNew     .add(mShadewiseCostingReportNew  = new JMenuItem(" Shadewise Receipe Costing Report "));
			
			

			ReverseChargeMenu.add(mReverseChargeFrame	  = new JMenuItem(" GST Cash Purchase Frame"));
			if(iUserCode==4 || iUserCode==16)
			ReverseChargeMenu       .add(mGICashReceiptCancel		= new JMenuItem("GI Cash Receipt Cancellation"));
			
		    SalesMenu       .add(mSalesFrame          	  = new JMenuItem(" SalesFrame"));
			SalesMenu       .add(mSalesInvoicePDF     	      = new JMenuItem("Sales Invoice PDF"));
			
 


		 // }
		  
          if(iUserCode==16)
          {
               KardexMenu      .add(mStockUpdate = new JMenuItem("Stock Updation"));
               KardexMenu      .add(mMasterStock = new JMenuItem("Master Stock Updation"));
               KardexMenu      .add(mStockRate   = new JMenuItem("Stock Rate Updation"));

               if(iMillCode==3)
               {
                    KardexMenu      .add(mMelTrans = new JMenuItem("Stock Updation to Mill"));
               }
          }

          mSpdlposting     = new JMenuItem("Spindle Posting");
          
          //mNewGRN             = new JMenuItem("GRN Accounting");
		  
		  mGRNNew             = new JMenuItem("GRN Accounting (Under Testing) ");	
		  
          mGstGRN             = new JMenuItem("GRN Accounting (GST)");
        //  mNewGRN1            = new JMenuItem("GRN Accounting - Test Entry");
          //mDirectGRN          = new JMenuItem("Gate Inward Accounting");
          mDirectGRNGst       = new JMenuItem("Gate Inward Accounting (GST)"); 
          //mDirectGRN1         = new JMenuItem("Gate Inward Accounting - Test Entry");
          mOthersGRN          = new JMenuItem("GI Accounting (Free,Trial & Others)");
//          mOthersGRN1         = new JMenuItem("GI Accounting (Free,Trial & Others - Test Entry)");
          mStockTransPend     = new JMenuItem("Stock Transfer Pending for GRN");
          mInspection         = new JMenuItem("Material Inspection");
          //mRejection          = new JMenuItem("Material Rejection");
          mRejectionGst       = new JMenuItem("Material Rejection (GST)");
          mOthersIssue        = new JMenuItem("SubStore Issue (Free,Trial & Cash)");
          mIssuedGRN          = new JMenuItem("GRN - Issued Materials");
          mGRNList            = new JMenuItem("GRN List");
		  MGrnUpdate          = new JMenuItem("GRN Updation");
          mGRNDetail          = new JMenuItem("GRN List - Detailed");
          mGRNAbs             = new JMenuItem("GRN Abstract");
          mGrnRejectionForm   = new JMenuItem("Grn Rejection Abstract");
          mRejRdcMatch        = new JMenuItem("Grn Rejection RDC Matrix");
          mRejGiMatch         = new JMenuItem("DirectRejection RDC Matrix");
          mGRedPendPrin       = new JMenuItem("DirectRejection Pending");
          mGIComp             = new JMenuItem("DirectRejection Completed");
          mGRNMonComp         = new JMenuItem("Comparisons");
          mGatePending        = new JMenuItem("Materials Pending @ Gate");
          mFinPending         = new JMenuItem("Pending @ Finpro");
          mSupplierList       = new JMenuItem("Supplierwise GRN");
          mGRNDelete          = new JMenuItem("GRN Deletion");
          mVMatIso            = new JMenuItem("Material for ISO Rating");
          mVMatQtyAllow       = new JMenuItem("Material for Qty Allowance");
          mInvNotReceived     = new JMenuItem("Invoice Not Received");
          mPendingInvReceived = new JMenuItem("Pending Invoice Received");
          mGRNPendingList     = new JMenuItem("GRN Authentication Pending List");

          mGreenTapeReceiptDetails      = new JMenuItem(" Green Tape Receipts");
          mGreenTapeDailyAuthentication = new JMenuItem(" Green Tape Daily Authentication");
          mGreenTapeAuthentication      = new JMenuItem(" Green Tape Weekly Authentication");
	  


          if(iAuthCode>0)
          {
               //GRNMenu   .add(mNewGRN);
			//GRNMenu   .add(mGRNNew);		//Commented on 20.11.2020  
               //GRNMenu   .add(mGstGRN);		//Commented on  10.04.2019
               //GRNMenu   .add(mDirectGRN);
               GRNMenu   .add(mDirectGRNGst);
               GRNMenu   .add(mOthersGRN);
               GRNMenu   .add(mStockTransPend);

               if(iMillCode==1)
               {
                    GRNMenu   .add(mOthersIssue);
                    GRNMenu   .add(mIssuedGRN);
		    //DyesMenu  . add(mPriceRevisionEntry);
               }

               GRNMenu   .add(mInspection);
               //GRNMenu   .add(mRejection);
               GRNMenu   .add(mRejectionGst);
               GRNMenu   .addSeparator();
               GRNMenu   .add(mGRNList);
               GRNMenu   .addSeparator();
               GRNMenu   .add(mInvNotReceived);
               GRNMenu   .add(mPendingInvReceived);
               GRNMenu   .addSeparator();
            //   DyesMenu  . add(mPriceRevisionEntry);
          }


          if(iAuthCode>0 || iUserCode==8)
          {
               GRNMenu   .add(mGRNDetail);
               GRNMenu   .add(mGRNAbs);
               GRNMenu   .add(mGrnRejectionForm);
               GRNMenu   .add(mRejRdcMatch);
               
               GRNMenu   .add(mRejGiMatch);
               GRNMenu   .add(mGRedPendPrin);
               GRNMenu   .add(mGIComp);
               GRNMenu   .add(mGRNMonComp);
               GRNMenu   .add(mGatePending);
               GRNMenu   .add(mFinPending);
               GRNMenu   .add(mSupplierList);
               GRNMenu   .addSeparator();
               GRNMenu   .add(mGRNPendingList);
        //       GRNMenu   .add(mDirectGRN1);
//               GRNMenu   .add(mNewGRN1);
   //            GRNMenu   .add(mOthersGRN1);

          
		  }
		  
		  if(iUserCode==16 || iUserCode==4){
			 GRNMenu.add(MGrnUpdate);
		  }
		  
          if(iAuthCode >2)
          {
             GRNMenu   .add(mGRNDelete);
             VitalMenu .add(mVMatIso);
             VitalMenu .add(mVMatQtyAllow);
             VitalMenu .add(mVMRSPosting);
			 VitalMenu .add(mVAbolish);
          }
          
          MRSMenu        . setMnemonic('M');

          if(iAuthCode>0 || iUserCode==8)
          {
               OrderMenu      . setMnemonic('O');
               GRNMenu        . setMnemonic('G');
          }

          KardexMenu     . setMnemonic('K');
          if(iAuthCode>0)
          {
               VitalMenu      . setMnemonic('V');
               //PlanningMenu   . setMnemonic('P');
               ENQuiryMenu    . setMnemonic('E');
               IssueMenu      . setMnemonic('U');
               SpdlPostMenu   . setMnemonic('S');

               //mMRS           . setMnemonic('M');
               mDepartment    . setMnemonic('D');
               mBookTo        . setMnemonic('B');
               mBookThro      . setMnemonic('T');
               //mMatStock      . setMnemonic('C');
          }

          
          if(iAuthCode>0)
          {
               IssueMenu       . add(mGreenTapeReceiptDetails);
               IssueMenu       . add(mGreenTapeDailyAuthentication);
               IssueMenu       . add(mGreenTapeAuthentication);
          }
          MenuPanel .add("West",theMenuBar);

          if(iAuthCode>0)
          {
               //PlanningMenu      .add(mRegularPlanning = new JMenuItem("Regular Material Planner"));

               if(iMillCode==0)
                    SpdlPostMenu      .add(mSpdlposting);
          }



     }

     public void AddListeners()
     {
          theFrame            . addWindowListener(new WinList());

          mPlanMRS            . addActionListener(new ActList());
		mMRSMonthPlan . addActionListener(new ActList());
		mRegularPlanViewFrame	. addActionListener(new ActList());
		
          mMRSList            . addActionListener(new ActList());
          mMRSListDetails     . addActionListener(new ActList());
          mMRSListPrint       . addActionListener(new ActList());
          mMRSPendingList     . addActionListener(new ActList());
          mMRSMonComp         . addActionListener(new ActList());
          

          if(iAuthCode>0 || iUserCode==8)
          {
               mOrderAuth          . addActionListener(new ActList());
               mAuthPending        . addActionListener(new ActList());

               //mOrderList          . addActionListener(new ActList());
               mOrderListGst       . addActionListener(new ActList());
               //mOrderDetail        . addActionListener(new ActList());
               mOrderDetailGst     . addActionListener(new ActList());
               mOrderSplit         . addActionListener(new ActList());
               mOrderDetailConv    . addActionListener(new ActList());
               mOrderMod           . addActionListener(new ActList());
               mOrderAbs           . addActionListener(new ActList());
               mYearlyOrderAbs     . addActionListener(new ActList());
               //mOrderAbsMail       . addActionListener(new ActList());
               mAdvReqAbs          . addActionListener(new ActList());
          }

          mOrderPending       . addActionListener(new ActList());

	  mOrderPendingProcess       . addActionListener(new ActList());

          if(iAuthCode>0 || iUserCode==8)
          {
               mLastPOComp         . addActionListener(new ActList());
               mOrderMonComp       . addActionListener(new ActList());

               mGRNDetail          . addActionListener(new ActList());
               mGRNAbs             . addActionListener(new ActList());
               mGrnRejectionForm   . addActionListener(new ActList());
               mRejRdcMatch        . addActionListener(new ActList());
               mGRedPendPrin       . addActionListener(new ActList());
               mRejGiMatch         . addActionListener(new ActList());
               mGIComp             . addActionListener(new ActList());
               mGRNMonComp         . addActionListener(new ActList());
               mGatePending        . addActionListener(new ActList());
               mFinPending         . addActionListener(new ActList());
               mSupplierList       . addActionListener(new ActList());
          }
		
         // mRateUpdation       . addActionListener(new ActList());
          mOrderDeletion      . addActionListener(new ActList());
          mAdvReqDeletion     . addActionListener(new ActList());
          mMRSDeletion        . addActionListener(new ActList());
          mMonthlyMRS         . addActionListener(new ActList());

          //mNewGRN             . addActionListener(new ActList());
		  mGRNNew             . addActionListener(new ActList());
          mGstGRN             . addActionListener(new ActList());
//          mNewGRN1            . addActionListener(new ActList());
          //mDirectGRN          . addActionListener(new ActList());
          mDirectGRNGst       . addActionListener(new ActList());
          //mDirectGRN1          . addActionListener(new ActList());
        //  mOthersGRN          . addActionListener(new ActList());
 //         mOthersGRN1         . addActionListener(new ActList());
        //  mStockTransPend     . addActionListener(new ActList());
          mInspection         . addActionListener(new ActList());
          //mRejection          . addActionListener(new ActList());
          mRejectionGst       . addActionListener(new ActList());
          mOthersIssue        . addActionListener(new ActList());
          mIssuedGRN          . addActionListener(new ActList());
          mGRNList            . addActionListener(new ActList());
		  MGrnUpdate          . addActionListener(new ActList());
          mGRNDelete          . addActionListener(new ActList());
          mInvNotReceived     . addActionListener(new ActList());
          mPendingInvReceived . addActionListener(new ActList());
          mGRNPendingList     . addActionListener(new ActList());

          mKardex             . addActionListener(new ActList());
         // mLedger             . addActionListener(new ActList());
          mItemSearch         . addActionListener(new ActList());
		  mtestledger 		  . addActionListener(new ActList());
          if(iUserCode==16)
          {
               //mStockUpdate        . addActionListener(new ActList());
               //mMasterStock        . addActionListener(new ActList());
               //mStockRate          . addActionListener(new ActList());

               if(iMillCode==3)
               {
                    //mMelTrans        . addActionListener(new ActList());
               }
          }

          mMonIssue           . addActionListener(new ActList());
          mMonAbstract        . addActionListener(new ActList());


          mVMatIso            . addActionListener(new ActList());
          mVMatQtyAllow       . addActionListener(new ActList());
          mVMRSPosting        . addActionListener(new ActList());

          mSpdlposting        . addActionListener(new ActList());

          if(iAuthCode>0)
          {
               mVSupplier          . addActionListener(new ActList());
               //mVMaterial          . addActionListener(new ActList());
             //  mVMatEntry          . addActionListener(new ActList());
               mVMatModi           . addActionListener(new ActList());
               mUom                . addActionListener(new ActList());
               mMatTransfer        . addActionListener(new ActList());
               mDepartment         . addActionListener(new ActList());
               mGroup              . addActionListener(new ActList());
               mBookTo             . addActionListener(new ActList());
               mBookThro           . addActionListener(new ActList());
               mStockGroup         . addActionListener(new ActList());
               mHod                . addActionListener(new ActList());
               mMatGroup           . addActionListener(new ActList());

               mIndustrySector        . addActionListener(new ActList());
               mBrand                 . addActionListener(new ActList());
               mTemperature           . addActionListener(new ActList());
               mSTGroup               . addActionListener(new ActList());
               mIndustrySubSector     . addActionListener(new ActList());
               mMatRoom               . addActionListener(new ActList());
               mMatRack               . addActionListener(new ActList());
               mMatRow                . addActionListener(new ActList());
               mMatColum              . addActionListener(new ActList());
               mMatTray               . addActionListener(new ActList());
               mMatBox                . addActionListener(new ActList());
               mRatePeriodical        . addActionListener(new ActList());
               mQualityPeriodical     . addActionListener(new ActList());
               mCostingType           . addActionListener(new ActList());

               mMonthlyPlanningItemFrame. addActionListener(new ActList());

               mMRSSplit           . addActionListener(new ActList());
               
               
               mMRSClosing         . addActionListener(new ActList());
               mMRSEnquiry         . addActionListener(new ActList());
               mDirectEnquiry      . addActionListener(new ActList());
               mEnquiryList        . addActionListener(new ActList());
               mEnquiryMail        . addActionListener(new ActList());

               //mMatStock           . addActionListener(new ActList());
	       mMatStockGst	   . addActionListener(new ActList());	
               //mMatMRSStock        . addActionListener(new ActList());
               mMatMRSStockGst     . addActionListener(new ActList());
               //mYearlyMRSOrder     . addActionListener(new ActList());
               mYearlyMRSOrderGst  . addActionListener(new ActList());
//               mDirectOrder        . addActionListener(new ActList());
               //mAdvReq             . addActionListener(new ActList());
               mAdvReqGst          . addActionListener(new ActList());

             //  mEPCGRequestFrame        . addActionListener(new ActList());
              // mEPCGRequestListFrame    . addActionListener(new ActList());
               //mEPCGOrdersListFrame     . addActionListener(new ActList());

               mVAbolish           . addActionListener(new ActList());

             miVMatEntryGst	   . addActionListener(new ActList());
	         miVHsnCodeUpdate	   . addActionListener(new ActList());
             miVHsnCodeModify   . addActionListener(new ActList());
	         miVGstRateUpdate    . addActionListener(new ActList());
             mGSTNo          .addActionListener(new ActList());
			 miMotorHistory	   . addActionListener(new ActList());
			   
          }

               mMRSItemAuthenticateFrame.addActionListener(new ActList());
               mMRSItemMatchingFrame. addActionListener(new ActList());
               mMRSTempList        . addActionListener(new ActList());
               mProjectList        . addActionListener(new ActList());
			   mMonthlyMRSItemRemoval . addActionListener(new ActList());
               mMRSConvList        . addActionListener(new ActList());

			 //  if((iUserCode==17)|| (iUserCode==2))
			//	{
					mStoreMaterialUnLoad          . addActionListener(new ActList());
					mStoreMaterialAuthentication  . addActionListener(new ActList());
				//}


          if(iAuthCode==3 && iMillCode==0)
          {
               //mItemDelete         . addActionListener(new ActList());
               mItemMerge          . addActionListener(new ActList());
          }

          mGreenTapeReceiptDetails      . addActionListener(new ActList());
          mGreenTapeDailyAuthentication . addActionListener(new ActList());
          mGreenTapeAuthentication      . addActionListener(new ActList());
          
		  mPriceRevisionEntry		. addActionListener(new ActList());
		  mPriceRevisionEntry_Old	. addActionListener(new ActList());
		  mPriceRevisionEntryNew	. addActionListener(new ActList());
		  mDyes_CostingProcess		. addActionListener(new ActList());
		  mDyesCostingProcessNew	. addActionListener(new ActList());
	  	  mReverseChargeFrame		. addActionListener(new ActList());
		  mShadewiseDyesReport		. addActionListener(new ActList());
		  mShadewiseDyesReportNew	. addActionListener(new ActList());
		  mShadewiseCostingReportNew. addActionListener(new ActList());
	  
	  
	      mSalesFrame                   . addActionListener(new ActList());
		  mSalesInvoicePDF 			    . addActionListener(new ActList());
		  if(iUserCode==4 || iUserCode==16)
		  mGICashReceiptCancel 			. addActionListener(new ActList());

     }    

     public class WinList extends WindowAdapter
     {
         public void windowClosing(WindowEvent we)
         {
            System.exit(0);
         }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==mVMaterial)
               {
                    System.runFinalization();
                    System.gc();
                    try
                    {
                         DeskTop.remove(materialFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         materialFrame   = new MaterialFrame(DeskTop,iMillCode,iAuthCode,iUserCode,SItemTable);
                         DeskTop.add(materialFrame);
                         DeskTop.repaint();
                         materialFrame.setSelected(true);
                         DeskTop.updateUI();
                         materialFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
                    System.runFinalization();
                    System.gc();
               }

               if(ae.getSource()==mVMatEntry)
               {
                    System.runFinalization();
                    System.gc();
                    try
                    {
                         DeskTop.remove(materialEntryFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         materialEntryFrame   = new MaterialEntryFrame(DeskTop,iMillCode,iAuthCode,iUserCode,SItemTable);
                         DeskTop.add(materialEntryFrame);
                         DeskTop.repaint();
                         materialEntryFrame.setSelected(true);
                         DeskTop.updateUI();
                         materialEntryFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
                    System.runFinalization();
                    System.gc();
               }

               if(ae.getSource()==mVMatModi)
               {
                    System.runFinalization();
                    System.gc();
                    try
                    {
                         DeskTop.remove(materialModiFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         materialModiFrame   = new MaterialModiFrame(DeskTop,iMillCode,iAuthCode,iUserCode,SItemTable);
                         DeskTop.add(materialModiFrame);
                         DeskTop.repaint();
                         materialModiFrame.setSelected(true);
                         DeskTop.updateUI();
                         materialModiFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
                    System.runFinalization();
                    System.gc();
               }

               if(ae.getSource() == mMRSItemMatchingFrame)
               {
                    try
                    {
                         DeskTop.remove(mrsitemmatchingframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}

                    try
                    {
                         mrsitemmatchingframe   = new MRSItemMatchingFrame(DeskTop,iMillCode,iAuthCode,iUserCode,SItemTable);
                         DeskTop.add(mrsitemmatchingframe);
                         DeskTop.repaint();
                         mrsitemmatchingframe.setSelected(true);
                         DeskTop.updateUI();
                         mrsitemmatchingframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource() == mMRSItemAuthenticateFrame)
               {
                    try
                    {
                         DeskTop.remove(mrsitemAuthenticateframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}

                    try
                    {
                         mrsitemAuthenticateframe   = new MRSItemAuthenticateFrame(DeskTop,iMillCode,iAuthCode,iUserCode,SItemTable);
                         DeskTop.add(mrsitemAuthenticateframe);
                         DeskTop.repaint();
                         mrsitemAuthenticateframe.setSelected(true);
                         DeskTop.updateUI();
                         mrsitemAuthenticateframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource() == mMonthlyPlanningItemFrame)
               {
                    try
                    {
                         DeskTop.remove(monthlyplanningitemframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}

                    try
                    {
                         monthlyplanningitemframe = new MonthlyPlanningItemFrame(DeskTop,iMillCode,iAuthCode,iUserCode,SItemTable);
                         DeskTop.add(monthlyplanningitemframe);
                         DeskTop.repaint();
                         monthlyplanningitemframe.setSelected(true);
                         DeskTop.updateUI();
                         monthlyplanningitemframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mVMRSPosting)
               {
                    try
                    {
                         DeskTop.remove(mrspostingframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}

                    try
                    {
                         mrspostingframe = new MRSPostingFrame(DeskTop,iMillCode,SSupTable);
                         DeskTop.add(mrspostingframe);
                         DeskTop.repaint();
                         mrspostingframe.setSelected(true);
                         DeskTop.updateUI();
                         mrspostingframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
                    //new MrsPost(iMillCode,SSupTable);
               }

               if(ae.getSource()==mVSupplier)
               {
                    try
                    {
                         DeskTop.remove(supplierFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {

                         SupplierFrame supplierFrame   = new SupplierFrame(DeskTop,iAuthCode);
                         DeskTop.add(supplierFrame);
                         DeskTop.repaint();
                         supplierFrame.setSelected(true);
                         DeskTop.updateUI();
                         supplierFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }


               if(ae.getSource()==mUom)
               {
                    try
                    {
                         DeskTop.remove(uomFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         uomFrame   = new UOMFrame(DeskTop,iUserCode,iAuthCode);
                         DeskTop.add(uomFrame);
                         DeskTop.repaint();
                         uomFrame.setSelected(true);
                         DeskTop.updateUI();
                         uomFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMatTransfer)
               {
                    try
                    {
                         DeskTop.remove(materialTransferFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         materialTransferFrame   = new MaterialTransferFrame(DeskTop,iMillCode,iAuthCode,iUserCode,SItemTable);
                         DeskTop.add(materialTransferFrame);
                         DeskTop.repaint();
                         materialTransferFrame.setSelected(true);
                         DeskTop.updateUI();
                         materialTransferFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mDepartment)
               {
                    try
                    {
                         DeskTop.remove(departmentMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         departmentMasterFrame   = new DepartmentMasterFrame(DeskTop,iUserCode);
                         DeskTop.add(departmentMasterFrame);
                         DeskTop.repaint();
                         departmentMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         departmentMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mGroup)
               {
                    try
                    {
                         DeskTop.remove(groupMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         groupMasterFrame   = new GroupMasterFrame(DeskTop,iUserCode);
                         DeskTop.add(groupMasterFrame);
                         DeskTop.repaint();
                         groupMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         groupMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mBookTo)
               {
                    try
                    {
                         DeskTop.remove(booktoMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         booktoMasterFrame   = new BookToMasterFrame(DeskTop,iUserCode);
                         DeskTop.add(booktoMasterFrame);
                         DeskTop.repaint();
                         booktoMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         booktoMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mBookThro)
               {
                    try
                    {
                         DeskTop.remove(bookthroMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         bookthroMasterFrame   = new BookThroMasterFrame(DeskTop,iUserCode);
                         DeskTop.add(bookthroMasterFrame);
                         DeskTop.repaint();
                         bookthroMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         bookthroMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mIndustrySector)
               {
                    try
                    {
                         DeskTop.remove(mindustrySectorFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         mindustrySectorFrame   = new IndustrySectorFrame(DeskTop);
                         DeskTop.add(mindustrySectorFrame);
                         DeskTop.repaint();
                         mindustrySectorFrame.setSelected(true);
                         DeskTop.updateUI();
                         mindustrySectorFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mIndustrySubSector)
               {
                    try
                    {
                         DeskTop.remove(mindustrySubSectorMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {   

                         mindustrySubSectorMasterFrame   = new IndustrySubSectorMasterFrame(DeskTop);
                         DeskTop.add(mindustrySubSectorMasterFrame);
                         DeskTop.repaint();
                         mindustrySubSectorMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         mindustrySubSectorMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mBrand)
               {
                    try
                    {
                         DeskTop.remove(mbrandMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {   

                         mbrandMasterFrame   = new BrandMasterFrame(DeskTop);
                         DeskTop.add(mbrandMasterFrame);
                         DeskTop.repaint();
                         mbrandMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         mbrandMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mTemperature)
               {
                    try
                    {
                         DeskTop.remove(mtemperatureMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {   

                         mtemperatureMasterFrame   = new TemperatureMasterFrame(DeskTop);
                         DeskTop.add(mtemperatureMasterFrame);
                         DeskTop.repaint();
                         mtemperatureMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         mtemperatureMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mSTGroup)
               {
                    try
                    {
                         DeskTop.remove(msTGroupMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {   

                         msTGroupMasterFrame   = new STGroupMasterFrame(DeskTop);
                         DeskTop.add(msTGroupMasterFrame);
                         DeskTop.repaint();
                         msTGroupMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         msTGroupMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               if(ae.getSource()==mMatRoom)
               {
                    try
                    {
                         DeskTop.remove(mmatRoomMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         mmatRoomMasterFrame   = new MatRoomMasterFrame(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(mmatRoomMasterFrame);
                         DeskTop.repaint();
                         mmatRoomMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         mmatRoomMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMatRack)
               {
                    try
                    {
                         DeskTop.remove(mmatRackMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         mmatRackMasterFrame   = new MatRackMasterFrame(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(mmatRackMasterFrame);
                         DeskTop.repaint();
                         mmatRackMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         mmatRackMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMatRow)
               {
                    try
                    {
                         DeskTop.remove(mmatRowMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         mmatRowMasterFrame   = new MatRowMasterFrame(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(mmatRowMasterFrame);
                         DeskTop.repaint();
                         mmatRowMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         mmatRowMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMatColum)
               {
                    try
                    {
                         DeskTop.remove(mmatColumMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         mmatColumMasterFrame   = new MatColumMasterFrame(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(mmatColumMasterFrame);
                         DeskTop.repaint();
                         mmatColumMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         mmatRoomMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMatTray)
               {
                    try
                    {
                         DeskTop.remove(mmatTrayMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         mmatTrayMasterFrame   = new MatTrayMasterFrame(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(mmatTrayMasterFrame);
                         DeskTop.repaint();
                         mmatTrayMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         mmatTrayMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMatBox)
               {
                    try
                    {
                         DeskTop.remove(mmatBoxMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         mmatBoxMasterFrame   = new MatBoxMasterFrame(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(mmatBoxMasterFrame);
                         DeskTop.repaint();
                         mmatBoxMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         mmatBoxMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mRatePeriodical)
               {

                    try
                    {

                         DeskTop.remove(mratePerodicalMasterFrame);

                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {   

                         mratePerodicalMasterFrame   = new RatePeriodicalMasterFrame(DeskTop);
                         DeskTop.add(mratePerodicalMasterFrame);
                         DeskTop.repaint();
                         mratePerodicalMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         mratePerodicalMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                          ex.printStackTrace();
                    }
               }

               if(ae.getSource()==mQualityPeriodical)
               {

                    try
                    {

                         DeskTop.remove(mqualityPeriodicalMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {   

                         mqualityPeriodicalMasterFrame   = new QualityPeriodicalMasterFrame(DeskTop);
                         DeskTop.add(mqualityPeriodicalMasterFrame);
                         DeskTop.repaint();
                         mqualityPeriodicalMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         mqualityPeriodicalMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mCostingType)
               {

                    try
                    {

                         DeskTop.remove(mcostingTypeMasterFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {   

                         mcostingTypeMasterFrame   = new CostingTypeMasterFrame(DeskTop);
                         DeskTop.add(mcostingTypeMasterFrame);
                         DeskTop.repaint();
                         mcostingTypeMasterFrame.setSelected(true);
                         DeskTop.updateUI();
                         mcostingTypeMasterFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               /*
               if(ae.getSource()==mItemDelete)
               {
                    try
                    {
                         DeskTop.remove(itemmasterdeletionframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         itemmasterdeletionframe = new ItemMasterDeletionFrame(DeskTop,iMillCode,SItemTable);
                         DeskTop.add(itemmasterdeletionframe);
                         DeskTop.repaint();
                         itemmasterdeletionframe.setSelected(true);
                         DeskTop.updateUI();
                         itemmasterdeletionframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               */

               if(ae.getSource()==mItemMerge)
               {
                    try
                    {
                         DeskTop.remove(itemmergingframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         itemmergingframe = new ItemMergingFrame(DeskTop);
                         DeskTop.add(itemmergingframe);
                         DeskTop.repaint();
                         itemmergingframe.setSelected(true);
                         DeskTop.updateUI();
                         itemmergingframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mMRS)
               {
                    try
                    {
                         DeskTop.remove(mrsFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         mrsFrame   = new MRSFrame(DeskTop,iUserCode,iMillCode,VCode,VName,VNameCode,VCat,VDraw,VPaperColor,VPaperSets,VPaperSize,VPaperSide,VSlipNo,SItemTable);
                         DeskTop.add(mrsFrame);
                         DeskTop.repaint();
                         mrsFrame.setSelected(true);
                         DeskTop.updateUI();
                         mrsFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMRSSplit)
               {
                    try
                    {
                         DeskTop.remove(mrsSplitFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         mrsSplitFrame   = new MrsSplitFrame(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,iAuthCode,SSupTable,SItemTable);
                         DeskTop.add(mrsSplitFrame);
                         DeskTop.repaint();
                         mrsSplitFrame.setSelected(true);
                         DeskTop.updateUI();
                         mrsSplitFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMRSList)
               {
                    try
                    {
                         DeskTop.remove(mrsListFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         mrsListFrame   = new MRSListFrameTest(DeskTop,iMillCode,iAuthCode,iUserCode,VCode,VName,VNameCode,VCat,VDraw,VPaperColor,VPaperSets,VPaperSize,VPaperSide,VSlipNo,SItemTable);
                         DeskTop.add(mrsListFrame);
                         DeskTop.repaint();
                         mrsListFrame.setSelected(true);
                         DeskTop.updateUI();
                         mrsListFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               if(ae.getSource()==mMRSTempList)
               {
                    try
                    {
                         DeskTop.remove(mrsTempListFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         mrsTempListFrame   = new MRSTempListFrame(DeskTop,iMillCode,iAuthCode,iUserCode,VCode,VName,VNameCode,VCat,VDraw,VPaperColor,VPaperSets,VPaperSize,VPaperSide,VSlipNo,SItemTable);
                         DeskTop.add(mrsTempListFrame);
                         DeskTop.repaint();
                         mrsTempListFrame.setSelected(true);
                         DeskTop.updateUI();
                         mrsTempListFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               if(ae.getSource()==mProjectList)
               {
                    try
                    {
                         DeskTop.remove(projectlistframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         projectlistframe   = new ProjectListFrame(DeskTop,iMillCode,iAuthCode,iUserCode,VCode,VName,VNameCode,VCat,VDraw,VPaperColor,VPaperSets,VPaperSize,VPaperSide,VSlipNo,SItemTable);
                         DeskTop.add(projectlistframe);
                         DeskTop.repaint();
                         projectlistframe.setSelected(true);
                         DeskTop.updateUI();
                         projectlistframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
				if(ae.getSource()==mMonthlyMRSItemRemoval)
               {
                    try
                    {
                         DeskTop.remove(mrsitemremoveframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         mrsitemremoveframe   = new MonthlyMRSItemRemovalFrame(DeskTop,iMillCode);
                         DeskTop.add(mrsitemremoveframe);
                         DeskTop.repaint();
                         mrsitemremoveframe.setSelected(true);
                         DeskTop.updateUI();
                         mrsitemremoveframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               if(ae.getSource()==mMRSConvList)
               {
                    try
                    {
                         DeskTop.remove(mrsConvListFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         mrsConvListFrame   = new MRSConversionPendingListFrame(DeskTop,iMillCode,iAuthCode,iUserCode);;
                         DeskTop.add(mrsConvListFrame);
                         DeskTop.repaint();
                         mrsConvListFrame.setSelected(true);
                         DeskTop.updateUI();
                         mrsConvListFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMRSMonthPlan)
               {
                    try
                    {
                         DeskTop.remove(planmrsframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         planmrsframe   = new MonthPlanMRSFrame(DeskTop,iMillCode,iUserCode,SItemTable);
                         DeskTop.add(planmrsframe);
                         DeskTop.repaint();
                         planmrsframe.setSelected(true);
                         DeskTop.updateUI();
                         planmrsframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			
               if(ae.getSource()==mRegularPlanViewFrame)
               {
                    try
                    {
                         DeskTop.remove(regularplanviewframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         regularplanviewframe   = new RegularPlanViewFrame(DeskTop,iMillCode,iUserCode,SItemTable,SMillName,SStDate,SEnDate);
                         DeskTop.add(regularplanviewframe);
                         DeskTop.repaint();
                         regularplanviewframe.setSelected(true);
                         DeskTop.updateUI();
                         regularplanviewframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMRSListDetails)
               {
                    try
                    {
                         DeskTop.remove(mrsListDetailsFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         mrsListDetailsFrame   = new MRSListDetailsFrame (DeskTop,iMillCode,iAuthCode,iUserCode,SItemTable,SMillName);
                         DeskTop.add(mrsListDetailsFrame);
                         DeskTop.repaint();
                         mrsListDetailsFrame.setSelected(true);
                         DeskTop.updateUI();
                         mrsListDetailsFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMRSListPrint)
               {
                    try
                    {
                         DeskTop.remove(mrsListPrint);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         mrsListPrint   = new MRSListPrint(DeskTop,iMillCode,iAuthCode,iUserCode,SItemTable,SSupTable,SMillName);
                         DeskTop.add(mrsListPrint);
                         DeskTop.repaint();
                         mrsListPrint.setSelected(true);
                         DeskTop.updateUI();
                         mrsListPrint.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMRSMonComp)
               {
                    try
                    {
                         DeskTop.remove(mrscompframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         mrscompframe = new MRSCompFrame(DeskTop,VCode,VName,VNameCode,iMillCode,SStDate,SEnDate,SItemTable);
                         DeskTop.add(mrscompframe);
                         DeskTop.repaint();
                         mrscompframe.setSelected(true);
                         DeskTop.updateUI();
                         mrscompframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }


               if(ae.getSource()==mMRSClosing)
               {
                    try
                    {
                         DeskTop.remove(mrspostingagainstcashgrnframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         mrspostingagainstcashgrnframe = new MRSPostingAgainstCashGrnFrame(DeskTop,VCode,VName,SPanel,iMillCode);
                         DeskTop.add(mrspostingagainstcashgrnframe);
                         DeskTop.repaint();
                         mrspostingagainstcashgrnframe.setSelected(true);
                         DeskTop.updateUI();
                         mrspostingagainstcashgrnframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mGRedPendPrin)
               {
                    try
                    {
                         DeskTop.remove(GrejectPendprint);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         GrejectPendprint = new GRejectionPendingPrint(DeskTop,iMillCode,iUserCode,SSupTable);
                         DeskTop.add(GrejectPendprint);
                         DeskTop.repaint();
                         GrejectPendprint.setSelected(true);
                         DeskTop.updateUI();
                         GrejectPendprint.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMRSPendingList)
               {
                    try
                    {
                         DeskTop.remove(mrsPendingList);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         mrsPendingList   = new MRSPendingList(DeskTop,iMillCode,iAuthCode,iUserCode,SItemTable,SSupTable,SMillName);
                         DeskTop.add(mrsPendingList);
                         DeskTop.repaint();
                         mrsPendingList.setSelected(true);
                         DeskTop.updateUI();
                         mrsPendingList.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mEnquiryList)
               {
                    try
                    {
                         DeskTop.remove(enquirylistframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         enquirylistframe = new EnquiryListFrame(DeskTop,VCode,VName,SPanel,iUserCode,iAuthCode,iMillCode,SItemTable,SSupTable,SMillName);
                         DeskTop.add(enquirylistframe);
                         DeskTop.repaint();
                         enquirylistframe.setSelected(true);
                         DeskTop.updateUI();
                         enquirylistframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               

               if(ae.getSource()==mEnquiryMail)
               {
                    try
                    {
                         DeskTop.remove(enquirylistform);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         enquirylistform = new EnquiryListForm(DeskTop,VCode,VName,SPanel,iAuthCode,iMillCode,SItemTable,SSupTable,SMillName); 
                         DeskTop.add(enquirylistform);
                         DeskTop.repaint();
                         enquirylistform.setSelected(true);
                         DeskTop.updateUI();
                         enquirylistform.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMRSEnquiry)
               {
                    try
                    {
                         DeskTop.remove(enquiryframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         enquiryframe = new EnquiryFrame(DeskTop,VCode,VName,SPanel,iMillCode,iUserCode,SItemTable,SSupTable);
                         DeskTop.add(enquiryframe);
                         DeskTop.repaint();
                         enquiryframe.setSelected(true);
                         DeskTop.updateUI();
                         enquiryframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mDirectEnquiry)
               {
                    try
                    {
                         DeskTop.remove(enquiryframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         directenquiryframe = new DirectEnquiryFrame(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,iUserCode,SItemTable,SSupTable);
                         DeskTop.add(directenquiryframe);
                         DeskTop.repaint();
                         directenquiryframe.setSelected(true);
                         DeskTop.updateUI();
                         directenquiryframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               /*if(ae.getSource()==mMatStock)
               {
                    MatReqframe = new MatReqFrame(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(MatReqframe);
                         DeskTop.repaint();
                         MatReqframe.setSelected(true);
                         DeskTop.updateUI();
                         MatReqframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex)
                    {
                        System.out.println(ex);
                    }
               }*/

               /*if(ae.getSource()==mMatMRSStock)
               {
                    MatReqMRSframe = new MatReqMRSFrame(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);

                    try
                    {
                         DeskTop.add(MatReqMRSframe);
                         DeskTop.repaint();
                         MatReqMRSframe.setSelected(true);
                         DeskTop.updateUI();
                         MatReqMRSframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex)
                    {
                        System.out.println(ex);
                    }
               }*/

               if(ae.getSource()==mMatMRSStockGst)
               {
                    MatReqMRSframegst = new MatReqMRSFrameGst(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);

                    try
                    {
                         DeskTop.add(MatReqMRSframegst);
                         DeskTop.repaint();
                         MatReqMRSframegst.setSelected(true);
                         DeskTop.updateUI();
                         MatReqMRSframegst.show();
                    }
                    catch(java.beans.PropertyVetoException ex)
                    {
                        System.out.println(ex);
                    }
               }

               /*if(ae.getSource()==mYearlyMRSOrder)
               {
                    YearlyMrsPendingframe = new YearlyMrsPendingFrame(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);

                    try
                    {
                         DeskTop.add(YearlyMrsPendingframe);
                         DeskTop.repaint();
                         YearlyMrsPendingframe.setSelected(true);
                         DeskTop.updateUI();
                         YearlyMrsPendingframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex)
                    {
                        System.out.println(ex);
                    }
               }*/

               if(ae.getSource()==mYearlyMRSOrderGst)
               {
                    YearlyMrsPendingframeGst = new YearlyMrsPendingFrameGst(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);

                    try
                    {
                         DeskTop.add(YearlyMrsPendingframeGst);
                         DeskTop.repaint();
                         YearlyMrsPendingframeGst.setSelected(true);
                         DeskTop.updateUI();
                         YearlyMrsPendingframeGst.show();
                    }
                    catch(java.beans.PropertyVetoException ex)
                    {
                        System.out.println(ex);
                    }
               }

               if(ae.getSource()==mDirectOrder)
               {
                    int iAmend = 0;

                    try
                    {
                         DeskTop.remove(directorderframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         directorderframe = new DirectOrderFrame(DeskTop,VCode,VName,VNameCode,SPanel,false,iAmend,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(directorderframe);
                         DeskTop.repaint();
                         directorderframe.setSelected(true);
                         DeskTop.updateUI();
                         directorderframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               if(ae.getSource()==mOrderAuth)
               {
                    try
                    {
                         DeskTop.remove(orderAuthFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         orderAuthFrame = new OrderAuthFrame(DeskTop,iUserCode,iMillCode,SSupTable);
                         DeskTop.add(orderAuthFrame);
                         DeskTop.repaint();
                         orderAuthFrame.setSelected(true);
                         DeskTop.updateUI();
                         orderAuthFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
               }
               if(ae.getSource()==mAuthPending)
               {
                    try
                    {
                         DeskTop.remove(authPendingFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         authPendingFrame = new AuthPendingFrame(DeskTop,iUserCode,iMillCode,SSupTable);
                         DeskTop.add(authPendingFrame);
                         DeskTop.repaint();
                         authPendingFrame.setSelected(true);
                         DeskTop.updateUI();
                         authPendingFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
               }

               /*if(ae.getSource()==mAdvReq)
               {
                    try
                    {
                         DeskTop.remove(advanceframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         advanceframe = new AdvanceFrame(DeskTop,VCode,VName,SPanel,iMillCode,iUserCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(advanceframe);
                         DeskTop.repaint();
                         advanceframe.setSelected(true);
                         DeskTop.updateUI();
                         advanceframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }*/

              if(ae.getSource()==mAdvReqGst)
               {
                    try
                    {
                         DeskTop.remove(advancegstframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         advancegstframe = new AdvanceGSTFrame(DeskTop,VCode,VName,SPanel,iMillCode,iUserCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(advancegstframe);
                         DeskTop.repaint();
                         advancegstframe.setSelected(true);
                         DeskTop.updateUI();
                         advancegstframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mAdvReqAbs)
               {
                    try
                    {
                         DeskTop.remove(advreqabsframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         advreqabsframe = new AdvReqAbsFrame(DeskTop,VCode,VName,SPanel,iMillCode,iAuthCode,SSupTable,SMillName);
                         DeskTop.add(advreqabsframe);
                         DeskTop.repaint();
                         advreqabsframe.setSelected(true);
                         DeskTop.updateUI();
                         advreqabsframe.show();

                      
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               /*if(ae.getSource()==mOrderList)
               {
                    try
                    {
                         DeskTop.remove(orderlistframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         orderlistframe = new OrderListFrame(DeskTop,VCode,VName,VNameCode,SPanel,iUserCode,iMillCode,iAuthCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(orderlistframe);
                         DeskTop.repaint();
                         orderlistframe.setSelected(true);
                         DeskTop.updateUI();
                         orderlistframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }*/

               if(ae.getSource()==mOrderListGst)
               {
                    try
                    {
                         DeskTop.remove(orderlistframegst);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         orderlistframegst = new OrderListFrameGst(DeskTop,VCode,VName,VNameCode,SPanel,iUserCode,iMillCode,iAuthCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(orderlistframegst);
                         DeskTop.repaint();
                         orderlistframegst.setSelected(true);
                         DeskTop.updateUI();
                         orderlistframegst.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mOrderMonComp)
               {
                    try
                    {
                         DeskTop.remove(ordercompframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         ordercompframe = new OrderCompFrame(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,SStDate,SEnDate,SSupTable);
                         DeskTop        . add(ordercompframe);
                         DeskTop        . repaint();
                         ordercompframe . setSelected(true);
                         DeskTop        . updateUI();
                         ordercompframe . show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }

               }

               if(ae.getSource()==mOrderDeletion)
               {
                    try
                    {
                         DeskTop.remove(deletionlistframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         deletionlistframe = new DeletionListFrame(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,iAuthCode,SSupTable);
                         DeskTop.add(deletionlistframe);
                         DeskTop.repaint();
                         deletionlistframe.setSelected(true);
                         DeskTop.updateUI();
                         deletionlistframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
               }

               if(ae.getSource()==mAdvReqDeletion)
               {
                    try
                    {
                         DeskTop.remove(advreqdeletionframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         advreqdeletionframe = new AdvReqDeletionFrame(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,iAuthCode,SSupTable);
                         DeskTop.add(advreqdeletionframe);
                         DeskTop.repaint();
                         advreqdeletionframe.setSelected(true);
                         DeskTop.updateUI();
                         advreqdeletionframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
               }

               if(ae.getSource()==mRateUpdation)
               {
                    try
                    {
                         DeskTop.remove(orderrateupdationframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         orderrateupdationframe = new OrderRateUpdationFrame(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,iAuthCode,SSupTable,SItemTable);
                         DeskTop.add(orderrateupdationframe);
                         DeskTop.repaint();
                         orderrateupdationframe.setSelected(true);
                         DeskTop.updateUI();
                         orderrateupdationframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
               }

               if(ae.getSource()==mMRSDeletion)
               {
                    try
                    {
                         DeskTop.remove(mrsdeletionlistframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         mrsdeletionlistframe = new MRSDeletionListFrame(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,iAuthCode);
                         DeskTop.add(mrsdeletionlistframe);
                         DeskTop.repaint();
                         mrsdeletionlistframe.setSelected(true);
                         DeskTop.updateUI();
                         mrsdeletionlistframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
               }
               if(ae.getSource()==mMonthlyMRS)
               {
                    try
                    {
                         DeskTop.remove(MonthlyMRS);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         MonthlyMRS = new NewMonthlyMRSNew(DeskTop,iMillCode,iAuthCode,iUserCode);
                         DeskTop.add(MonthlyMRS);
                         DeskTop.repaint();
                         MonthlyMRS.setSelected(true);
                         DeskTop.updateUI();
 					MonthlyMRS.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
               }

               /*if(ae.getSource()==mOrderDetail)
               {
                    //  For Modification
                    int iAmend = 1;
                    try
                    {
                         DeskTop.remove(orderdetailsframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         orderdetailsframe = new OrderDetailsFrame(DeskTop,VCode,VName,VNameCode,SPanel,iAmend,iUserCode,iMillCode,iAuthCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(orderdetailsframe);
                         DeskTop.repaint();
                         orderdetailsframe.setSelected(true);
                         DeskTop.updateUI();
                         orderdetailsframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }*/

               if(ae.getSource()==mOrderDetailGst)
               {
                    //  For Modification
                    int iAmend = 1;
                    try
                    {
                         DeskTop.remove(orderdetailsframegst);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         orderdetailsframegst = new OrderDetailsFrameGst(DeskTop,VCode,VName,VNameCode,SPanel,iAmend,iUserCode,iMillCode,iAuthCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(orderdetailsframegst);
                         DeskTop.repaint();
                         orderdetailsframegst.setSelected(true);
                         DeskTop.updateUI();
                         orderdetailsframegst.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mOrderSplit)
               {
                    try
                    {
                         DeskTop.remove(ordersplitframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         ordersplitframe = new OrderSplitFrame(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,iAuthCode,SSupTable,SYearCode);
                         DeskTop.add(ordersplitframe);
                         DeskTop.repaint();
                         ordersplitframe.setSelected(true);
                         DeskTop.updateUI();
                         ordersplitframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mOrderDetailConv)
               {
                    //  For Modification
                    int iAmend = 1;
                    try
                    {
                         DeskTop.remove(orderdetailsconvertedframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         orderdetailsconvertedframe = new OrderDetailsConvertedFrame(DeskTop,VCode,VName,VNameCode,SPanel,iAmend,iUserCode,iMillCode,iAuthCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(orderdetailsconvertedframe);
                         DeskTop.repaint();
                         orderdetailsconvertedframe.setSelected(true);
                         DeskTop.updateUI();
                         orderdetailsconvertedframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mOrderMod)
               {
                    //  List of Order Amendments for Super user 
                    int iAmend = 0;
                    try
                    {
                         DeskTop.remove(orderdetailsframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         orderdetailsframe = new OrderDetailsFrame(DeskTop,VCode,VName,VNameCode,SPanel,iAmend,iUserCode,iMillCode,iAuthCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(orderdetailsframe);
                         DeskTop.repaint();
                         orderdetailsframe.setSelected(true);
                         DeskTop.updateUI();
                         orderdetailsframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mOrderAbs)
               {
                    try
                    {
                         DeskTop.remove(orderabsframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         OrderAbsFrame orderabsframe = new OrderAbsFrame(DeskTop,VCode,VName,SPanel,SFinYear,iUserCode,iMillCode,SSupTable,SItemTable);
                         DeskTop.add(orderabsframe);
                         DeskTop.repaint();
                         orderabsframe.setSelected(true);
                         DeskTop.updateUI();
                         orderabsframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               if(ae.getSource()==mYearlyOrderAbs)
               {
                    try
                    {
                         DeskTop.remove(yearlyorderabsframegst);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         YearlyOrderAbsFrameGst yearlyorderabsframegst = new YearlyOrderAbsFrameGst(DeskTop,VCode,VName,VNameCode,SPanel,SFinYear,iUserCode,iAuthCode,iMillCode,SSupTable,SItemTable,SYearCode);
                         DeskTop.add(yearlyorderabsframegst);
                         DeskTop.repaint();
                         yearlyorderabsframegst.setSelected(true);
                         DeskTop.updateUI();
                         yearlyorderabsframegst.show();

                         /*YearlyOrderAbsFrame yearlyorderabsframe = new YearlyOrderAbsFrame(DeskTop,VCode,VName,VNameCode,SPanel,SFinYear,iUserCode,iAuthCode,iMillCode,SSupTable,SItemTable,SYearCode);
                         DeskTop.add(yearlyorderabsframe);
                         DeskTop.repaint();
                         yearlyorderabsframe.setSelected(true);
                         DeskTop.updateUI();
                         yearlyorderabsframe.show();*/
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

/*               if(ae.getSource()==mOrderAbsMail)
               {
                    try
                    {
                         DeskTop.remove(orderabsmailframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         OrderAbsMailFrame orderabsmailframe = new OrderAbsMailFrame(DeskTop,VCode,VName,SPanel,SFinYear,iUserCode,iMillCode);
                         DeskTop.add(orderabsmailframe);
                         DeskTop.repaint();
                         orderabsmailframe.setSelected(true);
                         DeskTop.updateUI();
                         orderabsmailframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
*/

               if(ae.getSource()==mOrderPending)
               {
                    try
                    {
                         DeskTop.remove(orderPendingList);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         OrderPendingList orderPendingList = new OrderPendingList(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,SSupTable,SItemTable);
                         DeskTop.add(orderPendingList);
                         DeskTop.repaint();
                         orderPendingList.setSelected(true);
                         DeskTop.updateUI();
                         orderPendingList.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
              
               if(ae.getSource()==mLastPOComp)
               {
                    try
                    {
                         DeskTop.remove(lastpocomparisonframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}

                    try
                    {                    
                         lastpocomparisonframe = new LastPOComparisonFrame(DeskTop,VCode,VName,SPanel,iMillCode,SSupTable);
                         DeskTop.add(lastpocomparisonframe);
                         DeskTop.repaint();
                         lastpocomparisonframe.setSelected(true);
                         DeskTop.updateUI();
                         lastpocomparisonframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               /*if(ae.getSource()==mNewGRN)
               {
                    DirectGRNFrame directgrnframe = new DirectGRNFrame(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,iAuthCode,iUserCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(directgrnframe);
                         DeskTop.repaint();
                         directgrnframe.setSelected(true);
                         DeskTop.updateUI();
                         directgrnframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }*/

               if(ae.getSource()==mGstGRN)
               {
                    DirectGRNFrameGst directgrnframegst = new DirectGRNFrameGst(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,iAuthCode,iUserCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(directgrnframegst);
                         DeskTop.repaint();
                         directgrnframegst.setSelected(true);
                         DeskTop.updateUI();
                         directgrnframegst.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
			   
               if(ae.getSource()==mGRNNew)
               {
                    GRNTest.DirectGRNFrameN directgrnframeN = new GRNTest.DirectGRNFrameN(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,iAuthCode,iUserCode,SYearCode,SItemTable,SSupTable);
					
                    try
                    {
                         DeskTop  		 . add(directgrnframeN);
                         DeskTop         . repaint();
                         directgrnframeN . setSelected(true);
                         DeskTop		 . updateUI();
                         directgrnframeN . show();
						 
                    }catch(java.beans.PropertyVetoException ex){
					}
               }
			   

/*               if(ae.getSource()==mNewGRN1)
               {
                    DirectGRNFrame1 directgrnframe = new DirectGRNFrame1(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,iAuthCode,iUserCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(directgrnframe);
                         DeskTop.repaint();
                         directgrnframe.setSelected(true);
                         DeskTop.updateUI();
                         directgrnframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }*/


               if(ae.getSource()==mInvNotReceived)
               {
                    InvNotReceivedFrame frame = new InvNotReceivedFrame(DeskTop);
                    try
                    {
                         DeskTop.add(frame);
                         DeskTop.repaint();
                         frame.setSelected(true);
                         DeskTop.updateUI();
                         frame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mGRNPendingList)
               {
                    GRNAuthenticationPendingList frame = new GRNAuthenticationPendingList(DeskTop,iMillCode);
                    try
                    {
                         DeskTop.add(frame);
                         DeskTop.repaint();
                         frame.setSelected(true);
                         DeskTop.updateUI();
                         frame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mPendingInvReceived)
               {
                    PendingInvocieReceivedFrame frame = new PendingInvocieReceivedFrame(DeskTop);
                    try
                    {
                         DeskTop.add(frame);
                         DeskTop.repaint();
                         frame.setSelected(true);
                         DeskTop.updateUI();
                         frame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               /*if(ae.getSource()==mDirectGRN)
               {
                    NewDirectGRNFrame directgrnframe = new NewDirectGRNFrame(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,iAuthCode,iUserCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(directgrnframe);
                         DeskTop.repaint();
                         directgrnframe.setSelected(true);
                         DeskTop.updateUI();
                         directgrnframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }*/


               /*if(ae.getSource()==mDirectGRN1)
               {
                    NewDirectGRNFrame1 directgrnframe = new NewDirectGRNFrame1(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,iAuthCode,iUserCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(directgrnframe);
                         DeskTop.repaint();
                         directgrnframe.setSelected(true);
                         DeskTop.updateUI();
                         directgrnframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }*/


               if(ae.getSource()==mIssuedGRN)
               {
                    IssuedMaterialGRNFrame issuedmaterialgrnframe = new IssuedMaterialGRNFrame(DeskTop,VCode,VName,SPanel,iMillCode,iAuthCode,iUserCode,SItemTable,SSupTable,SYearCode);
                    try
                    {
                         DeskTop.add(issuedmaterialgrnframe);
                         DeskTop.repaint();
                         issuedmaterialgrnframe.setSelected(true);
                         DeskTop.updateUI();
                         issuedmaterialgrnframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mOthersGRN)
               {
                    OthersGRNFrame othersgrnframe = new OthersGRNFrame(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,iAuthCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(othersgrnframe);
                         DeskTop.repaint();
                         othersgrnframe.setSelected(true);
                         DeskTop.updateUI();
                         othersgrnframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
                

/*               if(ae.getSource()==mOthersGRN1)
               {
                    OthersGRNFrame1 othersgrnframe = new OthersGRNFrame1(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,iAuthCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(othersgrnframe);
                         DeskTop.repaint();
                         othersgrnframe.setSelected(true);
                         DeskTop.updateUI();
                         othersgrnframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }*/
                
    
               if(ae.getSource()==mStockTransPend)
               {
                    StockTransferPendingList stocktransferpendinglist = new StockTransferPendingList(DeskTop,SPanel,iMillCode);
                    try
                    {
                         DeskTop.add(stocktransferpendinglist);
                         DeskTop.repaint();
                         stocktransferpendinglist.setSelected(true);
                         DeskTop.updateUI();
                         stocktransferpendinglist.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mOthersIssue)
               {
                    FreeTrialIssueFrame freetrialissueframe = new FreeTrialIssueFrame(DeskTop,VCode,VName,SPanel,iMillCode,iAuthCode,SSupTable);
                    try
                    {
                         DeskTop.add(freetrialissueframe);
                         DeskTop.repaint();
                         freetrialissueframe.setSelected(true);
                         DeskTop.updateUI();
                         freetrialissueframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mGRNMonComp)
               {
                    GRNCompFrame grncompframe = new GRNCompFrame(DeskTop,VCode,VName,SPanel,iMillCode,SStDate,SEnDate,SSupTable);
                    try
                    {
                         DeskTop.add(grncompframe);
                         DeskTop.repaint();
                         grncompframe.setSelected(true);
                         DeskTop.updateUI();
                         grncompframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mGatePending)
               {
                    GIPendingList gipendinglist = new GIPendingList(DeskTop,VCode,VName,SPanel,iMillCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(gipendinglist);
                         DeskTop.repaint();
                         gipendinglist.setSelected(true);
                         DeskTop.updateUI();
                         gipendinglist.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
// Inspection 18/03/2021  Enabled this menu item for Store Keeper Usage
               if(ae.getSource()==mInspection)
               {
                    InspectionFrame inspectionframe = new InspectionFrame(DeskTop,VCode,VName,SPanel,iMillCode,iUserCode,SSupTable);
                    try
                    {
                         DeskTop.add(inspectionframe);
                         DeskTop.repaint();
                         inspectionframe.setSelected(true);
                         DeskTop.updateUI();
                         inspectionframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               /*if(ae.getSource()==mRejection)
               {
                    GrnRejectionFrame grnrejectionframe = new GrnRejectionFrame(DeskTop,VCode,VName,SPanel,iMillCode,iUserCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(grnrejectionframe);
                         DeskTop.repaint();
                         grnrejectionframe.setSelected(true);
                         DeskTop.updateUI();
                         grnrejectionframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }*/

               if(ae.getSource()==mFinPending)
               {
                    FinPendingList finpendinglist = new FinPendingList(DeskTop,VCode,VName,SPanel,iMillCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(finpendinglist);
                         DeskTop.repaint();
                         finpendinglist.setSelected(true);
                         DeskTop.updateUI();
                         finpendinglist.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mSupplierList)
               {
                    SupplierGrnList supgrnlist = new SupplierGrnList(DeskTop,VCode,VName,SPanel,iMillCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(supgrnlist);
                         DeskTop.repaint();
                         supgrnlist.setSelected(true);
                         DeskTop.updateUI();
                         supgrnlist.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

              /* if(ae.getSource()==mLedger)
               {
                    try
                    {
                         DeskTop.remove(ledgerframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         ledgerframe = new LedgerFrame(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,SStDate,SEnDate,SItemTable,SSupTable);
                         DeskTop.add(ledgerframe);
                         DeskTop.repaint();
                         ledgerframe.setSelected(true);
                         DeskTop.updateUI();
                         ledgerframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }*/
               
               if(ae.getSource()==mItemSearch)
               {
                    /*
                    try
                    {
                         DeskTop.remove(itemsearchlist);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         itemsearchlist = new ItemSearchList(DeskTop,iMillCode,iUserCode,SItemTable);
                         DeskTop.add(itemsearchlist);
                         DeskTop.repaint();
                         itemsearchlist.setSelected(true);
                         DeskTop.updateUI();
                         itemsearchlist.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
                    */
               }

               if(ae.getSource()==mGRNList)
               {
                    GRNListFrame grnlistframe = new GRNListFrame(DeskTop,VCode,VName,SPanel,iMillCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(grnlistframe);
                         DeskTop.repaint();
                         grnlistframe.setSelected(true);
                         DeskTop.updateUI();
                         grnlistframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==MGrnUpdate)
               {
                    GrnUpdateFrame grnUpdateFrame = new GrnUpdateFrame(DeskTop,VCode,VName,SPanel,iMillCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(grnUpdateFrame);
                         DeskTop.repaint();
                         grnUpdateFrame.setSelected(true);
                         DeskTop.updateUI();
                         grnUpdateFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
			   
               if(ae.getSource()==mGRNDelete)
               {
                    GRNDeletionFrame grndeletionframe = new GRNDeletionFrame(DeskTop,VCode,VName,SPanel,iMillCode,SItemTable,SSupTable,iUserCode);
                    try
                    {
                         DeskTop.add(grndeletionframe);
                         DeskTop.repaint();
                         grndeletionframe.setSelected(true);
                         DeskTop.updateUI();
                         grndeletionframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }


               if(ae.getSource()==mGRNDetail)
               {
                    GRNListDetailsFrame grnlistdetailsframe = new GRNListDetailsFrame(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(grnlistdetailsframe);
                         DeskTop.repaint();
                         grnlistdetailsframe.setSelected(true);
                         DeskTop.updateUI();
                         grnlistdetailsframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mGRNAbs)
               {
                    GRNAbsFrame grnabsframe = new GRNAbsFrame(DeskTop,VCode,VName,SPanel,iMillCode,SYearCode,SItemTable,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(grnabsframe);
                         DeskTop.repaint();
                         grnabsframe.setSelected(true);
                         DeskTop.updateUI();
                         grnabsframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
                                  
               if(ae.getSource()==mGrnRejectionForm)
               {
                    try
                    {
                         DeskTop.remove(grnRejectionFormFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         grnRejectionFormFrame    = new GrnRejectionFormFrame(DeskTop,iMillCode,SSupTable,SMillName);
                         DeskTop                  . add(grnRejectionFormFrame);
                         DeskTop                  . repaint();
                         grnRejectionFormFrame    . setSelected(true);
                         DeskTop                  . updateUI();
                         grnRejectionFormFrame    . show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mRejRdcMatch)
               {
                    try
                    {
                         DeskTop.remove(rejectionRDCMatchFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         rejectionRDCMatchFrame   = new RejectionRDCMatchFrame(DeskTop,iUserCode,iMillCode,SSupTable);
                         DeskTop                  . add(rejectionRDCMatchFrame);
                         DeskTop                  . repaint();
                         rejectionRDCMatchFrame   . setSelected(true);
                         DeskTop                  . updateUI();
                         rejectionRDCMatchFrame   . show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mRejGiMatch)
               {
                    try
                    {
                         DeskTop.remove(rejectionGIMatchFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         rejectionGIMatchFrame   = new RejectionGIMatchFrame(DeskTop,iUserCode,iMillCode,SSupTable);
                         DeskTop                  . add(rejectionGIMatchFrame);
                         DeskTop                  . repaint();
                         rejectionGIMatchFrame    . setSelected(true);
                         DeskTop                  . updateUI();
                         rejectionGIMatchFrame    . show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mGIComp)
               {
                    try
                    {
                         DeskTop.remove(giframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         giframe        = new GIAllocationPrintFrame(DeskTop,iUserCode,iMillCode,SSupTable);
                         DeskTop       . add(giframe);
                         DeskTop       . repaint();
                         giframe       . setSelected(true);
                         DeskTop       . updateUI();
                         giframe       . show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mKardex)
               {
                    try
                    {
                         DeskTop.remove(kardexframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         kardexframe = new KardexFrame(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,SStDate,SEnDate,SItemTable,SSupTable);
                         DeskTop.add(kardexframe);
                         DeskTop.repaint();
                         kardexframe.setSelected(true);
                         DeskTop.updateUI();
                         kardexframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
			   
			   
			    if(ae.getSource()==mtestledger)
               {
                    try
                    {
                         DeskTop.remove(testledger);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         testledger = new TestingLedger(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,SStDate,SEnDate,SItemTable,SSupTable);
                         DeskTop.add(testledger);
                         DeskTop.repaint();
                         testledger.setSelected(true);
                         DeskTop.updateUI();
                         testledger.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
			   
                if(ae.getSource()==mDirectGRNGst)
               {
                    NewDirectGRNFrameGst directgrnframegst = new NewDirectGRNFrameGst(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,iAuthCode,iUserCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(directgrnframegst);
                         DeskTop.repaint();
                         directgrnframegst.setSelected(true);
                         DeskTop.updateUI();
                         directgrnframegst.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mStockUpdate)
               {
                    try
                    {
                         DeskTop.remove(itemstockupdationframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         //itemstockupdationframe = new ItemStockUpdationFrame(DeskTop,iMillCode,SItemTable);
                         itemstockupdationframe = new DeptWiseItemStockUpdationFrame(DeskTop,iMillCode,SItemTable);
                         DeskTop.add(itemstockupdationframe);
                         DeskTop.repaint();
                         itemstockupdationframe.setSelected(true);
                         DeskTop.updateUI();
                         itemstockupdationframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mMasterStock)
               {
                    try
                    {
                         DeskTop.remove(itemmasterstockupdationframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         itemmasterstockupdationframe = new ItemMasterStockUpdationFrame(DeskTop,iMillCode,SItemTable);
                         DeskTop.add(itemmasterstockupdationframe);
                         DeskTop.repaint();
                         itemmasterstockupdationframe.setSelected(true);
                         DeskTop.updateUI();
                         itemmasterstockupdationframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mStockRate)
               {
                    try
                    {
                         DeskTop.remove(itemstockrateupdationframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         itemstockrateupdationframe = new ItemStockRateUpdationFrame(DeskTop,iMillCode,SItemTable);
                         DeskTop.add(itemstockrateupdationframe);
                         DeskTop.repaint();
                         itemstockrateupdationframe.setSelected(true);
                         DeskTop.updateUI();
                         itemstockrateupdationframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mMelTrans)
               {
                    try
                    {
                         DeskTop.remove(melangestockupdationframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         melangestockupdationframe = new MelangeStockUpdationFrame(DeskTop,iUserCode,iMillCode,SItemTable,SSupTable,SYearCode);
                         DeskTop.add(melangestockupdationframe);
                         DeskTop.repaint();
                         melangestockupdationframe.setSelected(true);
                         DeskTop.updateUI();
                         melangestockupdationframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mMonIssue)
               {
                    try
                    {
                         DeskTop.remove(monissueframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}

                    try
                    {
                         monissueframe = new MonIssueFrame(DeskTop,iMillCode,SStDate,SEnDate,SMillName);
                         DeskTop.add(monissueframe);
                         DeskTop.repaint();
                         monissueframe.setSelected(true);
                         DeskTop.updateUI();
                         monissueframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mMonAbstract)
               {
                    try
                    {
                         DeskTop.remove(monAbstractframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}

                    try
                    {
                         monAbstractframe = new MonAbstractFrame(DeskTop,iMillCode,SStDate,SEnDate,SMillName);
                         DeskTop.add(monAbstractframe);
                         DeskTop.repaint();
                         monAbstractframe.setSelected(true);
                         DeskTop.updateUI();
                         monAbstractframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mPlanMRS)
               {
                    try
                    {
                         DeskTop.remove(planMRSFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         planMRSFrame   = new PlanMRSFrame(DeskTop,iMillCode,SStDate,SEnDate,iUserCode,SItemTable);
                         DeskTop        . add(planMRSFrame);
                         DeskTop        . repaint();
                         planMRSFrame   . setSelected(true);
                         DeskTop        . updateUI();
                         planMRSFrame   . show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mRegularPlanning)
               {
                    try
                    {
                         DeskTop.remove(newRegularPlanFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         newRegularPlanFrame = new NewRegularPlanFrame(DeskTop,VCode,VName,VNameCode,iMillCode,SFinYear,SStDate,SEnDate,iUserCode,SItemTable,SMillName);
                         DeskTop             . add(newRegularPlanFrame);
                         DeskTop             . repaint();
                         newRegularPlanFrame . setSelected(true);
                         DeskTop             . updateUI();
                         newRegularPlanFrame . show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mSpdlposting)
               {
                    try
                    {
                         DeskTop   . remove(spdlPostFrame);
                         DeskTop   . updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         spdlPostFrame  = new SpdlPostFrame(DeskTop,SFinYear,SStDate,SEnDate);
                         DeskTop        . add(spdlPostFrame);
                         DeskTop        . repaint();
                         spdlPostFrame  . setSelected(true);
                         DeskTop        . updateUI();
                         spdlPostFrame  . show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mVMatIso)
               {
                    MaterialISO MaterialIso = new MaterialISO(DeskTop,iMillCode,iAuthCode,SItemTable);
                    try
                    {
                         DeskTop.add(MaterialIso);
                         DeskTop.repaint();
                         MaterialIso.setSelected(true);
                         DeskTop.updateUI();
                         MaterialIso.show();
                    }
                    catch(java.beans.PropertyVetoException ex)
                    {
                        System.out.println(ex);
                    }
               }

               if(ae.getSource()==mVMatQtyAllow)
               {
                    MaterialQtyAllowance materialqtyallowance = new MaterialQtyAllowance(DeskTop,iMillCode,iAuthCode,SItemTable);
                    try
                    {
                         DeskTop.add(materialqtyallowance);
                         DeskTop.repaint();
                         materialqtyallowance.setSelected(true);
                         DeskTop.updateUI();
                         materialqtyallowance.show();
                    }
                    catch(java.beans.PropertyVetoException ex)
                    {
                        System.out.println(ex);
                    }
               }

               if(ae.getSource()==mVAbolish)
               {
                    MaterialAbolishedFrame materialabolishedframe = new MaterialAbolishedFrame(DeskTop,iMillCode,iAuthCode,SItemTable);
                    try
                    {
                         DeskTop.add(materialabolishedframe);
                         DeskTop.repaint();
                         materialabolishedframe.setSelected(true);
                         DeskTop.updateUI();
                         materialabolishedframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex)
                    {
                        System.out.println(ex);
                    }
               }

               if(ae.getSource()==mStockGroup)
               {
                    try
                    {
                         DeskTop.remove(stockgroupmaster);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         stockgroupmaster   = new StockGroupMaster(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(stockgroupmaster);
                         DeskTop.repaint();
                         stockgroupmaster.setSelected(true);
                         DeskTop.updateUI();
                         stockgroupmaster.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               if(ae.getSource()==mHod)
               {
                    try
                    {
                         DeskTop.remove(hodmasterframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         hodmasterframe   = new HodMasterFrame(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(hodmasterframe);
                         DeskTop.repaint();
                         hodmasterframe.setSelected(true);
                         DeskTop.updateUI();
                         hodmasterframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource()==mMatGroup)
               {
                    try
                    {
                         DeskTop.remove(matgroupmaster);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         matgroupmaster   = new MatGroupMaster(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(matgroupmaster);
                         DeskTop.repaint();
                         matgroupmaster.setSelected(true);
                         DeskTop.updateUI();
                         matgroupmaster.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource() == mEPCGRequestFrame)
               {
                    int iAmend = 0;

                    try
                    {
                         DeskTop.remove(epcgRequestFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}

                    try
                    {                    
                         epcgRequestFrame = new EPCGRequestFrame(DeskTop,VCode,VName,VNameCode,SPanel,false,iAmend,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(epcgRequestFrame);
                         DeskTop.repaint();
                         epcgRequestFrame.setSelected(true);
                         DeskTop.updateUI();
                         epcgRequestFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource() == mEPCGRequestListFrame)
               {
                    int iAmend = 0;

                    try
                    {
                         DeskTop.remove(epcgRequestListFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         epcgRequestListFrame = new EPCGRequestListFrame(DeskTop,VCode,VName,VNameCode,SPanel,iUserCode,iMillCode,iAuthCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(epcgRequestListFrame);
                         DeskTop.repaint();
                         epcgRequestListFrame.setSelected(true);
                         DeskTop.updateUI();
                         epcgRequestListFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

               if(ae.getSource() == mEPCGOrdersListFrame)
               {
                    int iAmend = 0;

                    try
                    {
                         DeskTop.remove(epcgorderslistframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         epcgorderslistframe = new EPCGOrdersListFrame(DeskTop,VCode,VName,VNameCode,SPanel,iUserCode,iMillCode,iAuthCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(epcgorderslistframe);
                         DeskTop.repaint();
                         epcgorderslistframe.setSelected(true);
                         DeskTop.updateUI();
                         epcgorderslistframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
             
               if(ae.getSource() == mGreenTapeReceiptDetails)
               {
                    int iAmend = 0;

                    try
                    {
                         DeskTop.remove(greentapeReceiptDetails);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         greentapeReceiptDetails = new GreenTapeReceiptDetails(DeskTop,iUserCode,iAuthCode,iMillCode,SStDate,SEnDate);
                         DeskTop.add(greentapeReceiptDetails);
                         DeskTop.repaint();
                         greentapeReceiptDetails.setSelected(true);
                         DeskTop.updateUI();
                         greentapeReceiptDetails.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
                }
                if(ae.getSource() == mGreenTapeDailyAuthentication)
               {
                    int iAmend = 0;

                    try
                    {
                         DeskTop.remove(greentapeDailyAuthentication);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         greentapeDailyAuthentication = new GreenTapeDailyAuthentication(DeskTop,iUserCode,iAuthCode,iMillCode);
                         DeskTop.add(greentapeDailyAuthentication);
                         DeskTop.repaint();
                         greentapeDailyAuthentication.setSelected(true);
                         DeskTop.updateUI();
                         greentapeDailyAuthentication.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               if(ae.getSource() == mGreenTapeAuthentication)
               {
                    int iAmend = 0;

                    try
                    {
                         DeskTop.remove(greentapeAuthFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                        int iAuthUserCode=0;
                         if(iUserCode==4)
                           iAuthUserCode=7225;
                         else if(iUserCode==2)
                           iAuthUserCode=1987;

                         greentapeAuthFrame = new GreenTapeAuthFrame(DeskTop,iUserCode,iAuthCode,iMillCode,iAuthUserCode);
                         DeskTop.add(greentapeAuthFrame);
                         DeskTop.repaint();
                         greentapeAuthFrame.setSelected(true);
                         DeskTop.updateUI();
                         greentapeAuthFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

				if(ae.getSource()==mOrderPendingProcess)
               {
                    try
                    {
                         DeskTop.remove(orderPendingprocessList);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {                    
                         OrderPendingProcessList orderPendingprocessList = new OrderPendingProcessList(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,SSupTable,SItemTable);
                         DeskTop.add(orderPendingprocessList);
                         DeskTop.repaint();
                         orderPendingprocessList.setSelected(true);
                         DeskTop.updateUI();
                         orderPendingprocessList.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			   
				if(ae.getSource()==mStoreMaterialUnLoad)
				{
                    StoreMaterialUnLoad theFrame1 = new StoreMaterialUnLoad(DeskTop,VCode,VName,VNameCode,SPanel,iMillCode,SStDate,SEnDate,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(theFrame1);
                         DeskTop.repaint();
                         theFrame1.setSelected(true);
                         DeskTop.updateUI();
                         theFrame1.show();
                    }
                    catch(java.beans.PropertyVetoException ex)
                    {
                        System.out.println(ex);
                    }
               }
				if(ae.getSource()==mStoreMaterialAuthentication)
				{
                    StoreMaterialAuthenticationFrame theFrame = new StoreMaterialAuthenticationFrame(DeskTop,iMillCode,SItemTable,iUserCode);
                    try
                    {
                         DeskTop.add(theFrame);
                         DeskTop.repaint();
                         theFrame.setSelected(true);
                         DeskTop.updateUI();
                         theFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex)
                    {
                        System.out.println(ex);
                    }
               }


               if(ae.getSource()==mPriceRevisionEntry){
                  PriceRevisionEntryFrame frame = new PriceRevisionEntryFrame(DeskTop);
                    try
                    {
                         DeskTop.add(frame);
                         DeskTop.repaint();
                         frame.setSelected(true);
                         DeskTop.updateUI();
                         frame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               
               
               if(ae.getSource()==mPriceRevisionEntryNew){
                  
                    try
                    {
                         DeskTop.remove(priceRevisionEntryFrameNew);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                  
                    try
                    {
                    	 priceRevisionEntryFrameNew = new PriceRevisionEntryFrameNew(DeskTop, iUserCode);
                         DeskTop				    . add(priceRevisionEntryFrameNew);
                         DeskTop			        . repaint();
                         priceRevisionEntryFrameNew . setSelected(true);
                         DeskTop					. updateUI();
                         priceRevisionEntryFrameNew . show();
                         
                    }
                    catch(java.beans.PropertyVetoException ex){
						System.out.println("ex:"+ex);
					}
                    catch(Exception ex1){
						System.out.println("ex1"+ex1);
					}					
               }
               

               if(ae.getSource()==mPriceRevisionEntry_Old){
			PriceRevisionEntryFrame_Old  frame = new PriceRevisionEntryFrame_Old(DeskTop);
                    try
                    {
                         DeskTop.add(frame);
                         DeskTop.repaint();
                         frame.setSelected(true);
                         DeskTop.updateUI();
                         frame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mDyes_CostingProcess){
                    try
                    {
                         DeskTop.remove(costingProcess);
                         DeskTop.updateUI();
                         
                    }catch(Exception ex){}
               
					costingProcess = new PriceRevisionCostingProcess(DeskTop);
					
                    try
                    {
                         DeskTop.add(costingProcess);
                         DeskTop.repaint();
                         costingProcess.setSelected(true);
                         DeskTop.updateUI();
                         costingProcess.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               
               
               if(ae.getSource()==mDyesCostingProcessNew){
                    try
                    {
                         DeskTop.remove(costingProcess);
                         DeskTop.updateUI();
                         
                    }catch(Exception ex){}
               
					costingProcess = new PriceRevisionCostingProcess(DeskTop);
					
                    try
                    {
                         DeskTop.add(costingProcess);
                         DeskTop.repaint();
                         costingProcess.setSelected(true);
                         DeskTop.updateUI();
                         costingProcess.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               

             if(ae.getSource()==mShadewiseDyesReport){
			 	shadewiseMultipleDyesReport = new ShadeWiseMultipleDyesReport(DeskTop);
                    try
                    {
                         DeskTop.add(shadewiseMultipleDyesReport);
                         DeskTop.repaint();
                         shadewiseMultipleDyesReport.setSelected(true);
                         DeskTop.updateUI();
                         shadewiseMultipleDyesReport.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
				

              if(ae.getSource()==mShadewiseDyesReportNew) {
              
                    try
                    {
                         DeskTop.remove(shadewiseDyesReport);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
              
                    try{
			 			shadewiseDyesReport = new ShadeWiseDyesReport(DeskTop);
                        DeskTop			    . add(shadewiseDyesReport);
                        DeskTop			    . repaint();
                        shadewiseDyesReport . setSelected(true);
                        DeskTop				. updateUI();
                        shadewiseDyesReport . show();
                        
                    }catch(java.beans.PropertyVetoException ex){
                    }
               }

              if(ae.getSource()==mShadewiseCostingReportNew) {
              
                    try
                    {
                         DeskTop.remove(shadeReceipeCostingReport);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
              
                    try{
			 			shadeReceipeCostingReport = new ShadeReceipeCostingReport(DeskTop);
                        DeskTop			    	  . add(shadeReceipeCostingReport);
                        DeskTop			    	  . repaint();
                        shadeReceipeCostingReport . setSelected(true);
                        DeskTop					  . updateUI();
                        shadeReceipeCostingReport . show();
                        
                    }catch(java.beans.PropertyVetoException ex){
                    }
              }

	    if(ae.getSource()==mReverseChargeFrame)
				{
                    int iDivisionCode = (iMillCode+1);
	 	   ReverseChargePurchaseFrame frame = new ReverseChargePurchaseFrame(DeskTop,iUserCode,iDivisionCode,SYearCode,iMillCode);
                    try
                    {
                         DeskTop.add(frame);
                         DeskTop.repaint();
                         frame.setSelected(true);
                         DeskTop.updateUI();
                         frame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }


               /*if(ae.getSource()==mSupplier)
               {
                    try
                    {
                         DeskTop.remove(supplierentryframe);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         supplierentryframe   = new SupplierEntryFrame(DeskTop,iUserCode,iMillCode);
                         DeskTop.add(supplierentryframe);
                         DeskTop.repaint();
                         supplierentryframe.setSelected(true);
                         DeskTop.updateUI();
                         supplierentryframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }*/


               if(ae.getSource()==miVMatEntryGst)
               {
                    System.runFinalization();
                    System.gc();
                    try
                    {
                         DeskTop.remove(gstMaterialEntryFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         gstMaterialEntryFrame   = new GstMaterialEntryFrame(DeskTop,iMillCode,iAuthCode,iUserCode,SItemTable);
                         DeskTop.add(gstMaterialEntryFrame);
                         DeskTop.repaint();
                         gstMaterialEntryFrame.setSelected(true);
                         DeskTop.updateUI();
                         gstMaterialEntryFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
                    System.runFinalization();
                    System.gc();
               }

	       if(ae.getSource()==miVHsnCodeUpdate)
               {
                    System.runFinalization();
                    System.gc();
                    try
                    {
                         DeskTop.remove(materialHsnCodeUpdation);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         materialHsnCodeUpdation   = new MaterialHsnCodeUpdation(DeskTop,iMillCode,iAuthCode,SItemTable);
                         DeskTop.add(materialHsnCodeUpdation);
                         DeskTop.repaint();
                         materialHsnCodeUpdation.setSelected(true);
                         DeskTop.updateUI();
                         materialHsnCodeUpdation.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
                    System.runFinalization();
                    System.gc();
               }

               if(ae.getSource() == mGSTNo)
               {
                    try
                    {
                         DeskTop.remove(gstnoupdation);
                         DeskTop.updateUI();
                    }catch(Exception ex){}

                    try
                    {
                         gstnoupdation		   = new GSTNoUpdationFrame(DeskTop);
                         DeskTop             	   . add(gstnoupdation);
                         DeskTop             	   . repaint();
                         gstnoupdation		   . setSelected(true);
                         DeskTop             	   . updateUI();
                         gstnoupdation		   . show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			   
			   if(ae.getSource()==miMotorHistory)
               {
                    System.runFinalization();
                    System.gc();
                    try
                    {
                         DeskTop.remove(motorHistoryUpdation);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         motorHistoryUpdation   = new MotorHistoryEntryFrame(DeskTop,iMillCode,iUserCode);
                         DeskTop.add(motorHistoryUpdation);
                         DeskTop.repaint();
                         motorHistoryUpdation.setSelected(true);
                         DeskTop.updateUI();
                         motorHistoryUpdation.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
                    System.runFinalization();
                    System.gc();
               }


               if(ae.getSource()==mRejectionGst)
               {
                    GrnRejectionFrameGst grnrejectionframegst = new GrnRejectionFrameGst(DeskTop,VCode,VName,SPanel,iMillCode,iUserCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(grnrejectionframegst);
                         DeskTop.repaint();
                         grnrejectionframegst.setSelected(true);
                         DeskTop.updateUI();
                         grnrejectionframegst.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

		
	       if(ae.getSource()==miVGstRateUpdate)
               {
                    System.runFinalization();
                    System.gc();
                    try
                    {
                         DeskTop.remove(materialGstRateUpdationFrame);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         materialGstRateUpdationFrame   = new MaterialGstRateUpdationFrame(DeskTop,iMillCode,iAuthCode,iUserCode,SItemTable);
                         DeskTop.add(materialGstRateUpdationFrame);
                         DeskTop.repaint();
                         materialGstRateUpdationFrame.setSelected(true);
                         DeskTop.updateUI();
                         materialGstRateUpdationFrame.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
                    System.runFinalization();
                    System.gc();
               }
			   
			    //sales frame
				
				
				      if(ae.getSource()==mSalesFrame)
               {
                    try
                    {
                         DeskTop.remove(salesframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         salesframe = new SalesFrame(DeskTop,VCode,VName,VNameCode,SPanel,iUserCode,iMillCode,SItemTable,SSupTable,SYearCode,SMillName,SStDate);
                         DeskTop.add(salesframe);
                         DeskTop.repaint();
                         salesframe.setSelected(true);
                         DeskTop.updateUI();
                         salesframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			   
			   if(ae.getSource()==mGICashReceiptCancel)
               {
                    try
                    {
                         DeskTop.remove(GIcashreceiptcancelframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         GIcashreceiptcancelframe = new GICashReceiptCancelFrame(DeskTop,iUserCode);
                         DeskTop.add(GIcashreceiptcancelframe);
                         DeskTop.repaint();
                         GIcashreceiptcancelframe.setSelected(true);
                         DeskTop.updateUI();
                         GIcashreceiptcancelframe.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
			   

				 if(ae.getSource()==mSalesInvoicePDF)
               {
                    try
                    {
                         DeskTop.remove(SalesPdf);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         SalesPdf = new SalesInvoicePdfPrint(DeskTop);
                         DeskTop.add(SalesPdf);
                         DeskTop.repaint();
                         SalesPdf.setSelected(true);
                         DeskTop.updateUI();
                         SalesPdf.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }

              // hsn modify

              if(ae.getSource()==miVHsnCodeModify)
               {
                    System.runFinalization();
                    System.gc();
                    try
                    {
                         DeskTop.remove(materialHsnCodeModification);
                         DeskTop.updateUI();
                    }
                    catch(Exception ex){}
                    try
                    {
                         materialHsnCodeModification   = new MaterialHsnCodeModification(DeskTop,iMillCode,iAuthCode,SItemTable);
                         DeskTop.add(materialHsnCodeModification);
                         DeskTop.repaint();
                         materialHsnCodeModification.setSelected(true);
                         DeskTop.updateUI();
                         materialHsnCodeModification.show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
                    System.runFinalization();
                    System.gc();
               }

	       if(ae.getSource()==mMatStockGst)
               {
                    matReqGstFrame = new MatReqGstFrame(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);
                    try
                    {
                         DeskTop.add(matReqGstFrame);
                         DeskTop.repaint();
                         matReqGstFrame.setSelected(true);
                         DeskTop.updateUI();
                         matReqGstFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex)
                    {
                        System.out.println(ex);
                    }
               }


          }
     }

     public void setDataIntoVector()
     {
          ResultSet result = null;

          VName.removeAllElements();
          VCode.removeAllElements();

          theMenuBar.setEnabled(false);

          int iMax = common.toInt(getID("Select count(*) From InvItems"));

          JProgressBar progress = new JProgressBar();
          progress.setMinimum(0);
          progress.setMaximum(iMax);
          SPanel.Panel2.add(progress);
          SPanel.Panel2.updateUI();

          String QS = "";

          if(iMillCode==0)
          {
               QS =  " Select Item_Name,Item_Code,InvItems.UoMCode,Catl,Draw,UomName,PaperColor,PaperSets,PaperSize,PaperSide,LastSlipNo+1 From InvItems "+
                     " Inner join Uom On Uom.UomCode=InvItems.UomCode "+
                     " Order By Item_Name";
          }
          else
          {
               QS  = " Select InvItems.Item_Name,"+SItemTable+".Item_Code,InvItems.UoMCode,InvItems.Catl,InvItems.Draw,UomName,PaperColor,PaperSets,PaperSize,PaperSide,"+SItemTable+".LastSlipNo+1 From "+SItemTable+" "+
                     " Inner Join InvItems On "+
                     " InvItems.Item_Code = "+SItemTable+".Item_Code "+
                     " Inner join Uom On Uom.UomCode=InvItems.UomCode "+
                     " Order By Item_Name  ";
          }
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
               result                        =  theStatement.executeQuery(QS);

               int ctr=0;
               while(result.next())
               {
                    ctr++;
                    String str1 = result.getString(1);
                    String str2 = result.getString(2);
                    String str3 = result.getString(3);
                    String str4 = result.getString(4);
                    String str5 = result.getString(5);
                    String str6 = (String)result.getString(6);

                    VName.addElement(str1);
                    VCode.addElement(str2);
                    VNameCode.addElement(str1+" - "+str6+" (Code : "+str2+")"+" (Catl : "+str4+")"+" (Draw : "+str5+")");
                    progress.setValue(ctr);

                    VCat    .addElement(common.parseNull((String)result.getString(4)));
                    VDraw   .addElement(common.parseNull((String)result.getString(5)));
                    VUomName.addElement(common.parseNull((String)result.getString(6)));

                    VPaperColor.addElement(common.parseNull(result.getString(7)));
                    VPaperSets.addElement(common.parseNull(result.getString(8)));
                    VPaperSize.addElement(common.parseNull(result.getString(9)));
                    VPaperSide.addElement(common.parseNull(result.getString(10)));
                    VSlipNo.addElement(common.parseNull(result.getString(11)));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               JOptionPane.showMessageDialog(null,"Unable to Establish a Connection with Server","Information",JOptionPane.INFORMATION_MESSAGE);
               System.exit(0);
          }
          SPanel.Panel2.removeAll();
          SPanel.Panel2.updateUI();
          theMenuBar.setEnabled(true);
          wSplash.setVisible(false);
     }

     public String getID(String QueryString)
     {
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
               ResultSet theResult           = theStatement.executeQuery(QueryString);
               while(theResult.next())
               {
                    ID = theResult.getString(1);
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);  
          }
          return ID;
     }

     private void setProjectPendingCount()
     {
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
               ResultSet theResult           = theStatement.executeQuery("Select GET_PROJECTSOAPPROVAL_PENDING("+iMillCode+") from dual");
               while(theResult.next())
               {
                    SProjectCount = theResult.getString(1);
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);  
          }
     }
     private int getItemMatchingCount()
     {
        int iCount=0;
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
               ResultSet theResult           = theStatement.executeQuery("Select count(1) from MRSItems  where Matching_Item_Code is null  and SoAuthenticationStatus=0 and SoRejectionStatus=0 ");
               while(theResult.next())
               {
                    iCount = theResult.getInt(1);
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               iCount=0;
          }
          return iCount;
     }
     public void updateLookAndFeel()
     {
          try
          {
               UIManager           . setLookAndFeel(currentLookAndFeel);

               /*UIManager           . put("InternalFrame.activeTitleBackground"  , new ColorUIResource(new Color(204,51,51)));
               UIManager           . put("Frame.activeTitleBackground"          , new ColorUIResource(new Color(204,51,51)));

               Border theBorder    = BorderFactory.createRaisedBevelBorder();
               Border doubleBorder = new CompoundBorder(theBorder, theBorder);

               UIManager           . put("Button.border"                        , doubleBorder);
               UIManager           . put("Panel.background"                     , new ColorUIResource(new Color(213,234,255)));
               UIManager           . put("MenuBar.background"                   , new ColorUIResource(new Color(213,234,255)));

               UIManager           . put("TabbedPane.selected"                  , new ColorUIResource(Color.PINK));
               UIManager           . put("TabbedPane.shadow"                    , new ColorUIResource(Color.GRAY));
               UIManager           . put("TabbedPane.highlight"                 , new ColorUIResource(Color.GRAY));
               UIManager           . put("TabbedPane.foreground"                , new ColorUIResource(Color.BLUE));*/
          }
          catch (Exception ex)
          {
               System.out.println("Failed loading L&F: " + currentLookAndFeel);
               System.out.println(ex);
          }
     }

     public int getUserCode()
     {
          return iUserCode;
     }
}
