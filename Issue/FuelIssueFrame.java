/*
Auto Generated Fuel Issuance Frame
*/

package Issue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class FuelIssueFrame extends JInternalFrame
{
     protected           JLayeredPane Layer;
     String              SStDate,SEnDate;
     
     protected String    SDSN = "jdbc:odbc:Electric";
     
     JPanel              TopPanel;
     JPanel              BottomPanel;
     JPanel              MiddlePanel;
     
     DateField           TStDate,TEnDate;
     JButton             BApply;
     JTextArea           TA;
     
     Vector              VDate,VGenIssue,VVehIssue,VUnit;

     int                 iMillCode,iUserCode;
     
     Common              common    = new Common();
     String              SMatCode  = "A11017100";

     
     public FuelIssueFrame(JLayeredPane Layer,String SStDate,String SEnDate,int iMillCode,int iUserCode)
     {
          this.Layer          = Layer;
          this.SStDate        = SStDate;
          this.SEnDate        = SEnDate;
          this.iMillCode      = iMillCode;
          this.iUserCode      = iUserCode;
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          TopPanel       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BottomPanel    = new JPanel(true);
          
          TStDate        = new DateField();
          TEnDate        = new DateField();
          BApply         = new JButton("Apply");
          
          TA             = new JTextArea();
          TA             . setEditable(false);
     }

     private void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          
          MiddlePanel.setLayout(new BorderLayout());
     }

     private void addComponents()
     {
          TopPanel            . add(new JLabel("Period"));
          TopPanel            . add(TStDate);
          TopPanel            . add(TEnDate);
          TopPanel            . add(BApply);
          TStDate             . setBorder(new TitledBorder("Start Date"));
          TEnDate             . setBorder(new TitledBorder("End Date"));
          
          MiddlePanel         . add("Center",new JScrollPane(TA));
          
          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("Center",MiddlePanel);
     }

     private void addListeners()
     {
          BApply    . addActionListener(new ApplyList());
     }

     private class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(isDateValid())
               {
                    BApply    . setEnabled(false);

                    fetchElectricalData();

                    TA        . setText("Data Fetching Over...");

                    insertElectricalData();

                    TA        . setText("Data Updation Over...");

                    updateIssueValuation();

                    System.out.println("Over");

                    removeHelpFrame();
               }
          }
     }

     private boolean isDateValid()
     {
          boolean bFlag = false;
          
          int iFromDate = common.toInt(TStDate.toNormal());
          int iToDate   = common.toInt(TEnDate.toNormal());
          int iStDate   = common.toInt(SStDate);
          int iEnDate   = common.toInt(SEnDate);
          
          if((iFromDate>=iStDate) && (iToDate<=iEnDate))
          {
               bFlag = true;
          }
          return bFlag;
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer     . remove(this);
               Layer     . updateUI();
               Layer     . repaint();
          }
          catch(Exception ex){}
     }

     private void fetchElectricalData()
     {
          VDate       = new Vector();
          VGenIssue   = new Vector();
          VVehIssue   = new Vector();
          VUnit       = new Vector();
          try
          {
               Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
               Connection     theConnection  = DriverManager.getConnection(SDSN,"","");
                              theConnection  .  setAutoCommit(true);
               Statement      theStatement   = theConnection.createStatement();

               ResultSet  result         = theStatement.executeQuery(getQString());
               while(result.next())
               {
                    VDate       .addElement(result.getString(1));
                    VGenIssue   .addElement(result.getString(2));
                    VVehIssue   .addElement(result.getString(3));
                    VUnit       .addElement(result.getString(4));
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);  
          }
     }

     private void insertElectricalData()
     {
          String    SIssueDateS    = "";
          String    SIssueDate     = "";
          String    SCreationDate  = common.getServerDate();
          String    SIssueNo       = "";
          double    dRate          = getRate();
          int       iSlNo          = 1;

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
                              theConnection  .  setAutoCommit(true);
               Statement      stat           =  theConnection.createStatement();

               stat.execute("Delete From Issue Where DKPost=9 And Code='A11017100' and IssueDate >='"+TStDate.toNormal()+"' And IssueDate <='"+TEnDate.toNormal()+"'");

               for(int i=0;i<VUnit.size();i++)
               {
                    SIssueDate          = (String)VDate.elementAt(i);

                    double dTotFuel     = (common.toDouble((String)VGenIssue.elementAt(i))) + (common.toDouble((String)VVehIssue.elementAt(i)));
                    double dIssVal      = dTotFuel*dRate;


                    if(!(SIssueDateS.trim()).equals(SIssueDate.trim()))
                    {
                         SIssueDateS    =    SIssueDate.trim();
                         iSlNo          =    1;

                         if(iMillCode==1)
                              SIssueNo  = String.valueOf(common.toInt(getIssueNo("Select maxno From Config where name='DyeingIssueNo'"))+1);
                         else
                              SIssueNo  = String.valueOf(common.toInt(getIssueNo("Select maxno From Config where name='IssueNo'"))+1);

                         UpdatedIssueNo();
                    }

                    String    QS = "Insert Into Issue(id,IssueNo,IssueDate,Code,Qty,IssRate,IssueValue,HodCode,Unit_Code,Dept_Code,Group_Code,CreationDate,UserCode,SlNO,IndentType,millcode,Serv_Code,DKPost) Values(";
                              QS = QS+"Issue_Seq.nextVal,";
                              QS = QS+SIssueNo+",";
                              QS = QS+"'"+(String)VDate.elementAt(i)+"',";
                              QS = QS+"'A11017100',";
                              QS = QS+common.getRound(dTotFuel,3)+",";
                              QS = QS+common.getRound(dRate,4)+",";
                              QS = QS+common.getRound(dTotFuel*dRate,2)+",";
                              QS = QS+"4,";
                              QS = QS+"0"+(String)VUnit.elementAt(i)+",";
                              QS = QS+"10,";
                              QS = QS+"3,";
                              QS = QS+"'"+SCreationDate+"',";
                              QS = QS+String.valueOf(iUserCode)+",";
                              QS = QS+String.valueOf(iSlNo)+",";
                              QS = QS+"0,";
                              QS = QS+String.valueOf(iMillCode)+",";
                              QS = QS+"1,";
                              QS = QS+"9)";
                    iSlNo++;
                    stat.execute(QS);
                    updateInvitemsData(dTotFuel,dIssVal);
               }
               stat .close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);  
          }
     }
     
     private void updateIssueValuation()
     {
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
                              theConnection  .  setAutoCommit(true);
               Statement      stat           =  theConnection.createStatement();
               
               stat.execute("Update Issue set ValuationStatus = 1 Where Code='A11017100' and (MillCode=0 or MillCode is Null) ");
               stat .close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);  
          }
     }
     
     private String getQString()
     {
          String QS =    " Select FuelDate,Consumption,Vehcons,Unit_Code "+
                         " From FuelLedger "+
                         " Where FuelDate >= '"+TStDate.toNormal()+"' And FuelDate <= '"+TEnDate.toNormal()+"' And (Consumption > 0 or vehcons>0)";
          return QS;
     }

     private double getRate()
     {
          SMatCode            = SMatCode.trim();
          
          Item IC             = new Item(SMatCode,iMillCode);

          double    dStock    = common.toDouble(IC.getClStock());
          double    dValue    = common.toDouble(IC.getClValue());
          double    dRate1    = 0;

          try
          {
               dRate1 = dValue/dStock;
          }
          catch(Exception ex)
          {
               dRate1=0;
          }
          return dRate1;
     }

     public void UpdatedIssueNo()
     {
          String SUString = "";

          Statement stat      = null;
          ResultSet result    = null;

          int iId=0;
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();
                              theConnection  .  setAutoCommit(true);
                              stat           =  theConnection.createStatement();

               if(iMillCode==1)
                    SUString  =   " Update config set MaxNo = MaxNo+1 where Name ='DyeingIssueNo'";
               else
                    SUString  =   " Update config set MaxNo = MaxNo+1 where Name ='IssueNo'";

               stat .executeUpdate(SUString);
               stat .close();
          }
          catch(Exception e)
          {
               System.out.println("FuelIssueFrame UpdatedIssueNo() ->"+e);
               e.printStackTrace();
          }
     }

     public String getIssueNo(String QueryString)
     {
          String ID = "";
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();   
                              theConnection  .  setAutoCommit(true);
               Statement      theStatement   = theConnection.createStatement();
               ResultSet      theResult      = theStatement.executeQuery(QueryString);

               while(theResult.next())
               {
                    ID = theResult.getString(1);
               }
               theResult      .close();
               theStatement   .close();
          }
          catch(SQLException SQLE)
          {
               System.out.println(SQLE);
               SQLE.printStackTrace();
          }
          return ID;
     }

     private void updateInvitemsData(double dIssueQty,double dIssueVal)
     {
          SMatCode            = SMatCode.trim();
          String  QString     = "";
          Statement      stat = null;

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();   
                              theConnection  .  setAutoCommit(true);
                              stat           = theConnection.createStatement();

               QString = " Update InvItems";
               QString = QString+" Set IssQty=IssQty+"+dIssueQty+",IssVal=IssVal+"+common.getRound(dIssueVal,2)+" ";
               QString = QString+" Where Item_Code = '"+SMatCode+"'";

               stat . executeUpdate(QString);
               stat . close();
          }
          catch(Exception e)
          {
               System.out.println("FuelIssue Frame updateInvitemsData() ->"+e);
               e.printStackTrace();
          }
     }
}
