package Issue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class MonAbstractFrame extends JInternalFrame
{
     protected      JLayeredPane DeskTop;
     protected      int iMillCode;
     Common         common = new Common();

     FileWriter     FW;
     JButton        BApply,BPrint;
     JTextArea      TA;
     JPanel         TopPanel,MiddlePanel,BottomPanel;
     JTextField     TFileName;

     Vector         VVarClass;

     String         SStDate,SEnDate;
     String         SMillName;

     String         SFileName = "Month.prn",SLine;

     int            iLctr = 100,iPctr   = 0;

     public MonAbstractFrame(JLayeredPane DeskTop,int iMillCode,String SStDate,String SEnDate,String SMillName)
     {
          super("Monthwise Issues - Departmentwise Abstract");
          this.DeskTop   = DeskTop;
          this.iMillCode = iMillCode;
          this.SStDate   = SStDate;
          this.SEnDate   = SEnDate;
          this.SMillName = SMillName;

          try
          {
               createComponents();
               setLayouts();
               addComponents();
               addListeners();
          }catch(Exception ex){System.out.println(ex);}
     }

     public void createComponents()
     {
          TopPanel       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BottomPanel    = new JPanel(true);

          BApply         = new JButton("Apply");
          BPrint         = new JButton("Print");

          TFileName      = new JTextField(10);
          TFileName      . setText(SFileName);

          TA             = new JTextArea();
     }
     
     public void setLayouts()
     {
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new GridLayout(1,3));
          
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,700,450);
     }

     public void addComponents()
     {
          MiddlePanel.add("Center",TA);
          
          BottomPanel.add(new JLabel("File Name"));
          BottomPanel.add(TFileName);
          BottomPanel.add(BPrint);
          
          iLctr     = 100;
          iPctr     = 0;
          setDataIntoVector();
          prtVarAbs();
          
          MiddlePanel.setLayout(new BorderLayout());
          MiddlePanel.add("Center",new JScrollPane(TA));
          
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BPrint.addActionListener(new PrintList());
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    SFileName = TFileName.getText();

                    if((SFileName.trim()).length()==0)
                       SFileName = "1.prn";

                    FW        = new FileWriter(common.getPrintPath()+SFileName);
                    FW        . write(TA.getText());
                    FW        . close();
               }catch(Exception ex){}
          }
     }

     public void setDataIntoVector()
     {
          VVarClass = new Vector();
     
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
     
               ResultSet res2  = stat.executeQuery(getQS());
               while (res2.next())
               {
                    organizeVarMonClass(res2);
               }
               res2.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private String getQS()
     {
          String QS = "";
          
          QS = QS + " SELECT Dept.Dept_Name, Dept.Dept_code,Issue.IssueDate, "+
                    " Issue.IssueValue AS IssVal "+
                    " FROM (Issue INNER JOIN InvItems ON Issue.Code = InvItems.ITEM_CODE) "+
                    " INNER JOIN Dept ON Issue.DEPT_CODE = Dept.Dept_Code "+
                    " Where Issue.MillCode = "+iMillCode+
                    " and Issue.IssueDate >= '"+SStDate+"' and Issue.IssueDate <= '"+SEnDate+"' "+
                    " UNION ALL"+
                    " SELECT Dept.Dept_Name, Dept.Dept_code,GRN.GRNDate,"+
                    " Grn.GrnValue AS IssVal "+
                    " FROM (GRN INNER JOIN InvItems ON GRN.Code = InvItems.ITEM_CODE) "+
                    " INNER JOIN Dept ON GRN.DEPT_CODE = Dept.Dept_Code "+
                    " WHERE GrnType<>2 and GrnBlock>1 And Grn.MillCode = "+iMillCode+
                    " and Grn.GrnDate >= '"+SStDate+"' and Grn.GrnDate <= '"+SEnDate+"' ";

          return QS;
     }

     private void organizeVarMonClass(ResultSet res2) throws Exception
     {
          String SVariety     = res2.getString(1);
          String SCode        = res2.getString(2);
          String SIssDate     = res2.getString(3);
          double dIssWt       = 0;
          double dIssValue    = res2.getDouble(4);
          
          int iIndex = indexOf(SCode);
          if(iIndex == -1)
          {
               VarMonClass VMC     = new VarMonClass(SCode,SVariety);
               VMC                 . append(dIssWt,SIssDate,dIssValue);
               VVarClass           . addElement(VMC);
          }
          else
          {
               VarMonClass VMC     = (VarMonClass)VVarClass.elementAt(iIndex);
               VMC                 . append(dIssWt,SIssDate,dIssValue);
               VVarClass           . setElementAt(VMC,iIndex);
          }
     }

     private int indexOf(String SCode)
     {
          int iIndex = -1;
          for(int i=0;i<VVarClass.size();i++)
          {
               VarMonClass VMC = (VarMonClass)VVarClass.elementAt(i);
               if(VMC.SCode.equals(SCode))
               {
                    iIndex = i;
                    break;
               } 
          }
          return iIndex;
     }

     private void prtPackHead()
     {
          if(iLctr<60)
          {
               return;
          }
          if(iPctr>0)
          {
               prtStrl(SLine+"");
          }
          iPctr++;
          prtStrl(SMillName);
          prtStrl("Consumption Monthwise Departmentwise ");
          prtStrl("Between "+common.parseDate(SStDate)+" - "+common.parseDate(SEnDate));
          prtStrl("Page "+iPctr);
          prtStrl(" ");
          
          String Str1 = common.Pad("Material Name",40)+common.Space(2);
          for(int i=4;i<=12;i++)
          {
               Str1 = Str1+common.Rad(common.getMonth(i),10)+common.Space(2);
          }
          for(int i=1;i<=3;i++)
          {
               Str1 = Str1+common.Rad(common.getMonth(i),10)+common.Space(2);
          }
          Str1      = Str1+common.Rad("Total",10);

          SLine     = common.Replicate("-",Str1.length());

          prtStrl(SLine);
          prtStrl(Str1);
          prtStrl(SLine);
          iLctr=8;
     }

     private void prtStrl(String Strl)
     {
          try
          {
               TA.append(Strl+"\n");
          }
          catch(Exception ex){System.out.println("IssueLed GSK99 : "+ex);}
     }

     private void prtVarAbs()
     {
          double dTot[] = new double[14];
          
          for(int i=0;i<14;i++)
               dTot[i]=0;
          
          prtPackHead();
          for(int j=0;j<VVarClass.size();j++)
          {
               VarMonClass VMC = (VarMonClass)VVarClass.elementAt(j);
               
               String Strl = common.Pad(VMC.SVariety,40)+common.Space(2);
               for(int i=4;i<=12;i++)
               {
                    double    dValue    = common.toDouble(VMC.getMonSalValue(i));
                              Strl      = Strl +common.Rad(common.getRound(dValue,2),10)+common.Space(2);
                              dTot[i]  += dValue;
               }
               for(int i=1;i<=3;i++)
               {
                    double    dValue    = common.toDouble(VMC.getMonSalValue(i));
                              Strl      = Strl +common.Rad(common.getRound(dValue,2),10)+common.Space(2);
                              dTot[i]  += dValue;
               }
               double    dValue    = common.toDouble(VMC.getTotSalVal());
                         Strl      = Strl +common.Rad(common.getRound(dValue,2),10);
                         dTot[13] += dValue;
               
               prtPackHead();
               prtStrl(Strl);
               iLctr++;
          }
          prtStrl(SLine);
          String Strl1 = common.Pad("Total ",40)+common.Space(2);
          for(int i=4;i<=12;i++)
               Strl1 += common.Rad(common.getRound(dTot[i],2),10)+common.Space(2);
          for(int i=1;i<=3;i++)
               Strl1 += common.Rad(common.getRound(dTot[i],2),10)+common.Space(2);
          Strl1 += common.Rad(common.getRound(dTot[13],2),10);
          prtStrl(Strl1);
          prtStrl(SLine);
          prtStrl(" ");
          prtStrl("<End Of Report>");
     }
}
