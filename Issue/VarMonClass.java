package Issue;

import java.util.*;
import java.sql.*;
import javax.swing.*;

import util.*;

//used by MonIssueFrame.java
public class VarMonClass
{
     String SVariety,SCode;
     Vector VIssWt,VIssDate,VIssValue;
     
     Common common = new Common();
     
     VarMonClass()
     {
     
     }

     VarMonClass(String SCode,String SVariety)
     {
          this.SCode    = SCode;
          this.SVariety = SVariety;
          
          VIssWt      = new Vector();
          VIssDate    = new Vector();
          VIssValue   = new Vector();
     }

     public void append(double dIssWt,String SIssDate,double dIssValue)
     {
          VIssWt      . addElement(common.getRound(dIssWt,2));
          VIssDate    . addElement(SIssDate);
          VIssValue   . addElement(common.getRound(dIssValue,2));
     }

     public String getMonSalWt(int iMonth)
     {
          double dWt=0;
          for(int i=0;i<VIssDate.size();i++)
          {
               String SDate = (String)VIssDate.elementAt(i);
               int    iMon  = common.toInt(SDate.substring(4,6));
               if(iMon==iMonth)
                    dWt += common.toDouble((String)VIssWt.elementAt(i));
          }
          return common.getRound(dWt,2);
     }

     public String getMonSalValue(int iMonth)
     {
          double dVal=0;
          for(int i=0;i<VIssDate.size();i++)
          {
               String SDate = (String)VIssDate.elementAt(i);
               int    iMon  = common.toInt(SDate.substring(4,6));
               if(iMon==iMonth)
                    dVal += common.toDouble((String)VIssValue.elementAt(i));
          }
          return common.getRound(dVal,2);
     }

     public String getTotSalWt()
     {
          double dWt = 0;
     
          for(int i=1;i<=12;i++)
          {
               dWt += common.toDouble(getMonSalWt(i));     
          }
          return common.getRound(dWt,2);
     }

     public String getTotSalVal()
     {
          double dVal = 0;
          
          for(int i=1;i<=12;i++)
          {
               dVal += common.toDouble(getMonSalValue(i));     
          }
          return common.getRound(dVal,2);
     }
}
