
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.util.*;
import java.io.*;

import util.*;
import jdbc.*;

import java.sql.*;
import guiutil.*;
import jdbc.*;

public class ItemStockDifferenceFrame extends JDialog
{

     protected    JLayeredPane Layer;
     JPanel       TopPanel,MiddlePanel,BottomPanel;

     TabReport    tabreport;


     String ColumnName[] = {"ItemCode","ItemName","Kardex Stock",
                            "Ledger Stock" ,"Stock Type","Negative Stock","Difference Qty"};

     String ColumnKey[]  = {"ITEMCODE","ITEM_NAME","KARDEXSTOCK",
                            "LEDGERSTOCK","STOCKTYPE","NEGATIVESTOCK","DIFFERENCEQTY"};

     int iColWidth[]     = {100,220,100,
                            100,135,100,100 };

     String ColumnType[] = {"N","S","S",
                            "S","S","S","S" };



							
     String    SFinYear,SStDate,SEnDate,SUserName="";
     Vector    theDataVect;
     int       iUserCode,iAuthCode,iMillCode;
     String    SMillName= "",SItemTable="",SSupTable="",SYearCode="";
     
     Common common = new Common();
     JButton        BOkay;
     Object[][]     RowData =null;

     Inventory inv = null;
	 
	 String SMillCode="";
	 
     public ItemStockDifferenceFrame(int iUserCode,int iAuthCode,int iMillCode,String SFinYear,String SStDate,String SEnDate,String SYearCode,String SItemTable,String SSupTable,String SMillName)
     {
          this.iUserCode = iUserCode;
          this.iAuthCode = iAuthCode;
          this.iMillCode = iMillCode;
		  this.SFinYear  = SFinYear;
          this.SStDate   = SStDate;
          this.SEnDate   = SEnDate;
          this.SYearCode = SYearCode;

          this.SItemTable= SItemTable;
          this.SSupTable = SSupTable;
          this.SMillName = SMillName;
  
  
			SMillCode = String.valueOf(iMillCode);
         

          try
          {
               createComponents	();
               setLayouts		();
               addComponents	();
               addListener		();
               //setOwnerCode		();
               showList			();
               //setUserName		();
			   
               if(tabreport.ReportTable.getRowCount()==0)
               {
                    setVisible(false);

                    inv = new  Inventory(iUserCode,iAuthCode,iMillCode,SFinYear,SStDate,SEnDate,SYearCode,SItemTable,SSupTable,SMillName);
               }

          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void createComponents()
     {
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();
          BOkay          = new JButton("Okay");
          //BTransfer      = new JButton(" Transfer to CommonStock");
          //BPrint         = new JButton(" Print");
     }

     private void setLayouts()
     {
         setTitle("Item Stock Problem List At Stores");

         setBounds(100,100,880,500); 

         MiddlePanel    .setBorder(new TitledBorder(""));
         MiddlePanel    .setLayout(new BorderLayout());
		 
     }

     private void addComponents()
     {
          BottomPanel.add(BOkay);
          //BottomPanel.add(BTransfer);
          //BottomPanel.add(BPrint);


          add(MiddlePanel, BorderLayout.CENTER);
          add(BottomPanel, BorderLayout.PAGE_END);

     }
     private void addListener()
     {
          BOkay.addActionListener(new ActList());
          //BTransfer.addActionListener(new ActList());
          //BPrint . addActionListener(new ActList());
     }

     public void showList()
     {
          theDataVect = new Vector();
          try
          {
               try
               {
                    MiddlePanel.updateUI();
                    this.remove(MiddlePanel);
               }
               catch(Exception e)
               {
                    System.out.println(e);
               }

               MiddlePanel    = new JPanel();


               MiddlePanel    .setBorder(new TitledBorder(""));
               MiddlePanel    .setLayout(new BorderLayout());

               Vector vect = getItemPendingList();
               theDataVect = vect;

               tabreport   = new TabReport(getRowData(vect),ColumnName,ColumnType);

               tabreport           .setPrefferedColumnWidth(iColWidth);
               MiddlePanel         .add(tabreport);

               getContentPane()    .add("Center",MiddlePanel);
			   
			   MiddlePanel.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }


     private Object[][] getRowData(Vector vect)
     {
          try
          {
               RowData = new Object[vect.size()][ColumnName.length];

               for(int i=0;i<vect.size();i++)
               {
                    HashMap row = (HashMap)vect.elementAt(i);

                    for(int j=0;j<ColumnKey.length;j++)
                    {
                        RowData[i][j] = common.parseNull((String)row.get(ColumnKey[j]));
                    }
                    //RowData[i][4] ="";
                    
               }
               return RowData;

          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return null;
     }
     private  Vector getItemPendingList()
     {
          Vector theVector = new Vector();
		  

          try
          { 

               ORAConnection jdbc         = ORAConnection.getORAConnection();
               Connection theConnection   = jdbc.getConnection();
               

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(getItemListQuery());
               ResultSetMetaData rsmd    = result.getMetaData();
               while(result.next())
               {
                    HashMap row = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }
                    theVector.addElement(row);
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return theVector;
     }
     private String getItemListQuery()
     {
          String SDate 	   = common.getCurrentDate();
		  StringBuffer sb = new StringBuffer();
		  
		  System.out.println("Mill code:"+SMillCode);

//-- start normal stock

			sb.append(" select  t.code as ITemCode,invitems.item_name as ITem_Name, t.itemstock as KardexStock,t.invstock as LedgerStock,");
			sb.append(" 'PURCHASE ITEM' as Stocktype,0 as negativestock , 0 as DifferenceQty from (  ");

			sb.append(" select sum(stock) as invstock,sum(itemstock) as itemstock,code from ( ");

			sb.append(" select sum(receipt)-sum(issue) as stock,0 as itemstock,code from ( ");

			sb.append(" select sum(nvl(OpgQty,0))  + sum(nvl(GrnQty1,0))   as receipt   ,sum(nvl(ISSQTY,0))+sum(nvl(GrnQty2,0)) as issue,code from ( ");


			sb.append(" SELECT c1.GroupName, c1.code, c1.Item_Name,  ");
			sb.append(" c1.UOMName, c1.OpgQty as OpgQty , c1.OpgVal ");
			sb.append(" ,  c2.GrnQty as GrnQty1, c2.GrnVal, c3.IssQty,  ");
			sb.append(" c3.IssVal, c2a.GrnQty as GrnQty2, c2a.GrnVal,  ");
			sb.append(" c1.LocName, c1.StkGroupCode,c1.ExClass ");
			sb.append(" From ( ");

			/*sb.append("  SELECT InvItems.Item_Code as Code, InvItems.Item_Name,  Sum(nvl(InvItems.OpgQty,0)) as OpgQty,  ");
			sb.append("  Sum(nvl(InvItems.OpgVal,0)) as OpgVal,  Uom.UomName, StockGroup.GroupName,InvItems.LocName,   ");
			sb.append("  InvItems.StkGroupCode,InvItems.ExClass  FROM ((InvItems  ");
			sb.append("  Inner Join StockGroup on  InvItems.StkGroupCode=StockGroup.GroupCode and InvItems.hsntype=0  )  ");
			sb.append("  Inner Join Uom on InvItems.UomCode=Uom.UomCode )   ");
			sb.append("  Group by InvItems.Item_Code, InvItems.Item_Name,   ");
			sb.append("  Uom.UomName, StockGroup.GroupName,  invitems.locname, ");
			sb.append("  InvItems.StkGroupCode,InvItems.ExClass  ");*/
			
			
            sb.append("   SELECT InvItems.Item_Code as Code, InvItems.Item_Name,  ");

			if(SMillCode.equals("99")){
				  sb.append("  Sum(nvl(InvItems.OpgQty,0))+Sum(nvl(DyeingInvitems.OpgQty,0)) as OpgQty, Sum(nvl(InvItems.OpgVal,0))+sum(nvl(DyeingInvItems.OpgVal,0)) as OpgVal, ");
			}	  
		    else
		    {
			    if(SMillCode.equals("0"))
				   sb.append(" Sum(nvl(InvItems.OpgQty,0)) as OpgQty, Sum(nvl(InvItems.OpgVal,0)) as OpgVal,  ");
			    else
				   sb.append(" Sum(nvl("+SItemTable+".OpgQty,0)) as OpgQty, Sum(nvl("+SItemTable+".OpgVal,0)) as OpgVal,  ");
			}

		    if((SMillCode.equals("99"))||(SMillCode.equals("0")))
		    {
				 sb.append(" Uom.UomName, StockGroup.GroupName,InvItems.LocName,  ");
                 sb.append(" InvItems.StkGroupCode,InvItems.ExClass  ");
		    }
		    else
		    {
				sb.append("  Uom.UomName, StockGroup.GroupName,"+SItemTable+".LocName, ");
                sb.append("  "+SItemTable+".StkGroupCode,InvItems.ExClass  ");
		    }
			if((SMillCode.equals("99"))||(SMillCode.equals("0")))
			{     
				sb.append("  FROM ((InvItems Inner Join StockGroup on  ");
                            sb.append(     " InvItems.StkGroupCode=StockGroup.GroupCode and InvItems.hsntype=0  )  ");
                            sb.append(     " Inner Join Uom on InvItems.UomCode=Uom.UomCode )  ");
			}
			else
			{
				 sb.append("  FROM (("+SItemTable+" Inner Join StockGroup on ");
                 sb.append("               "+SItemTable+".StkGroupCode=StockGroup.GroupCode ) ");
			}     
		
			int iIndicator=0;
			
			if(SMillCode.equals("99"))
			{
				 iIndicator=1;
				 sb.append("  left join DyeingInvItems on DyeingInvItems.Item_Code = InvItems.Item_Code  ");
			}
			if((!SMillCode.equals("0"))&&(iIndicator==0)) {
				
				sb.append("  inner join InvItems on "+SItemTable+".Item_Code = InvItems.Item_Code  and invitems.hsntype=0 )  ");
				sb.append("  inner join Uom on InvItems.UomCode=Uom.UomCode  ");
				
			}	

				//QS1  = QS1 +" "+SFilter+"";

				if((SMillCode.equals("99"))||(SMillCode.equals("0")))
				{
					sb.append(" Group by InvItems.Item_Code, InvItems.Item_Name,  ");
					sb.append(" Uom.UomName, StockGroup.GroupName,  ");
					sb.append(" invitems.locname,InvItems.StkGroupCode,InvItems.ExClass  ");
				}
				else
				{
				    sb.append(" Group by InvItems.Item_Code, InvItems.Item_Name, ");
					sb.append(" Uom.UomName, StockGroup.GroupName,  ");
					sb.append(" "+SItemTable+".locname,"+SItemTable+".StkGroupCode,InvItems.ExClass  ");
				}
				
			

			sb.append("  ) c1  ");
			sb.append("  Left JOIN ( ");

			sb.append("  Select Code,sum(GrnQty) as GrnQty,sum(GrnVal) as GrnVal from  ");
			sb.append(" ( ");
			sb.append(" SELECT GRN.Code, Sum(GRN.GrnQty) AS GrnQty,  ");
			sb.append(" Sum(GRN.GrnValue) as GrnVal  ");
			sb.append(" FROM GRN  ");
			
			if(SMillCode.equals("0")) {
			
				sb.append(" WHERE Grn.RejFlag=0 and GRN.GrnDate <="+common.getServerPureDate()+"  And (Grn.MillCode is Null OR GRN.MillCode = 0)  ");			
				
			}else {
				
				sb.append(" WHERE Grn.RejFlag=0 and GRN.GrnDate <="+common.getServerPureDate()+"   And  grn.MillCode = "+common.toInt(SMillCode)+" ");				
				
			}
			
			
			sb.append(" GROUP BY GRN.Code  ");
			sb.append(" Union All  ");
			sb.append(" SELECT GRN.Code, Sum(GRN.GrnQty) AS GrnQty,  ");
			sb.append(" Sum(GRN.GrnValue) as GrnVal  ");
			sb.append(" FROM GRN  ");
			
			if(SMillCode.equals("0")) {
			
			sb.append(" WHERE Grn.RejFlag=1 and GRN.RejDate  <="+common.getServerPureDate()+"  And (Grn.MillCode is Null OR GRN.MillCode = 0)  ");
			
			}else {
				
			sb.append(" WHERE Grn.RejFlag=1 and GRN.RejDate  <="+common.getServerPureDate()+"   And  grn.MillCode = "+common.toInt(SMillCode)+"  ");
			}
			sb.append(" GROUP BY GRN.Code ");
			sb.append(" )  ");
			sb.append(" Group by Code  ");

			sb.append("  )  ");
			sb.append(" c2 ON c1.code = c2.Code  ");

			sb.append("  Left JOIN ( ");

			sb.append(" Select Code,sum(GrnQty) as GrnQty,sum(GrnVal) as GrnVal from  ");
			sb.append(" (");
			sb.append(" SELECT GRN.Code, Sum(GRN.GrnQty) AS GrnQty, Sum(GRN.GrnValue) as GrnVal  ");
			sb.append(" FROM GRN  ");
			
			if(SMillCode.equals("0")) {

			sb.append(" WHERE Grn.RejFlag=0 and (GrnType<>2 and GrnBlock>1) And (Grn.MillCode is Null OR GRN.MillCode = 0)  ");
			
			}else {
				
			sb.append(" WHERE Grn.RejFlag=0 and (GrnType<>2 and GrnBlock>1) and grn.MillCode = "+common.toInt(SMillCode)+"    ");	
			}
			sb.append(" GROUP BY GRN.Code ");
			sb.append(" Union All  ");
			sb.append(" SELECT GRN.Code, Sum(GRN.GrnQty) AS GrnQty, Sum(GRN.GrnValue) as GrnVal  ");
			sb.append(" FROM GRN  ");
			
			if(SMillCode.equals("0")) {

			sb.append(" WHERE Grn.RejFlag=1 and (GrnType<>2 and GrnBlock>1) And (Grn.MillCode is Null OR GRN.MillCode = 0)  ");
			
			}else {
			sb.append(" WHERE Grn.RejFlag=1 and (GrnType<>2 and GrnBlock>1) and grn.MillCode = "+common.toInt(SMillCode)+"    ");
				
			}

			sb.append(" GROUP BY GRN.Code ");
			sb.append(" )  ");
			sb.append(" Group by Code   ");
			sb.append(" ) c2a ON c1.code = c2a.Code  ");

			sb.append(" Left JOIN ( ");

			sb.append(" SELECT Issue.Code, Sum(Issue.Qty) AS IssQty,  ");
			sb.append(" Sum(Issue.IssueValue) as IssVal  ");
			sb.append(" FROM Issue  ");
			sb.append(" WHERE Issue.IssueDate <="+common.getServerPureDate()+" ");
			
			if(SMillCode.equals("0")) {
				
				sb.append(" And (Issue.MillCode is Null OR Issue.MillCode = 0) ");
				
			}else {
				
				sb.append(" And Issue.MillCode = "+common.toInt(SMillCode)+"   ");
			}
			
			
			
			
			sb.append(" GROUP BY Issue.Code  ");

			sb.append(" ) c3 ON c1.code = c3.Code  ");

			sb.append(" ) group by code  ");
			sb.append(" ) ");
			sb.append(" group by code  ");
			sb.append(" having  sum(receipt)-sum(issue)>0 ");

			sb.append(" union all ");
			sb.append(" select 	0 as stock,sum(stock), itemcode from  ");
			sb.append(" itemstock where  stock>0 and hodcode not in(10371,6125) and millcode="+common.toInt(SMillCode)+"   ");
			sb.append(" group by itemcode ");

			sb.append(" ) ");
			sb.append(" group by code ");
			sb.append(" having sum(stock)<>sum(itemstock) ");

			sb.append(" )t ");
			sb.append(" inner join invitems on invitems.item_code = t.code ");


			sb.append(" union all ");

			//-- start service stock

			sb.append(" select t.itemcode,invitems.item_name, itemstock,t.transstock,'SERVICE STOCK',0 as negativestock,0 as DifferenceQty from ( ");
			sb.append(" select itemcode,sum(transstock) as transstock,sum(itemstock) as itemstock from ( ");

			sb.append(" select 	 itemcode,sum(stock) as transstock ,0 as itemstock from  ");
			sb.append(" servicestocktransfer where  to_Char(servicestocktransfer.TRANSFERSTATUSDATE,'yyyymmdd') >=20190330 ");
			sb.append(" and TRANSFERSTATUS=1  ");
			sb.append(" and millcode="+common.toInt(SMillCode)+"   ");
			sb.append(" group by itemcode ");
/*			sb.append(" union all ");
			sb.append(" select 	 itemcode,sum(receipt_Qty) as transstock ,0 as itemstock from  ");
			sb.append(" rdc_itemstock where  to_Char(rdc_itemstock.entrydate,'yyyymmdd') >=20190330 ");
			sb.append(" and millcode="+common.toInt(SMillCode)+"   ");
			sb.append(" group by itemcode ");
			sb.append(" union all ");
			sb.append(" select 	 itemcode,sum(stocktransferqty) as transstock ,0 as itemstock from  ");
			sb.append(" IndentMaterial_InternalService where  to_Char(IndentMaterial_InternalService.entrydatetime,'yyyymmdd') >=20190330 ");
			sb.append(" and millcode="+common.toInt(SMillCode)+"   ");
			sb.append(" group by itemcode ");*/

			sb.append(" union all ");
			sb.append(" select 	 itemcode,0 as stock,sum(stock) from  ");
			sb.append(" itemstock where  hodcode=10371 and millcode="+common.toInt(SMillCode)+"  ");
			sb.append(" group by itemcode ");
			sb.append(" union all ");
			//sb.append(" select 	 code,0 as stock,sum(qty) from  ");
			sb.append(" select 	 code,sum(qty)*-1 as stock,0  from  ");
			sb.append(" Servicestockissue where  issuedate>=20190330 and millcode="+common.toInt(SMillCode)+"   ");
			sb.append(" group by code ");

			sb.append(" ) group by itemcode  ");
			sb.append(" having sum(transstock)<>sum(itemstock)  ");
			sb.append(" )t ");
			sb.append(" inner join invitems on invitems.item_code = t.itemcode ");

			sb.append(" union all ");

			// start non stock

			sb.append(" select t.itemcode,item_name,t.itemstock,t.transstock,'NON STOCK',0 as negativestock,0 as DifferenceQty from  ");
			sb.append(" ( ");
			/*sb.append(" select itemcode,sum(transstock) as transstock,sum(itemstock) as itemstock from ( ");
			sb.append(" select 	 itemcode,sum(stock) as transstock ,0 as itemstock from  ");
			sb.append(" itemstock_transfer where  to_Char(itemstock_transfer.transferdate,'yyyymmdd') >=20190330 ");
			sb.append(" and TRANFERSTATUS=1  and millcode="+common.toInt(SMillCode)+"   ");
			sb.append(" group by itemcode ");
			sb.append(" union all ");
			sb.append(" select 	 itemcode,0 as stock,sum(stock) from  ");
			sb.append(" itemstock where  hodcode=6125 and  millcode="+common.toInt(SMillCode)+" ");
			sb.append(" group by itemcode ");
			sb.append(" union all ");
			sb.append(" select 	 code,0 as stock,sum(qty) from  ");
			sb.append(" nonstockissue where  issuedate>=20190330 and  millcode="+common.toInt(SMillCode)+" ");
			sb.append(" group by code ");
			sb.append(" ) group by itemcode  ");
			sb.append(" having sum(transstock)<>sum(itemstock)  ");
			sb.append(" )t ");*/
			
			sb.append(" select code as itemcode ,sum(stock) as transstock,sum(itemstock) as itemstock from ( ");
			sb.append(" Select Code,Sum(Stock+IssQty)-sum(REcQty) as Stock,0 as Itemstock from  ");
			sb.append(" (  ");
			sb.append(" Select ItemCode as Code,Sum(Stock) as Stock,0 as IssQty,0 as RecQty   ");
			sb.append(" From ItemStock   ");
			sb.append(" Where stock>0 and HodCode=6125  and MillCode="+common.toInt(SMillCode)+"  ");
			sb.append(" Group by ItemCode   ");
			sb.append(" Union All   ");
			sb.append(" Select Code,0 as Stock,Sum(Qty) as IssQty  ,0 as REcQty           ");
			sb.append(" From NonStockIssue   ");
			sb.append(" Where MillCode="+common.toInt(SMillCode)+"  and IssueDate>"+common.getServerPureDate()+" ");
			sb.append(" Group by Code			 			   ");
			sb.append(" Union All   ");
			sb.append(" Select ItemCode,0 ,0,sum(stock)  ");
			sb.append(" From ItemStock_transfer   ");
			sb.append(" Where MillCode="+common.toInt(SMillCode)+"  and  ");
			sb.append(" to_Char(itemstock_transfer.transferdate,'yyyymmdd') >"+common.getServerPureDate()+" ");
			sb.append(" Group by ITemCode			 			   ");
			sb.append(" )   ");
			sb.append(" Group by Code having Sum(Stock+IssQty)-sum(REcQty)>0  ");

			sb.append(" union all  ");

			sb.append(" select 	 itemcode,0 as stock,sum(stock) from   ");
			sb.append(" itemstock where stock>0 and hodcode=6125 and  millcode="+common.toInt(SMillCode)+"  ");
			sb.append(" group by itemcode  ");
			sb.append(" ) ");
			sb.append(" group by code ");
			sb.append(" having sum(stock)<>sum(itemstock) )t ");
			
			
			sb.append(" inner join invitems on invitems.item_code = t.itemcode ");
			
			sb.append(" union all ");
			
			// start Negative stock
			
						
			sb.append("	select t.code,item_name,0 as itemstock,0 as transstock,'' as nonstock, sum(t.open)-sum(t.issueopen)  as negativestock, 0 as DifferenceQty from ( ");
			sb.append("	select sum(OpgGrnQty) as open,code,0 as issueopen from ( ");
			sb.append(" Select Sum(OpgGrnQty) as OpgGrnQty, sum(OpgGrnVal) as OpgGrnVal,Code from  ");
			sb.append("	( ");
			sb.append("	SELECT Sum(Grn.GrnQty) as OpgGrnQty, Sum(GRN.GrnValue) as OpgGrnVal ,Code ");
			sb.append("	FROM GRN  ");
			sb.append("	WHERE (Grn.GrnBlock<2 Or Grn.GrnType=2)  And GRN.GrnDate <"+common.getServerPureDate()+" and millcode=0 ");
			sb.append("	and Grn.RejFlag=0 "); 
			sb.append("	group by Code ");
			sb.append("	Union All  ");
			sb.append("	SELECT Sum(Grn.GrnQty) as OpgGrnQty, Sum(GRN.GrnValue) as OpgGrnVal,Code "); 
			sb.append("	FROM GRN "); 
			sb.append("	WHERE (Grn.GrnBlock<2 Or Grn.GrnType=2)  And GRN.RejDate <"+common.getServerPureDate()+" and millcode=0 ");
			sb.append("	and Grn.RejFlag=1  ");
			sb.append("	group by code ");
			sb.append("	)group by code ");
			sb.append("	)group by code ");
			sb.append("	union all ");
			sb.append("	select OPGQty,item_Code,0 as issue from ( ");
			sb.append("	Select Item_Name,UoMName,locname,OPGQty,OpgVal,ROQ+MinQty,MinQty, "); 
			sb.append(" MaxQty,Draw,Catl,StkGroupCode,item_code From InvItems ");
			sb.append(" inner join uom on uom.uomcode = invitems.uomcode "); 
			sb.append(" ) ");
			sb.append(" union all ");
			sb.append("	select 0,code,OpgIssQty from ( ");
			sb.append("	SELECT Sum(Issue.Qty) as OpgIssQty, Sum(Issue.IssueValue) as OpgIssVal ,code ");
			sb.append("	FROM Issue  ");
			sb.append("	WHERE Issue.IssueDate <"+SDate+" and millcode=0 ");
			sb.append("	group by code ");
			sb.append(" )  ");                              
			sb.append("	) ");
			sb.append("	t ");
			sb.append(" inner join invitems on invitems.item_code = t.code ");
			sb.append(" group by t.code ,item_name ");
			sb.append(" having sum(t.open)-sum(t.issueopen)<0 ");
			
			
			sb.append(" union all ");
			// start difference stock
			
			
sb.append(" select code,item_name,0 as kardex,0 as ledger, ");
sb.append(" '' as stocktype,0 as negativestock,itemindent-indentpending as difference from ( ");
sb.append(" select sum(indentqty) as itemindent,t.indentpending,t.code,t.item_name from ( ");
sb.append(" select sum(indent.qty) -sum(issueqty) as indentpending,code,item_name from  ");
sb.append(" indent ");
 sb.append(" inner join invitems on invitems.item_code = indent.code ");
 sb.append(" where  ");
 sb.append(" indent.issuestatus=0 and indent.millcode="+common.toInt(SMillCode)+" ");
 sb.append(" group by code,item_name ");
 sb.append(" ) t inner join itemstock on itemstock.itemcode =t.code and itemstock.indentqty>0 ");
 sb.append(" and itemstock.millcode="+common.toInt(SMillCode)+"  ");
 sb.append(" group by t.indentpending,t.code ,t.item_name ");
 sb.append(" having sum(indentqty)<>t.indentpending ");
 sb.append(" ) ");
			
			
			/*sb.append("	select itemstock.itemcode,item_name,0 as kardex,0 as ledger,");
			sb.append(" '' as stocktype,0 as negativestock,");
			sb.append(" itemstock.indentqty -(sum(indent.qty) -sum(issueqty)) as differenceqty");
			sb.append(" from itemstock");
			sb.append(" inner join indent on indent.code= itemstock.itemcode");
			sb.append(" and indent.issuestatus=0");
			sb.append(" inner join invitems on invitems.item_code = itemstock.itemcode");
			sb.append(" where itemstock.indentqty>0"); 
			sb.append(" group by itemstock.itemcode,itemstock.indentqty ,item_name");
			sb.append(" having itemstock.indentqty-(sum(indent.qty) -sum(issueqty))<>0");	  */

          System.out.println(sb.toString());

          return sb.toString();
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {

               if(ae.getSource()==BOkay)
               {
                  //  Inventory inv = new  Inventory(iUserCode,iAuthCode,iMillCode,SFinYear,SStDate,SEnDate,SMillName,SItemTable,SSupTable,SYearCode,iHodCode,iDeptCode,iGroupCode,iInsStatus,iUnitCode,iPunchStatus,0);

                   if(inv ==null)
                    inv = new  Inventory(iUserCode,iAuthCode,iMillCode,SFinYear,SStDate,SEnDate,SYearCode,SItemTable,SSupTable,SMillName);
                    setVisible(false);
               }
               /*if(ae.getSource()==BTransfer)
               {
                    SaveData();
               }
               if(ae.getSource()==BPrint)
               {
                 printData();
               }*/
          }

     }
     /*
     private void printData()
     {
         initPDF();
     }
     private void initPDF()
     {
       String SFile     = "d:/ItemStockList.pdf"  ;
       String STitle    = " Item Stock List At Stores" ;
       String SSubTitle = " User Name : "+SUserName+"   Date : "+common.parseDate(common.getCurrentDate());
       String SColumnHead[] = {"SNo","ItemCode","ItemName","Qty","Days Old" ,"Transfer Qty","Transfer"};
       int iColumnWidth[]   = {20,45,125,40,40,30,30 };
       PDFGeneration pdfGen = new PDFGeneration(SFile,STitle,SSubTitle,SColumnHead,iColumnWidth,theDataVect);
       JOptionPane.showMessageDialog(null,"PDF File Created in D:/ItemStockList.pdf");
       try
       {
         Process p = Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler  "+SFile);
         p.waitFor(); 
       }
       catch(Exception ex)
       {
         System.out.println(" initPDF "+ex);
       }

     }*/
}
                                                                                                                  
