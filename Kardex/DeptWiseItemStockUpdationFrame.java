package Kardex;

import util.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.Vector;
import java.sql.*;
import java.awt.event.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DeptWiseItemStockUpdationFrame extends JInternalFrame
{
     JPanel TopPanel,MiddlePanel,BottomPanel;
     String mac      = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
     String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

     Common common   = new Common();

     Vector VItemCode,VStockQty,VStockVal;

     Vector VSUserCode,VSItemCode,VSStockQty,VSStockVal;

     JTextArea     TInfo;

     JButton   BApply;
     JLayeredPane layer;
     int          iMillCode;
     String       SItemTable;

     Connection theConnection=null;

     public DeptWiseItemStockUpdationFrame(JLayeredPane layer,int iMillCode,String SItemTable)
     {
          super("Utility for Uploading Item Stock");
          this.layer      = layer;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;

          updateLookAndFeel();
          setSize(500,500);
          
          TopPanel    = new JPanel(true);
          MiddlePanel = new JPanel(true);
          MiddlePanel.setLayout(new BorderLayout());

          TInfo       = new JTextArea(50,50);
          BApply      = new JButton("Apply");

          TopPanel.add(BApply);
          MiddlePanel.add(TInfo);

          BApply.addActionListener(new AppList());

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          setVisible(true);
          setClosable(true);
     }
      private class AppList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {

               BApply.setEnabled(false);
               try
               {
                    MiddlePanel.removeAll();
               }catch(Exception ex){System.out.println(ex);}
               MiddlePanel.add(TInfo);
               TInfo.append("Processing ....."+"\n");

               TInfo.append("Fetching Stock Data ....."+"\n");
               getStockData();

               if(VItemCode.size()>0)
               {
                    TInfo.append("Fetching User Stock Data ....."+"\n");
                    getUserStockData();
     
                    TInfo.append("Updation is going on ....."+"\n");
                    updateData();
               }

               TInfo.append("Updation Over .....");
               removeFrame();
          }
      }

     private void getStockData()
     {
          VItemCode     = new Vector();
          VStockQty     = new Vector();
          VStockVal     = new Vector();


          String QS = " select item_code as itemcode,stock,round(stockvalue/stock,4) as stockvalue from ( "+
                      " select item_code,sum(opgqty+recqty-issqty) as Stock,sum(opgval+recval-issval) as StockValue from ( "+
                      " select "+SItemTable+".item_code,nvl(opgqty,0) as opgqty,nvl(t1.recqty,0) as recqty,nvl(t2.issqty,0) as issqty, "+
                      " nvl(opgval,0) as opgval,nvl(t1.recval,0) as recval,nvl(t2.issval,0) as issval from "+SItemTable+
                      " left join (select code,sum(grnqty) as recqty,sum(grnvalue) as recval from grn where millcode="+iMillCode+
                      " and grnblock<=1 group by code) t1 on "+SItemTable+".item_code=t1.code "+
                      " left join (select code,sum(qty) as issqty,sum(issuevalue) as issval from issue where millcode="+iMillCode+
                      " group by code) t2 on "+SItemTable+".item_code=t2.code) "+
                      " group by item_code order by item_code) where stock>0 ";

                      // A11027160,A11037900,A08054634,A06108209,A08000291

          try
          {

               if(theConnection==null)
               {
                    ORAConnection jdbc       = ORAConnection.getORAConnection();
                    theConnection            = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    VItemCode.addElement(common.parseNull((String)theResult.getString(1)));
                    VStockQty.addElement(common.parseNull((String)theResult.getString(2)));
                    VStockVal.addElement(common.parseNull((String)theResult.getString(3)));
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void getUserStockData()
     {
          VSUserCode = new Vector();
          VSItemCode = new Vector();
          VSStockQty = new Vector();
          VSStockVal = new Vector();

          try
          {

               for(int i=0;i<VItemCode.size();i++)
               {
                    String SItemCode = (String)VItemCode.elementAt(i);
                    String SRate     = (String)VStockVal.elementAt(i);
                    double dStock    = common.toDouble((String)VStockQty.elementAt(i));

                    getUserData(SItemCode,SRate,dStock);
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void getUserData(String SItemCode,String SRate,double dStock)
     {
          try
          {
               int iIndex=-1;

               if(theConnection==null)
               {
                    ORAConnection jdbc       = ORAConnection.getORAConnection();
                    theConnection            = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();

               String QS1 = " Select GrnNo,GrnDate,Dept_Code,sum(GrnQty) from Grn "+
                            " Where GrnBlock<2 and Grn.MillCode="+iMillCode+
                            " and Code='"+SItemCode+"' Group by GrnNo,GrnDate,Dept_Code,Id "+
                            " Order by GrnDate Desc,GrnNo Desc,Id Desc ";

               ResultSet theResult      = theStatement.executeQuery(QS1);
               while(theResult.next())
               {
                    
                    iIndex=-1;

                    int iDeptCode    = theResult.getInt(3);
                    String SUserCode = getUserCode(iDeptCode);
                    double dGrnQty   = theResult.getDouble(4);

                    iIndex = getIndexOf(SUserCode,SItemCode);

                    if(dGrnQty>=dStock)
                    {
                         if(iIndex>=0)
                         {
                              double dOldQty = common.toDouble((String)VSStockQty.elementAt(iIndex));

                              double dNewQty = dOldQty + dStock;

                              VSStockQty.setElementAt(common.getRound(dNewQty,3),iIndex);
                         }
                         else
                         {
                              VSUserCode.addElement(SUserCode);
                              VSItemCode.addElement(SItemCode);
                              VSStockQty.addElement(""+dStock);
                              VSStockVal.addElement(SRate);
                         }
                         dStock = dStock - dGrnQty;

                         break;
                    }
                    else
                    {
                         if(iIndex>=0)
                         {
                              double dOldQty = common.toDouble((String)VSStockQty.elementAt(iIndex));

                              double dNewQty = dOldQty + dGrnQty;

                              VSStockQty.setElementAt(common.getRound(dNewQty,3),iIndex);
                         }
                         else
                         {
                              VSUserCode.addElement(SUserCode);
                              VSItemCode.addElement(SItemCode);
                              VSStockQty.addElement(""+dGrnQty);
                              VSStockVal.addElement(SRate);
                         }
                         dStock = dStock - dGrnQty;
                    }
               }
               theResult.close();
               theStatement.close();

               if(dStock>0)
               {
                    iIndex=-1;
     
                    String SUserCode = "1";
     
                    iIndex = getIndexOf(SUserCode,SItemCode);
     
                    if(iIndex>=0)
                    {
                         double dOldQty = common.toDouble((String)VSStockQty.elementAt(iIndex));

                         double dNewQty = dOldQty + dStock;

                         VSStockQty.setElementAt(common.getRound(dNewQty,3),iIndex);
                    }
                    else
                    {
                         VSUserCode.addElement(SUserCode);
                         VSItemCode.addElement(SItemCode);
                         VSStockQty.addElement(""+dStock);
                         VSStockVal.addElement(SRate);
                    }
               }
          }
          catch(Exception e)
          {              
               e.printStackTrace();
          }
     }

     public String getUserCode(int iDeptCode)
     {
          String SUserCode = "1";

          if(iMillCode==0)
          {
               if(iDeptCode==0 || iDeptCode==9 || iDeptCode==30)
               {
                    SUserCode = "1";
               }

               if(iDeptCode==1 || iDeptCode==2 || iDeptCode==3 || iDeptCode==4 || iDeptCode==5 || iDeptCode==6 || iDeptCode==7 || iDeptCode==14 || iDeptCode==20)
               {
                    SUserCode = "1988";
               }

               if(iDeptCode==8)
               {
                    SUserCode = "1985";
               }

               if(iDeptCode==10 || iDeptCode==41)
               {
                    SUserCode = "1982";
               }

               if(iDeptCode==11)
               {
                    SUserCode = "5725";
               }

               if(iDeptCode==17)
               {
                    SUserCode = "825";
               }

               if(iDeptCode==18)
               {
                    SUserCode = "386";
               }

               if(iDeptCode==19)
               {
                    SUserCode = "5905";
               }

               if(iDeptCode==43)
               {
                    SUserCode = "505";
               }
          }

          if(iMillCode==1)
          {
               if(iDeptCode==0 || iDeptCode==9 || iDeptCode==31 || iDeptCode==40)
               {
                    SUserCode = "1";
               }

               if(iDeptCode==8 || iDeptCode==24 || iDeptCode==26 || iDeptCode==28)
               {
                    SUserCode = "3885";
               }

               if(iDeptCode==10)
               {
                    SUserCode = "2885";
               }

               if(iDeptCode==11)
               {
                    SUserCode = "5725";
               }

               if(iDeptCode==18)
               {
                    SUserCode = "386";
               }

               if(iDeptCode==19)
               {
                    SUserCode = "5905";
               }

               if(iDeptCode==29)
               {
                    SUserCode = "5906";
               }

               if(iDeptCode==7 || iDeptCode==32 || iDeptCode==42)
               {
                    SUserCode = "1207";
               }
          }

          if(iMillCode==3)
          {
               if(iDeptCode==0 || iDeptCode==9)
               {
                    SUserCode = "1";
               }

               if(iDeptCode==10)
               {
                    SUserCode = "1982";
               }

               if(iDeptCode==11)
               {
                    SUserCode = "5725";
               }
          }

          return SUserCode;
     }

     public int getIndexOf(String SUserCode,String SItemCode)
     {
          int iIndex=-1;

          for(int i=0;i<VSUserCode.size();i++)
          {
               String SOUserCode = (String)VSUserCode.elementAt(i);
               String SOItemCode = (String)VSItemCode.elementAt(i);

               if(SUserCode.equals(SOUserCode) && SItemCode.equals(SOItemCode))
               {
                    iIndex = i;
                    break;
               }
               else
               {
                    continue;
               }
          }
          return iIndex;
     }

     private void updateData()
     {
          try
          {
               String DelQS = " Delete from ItemStock Where MillCode="+iMillCode;

               String QString = " Insert into ItemStock (MillCode,HodCode,ItemCode,Stock,StockValue) Values (";

               if(theConnection==null)
               {
                    ORAConnection  jdbc           = ORAConnection.getORAConnection();
                    theConnection                 = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();

               try
               {
                    stat.executeQuery(DelQS);
               }
               catch(Exception e){}


               for(int i=0;i<VSUserCode.size();i++)
               {
                    String    QS1 = QString;
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"0"+(String)VSUserCode.elementAt(i)+",";
                              QS1 = QS1+"'"+(String)VSItemCode.elementAt(i)+"',";
                              QS1 = QS1+"0"+(String)VSStockQty.elementAt(i)+",";
                              QS1 = QS1+"0"+(String)VSStockVal.elementAt(i)+")";

                    stat.executeUpdate(QS1);
               }
               stat.close();
               JOptionPane.showMessageDialog(null,VSItemCode.size()+" Items updated ");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               JOptionPane.showMessageDialog(null,"Updation Error:"+ex);
               removeFrame();
          }

     }

     private void removeFrame()
     {
          layer.remove(this);
          layer.repaint();
          layer.updateUI();
     }
     private void updateLookAndFeel()
     {
          try
          {
               UIManager.setLookAndFeel(windows);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }                    

     }
     
}



