package Kardex;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.*;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import util.*;
import guiutil.*;

public class GraphPanel extends JPanel
{
     Vector         VRow,VCol;
     Dimension      dim;
     Graphics2D     big;
     BufferedImage  bimg;
     Common         common = new Common();
     Vector         VX;
     double         dMax      = 0;
     int            iVLines   = 10;

     GraphPanel(Vector VRow,Vector VCol)
     {
          super(true);
          this.VRow = VRow;
          this.VCol = VCol;
               dMax = getMax(VCol);
          setLayouts();
     }

     public void setLayouts()
     {
          setLayout(new BorderLayout());
     }

     public void paint(Graphics g)
     {
               dim  = getSize();
          int  iCol = 0;
          int  iRow = 0;

          double    dval      = 0;
          double    dinc      = dMax/(iVLines-2);
          int       iWidth    = dim.width;
          int       iHeight   = dim.height;

                    VX        = new Vector();

                    bimg      = (BufferedImage) createImage(iWidth, iHeight);
                    big       = bimg.createGraphics();
                    big       . setBackground(getBackground());
                    big       . clearRect(0,0,iWidth,iHeight);
                    big       . setColor(Color.blue);
                    big       . drawRect(0,0,iWidth,iHeight);
                    big       . setColor(Color.blue);

          // Drawing Horizontal lines
          for(int i=0;i<iVLines;i++)
          {
                         iCol = iCol+(iHeight/iVLines);
               int       x1   = 0+30;
               int       y1   = iHeight-iCol;
               int       x2   = iWidth;
               int       y2   = iHeight-iCol;
                         dval = dval+dinc;
               String    str  = common.getRound(String.valueOf(dval),3);
                         big  . drawLine(x1,y1,x2,y2);
                         big  . drawString(str,x1-30,y1);
          }
          // Drawing Vertical lines
          for(int i=0;i<VRow.size()+2;i++)
          {
                    iRow = iRow+(iWidth/(VRow.size()+2));
               int  x1   = iRow;
               int  y1   = iHeight;
               int  x2   = iRow;
               int  y2   = 0;
                    big  . drawLine(x1,y1,x2,y2);
                    VX   . addElement(String.valueOf(iRow));
               try
               {
                    big  . drawString((String)VRow.elementAt(i),iRow,y1);
               }
               catch(Exception ex){}
          }
          int ox=0,oy=0;
          big.setColor(Color.cyan);
          for(int i=0;i<VCol.size();i++)
          {
               double    dCol = common.toDouble((String)VCol.elementAt(i));
               int       cy   = (int)(iHeight*dCol/dval);
                         cy   = iHeight-cy;
               int       cx   = common.toInt((String)VX.elementAt(i));
               if(i==0)
               {
                    ox=cx;
                    oy=cy;
               }
               big  . drawLine(ox,oy,cx,cy);
               big  . setColor(Color.red);
               big  . fillOval(cx,cy,6,6);
               if (!(common.getRound(dCol,2)).equals("0.00"))
               //big.drawString(common.getRound(dCol,2),cx,cy);
               big.setColor(Color.cyan);
               ox   = cx;
               oy   = cy;
          }
          g.drawImage(bimg,0,0,this);
     }

     public double getMax(Vector V)
     {
          double dmax = 0;
          for(int i=0;i<V.size();i++)
          {
               double dcur = common.toDouble((String)V.elementAt(i));
               if(dcur > dmax)
                    dmax = dcur;
          }
          return dmax;
     }
}
