package Kardex;

import util.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.Vector;
import java.sql.*;
import java.awt.event.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class ItemMasterStockUpdationFrame extends JInternalFrame
{
     JPanel TopPanel,MiddlePanel,BottomPanel;
     String mac      = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
     String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

     Common common   = new Common();

     Vector VItemCode;

     Vector VSItemCode,VSRecQty,VSRecVal,VSIssQty,VSIssVal;

     JTextArea     TInfo;

     JButton   BApply;
     JLayeredPane layer;
     int          iMillCode;
     String       SItemTable;

     Connection theConnection=null;

     public ItemMasterStockUpdationFrame(JLayeredPane layer,int iMillCode,String SItemTable)
     {
          super("Utility for Uploading Item Stock");
          this.layer      = layer;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;

          updateLookAndFeel();
          setSize(500,500);
          
          TopPanel    = new JPanel(true);
          MiddlePanel = new JPanel(true);
          MiddlePanel.setLayout(new BorderLayout());

          TInfo       = new JTextArea(50,50);
          BApply      = new JButton("Apply");

          TopPanel.add(BApply);
          MiddlePanel.add(TInfo);

          BApply.addActionListener(new AppList());

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          setVisible(true);
          setClosable(true);
     }
      private class AppList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {

               BApply.setEnabled(false);
               try
               {
                    MiddlePanel.removeAll();
               }catch(Exception ex){System.out.println(ex);}
               MiddlePanel.add(TInfo);
               TInfo.append("Processing ....."+"\n");

               TInfo.append("Fetching Stock Data ....."+"\n");
               getItemData();

               if(VItemCode.size()>0)
               {
                    TInfo.append("Fetching User Stock Data ....."+"\n");
                    getStockData();
     
                    TInfo.append("Updation is going on ....."+"\n");
                    updateData();
               }

               TInfo.append("Updation Over .....");
               removeFrame();
          }
      }

     private void getItemData()
     {
          VItemCode     = new Vector();

          String QS = "";

          if(iMillCode==0)
          {
               QS = " select item_code from ( "+
                    " select item_code,item_name,opgqty,recqty,issqty,sum(opgqty+recqty-issqty) as invstock,nvl(t1.stock,0) as itemstock "+
                    " from invitems left join (select itemcode,sum(stock) as stock from itemstock where millcode="+iMillCode+" group by itemcode) t1 on (invitems.item_code=t1.itemcode) "+
                    " where opgqty<>0 or recqty<>0 or issqty<>0 or nvl(t1.stock,0)<>0  "+
                    " group by item_code,item_name,opgqty,recqty,issqty,t1.stock having sum(opgqty+recqty-issqty)<>nvl(t1.stock,0) "+
                    " ) order by item_code ";
          }
          else
          {
               QS = " select item_code from ( "+
                    " select "+SItemTable+".item_code,item_name,"+SItemTable+".opgqty,"+SItemTable+".recqty,"+SItemTable+".issqty, "+
                    " sum("+SItemTable+".opgqty+"+SItemTable+".recqty-"+SItemTable+".issqty) as invstock,nvl(t1.stock,0) as itemstock "+
                    " from "+SItemTable+" inner join invitems on "+SItemTable+".item_code=invitems.item_code "+
                    " left join (select itemcode,sum(stock) as stock from itemstock where millcode="+iMillCode+" group by itemcode) t1 on ("+SItemTable+".item_code=t1.itemcode) "+
                    " where "+SItemTable+".opgqty<>0 or "+SItemTable+".recqty<>0 or "+SItemTable+".issqty<>0 or nvl(t1.stock,0)<>0 "+
                    " group by "+SItemTable+".item_code,item_name,"+SItemTable+".opgqty,"+SItemTable+".recqty,"+SItemTable+".issqty,nvl(t1.stock,0) "+
                    " having sum("+SItemTable+".opgqty+"+SItemTable+".recqty-"+SItemTable+".issqty)<>nvl(t1.stock,0) "+
                    " ) order by item_code ";
          }

          try
          {

               if(theConnection==null)
               {
                    ORAConnection jdbc       = ORAConnection.getORAConnection();
                    theConnection            = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    VItemCode.addElement(common.parseNull((String)theResult.getString(1)));
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void getStockData()
     {
          VSItemCode = new Vector();
          VSRecQty   = new Vector();
          VSRecVal   = new Vector();
          VSIssQty   = new Vector();
          VSIssVal   = new Vector();

          try
          {
               if(theConnection==null)
               {
                    ORAConnection jdbc       = ORAConnection.getORAConnection();
                    theConnection            = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();

               for(int i=0;i<VItemCode.size();i++)
               {
                    String SItemCode = (String)VItemCode.elementAt(i);

                    String QS = " Select Sum(RecQty),Sum(RecVal),Sum(IssQty),Sum(IssVal) from ( "+
                                " Select Sum(GrnQty) as RecQty,Sum(GrnValue) as RecVal,0 as IssQty,0 as IssVal "+
                                " From Grn Where MillCode="+iMillCode+" and Code='"+SItemCode+"'"+
                                " Union All "+
                                " Select 0 as RecQty,0 as RecVal,Sum(GrnQty) as IssQty,Sum(GrnValue) as IssVal "+
                                " From Grn Where GrnBlock>=2 and MillCode="+iMillCode+" and Code='"+SItemCode+"'"+
                                " Union All "+
                                " Select 0 as RecQty,0 as RecVal,Sum(Qty) as IssQty,sum(IssueValue) as IssVal "+
                                " From Issue Where MillCode="+iMillCode+" and Code='"+SItemCode+"'"+
                                " ) ";

                    ResultSet theResult      = theStatement.executeQuery(QS);
                    while(theResult.next())
                    {
                         VSItemCode.addElement(SItemCode);
                         VSRecQty.addElement(theResult.getString(1));
                         VSRecVal.addElement(theResult.getString(2));
                         VSIssQty.addElement(theResult.getString(3));
                         VSIssVal.addElement(theResult.getString(4));
                    }
                    theResult.close();
               }
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void updateData()
     {
          try
          {
               if(theConnection==null)
               {
                    ORAConnection  jdbc           = ORAConnection.getORAConnection();
                    theConnection                 = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();

               for(int i=0;i<VSItemCode.size();i++)
               {
                    String QS = " Update "+SItemTable+" Set "+
                                " RecQty="+(String)VSRecQty.elementAt(i)+","+
                                " RecVal="+(String)VSRecVal.elementAt(i)+","+
                                " IssQty="+(String)VSIssQty.elementAt(i)+","+
                                " IssVal="+(String)VSIssVal.elementAt(i)+" "+
                                " Where Item_Code='"+(String)VSItemCode.elementAt(i)+"'";


                    stat.executeUpdate(QS);
               }
               stat.close();
               JOptionPane.showMessageDialog(null,VSItemCode.size()+" Items updated ");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               JOptionPane.showMessageDialog(null,"Updation Error:"+ex);
               removeFrame();
          }

     }

     private void removeFrame()
     {
          layer.remove(this);
          layer.repaint();
          layer.updateUI();
     }
     private void updateLookAndFeel()
     {
          try
          {
               UIManager.setLookAndFeel(windows);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }                    

     }
     
}



