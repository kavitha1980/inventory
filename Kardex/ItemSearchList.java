package Kardex;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import util.Common;
import util.Item1;

import guiutil.*;
import jdbc.*;


public class ItemSearchList
{

     JDialog    dialog;
     JPanel     thePanel,TopPanel;
     JList      BrowList;
     JLabel     LIndicator;
     JTextField TMiddleText;

     String str="";

     Common common = new Common();

     Connection theConnection=null;
     Vector VCode,VName,VItemName;


     JTextField     TMatCode;
     JButton        BMatName;
     int            iMillCode;
     String         SItemTable;

     public ItemSearchList(JTextField TMatCode,JButton BMatName,int iMillCode,String SItemTable)
     {
          this.TMatCode   = TMatCode;
          this.BMatName   = BMatName;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;

	// System.out.println("Search List ==>"+TMatCode.getText());
          try
          {
               ORAConnection jdbc   = ORAConnection.getORAConnection();
               theConnection        = jdbc.getConnection();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          createComponents();
          setLayouts();
          addComponents();
          preset();
          addListeners();
          activate();
     }
          
     private void preset()
     {
          String SBName = (BMatName.getText()).trim();

          if(!SBName.equals("Select a Material"))
          {
               TMiddleText.setText(SBName);
               str = SBName;
               setItemList();
          }
     }

     private void createComponents()
     {
          TopPanel   = new JPanel();
          thePanel   = new JPanel(true);
          BrowList   = new JList();

          TMiddleText = new JTextField(30);

          dialog     = new JDialog(new Frame(),"",true);
     }
               
     private void setLayouts()
     {
          TopPanel.setLayout(new GridLayout(2,1));
          thePanel.setLayout(new  BorderLayout());
          BrowList.setFont(new Font("monospaced", Font.PLAIN, 11));
     }

     private void addComponents()
     {
          TopPanel.add(new JLabel(" Middle Text "));
          TopPanel.add(new JLabel(""));

          TopPanel.add(TMiddleText);
          TopPanel.add(new JLabel(""));

          thePanel.add("Center",new JScrollPane(BrowList));
     }

     private void addListeners()
     {
          TMiddleText.addKeyListener(new KeyList1());

          BrowList.addKeyListener(new KeyList3());
     }
     
     public void activate()
     {
          dialog.getContentPane().add("North",TopPanel);
          dialog.getContentPane().add("Center",thePanel);
          dialog.setBounds(50,50,500,500);
          dialog.setVisible(true);

          BrowList.requestFocus();
     }
     
     public void deactivate()
     {
          dialog.setVisible(false);
     }

     private class KeyList1 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setItemList();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setItemList();
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         TMiddleText.setText("");
                         str="";
                         VCode.removeAllElements();
                         VName.removeAllElements();
                         VItemName.removeAllElements();
                         BrowList.setListData(VItemDetails);
                    }
               }
               catch(Exception ex)
               {
               }
          }
     }

     private class KeyList3 extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    setDetails(BrowList.getSelectedIndex());
               }
               if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
               {
                    setDetails(-1);
               }
          }
     }
     private void setItemList()
     {
          VCode         = new Vector();
          VName         = new Vector();
          VItemName     = new Vector();

          try
          {
               String QS="";

               if(!str.equals(""))
               {
                    if(iMillCode==0)
                    {
                        QS = "  Select InvItems.Item_Code,rpad(InvItems.Item_Name,50),rpad(Uom.UomName,10),rpad(InvItems.Catl,30),rpad(InvItems.Draw,40),InvItems.HSNCode from InvItems "+
                             "  Inner join Uom on Uom.UomCode = InvItems.UomCode "+
                             "  where InvItems.Item_Name like  '%"+str+"%' and invitems.hsntype=0"+
                             "  order by 2 ";
                    }
                    else
                    {
                        QS = "  Select "+SItemTable+".Item_Code,rpad(Item_Name,50),rpad(Uom.UomName,10),rpad(InvItems.Catl,30),rpad(InvItems.Draw,40),InvItems.HSNCode from "+SItemTable+" "+
                             "  Inner Join InvItems on InvItems.Item_Code = "+SItemTable+".Item_Code"+
                             "  Inner join Uom on Uom.UomCode = InvItems.UomCode "+
                             "  where InvItems.Item_Name like  '%"+str+"%' and invitems.hsntype=0 "+
                             "  order by 2 ";
                    }

                    PreparedStatement thePrepare = theConnection.prepareStatement(QS);
                         ResultSet theResult = thePrepare.executeQuery();
                    while(theResult.next())
                    {
                         String SCode = theResult.getString(1);
                         String SName = theResult.getString(2);
                         String SUom  = theResult.getString(3);
                         String SCatl = theResult.getString(4);
                         String SDraw = theResult.getString(5); 
                         String SHsnCode =common.parseNull(theResult.getString(6)); 

                         String SItemName = SName+"  "+SCode+"  "+SUom+"      "+SHsnCode+"     "+SCatl+"  "+SDraw ;

                         VCode.addElement(SCode);
                         VName.addElement(SName);
                         VItemName.addElement(SItemName);
                    }
                    theResult.close();
                    thePrepare.close();
               }
               BrowList.setListData(VItemName);
               BrowList.setSelectedIndex(0);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

     }
     private void setDetails(int index)
     {
          
         if(index > -1)
         {
               String SItemCode = (String)VCode.elementAt(index);
               String SItemName = (String)VName.elementAt(index);

               TMatCode.setText(SItemCode);
               BMatName.setText(SItemName);

               dialog.setVisible(false);
          }
      }
}
