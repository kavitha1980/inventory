package Kardex;

import util.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.Vector;
import java.sql.*;
import java.awt.event.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class ItemStockRateUpdationFrame extends JInternalFrame
{
     JPanel TopPanel,MiddlePanel,BottomPanel;
     String mac      = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
     String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

     Common common   = new Common();

     Vector VItemCode,VStockQty,VStockVal;

     JTextArea     TInfo;

     JButton   BApply;
     JLayeredPane layer;
     int          iMillCode;
     String       SItemTable;

     Connection theConnection=null;

     public ItemStockRateUpdationFrame(JLayeredPane layer,int iMillCode,String SItemTable)
     {
          super("Utility for Uploading Item Stock");
          this.layer      = layer;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;

          updateLookAndFeel();
          setSize(500,500);
          
          TopPanel    = new JPanel(true);
          MiddlePanel = new JPanel(true);
          MiddlePanel.setLayout(new BorderLayout());

          TInfo       = new JTextArea(50,50);
          BApply      = new JButton("Apply");

          TopPanel.add(BApply);
          MiddlePanel.add(TInfo);

          BApply.addActionListener(new AppList());

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          setVisible(true);
          setClosable(true);
     }
      private class AppList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {

               BApply.setEnabled(false);
               try
               {
                    MiddlePanel.removeAll();
               }catch(Exception ex){System.out.println(ex);}
               MiddlePanel.add(TInfo);
               TInfo.append("Processing ....."+"\n");

               TInfo.append("Fetching Stock Data ....."+"\n");
               getStockData();

               if(VItemCode.size()>0)
               {
                    TInfo.append("Updation is going on ....."+"\n");
                    updateData();
               }

               TInfo.append("Updation Over .....");
               removeFrame();
          }
      }

     private void getStockData()
     {
          VItemCode     = new Vector();
          VStockQty     = new Vector();
          VStockVal     = new Vector();


          String QS = " select item_code as itemcode,stock,round(stockvalue/stock,4) as stockvalue from ( "+
                      " select item_code,sum(opgqty+recqty-issqty) as Stock,sum(opgval+recval-issval) as StockValue from ( "+
                      " select "+SItemTable+".item_code,nvl(opgqty,0) as opgqty,nvl(t1.recqty,0) as recqty,nvl(t2.issqty,0) as issqty, "+
                      " nvl(opgval,0) as opgval,nvl(t1.recval,0) as recval,nvl(t2.issval,0) as issval from "+SItemTable+
                      " left join (select code,sum(grnqty) as recqty,sum(grnvalue) as recval from grn where millcode="+iMillCode+
                      " and grnblock<=1 group by code) t1 on "+SItemTable+".item_code=t1.code "+
                      " left join (select code,sum(qty) as issqty,sum(issuevalue) as issval from issue where millcode="+iMillCode+
                      " group by code) t2 on "+SItemTable+".item_code=t2.code) "+
                      " group by item_code order by item_code) where stock>0 ";

                      // A11027160,A11037900,A08054634,A06108209,A08000291

          try
          {

               if(theConnection==null)
               {
                    ORAConnection jdbc       = ORAConnection.getORAConnection();
                    theConnection            = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    VItemCode.addElement(common.parseNull((String)theResult.getString(1)));
                    VStockQty.addElement(common.parseNull((String)theResult.getString(2)));
                    VStockVal.addElement(common.parseNull((String)theResult.getString(3)));
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void updateData()
     {
          try
          {
               if(theConnection==null)
               {
                    ORAConnection  jdbc           = ORAConnection.getORAConnection();
                    theConnection                 = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();

               for(int i=0;i<VItemCode.size();i++)
               {
                    String SItemCode = (String)VItemCode.elementAt(i);
                    String SRate     = (String)VStockVal.elementAt(i);

                    String QS = " Update ItemStock set StockValue="+SRate+
                                " Where MillCode="+iMillCode+
                                " and ItemCode='"+SItemCode+"'";

                    stat.executeUpdate(QS);
               }
               stat.close();
               JOptionPane.showMessageDialog(null,VItemCode.size()+" Items updated ");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               JOptionPane.showMessageDialog(null,"Updation Error:"+ex);
               removeFrame();
          }

     }

     private void removeFrame()
     {
          layer.remove(this);
          layer.repaint();
          layer.updateUI();
     }
     private void updateLookAndFeel()
     {
          try
          {
               UIManager.setLookAndFeel(windows);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }                    

     }
     
}



