package Kardex;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import jdbc.*;
import util.*;
import guiutil.*;

public class KardexFrame extends JInternalFrame
{
     JPanel         TopPanel,TopLeft,TopRight,MiddlePanel;
     
     JTabbedPane    JTP;
     
     JButton        BMaterial,BApply;
     JTextField     TMatCode;
     
     JLayeredPane   DeskTop;
     Vector         VCode,VName,VNameCode;
     StatusPanel    SPanel;
     
     Common         common   = new Common();
     int            iMillCode;
     String         SStDate,SEnDate;
     String         SItemTable,SSupTable;
	 
	 TabReport1 tabreportnew;
	 
	 Object         RowDataNew[][];	 
	  
     
     public KardexFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iMillCode,String SStDate,String SEnDate,String SItemTable,String SSupTable)
     {
          this.DeskTop    = DeskTop;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.SPanel     = SPanel;
          this.iMillCode  = iMillCode;
          this.SStDate    = SStDate;
          this.SEnDate    = SEnDate;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;

	System.out.println("ItemTable==>"+SItemTable);
          
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     
     public void createComponents()
     {
          TopPanel       = new JPanel(true);
          TopLeft        = new JPanel(true);
          TopRight       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          
          BMaterial      = new JButton("Select a Material");
          BApply         = new JButton("Apply");
          TMatCode       = new JTextField();
          
          TMatCode       . setEditable(false);
     }
     
     public void setLayouts()
     {
          TopPanel       . setLayout(new GridLayout(1,2));
          MiddlePanel    . setLayout(new BorderLayout());
          TopLeft        . setLayout(new GridLayout(2,1));
          TopRight       . setLayout(new BorderLayout());
          
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setTitle("Order Cardinal Points of a Selected Material (formerly Kardex)");
          setBounds(0,0,650,600);
     }
     
     public void addComponents()
     {
          TopLeft             . add(BMaterial);
          TopLeft             . add(TMatCode);
          TopRight            . add("Center",BApply);
          
          TopPanel            . add(TopLeft);
          TopPanel            . add(TopRight);
          
          TopLeft             . setBorder(new TitledBorder("Material Selection"));
          TopRight            . setBorder(new TitledBorder("Click To View"));
          
          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("Center",MiddlePanel);
     }
     
     public void addListeners()
     {
          //BMaterial           . addActionListener(new MaterialSearch(DeskTop,TMatCode,BMaterial,VCode,VName,VNameCode,iMillCode,SItemTable));
          BMaterial           . addActionListener(new ActList());
          BApply              . addActionListener(new ApplyList());
		  
		 // tabreport      . ReportTable.addKeyListener(new KeyList(this));
     }
	 
	 

	 public class KeyList extends KeyAdapter
     {
          KardexFrame KF;

          public KeyList(KardexFrame KF)
          {
               this.KF = KF;
          }

          public void keyReleased(KeyEvent ke)
          {
               String SPColour  = "";
               String SPSet     = "";
               String SPSize    = "";
               String SPSide    = "";


               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
				
				    int iRow =0;//tabreport.ReportTable.getSelectedRow();
					
                    int       i         	= tabreportnew.ReportTable.getSelectedRow();

					String    SStkGroup  	= ((String)RowDataNew[i][0]).trim();
					String 	  SOldBin		= ((String)RowDataNew[i][2]).trim();
					
					Item1 item          	= new Item1(TMatCode.getText(),SStDate,SEnDate,iMillCode,SItemTable,SSupTable);
					
					String SMaterialCode	= item.getCode();
					String SMateriaName		= item.getName();
					
					System.out.println("SMaterialCode"+SMaterialCode);
					System.out.println("SMaterialName"+SMateriaName);
					
					MaterialBinModification MF = new MaterialBinModification(iMillCode,SItemTable,SMaterialCode,SMateriaName,SStkGroup,i,SOldBin,RowDataNew,KF);
				
				
			   }	
				   
          }
     }
	 
     /*public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
				    int iRow =0;//tabreport.ReportTable.getSelectedRow();
					
                    int       i         	= tabreportnew.ReportTable.getSelectedRow();

					String    SStkGroup  	= ((String)RowDataNew[i][0]).trim();
					String 	  SOldBin		= ((String)RowDataNew[i][2]).trim();
					
					Item1 item          	= new Item1(TMatCode.getText(),SStDate,SEnDate,iMillCode,SItemTable,SSupTable);
					
					String SMaterialCode	= item.getCode();
					String SMateriaName		= item.getName();
					
					System.out.println("SMaterialCode"+SMaterialCode);
					System.out.println("SMaterialName"+SMateriaName);
					
					MaterialBinModification MF = new MaterialBinModification(iMillCode,SItemTable,SMaterialCode,SMateriaName,SStkGroup,i,SOldBin,RowDataNew,tabreportnew);
					
               }
          }
     }*/
	 
	 
	 
	 
     
    public class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
		
            ItemSearchListNew itemsearchlist = new ItemSearchListNew(TMatCode,BMaterial,iMillCode,SItemTable);
        }
    }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    MiddlePanel.remove(JTP);
               }
               catch(Exception ex){}
               JTP            = new JTabbedPane(JTabbedPane.RIGHT);
               JTP            .addTab("General",getStock());
               JTP            .addTab("Pending Orders",getPendingPO());
               JTP            .addTab("Orders Placed ",getPORatePane());
               JTP            .addTab("Swing in Rate", getPORateVariation());
               JTP            .addTab("Swing In Lot Size",getPOQtyVariation());
               JTP            .addTab("Seasonal Variation in PO",getPOSeasonalVariation());
               JTP            .addTab("PO Placed for Dept (Qty)",getPODeptQtyVariation());
               JTP            .addTab("PO Placed for Dept (Value)",getPODeptNetVariation());
               JTP            .addTab("Consumption",getConsumption());
               JTP            .addTab("MRS Placed",getMRSPlaced());
               JTP            .addTab("Receipts ",getReceiptPane());
               MiddlePanel    .add("Center",JTP);
               DeskTop        .updateUI();
          }
     }
	 
	 public void newLoad() {
		 
               try
               {
                    MiddlePanel.remove(JTP);
               }
               catch(Exception ex){}
               JTP            = new JTabbedPane(JTabbedPane.RIGHT);
               JTP            .addTab("General",getStock());
               JTP            .addTab("Pending Orders",getPendingPO());
               JTP            .addTab("Orders Placed ",getPORatePane());
               JTP            .addTab("Swing in Rate", getPORateVariation());
               JTP            .addTab("Swing In Lot Size",getPOQtyVariation());
               JTP            .addTab("Seasonal Variation in PO",getPOSeasonalVariation());
               JTP            .addTab("PO Placed for Dept (Qty)",getPODeptQtyVariation());
               JTP            .addTab("PO Placed for Dept (Value)",getPODeptNetVariation());
               JTP            .addTab("Consumption",getConsumption());
               JTP            .addTab("MRS Placed",getMRSPlaced());
               JTP            .addTab("Receipts ",getReceiptPane());
               MiddlePanel    .add("Center",JTP);
               DeskTop        .updateUI();

		 
	 }
	 
     
     public JPanel getStock()
     {
          JPanel panel        = new JPanel(true);
          JPanel IdPanel      = new JPanel(true);
          JPanel StockPanel   = new JPanel(true);
          JPanel IssRecPanel  = new JPanel(true);
          JPanel MiscPanel    = new JPanel(true);
          JPanel UserPanel    = new JPanel(true);
		  
		  //JPanel BinPanel    = new JPanel(true);
		  
		  //JPanel LastTwoPanel = new JPanel(true);

          Item1 item          = new Item1(TMatCode.getText(),SStDate,SEnDate,iMillCode,SItemTable,SSupTable);
          
          IdPanel             .setLayout(new GridLayout(4,4));
          StockPanel          .setLayout(new GridLayout(4,4));
          IssRecPanel         .setLayout(new GridLayout(2,4));
          MiscPanel           .setLayout(new GridLayout(2,4));
          UserPanel           .setLayout(new GridLayout(1,1));
		  //BinPanel            .setLayout(new GridLayout(1,1));
		  		  
		  
          
          IdPanel             .add(new JLabel("Material Name"));
          IdPanel             .add(new JLabel(item.getName()));
          IdPanel             .add(new JLabel("Material Code"));
          IdPanel             .add(new JLabel(item.getCode()));
          
          IdPanel             .add(new JLabel("UOM"));
          IdPanel             .add(new JLabel(item.getUoM()));
          IdPanel             .add(new JLabel("HSN Code"));
          IdPanel             .add(new JLabel(item.getHsnCode()));


          IdPanel             .add(new JLabel("Location"));
          JLabel theBin       = new JLabel(item.getLocation());
          theBin              .setForeground(Color.blue);
          IdPanel             .add(theBin);          
          IdPanel             .add(new JLabel("Catalogue"));
          IdPanel             .add(new JLabel(item.getCatl()));


          IdPanel             .add(new JLabel("Drawing No"));
          IdPanel             .add(new JLabel(item.getDraw()));
          IdPanel             .add(new JLabel(""));
          IdPanel             .add(new JLabel(""));
          
          

          IdPanel             .setBorder(new TitledBorder("Identification"));
          
          JLabel    theStock  = new JLabel(item.getClStock());
                    theStock  .setForeground(Color.blue);
          
          JLabel    theValue  = new JLabel(item.getClValue());
                    theValue  .setForeground(Color.blue);
          
          JLabel    NonStock  = new JLabel(item.getNonStock());
                    NonStock  .setForeground(Color.blue);
					
		 JLabel   ServiceStock = new JLabel(item.getServiceStock());
		  ServiceStock .setForeground(Color.blue);		

          StockPanel          .add(new JLabel("Opening Stock (Qty)"));
          StockPanel          .add(new JLabel(item.getOpgStock()));
          StockPanel          .add(new JLabel("Opening Value (Rs)"));
          StockPanel          .add(new JLabel(item.getOpgValue()));
          StockPanel          .add(new JLabel("Closing Stock (Qty)"));
          StockPanel          .add(theStock);
          StockPanel          .add(new JLabel("Closing Value (Rs)"));
          StockPanel          .add(theValue);
          StockPanel          .add(new JLabel("Non Stock (Qty)"));
          StockPanel          .add(NonStock);
          StockPanel          .add(new JLabel(""));
          StockPanel          .add(new JLabel(""));
		  StockPanel          .add(new JLabel("Service Stock (Qty)"));
          StockPanel          .add(ServiceStock);
		  StockPanel          .add(new JLabel(""));
          StockPanel          .add(new JLabel(""));
          StockPanel          .setBorder(new TitledBorder("Stock Level"));
          
          IssRecPanel         .add(new JLabel("Consumption (Qty)"));
          IssRecPanel         .add(new JLabel(item.getIssueQty()));
          IssRecPanel         .add(new JLabel("Consumption (Value)"));
          IssRecPanel         .add(new JLabel(item.getIssueValue()));
          IssRecPanel         .add(new JLabel("Purchase (Qty)"));
          IssRecPanel         .add(new JLabel(item.getGRNQty()));
          IssRecPanel         .add(new JLabel("Purchase (Value)"));
          IssRecPanel         .add(new JLabel(item.getGRNValue()));
          IssRecPanel         .setBorder(new TitledBorder("Receipts & Consumption"));
          

          MiscPanel           .add(new JLabel("Last Purchase Rate"));
          MiscPanel           .add(new JLabel(item.getRate()));
          MiscPanel           .add(new JLabel("Minimum Qty"));
          MiscPanel           .add(new JLabel(item.getMinQty()));
          MiscPanel           .add(new JLabel("Maximum Qty"));
          MiscPanel           .add(new JLabel(item.getMaxQty()));
          MiscPanel           .add(new JLabel("Re-Order Level"));
          MiscPanel           .add(new JLabel(item.getROLQty()));


		  
          //JLabel    thebin  = new JLabel("Press F3 in Bin Location Column to Modifiy Bin Location.");
                    //thebin  .setForeground(Color.blue);

          //BinPanel           .add(thebin);
          		
		  
          MiscPanel           .setBorder(new TitledBorder("Miscellaneous"));
		  
		  
		  
          
          /*String    ColumnData[] = {"User Name","Stock Qty"};
          String    ColumnType[] = {"S","N"};
          int       iColWidth[]  = {200,100};*/
		  
          String    ColumnData[] = {"User Name","Stock Qty","Location"};
          String    ColumnType[] = {"S","N","S"};
          int       iColWidth[]  = {180,100,140};
		  
          //TabReport1 tabreport   = new TabReport1(getUserStockData(),ColumnData,ColumnType);
		  
		  tabreportnew   = new TabReport1(getUserStockData(),ColumnData,ColumnType);
		  
		  tabreportnew           .setPrefferedColumnWidth1(iColWidth);
		  
          //tabreport           .setPrefferedColumnWidth1(iColWidth);

          //UserPanel           .add(tabreport);
		  
		  UserPanel           .add(tabreportnew);
          UserPanel           .setBorder(new TitledBorder("Userwise Stock & Press F3 in Bin Location Column to Modifiy Bin Location."));

          panel               .setLayout(new GridLayout(5,1));
          panel               .setBorder(new TitledBorder("General Details"));

          panel               .add(IdPanel);
          panel               .add(StockPanel);
          panel               .add(IssRecPanel);
          panel               .add(MiscPanel);
		  //panel               .add(BinPanel);
		  
          panel               .add(UserPanel);
		  
   		  tabreportnew .ReportTable.addKeyListener(new KeyList(this));


          return panel;
     }
     
     public JPanel getPendingPO()
     {
          JPanel    panel     = new JPanel(true);
                    panel     .setLayout(new BorderLayout());
                    panel     .setBorder(new TitledBorder("List of Pending Orders"));
                    panel     .setBackground(new Color(80,160,160));
          
          String    ColumnData[]   = {"Order No","Date","Block","Supplier","Pending Qty","Rate","Discount (%)","CGST","SGST","IGST","CESS","CenVat (%)","Tax (%)","Surcharge (%)","Net (Rs)","GST Status"};
          String    ColumnType[]   = {"N","S","S","S","N","N","N","N","N","N","N","N","N","N","N","S"};
          TabReport1 tabreport      = new TabReport1(getPendingPOData(),ColumnData,ColumnType);
                    panel          .add(tabreport);
          return panel;
     }

     public JPanel getMRSPlaced()
     {
          JPanel    panel     = new JPanel(true);
                    panel     .setLayout(new BorderLayout());
                    panel     .setBorder(new TitledBorder("List of MRSs Placed"));
                    panel     .setBackground(new Color(80,160,160));

          String ColumnData[] = {"MRS No","Date","Qty","Block","Order No","Department"};
          String ColumnType[] = {"N","S","N","S","N","S"};
          TabReport1 tabreport = new TabReport1(getMRSData(),ColumnData,ColumnType);
                    panel     .add(tabreport);
          return panel;
     }
     
     public JPanel getPORatePane()
     {
          JPanel    panel     = new JPanel(true);
                    panel     .setLayout(new BorderLayout());
                    panel     .setBorder(new TitledBorder("Order Placed"));
                    panel     .setBackground(new Color(163,163,209));

          String ColumnData[] = {"Order No","Date","Block","Supplier","Ordered Qty","Rate","Discount (%)","CGST","SGST","IGST","CESS","CenVat (%)","Tax (%)","Surcharge (%)","Net (Rs)","GST Status"};
          String ColumnType[] = {"N","S","S","S","N","N","N","N","N","N","N","N","N","N","N","S"};
          TabReport1 tabreport = new TabReport1(getPORateData(),ColumnData,ColumnType);
                    panel     .add(tabreport);
                    panel     .addMouseListener(new PORMouseList(tabreport));
          return panel;
     }
     
     public JPanel getReceiptPane()
     {
          JPanel    panel     = new JPanel(true);
                    panel     .setLayout(new BorderLayout());
                    panel     .setBorder(new TitledBorder("Receipts "));
                    panel     .setBackground(new Color(163,163,209));

          String ColumnData[] = {"GRN No","Date","Block","Supplier","Actual Qty"};
          String ColumnType[] = {"N"     ,"S"   ,"S"    ,"S"       ,"N"         };
          int iColWidth[]     = {50      ,80    ,50     ,250       ,50          };
          
          TabReport1 tabreport = new TabReport1(getReceiptData(),ColumnData,ColumnType);
          tabreport           .setPrefferedColumnWidth1(iColWidth);
          panel               .add(tabreport);
          return panel;
     }
     
     public class PORMouseList extends MouseAdapter
     {
          TabReport1 tabreport;

          PORMouseList(TabReport1 tabreport)
          {
               this.tabreport=tabreport;
          }

          public void mouseClicked(MouseEvent me)
          {
               if (me.getClickCount()==2)
               {
                    JInternalFrame jif = new JInternalFrame("Purchase Rate Variation of "+BMaterial.getText());
                    try
                    {
                         jif       .setClosable(true);
                         jif       .setBounds(0,0,500,300);
                         jif       .setResizable(true);
                         jif       .setSelected(true);
                         jif       .show();
                         DeskTop   .add(jif);
                         jif       .moveToFront();
                         DeskTop   .repaint();
                    }
                    catch(Exception ex){}

                    Vector VRow = new Vector();
                    Vector VCol = new Vector();
                    
                    for(int i=0;i<tabreport.RowData.length;i++)
                    {
                         VRow      .addElement((String)tabreport.RowData[i][1]);
                         VCol      .addElement((String)tabreport.RowData[i][9]);
                    }
                    JScrollPane    jsp  = new JScrollPane();
                                   jsp  .setViewportView(new GraphPanel(VRow,VCol));
                                   jif  .getContentPane().add("Center",jsp);
               }
          }
     }

     public JPanel getPORateVariation()
     {
          JPanel panel       = new JPanel();
          JPanel TopPanel    = new JPanel();
          
          panel.setBorder(new TitledBorder("Swing in Net Rate"));
          
          JTextField minrate = new JTextField();
          JTextField minsupl = new JTextField();
          JTextField minref  = new JTextField();
          JTextField maxrate = new JTextField();
          JTextField maxsupl = new JTextField();
          JTextField maxref  = new JTextField();
          
          minrate   .setEditable(false);
          minsupl   .setEditable(false);
          minref    .setEditable(false);
          maxrate   .setEditable(false);
          maxsupl   .setEditable(false);
          maxref    .setEditable(false);
          
          panel     .setLayout(new BorderLayout());
          TopPanel  .setLayout(new GridLayout(6,2));
          
          TopPanel  .add(new JLabel("Minimum Net Rate"));
          TopPanel  .add(minrate);
          TopPanel  .add(new JLabel("Min Rate Supplier"));
          TopPanel  .add(minsupl);
          TopPanel  .add(new JLabel("Min Rate PO Reference"));
          TopPanel  .add(minref);
          
          TopPanel  .add(new JLabel("Maximum Net Rate"));
          TopPanel  .add(maxrate);
          TopPanel  .add(new JLabel("Max Rate Supplier"));
          TopPanel  .add(maxsupl);
          TopPanel  .add(new JLabel("Max Rate PO Reference"));
          TopPanel  .add(maxref);
          
          setPOMinMaxRate(minrate,minsupl,minref,maxrate,maxsupl,maxref);
          
          panel.add("North",TopPanel);
          return panel;
     }
     public JPanel getPOQtyVariation()
     {
          JPanel panel       = new JPanel();
          JPanel TopPanel    = new JPanel();
          
          panel.setBorder(new TitledBorder("Lot Size Variation"));
          
          JTextField minqty  = new JTextField();
          JTextField minsupl = new JTextField();
          JTextField minref  = new JTextField();
          JTextField maxqty  = new JTextField();
          JTextField maxsupl = new JTextField();
          JTextField maxref  = new JTextField();
          
          minqty    .setEditable(false);
          minsupl   .setEditable(false);
          minref    .setEditable(false);
          maxqty    .setEditable(false);
          maxsupl   .setEditable(false);
          maxref    .setEditable(false);
          
          panel.setLayout(new BorderLayout());
          TopPanel.setLayout(new GridLayout(6,2));
          
          TopPanel.add(new JLabel("Minimum Qty"));
          TopPanel.add(minqty);
          TopPanel.add(new JLabel("Min Qty Supplier"));
          TopPanel.add(minsupl);
          TopPanel.add(new JLabel("Min Qty PO Reference"));
          TopPanel.add(minref);
          
          TopPanel.add(new JLabel("Maximum Qty"));
          TopPanel.add(maxqty);
          TopPanel.add(new JLabel("Max Qty Supplier"));
          TopPanel.add(maxsupl);
          TopPanel.add(new JLabel("Max Qty PO Reference"));
          TopPanel.add(maxref);
          
          setPOMinMaxQty(minqty,minsupl,minref,maxqty,maxsupl,maxref);
          
          panel.add("North",TopPanel);
          return panel;
     }
     public JPanel getPOSeasonalVariation()
     {
          JPanel panel       = new JPanel();
          panel.setLayout(new BorderLayout());
          panel.setBorder(new TitledBorder("Seasonal Order Scenario"));
          panel.setBackground(new Color(70,163,255));
          
          String ColumnData[]= {"Particulars","April","May","June","July","August","September","October","November","December","January","Febraury","March"}; 
          String ColumnType[]= {"S","N","N","N","N","N","N","N","N","N","N","N","N"};  
          
          TabReport1 tabreport = new TabReport1(getOrdMonVar(),ColumnData,ColumnType);
          panel.add("Center",tabreport);
          return panel;
     }

     public JPanel getConsumption()
     {
          JPanel panel       = new JPanel();
          panel.setLayout(new BorderLayout());
          panel.setBorder(new TitledBorder("Seasonal Consumption Scenario"));
          panel.setBackground(new Color(70,163,255));
          
          String ColumnData[]= {"Particulars","April","May","June","July","August","September","October","November","December","January","Febraury","March"}; 
          String ColumnType[]= {"S","N","N","N","N","N","N","N","N","N","N","N","N"};  
          
          TabReport1 tabreport = new TabReport1(getConMonVar(),ColumnData,ColumnType);
          panel.add("Center",tabreport);
          return panel;
     }
     
     public JPanel getPODeptQtyVariation()
     {
          JPanel    panel     = new JPanel();
                    panel     .setLayout(new BorderLayout());
                    panel     .setBorder(new TitledBorder("Seasonal Dept Order Qty Scenario"));
                    panel     .setBackground(Color.pink);
          
          String ColumnData[] = {"Particulars","April","May","June","July","August","September","October","November","December","January","Febraury","March"}; 
          String ColumnType[] = {"S","N","N","N","N","N","N","N","N","N","N","N","N"};  
          TabReport1 tabreport = new TabReport1(getDeptOrdQtyMonVar(),ColumnData,ColumnType);
                    panel     .add("Center",tabreport);
          return panel;
     }

     public JPanel getPODeptNetVariation()
     {
          JPanel    panel     = new JPanel();
                    panel     .setLayout(new BorderLayout());
                    panel     .setBorder(new TitledBorder("Seasonal Dept Order Net Value Scenario (Amount in Rs)"));
                    panel     .setBackground(new Color(255,255,191));
          
          String ColumnData[] = {"Particulars","April","May","June","July","August","September","October","November","December","January","Febraury","March"}; 
          String ColumnType[] = {"S","N","N","N","N","N","N","N","N","N","N","N","N"};  
          TabReport1 tabreport = new TabReport1(getDeptOrdNetMonVar(),ColumnData,ColumnType);
                    panel     .add("Center",tabreport);
          return panel;
     }
     
     public Object[][] getUserStockData()
     {
          String QS = "";
          
          /*QS =      " Select RawUser.UserName,ItemStock.Stock From ItemStock "+
                    " Inner Join RawUser on ItemStock.HodCode=RawUser.UserCode "+
                    " and ItemStock.ItemCode = '"+TMatCode.getText()+"'"+
                    " and ItemStock.MillCode="+iMillCode+
                    " and ItemStock.Stock>0 "+
                    " Order By RawUser.UserName ";*/
					
          QS =      " Select RawUser.UserName,ItemStock.Stock,decode(ItemStock.HodCode,10371,InvItems.ItemLocName,InvItems.LOCNAME) as  LocName From ItemStock "+
                    " Inner Join RawUser on ItemStock.HodCode=RawUser.UserCode "+
                    " and ItemStock.ItemCode = '"+TMatCode.getText()+"'"+
                    " and ItemStock.MillCode="+iMillCode+
                    " and ItemStock.Stock>0 "+
				    " Inner join InvItems on InvItems.Item_Code = ItemStock.ItemCode "+
                    " Order By RawUser.UserName ";
					
     
          Vector VUser   = new Vector();
          Vector VStock  = new Vector();
		  Vector VLocation  = new Vector();

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    VUser     .addElement(res.getString(1));
                    VStock    .addElement(res.getString(2));
					VLocation	.addElement(res.getString(3));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          //Object RowData[][] = new Object[VUser.size()][3];
		  
		   RowDataNew = new Object[VUser.size()][3];
          for(int i=0;i<VUser.size();i++)
          {
               RowDataNew[i][0]       = (String)VUser     .elementAt(i);
               RowDataNew[i][1]       = (String)VStock    .elementAt(i);
			   RowDataNew[i][2]       = (String)VLocation .elementAt(i);
          }
          return RowDataNew;
     }

     public Object[][] getPendingPOData()
     {
          String QS = "";
          
          QS =      " Select PurchaseOrder.OrderNo,PurchaseOrder.OrderDate, "+
                    " OrdBlock.BlockName,"+SSupTable+".Name,PurchaseOrder.Rate, "+
                    " PurchaseOrder.DiscPer,decode(PurchaseOrder.GstStatus,0,0,PurchaseOrder.CenVatPer) as CenvatPer, "+
                    " decode(PurchaseOrder.GstStatus,0,0,PurchaseOrder.TaxPer) as TaxPer,decode(PurchaseOrder.GstStatus,0,0,PurchaseOrder.SurPer) as SurPer, "+
                    " PurchaseOrder.Qty-PurchaseOrder.InvQty "+
                    " ,Purchaseorder.cgst,purchaseorder.sgst,purchaseorder.igst,purchaseorder.cess,purchaseorder.gststatus,PurchaseOrder.Net,purchaseorder.qty "+ 
                    " From PurchaseOrder Inner Join OrdBlock On "+
                    " OrdBlock.Block=PurchaseOrder.OrderBlock "+
                    " and PurchaseOrder.Item_Code = '"+TMatCode.getText()+"'"+
                    " and PurchaseOrder.InvQty < PurchaseOrder.Qty"+
                    " and PurchaseOrder.MillCode="+iMillCode+
                    " Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = PurchaseOrder.Sup_Code "+
                    " Order By PurchaseOrder.OrderNo";
     
          Vector VNo     = new Vector();
          Vector VDate   = new Vector();
          Vector VBlock  = new Vector();
          Vector VSupl   = new Vector();
          Vector VRate   = new Vector();
          Vector VDisc   = new Vector();
          Vector VVat    = new Vector();
          Vector VTax    = new Vector();
          Vector VSur    = new Vector();
          Vector VPending= new Vector();  
 
          Vector VCGST   = new Vector();
          Vector VSGST   = new Vector();
          Vector VIGST   = new Vector();
          Vector VCESS   = new Vector();
          Vector VGSTStatus= new Vector();   
          Vector VNet    = new Vector(); 
          Vector VQty    = new Vector();  

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    VNo       .addElement(res.getString(1));
                    VDate     .addElement(common.parseDate(res.getString(2)));
                    VBlock    .addElement(res.getString(3));
                    VSupl     .addElement(res.getString(4));
                    VRate     .addElement(res.getString(5));
                    VDisc     .addElement(res.getString(6));
                    VVat      .addElement(res.getString(7));
                    VTax      .addElement(res.getString(8));
                    VSur      .addElement(res.getString(9));
                    VPending  .addElement(res.getString(10));
                    VCGST     .addElement(res.getString(11));
                    VSGST     .addElement(res.getString(12));
                    VIGST     .addElement(res.getString(13));
                    VCESS     .addElement(res.getString(14));
                    VGSTStatus.addElement(res.getString(15));  
                    VNet      .addElement(res.getString(16));  
                    VQty      . addElement(res.getString(17));   
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          Object RowData[][] = new Object[VNo.size()][16];
          for(int i=0;i<VNo.size();i++)
          {
               double dGross       = common.toDouble((String)VRate.elementAt(i));
               double dDisc        = dGross*common.toDouble((String)VDisc.elementAt(i))/100;
               double dCenVat      = (dGross-dDisc)*common.toDouble((String)VVat.elementAt(i))/100;
               double dTax         = (dGross-dDisc+dCenVat)*common.toDouble((String)VTax.elementAt(i))/100;
               double dSur         = (dTax)*common.toDouble((String)VSur.elementAt(i))/100;
             //  double dNet         = dGross-dDisc+dCenVat+dTax+dSur;
               double dNet     = common.toDouble((String)VNet.elementAt(i)) /  common.toDouble((String)VQty.elementAt(i));
              int iGststatus =common.toInt((String)VGSTStatus.elementAt(i));
               
               RowData[i][0]       = (String)VNo       .elementAt(i);
               RowData[i][1]       = (String)VDate     .elementAt(i);
               RowData[i][2]       = (String)VBlock    .elementAt(i);
               RowData[i][3]       = (String)VSupl     .elementAt(i);
               RowData[i][4]       = (String)VPending  .elementAt(i);
               RowData[i][5]       = (String)VRate     .elementAt(i);
               RowData[i][6]       = (String)VDisc     .elementAt(i);
               RowData[i][7]       = (String)VCGST     .elementAt(i);
               RowData[i][8]       = (String)VSGST     .elementAt(i);
               RowData[i][9]       = (String)VIGST     .elementAt(i);  
               RowData[i][10]      = (String)VCESS     .elementAt(i);
               RowData[i][11]      = (String)VVat      .elementAt(i);
               RowData[i][12]      = (String)VTax      .elementAt(i);
               RowData[i][13]      = (String)VSur      .elementAt(i);
               RowData[i][14]      = common.getRound(dNet,3);
               String SStatus="";              

               if(iGststatus==0){

                SStatus="GST";
               }else{
                SStatus="NON GST"; 
                }

               RowData[i][15]    = SStatus;
      

          }
          return RowData;
     }

     public Object[][] getPORateData()
     {
          String QS = "";
          
          QS =      " Select PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,OrdBlock.BlockName,"+SSupTable+".Name,PurchaseOrder.Rate,PurchaseOrder.DiscPer,decode(PurchaseOrder.GstStatus,0,0,PurchaseOrder.CenVatPer) as CenvatPer,decode(PurchaseOrder.GstStatus,0,0,PurchaseOrder.TaxPer) as TaxPer,decode(PurchaseOrder.GstStatus,0,0,PurchaseOrder.SurPer) as SurPer,PurchaseOrder.Qty "+
                    " ,purchaseorder.cgst,purchaseorder.sgst,purchaseorder.igst,purchaseorder.cess,purchaseorder.gststatus,PurchaseOrder.Net as Net  "+
                    " From PurchaseOrder Inner Join OrdBlock On OrdBlock.Block=PurchaseOrder.OrderBlock "+
                    " and PurchaseOrder.WA < 9 And PurchaseOrder.Item_Code = '"+TMatCode.getText()+"'"+
                    " and PurchaseOrder.MillCode="+iMillCode+
                    " Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = PurchaseOrder.Sup_Code "+
                    " Union All "+
                    " Select PYOrder.OrderNo,PYOrder.OrderDate,OrdBlock.BlockName,"+SSupTable+".Name,PYOrder.Rate,PYOrder.DiscPer,PYOrder.CenVatPer,PYOrder.TaxPer,PYOrder.SurPer,PYOrder.Qty "+
                    " ,0 as cgst , 0 as sgst,0 as igst,0 as cess ,1 as GSTStatus,PYOrder.Net as Net "+
                    " From PYOrder Inner Join OrdBlock On OrdBlock.Block=PYOrder.OrderBlock "+
                    " and PYOrder.Item_Code = '"+TMatCode.getText()+"'"+
                    " and PYOrder.MillCode="+iMillCode+
                    " Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = PYOrder.Sup_Code "+
                    " Order By 2 Desc";

          Vector VNo    = new Vector();
          Vector VDate  = new Vector();
          Vector VBlock = new Vector();
          Vector VSupl  = new Vector();
          Vector VRate  = new Vector();
          Vector VDisc  = new Vector();
          Vector VVat   = new Vector();
          Vector VTax   = new Vector();
          Vector VSur   = new Vector();
          Vector VQty   = new Vector();

          Vector VCGST   = new Vector();
          Vector VSGST   = new Vector();
          Vector VIGST   = new Vector();
          Vector VCESS   = new Vector();
          Vector VGSTStatus= new Vector();   
          Vector VNet      = new Vector();
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    VNo       .addElement(res.getString(1));
                    VDate     .addElement(common.parseDate(res.getString(2)));
                    VBlock    .addElement(res.getString(3));
                    VSupl     .addElement(res.getString(4));
                    VRate     .addElement(res.getString(5));
                    VDisc     .addElement(res.getString(6));
                    VVat      .addElement(res.getString(7));
                    VTax      .addElement(res.getString(8));
                    VSur      .addElement(res.getString(9));
                    VQty      .addElement(res.getString(10));
                    VCGST     .addElement(res.getString(11));
                    VSGST     .addElement(res.getString(12));
                    VIGST     .addElement(res.getString(13));
                    VCESS     .addElement(res.getString(14));
                    VGSTStatus.addElement(res.getString(15));  
                    VNet      .addElement(res.getString(16));    
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          
          Object RowData[][] = new Object[VNo.size()][16];
          for(int i=0;i<VNo.size();i++)
          {
               double dGross   = common.toDouble((String)VRate.elementAt(i));
               double dDisc    = dGross*common.toDouble((String)VDisc.elementAt(i))/100;
               double dCenVat  = (dGross-dDisc)*common.toDouble((String)VVat.elementAt(i))/100;
               double dTax     = (dGross-dDisc+dCenVat)*common.toDouble((String)VTax.elementAt(i))/100;
               double dSur     = (dTax)*common.toDouble((String)VSur.elementAt(i))/100;
             
               double dNet     = common.toDouble((String)VNet.elementAt(i)) /  common.toDouble((String)VQty.elementAt(i));
              // double dNet     = dGross-dDisc+dCenVat+dTax+dSur;

              int iGststatus =common.toInt((String)VGSTStatus.elementAt(i));

          
               RowData[i][0]   = (String)VNo.elementAt(i);
               RowData[i][1]   = (String)VDate.elementAt(i);
               RowData[i][2]   = (String)VBlock.elementAt(i);
               RowData[i][3]   = (String)VSupl.elementAt(i);
               RowData[i][4]   = (String)VQty.elementAt(i);
               RowData[i][5]   = (String)VRate.elementAt(i);
               RowData[i][6]   = (String)VDisc.elementAt(i);
               RowData[i][7]   = (String)VCGST     .elementAt(i);
               RowData[i][8]   = (String)VSGST     .elementAt(i);
               RowData[i][9]   = (String)VIGST     .elementAt(i);  
               RowData[i][10]  = (String)VCESS     .elementAt(i);
               RowData[i][11]  = (String)VVat.elementAt(i);
               RowData[i][12]  = (String)VTax.elementAt(i);
               RowData[i][13]  = (String)VSur.elementAt(i);
               RowData[i][14]  = common.getRound(dNet,3);

               String SStatus="";              

               if(iGststatus==0){

                SStatus="GST";
               }else{
                SStatus="NON GST"; 
                }

               RowData[i][15]    = SStatus;
          }
          return RowData;
     }
     
     public Object[][] getReceiptData()
     {
          String QS = "";
          
          QS =      " Select Grn.GrnNo,Grn.GrnDate,OrdBlock.BlockName,"+SSupTable+".Name,Grn.Grnqty "+
                    " From Grn Inner Join "+SSupTable+" on Grn.sup_code="+SSupTable+".Ac_Code "+
                    " and Grn.Code ='"+TMatCode.getText().trim()+"'" +
                    " and Grn.MillCode="+iMillCode+
                    " Inner Join OrdBlock On OrdBlock.Block=Grn.GrnBlock "+
                    " Union All "+
                    " Select PyGrn.GrnNo,PyGrn.GrnDate,OrdBlock.BlockName,"+SSupTable+".Name,PyGrn.Grnqty "+
                    " From PyGrn Inner Join "+SSupTable+" on PyGrn.sup_code="+SSupTable+".Ac_Code "+
                    " and PyGrn.Code ='"+TMatCode.getText().trim()+"' "+
                    " and PyGrn.MillCode="+iMillCode+
                    " Inner Join OrdBlock On OrdBlock.Block=PyGrn.GrnBlock ";

          String QS1 = "Select * from ("+QS+") Order by 2 Desc";

          Vector VGRNNo = new Vector();
          Vector VDate  = new Vector();
          Vector VBlock = new Vector();
          Vector VSupl  = new Vector();
          Vector VQty   = new Vector();
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet res = stat.executeQuery(QS1);
               while(res.next())
               {
                    VGRNNo    .addElement(res.getString(1));
                    VDate     .addElement(common.parseDate(res.getString(2)));
                    VBlock    .addElement(res.getString(3));
                    VSupl     .addElement(res.getString(4));
                    VQty      .addElement(res.getString(5));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          
          Object RowData[][] = new Object[VGRNNo.size()][11];
          
          for(int i=0;i<VGRNNo.size();i++)
          {
               RowData[i][0] = (String)VGRNNo.elementAt(i);
               RowData[i][1] = (String)VDate .elementAt(i);
               RowData[i][2] = (String)VBlock.elementAt(i);
               RowData[i][3] = (String)VSupl .elementAt(i);
               RowData[i][4] = (String)VQty  .elementAt(i);
          }
          return RowData;
     }
     
     public void setPOMinMaxRate(JTextField minrate,JTextField minsupl,JTextField minref,JTextField maxrate,JTextField maxsupl,JTextField maxref)
     {
          ResultSet result    = null;
          String    QSMax     = "";
          String    QSMin     = "";

          maxrate   . setText("");
          maxsupl   . setText("");
          maxref    . setText("");
          minrate   . setText("");
          minsupl   . setText("");
          minref    . setText("");

          String QSString="";

          QSString =     " select count(*) from PurchaseOrder"+
                         " where PurchaseOrder.Item_Code = '"+TMatCode.getText()+"' "+
                         " and PurchaseOrder.MillCode="+iMillCode+
                         " and PurchaseOrder.OrderDate>='"+SStDate+"' and "+
                         " PurchaseOrder.OrderDate <='"+SEnDate+"'";

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();
               Statement      stat           = theConnection.createStatement();
                              result         = stat    . executeQuery(QSString);

                              result         . next();

               int            iCount         = result  . getInt(1);
                              result         . close();

               if(iCount>0)
               {
                    QSMax =   " Select PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,OrdBlock.BlockName,"+SSupTable+".Name,Avg(PurchaseOrder.Net/PurchaseOrder.Qty)"+
                              " From PurchaseOrder Inner Join OrdBlock On OrdBlock.Block=PurchaseOrder.OrderBlock "+
                              " and PurchaseOrder.Item_Code = '"+TMatCode.getText()+"' "+
                              " and PurchaseOrder.MillCode="+iMillCode+
                              " and PurchaseOrder.OrderDate>='"+SStDate+"' and "+
                              " PurchaseOrder.OrderDate <='"+SEnDate+"'"+
                              " Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = PurchaseOrder.Sup_Code "+
                              " Group By PurchaseOrder.Item_Code,PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,OrdBlock.BlockName,"+SSupTable+".Name,PurchaseOrder.MillCode "+
                              " Order By 5 ";
                    
                    QSMin =   " Select PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,OrdBlock.BlockName,"+SSupTable+".Name,Avg(PurchaseOrder.Net/PurchaseOrder.Qty)"+
                              " From PurchaseOrder Inner Join OrdBlock On OrdBlock.Block=PurchaseOrder.OrderBlock "+
                              " and PurchaseOrder.Item_Code = '"+TMatCode.getText()+"'"+
                              " and PurchaseOrder.MillCode="+iMillCode+
                              " and PurchaseOrder.OrderDate>='"+SStDate+"' and "+
                              " PurchaseOrder.OrderDate <='"+SEnDate+"'"+
                              " Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = PurchaseOrder.Sup_Code "+
                              " Group By PurchaseOrder.Item_Code,PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,OrdBlock.BlockName,"+SSupTable+".Name,PurchaseOrder.MillCode "+
                              " Order By 5 Desc ";

                    result   = stat.executeQuery(QSMax);
                    while(result.next())
                    {
                         maxrate   . setText(common.getRound(common.toDouble(result.getString(5)),4));
                         maxsupl   . setText(result.getString(4));
                         maxref    . setText(result.getString(3)+"-"+result.getString(1)+"Dt. "+common.parseDate(result.getString(2)));
                    }
                    result    . close();
     
                    result = stat.executeQuery(QSMin);
                    while(result.next())
                    {
                         minrate   . setText(common.getRound(common.toDouble(result.getString(5)),4));
                         minsupl   . setText(result.getString(4));
                         minref    . setText(result.getString(3)+"-"+result.getString(1)+"Dt. "+common.parseDate(result.getString(2)));
                    }
                    result    . close();
                    stat      . close();
               }
          }
          catch(Exception ex)
          {
               System.out.println(QSMax);
               System.out.println(QSMin);
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setPOMinMaxQty(JTextField minqty,JTextField minsupl,JTextField minref,JTextField maxqty,JTextField maxsupl,JTextField maxref)
     {
          String QSMax = "";
          String QSMin = "";
          
          QSMax =   " Select PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,OrdBlock.BlockName,"+SSupTable+".Name,Sum(PurchaseOrder.Qty)"+
                    " From (PurchaseOrder Inner Join OrdBlock On OrdBlock.Block=PurchaseOrder.OrderBlock) "+
                    " Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = PurchaseOrder.Sup_Code "+
                    " Group By PurchaseOrder.Item_Code,PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,OrdBlock.BlockName,"+SSupTable+".Name,PurchaseOrder.MillCode "+
                    " Having PurchaseOrder.Item_Code = '"+TMatCode.getText()+"'"+
                    " And PurchaseOrder.MillCode="+iMillCode+
                    " and PurchaseOrder.OrderDate>='"+SStDate+"' and "+
                    " PurchaseOrder.OrderDate <='"+SEnDate+"'"+
                    " Order By 5 ";
          
          QSMin =   " Select PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,OrdBlock.BlockName,"+SSupTable+".Name,Sum(PurchaseOrder.Qty)"+
                    " From (PurchaseOrder Inner Join OrdBlock On OrdBlock.Block=PurchaseOrder.OrderBlock) "+
                    " Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = PurchaseOrder.Sup_Code "+
                    " Group By PurchaseOrder.Item_Code,PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,OrdBlock.BlockName,"+SSupTable+".Name,PurchaseOrder.MillCode "+
                    " Having PurchaseOrder.Item_Code = '"+TMatCode.getText()+"'"+
                    " And PurchaseOrder.MillCode="+iMillCode+
                    " and PurchaseOrder.OrderDate>='"+SStDate+"' and "+
                    " PurchaseOrder.OrderDate <='"+SEnDate+"'"+
                    " Order By 5 Desc ";
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet res1 = stat.executeQuery(QSMax);
               while(res1.next())
               {
                    maxqty    .setText(res1.getString(5));
                    maxsupl   .setText(res1.getString(4));
                    maxref    .setText(res1.getString(3)+"-"+res1.getString(1)+"Dt. "+common.parseDate(res1.getString(2)));
               }
               res1 . close();
               ResultSet res2 = stat.executeQuery(QSMin);
               while(res2.next())
               {
                    minqty    .setText(res2.getString(5));
                    minsupl   .setText(res2.getString(4));
                    minref    .setText(res2.getString(3)+"-"+res2.getString(1)+"Dt. "+common.parseDate(res2.getString(2)));
               }
               res2 . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     
     public Object[][] getOrdMonVar()
     {
          Object RowData[][] = new Object[3][13];
          for(int i=0;i<RowData.length;i++)
          {
               for (int j=0;j<13;j++)
                    RowData[i][j]="0";
          }
          
          String QS1 = "";
          String QS2 = "";
          
          QS1 =     " SELECT substr(PurchaseOrder.OrderDate,5,2),sum(PurchaseOrder.Qty) "+
                    " FROM PurchaseOrder "+
                    " Where PurchaseOrder.OrderDate>='"+SStDate+"' and "+
                    " PurchaseOrder.OrderDate <='"+SEnDate+"'"+
                    " GROUP BY PurchaseOrder.Item_Code,substr(PurchaseOrder.OrderDate,5,2),PurchaseOrder.MillCode "+
                    " Having PurchaseOrder.Item_Code = '"+TMatCode.getText()+"'"+
                    " And PurchaseOrder.MillCode="+iMillCode;
          
          QS2 =     " SELECT substr(PurchaseOrder.OrderDate,5,2),sum(PurchaseOrder.Net) "+
                    " FROM PurchaseOrder "+
                    " Where PurchaseOrder.OrderDate>='"+SStDate+"' and "+
                    " PurchaseOrder.OrderDate <='"+SEnDate+"'"+
                    " GROUP BY PurchaseOrder.Item_Code,substr(PurchaseOrder.OrderDate,5,2),PurchaseOrder.MillCode "+
                    " Having PurchaseOrder.Item_Code = '"+TMatCode.getText()+"'"+
                    " And PurchaseOrder.MillCode="+iMillCode;

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet res1 = stat.executeQuery(QS1);
               RowData[0][0] = "Quantity";
               while(res1.next())
               {
                    int       iMon      = res1.getInt(1);
                    String    SQty      = res1.getString(2);
                    int       index     = (iMon >=4 && iMon<=12 ? iMon-3:iMon+9);
                    RowData[0][index]   = SQty;
               }
               res1 . close();
               ResultSet res2 = stat.executeQuery(QS2);
               RowData[1][0] = "Net Value";
               while(res2.next())
               {
                    int       iMon      = res2.getInt(1);
                    String    SValue    = res2.getString(2);
                    int       index     = (iMon >=4 && iMon<=12 ? iMon-3:iMon+9);
                    RowData[1][index]   = SValue;
               }
               res2 . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

          RowData[2][0] = "Net Rate";

          for(int i=1;i<13;i++)
          {
               double dQty   = common.toDouble((String)RowData[0][i]);
               double dValue = common.toDouble((String)RowData[1][i]);
               try
               {
                    RowData[2][i]  = common.getRound(dValue/dQty,3);
               }
               catch(Exception ex)
               {
               }
          }
          return RowData;
     }

     public Object[][] getDeptOrdQtyMonVar()
     {
          int ctr=-1;
          
          String QS2 = "";
          String QS1 = "";
          
          QS2 =     " SELECT Count(DeptCode) AS CountOfDept_Code "+
                    " FROM (SELECT PurchaseOrder.Dept_Code as DeptCode"+
                    " FROM PurchaseOrder "+
                    " Where PurchaseOrder.OrderDate>='"+SStDate+"' and "+
                    " PurchaseOrder.OrderDate <='"+SEnDate+"' "+
                    " GROUP BY PurchaseOrder.Dept_Code,PurchaseOrder.Item_Code,PurchaseOrder.MillCode "+
                    " HAVING PurchaseOrder.Item_Code='"+TMatCode.getText()+"' "+
                    " and PurchaseOrder.MillCode="+iMillCode+" ) ";
          
          QS1 =     " SELECT PurchaseOrder.Dept_Code,Dept.Dept_Name,substr(PurchaseOrder.OrderDate,5,2),sum(PurchaseOrder.Qty) "+
                    " FROM PurchaseOrder Inner Join Dept on PurchaseOrder.Dept_Code = Dept.Dept_Code "+
                    " Where PurchaseOrder.OrderDate>='"+SStDate+"' and "+
                    " PurchaseOrder.OrderDate <='"+SEnDate+"' "+
                    " GROUP BY PurchaseOrder.Dept_Code,PurchaseOrder.Item_Code,Dept.Dept_Name,substr(PurchaseOrder.OrderDate,5,2), PurchaseOrder.MillCode "+
                    " Having PurchaseOrder.Item_Code = '"+TMatCode.getText()+"' "+
                    " and PurchaseOrder.MillCode="+iMillCode+
                    " Order By 1 ";

          int       ArraySize = common.toInt(getID(QS2));
          Object    RowData[][] = new Object[ArraySize][13];
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet res1           = stat.executeQuery(QS1);
               String    SODeptCode     = " ";
               while(res1.next())
               {
                    String SDeptCode = res1.getString(1);
                    String SDept     = res1.getString(2);
                    int iMon         = res1.getInt(3);
                    String SQty      = res1.getString(4);
                    int index        = (iMon >=4 && iMon<=12 ? iMon-3:iMon+9);
                    if(!SDeptCode.equals(SODeptCode))
                    {
                         ctr++;
                         SODeptCode = SDeptCode;
                         RowData[ctr][0]=SDept;
                    }
                    RowData[ctr][index] = SQty;
               }
               res1 . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return RowData;
     }
     
     public Object[][] getDeptOrdNetMonVar()
     {
          int ctr=-1;
          
          String QS2 = "";
          String QS1 = "";
          
          QS2 =     " SELECT Count(DeptCode) AS CountOfDept_Code "+
                    " FROM (SELECT PurchaseOrder.Dept_Code as DeptCode"+
                    " FROM PurchaseOrder "+
                    " Where PurchaseOrder.OrderDate>='"+SStDate+"' and "+
                    " PurchaseOrder.OrderDate <='"+SEnDate+"' "+
                    " GROUP BY PurchaseOrder.Dept_Code,PurchaseOrder.Item_Code,PurchaseOrder.MillCode  "+
                    " HAVING PurchaseOrder.Item_Code='"+TMatCode.getText()+"' "+
                    " and PurchaseOrder.MillCode="+iMillCode+" ) ";
          
          QS1 =     " SELECT PurchaseOrder.Dept_Code,Dept.Dept_Name,substr(PurchaseOrder.OrderDate,5,2),sum(PurchaseOrder.Net) "+
                    " FROM PurchaseOrder Inner Join Dept on PurchaseOrder.Dept_Code = Dept.Dept_Code "+
                    " Where PurchaseOrder.OrderDate>='"+SStDate+"' and "+
                    " PurchaseOrder.OrderDate <='"+SEnDate+"' "+
                    " GROUP BY PurchaseOrder.Dept_Code,PurchaseOrder.Item_Code,Dept.Dept_Name,substr(PurchaseOrder.OrderDate,5,2),PurchaseOrder.MillCode "+
                    " Having PurchaseOrder.Item_Code = '"+TMatCode.getText()+"' "+
                    " and PurchaseOrder.MillCode="+iMillCode+
                    " Order By 1";

          int ArraySize = common.toInt(getID(QS2));

          Object RowData[][] = new Object[ArraySize][13];
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet res1 = stat.executeQuery(QS1);
               String SODeptCode = " ";
               while(res1.next())
               {
                    String SDeptCode = res1.getString(1);
                    String SDept     = res1.getString(2);
                    int iMon         = res1.getInt(3);
                    String SNet      = res1.getString(4);
                    int index        = (iMon >=4 && iMon<=12 ? iMon-3:iMon+9);
                    if(!SDeptCode.equals(SODeptCode))
                    {
                         ctr++;
                         SODeptCode = SDeptCode;
                         RowData[ctr][0]=SDept;
                    }
                    RowData[ctr][index] = SNet;
               }
               res1 . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return RowData;
     }

     public String getID(String QueryString)
     {
          String ID = "";
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();

               ResultSet theResult      = stat.executeQuery(QueryString);
               while(theResult.next())
               {
                    ID = theResult.getString(1);
               }
               theResult . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);  
          }
          return ID;
     }
     
     public Object[][] getConMonVar()
     {
          Object RowData[][] = new Object[3][13];
          for(int i=0;i<RowData.length;i++)
          {
               for (int j=0;j<13;j++)
                    RowData[i][j]="0";
          }
          String QS1a = "";
          String QS2a = "";
          String QS1  = "";
          String QS2  = "";

          QS1a =    " SELECT substr(Issue.IssueDate,5,2) as DocMon,sum(Issue.Qty) as DocQty"+
                    " FROM Issue "+
                    " Where Issue.Code = '"+TMatCode.getText()+"'"+
                    " and Issue.IssueDate>='"+SStDate+"' and "+
                    " Issue.IssueDate <='"+SEnDate+"' "+
                    " Group By substr(Issue.IssueDate,5,2),Issue.MillCode "+
                    " Having Issue.MillCode="+iMillCode+
                    " Union All "+
                    " SELECT substr(Grn.GrnDate,5,2) as DocMon,sum(Grn.GrnQty) as DocQty "+
                    " FROM Grn "+
                    " Where Grn.GrnBlock > 1 And Grn.Code = '"+TMatCode.getText()+"'"+
                    " and Grn.GrnDate>='"+SStDate+"' and "+
                    " Grn.GrnDate <='"+SEnDate+"' "+
                    " Group By substr(Grn.GrnDate,5,2),Grn.MillCode "+
                    " Having Grn.MillCode="+iMillCode;
          
          QS2a =    " SELECT substr(Issue.IssueDate,5,2) as DocMon,sum(Issue.IssueValue) as DocValue "+
                    " FROM Issue "+
                    " Where Issue.Code = '"+TMatCode.getText()+"'"+
                    " and Issue.IssueDate>='"+SStDate+"' and "+
                    " Issue.IssueDate <='"+SEnDate+"' "+
                    " Group By substr(Issue.IssueDate,5,2),Issue.MillCode "+
                    " Having Issue.MillCode="+iMillCode+
                    " Union All "+ 
                    " SELECT substr(Grn.GrnDate,5,2) as DocMon,sum(Grn.GrnValue) as DocValue "+
                    " FROM Grn "+
                    " Where Grn.GrnBlock > 1 And Grn.Code = '"+TMatCode.getText()+"'"+
                    " and Grn.GrnDate>='"+SStDate+"' and "+
                    " Grn.GrnDate <='"+SEnDate+"' "+
                    " Group By substr(Grn.GrnDate,5,2),Grn.MillCode "+
                    " Having Grn.MillCode="+iMillCode;

          QS1 = " Select DocMon,Sum(DocQty) From ("+QS1a+") Group By DocMon";
          QS2 = " Select DocMon,Sum(DocValue) From ("+QS2a+") Group By DocMon";
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet res1   = stat.executeQuery(QS1);
               RowData[0][0]    = "Quantity";
               while(res1.next())
               {
                    int iMon    = res1.getInt(1);
                    String SQty = res1.getString(2);
                    int index   = (iMon >=4 && iMon<=12 ? iMon-3:iMon+9);
                    RowData[0][index] = SQty;
               }
               res1 . close();
               ResultSet res2   = stat.executeQuery(QS2);
               RowData[1][0]    = "Net Value";
               
               while(res2.next())
               {
                    int iMon      = res2.getInt(1);
                    String SValue = res2.getString(2);
                    int index     = (iMon >=4 && iMon<=12 ? iMon-3:iMon+9);
                    RowData[1][index] = SValue;
               }
               res2 . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          RowData[2][0] = "Net Rate";
          for(int i=1;i<13;i++)
          {
               double dQty   = common.toDouble((String)RowData[0][i]);
               double dValue = common.toDouble((String)RowData[1][i]);
               try
               {
                    RowData[2][i]  = common.getRound(dValue/dQty,3);
               }
               catch(Exception ex)
               {
               }
          }
          return RowData;
     }

     public Object[][] getMRSData()
     {
          String QS = "";
          
          QS =      " Select MRS.MRSNo,MRS.MRSDate,MRS.Qty, "+
                    " OrdBlock.BlockName,MRS.OrderNo,Dept.Dept_Name "+
                    " From MRS Inner Join OrdBlock On OrdBlock.Block=MRS.BlockCode "+
                    " and MRS.Item_Code = '"+TMatCode.getText()+"'"+
                    " and Mrs.MillCode="+iMillCode+
                    " and Mrs.MrsDate>='"+SStDate+"' and "+
                    " Mrs.MrsDate <='"+SEnDate+"' "+
                    " Inner Join Dept On MRS.Dept_Code = Dept.Dept_Code "+
                    " Order By 5,2,1";

          Vector VNo      = new Vector();
          Vector VDate    = new Vector();
          Vector VQty     = new Vector();
          Vector VBlock   = new Vector();
          Vector VOrdNo   = new Vector();
          Vector VDept    = new Vector();
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet res    = stat.executeQuery(QS);
               
               while(res.next())
               {
                    VNo       .addElement(res.getString(1));
                    VDate     .addElement(common.parseDate(res.getString(2)));
                    VQty      .addElement(res.getString(3));
                    VBlock    .addElement(res.getString(4));
                    VOrdNo    .addElement(res.getString(5));
                    VDept     .addElement(res.getString(6));
               }
               res  . next();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          Object RowData[][] = new Object[VNo.size()][6];
          for(int i=0;i<VNo.size();i++)
          {
               RowData[i][0] = (String)VNo   . elementAt(i);
               RowData[i][1] = (String)VDate . elementAt(i);
               RowData[i][2] = (String)VQty  . elementAt(i);
               RowData[i][3] = (String)VBlock. elementAt(i);
               RowData[i][4] = (String)VOrdNo. elementAt(i);
               RowData[i][5] = (String)VDept . elementAt(i);
          }
          return RowData;
     }
}
