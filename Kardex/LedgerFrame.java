package Kardex;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;

public class LedgerFrame extends JInternalFrame
{
     JPanel         TopPanel,TopLeft,TopRight,MiddlePanel,BottomPanel;
     JButton        BMaterial,BApply;
     JTextField     TMatCode;

     JLayeredPane   DeskTop;
     Vector         VCode,VName,VNameCode;
     StatusPanel    SPanel;
     Common         common = new Common();
     int            iMillCode;
     String         SStDate,SEnDate;
     String         SItemTable,SSupTable;

     public LedgerFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iMillCode,String SStDate,String SEnDate,String SItemTable,String SSupTable)
     {
          this.DeskTop    = DeskTop;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.SPanel     = SPanel;
          this.iMillCode  = iMillCode;
          this.SStDate    = SStDate;
          this.SEnDate    = SEnDate;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel       = new JPanel(true);
          TopLeft        = new JPanel(true);
          TopRight       = new JPanel(true);
          BottomPanel    = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BMaterial      = new JButton("Select a Material");
          BApply         = new JButton("Apply");
          TMatCode       = new JTextField();
          TMatCode       . setEditable(false);
     }

     public void setLayouts()
     {
          TopPanel       . setLayout(new GridLayout(1,2));
          MiddlePanel    . setLayout(new BorderLayout());
          TopLeft        . setLayout(new GridLayout(2,1));
          TopRight       . setLayout(new BorderLayout());
          BottomPanel    . setLayout(new GridLayout(2,4));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setTitle("Stock Ledger for the Selected Material");
          setBounds(0,0,650,500);
     }

     public void addComponents()
     {
          TopLeft             . add(BMaterial);
          TopLeft             . add(TMatCode);
          TopRight            . add("Center",BApply);

          TopPanel            . add(TopLeft);
          TopPanel            . add(TopRight);

          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("Center",MiddlePanel);
          getContentPane()    . add("South",BottomPanel);

          TopLeft             . setBorder(new TitledBorder("Material Selection"));
          TopRight            . setBorder(new TitledBorder("Click To View"));
          BottomPanel         . setBorder(new TitledBorder("Control Figures"));
     }

     public void addListeners()
     {
          //BMaterial           . addActionListener(new MaterialSearch(DeskTop,TMatCode,BMaterial,VCode,VName,VNameCode,iMillCode,SItemTable));
          BMaterial           . addActionListener(new ActList());
          BApply              . addActionListener(new ApplyList());
     }

     public class ActList implements ActionListener
     {
         public void actionPerformed(ActionEvent ae)
         {
             ItemSearchList itemsearchlist = new ItemSearchList(TMatCode,BMaterial,iMillCode,SItemTable);
         }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               MiddlePanel    . removeAll();
               BottomPanel    . removeAll();

               Item1     IC             = new Item1(TMatCode.getText(),SStDate,SEnDate,iMillCode,SItemTable,SSupTable);
               TabReport tabreport      = new TabReport(IC.getRowData(),IC.getColumnData(),IC.getColumnType());
                         MiddlePanel    . add(tabreport);

               BottomPanel    . add(new JLabel("Receipt Qty"));
               BottomPanel    . add(new JLabel("Receipt Value"));
               BottomPanel    . add(new JLabel("Issue Qty"));
               BottomPanel    . add(new JLabel("Issue Value"));

               BottomPanel    . add(new JLabel(IC.getGRNQty()));
               BottomPanel    . add(new JLabel(IC.getGRNValue()));
               BottomPanel    . add(new JLabel(IC.getIssueQty()));
               BottomPanel    . add(new JLabel(IC.getIssueValue()));

               DeskTop        . updateUI();
          }
     }
}
