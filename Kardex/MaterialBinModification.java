package Kardex;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JFileChooser;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class MaterialBinModification //extends JInternalFrame
{
     //JLayeredPane   Layer;

     JTextField     TMatCode,TStockType,TOldLocation,TNewLocation;
     MyTextField    TMatName;

     JButton        BOk,BCancel;
	 
     JPanel         NamePanel;
     JPanel         Name1Panel,BottomPanel;

     JTabbedPane    JTP;

     Common         common    = new Common();
     int            iMillCode,iAuthCode,iUserCode;
     String         SItemTable,SItemCode,SItemName,SPhotoName;
     int            iRow;

     KardexFrame MMF;

     String         SStkGroup,SOldBin;

     boolean        bStatFlag = false;
     boolean        bAutoCommitFlag = true;

     Connection     theConnection  = null;
     Connection     theDConnection = null;
	 
     JDialog    dialog;

	 Object         RowData[][];
	 
	 //TabReport1 tabreportnew;
	 

     //public MaterialBinModification(JLayeredPane Layer,int iMillCode,String SItemTable,String SItemCode,String SItemName,String SStkGroup,KardexFrame MMF,int iRow,String SOldBin)
	 public MaterialBinModification(int iMillCode,String SItemTable,String SItemCode,String SItemName,String SStkGroup,int iRow,String SOldBin,Object[][] RowData,KardexFrame MMF)
     {
		 
          //this.Layer      	= Layer;
          this.iMillCode  	= iMillCode;
		  this.SItemTable	= SItemTable;
		  this.SItemCode	= SItemCode;
          this.SItemName  	= SItemName;
          this.SStkGroup  	= SStkGroup;
          this.MMF        	= MMF;
          this.iRow       	= iRow;
		  this.SOldBin		= SOldBin;
		  this.RowData	  = RowData;

		  

          createComponents();
          setLayouts();
          addListeners();
          setPreset();
		  
          addComponents();
     }

     public void createComponents()
     {
          TMatCode       = new JTextField();
          TMatName       = new MyTextField(50);
		  
		  TStockType	 = new JTextField();
		  TOldLocation   = new JTextField();
		  TNewLocation   = new JTextField();
		  
		  dialog     = new JDialog(new Frame(),"",true);

          JTP            = new JTabbedPane(JTabbedPane.RIGHT);

          BOk            = new JButton("Okay");
          BCancel        = new JButton("Cancel");

          NamePanel      = new JPanel();
          BottomPanel    = new JPanel();

          Name1Panel     = new JPanel();
          
          TMatCode       . setEditable(false);
		  TStockType	 . setEditable(false);
		  TOldLocation   . setEditable(false);
		  
     }

     public void setLayouts()
     {
          /*setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          setTitle("Administration Utility for Materials");*/

          NamePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          Name1Panel.setLayout(new GridLayout(5,2,10,10));
     }

     public void addComponents()
     {
          Name1Panel.add(new JLabel("Material Code"));
          Name1Panel.add(TMatCode);
          Name1Panel.add(new JLabel("Material Name"));
          Name1Panel.add(TMatName);
          Name1Panel.add(new JLabel("Stock Type"));
		  Name1Panel.add(TStockType);
          Name1Panel.add(new JLabel("Old Location"));
		  Name1Panel.add(TOldLocation);
          Name1Panel.add(new JLabel("New Location"));
		  Name1Panel.add(TNewLocation);

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          NamePanel.add("North",Name1Panel);

          JTP.addTab("Naming & Identification",NamePanel);

          //getContentPane().add("Center",JTP);
          //getContentPane().add("South",BottomPanel);
		  
          dialog.getContentPane().add("Center",JTP);
          dialog.getContentPane().add("South",BottomPanel);
          dialog.setBounds(50,50,500,500);
          dialog.setVisible(true);

		  
     }

     public void addListeners()
     {
          BOk.addActionListener(new ActList());
     }

     public void setPreset()
     {
          TMatCode.setText(SItemCode);
          TMatName.setText(SItemName);
          TMatName.setEditable(false);
		  TStockType.setText(SStkGroup);
		  TOldLocation.setText(SOldBin);
     }


     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
			  
			   if(common.parseNull(TNewLocation.getText()).length()>0) {
				   
				   updateMaterial();

				   try
				   {
						if(bAutoCommitFlag)
						{
							 theConnection  . commit();
							 System         . out.println("Commit");
							 theConnection  . setAutoCommit(true);
							 //removeHelpFrame();
							  //RowData[iRow][2]  = common.parseNull(TNewLocation.getText());
							MMF.newLoad();
			                dialog.setVisible(false);
							 //tabreportnew[iRow].ReportTable.requestFocus();


						}
						else
						{
							 theConnection  . rollback();
							 System         . out.println("RollBack");
							 theConnection  . setAutoCommit(true);
							 BOk.setEnabled(true);
						}
				   }catch(Exception ex)
				   {
						ex.printStackTrace();
				   }
				   
			   }   
          }
     }



     public void updateMaterial()
     {
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                              theConnection  = oraConnection.getConnection();
                              theConnection  . setAutoCommit(true);
               
			   
			   


               if(theConnection.getAutoCommit())
                         theConnection  . setAutoCommit(false);

			   PreparedStatement ps          = theConnection.prepareStatement(getUpdateQS());					 
               ps.executeUpdate();
               ps.close();
			   

			   PreparedStatement ps1          = theConnection.prepareStatement(getHistoryQS());					 
               ps1.executeUpdate();
               ps1.close();
			   

               //MMF.tabreport.RowData[iRow][2] = common.parseNull(TNewLocation.getText());

          }catch(Exception e)
          {
               System.out.println(e);
               bAutoCommitFlag     = false;
               e.printStackTrace();
          }
     }
	 
	 public String getHistoryQS() {
		 
          String QS = "";
		  
		  QS =" insert into binchange_history  (Id,stocktype,oldbin,newbin,entrydate) values(binchange_history_seq.nextval, "+ 
		      " '"+SStkGroup+"','"+SOldBin+"','"+common.parseNull(TNewLocation.getText())+"',sysdate) " ;

		  System.out.println("QS"+QS);

          return QS;
	 }

     public String getUpdateQS()
     {
          String QS = "";
		  
		  if(SStkGroup.equals("SERVICE STOCK")) {
			  
		  QS = " Update "+SItemTable+" set ITEMLOCNAME='"+common.parseNull(TNewLocation.getText())+"' "+
			   " Where Item_Code='"+SItemCode+"'";

			  
		  }
		  else {
		  QS = " Update "+SItemTable+" set LOCNAME='"+common.parseNull(TNewLocation.getText())+"' "+
			   " Where Item_Code='"+SItemCode+"'";
		  }
		  
		  
		  System.out.println("QS"+QS);
		  


          return QS;
     }

     public void removeHelpFrame()
     {
          /*Layer.remove(this);
          Layer.repaint();
          Layer.updateUI();*/
     }


}
