package Kardex;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.sql.*;
import util.*;
import guiutil.*;
import java.net.InetAddress;
import jdbc.*;



public class MaterialClass
{
    String SMaterialName = ""; 
	String SShiftType = "";
	
    Common common = new Common();  
    public ArrayList AList;
	
    public MaterialClass(String SMaterialName,String SShiftType)
    {
        this.SMaterialName    = SMaterialName ;
		this.SShiftType       = SShiftType ;
		
        AList =  new ArrayList();
    }
   
    public void appendDetails(String SMaterialName,String SMaterialCode,double dQuantity,double dRate,double dAmount,String SPaymentDate,String SUOMName,String SShiftType)
    {
		HashMap theMap = new HashMap();
		
		theMap.put("MaterialName",SMaterialName);
		theMap.put("MaterialCode",SMaterialCode);
		theMap.put("Quantity",dQuantity);
		theMap.put("Rate",dRate);
		theMap.put("Amount",dAmount);
		theMap.put("PaymentDate",SPaymentDate);
		theMap.put("UOMName",SUOMName);
		theMap.put("ShiftType",SShiftType);
		
		AList . add(theMap);
      
    }
	
         
}
