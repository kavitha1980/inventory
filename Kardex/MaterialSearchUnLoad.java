/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Kardex;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;


public class MaterialSearchUnLoad implements ActionListener 
{
     JLayeredPane   Layer;
     JTextField     TMatCode,TMatName;
     JButton        BMatName;
	 Vector         VName,VCode;
     Vector         VNameCode,VNameCode1;
     int            iMillCode;
     String         SItemTable;

     JTextField     TIndicator;
     JList          BrowList;
     JScrollPane    BrowScroll;
     JPanel         LeftPanel;
     JInternalFrame MaterialFrame;
     
     String         SName     = "",SCode= "";
     
     int            iSelectedRow;
     String         str       = "";
     ActionEvent    ae;
     Common         common    = new Common();
     JPanel         BottomPanel;
     JButton        BRefresh;
     int            iMaterialType;
     String         SEntryDate;
     StoreMaterialUnLoad theStoreMaterialUnLoad;
     MaterialSearchUnLoad(JLayeredPane Layer,JTextField TMatCode,JTextField TMatName,Vector VCode,Vector VName,Vector VNameCode,int iMillCode,String SItemTable,int iMaterialType,String SEntryDate) //,JTextField TMatCode
     {      

          this.Layer          = Layer;
          this.TMatCode       = TMatCode;
		  this.TMatName       = TMatName;
          //this.BMatName       = BMatName;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;
          this.iMaterialType  = iMaterialType;
          this.SEntryDate     = SEntryDate;

          setDataIntoVector();

          if(iMaterialType==4)
		 BrowList            = new JList(VNameCode1);
          else
		BrowList            = new JList(VNameCode);
          
          BrowList            . setFont(new Font("monospaced", Font.PLAIN, 11));
          
          BrowScroll          = new JScrollPane(BrowList);

          
          LeftPanel           = new JPanel(true);
          TIndicator          = new JTextField();
          TIndicator          . setEditable(false);

          MaterialFrame       = new JInternalFrame("Materials Selector");
          MaterialFrame       . show();
          MaterialFrame       . setBounds(80,100,550,350);
          MaterialFrame       . setClosable(true);
          MaterialFrame       . setResizable(true);

//          Layer.add(MaterialFrame);

          BrowList            . addKeyListener(new KeyList());

          System.out.println("After show");
          
          BRefresh            = new JButton("Refresh");
          BottomPanel         = new JPanel(true);
          BottomPanel         . setLayout(new GridLayout(1,2));
          BottomPanel         . add(TIndicator);
          BottomPanel         . add(BRefresh);
          
          MaterialFrame       . getContentPane().setLayout(new BorderLayout());
          MaterialFrame       . getContentPane().add("South",BottomPanel);
          MaterialFrame       . getContentPane().add("Center",BrowScroll);
                           
          
          BRefresh            . addActionListener(new RefreshList());
          
          activate();     
     }
     
     public void actionPerformed(ActionEvent ae)
     {
          this.ae = ae;
          removeHelpFrame();
          try
          {
               Layer          . add(MaterialFrame);
               MaterialFrame  . moveToFront();
               MaterialFrame  . setSelected(true);
               MaterialFrame  . show();
               BrowList       . requestFocus();
               setCursor(BMatName.getText());
               Layer          . repaint();
             
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public void activate()
     {

          try
          {
               Layer          . add(MaterialFrame);
               MaterialFrame  . moveToFront();
               MaterialFrame  . setSelected(true);
               MaterialFrame  . show();
               BrowList       . requestFocus();
//               setCursor(BMatName.getText());
               Layer          . repaint();
             
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }


     }

     public class RefreshList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               
               setDataIntoVector();
		
               if(iMaterialType==4)
               BrowList.setListData(VNameCode1);
               else
               BrowList.setListData(VNameCode);
	
          }
     }
     
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar  = ke.getKeyChar();
                    lastchar  = Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str  = str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str  = str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==116)    // F5 is pressed
               {
                    setDataIntoVector();
                     if(iMaterialType==4)
	                    BrowList  .setListData(VNameCode1);
                     else
 	                    BrowList  .setListData(VNameCode);
               }
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                   
                    int       index          = BrowList.getSelectedIndex();
		    String    SMatNameCode   = "";
		    if(iMaterialType == 4)
                    	SMatNameCode   = (String)VNameCode1.elementAt(index);
		    else
                        SMatNameCode   = (String)VNameCode.elementAt(index);

                    String    SMatName       = (String)VName.elementAt(index);
                    String    SMatCode       = (String)VCode.elementAt(index);
                    addMatDet(SMatName,SMatCode,index);
                    str  = "";
                    removeHelpFrame();
               }
          }
     }
     
     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<VName.size();index++)
          {
               String str1 = ((String)VName.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }
     
     public void setCursor(String xtr)
     {
          int index=0;
          for(index=0;index<VName.size();index++)
          {
               String str1 = ((String)VName.elementAt(index)).toUpperCase();
               if(str1.startsWith(xtr))
               {
                    BrowList  . setSelectedIndex(index);
                    BrowList  . ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }
     
     public void removeHelpFrame()
     {
         
          try
          {
               System.out.println(" Calling Here--->");

               Layer     . remove(MaterialFrame);
               Layer     . repaint();
               Layer     . updateUI();
               BMatName  . requestFocus();
          }
          catch(Exception ex) { }
     }
    public boolean addMatDet(String SMatName,String SMatCode,int index)
     {
          TMatCode  . setText(SMatCode);
          TMatName  . setText(SMatName);
          return true;    
     }
     
     public void setDataIntoVector()
     {

          VName     . removeAllElements();
          VCode     . removeAllElements();
	 
          VNameCode . removeAllElements();
          VNameCode1 = new Vector();
          
          String QString = "";
          if(iMillCode==0)
          {
	       if(iMaterialType == 4)
	       {
               QString = " Select Item_Name,Item_Code,'' as UoMName From GateInward where GiDate="+SEntryDate+" and MillCode=0 and InwNo in(0,1,2,10) "+
                              " Order By Item_Name";

               }
               else
               {	
               		QString = " Select Item_Name,Item_Code,Uom.UoMName From InvItems "+
                         " Inner Join Uom on InvItems.UomCode=Uom.UomCode "+
                         " Order By Item_Name";
               }
          }
          else
          {

		if(iMaterialType == 4)
	        {
              QString = " Select Item_Name,Item_Code,'' as UoMName From GateInward where GiDate="+SEntryDate+" and MillCode=1 and InwNo in(0,1,2,10) "+
                              " Order By Item_Name";

                }
		else
                {
                   QString = " Select Item_Name,"+SItemTable+".Item_Code,Uom.UoMName From "+SItemTable+" "+
                         " Inner Join InvItems on InvItems.Item_Code = "+SItemTable+".Item_Code"+
                         " Inner Join Uom on InvItems.UomCode=Uom.UomCode "+
                         " Order By Item_Name";
                }
          }
          
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
               ResultSet      res            = stat.executeQuery(QString);
               while(res.next())
               {
                    String Str1 = common.parseNull(res.getString(1));
                    String Str2 = common.parseNull(res.getString(2));
                    String Str3 = common.parseNull(res.getString(3));


                    VName.addElement(Str1);
                    VCode.addElement(common.parseNull(Str2));

                    if(iMaterialType ==4)
                          VNameCode1.addElement(Str1);
                    else      
                          VNameCode.addElement(Str1+"~"+Str2);
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
		ex.printStackTrace();
               System.out.println(ex);
          }
     }
}
