package Kardex;

import java.awt.*;
import java.awt.event.*;

import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import util.*;
import javax.swing.border.*;
import jdbc.*;
import java.sql.*;
import java.net.*;
import java.io.*;
import guiutil.*;

public class MelangeStockUpdationFrame extends JInternalFrame
{

     JPanel       TopPanel,MiddlePanel,BottomPanel;

     JLayeredPane theLayer;
     int iUserCode,iMillCode;
     String SItemTable,SSupTable;
     String SYearCode;

     String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

     JTextArea    TInfo;
     JButton      BApply;

     Common    common = new Common();

     String SSeleMillCode="";
     String SSItemTable="";
     String SSSupTable="";
     String SSeleUnitCode="";
     String SSeleDeptCode="";
     String SSeleGroupCode="";

     Vector VStockCode,VStockQty,VStockRate,VStockUser;

     int iIndentNo=0;
     int iIssueNo=0;
     int iGrnNo=0;

     Connection theConnection=null;

     boolean        bComflag  = true;

     public MelangeStockUpdationFrame(JLayeredPane theLayer,int iUserCode,int iMillCode,String SItemTable,String SSupTable,String SYearCode)
     {
          this.theLayer     = theLayer;
          this.iUserCode    = iUserCode;
          this.iMillCode    = iMillCode;
          this.SItemTable   = SItemTable;
          this.SSupTable    = SSupTable;
          this.SYearCode    = SYearCode;

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                               theConnection =  oraConnection.getConnection();


               updateLookAndFeel();
               setSize(500,500);

               TopPanel    = new JPanel(true);
               MiddlePanel = new JPanel(true);
               MiddlePanel.setLayout(new BorderLayout());
     
               TInfo       = new JTextArea(50,50);
               BApply      = new JButton("Apply");
     
               TopPanel.add(BApply);
               MiddlePanel.add(TInfo);
     
               BApply.addActionListener(new AppList());
     
               getContentPane().add("North",TopPanel);
               getContentPane().add("Center",MiddlePanel);
               setVisible(true);
               setClosable(true);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private class AppList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
 
               BApply.setEnabled(false);
               try
               {
                    MiddlePanel.removeAll();
               }catch(Exception ex){System.out.println(ex);}
               MiddlePanel.add(TInfo);
               TInfo.append("Processing ....."+"\n");
 
               TInfo.append("Fetching Stock Data ....."+"\n");
               getStockData();
 
               if(VStockCode.size()>0)
               {
                    TInfo.append("Updation is going on ....."+"\n");
                    updateData();
               }
 
               TInfo.append("Updation Over .....");
               removeHelpFrame();
          }
     }

     private void updateData()
     {
          SSeleMillCode = "0";
          SSItemTable   = "INVITEMS";
          SSSupTable    = "SUPPLIER";
          SSeleUnitCode = "4";

          getIndenNo();
          getIssueNo();
          getGrnNo();
          insertTransferDetails();
          getACommit();
          removeHelpFrame();
     }

     private void getIndenNo()
     {
          iIndentNo=0;
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               
               String QS = " Select IndentNo from IndentNoConfig for update of indentNo nowait";

               PreparedStatement thePrepare    = theConnection.prepareStatement(QS);

               ResultSet theResult             = thePrepare.executeQuery();
               if(theResult.next())
                  iIndentNo = theResult.getInt(1)+1;

               theResult.close();
               thePrepare.close();

               thePrepare = theConnection.prepareStatement(" Update IndentNoConfig set indentNo=?");
               thePrepare.setInt(1,iIndentNo);
               thePrepare.executeUpdate();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);

               String SException = String.valueOf(ex);

               System.out.println("Indent indentNo: "+ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getIndenNo();
                    bComflag = true;
               }
               else
               {
                    bComflag = false;
               }
          }
     }

     private void getIssueNo()
     {
          iIssueNo=0;
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               
               String QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" Where Id = 5 for update of MaxNo nowait";

               PreparedStatement thePrepare    = theConnection.prepareStatement(QS);

               ResultSet theResult             = thePrepare.executeQuery();
               if(theResult.next())
                  iIssueNo = theResult.getInt(1)+1;

               theResult.close();
               thePrepare.close();

               thePrepare = theConnection.prepareStatement(" Update Config"+iMillCode+""+SYearCode+" set MaxNo=? Where Id=5 ");
               thePrepare.setInt(1,iIssueNo);
               thePrepare.executeUpdate();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);

               String SException = String.valueOf(ex);

               System.out.println("IssueNo: "+ex);
     
               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getIssueNo();
                    bComflag = true;
               }
               else
               {
                    bComflag = false;
               }
          }
     }

     private void getGrnNo()
     {
          iGrnNo=0;
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               
               String QS = " Select MaxNo From Config"+SSeleMillCode+""+SYearCode+" Where Id = 2 for update of MaxNo nowait";

               PreparedStatement thePrepare    = theConnection.prepareStatement(QS);

               ResultSet theResult             = thePrepare.executeQuery();
               if(theResult.next())
                  iGrnNo = theResult.getInt(1)+1;

               theResult.close();
               thePrepare.close();

               thePrepare = theConnection.prepareStatement(" Update Config"+SSeleMillCode+""+SYearCode+" set MaxNo=? Where Id=2 ");
               thePrepare.setInt(1,iGrnNo);
               thePrepare.executeUpdate();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);

               String SException = String.valueOf(ex);

               System.out.println("GrnNo: "+ex);
     
               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getGrnNo();
                    bComflag = true;
               }
               else
               {
                    bComflag = false;
               }
          }
     }

     private void insertTransferDetails()
     {
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               
               String SInsQS1 = " Insert into ConcernTransfer(Id,TranNo,TranDate,BlockCode,Code,Qty,Rate,InvQty,IndentNo,MillCode,SlNo,AuthUserCode,ToMillCode,ToUnitCode,ToUserCode,CreationDate,EntrySystemName,InvRate,TaxPer,Tax,InvAmount,InvoiceStatus,PrintStatus)"+
                                " Values(ConcernTransfer_Seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

               String SInsQS2 = " Insert Into Issue(Id,IssueNo,IssueDate,Code,Qty,IssRate,HodCode,UIRefNo,Unit_Code,Dept_Code,Group_Code,UserCode,MillCode,Authentication,SlNo,CreationDate,IssueValue,IndentType,MrsAuthUserCode,IndentUserCode) "+
                                " Values (Issue_Seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

               String SInsQS3 = " Insert Into GRN (Id,GrnNo,GrnBlock,GrnDate,Sup_Code,Code,OrderQty,Pending,InvQty,InvRate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,InvAmount,Plus,Less,Misc,InvNet,Dept_Code,Group_Code,Unit_Code,Inspection,MillQty,RejQty,Qty,SPJNo,InvSlNo,MillCode,NoOfBills,NoofSPJ,spjdate,slno,usercode,creationdate,taxclaimable,grnqty,grnvalue,MrsAuthUserCode) "+
                                " Values (Grn_Seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


               int iInvSlNo = getInvSlNo();

               int iSlNo=0;

               String SSelePartyCode = getPartyCode();

               for(int i=0; i<VStockCode.size(); i++)
               {
                    iSlNo++;

                    SSeleDeptCode="";
                    SSeleGroupCode="";

                    PreparedStatement thePrepare1 = theConnection.prepareStatement(SInsQS1);
                    PreparedStatement thePrepare2 = theConnection.prepareStatement(SInsQS2);
                    PreparedStatement thePrepare3 = theConnection.prepareStatement(SInsQS3);

                    Statement stat = theConnection.createStatement();

                    String SItemCode    = (String)VStockCode.elementAt(i);
                    String SStockRate   = (String)VStockRate.elementAt(i);
                    double dQty         = common.toDouble((String)VStockQty.elementAt(i));
                    double dStockValue  = common.toDouble(common.getRound(dQty * common.toDouble(SStockRate),2));

                    String SMrsUserCode = (String)VStockUser.elementAt(i);

                    getDeptGroupCode(SItemCode);


                    thePrepare1.setInt(1,iIssueNo);
                    thePrepare1.setString(2,common.getServerPureDate());
                    thePrepare1.setInt(3,0);
                    thePrepare1.setString(4,SItemCode);
                    thePrepare1.setDouble(5,dQty);
                    thePrepare1.setString(6,SStockRate);
                    thePrepare1.setDouble(7,dQty);
                    thePrepare1.setInt(8,iIndentNo);
                    thePrepare1.setInt(9,iMillCode);
                    thePrepare1.setInt(10,iSlNo);
                    thePrepare1.setString(11,SMrsUserCode);
                    thePrepare1.setString(12,SSeleMillCode);
                    thePrepare1.setString(13,SSeleUnitCode);
                    thePrepare1.setString(14,SMrsUserCode);
                    thePrepare1.setString(15,common.getServerDate());
                    thePrepare1.setString(16,InetAddress.getLocalHost().getHostName());
                    thePrepare1.setString(17,SStockRate);
                    thePrepare1.setString(18,"0");
                    thePrepare1.setString(19,"0");
                    thePrepare1.setString(20,common.getRound(dStockValue,2));
                    thePrepare1.setString(21,"2");
                    thePrepare1.setString(22,"2");

                    thePrepare1.executeUpdate();
                    thePrepare1.close();


                    thePrepare2.setInt(1,iIssueNo);
                    thePrepare2.setString(2,common.getServerPureDate());
                    thePrepare2.setString(3,SItemCode);
                    thePrepare2.setDouble(4,dQty);
                    thePrepare2.setString(5,SStockRate);
                    thePrepare2.setInt(6,0);
                    thePrepare2.setInt(7,iIndentNo);
                    thePrepare2.setString(8,"6");
                    thePrepare2.setString(9,SSeleDeptCode);
                    thePrepare2.setString(10,SSeleGroupCode);
                    thePrepare2.setInt(11,2);
                    thePrepare2.setInt(12,iMillCode);
                    thePrepare2.setInt(13,1);
                    thePrepare2.setInt(14,iSlNo);
                    thePrepare2.setString(15,common.getServerDate());
                    thePrepare2.setString(16,common.getRound(dStockValue,2));
                    thePrepare2.setInt(17,0);
                    thePrepare2.setString(18,SMrsUserCode);
                    thePrepare2.setString(19,SMrsUserCode);

                    thePrepare2.executeUpdate();
                    thePrepare2.close();


                    thePrepare3.setInt(1,iGrnNo);
                    thePrepare3.setInt(2,0);
                    thePrepare3.setString(3,common.getServerPureDate());
                    thePrepare3.setString(4,SSelePartyCode);
                    thePrepare3.setString(5,SItemCode);
                    thePrepare3.setDouble(6,dQty);
                    thePrepare3.setDouble(7,dQty);
                    thePrepare3.setDouble(8,dQty);
                    thePrepare3.setString(9,SStockRate);
                    thePrepare3.setString(10,"0");
                    thePrepare3.setString(11,"0");
                    thePrepare3.setString(12,"0");
                    thePrepare3.setString(13,"0");
                    thePrepare3.setString(14,"0");
                    thePrepare3.setString(15,"0");
                    thePrepare3.setString(16,"0");
                    thePrepare3.setString(17,"0");
                    thePrepare3.setString(18,common.getRound(dStockValue,2));
                    thePrepare3.setString(19,"0");
                    thePrepare3.setString(20,"0");
                    thePrepare3.setString(21,"0");
                    thePrepare3.setString(22,common.getRound(dStockValue,2));
                    thePrepare3.setString(23,SSeleDeptCode);
                    thePrepare3.setString(24,SSeleGroupCode);
                    thePrepare3.setString(25,SSeleUnitCode);
                    thePrepare3.setString(26,"1");
                    thePrepare3.setDouble(27,dQty);
                    thePrepare3.setDouble(28,0);
                    thePrepare3.setDouble(29,dQty);
                    thePrepare3.setString(30,"99999");
                    thePrepare3.setInt(31,iInvSlNo);
                    thePrepare3.setString(32,SSeleMillCode);
                    thePrepare3.setString(33,"1");
                    thePrepare3.setString(34,"1");
                    thePrepare3.setString(35,common.getServerPureDate());
                    thePrepare3.setInt(36,iSlNo);
                    thePrepare3.setString(37,"2");
                    thePrepare3.setString(38,common.getServerDate());
                    thePrepare3.setString(39,"0");
                    thePrepare3.setDouble(40,dQty);
                    thePrepare3.setString(41,common.getRound(dStockValue,2));
                    thePrepare3.setString(42,SMrsUserCode);

                    thePrepare3.executeUpdate();
                    thePrepare3.close();


                    String QS1 = " Update ItemStock set Stock=nvl(Stock,0)-("+dQty+")"+
                                 " Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+iMillCode;

                    String QS2 = " Update "+SItemTable+" "+
                                 " Set IssQty=nvl(IssQty,0)+"+dQty+",IssVal=nvl(IssVal,0)+"+dStockValue+" "+
                                 " Where Item_Code = '"+SItemCode+"'";

                    stat.executeUpdate(QS1);
                    stat.executeUpdate(QS2);

                    updateItemMaster(stat,SItemCode,common.getRound(dQty,3),common.getRound(dStockValue,2));
                    updateUserItemStock(stat,SItemCode,common.getRound(dQty,3),common.getRound(dStockValue,2),SMrsUserCode);

                    stat.close();
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag = false;
          }
     }

     private void updateItemMaster(Statement stat,String SItemCode,String SGrnQty,String SGrnValue)
     {
          try
          {
               String QS1 = " Update "+SSItemTable+" Set RecVal=nvl(RecVal,0)+"+SGrnValue+","+
                            " RecQty=nvl(RecQty,0)+"+SGrnQty+" "+
                            " Where Item_Code = '"+SItemCode+"'";

               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);

               stat.execute(QS1);
          }
          catch(Exception e)
          {
               System.out.println("E6"+e);
               bComflag  = false;
          }
     }

     private void updateUserItemStock(Statement stat,String SItemCode,String SGrnQty,String SGrnValue,String SMrsUserCode)
     {
          try
          {
               Item1 IC = new Item1(SItemCode,common.toInt(SSeleMillCode),SSItemTable,SSSupTable);
     
               double dAllStock = common.toDouble(IC.getClStock());
               double dAllValue = common.toDouble(IC.getClValue());
     
               double dRate   = 0;
               try
               {
                    dRate = dAllValue/dAllStock;
               }
               catch(Exception ex)
               {
                    dRate=0;
               }
     
               if(dAllStock==0)
               {
                    dRate = common.toDouble(IC.SRate);
               }

               int iCount=0;

               String QS = " Select count(*) from ItemStock Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+SSeleMillCode;
               
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    iCount   =    res.getInt(1);
               }
               res.close();

               String QS1 = "";

               if(iCount>0)
               {
                    QS1 = "Update ItemStock Set Stock=nvl(Stock,0)+"+SGrnQty+" ";
                    QS1 = QS1+" Where ItemCode='"+SItemCode+"' and HodCode="+SMrsUserCode+" and MillCode="+SSeleMillCode;
               }
               else
               {
                    QS1 = " Insert into ItemStock (MillCode,HodCode,ItemCode,Stock,StockValue) Values (";
                    QS1 = QS1+"0"+SSeleMillCode+",";
                    QS1 = QS1+"0"+SMrsUserCode+",";
                    QS1 = QS1+"'"+SItemCode+"',";
                    QS1 = QS1+"0"+SGrnQty+",";
                    QS1 = QS1+"0"+common.getRound(dRate,4)+")";
               }

               String QS2 = " Update ItemStock set StockValue="+common.getRound(dRate,4)+
                            " Where ItemCode='"+SItemCode+"' and MillCode="+SSeleMillCode;


               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);

               stat.execute(QS1);
               stat.execute(QS2);
          }
          catch(Exception e)
          {
               System.out.println("E7"+e);
               bComflag  = false;
          }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnection  . commit();
                    JOptionPane    . showMessageDialog(null,"Data Saved Sucessfully");
                    System         . out.println("Commit");
               }
               else
               {
                    theConnection  . rollback();
                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theConnection   . setAutoCommit(true);
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void getStockData()
     {
          VStockCode = new Vector();
          VStockQty  = new Vector();
          VStockRate = new Vector();
          VStockUser = new Vector();

          try
          {
               String QS = " Select ItemCode,Stock,StockValue,HodCode from ItemStock "+
                           " Where MillCode=3 and HodCode<>6125 and Stock>0 "+
                           " Order by ItemCode,MillCode ";


               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VStockCode.addElement(common.parseNull(result.getString(1)));
                    VStockQty.addElement(common.parseNull(result.getString(2)));
                    VStockRate.addElement(common.parseNull(result.getString(3)));
                    VStockUser.addElement(common.parseNull(result.getString(4)));
               }
               theStatement.close();
               result.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public int getInvSlNo()
     {
          int iSlNo=0;
          try
          {
               Statement      stat           =  theConnection.createStatement();
               ResultSet      res            =  stat.executeQuery("Select Max(InvSlNo) From GRN");
               if(res.next())
                   iSlNo = res.getInt(1);

               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

          iSlNo++;

          return iSlNo;
     }

     public String getPartyCode()
     {
          String SPartyCode="";
          try
          {
               Statement      stat           =  theConnection.createStatement();
               ResultSet      res            =  stat.executeQuery(" Select Ac_Code from Mill where MillCode="+iMillCode);
               if(res.next())
                   SPartyCode = common.parseNull(res.getString(1));

               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag = false;
          }
          return SPartyCode;
     }

     public void getDeptGroupCode(String SItemCode)
     {
          SSeleDeptCode="0";
          SSeleGroupCode="0";
          
          try
          {
               Statement      stat           =  theConnection.createStatement();
               ResultSet      res            =  stat.executeQuery(" Select Dept_Code,Group_Code from Grn Where Id=(Select Max(Id) from Grn Where GrnQty>0 and MillCode="+iMillCode+" and Code='"+SItemCode+"')");
               if(res.next())
               {
                   SSeleDeptCode = common.parseNull(res.getString(1));
                   SSeleGroupCode = common.parseNull(res.getString(2));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               SSeleDeptCode="0";
               SSeleGroupCode="0";
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               theLayer.remove(this);
               theLayer.repaint();
               theLayer.updateUI();
          }
          catch(Exception ex) { }
     }

     private void updateLookAndFeel()
     {
          try
          {
               UIManager.setLookAndFeel(windows);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }                    

     }


}


