
package Kardex;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.sql.*;
import util.*;
import guiutil.*;
import java.net.InetAddress;
import jdbc.*;

public class StoreMaterialAuthenticationFrame extends JInternalFrame
{
    JPanel        TopPanel,MiddlePanel,BottomPanel;
    JButton       BSave,BExit,BApply,BPrint;
    DateField     TFromDate,TToDate;
    private       JTable  theTable;
    private       StoreMaterialAuthenticationModel    theModel;
    JLayeredPane  DeskTop;
    Common         common   = new Common();
    int            iMillCode;
    
    ORAConnection  connect;
    Connection     theconnect;
    
    Connection theConnection=null;
    Statement  theStatement=null;
    
    String AuthSystemName;
    
    Vector VCode,VNameCode,VName;
    StatusPanel    SPanel;
    
    String         SStDate,SEnDate;
    String         SItemTable,SSupTable;
    
    String         SFromDate,SToDate;
    int            iUserCode,iApplyStatus=0,FlowCode;
	    
    ArrayList      AMaterialList;
    StoreMaterialDateWiseFrame storematerialdatewiseframe;

	double  dOpening = 0.0;
   	double  dCarriedOver = 0.0 ;

   	int iCarriedOver = 0,iRoundedNet = 0,iOpeningValue = 0,iContractorEmpCode=0;
   	String SEmpName,SHRDTicketNo="",SID="";
      
    public StoreMaterialAuthenticationFrame(JLayeredPane DeskTop,int iMillCode,String SItemTable,int iUserCode)
    {
		try
        {
            AuthSystemName=InetAddress.getLocalHost().getHostName();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }	
        try
        {
            this.DeskTop    = DeskTop;
            this.iMillCode  = iMillCode;
            this.SItemTable = SItemTable;
		    this.iUserCode  = iUserCode;
    	}
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        try
        {
        	AuthSystemName=InetAddress.getLocalHost().getHostName();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        common = new Common();
		
        setContractorEmpCode();                             
        createComponents();
		setLayouts();
		addComponents();
		addListeners();
        setID();
        setContractorDetails();
		
       // setTableDetails();
           
    }
    private void createComponents()
    {
        TopPanel      = new JPanel();
        MiddlePanel   = new JPanel();
        BottomPanel   = new JPanel();
                 
        TFromDate     = new DateField();
        TToDate       = new DateField();
            
        BSave         = new JButton("Save");
        BExit         = new JButton("Exit");
        BApply        = new JButton("Apply");
		BPrint        = new JButton("Print");
                
        theModel      = new StoreMaterialAuthenticationModel();
        theTable      = new JTable(theModel);
                 
        for(int i=0;i<theModel.ColumnName.length;i++)
        {
           (theTable.getColumnModel()) . getColumn(i) . setPreferredWidth(theModel.iColumnWidth[i]);
        }
    }
    private void setLayouts()
    {
    	TopPanel      . setLayout(new GridLayout(1,5));
        MiddlePanel   . setLayout(new BorderLayout());
        BottomPanel   . setLayout(new FlowLayout(FlowLayout.CENTER));
          
        TopPanel      . setBorder(new TitledBorder(" Filter "));
        MiddlePanel   . setBorder(new TitledBorder(" Details "));
        BottomPanel   . setBorder(new TitledBorder(" Controls "));
         
        this . setTitle("Materials UnLoad Authentication Details");
        this . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this . setSize(700,500);
    }
    private void addComponents()
    {
    	//TopPanel . add(new JLabel(" Store Material UnLoad Details Entry Screen "));      
        TopPanel . add(new JLabel(" FROM DATE "))  ;
        TopPanel . add(TFromDate);
        TopPanel . add(new JLabel(" TO DATE "))  ;
        TopPanel . add(TToDate);
        TopPanel . add(BApply);
        
        MiddlePanel.add(new JScrollPane(theTable));
                  
        BottomPanel . add(BSave);
        BottomPanel . add(BExit);
		BottomPanel . add(BPrint);
        BottomPanel . add(new JLabel(" Select A Row and Press F4 --> Date Wise Details"));      
              
        this . add(TopPanel,BorderLayout.NORTH);
        this . add(MiddlePanel,BorderLayout.CENTER);
        this . add(BottomPanel,BorderLayout.SOUTH);
    }
    private void addListeners()
    {
        BApply . addActionListener(new ActList());
        BSave . addActionListener(new ActList());
        BExit . addActionListener(new ActList());
		BPrint . addActionListener(new ActList());
        theTable . addKeyListener(new keyList());
    }
    private class ActList implements ActionListener
    {
		public void actionPerformed(ActionEvent ae)   
		{
			if(ae.getSource()==BApply)
			{
				  theModel.setNumRows(0);
				  setTableDetails();
				  iApplyStatus=1;
				  
			}
			if(ae.getSource()==BSave)
			{
				if(iApplyStatus==1)
				{
					if(JOptionPane.showConfirmDialog(null, "Confirm Save the Data?", "Information", JOptionPane.YES_NO_OPTION) == 0)
					{
						if(checkSelectBox())
							getSave();
					}
				}
				else
				{
					JOptionPane . showMessageDialog(null,"Please Click Apply Button For Data");
				}         
			} 
			if(ae.getSource()==BPrint) 
			{
			        if(CheckDataSaved())
                                {
					try
					{
						String SFile = "D:\\StoreMaterialUnLoad.pdf";
					
						SFromDate     = common.pureDate(TFromDate.toString());
						SToDate       = common.pureDate(TToDate.toString());
									
						StoreMaterialUnLoadPDF createPDF = new StoreMaterialUnLoadPDF(SFile,SFromDate,SToDate,iCarriedOver,iRoundedNet,iOpeningValue);
		                                
						 try
						 {
							 Process p = Runtime.getRuntime()
							 .exec("rundll32 url.dll,FileProtocolHandler  "+SFile);
							 p.waitFor(); 
						}
						catch(Exception ex)
						{
							 System.out.println(" initPDF "+ex);
						}
						VoucherPDF voucherPdf = new VoucherPDF();

						String SVocPdfFile = "";
						String SServerDate = (common.getServerDate()).substring(0,8);
						int iRow = 0;

						String SPurpose = "Tractor Hire Charges for Loading & UnLoading Work From " + common.parseDate(SFromDate) + " To " + common.parseDate(SToDate);
						String SAcCode = "A_623";
						String SAcType = "STORES MATERIAL";

						String SVocNo = setVocNo();
						
						String SNarration = SEmpName + " Tractor Hire Charges for Loading & UnLoading Work From  " + common.parseDate(SFromDate) + " To " + common.parseDate(SToDate);
					       // String STransactionId = TTransactionId.getText();

						voucherPdf.setValues(SVocNo, SEmpName, SAcType,String.valueOf(iRoundedNet), SPurpose, SServerDate, SHRDTicketNo,SID);//, STransactionId
						SVocPdfFile = voucherPdf.SFile;
						voucherPdf.closePdf();
				                JOptionPane.showMessageDialog(null, "Payment Voucher Created in " + SVocPdfFile, "Info", JOptionPane.INFORMATION_MESSAGE);
						try
						 {
							 Process p = Runtime.getRuntime()
							 .exec("rundll32 url.dll,FileProtocolHandler  "+SVocPdfFile);
							 p.waitFor(); 
						}
						catch(Exception ex)
						{
							 System.out.println(" initPDF "+ex);
						}
																				
					}
					catch(Exception ex)
					{
					   System.out.println("PrnCreation"+ex);
					   ex.printStackTrace();
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Date Not Saved,So U Cant take Print ", "Info", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		
			if(ae.getSource()==BExit)
			{
			   dispose();
			}
		}
    }
	private void setContractorDetails() 
	{
		try 
		{
			StringBuffer sb = new StringBuffer();
            sb.append(" Select 'ANGAPPAN' as EMPNAME,'' as HrdTicketNo,CarriedOver,RoundedNet,Opening from StoreMaterial_Authentication ");
//	    sb.append(" Inner join StoreMaterial_Authentication on StoreMaterial_Authentication.EmpCode = ContractApprentice.EmpCode ");
	    	sb.append(" where UserCode=17 and ID="+SID+"  ");

			System.out.println("Carried Over :"+sb.toString());

            ORAConnection connect = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();

            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();
            while (theResult.next()) 
			{
                SEmpName = theResult.getString(1);
                SHRDTicketNo = theResult.getString(2);
                iCarriedOver = theResult.getInt(3);
                iRoundedNet = theResult.getInt(4);
				iOpeningValue = theResult.getInt(5);
			}
		} 
		catch (Exception ex) 
		{
            System.out.println(" getContractorDetails " + ex);
        }
    }
	private String setID() 
	{
		String SVocNo = "";
        StringBuffer sb = new StringBuffer();
        sb.append(" Select Max(ID) from StoreMaterial_Authentication where UserCode=17 ");

        try 
		{
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();
            Statement theStatement = theConnection.createStatement();
            ResultSet theResult = theStatement.executeQuery(sb.toString());
            while (theResult.next()) 
			{
                SID = theResult.getString(1);
            }
        } 
		catch (Exception ex) 
		{
            System.out.println(" setID " + ex);
        }
        return SVocNo;
    }

	private String setVocNo() 
	{
        String SVocNo = "";
        StringBuffer sb = new StringBuffer();
        sb.append(" Select StoreMaterial_Auth_seq.nextVal from dual");
        try 
		{
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();
            Statement theStatement = theConnection.createStatement();
            ResultSet theResult = theStatement.executeQuery(sb.toString());
            while (theResult.next()) 
			{
				SVocNo = theResult.getString(1);
			}
        } 
		catch (Exception ex) 
		{
            System.out.println(" setVocNo " + ex);
        }
        return SVocNo;
	}

    private void setTableDetails()
    {
        theModel                           . setNumRows(0);
		SetVector();
		int iSlNo                          = 1;
          
		double dTotalAmt = 0;
			
		for(int i=0;i<AMaterialList.size();i++)
		{
    		HashMap theMap = (HashMap)AMaterialList.get(i);
            
            ArrayList theList             = null;
            theList                       = new ArrayList();
            theList                       . clear();
            theList                       . add(String.valueOf(iSlNo++));
            theList                       . add(common.parseDate((String)theMap.get("PaymentDate")));
            theList                       . add(String.valueOf(theMap.get("Amount")));
            theList                       . add(new java.lang.Boolean(true));
               
            dTotalAmt = dTotalAmt + common.toDouble(String.valueOf(theMap.get("Amount")));
			   
	    	theModel                      . appendRow(new Vector(theList));
		}
        
       	ArrayList theList1             = null;
        theList1                       = new ArrayList();
        theList1                       . clear();
        theList1                       . add("Total");
        theList1                       . add("");
        theList1                       . add(dTotalAmt);
        theList1                       . add(null);
                 
        theModel                      . appendRow(new Vector(theList1));
    }
    private void SetVector()
    {
    	AMaterialList = new ArrayList();
        
        SFromDate     = common.pureDate(TFromDate.toString());
        SToDate       = common.pureDate(TToDate.toString());
                
        try
        {
            ORAConnection connect         =    ORAConnection.getORAConnection();
            Connection     theConnect     =    connect.getConnection();
            Statement      theStatement   =    theConnect.createStatement();
            ResultSet      theResult      =    theStatement.executeQuery(getQS());
            while(theResult.next())
            {
				double dAmount       = theResult.getDouble(1);
                String SPaymentDate  = theResult.getString(2);
                
                HashMap theMap = new HashMap();
                
                theMap.put("Amount",dAmount);
                theMap.put("PaymentDate",SPaymentDate);
                
                AMaterialList          . add(theMap);
            }
            theResult.close();

	    	ResultSet  theResult1                = theStatement.executeQuery(getOpeningQS());

			while(theResult1.next())
            {
            	dOpening  = theResult1.getDouble(1);
			}
		    theResult1.close();
            theStatement.close();
        }
        catch(Exception e)
        {
            
        }
    }

	private String getOpeningQS()
    {
    	StringBuffer sb = new StringBuffer();
      
        if(iUserCode == 17)
	  	{
	    	sb.append(" Select (-1)*CarriedOver from StoreMaterial_Authentication");
	        sb.append(" where FromDate =(Select  Max(FromDate) from StoreMaterial_Authentication) ");
			sb.append(" and ToDate =(Select  Max(ToDate) from StoreMaterial_Authentication) " );
        }
	 	if(iUserCode == 2)
        {
			sb.append(" Select Opening from StoreMaterial_Authentication");
	        sb.append(" where FromDate =(Select  Max(FromDate) from StoreMaterial_Authentication) ");
			sb.append(" and ToDate =(Select  Max(ToDate) from StoreMaterial_Authentication)   and UserCode=17" );
	 	}
		System.out.println("Opening QS:"+sb.toString()); 
         
        return sb.toString();
	}
    private String getQS()
    {
    	String QS = " Select sum(TotalAmount),PaymentDate from StoreMaterial_Movement_Payment "+
                    " where PaymentDate>="+SFromDate+" and PaymentDate<="+SToDate+" and PaymentDate!=20190916 group by PaymentDate order by PaymentDate";
        //System.out.println("QS:"+QS);
        return QS;
    }
	public boolean checkSelectBox()
	{
		boolean bFlag = true;
		for(int i=0; i<theModel.getRowCount()-1; i++)
        {
    		Boolean bSelect           = (Boolean)theModel.getValueAt(i,3);
                    
           	if(bSelect.booleanValue())
         		bFlag  = true;
			else
			{
				bFlag = false;
				JOptionPane.showMessageDialog(null, "Any One CheckBox is Not Selected For Save ", "Info", JOptionPane.INFORMATION_MESSAGE);
				break;
			}	
		}
		return bFlag;
	}
	public void getSave()
	{
		int savevalue=-1;
        double dTotalAmt = 0.0 ;
        		
        for(int i=0; i<theModel.getRowCount()-1; i++)
        {
    		Boolean bSelect           = (Boolean)theModel.getValueAt(i,3);
                    
           	if(bSelect.booleanValue())
           	{
            	String  SDate = (String)theModel.getValueAt(i,1);
             	double  dAmount = common.toDouble((String)theModel.getValueAt(i,2));
             	dTotalAmt = dTotalAmt + dAmount ; 
			}
       	}

		String SFromDate     = common.pureDate(TFromDate.toString());
		String SToDate       = common.pureDate(TToDate.toString());

		savevalue=setInsertData(SFromDate,SToDate,dTotalAmt,iUserCode,AuthSystemName);    
     
       	if(savevalue==0) 
       	{
        	JOptionPane.showMessageDialog(null,"Data Saved","Information",JOptionPane.INFORMATION_MESSAGE);                                                 
            dispose();   
       	}
      	else if(savevalue==2)
       	{
        	JOptionPane.showMessageDialog(null,"Data AlreadyExist","Information",JOptionPane.INFORMATION_MESSAGE);
       	}
     	else if(savevalue==1)
      	{
           JOptionPane.showMessageDialog(null,"Data Not Saved,Please Check the Previous Authentication","Information",JOptionPane.INFORMATION_MESSAGE);
      	}
	}
	public int setInsertData( String SFromDate,String SToDate, Double dTotalAmount,int iUserCode,String SSystemName)
  	{
   		int isave=0; 

      	int flowvalue=setAuthenticationFlow(SFromDate,SToDate,iUserCode);
         
      	if(flowvalue==1)
      	{
        	boolean bs  = isExist(SFromDate,SToDate,iUserCode);
            if (bs)
            {
               isave=2;
            }
     
           	if(isave!=2)
           	{
            	try
             	{
    				StringBuffer sb=new StringBuffer(); 
         
         	 		sb.append(" insert into StoreMaterial_Authentication (ID,FromDate,ToDate,Amount,UserCode,FlowCode,Opening,CarriedOver,RoundedNet,ContractorEmpCode,AuthDate,AuthDateTime,AuthSystemName,EmpBalanceStatus,EmpCode)");
					sb.append(" values (StoreMaterial_Auth_seq.nextval,?,?,?,?,?,?,?,?,?,to_Char(SysDate, 'YYYYMMDD'),to_Char(SysDate, 'DD.MM.YYYY HH24:MI:SS'),?,?,?)");
         
         			if (theConnection==null)
              	 	{
                    	ORAConnection oraConnection     =   ORAConnection.getORAConnection();
                    	theConnection                   =   oraConnection.getConnection();
               		}

					theConnection	                   . setAutoCommit(false);
	
                 	double dRoundedNet = getRoundedNet(dTotalAmount);
		 			int iFlowCode =0 ;

					PreparedStatement ps=theConnection.prepareStatement(sb.toString());

                 	ps                    . setInt(1,common.toInt(SFromDate));
					ps                    . setInt(2,common.toInt(SToDate));
					ps                    . setDouble(3,dTotalAmount);
					ps                    . setInt(4,iUserCode);
					ps                    . setInt(5,iFlowCode);
					ps                    . setDouble(6,dOpening);
					ps                    . setDouble(7,dCarriedOver);
					ps                    . setDouble(8,dRoundedNet);
					ps                    . setInt(9,iContractorEmpCode);
					ps                    . setString(10,AuthSystemName);
					ps                    . setInt(11,0);
					ps                    . setInt(12, 191197);

					ps . executeUpdate();

					theConnection		  . commit();
					theConnection   	  . setAutoCommit(true);
              
                 	ps.close();
                 	ps=null;
            	}  
      			catch(Exception e)
      			{
					try
					{
						theConnection	.	rollback();
						theConnection	.	setAutoCommit(true);
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
						System.out.println(ex);

					}
          			isave=1;
          			e.printStackTrace();
        		}
    		}
   		}
  		else
  		{
       		isave=1;
  		}
  		return isave;
	}

    public double getRoundedNet(double dTotalAmount)
	{
    	double dNetBalance = (dTotalAmount+dOpening)%50;

        if(dNetBalance<=25)
        {
			if(dNetBalance==0)
				dCarriedOver =  dNetBalance;
			else
				dCarriedOver = -dNetBalance;
        }
		else
		{
			dCarriedOver = 50-dNetBalance;
		}
		return ((dTotalAmount+dOpening)+dCarriedOver);
    }
	public boolean isExist(String SFromDate,String SToDate,int iUserCode)
  	{
		boolean result=false;
     	try
      	{
        	StringBuffer sb=new StringBuffer(); 
         
        	sb.append(" select FromDate,Todate,UserCode");
         	sb.append(" from StoreMaterial_Authentication");
      		
         	if (theConnection==null)
            {
            	ORAConnection oraConnection     =   ORAConnection.getORAConnection();
                theConnection                   =   oraConnection.getConnection();
            }
        	PreparedStatement ps=theConnection.prepareStatement(sb.toString());

       		ResultSet rs=ps.executeQuery();
        	while(rs.next())  
        	{
            	String SFromDate1     = rs.getString(1);
                String SToDate1       = rs.getString(2);
                int    iUserCode1     = rs.getInt(3);
             
				if( (iUserCode1==iUserCode)&&(SFromDate1.equals(common.pureDate(SFromDate))) &&(SToDate1.equals(common.pureDate(SToDate))) )
				{
					result=true;
					break;
				}   
				else
				{
					result=false;
				}
     		}
            rs.close();
            ps.close();
            ps=null;
            rs=null;
            theConnection=null;
		}    
      	catch(Exception e)
      	{
        	System.out.println("Exist method");
          	e.printStackTrace();
      	}
		return result;
	}
 	public int setAuthenticationFlow (String SFromDate,String SToDate, int iUserCode)
  	{
    	int flowvalue=0;
     
    	if(iUserCode==17)
    	{
    	    flowvalue=1;
    	}
    	try
    	{
    		StringBuffer sb=new StringBuffer(); 
        	sb.append(" select FromDate,ToDate,Amount,UserCode");  //,AuthUserCode
        	sb.append(" from StoreMaterial_Authentication");
   
         	if (theConnection==null)
        	{
            	ORAConnection oraConnection     =   ORAConnection.getORAConnection();
            	theConnection                   =   oraConnection.getConnection();
            }
        
			PreparedStatement ps=theConnection.prepareStatement(sb.toString());
        	ResultSet rs=ps.executeQuery();
        
  			while(rs.next()) 
        	{
            	String SFromDate1  = rs.getString(1);
            	String SToDate1    = rs.getString(2);
            	double dAmt        = rs.getDouble(3);
            	int   iUserCode1   = rs.getInt(4);
			
				if(iUserCode==2)
				{
            		if((SFromDate1.equals(common.pureDate(SFromDate))) &&  (SToDate1.equals(common.pureDate(SToDate)))  && iUserCode1==17)
                	{
                    	flowvalue=1;
                	}
            	}    
        	}
        	rs.close();
        	ps.close();
        	ps=null;
    	}
    	catch(Exception e)
    	{
        	System.out.println("Error in Authenticaion Flow Code ");
        	e.printStackTrace();
    	}
    	return flowvalue;
 	}
 
    private boolean CheckDataSaved()
    {
		boolean bFlag = false ;
		int   iCount = 0;

		//SFromDate     = common.pureDate(TFromDate.toString());
		//SToDate       = common.pureDate(TToDate.toString());

        StringBuffer sb = new StringBuffer();
        sb.append(" Select count(1) from StoreMaterial_Authentication where UserCode=17 and FromDate>="+SFromDate+" and ToDate<="+SToDate+" ");
		System.out.println("Check "+sb.toString());
        try 
		{
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();

            Statement theStatement = theConnection.createStatement();
            ResultSet theResult = theStatement.executeQuery(sb.toString());
            while (theResult.next()) 
	    	{
            	iCount = theResult.getInt(1);
            }
			if(iCount>0)
		   	{
				bFlag = true;
			}	
            else
            {
				bFlag = false;
	    	}
        } 
		catch (Exception ex) 
		{
            System.out.println(" Check DataSaved " + ex);
        }
		return bFlag;
    }
    private class keyList extends KeyAdapter
    {
    	public keyList()
     	{
     	}
     	public void keyPressed(KeyEvent ke)
     	{
			if(ke.getKeyCode()==KeyEvent.VK_F4)
          	{
            	int    iRow  = theTable.getSelectedRow();
              	int    iCol  = theTable.getSelectedColumn();
              
             	String SDate = (String)theModel.getValueAt(iRow,1);
	     
              	storematerialdatewiseframe  = new StoreMaterialDateWiseFrame(SDate);
              	storematerialdatewiseframe.setBounds(100,100,900,450);
	      		storematerialdatewiseframe.setVisible(true);
			}
		}
    }
  	private void setContractorEmpCode() 
	{
		try 
		{
			StringBuffer sb = new StringBuffer();
            sb.append(" Select ContractorEmpCode from Contractor where ContractorCode=32 ");

            ORAConnection4 connect = ORAConnection4.getORAConnection();
            Connection theConnection = connect.getConnection();
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();
            while (theResult.next()) 
			{
				iContractorEmpCode = theResult.getInt(1);
			}
		} 
		catch (Exception ex) 
		{
            System.out.println(" getContractorEmpCode " + ex);
        }
    }
    
}
