/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Kardex;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.sql.*;
import util.*;
import guiutil.*;
import java.net.InetAddress;
import jdbc.*;

public class StoreMaterialDateWiseFrame extends JDialog
{
    JPanel        TopPanel,MiddlePanel,BottomPanel;
    JButton       BSave,BExit,BApply;
    DateField  TFromDate,TToDate;
    private       JTable  theTable;
    private       StoreMaterialDateWiseModel    theModel;
    JLayeredPane  DeskTop;
    Common        common   = new Common();
    int           iMillCode;
    
    ORAConnection  connect;
    Connection     theconnect;
    
    Connection theConnection=null;
    Statement  theStatement=null;
    
    String AuthSystemName;
    
    Vector VCode,VNameCode,VName;
    StatusPanel    SPanel;
    
    String         SStDate,SEnDate;
    String         SItemTable,SSupTable;
    
    String         SDate;
    
    ArrayList      AMaterialList;
    
    StoreMaterialDateWiseFrame storematerialdatewiseframe;
    
    public StoreMaterialDateWiseFrame(String SDate)
    {
        try
        {
            this.SDate  = SDate;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        try
        {
                AuthSystemName=InetAddress.getLocalHost().getHostName();
        }
        catch(Exception e)
        {
                e.printStackTrace();
        }
        common = new Common();
                               
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
        setTableDetails();
           
    }
    private void createComponents()
    {
        TopPanel      = new JPanel();
        MiddlePanel   = new JPanel();
        BottomPanel   = new JPanel();
        
          
        TFromDate     = new DateField();
        TToDate       = new DateField();
             
        BExit         = new JButton("Exit");
                        
        theModel      = new StoreMaterialDateWiseModel();
        theTable      = new JTable(theModel);
                 
        for(int i=0;i<theModel.ColumnName.length;i++)
        {
           (theTable.getColumnModel()) . getColumn(i) . setPreferredWidth(theModel.iColumnWidth[i]);
        }
    }
    private void setLayouts()
    {
    
         TopPanel      . setLayout(new FlowLayout(FlowLayout.CENTER));
         MiddlePanel   . setLayout(new BorderLayout());
         BottomPanel   . setLayout(new FlowLayout(FlowLayout.CENTER));
          
         TopPanel      . setBorder(new TitledBorder(" Filter "));
         MiddlePanel   . setBorder(new TitledBorder(" Details "));
         BottomPanel   . setBorder(new TitledBorder(" Controls "));
         
         setTitle("STORE MATERIAL FRAME");
         setModal(true) ;

         this.getContentPane().setLayout(new BorderLayout());
         
      }
    private void addComponents()
    {
         
        TopPanel . add(new JLabel(" STORE MATERIAL UNLOAD DATE WISE DETAILS "));      
        
        MiddlePanel.add(new JScrollPane(theTable));
                 
        BottomPanel . add(BExit);
                            
        this.getContentPane().add(TopPanel,BorderLayout.NORTH);
        this.getContentPane().add(MiddlePanel,BorderLayout.CENTER);
        this.getContentPane().add(BottomPanel,BorderLayout.SOUTH);
               
    }
    private void addListeners()
    {        
        BExit . addActionListener(new ActList());
    }
    private class ActList implements ActionListener
    {
      public void actionPerformed(ActionEvent ae)   
      {
          if(ae.getSource()==BExit)
          {
              dispose();
          }
      }
    }
    private void setTableDetails()
    {
        theModel                           . setNumRows(0);
	SetVector();
	int iSlNo                          = 1;
          
	double dTotalAmt = 0;
			
	for(int i=0;i<AMaterialList.size();i++)
	{
                        
            HashMap theMap = (HashMap)AMaterialList.get(i);
            
            ArrayList theList             = null;
            theList                       = new ArrayList();
            theList                       . clear();
            theList                       . add(String.valueOf(iSlNo++));
            theList                       . add((String)theMap.get("MaterialName"));
            theList                       . add((String)theMap.get("MaterialCode"));
            theList                       . add(String.valueOf(theMap.get("Quantity")));
            theList                       . add(String.valueOf(theMap.get("Rate")));
            theList                       . add(String.valueOf(theMap.get("Amount")));
            theList                       . add((String)theMap.get("UOMName"));
            theList                       . add(new java.lang.Boolean(false));
               
            dTotalAmt = dTotalAmt + common.toDouble(String.valueOf(theMap.get("Amount")));
			   
	    theModel                      . appendRow(new Vector(theList));
        }
        
       ArrayList theList1             = null;
        theList1                       = new ArrayList();
        theList1                       . clear();
        theList1                       . add("Total");
        theList1                       . add("");
        theList1                       . add("");
        theList1                       . add("");
        theList1                       . add("");
        theList1                       . add(dTotalAmt);
        theList1                       . add(null);
        
             
        theModel                      . appendRow(new Vector(theList1));
    }
    private void SetVector()
    {
        AMaterialList = new ArrayList();
        
        SDate     = common.pureDate(SDate);
            
        
        try
        {
            ORAConnection connect         =    ORAConnection.getORAConnection();
            Connection     theConnect     =    connect.getConnection();
            Statement      theStatement   =    theConnect.createStatement();
            ResultSet      theResult      =    theStatement.executeQuery(getQS());
            while(theResult.next())
            {
                String SMaterialName = theResult.getString(1);
                String SMaterialCode = common.parseNull(theResult.getString(2));
                double dQuantity     = theResult.getDouble(3);
                double dRate         = theResult.getDouble(4);
                double dAmount       = theResult.getDouble(5);
                String SUOMName      = theResult.getString(6);
                
                HashMap theMap = new HashMap();
                
                theMap.put("MaterialName",SMaterialName);
                theMap.put("MaterialCode",SMaterialCode);
                theMap.put("Quantity",dQuantity);
                theMap.put("Rate",dRate); 
                theMap.put("Amount",dAmount);
                theMap.put("UOMName",SUOMName);
                
                AMaterialList          . add(theMap);
            }
            theResult.close();
            theStatement.close();
          
            
        }
        catch(Exception e)
        {
            
        }
    }
    private String getQS()
    {
        
        String QS = " Select MaterialName,MaterialCode,Quantity,Rate,TotalAmount,UOMName "+
                    " from StoreMaterial_Movement_Payment "+
                    " Inner join UOM on UOM.UOMCode = StoreMaterial_Movement_Payment.UOMCode "+
                    " where PaymentDate="+SDate+" ";
                
        System.out.println("QS:"+QS);
        return QS;
    }
}
