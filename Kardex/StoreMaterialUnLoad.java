/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Kardex;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.sql.*;
import util.*;
import guiutil.*;
import java.net.InetAddress;
import jdbc.*;

public class StoreMaterialUnLoad extends JInternalFrame
{
	JPanel        TitlePanel,TopPanel,TopLeftPanel,MiddlePanel,BottomPanel;
    JButton       BSave,BExit,BUpdate;
    JButton       BMaterial;
    JComboBox     JCType,JCMaterial,JCMaterialType,JCUOM,JCShiftingType;
    JTextField    TMatCode ;
    JTextField    TQuantity,TRate,TDate;
    AddressField  TMatName;
    Vector        VMaterialName,VMaterialCode,VMaterialType,VType,VCode;
    private       JTable  theTable;
    private       StoreMaterialModel    theModel;
    JLayeredPane   DeskTop;
    StoreMaterialUnLoad theMainFrame;
    MaterialSearchUnLoad theMaterialSearchUnLoad;
    Vector         VName,VNameCode,VCode1,theVector;
    Vector         VUOMName,VUOMCode,VShiftingCode,VShiftingType;
    StatusPanel    SPanel;
    DateField      TPaymentDate;
     
    Common         common   = new Common();
    int            iMillCode;
    String         SStDate,SEnDate;
    String         SItemTable,SSupTable;
     
    ORAConnection  connect;
    Connection     theconnect;
    
    Connection theConnection=null;
    Statement  theStatement=null;
    
    String AuthSystemName;
    boolean        bFlag=false;
    
    public StoreMaterialUnLoad(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iMillCode,String SStDate,String SEnDate,String SItemTable,String SSupTable)
    {
    	try
        {
        	this.DeskTop    = DeskTop;
          	this.VCode      = VCode;
          	this.VName      = VName;
         	this.VNameCode  = VNameCode;
          	this.SPanel     = SPanel;
          	this.iMillCode  = iMillCode;
          	this.SStDate    = SStDate;
          	this.SEnDate    = SEnDate;
          	this.SItemTable = SItemTable;
          	this.SSupTable  = SSupTable;
		}
        catch(Exception e)
        {
        	e.printStackTrace();
        }
        
        try
        {
			AuthSystemName=InetAddress.getLocalHost().getHostName();
        }
        catch(Exception e)
        {
			e.printStackTrace();
        }
        common = new Common();
                        
        setDataVector();
        createComponents();
		setLayouts();
        addComponents();
        addListeners();
	}
    private void createComponents()
    {
        TopPanel      = new JPanel();
        MiddlePanel   = new JPanel();
        BottomPanel   = new JPanel();
        TitlePanel    = new JPanel();
        TopLeftPanel  = new JPanel();
          
        TQuantity     = new JTextField(20);
        TRate         = new JTextField(20);
        TDate         = new JTextField(20);
        
        JCType        = new JComboBox(VType);
        JCMaterialType= new JComboBox(VMaterialName);
        JCUOM         = new JComboBox(VUOMName);
		JCShiftingType = new JComboBox(VShiftingType);
        
        BSave         = new JButton("Save");
        BExit         = new JButton("Exit");
        BUpdate       = new JButton("Update");
		BUpdate       .setEnabled(false);
        BMaterial     = new JButton("Select a Material");
        
        TMatName      = new AddressField();
    	TMatCode      = new JTextField(20);
        
        TPaymentDate  = new DateField();
        TPaymentDate  . setTodayDate();
       	
        theModel      = new StoreMaterialModel();
        theTable      = new JTable(theModel);
                 
        for(int i=0;i<theModel.ColumnName.length;i++)
        {
           (theTable.getColumnModel()) . getColumn(i) . setPreferredWidth(theModel.iColumnWidth[i]);
        }
    }
    private void setLayouts()
    {
		TitlePanel    . setLayout(new BorderLayout());
        TopLeftPanel  . setLayout(new GridLayout(11,2));
        TopPanel      . setLayout(new GridLayout(2,1));
        MiddlePanel   . setLayout(new BorderLayout());
        BottomPanel   . setLayout(new FlowLayout(FlowLayout.CENTER));
        TopLeftPanel  . setBorder(new TitledBorder(" Filter "));
        MiddlePanel   . setBorder(new TitledBorder(" Entry Details "));
        BottomPanel   . setBorder(new TitledBorder(" Controls "));
         
		this . setTitle("Materials UnLoad Details");
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setResizable(true);
        this.setBounds(0,0,750,600);
    }
    private void addComponents()
    {
    	TitlePanel . add(new JLabel(" Store Material UnLoad Details Entry Screen "));
      
		TopLeftPanel . add(new JLabel(" PAYMENT DATE "))  ;
		TopLeftPanel . add(TPaymentDate);
		TopLeftPanel . add(new JLabel(" ITEM TYPE "))  ;
		TopLeftPanel . add(JCType);
		TopLeftPanel . add(new JLabel(" ITEM DATE "));
		TopLeftPanel . add(TDate);
		TopLeftPanel . add(new JLabel(" MATERIAL TYPE "));
		TopLeftPanel . add(JCMaterialType);
		TopLeftPanel . add(new JLabel(" MATERIAL NAME "));
		TopLeftPanel . add(TMatName);
		TopLeftPanel . add(new JLabel(" "));
		TopLeftPanel . add(BMaterial);
		TopLeftPanel . add(new JLabel(" MATERIAL CODE "));
		TopLeftPanel . add(TMatCode);
		TopLeftPanel . add(new JLabel(" QUANTITY "));
		TopLeftPanel . add(TQuantity);
		TopLeftPanel . add(new JLabel(" RATE "));
		TopLeftPanel . add(TRate);
		TopLeftPanel . add(new JLabel(" UOM TYPE "));
		TopLeftPanel . add(JCUOM);
		TopLeftPanel . add(new JLabel(" SHIFTING TYPE "));
		TopLeftPanel . add(JCShiftingType);
         
		MiddlePanel.add(new JScrollPane(theTable));
			  
		BottomPanel . add(BSave);
		BottomPanel . add(BUpdate);
		BottomPanel . add(BExit);

		TopPanel .add(TitlePanel);
		TopPanel .add(TopLeftPanel);

		this . add(TopLeftPanel,BorderLayout.NORTH);
		this . add(MiddlePanel,BorderLayout.CENTER);
		this . add(BottomPanel,BorderLayout.SOUTH);
	}
    private void addListeners()
    {
        JCMaterialType . addActionListener(new ActList());
        JCType         . addActionListener(new ActList());
        BSave          . addActionListener(new ActList());
        BUpdate        . addActionListener(new ActList());
        BExit          . addActionListener(new ActList());
       	BMaterial      . addActionListener(new ActList());

//    BMaterial      . addActionListener(new MaterialSearchUnLoad(DeskTop,TMatCode,TMatName,VCode,VName,VNameCode,iMillCode,SItemTable,4,TPaymentDate.toNormal()));

		TPaymentDate .TYear.   addFocusListener(new FocListener());             
        theTable       . addMouseListener(new mouseList());
    }
	
    private class ActList implements ActionListener
    {
		public void actionPerformed(ActionEvent ae)   
      	{
        	if(ae.getSource()==JCMaterialType)
          	{
            	int iMaterialType = JCMaterialType.getSelectedIndex();
                           
              	if(iMaterialType==0)
              	{
                	BMaterial.setEnabled(false);
		 			TMatCode.setEnabled(false);
                 	TMatName.setEnabled(true);
              	}
               	if(iMaterialType==1)
              	{
                	TMatName.setEnabled(false);
					BMaterial.setEnabled(true);
              	}
               theModel.setNumRows(0);
               ShowList();
			}
			if(ae.getSource()==JCType)
          	{
            	int iMaterial = JCType.getSelectedIndex();
              
              	if(iMaterial==3)
              	{
                	TDate.setEnabled(false);
              	}
              	else
              	{
                	TDate.setEnabled(true);
              	}
                           
              	theModel.setNumRows(0);
              	ShowList();
			}
                             
          	if(ae.getSource()==BSave)
          	{
            	if((JOptionPane.showConfirmDialog(null,"Do you want to save the Data? ","Confirmation",JOptionPane.YES_NO_OPTION))== 0)
                {
					if(!AlreadyAuthenticate())
                		saveData();
					else
						JOptionPane.showMessageDialog(null,"Data Authenticated For this Month","Info",JOptionPane.INFORMATION_MESSAGE);
                               
                    theModel      . setNumRows(0);
                    ShowList();
				}
          	}
          	if(ae.getSource()==BUpdate)
          	{
				if((JOptionPane.showConfirmDialog(null,"Do you want to Update the Data? ","Confirmation",JOptionPane.YES_NO_OPTION))== 0)
                {
                	UpdateData();
                               
                    theModel      . setNumRows(0);
                    ShowList();
				}
				BSave.setEnabled(true);
				BUpdate.setEnabled(false);
          	}
	  		if(ae.getSource()==BMaterial)
          	{
            	int iMaterialType = JCType.getSelectedIndex();
				String SEntryDate = TPaymentDate.toNormal();
		
           		try
           		{
                	new MaterialSearchUnLoad(DeskTop,TMatCode,TMatName,VCode,VName,VNameCode,iMillCode,SItemTable,iMaterialType,TPaymentDate.toNormal());
				}
           		catch(Exception ex)
           		{
               		System.out.println(ex);
           		}
			}
          	if(ae.getSource()==BExit)
          	{
            	dispose();
          	}
      	}
    }
    private class ItemList implements ItemListener
    {
        public void itemStateChanged(ItemEvent ie)
        {
        	theModel.setNumRows(0);     
		}
    }
	private  class FocListener implements FocusListener
   	{
    	public void focusLost(FocusEvent fe)
        {
        	if (fe.getSource() == TPaymentDate.TYear)
            {
				ShowList();
            }
		}
        public void focusGained(FocusEvent fe)
        {
        }
	}
    private class mouseList extends MouseAdapter
    {
    	public void mouseClicked(MouseEvent me)
        {
        	if(me.getClickCount()==2)
            {              
				BUpdate.setEnabled(true);
				BSave.setEnabled(false);
                clearData();
                int iRow = theTable.getSelectedRow();
                if(iRow==-1)
                	return;
				setData(iRow);
			}
		}
	}
   	private void setDataVector()
    {
        VCode1    = new Vector();
        VType    = new Vector();
        
        VMaterialName = new Vector();
        VMaterialCode = new Vector();
        
        VUOMName = new Vector();
        VUOMCode = new Vector();
		
		VShiftingType = new Vector();
        VShiftingCode = new Vector();
        
        try
        {
			VType.addElement("GRN");
			VCode1.addElement("0");
			VType.addElement("RDC");
			VCode1.addElement("1");
			VType.addElement("ISSUE");
			VCode1.addElement("2");
			VType.addElement("OTHERS");
			VCode1.addElement("3");
			VType.addElement("GATE INWARD");
			VCode1.addElement("4");

			VMaterialName.addElement("Other Items");
			VMaterialCode.addElement("0");
			VMaterialName.addElement("Inventory Items");
			VMaterialCode.addElement("1");
            
            String QS  = " Select UOMCode,UOMName from UOM Order by 2";
            
            ORAConnection connect         =    ORAConnection.getORAConnection();
            Connection     theConnect     =    connect.getConnection();
            Statement      theStatement   =    theConnect.createStatement();
            ResultSet      theResult      =    theStatement.executeQuery(QS);
            while(theResult.next())
            {
           		VUOMCode.addElement(theResult.getString(1));
                VUOMName.addElement(theResult.getString(2));
            }
            theResult.close();
			
			theResult      =    theStatement.executeQuery("Select ShiftCode,ShiftType from ShiftingType");
            while(theResult.next())
            {
                VShiftingCode.addElement(theResult.getString(1));
                VShiftingType.addElement(theResult.getString(2));
            }
            theResult.close();
            theStatement.close();
            
        }
        catch(Exception e)
        {
            
        }
	}
	public boolean AlreadyAuthenticate()
	{
		boolean bFlag = false;
		int iCount = 0;
		String SDate = common.pureDate(TPaymentDate.toNormal());
		
        try
        {
        	String QS = " Select count(*) from  STOREMATERIAL_AUTHENTICATION where "+SDate+" between FromDate and ToDate ";
                        
			ORAConnection  connect        =    ORAConnection.getORAConnection();
			Connection     theConnect     =    connect.getConnection();
			Statement      theStatement   =    theConnect.createStatement();
			ResultSet      theResult      =    theStatement.executeQuery(QS);
			  System.out.println("QS :"+QS);
			while(theResult.next())
            {
            	iCount=theResult.getInt(1);
            }
			if(iCount>0)
				bFlag = true;
         }
         catch(Exception ex)
         {
           ex.printStackTrace();
           return true;
         }        

		return bFlag;
	}
   	public void saveData()
    {
    	try
        {
        	if(theconnect==null)
            {
				connect=ORAConnection.getORAConnection();
				theconnect=connect.getConnection();
			}
			if(theconnect.getAutoCommit())
            	theconnect.setAutoCommit(false);
      
			PreparedStatement prepare = null;
			ResultSet         addResult= null;
			Statement         stat= null;
			String            Qry;
                      
         	if(!isExist())
            {
            	String    QS    = " Select StoreMaterial_Movement_seq.nextval from dual ";
               	stat            = theconnect.createStatement();
               	addResult       = stat.executeQuery(QS);
               	addResult.next();

               	int  iId = addResult.getInt(1);
               
               	String SMaterialName  = "";
               
             //  if((JCMaterialType.getSelectedIndex())==0)
                 SMaterialName  = TMatName.getText().trim();
             /*  if((JCMaterialType.getSelectedIndex())==1)
                     SMaterialName  = BMaterial.getText().trim();*/
               
				String SMaterialCode = common.parseNull(TMatCode.getText().trim());
               	double dQty          = common.toDouble(TQuantity.getText().trim());
               	double dRate         = common.toDouble(TRate.getText().trim());
               	double dAmount       = (dQty * dRate);
               	int iMaterialType    = JCMaterialType.getSelectedIndex();
               	int iItemType        = JCType.getSelectedIndex();
               	int iUOMCode         = getUOMCode((String)JCUOM.getSelectedItem());
               	int iDate            = common.toInt(common.pureDate(TDate.getText()));
               	int iPaymentDate     = common.toInt(TPaymentDate.toNormal());
			   	int iShiftingCode    = getShiftingCode((String)JCShiftingType.getSelectedItem());
			   
               Qry= "insert into StoreMaterial_Movement_Payment(ID,MaterialName,MaterialCode,Quantity,Rate,TotalAmount,SystemName,MaterialType,UOMCode,ItemType,ItemDate,EntryDate,EntryDateTime,PaymentDate,ShiftingCode) values(?,?,?,?,?,?,?,?,?,?,?,to_Char(SysDate, 'YYYYMMDD'),to_Char(SysDate, 'DD.MM.YYYY HH24:MI:SS'),?,?)";
               prepare   = theconnect.prepareStatement(Qry);

               prepare.setInt(1,iId);
               prepare.setString(2,SMaterialName);
               prepare.setString(3,SMaterialCode);
               prepare.setDouble(4,dQty);
               prepare.setDouble(5,dRate);
               prepare.setDouble(6,dAmount);
               prepare.setString(7,AuthSystemName);
               prepare.setInt(8,iMaterialType);
               prepare.setInt(9,iUOMCode);
               prepare.setInt(10,iItemType);
               prepare.setInt(11,iDate);
			   prepare.setInt(12,iPaymentDate);
               prepare.setInt(13,iShiftingCode);
			   
               prepare.executeUpdate();
               
               JOptionPane.showMessageDialog(null,"Data Saved Successfully","Info",JOptionPane.INFORMATION_MESSAGE);
               theconnect.commit();
				
				prepare.close();
                stat.close();
                addResult.close();
               }
               else
               {
                  JOptionPane.showMessageDialog(null," Data Already Exists ","Info",JOptionPane.INFORMATION_MESSAGE);
               }
               
               TMatName.setText("");
               TMatCode.setText("");
               TQuantity.setText("");
               TRate.setText("");
               
          }
          catch(Exception e)
          {
              try
              {
               theconnect.rollback(); 
              }
              catch(Exception ex){}
               String SException = String.valueOf(e);
               if((SException.trim()).equals("java.sql.SQLException: ORA-00001: unique constraint  violated"))
               {
                 JOptionPane . showMessageDialog(null,"Material Name Already Exists","Info",JOptionPane.INFORMATION_MESSAGE);
                  return;
               }
    
  
               System.out.println(e);
               e.printStackTrace();
          }
     }
   private boolean isValidData()
     {
          try
          {
               if(common.parseNull((String)TMatName.getText()).equals("")  )
               {
                    JOptionPane.showMessageDialog(null,"Material Name Must be filled","Error",JOptionPane.ERROR_MESSAGE);
                    TMatName.requestFocus();
                    return false;
               }
             
               return true;
          }
          catch(Exception e)
          {
               System.out.println(e);
          }
          return true;
     }
   
      private boolean isExist()
      {
         int iCount = 0;
		 String SMaterialName = TMatName.getText().trim();
		 String SDate = TPaymentDate.toNormal();
		 int iShiftingCode = getShiftingCode((String)JCShiftingType.getSelectedItem());
         try
         {
            String QS = " select count(1) from StoreMaterial_Movement_Payment where MaterialName ='"+SMaterialName+"' and PaymentDate="+SDate+" and ShiftingCode="+iShiftingCode+" ";
                        
               ORAConnection  connect        =    ORAConnection.getORAConnection();
               Connection     theConnect     =    connect.getConnection();
               Statement      theStatement   =    theConnect.createStatement();
               ResultSet      theResult      =    theStatement.executeQuery(QS);
			   //System.out.println("QS:"+QS);
               while(theResult.next())
               {
                  iCount=theResult.getInt(1);
               }
         }
         catch(Exception ex)
         {
           ex.printStackTrace();
           return true;
         }        
		
		 
         if(iCount>0)
             return true;
         else 
             return false;
    }
    private boolean IsAuthenticate()
      {
         int iCount = 0;
		 String SMaterialName = TMatName.getText().trim();
         try
         {
            String QS = " select count(1) from StoreMaterial_Movement_Payment where MaterialName ='"+SMaterialName+"' and AuthenticateStatus=1 ";
                        
               ORAConnection  connect        =    ORAConnection.getORAConnection();
               Connection     theConnect     =    connect.getConnection();
               Statement      theStatement   =    theConnect.createStatement();
               ResultSet      theResult      =    theStatement.executeQuery(QS);
			   
               while(theResult.next())
               {
                  iCount=theResult.getInt(1);
               }
         }
         catch(Exception ex)
         {
           ex.printStackTrace();
           return true;
         }        
		
		 
         if(iCount>0)
             return true;
         else 
             return false;
    }
     
    public void UpdateData()
    {
        try
        {
            if(theconnect==null)
            {
                connect=ORAConnection.getORAConnection();
                theconnect=connect.getConnection();
                    
            }
            if(theconnect.getAutoCommit())
                 theconnect.setAutoCommit(false);
            
		 Statement         stat= null;
		//ResultSet         addResult= null;
               if(!IsAuthenticate())
               {
                     
            PreparedStatement prepare = null;
            
           
            String            Qry;
               
            String SMaterialName  = "";
               
           /* if((JCMaterialType.getSelectedIndex())==0)*/
                 SMaterialName  = TMatName.getText().trim();
           /* if((JCMaterialType.getSelectedIndex())==1)
                  SMaterialName  = BMaterial.getText().trim();*/
               
               String SMaterialCode = common.parseNull(TMatCode.getText().trim());
               double dQty          = common.toDouble(TQuantity.getText().trim());
               double dRate         = common.toDouble(TRate.getText().trim());
               double dAmount       = (dQty * dRate);
               int iMaterialType    = JCMaterialType.getSelectedIndex();
               int iItemType        = JCType.getSelectedIndex();
               int iUOMCode         = getUOMCode((String)JCUOM.getSelectedItem());
               int iDate            = common.toInt(common.pureDate(TDate.getText()));
			   int iShiftingCode    = getShiftingCode((String)JCShiftingType.getSelectedItem());

			   int iID = getID(SMaterialName);
               
               String SDate = TPaymentDate.toNormal();
               Qry= "update StoreMaterial_Movement_Payment set MaterialName=?,MaterialCode=?,Quantity=?,Rate=?,TotalAmount=?,SystemName=?,MaterialType=?,UOMCode=?,ItemType=?,ItemDate=?,UpdatedDate=to_Char(SysDate, 'YYYYMMDD'),UpdatedDateTime=to_Char(SysDate, 'DD.MM.YYYY HH24:MI:SS'),ShiftingCode=? where ID = "+iID+" "; //MaterialName='"+SMaterialName+"' and PaymentDate="+SDate+" "
               prepare   = theconnect.prepareStatement(Qry);

               prepare.setString(1,SMaterialName);
               prepare.setString(2,SMaterialCode);
               prepare.setDouble(3,dQty);
               prepare.setDouble(4,dRate);
               prepare.setDouble(5,dAmount);
               prepare.setString(6,AuthSystemName);
               prepare.setInt(7,iMaterialType);
               prepare.setInt(8,iUOMCode);
               prepare.setInt(9,iItemType);
               prepare.setInt(10,iDate);
			   prepare.setInt(11,iShiftingCode);
               
               prepare.executeUpdate();
               
               JOptionPane.showMessageDialog(null,"Data Updated","Info",JOptionPane.INFORMATION_MESSAGE);
               theconnect.commit();
				
        	prepare.close();
                
                

               }
               else
               {
                   JOptionPane.showMessageDialog(null," Data Authenticated , U Cann't Update ","Info",JOptionPane.INFORMATION_MESSAGE);
               }
                    
               TMatName.setText("");
               TMatCode.setText("");
               TQuantity.setText("");
               TRate.setText("");

             //   addResult.close();
		//stat.close();               
          }
          catch(Exception e)
          {
              try
              {
               theconnect.rollback(); 
              }
              catch(Exception ex){}
               String SException = String.valueOf(e);
               if((SException.trim()).equals("java.sql.SQLException: ORA-00001: unique constraint  violated"))
               {
                 JOptionPane . showMessageDialog(null,"Material Name Already Exists","Info",JOptionPane.INFORMATION_MESSAGE);
                  return;
               }
    
  
               System.out.println(e);
               e.printStackTrace();
          }
     }
	 private int getID(String SMaterialName)
	 {
		 int iID = 0;
		 String SDate = common.pureDate(TPaymentDate.toNormal());
		 try
		 {
		 
		 ORAConnection oraConnection     =   ORAConnection.getORAConnection();
         theConnection                   =   oraConnection.getConnection();
         Statement  theStatement         =   theConnection.createStatement();
         ResultSet  theResult            =   theStatement.executeQuery("Select ID from StoreMaterial_Movement_Payment where MaterialName='"+SMaterialName+"' and PaymentDate="+SDate+"");
         while (theResult.next())
         {
            iID = theResult.getInt(1);
             
         }
            theResult.close();
            theStatement.close();
		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		 return iID;
		 
	 }
    private void ShowList()
     {
		 
          theModel.setNumRows(0);
          try
          {
               ORAConnection connect         =    ORAConnection.getORAConnection();
               Connection     theConnect     =    connect.getConnection();
               Statement      theStatement   =    theConnect.createStatement();
               ResultSet      theResult      =    theStatement.executeQuery(getQuery());
               int i=0;
               while(theResult.next())
               {
                    i = i+1;
                    organizeMaterialData(theResult,i);
               } 
               theResult.close();
               theStatement.close();
               
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private String getQuery()
     {
         String SDate = common.pureDate(TPaymentDate.toNormal());
         
         String QS = " Select MaterialName,Quantity,Rate,TotalAmount,ItemDate from StoreMaterial_Movement_Payment where PaymentDate="+SDate+" order by 1";
    //System.out.println("QS"+QS);
         return QS;
     }
     private void organizeMaterialData(ResultSet theResult,int iCount) throws Exception
     {
        
          theVector       = new Vector();
          String S1       = theResult.getString(1);
          double S2       = theResult.getDouble(2);
          double S3       = theResult.getDouble(3);
          double S4       = theResult.getDouble(4);
          String S5       = common.parseDate(theResult.getString(5));
        

          theVector.addElement(String.valueOf(iCount));
          theVector.addElement(S1);
          theVector.addElement(String.valueOf(S5));
          theVector.addElement(String.valueOf(S2));
          theVector.addElement(String.valueOf(S3));
          theVector.addElement(String.valueOf(S4));
          

          theModel.appendRow(theVector);
     }
     
    private int getUOMCode(String SUOMName)
    {
        int iUOMCode=-1;
        for(int i=0;i<VUOMCode.size();i++)
        {
            String sUOMName = (String)VUOMName.elementAt(i);
            if(SUOMName.equals(sUOMName))
            {
                iUOMCode = common.toInt(String.valueOf(VUOMCode.elementAt(i)));
                break;
            }
        }
        return iUOMCode;
    }
	private int getShiftingCode(String SShiftingType)
    {
        int iShiftingCode=-1;
        for(int i=0;i<VShiftingCode.size();i++)
        {
            String SShiftingName = (String)VShiftingType.elementAt(i);
            if(SShiftingType.equals(SShiftingName))
            {
                iShiftingCode = common.toInt(String.valueOf(VShiftingCode.elementAt(i)));
                break;
            }
        }
        return iShiftingCode;
    }
  /*  public static void main(String[] args) 
    {
       StoreMaterialUnLoad UnLoad = new StoreMaterialUnLoad();
       UnLoad . setVisible(true);
               
    }*/
    private void clearData()
     {
          TMatName.setText("");
          TQuantity.setText("");
          TRate.setText("");
          TDate.setText("");

         /*inDate.TDay.setText("");
          inDate.TMonth.setText("");
          inDate.TYear.setText("");

          toDate.TDay.setText("");
          toDate.TMonth.setText("");
          toDate.TYear.setText("");

          TOutTime.THours.setText("");
          TOutTime.TMinutes.setText("");


          TInTime.THours.setText("");
          TInTime.TMinutes.setText("");

          JTotDays.setText("");
          JPhoneNo.setText("");*/
         
     }
private void setData(int iRow)
{
    bFlag = true;
    
    String SMaterialName = (String)theModel.getValueAt(iRow, 1);
    String SDate         = TPaymentDate.toNormal();
    
    try
    {
       ORAConnection connect         =    ORAConnection.getORAConnection();
       Connection     theConnect     =    connect.getConnection();
       Statement      theStatement   =    theConnect.createStatement();
       ResultSet      theResult      =    theStatement.executeQuery(getQS(SMaterialName,SDate));
       while(theResult.next())
       {
           String SMatName  = theResult.getString(1);
           double dQuantity = common.toDouble(theResult.getString(2));
           double dRate     = common.toDouble(theResult.getString(3));
           String SUOMName  = theResult.getString(4);
           String SItemDate = common.parseDate(theResult.getString(5));
           int iItemType    = common.toInt(theResult.getString(6));
           int iMaterialType= common.toInt(theResult.getString(7));
		   String SShiftingType  = theResult.getString(8);
          
           TMatName.setText(""+SMatName);
           TQuantity.setText(""+String.valueOf(dQuantity));
           TRate.setText(""+String.valueOf(dRate));
           TDate.setText(SItemDate);
           JCType.setSelectedIndex(iItemType);
           JCMaterialType.setSelectedIndex(iMaterialType);
           JCUOM.setSelectedItem(SUOMName);
		   JCShiftingType.setSelectedItem(SShiftingType);
       }
       
    }
    catch(Exception e)
    {
        e.printStackTrace();
        System.out.println(e);
    }

}
private String getQS(String SMaterialName,String SDate)
 {
     String QS = "";
     
            QS = " Select MaterialName,Quantity,Rate,UOMName,ItemDate,ItemType,MaterialType ,ShiftingCode from StoreMaterial_Movement_Payment"+
                 " Inner join UOM on UOM.UOMCode = StoreMaterial_Movement_Payment.UOMCode"+ 
				 " Inner join ShiftingType on ShiftingType.ShiftCode = StoreMaterial_Movement_Payment.ShiftingCode"+ 				 
                 " where PaymentDate="+SDate+" and MaterialName='"+SMaterialName+"' "   ;
   //  System.out.println("QS:"+QS);
     return QS;
 }
}

