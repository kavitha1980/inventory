
package Kardex;

import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.sql.*;
import java.io.*;
import util.*;
import guiutil.*;
import jdbc.*;

        


import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class StoreMaterialUnLoadPDF
{

    String SFile="",SFromDate="",SToDate="",SDriverName="";
    Vector theVector;
	    
    Common common = new Common();

    int            Lctr      = 100;
    int            Pctr      = 0;

    private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD);
    private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL, BaseColor.RED);
    private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);

    private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
    private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

    private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

    private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
    private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

    private static Font tinyBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
    private static Font tinyNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

    private static Font underBold  = new Font(Font.FontFamily.TIMES_ROMAN, 14,Font.UNDERLINE);

    

   // String SHead[]  = {"SNO","DATE","OPENING KM","CLOSING KM","TOT NOOF KM RUN/DAY","STANDARD TRIPS/DAY ON STD.KM","ACTUAL TRIPS/DAY ON ACTUAL KMS","ACTUAL WATER LOAD TRIPS","CEILING WATER LOAD TRIPS","EXTRA TRIP","INCENTIVE RATE RS.20/EXTRA TRIP","TOT INCENTIVE"};
    int    iWidth[] = {8,25,18,16,13,13,15,25};
     
    Document document;
    PdfPTable table;
    Connection theConnection = null;
	
    ArrayList APaymentDateList;
    StoreMaterialUnLoadPDFClass theClass;

    int iCarriedOver = 0 ;
    int iRoundedNet = 0; 
    int iOpeningValue = 0;
   
    public StoreMaterialUnLoadPDF(String SFile,String SFromDate,String SToDate,int iCarriedOver,int iRoundedNet,int iOpeningValue)
    {
        this.SFile         = SFile;
        this.SFromDate     = SFromDate;
        this.SToDate       = SToDate;
        this.iRoundedNet   = iRoundedNet ;
        this.iCarriedOver  = iCarriedOver ;
        this.iOpeningValue = iOpeningValue;
		
		SetDataVector();
		createPDFFile();
          
    }
	private void SetDataVector()
    {
        APaymentDateList = new ArrayList();
             
        try
        {
            ORAConnection connect         =    ORAConnection.getORAConnection();
            Connection     theConnect     =    connect.getConnection();
            Statement      theStatement   =    theConnect.createStatement();
            ResultSet      theResult      =    theStatement.executeQuery(getQS());
            while(theResult.next())
            {
                String SMaterialName = common.parseNull(theResult.getString(1));
		String SMaterialCode = common.parseNull(theResult.getString(2));
                double dQuantity     = common.toDouble(common.parseNull(theResult.getString(3)));
                double dRate         = common.toDouble(common.parseNull(theResult.getString(4)));
                double dAmount       = common.toDouble(common.parseNull(theResult.getString(5)));
		String SPaymentDate  = common.parseNull(theResult.getString(6));
		String SUOMName      = common.parseNull(theResult.getString(7));
                String SShiftType    = common.parseNull(theResult.getString(8));
                
                int iIndex=getIndexOf(SPaymentDate);

                if(iIndex==-1)
                {
                    theClass            = new StoreMaterialUnLoadPDFClass(SPaymentDate,dQuantity);
                    APaymentDateList.add(theClass);
                    iIndex=APaymentDateList.size()-1;
                }
                theClass = (StoreMaterialUnLoadPDFClass)APaymentDateList.get(iIndex);
                theClass . appendDetails(SMaterialName,SMaterialCode,dQuantity,dRate,dAmount,SPaymentDate,SUOMName,SShiftType);
            }
            theResult.close();
            theStatement.close();
          
            
        }
        catch(Exception e)
        {
            
        }
    }
	
    private String getQS()
    {
        
        String QS = " Select MaterialName,MaterialCode,Quantity,Rate,TotalAmount,PaymentDate,UOMName,ShiftType "+
                    " from StoreMaterial_Movement_Payment "+
                    " Inner join UOM on UOM.UOMCode = StoreMaterial_Movement_Payment.UOMCode "+
					" Left join ShiftingType on ShiftingType.ShiftCode = StoreMaterial_Movement_Payment.ShiftingCode "+
                    " where PaymentDate>="+SFromDate+"  and PaymentDate<="+SToDate+"  and PaymentDate!=21090916 order by PaymentDate";
        System.out.println("QS:"+QS);
        return QS;
    }
    public int getIndexOf(String SPaymentDate)
    {
		int iIndex=-1;

		for(int i=0;i<APaymentDateList.size();i++)
		{
			StoreMaterialUnLoadPDFClass theClass = (StoreMaterialUnLoadPDFClass)APaymentDateList.get(i);
				
			if((theClass.SDate.equals(SPaymentDate))  )
			{
				iIndex=i;
				break;
			}
		}
		return iIndex;
	}   
	private void createPDFFile()
	{
		try
        {
            document = new Document(PageSize.A4);
            PdfWriter.getInstance(document, new FileOutputStream(SFile));
            document.open();

			document.newPage();
			
            table = new PdfPTable(8);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);

            addHead(document,table);
            addBody(document,table);
                        
            JOptionPane.showMessageDialog(null, "PDF File Created in "+SFile,"Info",JOptionPane.INFORMATION_MESSAGE);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
	}
	private void addHead(Document document,PdfPTable table) throws BadElementException
	{
		String        Str1="",Str2="";
		try
        {
            if(Lctr < 42)
                return;

            if(Pctr > 0)
                //  addFoot(document,table,1);

            Pctr++;
              
            Paragraph paragraph;
               
            Str1   = " STORE MATERIAL UNLOAD , FROM  "+common.parseDate(SFromDate)+" TO "+common.parseDate(SToDate);
               
            paragraph = new Paragraph(Str1,bigBold);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setSpacingAfter(10);
            document.add(paragraph);
                           
            Lctr = 16;
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
	}
      
	private void addBody(Document document,PdfPTable table) throws BadElementException
	{
		try
		{
			
			addIncentiveData(document,table);
			document.close();
			
		}
		catch(Exception ex)
		{
			System.out.println(" addIncentivedata PDF "+ex);
			ex.printStackTrace();
		}
	}
   
	private void addIncentiveData(Document document,PdfPTable table)//,String SPaymentDate
	{
		Paragraph paragraph;
        PdfPCell c1;
		double dGrandTotal = 0.0;
		
        try
		{
			
          		c1 = new PdfPCell(new Phrase("",mediumBold));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				c1.setBorder(0);
				c1.setColspan(8);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("S.No",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("MATERIAL NAME",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("MATERIAL CODE",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("QUANTITY",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("RATE",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("AMOUNT",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("UON NAME",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("NATURE OF WORK",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
				table.addCell(c1);
			System.out.println("APaymentDateList.size():"+APaymentDateList.size());
				for(int i=0;i<APaymentDateList.size();i++)
				{
				int iSlNo = 0 ;
				double dTotalAmt = 0;
                StoreMaterialUnLoadPDFClass theClass = (StoreMaterialUnLoadPDFClass)APaymentDateList.get(i);
				String SPaymentDate		     = theClass.SDate;
				
				c1 = new PdfPCell(new Phrase("DATE :"+common.parseDate(SPaymentDate),bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_LEFT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
				c1.setColspan(8);
				table.addCell(c1);
						
				for(int j=0;j<theClass.AMaterialList.size();j++)
				{
					MaterialClass materialclass  = (MaterialClass)theClass.AMaterialList.get(j)   ;
		    
					HashMap theMap = (HashMap)materialclass.AList.get(0);
				
					String SDate		     = ((String)theMap.get("PaymentDate"));
												
					if(SPaymentDate.equals(SDate))
					{
						iSlNo = iSlNo+1					;
						String SSno              	 = String.valueOf(iSlNo);
						String SMaterialName		 = ((String)theMap.get("MaterialName"));
						String SMaterialCode		 = ((String)theMap.get("MaterialCode"));
						double dQuantity       		 = (common.toDouble(String.valueOf(theMap.get("Quantity"))));
						double dRate        		 = (common.toDouble(String.valueOf(theMap.get("Rate"))));
						double dAmount       		 = (common.toDouble(String.valueOf(theMap.get("Amount"))));
						String SUOMName   		     = ((String)theMap.get("UOMName"));
						String SShiftType 		     = ((String)theMap.get("ShitType"));
																
						dTotalAmt = dTotalAmt + dAmount ;
													
						c1 = new PdfPCell(new Phrase(common.Pad(SSno,9),bigNormal));
						c1.setHorizontalAlignment(Element.ALIGN_CENTER);
						c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
						table.addCell(c1);

						c1 = new PdfPCell(new Phrase(SMaterialName,bigNormal));
						c1.setHorizontalAlignment(Element.ALIGN_LEFT);
						c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
						table.addCell(c1);
						  
						c1 = new PdfPCell(new Phrase(SMaterialCode,bigNormal));
						c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
						table.addCell(c1);
							 
						c1 = new PdfPCell(new Phrase(String.valueOf(dQuantity),bigNormal));
						c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
						table.addCell(c1);

						c1 = new PdfPCell(new Phrase(String.valueOf(dRate),bigNormal));
						c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
						table.addCell(c1);
							 
						c1 = new PdfPCell(new Phrase(String.valueOf(dAmount),bigNormal));
						c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
						table.addCell(c1);
							 
						c1 = new PdfPCell(new Phrase(SUOMName,bigNormal));
						c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
						table.addCell(c1);
						
						c1 = new PdfPCell(new Phrase(SShiftType,bigNormal));
						c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
						table.addCell(c1);
						
						
									
						Lctr = Lctr+1;
			   
						
					}
					
				}
				dGrandTotal = dGrandTotal + dTotalAmt ;
				
				c1 = new PdfPCell(new Phrase("DATE",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(3);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase(common.parseDate(SPaymentDate),bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(1);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("TOTAL",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(1);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase(String.valueOf(dTotalAmt),bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder( Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(1);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder( Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(2);
				table.addCell(c1);
				
				
			}	
			c1 = new PdfPCell(new Phrase("GRAND TOTAL",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(5);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase(String.valueOf(dGrandTotal),bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder( Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(1);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder( Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(2);
				table.addCell(c1);

				// For CarriedOver, RoundedNet

				c1 = new PdfPCell(new Phrase("Opening ",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(5);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase(String.valueOf(iOpeningValue),bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder( Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(1);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder( Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(2);
				table.addCell(c1);

				c1 = new PdfPCell(new Phrase("Carried Over",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(5);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase(String.valueOf(iCarriedOver),bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder( Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(1);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder( Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(2);
				table.addCell(c1);

				c1 = new PdfPCell(new Phrase("RoundedNet",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(5);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase(String.valueOf(iRoundedNet),bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder( Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(1);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase("",bigBold));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder( Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				c1.setColspan(2);
				table.addCell(c1);
			
		    document.add(table);
			
		} 
		catch(Exception ex)
		{
			ex.printStackTrace();
			System.out.println(ex);
		}
				
/*			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
					 
			c1 = new PdfPCell(new Phrase(common.Pad(" TOTAL ",9),bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
				 
			c1 = new PdfPCell(new Phrase(common.getRound(dTotalAmt,2),bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
                                              */
        
		

	}
   
   }
