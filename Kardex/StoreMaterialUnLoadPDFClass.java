package Kardex;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.sql.*;
import util.*;
import guiutil.*;
import java.net.InetAddress;
import jdbc.*;



public class StoreMaterialUnLoadPDFClass
{
    String SDate = ""; 
    double dAmount = 0.0,dQuantity= 0.0 ;  
    Common common = new Common();  
    public ArrayList AMaterialList;
	
    public StoreMaterialUnLoadPDFClass(String SDate,double dQuantity)
    {
        this.SDate    = SDate ;
        this.dQuantity = dQuantity;
        AMaterialList =  new ArrayList();
    }
   
    public void appendDetails(String SMaterialName,String SMaterialCode,double dQuantity,double dRate,double dAmount,String SPaymentDate,String SUOMName,String SShiftType)
    {
		
		MaterialClass     materialclass = null;
        int iIndex = getIndexOf(SMaterialName,SShiftType);
                
        if(iIndex==-1)
        {
            materialclass = new MaterialClass( SMaterialName,SShiftType);
            AMaterialList.add(materialclass);
            iIndex=AMaterialList.size()-1;
        }
        materialclass = (MaterialClass)AMaterialList.get(iIndex);
        materialclass.appendDetails(SMaterialName,SMaterialCode,dQuantity,dRate,dAmount,SPaymentDate,SUOMName,SShiftType);
		
		/*HashMap theMap = new HashMap();
		
		theMap.put("MaterialName",SMaterialName);
		theMap.put("MaterialCode",SMaterialCode);
		theMap.put("Quantity",dQuantity);
		theMap.put("Rate",dRate);
		theMap.put("Amount",dAmount);
		theMap.put("PaymentDate",SPaymentDate);
		theMap.put("UOMName",SUOMName);
		theMap.put("ShiftType",SShiftType);
		
		AMaterialList . add(theMap);*/
      
    }
	public int getIndexOf(String SMaterialName,String SShiftType)
    {
        int iIndex=-1;
        for(int i=0; i<AMaterialList.size(); i++)
        {    
            MaterialClass materialclass  = (MaterialClass)AMaterialList.get(i);
             if((materialclass.SMaterialName.equals(SMaterialName))  && (materialclass.SShiftType.equals(SShiftType)) )
              {
                iIndex = i;
                break;
            }
        }
        return iIndex;           
    }
         
}
