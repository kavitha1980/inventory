package Kardex;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import jdbc.*;

import util.*;
import guiutil.*;


public class TestingLedger extends JInternalFrame
{
     JPanel         TopPanel,TopLeft,TopRight,MiddlePanel,BottomPanel,EntryPanel,NonStockPanel,ServiceStockPanel;
     JButton        BMaterial,BApply;
     JTextField     TMatCode;
	 JTabbedPane jTabPane;

     JLayeredPane   DeskTop;
     Vector         VCode,VName,VNameCode;
     StatusPanel    SPanel;
     Common         common = new Common();
     int            iMillCode;
     String         SStDate,SEnDate;
     String         SItemTable;
     String         SSupTable;
	 Font      font;
	 JTable theTable,theTable1;
	 TestingLedgerModel theModel;
	 TestingLedgerModel1 theModel1;
	 ArrayList ADataList,AServiceStockList;
	 double  dOpeningQty,dOpeningValue,dRate,dServiceStockRate;
	 
	

     public TestingLedger(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iMillCode,String SStDate,String SEnDate,String SItemTable,String SSupTable)
     {
          this.DeskTop   = DeskTop;
          this.VCode     = VCode;
          this.VName     = VName;
          this.VNameCode = VNameCode;
          this.SPanel    = SPanel;
          this.iMillCode = iMillCode;
          this.SStDate   = SStDate;
          this.SEnDate   = SEnDate;
          this.SItemTable     = SItemTable;
          this.SSupTable  = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel       = new JPanel(true);
          TopLeft        = new JPanel(true);
          TopRight       = new JPanel(true);
          BottomPanel    = new JPanel(true);
          MiddlePanel    = new JPanel(true);
		  EntryPanel    	= new JPanel(true);
		  NonStockPanel = new JPanel(true);
		   ServiceStockPanel = new JPanel(true);
          BMaterial      = new JButton("Select a Material");
          BApply         = new JButton("Apply");
          TMatCode       = new JTextField();
          TMatCode       . setEditable(false);
		  jTabPane         = new JTabbedPane();
		  font             = new Font("Arial",Font.BOLD,11);
		  jTabPane.setFont(font);
		 theModel            = new TestingLedgerModel();
        theTable            = new JTable(theModel);
		 theModel1 			 = new TestingLedgerModel1();
                 
        for(int i=0;i<theModel.ColumnName.length;i++)
        {
           
            (theTable.getColumnModel()) . getColumn(i) . setPreferredWidth(theModel.iColumnWidth[i]);
        }
		
		theTable1           = new JTable(theModel1);
		
		for(int i=0;i<theModel1.ColumnName.length;i++)
        {
           
            (theTable1.getColumnModel()) . getColumn(i) . setPreferredWidth(theModel1.iColumnWidth[i]);
        }
		  
		  
     }

     public void setLayouts()
     {
          TopPanel       . setLayout(new GridLayout(1,2));
          MiddlePanel    . setLayout(new BorderLayout());
          TopLeft        . setLayout(new GridLayout(2,1));
          TopRight       . setLayout(new BorderLayout());
          BottomPanel    . setLayout(new GridLayout(2,4));
		  EntryPanel.setLayout(new BorderLayout());
		  NonStockPanel.setLayout(new BorderLayout());
		  ServiceStockPanel.setLayout(new BorderLayout());
		  jTabPane.setBackground(new Color(213,234,255));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setTitle("Stock Ledger for the Selected Material");
          setBounds(0,0,1200,700);
     }

     public void addComponents()
     {
          TopLeft             . add(BMaterial);
          TopLeft             . add(TMatCode);
          TopRight            . add("Center",BApply);

          TopPanel            . add(TopLeft);
          TopPanel            . add(TopRight);
		  MiddlePanel.add("North", jTabPane);
		  jTabPane.addTab("Stock",EntryPanel);
		  jTabPane.addTab("NonStock",NonStockPanel);
		  jTabPane.addTab("ServiceStock",ServiceStockPanel);
		  

          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("Center",MiddlePanel);
          getContentPane()    . add("South",BottomPanel);

          TopLeft             . setBorder(new TitledBorder("Material Selection"));
          TopRight            . setBorder(new TitledBorder("Click To View"));
          BottomPanel         . setBorder(new TitledBorder("Control Figures"));
     }

     public void addListeners()
     {
          //BMaterial           . addActionListener(new MaterialSearch(DeskTop,TMatCode,BMaterial,VCode,VName,VNameCode,iMillCode,SItemTable));
		  BMaterial           . addActionListener(new ActList());
          BApply              . addActionListener(new ApplyList());
     }
	 
	 public class ActList implements ActionListener
     {
         public void actionPerformed(ActionEvent ae)
         {
             ItemSearchListNew itemsearchlist = new ItemSearchListNew(TMatCode,BMaterial,iMillCode,SItemTable);
			 
         }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               EntryPanel    . removeAll();
               BottomPanel    . removeAll();
			   NonStockPanel    . removeAll();
			    ServiceStockPanel .removeAll();
			   dRate=0;
			   
               Item1     IC             = new Item1(TMatCode.getText(),SStDate,SEnDate,iMillCode,SItemTable,SSupTable);
               TabReport tabreport      = new TabReport(IC.getRowData(),IC.getColumnData(),IC.getColumnType());
               EntryPanel    . add(tabreport);
			   
			
			   setDataVector();
			   setRateVector();
			   setOpening();
			   setTableDetails();
			   NonStockPanel        .add(new JScrollPane(theTable));
			   
			   serviceStockVector();
			   setServiceRateVector();
			   setServiceOpening();
			   setServiceStockTableDetails();
			   ServiceStockPanel        .add(new JScrollPane(theTable1));

              

               DeskTop        . updateUI();
          }
     }
	 
	 
private void setTableDetails()
{
        
		 double dReceiptQty =0; 
		 double dStock =0;
		 
		 double dPrevStk=0;
          
           theModel.setNumRows(0);
		   
		    ArrayList theList1;
		   theList1 = new ArrayList();
		   
		   System.out.println("comming non stock open qty"+dOpeningQty);
		   
		   //dOpeningQty=5;
		   
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("Opening");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   //theList1 . add(common.getRound(dOpeningQty,1));
		   
		   theList1 . add(String.valueOf(dOpeningQty));
		   
		   //theList1 . add(common.getRound(dOpeningValue,2));
		   //theList1 . add(common.getRound(dOpeningValue/dOpeningQty,3));

		   theList1 . add(common.getRound(dRate*dOpeningQty,2));
		   theList1 . add(common.getRound((dRate*dOpeningQty)/dOpeningQty,3));
		   
		   
		   theModel . appendRow(new Vector(theList1));
		   theList1 = null;
		   
		   dPrevStk = dOpeningQty;
		   
          for(int i=0;i<ADataList.size();i++)
          {
               HashMap theMap = (HashMap)ADataList.get(i);
			    
				double dStockQty = common.toDouble((String)theMap.get("DOCQTY"));
				int iType =common.toInt((String)theMap.get("DOCTYPE"));
				
				
				//System.out.println("Stock Qty Non Stock	:"+dStockQty);
				//System.out.println("Type Non Stock		:"+iType);
				
               
			    ArrayList theList;
               
			   if(iType==1)
			   {
				   
				      dPrevStk = dPrevStk+common.toDouble((String)theMap.get("DOCQTY"));
				   
				  
				   theList                     = new ArrayList();
				   theList                     . clear();
				   theList                     . add(common.parseDate((String)theMap.get("DOCDATE")));
				   theList                     . add((String)theMap.get("DOCNO2"));
				   theList                     . add((String)theMap.get("DOCNO"));
				   theList                     . add("Receipt");
				   theList                     . add((String)theMap.get("UNIT_NAME"));
				   theList                     . add("");
				   theList                     . add((String)theMap.get("DEPT_NAME"));
				   theList                     . add((String)theMap.get("GROUPNAME"));
				   theList                     . add((String)theMap.get("DOCQTY"));
				   theList                     . add(common.getRound(dStockQty*dRate,2));
				   theList                     . add("");
				   theList                     . add("");
				   theList                     . add(common.getRound(dPrevStk,3));
				   theList                     . add(common.getRound(dPrevStk*dRate,2));
				   theList                     . add(common.getRound(dRate,3));
				   theModel                    . appendRow(new Vector(theList));
				   theList                     = null;
			   }
			   if(iType==2)
			   {
				   
				   
				   if(dPrevStk> common.toDouble((String)theMap.get("DOCQTY")) ) {
					   
					   dPrevStk = dPrevStk-common.toDouble((String)theMap.get("DOCQTY"));
				   }else {
					   dPrevStk = common.toDouble((String)theMap.get("DOCQTY"))-dPrevStk;
				   }
				   
				   theList                     = new ArrayList();
				   theList                     . clear();
				   theList                     . add(common.parseDate((String)theMap.get("DOCDATE")));
				   theList                     . add((String)theMap.get("DOCNO2"));
				   theList                     . add((String)theMap.get("DOCNO"));
				   theList                     . add("Issue");
				   theList                     . add((String)theMap.get("UNIT_NAME"));
				   theList                     . add("");
				   theList                     . add((String)theMap.get("DEPT_NAME"));
				   theList                      . add((String)theMap.get("GROUPNAME"));
				   theList                      . add("");
				   theList                      . add("");
				   theList                      . add((String)theMap.get("DOCQTY"));
				   theList                      . add(common.getRound(dStockQty*dRate,2));
				   theList                      . add(common.getRound(dPrevStk,3));
				   theList                      . add(common.getRound(dPrevStk*dRate,2));
				   theList                      . add(common.getRound(dRate,3));
				   theModel                     . appendRow(new Vector(theList));
				   theList                         = null;
			  
			   }
               
               
         }
		  
}
	 
	 

/*private void setTableDetails1()
{
        
		 double dReceiptQty =0; 
		 double dStock =0;
          
           theModel.setNumRows(0);
		   
		    ArrayList theList1;
		   theList1 = new ArrayList();
		   
		   System.out.println("comming non stock open qty"+dOpeningQty);
		   
		   //dOpeningQty=5;
		   
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("Opening");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   //theList1 . add(common.getRound(dOpeningQty,1));
		   
		   theList1 . add(String.valueOf(dOpeningQty));
		   
		   //theList1 . add(common.getRound(dOpeningValue,2));
		   //theList1 . add(common.getRound(dOpeningValue/dOpeningQty,3));

		   theList1 . add(common.getRound(dRate*dOpeningQty,2));
		   theList1 . add(common.getRound((dRate*dOpeningQty)/dOpeningQty,3));
		   
		   
		   theModel . appendRow(new Vector(theList1));
		   theList1 = null;
		   
          for(int i=0;i<ADataList.size();i++)
          {
               HashMap theMap = (HashMap)ADataList.get(i);
			    
				double dStockQty = common.toDouble((String)theMap.get("DOCQTY"));
				int iType =common.toInt((String)theMap.get("DOCTYPE"));
				
				
				//System.out.println("Stock Qty Non Stock	:"+dStockQty);
				//System.out.println("Type Non Stock		:"+iType);
				
               
			    ArrayList theList;
               
			   if(iType==1)
			   {
				   //dReceiptQty = dStock+common.toDouble((String)theMap.get("DOCQTY"));
				   
				   
				    if(dReceiptQty==0) {
						
						dReceiptQty = dOpeningQty;
						
					}
				   
				   
				   
				   
				   dReceiptQty += common.toDouble((String)theMap.get("DOCQTY"));
				   
				   
				   
				  
				   theList                     = new ArrayList();
				   theList                     . clear();
				   theList                     . add(common.parseDate((String)theMap.get("DOCDATE")));
				   theList                     . add((String)theMap.get("DOCNO2"));
				   theList                     . add((String)theMap.get("DOCNO"));
				   theList                     . add("Receipt");
				   theList                     . add((String)theMap.get("UNIT_NAME"));
				   theList                     . add("");
				   theList                     . add((String)theMap.get("DEPT_NAME"));
				   theList                     . add((String)theMap.get("GROUPNAME"));
				   theList                     . add((String)theMap.get("DOCQTY"));
				   theList                     . add(common.getRound(dStockQty*dRate,2));
				   theList                     . add("");
				   theList                     . add("");
				   theList                     . add(common.getRound(dReceiptQty,3));
				   theList                     . add(common.getRound(dReceiptQty*dRate,2));
				   theList                     . add(common.getRound(dRate,3));
				   theModel                    . appendRow(new Vector(theList));
				   theList                     = null;
			   }
			   if(iType==2)
			   {
				    if(dReceiptQty==0) {
						
						dReceiptQty = dOpeningQty;
						
					}
				   
				     dStock =  dReceiptQty - common.toDouble((String)theMap.get("DOCQTY"));
				   
				   
               theList                     = new ArrayList();
               theList                     . clear();
               theList                     . add(common.parseDate((String)theMap.get("DOCDATE")));
               theList                     . add((String)theMap.get("DOCNO2"));
               theList                     . add((String)theMap.get("DOCNO"));
               theList                     . add("Issue");
               theList                     . add((String)theMap.get("UNIT_NAME"));
               theList                     . add("");
               theList                     . add((String)theMap.get("DEPT_NAME"));
               theList                      . add((String)theMap.get("GROUPNAME"));
			   theList                      . add("");
			   theList                      . add("");
			   theList                      . add((String)theMap.get("DOCQTY"));
			   theList                      . add(common.getRound(dStockQty*dRate,2));
			   theList                      . add(common.getRound(dStock,3));
			   theList                      . add(common.getRound(dStock*dRate,2));
			   theList                      . add(common.getRound(dRate,3));
               theModel                     . appendRow(new Vector(theList));
			   
			   dReceiptQty = dStock;
			   
			   //dReceiptQty=0;
			   
               theList                         = null;
			  
			   }
               
               
         }
		  //dReceiptQty=0;
}*/

private void setRateVector()
{
	dRate =0;
	String SCode=TMatCode.getText() ;
	
	try
	{
		String QS =  	" select itemcode as code,unitrate as rate,netvalue as value,stock as nonstock"+ 
						" from"+
						" ("+
						" Select t1.ItemCode , t1.Item_Name, t1.Stock,"+ 
						" round((GRN.invamount / GRN.invqty), 2) as unitrate,"+ 
						" round((t1.stock * ((GRN.invamount/GRN.invqty))), 2) as netvalue"+
						" from"+  
						" ( "+
						" Select t.ItemCode, t.Item_Name, t.Stock, nvl(Max(Grn.Id), 0) as MaxGRNId from"+ 
						" ("+ 
						" Select itemstock_transfer.itemcode, invitems.item_name, Sum(itemstock_transfer.stock) as Stock"+
						" from itemstock_transfer"+  
						" Inner join invitems on invitems.item_code =  itemstock_transfer.itemcode"+  
						" where itemcode='"+SCode+"' and itemstock_transfer.TRANFERSTATUS= 1 and itemstock_transfer.millcode=0"+  
						" and to_char(ItemStock_transfer.Transferdate,'yyyymmdd') <= '"+SEnDate+"'"+
						" group  by Itemstock_transfer.itemcode, invitems.item_name"+
						" )t "+ 
						" left Join GRN on GRN.Code = t.ItemCode and grn.rejflag=0"+ 
						" Group by t.ItemCode, t.Item_Name, t.Stock"+
						" Order by 1"+
						" )t1"+ 
						" left Join GRN on GRN.Id = t1.MaxGrnId"+ 
						" Order by 1"+ 
						" )";	
					 
			    ORAConnection  connect        = ORAConnection.getORAConnection();
				Connection     theConnection  = connect.getConnection();
				Statement      state           = theConnection.createStatement();
                ResultSet res = state.executeQuery(QS);
			
               while(res.next())
				{
					
					dRate  =common.toDouble(res.getString(2));
				}
				
			 res.close();
			 state.close();
	}
			
			catch(Exception ex)
			{
			ex.printStackTrace();
			}
	
}

private void setOpening()
{
	try
	{
		dOpeningQty=0;
		dOpeningValue=0;
		String SCode=TMatCode.getText() ;
	
	/*String QS = " select item_name,sum(itemstock+stock-issueqty) as openingqty,"+
				" sum(itemvalue+stockvalue-issuevalue) as openingval from("+
				" select item_name,sum(itemstock.stock) as itemstock,sum(ItemStock_transfer.stock) as stock ,"+
				" sum(NonstockIssue.qty) as issueqty,"+
				" sum(itemstock.stockvalue) as itemvalue,sum(ItemStock_transfer.stockvalue) as stockvalue ,"+
				" sum(NonstockIssue.issuevalue) as issuevalue"+
				" from invitems"+
				" inner join ItemStock_transfer on ItemStock_transfer.itemcode = invitems.item_code"+
				" inner join NonstockIssue on NonstockIssue.code =  invitems.item_code"+
				" inner join itemstock on itemstock.itemcode = invitems.item_code"+
				" where invitems.item_code='"+SCode+"' "+
				" group by item_name)"+
				" group by item_name";*/
				
			/*String QS= "SELECT SUM(DOCqTY),SUM(dOCvALUE) FROM ( "+
			" select  to_Char(itemstock_transfer.transferdate,'yyyymmdd') as docdate, "+
				 " ItemStock_transfer.Stock as docqty,0 as  docno,ItemStock_transfer.Stockvalue as docvalue, "+
				 " ''as unit_name,'' as dept_name,0 as DocNo2,'1' as doctype,'' as Group_Name "+
				 " from ItemStock_transfer "+
				 " where  ItemStock_transfer.itemcode = '"+SCode+"'  "+
				 " and to_Char(itemstock_transfer.transferdate,'yyyymmdd') <'"+SStDate+"' "+
				 " and ItemStock_transfer.MillCode=0 "+
				 " union all "+
				 " select to_Char(to_date(nonstockissue.issuedate,'yyyymmdd'),'yyyymmdd') as docdate, "+
				 " nonstockissue.qty*-1 as docqty,nonstockissue.uirefno as docno,nonstockissue.issuevalue as docvalue, "+
				 " unit.unit_name,Dept.dept_name,nonstockissue.issueno as DocNo2,'2' as doctype,Cata.Group_Name "+
				 " from nonstockissue "+
				 " Inner Join Dept On NonstockIssue.Dept_Code = Dept.Dept_Code "+
				 " and NonstockIssue.Code = '"+SCode+"'  "+
				 " and NonstockIssue.IssueDate <'"+SStDate+"'  "+
				 " and NonstockIssue.MillCode=0 "+
				 " Inner Join Unit On NonstockIssue.Unit_Code = Unit.Unit_Code "+
				 " Inner Join Cata On Cata.Group_Code = NonstockIssue.Group_Code order by 1,8,3) ";*/
				 
				 
	/*String QS	=	 " Select Code,Sum(Stock+IssQty) as Stock from "+
					 " (Select ItemCode as Code,Sum(Stock) as Stock,0 as IssQty"+
					 " From ItemStock "+
					 " Where HodCode=6125 AND itemcode='"+SCode+"'   and MillCode="+iMillCode+
					 " Group by ItemCode "+
					 " Union All "+
					 " Select Code,0 as Stock,Sum(Qty) as IssQty "+          
					 " From NonStockIssue "+
					 " Where MillCode="+iMillCode+"  AND code='"+SCode+"'  and IssueDate >'"+SStDate+"'"+
					 " Group by Code) "+
					 " Group by Code having Sum(Stock+IssQty)>0 ";*/
					 
					 
				String SQS1 =" select Stock from itemstock where itemcode='"+SCode+"' and hodcode=6125 and MillCode="+iMillCode+"	";				 
				
				String SQS2 ="  select sum(qty) from NonstockIssue where code='"+SCode+"' and issuedate>='"+SStDate+"' and MillCode="+iMillCode+"	";				 				
				 
				String SQS3	= " select sum(stock) from ItemStock_transfer where itemcode='"+SCode+"' and to_Char(itemstock_transfer.transferdate,'yyyymmdd') >='"+SStDate+"'  and MillCode="+iMillCode+"	 ";				 
				 
				 
				 
				 System.out.println("noN sTOCK oPEN QS1 -->"+SQS1);
				 
				 System.out.println("noN sTOCK oPEN QS2 -->"+SQS2);
				 
				 System.out.println("noN sTOCK oPEN QS3 -->"+SQS3);
				 
				 
				 double dItemStk=0;
				 double dIssStk=0;
				 double dRecStk=0;
				
				
				ORAConnection  connect        = ORAConnection.getORAConnection();
				Connection     theConnection  = connect.getConnection();
				Statement      state           = theConnection.createStatement();
				
				
                /*ResultSet resultset = state.executeQuery(QS);
			
               while(resultset.next())
				{
					//dOpeningQty = common.toDouble(resultset.getString(1));
					//dOpeningValue = common.toDouble(resultset.getString(2)); 
					
					dOpeningQty = common.toDouble(resultset.getString(2));
					//dOpeningValue = common.toDouble(resultset.getString(2)); 
					
				}*/
				
                ResultSet resultset = state.executeQuery(SQS1);
			
                while(resultset.next())
				{
					dItemStk = common.toDouble(resultset.getString(1));
				}
				
				resultset.close();
				
				resultset = state.executeQuery(SQS2);
				
                while(resultset.next())
				{
					dIssStk = common.toDouble(resultset.getString(1));
				}
				resultset.close();
				
				resultset = state.executeQuery(SQS3);
				
                while(resultset.next())
				{
					dRecStk = common.toDouble(resultset.getString(1));
				}
				resultset.close();
				state.close();				
				
				
				System.out.println("Non Stock dItemStk :"+dItemStk);
				System.out.println("Non Stock dIssStk :"+dIssStk);
				System.out.println("Non Stock dRecStk :"+dRecStk);
				
				dOpeningQty = (dItemStk+dIssStk)-dRecStk;
				
				System.out.println("Non Stock dOpeningQty :"+dOpeningQty);
				
							 
			 
	}
			
			catch(Exception ex)
			{
			ex.printStackTrace();
			}
}	 
private void setDataVector()
{
	ADataList = new ArrayList();
	String SCode=TMatCode.getText() ;
	try
	{
	
	String QS = 	" select  to_Char(itemstock_transfer.transferdate,'yyyymmdd') as docdate,"+
					" ItemStock_transfer.Stock as docqty,0 as  docno,ItemStock_transfer.Stockvalue as docvalue,"+
					" ''as unit_name,'' as dept_name,0 as DocNo2,'1' as doctype,'' as Group_Name"+
					" from ItemStock_transfer"+
					" where  ItemStock_transfer.itemcode = '"+SCode+"' "+
					" and to_Char(itemstock_transfer.transferdate,'yyyymmdd') >='"+SStDate+"' "+
					" and to_Char(itemstock_transfer.transferdate,'yyyymmdd') <='"+SEnDate+"' "+
					" and ItemStock_transfer.MillCode=0"+
					" union all"+
					" select to_Char(to_date(nonstockissue.issuedate,'yyyymmdd'),'yyyymmdd') as docdate,"+
					" nonstockissue.qty as docqty,nonstockissue.uirefno as docno,nonstockissue.issuevalue as docvalue,"+
					" unit.unit_name,Dept.dept_name,nonstockissue.issueno as DocNo2,'2' as doctype,Cata.Group_Name"+
					" from nonstockissue"+
					" Inner Join Dept On NonstockIssue.Dept_Code = Dept.Dept_Code"+
					" and NonstockIssue.Code = '"+SCode+"' "+
					" and NonstockIssue.IssueDate >='"+SStDate+"' and NonstockIssue.IssueDate <='"+SEnDate+"' "+
					" and NonstockIssue.MillCode=0"+
					" Inner Join Unit On NonstockIssue.Unit_Code = Unit.Unit_Code"+
					" Inner Join Cata On Cata.Group_Code = NonstockIssue.Group_Code order by 1,8,3";

						
  
				ORAConnection  connect        = ORAConnection.getORAConnection();
				Connection     theConnection  = connect.getConnection();
				Statement      state           = theConnection.createStatement();
                ResultSet resultset = state.executeQuery(QS);
			
               while(resultset.next())
				{
					 HashMap theMap = new HashMap();
					 
					 theMap.put("DOCDATE",common.parseNull(resultset.getString(1)));
					 theMap.put("DOCQTY",common.parseNull(resultset.getString(2)));
					 theMap.put("DOCNO",common.parseNull(resultset.getString(3)));
					 theMap.put("DOCVALUE",common.parseNull(resultset.getString(4)));
					 theMap.put("UNIT_NAME",common.parseNull(resultset.getString(5)));
					 theMap.put("DEPT_NAME",common.parseNull(resultset.getString(6)));
					 theMap.put("DOCNO2",common.parseNull(resultset.getString(7)));
					 theMap.put("DOCTYPE",common.parseNull(resultset.getString(8)));
					 theMap.put("GROUPNAME",common.parseNull(resultset.getString(9)));
					 
					 
					 ADataList.add(theMap);
					 
				}
			 resultset.close();
			 state.close();
		}
			catch(Exception ex){
			ex.printStackTrace();
			}
			
} 


private void serviceStockVector()
{
	AServiceStockList = new ArrayList();
	String SCode=TMatCode.getText() ;
	try
	{
	/*
	String QS = " select  to_char(servicestocktransfer.transferstatusdate,'yyyymmdd') as docdate,"+
				" servicestocktransfer.Stock as docqty,'0 ' as  docno,servicestocktransfer.Stockvalue as docvalue,"+
				" ''as unit_name,stockgroup.groupname as deptname ,'0' as DocNo2,'1' as doctype,'' as Group_Name"+
				" from servicestocktransfer"+
				" Inner Join stockgroup On servicestocktransfer.DeptCode = stockgroup.groupcode"+
				" where  servicestocktransfer.itemcode = '"+SCode+"' "+
				" and to_char(servicestocktransfer.transferstatusdate,'yyyymmdd') >='"+SStDate+"' "+
				" and to_char(servicestocktransfer.transferstatusdate,'yyyymmdd') <='"+SEnDate+"' "+
				" and servicestocktransfer.MillCode=0"+
				" union all"+
				" select Servicestockissue.issuedate as docdate,"+
				" Servicestockissue.qty as docqty,Servicestockissue.uirefno as docno,Servicestockissue.issuevalue as docvalue,"+
				" unit.unit_name,Dept.dept_name,Servicestockissue.issueno as DocNo2,'2' as doctype,Cata.Group_Name"+
				" from Servicestockissue"+
				" Inner Join Dept On Servicestockissue.DeptCode = Dept.Dept_Code"+
				" and Servicestockissue.Code = '"+SCode+"' "+
				" and Servicestockissue.IssueDate >='"+SStDate+"'  and Servicestockissue.IssueDate <='"+SEnDate+"' "+
				" and Servicestockissue.MillCode=0"+
				" Inner Join Unit On Servicestockissue.UnitCode = Unit.Unit_Code"+
				" Inner Join Cata On Cata.Group_Code = Servicestockissue.GroupCode order by 1,8,3";   */
				
				
	String QS = " select docdate,docqty,docno,docvalue,unit_name,deptname,DocNo2,doctype,Group_Name from ( "+
				" select  to_char(servicestocktransfer.transferstatusdate,'yyyymmdd') as docdate,"+
				" servicestocktransfer.Stock as docqty,'0 ' as  docno,servicestocktransfer.Stockvalue as docvalue,"+
				" ''as unit_name,stockgroup.groupname as deptname ,'0' as DocNo2,'1' as doctype,'' as Group_Name"+
				" from servicestocktransfer"+
				" left Join stockgroup On servicestocktransfer.DeptCode = stockgroup.groupcode"+
				" where  servicestocktransfer.itemcode = '"+SCode+"' "+
				" and to_char(servicestocktransfer.transferstatusdate,'yyyymmdd') >='"+SStDate+"' "+
				" and to_char(servicestocktransfer.transferstatusdate,'yyyymmdd') <='"+SEnDate+"' "+
				" and servicestocktransfer.MillCode=0"+
				" union all"+
				" select to_char(Servicestockissue.issuedate) as docdate,"+
				" to_char(Servicestockissue.qty) as docqty,to_Char(Servicestockissue.uirefno) as docno,to_char(Servicestockissue.issuevalue) as docvalue,"+
				" unit.unit_name,Dept.dept_name,to_char(Servicestockissue.issueno) as DocNo2,'2' as doctype,Cata.Group_Name"+
				" from Servicestockissue"+
				" inner Join Dept On Servicestockissue.Dept_Code = Dept.Dept_Code"+
				" and Servicestockissue.Code = '"+SCode+"' "+
				" and Servicestockissue.IssueDate >='"+SStDate+"'  and Servicestockissue.IssueDate <='"+SEnDate+"' "+
				" and Servicestockissue.MillCode=0"+
				" Inner Join Unit On Servicestockissue.Unit_Code = Unit.Unit_Code"+
				" Inner Join Cata On Cata.Group_Code = Servicestockissue.Group_Code "+
				/*" union all "+
				"  select  to_char(IndentMaterial_InternalService.ENTRYDATETIME,'yyyymmdd') as docdate, "+
				"  to_char(IndentMaterial_InternalService.Req_Qty) as docqty,'0' as  docno,'0' as Stockvalue, "+
				"  unit.unit_name, deptname  as deptname ,'0' as DocNo2,'1' as doctype,'' as Group_Name "+
				"  from IndentMaterial_InternalService "+
				"  Inner join inventory.indent on indent.IndentNo=IndentMaterial_InternalService.IndentNo "+
				"  and indent.Code = IndentMaterial_InternalService.ItemCode	  "+
				"  inner join inventory.unit on unit.unit_code= indent.unit_code "+
				"  inner join hrdnew.department on department.deptcode =  indent.deptcode "+
				"  where  IndentMaterial_InternalService.itemcode =  '"+SCode+"' "+
				"  and to_char(IndentMaterial_InternalService.ENTRYDATETIME,'yyyymmdd') >= '"+SStDate+"' "+
				"  and to_char(IndentMaterial_InternalService.ENTRYDATETIME,'yyyymmdd') <=  '"+SEnDate+"' "+
				"  and indent.millcode=0 "+

				"  union all "+
				
				"  select  to_char(ItemStock.ENTRYDATETIME,'yyyymmdd') as docdate "+
				"  , to_char(ItemStock.Stock) as docqty "+
				"  ,'0' 	as  docno "+
				"  ,'0' 	as Stockvalue "+
				"  , '' 	as unit_name "+
				"  , '' 	as deptname "+
				"  ,'0' 	as DocNo2 "+
				"  ,'1' 	as doctype "+
				"  ,'RDC' as Group_Name   "+
				"  from ItemStock   "+
				"  where  ItemStock.itemcode =  '"+SCode+"'  "+
				"  and to_char(ItemStock.ENTRYDATETIME,'yyyymmdd') >=  '"+SStDate+"'     "+
				"  and to_char(ItemStock.ENTRYDATETIME,'yyyymmdd') <=  '"+SEnDate+"'   "+
				"  and ItemStock.millcode=0 "+
				
		 " select  to_char(rdc_itemstock.entrydate,'yyyymmdd') as docdate "+
		 " , to_char(rdc_itemstock.Receipt_Qty) as docqty "+
		 " ,gateinward.InvNo 	as  docno "+
		 " ,'0' 	as Stockvalue "+
		 " , '' 	as unit_name "+
		 " , dept_name "+
		 " ,'0' 	as DocNo2 "+
		 " ,'1' 	as doctype "+
		 " ,'RDC' as Group_Name   "+
		 " from rdc_itemstock   "+
		 " inner join gateinward on gateinward.gino=rdc_itemstock.gino "+
		 " inner join rdc on rdc.rdcno = rdc_itemstock.rdcno "+
		 " and rdc.HSNMATCODE=rdc_itemstock.itemcode "+
		 " inner join dept on dept.dept_code = rdc.dept_code "+
		 " where  rdc_itemstock.itemcode =  '"+SCode+"'  "+
		 " and to_char(rdc_itemstock.entrydate,'yyyymmdd') >= '"+SStDate+"'       "+
		 " and to_char(rdc_itemstock.entrydate,'yyyymmdd') <=  '"+SEnDate+"'   "+
		 " and rdc.millcode=0 "+ */
				

				" ) "+
				" order by 1,8,3 "; 
				
				
				
				//System.out.println("Service QS -->"+QS);


						
  
				ORAConnection  connect        = ORAConnection.getORAConnection();
				Connection     theConnection  = connect.getConnection();
				
               /*Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection1 = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");*/
				
				Statement      state           = theConnection.createStatement();
                ResultSet resultset = state.executeQuery(QS);
			
               while(resultset.next())
				{
					 HashMap theMap = new HashMap();
					 
					 theMap.put("DOCDATE",common.parseNull(resultset.getString(1)));
					 theMap.put("DOCQTY",common.parseNull(resultset.getString(2)));
					 theMap.put("DOCNO",common.parseNull(resultset.getString(3)));
					 theMap.put("DOCVALUE",common.parseNull(resultset.getString(4)));
					 theMap.put("UNIT_NAME",common.parseNull(resultset.getString(5)));
					 theMap.put("DEPTNAME",common.parseNull(resultset.getString(6)));
					 theMap.put("DOCNO2",common.parseNull(resultset.getString(7)));
					 theMap.put("DOCTYPE",common.parseNull(resultset.getString(8)));
					 theMap.put("GROUPNAME",common.parseNull(resultset.getString(9)));
					 
					 
					 AServiceStockList.add(theMap);
					 
				}
			 resultset.close();
			 state.close();
		}
			catch(Exception ex){
			ex.printStackTrace();
			}
			
}


private void setServiceOpening()
{
	try
	{
		dOpeningQty=0;
		dOpeningValue=0;
		String SCode=TMatCode.getText() ;
	
				
				 
	String QS = "SELECT SUM(DOCqTY),SUM(dOCvALUE) FROM ( "+
				" select  "+
				" servicestocktransfer.Stock as docqty,servicestocktransfer.Stockvalue as docvalue "+
				" from servicestocktransfer "+
				" where  servicestocktransfer.itemcode = '"+SCode+"' "+
				" and to_char(servicestocktransfer.transferstatusdate,'yyyymmdd') <'"+SStDate+"' "+
				" and servicestocktransfer.MillCode="+iMillCode+" "+
				" union all"+
				" select "+
				" to_char(Servicestockissue.qty*-1) as docqty,to_char(Servicestockissue.issuevalue) as docvalue "+
				" from Servicestockissue"+
				" where  Servicestockissue.Code = '"+SCode+"' "+
				" and Servicestockissue.IssueDate <'"+SStDate+"'  "+
				" and Servicestockissue.MillCode="+iMillCode+" "+
				" ) ";
				 
				
				
				ORAConnection  connect        = ORAConnection.getORAConnection();
				Connection     theConnection  = connect.getConnection();
				Statement      state           = theConnection.createStatement();
                ResultSet resultset = state.executeQuery(QS);
			
               while(resultset.next())
				{
					dOpeningQty = common.toDouble(resultset.getString(1));
					dOpeningValue = common.toDouble(resultset.getString(2)); 
				}
				
							 
			 resultset.close();
			 state.close();
			}
			
			catch(Exception ex)
			{
			ex.printStackTrace();
			}
}	 


private void setServiceRateVector()
{
	dServiceStockRate =0;
	String SCode=TMatCode.getText() ;
	
	try
	{
		/*String QS =  	" select itemcode as code,unitrate as rate,netvalue as value,stock as nonstock"+ 
						" from"+
						" ("+
						" Select t1.ItemCode , t1.Item_Name, t1.Stock,"+ 
						" round((GRN.invamount / GRN.invqty), 2) as unitrate,"+ 
						" round((t1.stock * ((GRN.invamount/GRN.invqty))), 2) as netvalue"+
						" from"+  
						" ( "+
						" Select t.ItemCode, t.Item_Name, t.Stock, nvl(Max(Grn.Id), 0) as MaxGRNId from"+ 
						" ("+ 
						" Select ServiceStocktransfer.itemcode, invitems.item_name, Sum(ServiceStocktransfer.stock) as Stock"+
						" from ServiceStocktransfer"+  
						" Inner join invitems on invitems.item_code =  ServiceStocktransfer.itemcode"+  
						" where itemcode='"+SCode+"' and ServiceStocktransfer.TRANSFERSTATUS= 1 and ServiceStocktransfer.millcode=0"+  
						" and to_char(ServiceStocktransfer.TransferStatusdate,'yyyymmdd') <= '"+SEnDate+"'"+
						" group  by ServiceStocktransfer.itemcode, invitems.item_name"+
						" )t "+ 
						" left Join GRN on GRN.Code = t.ItemCode and grn.rejflag=0"+ 
						" Group by t.ItemCode, t.Item_Name, t.Stock"+
						" Order by 1"+
						" )t1"+ 
						" left Join GRN on GRN.Id = t1.MaxGrnId"+ 
						" Order by 1"+ 
						" )";*/

				String QS =  	" Select t1.ItemCode , t1.Item_Name,  "+
								"  round((GRN.invamount / GRN.invqty), 2) as unitrate "+
								"  from  "+
								"  ( "+
								"  Select t.ItemCode, t.Item_Name,  nvl(Max(Grn.Id), 0) as MaxGRNId from "+
								"  ( "+
								"  Select invitems.item_code as itemcode, invitems.item_name"+
								"  from invitems  "+
								"  where item_code='"+SCode+"' "+
								"  )t  "+
								"  left Join GRN on GRN.Code = t.ItemCode and grn.rejflag=0 "+
								"  Group by t.ItemCode, t.Item_Name"+
								"  Order by 1"+
								"  )t1 "+
								 " left Join GRN on GRN.Id = t1.MaxGrnId 						";
					 
			    ORAConnection  connect        = ORAConnection.getORAConnection();
				Connection     theConnection  = connect.getConnection();
				Statement      state           = theConnection.createStatement();
                ResultSet res = state.executeQuery(QS);
			
               while(res.next())
				{
					
					//dServiceStockRate  =common.toDouble(res.getString(2));
					dServiceStockRate  =common.toDouble(res.getString(3));
					
				}
				
			 res.close();
			 state.close();
	}
			
			catch(Exception ex)
			{
			ex.printStackTrace();
			}
	
}

private void setServiceStockTableDetails()
{

		 double dReceiptQty =0; 
		 double dStock =0;
		 
		 		 double dPrevStk=0;

          
           theModel1.setNumRows(0);
		   
		    ArrayList theList1;
		   theList1 = new ArrayList();
		   
		   System.out.println("comming non stock open qty"+dOpeningQty);
		   
		   //dOpeningQty=5;
		   
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("Opening");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   //theList1 . add(common.getRound(dOpeningQty,1));
		   
		   theList1 . add(String.valueOf(dOpeningQty));
		   
		   //theList1 . add(common.getRound(dOpeningValue,2));
		   //theList1 . add(common.getRound(dOpeningValue/dOpeningQty,3));

		   theList1 . add(common.getRound(dRate*dOpeningQty,2));
		   theList1 . add(common.getRound((dRate*dOpeningQty)/dOpeningQty,3));
		   
		   
		   theModel1 . appendRow(new Vector(theList1));
		   theList1 = null;
		   
		   dPrevStk = dOpeningQty;
		   
		   System.out.println("dPrev Stock Start"+dPrevStk);
		   
          for(int i=0;i<AServiceStockList.size();i++)
          {
               HashMap theMap = (HashMap)AServiceStockList.get(i);
			    
				double dStockQty = common.toDouble((String)theMap.get("DOCQTY"));
				int iType =common.toInt((String)theMap.get("DOCTYPE"));
				
				
				
               
			    ArrayList theList;
               
			   if(iType==1)
			   {
				   
				   
				      dPrevStk = dPrevStk+common.toDouble((String)theMap.get("DOCQTY"));
					  
					  System.out.println("dPrev Stock receipt"+dPrevStk);
				   
				   
				  
				   theList                     = new ArrayList();
				   theList                     . clear();
				   theList                     . add(common.parseDate((String)theMap.get("DOCDATE")));
				   theList                     . add((String)theMap.get("DOCNO2"));
				   theList                     . add((String)theMap.get("DOCNO"));
				   theList                     . add("Receipt");
				   theList                     . add((String)theMap.get("UNIT_NAME"));
				   theList                     . add("");
				   theList                     . add((String)theMap.get("DEPT_NAME"));
				   theList                     . add((String)theMap.get("GROUPNAME"));
				   theList                     . add((String)theMap.get("DOCQTY"));
				   theList                     . add(common.getRound(dStockQty*dRate,2));
				   theList                     . add("");
				   theList                     . add("");
				   theList                     . add(common.getRound(dPrevStk,3));
				   theList                     . add(common.getRound(dPrevStk*dRate,2));
				   theList                     . add(common.getRound(dRate,3));
				   theModel1                    . appendRow(new Vector(theList));
				   theList                     = null;
			   }
			   if(iType==2)
			   {


		   				   if(dPrevStk> common.toDouble((String)theMap.get("DOCQTY")) ) {
					   
					   dPrevStk = dPrevStk-common.toDouble((String)theMap.get("DOCQTY"));
				   }else {
					   dPrevStk = common.toDouble((String)theMap.get("DOCQTY"))-dPrevStk;
				   }
					  System.out.println("dPrev Stock issue"+dPrevStk);

				   
               theList                     = new ArrayList();
               theList                     . clear();
               theList                     . add(common.parseDate((String)theMap.get("DOCDATE")));
               theList                     . add((String)theMap.get("DOCNO2"));
               theList                     . add((String)theMap.get("DOCNO"));
               theList                     . add("Issue");
               theList                     . add((String)theMap.get("UNIT_NAME"));
               theList                     . add("");
               theList                     . add((String)theMap.get("DEPT_NAME"));
               theList                      . add((String)theMap.get("GROUPNAME"));
			   theList                      . add("");
			   theList                      . add("");
			   theList                      . add((String)theMap.get("DOCQTY"));
			   theList                      . add(common.getRound(dPrevStk*dRate,2));
			   theList                      . add(common.getRound(dPrevStk,3));
			   theList                      . add(common.getRound(dPrevStk*dRate,2));
			   theList                      . add(common.getRound(dRate,3));
               theModel1                     . appendRow(new Vector(theList));
			   
			   
               theList                         = null;
			  
			   }
               
               
         }
	
	
		}	


/*private void setServiceStockTableDetails()
{
        
         
		 double dReceiptQty =0; 
		 double dStock =0;
          
           theModel1.setNumRows(0);
		   
		    ArrayList theList1;
		   theList1 = new ArrayList();
		   
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("Opening");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   theList1 . add("");
		   
		   theList1 . add(common.getRound(dOpeningQty,3));
		   theList1 . add(common.getRound(dRate*dOpeningQty,2));
		   theList1 . add(common.getRound((dRate*dOpeningQty)/dOpeningQty,3));
		   
		   System.out.println("Service Open Qty"+dOpeningQty);
		   
		   theModel1 . appendRow(new Vector(theList1));
		   theList1 = null;
		   
          for(int i=0;i<AServiceStockList.size();i++)
          {
               HashMap theMap = (HashMap)AServiceStockList.get(i);
			    
				double dStockQty = common.toDouble((String)theMap.get("DOCQTY"));
				int iType =common.toInt((String)theMap.get("DOCTYPE"));
				
               
			    ArrayList theList;
               
			   if(iType==1)
			   {
				    //dReceiptQty = dStock+common.toDouble((String)theMap.get("DOCQTY"));
					
					dReceiptQty += common.toDouble((String)theMap.get("DOCQTY"));
					
					
				   theList                     = new ArrayList();
				   theList                     . clear();
				   theList                     . add(common.parseDate((String)theMap.get("DOCDATE")));
				   theList                     . add((String)theMap.get("DOCNO2"));
				   theList                     . add((String)theMap.get("DOCNO"));
				   theList                     . add("Receipt");
				   theList                     . add((String)theMap.get("UNIT_NAME"));
				   theList                     . add("");
				   theList                     . add((String)theMap.get("DEPT_NAME"));
				   theList                     . add((String)theMap.get("GROUPNAME"));
				   theList                     . add((String)theMap.get("DOCQTY"));
				   theList                     . add(common.getRound(dStockQty*dServiceStockRate,2));
				   theList                     . add("");
				   theList                     . add("");
				   theList                     . add(common.getRound(dReceiptQty,3));
				   theList                     . add(common.getRound(dReceiptQty*dServiceStockRate,2));
				   theList                     . add(common.getRound(dServiceStockRate,3));
				   theModel1                    . appendRow(new Vector(theList));
				   theList                     = null;
			   }
			   if(iType==2)
			   {
				     dStock =  (dOpeningQty+dReceiptQty) - common.toDouble((String)theMap.get("DOCQTY"));
					 
					 System.out.println("Service Fianl Qty"+dStock);
					 
					 //dStock =  dReceiptQty) - common.toDouble((String)theMap.get("DOCQTY"));
					 
				   
				   
				   
               theList                     = new ArrayList();
               theList                     . clear();
               theList                     . add(common.parseDate((String)theMap.get("DOCDATE")));
               theList                     . add((String)theMap.get("DOCNO2"));
               theList                     . add((String)theMap.get("DOCNO"));
               theList                     . add("Issue");
               theList                     . add((String)theMap.get("UNIT_NAME"));
               theList                     . add("");
               theList                     . add((String)theMap.get("DEPT_NAME"));
               theList                      . add((String)theMap.get("GROUPNAME"));
			   theList                      . add("");
			   theList                      . add("");
			   theList                      . add((String)theMap.get("DOCQTY"));
			   theList                      . add(common.getRound(dStockQty*dServiceStockRate,2));
			   theList                      . add(common.getRound(dStock,3));
			   theList                      . add(common.getRound(dStock*dServiceStockRate,2));
			   theList                      . add(common.getRound(dServiceStockRate,3));
               theModel1                     . appendRow(new Vector(theList));
			   
			   //dReceiptQty = dStock;
			   
               theList                         = null;
			  
			   }
               
               
         }
		  //dReceiptQty=0;
} */
}
