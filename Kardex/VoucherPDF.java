/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Kardex;

import java.io.*;
import java.sql.*;
import java.util.*;
import util.*;
import guiutil.*;
import javax.swing.JOptionPane;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javax.swing.JTable;
import java.net.InetAddress;

 public class VoucherPDF{
     String SFile="",SAdvanceMonth;

     String     sAuthSysName="";


     Common common = new Common();
    // String 	SAmount="0";
     int            Lctr      = 100;
     int            Pctr      = 0;
     String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
     int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font tinyBold   = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
     private static Font tinyNormal = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

     private static Font underBold  = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
     

       String SHead1[] = {"","","","","",""};
       int    iWidth1[]= {30,20,20,20,20,30};

     Document document;
     PdfPTable table;
     Connection theConnection = null;
     
     String         sFromDate, sToDate,sUnit,SVocNo,SVochName,SAcType,SPurpose,SServerDate,STransactionID,SHrdTicketNo,SAmount;
     int 	    iContractorEmpCode;	
    // double	    dAmount;	

 //public VoucherPDF(String SVocNo,String SVochName,String SAcType,double dAmount,String SPurpose,String SServerDate,String STransactionID)
 public VoucherPDF()
 {
   
    try
    {
          sAuthSysName=InetAddress.getLocalHost().getHostName();
    }
    catch(Exception e)
    {
          e.printStackTrace();
    }
    createPDFFile();
      
}
 public void setValues(String SVocNo,String SVochName,String SAcType,String SAmount,String SPurpose,String SServerDate,String SHrdTicketNo,String STransactionID) //
 {
    this . SVocNo    = SVocNo;
    this . SVochName = SVochName;
    this . SAcType   = SAcType;
    this . SAmount   = SAmount;
    this . SPurpose  = SPurpose;
    this . SServerDate    = SServerDate;
    this . STransactionID = STransactionID;   
    this . SHrdTicketNo   = SHrdTicketNo;
    try
    {
      addBody(document,table,SVocNo,SVochName,SAcType,SAmount,SPurpose,SServerDate,STransactionID); //
    }
    catch(Exception ex)
    {
        System.out.println(" setValues "+ex);
    }
 }
  public void createPDFFile()
   {
          try
          {
            
            SFile = "d://StoreMaterialPDF.pdf"; 

            document = new Document(PageSize.A4);
            PdfWriter.getInstance(document, new FileOutputStream(SFile));
            document.open();
            
            table = new PdfPTable(6);
            table.setWidths(iWidth1);
            table.setWidthPercentage(100);
            
          }
          catch (Exception e)
          {
               e.printStackTrace();
          }
   }
   public void  closePdf()
   {
       document.close();
   }
   public void addBody(Document document,PdfPTable table,String SVocNo,String sVochName,String  sACType,String SAmount,String SPurpose,String SServerDate,String sTransactionID) throws BadElementException 
   {
       System.out.println("Body");
       try
       {

       table = new PdfPTable(6);       
       table.flushContent();
       table.setWidths(iWidth1);
       table.setWidthPercentage(100);
       table.setSpacingAfter(50);

        PdfPCell c1;

      //  SAmount		= String.valueOf(dAmount);
	
        AddCellTable("AMARJOTHI SPINNING MILLS LIMITED", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 6, 1,25f, 4, 1, 8, 2, bigBold);
        AddCellTable("PUDUSURIPALAYAM - NAMBIYUR- 638 458", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 6, 1,25f, 4, 1, 8, 2, bigBold);
        AddCellTable("TRANSACTION ID : ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable(sTransactionID, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable("CASH PAYMENT VOUCHER ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable("DATE :", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1,25f, 4, 1, 8, 2, bigNormal);

        String sDate =common.parseDate(SServerDate);
        AddCellTable(String.valueOf(sDate), table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable("ACCOUND HEAD :", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable(String.valueOf(sACType), table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 4, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable("NAME OF THE PERSON", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable(String.valueOf(sVochName), table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 4, 1,25f, 4, 1, 8, 2, bigNormal);//+"("+SHrdTicketNo+")"
        AddCellTable("PURPOSE", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable(String.valueOf(SPurpose), table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 4, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable("AMOUNT IN WORDS :", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable(common.getRupee(SAmount), table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 4, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable("COST CENTER NO.", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 3, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable("CODE NO.", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable("AMOUNT RS", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 3, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2, 1,25f, 4, 1, 8, 2, bigNormal);
        AddCellTable(SAmount, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 3, 1,25f, 4, 1, 8, 2, bigNormal);
        
        AddCellTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1,35f, 4, 1, 8, 2, bigNormal);
        AddCellTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1,35f, 4, 1, 8, 2, bigNormal);
        AddCellTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1,35f, 4, 1, 8, 2, bigNormal);
        AddCellTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1,35f, 4, 1, 8, 2, bigNormal);
        AddCellTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1,35f, 4, 1, 8, 2, bigNormal);
        AddCellTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1,35f, 4, 1, 8, 2, bigNormal);

        AddCellTable("DEPT HEAD", table, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 1, 1, 25f,4, 1, 8, 2, bigNormal);
        AddCellTable("CM/JMD", table, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 1, 1, 25f,4, 1, 8, 2, bigNormal);
        AddCellTable("J.O(AC)", table, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 1, 1, 25f,4, 1, 8, 2, bigNormal);
        AddCellTable("IA", table, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 1, 1, 25f,4, 1, 8, 2,bigNormal);
        AddCellTable("CASHIER", table, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 1, 1, 25f,4, 1, 8, 2, bigNormal);
        AddCellTable("RECEIVEDBY", table, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 1, 1, 25f,4, 1, 8, 2, bigNormal);

        document.add(table);


       }
       catch(Exception ex)
       {
           
       }
   }
   /*
     * ------------------------ Function for Adding Cell into PDF Table
     * ------------------
     */

    private void AddCellTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, com.itextpdf.text.Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    private void AddCellTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iRowSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, com.itextpdf.text.Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    private void AddCellTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        //c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    private void AddCellTable(double dValue, int iRound, int iRad, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder) {
        Double DValue = new Double(dValue);
        String Str;
        if (DValue.isNaN() || dValue == 0) {
            Str = "";
        } else {
            Str = (common.Rad(common.getRound(dValue, iRound), iRad));
        }

        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);

    }

    private void AddCellTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, com.itextpdf.text.Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    private void AddCellTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, com.itextpdf.text.Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }
}
