Select Issue.IssueDate as DocDate,Issue.UIRefNo as DocNo,' ' as Block,  
Issue.Qty as DocQty,Issue.IssueValue as DocValue,' ' as Supplier,
Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'2' as DocType,'1' as GrnType, 
Issue.IssueNo as DocNo2 
From ((Issue Inner Join Dept On Issue.Dept_Code = Dept.Dept_Code) 
Inner Join Cata On Cata.Group_Code = Issue.Group_Code) 
Inner Join Unit On Issue.Unit_Code = Unit.Unit_Code 
Where Issue.Code = 'A10000103' 
and Issue.MillCode=0
Union All 
Select Grn.GrnDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, 
Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,Supplier.Name as Supplier, 
Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, 
0 as DocNo2 
From ((((Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock) 
Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code) 
Inner Join Cata On Cata.Group_Code = Grn.Group_Code) 
Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code) 
Inner Join Supplier On Grn.Sup_Code = Supplier.Ac_Code 
Where Grn.RejFlag=0 and Grn.Code = 'A10000103' 
and Grn.MillCode=0
Union All 
Select Grn.RejDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, 
Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,Supplier.Name as Supplier, 
Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, 
0 as DocNo2 
From ((((Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock) 
Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code) 
Inner Join Cata On Cata.Group_Code = Grn.Group_Code) 
Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code) 
Inner Join Supplier On Grn.Sup_Code = Supplier.Ac_Code 
Where Grn.RejFlag=1 and Grn.Code = 'A10000103' 
and Grn.MillCode=0
Order By 1,10
