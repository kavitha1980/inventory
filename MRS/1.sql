create table mo1 as (SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, MRS.Remarks, MRS.Make, InvItems.Catl, InvItems.Draw, MRS.Qty, MRS.Dept_Code, Dept.Dept_Name,MRS.DueDate,Nature.Nature,OrdBlock.BlockName,Cata.Group_Name,RawUser.UserName,'' as PendingStatus
 FROM ((((MRS INNER JOIN CATA on MRS.GROUP_CODE=CATA.GROUP_CODE) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) 
 Inner Join InvItems on Mrs.Item_Code=InvItems.Item_Code) 
 Left Join Nature on MRS.NatureCode=Nature.NatureCode) 
 Left Join OrdBlock on MRS.BlockCode=OrdBlock.Block 
 Inner Join RawUser on decode(Mrs.MrsAuthUserCode,null,1,0,1,Mrs.MrsAuthUserCode) = RawUser.UserCode 
 WHERE (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MRS.Qty>0 And MRS.OrderNo=0 and MRS.MrsDate>='20150401' and MRS.MrsDate <='20150408'
 and Mrs.MillCode = 0
 union all
 SELECT Mrs_TEmp.MrsDate, Mrs_TEmp.MrsNo, Mrs_TEmp.Item_Code, Mrs_TEmp.Remarks, Mrs_TEmp.Make, InvItems.Catl, InvItems.Draw, Mrs_TEmp.Qty, Mrs_TEmp.Dept_Code, Dept.Dept_Name,Mrs_TEmp.DueDate,Nature.Nature,OrdBlock.BlockName,Cata.Group_Name,RawUser.UserName,
decode(Mrs_Temp.ReadyForApproval,1,'Jmd Pending','Store Pending') as PendingStatus
 FROM ((((MRS_Temp INNER JOIN CATA on Mrs_TEmp.GROUP_CODE=CATA.GROUP_CODE) INNER JOIN Dept ON Mrs_TEmp.Dept_Code=Dept.Dept_code) 
 Inner Join InvItems on Mrs_TEmp.Item_Code=InvItems.Item_Code) 
 Left Join Nature on Mrs_TEmp.NatureCode=Nature.NatureCode) 
 Left Join OrdBlock on Mrs_TEmp.BlockCode=OrdBlock.Block 
 Inner Join RawUser on decode(Mrs_TEmp.AuthUserCode,null,1,0,1,Mrs_TEmp.AuthUserCode) = RawUser.UserCode 
 WHERE (Mrs_TEmp.MrsFlag=0 or Mrs_TEmp.DeptMrsAuth=1) and Mrs_TEmp.Qty>0 and Mrs_TEmp.MrsDate>='20150401' and Mrs_TEmp.MrsDate <='20150408'
 and Mrs_TEmp.MillCode = 0 and Mrs_Temp.isdelete=0 and Mrs_Temp.itemdelete=0
and Mrs_Temp.RejectionStatus=0 and Mrs_Temp.StoresRejectionStatus=0 and Mrs_Temp.ApprovalStatus=0)
