 create table Temp1MRSOrder as (
 Select PYOrder.OrderDate as OrderDate,PYOrder.Item_Code,
 Supplier.Name,PYOrder.Rate,PYOrder.DiscPer,
 PYOrder.CenVatPer,PYOrder.TaxPer,PYOrder.SurPer,
 PYOrder.Net/PYOrder.Qty as NetRate
 From PYOrder Inner Join Supplier On Supplier.Ac_Code = PyOrder.Sup_Code
 Inner join TempMRSOrder11 on TempMRSOrder11.OrderDate= PYOrder.Orderdate
 and TempMRSOrder11.Item_Code=PYOrder.Item_Code
 Where PYOrder.Qty > 0 And PYOrder.Net > 0
 Union All
 Select PurchaseOrder.OrderDate as OrderDate,PurchaseOrder.Item_Code,
 Supplier.Name,PurchaseOrder.Rate,PurchaseOrder.DiscPer,
 PurchaseOrder.CenVatPer,PurchaseOrder.TaxPer,PurchaseOrder.SurPer, 
 PurchaseOrder.Net/PurchaseOrder.Qty as NetRate
 From PurchaseOrder Inner Join Supplier On Supplier.Ac_Code = PurchaseOrder.Sup_Code
 Inner join TempMRSOrder11 on TempMRSOrder11.OrderDate= PurchaseOrder.Orderdate
 and TempMRSOrder11.Item_Code=PurchaseOrder.Item_Code
 Where PurchaseOrder.Qty > 0 And PurchaseOrder.Net > 0)
