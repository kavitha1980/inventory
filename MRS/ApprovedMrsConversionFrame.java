package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class ApprovedMrsConversionFrame extends JInternalFrame
{
     JButton        BApply;
     JPanel         TopPanel,BottomPanel;
     JPanel         First,Second,Third,Fourth,Fifth;
     JTextField     TOrder;
     JLayeredPane   DeskTop;
     JRadioButton   JRPeriod,JRNo;

     StatusPanel    SPanel;
     TabReport      tabreport;
     DateField     TStDate,TEnDate;
     NextField      TStNo,TEnNo;
     Common         common;

     Object         RowData[][];

     String         ColumnData[] = {"Date","Mrs No","Code","Name","Qty","Unit","Department","Group","Status","Due Date"};
     String         ColumnType[] = {"S","N","S","S","N","S","S","S","S","S"};

     int            iMillCode,iAuthCode,iUserCode;

     String SItemTable;


     Vector   VDate,VMrsNo,VRCode,VRName,VQty  ,
              VDept ,VGroup,VStatus,VDue,VUnit ,
              VId   ,VHodName,VMrsOrdNo,VMrsFlag,VMrsTypeCode,VBudgetStatus;


     public ApprovedMrsConversionFrame(JLayeredPane Layer,int iMillCode,int iAuthCode,int iUserCode)
     {
          super("MRSs Placed During a Period");
     
          this.DeskTop        = Layer;
          this.iMillCode      = iMillCode;
          this.iAuthCode      = iAuthCode;
          this.iUserCode      = iUserCode;

          common         = new Common();
     
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TStDate     = new DateField();
          TEnDate     = new DateField();
          BApply      = new JButton("Apply");
          TopPanel    = new JPanel();
          First       = new JPanel();
          Second      = new JPanel();
          Third       = new JPanel();
          Fourth      = new JPanel();
          Fifth       = new JPanel();
          BottomPanel = new JPanel();
          TOrder      = new JTextField("1,2,3");
          TStNo       = new NextField(0);
          TEnNo       = new NextField(0);
     
          JRPeriod    = new JRadioButton("Periodical",true);
          JRNo        = new JRadioButton("MRS No",false);
     
          TStDate.setTodayDate();
          TEnDate.setTodayDate();
     }

     public void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(1,5));
          First     .setLayout(new GridLayout(1,1));
          Second    .setLayout(new GridLayout(1,1));
          Third     .setLayout(new GridLayout(2,1));
          Fourth    .setLayout(new GridLayout(2,1));
          Fifth     .setLayout(new GridLayout(1,1));
     
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          
          First.setBorder(new TitledBorder("Sorted On (Column No)"));
          First.add(TOrder);
          
          
          Third.setBorder(new TitledBorder("Basis"));
          Third.add(JRPeriod);
          Third.add(JRNo);
          
          Fourth.setBorder(new TitledBorder("Periodical"));
          Fourth.add(TStDate);
          Fourth.add(TEnDate);
          
          Fifth.add(BApply);
          
          TopPanel.add(First);
          TopPanel.add(Second);
          TopPanel.add(Third);
          TopPanel.add(Fourth);
          TopPanel.add(Fifth);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          JRPeriod.addActionListener(new JRList());
          JRNo.addActionListener(new JRList()); 
          BApply.addActionListener(new ApplyList());
     }

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    Fourth.setBorder(new TitledBorder("Periodical"));
                    Fourth.removeAll();
                    Fourth.add(TStDate);
                    Fourth.add(TEnDate);
                    Fourth.updateUI();
                    JRNo.setSelected(false);
               }
               else
               {
                    Fourth.setBorder(new TitledBorder("Numbered"));
                    Fourth.removeAll();
                    Fourth.add(TStNo);
                    Fourth.add(TEnNo);
                    Fourth.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    setDataIntoVector();
                    setRowData();
               try
               {
                  getContentPane().remove(tabreport);
               }
               catch(Exception ex)
               {
               }
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.setBorder(new TitledBorder("List (Insert For Modification)"));
               getContentPane().add(tabreport,BorderLayout.CENTER);

               for(int j=0; j<tabreport.ReportTable.getRowCount(); j++)
               {
                    if(((String)tabreport.ReportTable.getValueAt(j,2)).equals("-1"))
                    {
                         tabreport.setBGColor(0,j,j);
                    }
               }
               if(iAuthCode>1)
               {
                    tabreport.ReportTable.addKeyListener(new KeyList());
               }
                    setSelected(true);
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
          }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {



          } 
     }

     public void setDataIntoVector()
     {
          ResultSet result = null;

          VDate         = new Vector();
          VMrsNo        = new Vector();
          VRCode        = new Vector();
          VRName        = new Vector();
          VQty          = new Vector();
          VDept         = new Vector();
          VGroup        = new Vector();
          VStatus       = new Vector();
          VDue          = new Vector();
          VUnit         = new Vector();
          VId           = new Vector();
          VHodName      = new Vector();
          VMrsOrdNo     = new Vector();
          VMrsFlag      = new Vector();
          VMrsTypeCode  = new Vector();
          VBudgetStatus = new Vector();

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
                              result        =  stat.executeQuery(getQString());
               
               while (result.next())
               {
                    VMrsNo       .addElement((String)result.getString(2));
                    VDate        .addElement(common.parseDate((String)result.getString(1)));
                    VRCode       .addElement((String)result.getString(3));
                    VRName       .addElement((String)result.getString(4));
                    VQty         .addElement((String)result.getString(5));
                    VDept        .addElement((String)result.getString(6));
                    VGroup       .addElement((String)result.getString(7));
                    VDue         .addElement(common.parseDate((String)result.getString(8)));
                    VUnit        .addElement((String)result.getString(9));
                    VId          .addElement((String)result.getString(10));
                    VStatus      .addElement(" ");
                    VHodName     .addElement((String)result.getString(11));
                    VMrsOrdNo    .addElement((String)result.getString(12));
                    VMrsFlag     .addElement((String)result.getString(13));
                    VMrsTypeCode .addElement((String)result.getString(14));
                    VBudgetStatus.addElement((String)result.getString(15));
               }
               result.close();
          }
          catch(Exception ex)
          {
               System.out.println("setDataIntoVector: "+ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VDate.size()][ColumnData.length];
          for(int i=0;i<VDate.size();i++)

          {

               RowData[i][0] = (String)VDate.elementAt(i);
               RowData[i][1] = (String)VMrsNo.elementAt(i);
               RowData[i][2] = (String)VRCode.elementAt(i);
               RowData[i][3] = (String)VRName.elementAt(i);
               RowData[i][4] = (String)VQty.elementAt(i);
               RowData[i][5] = (String)VUnit.elementAt(i);
               RowData[i][6] = (String)VDept.elementAt(i);
               RowData[i][7] = common.parseNull((String)VGroup.elementAt(i));
               RowData[i][8] = (String)VStatus.elementAt(i);
               RowData[i][9] = common.parseNull((String)VDue.elementAt(i));
          }  
     }

     public String getQString()
     {
          DateField df = new DateField();
          df.setTodayDate();
          String SToday = df.toNormal();
          String QString = "";
          
          if(JRPeriod.isSelected())
          {
               QString = "SELECT MRS_Temp.MrsDate, MRS_TEMP.MrsNo, MRS_TEMP.Item_Code, Mrs_Temp.Item_Name , MRS_TEMP.Qty, Dept.Dept_Name , Cata.Group_Name , MRS_TEMP.DueDate, Unit.Unit_Name,MRS_TEMP.Id,Hod.hodname,MRS_TEMP.orderno,MRS_TEMP.MrsFlag,MRS_TEMP.MrsTypeCode,MRS_TEMP.BudgetStatus "+
                         "FROM (((MRS_Temp Left JOIN InvItems ON MRS_TEMP.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON MRS_TEMP.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON MRS_TEMP.Group_Code=Cata.Group_Code) INNER JOIN Unit ON MRS_TEMP.Unit_Code=Unit.Unit_Code "+
                         "LEFT JOIN Hod ON MRS_TEMP.HODCode=HOD.HodCode Where (MRS_TEMP.MrsFlag=0 or MRS_TEMP.DeptMrsAuth=1) and MRS_TEMP.Qty >= 0 And MRS_TEMP.MrsDate >= '"+TStDate.toNormal()+"' and MRS_TEMP.MrsDate <='"+TEnDate.toNormal()+"' and MRS_TEMP.MillCode="+iMillCode+" and ApprovalStatus=1 and isConverted=0 ";
          }
          else
          {
               QString = "SELECT MRS_TEMP.MrsDate, MRS_TEMP.MrsNo, MRS_TEMP.Item_Code, Mrs_Temp.Item_Name , MRS_TEMP.Qty, Dept.Dept_Name , Cata.Group_Name , MRS_TEMP.DueDate, Unit.Unit_Name,MRS_TEMP.Id,Hod.hodname,MRS_TEMP.orderno,MRS_TEMP.MrsFlag,MRS_TEMP.MrsTypeCode,MRS_TEMP.BudgetStatus "+
                         "FROM (((MRS_Temp Left JOIN InvItems ON MRS_TEMP.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON MRS_TEMP.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON MRS_TEMP.Group_Code=Cata.Group_Code) INNER JOIN Unit ON MRS_TEMP.Unit_Code=Unit.Unit_Code "+
                         "LEFT JOIN Hod ON MRS_TEMP.HODCode=HOD.HodCode Where (MRS_TEMP.MrsFlag=0 or MRS_TEMP.DeptMrsAuth=1) and MRS_TEMP.Qty >= 0 And MRS_TEMP.MrsNo >="+TStNo.getText()+" and MRS_TEMP.MrsNo <="+TEnNo.getText()+" and MRS_TEMP.MillCode="+iMillCode+" and ApprovalStatus=1  and isConverted=0 ";              
          }

          QString = QString+" Order By "+TOrder.getText()+",MRS_TEMP.Id";

          return QString;
     }

     private String getHodName(String SMrsNo)
     {
          int iIndex=-1;

          iIndex = VMrsNo.indexOf(SMrsNo);
          if(iIndex!=-1)
               return common.parseNull((String)VHodName.elementAt(iIndex));
          else
               return "";
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
}
