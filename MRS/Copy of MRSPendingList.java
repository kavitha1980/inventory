package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MRSPendingList extends JInternalFrame
{
     JPanel         TopPanel;
     JPanel         BottomPanel;
     JPanel         First,Second,Third,Fourth,Fifth;
     
     String         SStDate,SEnDate;
     JComboBox      JCFilter;
     JButton        BApply,BPrint,BPrint1A,BPrint2E;
     JTextField     TFile;
     
     DateField      TStDate,TEnDate;
     JRadioButton   JRAsOn,JRPeriod;
     
     String         SInt      = "  ";
     String         Strline   = "";
     int            iLen      = 0;
     String         SDepName,SODepName;
     
     JComboBox      JCUnit;
     Vector         VUnitCode,VUnit;
     
     Object         RowData[][];
     String         ColumnData[] = {"Date","MRS No","Code","Name","Quantity","Catl","Draw","Make","Dept","Due Date","Delay Days","User Name","Remarks"};
     String         ColumnType[] = {"S","N","S","S","N","S","S","S","S","S","N","S","S"};
     
     JLayeredPane   DeskTop;
     Vector         VCode,VName;
     StatusPanel    SPanel;
     
     Common common = new Common();
     
     Vector         VMrsDate,VMrsNo,VMrsCode,VMrsName,VRemarks,VMake,VCatl,VDraw,VMrsQty,VReOrd,VStock,VPending,VToOrder,VDeptCode,VDeptName,VDueDate,VNature,VBlock,VUoM,VGroup,VUserName;
     Vector         VDueDays,VFreq,VOrdBlock,VOrdDate,VOrdNo,VSupName;
     Vector         VOrdRate,VOrdDisc,VOrdCenVat,VOrdSalTax,VSurChg;

     TabReport1     tabreport;
     int            iMillCode;
     String         SItemTable,SSupTable,SMillName;

     public MRSPendingList(JLayeredPane DeskTop,int iMillCode,int iAuthCode,int iUserCode,String SItemTable,String SSupTable,String SMillName)
     {
          super("MRSs Pending As On Date");
          this.DeskTop    = DeskTop;
          this.VCode      = VCode;
          this.VName      = VName;
          this.SPanel     = SPanel;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;
     
          setUnitCombo(); 
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          First          = new JPanel();
          Second         = new JPanel();
          Third          = new JPanel();
          Fourth         = new JPanel();
          Fifth          = new JPanel();
     
          TStDate        = new DateField();
          TEnDate        = new DateField();
          JCUnit         = new JComboBox(VUnit);
          JCFilter       = new JComboBox();
          BApply         = new JButton("Apply");
          BPrint         = new JButton("Print");
          BPrint1A       = new JButton("Print1A");
          BPrint2E       = new JButton("Print2E");
          TFile          = new JTextField(15);

          JRAsOn         = new JRadioButton("As On",true);
          JRPeriod       = new JRadioButton("Periodical",false);

     
          TStDate.setTodayDate();
          TEnDate.setTodayDate();
     }

     public void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(1,5));
          First     .setLayout(new GridLayout(2,1));
          Second    .setLayout(new GridLayout(2,1));
          Third     .setLayout(new GridLayout(1,1));
          Fourth    .setLayout(new GridLayout(1,1));
          Fifth     .setLayout(new GridLayout(1,1));

     
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,700,575);
     }

     public void addComponents()
     {
          JCFilter.addItem("All");
          JCFilter.addItem("Regular MRS");
          JCFilter.addItem("Planning MRS");
          JCFilter.addItem("Yearly MRS");

          First.setBorder(new TitledBorder("Basis"));
          First.add(JRAsOn);
          First.add(JRPeriod);
          
          Second.setBorder(new TitledBorder("As On"));
          Second.add(TEnDate);
          Second.add(new JLabel(""));

          Third.setBorder(new TitledBorder("Unit"));
          Third.add(JCUnit);

          Fourth.setBorder(new TitledBorder("Filter"));
          Fourth.add(JCFilter);

          Fifth.add(BApply);

          TopPanel.add(First);
          TopPanel.add(Second);
          TopPanel.add(Third);
          TopPanel.add(Fourth);
          TopPanel.add(Fifth);

          BottomPanel    . add(BPrint);
          BottomPanel    . add(BPrint1A);
          BottomPanel    . add(BPrint2E);
          BottomPanel    . add(TFile);
     
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          JRPeriod  .addActionListener(new JRList());
          JRAsOn    .addActionListener(new JRList()); 

          BApply    . addActionListener(new ApplyList());

          BPrint    . addActionListener(new PrintList());
          BPrint1A  . addActionListener(new PrintList1A());
          BPrint2E  . addActionListener(new Print2EList());
     }

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    Second.setBorder(new TitledBorder("Periodical"));
                    Second.removeAll();
                    Second.add(TStDate);
                    Second.add(TEnDate);
                    Second.updateUI();
                    JRAsOn.setSelected(false);
               }
               else
               {
                    Second.setBorder(new TitledBorder("As On"));
                    Second.removeAll();
                    Second.add(TEnDate);
                    Second.add(new JLabel(""));
                    Second.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }

     public class Print2EList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               new Print2e(TEnDate,TFile,iMillCode,SItemTable,SSupTable,SMillName);
               removeHelpFrame();
          }            
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               String STitle="";

               if(JRPeriod.isSelected())
               {
                    STitle    = "MRSs Pending List From "+TStDate.toString()+"  To "+TEnDate.toString()+" \n";
               }
               else
               {
                    STitle    = "MRSs Pending List As On "+TEnDate.toString()+" \n";
               }

               Vector    VHead     = getPendingHead();
                         iLen      = ((String)VHead.elementAt(0)).length();
                         Strline   = common.Replicate("-",iLen)+"\n";
               Vector    VBody     = getPendingBody();
               String    SFile     = TFile.getText();
               new       DocPrint(VBody,VHead,STitle,SFile,iMillCode,SMillName);
               removeHelpFrame();
          }
     }

     public class PrintList1A implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {

               String STitle = "";

               if(JRPeriod.isSelected())
               {
                    STitle = "MRSs Pending List with Last PO Details From "+TStDate.toString()+" To "+TEnDate.toString()+" \n";
               }
               else
               {
                    STitle = "MRSs Pending List with Last PO Details As On "+TEnDate.toString()+" \n";
               }

               Vector    VHead     = getPendingHead1A();
                         iLen      = ((String)VHead.elementAt(0)).length();
                         Strline   = common.Replicate("-",iLen)+"\n";
               Vector    VBody     = getPendingBody1A();
               String    SFile     = TFile.getText();
               new       DocPrint(VBody,VHead,STitle,SFile,iMillCode,SMillName);
               removeHelpFrame();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public Vector getPendingHead()
     {
          Vector vect = new Vector();
          
          String Head1[] = {"MRS Date","MRS No","Mat Code","Mat Name","UoM","Special","Item Make","Catalogue","Drawing No","Requested","ReOrder","Stock","Pending @","To be","Freq","Due Date","Nature","Stocking","Group","Delay"};
          String Head2[] = {"Information","Qty","Qty","Qty","Order Qty","Ordered Qty","Days","Type","Days"};
          
          String Sha1    =    ((String)Head1[0]).trim();
          String Sha2    =    ((String)Head1[1]).trim();
          String Sha3    =    ((String)Head1[2]).trim();
          String Sha4    =    ((String)Head1[3]).trim();
          String Sha5    =    ((String)Head1[4]).trim();
          String Sha6    =    ((String)Head1[5]).trim();
          String Sha7    =    common.parseNull((String)Head1[6]);
          String Sha8    =    common.parseNull((String)Head1[7]);
          String Sha9    =    ((String)Head1[8]).trim();
          String Sha10   =    ((String)Head1[9]).trim();
          String Sha11   =    ((String)Head1[10]).trim();
          String Sha12   =    ((String)Head1[11]).trim();
          String Sha13   =    ((String)Head1[12]).trim();
          String Sha14   =    ((String)Head1[13]).trim();
          String Sha15   =    ((String)Head1[14]).trim();
          String Sha16   =    ((String)Head1[15]).trim();
          String Sha17   =    ((String)Head1[16]).trim();
          String Sha18   =    ((String)Head1[17]).trim();
          String Sha19   =    ((String)Head1[18]).trim();
          String Sha20   =    ((String)Head1[19]).trim();
          
          String Shb1    =    "";
          String Shb2    =    "";
          String Shb3    =    "";
          String Shb4    =    "";
          String Shb5    =    "";
          String Shb6    =    ((String)Head2[0]).trim();
          String Shb7    =    "";
          String Shb8    =    "";
          String Shb9    =    "";
          String Shb10   =    ((String)Head2[1]).trim();
          String Shb11   =    ((String)Head2[2]).trim();
          String Shb12   =    ((String)Head2[3]).trim();
          String Shb13   =    ((String)Head2[4]).trim();
          String Shb14   =    ((String)Head2[5]).trim();
          String Shb15   =    ((String)Head2[6]).trim();
          String Shb16   =    "";
          String Shb17   =    "";
          String Shb18   =    ((String)Head2[7]).trim();
          String Shb19   =    "";
          String Shb20   =    ((String)Head2[8]).trim();
          
          Sha1  = common.Pad(Sha1,10);
          Sha2  = common.Rad(Sha2,9)+SInt;
          Sha3  = common.Pad(Sha3,9)+SInt;
          Sha4  = common.Pad(Sha4,40)+SInt;
          Sha5  = common.Pad(Sha5,7)+SInt;
          Sha6  = common.Pad(Sha6,25)+SInt;
          Sha7  = common.Pad(Sha7,10)+SInt;
          Sha8  = common.Pad(Sha8,10)+SInt;
          Sha9  = common.Pad(Sha9,15)+SInt;
          Sha10 = common.Rad(Sha10,10);
          Sha11 = common.Rad(Sha11,10);
          Sha12 = common.Rad(Sha12,10)+SInt;
          Sha13 = common.Rad(Sha13,10)+SInt;
          Sha14 = common.Rad(Sha14,12);
          Sha15 = common.Rad(Sha15,6)+SInt;
          Sha16 = common.Pad(Sha16,10)+SInt;
          Sha17 = common.Pad(Sha17,12);
          Sha18 = common.Pad(Sha18,5)+SInt;
          Sha19 = common.Pad(Sha19,5);
          Sha20 = common.Rad(Sha20,15);
          
          Shb1  = common.Pad(Shb1,10);
          Shb2  = common.Rad(Shb2,9)+SInt;
          Shb3  = common.Pad(Shb3,9)+SInt;
          Shb4  = common.Pad(Shb4,40)+SInt;
          Shb5  = common.Pad(Shb5,7)+SInt;
          Shb6  = common.Pad(Shb6,25)+SInt;
          Shb7  = common.Pad(Shb7,10)+SInt;
          Shb8  = common.Pad(Shb8,10)+SInt;
          Shb9  = common.Pad(Shb9,15)+SInt;
          Shb10 = common.Rad(Shb10,10);
          Shb11 = common.Rad(Shb11,10);
          Shb12 = common.Rad(Shb12,10)+SInt;
          Shb13 = common.Rad(Shb13,10)+SInt;
          Shb14 = common.Rad(Shb14,12);
          Shb15 = common.Rad(Shb15,6)+SInt;
          Shb16 = common.Pad(Shb16,10)+SInt;
          Shb17 = common.Pad(Shb17,12);
          Shb18 = common.Pad(Shb18,5)+SInt;
          Shb19 = common.Pad(Shb19,5);
          Shb20 = common.Rad(Shb20,15);
          
          String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+Sha16+Sha17+Sha18+Sha19+Sha20+"\n";
          String Strh2 = Shb1+Shb2+Shb3+Shb4+Shb5+Shb6+Shb7+Shb8+Shb9+Shb10+Shb11+Shb12+Shb13+Shb14+Shb15+Shb16+Shb17+Shb18+Shb19+Shb20+"\n";
          vect.add(Strh1);
          vect.add(Strh2);
          return vect;
     }

     public Vector getPendingBody()
     {
          Vector vect = new Vector();
          for(int i=0;i<VMrsDate.size();i++)
          {
               if(i>0)
               {
                    SDepName  = ((String)VDeptName.elementAt(i)).trim();
                    SODepName = ((String)VDeptName.elementAt(i-1)).trim();
               }
               
               String Sda1  = (String)VMrsDate    . elementAt(i);
               String Sda2  = (String)VMrsNo      . elementAt(i);
               String Sda3  = (String)VMrsCode    . elementAt(i);
               String Sda4  = (String)VMrsName    . elementAt(i);
               String Sda5  = (String)VUoM        . elementAt(i);
               String Sda6  = (String)VRemarks    . elementAt(i);
               String Sda7  = (String)VMake       . elementAt(i);
               String Sda8  = (String)VCatl       . elementAt(i);
               String Sda9  = (String)VDraw       . elementAt(i);
               String Sda10 = (String)VMrsQty     . elementAt(i);
               String Sda11 = (String)VReOrd      . elementAt(i);
               String Sda12 = (String)VStock      . elementAt(i);
               String Sda13 = (String)VPending    . elementAt(i);
               String Sda14 = (String)VToOrder    . elementAt(i);
               String Sda15 = (String)VFreq       . elementAt(i);
               String Sda16 = (String)VDueDate    . elementAt(i);
               String Sda17 = (String)VNature     . elementAt(i);
               String Sda18 = (String)VBlock      . elementAt(i);
               String Sda19 = (String)VGroup      . elementAt(i);
               String Sda20 = (String)VDueDays    . elementAt(i);
               
               Sda1  = common.Pad(Sda1,10);
               Sda2  = common.Rad(Sda2,9)+SInt;
               Sda3  = common.Pad(Sda3,9)+SInt;
               Sda4  = common.Pad(Sda4,40)+SInt;
               Sda5  = common.Pad(Sda5,7)+SInt;
               Sda6  = common.Pad(Sda6,25)+SInt;
               Sda7  = common.Pad(Sda7,10)+SInt;
               Sda8  = common.Pad(Sda8,10)+SInt;
               Sda9  = common.Pad(Sda9,15)+SInt;
               Sda10 = common.Rad(Sda10,10);
               Sda11 = common.Rad(Sda11,10);
               Sda12 = common.Rad(Sda12,10)+SInt;
               Sda13 = common.Rad(Sda13,10)+SInt;
               Sda14 = common.Rad(Sda14,12);
               Sda15 = common.Rad(Sda15,6)+SInt;
               Sda16 = common.Pad(Sda16,10)+SInt;
               Sda17 = common.Pad(Sda17,12);
               Sda18 = common.Pad(Sda18,5)+SInt;
               Sda19 = common.Pad(Sda19,15);
               Sda20 = common.Rad(Sda20,5);
               
               String Sdb1 = (String)VDeptName.elementAt(i);
               Sdb1 = common.Pad(Sdb1,25);
               String Strd1 = common.Rad("Dept Name : ",25)+" "+Sdb1+"\n";
               String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+Sda16+Sda17+Sda18+Sda19+Sda20+"\n";
               if(i==0)
               {
                    vect.add(Strd1);
                    vect.add(Strd);
               }
               else
               {
                    if(SDepName.equals(SODepName))
                         vect.add(Strd);
                    else
                    {
                         vect.add(Strline);
                         vect.add(Strd1);
                         vect.add(Strd);
                    }
               }
          }
          return vect;
     }

     public Vector getPendingHead1A()
     {
          Vector vect = new Vector();
          
          String Head1[] = {"MRS Date","MRS No","Mat Code","Mat Name","UoM","Special",    "Item Make","Catalogue","Drawing No","Requested","Freq","Due Date","Stock","Delay","OrderNo","Order","Order","Supplier","Rate","Disc","CenVa","St","Sur"};
          String Head2[] = {                                                "Information"                                     , "Qty"     ,"Days",           "Type" ,"Days" ,          "Block","Date" ,"Name"    ,"Rs"  ,"%"   ,"%"   ,"%"   ,"%"};
          
          String Sha1    = ((String)Head1[0]).trim();
          String Sha2    = ((String)Head1[1]).trim();
          String Sha3    = ((String)Head1[2]).trim();
          String Sha4    = ((String)Head1[3]).trim();
          String Sha5    = ((String)Head1[4]).trim();
          String Sha6    = ((String)Head1[5]).trim();
          String Sha7    = common.parseNull((String)Head1[6]);
          String Sha8    = common.parseNull((String)Head1[7]);
          String Sha9    = ((String)Head1[8]).trim();
          String Sha10   = ((String)Head1[9]).trim();
          String Sha11   = ((String)Head1[10]).trim();
          String Sha12   = ((String)Head1[11]).trim();
          String Sha13   = ((String)Head1[12]).trim();
          String Sha14   = ((String)Head1[13]).trim();
          String Sha15   = ((String)Head1[14]).trim();
          String Sha16   = ((String)Head1[15]).trim();
          String Sha17   = ((String)Head1[16]).trim();
          String Sha18   = ((String)Head1[17]).trim();
          String Sha19   = ((String)Head1[18]).trim();
          String Sha20   = ((String)Head1[19]).trim();
          String Sha21   = ((String)Head1[20]).trim();
          String Sha22   = ((String)Head1[21]).trim();
          String Sha23   = ((String)Head1[22]).trim();
          
          String Shb1    = "";
          String Shb2    = "";
          String Shb3    = "";
          String Shb4    = "";
          String Shb5    = "";
          String Shb6    = ((String)Head2[0])     .trim();
          String Shb7    = "";
          String Shb8    = "";
          String Shb9    = "";
          String Shb10   = ((String)Head2[1])     .trim();
          String Shb11   = ((String)Head2[2])     .trim();
          String Shb12   = "";
          String Shb13   = ((String)Head2[3])     .trim();
          String Shb14   = ((String)Head2[4])     .trim();
          String Shb15   = "";
          String Shb16   = ((String)Head2[5])     .trim();
          String Shb17   = ((String)Head2[6])     .trim();
          String Shb18   = ((String)Head2[7])     .trim();
          String Shb19   = ((String)Head2[8])     .trim();
          String Shb20   = ((String)Head2[9])     .trim();
          String Shb21   = ((String)Head2[10])    .trim();
          String Shb22   = ((String)Head2[11])    .trim();
          String Shb23   = ((String)Head2[12])    .trim();

          Sha1  = common.Cad(Sha1,10)+SInt;
          Sha2  = common.Cad(Sha2,9)+SInt;
          Sha3  = common.Cad(Sha3,9)+SInt;
          Sha4  = common.Cad(Sha4,40)+SInt;
          Sha5  = common.Cad(Sha5,7)+SInt;
          Sha6  = common.Cad(Sha6,25)+SInt;
          Sha7  = common.Cad(Sha7,10)+SInt;
          Sha8  = common.Cad(Sha8,10)+SInt;
          Sha9  = common.Cad(Sha9,15)+SInt;
          Sha10 = common.Cad(Sha10,10)+SInt;
          Sha11 = common.Cad(Sha11,5)+SInt;
          Sha12 = common.Cad(Sha12,10)+SInt;
          Sha13 = common.Cad(Sha13,5)+SInt;
          Sha14 = common.Cad(Sha14,5)+SInt;
          Sha15 = common.Cad(Sha15,12)+SInt;
          Sha16 = common.Cad(Sha16,5)+SInt;
          Sha17 = common.Cad(Sha17,10)+SInt;
          Sha18 = common.Cad(Sha18,15)+SInt;
          Sha19 = common.Cad(Sha19,13);
          Sha20 = common.Cad(Sha20,7);
          Sha21 = common.Cad(Sha21,7);
          Sha22 = common.Cad(Sha22,7);
          Sha23 = common.Cad(Sha23,7);

          Shb1  = common.Cad(Shb1,10)+SInt;
          Shb2  = common.Cad(Shb2,9)+SInt;
          Shb3  = common.Cad(Shb3,9)+SInt;
          Shb4  = common.Cad(Shb4,40)+SInt;
          Shb5  = common.Cad(Shb5,7)+SInt;
          Shb6  = common.Cad(Shb6,25)+SInt;
          Shb7  = common.Cad(Shb7,10)+SInt;
          Shb8  = common.Cad(Shb8,10)+SInt;
          Shb9  = common.Cad(Shb9,15)+SInt;
          Shb10 = common.Cad(Shb10,10)+SInt;
          Shb11 = common.Cad(Shb11,5)+SInt;
          Shb12 = common.Cad(Shb12,10)+SInt;
          Shb13 = common.Cad(Shb13,5)+SInt;
          Shb14 = common.Cad(Shb14,5)+SInt;
          Shb15 = common.Cad(Shb15,12)+SInt;
          Shb16 = common.Cad(Shb16,5)+SInt;
          Shb17 = common.Cad(Shb17,10)+SInt;
          Shb18 = common.Cad(Shb18,15)+SInt;
          Shb19 = common.Cad(Shb19,13);
          Shb20 = common.Cad(Shb20,7);
          Shb21 = common.Cad(Shb21,7);
          Shb22 = common.Cad(Shb22,7);
          Shb23 = common.Cad(Shb23,7);

          String    Strh1     = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+Sha16+Sha17+Sha18+Sha19+Sha20+Sha21+Sha22+Sha23+"\n";
          String    Strh2     = Shb1+Shb2+Shb3+Shb4+Shb5+Shb6+Shb7+Shb8+Shb9+Shb10+Shb11+Shb12+Shb13+Shb14+Shb15+Shb16+Shb17+Shb18+Shb19+Shb20+Shb21+Shb22+Shb23+"\n";
                    vect      . add(Strh1);
                    vect      . add(Strh2);
          return vect;
     }

     public Vector getPendingBody1A()
     {
          Vector vect = new Vector();
          for(int i=0;i<VMrsDate.size();i++)
          {
               if(i>0)
               {
                    SDepName  = ((String)VDeptName.elementAt(i)).trim();
                    SODepName = ((String)VDeptName.elementAt(i-1)).trim();
               }
               
               String Sda1  = (String)VMrsDate    . elementAt(i);
               String Sda2  = (String)VMrsNo      . elementAt(i);
               String Sda3  = (String)VMrsCode    . elementAt(i);
               String Sda4  = (String)VMrsName    . elementAt(i);
               String Sda5  = (String)VUoM        . elementAt(i);
               String Sda6  = (String)VRemarks    . elementAt(i);
               String Sda7  = (String)VMake       . elementAt(i);
               String Sda8  = (String)VCatl       . elementAt(i);
               String Sda9  = (String)VDraw       . elementAt(i);
               String Sda10 = (String)VMrsQty     . elementAt(i);
               String Sda11 = (String)VFreq       . elementAt(i);
               String Sda12 = (String)VDueDate    . elementAt(i);
               String Sda13 = (String)VBlock      . elementAt(i);
               String Sda14 = (String)VDueDays    . elementAt(i);
               String Sda15 = (String)VOrdNo      . elementAt(i);
               String Sda16 = (String)VOrdBlock   . elementAt(i);
               String Sda17 = common.parseDate(common.parseNull((String)VOrdDate    . elementAt(i)));
               String Sda18 = (String)VSupName    . elementAt(i);
               String Sda19 = (String)VOrdRate    . elementAt(i);
               String Sda20 = (String)VOrdDisc    . elementAt(i);
               String Sda21 = (String)VOrdCenVat  . elementAt(i);
               String Sda22 = (String)VOrdSalTax  . elementAt(i);
               String Sda23 = (String)VSurChg     . elementAt(i);

               Sda1  = common.Pad(Sda1,10);
               Sda2  = common.Rad(Sda2,9)+SInt;
               Sda3  = common.Pad(Sda3,9)+SInt;
               Sda4  = common.Pad(Sda4,40)+SInt;
               Sda5  = common.Pad(Sda5,7)+SInt;
               Sda6  = common.Pad(Sda6,25)+SInt;
               Sda7  = common.Pad(Sda7,10)+SInt;
               Sda8  = common.Pad(Sda8,10)+SInt;
               Sda9  = common.Pad(Sda9,15)+SInt;
               Sda10 = common.Rad(Sda10,10)+SInt;
               Sda11 = common.Rad(Sda11,5)+SInt;
               Sda12 = common.Rad(Sda12,10)+SInt;
               Sda13 = common.Rad(Sda13,5)+SInt;
               Sda14 = common.Rad(Sda14,5)+SInt;
               Sda15 = common.Pad(Sda15,12)+SInt;
               Sda16 = common.Pad(Sda16,5)+SInt;
               Sda17 = common.Pad(Sda17,10)+SInt;
               Sda18 = common.Pad(Sda18,15)+SInt;
               Sda19 = common.Rad(common.getRound(Sda19,4),13);
               Sda20 = common.Rad(Sda20,7);
               Sda21 = common.Rad(Sda21,7);
               Sda22 = common.Rad(Sda22,7);
               Sda23 = common.Rad(Sda23,7);
               
               String    Sdb1      = (String)VDeptName.elementAt(i);
                         Sdb1      = common.Pad(Sdb1,25);
               String    Strd1     = common.Rad("Dept Name : ",25)+" "+Sdb1+"\n";
               String    Strd      = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+Sda16+Sda17+Sda18+Sda19+Sda20+Sda21+Sda22+Sda23+"\n";
               if(i==0)
               {
                    vect . add(Strd1);
                    vect . add(Strd);
               }
               else
               {
                    if(SDepName.equals(SODepName))
                         vect.add(Strd);
                    else
                    {
                         vect.add(Strline);
                         vect.add(Strd1);
                         vect.add(Strd);
                    }
               }
          }
          return vect;
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               
               try
               {
                    tabreport           = new TabReport1(RowData,ColumnData,ColumnType);
                    getContentPane()    . add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    DeskTop             . repaint();
                    DeskTop             . updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public void setDataIntoVector()
     {
          VMrsDate       = new Vector();
          VMrsNo         = new Vector();
          VMrsCode       = new Vector();
          VMrsName       = new Vector();
          VUoM           = new Vector();
          VRemarks       = new Vector();
          VMake          = new Vector();
          VCatl          = new Vector();
          VDraw          = new Vector();
          VMrsQty        = new Vector();
          VReOrd         = new Vector();
          VStock         = new Vector();
          VPending       = new Vector();
          VToOrder       = new Vector();
          VDeptCode      = new Vector();
          VDeptName      = new Vector();
          VDueDate       = new Vector();
          VNature        = new Vector();
          VBlock         = new Vector();
          VDueDays       = new Vector();
          VFreq          = new Vector();
          VGroup         = new Vector();
          VOrdBlock      = new Vector();
          VOrdDate       = new Vector();
          VOrdNo         = new Vector();
          VSupName       = new Vector();
          VOrdRate       = new Vector();
          VOrdDisc       = new Vector();
          VOrdCenVat     = new Vector();
          VOrdSalTax     = new Vector();
          VSurChg        = new Vector();
          VUserName      = new Vector();

          
          SStDate = TStDate.toNormal();
          SEnDate = TEnDate.toNormal();

          String QS1 = "";

          
          if(JRPeriod.isSelected())
          {
               QS1 =   " create table m1 as SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, MRS.Remarks, MRS.Make, InvItems.Catl, InvItems.Draw, MRS.Qty, MRS.Dept_Code, Dept.Dept_Name,MRS.DueDate,Nature.Nature,OrdBlock.BlockName,Cata.Group_Name,RawUser.UserName"+
                       " FROM ((((MRS INNER JOIN CATA on MRS.GROUP_CODE=CATA.GROUP_CODE) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) "+
                       " Inner Join InvItems on Mrs.Item_Code=InvItems.Item_Code) "+
                       " Left Join Nature on MRS.NatureCode=Nature.NatureCode) "+
                       " Left Join OrdBlock on MRS.BlockCode=OrdBlock.Block "+
                       " Inner Join RawUser on decode(Mrs.MrsAuthUserCode,null,1,0,1,Mrs.MrsAuthUserCode) = RawUser.UserCode "+
                       " WHERE (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MRS.Qty>0 And MRS.OrderNo=0 and MRS.MrsDate>='"+SStDate+"' and MRS.MrsDate <= '"+SEnDate+"'"+
                       " and Mrs.MillCode = "+iMillCode;
          }
          else
          {
               QS1 =   " create table m1 as SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, MRS.Remarks, MRS.Make, InvItems.Catl, InvItems.Draw, MRS.Qty, MRS.Dept_Code, Dept.Dept_Name,MRS.DueDate,Nature.Nature,OrdBlock.BlockName,Cata.Group_Name,RawUser.UserName"+
                       " FROM ((((MRS INNER JOIN CATA on MRS.GROUP_CODE=CATA.GROUP_CODE) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) "+
                       " Inner Join InvItems on Mrs.Item_Code=InvItems.Item_Code) "+
                       " Left Join Nature on MRS.NatureCode=Nature.NatureCode) "+
                       " Left Join OrdBlock on MRS.BlockCode=OrdBlock.Block "+
                       " Inner Join RawUser on decode(Mrs.MrsAuthUserCode,null,1,0,1,Mrs.MrsAuthUserCode) = RawUser.UserCode "+
                       " WHERE (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MRS.Qty>0 And MRS.OrderNo=0 and MRS.MrsDate <= '"+SEnDate+"'"+
                       " and Mrs.MillCode = "+iMillCode;
          }

          
          if(!(JCUnit.getSelectedItem()).equals("ALL"))
               QS1 = QS1 + " and mrs.Unit_code="+VUnitCode.elementAt(JCUnit.getSelectedIndex());
          
          if(JCFilter.getSelectedIndex()==1)
               QS1 = QS1 + " and Mrs.PlanMonth = 0 and YearlyPlanningStatus=0 ";
          if(JCFilter.getSelectedIndex()==2)
               QS1 = QS1 + " and Mrs.PlanMonth > 0 and YearlyPlanningStatus=0 ";
          if(JCFilter.getSelectedIndex()==3)
               QS1 = QS1 + " and Mrs.PlanMonth = 0 and YearlyPlanningStatus=1 ";
          

          String QS2 =   " create table m2 as  SELECT m1.Item_Code"+
                         " FROM m1 GROUP BY m1.Item_Code ";
          
          String QS3 = " ";
          
          if(iMillCode==0)
          {               
               QS3  =    " create table m3 as SELECT m2.Item_Code, InvItems.OPGQTY, (ROQ+MinQty) AS ReOrd, InvItems.ITEM_NAME,UOM.UoMName"+
                         " FROM m2 LEFT JOIN InvItems ON m2.Item_Code = InvItems.ITEM_CODE LEFT JOIN UOM ON UOM.UoMCode = InvItems.UOMCODE ";
          }
          else
          {
               QS3  =    " create table m3 as SELECT m2.Item_Code, "+SItemTable+".OPGQTY, ("+SItemTable+".ROQ+"+SItemTable+".MinQty) AS ReOrd, InvItems.ITEM_NAME,UOM.UoMName"+
                         " FROM (m2 LEFT JOIN InvItems ON m2.Item_Code = InvItems.ITEM_CODE  LEFT JOIN UOM ON UOM.UoMCode = InvItems.UOMCODE ) "+
                         " Left Join "+SItemTable+" On InvItems.Item_Code = "+SItemTable+".Item_Code ";
          }
          
          String QS4 = " create table m4 as SELECT m2.Item_Code, Sum((Qty-InvQty)) AS Pending"+
                       " FROM m2 LEFT JOIN PurchaseOrder ON m2.Item_Code=PurchaseOrder.Item_Code "+
                       " Where PurchaseOrder.MillCode = "+iMillCode+
                       " GROUP BY m2.Item_Code ";

          String QS5 =   " create table m5 as SELECT m2.Item_Code, Sum(GRN.GrnQty) AS GRNQty"+
                         " FROM m2 LEFT JOIN GRN ON m2.Item_Code = GRN.Code  WHERE Grn.GrnBlock<2 "+
                         " and Grn.MillCode = "+iMillCode+
                         " GROUP BY m2.Item_Code ";

          String QS6 =   " create table m6 as SELECT m2.Item_Code, Sum(Issue.Qty) AS IssueQty"+
                         " FROM m2 LEFT JOIN Issue ON m2.Item_Code = Issue.Code "+
                         " Where Issue.MillCode = "+iMillCode+
                         " GROUP BY m2.Item_Code";

          String QS7 =   " create table m7 as (select Item_code,blockName,"+
                         " Purchaseorder.OrderNo as OrderNo,Purchaseorder.OrderDate as OrderDate,"+
                         " "+SSupTable+".name as SupName,Purchaseorder.RATE,Purchaseorder.DISCPER,"+
                         " Purchaseorder.CENVATPER, Purchaseorder.TAXPER,Purchaseorder.SURPER"+
                         " from purchaseorder"+
                         " inner join "+SSupTable+" on "+SSupTable+".ac_code = purchaseorder.sup_code"+
                         " inner join ordblock on ordblock.block   = purchaseorder.Orderblock"+
                         " where id in "+
                         " (select Ordid from (select max(id) as OrdId,ICode,OrdNo from purchaseorder"+
                         " inner join "+
                         " (select max(OrderNo) as OrdNo,item_code as ICode from"+
                         " (select purchaseOrder.Orderno,purchaseorder.item_code,max(orderdate)"+
                         " from purchaseorder"+
                         " inner join m1 on m1.item_code = purchaseorder.item_code"+
                         " group by purchaseorder.item_code,purchaseorder.Orderno)"+
                         " group by item_code) t on t.OrdNo = PurChaseOrder.orderNo and"+
                         " t.ICode = purchaseOrder.item_code"+
                         " group by Icode,OrdNo"+
                         " Order by ICode)))";


          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               try{stat.execute("Drop Table m1");}catch(Exception ex){}
               try{stat.execute("Drop Table m2");}catch(Exception ex){}
               try{stat.execute("Drop Table m3");}catch(Exception ex){}
               try{stat.execute("Drop Table m4");}catch(Exception ex){}
               try{stat.execute("Drop Table m5");}catch(Exception ex){}
               try{stat.execute("Drop Table m6");}catch(Exception ex){}
               try{stat.execute("Drop Table m7");}catch(Exception ex){}

               stat.execute(QS1);
               stat.execute(QS2);
               stat.execute(QS3);
               stat.execute(QS4);
               stat.execute(QS5);
               stat.execute(QS6);
               stat.execute(QS7);

               ResultSet res = stat.executeQuery(getQString());
               
               while (res.next())
               {
                    String SMrsDate = common.parseDate(res.getString(1));
                    String SDueDate = common.parseDate(res.getString(15));
                    String SDays    = common.getDateDiff(common.parseDate(SEnDate),SMrsDate);
                    String SFreq    = common.getDateDiff(SDueDate,SMrsDate);
                    
                    double dReq     = common.toDouble(res.getString(9));
                    double dReOrd   = common.toDouble(res.getString(10));
                    double dStock   = common.toDouble(res.getString(11));
                    double dPending = common.toDouble(res.getString(12));
                    double dToOrder = dReq-(dStock+dPending-dReOrd);
                    
                    VMrsDate       . addElement(SMrsDate);
                    VMrsNo         . addElement(res.getString(2));
                    VMrsCode       . addElement(res.getString(3));
                    VMrsName       . addElement(res.getString(4));
                    VRemarks       . addElement(res.getString(5));
                    VMake          . addElement(res.getString(6));
                    VCatl          . addElement(res.getString(7));
                    VDraw          . addElement(res.getString(8));
                    VMrsQty        . addElement(""+dReq);
                    VReOrd         . addElement(""+dReOrd);
                    VStock         . addElement(""+dStock);
                    VPending       . addElement(""+dPending);
                    VToOrder       . addElement(""+dToOrder);
                    VDeptCode      . addElement(res.getString(13));
                    VDeptName      . addElement(res.getString(14));
                    VDueDate       . addElement(SDueDate);
                    VNature        . addElement(res.getString(16));
                    VBlock         . addElement(res.getString(17));
                    VDueDays       . addElement(SDays);
                    VFreq          . addElement(SFreq);
                    VUoM           . addElement(res.getString(18));
                    VGroup         . addElement(res.getString(19));

                    VOrdBlock      . addElement(common.parseNull((String)res.getString(20)));
                    VOrdDate       . addElement(common.parseNull((String)res.getString(21)));
                    VOrdNo         . addElement(common.parseNull((String)res.getString(22)));
                    VSupName       . addElement(common.parseNull((String)res.getString(23)));

                    VOrdRate       . addElement(common.parseNull((String)res.getString(24)));
                    VOrdDisc       . addElement(common.parseNull((String)res.getString(25)));
                    VOrdCenVat     . addElement(common.parseNull((String)res.getString(26)));
                    VOrdSalTax     . addElement(common.parseNull((String)res.getString(27)));
                    VSurChg        . addElement(common.parseNull((String)res.getString(28)));
                    VUserName      . addElement(common.parseNull((String)res.getString(29)));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VMrsDate.size()][ColumnData.length];
          for(int i=0;i<VMrsDate.size();i++)
          {
               RowData[i][0]  = (String)VMrsDate  .elementAt(i);
               RowData[i][1]  = (String)VMrsNo    .elementAt(i);
               RowData[i][2]  = (String)VMrsCode  .elementAt(i);
               RowData[i][3]  = (String)VMrsName  .elementAt(i);
               RowData[i][4]  = (String)VMrsQty   .elementAt(i);
               RowData[i][5]  = (String)VCatl     .elementAt(i);
               RowData[i][6]  = (String)VDraw     .elementAt(i);
               RowData[i][7]  = (String)VMake     .elementAt(i);
               RowData[i][8]  = (String)VDeptName .elementAt(i);
               RowData[i][9]  = (String)VDueDate  .elementAt(i);
               RowData[i][10] = (String)VDueDays  .elementAt(i);
               RowData[i][11] = (String)VUserName .elementAt(i);
               RowData[i][12] = (String)VRemarks  .elementAt(i);
          }  
     }

     public String getQString()
     {
          String QString =    " SELECT m1.MrsDate,m1.MrsNo,m1.Item_Code,"+
                              " m3.ITEM_NAME,m1.Remarks,m1.Make,m1.Catl,"+
                              " m1.Draw, m1.Qty, m3.ReOrd,"+
                              " (nvl(m3.OPGQTY,0)+nvl(m5.GRNQty,0)-nvl(m6.IssueQty,0)) AS Stock,"+
                              " m4.Pending, m1.Dept_Code, m1.Dept_Name,"+
                              " m1.DueDate,m1.Nature,m1.BlockName,m3.UoMName,"+
                              " m1.group_name,m7.BlockName,m7.OrderDate,"+
                              " m7.OrderNo,m7.SupName,"+
                              " m7.RATE,m7.DISCPER,m7.CENVATPER,"+
                              " m7.TAXPER,m7.SURPER,m1.UserName "+
                              " FROM (((((m1"+
                              " INNER JOIN m3 ON m1.Item_Code = m3.Item_Code)"+
                              " LEFT JOIN m4 ON m1.Item_Code = m4.Item_Code)"+
                              " LEFT JOIN m5 ON m1.Item_Code = m5.Item_Code)"+
                              " LEFT JOIN m6 ON m1.Item_Code = m6.Item_Code)"+
                              " LEFT JOIN m7 ON m1.Item_Code = m7.Item_Code)"+
                              " ORDER BY 14,1,2,3 ";
          return QString;
     }

     private void setUnitCombo()
     {
          VUnit     = new Vector();
          VUnitCode = new Vector();
          
          VUnit     . addElement("ALL");
          VUnitCode . addElement("9999");
          
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               String QS1 = "";
               
               QS1 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";
               
               ResultSet result3 = stat.executeQuery(QS1);
               while(result3.next())
               {
                    VUnit     .addElement(result3.getString(1));
                    VUnitCode .addElement(result3.getString(2));
               }
               result3.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept,Group & Unit :"+ex);
               System.exit(0);
          }
     }
}
