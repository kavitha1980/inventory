package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class CriteriaPanel extends JPanel
{
     JPanel DeptPanel;
     JPanel GroupPanel;
     JPanel UnitPanel;
     JPanel MaterialPanel;
     JPanel FilterPanel;
     JPanel ApplyPanel;

     Common common = new Common();
     Vector VDept,VDeptCode,VGroup,VGroupCode,VUnit,VUnitCode;
     JComboBox JCDept,JCGroup,JCUnit;
     JRadioButton JRAllDept,JRSeleDept;
     JRadioButton JRAllGroup,JRSeleGroup;
     JRadioButton JRAllUnit,JRSeleUnit;
     JRadioButton JRAllMat,JRSeleMat;
     JRadioButton JRAllDoc,JRHonDoc;

     JLayeredPane Layer; 
     JButton      BMaterial,BApply;
     JTextField   TMatCode;

     JLayeredPane DeskTop;
     StatusPanel SPanel;
     Vector VCode,VName,VNameCode;
     int iUserCode;
     int iMillCode;
     String SItemTable;

     CriteriaPanel(JLayeredPane Layer,Vector VCode,Vector VName,Vector VNameCode,int iMillCode,String SItemTable)
     {
          this.Layer      = Layer;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
     
          getDeptGroupUnit();        
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          DeptPanel     = new JPanel();
          GroupPanel    = new JPanel();
          UnitPanel     = new JPanel();
          MaterialPanel = new JPanel();
          FilterPanel   = new JPanel();
          ApplyPanel    = new JPanel();
     
          JRAllDept   = new JRadioButton("All",true);
          JRSeleDept  = new JRadioButton("Selected");
          JCDept      = new JComboBox(VDept);
          JCDept.setEnabled(false);
     
          JRAllGroup  = new JRadioButton("All",true);
          JRSeleGroup = new JRadioButton("Selected");
          JCGroup     = new JComboBox(VGroup);
          JCGroup.setEnabled(false);
     
          JRAllUnit   = new JRadioButton("All",true);
          JRSeleUnit  = new JRadioButton("Selected");
          JCUnit      = new JComboBox(VUnit);
          JCUnit.setEnabled(false);
     
          JRAllMat    = new JRadioButton("All",true);
          JRSeleMat   = new JRadioButton("Selected");
          BMaterial   = new JButton("Material");
          BMaterial.setEnabled(false);
     
          JRAllDoc    = new JRadioButton("All",true);
          JRHonDoc    = new JRadioButton("Honoured");
     
          BApply      = new JButton("Apply");
          TMatCode    = new JTextField();
     }

     public void setLayouts()
     {
          setLayout(new GridLayout(1,6));
          DeptPanel    .setLayout(new GridLayout(3,1));   
          GroupPanel   .setLayout(new GridLayout(3,1));   
          UnitPanel    .setLayout(new GridLayout(3,1));
          MaterialPanel.setLayout(new GridLayout(3,1));
          FilterPanel  .setLayout(new GridLayout(2,1));   
     
          DeptPanel.setBorder(new TitledBorder("Department"));
          GroupPanel.setBorder(new TitledBorder("Catagory"));
          UnitPanel.setBorder(new TitledBorder("Processing Units"));
          MaterialPanel.setBorder(new TitledBorder("Material"));
          FilterPanel.setBorder(new TitledBorder("Documents"));
     }

     public void addComponents()
     {
          add(DeptPanel);
          add(GroupPanel);
          add(UnitPanel);
          add(MaterialPanel);
          add(FilterPanel);
          add(ApplyPanel);
     
          DeptPanel.add(JRAllDept);
          DeptPanel.add(JRSeleDept);
          DeptPanel.add(JCDept);    
     
          GroupPanel.add(JRAllGroup);
          GroupPanel.add(JRSeleGroup);
          GroupPanel.add(JCGroup);    
     
          UnitPanel.add(JRAllUnit);  
          UnitPanel.add(JRSeleUnit); 
          UnitPanel.add(JCUnit);     
     
          MaterialPanel.add(JRAllMat);  
          MaterialPanel.add(JRSeleMat); 
          MaterialPanel.add(BMaterial);
     
          FilterPanel.add(JRAllDoc); 
          FilterPanel.add(JRHonDoc); 
     
          ApplyPanel.add(BApply);
     }

     public void addListeners()
     {
          JRAllDept.addActionListener(new DeptList());
          JRSeleDept.addActionListener(new DeptList());
     
          JRAllGroup.addActionListener(new GroupList());
          JRSeleGroup.addActionListener(new GroupList());
     
          JRAllUnit.addActionListener(new UnitList());
          JRSeleUnit.addActionListener(new UnitList());
     
          JRAllMat.addActionListener(new MaterialList());
          JRSeleMat.addActionListener(new MaterialList());
     
          JRAllDoc.addActionListener(new FilterList());
          JRHonDoc.addActionListener(new FilterList());
     
          BMaterial.addActionListener(new MaterialSearch(Layer,TMatCode,BMaterial,VCode,VName,VNameCode,iMillCode,SItemTable));  
     }

     public class DeptList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllDept)
               {
                    JRAllDept.setSelected(true);
                    JRSeleDept.setSelected(false);
                    JCDept.setEnabled(false);
               }
               if(ae.getSource()==JRSeleDept)
               {   
                    JRAllDept.setSelected(false);
                    JRSeleDept.setSelected(true);
                    JCDept.setEnabled(true);
               }
          }
     }

     public class GroupList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllGroup)
               {
                    JRAllGroup.setSelected(true);
                    JRSeleGroup.setSelected(false);
                    JCGroup.setEnabled(false);
               }
               if(ae.getSource()==JRSeleGroup)
               {
                    JRAllGroup.setSelected(false);
                    JRSeleGroup.setSelected(true);
                    JCGroup.setEnabled(true);
               }
          }
     }

     public class UnitList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllUnit)
               {
                    JRAllUnit.setSelected(true);
                    JRSeleUnit.setSelected(false);
                    JCUnit.setEnabled(false);
               }
               if(ae.getSource()==JRSeleUnit)
               {
                    JRAllUnit.setSelected(false);
                    JRSeleUnit.setSelected(true);
                    JCUnit.setEnabled(true);
               }
          }
     }

     public class MaterialList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllMat)
               {
                    JRAllMat.setSelected(true);
                    JRSeleMat.setSelected(false);
                    BMaterial.setEnabled(false);
               }
               if(ae.getSource()==JRSeleMat)
               {
                    JRAllMat.setSelected(false);
                    JRSeleMat.setSelected(true);
                    BMaterial.setEnabled(true);
               }
          }
     }

     public class FilterList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllDoc)
               {
                    JRAllDoc.setSelected(true);
                    JRHonDoc.setSelected(false);
               }
               if(ae.getSource()==JRHonDoc)
               {
                    JRAllDoc.setSelected(false);
                    JRHonDoc.setSelected(true);
               }
          }
     }

     public void getDeptGroupUnit()
     {
          VDept  = new Vector();
          VGroup = new Vector();

          VDeptCode  = new Vector();
          VGroupCode = new Vector();

          VUnit      = new Vector();
          VUnitCode  = new Vector();

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               String QS1 = " ";

               QS1 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";

               ResultSet result1 = stat.executeQuery(QS1);
               while(result1.next())
               {
                    VDept.addElement(result1.getString(1));
                    VDeptCode.addElement(result1.getString(2));
               }
               result1.close();

               String QS2 = " ";

               QS2 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By Group_Name";

               ResultSet result2 = stat.executeQuery(QS2);
               while(result2.next())
               {
                    VGroup.addElement(result2.getString(1));
                    VGroupCode.addElement(result2.getString(2));
               }
               result2.close();

               String QS3 = " ";

               QS3 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";

               ResultSet result3 = stat.executeQuery(QS3);
               while(result3.next())
               {
                    VUnit.addElement(result3.getString(1));
                    VUnitCode.addElement(result3.getString(2));
               }
               result3.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept,Group & Unit :"+ex);
          }
     }
}
