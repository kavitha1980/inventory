/*
     Document List Frame
*/

package MRS;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;
import java.io.*;


import java.net.*;



public class DocumentListFrame extends JInternalFrame
{

     JLayeredPane theLayer;

     JPanel       MiddlePanel;

     JButton      DocButton1,DocButton2,DocButton3;
     String      SFile1="",SFile2="",SFile3="",SImg1="",SImg2="",SImg3="";
     int         iDocId;
     Common      common = new Common();          
     public DocumentListFrame(JLayeredPane theLayer,int iDocId)
     {
          this.theLayer = theLayer;
          this.iDocId   = iDocId;


          setImage(iDocId);
          createComponents();
          setLayouts();
          addComponents();
          addListeners();

     }
     public void createComponents()
     {
          DocButton1 = new JButton("DOC1");
          DocButton2 = new JButton("DOC2");
          DocButton3 = new JButton("DOC3");

          MiddlePanel = new JPanel();

          if(SFile1.length()>0)
          {
              DocButton1.setText(""+SFile1+"");

          }
          if(SFile2.length()>0)
          {
              DocButton2.setText(""+SFile2+"");

          }
          if(SFile3.length()>0)
          {
              DocButton3.setText(""+SFile3+"");

          }

     }
     public void setLayouts()
     {
          setTitle("Document List Frame ");
          setClosable(true);                    
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,250,250);

          MiddlePanel.setLayout(new FlowLayout());

     }
     public void addComponents()
     {
          MiddlePanel.add(DocButton1);
          MiddlePanel.add(DocButton2);
          MiddlePanel.add(DocButton3);

          getContentPane().add("Center",MiddlePanel);
     }
     public void addListeners()
     {
          DocButton1.addActionListener(new ActList());
          DocButton2.addActionListener(new ActList());
          DocButton3.addActionListener(new ActList());
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {


               if(ae.getSource()==DocButton2)
               {
			try {

                         if(!SFile2.equals("File2")) {
     
                              if(SFile2.equals(common.getPrintPath()+"picture1"+iDocId+".pdf") ) {

                                   if(SFile2.startsWith("/")) {
               
                                        String[] cmd= {"gnome-open",common.getPrintPath()+"picture1"+iDocId+".pdf"};
                         
                                        Process p   =  Runtime.getRuntime().exec(cmd);

               
                                   } else {
               
                                        Process p = Runtime.getRuntime()
                                                    .exec("rundll32 url.dll,FileProtocolHandler  "+common.getPrintPath()+"picture1"+iDocId+".pdf");
                                        p.waitFor(); 
               
                                   }
			      }
			  }	

			}catch(Exception ex) {
				ex.printStackTrace();
			}
	       }

               if(ae.getSource()==DocButton3)
               {
			try {

                         if(!SFile3.equals("File3")) {
     
                              if(SFile3.equals(common.getPrintPath()+"picture2"+iDocId+".pdf") ) {

                                   if(SFile3.startsWith("/")) {
               
                                        String[] cmd= {"gnome-open",common.getPrintPath()+"picture2"+iDocId+".pdf"};
                         
                                        Process p   =  Runtime.getRuntime().exec(cmd);
                                   } else {
               
                                        Process p = Runtime.getRuntime()
                                                    .exec("rundll32 url.dll,FileProtocolHandler  "+common.getPrintPath()+"picture2"+iDocId+".pdf");
                                        p.waitFor(); 
               
                                   }
			      }
			  }	

			}catch(Exception ex) {
				ex.printStackTrace();
			}
	       }




               if(ae.getSource()==DocButton1)
               {

                    try {




                         if(!SFile1.equals("File1")) {
     
                              if(SFile1.equals(common.getPrintPath()+"picture"+iDocId+".pdf") ) {



                                   if(SFile1.startsWith("/")) {
               
                                        String[] cmd= {"gnome-open",common.getPrintPath()+"picture"+iDocId+".pdf"};
                         
                                        Process p   =  Runtime.getRuntime().exec(cmd);

               
                                   } else {
               
                                        Process p = Runtime.getRuntime()
                                                    .exec("rundll32 url.dll,FileProtocolHandler  "+common.getPrintPath()+"picture"+iDocId+".pdf");
                                        p.waitFor(); 
               
                                   }
                              }

                              if(SFile1.equals(common.getPrintPath()+"picture1"+iDocId+".PDF")) {


                                   System.out.println("comm PDF"+common.getPrintPath()+"picture"+iDocId+".PDF");

                                   if(SFile1.startsWith("/")) {
               
                                        String[] cmd= {"gnome-open",common.getPrintPath()+"picture"+iDocId+".PDF"};
                         
                                        Process p   =  Runtime.getRuntime().exec(cmd);

               
                                   } else {
               
                                        Process p = Runtime.getRuntime()
                                                    .exec("rundll32 url.dll,FileProtocolHandler  "+common.getPrintPath()+"picture"+iDocId+".PDF");
                                        p.waitFor(); 
               
                                   }


                              }

                              if(SFile1.equals(common.getPrintPath()+"picture"+iDocId+".txt") || SFile1.equals(common.getPrintPath()+"picture1.TXT")) {
     

                                   if(SFile1.startsWith("/")) {
               
                                        String[] cmd= {"gnome-open",common.getPrintPath()+"picture"+iDocId+".txt"};
                         
                                        Process p   =  Runtime.getRuntime().exec(cmd);
               
                                   } else {
               
                                        Process p = Runtime.getRuntime()
                                                    .exec("rundll32 url.dll,FileProtocolHandler  "+common.getPrintPath()+"picture"+iDocId+".txt");
                                        p.waitFor(); 
               
                                   }
                              }
                              if(SFile1.equals(common.getPrintPath()+"picture"+iDocId+".jpg") ||  SFile1.equals(common.getPrintPath()+"picture"+iDocId+".JPG") ) {
     

                                   if(SFile1.startsWith("/")) {
               
                                        String[] cmd= {"gnome-open",common.getPrintPath()+"picture"+iDocId+".jpg"};
                         
                                        Process p   =  Runtime.getRuntime().exec(cmd);
               
                                   } else {
               
                                        Process p = Runtime.getRuntime()
                                                    .exec("rundll32 url.dll,FileProtocolHandler  "+common.getPrintPath()+"picture"+iDocId+".jpg");
                                        p.waitFor(); 
               
                                   }
                              }

                         }





                     } catch(Exception ex) {

                         ex.printStackTrace();
                     }
               }

          }
     }
     public void setImage(int iId)
     {

          String QS1 =" Select Img1,Img2,Img3,fileformat,fileformat1,fileformat2 From Document "+
                      " Where  Id ="+iId;


          System.out.println(QS1);
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");

               Statement theStatement = theConnection.createStatement();
               ResultSet theResult = theStatement.executeQuery(QS1);
               byte[] imgbytes =null;
               byte[] imgbytes1=null;
               byte[] imgbytes2=null;

               String SFor1 ="";
               String SFor2 ="";
               String SFor3 ="";


               if(theResult.next())
               {
                    imgbytes  =  getOracleBlob(theResult,"IMG1");
                    imgbytes1 =  getOracleBlob(theResult,"IMG2");
                    imgbytes2 =  getOracleBlob(theResult,"IMG3");
                    SFor1     =  common.parseNull(theResult.getString(4));
                    SFor2     =  common.parseNull(theResult.getString(5));
                    SFor3     =  common.parseNull(theResult.getString(6));
               }
               theResult.close();
               theStatement.close();

               Image   image  =null;
               Image   image1 =null;
               Image   image2 =null;

	       System.out.println("Format 1===>"+SFor1);
	       System.out.println("Format 2===>"+SFor2);
	       System.out.println("Format 3===>"+SFor3);


               Image  resizeimage =null;

               Image  resizeimage1=null;

               Image  resizeimage2=null;

               try
               {
                    if(imgbytes!=null)
                    {
                         String SFormat = SFor1;


                         image = Toolkit.getDefaultToolkit().createImage(imgbytes);

                         if(SFormat.equals("PDF") || SFormat.equals("pdf")) {

                              SFormat="pdf";

                              SFile1 = common.getPrintPath()+"picture"+iDocId+"."+SFormat;

                              File file = new File(SFile1);
                              FileOutputStream fos = new FileOutputStream(file);
                              fos.write(imgbytes);
                              fos.close();
                         }
                         else if(SFormat.equals("txt")) {

                              SFile1 = common.getPrintPath()+"picture2."+SFormat;

                               File file = new File(SFile1);
                               FileOutputStream fos = new FileOutputStream(file);
                               fos.write(imgbytes);
                               fos.close();
                         }
                         else {

                              SFile1 = common.getPrintPath()+"picture"+iDocId+"."+SFormat;

                              File file = new File(SFile1);
                              FileOutputStream fos = new FileOutputStream(file);
                              fos.write(imgbytes);
                              fos.close();
                         }
                    }
                    else
                    {
                         image = Toolkit.getDefaultToolkit().createImage("");
                         SImg1 = "null";   

                    }

                    System.out.println(SFile1);


                    if(imgbytes1!=null)
                    {
                         image1 = Toolkit.getDefaultToolkit().createImage(imgbytes1);

                         String SFormat = SFor2;


                         if(SFormat.equals("PDF") || SFormat.equals("pdf")) {

                              SFormat="pdf";

                              SFile2 = common.getPrintPath()+"picture1"+iDocId+"."+SFormat;

                               File file = new File(SFile2);
                               FileOutputStream fos = new FileOutputStream(file);
                               fos.write(imgbytes1);
                               fos.close();
                         }
                         else if(SFormat.equals("txt")) {

                              SFile2 = common.getPrintPath()+"picture"+iDocId+"."+SFormat;

                               File file = new File(SFile2);
                               FileOutputStream fos = new FileOutputStream(file);
                               fos.write(imgbytes1);
                               fos.close();
                         }
                         else {

                              SFile2 = common.getPrintPath()+"picture"+iDocId+"."+SFormat;

                              File file = new File(SFile2);
                              FileOutputStream fos = new FileOutputStream(file);
                              fos.write(imgbytes1);
                              fos.close();
                         }

                    }
                    else
                    {
                         image1 = Toolkit.getDefaultToolkit().createImage("");
                         SImg2 = "null";   

                    }

                    System.out.println(SFile2);


                    if(imgbytes2!=null)
                    {

                         image2 = Toolkit.getDefaultToolkit().createImage(imgbytes2);

                         String SFormat = SFor3;

                         if(SFormat.equals("PDF") || SFormat.equals("pdf")) {

                              SFormat="pdf";

                               SFile3 = common.getPrintPath()+"picture2"+iDocId+"."+SFormat;

                               File file 		= new File(SFile3);
                               FileOutputStream fos 	= new FileOutputStream(file);
                               fos.write(imgbytes2);
                               fos.close();

                         }
                         else if(SFormat.equals("txt")) {

                               SFile3 = common.getPrintPath()+"picture2."+SFormat;

                               File file 		= new File(SFile3);
                               FileOutputStream fos 	= new FileOutputStream(file);
                               fos.write(imgbytes2);
                               fos.close();

                         }
                         else {

                              SFile3 = common.getPrintPath()+"picture3."+SFormat;

                              File file = new File(SFile3);
                              FileOutputStream fos = new FileOutputStream(file);
                              fos.write(imgbytes2);
                              fos.close();
                         }
                    }
                    else
                    {
                         image2 = Toolkit.getDefaultToolkit().createImage("");
                         SImg3 = "null";   
                    }



/*                  ImageFilter replicate = new ReplicateScaleFilter(iSize1,iSize2);
                    ImageProducer prod    = new FilteredImageSource(image.getSource(),replicate);


                    ImageFilter replicate1 = new ReplicateScaleFilter(iSize1,iSize2);
                    ImageProducer prod1    = new FilteredImageSource(image1.getSource(),replicate1);

                    ImageFilter replicate2 = new ReplicateScaleFilter(iSize1,iSize2);
                    ImageProducer prod2    = new FilteredImageSource(image2.getSource(),replicate2);


                    resizeimage  = createImage(prod);
                    resizeimage1 = createImage(prod1);
                    resizeimage2 = createImage(prod2);*/ 

               }
               catch(Exception ex1)
               {
                    System.out.println(ex1);
                    ex1.printStackTrace();
               }


               SImg1="";
               SImg2="";
               SImg3="";

               theConnection.close();

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
      private byte[] getOracleBlob(ResultSet result, String columnName) throws SQLException
      {
    
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        byte[] bytes = null;
        
        try {    
            oracle.sql.BLOB blob = ((oracle.jdbc.OracleResultSet)result).getBLOB(columnName);
            if(blob!=null)
            {
                 inputStream = blob.getBinaryStream();        
                 int bytesRead = 0;        
                 
                 while((bytesRead = inputStream.read()) != -1) {        
                     outputStream.write(bytesRead);    
                 }
                 
                 bytes = outputStream.toByteArray();
             }
        } catch(IOException e) {
            throw new SQLException(e.getMessage());
        } finally 
        {
        }
        return bytes;
     
     }
}
