package MRS;

import javax.swing.*;
import javax.swing.table.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import java.awt.image.*;

import guiutil.*;
import util.*;
import jdbc.*;
import java.net.*;

public class EnquiryPartyFrame extends JInternalFrame
{
     JLayeredPane theLayer;
     JPanel TopPanel,MiddlePanel,BottomPanel;

     JButton    BApply,BSave,BAdd;
     JLabel     LMrsNo,LItemCode;



     JTable     theTable;

     JList      LSupplierList;
     SupplierListModel theListModel;


     Vector           VSupCode,VSupName;
     Common           common = new Common();

     Vector           VSupplier;
     Connection       theConnect=null;
     String           SMrsNo,SItemCode;
     int              iMillCode,iUserCode;

     String           SItemName,SDesc,
                      SMake , SCatl, SDraw,SQty;

     public EnquiryPartyFrame(JLayeredPane theLayer,String SMrsNo,String SItemCode,
                              int iMillCode, int    iUserCode,String SItemName,String SDesc,
                              String SMake , String SCatl,String SDraw,String SQty    )
     {
          this.theLayer = theLayer;

          this.SMrsNo   = SMrsNo;
          this.SItemCode= SItemCode;
          this.iMillCode= iMillCode;
          this.iUserCode= iUserCode;
          this.SItemName= SItemName;
          this.SDesc    = SDesc;
          this.SMake    = SMake;
          this.SCatl    = SCatl;
          this.SDraw    = SDraw;
          this.SQty     = SQty;



          setData();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setSupplierList(SItemCode,SMrsNo);


     }

     private void createComponents()
     {

          TopPanel       = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();

          LMrsNo         = new JLabel("");
          LItemCode      = new JLabel("");

          theListModel   = new SupplierListModel();
          LSupplierList  = new JList(theListModel);

          BApply         = new JButton(" Apply ");
          BSave          = new JButton(" Save  ");
          BAdd           = new JButton(" Add Supplier");


          LMrsNo.setText(SMrsNo);
          LItemCode.setText(SItemCode);

     }
     private void setLayouts()
     {
          TopPanel  .setLayout(new FlowLayout(FlowLayout.CENTER));
          MiddlePanel.setLayout(new BorderLayout());


          setTitle(" Place Enquiry");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,350,350);
          
     

     }
     private void addComponents()
     {
          TopPanel.add(new MyLabel(" MrsNo "));
          TopPanel.add(LMrsNo);

          TopPanel.add(new MyLabel(" ItemCode "));
          TopPanel.add(LItemCode);

          MiddlePanel.add("Center",LSupplierList);
          MiddlePanel.add("South" ,BAdd);

          BottomPanel.add(BSave);


          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

     }

     private void addListeners()
     {
          BAdd.addActionListener(new AddList());
          BSave.addActionListener(new SaveList());
          LSupplierList.addKeyListener(new KeyList());
     }
     private class AddList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               new SuppliersSearch(theLayer,LSupplierList,theListModel);
               
          }
     }
     private class SaveList  implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               placeEnquiry();
          }
     }
     private class KeyList extends KeyAdapter
     {

          public void keyPressed(KeyEvent ke)
          {

               if(ke.getKeyCode()==KeyEvent.VK_DELETE)
               {

                    int iIndex = LSupplierList.getSelectedIndex();
                    
                    theListModel.removeElement(iIndex);
               }
          }

     }
     private void setData()
     {

          VSupCode = new Vector();
          VSupName = new Vector();

          try
          {


               String QString = "Select Name,Ac_Code From Supplier Order By Name";

               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
          
               ResultSet res = stat.executeQuery(QString);
               while(res.next())
               {
                    VSupName.addElement(res.getString(1));
                    VSupCode.addElement(res.getString(2));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void placeEnquiry()
     {
          try
          {
               String SEnqNo = getInsertEnquiryNo();

               for(int i=0; i<theListModel.getSize(); i++)
               {

                    Statement stat      = theConnect. createStatement();

                    stat.execute(getInsertQS());
                    stat.execute(getDeleteQS());

                    PreparedStatement thePrepare =  theConnect.prepareStatement(getPrepareQS());
                    PreparedStatement thePrepare1 =  theConnect.prepareStatement(getUpdatePrepareQS());

                    int iSSlNo = 1;

                    String SSupplier = (String)theListModel.get(i);
                    String SSupCode  =  getSupCode(SSupplier);

                    String SSlNo     = String         . valueOf(iSSlNo+i);


                    thePrepare.setString(1,SEnqNo);
                    thePrepare.setString(2,common.getServerPureDate());
                    thePrepare.setString(3,SSupCode);
                    thePrepare.setString(4,SMrsNo);
                    thePrepare.setString(5,SItemCode);
                    thePrepare.setString(6,SQty);
                    thePrepare.setString(7,SCatl);
                    thePrepare.setString(8,SDraw);
                    thePrepare.setString(9,SMake);
                    thePrepare.setInt(10,iMillCode);
                    thePrepare.setInt(11,iUserCode);
                    thePrepare.setString(12,common.getServerDateTime2());
                    thePrepare.setString(13,SSlNo);
                    thePrepare.setString(14,SDesc);  // Desc
                    thePrepare.setString(15,"1");  

                    thePrepare.executeUpdate();


                    thePrepare1.setInt(1,1);
                    thePrepare1.setString(2,SItemCode);
                    thePrepare1.setString(3,SMrsNo);

                    thePrepare1.executeUpdate();
                    thePrepare.close();
                    thePrepare1.close();
               }
               theConnect.commit();

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
               try
               {
                    JOptionPane.showMessageDialog(null," Problem in Storing Data");
                    theConnect.rollback();
                    return;
               }catch(Exception e){}
          }
          JOptionPane.showMessageDialog(null," Data Stored Successfully");
          theListModel.clear();

               
     }
     private String getInsertQS()
     {
          StringBuffer sb = new StringBuffer();

          sb.append(" Insert into EnquiryChanges(Id,EnqNo,EnqDate,Sup_Code,Mrs_No,");
          sb.append(" Item_Code,Qty,Catl,Drawing,Make,MillCode,UserCode,CreationDate,SLNo,Descr)");
          sb.append(" (select Id,EnqNo,EnqDate,Sup_Code,Mrs_No,");
          sb.append(" Item_Code,Qty,Catl,Drawing,Make,MillCode,UserCode,CreationDate,SLNo,Descr");
          sb.append(" From Enquiry where Item_Code='"+SItemCode+"' and Mrs_No='"+SMrsNo+"' and Status=0 and MailStatus=0)");



          return sb.toString();
     }
     private String getDeleteQS()
     {

          StringBuffer sb = new StringBuffer();

          sb.append(" delete from Enquiry where  Item_Code='"+SItemCode+"' and Mrs_No='"+SMrsNo+"' and Status=0 and MailStatus=0");

          return sb.toString();
     }
     private String getPrepareQS()
     {
          StringBuffer sb = new StringBuffer();

          sb.append(" insert into Enquiry(Id,EnqNo,EnqDate,Sup_Code,Mrs_No,Item_Code,Qty,Catl,Drawing,Make,MillCode,UserCode,CreationDate,SLNo,Descr,AutoMail)");
          sb.append(" values(Enq_Seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

          return sb.toString();
     }
     private String getUpdatePrepareQS()
     {
          StringBuffer sb = new StringBuffer();

          sb.append(" Update Mrs_temp set EnquiryStatus=? where Item_Code=? and MrsNo=? ");

          return sb.toString();

     }

     public String getInsertEnquiryNo()
     {
          String SEnqNo = "";
          String QS     = "";

          QS = " Select (maxno+1) From ConfigAll where Id=3 for update of MaxNo noWait";


          try
          {

               Class.forName("oracle.jdbc.OracleDriver");
               theConnect = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
               theConnect. setAutoCommit(false);

               Statement stat      = theConnect. createStatement();

               PreparedStatement thePrepare = theConnect.prepareStatement(" Update ConfigAll set MaxNo = ?  where Id = 3");

               ResultSet result    = stat         . executeQuery(QS);
               if(result    . next())
               {
                    SEnqNo    = common.parseNull((String)result.getString(1));
               }
               result    . close();

               thePrepare.setInt(1,common.toInt(SEnqNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getInsertEnquiryNo();
               }
               else
               {
               }
          }
          return SEnqNo.trim();
     }                                          
     private String getSupCode(String SSupplier)
     {
          int iIndex = VSupName.indexOf(SSupplier);

          if(iIndex==-1)
               return "";

          return (String)VSupCode.elementAt(iIndex);
     }
     private void setSupplierList(String SItemCode,String SMrsNo)
     {
          try
          {

               String QS = " Select Supplier.Name from Enquiry"+
                           " Inner join Supplier on Supplier.Ac_Code=Enquiry.Sup_Code"+
                           " where Mrs_No=? and Item_Code=?";


               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();
               PreparedStatement    thePrepare = theConnection.prepareStatement(QS);

               thePrepare.setString(1,SMrsNo);
               thePrepare.setString(2,SItemCode);


               ResultSet theResult = thePrepare.executeQuery();
               while(theResult.next())
               {
                    String SSupplier = theResult.getString(1);
                    theListModel.appendElement(SSupplier);
               }
               theResult.close();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

}


/*
    ******************** Query For Finding Party Involved in Items ********************

 select Item_Code,Item_Name,Name,TableName,Item_Industry_Sector.SectorName,Item_Brand.Item_Brand_Name
 from (
 Select InvItems.Item_Code,InvItems.Item_Name,Party_Code,Master_Table_Name as TableName ,
 InvItems_Master_data.TabledataCode as TableDataCode
 from Invitems
 Inner join InvItems_Master_Data on InvItems_Master_Data.Item_Code=InvItems.Item_code
 Inner join Party_Master_Data on Party_Master_Data.TableCode=InvItems_Master_Data.TableCode
 and  Party_Master_Data.TableDataCode=InvItems_Master_Data.TableDataCode
 Inner join  InvItems_MasterTables_Table on InvItems_MasterTables_Table.Id=InvItems_Master_Data.TableCode) t
 Inner join Supplier on Supplier.Ac_Code=t.Party_Code
 Inner join item_industry_sector on Item_industry_Sector.Id=t.TableDataCode
 Inner join Item_Brand on Item_Brand.Id=t.TableDataCode


*/

