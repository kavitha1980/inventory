package MRS;

import javax.swing.*;
import javax.swing.table.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import java.awt.image.*;

import enquiry.EnquiryFormDesign;
import guiutil.*;
import util.*;
import jdbc.*;
import java.net.*;


public class EnquiryPlacementFrame extends JInternalFrame
{
     JLayeredPane theLayer;
     JPanel TopPanel,MiddlePanel,BottomPanel,
            MidLeft,MidRight,MidLeftBot,MidRightBot;

     MyComboBox CIndustryGroup;
     JButton    BApply,BSave,BAdd;


     JTable     theTable;

     JList      LSupplierList;
     SupplierListModel theListModel;

     MRSTempListModel theModel;
     ItemListModel    theItemListModel;
     int              iMillCode;


     Vector           VIndustrySectorCode,VIndustrySectorName,VSupCode,VSupName,VEMail;
     Common           common = new Common();
     Vector           VSupplier;
     Connection       theConnect=null;
     int              iUserCode;
     String           SIds;
     Vector           VEnqNo,VDate,VSSupCode,VMail;
     public EnquiryPlacementFrame(JLayeredPane theLayer,MRSTempListModel theModel,int iMillCode,int iUserCode,String SIds)
     {
          this.theLayer = theLayer;
          this.theModel = theModel;
          this.iMillCode= iMillCode;
          this.iUserCode= iUserCode;
          this.SIds     = SIds;

          setData();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();

     }

     private void createComponents()
     {

          TopPanel       = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();

          MidLeft        = new JPanel();
          MidRight       = new JPanel();

          MidLeftBot     = new JPanel();
          MidRightBot    = new JPanel();

          CIndustryGroup  = new MyComboBox(VIndustrySectorName);

          theListModel   = new SupplierListModel();
          LSupplierList  = new JList(theListModel);


          BApply         = new JButton(" Apply ");
          BSave          = new JButton(" Save  ");
          BAdd           = new JButton(" Add Supplier");

          theItemListModel = new ItemListModel();
          theTable         = new JTable(theItemListModel);

     }
     private void setLayouts()
     {
          TopPanel  .setLayout(new FlowLayout(FlowLayout.CENTER));
          MiddlePanel.setLayout(new GridLayout(2,1));

          MidLeft.setLayout(new BorderLayout());
          MidRight.setLayout(new BorderLayout());


          MidRightBot.setLayout(new BorderLayout());

          setTitle(" Place Enquiry");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,950,450);
          
     

     }
     private void addComponents()
     {
          TopPanel.add(new MyLabel(" Industry Group"));
          TopPanel.add(CIndustryGroup);

          TopPanel.add(BApply);

          MiddlePanel.add(MidLeft);
          MiddlePanel.add(MidRight);

          MidLeft.add(new JScrollPane(theTable));

          MidRight.add(LSupplierList);
          MidRightBot.add(BAdd);
          MidRight.add("South",MidRightBot);

          BottomPanel.add(BSave);


          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

     }

     private void addListeners()
     {
          BApply.addActionListener(new ApplyList());
          BAdd.addActionListener(new AddList());
//          BSave.addActionListener(new SaveList());
          LSupplierList.addKeyListener(new KeyList());
     }
     private class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setItemList();
          }
     }
     private class AddList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               new SuppliersSearch(theLayer,LSupplierList,theListModel);
               
          }
     }
     private class SaveList  implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               placeEnquiry();
          }
     }
     private class KeyList extends KeyAdapter
     {

          public void keyPressed(KeyEvent ke)
          {

               if(ke.getKeyCode()==KeyEvent.VK_DELETE)
               {

                    int iIndex = LSupplierList.getSelectedIndex();

                    theListModel.removeElement(iIndex);

               }


          }

     }
     private void setData()
     {
          VIndustrySectorCode = new Vector();
          VIndustrySectorName = new Vector();

          VSupCode = new Vector();
          VSupName = new Vector();
          VEMail   = new Vector();

          try
          {


               String QString = "Select Name,Ac_Code,Email From Supplier Order By Name";

               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
          
               ResultSet res = stat.executeQuery(QString);
               while(res.next())
               {
                    VSupName.addElement(res.getString(1));
                    VSupCode.addElement(res.getString(2));
                    VEMail.addElement(res.getString(3));
               }
               res  . close();
               stat . close();

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(" Select Id , SectorName from Item_Industry_Sector");
               while(result.next())
               {
                    VIndustrySectorCode.addElement(result.getString(1));
                    VIndustrySectorName.addElement(result.getString(2));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private void setItemList()
     {
          VSupplier = new Vector();
          theItemListModel.setNumRows(0);

          String QS="";
          if(((String)CIndustryGroup.getSelectedItem()).equals("NONE"))
               QS = getItemDataQS();
          else
               QS = getItemDataCondQS();
          try
          {

               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();

               Statement theStatement       = theConnection.createStatement();
               ResultSet theResult          = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    HashMap theMap = new HashMap();

                    theMap.put("ItemCode", common.parseNull(theResult.getString(1)));
                    theMap.put("ItemName", common.parseNull(theResult.getString(2)));
                    theMap.put("MrsNo"   , common.parseNull(theResult.getString(3)));
                    theMap.put("Make"    , common.parseNull(theResult.getString(4)));
                    theMap.put("Catl"    , common.parseNull(theResult.getString(5)));
                    theMap.put("Draw"    , common.parseNull(theResult.getString(6)));
                    theMap.put("Qty"     , common.parseNull(theResult.getString(7)));

                    String SSupplier = common.parseNull(theResult.getString(8));
                    theMap.put("Desc"     , common.parseNull(theResult.getString(9)));


                    if(!((String)CIndustryGroup.getSelectedItem()).equals("NONE"))
                    {
                         int iIndex = VSupplier.indexOf(SSupplier);
     
                         if(iIndex==-1)
                         VSupplier.addElement(SSupplier);
                         LSupplierList.setListData(VSupplier);
                    }
                    theItemListModel.appendRow(theMap);
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }



     }

     private int getIndustrySectorCode(String SIndustrySectorName)
     {
          int iIndex = VIndustrySectorName.indexOf(SIndustrySectorName);

          if(iIndex==-1)
               return 0;

          return common.toInt((String)VIndustrySectorCode.elementAt(iIndex));
     }

     public void placeEnquiry()
     {
          int iCount=0;
          try
          {
               VEnqNo    = new Vector();
               VDate     = new Vector();
               VMail     = new Vector();
               VSSupCode  = new Vector();

               String SEnqNo = getInsertEnquiryNo();

               for(int j=0; j<theItemListModel.getRows(); j++)
               {

                    Boolean bValue = (Boolean)theItemListModel.getValueAt(j,8);

                    if(bValue)
                    {
     
                         PreparedStatement thePrepare =  theConnect.prepareStatement(getPrepareQS());
                         PreparedStatement thePrepare1 =  theConnect.prepareStatement(getUpdatePrepareQS());
     
                         int iSSlNo = 1;

                         for(int i=0; i<theListModel.getSize(); i++)
                         {

                              String SSupplier = (String)theListModel.get(i);
                              String SSupCode  =  getSupCode(SSupplier);
                              String SMail     =  getEMail(SSupplier);

                              String SItemCode = (String)theItemListModel.getValueAt(j,1);
                              String SMrsNo    = (String)theItemListModel.getValueAt(j,0);
                              String SDesc     = (String)theItemListModel.getValueAt(j,3);
                              String SQty      = (String)theItemListModel.getValueAt(j,7);
                              String SCatl     = (String)theItemListModel.getValueAt(j,5);
                              String SDraw     = (String)theItemListModel.getValueAt(j,6);
                              String SMake     = (String)theItemListModel.getValueAt(j,4);
                              String SSlNo     = String         . valueOf(iSSlNo+j);


                              String SEnqDate = common.getServerPureDate();
                              thePrepare.setString(1,SEnqNo);
                              thePrepare.setString(2,SEnqDate);
                              thePrepare.setString(3,SSupCode);
                              thePrepare.setString(4,SMrsNo);
                              thePrepare.setString(5,SItemCode);
                              thePrepare.setString(6,SQty);
                              thePrepare.setString(7,SCatl);
                              thePrepare.setString(8,SDraw);
                              thePrepare.setString(9,SMake);
                              thePrepare.setInt(10,iMillCode);
                              thePrepare.setInt(11,iUserCode);
                              thePrepare.setString(12,common.getServerDateTime2());
                              thePrepare.setString(13,SSlNo);
                              thePrepare.setString(14,SDesc);  // Desc
                              thePrepare.setString(15,"1"); 


                              thePrepare.executeUpdate();

                              thePrepare1.setInt(1,1);
                              thePrepare1.setString(2,SEnqNo);
                              thePrepare1.setString(3,SItemCode);
                              thePrepare1.setString(4,SMrsNo);

                              thePrepare1.executeUpdate();

                              VEnqNo.addElement(SEnqNo);
                              VDate.addElement(SEnqDate);
                              VMail.addElement(SMail);
                              VSSupCode.addElement(SSupCode);
                              iCount=iCount+1;
                         }
                         thePrepare.close();
                         thePrepare1.close();
                    }
               }
               theConnect.commit();
               MailEnqs();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
               try
               {
                    JOptionPane.showMessageDialog(null," Problem in Storing Data");
                    theConnect.rollback();
                    return;
               }catch(Exception e){}
          }
          if(iCount>0)
          {
               JOptionPane.showMessageDialog(null," Data Stored Successfully");
          }
          else
          {
               JOptionPane.showMessageDialog(null," No Rows Selected For Save...");
          }

          theItemListModel.setNumRows(0);
          theListModel.clear();
     }
     private void toForm(String SEnqNo,String SEnqDate,String SMail,String SSupCode,String SApproved)
     {
               new enquiry.EnquiryFormDesign(SEnqNo,SEnqDate,SSupCode,iMillCode,SMail,SApproved,"Supplier");
     }

     private String getPrepareQS()
     {
          StringBuffer sb = new StringBuffer();

          sb.append(" insert into Enquiry(Id,EnqNo,EnqDate,Sup_Code,Mrs_No,Item_Code,Qty,Catl,Drawing,Make,MillCode,UserCode,CreationDate,SLNo,Descr,AutoMail)");
          sb.append(" values(Enq_Seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

          return sb.toString();
     }
     private String getUpdatePrepareQS()
     {
          StringBuffer sb = new StringBuffer();

          sb.append(" Update Mrs_temp set EnquiryStatus=?,EnquiryNo=? where Item_Code=? and MrsNo=? ");

          return sb.toString();

     }

     private String getItemDataQS()
     {

          StringBuffer sb = new StringBuffer();

          sb.append(" Select Mrs_Temp.Item_Code,InvItems.Item_Name,");
          sb.append(" Mrs_Temp.MrsNo,Mrs_Temp.Make,Mrs_Temp.Catl,Mrs_Temp.Draw,Mrs_Temp.Qty,'' as Name,Mrs_temp.Remarks");
          sb.append(" from Invitems");
          sb.append(" Inner join Mrs_Temp on Mrs_Temp.Item_Code=InvItems.Item_Code and Mrs_Temp.ReadyForApproval=0");
          sb.append(" and Mrs_Temp.ConvertToOrder=0");
          sb.append(" and Mrs_Temp.ReadyForApproval=0");
          sb.append(" and Mrs_Temp.RejectionStatus=0 and Mrs_Temp.isDelete=0 and Mrs_Temp.Itemdelete=0");
          sb.append(" and Mrs_Temp.StoresRejectionStatus=0");
          sb.append(" and Mrs_Temp.DeptMrsAuth=1 and Mrs_temp.Id in("+SIds+")");
          sb.append(" and Mrs_temp.MillCode="+iMillCode+" and Mrs_Temp.EnquiryStatus=0");

               

          return sb.toString();
     }
     private String getItemDataCondQS()
     {

          StringBuffer sb = new StringBuffer();

          sb.append(" Select Mrs_Temp.Item_Code,t.Item_Name,");
          sb.append(" Mrs_Temp.MrsNo,Mrs_Temp.Make,Mrs_Temp.Catl,Mrs_Temp.Draw,Mrs_Temp.Qty,Supplier.Name,Mrs_temp.Remarks");
          sb.append(" from (");
          sb.append(" Select InvItems.Item_Code,InvItems.Item_Name,Party_Code,Master_Table_Name as TableName ,");
          sb.append(" InvItems_Master_data.TabledataCode as TableDataCode");
          sb.append(" from Invitems");
          sb.append(" Inner join InvItems_Master_Data on InvItems_Master_Data.Item_Code=InvItems.Item_code");
          sb.append(" Inner join Party_Master_Data on Party_Master_Data.TableCode=InvItems_Master_Data.TableCode");
          sb.append(" and  Party_Master_Data.TableDataCode=InvItems_Master_Data.TableDataCode");
          sb.append(" Inner join  InvItems_MasterTables_Table on InvItems_MasterTables_Table.Id=InvItems_Master_Data.TableCode) t");
          sb.append(" Inner join Mrs_Temp on Mrs_Temp.Item_Code=t.Item_Code and Mrs_Temp.ReadyForApproval=0");
          sb.append(" and Mrs_Temp.ConvertToOrder=1");
          sb.append(" and ReadyForApproval=0");
          sb.append(" and RejectionStatus=0 and isDelete=0 and Itemdelete=0");
          sb.append(" and StoresRejectionStatus=0");
          sb.append(" and Mrs_Temp.DeptMrsAuth=1 and Mrs_temp.Id in("+SIds+")");
          sb.append("  and Mrs_temp.MillCode=0");
          sb.append(" Inner join Supplier on Supplier.Ac_Code=t.Party_Code");
          sb.append(" Inner join item_industry_sector on Item_industry_Sector.Id=t.TableDataCode");
          sb.append(" and Item_Industry_Sector.Id="+getIndustrySectorCode((String)CIndustryGroup.getSelectedItem()));

               

          return sb.toString();
     }

     private int getIndustrySectoryCode(String SName)
     {
          int iIndex = VIndustrySectorName.indexOf(SName);

          if(iIndex==-1)
               return 0;

          return common.toInt((String)VIndustrySectorCode.elementAt(iIndex));

     }
     public String getInsertEnquiryNo()
     {
          String SEnqNo = "";
          String QS     = "";

          QS = " Select (maxno+1) From ConfigAll where Id=3 for update of MaxNo noWait";

          try
          {

               Class.forName("oracle.jdbc.OracleDriver");
               theConnect = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
               theConnect. setAutoCommit(false);

               Statement stat      = theConnect. createStatement();

               PreparedStatement thePrepare = theConnect.prepareStatement(" Update ConfigAll set MaxNo = ?  where Id = 3");

               ResultSet result    = stat         . executeQuery(QS);
                         if(result    . next())
                         {
                              SEnqNo    = common.parseNull((String)result.getString(1));
                         }
                         result    . close();


               thePrepare.setInt(1,common.toInt(SEnqNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getInsertEnquiryNo();
               }
               else
               {
               }
          }
          return SEnqNo.trim();
     }                                          
     private String getSupCode(String SSupplier)
     {
          int iIndex = VSupName.indexOf(SSupplier);

          if(iIndex==-1)
               return "";

          return (String)VSupCode.elementAt(iIndex);
     }
     private String getEMail(String SSupplier)
     {
          int iIndex = VSupName.indexOf(SSupplier);

          if(iIndex==-1)
               return "";

          return (String)VEMail.elementAt(iIndex);
     }
     private void MailEnqs()
     {
          try
          {
               for(int i=0;i<VEnqNo.size();i++)
               {
                    String SEnqNo   = (String)VEnqNo   . elementAt(i);
                    String SEnqDate = (String)VDate    . elementAt(i);
                    String SMail    = (String)VMail   . elementAt(i);
                    String SSupCode = (String)VSSupCode . elementAt(i);
                    String SApproved= "1";
                                 toForm(SEnqNo,SEnqDate,SMail,SSupCode,SApproved);
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     public void removeFrame()
     {
          try
          {
               theLayer.remove(this);
               theLayer.updateUI();
          }catch(Exception e)
          {
          }

     }

}


/*
    ******************** Query For Finding Party Involved in Items ********************

 select Item_Code,Item_Name,Name,TableName,Item_Industry_Sector.SectorName,Item_Brand.Item_Brand_Name
 from (
 Select InvItems.Item_Code,InvItems.Item_Name,Party_Code,Master_Table_Name as TableName ,
 InvItems_Master_data.TabledataCode as TableDataCode
 from Invitems
 Inner join InvItems_Master_Data on InvItems_Master_Data.Item_Code=InvItems.Item_code
 Inner join Party_Master_Data on Party_Master_Data.TableCode=InvItems_Master_Data.TableCode
 and  Party_Master_Data.TableDataCode=InvItems_Master_Data.TableDataCode
 Inner join  InvItems_MasterTables_Table on InvItems_MasterTables_Table.Id=InvItems_Master_Data.TableCode) t
 Inner join Supplier on Supplier.Ac_Code=t.Party_Code
 Inner join item_industry_sector on Item_industry_Sector.Id=t.TableDataCode
 Inner join Item_Brand on Item_Brand.Id=t.TableDataCode


*/

