package MRS;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import guiutil.*;
import util.*;

public class ItemListModel extends DefaultTableModel 
{

     protected String SWhere;
     protected String SIndentNo;

     Vector theVector;

     String         ColumnName[] = {"Mrs No","Code","Name","Desc","Make","Catl","Draw","Qty","Select"};
     String         ColumnType[] = {"S"     ,"S"   ,"S"   ,"S"   ,"S"   ,"S"   , "S"  ,"S",  "B"};

     Common common = new Common();

     int iList=0;

     ItemListModel()
     {
          setDataVector(getRowData(),ColumnName);
     }
     public Class getColumnClass(int col)
     {
          return getValueAt(0,col).getClass();
     }
     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol]=="B" || ColumnType[iCol]=="E")
               return true;
          return false;
     }

     public int getRows()
     {
          return super.dataVector.size();
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[0][ColumnName.length];

          return RowData;
     }
     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }
     public void appendRow(HashMap theMap)
     {
          Vector theVect = new Vector();

          theVect.addElement((String)theMap.get("MrsNo"));
          theVect.addElement((String)theMap.get("ItemCode"));
          theVect.addElement((String)theMap.get("ItemName"));
          theVect.addElement((String)theMap.get("Desc"));
          theVect.addElement((String)theMap.get("Make"));
          theVect.addElement((String)theMap.get("Catl"));
          theVect.addElement((String)theMap.get("Draw"));
          theVect.addElement((String)theMap.get("Qty"));

          theVect.addElement(new Boolean(false));


          insertRow(getRows(),theVect);
     }

     public String getValueFrom(int iRow,int iCol)
     {
          return (String)getValueAt(iRow,iCol);
     }
     public void setMasterDetails(String SValue,int iRow,int iCol)
     {
          setValueAt(SValue,iRow,iCol);                        
     }
     public void deleteRow(int index)
     {
         if(getRows()==1)
         return;
         if(index>=getRows())
         return;
         if(index==-1)
         return;
         removeRow(index);
     }
     public void setValueAt(Object aValue, int row, int column)
     {
            try
            {

                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));

            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
     }
     private void setRejectionStatus(int iRow)
     {
     }
     private void setApprovalStatus(int iRow)
     {
     }
}

