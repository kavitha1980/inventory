package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MRSCompFrame extends JInternalFrame
{
     CriteriaPanel  criteriapanel;
     TabReport      tabreport;
     JScrollPane    TabScroll;
     Object         RowData[][];
     String         ColumnData[]= {"Material Code","Material Name","April","May","June","July","August","September","October","November","December","January","Febraury","March"}; 
     String         ColumnType[]= {"S","S","N","N","N","N","N","N","N","N","N","N","N","N"};  
     Vector         VMatRec;
     Common         common = new Common();
     JLayeredPane   DeskTop;
     Vector         VCode,VName,VNameCode;
     Vector         VMName,VMQty,VMValue;
     int            iMillCode;
     String         SStDate,SEnDate;
     String         SItemTable;

     public MRSCompFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,int iMillCode,String SStDate,String SEnDate,String SItemTable)
     {
          super("Monthly Comparison on MRSs");
          this.DeskTop    = DeskTop;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.iMillCode  = iMillCode;
          this.SStDate    = SStDate;
          this.SEnDate    = SEnDate;
          this.SItemTable = SItemTable;
          
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          criteriapanel = new CriteriaPanel(DeskTop,VCode,VName,VNameCode,iMillCode,SItemTable);
     }

     public void setLayouts()
     {
          setClosable(true);
          setIconifiable(true);
          setResizable(true);
          setMaximizable(true);
          setBounds(0,0,650,500);
     }

     public void addComponents()
     {
          getContentPane().add(criteriapanel,BorderLayout.NORTH);
     }

     public void addListeners()
     {
          criteriapanel.BApply.addActionListener(new ApplyList());
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(TabScroll); 
               }
               catch(Exception ex)
               {
               }
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    TabScroll           = new JScrollPane(tabreport);
                    getContentPane()    . add(TabScroll,BorderLayout.CENTER);
                    tabreport           . ReportTable.setShowHorizontalLines(true);
                    setSelected(true);
                    DeskTop             . repaint();
                    DeskTop             . updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }
     
     public void setDataIntoVector()
     {
          ResultSet result    = null;
                    VMatRec   = new Vector();
          MatMonRec matmonrec = new MatMonRec();
          String    SMatCode  = "";      
          try
          {
               ORAConnection  oraConnection = ORAConnection.getORAConnection();
               Connection     theConnection = oraConnection.getConnection();
               Statement      stat          = theConnection.createStatement();
                              result        = stat.executeQuery(getQString());

               while (result.next())
               {
                    int       iMon = result.getInt(1);  
                    String    str2 = result.getString(2);
                    String    str3 = result.getString(3);
                    String    str4 = result.getString(4);

                    if(!str2.equals(SMatCode))
                    {
                         VMatRec   . addElement(matmonrec); 
                         matmonrec = new MatMonRec();
                         matmonrec . SMatCode = str2;
                         matmonrec . SMatName = str3;
                         SMatCode  = str2;
                    }
                    matmonrec.setQty(str4,iMon);
               }
               VMatRec   . addElement(matmonrec);
               VMatRec   . removeElementAt(0);
               result.close();
               stat.close();
           }
           catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VMatRec.size()][14];
          for(int i=0;i<VMatRec.size();i++)
          {
               MatMonRec matmonrec      = (MatMonRec)VMatRec.elementAt(i);
                         RowData[i][0]  = (String)matmonrec.SMatCode;
                         RowData[i][1]  = (String)matmonrec.SMatName;
               for(int j=0;j<matmonrec.VQty.size();j++)
                    RowData[i][j+2]     = (String)matmonrec.VQty.elementAt(j);                
          }  
     }

     public String getQString()
     {
          String QString = "",HString="",str="";
          int    iSig    = 0;
          
          QString       = " SELECT  substr(MRS.MrsDate,5,2) as fn, MRS.Item_Code, InvItems.Item_Name, sum(MRS.Qty) "+
                          " FROM MRS INNER JOIN InvItems ON MRS.Item_Code=InvItems.Item_Code "+
                          " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) "+
                          " and MRS.MillCode="+iMillCode+
                          " and MRS.MrsDate>='"+SStDate+"'"+
                          " and MRS.MrsDate<='"+SEnDate+"'"+
                          " GROUP BY substr(MRS.MrsDate,5,2),MRS.Item_Code,InvItems.Item_Name ";

          if(criteriapanel.JRSeleDept.isSelected())
          {                         
               QString = QString+", MRS.Dept_Code ";
               HString = "MRS.Dept_Code="+(String)criteriapanel.VDeptCode.elementAt(criteriapanel.JCDept.getSelectedIndex());
               iSig    = 1;            
          }

          if(criteriapanel.JRSeleMat.isSelected())
          {                         
               str       = "MRS.Item_Code='"+criteriapanel.TMatCode.getText()+"'";
               HString   = (iSig==1 ? " "+HString+" and "+str:str);
               iSig      = 1;            
          }
          
          if(criteriapanel.JRSeleGroup.isSelected())
          {
               QString   = QString+", MRS.Group_Code ";
               str       = "MRS.Group_Code="+(String)criteriapanel.VGroupCode.elementAt(criteriapanel.JCGroup.getSelectedIndex());
               HString   = (iSig==1 ? " "+HString+" and "+str:str);
               iSig      = 1;            
          }

          if(criteriapanel.JRSeleUnit.isSelected())
          {
               QString   = QString+", MRS.Unit_Code ";
               str       = "MRS.Unit_Code="+(String)criteriapanel.VUnitCode.elementAt(criteriapanel.JCUnit.getSelectedIndex());
               HString   = (iSig==1 ? " "+HString+" and "+str:str);
               iSig      = 1;
          }
          HString = (iSig>0 ? "Having "+HString : " ");

          return QString+" "+HString+" Order By MRS.Item_Code";
     }
}
