package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MRSConversionPendingListFrame extends JInternalFrame
{
     JButton        BApply;
     JPanel         TopPanel,BottomPanel;
     JPanel         First,Second,Third,Fourth,Fifth;
     JComboBox      JCFilter;
     JTextField     TOrder;
     JLayeredPane   DeskTop;
     JRadioButton   JRPeriod,JRNo;
     MyComboBox     JCPending;
     JLayeredPane   theLayer;

     StatusPanel    SPanel;
     TabReport      tabreport;
     DateField     TStDate,TEnDate;
     NextField      TStNo,TEnNo;
     Common         common;

     Object         RowData[][];

     String         ColumnData[] = {"Date","Mrs No","Code","Name","Qty","NetRate","NetValue","Unit","Department","Group","Status","Due Date","User","Status","DocId","UserEntryItemName","ApprDatTime"};
     String         ColumnType[] = {"S","N","S","S","N","S","S","S","S","S","S","S","S","S","S","S","S"};

     int            iMillCode,iAuthCode,iUserCode;

     Vector VCode,VName,VNameCode,VCata,VDraw;
     Vector VPaperColor,VPaperSets,VPaperSize,VPaperSide,VSlipNo;

     Vector VDate,VMrsNo,VRCode,VRName,VQty,VDept,VGroup,VStatus,VUnit,VDue,VId,VHodName,VMrsOrdNo,VNetRate,VNetValue,VUser,
          VDeptMrsAuth , VReadyForApproval ,
          VApprovalStatus , VStoresRejectionStatus,VRejectionStatus ,VDocId,VEntryItemName  ,VApprDateTime   ;

     int iOwnerCode;
     public MRSConversionPendingListFrame(JLayeredPane Layer,int iMillCode,int iAuthCode,int iUserCode) //,Vector VCode,Vector VName,Vector VNameCode,Vector VCata,Vector VDraw,Vector VPaperColor,Vector VPaperSets,Vector VPaperSize,Vector VPaperSide,Vector VSlipNo
     {
          super("MRSs Placed During a Period");
     
          this.DeskTop        = Layer;
          this.iMillCode      = iMillCode;
          this.iAuthCode      = iAuthCode;
          this.iUserCode      = iUserCode;


/*
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.VCata          = VCata;
          this.VDraw          = VDraw;

          this.VPaperColor    = VPaperColor;
          this.VPaperSets     = VPaperSets;
          this.VPaperSize     = VPaperSize;
          this.VPaperSide     = VPaperSide;
          this.VSlipNo        = VSlipNo;

*/


          common         = new Common();

          setOwnerCode();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TStDate     = new DateField();
          TEnDate     = new DateField();
          BApply      = new JButton("Apply");
          TopPanel    = new JPanel();
          First       = new JPanel();
          Second      = new JPanel();
          Third       = new JPanel();
          Fourth      = new JPanel();
          Fifth       = new JPanel();
          BottomPanel = new JPanel();
          TOrder      = new JTextField("1,2,3");
          JCFilter    = new JComboBox();
          TStNo       = new NextField(0);
          TEnNo       = new NextField(0);
          JCPending   = new MyComboBox();

     
          JRPeriod    = new JRadioButton("Periodical",true);
          JRNo        = new JRadioButton("MRS No",false);
     
          TStDate.setTodayDate();
          TEnDate.setTodayDate();

          JCPending.addItem(" All");
          JCPending.addItem(" Pending With HOD Authentication");
          JCPending.addItem(" Pending With Stores Authentication");
          JCPending.addItem(" Pending With JMD");
          JCPending.addItem(" Converted");
          JCPending.addItem(" Rejected Item By Stores");
          JCPending.addItem(" Rejected Item By Jmd");



     }

     public void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(1,5));
          First     .setLayout(new GridLayout(1,1));
          Second    .setLayout(new GridLayout(1,1));
          Third     .setLayout(new GridLayout(2,1));
          Fourth    .setLayout(new GridLayout(2,1));
          Fifth     .setLayout(new GridLayout(1,1));
     
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          JCFilter.addItem("All");
          JCFilter.addItem("Over-Due");
          JCFilter.addItem("Order Not Placed");
          
          First.setBorder(new TitledBorder("Sorted On (Column No)"));
          First.add(TOrder);
          
          Second.setBorder(new TitledBorder("Filter"));
          Second.add(JCPending);
          
          Third.setBorder(new TitledBorder("Basis"));
          Third.add(JRPeriod);
          Third.add(JRNo);
          
          Fourth.setBorder(new TitledBorder("Periodical"));
          Fourth.add(TStDate);
          Fourth.add(TEnDate);
          
          Fifth.add(BApply);
          
          TopPanel.add(First);
          TopPanel.add(Second);
          TopPanel.add(Third);
          TopPanel.add(Fourth);
          TopPanel.add(Fifth);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          JRPeriod.addActionListener(new JRList());
          JRNo.addActionListener(new JRList()); 
          BApply.addActionListener(new ApplyList());

     }

     public class KeyList extends KeyAdapter
     {

          public void keyPressed(KeyEvent ke)
          {

               if (ke.getKeyCode()==KeyEvent.VK_HOME)
               {

                         // changes to be made

                         int iRow  = tabreport.ReportTable.getSelectedRow();

                         int iDocId = common.toInt((String)tabreport.ReportTable.getValueAt(iRow,14));
                         try
                         {
                              DocumentListFrame doclistframe = new DocumentListFrame(DeskTop,iDocId);
                              DeskTop.add(doclistframe);
                              DeskTop.repaint();
                              doclistframe.setSelected(true);
                              DeskTop.updateUI();
                              doclistframe.show();
                         }catch(Exception ex)
                         {
                              System.out.println(ex);
                              ex.printStackTrace();
                         }

               }
          }
     }
     

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    Fourth.setBorder(new TitledBorder("Periodical"));
                    Fourth.removeAll();
                    Fourth.add(TStDate);
                    Fourth.add(TEnDate);
                    Fourth.updateUI();
                    JRNo.setSelected(false);
               }
               else
               {
                    Fourth.setBorder(new TitledBorder("Numbered"));
                    Fourth.removeAll();
                    Fourth.add(TStNo);
                    Fourth.add(TEnNo);
                    Fourth.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    setDataIntoVector();
                    setRowData();
               try
               {
                  getContentPane().remove(tabreport);
               }
               catch(Exception ex)
               {}
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.setBorder(new TitledBorder("List"));
               getContentPane().add(tabreport,BorderLayout.CENTER);
               tabreport.ReportTable.addKeyListener(new KeyList());
               setSelected(true);
               DeskTop.repaint();
               DeskTop.updateUI();
               }
               catch(Exception ex)
               {
               }
          }
     }

     public void setDataIntoVector()
     {
          ResultSet result = null;

          VDate       = new Vector();
          VMrsNo      = new Vector();
          VRCode      = new Vector();
          VRName      = new Vector();
          VQty        = new Vector();
          VDept       = new Vector();
          VGroup      = new Vector();
          VStatus     = new Vector();
          VDue        = new Vector();
          VUnit       = new Vector();
          VId         = new Vector();
          VHodName    = new Vector();
          VMrsOrdNo   = new Vector();
          VNetRate    = new Vector();
          VNetValue   = new Vector();
          VUser               = new Vector();
          VDeptMrsAuth        = new Vector();
          VReadyForApproval   = new Vector();
          VApprovalStatus     = new Vector();
          VStoresRejectionStatus   = new Vector();
          VRejectionStatus         = new Vector();
          VDocId                   = new Vector();
          VEntryItemName           = new Vector();
          VApprDateTime            = new Vector();


          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
                              result        =  stat.executeQuery(getQString());
               
               while (result.next())
               {
                    VMrsNo    .addElement((String)result.getString(2));
                    VDate     .addElement(common.parseDate((String)result.getString(1)));
                    VRCode    .addElement((String)result.getString(3));
                    VRName    .addElement((String)result.getString(4));
                    VQty      .addElement((String)result.getString(5));
                    VDept     .addElement((String)result.getString(6));
                    VGroup    .addElement((String)result.getString(7));
                    VDue      .addElement(common.parseDate((String)result.getString(8)));
                    VUnit     .addElement((String)result.getString(9));
                    VId       .addElement((String)result.getString(10));
                    VStatus   .addElement(" ");
                    VHodName  .addElement((String)result.getString(11));
                    VMrsOrdNo .addElement((String)result.getString(12));
                    VNetRate  .addElement((String)result.getString(13));
                    VNetValue .addElement((String)result.getString(14));
                    VUser     .addElement((String)result.getString(15));
                    VDeptMrsAuth.addElement((String)result.getString(16));
                    VReadyForApproval.addElement((String)result.getString(17));
                    VApprovalStatus.addElement((String)result.getString(18));
                    VStoresRejectionStatus.addElement((String)result.getString(19));
                    VRejectionStatus.addElement((String)result.getString(20));
                    VDocId          .addElement((String)result.getString(21));
                    VEntryItemName  .addElement((String)result.getString(22));
                    VApprDateTime   .addElement((String)result.getString(23));
               }
               result.close();
          }
          catch(Exception ex)
          {
               System.out.println("setDataIntoVector: "+ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VDate.size()][ColumnData.length];
          for(int i=0;i<VDate.size();i++)
          {

               RowData[i][0] = (String)VDate.elementAt(i);
               RowData[i][1] = (String)VMrsNo.elementAt(i);
               RowData[i][2] = (String)VRCode.elementAt(i);
               RowData[i][3] = (String)VRName.elementAt(i);
               RowData[i][4] = (String)VQty.elementAt(i);
               RowData[i][5] = (String)VNetRate.elementAt(i);
               RowData[i][6] = (String)VNetValue.elementAt(i);
               RowData[i][7] = (String)VUnit.elementAt(i);
               RowData[i][8] = (String)VDept.elementAt(i);
               RowData[i][9] = common.parseNull((String)VGroup.elementAt(i));
               RowData[i][10] = (String)VStatus.elementAt(i);
               RowData[i][11] = common.parseNull((String)VDue.elementAt(i));
               RowData[i][12] = common.parseNull((String)VUser.elementAt(i));
               RowData[i][13] = getStatus(i);
               RowData[i][14] = common.parseNull((String)VDocId.elementAt(i));
               RowData[i][15] = common.parseNull((String)VEntryItemName.elementAt(i));
               RowData[i][16] = common.parseNull((String)VApprDateTime.elementAt(i));
          }
     }

     public String getQString()
     {
          DateField df = new DateField();
          df.setTodayDate();
          String SToday = df.toNormal();
          String QString = "";

          
          if(iMillCode!=1)
          {               
               if(JRPeriod.isSelected())
               {
                    QString = "SELECT Mrs_Temp.MrsDate, Mrs_Temp.MrsNo, Mrs_Temp.Item_Code, decode(InvItems.Item_Name,null,Mrs_Temp.Item_Name,InvItems.Item_Name) , Mrs_Temp.Qty, Dept.Dept_Name , Cata.Group_Name , Mrs_Temp.DueDate, Unit.Unit_Name,Mrs_Temp.Id,Hod.hodname,Mrs_Temp.orderno,Mrs_temp.NetRate,Mrs_temp.NetValue,RawUser.UserName, "+
                              " Mrs_Temp.DeptMrsAuth,Mrs_Temp.ReadyForApproval,Mrs_temp.ApprovalStatus,Mrs_temp.StoresRejectionStatus,Mrs_Temp.RejectionStatus,Mrs_temp.DocumentId,EntryItemName,Mrs_Temp.ApprovalDateTime"+
                              " FROM (((MRS_Temp Left JOIN InvItems ON Mrs_Temp.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON Mrs_Temp.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON Mrs_Temp.Group_Code=Cata.Group_Code) INNER JOIN Unit ON Mrs_Temp.Unit_Code=Unit.Unit_Code "+
                              " INNER JOIN RAWUSER on RAWUSER.USERCODE=Mrs_Temp.AuthUserCode "+
                              " LEFT JOIN Hod ON Mrs_Temp.HODCode=HOD.HodCode Where Mrs_Temp.MrsFlag=1 and Mrs_Temp.Qty >= 0 And Mrs_Temp.MrsDate >= '"+TStDate.toNormal()+"' and Mrs_Temp.MrsDate <='"+TEnDate.toNormal()+"' and Mrs_Temp.MillCode="+iMillCode+
						" and Isdelete=0 and ItemDelete=0  ";

              if(JCPending.getSelectedIndex()==1)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 0 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0 ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==2)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0 ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==3)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=0 and RejectionStatus=0   " ;           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==4)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=1 and RejectionStatus=0   ";           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==5)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=0 and StoresRejectionStatus=1  " ;                           // and AuthUserCode="+iOwnerCode
              if(JCPending.getSelectedIndex()==6)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=1 and RejectionStatus=1  ";                                // and AuthUserCode="+iOwnerCode

                            QString=QString+  " Union All"+
                              " SELECT to_char(ProjectSkeleton.EntryDate), ProjectSkeleton.MrsNo, ProjectSkeleton.Item_Code, decode(InvItems.Item_Name,null,ProjectSkeleton.Item_Name,InvItems.Item_Name) , ProjectSkeleton.Qty, Dept.Dept_Name , Cata.Group_Name ,to_Char(ProjectSkeleton.DueDate), Unit.Unit_Name,ProjectSkeleton.Id,'' as hodname,0 as ORderNo,ProjectSkeleton.NetRate,ProjectSkeleton.NetValue,RawUser.UserName,"+
                              " ProjectSkeleton.DeptMrsAuth,ProjectSkeleton.ReadyForApproval,ProjectSkeleton.ApprovalStatus,ProjectSkeleton.StoresRejectionStatus,ProjectSkeleton.RejectionStatus,ProjectSkeleton.DocumentId,EntryItemName,ProjectSkeleton.ApprovalDateTime"+
                              " FROM (((ProjectSkeleton Left JOIN InvItems ON ProjectSkeleton.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON ProjectSkeleton.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON ProjectSkeleton.Group_Code=Cata.Group_Code) INNER JOIN Unit ON ProjectSkeleton.Unit_Code=Unit.Unit_Code"+
                              " INNER JOIN RAWUSER on RAWUSER.USERCODE=ProjectSkeleton.AuthUserCode"+
                              " Where  ProjectSkeleton.Qty >= 0 "+
                              " And ProjectSkeleton.EntryDate >= '"+TStDate.toNormal()+"' and ProjectSkeleton.EntryDate <='"+TEnDate.toNormal()+"' and ProjectSkeleton.MillCode="+iMillCode+
						" and Isdelete=0 and ItemDelete=0  ";

              if(JCPending.getSelectedIndex()==1)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 0 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0  ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==2)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0  ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==3)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=0 and RejectionStatus=0   " ;           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==4)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=1 and RejectionStatus=0  ";           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==5)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=0 and StoresRejectionStatus=1 " ;                           // and AuthUserCode="+iOwnerCode
              if(JCPending.getSelectedIndex()==6)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=1 and RejectionStatus=1  ";                                // and AuthUserCode="+iOwnerCode


               }
               else
               {
                    QString = " SELECT Mrs_Temp.MrsDate, Mrs_Temp.MrsNo, Mrs_Temp.Item_Code, decode(InvItems.Item_Name,null,Mrs_Temp.Item_Name,InvItems.Item_Name) , Mrs_Temp.Qty, Dept.Dept_Name , Cata.Group_Name , Mrs_Temp.DueDate, Unit.Unit_Name,Mrs_Temp.Id,hodname,Mrs_Temp.orderno,Mrs_temp.NetRate,Mrs_temp.NetValue,RawUser.UserName, "+
                              " Mrs_Temp.DeptMrsAuth,Mrs_Temp.ReadyForApproval,Mrs_temp.ApprovalStatus,Mrs_temp.StoresRejectionStatus,Mrs_Temp.RejectionStatus,Mrs_temp.DocumentId,EntryItemName,Mrs_Temp.ApprovalDateTime"+
                              " FROM (((MRS_Temp Left JOIN InvItems ON Mrs_Temp.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON Mrs_Temp.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON Mrs_Temp.Group_Code=Cata.Group_Code) INNER JOIN Unit ON Mrs_Temp.Unit_Code=Unit.Unit_Code "+
                              " INNER JOIN RAWUSER on RAWUSER.USERCODE=Mrs_Temp.AuthUserCode "+
                              " LEFT JOIN Hod ON Mrs_Temp.HODCode=HOD.HodCode Where Mrs_Temp.MrsFlag=1 and Mrs_Temp.Qty >= 0 And Mrs_Temp.MrsNo >="+TStNo.getText()+" and Mrs_Temp.MrsNo <="+TEnNo.getText()+" and Mrs_Temp.MillCode="+iMillCode+
						" and Isdelete=0 and ItemDelete=0  ";

              if(JCPending.getSelectedIndex()==1)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 0 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0 ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==2)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0 ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==3)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=0 and RejectionStatus=0   " ;           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==4)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=1 and RejectionStatus=0   ";           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==5)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=0 and StoresRejectionStatus=1 " ;                           // and AuthUserCode="+iOwnerCode
              if(JCPending.getSelectedIndex()==6)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=1 and RejectionStatus=1  ";                                // and AuthUserCode="+iOwnerCode
          if(JCPending.getSelectedIndex()==0)
               QString = QString+" and Mrs_Temp.DeptMRsAuth=1  ";

                            QString=QString+  " Union All"+
                              " SELECT to_char(ProjectSkeleton.EntryDate), ProjectSkeleton.MrsNo, ProjectSkeleton.Item_Code, decode(InvItems.Item_Name,null,ProjectSkeleton.Item_Name,InvItems.Item_Name) , ProjectSkeleton.Qty, Dept.Dept_Name , Cata.Group_Name ,to_Char(ProjectSkeleton.DueDate), Unit.Unit_Name,ProjectSkeleton.Id,'' as hodname,0 as ORderNo,ProjectSkeleton.NetRate,ProjectSkeleton.NetValue,RawUser.UserName,"+
                              " ProjectSkeleton.DeptMrsAuth,ProjectSkeleton.ReadyForApproval,ProjectSkeleton.ApprovalStatus,ProjectSkeleton.StoresRejectionStatus,ProjectSkeleton.RejectionStatus,ProjectSkeleton.DocumentId,EntryItemName,ProjectSkeleton.ApprovalDateTime"+
                              " FROM (((ProjectSkeleton Left JOIN InvItems ON ProjectSkeleton.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON ProjectSkeleton.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON ProjectSkeleton.Group_Code=Cata.Group_Code) INNER JOIN Unit ON ProjectSkeleton.Unit_Code=Unit.Unit_Code"+
                              " INNER JOIN RAWUSER on RAWUSER.USERCODE=ProjectSkeleton.AuthUserCode"+
                              " Where  ProjectSkeleton.Qty >= 0 "+
                              " And ProjectSkeleton.MrsNo >= "+TStNo.getText()+" and ProjectSkeleton.MrsNo <="+TEnNo.getText()+" and ProjectSkeleton.MillCode="+iMillCode+
						" and Isdelete=0 and ItemDelete=0  ";

              if(JCPending.getSelectedIndex()==1)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 0 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0 ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==2)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0 ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==3)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=0 and RejectionStatus=0   " ;           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==4)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=1 and RejectionStatus=0   ";           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==5)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=0 and StoresRejectionStatus=1  " ;                           // and AuthUserCode="+iOwnerCode
              if(JCPending.getSelectedIndex()==6)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=1 and RejectionStatus=1 ";                                // and AuthUserCode="+iOwnerCode
              if(JCPending.getSelectedIndex()==0)
                   QString = QString+" and ProjectSkeleton.DeptMRsAuth=1  ";

               }
          }
          else
          {
               if(JRPeriod.isSelected())
               {

                    QString = " SELECT Mrs_Temp.MrsDate, Mrs_Temp.MrsNo, Mrs_Temp.Item_Code,decode(InvItems.Item_Name,null,Mrs_Temp.Item_Name,InvItems.Item_Name) , Mrs_Temp.Qty, Dept.Dept_Name , Cata.Group_Name , Mrs_Temp.DueDate, Unit.Unit_Name,Mrs_Temp.Id,hodname,Mrs_Temp.orderno,Mrs_temp.NetRate,Mrs_temp.NetValue,RawUser.UserName, "+
                              " Mrs_Temp.DeptMrsAuth,Mrs_Temp.ReadyForApproval,Mrs_temp.ApprovalStatus,Mrs_temp.StoresRejectionStatus,Mrs_Temp.RejectionStatus,Mrs_temp.DocumentId,EntryItemName,Mrs_Temp.ApprovalDateTime"+
                              " FROM (((MRS_Temp Left JOIN InvItems ON Mrs_Temp.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON Mrs_Temp.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON "+
                              " Mrs_Temp.Group_Code=Cata.Group_Code) INNER JOIN Unit ON Mrs_Temp.Unit_Code=Unit.Unit_Code "+
                              " INNER JOIN RAWUSER on RAWUSER.USERCODE=Mrs_Temp.AuthUserCode "+
                              " LEFT JOIN Hod ON Mrs_Temp.HODCode=HOD.HodCode Where Mrs_Temp.MrsFlag=1 and Mrs_Temp.Qty >= 0 And Mrs_Temp.MrsDate >= '"+TStDate.toNormal()+"' and Mrs_Temp.MrsDate <='"+TEnDate.toNormal()+"' and Mrs_Temp.MillCode ="+iMillCode+
						" and Isdelete=0 and ItemDelete=0  ";

              if(JCPending.getSelectedIndex()==1)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 0 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0 ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==2)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0 ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==3)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=0 and RejectionStatus=0   " ;           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==4)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=1 and RejectionStatus=0  ";           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==5)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=0 and StoresRejectionStatus=1 " ;                           // and AuthUserCode="+iOwnerCode
              if(JCPending.getSelectedIndex()==6)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=1 and RejectionStatus=1 ";                                // and AuthUserCode="+iOwnerCode
              if(JCPending.getSelectedIndex()==0)
                   QString = QString+" and Mrs_Temp.DeptMRsAuth=1  ";
                                             
                            QString=QString+  " Union All"+
                              " SELECT to_char(ProjectSkeleton.EntryDate), ProjectSkeleton.MrsNo, ProjectSkeleton.Item_Code, decode(InvItems.Item_Name,null,ProjectSkeleton.Item_Name,InvItems.Item_Name) , ProjectSkeleton.Qty, Dept.Dept_Name , Cata.Group_Name ,to_Char(ProjectSkeleton.DueDate), Unit.Unit_Name,ProjectSkeleton.Id,'' as hodname,0 as ORderNo,ProjectSkeleton.NetRate,ProjectSkeleton.NetValue,RawUser.UserName,"+
                              " ProjectSkeleton.DeptMrsAuth,ProjectSkeleton.ReadyForApproval,ProjectSkeleton.ApprovalStatus,ProjectSkeleton.StoresRejectionStatus,ProjectSkeleton.RejectionStatus,ProjectSkeleton.DocumentId,EntryItemName,ProjectSkeleton.ApprovalDateTime"+
                              " FROM (((ProjectSkeleton Left JOIN InvItems ON ProjectSkeleton.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON ProjectSkeleton.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON ProjectSkeleton.Group_Code=Cata.Group_Code) INNER JOIN Unit ON ProjectSkeleton.Unit_Code=Unit.Unit_Code"+
                              " INNER JOIN RAWUSER on RAWUSER.USERCODE=ProjectSkeleton.AuthUserCode"+
                              " Where  ProjectSkeleton.Qty >= 0 "+
                              " And ProjectSkeleton.EntryDate >= '"+TStDate.toNormal()+"' and ProjectSkeleton.EntryDate <='"+TEnDate.toNormal()+"' and ProjectSkeleton.MillCode="+iMillCode+
						" and Isdelete=0 and ItemDelete=0  ";

              if(JCPending.getSelectedIndex()==1)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 0 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0  ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==2)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0 ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==3)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=0 and RejectionStatus=0  " ;           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==4)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=1 and RejectionStatus=0   ";           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==5)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=0 and StoresRejectionStatus=1 " ;                           // and AuthUserCode="+iOwnerCode
              if(JCPending.getSelectedIndex()==6)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=1 and RejectionStatus=1 ";                                // and AuthUserCode="+iOwnerCode
              if(JCPending.getSelectedIndex()==0)
                   QString = QString+" and ProjectSkeleton.DeptMRsAuth=1 ";


               }
               else
               {

                    QString = " SELECT Mrs_Temp.MrsDate, Mrs_Temp.MrsNo, Mrs_Temp.Item_Code,decode(InvItems.Item_Name,null,Mrs_Temp.Item_Name,InvItems.Item_Name) , Mrs_Temp.Qty, Dept.Dept_Name , Cata.Group_Name , Mrs_Temp.DueDate, Unit.Unit_Name,Mrs_Temp.Id,hodname,Mrs_Temp.orderno,Mrs_temp.NetRate,Mrs_temp.NetValue,RawUser.UserName, "+
                              " Mrs_Temp.DeptMrsAuth,Mrs_Temp.ReadyForApproval,Mrs_temp.ApprovalStatus,Mrs_temp.StoresRejectionStatus,Mrs_Temp.RejectionStatus,Mrs_temp.DocumentId,EntryItemName,Mrs_Temp.ApprovalDateTime"+
                              " FROM (((MRS_Temp Left JOIN InvItems ON Mrs_Temp.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON Mrs_Temp.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON "+
                              " Mrs_Temp.Group_Code=Cata.Group_Code) INNER JOIN Unit ON Mrs_Temp.Unit_Code=Unit.Unit_Code "+
                              " INNER JOIN RAWUSER on RAWUSER.USERCODE=Mrs_Temp.AuthUserCode "+
                              " LEFT JOIN Hod ON Mrs_Temp.HODCode=HOD.HodCode Where Mrs_Temp.MrsFlag=1 and Mrs_Temp.Qty >= 0 And Mrs_Temp.MrsNo >="+TStNo.getText()+" and Mrs_Temp.MrsNo <="+TEnNo.getText()+" and Mrs_Temp.MillCode= "+iMillCode+
						" and Isdelete=0 and ItemDelete=0  ";

              if(JCPending.getSelectedIndex()==1)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 0 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0 ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==2)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0 ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==3)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=0 and RejectionStatus=0  ";           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==4)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=1 and RejectionStatus=0  ";           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==5)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=0 and StoresRejectionStatus=1 " ;                           // and AuthUserCode="+iOwnerCode
              if(JCPending.getSelectedIndex()==6)
                   QString = QString+" and Mrs_Temp.DeptMrsAuth = 1 and ReadyForApproval=1 and RejectionStatus=1 ";                                // and AuthUserCode="+iOwnerCode
          if(JCPending.getSelectedIndex()==0)
               QString = QString+" and Mrs_Temp.DeptMRsAuth=1  ";


                            QString=QString+  " Union All"+
                              " SELECT to_char(ProjectSkeleton.EntryDate), ProjectSkeleton.MrsNo, ProjectSkeleton.Item_Code, decode(InvItems.Item_Name,null,ProjectSkeleton.Item_Name,InvItems.Item_Name) , ProjectSkeleton.Qty, Dept.Dept_Name , Cata.Group_Name ,to_Char(ProjectSkeleton.DueDate), Unit.Unit_Name,ProjectSkeleton.Id,'' as hodname,0 as ORderNo,ProjectSkeleton.NetRate,ProjectSkeleton.NetValue,RawUser.UserName,"+
                              " ProjectSkeleton.DeptMrsAuth,ProjectSkeleton.ReadyForApproval,ProjectSkeleton.ApprovalStatus,ProjectSkeleton.StoresRejectionStatus,ProjectSkeleton.RejectionStatus,ProjectSkeleton.DocumentId,EntryItemName,ProjectSkeleton.ApprovalDateTime"+
                              " FROM (((ProjectSkeleton Left JOIN InvItems ON ProjectSkeleton.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON ProjectSkeleton.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON ProjectSkeleton.Group_Code=Cata.Group_Code) INNER JOIN Unit ON ProjectSkeleton.Unit_Code=Unit.Unit_Code"+
                              " INNER JOIN RAWUSER on RAWUSER.USERCODE=ProjectSkeleton.AuthUserCode"+
                              " Where  ProjectSkeleton.Qty >= 0 "+
                              " And ProjectSkeleton.MrsNo >= "+TStNo.getText()+" and ProjectSkeleton.MrsNo <="+TEnNo.getText()+" and ProjectSkeleton.MillCode="+iMillCode+
						" and Isdelete=0 and ItemDelete=0  ";

              if(JCPending.getSelectedIndex()==1)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 0 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0 ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==2)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=0 and ApprovalStatus=0 and StoresRejectionStatus=0 ";      // and AuthUserCode="+iOwnerCode+" 
              if(JCPending.getSelectedIndex()==3)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=0 and RejectionStatus=0   " ;           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==4)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=1 and ApprovalStatus=1 and RejectionStatus=0  ";           // and AuthUserCode="+iOwnerCode+"
              if(JCPending.getSelectedIndex()==5)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=0 and StoresRejectionStatus=1 " ;                           // and AuthUserCode="+iOwnerCode
              if(JCPending.getSelectedIndex()==6)
                   QString = QString+" and ProjectSkeleton.DeptMrsAuth = 1 and ReadyForApproval=1 and RejectionStatus=1 ";                                // and AuthUserCode="+iOwnerCode

              if(JCPending.getSelectedIndex()==0)
                   QString = QString+" and ProjectSkeleton.DeptMRsAuth=1  ";

               }
          }



          QString = QString+" Order By "+TOrder.getText()+"";   //,Mrs_Temp.Id




          return QString;
     }
     private String getStatus(int i)
     {
          String SStatus  ="";

          int iDeptMrsAuth = common.toInt((String)VDeptMrsAuth.elementAt(i));
          int iReadyForApp = common.toInt((String)VReadyForApproval.elementAt(i));
          int iApproval    = common.toInt((String)VApprovalStatus.elementAt(i));
          int iStoreReject = common.toInt((String)VStoresRejectionStatus.elementAt(i));
          int iReject      = common.toInt((String)VRejectionStatus.elementAt(i));
          int iDocId       = common.toInt((String)VDocId.elementAt(i));



          if(iDeptMrsAuth==0 &&  iReadyForApp==0 &&  iApproval==0 && iStoreReject==0)
               SStatus = "Pending With HOD Authentication";

          if(iDeptMrsAuth==1 &&  iReadyForApp==0 &&  iApproval==0 && iStoreReject==0)
               SStatus =  " Pending With Stores Authentication";   

          if(iDeptMrsAuth==1 &&  iReadyForApp==1 &&  iApproval==0 && iReject==0)
               SStatus =  " Pending With JMD";                     

          if(iDeptMrsAuth==1 &&  iReadyForApp==1 &&  iApproval==1 && iReject==0)
               SStatus = "Converted";

          if(iDeptMrsAuth==1 &&  iReadyForApp==0  && iStoreReject==1)
               SStatus = " Rejected Item By Stores";

          if(iDeptMrsAuth==1 &&  iReadyForApp==1 &&  iReject==1)
               SStatus = " Rejected Item By Jmd";


          return SStatus;

     }

     private String getHodName(String SMrsNo)
     {
          int iIndex=-1;

          iIndex = VMrsNo.indexOf(SMrsNo);
          if(iIndex!=-1)
               return common.parseNull((String)VHodName.elementAt(iIndex));
          else
               return "";
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
     public void setOwnerCode()
     {
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               ResultSet      res            =  stat.executeQuery(" select distinct AuthUserCode from MrsUserAuthentication where UserCode="+iUserCode);

               if(res.next())
                iOwnerCode = res.getInt(1);

               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

}
