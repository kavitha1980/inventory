package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import javax.swing.table.*;


import util.*;
import guiutil.*;
import jdbc.*;

public class MRSDeletionListFrame extends JInternalFrame
{
     JPanel    TopPanel;
     JPanel    BottomPanel;
     JPanel    First,Second,Third,Fourth,Fifth;
	 JPanel    Left,Right;
     
     JButton   BApply,BOk,BExit,BDetails;

     JComboBox      JCFilter;
     JTextField     TOrder;
     JRadioButton   JRPeriod,JRNo;

     DateField TStDate,TEnDate;
     NextField TStNo,TEnNo;

     Vector VCCode,VCName;
     
     Object RowData[][];
     String ColumnData[] = {"MRS No","Date","Code","Name","Qty","Department","Group","Unit","OrderNo","Block","Month","Status"};
     String ColumnType[] = {"N"     ,"S"   ,"S"   ,"S"   ,"N"  ,"S"         ,"S"    ,"S"   ,"N"      ,"S"    ,"E","B"};
	

     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;
     int iUserCode,iMillCode,iAuthCode;

     Common common = new Common();
	 String SPatch;
     
     Vector VMRSDate,VBlock,VBlockCode,VOrderNo,VMRSNo,VDeptName,VOrdQty,VOrdStatus,VSlNo,VGroupName,VUnitName,VMrsUserCode;
     
     AlterTabReport tabreport;
	 JComboBox JCMonth;
	 Vector VMonth,VMonthCode;
	 JTable theTable;
     
     public MRSDeletionListFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode)
     {
          try
          {
               this.DeskTop   = DeskTop;
               this.VCode     = VCode;
               this.VName     = VName;
               this.SPanel    = SPanel;
               this.iUserCode = iUserCode;
               this.iMillCode = iMillCode;
               this.iAuthCode = iAuthCode;
			 
              
			   createComponents();
               setLayouts();
               addComponents();
               addListeners();
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }

     }

     public void createComponents()
     {
          try
          {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          First       = new JPanel();
          Second      = new JPanel();
          Third       = new JPanel();
          Fourth      = new JPanel();
          Fifth       = new JPanel();
		  Left		  = new JPanel();
		  Right 	  = new JPanel();

          TStDate     = new DateField();
          TEnDate     = new DateField();

          TOrder      = new JTextField("1,2,3");
          JCFilter    = new JComboBox();
          TStNo       = new NextField(0);
          TEnNo       = new NextField(0);
     
          JRPeriod    = new JRadioButton("Periodical",true);
          JRNo        = new JRadioButton("MRS No",false);
     
          TStDate.setTodayDate();
          TEnDate.setTodayDate();
		  
		  

          BOk            = new JButton("Delete");
          BExit          = new JButton("Exit");
          BApply         = new JButton("Apply");
		  BDetails		 = new JButton("Details");
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void setLayouts()
     {
          try
          {
          TopPanel  .setLayout(new GridLayout(1,5));
          First     .setLayout(new GridLayout(1,1));
          Second    .setLayout(new GridLayout(1,1));
          Third     .setLayout(new GridLayout(2,1));
          Fourth    .setLayout(new GridLayout(2,1));
          Fifth     .setLayout(new GridLayout(1,1));
		  Left      .setLayout(new FlowLayout());
		  Right	    .setLayout(new FlowLayout());
          BottomPanel.setLayout(new GridLayout(1,2));

          setTitle("MRS Deletion Frame");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     public void addComponents()
     {
          try
          {
          JCFilter.addItem("All");
          JCFilter.addItem("Over-Due");
          JCFilter.addItem("Order Not Placed");
          
          First.setBorder(new TitledBorder("Sorted On (Column No)"));
          First.add(TOrder);
          
          Second.setBorder(new TitledBorder("Filter"));
          Second.add(JCFilter);
          
          Third.setBorder(new TitledBorder("Basis"));
          Third.add(JRPeriod);
          Third.add(JRNo);
          
          Fourth.setBorder(new TitledBorder("Periodical"));
          Fourth.add(TStDate);
          Fourth.add(TEnDate);
          
          Fifth.add(BApply);
          
          TopPanel.add(First);
          TopPanel.add(Second);
          TopPanel.add(Third);
          TopPanel.add(Fourth);
          TopPanel.add(Fifth);

          
		  Left.setBorder(new TitledBorder("Controls"));
		  Left.add(BOk);
		  Left.add(BExit);
		  
		  Right.setBorder(new TitledBorder("Info"));
		  Right.add(BDetails);
		  
		 
		  BottomPanel.add(Left);
          BottomPanel.add(Right);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }

     }
     
     public void addListeners()
     {
          try
          {
               JRPeriod.addActionListener(new JRList());
               JRNo.addActionListener(new JRList()); 
               BApply.addActionListener(new ApplyList());
               BExit.addActionListener(new ApplyList());
               BOk.addActionListener(new ApplyList());
			   BDetails.addActionListener(new ApplyList());
				
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    Fourth.setBorder(new TitledBorder("Periodical"));
                    Fourth.removeAll();
                    Fourth.add(TStDate);
                    Fourth.add(TEnDate);
                    Fourth.updateUI();
                    JRNo.setSelected(false);
               }
               else
               {
                    Fourth.setBorder(new TitledBorder("Numbered"));
                    Fourth.removeAll();
                    Fourth.add(TStNo);
                    Fourth.add(TEnNo);
                    Fourth.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
               setDataIntoVector();
			   setMonths();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                     
					JCMonth = new JComboBox(VMonth);
					tabreport = new AlterTabReport(RowData,ColumnData,ColumnType,VOrdStatus);
					tabreport      . ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
					TableColumn    MonthColumn    = tabreport.ReportTable.getColumn("Month");  
                                   MonthColumn    . setCellEditor(new DefaultCellEditor(JCMonth));
					 

                    getContentPane().add(tabreport,BorderLayout.CENTER);
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
               }
               if(ae.getSource()==BExit)
               {
					 
				    removeHelpFrame();
               }
			    if(ae.getSource()==BDetails)
               {
					 
				   MRSDeleteDetails mrsdelete = new MRSDeleteDetails();
               }
               if(ae.getSource()==BOk)
               {
                    
					for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
                    {
                         //Boolean bFlag     = (Boolean)tabreport.ReportTable.getValueAt(i,10);
						  Boolean bFlag     = (Boolean)tabreport.ReportTable.getValueAt(i,11);
						  

                         if(bFlag.booleanValue())
                         {
                              String SMRSNo       = common.parseNull((String)VMRSNo.elementAt(i));
                              String SItemCode    = common.parseNull((String)VCCode.elementAt(i));
                              String SItemName    = common.parseNull((String)VCName.elementAt(i));
                              String SMrsUserCode = common.parseNull((String)VMrsUserCode.elementAt(i));
                              int iSlNo           = common.toInt((String)VSlNo.elementAt(i));
							  //int iMonthCode   = common.toInt(getMonthCode((String)JCMonth.getSelectedItem()));
							 String SMonthCode   = getMonthCode((String)JCMonth.getSelectedItem());

                              DeleteData(SMRSNo,SItemCode,iSlNo,SItemName,SMrsUserCode,SMonthCode);
							 
                         }
                    }
                    removeHelpFrame();
               }
          }
     }

     public void setDataIntoVector()
     {
          VMRSDate       = new Vector();
          VMRSNo         = new Vector();
          VBlock         = new Vector();
          VCCode         = new Vector();
          VCName         = new Vector();
          VOrdQty        = new Vector();
          VBlockCode     = new Vector();
          VSlNo          = new Vector();
          VOrdStatus     = new Vector();
          VOrderNo       = new Vector();
          VDeptName      = new Vector();
          VGroupName     = new Vector();
          VUnitName      = new Vector();
          VMrsUserCode   = new Vector();


          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(getQString());

               while (res.next())
               {
                    VMRSNo         .addElement(res.getString(1));
                    VMRSDate       .addElement(res.getString(2));
                    VCCode         .addElement(res.getString(3));
                    VCName         .addElement(res.getString(4));     
                    VOrdQty        .addElement(res.getString(5));
                    VDeptName      .addElement(res.getString(6));
                    VGroupName     .addElement(res.getString(7));
                    VUnitName      .addElement(res.getString(8));
                    VOrderNo       .addElement(res.getString(9));
                    VBlock         .addElement(res.getString(10));
                    VBlockCode     .addElement(res.getString(11));
                    VSlNo          .addElement(res.getString(12));
                    VOrdStatus     .addElement(res.getString(13));
                    VMrsUserCode   .addElement(res.getString(14));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void DeleteData(String SMRSNo,String SItemCode,int iSlNo,String SItemName,String SMrsUserCode,String SMonthCode)
     {
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ORAConnection3 oraConnection3 =  ORAConnection3.getORAConnection();
               Connection     theConnection3 =  oraConnection3.getConnection();               
               Statement      stat2          =  theConnection3.createStatement();


               String SDateTime = common.getServerDate();
               String SDate     = common.getServerPureDate();

               int iCount=0;

               String SMsgStatus = "STATUS227";
               String SMsgAlert  = "ALERT1";

               String SMailStatus = "STATUS228";
               String SMailAlert  = "ALERT2";

               String QS1 = " Select Count(*) from Tbl_Alert_Sender_Master Where EventId='"+SMsgStatus+"' and AlertId='"+SMsgAlert+"' and UserCode='"+SMrsUserCode+"'";

               ResultSet res  = stat2.executeQuery(QS1);

               while (res.next())
               {
                    iCount = res.getInt(1);
               }
               res.close();

               if(iCount<=0)
               {
                    int iAlertId = 0;
                    String SCustCode = "2127";

                    String QS2 = " Select Max(Id+1) from Tbl_Alert_Sender_Master ";
     
                    res  = stat2.executeQuery(QS2);
     
                    while (res.next())
                    {
                         iAlertId = res.getInt(1);
                    }
                    res.close();

                    String QS3 = " Insert Into Tbl_Alert_Sender_Master (Id,CustomerCode,UserCode,EventId,AlertId) "+
                                 " Values ("+iAlertId+",'"+SCustCode+"','"+SMrsUserCode+"','"+SMsgStatus+"','"+SMsgAlert+"')";


                    stat2.executeUpdate(QS3);
               }

               String QS = " Insert into DeletedMRS (DELETEDDATE,DELETEDTIME,ID,MRSNO,MRSDATE,ITEM_CODE,QTY,UNIT_CODE,DEPT_CODE,GROUP_CODE,CATL,DRAW,MAKE,DUEDATE,ORDERNO,"+
                           " ORDERBLOCK,GRNNO,ENQUIRYNO,HODCODE,REFNO,NATURECODE,BLOCKCODE,URGORD,POSTED,SLNO,PYPOST,WA,DM,"+
                           " PLANMONTH,MILLCODE,REMARKS,MAKE_CODE,SERV_PLAN_NO,SERV_CODE,MAC_CODE,ISSUENO,STATUS,FLAG,USERCODE,"+
                           " CREATIONDATE,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE,SLIPFROMNO,SLIPTONO,MRSFLAG,MRSTYPECODE,"+
                           " BUDGETSTATUS,NETRATE,NETVALUE,DEPTMRSAUTH,ITEMNAME,MRSAUTHUSERCODE,YEARLYPLANNINGSTATUS,YEARLYPLANNINGMONTH,"+
                           " DOCUMENTID,PROJECTMRSTYPECODE,SYSTEMNAME,ENTRYDATETIME,REASONFORMRS,MRSREASONCODE,AUTOORDERCONVERSION) "+
                           " (Select "+SDate+" as DeletedDate,"+
                           "'"+SDateTime+"' as DeletedTime,"+
                           " ID,MRSNO,MRSDATE,ITEM_CODE,QTY,UNIT_CODE,DEPT_CODE,GROUP_CODE,CATL,DRAW,MAKE,DUEDATE,ORDERNO,"+
                           " ORDERBLOCK,GRNNO,ENQUIRYNO,HODCODE,REFNO,NATURECODE,BLOCKCODE,URGORD,POSTED,SLNO,PYPOST,WA,DM,"+
                           " PLANMONTH,MILLCODE,REMARKS,MAKE_CODE,SERV_PLAN_NO,SERV_CODE,MAC_CODE,ISSUENO,STATUS,FLAG,USERCODE,"+
                           " CREATIONDATE,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE,SLIPFROMNO,SLIPTONO,MRSFLAG,MRSTYPECODE,"+
                           " BUDGETSTATUS,NETRATE,NETVALUE,DEPTMRSAUTH,ITEMNAME,MRSAUTHUSERCODE,YEARLYPLANNINGSTATUS,YEARLYPLANNINGMONTH,"+
                           " DOCUMENTID,PROJECTMRSTYPECODE,SYSTEMNAME,ENTRYDATETIME,REASONFORMRS,MRSREASONCODE,AUTOORDERCONVERSION "+
                           " from MRS Where MRSNo='"+SMRSNo+"' and Item_Code='"+SItemCode+"' and SlNo="+iSlNo+" and MillCode ="+iMillCode+" ) ";


               stat.executeUpdate(QS);

               String QString  ="Delete from MRS Where MRSNo='"+SMRSNo+"' and Item_Code='"+SItemCode+"' and SlNo="+iSlNo+" and MillCode="+iMillCode+" ";
              
			  String QSUpdate = " Update DeletedMrs set DeletedMonth="+SMonthCode+" Where MRSNo='"+SMRSNo+"' and Item_Code='"+SItemCode+"' and SlNo="+iSlNo+" and MillCode="+iMillCode+" ";
			  
			  String QString1 ="Update Mrs_Temp set ConvertToOrder=0,ItemDelete=1,DeletedMonth="+SMonthCode+" Where MRSNo='"+SMRSNo+"' and Item_Code='"+SItemCode+"' and SlNo="+iSlNo+" and MillCode="+iMillCode+" ";

               String SMessage = SMRSNo+" - Item : "+SItemName+" has been Deleted ";

               String QS4 = " Insert Into Tbl_Alert_Collection (CollectionId,StatusId,AlertMessage,CustomerCode,Status) "+
                            " Values (AlertCollection_Seq.nextval,'"+SMsgStatus+"','"+SMessage+"','"+SMrsUserCode+"',0) ";

               String QS5 = " Insert Into Tbl_Alert_Recipient_Collection (CollectionId,StatusId,AlertMessage,CustomerCode,Status,Type) "+
                            " Values (AlertRecipientCollection_Seq.nextval,'"+SMailStatus+"','"+SMessage+"','"+SMrsUserCode+"',0,1) ";

               stat.executeUpdate(QString);
			   stat.executeUpdate(QSUpdate);
               stat.executeUpdate(QString1);
               stat2.executeUpdate(QS4);
               stat2.executeUpdate(QS5);
               stat.close();
               stat2.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          try
          {
         // RowData     = new Object[VMRSDate.size()][11];
		  RowData     = new Object[VMRSDate.size()][12];
          for(int i=0;i<VMRSDate.size();i++)
          {
               RowData[i][0]  = common.parseNull((String)VMRSNo         .elementAt(i));
               RowData[i][1]  = common.parseDate((String)VMRSDate       .elementAt(i));
               RowData[i][2]  = common.parseNull((String)VCCode         .elementAt(i));
               RowData[i][3]  = common.parseNull((String)VCName         .elementAt(i));
               RowData[i][4]  = common.parseNull((String)VOrdQty        .elementAt(i));
               RowData[i][5]  = common.parseNull((String)VDeptName      .elementAt(i));
               RowData[i][6]  = common.parseNull((String)VGroupName     .elementAt(i));
               RowData[i][7]  = common.parseNull((String)VUnitName      .elementAt(i));
               RowData[i][8]  = common.parseNull((String)VOrderNo       .elementAt(i));
               RowData[i][9]  = common.parseNull((String)VBlock         .elementAt(i));
			   RowData[i][11] = new Boolean(false);
          }
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
      public void removeHelpFrame()
      {
            try
            {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
            }
            catch(Exception ex) { }
      }

     public String getQString()
     {
          DateField df = new DateField();
          df.setTodayDate();
          String SToday = df.toNormal();
          String QString = "";
          
          if(JRPeriod.isSelected())
          {
               QString = " Select MRSNO,MRSDate,MRS.Item_Code,Item_Name,MRS.Qty,Dept_Name,Group_Name,Unit_Name,MRS.OrderNo,decode(Mrs.OrderNo,0,'',BlockName) as Block,OrderBlock,MRS.SlNo,decode(MRS.OrderNo,0,0,MRS.OrderNo,1),Mrs.MrsAuthUserCode From MRS "+
                         " Left join OrdBlock On OrdBlock.Block=Mrs.OrderBlock "+
                         " Inner join Dept ON Dept.Dept_Code=MRS.Dept_Code "+
                         " Inner join Cata ON Cata.Group_Code=MRS.Group_Code "+
                         " Inner join Unit ON Unit.Unit_Code=MRS.Unit_Code "+
                         " Inner join InvItems On InvItems.Item_Code=MRS.Item_Code "+
                         " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and Mrs.Qty > 0 "+
                         " And Mrs.MrsDate >= '"+TStDate.toNormal()+"' and Mrs.MrsDate <='"+TEnDate.toNormal()+"'"+
                         " and Mrs.MillCode="+iMillCode;
          }
          else
          {
               QString = " Select MRSNO,MRSDate,MRS.Item_Code,Item_Name,MRS.Qty,Dept_Name,Group_Name,Unit_Name,MRS.OrderNo,decode(Mrs.OrderNo,0,'',BlockName) as Block,OrderBlock,MRS.SlNo,decode(MRS.OrderNo,0,0,MRS.OrderNo,1),Mrs.MrsAuthUserCode From MRS "+
                         " Left join OrdBlock On OrdBlock.Block=Mrs.OrderBlock "+
                         " Inner join Dept ON Dept.Dept_Code=MRS.Dept_Code "+
                         " Inner join Cata ON Cata.Group_Code=MRS.Group_Code "+
                         " Inner join Unit ON Unit.Unit_Code=MRS.Unit_Code "+
                         " Inner join InvItems On InvItems.Item_Code=MRS.Item_Code "+
                         " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and Mrs.Qty > 0 "+
                         " And Mrs.MrsNo >="+TStNo.getText()+" and Mrs.MrsNo <="+TEnNo.getText()+
                         " and Mrs.MillCode="+iMillCode;
          }
          
          if(JCFilter.getSelectedIndex()==1)
               QString = QString+" and Mrs.DueDate <= '"+SToday+"'";
          if(JCFilter.getSelectedIndex()==2)
               QString = QString+" and Mrs.OrderNo = 0";
          
          QString = QString+" Order By "+TOrder.getText()+",MRS.Id";

          return QString;
     }
	 
 private void setMonths()
 {
          VMonth    = new Vector();
          VMonthCode= new Vector();

		  VMonth.add("None");
		  VMonthCode.add("");
		  
          String SDate     = "20200601";
          String SMonth    = SDate.substring(0,6);

          for(int i=0; i<7; i++)
          {
               String SMonthName = common.getMonthName(common.toInt(SMonth));

               VMonthCode.addElement(SMonth);
               VMonth.addElement(SMonthName);

			 SMonth = String.valueOf(common.getNextMonth(common.toInt(SMonth)));
			 
          }
		  
 }
 private String getMonthCode(String SMonth)
 {
          System.out.println("Enter method");
		  int iIndex = VMonth.indexOf(SMonth);
		  System.out.println("iIndex-->"+iIndex);
          if(iIndex==-1)
		  {
			return "";
		  }
          return   (String)VMonthCode.elementAt(iIndex);
 }


}
