/*
     MRS Frame
*/
package MRS;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;
import java.io.*;

public class MRSFrame extends JInternalFrame 
{
     protected     JLayeredPane Layer;

     Connection    theConnection=null;

     JPanel        TopPanel,MiddlePanel,BottomPanel;
     JTabbedPane   thePane;
     TabReport     theReport;
     MyButton      BOkay,BApply;
     JButton       BExit;
     Common        common = new Common();
     int           iUserCode=0;
     int           iCount=0;

     DateField     TFromDt;
     JTextField    TMRSNo;
     JComboBox     JCDept,JCDepartment,JCUnit,JCGroup,JCBlock,JCNature,JCUrgent;
     boolean       bFlag=false;

     MRSModel                      theModel;

     JTable                        theTable;
     int                           iMillCode=0;
     MRSRecordFrame                mrsrecordframe;
     MRSStationaryPropertiesFrame  mrsStationaryPropertiesFrame;

     String    SMrsNo        = "";
     String    SMrsDate      = "";
     String    SName         = "";
     String    SMrsFlag      = "";
     String    SMrsTypeCode  = "";
     String    SBudgetStatus = "";

     int       iList     = 0;

     boolean bUpdateFlag=true;

     Vector VUDeptName,VUDeptCode,VDeptName,VDeptCode,VUnitName,VUnitCode,VGroupName,VGroupCode,VBlockName,VBlockCode,VUrgentName,VUrgentCode,VNatureName,VNatureCode;
     Vector VCode,VName,VNameCode,VCata,VDraw;
     Vector VECode,VEName,VEQty,VEUnit,VEDept,VEGroup,VEBlock,VEUrgent,VENature,VEDueDate,VECatl,VEDraw,VEMake,VERemarks,VEId,VEOrderNo,VESlipFrNo,VESlipToNo;

     Vector VPaperColor,VPaperSets,VPaperSize,VPaperSide,VItemCode;

     Vector VSItemCode,VSSlipFrNo,VSSlipToNo,VSlipItemCode,VSlipNo;
     String SItemTable;

     boolean   bComFlag = true,bCCheck = false;

     int iMaxSlNo=0;


     public  MRSFrame(JLayeredPane Layer,int iUserCode,int iMillCode,Vector VCode,Vector VName,Vector VNameCode,Vector VCata,Vector VDraw,Vector VPaperColor,Vector VPaperSets,Vector VPaperSize,Vector VPaperSide,Vector VSlipNo,String SItemTable)
     {
          this.Layer          = Layer;
          this.iUserCode      = iUserCode;
          this.iMillCode      = iMillCode;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.VCata          = VCata;
          this.VDraw          = VDraw;

          this.VPaperColor    = VPaperColor;
          this.VPaperSets     = VPaperSets;
          this.VPaperSize     = VPaperSize;
          this.VPaperSide     = VPaperSide;
          this.VSlipNo        = VSlipNo;
          this.SItemTable     = SItemTable;

          VItemCode      = VCode;
          VSlipItemCode  = VCode;

          SMrsFlag      = "0";
          SMrsTypeCode  = "0";
          SBudgetStatus = "0";

          setVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          JCDept.setSelectedItem("GENERAL");
     }

     public  MRSFrame(JLayeredPane Layer,int iUserCode,int iMillCode,String SMrsNo,String SName,String SMrsDate,String SMrsFlag,String SMrsTypeCode,String SBudgetStatus,Vector VCode,Vector VName,Vector VNameCode,Vector VCata,Vector VDraw,Vector VPaperColor,Vector VPaperSets,Vector VPaperSize,Vector VPaperSide,Vector VSlipNo,String SItemTable)
     {
          iList              = 1;
          this.Layer         = Layer;
          this.iUserCode     = iUserCode;
          this.iMillCode     = iMillCode;
          this.SMrsNo        = SMrsNo;
          this.SName         = SName;
          this.SMrsDate      = SMrsDate;
          this.SMrsFlag      = SMrsFlag;
          this.SMrsTypeCode  = SMrsTypeCode;
          this.SBudgetStatus = SBudgetStatus;
          this.VCode         = VCode;
          this.VName         = VName;
          this.VNameCode     = VNameCode;
          this.VCata         = VCata;
          this.VDraw         = VDraw;

          this.VPaperColor    = VPaperColor;
          this.VPaperSets     = VPaperSets;
          this.VPaperSize     = VPaperSize;
          this.VPaperSide     = VPaperSide;
          this.VSlipNo        = VSlipNo;
          this.SItemTable     = SItemTable;

          VItemCode      = VCode;
          VSlipItemCode  = VCode;

          setVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setExistVector();
          setExistData();

          JCDept    .setSelectedItem(SName);
          TMRSNo    .setText(SMrsNo);
          TFromDt   .fromString1(SMrsDate);

          TMRSNo    .setEditable(false);
          TFromDt   .setEditable(false);
          JCDept    .setEnabled(false);
          Runtime   .getRuntime().freeMemory();
     }

     private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               thePane        = new JTabbedPane();

               TFromDt        = new DateField();
               TFromDt.setTodayDate();

               TMRSNo         = new JTextField();
               JCDept         = new JComboBox(VUDeptName);
               JCDepartment   = new JComboBox(VDeptName);
               JCUnit         = new JComboBox(VUnitName);
               JCBlock        = new JComboBox(VBlockName);
               JCGroup        = new JComboBox(VGroupName);
               JCNature       = new JComboBox(VNatureName);

               VUrgentName = new Vector();
               VUrgentCode = new Vector();

               VUrgentName.addElement("Ordinary");
               VUrgentName.addElement("Urgent");
               VUrgentCode.addElement("0");
               VUrgentCode.addElement("1");
               JCUrgent       = new JComboBox(VUrgentName);

               if(iList==1)
                    theModel       = new MRSModel(1);
               else
                    theModel       = new MRSModel(0);

               theTable       = new JTable(theModel);
     
               BOkay          = new MyButton("Save");
               BExit          = new JButton("Exit");
               BApply         = new MyButton("Apply");

               VSItemCode     = new Vector();
               VSSlipFrNo     = new Vector();
               VSSlipToNo     = new Vector();
          }
          catch(Exception e)
          {
            System.out.println(e);
          }
     }

     private void setLayouts()
     {
          setTitle("MRS Frame ");
          setClosable(true);                    
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,750,500);

          TopPanel.setLayout(new GridLayout(3,1));

          MiddlePanel.setLayout(new BorderLayout());
          TopPanel.setBorder(new TitledBorder(""));
          BottomPanel.setLayout(new FlowLayout());

          MiddlePanel.setBorder(new TitledBorder(""));
     }

     private void addComponents()
     {
          TopPanel.add(new JLabel("Department"));
          TopPanel.add(JCDept);
          TopPanel.add(new JLabel("Date"));
          TopPanel.add(TFromDt);
          TopPanel.add(new JLabel("MRS No"));
          TopPanel.add(TMRSNo);

          thePane.addTab("MRS Details ",theReport);

          theTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
          MiddlePanel.add(new JScrollPane(theTable));

          BottomPanel.add(BOkay);
          BottomPanel.add(BExit);
          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
          BOkay.setMnemonic('S');
          BExit.setMnemonic('E');
     }

     private void addListeners()
     {
          BOkay.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
          BApply.addActionListener(new ActList());

          if(iList!=1)
          {
               theTable.getTableHeader().addMouseListener(new MouseMaterialsSearch(Layer,VCode,VName,VNameCode,theModel,iMillCode,VCata,VDraw,this));
          }

          theTable.addKeyListener(new KeyList(this));
     

          TableColumn deptColumn   = theTable.getColumn("Department");
          deptColumn  .setCellEditor(new DefaultCellEditor(JCDepartment));
          TableColumn unitColumn   = theTable.getColumn("Unit");
          unitColumn  .setCellEditor(new DefaultCellEditor(JCUnit));
          TableColumn groupColumn   = theTable.getColumn("Group");
          groupColumn  .setCellEditor(new DefaultCellEditor(JCGroup));
          TableColumn blockColumn   = theTable.getColumn("Block");
          blockColumn  .setCellEditor(new DefaultCellEditor(JCBlock));
          TableColumn urgentColumn   = theTable.getColumn("Urgent");
          urgentColumn  .setCellEditor(new DefaultCellEditor(JCUrgent));
          TableColumn natureColumn   = theTable.getColumn("Nature");
          natureColumn  .setCellEditor(new DefaultCellEditor(JCNature));

     }                 

     private class KeyList extends KeyAdapter
     {
          MRSFrame mrsframe;

          public KeyList(MRSFrame mrsframe)
          {
               this.mrsframe = mrsframe;
          }

          public void keyReleased(KeyEvent ke)
          {
               try
               {
                    int iRow = theTable.getSelectedRow();
                    int iCol = theTable.getSelectedColumn();
     
                    setData();
     
                    Vector VMac  = new Vector();
                    Vector VServ = new Vector();
                    VMac.addElement("NONE");
                    VServ.addElement("NONE");
     
                    if(ke.getKeyCode()==KeyEvent.VK_F3)
                    {
                         try
                         {
                              int iOrderNo =0;
                              if(iList==1)
                              {
                                   if(iRow<VEOrderNo.size())
                                   {
                                        iOrderNo = common.toInt((String)VEOrderNo.elementAt(iRow));
                                   }
                              }
                              if(iOrderNo==0)
                              {
                                   mrsrecordframe = new MRSRecordFrame(Layer,VCode,VName,VNameCode,iRow,VDeptName,VGroupName,VUnitName,VServ,VMac,VBlockName,VNatureName,VUrgentName,TFromDt,iMillCode,mrsframe,SItemTable);
                                   Layer.add(mrsrecordframe);
                                   Layer.repaint();
                                   mrsrecordframe.setSelected(true);
                                   Layer.updateUI();
                                   mrsrecordframe.show();
                                   setRecordData(iRow);
                                   mrsrecordframe.BOk.addActionListener(new ActList1(iRow));
                                   mrsrecordframe.BCancel.addActionListener(new ActList1(iRow));
                              }
                              else
                              {
                                   JOptionPane.showMessageDialog(null,"Order is Entered U Can't Changed","Dear User",JOptionPane.INFORMATION_MESSAGE);
                              }
                         }
                         catch(Exception e)
                         {
                              System.out.println(e);
                         }
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_INSERT)
                    {
                         theModel.appendEmptyRow(theModel.getRows());
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         if(common.parseNull((String)theModel.getValueAt(iRow,1)).equals(""))
                              theModel.deleteRow(theModel.getRows()-1);
                         else
                              JOptionPane.showMessageDialog(null,getDeleteMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_F2)
                    {

                         String SItemCode = common.parseNull((String)theModel.getValueAt(iRow,1));
                         String SItemName = common.parseNull((String)theModel.getValueAt(iRow,2));
                         if(isStationary(SItemCode))
                         {
                              String SSlipFrNo="";
                              String SSlipToNo="";
                              if(iList==1)
                              {
                                   SSlipFrNo   = common.parseNull(getExistSlipFrNo(SItemCode));
                                   SSlipToNo   = common.parseNull(getExistSlipToNo(SItemCode)); 
                              }
                              else
                              {
                                   SSlipFrNo = getSlipNo(SItemCode);
                                   SSlipToNo = getSlipNo(SItemCode);
                              }
                              mrsStationaryPropertiesFrame = new MRSStationaryPropertiesFrame(Layer,SItemName,SItemCode,SSlipFrNo,SSlipToNo,theTable,VSItemCode,VSSlipFrNo,VSSlipToNo);
                              Layer.add(mrsStationaryPropertiesFrame);
                              Layer.repaint();
                              mrsStationaryPropertiesFrame.setSelected(true);
                              Layer.updateUI();
                              mrsStationaryPropertiesFrame.show();
                         }
                    }
               }
               catch(Exception e)
               {
                    System.out.println(e);
                    e.printStackTrace();
               }

          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();

               }
               if(ae.getSource()==BOkay)               
               {
                    if(iList==1)
                    {
                         bCCheck = true;
                         updateMRS();
                         insertMRS();
                    try
                    {
                         if(bComFlag)
                         {
                              theConnection  . commit();
                              theConnection  . setAutoCommit(true);
                         }
                         else
                         {
                              theConnection  . rollback();
                              theConnection  . setAutoCommit(true);
                         }
                    }catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }

                    }
                    else
                    {
                         if(isValidData())
                         {
                              insertMRS();
                         }
                    }    
                    if(bFlag)
                    {
                         if(iList==1)
                         {
                              if(bUpdateFlag)
                                   removeHelpFrame();
                         }
                         else
                         {
                              JOptionPane.showMessageDialog(null,getSaveMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
                              Runtime.getRuntime().gc();
     
                              VCode     = null;
                              VName     = null;
                              VNameCode = null;
     
                              VUnitName = null;
                              VUnitCode = null;
                              VUDeptName= null;
                              VUDeptCode= null;
                              VDeptName = null;
                              VDeptCode = null;
                              VGroupName= null;
                              VGroupCode= null;
                              VBlockName= null;
                              VBlockCode= null;
                              VNatureName=null;
                              VNatureCode=null;
                              VItemCode  =null;
                              VPaperColor=null;
                              VPaperSets =null;
                              VPaperSize =null;
                              VPaperSide =null;
                              VSlipItemCode  =null;
                              VSlipNo        =null;
     
                              removeHelpFrame();
                         }
                    }


               }

          }
     }
     private class ActList1 implements ActionListener
     {
          int iRow=0;
          public ActList1(int iRow)
          {
               this.iRow = iRow;
          }
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==mrsrecordframe.BOk)               
               {
                    setModifyData(iRow);
                    mrsrecordframe.removeHelpFrame();
               }
               if(ae.getSource()==mrsrecordframe.BCancel)               
               {
                    mrsrecordframe.removeHelpFrame();
               }


          }
     }

     private void showMessage()
     {
          JOptionPane.showMessageDialog(null,getSaveMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getSaveMessage()
     {
          String str = "<html><body>";
          str = str + "Ur Data is Successfully Saved<br>";
          str = str + "</body></html>";
          return str;
     }

     private String getDeleteMessage()
     {
          String str = "<html><body>";
          str = str + "U Have Item is Available <br>";
          str = str + "Dont Delete<br>";          
          str = str + "</body></html>";
          return str;
     }


     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public void setVector()
     {
          VUnitName = new Vector();  
          VUnitCode = new Vector();  
          VUDeptName = new Vector();
          VUDeptCode = new Vector();
          VDeptName = new Vector();
          VDeptCode = new Vector();
          VGroupName= new Vector();
          VGroupCode= new Vector();
          VBlockName= new Vector();
          VBlockCode= new Vector();
          VNatureName=new Vector();
          VNatureCode=new Vector();

          String QS1="";
          String QS2= "";
          String QS3= "";
          String QS4= "Select BlockName,Block from OrdBlock Order By 2 ";
          String QS5= "Select Nature,NatureCode from Nature Order By 1 ";
          String QS6="";

          QS1 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";
          QS2 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
          QS3 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By Group_Name";


          if(iMillCode==0)
               QS6 = " Select Item_Name,Item_Code,UomName,Catl,Draw From InvItems "+
                     " Inner join Uom On Uom.UomCode=InvItems.UomCode "+
                     " Order By Item_Name";
          else
               QS6 = " Select InvItems.Item_Name,"+SItemTable+".Item_Code, "+
                     " UomName,InvItems.Catl,InvItems.Draw "+
                     " From "+
                     " "+SItemTable+" Left Join InvItems On "+
                     " InvItems.Item_Code = "+SItemTable+".Item_Code "+
                     " Inner join Uom On Uom.UomCode=InvItems.UomCode "+
                     " Order By Item_Name";

          String QS7 =" Select HodName,HodCode From Hod Order By HodName ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS1);

               while(result.next())
               {
                    VUnitName.addElement(result.getString(1));
                    VUnitCode.addElement(result.getString(2));
               }
               result.close();
               result          = theStatement.executeQuery(QS2);
               while(result.next())
               {
                    VDeptName.addElement(result.getString(1));
                    VDeptCode.addElement(result.getString(2));
               }
               result.close();
               result          = theStatement.executeQuery(QS3);
               while(result.next())
               {
                    VGroupName.addElement(result.getString(1));
                    VGroupCode.addElement(result.getString(2));
               }
               result.close();
               result          = theStatement.executeQuery(QS4);
               while(result.next())
               {
                    VBlockName.addElement(result.getString(1));
                    VBlockCode.addElement(result.getString(2));
               }
               result.close();
               result          = theStatement.executeQuery(QS5);
               while(result.next())
               {
                    VNatureName.addElement(result.getString(1));
                    VNatureCode.addElement(result.getString(2));
               }
               result.close();
               /*
               result          = theStatement.executeQuery(QS6);
               while(result.next())
               {
                    String str1 = result.getString(1);
                    String str2 = result.getString(2);
                    String str3 = result.getString(3);
                    String str4 = result.getString(4);
                    String str5 = result.getString(5);
                    VName.addElement(str1);
                    VCode.addElement(str2);
                    VNameCode.addElement(str1+" - "+str3+" (Code : "+str2+")"+" (Catl : "+str4+")"+" (Draw : "+str5+")");
               }
               result.close();
               */
               result          = theStatement.executeQuery(QS7);
               while(result.next())
               {
                    VUDeptName.addElement(result.getString(1));
                    VUDeptCode.addElement(result.getString(2));
               }
               result.close();

               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public void setVector1()
     {
          VItemCode  .removeAllElements();
          VCode.removeAllElements();
          VName.removeAllElements();
          VNameCode.removeAllElements();
          VCata.removeAllElements();
          VDraw.removeAllElements();

          VSlipItemCode.removeAllElements();
          VPaperColor.removeAllElements();
          VPaperSets .removeAllElements();
          VPaperSize .removeAllElements();
          VPaperSide .removeAllElements();
          VSlipNo    .removeAllElements();

          String QS8="";

          if(iMillCode==0)
               QS8 = " Select Item_Name,Item_Code,UomName,Catl,Draw,PaperColor,PaperSets,PaperSize,PaperSide,LastSlipNo+1 From InvItems "+
                     " Inner join Uom On Uom.UomCode=InvItems.UomCode "+
                     " Order By Item_Name";
          else
               QS8 = " Select InvItems.Item_Name,"+SItemTable+".Item_Code, "+
                     " UomName,InvItems.Catl,InvItems.Draw,PaperColor,PaperSets,PaperSize,PaperSide,"+SItemTable+".LastSlipNo+1 "+
                     " From "+
                     " "+SItemTable+" Left Join InvItems On "+
                     " InvItems.Item_Code = "+SItemTable+".Item_Code "+
                     " Inner join Uom On Uom.UomCode=InvItems.UomCode "+
                     " Order By Item_Name";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS8);
               while(result.next())
               {
                    String SName = result.getString(1);
                    String SCode = result.getString(2);
                    String SUom  = result.getString(3);
                    String SCata = result.getString(4);
                    String SDraw = result.getString(5);

                    VPaperColor.addElement(result.getString(6));
                    VPaperSets.addElement(result.getString(7));
                    VPaperSize.addElement(result.getString(8));
                    VPaperSide.addElement(result.getString(9));
                    VSlipNo.addElement(result.getString(10));

                    VName.addElement(SName);
                    VCode.addElement(SCode);
                    VNameCode.addElement(SName+" - "+SUom+" (Code : "+SCode+")"+" (Catl : "+SCata+")"+" (Draw : "+SDraw+")");
                    VCata.addElement(SCata);
                    VDraw.addElement(SDraw);
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setData()
     {
          try
          {
               String SUnit = common.parseNull((String)theModel.getValueAt(0,4));
               String SDept = common.parseNull((String)theModel.getValueAt(0,5));
               String SGroup = common.parseNull((String)theModel.getValueAt(0,6));
               String SBlock = common.parseNull((String)theModel.getValueAt(0,7));
               String SUrgent = common.parseNull((String)theModel.getValueAt(0,8));
               String SNature = common.parseNull((String)theModel.getValueAt(0,9));
               String SDate   = ((String)theModel.getValueAt(0,10));

               for(int i=1;i<theModel.getRows();i++)
               {
                    if(((String)theModel.getValueAt(i,4)).equals(""))
                    {
                         theModel.setValueAt(SUnit,i,4);
                    }
                    if(((String)theModel.getValueAt(i,5)).equals(""))
                    {
                         theModel.setValueAt(SDept,i,5);
                    }
                    if(((String)theModel.getValueAt(i,6)).equals(""))
                    {
                         theModel.setValueAt(SGroup,i,6);
                    }
                    if(((String)theModel.getValueAt(i,7)).equals(""))
                    {
                         theModel.setValueAt(SBlock,i,7);
                    }

                    if(((String)theModel.getValueAt(i,10)).equals(""))
                    {
                         theModel.setValueAt(SDate,i,10);
                    }
                    if(((String)theModel.getValueAt(i,1)).equals(""))
                    {
                         theModel.setValueAt("",i,4);
                         theModel.setValueAt("",i,5);
                         theModel.setValueAt("",i,6);
                         theModel.setValueAt("",i,7);
                         theModel.setValueAt("",i,8);
                         theModel.setValueAt("",i,9);
                         theModel.setValueAt("",i,10);
                    }
               }
          }
          catch(Exception e)
          {
               System.out.println(e);
          }
     }
     public void setModifyData(int iRow)
     {
          try
          {
               theModel.setValueAt(mrsrecordframe.TMatCode.getText(),iRow,1);
               theModel.setValueAt(mrsrecordframe.BMatName.getText(),iRow,2);
               theModel.setValueAt(mrsrecordframe.TQty.getText(),iRow,3);
               theModel.setValueAt(mrsrecordframe.JCUnit.getSelectedItem(),iRow,4);
               theModel.setValueAt(mrsrecordframe.JCDept.getSelectedItem(),iRow,5);
               theModel.setValueAt(mrsrecordframe.JCGroup.getSelectedItem(),iRow,6);
               theModel.setValueAt(mrsrecordframe.JCBlock.getSelectedItem(),iRow,7);
               theModel.setValueAt(mrsrecordframe.JCUrgOrd.getSelectedItem(),iRow,8);
               theModel.setValueAt(mrsrecordframe.JCNature.getSelectedItem(),iRow,9);
               theModel.setValueAt(common.parseDate(mrsrecordframe.TDueDate.toNormal()),iRow,10);
               theModel.setValueAt(mrsrecordframe.TMatCatl.getText(),iRow,11);
               theModel.setValueAt(mrsrecordframe.TMatDraw.getText(),iRow,12);
               theModel.setValueAt(mrsrecordframe.TMake.getText(),iRow,13);
               theModel.setValueAt(mrsrecordframe.TRemarks.getText(),iRow,14);
          }
          catch(Exception e)
          {
               System.out.println(e);
          }
     }

     public String getItemName(int iRow)
     {
          return (String)theModel.getValueAt(iRow,2);
     }

     public void insertMRS()
     {
          try
          {
               int iSize=0;

               if(iList==1)
                    iSize = VECode.size();


               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               PreparedStatement   psmt = theConnection.prepareStatement(getInsertMRSQS());


               for(int i=iSize;i<theModel.getRows();i++)
               {
                    double dQty  = common.toDouble((String)theModel.getValueAt(i,3));
                    String SUnit = common.parseNull((String)theModel.getValueAt(i,4));
                    String SDept = common.parseNull((String)theModel.getValueAt(i,5));
                    String SGroup= common.parseNull((String)theModel.getValueAt(i,6));
                    String SBlock= common.parseNull((String)theModel.getValueAt(i,7));
                    int    iDate = common.toInt(common.pureDate((String)theModel.getValueAt(i,10)));
                    String SDate = ((String)theModel.getValueAt(i,10));

                    if(!common.parseNull((String)theModel.getValueAt(i,1)).equals(""))
                    {
                         if(!isCheck(SUnit,SDept,SGroup,SBlock,dQty,iDate,SDate))
                              return;
                    }
               }
               for(int i=iSize;i<theModel.getRows();i++)
               {
                    int iUnitCode = getUnitCode(((String)theModel.getValueAt(i,4)));
                    int iDeptCode = getDeptCode(((String)theModel.getValueAt(i,5)));
                    int iGroupCode= getGroupCode(((String)theModel.getValueAt(i,6)));
                    int iBlockCode= getBlockCode(((String)theModel.getValueAt(i,7)));
                    int iUrgentCode=getUrgentCode(((String)theModel.getValueAt(i,8)));
                    int iNatureCode=getNatureCode(((String)theModel.getValueAt(i,9)));
                    int iHODCode   =getHODCode(((String)JCDept.getSelectedItem()));

                    String SItemCode =((String)theModel.getValueAt(i,1));
                    double dQty  = common.toDouble((String)theModel.getValueAt(i,3));

                    String SPaperColor = common.parseNull(getPaperColor(SItemCode));
                    String SPaperSets  = common.parseNull(getPaperSets(SItemCode));
                    String SPaperSize  = common.parseNull(getPaperSize(SItemCode));
                    String SPaperSide  = common.parseNull(getPaperSide(SItemCode));

                    String SSlipFrNo   = common.parseNull(getSlipFrNo(SItemCode));
                    String SSlipToNo   = common.parseNull(getSlipToNo(SItemCode)); 
                                         
                    String SNetRate="";
                    String SNetValue="";
                    String SMrsAuth="";

                    if(SMrsFlag.equals("0"))
                    {
                         SNetRate  = "0";
                         SNetValue = "0";
                         SMrsAuth  = "0";     
                    }
                    else
                    {
                         SNetRate  = common.getLastNetRate(SItemCode,iMillCode);
                         SNetValue = common.getRound(dQty * common.toDouble(SNetRate),2);
                         SMrsAuth  = "1";     
                    }

                    iMaxSlNo++;

                    psmt.setInt(1,common.toInt(TMRSNo.getText()));
                    psmt.setString(2,TFromDt.toNormal());
                    psmt.setString(3,((String)theModel.getValueAt(i,1)));
                    psmt.setDouble(4,dQty);
                    psmt.setInt(5,iUnitCode);
                    psmt.setInt(6,iDeptCode);
                    psmt.setInt(7,iGroupCode);
                    psmt.setString(8,((String)theModel.getValueAt(i,11)));
                    psmt.setString(9,((String)theModel.getValueAt(i,12)));
                    psmt.setString(10,((String)theModel.getValueAt(i,13)));
                    psmt.setString(11,common.pureDate((String)theModel.getValueAt(i,10)));
                    psmt.setInt(12,iHODCode);
                    psmt.setInt(13,common.toInt(TMRSNo.getText()));
                    psmt.setInt(14,iNatureCode);
                    psmt.setInt(15,iBlockCode);
                    psmt.setInt(16,iUrgentCode);
                    psmt.setInt(17,iMaxSlNo);
                    psmt.setInt(18,iMillCode);
                    psmt.setString(19,((String)theModel.getValueAt(i,14)));
                    psmt.setInt(20,0);
                    psmt.setInt(21,0);
                    psmt.setInt(22,iUserCode);
                    psmt.setString(23,common.getServerDate());

                    psmt.setString(24,SPaperColor);
                    psmt.setString(25,SPaperSets);
                    psmt.setString(26,SPaperSize);
                    psmt.setString(27,SPaperSide);

                    psmt.setString(28,SSlipFrNo);
                    psmt.setString(29,SSlipToNo);
                    psmt.setString(30,SMrsFlag);
                    psmt.setString(31,SMrsTypeCode);
                    psmt.setString(32,SBudgetStatus);
                    psmt.setString(33,SNetRate);
                    psmt.setString(34,SNetValue);
                    psmt.setString(35,SMrsAuth);

                    if(bCCheck)
                    {
                         if(theConnection.getAutoCommit())
                                   theConnection . setAutoCommit(false);
                    }

                    if(!common.parseNull((String)theModel.getValueAt(i,1)).equals(""))
                         psmt.execute();


                    if(isStationary(SItemCode))
                         UpdateInvItems(SItemCode,SSlipToNo);
               }
               psmt.close();
               bFlag = true;

          }
          catch(Exception ex)
          {
               if(bCCheck)
                    bComFlag  = false;

               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private String getInsertMRSQS()
     {
          String QS = "";
          try
          {
               QS = " Insert Into MRS( "+
                    " ID,MRSNO,MRSDATE,ITEM_CODE,QTY,UNIT_CODE,DEPT_CODE,GROUP_CODE,CATL, "+     
                    " DRAW,MAKE,DUEDATE,HODCODE,REFNO, "+
                    " NATURECODE,BLOCKCODE,URGORD,SLNO,MILLCODE ,REMARKS,STATUS,FLAG,USERCODE,CREATIONDATE,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE,SLIPFROMNO,SLIPTONO,MRSFLAG,MRSTYPECODE,BUDGETSTATUS,NETRATE,NETVALUE,DEPTMRSAUTH) "+
                    " Values (MRS_seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
               
          }
          catch(Exception e)
          {
               System.out.println("Insert MRS  "+e);
          }
          return QS;
     }
     private int getUnitCode(String SUnitName)
     {
          int iIndex=-1;

          iIndex = VUnitName.indexOf(SUnitName);
          if(iIndex!=-1)
               return common.toInt((String)VUnitCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getDeptCode(String SDeptName)
     {
          int iIndex=-1;

          iIndex = VDeptName.indexOf(SDeptName);
          if(iIndex!=-1)
               return common.toInt((String)VDeptCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getGroupCode(String SGroupName)
     {
          int iIndex=-1;

          iIndex = VGroupName.indexOf(SGroupName);
          if(iIndex!=-1)
               return common.toInt((String)VGroupCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getBlockCode(String SBlockName)
     {
          int iIndex=-1;

          iIndex = VBlockName.indexOf(SBlockName);
          if(iIndex!=-1)
               return common.toInt((String)VBlockCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getUrgentCode(String SUrgentName)
     {
          int iIndex=-1;

          iIndex = VUrgentName.indexOf(SUrgentName);
          if(iIndex!=-1)
               return common.toInt((String)VUrgentCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getNatureCode(String SNatureName)
     {
          int iIndex=-1;

          iIndex = VNatureName.indexOf(SNatureName);
          if(iIndex!=-1)
               return common.toInt((String)VNatureCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getHODCode(String SHODName)
     {
          int iIndex=-1;

          iIndex = VUDeptName.indexOf(SHODName);
          if(iIndex!=-1)
               return common.toInt((String)VUDeptCode.elementAt(iIndex));
          else
               return 0;
     }
     private void setRecordData(int iRow)
     {
          mrsrecordframe.TMatCode.setText(((String)theModel.getValueAt(iRow,1)));
          mrsrecordframe.BMatName.setText(((String)theModel.getValueAt(iRow,2)));
          mrsrecordframe.TMatCatl.setText(((String)theModel.getValueAt(iRow,11)));
          mrsrecordframe.TMake.setText(((String)theModel.getValueAt(iRow,13)));
          mrsrecordframe.TMatDraw.setText(((String)theModel.getValueAt(iRow,12)));
          mrsrecordframe.TQty.setText(((String)theModel.getValueAt(iRow,3)));
          mrsrecordframe.TDueDays.setText("");
          mrsrecordframe.TDueDate.fromString(((String)theModel.getValueAt(iRow,10)));
          mrsrecordframe.JCUnit.setSelectedItem(((String)theModel.getValueAt(iRow,4)));
          mrsrecordframe.JCDept.setSelectedItem(((String)theModel.getValueAt(iRow,5)));
          mrsrecordframe.JCGroup.setSelectedItem(((String)theModel.getValueAt(iRow,6)));
          mrsrecordframe.JCBlock.setSelectedItem(((String)theModel.getValueAt(iRow,7)));
          mrsrecordframe.JCUrgOrd.setSelectedItem(((String)theModel.getValueAt(iRow,8)));
          mrsrecordframe.JCNature.setSelectedItem(((String)theModel.getValueAt(iRow,9)));
          mrsrecordframe.TRemarks.setText(((String)theModel.getValueAt(iRow,14)));

          String SNature = ((String)theModel.getValueAt(iRow,9));

          if(SNature.equals(""))
          {
               mrsrecordframe.JCNature.setSelectedItem("Consumable");
          }

     }
     public void setExistData()
     {
          theModel.setNumRows(0);

          for(int i=0;i<VECode.size();i++)
          {
               Vector theVect = new Vector();
               String SUrgent="";

               theVect.addElement(String.valueOf(i+1));
               theVect.addElement(common.parseNull((String)VECode.elementAt(i)));
               theVect.addElement(common.parseNull((String)VEName.elementAt(i)));
               theVect.addElement(common.parseNull((String)VEQty.elementAt(i)));
               theVect.addElement(common.parseNull((String)VEUnit.elementAt(i)));
               theVect.addElement(common.parseNull((String)VEDept.elementAt(i)));
               theVect.addElement(common.parseNull((String)VEGroup.elementAt(i)));
               theVect.addElement(common.parseNull((String)VEBlock.elementAt(i)));

               if(common.toInt((String)VEUrgent.elementAt(i))==0)
                    SUrgent = "Ordinary";
               else
                    SUrgent = "Urgent";

               theVect.addElement(SUrgent);

               theVect.addElement(common.parseNull((String)VENature.elementAt(i)));
               theVect.addElement(common.parseNull((String)VEDueDate.elementAt(i)));
               theVect.addElement(common.parseNull((String)VECatl.elementAt(i)));
               theVect.addElement(common.parseNull((String)VEDraw.elementAt(i)));
               theVect.addElement(common.parseNull((String)VEMake.elementAt(i)));
               theVect.addElement(common.parseNull((String)VERemarks.elementAt(i)));

               theModel.appendRow(theVect);
               TFromDt.fromString1(SMrsDate);
          }
     }
     public void setExistVector()
     {
          JCDept.setSelectedItem(SName);
          TMRSNo.setText(SMrsNo);
          TFromDt.fromString1(SMrsDate);

          VECode      = new Vector();
          VEName      = new Vector();
          VEQty       = new Vector();
          VEUnit      = new Vector();
          VEDept      = new Vector();
          VEGroup     = new Vector();
          VEBlock     = new Vector();
          VEUrgent    = new Vector();
          VENature    = new Vector();
          VEDueDate   = new Vector();
          VECatl      = new Vector();
          VEDraw      = new Vector();
          VEMake      = new Vector();
          VERemarks   = new Vector();
          VEId        = new Vector();
          VEOrderNo   = new Vector();
          VESlipFrNo  = new Vector();
          VESlipToNo  = new Vector();

          String QS=" Select MRS.Item_Code,decode(Item_Name,null,MRs.ItemName,Item_Name),Qty,Unit_Name,Dept_Name,Group_Name,BlockName,UrgOrd,Nature ,DUEDATE,InvItems.CATL ,InvItems.DRAW ,MAKE,Remarks,MRS.Id,MRS.OrderNo,MRs.SlipFromNo,Mrs.SlipToNo from MRS "+
                    " Left join InvItems on InvItems.Item_Code=MRS.Item_Code "+
                    " left join Unit on unit.unit_Code=MRS.Unit_Code "+
                    " left join Dept on Dept.Dept_Code=MRS.Dept_Code "+
                    " left join Cata on Cata.Group_Code=MRS.Group_Code "+
                    " Left join OrdBlock on OrdBlock.Block=MRS.BlockCode "+
                    " left join Nature on Nature.NatureCode=MRS.NatureCode "+
                    " Where MRSNO='"+SMrsNo+"' and MRSDate="+SMrsDate+" and MRS.MillCOde ="+iMillCode+" Order By Mrs.Id ";


          String QS1 = " Select Max(SlNo) from Mrs Where MrsNo='"+SMrsNo+"' and MillCode="+iMillCode;

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);

               while(result.next())
               {
                    VECode.addElement(result.getString(1));
                    VEName.addElement(result.getString(2));
                    VEQty.addElement(result.getString(3));
                    VEUnit.addElement(result.getString(4));
                    VEDept.addElement(result.getString(5));
                    VEGroup.addElement(result.getString(6));
                    VEBlock.addElement(result.getString(7));
                    VEUrgent.addElement(result.getString(8));
                    VENature.addElement(result.getString(9));
                    VEDueDate.addElement(common.parseDate(result.getString(10)));
                    VECatl.addElement(result.getString(11));
                    VEDraw.addElement(result.getString(12));
                    VEMake.addElement(result.getString(13));
                    VERemarks.addElement(result.getString(14));
                    VEId.addElement(result.getString(15));
                    VEOrderNo.addElement(result.getString(16));
                    VESlipFrNo.addElement(result.getString(17));
                    VESlipToNo.addElement(result.getString(18));
               }
               result.close();

               result          = theStatement.executeQuery(QS1);

               while(result.next())
               {
                    iMaxSlNo = result.getInt(1);
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void updateMRS()
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               for(int i=0;i<VECode.size();i++)
               {
                    double dQty  = common.toDouble((String)theModel.getValueAt(i,3));
                    String SUnit = common.parseNull((String)theModel.getValueAt(i,4));
                    String SDept = common.parseNull((String)theModel.getValueAt(i,5));
                    String SGroup= common.parseNull((String)theModel.getValueAt(i,6));
                    String SBlock= common.parseNull((String)theModel.getValueAt(i,7));
                    int    iDate = common.toInt(common.pureDate((String)theModel.getValueAt(i,10)));
                    String SDate = ((String)theModel.getValueAt(i,10));

                    if(!common.parseNull((String)theModel.getValueAt(i,1)).equals(""))
                    {
                         if(!isCheck(SUnit,SDept,SGroup,SBlock,dQty,iDate,SDate))
                         {
                              bUpdateFlag=false;
                              return;
                         }
                    }
               }

               for(int i=0;i<VECode.size();i++)
               {
                    if(bCCheck)
                    {
                         if(theConnection.getAutoCommit())
                                   theConnection  . setAutoCommit(false);
                    }

                    int iId = common.toInt((String)VEId.elementAt(i));

                    int iUnitCode = getUnitCode(((String)theModel.getValueAt(i,4)));
                    int iDeptCode = getDeptCode(((String)theModel.getValueAt(i,5)));
                    int iGroupCode= getGroupCode(((String)theModel.getValueAt(i,6)));
                    int iBlockCode= getBlockCode(((String)theModel.getValueAt(i,7)));
                    int iUrgentCode=getUrgentCode(((String)theModel.getValueAt(i,8)));
                    int iNatureCode=getNatureCode(((String)theModel.getValueAt(i,9)));
                    int iHODCode   =getHODCode(((String)JCDept.getSelectedItem()));

                    String SItemCode =((String)theModel.getValueAt(i,1));
                    double dQty  = common.toDouble((String)theModel.getValueAt(i,3));

                    String SSlipFrNo   = common.parseNull(getSlipFrNo(SItemCode));
                    String SSlipToNo   = common.parseNull(getSlipToNo(SItemCode)); 

                    if(SSlipFrNo.equals(""))
                    {
                         SSlipFrNo   = common.parseNull(getExistSlipFrNo(SItemCode));
                         SSlipToNo   = common.parseNull(getExistSlipToNo(SItemCode)); 
                    }

                    String SNetRate="";
                    String SNetValue="";

                    if(SMrsFlag.equals("0"))
                    {
                         SNetRate  = "0";
                         SNetValue = "0";
                    }
                    else
                    {
                         SNetRate  = common.getLastNetRate(SItemCode,iMillCode);
                         SNetValue = common.getRound(dQty * common.toDouble(SNetRate),2);
                    }


                    String QS1 = "Update MRS Set ";
                    QS1 = QS1+"MRSDate  = '"+TFromDt.toNormal()+"',";
                    QS1 = QS1+"Item_Code= '"+((String)theModel.getValueAt(i,1))+"',";
                    QS1 = QS1+"HodCode=0"+iHODCode+",";
                    QS1 = QS1+"RefNo=0"+TMRSNo.getText()+",";
                    QS1 = QS1+"Catl='"+((String)theModel.getValueAt(i,11))+"',";
                    QS1 = QS1+"Draw='"+((String)theModel.getValueAt(i,12))+"',";
                    QS1 = QS1+"Qty=0"+((String)theModel.getValueAt(i,3))+",";
                    QS1 = QS1+"Unit_Code=0"+iUnitCode+",";
                    QS1 = QS1+"Dept_Code=0"+iDeptCode+",";
                    QS1 = QS1+"Group_Code=0"+iGroupCode+",";
                    QS1 = QS1+"Serv_Code=0,";
                    QS1 = QS1+"Mac_Code=0,";
                    QS1 = QS1+"BlockCode=0"+iBlockCode+",";
                    QS1 = QS1+"NatureCode=0"+iNatureCode+",";
                    QS1 = QS1+"UrgOrd=0"+iUrgentCode+",";
                    QS1 = QS1+"Remarks='"+((String)theModel.getValueAt(i,14))+"',";
                    QS1 = QS1+"Make   ='"+((String)theModel.getValueAt(i,13))+"',";
                    QS1 = QS1+"DueDate='"+common.pureDate((String)theModel.getValueAt(i,10))+"',";
                    QS1 = QS1+"NetRate=0"+SNetRate+",";
                    QS1 = QS1+"NetValue=0"+SNetValue+",";
                    QS1 = QS1+"MillCode="+iMillCode+",UserCode="+iUserCode+",CreationDate='"+common.getServerDate()+"',SlipFromNo='"+SSlipFrNo+"',SlipToNo='"+SSlipToNo+"'  Where id = "+iId;

                    theStatement.executeUpdate(QS1);
                    UpdateInvItems(SItemCode,SSlipToNo);
                }
                  bUpdateFlag=true;
          }
          catch(Exception ex)
          {
               if(bCCheck)
                    bComFlag  = false;

               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private boolean isValidData()
     {
          try
          {
               int iMrsNo = getMRSNo(common.toInt(TMRSNo.getText()));

               if(TMRSNo.getText().equals(""))
               {
                    JOptionPane.showMessageDialog(null,"MRS No is Empty ","Error",JOptionPane.ERROR_MESSAGE);
                    TMRSNo.setText("");
                    TMRSNo.requestFocus();

                    return false;
               }
               if(common.toInt(TMRSNo.getText())==0)
               {
                    JOptionPane.showMessageDialog(null,"MRS No Shoule Be Greater Than Zero ","Error",JOptionPane.ERROR_MESSAGE);
                    TMRSNo.setText("");
                    TMRSNo.requestFocus();

                    return false;
               }
               if(iMrsNo>0)
               {
                    JOptionPane.showMessageDialog(null,"MRS No is Already Exist ","Error",JOptionPane.ERROR_MESSAGE);
                    TMRSNo.setText("");
                    TMRSNo.requestFocus();

                    return false;
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
               return true;
     }
     private boolean isCheck(String SUnit,String SDept,String SGroup,String SBlock,double dQty,int iDate,String SDate)
     {
          try
          {
               if(dQty<=0)
               {
                    JOptionPane.showMessageDialog(null,"Qty is Greater Than Zero ","Error",JOptionPane.ERROR_MESSAGE);

                    return false;
               }

               if(SUnit.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Unit is Empty ","Error",JOptionPane.ERROR_MESSAGE);

                    return false;
               }
               if(SDept.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Dept is Empty ","Error",JOptionPane.ERROR_MESSAGE);

                    return false;
               }
               if(SGroup.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Group is Empty ","Error",JOptionPane.ERROR_MESSAGE);

                    return false;
               }
               if(SBlock.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Block is Empty ","Error",JOptionPane.ERROR_MESSAGE);

                    return false;
               }
               if(SDate.length() != 10)
               {
                    JOptionPane.showMessageDialog(null,"DueDate Format is Not Correct","Error",JOptionPane.ERROR_MESSAGE);

                    return false;
               }

               if(iDate==0)
               {
                    JOptionPane.showMessageDialog(null,"DueDate is Empty ","Error",JOptionPane.ERROR_MESSAGE);

                    return false;
               }
               if(common.toInt(TFromDt.toNormal())>iDate)
               {
                    JOptionPane.showMessageDialog(null,"DueDate is Greater Than MRS Date ","Error",JOptionPane.ERROR_MESSAGE);

                    return false;
               }


          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
               return true;
     }     

     public int getMRSNo(int iMrsNo)
     {
          int iCount = 0;
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS="";

               QS = "Select Count(*) From MRS where MRSNo="+iMrsNo+" and MillCode = "+iMillCode;

               ResultSet result          = theStatement.executeQuery(QS);

               while(result.next())
               {
                    iCount = result.getInt(1);
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return iCount;
     }
     private String getPaperColor(String SItemCode)
     {
          int iIndex=-1;

          iIndex = VItemCode.indexOf(SItemCode);
          if(iIndex!=-1)
               return common.parseNull((String)VPaperColor.elementAt(iIndex));
          else
               return "";
     }
     private String getPaperSets(String SItemCode)
     {
          int iIndex=-1;

          iIndex = VItemCode.indexOf(SItemCode);
          if(iIndex!=-1)
               return common.parseNull((String)VPaperSets.elementAt(iIndex));
          else
               return "";
     }
     private String getPaperSize(String SItemCode)
     {
          int iIndex=-1;

          iIndex = VItemCode.indexOf(SItemCode);

          if(iIndex!=-1)
               return common.parseNull((String)VPaperSize.elementAt(iIndex));
          else
               return "";
     }
     private String getPaperSide(String SItemCode)
     {
          int iIndex=-1;

          iIndex = VItemCode.indexOf(SItemCode);
          if(iIndex!=-1)
               return common.parseNull((String)VPaperSide.elementAt(iIndex));
          else
               return "";
     }
     private String getSlipFrNo(String SItemCode)
     {
          int iIndex=-1;

          iIndex = VSItemCode.indexOf(SItemCode);
          if(iIndex!=-1)
               return common.parseNull((String)VSSlipFrNo.elementAt(iIndex));
          else
               return "";
     }

     private String getSlipToNo(String SItemCode)
     {
          int iIndex=-1;

          iIndex = VSItemCode.indexOf(SItemCode);
          if(iIndex!=-1)
               return common.parseNull((String)VSSlipToNo.elementAt(iIndex));
          else
               return "";
     }
     private String getExistSlipFrNo(String SItemCode)
     {
          int iIndex=-1;

          iIndex = VECode.indexOf(SItemCode);
          if(iIndex!=-1)
               return common.parseNull((String)VESlipFrNo.elementAt(iIndex));
          else
               return "";
     }

     private String getExistSlipToNo(String SItemCode)
     {
          int iIndex=-1;

          iIndex = VECode.indexOf(SItemCode);
          if(iIndex!=-1)
               return common.parseNull((String)VESlipToNo.elementAt(iIndex));
          else
               return "";
     }

     private String getSlipNo(String SItemCode)
     {
          int iIndex=-1;

          iIndex = VSlipItemCode.indexOf(SItemCode);
          if(iIndex!=-1)
               return common.parseNull((String)VSlipNo.elementAt(iIndex));
          else
               return "";
     }

     public boolean isStationary(String SItemCode)
     {
          String SStkGroupName     = "";
          String QS                = "";

          if(iMillCode==0)
          {
               QS =    " select InvItems.stkgroupcode,stockgroup.groupname,invitems.item_code from invitems"+
                       " inner join stockgroup on stockgroup.groupcode = invitems.stkgroupcode"+
                       " where invitems.item_code = '"+SItemCode+"'";
          }
          else
          {
               QS =    " select "+SItemTable+".stkgroupcode,stockgroup.groupname,"+SItemTable+".item_code from "+SItemTable+""+
                       " inner join stockgroup on stockgroup.groupcode = "+SItemTable+".stkgroupcode"+
                       " where "+SItemTable+".item_code = '"+SItemCode+"'";
          }
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
     
               Statement      stat          = theConnection.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result.next())
               {
                    SStkGroupName  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }

          if(SStkGroupName.equals("B01"))
               return true;

          return false;
     }

     public void UpdateInvItems(String SItemCode,String SSlipNo)
     {
          String QS = "";
          Statement stat      = null;
          try
          {
               int iSlipNo = common.toInt(SSlipNo);

               if(iSlipNo>0)
               {
                    if(isStationary(SItemCode))
                    {
                         if(iMillCode==0)
                         {
                              QS = " Update Invitems        set LASTSLIPNO ="+iSlipNo+" "+
                                   " where invitems.item_code = '"+SItemCode+"'";
                         }
                         else
                         {
                              QS = " Update "+SItemTable+"  set LASTSLIPNO ="+iSlipNo+" "+
                                   " where "+SItemTable+".item_code = '"+SItemCode+"' ";
                         }
     
                         if(theConnection == null)
                         {
                              ORAConnection jdbc   = ORAConnection.getORAConnection();
                              theConnection        = jdbc.getConnection();
                         }

                         if(bCCheck)
                         {
                              if(theConnection.getAutoCommit())
                                        theConnection  . setAutoCommit(false);
                         }

                         stat =    theConnection. createStatement();
                                   stat      . executeUpdate(QS);
     
                    }
                    stat.close();
               }
          }catch(Exception ex)
          {
               if(bCCheck)
                    bComFlag  = false;

               System.out.println(ex);
               ex.printStackTrace();
          }
     }
}
