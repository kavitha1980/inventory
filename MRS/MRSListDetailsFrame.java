package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MRSListDetailsFrame extends JInternalFrame
{
     Object RowData[][];
     String ColumnData[] = {"Date","Mrs No","Code","Name","Qty","Unit","Department","Group","Status","Due Date","UserName","Remarks"};
     String ColumnType[] = {"S","N","S","S","N","S","S","S","S","S","S","S"};
     String ReportHead="",SDate = "",EDate = "",SStart = "",SEnd   = "";
     int len=0,iPrintLine = 0,iUserCode = 0,iMillCode= 0,iAuthCode=0,ipagec = 1;
     String SItemTable,SMillName;
     
     FileWriter FW;
     Vector VDate,VMrsNo,VRCode,VRName,VQty,VDept,VGroup,VStatus,VUnit,VDue,VId,VUserName,VRemarks;
     
     JLayeredPane DeskTop;
     TabReport tabreport;
     JPanel BottomPanel;
     JButton BPrint;
     JTextField TFileName;
     
     MRSCritPanel   TopPanel;
     Common common;

     public MRSListDetailsFrame(JLayeredPane DeskTop,int iMillCode,int iAuthCode,int iUserCode,String SItemTable,String SMillName)
     {
          super("MRS Details Frame");
     
          this.DeskTop    = DeskTop;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.iAuthCode  = iAuthCode;
          this.SItemTable = SItemTable;
          this.SMillName  = SMillName;

          common = new Common();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel    = new MRSCritPanel(iMillCode,SItemTable);
          BottomPanel = new JPanel();
          BPrint      = new JButton("Print");
          TFileName   = new JTextField(25);
          TFileName.setFont(new Font("Arial",Font.BOLD,11));
          TFileName.setForeground(new Color(100,100,220));
          TFileName.setText("MRSListDetails.prn");
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          BPrint.setEnabled(false);
          BottomPanel.add(BPrint);
          BottomPanel.add(TFileName);
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          TopPanel.BApply.addActionListener(new ApplyList());
          BPrint.addActionListener(new PrintList());
     }

     private void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.updateUI();
               DeskTop.repaint();
          }
          catch(Exception ex){}
     }
          
     public class PrintList implements  ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               PrnFileWrite();
               JOptionPane.showMessageDialog(null,getInfo(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
          }
     }

     public void PrnFileWrite()
     {
          try
          {
               String SFile =TFileName.getText();

               if((SFile.trim()).length()==0)
                    SFile = "1.prn";

               FW = new FileWriter(common.getPrintPath()+SFile);
               PrintMainHead();
               PrintHead();
               PrintData();
               prtStrl(common.Replicate("-",len));
               PrnReport();
               FW.close();
          }
          catch(Exception e)
          {
               System.out.println(e);
          }
     }

     private void PrintMainHead()
     {

          prtStrl("g"+SMillName);

          if(TopPanel.bsig==true)
          {
               prtStrl("MRSListDetails For the Period From"+common.Space(3)+common.parseDate(SDate)+common.Space(3)+"TO"+common.Space(3)+common.parseDate(EDate));
          }
          else
          {
               prtStrl("MRSListDetails For MRS Number : "+SStart);
          }

          prtStrl(ReportHead);
          prtStrl("Page:"+ipagec);
          prtStrl("\n");
     }

     private void PrnReport()
     {
          prtStrl("Report Taken  As On Date : "+common.parseDate(common.getCurrentDate())+" Time : "+common.getTime());
     }

     private void PrintHead()
     {
          String   S1  = common.Cad("Date",15);
          String   S2  = common.Cad("Mrs No",15);
          String   S3  = common.Cad("Code",15);
          String   S4  = common.Cad("Name",30);
          String   S5  = common.Rad("Qty",15);
          String   S6  = common.Cad("Unit",15);
          String   S7  = common.Cad("Department",25);
          String   S8  = common.Cad("Group",25);
          String   S9  = common.Cad("Due Date",15);
          String   S10 = common.Cad("User Name",15);
          
          String Mar=S1+S2+S3+S4+S5+S6+S7+S8+S9+S10;
          len=Mar.length();
          prtStrl(common.Replicate("-",len));
          prtStrl(Mar);
          prtStrl(common.Replicate("-",len));
     }

     private void PrintData()
     {
          for(int i=0; i<VDate.size(); i++)
          {
               String Str1  = (String)VDate.elementAt(i);
               String Str2  = (String)VMrsNo.elementAt(i);
               String Str3  = (String)VRCode.elementAt(i);
               String Str4  = (String)VRName.elementAt(i);
               String Str5  = (String)VQty.elementAt(i);
               String Str6  = (String)VUnit.elementAt(i);
               String Str7  = (String)VDept.elementAt(i);
               String Str8  = (String)VGroup.elementAt(i);
               String Str9  = (String)VDue.elementAt(i);
               String Str10 = (String)VUserName.elementAt(i);
               
               String   S1  = common.Cad(Str1,15);
               String   S2  = common.Rad(Str2,15);
               String   S3  = common.Cad(Str3,15);
               String   S4  = common.Pad(Str4,30);
               String   S5  = common.Rad(Str5,15);
               String   S6  = common.Cad(Str6,15);
               String   S7  = common.Cad(Str7,25);
               String   S8  = common.Cad(Str8,25);
               String   S9  = common.Cad(Str9,15);
               String   S10 = common.Cad(Str10,15);
               
               String Mar=S1+S2+S3+S4+S5+S6+S7+S8+S9+S10;
               prtStrl(Mar);
          }
     }

     private void prtStrl(String Strl)
     {
          iPrintLine=iPrintLine+1;
          try
          {
               if(iPrintLine >=60)
               {
                    ipagec= ipagec+1; 
                    iPrintLine=1;
                    prtStrl(common.Replicate("-",len));
                    FW.write(""+"\n");
                    PrintMainHead();
                    PrintHead();
               }
               FW.write(Strl+"\n");
          
          }
          catch(Exception e)
          {
               System.out.println(e);
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    ReportHead="";
                    ipagec =1;
                    iPrintLine=0;
                    setDataIntoVector();
                    setRowData();
                    BPrint.setEnabled(true);
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
          
               try
               {
                    tabreport = new TabReport(RowData,ColumnData,ColumnType);
                    getContentPane().add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
          }
     }

     public void setDataIntoVector()
     {
          ResultSet result = null;
          VDate       = new Vector();
          VMrsNo      = new Vector();
          VRCode      = new Vector();
          VRName      = new Vector();
          VQty        = new Vector();
          VDept       = new Vector();
          VGroup      = new Vector();
          VStatus     = new Vector();
          VDue        = new Vector();
          VUnit       = new Vector();
          VId         = new Vector();
          VUserName   = new Vector();
          VRemarks    = new Vector();
          

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               result        = stat.executeQuery(getQString());

               while (result.next())
               {
                    VMrsNo    .addElement((String)result.getString(2));
                    VDate     .addElement(common.parseDate((String)result.getString(1)));
                    VRCode    .addElement((String)result.getString(3));
                    VRName    .addElement((String)result.getString(4));
                    VQty      .addElement((String)result.getString(5));
                    VUnit     .addElement((String)result.getString(6));
                    VDept     .addElement((String)result.getString(7));
                    VGroup    .addElement(common.parseDate((String)result.getString(8)));
                    VDue      .addElement(common.parseDate((String)result.getString(9)));
                    VId       .addElement(result.getString(10));
                    VUserName .addElement(result.getString(11));
                    VRemarks  .addElement(result.getString(12));
                    VStatus   .addElement(" ");
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("setDataIntoVector: "+ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VDate.size()][ColumnData.length];
          for(int i=0;i<VDate.size();i++)
          {
               RowData[i][0]  = (String)VDate.elementAt(i);
               RowData[i][1]  = (String)VMrsNo.elementAt(i);
               RowData[i][2]  = (String)VRCode.elementAt(i);
               RowData[i][3]  = (String)VRName.elementAt(i);
               RowData[i][4]  = (String)VQty.elementAt(i);
               RowData[i][5]  = (String)VUnit.elementAt(i);
               RowData[i][6]  = (String)VDept.elementAt(i);
               RowData[i][7]  = common.parseNull((String)VGroup.elementAt(i));
               RowData[i][8]  = (String)VStatus.elementAt(i);
               RowData[i][9]  = common.parseNull((String)VDue.elementAt(i));
               RowData[i][10] = common.parseNull((String)VUserName.elementAt(i));
               RowData[i][11] = common.parseNull((String)VRemarks.elementAt(i));
          }  
     }

     public String getQString()
     {
          String QString ="";

          if(TopPanel.bsig==true)
          {
               SDate  = TopPanel.TStDate.toNormal();
               EDate  = TopPanel.TEnDate.toNormal();
          }
          else
          {
               SStart  = TopPanel.TStNo.getText();
          }

          String str ="";
          QString = " SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, InvItems.Item_Name , MRS.Qty,Unit.Unit_Name, Dept.Dept_Name , Cata.Group_Name , MRS.DueDate, MRS.Id, RawUser.UserName, MRS.Remarks "+
                    " FROM (((MRS INNER JOIN InvItems ON MRS.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON MRS.Group_Code=Cata.Group_Code) INNER JOIN Unit ON MRS.Unit_Code=Unit.Unit_Code "+
                    " Inner Join RawUser on decode(Mrs.MrsAuthUserCode,null,1,0,1,Mrs.MrsAuthUserCode) = RawUser.UserCode "+
                    " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and Mrs.MillCode="+iMillCode+" and ";
          
          String SHave="";
          if(TopPanel.bsig==true)
          {
               str= "MRS.MrsDate >= '"+SDate+"' and MRS.MrsDate <= '"+EDate+"'" ;
               SHave = SHave+str;
          }
          else
          {
               str   = "MRS.mrsNo = "+common.toInt(SStart);
               SHave = SHave+str;
          }

          if(TopPanel.JRSeleUnit.isSelected())
          {
               str = "Unit.Unit_Code = "+(String)TopPanel.VUnitCode.elementAt(TopPanel.JCUnit.getSelectedIndex());
               SHave=SHave+" and "+str;
               ReportHead=ReportHead+"Unit:"+(String)TopPanel.JCUnit.getSelectedItem()+" ,";
          }

          if(TopPanel.JRSeleDept.isSelected())
          {
               str = "Dept.Dept_Code = "+(String)TopPanel.VDeptCode.elementAt(TopPanel.JCDept.getSelectedIndex());
               SHave=SHave+" and "+str;
               ReportHead=ReportHead+"Dept:"+(String)TopPanel.JCDept.getSelectedItem() +",";
          }

          if(TopPanel.JRSeleGroup.isSelected())
          {
               str = "Cata.Group_Code = "+(String)TopPanel.VGroupCode.elementAt(TopPanel.JCGroup.getSelectedIndex());
               SHave=SHave+" and "+str;
               ReportHead=ReportHead+"Group:"+(String)TopPanel.JCGroup.getSelectedItem()+",";
          }

          if(TopPanel.JRSeleBlock.isSelected())
          {
               str = "Invitems.item_Code = '"+(String)TopPanel.VMaterialCode.elementAt(TopPanel.JCBlock.getSelectedIndex())+"'";
               SHave=SHave+" and "+str;
               ReportHead=ReportHead+"Material:"+(String)TopPanel.JCBlock.getSelectedItem()+",";
          }

          QString = QString+ SHave;

          if(TopPanel.JRSeleMrs.isSelected())
          {
               int iSelect = TopPanel.JCMrs.getSelectedIndex();
     
               if (iSelect==0)
               {
                    QString = QString+" and Mrs.PlanMonth = 0 ";
                    ReportHead=ReportHead+ "List : Regular MRS ,";
               }
     
               if(iSelect==1)
               {
                    QString = QString+" and Mrs.PlanMonth > 0 ";
                    ReportHead=ReportHead+" List : Monthly MRS ,";
               }
               if(iSelect==2)
               {
                    QString = QString+" and Mrs.YearlyPlanningStatus > 0 ";
                    ReportHead=ReportHead+" List : Monthly MRS ,";
               }

          }

          if(TopPanel.JRSeleList.isSelected())
          {
               DateField df = new DateField();
               df.setTodayDate();
               String SToday = df.toNormal();
               int iSelect = TopPanel.JCList.getSelectedIndex();
     
               if (iSelect==0)
               {
                    QString = QString+" and Mrs.DueDate <= '"+SToday+"'";
                    ReportHead=ReportHead+ "List :Over Due ,";
               }
     
               if(iSelect==1)
               {
                    QString = QString+" and Mrs.OrderNo = 0";
                    ReportHead=ReportHead+" List :Order Not Placed ,";
               }
          }


          String SOrder=(TopPanel.TSort.getText()).trim();

          if(SOrder.length()>0)
               QString = QString+" Order By "+TopPanel.TSort.getText()+" ";
          else
               QString = QString+" Order By 1,2,3,4";

         return QString;
     }

     private String getInfo()
     {
          String str = "<html><body>"; 
          str = str+"<h4><font color='green'> PRN File Successfully Created</font></h4>";
          str = str+"</body></html>";
     
          return str;
     }
}
