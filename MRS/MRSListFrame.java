package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MRSListFrame extends JInternalFrame
{
     JButton        BApply;
     JPanel         TopPanel,BottomPanel;
     JPanel         First,Second,Third,Fourth,Fifth;
     JComboBox      JCFilter;
     JTextField     TOrder;
     JLayeredPane   DeskTop;
     JRadioButton   JRPeriod,JRNo;

     StatusPanel    SPanel;
     TabReport      tabreport;
     DateField     TStDate,TEnDate;
     NextField      TStNo,TEnNo;
     Common         common;

     Object         RowData[][];
     String         ColumnData[] = {"Date","Mrs No","Code","Name","Qty","Unit","Department","Group","Status","Due Date"};
     String         ColumnType[] = {"S","N","S","S","N","S","S","S","S","S"};
     int            iMillCode,iAuthCode,iUserCode;

     Vector VCode,VName,VNameCode,VCata,VDraw;
     Vector VPaperColor,VPaperSets,VPaperSize,VPaperSide,VSlipNo;

     String SItemTable;

     Vector VDate,VMrsNo,VRCode,VRName,VQty,VDept,VGroup,VStatus,VUnit,VDue,VId,VHodName,VMrsOrdNo,VMrsFlag,VMrsTypeCode,VBudgetStatus;

     public MRSListFrame(JLayeredPane Layer,int iMillCode,int iAuthCode,int iUserCode,Vector VCode,Vector VName,Vector VNameCode,Vector VCata,Vector VDraw,Vector VPaperColor,Vector VPaperSets,Vector VPaperSize,Vector VPaperSide,Vector VSlipNo,String SItemTable)
     {
          super("MRSs Placed During a Period");
     
          this.DeskTop        = Layer;
          this.iMillCode      = iMillCode;
          this.iAuthCode      = iAuthCode;
          this.iUserCode      = iUserCode;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.VCata          = VCata;
          this.VDraw          = VDraw;

          this.VPaperColor    = VPaperColor;
          this.VPaperSets     = VPaperSets;
          this.VPaperSize     = VPaperSize;
          this.VPaperSide     = VPaperSide;
          this.VSlipNo        = VSlipNo;
          this.SItemTable     = SItemTable;

          common         = new Common();
     
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TStDate     = new DateField();
          TEnDate     = new DateField();
          BApply      = new JButton("Apply");
          TopPanel    = new JPanel();
          First       = new JPanel();
          Second      = new JPanel();
          Third       = new JPanel();
          Fourth      = new JPanel();
          Fifth       = new JPanel();
          BottomPanel = new JPanel();
          TOrder      = new JTextField("1,2,3");
          JCFilter    = new JComboBox();
          TStNo       = new NextField(0);
          TEnNo       = new NextField(0);
     
          JRPeriod    = new JRadioButton("Periodical",true);
          JRNo        = new JRadioButton("MRS No",false);
     
          TStDate.setTodayDate();
          TEnDate.setTodayDate();
     }

     public void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(1,5));
          First     .setLayout(new GridLayout(1,1));
          Second    .setLayout(new GridLayout(1,1));
          Third     .setLayout(new GridLayout(2,1));
          Fourth    .setLayout(new GridLayout(2,1));
          Fifth     .setLayout(new GridLayout(1,1));
     
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          JCFilter.addItem("All");
          JCFilter.addItem("Over-Due");
          JCFilter.addItem("Order Not Placed");
          
          First.setBorder(new TitledBorder("Sorted On (Column No)"));
          First.add(TOrder);
          
          Second.setBorder(new TitledBorder("Filter"));
          Second.add(JCFilter);
          
          Third.setBorder(new TitledBorder("Basis"));
          Third.add(JRPeriod);
          Third.add(JRNo);
          
          Fourth.setBorder(new TitledBorder("Periodical"));
          Fourth.add(TStDate);
          Fourth.add(TEnDate);
          
          Fifth.add(BApply);
          
          TopPanel.add(First);
          TopPanel.add(Second);
          TopPanel.add(Third);
          TopPanel.add(Fourth);
          TopPanel.add(Fifth);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          JRPeriod.addActionListener(new JRList());
          JRNo.addActionListener(new JRList()); 
          BApply.addActionListener(new ApplyList());
     }

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    Fourth.setBorder(new TitledBorder("Periodical"));
                    Fourth.removeAll();
                    Fourth.add(TStDate);
                    Fourth.add(TEnDate);
                    Fourth.updateUI();
                    JRNo.setSelected(false);
               }
               else
               {
                    Fourth.setBorder(new TitledBorder("Numbered"));
                    Fourth.removeAll();
                    Fourth.add(TStNo);
                    Fourth.add(TEnNo);
                    Fourth.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    setDataIntoVector();
                    setRowData();
               try
               {
                  getContentPane().remove(tabreport);
               }
               catch(Exception ex)
               {}
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.setBorder(new TitledBorder("List (Insert For Modification)"));
               getContentPane().add(tabreport,BorderLayout.CENTER);
     
               if(iAuthCode>1)
               {
                    //tabreport.ReportTable.addKeyListener(new KeyList());
               }
                    setSelected(true);
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
               catch(Exception ex)
               {
               }
          }
     }

     /*public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if (ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    int i = tabreport.ReportTable.getSelectedRow();
                    String SMrsNo       = (String)RowData[i][1];
                    String SMrsDate     = common.pureDate((String)RowData[i][0]);
                    String SName        = getHodName(SMrsNo);
                    String SMrsFlag     = (String)VMrsFlag.elementAt(i);
                    String SMrsTypeCode = (String)VMrsTypeCode.elementAt(i);
                    String SBudgetStatus= (String)VBudgetStatus.elementAt(i);

                    Runtime.getRuntime().freeMemory();
                    Runtime.getRuntime().gc();

                    MRSFrame mrsframe   = new MRSFrame(DeskTop,iUserCode,iMillCode,SMrsNo,SName,SMrsDate,SMrsFlag,SMrsTypeCode,SBudgetStatus,VCode,VName,VNameCode,VCata,VDraw,VPaperColor,VPaperSets,VPaperSize,VPaperSide,VSlipNo,SItemTable);

                    DeskTop.add(mrsframe);
                    mrsframe.show();
                    mrsframe.moveToFront();
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
          } 
     }*/

     public void setDataIntoVector()
     {
          ResultSet result = null;

          VDate         = new Vector();
          VMrsNo        = new Vector();
          VRCode        = new Vector();
          VRName        = new Vector();
          VQty          = new Vector();
          VDept         = new Vector();
          VGroup        = new Vector();
          VStatus       = new Vector();
          VDue          = new Vector();
          VUnit         = new Vector();
          VId           = new Vector();
          VHodName      = new Vector();
          VMrsOrdNo     = new Vector();
          VMrsFlag      = new Vector();
          VMrsTypeCode  = new Vector();
          VBudgetStatus = new Vector();

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
                              result        =  stat.executeQuery(getQString());
               
               while (result.next())
               {
                    VMrsNo       .addElement((String)result.getString(2));
                    VDate        .addElement(common.parseDate((String)result.getString(1)));
                    VRCode       .addElement((String)result.getString(3));
                    VRName       .addElement((String)result.getString(4));
                    VQty         .addElement((String)result.getString(5));
                    VDept        .addElement((String)result.getString(6));
                    VGroup       .addElement((String)result.getString(7));
                    VDue         .addElement(common.parseDate((String)result.getString(8)));
                    VUnit        .addElement((String)result.getString(9));
                    VId          .addElement((String)result.getString(10));
                    VStatus      .addElement(" ");
                    VHodName     .addElement((String)result.getString(11));
                    VMrsOrdNo    .addElement((String)result.getString(12));
                    VMrsFlag     .addElement((String)result.getString(13));
                    VMrsTypeCode .addElement((String)result.getString(14));
                    VBudgetStatus.addElement((String)result.getString(15));
               }
               result.close();
          }
          catch(Exception ex)
          {
               System.out.println("setDataIntoVector: "+ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VDate.size()][ColumnData.length];
          for(int i=0;i<VDate.size();i++)
          {
               RowData[i][0] = (String)VDate.elementAt(i);
               RowData[i][1] = (String)VMrsNo.elementAt(i);
               RowData[i][2] = (String)VRCode.elementAt(i);
               RowData[i][3] = (String)VRName.elementAt(i);
               RowData[i][4] = (String)VQty.elementAt(i);
               RowData[i][5] = (String)VUnit.elementAt(i);
               RowData[i][6] = (String)VDept.elementAt(i);
               RowData[i][7] = common.parseNull((String)VGroup.elementAt(i));
               RowData[i][8] = (String)VStatus.elementAt(i);
               RowData[i][9] = common.parseNull((String)VDue.elementAt(i));
          }  
     }

     public String getQString()
     {
          DateField df = new DateField();
          df.setTodayDate();
          String SToday = df.toNormal();
          String QString = "";
          
          if(JRPeriod.isSelected())
          {
               QString = "SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, InvItems.Item_Name , MRS.Qty, Dept.Dept_Name , Cata.Group_Name , MRS.DueDate, Unit.Unit_Name,MRS.Id,Hod.hodname,mrs.orderno,Mrs.MrsFlag,Mrs.MrsTypeCode,Mrs.BudgetStatus "+
                         "FROM (((MRS INNER JOIN InvItems ON MRS.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON MRS.Group_Code=Cata.Group_Code) INNER JOIN Unit ON MRS.Unit_Code=Unit.Unit_Code "+
                         "LEFT JOIN Hod ON MRS.HODCode=HOD.HodCode Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and Mrs.Qty >= 0 And Mrs.MrsDate >= '"+TStDate.toNormal()+"' and Mrs.MrsDate <='"+TEnDate.toNormal()+"' and Mrs.MillCode="+iMillCode;
          }
          else
          {
               QString = "SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, InvItems.Item_Name , MRS.Qty, Dept.Dept_Name , Cata.Group_Name , MRS.DueDate, Unit.Unit_Name,MRS.Id,Hod.hodname,mrs.orderno,Mrs.MrsFlag,Mrs.MrsTypeCode,Mrs.BudgetStatus "+
                         "FROM (((MRS INNER JOIN InvItems ON MRS.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON MRS.Group_Code=Cata.Group_Code) INNER JOIN Unit ON MRS.Unit_Code=Unit.Unit_Code "+
                         "LEFT JOIN Hod ON MRS.HODCode=HOD.HodCode Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and Mrs.Qty >= 0 And Mrs.MrsNo >="+TStNo.getText()+" and Mrs.MrsNo <="+TEnNo.getText()+" and Mrs.MillCode="+iMillCode;              
          }

          if(JCFilter.getSelectedIndex()==1)
               QString = QString+" and Mrs.DueDate <= '"+SToday+"'";
          if(JCFilter.getSelectedIndex()==2)
               QString = QString+" and Mrs.OrderNo = 0";
          
          QString = QString+" Order By "+TOrder.getText()+",MRS.Id";

          return QString;
     }

     private String getHodName(String SMrsNo)
     {
          int iIndex=-1;

          iIndex = VMrsNo.indexOf(SMrsNo);
          if(iIndex!=-1)
               return common.parseNull((String)VHodName.elementAt(iIndex));
          else
               return "";
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
}
