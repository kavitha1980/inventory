package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MRSListFrameTest extends JInternalFrame
{
     JButton        BApply;
     JPanel         TopPanel,BottomPanel;
     JPanel         First,Second,Third,Fourth,Fifth,Sixth;
     JComboBox      JCFilter,JCFilter1;
     JTextField     TOrder;
     JLayeredPane   DeskTop;
     JRadioButton   JRPeriod,JRNo;
     JTabbedPane    theTab;

     StatusPanel    SPanel;
     TabReport      tabreport,tabreport1;
     DateField     TStDate,TEnDate;
     NextField      TStNo,TEnNo;
     Common         common;

     Object         RowData[][],RowData1[][];
     String         ColumnData[] = {"Date","Mrs No","Code","Name","Qty","Unit","Department","Group","Status","Due Date","User","Remarks"};
     String         ColumnType[] = {"S","N","S","S","N","S","S","S","S","S","S","S"};
     int            iMillCode,iAuthCode,iUserCode;

     Vector VCode,VName,VNameCode,VCata,VDraw;
     Vector VPaperColor,VPaperSets,VPaperSize,VPaperSide,VSlipNo;

     String SItemTable;

     Vector VDate,VMrsNo,VRCode,VRName,VQty,VDept,VGroup,VStatus,VUnit,VDue,VId,VHodName,VMrsOrdNo,VMrsFlag,VMrsTypeCode,VBudgetStatus,VUserName,VRemarks;

     public MRSListFrameTest(JLayeredPane Layer,int iMillCode,int iAuthCode,int iUserCode,Vector VCode,Vector VName,Vector VNameCode,Vector VCata,Vector VDraw,Vector VPaperColor,Vector VPaperSets,Vector VPaperSize,Vector VPaperSide,Vector VSlipNo,String SItemTable)
     {
          super("MRSs Placed During a Period");
     
          this.DeskTop        = Layer;
          this.iMillCode      = iMillCode;
          this.iAuthCode      = iAuthCode;
          this.iUserCode      = iUserCode;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.VCata          = VCata;
          this.VDraw          = VDraw;

          this.VPaperColor    = VPaperColor;
          this.VPaperSets     = VPaperSets;
          this.VPaperSize     = VPaperSize;
          this.VPaperSide     = VPaperSide;
          this.VSlipNo        = VSlipNo;
          this.SItemTable     = SItemTable;

          common         = new Common();
     
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TStDate     = new DateField();
          TEnDate     = new DateField();
          BApply      = new JButton("Apply");
          TopPanel    = new JPanel();
          First       = new JPanel();
          Second      = new JPanel();
          Third       = new JPanel();
          Fourth      = new JPanel();
          Fifth       = new JPanel();
          Sixth       = new JPanel();
          BottomPanel = new JPanel();
          TOrder      = new JTextField("1,2,3");
          JCFilter    = new JComboBox();
          JCFilter1   = new JComboBox();
          TStNo       = new NextField(0);
          TEnNo       = new NextField(0);
     
          JRPeriod    = new JRadioButton("Periodical",true);
          JRNo        = new JRadioButton("MRS No",false);

     
          TStDate.setTodayDate();
          TEnDate.setTodayDate();
     }

     public void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(1,6));
          First     .setLayout(new GridLayout(1,1));
          Second    .setLayout(new GridLayout(1,1));
          Third     .setLayout(new GridLayout(2,1));
          Fourth    .setLayout(new GridLayout(2,1));
          Fifth     .setLayout(new GridLayout(1,1));
          Sixth     .setLayout(new GridLayout(1,1));
     
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          JCFilter.addItem("All");
          JCFilter.addItem("Over-Due");
          JCFilter.addItem("Order Not Placed");
          
          JCFilter1.addItem("All");
          JCFilter1.addItem("Regular MRS");
          JCFilter1.addItem("Monthly MRS");
          JCFilter1.addItem("Yearly MRS");


          First.setBorder(new TitledBorder("Sorted On (Column No)"));
          First.add(TOrder);
          
          Second.setBorder(new TitledBorder("Filter"));
          Second.add(JCFilter);
          
          Third.setBorder(new TitledBorder("Basis"));
          Third.add(JRPeriod);
          Third.add(JRNo);
          
          Fourth.setBorder(new TitledBorder("Periodical"));
          Fourth.add(TStDate);
          Fourth.add(TEnDate);
          
          Fifth.add(BApply);
          
          Sixth.setBorder(new TitledBorder("Filter"));
          Sixth.add(JCFilter1);

          TopPanel.add(First);
          TopPanel.add(Second);
          TopPanel.add(Sixth);
          TopPanel.add(Third);
          TopPanel.add(Fourth);
          TopPanel.add(Fifth);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

     }


     public void addListeners()
     {
          JRPeriod.addActionListener(new JRList());
          JRNo.addActionListener(new JRList()); 
          BApply.addActionListener(new ApplyList());
     }

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    Fourth.setBorder(new TitledBorder("Periodical"));
                    Fourth.removeAll();
                    Fourth.add(TStDate);
                    Fourth.add(TEnDate);
                    Fourth.updateUI();
                    JRNo.setSelected(false);
               }
               else
               {
                    Fourth.setBorder(new TitledBorder("Numbered"));
                    Fourth.removeAll();
                    Fourth.add(TStNo);
                    Fourth.add(TEnNo);
                    Fourth.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    setDataIntoVector();
                    setRowData();

               try
               {
                  getContentPane().remove(theTab);
               }
               catch(Exception ex)
               {
               }
               theTab      = new JTabbedPane();


               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.setBorder(new TitledBorder("List (Insert For Modification)"));

               tabreport1 = new TabReport(RowData1,ColumnData,ColumnType);
               tabreport1.setBorder(new TitledBorder("List (Insert For Modification)"));

               theTab.add(tabreport," Matched Items");
               theTab.add(tabreport1," Non Match Items");

               getContentPane().add(theTab,BorderLayout.CENTER);
     
               if(iAuthCode>1)
               {
                    //tabreport.ReportTable.addKeyListener(new KeyList());
                    //tabreport1.ReportTable.addKeyListener(new KeyList1());

               }
                    setSelected(true);
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
          }
     }

     /*public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if (ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    
                    int i = tabreport.ReportTable.getSelectedRow();
                    String SMrsNo       = (String)RowData[i][1];
                    String SMrsDate     = common.pureDate((String)RowData[i][0]);
                    String SName        = getHodName(SMrsNo);
                    String SMrsFlag     = (String)VMrsFlag.elementAt(i);
                    String SMrsTypeCode = (String)VMrsTypeCode.elementAt(i);
                    String SBudgetStatus= (String)VBudgetStatus.elementAt(i);

                    Runtime.getRuntime().freeMemory();
                    Runtime.getRuntime().gc();

                    MRSFrame mrsframe   = new MRSFrame(DeskTop,iUserCode,iMillCode,SMrsNo,SName,SMrsDate,SMrsFlag,SMrsTypeCode,SBudgetStatus,VCode,VName,VNameCode,VCata,VDraw,VPaperColor,VPaperSets,VPaperSize,VPaperSide,VSlipNo,SItemTable);

                    DeskTop.add(mrsframe);
                    mrsframe.show();
                    mrsframe.moveToFront();
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
          } 
     }
     public class KeyList1 extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if (ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    
                    int i = tabreport1.ReportTable.getSelectedRow();
                    String SMrsNo       = (String)RowData1[i][1];
                    String SMrsDate     = common.pureDate((String)RowData1[i][0]);
                    String SName        = getHodName(SMrsNo);
                    String SMrsFlag     = (String)VMrsFlag.elementAt(i);
                    String SMrsTypeCode = (String)VMrsTypeCode.elementAt(i);
                    String SBudgetStatus= (String)VBudgetStatus.elementAt(i);

                    Runtime.getRuntime().freeMemory();
                    Runtime.getRuntime().gc();

                    MRSFrame mrsframe   = new MRSFrame(DeskTop,iUserCode,iMillCode,SMrsNo,SName,SMrsDate,SMrsFlag,SMrsTypeCode,SBudgetStatus,VCode,VName,VNameCode,VCata,VDraw,VPaperColor,VPaperSets,VPaperSize,VPaperSide,VSlipNo,SItemTable);

                    DeskTop.add(mrsframe);
                    mrsframe.show();
                    mrsframe.moveToFront();
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
          } 
     }*/

     public void setDataIntoVector()
     {
          ResultSet result = null;

          VDate         = new Vector();
          VMrsNo        = new Vector();
          VRCode        = new Vector();
          VRName        = new Vector();
          VQty          = new Vector();
          VDept         = new Vector();
          VGroup        = new Vector();
          VStatus       = new Vector();
          VDue          = new Vector();
          VUnit         = new Vector();
          VId           = new Vector();
          VHodName      = new Vector();
          VMrsOrdNo     = new Vector();
          VMrsFlag      = new Vector();
          VMrsTypeCode  = new Vector();
          VBudgetStatus = new Vector();
          VUserName     = new Vector();
          VRemarks      = new Vector();

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
                              result        =  stat.executeQuery(getQString());
               
               while (result.next())
               {
                    VMrsNo       .addElement((String)result.getString(2));
                    VDate        .addElement(common.parseDate((String)result.getString(1)));
                    VRCode       .addElement((String)result.getString(3));
                    VRName       .addElement((String)result.getString(4));
                    VQty         .addElement((String)result.getString(5));
                    VDept        .addElement((String)result.getString(6));
                    VGroup       .addElement((String)result.getString(7));
                    VDue         .addElement(common.parseDate((String)result.getString(8)));
                    VUnit        .addElement((String)result.getString(9));
                    VId          .addElement((String)result.getString(10));
                    VStatus      .addElement(" ");
                    VHodName     .addElement((String)result.getString(11));
                    VMrsOrdNo    .addElement((String)result.getString(12));
                    VMrsFlag     .addElement((String)result.getString(13));
                    VMrsTypeCode .addElement((String)result.getString(14));
                    VBudgetStatus.addElement((String)result.getString(15));
                    VUserName    .addElement((String)result.getString(16));
                    VRemarks     .addElement((String)result.getString(17));
               }
               result.close();

               result        =  stat.executeQuery(getNonItemQS());
               while (result.next())
               {
                    VMrsNo       .addElement((String)result.getString(2));
                    VDate        .addElement(common.parseDate((String)result.getString(1)));
                    VRCode       .addElement((String)result.getString(3));
                    VRName       .addElement((String)result.getString(4));
                    VQty         .addElement((String)result.getString(5));
                    VDept        .addElement((String)result.getString(6));
                    VGroup       .addElement((String)result.getString(7));
                    VDue         .addElement(common.parseDate((String)result.getString(8)));
                    VUnit        .addElement((String)result.getString(9));
                    VId          .addElement((String)result.getString(10));
                    VStatus      .addElement(" ");
                    VHodName     .addElement((String)result.getString(11));
                    VMrsOrdNo    .addElement((String)result.getString(12));
                    VMrsFlag     .addElement((String)result.getString(13));
                    VMrsTypeCode .addElement((String)result.getString(14));
                    VBudgetStatus.addElement((String)result.getString(15));
                    VUserName    .addElement((String)result.getString(16));
                    VRemarks     .addElement((String)result.getString(17));
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("setDataIntoVector: "+ex);
          }
     }

     public void setRowData()
     {
          int iSize    = getItemSize();
          int iNonSize = getNonItemSize();


          System.out.println(iSize+","+iNonSize);

          RowData     = new Object[iSize][ColumnData.length];
          RowData1    = new Object[iNonSize][ColumnData.length];

          int j=0,k=0;
          for(int i=0;i<VDate.size();i++)
          {
               
               if(((String)VRCode.elementAt(i)).equals("-1"))
               {
                    RowData1[j][0]  = common.parseNull( (String)VDate.elementAt(i));
                    RowData1[j][1]  = common.parseNull((String)VMrsNo.elementAt(i));
                    RowData1[j][2]  = common.parseNull((String)VRCode.elementAt(i));
                    RowData1[j][3]  = common.parseNull((String)VRName.elementAt(i));
                    RowData1[j][4]  = common.parseNull((String)VQty.elementAt(i));
                    RowData1[j][5]  = common.parseNull((String)VUnit.elementAt(i));
                    RowData1[j][6]  = common.parseNull((String)VDept.elementAt(i));
                    RowData1[j][7]  = common.parseNull((String)VGroup.elementAt(i));
                    RowData1[j][8]  = (String)VStatus.elementAt(i);
                    RowData1[j][9]  = common.parseNull((String)VDue.elementAt(i));
                    RowData1[j][10] = common.parseNull((String)VUserName.elementAt(i));
                    RowData1[j][11] = common.parseNull((String)VRemarks.elementAt(i));
                    j++;
               }
               else
               {
                    RowData[k][0]  = (String)VDate.elementAt(i);
                    RowData[k][1]  = (String)VMrsNo.elementAt(i);
                    RowData[k][2]  = (String)VRCode.elementAt(i);
                    RowData[k][3]  = (String)VRName.elementAt(i);
                    RowData[k][4]  = (String)VQty.elementAt(i);
                    RowData[k][5]  = (String)VUnit.elementAt(i);
                    RowData[k][6]  = (String)VDept.elementAt(i);
                    RowData[k][7]  = common.parseNull((String)VGroup.elementAt(i));
                    RowData[k][8]  = (String)VStatus.elementAt(i);
                    RowData[k][9]  = common.parseNull((String)VDue.elementAt(i));
                    RowData[k][10] = common.parseNull((String)VUserName.elementAt(i));
                    RowData[k][11] = common.parseNull((String)VRemarks.elementAt(i));
                    k++;
               }
          }
     }
     private int getItemSize()
     {
          int iSize=0;
          for(int i=0;i<VDate.size();i++)
          {
               
               if(!((String)VRCode.elementAt(i)).equals("-1"))
               {
                   iSize++;
               }
          }
          return iSize;
     }
     private int getNonItemSize()
     {
          int iSize=0;
          for(int i=0;i<VDate.size();i++)
          {
               
               if(((String)VRCode.elementAt(i)).equals("-1"))
               {
                   iSize++;
               }
          }
          return iSize;
     }


     public String getQString()
     {
          DateField df = new DateField();
          df.setTodayDate();
          String SToday = df.toNormal();
          String QString = "";
          
          if(JRPeriod.isSelected())
          {
               QString = "SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, InvItems.Item_Name , MRS.Qty, Dept.Dept_Name , Cata.Group_Name , MRS.DueDate, Unit.Unit_Name,MRS.Id,Hod.hodname,mrs.orderno,Mrs.MrsFlag,Mrs.MrsTypeCode,Mrs.BudgetStatus,RawUser.UserName,Mrs.Remarks "+
                         "FROM (((MRS INNER JOIN InvItems ON MRS.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON MRS.Group_Code=Cata.Group_Code) INNER JOIN Unit ON MRS.Unit_Code=Unit.Unit_Code "+
                         " Inner Join RawUser on decode(Mrs.MrsAuthUserCode,null,1,0,1,Mrs.MrsAuthUserCode) = RawUser.UserCode "+
                         "LEFT JOIN Hod ON MRS.HODCode=HOD.HodCode Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and Mrs.Qty >= 0 And Mrs.MrsDate >= '"+TStDate.toNormal()+"' and Mrs.MrsDate <='"+TEnDate.toNormal()+"' and Mrs.MillCode="+iMillCode;
          }
          else
          {
               QString = "SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, InvItems.Item_Name , MRS.Qty, Dept.Dept_Name , Cata.Group_Name , MRS.DueDate, Unit.Unit_Name,MRS.Id,Hod.hodname,mrs.orderno,Mrs.MrsFlag,Mrs.MrsTypeCode,Mrs.BudgetStatus,RawUser.UserName,Mrs.Remarks "+
                         "FROM (((MRS INNER JOIN InvItems ON MRS.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON MRS.Group_Code=Cata.Group_Code) INNER JOIN Unit ON MRS.Unit_Code=Unit.Unit_Code "+
                         " Inner Join RawUser on decode(Mrs.MrsAuthUserCode,null,1,0,1,Mrs.MrsAuthUserCode) = RawUser.UserCode "+
                         "LEFT JOIN Hod ON MRS.HODCode=HOD.HodCode Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and Mrs.Qty >= 0 And Mrs.MrsNo >="+TStNo.getText()+" and Mrs.MrsNo <="+TEnNo.getText()+" and Mrs.MillCode="+iMillCode;              
          }

          if(JCFilter.getSelectedIndex()==1)
               QString = QString+" and Mrs.DueDate <= '"+SToday+"'";
          if(JCFilter.getSelectedIndex()==2)
               QString = QString+" and Mrs.OrderNo = 0";
          
          if(JCFilter1.getSelectedIndex()==1)
               QString = QString+" and (Mrs.PlanMonth = 0 or Mrs.PlanMonth is null) ";
          if(JCFilter1.getSelectedIndex()==2)
               QString = QString+" and Mrs.PlanMonth > 0 ";
          if(JCFilter1.getSelectedIndex()==3)
               QString = QString+" and Mrs.YearlyPlanningStatus > 0 ";

          QString = QString+" Order By "+TOrder.getText()+",MRS.Id";

          return QString;
     }
     private String getNonItemQS()
     {

          String QS  = "SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, decode(InvItems.Item_Name,null,mrs.itemname,InvItems.Item_Name) , MRS.Qty, Dept.Dept_Name , Cata.Group_Name , MRS.DueDate, Unit.Unit_Name,MRS.Id,Hod.hodname,mrs.orderno,Mrs.MrsFlag,Mrs.MrsTypeCode,Mrs.BudgetStatus,RawUser.UserName,Mrs.Remarks "+
                       "FROM (((MRS Left JOIN InvItems ON MRS.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON MRS.Group_Code=Cata.Group_Code) INNER JOIN Unit ON MRS.Unit_Code=Unit.Unit_Code "+
                       " Inner Join RawUser on decode(Mrs.MrsAuthUserCode,null,1,0,1,Mrs.MrsAuthUserCode) = RawUser.UserCode "+
                       "LEFT JOIN Hod ON MRS.HODCode=HOD.HodCode Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and Mrs.Qty >= 0 And Mrs.MrsDate >= '"+TStDate.toNormal()+"' and Mrs.MrsDate <='"+TEnDate.toNormal()+"' and Mrs.MillCode="+iMillCode+" and InvItems.Item_Name is null";

          return QS;
     }

     private String getHodName(String SMrsNo)
     {
          int iIndex=-1;

          iIndex = VMrsNo.indexOf(SMrsNo);
          if(iIndex!=-1)
               return common.parseNull((String)VHodName.elementAt(iIndex));
          else
               return "";
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
}
