package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;
import MRS.*;


public class MRSListPrint extends JInternalFrame
{
     JPanel         TopPanel;
     JPanel         BottomPanel;
     
     String         SDate;
     JComboBox      JCFilter;
     JButton        BApply,BPrint,BPrint2C;
     JTextField     TFile;
     
     DateField      TStDate,TEnDate;
     
     String         SInt = "  ";
     String         Strline = "";
     int            iLen=0;
     String         SDepName,SODepName;
     
     Object         RowData[][];
     String         ColumnData[] = {"Date","MRS No","Code","Name","Quantity","Catl","Draw","Make","Dept","Due Date","User Name","Remarks"};
     String         ColumnType[] = {"S","N","S","S","N","S","S","S","S","S","S","S"};
     
     JLayeredPane   DeskTop;
     
     Common         common;
     
     Vector         VMrsDate,VMrsNo,VMrsCode,VMrsName,VRemarks,VMake,VCatl,VDraw,VMrsQty,VDeptCode,VDeptName,VDueDate,VNature,VBlock,VGroup,VUserName;
     Vector         VFreq,VUoM;
     TabReport1     tabreport;
     int            iMillCode,iAuthCode,iUserCode;
     String         SItemTable,SSupTable,SMillName;
     
     public MRSListPrint(JLayeredPane DeskTop,int iMillCode,int iAuthCode,int iUserCode,String SItemTable,String SSupTable,String SMillName)
     {
          super("MRSs Placed during a Period");
          this.DeskTop = DeskTop;
          this.iMillCode     = iMillCode;
          this.iAuthCode     = iAuthCode;
          this.iUserCode     = iUserCode;
          this.SItemTable    = SItemTable;
          this.SSupTable     = SSupTable;
          this.SMillName     = SMillName;
          
          common = new Common();
          
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     
     public void createComponents()
     {
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();
          
          TStDate  = new DateField();
          TEnDate  = new DateField();
          JCFilter = new JComboBox();
          BApply   = new JButton("Apply");
          BPrint   = new JButton("Print");
          BPrint2C = new JButton("Print-2C");
          TFile    = new JTextField(15);
          
          TStDate.setTodayDate();
          TEnDate.setTodayDate();
     }
         
     public void setLayouts()
     {
          TopPanel.setLayout(new GridLayout(1,5));
          
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }

     public void addComponents()
     {
          JCFilter.addItem("All");
          JCFilter.addItem("Regular MRS");
          JCFilter.addItem("Monthly MRS");
          JCFilter.addItem("Yearly MRS");


          TopPanel.add(new JLabel("Period"));
          TopPanel.add(TStDate);
          TopPanel.add(TEnDate);
          TopPanel.add(JCFilter);
          TopPanel.add(BApply);
          
          BottomPanel.add(BPrint);
          BottomPanel.add(BPrint2C);
          BottomPanel.add(TFile);
          
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply.addActionListener(new ApplyList());
          BPrint.addActionListener(new PrintList());
          BPrint2C.addActionListener(new Print2CList());
     }

     public class Print2CList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    new Print2C(TStDate,TEnDate,TFile,iMillCode,SMillName,SSupTable);
                    removeHelpFrame();
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
          }
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               String    STitle    = "MRSs List For the Period From "+TStDate.toString()+" To "+TEnDate.toString()+" \n";
               Vector    VHead     = getPendingHead();
                         iLen      = ((String)VHead.elementAt(0)).length();
                         Strline   = common.Replicate("-",iLen)+"\n";
               Vector    VBody     = getPendingBody();
               String    SFile     = TFile.getText();

               new DocPrint(VBody,VHead,STitle,SFile,iMillCode,SMillName);
               removeHelpFrame();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public Vector getPendingHead()
     {
          Vector vect = new Vector();
          
          String Head1[] = {"MRS Date","MRS No","Mat Code","Mat Name","Special","Item Make","Catalogue","Drawing No","Uom","Requested","Freq","Due Date","Nature","Stocking","Group"};
          String Head2[] = {"Information","Qty","Days","Type"};
          
          String Sha1=((String)Head1[0]).trim();
          String Sha2=((String)Head1[1]).trim();
          String Sha3=((String)Head1[2]).trim();
          String Sha4=((String)Head1[3]).trim();
          String Sha5=((String)Head1[4]).trim();
          String Sha6=((String)Head1[5]).trim();
          String Sha7=((String)Head1[6]).trim();
          String Sha8=((String)Head1[7]).trim();
          String Sha9=((String)Head1[8]).trim();
          String Sha10=((String)Head1[9]).trim();
          String Sha11=((String)Head1[10]).trim();
          String Sha12=((String)Head1[11]).trim();
          String Sha13=((String)Head1[12]).trim();
          String Sha14=((String)Head1[13]).trim();
          String Sha15=((String)Head1[14]).trim();
          
          
          String Shb1="";
          String Shb2="";
          String Shb3="";
          String Shb4="";
          String Shb5=((String)Head2[0]).trim();
          String Shb6="";
          String Shb7="";
          String Shb8="";
          String Shb9="";
          String Shb10=((String)Head2[1]).trim();
          String Shb11=((String)Head2[2]).trim();
          String Shb12="";
          String Shb13="";
          String Shb14=((String)Head2[3]).trim();
          String Shb15="";
          
          Sha1  = common.Pad(Sha1,10)+SInt;
          Sha2  = common.Rad(Sha2,6)+SInt;
          Sha3  = common.Pad(Sha3,9)+SInt;
          Sha4  = common.Pad(Sha4,40)+SInt;
          Sha5  = common.Pad(Sha5,25)+SInt;
          Sha6  = common.Pad(Sha6,15)+SInt;
          Sha7  = common.Pad(Sha7,15)+SInt;
          Sha8  = common.Pad(Sha8,15)+SInt;
          Sha9  = common.Pad(Sha9,5)+SInt;
          Sha10 = common.Rad(Sha10,10)+SInt;
          Sha11 = common.Rad(Sha11,6)+SInt;
          Sha12 = common.Pad(Sha12,10)+SInt;
          Sha13 = common.Pad(Sha13,12)+SInt;
          Sha14 = common.Pad(Sha14,5)+SInt;
          Sha15 = common.Pad(Sha15,15)+SInt;
          
          Shb1  = common.Pad(Shb1,10)+SInt;
          Shb2  = common.Rad(Shb2,6)+SInt;
          Shb3  = common.Pad(Shb3,9)+SInt;
          Shb4  = common.Pad(Shb4,40)+SInt;
          Shb5  = common.Pad(Shb5,25)+SInt;
          Shb6  = common.Pad(Shb6,15)+SInt;
          Shb7  = common.Pad(Shb7,15)+SInt;
          Shb8  = common.Pad(Shb8,15)+SInt;
          Shb9  = common.Pad(Shb9,5)+SInt;
          Shb10 = common.Rad(Shb10,10)+SInt;
          Shb11 = common.Rad(Shb11,6)+SInt;
          Shb12 = common.Pad(Shb12,10)+SInt;
          Shb13 = common.Pad(Shb13,12)+SInt;
          Shb14 = common.Pad(Shb14,5)+SInt;
          Shb15 = common.Pad(Shb15,15)+SInt;
          
          String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+"\n";
          String Strh2 = Shb1+Shb2+Shb3+Shb4+Shb5+Shb6+Shb7+Shb8+Shb9+Shb10+Shb11+Shb12+Shb13+Shb14+Shb15+"\n";
          vect.add(Strh1);
          vect.add(Strh2);

          return vect;
     }

     public Vector getPendingBody()
     {
          Vector vect = new Vector();
          for(int i=0;i<VMrsDate.size();i++)
          {
               if(i>0)
               {
                    SDepName  = ((String)VDeptName.elementAt(i)).trim();
                    SODepName = ((String)VDeptName.elementAt(i-1)).trim();
               }

               String Sda1  = (String)VMrsDate.elementAt(i);
               String Sda2  = (String)VMrsNo.elementAt(i);
               String Sda3  = (String)VMrsCode.elementAt(i);
               String Sda4  = (String)VMrsName.elementAt(i);
               String Sda5  = (String)VRemarks.elementAt(i);
               String Sda6  = (String)VMake.elementAt(i);
               String Sda7  = (String)VCatl.elementAt(i);
               String Sda8  = (String)VDraw.elementAt(i);
               String Sda9  = (String)VUoM.elementAt(i);
               String Sda10 = (String)VMrsQty.elementAt(i);
               String Sda11 = (String)VFreq.elementAt(i);
               String Sda12 = (String)VDueDate.elementAt(i);
               String Sda13 = (String)VNature.elementAt(i);
               String Sda14 = (String)VBlock.elementAt(i);
               String Sda15 = (String)VGroup.elementAt(i);
               
               Sda1  = common.Pad(Sda1,10)+SInt;
               Sda2  = common.Rad(Sda2,6)+SInt;
               Sda3  = common.Pad(Sda3,9)+SInt;
               Sda4  = common.Pad(Sda4,40)+SInt;
               Sda5  = common.Pad(Sda5,25)+SInt;
               Sda6  = common.Pad(Sda6,15)+SInt;
               Sda7  = common.Pad(Sda7,15)+SInt;
               Sda8  = common.Pad(Sda8,15)+SInt;
               Sda9  = common.Pad(Sda9,5)+SInt;
               Sda10 = common.Rad(Sda10,10)+SInt;
               Sda11 = common.Rad(Sda11,6)+SInt;
               Sda12 = common.Pad(Sda12,10)+SInt;
               Sda13 = common.Pad(Sda13,12)+SInt;
               Sda14 = common.Pad(Sda14,5)+SInt;
               Sda15 = common.Pad(Sda15,15)+SInt;
               
               String Sdb1 = (String)VDeptName.elementAt(i);
               Sdb1 = common.Pad(Sdb1,25);
               String Strd1 = common.Rad("Dept Name : ",25)+" "+Sdb1+"\n";
               
               String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+"\n";
               if(i==0)
               {
                    vect.add(Strd1);
                    vect.add(Strd);
               }
               else
               {
                    if(SDepName.equals(SODepName))
                         vect.add(Strd);
                    else
                    {
                         vect.add(Strline);
                         vect.add(Strd1);
                         vect.add(Strd);
                    }
               }
          }
          return vect;
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}

               try
               {
                    tabreport = new TabReport1(RowData,ColumnData,ColumnType);
                    getContentPane().add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public void setDataIntoVector()
     {
          VMrsDate  = new Vector();
          VMrsNo    = new Vector();
          VMrsCode  = new Vector();
          VMrsName  = new Vector();
          VRemarks  = new Vector();
          VMake     = new Vector();
          VCatl     = new Vector();
          VDraw     = new Vector();
          VMrsQty   = new Vector();
          VDeptCode = new Vector();
          VDeptName = new Vector();
          VDueDate  = new Vector();
          VNature   = new Vector();
          VBlock    = new Vector();
          VGroup    = new Vector();
          VFreq     = new Vector();
          VUoM      = new Vector();
          VUserName = new Vector();
          
          String SStDate   = TStDate.toNormal();
          String SEnDate   = TEnDate.toNormal();
          String QS = " ";
          
          QS = " SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, "+
               " InvItems.Item_Name, MRS.Remarks, MRS.Make, "+
               " MRS.Catl, MRS.Draw, MRS.Qty, MRS.Dept_Code, "+
               " Dept.Dept_Name,MRS.DueDate,Nature.Nature, "+
               " OrdBlock.BlockName,UOM.UoMName,cata.Group_Name,RawUser.UserName "+
               " FROM ((((MRS INNER JOIN CATA ON MRS.GROUP_CODE = CATA.GROUP_CODE) "+
               " INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) "+
               " Left Join Nature on MRS.NatureCode=Nature.NatureCode) "+
               " Left Join OrdBlock on MRS.BlockCode=OrdBlock.Block) "+
               " Inner Join InvItems on MRS.Item_Code=InvItems.Item_Code "+
               " Inner Join UOM on UOM.UOMCode=InvItems.UOMCode"+
               " Inner Join RawUser on decode(Mrs.MrsAuthUserCode,null,1,0,1,Mrs.MrsAuthUserCode) = RawUser.UserCode "+
               " WHERE (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) "+
               " and MRS.Qty > 0 And MRS.MrsDate >= '"+SStDate+"' "+
               " and MRS.MrsDate <= '"+SEnDate+"' "+
               " and MRS.MillCode="+iMillCode;

          if(JCFilter.getSelectedIndex()==1)
               QS = QS+" and Mrs.PlanMonth = 0 ";
          if(JCFilter.getSelectedIndex()==2)
               QS = QS+" and Mrs.PlanMonth > 0 ";
          if(JCFilter.getSelectedIndex()==3)
               QS = QS+" and Mrs.YearlyPlanningStatus > 0 ";

               QS = QS + " Order by 11,1,2,3 ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               ResultSet      res = stat.executeQuery(QS);

               while (res.next())
               {
                    String SMrsDate = common.parseDate(res.getString(1));
                    String SDueDate = common.parseDate(res.getString(12));
                    String SFreq    = common.getDateDiff(SDueDate,SMrsDate);
                    
                    double dReq     = common.toDouble(res.getString(9));
                    
                    VMrsDate  .addElement(SMrsDate);
                    VMrsNo    .addElement(res.getString(2));
                    VMrsCode  .addElement(res.getString(3));
                    VMrsName  .addElement(res.getString(4));
                    VRemarks  .addElement(res.getString(5));
                    VMake     .addElement(res.getString(6));
                    VCatl     .addElement(res.getString(7));
                    VDraw     .addElement(res.getString(8));
                    VMrsQty   .addElement(""+dReq);
                    VDeptCode .addElement(res.getString(10));
                    VDeptName .addElement(res.getString(11));
                    VDueDate  .addElement(SDueDate);
                    VNature   .addElement(res.getString(13));
                    VBlock    .addElement(res.getString(14));
                    VFreq     .addElement(SFreq);
                    VUoM      .addElement(res.getString(15));
                    VGroup    .addElement(res.getString(16));
                    VUserName .addElement(res.getString(17));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex){System.out.println("Mis"+ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VMrsDate.size()][ColumnData.length];
          for(int i=0;i<VMrsDate.size();i++)
          {
               RowData[i][0]  = (String)VMrsDate.elementAt(i);
               RowData[i][1]  = (String)VMrsNo.elementAt(i);
               RowData[i][2]  = (String)VMrsCode.elementAt(i);
               RowData[i][3]  = (String)VMrsName.elementAt(i);
               RowData[i][4]  = (String)VMrsQty.elementAt(i);
               RowData[i][5]  = (String)VCatl.elementAt(i);
               RowData[i][6]  = (String)VDraw.elementAt(i);
               RowData[i][7]  = (String)VMake.elementAt(i);
               RowData[i][8]  = (String)VDeptName.elementAt(i);
               RowData[i][9]  = (String)VDueDate.elementAt(i);
               RowData[i][10] = (String)VUserName.elementAt(i);
               RowData[i][11] = (String)VRemarks.elementAt(i);
          }  
     }
}
