package MRS;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import guiutil.*;
import util.*;

public class MRSModel extends DefaultTableModel 
{

     protected String SWhere;
     protected String SIndentNo;

     Vector theVector;

     String ColumnName[] = {"Sl No","Code","Name","Qty","Unit","Department","Group","Block","Urgent","Nature","Due Date","Catl","Draw","Make","Remarks"};
     String ColumnType[] = {"N","S","S","E","E","B","E","E","E","E","E","S","S","E","E"};
     String ColumnType1[]= {"N","S","S","S","S","S","S","S","S","S","S","S","S","S","S"};


     Common common = new Common();



     boolean bIndent=false;
     int iList=0;

     MRSModel(int iList)
     {
          this.iList=iList;
          setDataVector(getRowData(),ColumnName);

     }
     public Class getColumnClass(int col)
     {
          return getValueAt(0,col).getClass();
     }
     public boolean isCellEditable(int iRow,int iCol)
     {
          if(iList==1)
          {
               if(ColumnType1[iCol]=="B" || ColumnType1[iCol]=="E")
                    return true;
               return false;
          }
          else
          {
               if(ColumnType[iCol]=="B" || ColumnType[iCol]=="E")
                    return true;
               return false;
          }

     }


     public int getRows()
     {
          return super.dataVector.size();
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          RowData[0][0]= "";
          RowData[0][1]= "";
          RowData[0][2]= "";
          RowData[0][3]= "";
          RowData[0][4]= "";
          RowData[0][5]= "";
          RowData[0][6]= "";
          RowData[0][7]= "";
          RowData[0][8]= "";
          RowData[0][9]= "";
          RowData[0][10]= "";
          RowData[0][11]= "";
          RowData[0][12]= "";
          RowData[0][13]= "";
          RowData[0][14]= "";

          return RowData;
     }
     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }
     public String getFibreCode(int iRow)
     {
          return (String)getValueAt(iRow,0);
     }

     public String getFibreName(int iRow)
     {
          return (String)getValueAt(iRow,1);
     }

     public double getStockAt(int iRow)
     {
          return common.toDouble((String)getValueAt(iRow,6));
     }
     public String getValueFrom(int iRow,int iCol)
     {
          return (String)getValueAt(iRow,iCol);
     }
     public void setMasterDetails(String SValue,int iRow,int iCol)
     {
          setValueAt(SValue,iRow,iCol);                        
     }
     public void deleteRow(int index)
     {
         if(getRows()==1)
         return;
         if(index>=getRows())
         return;
         if(index==-1)
         return;
         removeRow(index);
     }
     public void appendEmptyRow(int iRow)
     {
          Vector temp = new Vector();

          temp.addElement(String.valueOf(iRow+1));
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");

          insertRow(getRows(),temp);

     } 

}

