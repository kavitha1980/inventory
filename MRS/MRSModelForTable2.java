package MRS;

import java.io.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;

import java.util.*;
import util.*;

public class MRSModelForTable2 extends DefaultTableModel
{
     String ColumnName[]  = {"Department","NetValue"};
     String ColumnType[]  = {"S"         ,"S"    }; 
     int    ColumnWidth[] = { 20         ,10     };

     public MRSModelForTable2()
     {
          setDataVector(getRowData(),ColumnName);
     }
     public Object[][] getRowData()
     {
          Object RowData[][] = new Object[0][ColumnName.length];

          return RowData;
     }
     public boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="B" || ColumnType[col]=="E")
               return true;
          return false;
     }
     public Class getColumnClass(int col)
     {
          return getValueAt(0,col).getClass();
     }
     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
          }
          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }
     public int getRows()
     {
         return super.dataVector.size(); 
     }
     public void appendEmptyRow()
     {
          Vector theVect = new Vector();

          theVect.addElement("");
          theVect.addElement("");

          insertRow(getRows(),theVect);
     }
  /*   public void deleteRow(int index)
     {
         if(index==-1)
         return;
         removeRow(index);
     }
 */
     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }  

}

