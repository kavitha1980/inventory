package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;

import jxl.CellReferenceHelper;
import jxl.CellView;
import jxl.HeaderFooter;
import jxl.Range;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.SheetSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.Orientation;
import jxl.format.PageOrientation;
import jxl.format.PaperSize;
import jxl.format.ScriptStyle;
import jxl.format.UnderlineStyle;
import jxl.write.Blank;
import jxl.write.DateFormat;
import jxl.write.DateFormats;
import jxl.write.DateTime;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormat;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFeatures;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableHyperlink;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class MRSPendingList extends JInternalFrame
{
     JPanel         TopPanel;
     JPanel         BottomPanel;
     JPanel         First,Second,Third,Fourth,Fifth,Sixth;
     
     String         SStDate,SEnDate;
     JComboBox      JCFilter,JCPending,JCDelay;
     JButton        BApply,BPrint,BPrint1A,BPrint2E,BPDF,BExcel;
     JTextField     TFile;
     
     DateField      TStDate,TEnDate;
     JRadioButton   JRAsOn,JRPeriod;
     
     String         SInt      = "  ";
     String         Strline   = "";
     int            iLen      = 0;
     String         SDepName,SODepName;
     
     JComboBox      JCUnit;
     Vector         VUnitCode,VUnit;
     
     Object         RowData[][];
     String         ColumnData[] = {"Date","MRS No","Code","Name","Quantity","Catl","Draw","Make","Dept","Due Date","Delay Days","User Name","Remarks"};
     String         ColumnType[] = {"S","N","S","S","N","S","S","S","S","S","N","S","S"};
     
     JLayeredPane   DeskTop;
     Vector         VCode,VName;
     StatusPanel    SPanel;
     
     Common common = new Common();
     
     Vector         VMrsDate,VMrsNo,VMrsCode,VMrsName,VRemarks,VMake,VCatl,VDraw,VMrsQty,VReOrd,VStock,VPending,VToOrder,VDeptCode,VDeptName,VDueDate,VNature,VBlock,VUoM,VGroup,VUserName,VStatus;
     Vector         VDueDays,VFreq,VOrdBlock,VOrdDate,VOrdNo,VSupName;
     Vector         VOrdRate,VOrdDisc,VOrdCenVat,VOrdSalTax,VSurChg;

     TabReport1     tabreport;
     int            iMillCode;
     String         SItemTable,SSupTable,SMillName;

//pdf
 
    Document document;
    PdfPCell c1;
    int iTotalColumns=16;
    int[] iWidth={14,10,13,30,8,24,15,19,15,10,10,10,14,30,11,8};
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 7, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 4, Font.BOLD);
    String              SFileName, SFileOpenPath, SFileWritePath; 
    PdfPTable table;
    PdfWriter writer;

//Excel Print

	WritableWorkbook workbook;
	WritableSheet sheet;

    int iRowCount = 1,iColCount=0;
    int Pctr=0;
    int iLctr = 0;

	jxl.write.WritableImage image;
    jxl.write.Label         label;

    JTextField              JTName;

	public MRSPendingList(JLayeredPane DeskTop,int iMillCode,int iAuthCode,int iUserCode,String SItemTable,String SSupTable,String SMillName)
    {
		super("MRSs Pending As On Date");
		this.DeskTop    = DeskTop;
		this.VCode      = VCode;
		this.VName      = VName;
		this.SPanel     = SPanel;
		this.iMillCode  = iMillCode;
		this.SItemTable = SItemTable;
		this.SSupTable  = SSupTable;
		this.SMillName  = SMillName;
	
		SFileName      	= common.getPrintPath()+"MRSPendingList.pdf";
     
		setUnitCombo(); 
		createComponents();
		setLayouts();
		addComponents();
		addListeners();
     }

     public void createComponents()
     {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          First          = new JPanel();
          Second         = new JPanel();
          Third          = new JPanel();
          Fourth         = new JPanel();
          Fifth          = new JPanel();
          Sixth          = new JPanel();
     
          TStDate        = new DateField();
          TEnDate        = new DateField();
          JCUnit         = new JComboBox(VUnit);
          JCFilter       = new JComboBox();
          JCPending      = new JComboBox();
		  JCDelay		 = new JComboBox();
          BApply         = new JButton("Apply");
          BPrint         = new JButton("Print");
          BPrint1A       = new JButton("Print1A");
          BPrint2E       = new JButton("Print2E");
          BPDF			 = new JButton("PDF");
          BExcel		 = new JButton("Excel Print");
          TFile          = new JTextField(15);

          JRAsOn         = new JRadioButton("As On",true);
          JRPeriod       = new JRadioButton("Periodical",false);

     
          TStDate.setTodayDate();
          TEnDate.setTodayDate();
     }

     public void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(1,5));
          First     .setLayout(new GridLayout(2,1));
          Second    .setLayout(new GridLayout(2,1));
          Third     .setLayout(new GridLayout(1,1));
          Fourth    .setLayout(new GridLayout(1,1));
          Fifth     .setLayout(new GridLayout(2,2));
          Sixth     .setLayout(new GridLayout(1,1));

     
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,700,575);
     }

     public void addComponents()
     {
          JCFilter.addItem("All");
          JCFilter.addItem("Regular MRS");
          JCFilter.addItem("Planning MRS");
          JCFilter.addItem("Yearly MRS");
		
		JCDelay.addItem("All");
		JCDelay.addItem("Delay Days > 10 Days");
		 

          JCPending.addItem("Mrs Converted");
          JCPending.addItem("Pending List With (SO/JMD)");
//          JCPending.addItem("All");
          

          First.setBorder(new TitledBorder("Basis"));
          First.add(JRAsOn);
          First.add(JRPeriod);
          
          Second.setBorder(new TitledBorder("As On"));
          Second.add(TEnDate);
          Second.add(new JLabel(""));

          Third.setBorder(new TitledBorder("Unit"));
          Third.add(JCUnit);

          Fourth.setBorder(new TitledBorder("Filter"));
          Fourth.add(JCFilter);

          Fifth.setBorder(new TitledBorder(" Status Filter"));
		  Fifth.add(new JLabel("Status"));
          Fifth.add(JCPending);

  		  Fifth.add(new JLabel("Delay"));
          Fifth.add(JCDelay);


          Sixth.add(BApply);

          TopPanel.add(First);
          TopPanel.add(Second);
          TopPanel.add(Third);
          TopPanel.add(Fourth);
          TopPanel.add(Fifth);
          TopPanel.add(Sixth);


          BottomPanel    . add(BPrint);
          BottomPanel    . add(BPrint1A);
          BottomPanel    . add(BPrint2E);
		  BottomPanel    . add(BPDF);
		  BottomPanel    . add(BExcel);
          BottomPanel    . add(TFile);
     
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          JRPeriod  .addActionListener(new JRList());
          JRAsOn    .addActionListener(new JRList()); 

          BApply    . addActionListener(new ApplyList());

          BPrint    . addActionListener(new PrintList());
          BPrint1A  . addActionListener(new PrintList1A());
          BPrint2E  . addActionListener(new Print2EList());
		  BPDF      . addActionListener(new PDFList());
		  BExcel    . addActionListener(new ExcelPrint());
     }

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    Second.setBorder(new TitledBorder("Periodical"));
                    Second.removeAll();
                    Second.add(TStDate);
                    Second.add(TEnDate);
                    Second.updateUI();
                    JRAsOn.setSelected(false);
               }
               else
               {
                    Second.setBorder(new TitledBorder("As On"));
                    Second.removeAll();
                    Second.add(TEnDate);
                    Second.add(new JLabel(""));
                    Second.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }

     public class Print2EList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               new Print2e(TEnDate,TFile,iMillCode,SItemTable,SSupTable,SMillName);
               removeHelpFrame();
          }            
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               String STitle="";

               if(JRPeriod.isSelected())
               {
                    STitle    = "MRSs Pending List From "+TStDate.toString()+"  To "+TEnDate.toString()+" \n";
               }
               else
               {
                    STitle    = "MRSs Pending List As On "+TEnDate.toString()+" \n";
               }

               Vector    VHead     = getPendingHead();
                         iLen      = ((String)VHead.elementAt(0)).length();
                         Strline   = common.Replicate("-",iLen)+"\n";
               Vector    VBody     = getPendingBody();
               String    SFile     = TFile.getText();
               new       DocPrint(VBody,VHead,STitle,SFile,iMillCode,SMillName);
               removeHelpFrame();
          }
     }

     public class PrintList1A implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {

               String STitle = "";

               if(JRPeriod.isSelected())
               {
                    STitle = "MRSs Pending List with Last PO Details From "+TStDate.toString()+" To "+TEnDate.toString()+" \n";
               }
               else
               {
                    STitle = "MRSs Pending List with Last PO Details As On "+TEnDate.toString()+" \n";
               }

               Vector    VHead     = getPendingHead1A();
                         iLen      = ((String)VHead.elementAt(0)).length();
                         Strline   = common.Replicate("-",iLen)+"\n";
               Vector    VBody     = getPendingBody1A();
               String    SFile     = TFile.getText();
               new       DocPrint(VBody,VHead,STitle,SFile,iMillCode,SMillName);
               removeHelpFrame();
          }
     }
	public class PDFList implements ActionListener
    {
          public void actionPerformed(ActionEvent ae)
          {
               CreatePDF();
          }
    }
	public class ExcelPrint implements ActionListener
	{
          public void actionPerformed(ActionEvent ae)
          {
               CreateExcelFile();
          }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public Vector getPendingHead()
     {
          Vector vect = new Vector();
          
          String Head1[] = {"MRS Date","MRS No","Mat Code","Mat Name","UoM","Special","Item Make","Catalogue","Drawing No","Requested","ReOrder","Stock","Pending @","To be","Freq","Due Date","Nature","Stocking","Group","Delay","Pending"};
          String Head2[] = {"Information","Qty","Qty","Qty","Order Qty","Ordered Qty","Days","Type","Days","Status"};
          
          String Sha1    =    ((String)Head1[0]).trim();
          String Sha2    =    ((String)Head1[1]).trim();
          String Sha3    =    ((String)Head1[2]).trim();
          String Sha4    =    ((String)Head1[3]).trim();
          String Sha5    =    ((String)Head1[4]).trim();
          String Sha6    =    ((String)Head1[5]).trim();
          String Sha7    =    common.parseNull((String)Head1[6]);
          String Sha8    =    common.parseNull((String)Head1[7]);
          String Sha9    =    ((String)Head1[8]).trim();
          String Sha10   =    ((String)Head1[9]).trim();
          String Sha11   =    ((String)Head1[10]).trim();
          String Sha12   =    ((String)Head1[11]).trim();
          String Sha13   =    ((String)Head1[12]).trim();
          String Sha14   =    ((String)Head1[13]).trim();
          String Sha15   =    ((String)Head1[14]).trim();
          String Sha16   =    ((String)Head1[15]).trim();
          String Sha17   =    ((String)Head1[16]).trim();
          String Sha18   =    ((String)Head1[17]).trim();
          String Sha19   =    ((String)Head1[18]).trim();
          String Sha20   =    ((String)Head1[19]).trim();
          String Sha21   =    ((String)Head1[20]).trim();
          
          String Shb1    =    "";
          String Shb2    =    "";
          String Shb3    =    "";
          String Shb4    =    "";
          String Shb5    =    "";
          String Shb6    =    ((String)Head2[0]).trim();
          String Shb7    =    "";
          String Shb8    =    "";
          String Shb9    =    "";
          String Shb10   =    ((String)Head2[1]).trim();
          String Shb11   =    ((String)Head2[2]).trim();
          String Shb12   =    ((String)Head2[3]).trim();
          String Shb13   =    ((String)Head2[4]).trim();
          String Shb14   =    ((String)Head2[5]).trim();
          String Shb15   =    ((String)Head2[6]).trim();
          String Shb16   =    "";
          String Shb17   =    "";
          String Shb18   =    ((String)Head2[7]).trim();
          String Shb19   =    "";
          String Shb20   =    ((String)Head2[8]).trim();
          String Shb21   =    ((String)Head2[9]).trim();

          
          Sha1  = common.Pad(Sha1,10);
          Sha2  = common.Rad(Sha2,9)+SInt;
          Sha3  = common.Pad(Sha3,9)+SInt;
          Sha4  = common.Pad(Sha4,40)+SInt;
          Sha5  = common.Pad(Sha5,7)+SInt;
          Sha6  = common.Pad(Sha6,25)+SInt;
          Sha7  = common.Pad(Sha7,10)+SInt;
          Sha8  = common.Pad(Sha8,10)+SInt;
          Sha9  = common.Pad(Sha9,15)+SInt;
          Sha10 = common.Rad(Sha10,10);
          Sha11 = common.Rad(Sha11,10);
          Sha12 = common.Rad(Sha12,10)+SInt;
          Sha13 = common.Rad(Sha13,10)+SInt;
          Sha14 = common.Rad(Sha14,12);
          Sha15 = common.Rad(Sha15,6)+SInt;
          Sha16 = common.Pad(Sha16,10)+SInt;
          Sha17 = common.Pad(Sha17,12);
          Sha18 = common.Pad(Sha18,5)+SInt;
          Sha19 = common.Pad(Sha19,5);
          Sha20 = common.Rad(Sha20,15);
          Sha21 = common.Rad(Sha21,15);
          
          Shb1  = common.Pad(Shb1,10);
          Shb2  = common.Rad(Shb2,9)+SInt;
          Shb3  = common.Pad(Shb3,9)+SInt;
          Shb4  = common.Pad(Shb4,40)+SInt;
          Shb5  = common.Pad(Shb5,7)+SInt;
          Shb6  = common.Pad(Shb6,25)+SInt;
          Shb7  = common.Pad(Shb7,10)+SInt;
          Shb8  = common.Pad(Shb8,10)+SInt;
          Shb9  = common.Pad(Shb9,15)+SInt;
          Shb10 = common.Rad(Shb10,10);
          Shb11 = common.Rad(Shb11,10);
          Shb12 = common.Rad(Shb12,10)+SInt;
          Shb13 = common.Rad(Shb13,10)+SInt;
          Shb14 = common.Rad(Shb14,12);
          Shb15 = common.Rad(Shb15,6)+SInt;
          Shb16 = common.Pad(Shb16,10)+SInt;
          Shb17 = common.Pad(Shb17,12);
          Shb18 = common.Pad(Shb18,5)+SInt;
          Shb19 = common.Pad(Shb19,5);
          Shb20 = common.Rad(Shb20,15);
          Shb21 = common.Rad(Shb21,15);
          
          String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+Sha16+Sha17+Sha18+Sha19+Sha20+Sha21+"\n";
          String Strh2 = Shb1+Shb2+Shb3+Shb4+Shb5+Shb6+Shb7+Shb8+Shb9+Shb10+Shb11+Shb12+Shb13+Shb14+Shb15+Shb16+Shb17+Shb18+Shb19+Shb20+Shb21+"\n";
          vect.add(Strh1);
          vect.add(Strh2);
          return vect;
     }

     public Vector getPendingBody()
     {
          Vector vect = new Vector();
          for(int i=0;i<VMrsDate.size();i++)
          {
               if(i>0)
               {
                    SDepName  = ((String)VDeptName.elementAt(i)).trim();
                    SODepName = ((String)VDeptName.elementAt(i-1)).trim();
               }
                String SMrsDate = common.parseDate((String)VMrsDate  .elementAt(i));
		       String SDays    = common.getDateDiff(common.parseDate(SEnDate),SMrsDate);
			   
               String Sda1  = common.parseDate((String)VMrsDate    . elementAt(i));
               String Sda2  = (String)VMrsNo      . elementAt(i);
               String Sda3  = (String)VMrsCode    . elementAt(i);
               String Sda4  = (String)VMrsName    . elementAt(i);
               String Sda5  = (String)VUoM        . elementAt(i);
               String Sda6  = (String)VRemarks    . elementAt(i);
               String Sda7  = (String)VMake       . elementAt(i);
               String Sda8  = (String)VCatl       . elementAt(i);
               String Sda9  = (String)VDraw       . elementAt(i);
               String Sda10 = (String)VMrsQty     . elementAt(i);
               String Sda11 = (String)VReOrd      . elementAt(i);
               String Sda12 = (String)VStock      . elementAt(i);
               String Sda13 = (String)VPending    . elementAt(i);
               String Sda14 = (String)VToOrder    . elementAt(i);
               String Sda15 = (String)VFreq       . elementAt(i);
               String Sda16 = (String)VDueDate    . elementAt(i);
               String Sda17 = (String)VNature     . elementAt(i);
               String Sda18 = (String)VBlock      . elementAt(i);
               String Sda19 = (String)VGroup      . elementAt(i);
              // String Sda20 = (String)VDueDays    . elementAt(i);
			   String Sda20 = SDays;
                  String Sda21 ="";
               if(JCPending.getSelectedIndex()==1 || JCPending.getSelectedIndex()==2)
                    Sda21 = (String)VStatus    . elementAt(i);
               else
                    Sda21 = "";
               
               Sda1  = common.Pad(Sda1,10);
               Sda2  = common.Rad(Sda2,9)+SInt;
               Sda3  = common.Pad(Sda3,9)+SInt;
               Sda4  = common.Pad(Sda4,40)+SInt;
               Sda5  = common.Pad(Sda5,7)+SInt;
               Sda6  = common.Pad(Sda6,25)+SInt;
               Sda7  = common.Pad(Sda7,10)+SInt;
               Sda8  = common.Pad(Sda8,10)+SInt;
               Sda9  = common.Pad(Sda9,15)+SInt;
               Sda10 = common.Rad(Sda10,10);
               Sda11 = common.Rad(Sda11,10);
               Sda12 = common.Rad(Sda12,10)+SInt;
               Sda13 = common.Rad(Sda13,10)+SInt;
               Sda14 = common.Rad(Sda14,12);
               Sda15 = common.Rad(Sda15,6)+SInt;
               Sda16 = common.Pad(Sda16,10)+SInt;
               Sda17 = common.Pad(Sda17,12);
               Sda18 = common.Pad(Sda18,5)+SInt;
               Sda19 = common.Pad(Sda19,15);
               Sda20 = common.Rad(Sda20,5);
               Sda21 = common.Rad(Sda21,15);

               
               String Sdb1 = (String)VDeptName.elementAt(i);
               Sdb1 = common.Pad(Sdb1,25);
               String Strd1 = common.Rad("Dept Name : ",25)+" "+Sdb1+"\n";
               String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+Sda16+Sda17+Sda18+Sda19+Sda20+Sda21+"\n";
               if(i==0)
               {
                    vect.add(Strd1);
                    vect.add(Strd);
               }
               else
               {
                    if(SDepName.equals(SODepName))
                         vect.add(Strd);
                    else
                    {
                         vect.add(Strline);
                         vect.add(Strd1);
                         vect.add(Strd);
                    }
               }
          }
          return vect;
     }

     public Vector getPendingHead1A()
     {
          Vector vect = new Vector();
          
          String Head1[] = {"MRS Date","MRS No","Mat Code","Mat Name","UoM","Special",    "Item Make","Catalogue","Drawing No","Requested","Freq","Due Date","Stock","Delay","OrderNo","Order","Order","Supplier","Rate","Disc","CenVa","St","Sur"};
          String Head2[] = {                                                "Information"                                     , "Qty"     ,"Days",           "Type" ,"Days" ,          "Block","Date" ,"Name"    ,"Rs"  ,"%"   ,"%"   ,"%"   ,"%"};
          
          String Sha1    = ((String)Head1[0]).trim();
          String Sha2    = ((String)Head1[1]).trim();
          String Sha3    = ((String)Head1[2]).trim();
          String Sha4    = ((String)Head1[3]).trim();
          String Sha5    = ((String)Head1[4]).trim();
          String Sha6    = ((String)Head1[5]).trim();
          String Sha7    = common.parseNull((String)Head1[6]);
          String Sha8    = common.parseNull((String)Head1[7]);
          String Sha9    = ((String)Head1[8]).trim();
          String Sha10   = ((String)Head1[9]).trim();
          String Sha11   = ((String)Head1[10]).trim();
          String Sha12   = ((String)Head1[11]).trim();
          String Sha13   = ((String)Head1[12]).trim();
          String Sha14   = ((String)Head1[13]).trim();
          String Sha15   = ((String)Head1[14]).trim();
          String Sha16   = ((String)Head1[15]).trim();
          String Sha17   = ((String)Head1[16]).trim();
          String Sha18   = ((String)Head1[17]).trim();
          String Sha19   = ((String)Head1[18]).trim();
          String Sha20   = ((String)Head1[19]).trim();
          String Sha21   = ((String)Head1[20]).trim();
          String Sha22   = ((String)Head1[21]).trim();
          String Sha23   = ((String)Head1[22]).trim();
          
          String Shb1    = "";
          String Shb2    = "";
          String Shb3    = "";
          String Shb4    = "";
          String Shb5    = "";
          String Shb6    = ((String)Head2[0])     .trim();
          String Shb7    = "";
          String Shb8    = "";
          String Shb9    = "";
          String Shb10   = ((String)Head2[1])     .trim();
          String Shb11   = ((String)Head2[2])     .trim();
          String Shb12   = "";
          String Shb13   = ((String)Head2[3])     .trim();
          String Shb14   = ((String)Head2[4])     .trim();
          String Shb15   = "";
          String Shb16   = ((String)Head2[5])     .trim();
          String Shb17   = ((String)Head2[6])     .trim();
          String Shb18   = ((String)Head2[7])     .trim();
          String Shb19   = ((String)Head2[8])     .trim();
          String Shb20   = ((String)Head2[9])     .trim();
          String Shb21   = ((String)Head2[10])    .trim();
          String Shb22   = ((String)Head2[11])    .trim();
          String Shb23   = ((String)Head2[12])    .trim();

          Sha1  = common.Cad(Sha1,10)+SInt;
          Sha2  = common.Cad(Sha2,9)+SInt;
          Sha3  = common.Cad(Sha3,9)+SInt;
          Sha4  = common.Cad(Sha4,40)+SInt;
          Sha5  = common.Cad(Sha5,7)+SInt;
          Sha6  = common.Cad(Sha6,25)+SInt;
          Sha7  = common.Cad(Sha7,10)+SInt;
          Sha8  = common.Cad(Sha8,10)+SInt;
          Sha9  = common.Cad(Sha9,15)+SInt;
          Sha10 = common.Cad(Sha10,10)+SInt;
          Sha11 = common.Cad(Sha11,5)+SInt;
          Sha12 = common.Cad(Sha12,10)+SInt;
          Sha13 = common.Cad(Sha13,5)+SInt;
          Sha14 = common.Cad(Sha14,5)+SInt;
          Sha15 = common.Cad(Sha15,12)+SInt;
          Sha16 = common.Cad(Sha16,5)+SInt;
          Sha17 = common.Cad(Sha17,10)+SInt;
          Sha18 = common.Cad(Sha18,15)+SInt;
          Sha19 = common.Cad(Sha19,13);
          Sha20 = common.Cad(Sha20,7);
          Sha21 = common.Cad(Sha21,7);
          Sha22 = common.Cad(Sha22,7);
          Sha23 = common.Cad(Sha23,7);

          Shb1  = common.Cad(Shb1,10)+SInt;
          Shb2  = common.Cad(Shb2,9)+SInt;
          Shb3  = common.Cad(Shb3,9)+SInt;
          Shb4  = common.Cad(Shb4,40)+SInt;
          Shb5  = common.Cad(Shb5,7)+SInt;
          Shb6  = common.Cad(Shb6,25)+SInt;
          Shb7  = common.Cad(Shb7,10)+SInt;
          Shb8  = common.Cad(Shb8,10)+SInt;
          Shb9  = common.Cad(Shb9,15)+SInt;
          Shb10 = common.Cad(Shb10,10)+SInt;
          Shb11 = common.Cad(Shb11,5)+SInt;
          Shb12 = common.Cad(Shb12,10)+SInt;
          Shb13 = common.Cad(Shb13,5)+SInt;
          Shb14 = common.Cad(Shb14,5)+SInt;
          Shb15 = common.Cad(Shb15,12)+SInt;
          Shb16 = common.Cad(Shb16,5)+SInt;
          Shb17 = common.Cad(Shb17,10)+SInt;
          Shb18 = common.Cad(Shb18,15)+SInt;
          Shb19 = common.Cad(Shb19,13);
          Shb20 = common.Cad(Shb20,7);
          Shb21 = common.Cad(Shb21,7);
          Shb22 = common.Cad(Shb22,7);
          Shb23 = common.Cad(Shb23,7);

          String    Strh1     = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+Sha16+Sha17+Sha18+Sha19+Sha20+Sha21+Sha22+Sha23+"\n";
          String    Strh2     = Shb1+Shb2+Shb3+Shb4+Shb5+Shb6+Shb7+Shb8+Shb9+Shb10+Shb11+Shb12+Shb13+Shb14+Shb15+Shb16+Shb17+Shb18+Shb19+Shb20+Shb21+Shb22+Shb23+"\n";
                    vect      . add(Strh1);
                    vect      . add(Strh2);
          return vect;
     }

     public Vector getPendingBody1A()
     {
          Vector vect = new Vector();
          for(int i=0;i<VMrsDate.size();i++)
          {
               if(i>0)
               {
                    SDepName  = ((String)VDeptName.elementAt(i)).trim();
                    SODepName = ((String)VDeptName.elementAt(i-1)).trim();
               }
               String SMrsDate = common.parseDate((String)VMrsDate  .elementAt(i));
		       String SDays    = common.getDateDiff(common.parseDate(SEnDate),SMrsDate);
			   
               String Sda1  = common.parseDate((String)VMrsDate    . elementAt(i));
               String Sda2  = (String)VMrsNo      . elementAt(i);
               String Sda3  = (String)VMrsCode    . elementAt(i);
               String Sda4  = (String)VMrsName    . elementAt(i);
               String Sda5  = (String)VUoM        . elementAt(i);
               String Sda6  = (String)VRemarks    . elementAt(i);
               String Sda7  = (String)VMake       . elementAt(i);
               String Sda8  = (String)VCatl       . elementAt(i);
               String Sda9  = (String)VDraw       . elementAt(i);
               String Sda10 = (String)VMrsQty     . elementAt(i);
               String Sda11 = (String)VFreq       . elementAt(i);
               String Sda12 = (String)VDueDate    . elementAt(i);
               String Sda13 = (String)VBlock      . elementAt(i);
            //   String Sda14 = (String)VDueDays    . elementAt(i);
			   String Sda14 = SDays;
               String Sda15 = (String)VOrdNo      . elementAt(i);
               String Sda16 = (String)VOrdBlock   . elementAt(i);
               String Sda17 = common.parseDate(common.parseNull((String)VOrdDate    . elementAt(i)));
               String Sda18 = (String)VSupName    . elementAt(i);
               String Sda19 = (String)VOrdRate    . elementAt(i);
               String Sda20 = (String)VOrdDisc    . elementAt(i);
               String Sda21 = (String)VOrdCenVat  . elementAt(i);
               String Sda22 = (String)VOrdSalTax  . elementAt(i);
               String Sda23 = (String)VSurChg     . elementAt(i);

               Sda1  = common.Pad(Sda1,10);
               Sda2  = common.Rad(Sda2,9)+SInt;
               Sda3  = common.Pad(Sda3,9)+SInt;
               Sda4  = common.Pad(Sda4,40)+SInt;
               Sda5  = common.Pad(Sda5,7)+SInt;
               Sda6  = common.Pad(Sda6,25)+SInt;
               Sda7  = common.Pad(Sda7,10)+SInt;
               Sda8  = common.Pad(Sda8,10)+SInt;
               Sda9  = common.Pad(Sda9,15)+SInt;
               Sda10 = common.Rad(Sda10,10)+SInt;
               Sda11 = common.Rad(Sda11,5)+SInt;
               Sda12 = common.Rad(Sda12,10)+SInt;
               Sda13 = common.Rad(Sda13,5)+SInt;
               Sda14 = common.Rad(Sda14,5)+SInt;
               Sda15 = common.Pad(Sda15,12)+SInt;
               Sda16 = common.Pad(Sda16,5)+SInt;
               Sda17 = common.Pad(Sda17,10)+SInt;
               Sda18 = common.Pad(Sda18,15)+SInt;
               Sda19 = common.Rad(common.getRound(Sda19,4),13);
               Sda20 = common.Rad(Sda20,7);
               Sda21 = common.Rad(Sda21,7);
               Sda22 = common.Rad(Sda22,7);
               Sda23 = common.Rad(Sda23,7);
               
               String    Sdb1      = (String)VDeptName.elementAt(i);
                         Sdb1      = common.Pad(Sdb1,25);
               String    Strd1     = common.Rad("Dept Name : ",25)+" "+Sdb1+"\n";
               String    Strd      = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+Sda16+Sda17+Sda18+Sda19+Sda20+Sda21+Sda22+Sda23+"\n";
               if(i==0)
               {
                    vect . add(Strd1);
                    vect . add(Strd);
               }
               else
               {
                    if(SDepName.equals(SODepName))
                         vect.add(Strd);
                    else
                    {
                         vect.add(Strline);
                         vect.add(Strd1);
                         vect.add(Strd);
                    }
               }
          }
          return vect;
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
			 //  CreatePDF();	
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               
               try
               {
                    tabreport           = new TabReport1(RowData,ColumnData,ColumnType);
                    getContentPane()    . add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    DeskTop             . repaint();
                    DeskTop             . updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public void setDataIntoVector()
     {
          VMrsDate       = new Vector();
          VMrsNo         = new Vector();
          VMrsCode       = new Vector();
          VMrsName       = new Vector();
          VUoM           = new Vector();
          VRemarks       = new Vector();
          VMake          = new Vector();
          VCatl          = new Vector();
          VDraw          = new Vector();
          VMrsQty        = new Vector();
          VReOrd         = new Vector();
          VStock         = new Vector();
          VPending       = new Vector();
          VToOrder       = new Vector();
          VDeptCode      = new Vector();
          VDeptName      = new Vector();
          VDueDate       = new Vector();
          VNature        = new Vector();
          VBlock         = new Vector();
          VDueDays       = new Vector();
          VFreq          = new Vector();
          VGroup         = new Vector();
          VOrdBlock      = new Vector();
          VOrdDate       = new Vector();
          VOrdNo         = new Vector();
          VSupName       = new Vector();
          VOrdRate       = new Vector();
          VOrdDisc       = new Vector();
          VOrdCenVat     = new Vector();
          VOrdSalTax     = new Vector();
          VSurChg        = new Vector();
          VUserName      = new Vector();
          VStatus        = new Vector();

          
          SStDate = TStDate.toNormal();
          SEnDate = TEnDate.toNormal();

          String QS1 = "";

          
          if(JRPeriod.isSelected())
          {

               QS1 =   " (";

              if(JCPending.getSelectedIndex()==2 ||  JCPending.getSelectedIndex()==0)
              {


                    QS1=QS1+" SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, MRS.Remarks, MRS.Make, InvItems.Catl, InvItems.Draw, MRS.Qty, MRS.Dept_Code, Dept.Dept_Name,MRS.DueDate,Nature.Nature,OrdBlock.BlockName,Cata.Group_Name,RawUser.UserName,'Converted' as PendingStatus"+
                            " FROM ((((MRS INNER JOIN CATA on MRS.GROUP_CODE=CATA.GROUP_CODE) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) "+
                            " Inner Join InvItems on Mrs.Item_Code=InvItems.Item_Code) "+
                            " Left Join Nature on MRS.NatureCode=Nature.NatureCode) "+
                            " Left Join OrdBlock on MRS.BlockCode=OrdBlock.Block "+
                            " Inner Join RawUser on decode(Mrs.MrsAuthUserCode,null,1,0,1,Mrs.MrsAuthUserCode) = RawUser.UserCode "+
                            " WHERE (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MRS.Qty>0 And MRS.OrderNo=0 and MRS.MrsDate>='"+SStDate+"' and MRS.MrsDate <= '"+SEnDate+"'"+
                            " and Mrs.MillCode = "+iMillCode;
     
                    if(!(JCUnit.getSelectedItem()).equals("ALL"))
                         QS1 = QS1 + " and mrs.Unit_code="+VUnitCode.elementAt(JCUnit.getSelectedIndex());
                    
                    if(JCFilter.getSelectedIndex()==1)
                         QS1 = QS1 + " and nvl(Mrs.PlanMonth,0)=0 and YearlyPlanningStatus=0 ";
                    if(JCFilter.getSelectedIndex()==2)
                         QS1 = QS1 + " and Mrs.PlanMonth > 0 and YearlyPlanningStatus=0 ";
                    if(JCFilter.getSelectedIndex()==3)
                         QS1 = QS1 + " and Mrs.PlanMonth = 0 and YearlyPlanningStatus=1 ";
               }
               if((JCFilter.getSelectedIndex()==1 || JCFilter.getSelectedIndex()==0 ) && (JCPending.getSelectedIndex()==1 || JCPending.getSelectedIndex()==2) )
               {

                          if(JCPending.getSelectedIndex()==2 )
                               QS1=QS1+ " union all";


                          QS1=QS1+" Select Mrs_TEmp.MrsDate, Mrs_TEmp.MrsNo, Mrs_TEmp.Item_Code, Mrs_TEmp.Remarks, Mrs_TEmp.Make, InvItems.Catl, InvItems.Draw, Mrs_TEmp.Qty, Mrs_TEmp.Dept_Code, Dept.Dept_Name,Mrs_TEmp.DueDate,Nature.Nature,OrdBlock.BlockName,Cata.Group_Name,RawUser.UserName,"+
                            " decode(Mrs_Temp.ReadyForApproval,1,'Jmd Pending','Store Pending') as PendingStatus"+
                            " FROM ((((MRS_Temp INNER JOIN CATA on Mrs_TEmp.GROUP_CODE=CATA.GROUP_CODE) INNER JOIN Dept ON Mrs_TEmp.Dept_Code=Dept.Dept_code) "+
                            " Left Join InvItems on Mrs_TEmp.Item_Code=InvItems.Item_Code) "+
                            " Left Join Nature on Mrs_TEmp.NatureCode=Nature.NatureCode) "+
                            " Left Join OrdBlock on Mrs_TEmp.BlockCode=OrdBlock.Block "+
                            " Inner Join RawUser on decode(Mrs_TEmp.AuthUserCode,null,1,0,1,Mrs_TEmp.AuthUserCode) = RawUser.UserCode "+
                            " WHERE Mrs_TEmp.DeptMrsAuth=1 and Mrs_TEmp.Qty>0 and Mrs_TEmp.MrsDate>='"+SStDate+"' and Mrs_TEmp.MrsDate <='"+SEnDate+"'"+
                            " and Mrs_TEmp.MillCode = "+iMillCode+" and Mrs_Temp.isdelete=0 and Mrs_Temp.itemdelete=0"+
                            " and Mrs_Temp.RejectionStatus=0 and Mrs_Temp.StoresRejectionStatus=0 and Mrs_Temp.ApprovalStatus=0)";

               }
               else
               {
                         QS1= QS1+" ) ";

               }
          }
          else
          {

                    QS1 =   " (";

              if(JCPending.getSelectedIndex()==2 ||  JCPending.getSelectedIndex()==0)
              {

                         QS1 =   QS1+" SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, MRS.Remarks, MRS.Make, InvItems.Catl, InvItems.Draw, MRS.Qty, MRS.Dept_Code, Dept.Dept_Name,MRS.DueDate,Nature.Nature,OrdBlock.BlockName,Cata.Group_Name,RawUser.UserName,'Converted' as PendingStatus    "+
                                 " FROM ((((MRS INNER JOIN CATA on MRS.GROUP_CODE=CATA.GROUP_CODE) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) "+
                                 " Inner Join InvItems on Mrs.Item_Code=InvItems.Item_Code) "+
                                 " Left Join Nature on MRS.NatureCode=Nature.NatureCode) "+
                                 " Left Join OrdBlock on MRS.BlockCode=OrdBlock.Block "+
                                 " Inner Join RawUser on decode(Mrs.MrsAuthUserCode,null,1,0,1,Mrs.MrsAuthUserCode) = RawUser.UserCode "+
                                 " WHERE (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MRS.Qty>0 And MRS.OrderNo=0 and MRS.MrsDate <= '"+SEnDate+"'"+
                                 " and Mrs.MillCode = "+iMillCode;
          
                    if(!(JCUnit.getSelectedItem()).equals("ALL"))
                         QS1 = QS1 + " and mrs.Unit_code="+VUnitCode.elementAt(JCUnit.getSelectedIndex());
                    
                    if(JCFilter.getSelectedIndex()==1)
                         QS1 = QS1 + " and nvl(Mrs.PlanMonth , 0 )=0 and YearlyPlanningStatus=0 ";
                    if(JCFilter.getSelectedIndex()==2)
                         QS1 = QS1 + " and Mrs.PlanMonth > 0 and YearlyPlanningStatus=0 ";
                    if(JCFilter.getSelectedIndex()==3)
                         QS1 = QS1 + " and Mrs.PlanMonth = 0 and YearlyPlanningStatus=1 ";
               }
               if((JCFilter.getSelectedIndex()==1  || JCFilter.getSelectedIndex()==0) && (JCPending.getSelectedIndex()==1 || JCPending.getSelectedIndex()==2))
               {

                       if(JCPending.getSelectedIndex()==2 )
                            QS1=QS1+ " union all";


                       QS1= QS1+" SELECT Mrs_Temp.MrsDate, Mrs_Temp.MrsNo, Mrs_Temp.Item_Code, Mrs_Temp.Remarks, Mrs_Temp.Make, InvItems.Catl, InvItems.Draw, Mrs_Temp.Qty, Mrs_Temp.Dept_Code, Dept.Dept_Name,Mrs_Temp.DueDate,Nature.Nature,OrdBlock.BlockName,Cata.Group_Name,RawUser.UserName,"+
                       " decode(Mrs_Temp.ReadyForApproval,1,'Jmd Pending','Store Pending') as PendingStatus"+
                       " FROM ((((Mrs_Temp INNER JOIN CATA on Mrs_Temp.GROUP_CODE=CATA.GROUP_CODE) INNER JOIN Dept ON Mrs_Temp.Dept_Code=Dept.Dept_code) "+
                       " Left Join InvItems on Mrs_Temp.Item_Code=InvItems.Item_Code) "+
                       " Left Join Nature on Mrs_Temp.NatureCode=Nature.NatureCode) "+
                       " Left Join OrdBlock on Mrs_Temp.BlockCode=OrdBlock.Block "+
                       " Inner Join RawUser on decode(Mrs_Temp.AuthUserCode,null,1,0,1,Mrs_Temp.AuthUserCode) = RawUser.UserCode "+
                       " WHERE Mrs_Temp.DeptMrsAuth=1 and Mrs_Temp.Qty>0  and Mrs_Temp.MrsDate <= '"+SEnDate+"'"+
                       " and Mrs_TEmp.MillCode = "+iMillCode+" and Mrs_Temp.isdelete=0 and Mrs_Temp.itemdelete=0"+
                       " and Mrs_Temp.RejectionStatus=0 and Mrs_Temp.StoresRejectionStatus=0 and Mrs_Temp.ApprovalStatus=0)";

               }
               else
               {
                         QS1= QS1+" ) ";

               }


          }

          
          String QS2 =   " SELECT m1.Item_Code"+
                         " FROM ("+QS1+") m1 GROUP BY m1.Item_Code ";
          
          String QS3 = " ";
          
          if(iMillCode==0)
          {               
               QS3  =    " SELECT m2.Item_Code, InvItems.OPGQTY, (ROQ+MinQty) AS ReOrd, InvItems.ITEM_NAME,UOM.UoMName"+
                         " FROM ("+QS2+") m2 LEFT JOIN InvItems ON m2.Item_Code = InvItems.ITEM_CODE LEFT JOIN UOM ON UOM.UoMCode = InvItems.UOMCODE ";
          }
          else
          {
               QS3  =    " SELECT m2.Item_Code, "+SItemTable+".OPGQTY, ("+SItemTable+".ROQ+"+SItemTable+".MinQty) AS ReOrd, InvItems.ITEM_NAME,UOM.UoMName"+
                         " FROM ("+QS2+") m2 LEFT JOIN InvItems ON m2.Item_Code = InvItems.ITEM_CODE  "+
                         " LEFT JOIN UOM ON UOM.UoMCode = InvItems.UOMCODE "+
                         " Left Join "+SItemTable+" On InvItems.Item_Code = "+SItemTable+".Item_Code ";
          }
          
          String QS4 = " SELECT m2.Item_Code, Sum((Qty-InvQty)) AS Pending"+
                       " FROM ("+QS2+") m2 LEFT JOIN PurchaseOrder ON m2.Item_Code=PurchaseOrder.Item_Code "+
                       " Where PurchaseOrder.MillCode = "+iMillCode+
                       " GROUP BY m2.Item_Code ";

          String QS5 =   " SELECT m2.Item_Code, Sum(GRN.GrnQty) AS GRNQty"+
                         " FROM ("+QS2+") m2 LEFT JOIN GRN ON m2.Item_Code = GRN.Code  WHERE Grn.GrnBlock<2 "+
                         " and Grn.MillCode = "+iMillCode+
                         " GROUP BY m2.Item_Code ";

          String QS6 =   " SELECT m2.Item_Code, Sum(Issue.Qty) AS IssueQty"+
                         " FROM ("+QS2+") m2 LEFT JOIN Issue ON m2.Item_Code = Issue.Code "+
                         " Where Issue.MillCode = "+iMillCode+
                         " GROUP BY m2.Item_Code";

          String QS7 =   " (select Item_code,blockName,"+
                         " Purchaseorder.OrderNo as OrderNo,Purchaseorder.OrderDate as OrderDate,"+
                         " "+SSupTable+".name as SupName,Purchaseorder.RATE,Purchaseorder.DISCPER,"+
                         " Purchaseorder.CENVATPER, Purchaseorder.TAXPER,Purchaseorder.SURPER"+
                         " from purchaseorder"+
                         " inner join "+SSupTable+" on "+SSupTable+".ac_code = purchaseorder.sup_code"+
                         " inner join ordblock on ordblock.block   = purchaseorder.Orderblock"+
                         " where id in "+
                         " (select Ordid from (select max(id) as OrdId,ICode,OrdNo from purchaseorder"+
                         " inner join "+
                         " (select max(OrderNo) as OrdNo,item_code as ICode from"+
                         " (select purchaseOrder.Orderno,purchaseorder.item_code,max(orderdate)"+
                         " from purchaseorder"+
                         " inner join ("+QS1+") m1 on m1.item_code = purchaseorder.item_code"+
                         " group by purchaseorder.item_code,purchaseorder.Orderno)"+
                         " group by item_code) t on t.OrdNo = PurChaseOrder.orderNo and"+
                         " t.ICode = purchaseOrder.item_code"+
                         " group by Icode,OrdNo"+
                         " Order by ICode)))";

          String QString =    " SELECT m1.MrsDate,m1.MrsNo,m1.Item_Code,"+
                              " m3.ITEM_NAME,m1.Remarks,m1.Make,m1.Catl,"+
                              " m1.Draw, m1.Qty, m3.ReOrd,"+
                              " (nvl(m3.OPGQTY,0)+nvl(m5.GRNQty,0)-nvl(m6.IssueQty,0)) AS Stock,"+
                              " m4.Pending, m1.Dept_Code, m1.Dept_Name,"+
                              " m1.DueDate,m1.Nature,m1.BlockName,m3.UoMName,"+
                              " m1.group_name,m7.BlockName,m7.OrderDate,"+
                              " m7.OrderNo,m7.SupName,"+
                              " m7.RATE,m7.DISCPER,m7.CENVATPER,"+
                              " m7.TAXPER,m7.SURPER,m1.UserName,m1.PendingStatus "+
                              " FROM ("+QS1+") m1"+
                              " INNER JOIN ("+QS3+") m3 ON m1.Item_Code = m3.Item_Code"+
                              " LEFT JOIN ("+QS4+") m4 ON m1.Item_Code = m4.Item_Code"+
                              " LEFT JOIN ("+QS5+") m5 ON m1.Item_Code = m5.Item_Code"+
                              " LEFT JOIN ("+QS6+") m6 ON m1.Item_Code = m6.Item_Code"+
                              " LEFT JOIN ("+QS7+") m7 ON m1.Item_Code = m7.Item_Code"+
                              " ORDER BY 14,1,2,3 ";


		try
        {
        	ORAConnection  oraConnection =  ORAConnection.getORAConnection();
            Connection     theConnection =  oraConnection.getConnection();               
            Statement      stat          =  theConnection.createStatement();
               
            ResultSet res = stat.executeQuery(QString);
               
            while (res.next())
            {
            	String SMrsDate = common.parseDate(res.getString(1));
				   
				String SDueDate = common.parseDate(res.getString(15));
				String SDays    = common.getDateDiff(common.parseDate(SEnDate),SMrsDate);
				String SFreq    = common.getDateDiff(SDueDate,SMrsDate);

				double dReq     = common.toDouble(res.getString(9));
				double dReOrd   = common.toDouble(res.getString(10));
				double dStock   = common.toDouble(res.getString(11));
				double dPending = common.toDouble(res.getString(12));
				double dToOrder = dReq-(dStock+dPending-dReOrd);
                    
				if(JCDelay.getSelectedIndex()==0)
				{
                	VMrsDate       . addElement(SMrsDate);
                    VMrsNo         . addElement(res.getString(2));
                    VMrsCode       . addElement(res.getString(3));
                    VMrsName       . addElement(res.getString(4));
                    VRemarks       . addElement(res.getString(5));
                    VMake          . addElement(res.getString(6));
                    VCatl          . addElement(res.getString(7));
                    VDraw          . addElement(res.getString(8));
                    VMrsQty        . addElement(""+dReq);
                    VReOrd         . addElement(""+dReOrd);
                    VStock         . addElement(""+dStock);
                    VPending       . addElement(""+dPending);
                    VToOrder       . addElement(""+dToOrder);
                    VDeptCode      . addElement(res.getString(13));
                    VDeptName      . addElement(res.getString(14));
                    VDueDate       . addElement(SDueDate);
                    VNature        . addElement(res.getString(16));
                    VBlock         . addElement(res.getString(17));

                  //  VDueDays       . addElement(SDays);

                    VDueDays       . addElement("");

                    VFreq          . addElement(SFreq);
                    VUoM           . addElement(res.getString(18));
                    VGroup         . addElement(res.getString(19));

                    VOrdBlock      . addElement(common.parseNull((String)res.getString(20)));
                    VOrdDate       . addElement(common.parseNull((String)res.getString(21)));
                    VOrdNo         . addElement(common.parseNull((String)res.getString(22)));
                    VSupName       . addElement(common.parseNull((String)res.getString(23)));

                    VOrdRate       . addElement(common.parseNull((String)res.getString(24)));
                    VOrdDisc       . addElement(common.parseNull((String)res.getString(25)));
                    VOrdCenVat     . addElement(common.parseNull((String)res.getString(26)));
                    VOrdSalTax     . addElement(common.parseNull((String)res.getString(27)));
                    VSurChg        . addElement(common.parseNull((String)res.getString(28)));
                    VUserName      . addElement(common.parseNull((String)res.getString(29)));
                    VStatus        . addElement(common.parseNull((String)res.getString(30)));
				}
				if(JCDelay.getSelectedIndex()==1 && common.toInt(SDays)>10)
				{
					VMrsDate       . addElement(SMrsDate);
                    VMrsNo         . addElement(res.getString(2));
                    VMrsCode       . addElement(res.getString(3));
                    VMrsName       . addElement(res.getString(4));
                    VRemarks       . addElement(res.getString(5));
                    VMake          . addElement(res.getString(6));
                    VCatl          . addElement(res.getString(7));
                    VDraw          . addElement(res.getString(8));
                    VMrsQty        . addElement(""+dReq);
                    VReOrd         . addElement(""+dReOrd);
                    VStock         . addElement(""+dStock);
                    VPending       . addElement(""+dPending);
                    VToOrder       . addElement(""+dToOrder);
                    VDeptCode      . addElement(res.getString(13));
                    VDeptName      . addElement(res.getString(14));
                    VDueDate       . addElement(SDueDate);
                    VNature        . addElement(res.getString(16));
                    VBlock         . addElement(res.getString(17));

                  //  VDueDays       . addElement(SDays);

                    VDueDays       . addElement("");

                    VFreq          . addElement(SFreq);
                    VUoM           . addElement(res.getString(18));
                    VGroup         . addElement(res.getString(19));

                    VOrdBlock      . addElement(common.parseNull((String)res.getString(20)));
                    VOrdDate       . addElement(common.parseNull((String)res.getString(21)));
                    VOrdNo         . addElement(common.parseNull((String)res.getString(22)));
                    VSupName       . addElement(common.parseNull((String)res.getString(23)));

                    VOrdRate       . addElement(common.parseNull((String)res.getString(24)));
                    VOrdDisc       . addElement(common.parseNull((String)res.getString(25)));
                    VOrdCenVat     . addElement(common.parseNull((String)res.getString(26)));
                    VOrdSalTax     . addElement(common.parseNull((String)res.getString(27)));
                    VSurChg        . addElement(common.parseNull((String)res.getString(28)));
                    VUserName      . addElement(common.parseNull((String)res.getString(29)));
                    VStatus        . addElement(common.parseNull((String)res.getString(30)));

				}
			}
//                           setMRSDate(stat);
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void setMRSDate(Statement theStatement) throws Exception
     {
          for(int i=0;i<VMrsNo.size();i++)
          {
               String str="";

                  if(JCPending.getSelectedIndex()>=1)
                    {

               ResultSet result = theStatement.executeQuery("Select MRSDate From MRS_Temp Where MillCode="+iMillCode+" and MRSNo = "+(String)VMrsNo.elementAt(i));

               while(result.next())
				{
                    str=result.getString(1);
					
				}
				result.close();

                    }

                    if(JCPending.getSelectedIndex()==0)
                    {
					ResultSet result1 = theStatement.executeQuery("Select MRSDate From MRS Where MillCode="+iMillCode+" and MRSNo = "+(String)VMrsNo.elementAt(i));
					while(result1.next())
					{
						str=result1.getString(1);
					}
					result1.close();
					VMrsDate.setElementAt(str,i);
					
                    }
                    else
                    {
					VMrsDate.setElementAt(str,i);
					
                    }
          }
     }
     public void setRowData()
     {
          RowData     = new Object[VMrsDate.size()][ColumnData.length];
          for(int i=0;i<VMrsDate.size();i++)
          {
			   String SMrsDate = common.parseDate((String)VMrsDate  .elementAt(i));
		       String SDays    = common.getDateDiff(common.parseDate(SEnDate),SMrsDate);
			   
               RowData[i][0]  = common.parseDate((String)VMrsDate  .elementAt(i));
               RowData[i][1]  = (String)VMrsNo    .elementAt(i);
               RowData[i][2]  = (String)VMrsCode  .elementAt(i);
               RowData[i][3]  = (String)VMrsName  .elementAt(i);
               RowData[i][4]  = (String)VMrsQty   .elementAt(i);
               RowData[i][5]  = (String)VCatl     .elementAt(i);
               RowData[i][6]  = (String)VDraw     .elementAt(i);
               RowData[i][7]  = (String)VMake     .elementAt(i);
               RowData[i][8]  = (String)VDeptName .elementAt(i);
               RowData[i][9]  = (String)VDueDate  .elementAt(i);
            //   RowData[i][10] = (String)VDueDays  .elementAt(i);
               RowData[i][10] = SDays;
               RowData[i][11] = (String)VUserName .elementAt(i);
               RowData[i][12] = (String)VRemarks  .elementAt(i);
          }  
     }

     private void setUnitCombo()
     {
          VUnit     = new Vector();
          VUnitCode = new Vector();
          
          VUnit     . addElement("ALL");
          VUnitCode . addElement("9999");
          
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               String QS1 = "";
               
               QS1 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";
               
               ResultSet result3 = stat.executeQuery(QS1);
               while(result3.next())
               {
                    VUnit     .addElement(result3.getString(1));
                    VUnitCode .addElement(result3.getString(2));
               }
               result3.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept,Group & Unit :"+ex);
               System.exit(0);
          }
     }
	private void CreatePDF() 
	{
		try 
		{
            MyHeader event = new MyHeader();  
			// String PDFFile = "/root/OrganicWeb.pdf";
            document = new Document();
//            PdfWriter.getInstance(document, new FileOutputStream(SFileName));
            document.setPageSize(PageSize.A4.rotate());


			writer = PdfWriter.getInstance(document, new FileOutputStream(SFileName));
            writer.setPageEvent(event);

            document.open();
            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            table.setHeaderRows(5);

			SetPDFHead();

			File pdfFile = new File(SFileName);
						
			if (Desktop.isDesktopSupported()) // For Both Windows and Linux
			{
				Desktop.getDesktop().open(pdfFile);
			} 
			else 
			{
				System.out.println("Awt Desktop is not supported!");
			}
          
        } 
		catch (Exception ex) 
		{
            ex.printStackTrace();
			System.out.println(ex);
        }
	}
	private  class MyHeader extends PdfPageEventHelper
    {
       PdfTemplate total;
       Font bfont = new Font(Font.FontFamily.UNDEFINED,14,Font.ITALIC);
       Font sfont = new Font(Font.FontFamily.UNDEFINED,10,Font.ITALIC);
       public void onEndPage(PdfWriter writer,Document document)
       {
          try
          {
                PdfPTable  tablehead = new PdfPTable(1);
                tablehead . setHorizontalAlignment(Element.ALIGN_LEFT);
            
                tablehead.setTotalWidth(523);
                tablehead.setLockedWidth(true);
                tablehead.getDefaultCell().setBorderColor(BaseColor.WHITE);
                Phrase header5 = new Phrase("Page No :"+String.format("%d", writer.getPageNumber()),bigbold);
                tablehead.addCell(header5);
                tablehead.writeSelectedRows(0, -1,document.right(75),document.top() + ((document.topMargin() + tablehead.getTotalHeight()) / 2)-5,writer.getDirectContent());
                
          
          }
          catch(Exception e)
          { 
            System.out.println(" PageNo Pdf "+e);
          }

        }
    }
	private void SetPDFHead()
	{
		String SStDate = TStDate.toNormal();
        String SEnDate = TEnDate.toNormal();

		AddCellIntoTable(""+SMillName+"" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,iTotalColumns,0,0,0,0,bigbold);

		String STitle = "";

       if(JRPeriod.isSelected())
       {
            STitle = "MRSs Pending List with Last PO Details From "+TStDate.toString()+" To "+TEnDate.toString()+" \n";
       }
       else
       {
            STitle = "MRSs Pending List with Last PO Details As On "+TEnDate.toString()+" \n";
       }
		
		AddCellIntoTable(STitle,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,iTotalColumns,0,0,0,0,bigbold);
		

		String SPending = (String)JCPending.getSelectedItem();
		String SFilter  = (String)JCFilter.getSelectedItem();
		String SUnit	= (String)JCUnit.getSelectedItem();
		AddCellIntoTable("Unit :"+SUnit+" , Pending :"+SPending+" , Filter :"+SFilter+" ",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,iTotalColumns,0,0,0,0,bigbold);

		AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,iTotalColumns,0,0,0,0,bigbold);

		AddCellIntoTable("Mrs Date",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Mrs No",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Mat Code",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
		AddCellIntoTable("Mat Name",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("UOM",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Spec.Information",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Item Make",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
		AddCellIntoTable("Catalogue",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Drawing No",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Requested Qty",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Delay Days",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Order No",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Order Date",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Supplier Name",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2,mediumbold);
		AddCellIntoTable("Rate (Rs)",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,mediumbold);
		AddCellIntoTable("Disc (%)",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,mediumbold);

		for(int i=0;i<VMrsDate.size();i++)
        {
        	if(i>0)
			{
				SDepName  = ((String)VDeptName.elementAt(i)).trim();
				SODepName = ((String)VDeptName.elementAt(i-1)).trim();
			}
           String SMrsDate = common.parseDate((String)VMrsDate  .elementAt(i));
	       String SDays    = common.getDateDiff(common.parseDate(SEnDate),SMrsDate);
		   
			if(i==0)
           	{
//                vect . add(Strd1);
//                vect . add(Strd);

				AddCellIntoTable("Dept Name :"+(String)VDeptName.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,iTotalColumns,4,0,8,0,mediumbold);

				AddCellIntoTable(common.parseDate((String)VMrsDate.elementAt(i)),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable((String)VMrsNo.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable((String)VMrsCode.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable((String)VMrsName.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable((String)VUoM.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable((String)VRemarks.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable((String)VMake.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable((String)VCatl.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable((String)VDraw.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable((String)VMrsQty.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable(SDays,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable((String)VOrdNo.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable(common.parseDate(common.parseNull((String)VOrdDate.elementAt(i))),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable((String)VSupName.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable((String)VOrdRate.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				AddCellIntoTable((String)VOrdDisc.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			}
			else
           	{
            	if(SDepName.equals(SODepName))
				{
                	//	vect.add(Strd);
					AddCellIntoTable(common.parseDate((String)VMrsDate.elementAt(i)),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VMrsNo.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VMrsCode.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VMrsName.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VUoM.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VRemarks.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VMake.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VCatl.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VDraw.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VMrsQty.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable(SDays,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VOrdNo.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable(common.parseDate(common.parseNull((String)VOrdDate.elementAt(i))),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VSupName.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VOrdRate.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VOrdDisc.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
				}
                else
                {
					//vect.add(Strline);
                    //vect.add(Strd1);
                    //vect.add(Strd);
					AddCellIntoTable("Dept Name :"+(String)VDeptName.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,iTotalColumns,4,0,8,0,mediumbold);

					AddCellIntoTable(common.parseDate((String)VMrsDate.elementAt(i)),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VMrsNo.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VMrsCode.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VMrsName.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VUoM.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VRemarks.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VMake.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VCatl.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VDraw.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VMrsQty.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable(SDays,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VOrdNo.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable(common.parseDate(common.parseNull((String)VOrdDate.elementAt(i))),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VSupName.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VOrdRate.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
					AddCellIntoTable((String)VOrdDisc.elementAt(i),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
                }
			}
		}

		try
		{
			document.add(table);
			document.close();
		}catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e);
		}

		JOptionPane.showMessageDialog(null, "PDF File Created in "+SFileName,"Info",JOptionPane.INFORMATION_MESSAGE);
	}

	public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
 
   
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {


        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));

      
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

	private void CreateExcelFile()
	{
		try
        {
			String SStDate = TStDate.toNormal();
        	String SEnDate = TEnDate.toNormal();

			
			String STitle = "";

		   	if(JRPeriod.isSelected())
		   		STitle = "MRSs Pending List with Last PO Details From "+TStDate.toString()+" To "+TEnDate.toString()+"";
		   	else
				STitle = "MRSs Pending List with Last PO Details As On "+TEnDate.toString()+" ";

		
			String SPending = (String)JCPending.getSelectedItem();
			String SFilter  = (String)JCFilter.getSelectedItem();
			String SUnit	= (String)JCUnit.getSelectedItem();
			String SFilterType = "Unit : "+SUnit+" , Pending : "+SPending+" , Filter : "+SFilter+"  ";

        	File F;
			String SFile = common.getPrintPath()+ "MRSPendingList.xls";
			F = new File(SFile);

               workbook = Workbook.createWorkbook(F);
               sheet = workbook.createSheet("MRS Pending List", 0);
               sheet.getSettings().setPaperSize(PaperSize.A4_ROTATED);
               sheet.getSettings().setTopMargin(0.5);
               sheet.getSettings().setBottomMargin(2.5);
               sheet.getSettings().setLeftMargin(0.5);
               sheet.getSettings().setRightMargin(0.5);

               sheet.setColumnView(0,13);
               sheet.setColumnView(1,10);
               sheet.setColumnView(2,13);
               sheet.setColumnView(3,30);
               sheet.setColumnView(4,10);
               sheet.setColumnView(5,25);
               sheet.setColumnView(6,20);
               sheet.setColumnView(7,15);
               sheet.setColumnView(8,15);
               sheet.setColumnView(9,15);
               sheet.setColumnView(10,12);
               sheet.setColumnView(11,12);
               sheet.setColumnView(12,12);
               sheet.setColumnView(13,25);
               sheet.setColumnView(14,10);
               sheet.setColumnView(15,10);

			WritableFont timesline = new WritableFont(WritableFont.TIMES,12,WritableFont.BOLD,false);
			WritableCellFormat timesBold = new WritableCellFormat(timesline);
			timesBold.setAlignment(Alignment.CENTRE);

			WritableFont times8ptBoldUnderline = new WritableFont(WritableFont.TIMES,11,WritableFont.BOLD,false);
			WritableCellFormat timesBoldUnderline = new WritableCellFormat(times8ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.LEFT);

			label = new jxl.write.Label(iColCount, iRowCount,"AMARJOTHI SPINNING MILLS LTD",timesBold);
			sheet.addCell(label);
			sheet.mergeCells(iColCount,iRowCount,iColCount+15,iRowCount);

			iRowCount = iRowCount+1;

			label = new jxl.write.Label(iColCount, iRowCount,STitle,timesBold);
			sheet.addCell(label);
			sheet.mergeCells(iColCount,iRowCount,iColCount+15,iRowCount);

			iRowCount = iRowCount+1;

			label = new jxl.write.Label(iColCount, iRowCount,SFilterType,timesBold);
			sheet.addCell(label);
			sheet.mergeCells(iColCount,iRowCount,iColCount+15,iRowCount);


			iRowCount = iRowCount+3;
 
			for(int i=0;i<VMrsDate.size();i++)
			{				
				Pctr++;

				iLctr++;
				iColCount=0;
								                
				if(i==0)
				{
					label = new jxl.write.Label(iColCount, iRowCount,"Mrs Date",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount,iRowCount,iColCount,iRowCount);

					label = new jxl.write.Label(iColCount+1, iRowCount, "Mrs No",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+1,iRowCount,iColCount+1,iRowCount);

					label = new jxl.write.Label(iColCount+2, iRowCount, "Mat Code",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+2,iRowCount,iColCount+2,iRowCount);

					label = new jxl.write.Label(iColCount+3, iRowCount, "Mat Name",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+3,iRowCount,iColCount+3,iRowCount);

					label = new jxl.write.Label(iColCount+4, iRowCount, "UOM",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+4,iRowCount,iColCount+4,iRowCount);

					label = new jxl.write.Label(iColCount+5, iRowCount, "Spec.Information",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+5,iRowCount,iColCount+5,iRowCount);

					label = new jxl.write.Label(iColCount+6, iRowCount, "Item Make",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+6,iRowCount,iColCount+6,iRowCount);
				
					label = new jxl.write.Label(iColCount+7, iRowCount, "Catelogue ",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+7,iRowCount,iColCount+7,iRowCount);

					label = new jxl.write.Label(iColCount+8, iRowCount, "Drawing No",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+8,iRowCount,iColCount+8,iRowCount);

					label = new jxl.write.Label(iColCount+9, iRowCount, "Requested Qty",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+9,iRowCount,iColCount+9,iRowCount);

					label = new jxl.write.Label(iColCount+10, iRowCount, "Delay Days",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+10,iRowCount,iColCount+10,iRowCount);

					label = new jxl.write.Label(iColCount+11, iRowCount, "Order No",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+11,iRowCount,iColCount+11,iRowCount);

					label = new jxl.write.Label(iColCount+12, iRowCount, "Order Date",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+12,iRowCount,iColCount+12,iRowCount);

					label = new jxl.write.Label(iColCount+13, iRowCount, "Supplier Name",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+13,iRowCount,iColCount+13,iRowCount);

					label = new jxl.write.Label(iColCount+14, iRowCount, "Rate(Rs)",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+14,iRowCount,iColCount+14,iRowCount);

					label = new jxl.write.Label(iColCount+15, iRowCount, "Disc(%)",timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+15,iRowCount,iColCount+15,iRowCount);

					iRowCount = iRowCount+1;
				}  
				if(i>0)
				{
					SDepName  = ((String)VDeptName.elementAt(i)).trim();
					SODepName = ((String)VDeptName.elementAt(i-1)).trim();
				}
				String SMrsDate = common.parseDate((String)VMrsDate  .elementAt(i));
				String SDays    = common.getDateDiff(common.parseDate(SEnDate),SMrsDate);

				if(i==0)
				{
					label = new jxl.write.Label(iColCount, iRowCount, "Dept Name :"+(String)VDeptName.elementAt(i),timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount,iRowCount,iColCount+15,iRowCount);

					iRowCount = iRowCount+1;

					label = new jxl.write.Label(iColCount, iRowCount, common.parseDate((String)VMrsDate.elementAt(i)));
					sheet.addCell(label);
					sheet.mergeCells(iColCount,iRowCount,iColCount,iRowCount);

					label = new jxl.write.Label(iColCount+1, iRowCount, (String)VMrsNo.elementAt(i));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+1,iRowCount,iColCount+1,iRowCount);

					label = new jxl.write.Label(iColCount+2, iRowCount, (String)VMrsCode.elementAt(i));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+2,iRowCount,iColCount+2,iRowCount);

					label = new jxl.write.Label(iColCount+3, iRowCount, (String)VMrsName.elementAt(i));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+3,iRowCount,iColCount+3,iRowCount);

					label = new jxl.write.Label(iColCount+4, iRowCount,(String)VUoM.elementAt(i));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+4,iRowCount,iColCount+4,iRowCount);

					label = new jxl.write.Label(iColCount+5, iRowCount,(String)VRemarks.elementAt(i));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+5,iRowCount,iColCount+5,iRowCount);

					label = new jxl.write.Label(iColCount+6, iRowCount,(String)VMake.elementAt(i));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+6,iRowCount,iColCount+6,iRowCount);

					label = new jxl.write.Label(iColCount+7, iRowCount,(String)VCatl.elementAt(i));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+7,iRowCount,iColCount+7,iRowCount);

					label = new jxl.write.Label(iColCount+8, iRowCount,(String)VDraw.elementAt(i));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+8,iRowCount,iColCount+8,iRowCount);

					label = new jxl.write.Label(iColCount+9, iRowCount,(String)VMrsQty.elementAt(i));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+9,iRowCount,iColCount+9,iRowCount);

					label = new jxl.write.Label(iColCount+10, iRowCount,SDays,timesBoldUnderline);
					sheet.addCell(label);
					sheet.mergeCells(iColCount+10,iRowCount,iColCount+10,iRowCount);

					label = new jxl.write.Label(iColCount+11, iRowCount,(String)VOrdNo.elementAt(i));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+11,iRowCount,iColCount+11,iRowCount);

					label = new jxl.write.Label(iColCount+12, iRowCount,common.parseDate(common.parseNull((String)VOrdDate.elementAt(i))));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+12,iRowCount,iColCount+12,iRowCount);
					
					label = new jxl.write.Label(iColCount+13, iRowCount,(String)VSupName.elementAt(i));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+13,iRowCount,iColCount+13,iRowCount);

					label = new jxl.write.Label(iColCount+14, iRowCount,(String)VOrdRate.elementAt(i));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+14,iRowCount,iColCount+14,iRowCount);

					label = new jxl.write.Label(iColCount+15, iRowCount,(String)VOrdDisc.elementAt(i));
					sheet.addCell(label);
					sheet.mergeCells(iColCount+15,iRowCount,iColCount+15,iRowCount);
				}
				else
				{
					if(SDepName.equals(SODepName))
					{
						iRowCount = iRowCount+1;

						label = new jxl.write.Label(iColCount, iRowCount, common.parseDate((String)VMrsDate.elementAt(i)));
						sheet.addCell(label);
						sheet.mergeCells(iColCount,iRowCount,iColCount,iRowCount);

						label = new jxl.write.Label(iColCount+1, iRowCount, (String)VMrsNo.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+1,iRowCount,iColCount+1,iRowCount);

						label = new jxl.write.Label(iColCount+2, iRowCount, (String)VMrsCode.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+2,iRowCount,iColCount+2,iRowCount);

						label = new jxl.write.Label(iColCount+3, iRowCount, (String)VMrsName.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+3,iRowCount,iColCount+3,iRowCount);

						label = new jxl.write.Label(iColCount+4, iRowCount,(String)VUoM.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+4,iRowCount,iColCount+4,iRowCount);

						label = new jxl.write.Label(iColCount+5, iRowCount,(String)VRemarks.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+5,iRowCount,iColCount+5,iRowCount);

						label = new jxl.write.Label(iColCount+6, iRowCount,(String)VMake.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+6,iRowCount,iColCount+6,iRowCount);

						label = new jxl.write.Label(iColCount+7, iRowCount,(String)VCatl.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+7,iRowCount,iColCount+7,iRowCount);

						label = new jxl.write.Label(iColCount+8, iRowCount,(String)VDraw.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+8,iRowCount,iColCount+8,iRowCount);

						label = new jxl.write.Label(iColCount+9, iRowCount,(String)VMrsQty.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+9,iRowCount,iColCount+9,iRowCount);

						label = new jxl.write.Label(iColCount+10, iRowCount,SDays);
						sheet.addCell(label);
						sheet.mergeCells(iColCount+10,iRowCount,iColCount+10,iRowCount);

						label = new jxl.write.Label(iColCount+11, iRowCount,(String)VOrdNo.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+11,iRowCount,iColCount+11,iRowCount);

						label = new jxl.write.Label(iColCount+12, iRowCount,common.parseDate(common.parseNull((String)VOrdDate.elementAt(i))));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+12,iRowCount,iColCount+12,iRowCount);
					
						label = new jxl.write.Label(iColCount+13, iRowCount,(String)VSupName.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+13,iRowCount,iColCount+13,iRowCount);

						label = new jxl.write.Label(iColCount+14, iRowCount,(String)VOrdRate.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+14,iRowCount,iColCount+14,iRowCount);

						label = new jxl.write.Label(iColCount+15, iRowCount,(String)VOrdDisc.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+15,iRowCount,iColCount+15,iRowCount);
					}
					else
					{
						iRowCount = iRowCount+1;
					
						label = new jxl.write.Label(iColCount, iRowCount, "Dept Name :"+(String)VDeptName.elementAt(i),timesBoldUnderline);
						sheet.addCell(label);
						sheet.mergeCells(iColCount,iRowCount,iColCount+15,iRowCount);

						iRowCount = iRowCount+1;

						label = new jxl.write.Label(iColCount, iRowCount, common.parseDate((String)VMrsDate.elementAt(i)));
						sheet.addCell(label);
						sheet.mergeCells(iColCount,iRowCount,iColCount,iRowCount);

						label = new jxl.write.Label(iColCount+1, iRowCount, (String)VMrsNo.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+1,iRowCount,iColCount+1,iRowCount);

						label = new jxl.write.Label(iColCount+2, iRowCount, (String)VMrsCode.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+2,iRowCount,iColCount+2,iRowCount);

						label = new jxl.write.Label(iColCount+3, iRowCount, (String)VMrsName.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+3,iRowCount,iColCount+3,iRowCount);

						label = new jxl.write.Label(iColCount+4, iRowCount,(String)VUoM.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+4,iRowCount,iColCount+4,iRowCount);

						label = new jxl.write.Label(iColCount+5, iRowCount,(String)VRemarks.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+5,iRowCount,iColCount+5,iRowCount);

						label = new jxl.write.Label(iColCount+6, iRowCount,(String)VMake.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+6,iRowCount,iColCount+6,iRowCount);

						label = new jxl.write.Label(iColCount+7, iRowCount,(String)VCatl.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+7,iRowCount,iColCount+7,iRowCount);

						label = new jxl.write.Label(iColCount+8, iRowCount,(String)VDraw.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+8,iRowCount,iColCount+8,iRowCount);

						label = new jxl.write.Label(iColCount+9, iRowCount,(String)VMrsQty.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+9,iRowCount,iColCount+9,iRowCount);

						label = new jxl.write.Label(iColCount+10, iRowCount,SDays);
						sheet.addCell(label);
						sheet.mergeCells(iColCount+10,iRowCount,iColCount+10,iRowCount);

						label = new jxl.write.Label(iColCount+11, iRowCount,(String)VOrdNo.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+11,iRowCount,iColCount+11,iRowCount);

						label = new jxl.write.Label(iColCount+12, iRowCount,common.parseDate(common.parseNull((String)VOrdDate.elementAt(i))));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+12,iRowCount,iColCount+12,iRowCount);
					
						label = new jxl.write.Label(iColCount+13, iRowCount,(String)VSupName.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+13,iRowCount,iColCount+13,iRowCount);

						label = new jxl.write.Label(iColCount+14, iRowCount,(String)VOrdRate.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+14,iRowCount,iColCount+14,iRowCount);

						label = new jxl.write.Label(iColCount+15, iRowCount,(String)VOrdDisc.elementAt(i));
						sheet.addCell(label);
						sheet.mergeCells(iColCount+15,iRowCount,iColCount+15,iRowCount);
					}
				}
			}
			workbook.write();
            workbook.close();
           // setVisible(false);
			JOptionPane.showMessageDialog(null, "Excel File Created in "+SFile,"Info",JOptionPane.INFORMATION_MESSAGE);
		}
        catch(Exception ex)
        {
        	System.out.println(ex);
            ex.printStackTrace();
		}
	}
}
