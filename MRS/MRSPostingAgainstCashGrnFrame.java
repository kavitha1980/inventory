package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class MRSPostingAgainstCashGrnFrame extends JInternalFrame
{
     JPanel    TopPanel,BottomPanel;
     JButton   BApply,BOk;
     JComboBox JCUnit,JCOrder;
     JLayeredPane DeskTop;
     
     StatusPanel SPanel;
     DateField TDate;
     Common common;
     MRSPostingTabReport tabreport;
     
     Vector VMrsDate,VMrsNo,VMrsCode,VMrsName,VMrsQty,VCatl,VDraw,VMake,VDeptCode,VDeptName,VMrsId;
     Vector VDueDays,VFreq;
     Vector VCode,VName;
     Vector VUnitCode,VUnit;
     Object RowData[][];
     String ColumnData[] = {"Date","MRS No","Code","Name","Quantity","Catl","Draw","Make","Dept","GateInward No","Update"};
     String ColumnType[] = {"S","N","S","S","N","S","S","S","S","E","B"};
     String    SDate;
     String SDepName,SODepName;
     
     int iMillCode;
     
     public MRSPostingAgainstCashGrnFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode)
     {
          super("MRSs Pending As On Date");
          this.DeskTop   = DeskTop;
          this.VCode     = VCode;
          this.VName     = VName;
          this.SPanel    = SPanel;
          this.iMillCode = iMillCode;

          common = new Common();
     
          setUnitCombo(); 
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();
          JCOrder  = new JComboBox();
          TDate    = new DateField();
          JCUnit   = new JComboBox(VUnit);
          BApply   = new JButton("Apply");
          BOk      = new JButton("Update");
     
          TDate.setTodayDate();
     }

     public void setLayouts()
     {
          TopPanel.setLayout(new GridLayout(1,7));
     
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,700,475);
     }

     public void addComponents()
     {

		JCOrder.addItem("Materialwise");
		JCOrder.addItem("Departmentwise");
		JCOrder.addItem("MRS No");
	
 
          TopPanel.add(new JLabel("As On "));
          TopPanel.add(TDate);
          TopPanel.add(new JLabel("Sorted On "));
          TopPanel.add(JCOrder);
          TopPanel.add(new JLabel("Unit "));
          TopPanel.add(JCUnit);
          TopPanel.add(BApply);
     
          BottomPanel.add(BOk);
     
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply    .addActionListener(new ApplyList());
          BOk       .addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(false);
               updateMRS();
               removeHelpFrame();
          }
     }

     private void updateMRS()
     {
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               for(int i=0;i<RowData.length;i++)
               {
                    Boolean bValue = (Boolean)RowData[i][10];
                    
                    if(!bValue.booleanValue())
                         continue;
                    
                    String SGINo = (String)RowData[i][9];

                    if(common.toInt(SGINo)==0)
                         continue;
                    
                    String SMrsId = (String)VMrsId.elementAt(i);

                    if(SMrsId.equals("0"))
                         continue;
                    
                    String    QS = "Update MRS Set ";
                              QS = QS+" OrderNo = "+SGINo+", ";
                              QS = QS+" OrderBlock = 99 ";
                              QS = QS+" Where Id = "+SMrsId;
                              QS = QS+" and MillCode = "+iMillCode;

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               
               try
               {
                    tabreport = new MRSPostingTabReport(RowData,ColumnData,ColumnType);
                    getContentPane().add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public void setDataIntoVector()
     {
          VMrsDate  = new Vector();
          VMrsNo    = new Vector();
          VMrsCode  = new Vector();
          VMrsName  = new Vector();
          VMrsQty   = new Vector();
          VCatl     = new Vector();
          VDraw     = new Vector();
          VMake     = new Vector();
          VDeptCode = new Vector();
          VDeptName = new Vector();
          VMrsId    = new Vector();
          
          String SDate   = TDate.toNormal();
          
          String QS  =   " SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, InvItems.Item_Name, MRS.Qty, InvItems.Catl, InvItems.Draw, MRS.Make, MRS.Dept_Code, Dept.Dept_Name,MRS.Id "+
                         " FROM (MRS INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) "+
                         " Inner Join InvItems on Mrs.Item_Code=InvItems.Item_Code "+
                         " WHERE (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MRS.Qty>0 And MRS.OrderNo=0 and MRS.MrsDate <= '"+SDate+"'"+
                         " and Mrs.MillCode = "+iMillCode;
     
          if(!(JCUnit.getSelectedItem()).equals("ALL"))
               QS = QS + " and mrs.Unit_code="+VUnitCode.elementAt(JCUnit.getSelectedIndex());

			if(JCOrder.getSelectedIndex() == 0)      
			QS = QS+" Order By InvItems.Item_Name,Mrs.MrsDate";
			
			if(JCOrder.getSelectedIndex() == 1)      
			QS = QS+" Order By Dept.Dept_Name,Mrs.MrsDate";
			if(JCOrder.getSelectedIndex() == 2)      
			QS = QS+" Order By Mrs.MrsNo,Mrs.MrsDate";

			

     
        //  QS = QS + " Order by 10,1,2,3 ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
          
               ResultSet res = stat.executeQuery(QS);
          
               while (res.next())
               {
                    VMrsDate  .addElement(common.parseDate(res.getString(1)));
                    VMrsNo    .addElement(res.getString(2));
                    VMrsCode  .addElement(res.getString(3));
                    VMrsName  .addElement(res.getString(4));
                    VMrsQty   .addElement(res.getString(5));
                    VCatl     .addElement(res.getString(6));
                    VDraw     .addElement(res.getString(7));
                    VMake     .addElement(res.getString(8));
                    VDeptCode .addElement(res.getString(9));
                    VDeptName .addElement(res.getString(10));
                    VMrsId    .addElement(res.getString(11));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VMrsDate.size()][ColumnData.length];
          for(int i=0;i<VMrsDate.size();i++)
          {
               RowData[i][0]  = (String)VMrsDate  .elementAt(i);
               RowData[i][1]  = (String)VMrsNo    .elementAt(i);
               RowData[i][2]  = (String)VMrsCode  .elementAt(i);
               RowData[i][3]  = (String)VMrsName  .elementAt(i);
               RowData[i][4]  = (String)VMrsQty   .elementAt(i);
               RowData[i][5]  = (String)VCatl     .elementAt(i);
               RowData[i][6]  = (String)VDraw     .elementAt(i);
               RowData[i][7]  = (String)VMake     .elementAt(i);
               RowData[i][8]  = (String)VDeptName .elementAt(i);
               RowData[i][9]  = "";
               RowData[i][10] = new Boolean(false);
          }  
     }

     private void setUnitCombo()
     {
          VUnit       = new Vector();
          VUnitCode   = new Vector();
          
          VUnit.addElement("ALL");
          VUnitCode.addElement("9999");
          
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
          
          String QS1 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";
          
          ResultSet result3 = stat.executeQuery(QS1);
          while(result3.next())
          {
               VUnit.addElement(result3.getString(1));
               VUnitCode.addElement(result3.getString(2));
          }
          result3.close();
          stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept,Group & Unit :"+ex);
               System.exit(0);
          }
     }
}
