package MRS;
     
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MRSStationaryPropertiesFrame extends JInternalFrame
{
     JTextField     TMatCode,TMatName;
     JTextField     TSlipFromNo,TSlipToNo;

     JButton        BOk,BCancel;
     
     JPanel         TopPanel,BottomPanel;
     JLayeredPane   Layer;
     JTable         ReportTable;

     JTable theModel;

     Vector VSItemCode,VSSlipFrNo,VSSlipToNo;


     Common         common         = new Common();
     Connection     theConnection  = null;

     String SItemName="";
     String SItemCode="";
     String SSlipFrNo="";
     String SSlipToNo="";

     
     MRSStationaryPropertiesFrame(JLayeredPane Layer,String SItemName,String SItemCode,String SSlipFrNo,String SSlipToNo,JTable theModel,Vector VSItemCode,Vector VSSlipFrNo,Vector VSSlipToNo)
     {
          this.Layer          = Layer;

          this.SItemCode     = SItemCode;
          this.SItemName     = SItemName;
          this.SSlipFrNo     = SSlipFrNo;
          this.SSlipToNo     = SSlipToNo;
          this.theModel      = theModel;

          this.VSItemCode    = VSItemCode;
          this.VSSlipFrNo    = VSSlipFrNo;
          this.VSSlipToNo    = VSSlipToNo;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          BOk                      = new JButton("Okay");
          BCancel                  = new JButton("Cancel");
          
          TopPanel                 = new JPanel(true);
          BottomPanel              = new JPanel(true);
          
          TMatCode                 = new JTextField(SItemCode);
          TMatName                 = new JTextField(SItemName);
          
          TSlipFromNo              = new JTextField(SSlipFrNo);
          TSlipToNo                = new JTextField(SSlipToNo);
          
          TMatCode                 . setEditable(false);
          TMatName                 . setEditable(false);
     }
     
     public void setLayouts()
     {
          setBounds(80,100,450,145);
          setResizable(true);
          setTitle("Stationary Details");
          TopPanel  . setLayout(new GridLayout(4,2));
     }
     
     public void addComponents()
     {
          TopPanel            . add(new JLabel("Material Name"));
          TopPanel            . add(TMatName);
          TopPanel            . add(new JLabel("Material Code"));
          TopPanel            . add(TMatCode);

          TopPanel            . add(new JLabel("Slip FromNo"));
          TopPanel            . add(TSlipFromNo);
          TopPanel            . add(new JLabel("Slip ToNo"));
          TopPanel            . add(TSlipToNo);

          BottomPanel         . add(BOk);
          BottomPanel         . add(BCancel);
          
          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("South",BottomPanel);
     }
     
     public void addListeners()
     {
          BOk       . addActionListener(new ActList());
          BCancel   . addActionListener(new ActList());
     }


     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    setTableDetails();
                    removeHelpFrame();
                    theModel    . requestFocus();
               }
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
                    theModel    . requestFocus();
               }
          }
     }
     private void setTableDetails()
     {
          VSItemCode. addElement(TMatCode       . getText());
          VSSlipFrNo. addElement(TSlipFromNo    . getText());
          VSSlipToNo. addElement(TSlipToNo      . getText());
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer     . remove(this);
               Layer     . repaint();
               Layer     . updateUI();
          }
          catch(Exception ex) { }
     }
}
