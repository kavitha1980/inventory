package MRS;

import javax.swing.*;
import javax.swing.table.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import java.awt.image.*;

import guiutil.*;
import util.*;
import jdbc.*;
import java.net.*;

public class MRSTempListFrame extends JInternalFrame
{

     JButton        BApply,BConvert,BSave,BPrint,BPlaceEnquiry,BRateChart;
     JPanel         TopPanel,BottomPanel,MiddlePanel;
     JPanel         TopFirst,TopSecond,TopThird;
     JPanel         First,Second,Third,Fourth,Fifth,Sixth,critPanel;
     JComboBox      JCMrsUsers,JCMrsType;
     JLayeredPane   theLayer;
     JRadioButton   JRPeriod,JRNo;

     StatusPanel    SPanel;
     TabReport      tabreport;

     MRSTempListModel theModel;
     JTable           theTable;

     DateField      TStDate,TEnDate;
     NextField      TStNo,TEnNo;
     Common         common;

     JCheckBox      CAsOn;

     JCheckBox      CCatl,CDraw,CRemarksExt,CUom ,CUnit,
                    CDepartment,CGroup,CBlock,CUrgent,CNature,
                    CRate,CStatus,CDueDate,CReject,CRejectReason,CCondition,
                    CDocId,CDisc,CCenvat,CTax,CSurcharge,
                    CNetRate,COrderSource,CStock;


     JComboBox     JCType,JCDepartment,JCUnit,JCGroup,JCBlock,JCNature,JCUrgent,JCProjectType,JCConditionType;

     Object         RowData[][];
     int            iMillCode,iAuthCode,iUserCode;

     Vector VCode,VName,VNameCode,VCata,VDraw;
     Vector VPaperColor,VPaperSets,VPaperSize,VPaperSide,VSlipNo;

     String SItemTable;

     Vector VDate,VMrsNo,VRCode,VRName,VQty,VDept,VGroup,VStatus,VUnit,VDue,VId,VHodName,VMrsOrdNo,VMrsFlag,VMrsTypeCode,VBudgetStatus,VUserCode,VUserName,VMrsUserCode,VDocId,
            VMake,VRemarks,VCatl,VDrawing,VMrsType,VMrsTypeCd,VMrsTypeName,
            VSupplier,VPurchaseDate,VPurchaseRate,VDiscPer,VCenvatPer,VTaxPer,VSuPer,VNetRate,VUOM,VBlock,VUrgent,VNature,VReasonForMrs,VRate,VERCode,VERName,VReadyForApprovalSub,
            VConvertToOrder,VOrderSource,VOrderStatus,VEnquiryStatus,VOrderStatusCode,VPrevSup,VPrevDate,VPrevRate,VStock;
			
	Vector		VServiceStock,VNonStock, VOtherStock,VRejectReason;
	

     int iCount,iApplyCount=0,iSize;

     int Date       = 0;
     int MrsNo      = 1;
     int MrsType    = 2;
     int ItemCode   = 3;
     int ItemName   = 4;
     int EntryCode  = 5;
     int EntryName  = 6;
     int Make       = 7;
     int Catl       = 8;
     int Draw       = 9;
     int Remarks    = 10;
     int Qty        = 11;
     int Uom        = 12;
     int Unit       = 13;
     int Department = 14;
     int Group      = 15;
     int Block      = 16;
     int Urgent     = 17;
     int Nature     = 18;
     int Reason     = 19;
     int Rate       = 20;
     int TotalValue = 21;
     int Status     = 22;
     int DueDate    = 23;
     int ConditionType=24;
     int Appr       = 25;
     int Rej        = 26;
     int RejReason  = 27;
     int ToOrder    = 28;

     int JmdMrsApproval=29;
     int JmdOrderApproval=30;

     int UserName   = 31;
     int DocId      = 32;
     int SupName    = 33;
     int OrderDate  = 34;
     int PurRate    = 35;
     int DiscPer    = 36;
     int CenvatPer  = 37;
     int TaxPer     = 38;
     int SurPer     = 39;
     int NetRate    = 40;
     int Id         = 41;

     int projectpurpose  = 42;
     int OrderSource     = 43;
     int RCPrevSup       = 44;
     int RCPrevLastDate  = 45;
     int RCPrevLastRate  = 46;
     int Stock           = 47;


     int            iColWidth[]={60,60,60,60,60,60,120,40,50,50,50,50,50,40,
                                 40,40,40,60,50,50,50,50,50,50,50,50,50,
                                 50,50,70,60,100,60,60,50,50,50,50
                                 ,50,50,50,50,50,50,50,50};



     Vector VDeptName,VDeptCode,VUnitName,VUnitCode,VGroupName,VGroupCode,VBlockName,VBlockCode,VUrgentName,VUrgentCode,VNatureName,VNatureCode,VConditionType,VConditionTypeCode;
     MRSTempListRecordFrame mrsrecordframe;
     RateChartDialog       ratechart;
     EnquiryPlacementFrame theFrame;


     String[]        ColumnName;
     String[]        ColumnType;
     Boolean         bFlag=true,bPunchStatus=false;
     //CaptureForm    form;
     public MRSTempListFrame(JLayeredPane theLayer,int iMillCode,int iAuthCode,int iUserCode,Vector VCode,Vector VName,Vector VNameCode,Vector VCata,Vector VDraw,Vector VPaperColor,Vector VPaperSets,Vector VPaperSize,Vector VPaperSide,Vector VSlipNo,String SItemTable)
     {
          super("To Convert List(MRS)");
     
          this.theLayer       = theLayer;
          this.iMillCode      = iMillCode;
          this.iAuthCode      = iAuthCode;
          this.iUserCode      = iUserCode;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.VCata          = VCata;
          this.VDraw          = VDraw;

          this.VPaperColor    = VPaperColor;
          this.VPaperSets     = VPaperSets;
          this.VPaperSize     = VPaperSize;
          this.VPaperSide     = VPaperSide;
          this.VSlipNo        = VSlipNo;
          this.SItemTable     = SItemTable;

          common         = new Common();

          setUsers();
          setVector();
          setVector1();

          try
          {

               createComponents();
               setLayouts();
               addComponents();
               setColumns();
               setModel();
               addListeners();
               setDataIntoVector(0);
               setRowData();
               setColor();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void createComponents()
     {
          TStDate     = new DateField();
          TEnDate     = new DateField();
          BApply      = new JButton("Apply");
          BConvert    = new JButton(" Convert To MRS ");
          BSave       = new JButton("  Save ");
          BPlaceEnquiry = new JButton(" Place Enquiry ");
          BRateChart    = new JButton(" Rate Chart");
          BPrint      = new JButton("  Print ");
          TopPanel    = new JPanel();
          First       = new JPanel();
          Second      = new JPanel();
          Third       = new JPanel();
          critPanel   = new JPanel();
          Fourth      = new JPanel();
          Fifth       = new JPanel();
          Sixth       = new JPanel();
          BottomPanel = new JPanel();
          MiddlePanel = new JPanel();
          JCMrsUsers  = new JComboBox(VUserName);
          JCMrsType   = new JComboBox(VMrsTypeName);
          TStNo       = new NextField(0);
          TEnNo       = new NextField(0);
          CAsOn       = new JCheckBox("As On" ,true);

          TopFirst    = new JPanel();
          TopSecond   = new JPanel();
          TopThird    = new JPanel();

          CCatl       = new JCheckBox(" Catl   ");
          CDraw       = new JCheckBox(" Draw   ");
          CRemarksExt = new JCheckBox(" Remarks(Ex)");

          CUom        = new JCheckBox(" Uom ");
          CUnit       = new JCheckBox(" Unit ");
          CDepartment = new JCheckBox(" Department ");
          CGroup      = new JCheckBox(" CGroup ");   
          CBlock      = new JCheckBox(" Block ");   
          CUrgent     = new JCheckBox(" Urgent ");
          CNature     = new JCheckBox(" Nature ");
          CRate       = new JCheckBox(" Rate   ");
          CCondition  = new JCheckBox(" Condition");
          CStatus     = new JCheckBox(" Status   ");
          CDueDate    = new JCheckBox(" DueDate   ");
          CDocId      = new JCheckBox(" DocId   ");
          CReject     = new JCheckBox(" Reject   ");
          CRejectReason= new JCheckBox(" Rejec.Reason ");
          CDisc        = new JCheckBox("Disc");
          CCenvat      = new JCheckBox("CenVat");
          CTax         = new JCheckBox("Tax");
          CSurcharge   = new JCheckBox("Surcharge");
          CNetRate     = new JCheckBox("NetRate");
          COrderSource = new JCheckBox("OrderSource");
          CStock       = new JCheckBox("Stock");



          JCDepartment   = new JComboBox(VDeptName);
          JCConditionType= new JComboBox(VConditionType);
          JCUnit         = new JComboBox(VUnitName);
          JCBlock        = new JComboBox(VBlockName);
          JCGroup        = new JComboBox(VGroupName);
          JCNature       = new JComboBox(VNatureName);
          JCUrgent       = new JComboBox();


          JRPeriod    = new JRadioButton("Periodical",true);
          JRNo        = new JRadioButton("MRS No",false);


          TStDate.setTodayDate();
          TEnDate.setTodayDate();
          JCMrsUsers.setSelectedItem("All");
          JCMrsType.setSelectedItem("All");

          JCUrgent.addItem("Ordinary");
          JCUrgent.addItem("Urgent");



     }

     public void setLayouts()
     {
          TopFirst  .setLayout(new GridLayout(1,2));
          TopSecond .setLayout(new GridLayout(1,1));
          TopThird  .setLayout(new GridLayout(1,2));
          
          TopPanel  .setLayout(new GridLayout(1,3));
          First     .setLayout(new GridLayout(1,1));
          Second    .setLayout(new GridLayout(1,1));
          Third     .setLayout(new GridLayout(2,1));
          critPanel .setLayout(new GridLayout(5,5));
          Fourth    .setLayout(new GridLayout(3,1));
          Fifth     .setLayout(new GridLayout(1,1));
          Sixth     .setLayout(new GridLayout(1,1));

          MiddlePanel.setLayout(new BorderLayout());

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,850,800);
     }
     
     public void addComponents()
     {
          
          
          
          Third.setBorder(new TitledBorder("Basis"));
          Third.add(JRPeriod);
          Third.add(JRNo);
          
          Fourth.setBorder(new TitledBorder("Periodical"));
          Fourth.add(TStDate);
          Fourth.add(TEnDate);
          Fourth.add(CAsOn);

          Fifth.add(BApply);

          Sixth.add(JCMrsUsers);
          Sixth.add(JCMrsType);


          critPanel.add(CCatl);
          critPanel.add(CDraw);
          critPanel.add(CRemarksExt);
          critPanel.add(CUom);
          critPanel.add(CUnit);

          critPanel.add(CDepartment);
          critPanel.add(CGroup);          
          critPanel.add(CBlock);
          critPanel.add(CUrgent);
          critPanel.add(CNature);

          critPanel.add(CRate);
          critPanel.add(CStatus);
          critPanel.add(CDueDate);
          critPanel.add(CReject);
          critPanel.add(CRejectReason);

          critPanel.add(CDocId);
          critPanel.add(CDisc);
          critPanel.add(CCenvat);
          critPanel.add(CTax);
          critPanel.add(CSurcharge);

          critPanel.add(CNetRate);
          critPanel.add(COrderSource);
          critPanel.add(CCondition);
          critPanel.add(CStock);


          TopFirst.add(Third);
          TopFirst.add(Fourth);
          TopSecond.add(critPanel);
          TopThird.add(Sixth);
          TopThird.add(Fifth);

          TopPanel.add(TopFirst);
          TopPanel.add(TopSecond);
          TopPanel.add(TopThird);

//          MiddlePanel.add("Center",new JScrollPane(theTable));

          BottomPanel.add(new JLabel(" Select a Row Press F4 for Biometric Authentication"));
          BottomPanel.add(BSave);
          BottomPanel.add(BConvert);
          BottomPanel.add(BPrint);
          BottomPanel.add(BPlaceEnquiry);
          BottomPanel.add(BRateChart);



          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          if(iUserCode==2)
          BConvert.setEnabled(true);
          else
          BConvert.setEnabled(false);


     }

     public void addListeners()
     {
          JRPeriod.addActionListener(new JRList());
          JRNo.addActionListener(new JRList()); 
          BApply.addActionListener(new ApplyList());
          BConvert.addActionListener(new ConvertList());
          BSave.addActionListener(new SaveList());
          BPlaceEnquiry.addActionListener(new ActList());
          BRateChart.addActionListener(new ActList());

          BPrint.addActionListener(new ActList());

          theTable            . addMouseMotionListener
          (
               new MouseMotionAdapter()
               {
                    public void mouseMoved(MouseEvent e)
                    {
                         Point p   = e.getPoint();

                         int iRow  = theTable.rowAtPoint(p);
                         int iCol  = theTable.columnAtPoint(p);


                         String SMrsNo   = (String)theModel.getValueAt(iRow,MrsNo);
                         String SItemCode= (String)theModel.getValueAt(iRow,ItemCode);
                         
                         String SSupplierList = getSupplierList(SItemCode,SMrsNo);
                         

                         theTable  . setToolTipText(SSupplierList);
                         
                    }
               }
          );

          theTable.addMouseListener(new MouseList());

          theTable.addKeyListener(new KeyList(this));

     }

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    Fourth.setBorder(new TitledBorder("Periodical"));
                    Fourth.removeAll();
                    Fourth.add(TStDate);
                    Fourth.add(TEnDate);
                    Fourth.add(CAsOn);
                    Fourth.updateUI();
                    JRNo.setSelected(false);
               }
               else
               {
                    Fourth.setBorder(new TitledBorder("Numbered"));
                    Fourth.removeAll();
                    Fourth.add(TStNo);
                    Fourth.add(TEnNo);
                    Fourth.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    setColumns();
                    setModel();
                    theModel.setNumRows(0);
                    setDataIntoVector(1);
                    setRowData();
                    setColor();
                    iApplyCount=1;
               }
               catch(Exception ex)
               {
               }

          }
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BPrint)
               {
                    printData();
               }
               if(ae.getSource()==BPlaceEnquiry)
               {
                    try
                    {
                         String SIds = getMrsTempIds();

                         if(SIds.length()==0)
                         {
                              JOptionPane.showMessageDialog(null,"No Rows Selected For Enquiry");
                              return;
                         }

                         theFrame = new EnquiryPlacementFrame(theLayer,theModel,iMillCode,iUserCode,SIds);
                         theLayer.add(theFrame);
                         theLayer.repaint();
                         theFrame.setSelected(true);
                         theLayer.updateUI();
                         theFrame.show();
                         theFrame.BSave.addActionListener(new ActList3());


                    }catch(Exception ex){
                    }
               }
               if(ae.getSource()==BRateChart)
               {
                    try
                    {
                         ratechart = new RateChartDialog(theLayer,theModel,Appr,ItemCode,ItemName,SupName);
                         theLayer.add(ratechart);
                         theLayer.repaint();
                         ratechart.setSelected(true);
                         theLayer.updateUI();
                         ratechart.show();
                         ratechart.BSave.addActionListener(new ActList2());
                    }catch(Exception ex){
                    }
               }

          }
     }
     private class MouseList extends MouseAdapter
     {

          public void mouseClicked(MouseEvent me)
          {

               int iRow = theTable.getSelectedRow();
                    
               int iCol = theTable.getSelectedColumn();

               if(iCol==1)
               {
                    try
                    {
                         String SMrsNo    = (String)theModel.getValueAt(iRow,MrsNo);
                         String SItemCode = (String)theModel.getValueAt(iRow,ItemCode);
                         String SItemName = (String)theModel.getValueAt(iRow,ItemName);
                         String SDesc     ="";

                         if(CRemarksExt.isSelected()==true)
                         {
                              SDesc     = (String)theModel.getValueAt(iRow,Remarks);
                         }
                         String SCatl     = "";

                         if(CCatl.isSelected()==true)
                         {
                              SCatl       =  (String)theModel.getValueAt(iRow,Catl);
                         }
                         String SDraw     = "";

                         if(CDraw.isSelected()==true)
                         {
                              SDraw       = (String)theModel.getValueAt(iRow,Draw);
                         }
                         String SMake     = (String)theModel.getValueAt(iRow,Make);
                         String SQty      = (String)theModel.getValueAt(iRow,Qty);
     
     
                         EnquiryPartyFrame theFrame = new EnquiryPartyFrame(theLayer,SMrsNo,SItemCode,iMillCode,iUserCode,SItemName,SDesc,SMake,SCatl,SDraw,SQty);
                         theLayer.add(theFrame);
                         theLayer.repaint();
                         theFrame.setSelected(true);
                         theLayer.updateUI();
                         theFrame.show();
                    }catch(Exception ex){
                    }
              }

          }

     }

     private class ConvertList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validData())
               {

                    if(JOptionPane.showConfirmDialog(null,"Are You Sure that Convert to MRS or Reject the MrsItem ?","Information",JOptionPane.YES_NO_OPTION) == 0)
                    {
                              convertData();
                    }
               }
          }
     }
     private class SaveList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               saveData();
          }
     }

     private void saveData()
     {
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();

               Statement         theStatement = theConnection.createStatement();

               for(int i=0; i<theModel.getRows(); i++)
               {
                    Boolean  bAppValue     =   (Boolean)theModel.getValueAt(i,Appr);
                    if(bAppValue)
                    {
                         updateData(i,theStatement,0);
                    }                
               }
               theStatement.close();
               JOptionPane.showMessageDialog(null," Mrs Saved Successfully ");
               theModel.setNumRows(0);
               setDataIntoVector(1);
               setRowData();
               setColor();
               iApplyCount=1;
          }
          catch(Exception ex)
          {
               System.out.println(" Save Data"+ex);
               ex.printStackTrace();
          }
     }



     private void convertData()
     {
          try
          {

               String QS = " Update Mrs_Temp set ReadyForApproval=?,StoresRejectionStatus=?,ReadyForApprovalUserCode=?"+
                           " , ReadyForApprovalSystemName=?,ConvertToOrder=?,ReasonForRejectionStores=?,OrderTypeCode=?,RateChartApproveStatus=?,ConditionLesTypeCode=?,ReadyForApprovalDatetime=to_char(SysDate,'DD.MM.YYYY hh24:mi:ss')"+
                           " , ApprovalStatus=? , ApprovalUserCode=? ,ApprovalDateTime=?,JmdOrderApprovalStatus=? ,AutoPOGroupNo=?"+
                           " where Item_Code=? and mrsNo=? and AuthUserCode= ? and MillCode=? and Id=? and IsDelete=0 and ItemDelete=0  ";




               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();

               PreparedStatement thePrepare =  theConnection.prepareStatement(QS);

               Statement         theStatement = theConnection.createStatement();

               String SAutoGroupNo="0";

               if(checkOrderAutoApproveStatus())
               {
                    Statement theStatement1 = theConnection.createStatement();

                    ResultSet theResult = theStatement1.executeQuery(" Select AutoPoGroupNo_Seq.nextval from dual" );
                    if(theResult.next())
                    {
                         SAutoGroupNo = theResult.getString(1);
                    }
                    theResult.close();
                    theStatement1.close();
               }
               int iCount=0;
               for(int i=0; i<theModel.getRows(); i++)
               {
                    String SItemCode    =   (String)theModel.getValueAt(i,ItemCode);
                    String SItemName    =   (String)theModel.getValueAt(i,ItemName);

                    int    iMrsNo       =   common.toInt((String)theModel.getValueAt(i,MrsNo));

                    // changes to be made

                    Boolean  bAppValue     =   (Boolean)theModel.getValueAt(i,Appr);
                    Boolean  bToOrder      =   (Boolean)theModel.getValueAt(i,ToOrder);

                    Boolean  bJmdMrsApp  =   (Boolean)theModel.getValueAt(i,JmdMrsApproval);    
                    Boolean  bJmdOrderApp  =  (Boolean)theModel.getValueAt(i,JmdOrderApproval);


                    Boolean  bRejValue     = false;
                    String   SReason       =  "";

                    if(CReject.isSelected()==true)
                         bRejValue     =   (Boolean)theModel.getValueAt(i,Rej);
                    if(CRejectReason.isSelected()==true)
                         SReason       =   common.parseNull((String)theModel.getValueAt(i,RejReason));

                    String   SId           =   (String)VId.elementAt(i);   //  (String)theModel.getValueAt(i,Id);

                    int      OrderTypeCode  =   getOrderSourceCode((String)VOrderSource.elementAt(i));
                    int      iOrderStatus   =   common.toInt((String)VOrderStatus.elementAt(i));
                    int      iOrderStatusCode=  common.toInt((String)VOrderStatusCode.elementAt(i));

                    int       iTypeCode     =   0;

                    if(CCondition.isSelected()==true)
                    {
                         iTypeCode      =   getConditionTypeCode(((String)theModel.getValueAt(i,ConditionType)));
                    }

                    if(bAppValue || bRejValue)
                    {
                         if(SItemCode.equals("-1") && bAppValue==true )
                         {
                              JOptionPane.showMessageDialog(null,SItemName+" is not Matched");
                              continue;
                         }

                         updateData(i,theStatement,1);
     
                         if(bAppValue)
                         {
                              thePrepare.setInt(1,1);
                              thePrepare.setInt(2,0);
                         }
                         if(bRejValue)
                         {
                              thePrepare.setInt(1,0);
                              thePrepare.setInt(2,1);
                         }
                         thePrepare.setInt(3,iUserCode);
                         thePrepare.setString(4,common.getLocalHostName());

                         if(bToOrder  && (iOrderStatusCode>=1 && iOrderStatusCode<=3))
                         {
                              thePrepare.setInt(5,1);
                         }                      
                         else
                         {
                              thePrepare.setInt(5,0);
                         }
     
                         thePrepare.setString(6,SReason);
                         thePrepare.setInt(7,OrderTypeCode);
                         if(iOrderStatus==3)
                              thePrepare.setString(8,"1");
                         else
                              thePrepare.setString(8,"0");

                         thePrepare.setInt(9,iTypeCode);

                         if(bJmdMrsApp)
                         {
                              thePrepare.setInt(10,1);
                              thePrepare.setInt(11,1987);
                              thePrepare.setString(12,common.getServerDateTime());
                         }
                         else
                         {
                              thePrepare.setInt(10,0);
                              thePrepare.setString(11,"");
                              thePrepare.setString(12,"");
                         }
                         if(bJmdOrderApp)
                         {
                              thePrepare.setInt(13,1);
                              thePrepare.setString(14,SAutoGroupNo);

                         }
                         else
                         {
                              thePrepare.setInt(13,0);
                              thePrepare.setString(14,"0");
                         } 

/*                            thePrepare.setInt(10,0);
                              thePrepare.setString(11,"");
                              thePrepare.setString(12,"");
                              thePrepare.setInt(13,0);
                              thePrepare.setString(14,"0"); */


                         thePrepare.setString(15,SItemCode);
                         thePrepare.setInt(16,iMrsNo);
                         thePrepare.setInt(17,getUserCode((String)theModel.getValueAt(i,UserName)));
                         thePrepare.setInt(18,iMillCode);
                         thePrepare.setString(19,SId);

                         thePrepare.executeUpdate();


                         iCount++;
                    }
               }
               theStatement.close();
               thePrepare.close();

               if(iCount>0)
               {
                    JOptionPane.showMessageDialog(null," Mrs Ready For Approval or Rejection Made ");
                    //form=null;
                    theModel.setNumRows(0);
                    setDataIntoVector(1);
                    setRowData();
                    setColor();
                    iApplyCount=1;
               }
          }
          catch(Exception ex)
          {
               System.out.println(" Convert Data"+ex);
               ex.printStackTrace();
          }
     }

     public class KeyList extends KeyAdapter
     {
          MRSTempListFrame mrsframe;

          public KeyList(MRSTempListFrame mrsframe)
          {
               this.mrsframe = mrsframe;
          }


          public void keyReleased(KeyEvent ke)
          {
               Vector VMac  = new Vector();
               Vector VServ = new Vector();
               VMac.addElement("NONE");
               VServ.addElement("NONE");

               int iRow = theTable.getSelectedRow();


               if (ke.getKeyCode()==KeyEvent.VK_HOME)
               {

                         // changes to be made
                         int iDocId = common.toInt((String)theModel.getValueAt(iRow,DocId));

                         try
                         {
                              DocumentListFrame doclistframe = new DocumentListFrame(theLayer,iDocId);
                              theLayer.add(doclistframe);
                              theLayer.repaint();
                              doclistframe.setSelected(true);
                              theLayer.updateUI();
                              doclistframe.show();
                         }catch(Exception ex)
                         {

                         }
               }
               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    try
                    {

                         if(bFlag)
                         {
                              mrsrecordframe = new MRSTempListRecordFrame(theLayer,VCode,VName,VNameCode,iRow,VDeptName,VGroupName,VUnitName,VServ,VMac,VBlockName,VNatureName,VUrgentName,TStDate,iMillCode,mrsframe);
                              theLayer.add(mrsrecordframe);
                              theLayer.repaint();
                              mrsrecordframe.setSelected(true);
                              theLayer.updateUI();
                              mrsrecordframe.show();
                              setRecordData(iRow);
                              mrsrecordframe.BOk.addActionListener(new ActList1(iRow));
                              mrsrecordframe.BCancel.addActionListener(new ActList1(iRow));
                              bFlag  = false;
                        }
                    }
                    catch(Exception e)
                    {
                         System.out.println(e);
                         e.printStackTrace();
                    }
               }
               if (ke.getKeyCode()==KeyEvent.VK_F4)
               {

                    //form = new CaptureForm(iUserCode);
                    //form.setVisible(true);
               }


          }
     }
     

     public void setDataIntoVector(int iStatus)
     {
          ResultSet result = null;

          VDate         = new Vector();
          VMrsNo        = new Vector();
          VMrsType      = new Vector();
          VRCode        = new Vector();
          VRName        = new Vector();
          VERCode        = new Vector();
          VERName        = new Vector();

          VQty          = new Vector();
          VDept         = new Vector();
          VGroup        = new Vector();
          VStatus       = new Vector();
          VDue          = new Vector();
          VUnit         = new Vector();
          VId           = new Vector();
          VOrderSource  = new Vector();
          VHodName      = new Vector();
          VMrsOrdNo     = new Vector();
          VMrsFlag      = new Vector();
          VMrsTypeCode  = new Vector();
          VBudgetStatus = new Vector();
          VMrsUserCode  = new Vector();
          VDocId        = new Vector();
          VMake         = new Vector();
          VRemarks      = new Vector();
          VCatl         = new Vector();
          VDrawing       = new Vector();
          VPurchaseDate  = new Vector();
          VSupplier      = new Vector();
          VUOM           = new Vector();
          VPurchaseRate  = new Vector();
          VDiscPer       = new Vector();
          VCenvatPer     = new Vector();
          VTaxPer        = new Vector();
          VSuPer         = new Vector();
          VNetRate       = new Vector();

          VBlock         = new Vector();
          VUrgent        = new Vector();
          VNature        = new Vector();
          VReasonForMrs  = new Vector();
          VRate          = new Vector();

          VOrderStatus         = new Vector();
          VEnquiryStatus       = new Vector();
          VReadyForApprovalSub = new Vector();
          VConvertToOrder      = new Vector();

          VOrderStatusCode     = new Vector();

          VPrevSup        = new Vector();
          VPrevDate       = new Vector();     
          VPrevRate       = new Vector();
          VStock          = new Vector();
		  
		  VServiceStock	= new Vector();
		  VNonStock		= new Vector();
		  VOtherStock	= new Vector();
		  VRejectReason	= new Vector();

          iCount=0;

          String QSTemp   = " Create Or Replace View TempMrsOrder1 as("+
                        " Select max(OrderDate) as OrderDate,Item_Code,OrderStatusCode from("+
                        " select  max(OrderDate) as OrderDate,PurchaseOrder.Item_Code,1 as OrderStatusCode"+
                        " From PurchaseOrder "+
                        " Inner join Mrs_Temp on Mrs_Temp.Item_Code=PurchaseOrder.Item_Code"+
                        " and Mrs_Temp.DeptMrsAuth=1 and MRS_TEmp.IsConverted=0  and Mrs_Temp.RejectionStatus=0 and MRS_TEmp.ApprovalStatus=0 and MRS_TEmp.ReadyForApproval=0 and MRS_TEmp.StoresRejectionStatus=0 and MRS_TEmp.IsDelete=0 and MRS_TEmp.ItemDelete=0"+
                        " Group by Purchaseorder.Item_Code"+

/*                      " Left Join ComparsionEnquiry on ComparsionEnquiry.Item_code=PurchaseOrder.Item_Code"+
                        " and ComparsionEnquiry.Type=3 and (ComparsionEnquiry.JmdOrderApproval=1 or ComparsionEnquiry.JmdOrderApproval=0)"+
                        " where ComparsionEnquiry.Item_Code is null"+ 
                        " and PurchaseOrder.OrderDate>=20170701"+     
                        " Union all"+
                        " select  to_Char(max(ComparsionDate)) as OrderDate,ComparsionEnquiry.Item_Code,2 as OrderStatusCode"+
                        " From  ComparsionEnquiry "+
                        " Inner join Mrs_Temp on Mrs_Temp.Item_Code=ComparsionEnquiry.Item_Code"+
                        " and Mrs_Temp.DeptMrsAuth=1 and MRS_TEmp.IsConverted=0  and Mrs_Temp.RejectionStatus=0 and MRS_TEmp.ApprovalStatus=0 and MRS_TEmp.ReadyForApproval=0 and MRS_TEmp.StoresRejectionStatus=0 and MRS_TEmp.IsDelete=0 and MRS_TEmp.ItemDelete=0"+
                        " where ComparsionEnquiry.Type=3 and (ComparsionEnquiry.JmdOrderApproval=1 or RateChartEntryStatus=1 )"+     
                        " Group by ComparsionEnquiry.Item_Code "+*/
                        " )group by Item_Code,OrderStatusCode) ";


          String QSTemp1 ="   create or replace view TempMrsOrder11 as("+
                   "  select max(Id) as Id,OrderDate,Item_Code,OrderStatusCOde from("+
                   "  select Max(Purchaseorder.Id) as Id,TempMRsOrder1.OrderDate,TempMrsOrder1.Item_Code as Item_Code,TempMrsOrder1.OrderStatusCode from PurchaseOrder"+
                   "  Inner join TempMrsOrder1 on  TempMRsOrder1.OrderDate=PurchaseOrder.OrderDate"+
                   "  and TempMrsOrder1.Item_Code=PurchaseOrder.Item_code  and TempMrsOrder1.OrderStatusCode=1"+
                   "  group by tempMrsOrder1.Orderdate,TempMRsOrder1.Item_Code,TempMrsOrder1.OrderStatusCode "+
                   "  union All"+
                   "  select Max(ComparsionEnquiry.ComparsionId) as Id,TempMRsOrder1.OrderDate,TempMrsOrder1.Item_Code as Item_Code,TempMrsOrder1.OrderStatusCode from ComparsionEnquiry"+
                   "  Inner join TempMrsOrder1 on  TempMRsOrder1.OrderDate=ComparsionEnquiry.ComparsionDate"+
                   "  and TempMrsOrder1.Item_Code=ComparsionEnquiry.Item_code  and TempMrsOrder1.OrderStatusCode=2"+
                   "  group by tempMrsOrder1.Orderdate,TempMRsOrder1.Item_Code,TempMrsOrder1.OrderStatusCode) group by OrderDate,Item_Code,OrderStatusCode) ";


          String QSTemp2= " create or Replace View Temp1Mrs_TempOrder as ("+
                     " Select OrderDate,Item_Code,"+
                     " Name,Rate,DiscPer, "+
                     " CenVatPer,TaxPer,SurPer, "+
                     " NetRate,OrderSource,OrderStatus,OrderStatusCode,decode(OrderStatusCode,2,getSupplier(Item_Code),'') as PrevDet"+
                     " from ("+
                     " Select to_number(PurchaseOrder.OrderDate) as OrderDate,PurchaseOrder.Item_Code, "+
                     " Supplier.Name,PurchaseOrder.Rate,PurchaseOrder.DiscPer, "+
                     " PurchaseOrder.CenVatPer,PurchaseOrder.TaxPer,PurchaseOrder.SurPer, " +
                     " PurchaseOrder.Net/PurchaseOrder.Qty as NetRate,'PrevOrder' as OrderSource,1 as OrderStatus ,TempMRsOrder11.OrderStatusCode"+
                     " From PurchaseOrder Inner Join Supplier On Supplier.Ac_Code = PurchaseOrder.Sup_Code "+
                     " Inner join TempMRSOrder11 on TempMRSOrder11.OrderDate= PurchaseOrder.Orderdate"+
                     " and TempMRSOrder11.Item_Code=PurchaseOrder.Item_Code and TempMRSOrder11.Id=PurchaseOrder.Id and TempMrsOrder11.OrderStatusCode=1 "+
                     " Where PurchaseOrder.Qty > 0 And PurchaseOrder.Net > 0 "+
/*                    " Union All"+
                     " select max(ComparsionDate) as OrderDate,ComparsionEnquiry.Item_Code,"+
                     " SUpplier.Name,ComparsionEnquiry.Rate,ComparsionEnquiry.DiscPer,"+
                     " ComparsionEnquiry.CenVatPer,ComparsionEnquiry.TaxPer,ComparsionEnquiry.SurPer,TotalValue as NetRate,'RateChart' as OrderSource,decode(jmdOrderApproval,0,3,2) as OrderStatus,TempMRsOrder11.OrderStatusCode"+
                     " from ComparsionEnquiry "+
                     " Inner join Supplier on Supplier.AC_Code=ComparsionEnquiry.Sup_Code"+
                     " Inner join TempMRSOrder11 on TempMRSOrder11.OrderDate= ComparsionEnquiry.Comparsiondate"+
                     " and TempMRSOrder11.Item_Code=ComparsionEnquiry.Item_Code and TempMRSOrder11.Id=ComparsionEnquiry.ComparsionId and TempMrsOrder11.OrderStatusCode=2 "+
                     " where "+
                     " ComparsionEnquiry.Type=3 "+
                     " and (ComparsionEnquiry.JmdOrderApproval=1 or (ComparsionEnquiry.JmdOrderApproval=0 and RateChartEntryStatus=1)) "+
                     "   group by ComparsionEnquiry.Item_Code, "+
                     " SUpplier.Name,ComparsionEnquiry.Rate,ComparsionEnquiry.DiscPer, "+
                     " ComparsionEnquiry.CenVatPer,ComparsionEnquiry.TaxPer,ComparsionEnquiry.SurPer,TotalValue,decode(jmdOrderApproval,0,3,2),TempMRsOrder11.OrderStatusCode"+*/
                     " ))";


          try
          {


               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

/*             try{stat.execute("Drop Table TempMRSOrder1");}catch(Exception ex){}
               try{stat.execute("Drop Table TempMRSOrder11");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp1Mrs_TempOrder");}catch(Exception ex){}*/



               stat.execute(QSTemp);
               stat.execute(QSTemp1);
               stat.execute(QSTemp2);

               result        =  stat.executeQuery(getQString(iStatus));
               while (result.next())
               {
                    VMrsNo       .addElement((String)result.getString(2));
                    VDate        .addElement(common.parseDate((String)result.getString(1)));
                    VRCode       .addElement((String)result.getString(3));
                    VRName       .addElement((String)result.getString(4));
                    VQty         .addElement((String)result.getString(5));
                    VDept        .addElement((String)result.getString(6));
                    VGroup       .addElement((String)result.getString(7));
                    VDue         .addElement(common.parseDate((String)result.getString(8)));
                    VUnit        .addElement((String)result.getString(9));
                    VId          .addElement((String)result.getString(10));
                    VStatus      .addElement(" ");
                    VHodName     .addElement((String)result.getString(11));
                    VMrsOrdNo    .addElement((String)result.getString(12));
                    VMrsFlag     .addElement((String)result.getString(13));
                    VMrsTypeCode .addElement((String)result.getString(14));
                    VBudgetStatus.addElement((String)result.getString(15));
                    VMrsUserCode .addElement((String)result.getString(16));
                    VDocId       .addElement(common.parseNull((String)result.getString(17)));
                    VRemarks     .addElement(common.parseNull((String)result.getString(18)));
                    VCatl        .addElement(common.parseNull((String)result.getString(19)));
                    VDrawing     .addElement(common.parseNull((String)result.getString(20)));
                    VMake        .addElement(common.parseNull((String)result.getString(21)));
                    VPurchaseDate.addElement(common.parseDate(common.parseNull((String)result.getString(22))));
                    VPurchaseRate.addElement(common.parseNull((String)result.getString(23)));
                    VDiscPer     .addElement(common.parseNull((String)result.getString(24)));
                    VCenvatPer   .addElement(common.parseNull((String)result.getString(25)));
                    VTaxPer      .addElement(common.parseNull((String)result.getString(26)));
                    VSuPer       .addElement(common.parseNull((String)result.getString(27)));
                    VNetRate     .addElement(common.parseNull((String)result.getString(28)));
                    VSupplier    .addElement(common.parseNull((String)result.getString(29)));
                    VUOM         .addElement(common.parseNull((String)result.getString(30)));

                    VBlock       .addElement(common.parseNull((String)result.getString(31)));

                    VUrgent      .addElement(common.parseNull((String)result.getString(32)));

                    VNature      .addElement(common.parseNull((String)result.getString(33)));
                    VReasonForMrs.addElement(common.parseNull((String)result.getString(34)));
                    VRate        .addElement(common.parseNull((String)result.getString(35)));

                    VERCode       .addElement((String)result.getString(36));
                    VERName       .addElement((String)result.getString(37));

                    VReadyForApprovalSub.addElement((String)result.getString(38));
                    VConvertToOrder.addElement((String)result.getString(39));
                    VMrsType.addElement(common.parseNull(result.getString(40)));
                    VOrderSource.addElement(common.parseNull(result.getString(41)));
                    VOrderStatus.addElement(common.parseNull(result.getString(42)));
                    VEnquiryStatus.addElement(common.parseNull(result.getString(43)));

                    VOrderStatusCode.addElement(common.parseNull(result.getString(44)));

                    String str     =   common.parseNull(result.getString(45));

                    String str1 ="",str2="",str3="";

                    if(str.contains(":"))
                    {
                         String[] parts = str.split(":");

                         str1 =parts[0];
                         str2 =parts[1];
                         str3 =parts[2];

                    } 
                    VPrevSup.addElement(common.parseNull(str1));
                    VPrevDate.addElement(common.parseNull(str2));
                    VPrevRate.addElement(common.parseNull(str3));

                    VStock.addElement(common.parseNull(result.getString(46)));
					
					VServiceStock.addElement(common.parseNull(result.getString(47)));
					VNonStock.addElement(common.parseNull(result.getString(48)));
					VOtherStock.addElement(common.parseNull(result.getString(49)));
					
					
					VRejectReason.addElement(common.parseNull(result.getString(50)));


                    iCount++;
               }
               result.close();
               if(iCount==0 && iApplyCount==0)
               {
                    removeHelpFrame();
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("setDataIntoVector: "+ex);
               ex.printStackTrace();
          }
     }

     public void setUsers()
     {
          ResultSet result = null;

          VUserCode = new Vector();
          VUserName = new Vector();
          VMrsTypeCd =new Vector();
          VMrsTypeName=new Vector();

          try
          {

               String QS = " select distinct AuthUserCode,RawUser.UserName from RawUser"+
                           " inner join MrsUserAuthentication on MrsUserAuthentication.AuthUserCode=RawUser.UserCode";

               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
                              result        =  stat.executeQuery(QS);

               while (result.next())
               {
                    VUserCode.addElement(result.getString(1));
                    VUserName.addElement(result.getString(2));
               }

               VUserCode.addElement("-1");
               VUserName.addElement("All");

               result.close();
                              result        =  stat.executeQuery(" Select MrsTypeCode,MrsTypeName from mrsType");
               while (result.next())
               {
                    VMrsTypeCd.addElement(result.getString(1));
                    VMrsTypeName.addElement(result.getString(2));
               }
               VMrsTypeCd.addElement("-1");
               VMrsTypeName.addElement("All");

          }
          catch(Exception ex)
          {
               System.out.println("set Users: "+ex);
               ex.printStackTrace();
          }
     }


     public void setRowData()
     {

          for(int i=0;i<VDate.size();i++)
          {
               Vector theVect = new Vector();

               // changes to be made

               double dValue = common.toDouble((String)VQty.elementAt(i))*common.toDouble((String)VRate.elementAt(i));

               theVect.addElement(common.parseNull((String)VDate.elementAt(i)));
               theVect.addElement(common.parseNull((String)VMrsNo.elementAt(i)));
               theVect.addElement(common.parseNull((String)VMrsType.elementAt(i)));
               theVect.addElement(common.parseNull((String)VRCode.elementAt(i)));
               theVect.addElement(common.parseNull((String)VRName.elementAt(i)));
               theVect.addElement(common.parseNull((String)VERCode.elementAt(i)));
               theVect.addElement(common.parseNull((String)VERName.elementAt(i)));
               theVect.addElement(common.parseNull((String)VMake.elementAt(i)));

               if(CCatl.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VCatl.elementAt(i)));
               }
               if(CDraw.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VDrawing.elementAt(i)));
               }
               if(CStock.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VStock.elementAt(i)));
               }

               if(CRemarksExt.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VRemarks.elementAt(i)));
               }
               theVect.addElement(common.parseNull((String)VQty.elementAt(i)));
               if(CUom.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VUOM.elementAt(i)));
               }
               if(CUnit.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VUnit.elementAt(i)));

               }
               if(CDepartment.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VDept.elementAt(i)));
               }
               if(CGroup.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VGroup.elementAt(i)));
               }
               if(CBlock.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VBlock.elementAt(i)));
               }

               String SUrgent ="";
                    if(common.toInt((String)VUrgent.elementAt(i))==0)
                         SUrgent = "Ordinary";
                    else
                         SUrgent = "Urgent";
               if(CUrgent.isSelected()==true)
               {
                    theVect.addElement(common.parseNull(SUrgent));
               }
               if(CNature.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VNature.elementAt(i)));
               }
               theVect.addElement(common.parseNull((String)VReasonForMrs.elementAt(i)));


               if(CRate.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VRate.elementAt(i)));
               }
                    theVect.addElement(common.getRound(dValue,2));

               if(CStatus.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VStatus.elementAt(i)));

               }
               if(CDueDate.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VDue.elementAt(i)));
               }
               if(CCondition.isSelected()==true)
               {
                    theVect.addElement("NIL");
               }
               theVect.addElement(new Boolean(false));

               if(CReject.isSelected()==true)
               {
                    theVect.addElement(new Boolean(false));
               }
               if(CRejectReason.isSelected()==true)
               {
                    theVect.addElement("");
               }
               theVect.addElement(new Boolean(false));
               theVect.addElement(new Boolean(false));  
               theVect.addElement(new Boolean(false));


               theVect.addElement(getUserName((String)VMrsUserCode.elementAt(i)));


               if(CDocId.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VDocId.elementAt(i)));
               }

//               String SSupplier =" <html><body><font color='"+getColor(common.toInt((String)VOrderStatus.elementAt(i)))+"'><b><p>"+common.parseNull((String)VSupplier.elementAt(i))+"</b></p></body></html>";

               if(common.toInt((String)VOrderStatusCode.elementAt(i))==2)
               {
                    theVect.addElement(common.parseNull((String)VSupplier.elementAt(i)));         
                    theVect.addElement(common.parseNull((String)VPurchaseDate.elementAt(i)));
                    theVect.addElement(common.parseNull((String)VPurchaseRate.elementAt(i)));

                    theVect.addElement(common.parseNull((String)VPrevSup.elementAt(i)));         
                    theVect.addElement(common.parseDate((String)VPrevDate.elementAt(i)));
                    theVect.addElement(common.parseNull((String)VPrevRate.elementAt(i)));

               }
               else
               {
     
                    theVect.addElement(common.parseNull(""));         
                    theVect.addElement(common.parseNull(""));
                    theVect.addElement(common.parseNull(""));


                    theVect.addElement(common.parseNull((String)VSupplier.elementAt(i)));         
                    theVect.addElement(common.parseNull((String)VPurchaseDate.elementAt(i)));
                    theVect.addElement(common.parseNull((String)VPurchaseRate.elementAt(i)));
               }



               if(CDisc.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VDiscPer.elementAt(i)));
               }
               if(CCenvat.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VCenvatPer.elementAt(i)));
               }
               if(CTax.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VTaxPer.elementAt(i)));
               }
               if(CSurcharge.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VSuPer.elementAt(i)));
               }
               if(CNetRate.isSelected()==true)
               {
                    theVect.addElement(common.parseNull((String)VNetRate.elementAt(i)));
               }
			   
               if(COrderSource.isSelected()==true)
               {
                    theVect.addElement((String)VOrderSource.elementAt(i));
               }
//             theVect.addElement(common.parseNull((String)VId.elementAt(i)));

             theVect.addElement(common.parseNull((String)VServiceStock.elementAt(i)));
			 theVect.addElement(common.parseNull((String)VNonStock.elementAt(i)));
			 theVect.addElement(common.parseNull((String)VOtherStock.elementAt(i)));
			 
			 theVect.addElement(common.parseNull((String)VRejectReason.elementAt(i)));
			 
			 
			 

               theModel.appendRow(theVect);
          }
     }
                                             
     public String getQString(int iStatus)
     {
          DateField df = new DateField();
          df.setTodayDate();
          String SToday = df.toNormal();
          String QString = "";

          if(JRPeriod.isSelected())
          {
               QString = " SELECT substr(MRS_TEmp.AuthDateTime,1,8) as MrsDate, MRS_TEmp.MrsNo, MRS_TEmp.Item_Code, Mrs_Temp.Item_Name , MRS_TEmp.Qty, Dept.Dept_Name , Cata.Group_Name , MRS_TEmp.DueDate, Unit.Unit_Name,MRS_TEmp.Id,Hod.hodname,MRS_TEmp.orderno,MRS_TEmp.MrsFlag,MRS_TEmp.MrsTypeCode,MRS_TEmp.BudgetStatus,Mrs_Temp.AuthUserCode,Mrs_Temp.DocumentId, "+
                         " Mrs_Temp.Remarks,decode(Mrs_Temp.Catl,null,InvItems.Catl,Mrs_temp.Catl),decode(Mrs_Temp.Draw,'',InvItems.Draw,Mrs_Temp.Draw),Mrs_Temp.Make, "+
                         " Temp1Mrs_TempOrder.OrderDate as LastPurchaseDate,Temp1Mrs_TempOrder.Rate as LastPurchaseRate,"+
                         " Temp1Mrs_TempOrder.DiscPer,Temp1Mrs_TempOrder.CenvatPer,Temp1Mrs_TempOrder.TaxPer,Temp1Mrs_TempOrder.SurPer,"+
                         " Temp1Mrs_TempOrder.Netrate,decode(Temp1Mrs_TempOrder.Name,null,decode(Mrs_Temp.EnquiryNo,0,'',Mrs_Temp.EnquiryNo),Temp1Mrs_TempOrder.Name),decode(Mrs_TEmp.UOM,null,UOM.UOMNAME,Mrs_Temp.UOM) "+
                         " ,OrdBlock.BlockName,Mrs_Temp.UrgOrd,Nature.Nature ,Mrs_Temp.ReasonForMrs,Mrs_Temp.NetRate,Mrs_Temp.EntryItemCode,Mrs_Temp.EntryItemName"+
                         " ,Mrs_Temp.ReadyForApprovalSub,Mrs_Temp.ConvertToOrder,MrsType.MrsTypeName,decode(Temp1Mrs_TempOrder.OrderSource,null,'',Temp1Mrs_TempOrder.OrderSource),Temp1Mrs_TempOrder.OrderStatus,Mrs_Temp.EnquiryStatus,Temp1Mrs_TempOrder.OrderStatusCode,Temp1Mrs_TempOrder.PrevDet,getTotalStock(InvItems.Item_Code,"+iMillCode+") "+   // ,Temp1Mrs_TempOrder.RCPrevLastDate,Temp1Mrs_TempOrder.RCPrevLastRate 
						 
						 " ,GETSERVICESTOCK(InvItems.Item_Code,"+iMillCode+"),GETNONSTOCK(InvItems.Item_Code,"+iMillCode+"),GETOTHERSTOCK(InvItems.Item_Code,"+iMillCode+"),MRS_TEMP.REQUESTREMARKS  "+
						 
                         " FROM (((MRS_TEMP Left JOIN InvItems ON MRS_TEmp.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON MRS_TEmp.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON MRS_TEmp.Group_Code=Cata.Group_Code) INNER JOIN Unit ON MRS_TEmp.Unit_Code=Unit.Unit_Code "+
                         " LEFT JOIN Temp1Mrs_TempOrder on Temp1Mrs_TempOrder.ITem_Code=MRS_Temp.Item_code"+
                         " LEFT JOIN UOM on UOM.UOMCode=Invitems.UomCode"+
                         " Left join OrdBlock on OrdBlock.Block=MRS_TEMP.BlockCode "+
                         " left join Nature on Nature.NatureCode=MRS_TEMP.NatureCode "+
                         " Left join MrsType on MrsType.MrsTypeCode=Mrs_Temp.MrsTypeCode"+
                         " LEFT JOIN Hod ON MRS_TEmp.HODCode=HOD.HodCode Where MRS_TEmp.DeptMrsAuth=1 and MRS_TEmp.Qty >= 0 and Mrs_temp.MrsDate>0  and MRS_TEmp.MrsDate <='"+TEnDate.toNormal()+"' and MRS_TEmp.MillCode="+iMillCode;

                         if(!CAsOn.isSelected())
                             QString =QString+ " And substr(MRS_TEmp.AuthDateTime,1,8) >= '"+TStDate.toNormal()+"' And  substr(MRS_TEmp.AuthDateTime,1,8)<='"+TEnDate.toNormal()+"'";

                        QString= QString+ " ANd MRS_TEmp.IsConverted=0  and Mrs_Temp.RejectionStatus=0 and MRS_TEmp.ApprovalStatus=0 and MRS_TEmp.ReadyForApproval=0 and MRS_TEmp.StoresRejectionStatus=0 and MRS_TEmp.IsDelete=0 and MRS_TEmp.ItemDelete=0";

                  if(getUserCode((String)JCMrsUsers.getSelectedItem())!=-1)
                  QString = QString+" And Mrs_Temp.AuthUserCode="+getUserCode((String)JCMrsUsers.getSelectedItem());
                  if(getMrsTypeCode((String)JCMrsType.getSelectedItem())!=-1)
                  QString = QString+" And Mrs_Temp.MrsTypeCode="+getMrsTypeCode((String)JCMrsType.getSelectedItem());

          }
          else
          {
               QString = " SELECT substr(MRS_TEmp.AuthDateTime,1,8), MRS_TEmp.MrsNo, MRS_TEmp.Item_Code, Mrs_Temp.Item_Name , MRS_TEmp.Qty, Dept.Dept_Name , Cata.Group_Name , MRS_TEmp.DueDate, Unit.Unit_Name,MRS_TEmp.Id,Hod.hodname,MRS_TEmp.orderno,MRS_TEmp.MrsFlag,MRS_TEmp.MrsTypeCode,MRS_TEmp.BudgetStatus,Mrs_Temp.AuthUserCode,Mrs_Temp.DocumentId, "+
                         " Mrs_Temp.Remarks,decode(Mrs_Temp.Catl,null,InvItems.Catl,Mrs_temp.Catl),decode(Mrs_Temp.Draw,'',InvItems.Draw,Mrs_Temp.Draw),Mrs_Temp.Make, "+
                         " Temp1Mrs_TempOrder.OrderDate as LastPurchaseDate,Temp1Mrs_TempOrder.Rate as LastPurchaseRate,"+
                         " Temp1Mrs_TempOrder.DiscPer,Temp1Mrs_TempOrder.CenvatPer,Temp1Mrs_TempOrder.TaxPer,Temp1Mrs_TempOrder.SurPer,"+
                         " Temp1Mrs_TempOrder.Netrate,decode(Temp1Mrs_TempOrder.Name,null,decode(Mrs_Temp.EnquiryNo,0,'',Mrs_Temp.EnquiryNo),Temp1Mrs_TempOrder.Name),decode(Mrs_TEmp.UOM,null,UOM.UOMNAME,Mrs_Temp.UOM)"+
                         " ,OrdBlock.BlockName,Mrs_Temp.UrgOrd,Nature.Nature ,Mrs_Temp.ReasonForMrs,Mrs_Temp.NetRate,Mrs_Temp.EntryItemCode,Mrs_Temp.EntryItemName"+
                         " ,Mrs_Temp.ReadyForApprovalSub,Mrs_Temp.ConvertToOrder,MrsType.MrsTypeName,decode(Temp1Mrs_TempOrder.OrderSource,null,'',Temp1Mrs_TempOrder.OrderSource),Temp1Mrs_TempOrder.OrderStatus,Mrs_Temp.EnquiryStatus,Temp1Mrs_TempOrder.OrderStatusCode,Temp1Mrs_TempOrder.PrevDet,getTotalStock(InvItems.Item_Code,"+iMillCode+")"+  // ,Temp1Mrs_TempOrder.RCPrevLastDate,Temp1Mrs_TempOrder.RCPrevLastRate 
						 " ,GETSERVICESTOCK(InvItems.Item_Code,"+iMillCode+"),GETNONSTOCK(InvItems.Item_Code,"+iMillCode+"),GETOTHERSTOCK(InvItems.Item_Code,"+iMillCode+") ,MRS_TEMP.REQUESTREMARKS "+						 
                         " FROM (((MRS_TEMP Left JOIN InvItems ON MRS_TEmp.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON MRS_TEmp.Dept_Code=Dept.Dept_code) LEFT JOIN Cata ON MRS_TEmp.Group_Code=Cata.Group_Code) INNER JOIN Unit ON MRS_TEmp.Unit_Code=Unit.Unit_Code "+
                         " LEFT JOIN Temp1Mrs_TempOrder on Temp1Mrs_TempOrder.ITem_Code=MRS_Temp.Item_code"+
                         " LEFT JOIN UOM on UOM.UOMCode=Invitems.UomCode "+
                         " Left join OrdBlock on OrdBlock.Block=MRS_TEMP.BlockCode "+
                         " left join Nature on Nature.NatureCode=MRS_TEMP.NatureCode "+
                         " LEFT JOIN Hod ON MRS_TEmp.HODCode=HOD.HodCode Where  MRS_TEmp.DeptMrsAuth=1 and MRS_TEmp.Qty >= 0 And MRS_TEmp.MrsNo >="+TStNo.getText()+" and MRS_TEmp.MrsNo <="+common.toInt(TEnNo.getText())+" and MRS_TEmp.MillCode="+iMillCode+
                         " ANd  MRS_TEmp.IsConverted=0 and Mrs_Temp.RejectionStatus=0 and MRS_TEmp.ApprovalStatus=0 and MRS_TEmp.ReadyForApproval=0 and MRS_TEmp.StoresRejectionStatus=0 and MRS_TEmp.IsDelete=0 and MRS_TEmp.ItemDelete=0";

                  if(getUserCode((String)JCMrsUsers.getSelectedItem())!=-1)
                  QString = QString+" And AuthUserCode="+getUserCode((String)JCMrsUsers.getSelectedItem());
                  if(getMrsTypeCode((String)JCMrsType.getSelectedItem())!=-1)
                  QString = QString+" And Mrs_Temp.MrsTypeCode="+getMrsTypeCode((String)JCMrsType.getSelectedItem());

          }

          QString = QString+" Order By MrsNo,MRS_TEmp.Id";
		  
		  
		  System.out.println("New "+QString);

          return QString;
     }

     private String getHodName(String SMrsNo)
     {
          int iIndex=-1;

          iIndex = VMrsNo.indexOf(SMrsNo);
          if(iIndex!=-1)
               return common.parseNull((String)VHodName.elementAt(iIndex));
          else
               return "";
     }
     private int getUserCode(String SUserName)
     {
          int iIndex=-1;

          iIndex = VUserName.indexOf(SUserName);
          if(iIndex!=-1)
               return common.toInt((String)VUserCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getMrsTypeCode(String SMrsType)
     {
          int iIndex=-1;

          iIndex = VMrsTypeName.indexOf(SMrsType);
          if(iIndex!=-1)
               return common.toInt((String)VMrsTypeCd.elementAt(iIndex));
          else
               return 0;
     }

     private String getUserName(String SUserCode)
     {
          int iIndex=-1;

          iIndex = VUserCode.indexOf(SUserCode);
          if(iIndex!=-1)
               return (String)VUserName.elementAt(iIndex);
          else
               return "";
     }
 
     public void removeHelpFrame()
     {
          try
          {
               theLayer.remove(this);
               theLayer.repaint();
               theLayer.updateUI();
          }
          catch(Exception ex) { }
     }
     public int getListCount()
     {
          return iCount;
     }

     public void setVector()
     {
          VUnitName     = new Vector();  
          VUnitCode     = new Vector();  
          VDeptName     = new Vector();
          VConditionType= new Vector();
          VConditionTypeCode= new Vector();
          VDeptCode     = new Vector();
          VGroupName    = new Vector();
          VGroupCode    = new Vector();
          VBlockName    = new Vector();
          VBlockCode    = new Vector();
          VNatureName   = new Vector();
          VNatureCode   = new Vector();

          VUrgentName = new Vector();
          VUrgentCode = new Vector();

          VUrgentName.addElement("Ordinary");
          VUrgentName.addElement("Urgent");

          VUrgentCode.addElement("0");
          VUrgentCode.addElement("1");


          String QS1="";
          String QS2= "";
          String QS3= "";
          String QS4= "Select BlockName,Block from OrdBlock Order By 2 ";  //where showStatus=0
          String QS5= "Select Nature,NatureCode from Nature Order By 1 ";

          if(iMillCode!=1)
               QS1 = "Select Unit_Name,Unit_Code From Unit where (MillCode="+iMillCode+" OR MillCode = 2) Order By Unit_Name";
          else
               QS1 = "Select Unit_Name,Unit_Code From Unit where (MillCode="+iMillCode+" OR MillCode = 2) Order By Unit_Name";

          if(iMillCode!=1)
               QS2 = "Select Dept_Name,Dept_Code From Dept where (MillCode=0 OR MillCode = 2) Order By Dept_Name";
          else
               QS2 = "Select Dept_Name,Dept_Code From Dept where (MillCode=1 OR MillCode = 2) Order By Dept_Name";

          if(iMillCode!=1)
               QS3 = "Select Group_Name,Group_Code From Cata where (MillCode=0 OR MillCode = 2) Order By Group_Name";
          else
               QS3 = "Select Group_Name,Group_Code From Cata where (MillCode=1 OR MillCode = 2) Order By Group_Name";


          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS1);

               while(result.next())
               {
                    VUnitName.addElement(result.getString(1));
                    VUnitCode.addElement(result.getString(2));
               }
               result.close();
               result          = theStatement.executeQuery(QS2);
               while(result.next())
               {
                    VDeptName.addElement(result.getString(1));
                    VDeptCode.addElement(result.getString(2));
               }
               result.close();
               result          = theStatement.executeQuery(QS3);
               while(result.next())
               {
                    VGroupName.addElement(result.getString(1));
                    VGroupCode.addElement(result.getString(2));
               }
               result.close();
               result          = theStatement.executeQuery(QS4);
               while(result.next())
               {
                    VBlockName.addElement(result.getString(1));
                    VBlockCode.addElement(result.getString(2));
               }
               result.close();
               result          = theStatement.executeQuery(QS5);
               while(result.next())
               {
                    VNatureName.addElement(result.getString(1));
                    VNatureCode.addElement(result.getString(2));
               }
               result.close();
               result          = theStatement.executeQuery("Select Code,Type from ConditionlesType order by code");
               while(result.next())
               {
                    VConditionTypeCode.addElement(result.getString(1));
                    VConditionType.addElement(result.getString(2));
               }
               result.close();

               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private int getUnitCode(String SUnitName)
     {
          int iIndex=-1;

          iIndex = VUnitName.indexOf(SUnitName);
          if(iIndex!=-1)
               return common.toInt((String)VUnitCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getDeptCode(String SDeptName)
     {
          int iIndex=-1;

          iIndex = VDeptName.indexOf(SDeptName);
          if(iIndex!=-1)
               return common.toInt((String)VDeptCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getConditionTypeCode(String SName)
     {
          int iIndex=-1;

          iIndex = VConditionType.indexOf(SName);
          if(iIndex!=-1)
               return common.toInt((String)VConditionTypeCode.elementAt(iIndex));
          else
               return 0;
     }

     private String getDeptName(int iDeptCode)
     {
          int iIndex=-1;

          iIndex = VDeptCode.indexOf(""+iDeptCode);
          if(iIndex!=-1)
               return (String)VDeptName.elementAt(iIndex);
          else
               return "";
     }

     private int getGroupCode(String SGroupName)
     {
          int iIndex=-1;

          iIndex = VGroupName.indexOf(SGroupName);
          if(iIndex!=-1)
               return common.toInt((String)VGroupCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getBlockCode(String SBlockName)
     {
          int iIndex=-1;

          iIndex = VBlockName.indexOf(SBlockName);
          if(iIndex!=-1)
               return common.toInt((String)VBlockCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getUrgentCode(String SUrgentName)
     {
          int iIndex=-1;

          iIndex = VUrgentName.indexOf(SUrgentName);
          if(iIndex!=-1)
               return common.toInt((String)VUrgentCode.elementAt(iIndex));
          else
               return 0;
     }
     private int getNatureCode(String SNatureName)
     {
          int iIndex=-1;

          iIndex = VNatureName.indexOf(SNatureName);
          if(iIndex!=-1)
               return common.toInt((String)VNatureCode.elementAt(iIndex));
          else
               return 0;
     }
     private class ActList1 implements ActionListener
     {
          int iRow=0;
          public ActList1(int iRow)
          {
               this.iRow = iRow;
          }
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==mrsrecordframe.BOk)               
               {
                    setModifyData(iRow);
                    mrsrecordframe.removeHelpFrame();
                    bFlag  = true;
               }
               if(ae.getSource()==mrsrecordframe.BCancel)               
               {
                    mrsrecordframe.removeHelpFrame();
                    bFlag  = true;
               }


          }
     }
     private class ActList3 implements ActionListener
     {
          public ActList3()
          {
          }
          public void actionPerformed(ActionEvent ae)
          {
               theFrame.placeEnquiry();
               theModel.setNumRows(0);
               setDataIntoVector(1);
               setRowData();
               setColor();
               iApplyCount=1;
               theFrame.removeFrame();
          }
     }
     private class ActList2 implements ActionListener
     {
          public ActList2()
          {
          }
          public void actionPerformed(ActionEvent ae)
          {

               ratechart.BSave.setEnabled(false);


                    Connection theConnection = null;

                    if(!isValidData())
                    {
                       ratechart.BSave.setEnabled(true);

                         return;
                    }

                    try
                    {
                         Class.forName("oracle.jdbc.OracleDriver");
                         theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
     
                         for(int i=0; i<ratechart.theModel.getRows(); i++)
                         {
                              Boolean bFlag = (Boolean)ratechart.theModel.getValueAt(i,16);
     
                              double dNet   = common.toDouble((String)ratechart.theModel.getValueAt(i,14));

                              if(bFlag && dNet>0)
                              {
     
                                        theConnection.setAutoCommit(false);
     
                                        String SItemCode    =   (String)ratechart.theModel.getValueAt(i,0);
                                        String SItemName    =   (String)ratechart.theModel.getValueAt(i,1);
                                        String SSupName     =   (String)ratechart.theModel.getValueAt(i,2);
                                        String SQty         = "1";
                                        String SRate        =   (String)ratechart.theModel.getValueAt(i,3);
     
                                        String SDiscPer     =   (String)ratechart.theModel.getValueAt(i,4);
                                        String SDisc        =   (String)ratechart.theModel.getValueAt(i,5);
                                        String SCenVatPer   =   (String)ratechart.theModel.getValueAt(i,6);
                                        String SCenvat      =   (String)ratechart.theModel.getValueAt(i,7);
                                        String STaxPer      =   (String)ratechart.theModel.getValueAt(i,8);
                                        String STax         =   (String)ratechart.theModel.getValueAt(i,9);
                                        String SCstPer      =   (String)ratechart.theModel.getValueAt(i,10);
                                        String SCst         =   (String)ratechart.theModel.getValueAt(i,11);
                                  
                                        String SSurPer      =   (String)ratechart.theModel.getValueAt(i,12);
                                        String SSur         =   (String)ratechart.theModel.getValueAt(i,13);

                                        String SNet         =    (String)ratechart.theModel.getValueAt(i,14);
                                        String SDocId       =    (String)ratechart.theModel.getValueAt(i,15);
     
     
                                        PreparedStatement thePrepare = theConnection.prepareStatement(getQS());

                                        thePrepare.setString(1,"0");
                                        thePrepare.setString(2,"0");
                                        thePrepare.setString(3,"0");
                                        thePrepare.setString(4,SSupName);
                                        thePrepare.setString(5,SItemCode);
                                        thePrepare.setString(6,SQty);
                                        thePrepare.setString(7,SRate);
                                        thePrepare.setString(8,SDiscPer);
                                        thePrepare.setString(9,SCenVatPer);
                                        thePrepare.setString(10,STaxPer);
                                        thePrepare.setString(11,SSurPer);
                                        thePrepare.setString(12,SNet);
                                        thePrepare.setInt(13,iUserCode); 
                                        thePrepare.setString(14,SCstPer);
                                        thePrepare.setString(15,SCst);
                                        thePrepare.setString(16,SDisc);
                                        thePrepare.setString(17,SCenvat);
                                        thePrepare.setString(18,STax);
                                        thePrepare.setString(19,SSur);
                                        thePrepare.setString(20,"0");
                                        thePrepare.setString(21,SNet);
                                        thePrepare.setString(22,"0");      // ComprsionNo
                                        thePrepare.setInt(23,iUserCode);   // UserCode
                                        thePrepare.setString(24,common.getLocalHostName()); // UserCode
                                        thePrepare.setString(25,"3");
                                        thePrepare.setString(26,SDocId);

                                        thePrepare.executeUpdate();
                                        thePrepare.close();

                                        theConnection.commit();
                              }
                         }
                         theConnection.close();

                    }
                    catch(Exception ex)
                    {
                         try
                         {
                              ex.printStackTrace();
                              ratechart.BSave.setEnabled(true);

                              JOptionPane.showMessageDialog(null," Problem in Storing Data");
                              theConnection.rollback();
                              return;
                         }
                         catch(Exception e)
                         {
                              
                         }
                    }
                    theModel.setNumRows(0);
                    setDataIntoVector(1);
                    setRowData();
                    setColor();
                    JOptionPane.showMessageDialog(null," Direct Rate Chart Stored Successfully");
                    ratechart.BSave.setEnabled(true);
                    iApplyCount=1;
                    ratechart.removeFrame();
          }
     }

     public void setModifyData(int iRow)
     {
          try
          {


               double dValue = common.toDouble(mrsrecordframe.TRate.getText())*common.toDouble(mrsrecordframe.TQty.getText());

               theModel.setValueAt(mrsrecordframe.TMatCode.getText(),iRow,ItemCode);
               theModel.setValueAt(mrsrecordframe.BMatName.getText(),iRow,ItemName);
               theModel.setValueAt(mrsrecordframe.TQty.getText(),iRow,Qty);

               if(CUnit.isSelected()==true)
               {
                    theModel.setValueAt(mrsrecordframe.JCUnit.getSelectedItem(),iRow,Unit);
               }
               if(CDepartment.isSelected()==true)
               {
                    theModel.setValueAt(mrsrecordframe.JCDept.getSelectedItem(),iRow,Department);
               }
               if(CGroup.isSelected()==true)
               {
                    theModel.setValueAt(mrsrecordframe.JCGroup.getSelectedItem(),iRow,Group);
               }
               if(CBlock.isSelected()==true)
               {
                    theModel.setValueAt(mrsrecordframe.JCBlock.getSelectedItem(),iRow,Block);
               }
               if(CUrgent.isSelected()==true)
               {
                    theModel.setValueAt(mrsrecordframe.JCUrgOrd.getSelectedItem(),iRow,Urgent);
               }
               if(CNature.isSelected()==true)
               {
                    theModel.setValueAt(mrsrecordframe.JCNature.getSelectedItem(),iRow,Nature);
               }
               if(CDueDate.isSelected()==true)
               {
                    theModel.setValueAt(common.parseDate(mrsrecordframe.TDueDate.toNormal()),iRow,DueDate);
               }
               if(CCatl.isSelected()==true)
               {
                    theModel.setValueAt(mrsrecordframe.TMatCatl.getText(),iRow,Catl);
               }
               if(CDraw.isSelected()==true)
               {
                    theModel.setValueAt(mrsrecordframe.TMatDraw.getText(),iRow,Draw);
               }
/*            if(CStock.isSelected()==true)
               {
                    theModel.setValueAt(mrsrecordframe.TStock.getText(),iRow,Draw);
               } */

               theModel.setValueAt(mrsrecordframe.TMake.getText(),iRow,Make);
               if(CRemarksExt.isSelected()==true)
               {
                    theModel.setValueAt(mrsrecordframe.TRemarks.getText(),iRow,Remarks);
               }
               if(CRate.isSelected()==true)
               {
                    theModel.setValueAt(mrsrecordframe.TRate.getText(),iRow,Rate);
                    theModel.setValueAt(common.getRound(dValue,2),iRow,TotalValue);
               }
               theModel.setValueAt(mrsrecordframe.TReasonForMrs.getText(),iRow,Reason);
               setPurchaseDetails(mrsrecordframe.TMatCode.getText(),iRow);
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     private void setRecordData(int iRow)
     {
          mrsrecordframe.TMatCode.setText(((String)theModel.getValueAt(iRow,ItemCode)));
          mrsrecordframe.BMatName.setText(((String)theModel.getValueAt(iRow,ItemName)));
          if(CCatl.isSelected()==true)
          {
               mrsrecordframe.TMatCatl.setText(((String)theModel.getValueAt(iRow,Catl)));
          }
          mrsrecordframe.TMake.setText(((String)theModel.getValueAt(iRow,Make)));
          if(CDraw.isSelected()==true)
          {
               mrsrecordframe.TMatDraw.setText(((String)theModel.getValueAt(iRow,Draw)));
          }
          mrsrecordframe.TQty.setText(((String)theModel.getValueAt(iRow,Qty)));
          mrsrecordframe.TDueDays.setText("");
          if(CDueDate.isSelected()==true)
          {
               mrsrecordframe.TDueDate.fromString(((String)theModel.getValueAt(iRow,DueDate)));
          }
          if(CUnit.isSelected()==true)
          {
               mrsrecordframe.JCUnit.setSelectedItem(((String)theModel.getValueAt(iRow,Unit)));
          }
          if(CDepartment.isSelected()==true)
          {
               mrsrecordframe.JCDept.setSelectedItem(((String)theModel.getValueAt(iRow,Department)));
          }
          if(CGroup.isSelected()==true)
          {
               mrsrecordframe.JCGroup.setSelectedItem(((String)theModel.getValueAt(iRow,Group)));
          }
          if(CBlock.isSelected()==true)
          {
               mrsrecordframe.JCBlock.setSelectedItem(((String)theModel.getValueAt(iRow,Block)));
          }
          if(CUrgent.isSelected()==true)
          {
               mrsrecordframe.JCUrgOrd.setSelectedItem(((String)theModel.getValueAt(iRow,Urgent)));
          }
          if(CNature.isSelected()==true)
          {
               mrsrecordframe.JCNature.setSelectedItem(((String)theModel.getValueAt(iRow,Nature)));
          }
          if(CRemarksExt.isSelected()==true)
          {
               mrsrecordframe.TRemarks.setText(((String)theModel.getValueAt(iRow,Remarks)));
          }
          if(CRate.isSelected()==true)
          {
               mrsrecordframe.TRate.setText(((String)theModel.getValueAt(iRow,Rate)));
          }
          mrsrecordframe.TReasonForMrs.setText(((String)theModel.getValueAt(iRow,Reason)));
   
          String SNature  = "";

          if(CNature.isSelected()==true)
          {
               SNature = ((String)theModel.getValueAt(iRow,Nature));
          }
          if(SNature.equals(""))
          {
               mrsrecordframe.JCNature.setSelectedItem("Consumable");
          }

     }
     public void setVector1()
     {
          VCode.removeAllElements();
          VName.removeAllElements();
          VNameCode.removeAllElements();
          VCata.removeAllElements();
          VDraw.removeAllElements();

          VPaperColor.removeAllElements();
          VPaperSets .removeAllElements();
          VPaperSize .removeAllElements();
          VPaperSide .removeAllElements();
          VSlipNo    .removeAllElements();

          String QS8="";

               QS8 = " Select Item_Name,Item_Code,UomName,Catl,Draw,PaperColor,PaperSets,PaperSize,PaperSide,LastSlipNo+1 From InvItems "+
                     " Inner join Uom On Uom.UomCode=InvItems.UomCode "+
                     " Order By Item_Name";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS8);
               while(result.next())
               {
                    String SName = result.getString(1);
                    String SCode = result.getString(2);
                    String SUom  = result.getString(3);
                    String SCata = result.getString(4);
                    String SDraw = result.getString(5);

                    VPaperColor.addElement(result.getString(6));
                    VPaperSets.addElement(result.getString(7));
                    VPaperSize.addElement(result.getString(8));
                    VPaperSide.addElement(result.getString(9));
                    VSlipNo.addElement(result.getString(10));

                    VName.addElement(SName);
                    VCode.addElement(SCode);
                    VNameCode.addElement(SName+" - "+SUom+" (Code : "+SCode+")"+" (Catl : "+SCata+")"+" (Draw : "+SDraw+")");
                    VCata.addElement(SCata);
                    VDraw.addElement(SDraw);
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void updateData(int i,Statement theStatement,int iStatus) throws Exception
     {
               String SNetRate="";
               String SNetValue="";
               String SMrsAuth="";


               double dQty = common.toDouble((String)theModel.getValueAt(i,Qty));

               if(CRate.isSelected()==true)
               {
                    SNetRate  = (String)theModel.getValueAt(i,Rate);
                    SNetValue = common.getRound(dQty * common.toDouble(SNetRate),2);
               }
               else
               {
                    SNetValue = (String)theModel.getValueAt(i,TotalValue);
               }
               int iId   = common.toInt((String)VId.elementAt(i));  // common.toInt((String)theModel.getValueAt(i,Id));



               Boolean  bAppValue     =   (Boolean)theModel.getValueAt(i,Appr);
               Boolean  bToOrder      =   (Boolean)theModel.getValueAt(i,ToOrder);


               String QS1 = "Update MRS_TEMP Set ";
//               QS1 = QS1+"MRSDate  = '"+common.pureDate((String)theModel.getValueAt(i,Date))+"',";
               QS1 = QS1+"Item_Code= '"+((String)theModel.getValueAt(i,ItemCode))+"',";
               QS1 = QS1+"Item_Name= '"+common.getNarration(((String)theModel.getValueAt(i,ItemName)))+"',";
               QS1 = QS1+"RefNo=0"+(String)theModel.getValueAt(i,MrsNo)+",";
               if(CCatl.isSelected()==true)
               {
                    QS1 = QS1+"Catl='"+((String)theModel.getValueAt(i,Catl))+"',";
               }
               if(CDraw.isSelected()==true)
               {
                    QS1 = QS1+"Draw='"+((String)theModel.getValueAt(i,Draw))+"',";
               }
               QS1 = QS1+"Qty=0"+((String)theModel.getValueAt(i,Qty))+",";
               if(CUnit.isSelected()==true)
               {
                    QS1 = QS1+"Unit_Code=0"+getUnitCode(((String)theModel.getValueAt(i,Unit)))+",";
               }
               if(CDepartment.isSelected()==true)
               {
                    QS1 = QS1+"Dept_Code=0"+getDeptCode(((String)theModel.getValueAt(i,Department)))+",";
               }

               if(CGroup.isSelected()==true)
               {
                    QS1 = QS1+"Group_Code=0"+getGroupCode(((String)theModel.getValueAt(i,Group)))+",";
               }
               QS1 = QS1+"Serv_Code=0,";
               QS1 = QS1+"Mac_Code=0,";
               if(CBlock.isSelected()==true)
               {
                    QS1 = QS1+"BlockCode=0"+getBlockCode(((String)theModel.getValueAt(i,Block)))+",";
               }
               if(CNature.isSelected()==true)
               {
                    QS1 = QS1+"NatureCode=0"+getNatureCode(((String)theModel.getValueAt(i,Nature)))+",";
               }
               if(CUrgent.isSelected()==true)
               {
                    QS1 = QS1+"UrgOrd=0"+getUrgentCode(((String)theModel.getValueAt(i,Urgent)))+",";
               }
               if(CRemarksExt.isSelected()==true)
               {
                    QS1 = QS1+"Remarks='"+common.getNarration((String)theModel.getValueAt(i,Remarks))+"',";
               }
               QS1 = QS1+"Make   ='"+((String)theModel.getValueAt(i,Make))+"',";
               if(CDueDate.isSelected()==true)
               {
                    QS1 = QS1+"DueDate='"+common.pureDate((String)theModel.getValueAt(i,DueDate))+"',";
               }
               if(CRate.isSelected()==true)
               {
                    QS1 = QS1+"NetRate=0"+SNetRate+",";
               }
               QS1 = QS1+"NetValue=0"+SNetValue+",";
               QS1 = QS1+"ReasonForMrs='"+common.getNarration((String)theModel.getValueAt(i,Reason))+"',";

               if(iStatus==0)
               {
                    if(bAppValue)
                    {
                         QS1= QS1+"ReadyForApprovalSub=1"+",";
     
                    }
                    if(bToOrder)
                    {
                         QS1= QS1+"ConvertToOrder=1"+",";
     
     
                    }
                    QS1 = QS1+"ReadyForApprovalSubUserCode="+iUserCode+",";
                    QS1 = QS1+"ReadyForApprovalSubSystemName='"+"smb3"+"',";

               }

               QS1 = QS1+"MillCode="+iMillCode+",ModifiedUserCode="+iUserCode+",ModifiedDateTime='"+common.getServerDate()+"' Where id = "+iId;

               theStatement.executeQuery(QS1);
     }
     private void setEditablefalse(int iRow)
     {
          

     }
     private void setPurchaseDetails(String SItemCode,int iRow)
     {
          try
          {
               String QS = " Select Temp1Mrs_TempOrder.OrderDate as LastPurchaseDate,Temp1Mrs_TempOrder.Rate as LastPurchaseRate,"+
                         " Temp1Mrs_TempOrder.DiscPer,Temp1Mrs_TempOrder.CenvatPer,Temp1Mrs_TempOrder.TaxPer,Temp1Mrs_TempOrder.SurPer,"+
                         " Temp1Mrs_TempOrder.Netrate,Temp1Mrs_TempOrder.Name "+
                         " From Temp1Mrs_TempOrder"+
                         " where Temp1Mrs_TempOrder.Item_Code='"+SItemCode+"'";

               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();

               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               if(result.next())
               {
                    theModel.setValueAt(result.getString(8),iRow,RCPrevSup);
                    theModel.setValueAt(common.parseDate(result.getString(1)),iRow,RCPrevLastDate);
                    theModel.setValueAt(result.getString(2),iRow,RCPrevLastRate);
                    if(CDisc.isSelected()==true)
                    {
                         theModel.setValueAt(result.getString(3),iRow,DiscPer);
                    }
                   
                    if(CCenvat.isSelected()==true)
                    {
                         theModel.setValueAt(result.getString(4),iRow,CenvatPer);
                    }
                    if(CTax.isSelected()==true)
                    {
                         theModel.setValueAt(result.getString(5),iRow,TaxPer);
                    }
                    if(CSurcharge.isSelected()==true)
                    {
                         theModel.setValueAt(result.getString(6),iRow,SurPer);
                    }
                    if(CNetRate.isSelected()==true)
                    {
                         theModel.setValueAt(result.getString(7),iRow,NetRate);
                    }
               }
               result.close();
               theStatement.close();
          }catch(Exception ex)
          {
              System.out.println(" Purchase Details "+ex);
              ex.printStackTrace();
          }

     }
     private void printData()
     {
          String SHead ="",STitle="";          
          String SLine ="",SLine1="";
          String[] SAlign = new String[20];
          int[]    iLength= new int[20];

          int      iWidth[]={10,10,10,13,50,13,13,40,5,10,10,15,
                            15,7,8,8,40,6,6,10};


          for(int i=0; i<20; i++)
          {
               if(i==0)
               {
                    SLine = "|"+common.Replicate("-",(iWidth[i]))+"-";

                    SLine1= "|"+common.Replicate("-",(iWidth[i]))+"-";
               }
               if(i>0 && i<19)
               {
                    SLine =SLine+common.Replicate("-",(iWidth[i]))+"-";
                    SLine1=SLine1+common.Replicate("-",(iWidth[i]))+"-";

               }
               if(i==19)
               {
                    SLine =SLine+common.Replicate("-",(iWidth[i]))+"|";
                    SLine1=SLine1+common.Replicate("-",(iWidth[i]))+"|";
               }

               SHead =SHead+"|"+common.Pad(theModel.ColumnName[i],iWidth[i])+"";
               if(i==19)
                SHead= SHead+"|";

               if(theModel.ColumnType[i]=="N")
                    SAlign[i]="R";
               else
                    SAlign[i]="L";
               iLength[i]= (iWidth[i]); //+2
          }

          SHead = SLine+"\n"+SHead+"\n"+SLine1+"\n";

          try
          {
               FileWriter FW = new FileWriter("D:\\MrsList.prn");
               STitle =" Amarjothi Spinning Mills Ltd"+"\n";
               STitle = STitle+" Mrs List For Approval"+"\n";
               STitle = STitle+" g\n";

               PrnGenerationWithTotal prngeneration = new   PrnGenerationWithTotal(FW,STitle,SHead,SAlign,iLength);

               for(int i=0; i<theModel.getRows(); i++)
               {
                    String[] SBody  = new String[20];
                    for(int j=0; j<20; j++)
                    {
                        SBody[j] =(String)theModel.getValueAt(i,j);
                    }
                    prngeneration.setBodyPrn(SBody);
               }
               prngeneration.setFootPrn();
               FW.close();
               JOptionPane.showMessageDialog(null," Prn Generated in D:\\MrsList.prn");

          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null," Problem in Generating Prn");
               System.out.println(ex);
          }
     }

     private boolean validData()
     {
          boolean bFlag=true;
          try
          {
               int iCount = 0;

               for(int i=0; i<theModel.getRows(); i++)
               {
                    Boolean  bAppValue     =   (Boolean)theModel.getValueAt(i,Appr);
                    Boolean  bToOrder      =   (Boolean)theModel.getValueAt(i,ToOrder);

                    Boolean  bJmdMrsApp    =   (Boolean)theModel.getValueAt(i,JmdMrsApproval);      //false; //
                    Boolean  bJmdOrderApp  =  (Boolean)theModel.getValueAt(i,JmdOrderApproval);    // false; //

                    String SItemCode       = (String)theModel.getValueAt(i,ItemCode);

                    if(bAppValue)
                    {

                                                     
                         String SRCSupName  =   (String)theModel.getValueAt(i,SupName);
                         String SSupName    =   (String)theModel.getValueAt(i,RCPrevSup);
                         String SType       =  "NIL";
                         if(CCondition.isSelected()==true)
                         {
                              SType       =   (String)theModel.getValueAt(i,ConditionType);
                         }

                         if(!SType.equals("NIL"))
                              continue;

                         if(SSupName.length()<=0 && SRCSupName.length()<=0)
                         {
                              JOptionPane.showMessageDialog(null," Enquiry Not Updated Or Rate Chart Not Updated..  "+SItemCode);
                              bFlag=false;
                              break;
                         }
                         int iStatus    = common.toInt((String)VOrderStatus.elementAt(i));
                         int iEnqStatus    = common.toInt((String)VEnquiryStatus.elementAt(i));

                         if(iStatus>0 && !bToOrder && iEnqStatus==0)
                         {

                              JOptionPane.showMessageDialog(null," To Order Conversion Not Selected for..  "+SItemCode);
                              bFlag=false;
                              break;
                         }
                    }
                    System.out.println(bJmdMrsApp+","+bJmdOrderApp+",");
/*                  if((bJmdMrsApp || bJmdOrderApp)  && (form==null)  )  
                    {


                         JOptionPane.showMessageDialog(null,"S.O Punching Needed 1 "+SItemCode);

                         bFlag=false;
                    }

                    if((bJmdMrsApp || bJmdOrderApp) && form!=null && !form.getVerified())
                    {
                         System.out.println(bJmdMrsApp+","+bJmdOrderApp+","+form.getVerified());

                         JOptionPane.showMessageDialog(null,"S.O Punching Needed 2  "+SItemCode);

                         bFlag=false;
                    } */



                    int iPrevOrderDate     =   common.toInt(common.pureDate((String)theModel.getValueAt(i,RCPrevLastDate)));

                    System.out.println(iPrevOrderDate+","+RCPrevLastDate);
                    

                    if(iPrevOrderDate<20170701 && bToOrder) 
                    {
                         JOptionPane.showMessageDialog(null,"Previous purchase Before 01.07.2017 Please deselect ConvertToOrder "+SItemCode);

                         bFlag =false;
                    }


               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bFlag=false;
               JOptionPane.showMessageDialog(null," Problem in Storing Contact Mr.Mohan Raj..");

               ex.printStackTrace();
          }
          return bFlag;
     }
     private boolean checkOrderAutoApproveStatus()
     {
          boolean bFlag=true;
          try
          {
               int iCount = 0;

               for(int i=0; i<theModel.getRows(); i++)
               {
                    Boolean  bJmdOrderApp  =false; //   (Boolean)theModel.getValueAt(i,JmdOrderApproval);


                    if(bJmdOrderApp)
                    {

                         bFlag=true;
                         break;
                    }              
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bFlag=false;
               JOptionPane.showMessageDialog(null," Problem in Storing Contact Mr.Mohan Raj..");

               ex.printStackTrace();
          }
          return bFlag;
     }

     private String getSupplierList(String SItemCode,String SMrsNo)
     {
          StringBuffer  SSupplierList = new StringBuffer();

          SSupplierList.append("<html>");
          try
          {
               String QS = " Select Supplier.Name from Enquiry"+
                           " Inner join Supplier on Supplier.Ac_Code=Enquiry.Sup_Code"+
                           " where Mrs_No=? and Item_Code=?";


               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();
               PreparedStatement    thePrepare = theConnection.prepareStatement(QS);

               thePrepare.setString(1,SMrsNo);
               thePrepare.setString(2,SItemCode);

               ResultSet theResult = thePrepare.executeQuery();
               while(theResult.next())
               {
                    String SSupplier = theResult.getString(1);
                    SSupplierList.append("<p>"+SSupplier+"</p>");
               }
               theResult.close();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          SSupplierList.append("</html>");

          return SSupplierList.toString();
     }


     private String getMrsTempIds()
     {
          String SId ="";
          for(int i=0; i<theModel.getRows(); i++)
          {
               Boolean  bAppValue     =   (Boolean)theModel.getValueAt(i,Appr);

               if(bAppValue)
               {
                    SId = SId+(String)VId.elementAt(i)+",";     //(String)theModel.getValueAt(i,Id)
               }
          }
          if(SId.length()>0)
               SId=SId.substring(0,SId.length()-1);

          return SId;
     }


     private void setColumns()
     {

          //iSize = getColSize()+22; //20
		  
		  //iSize = getColSize()+25; //20
		  
          iSize = getColSize()+22; //20
		  
		  iSize = getColSize()+26; //20
		  

          ColumnName      = new String[iSize];  //20
          ColumnType      = new String[iSize];  //20

          int iColNo=0;

          ColumnName[iColNo]   = "Date" ;
          ColumnType[iColNo]   = "S";
          iColNo=iColNo+1;

          ColumnName[iColNo]   = "MrsNo" ;
          ColumnType[iColNo]   = "S";
          iColNo=iColNo+1;

          ColumnName[iColNo]   = "MrsType" ;
          ColumnType[iColNo]   = "S";
          iColNo=iColNo+1;

          ColumnName[iColNo]   = "Code" ;
          ColumnType[iColNo]   = "S";
          iColNo=iColNo+1;

          ColumnName[iColNo]   = "Name" ;
          ColumnType[iColNo]   = "S";
          iColNo=iColNo+1;

          ColumnName[iColNo]   = "EntryCode" ;
          ColumnType[iColNo]   = "S";
          iColNo=iColNo+1;

          ColumnName[iColNo]   = "EntryName" ;
          ColumnType[iColNo]   = "S";
          iColNo=iColNo+1;



          ColumnName[iColNo]   = "Make" ;
          ColumnType[iColNo]   = "E";
          iColNo=iColNo+1;

          if(CCatl.isSelected()==true)
          {
               ColumnName[iColNo]   = "Catl" ;
               ColumnType[iColNo]   = "E";
               Catl                 = iColNo;
               iColNo               = iColNo+1;

          }

          if(CDraw.isSelected()==true)
          {
               ColumnName[iColNo]   = "Draw" ;
               ColumnType[iColNo]   = "E";
               Draw                 = iColNo;
               iColNo               = iColNo+1;

          }

          if(CStock.isSelected()==true)
          {

               ColumnName[iColNo]   = "Stock" ;
               ColumnType[iColNo]   = "S";
               iColNo=iColNo+1;

          }
          if(CRemarksExt.isSelected()==true)
          {
               ColumnName[iColNo]   = "Remarks(Ext)" ;
               ColumnType[iColNo]   = "E";
               Remarks              = iColNo;
               iColNo               = iColNo+1;

          }


          ColumnName[iColNo]   = "Qty" ;
          ColumnType[iColNo]   = "E";
          Qty                  = iColNo;
          iColNo               = iColNo+1;


          if(CUom.isSelected()==true)
          {
               ColumnName[iColNo]   = "Uom";
               ColumnType[iColNo]   = "E";
               Uom                  = iColNo;
               iColNo               = iColNo+1;

          }
          if(CUnit.isSelected()==true)
          {

               ColumnName[iColNo]   = "Unit";
               ColumnType[iColNo]   = "E";
               Unit                 = iColNo;
               iColNo               = iColNo+1;

          }

          if(CDepartment.isSelected()==true)
          {

               ColumnName[iColNo]   = "Department";
               ColumnType[iColNo]   = "E";
               Department           = iColNo;
               iColNo               = iColNo+1;


          }
          if(CGroup.isSelected()==true)
          {

               ColumnName[iColNo]   = "Group";
               ColumnType[iColNo]   = "E";
               Group                = iColNo;
               iColNo               = iColNo+1;


          }
          if(CBlock.isSelected()==true)
          {

               ColumnName[iColNo]   = "Block";
               ColumnType[iColNo]   = "E";
               Block                = iColNo;
               iColNo               = iColNo+1;


          }
          if(CUrgent.isSelected()==true)
          {
               ColumnName[iColNo]   = "Urgent";
               ColumnType[iColNo]   = "S";
               Urgent               = iColNo;
               iColNo               = iColNo+1;


          }
          if(CNature.isSelected()==true)
          {
               ColumnName[iColNo]   = "Nature";
               ColumnType[iColNo]   = "S";
               Nature               = iColNo;
               iColNo               = iColNo+1;

          }


          ColumnName[iColNo]   = "Reason";
          ColumnType[iColNo]   = "S";
          Reason      = iColNo;
          iColNo =iColNo+1;


          if(CRate.isSelected()==true)
          {

               ColumnName[iColNo]   = "Rate";
               ColumnType[iColNo]   = "E";
               Rate      = iColNo;
               iColNo =iColNo+1;


          }

          ColumnName[iColNo]   = "TValue";
          ColumnType[iColNo]   = "S";
          TotalValue           = iColNo;
          iColNo               = iColNo+1;


          if(CStatus.isSelected()==true)
          {

               ColumnName[iColNo]   = "Status";
               ColumnType[iColNo]   = "S";
               Status               = iColNo;
               iColNo               = iColNo+1;

          }
          if(CDueDate.isSelected()==true)
          {

               ColumnName[iColNo]   = "DueDate";
               ColumnType[iColNo]   = "S";
               DueDate              = iColNo;
               iColNo               = iColNo+1;

          }
          if(CCondition.isSelected()==true)
          {

               ColumnName[iColNo]   = "ConditionType" ;
               ColumnType[iColNo]   = "E";
               ConditionType         = iColNo;
               iColNo               = iColNo+1;

          }



          ColumnName[iColNo]   = "Appr" ;
          ColumnType[iColNo]   = "B";
          Appr                 = iColNo;
          iColNo               = iColNo+1;


          if(CReject.isSelected()==true)
          {
               ColumnName[iColNo]   = "Reject";
               ColumnType[iColNo]   = "B";
               Rej         = iColNo;
               iColNo      = iColNo+1;

          }
          if(CRejectReason.isSelected()==true)
          {
               ColumnName[iColNo]   = "RejectReason";
               ColumnType[iColNo]   = "E";
               RejReason            = iColNo;
               iColNo               = iColNo+1;

          }


          ColumnName[iColNo]   = "ToOrder" ;
          ColumnType[iColNo]   = "B";
          ToOrder              = iColNo;
          iColNo               = iColNo+1;


          ColumnName[iColNo]   = "JmdMrsApp." ;
          ColumnType[iColNo]   = "B";
          JmdMrsApproval       = iColNo;
          iColNo               = iColNo+1;


          ColumnName[iColNo]   = "JmdOrderApp." ;
          ColumnType[iColNo]   = "B";
          JmdOrderApproval     = iColNo;
          iColNo               = iColNo+1;


          ColumnName[iColNo]   = "UserName";
          ColumnType[iColNo]   = "S";
          UserName             = iColNo;
          iColNo               = iColNo+1;

          if(CDocId.isSelected()==true)
          {
               ColumnName[iColNo]   = "DocId";
               ColumnType[iColNo]   = "S";
               DocId                = iColNo;
               iColNo               = iColNo+1;

          }

          ColumnName[iColNo]   = "SupName" ;
          ColumnType[iColNo]   = "S";
          SupName              = iColNo;
          iColNo               = iColNo+1;


          ColumnName[iColNo]   = "OrderDate" ;
          ColumnType[iColNo]   = "S";
          OrderDate            = iColNo;
          iColNo               = iColNo+1;


          ColumnName[iColNo]   = "PurRate" ;
          ColumnType[iColNo]   = "S";
          PurRate              = iColNo;
          iColNo               = iColNo+1;


          if(CDisc.isSelected()==true)
          {

               ColumnName[iColNo]   = "DiscPer" ;
               ColumnType[iColNo]   = "S";
               DiscPer              = iColNo;
               iColNo               = iColNo+1;

          }

          if(CCenvat.isSelected()==true)
          {

               ColumnName[iColNo]   = "CenVatPer" ;
               ColumnType[iColNo]   = "S";
               CenvatPer           = iColNo;
               iColNo               = iColNo+1;

          }

          if(CTax.isSelected()==true)
          {
               ColumnName[iColNo]   = "TaxPer" ;
               ColumnType[iColNo]   = "S";
               TaxPer               = iColNo;
               iColNo               = iColNo+1;

          }
          if(CSurcharge.isSelected()==true)
          {

               ColumnName[iColNo]   = "SurPer" ;
               ColumnType[iColNo]   = "S";
               SurPer               = iColNo;
               iColNo               = iColNo+1;

          }
          if(CNetRate.isSelected()==true)
          {

               ColumnName[iColNo]   = "NetRate" ;
               ColumnType[iColNo]   = "S";
               NetRate              = iColNo;
               iColNo               = iColNo+1;

          }
          if(COrderSource.isSelected()==true)
          {
          
               ColumnName[iColNo]   = "OrderSource" ;
               ColumnType[iColNo]   = "S";
               NetRate              = iColNo;
               iColNo               = iColNo+1;

          }

          ColumnName[iColNo]   = "Prev.SupName" ;
          ColumnType[iColNo]   = "S";

          RCPrevSup            = iColNo;
          iColNo               = iColNo+1;


          ColumnName[iColNo]   = "Prev.OrderDate" ;
          ColumnType[iColNo]   = "S";

          RCPrevLastDate       = iColNo;
          iColNo               = iColNo+1;


          ColumnName[iColNo]   = "Prev.PurRate" ;
          ColumnType[iColNo]   = "S";
          RCPrevLastRate       = iColNo;
          iColNo               = iColNo+1;
		  
		  
          ColumnName[iColNo]   = "Service Stock" ;
          ColumnType[iColNo]   = "S";
          RCPrevLastRate       = iColNo;
          iColNo               = iColNo+1;
		  
          ColumnName[iColNo]   = "Non Stock" ;
          ColumnType[iColNo]   = "S";
          RCPrevLastRate       = iColNo;
          iColNo               = iColNo+1;
		  
          ColumnName[iColNo]   = "Other Stock" ;
          ColumnType[iColNo]   = "S";
          RCPrevLastRate       = iColNo;
          iColNo               = iColNo+1;
		  
          ColumnName[iColNo]   = "Rejection Reason" ;
          ColumnType[iColNo]   = "S";
          RCPrevLastRate       = iColNo;
          iColNo               = iColNo+1;


		  
		  

     }

     private void setModel()
     {
          try
          {
               theModel = null;
               theTable = null;
               if(MiddlePanel!=null)
               {
                    MiddlePanel.updateUI();
                    this.remove(MiddlePanel);
               }
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
          MiddlePanel    = new JPanel(true);

          theModel       = new MRSTempListModel(ColumnName,ColumnType,iSize,Rate,TotalValue);
          theTable       = new JTable(theModel);

          RowSorter<TableModel> sorter =
             new TableRowSorter<TableModel>(theModel);

           theTable.setRowSorter(sorter);



/*        for(int i=0; i<iSize; i++)
          {
               TableColumn name = theTable.getColumnModel().getColumn(i);
               name.setPreferredWidth(iColWidth[i]);
          }
*/

          theTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

          MiddlePanel    . setLayout(new BorderLayout());

          MiddlePanel         . add(new JScrollPane(theTable));

          MiddlePanel.updateUI();
//          theTable.getTableHeader().setReorderingAllowed(false);

          if(CDepartment.isSelected()==true)
          {
               TableColumn deptColumn   = theTable.getColumn("Department");
               deptColumn  .setCellEditor(new DefaultCellEditor(JCDepartment));
          }
          if(CUnit.isSelected()==true)
          {
               TableColumn unitColumn   = theTable.getColumn("Unit");
               unitColumn  .setCellEditor(new DefaultCellEditor(JCUnit));
          }
          if(CGroup.isSelected()==true)
          {
               TableColumn groupColumn   = theTable.getColumn("Group");
               groupColumn  .setCellEditor(new DefaultCellEditor(JCGroup));
          }
          if(CBlock.isSelected()==true)
          {
               TableColumn blockColumn   = theTable.getColumn("Block");
               blockColumn  .setCellEditor(new DefaultCellEditor(JCBlock));
          }
          if(CUrgent.isSelected()==true)
          {
               TableColumn urgentColumn   = theTable.getColumn("Urgent");
               urgentColumn  .setCellEditor(new DefaultCellEditor(JCUrgent));
          }
          if(CNature.isSelected()==true)
          {
               TableColumn natureColumn   = theTable.getColumn("Nature");
               natureColumn  .setCellEditor(new DefaultCellEditor(JCNature));
          }
          if(CCondition.isSelected()==true)
          {
               TableColumn natureColumn   = theTable.getColumn("ConditionType");
               natureColumn  .setCellEditor(new DefaultCellEditor(JCConditionType));
          }
          theTable            . addMouseMotionListener
          (
               new MouseMotionAdapter()
               {
                    public void mouseMoved(MouseEvent e)
                    {
                         Point p   = e.getPoint();

                         int iRow  = theTable.rowAtPoint(p);
                         int iCol  = theTable.columnAtPoint(p);


                         String SMrsNo   = (String)theModel.getValueAt(iRow,MrsNo);
                         String SItemCode= (String)theModel.getValueAt(iRow,ItemCode);
                         
                         String SSupplierList = getSupplierList(SItemCode,SMrsNo);

                         theTable  . setToolTipText(SSupplierList);
                         
                    }
               }
          );

          theTable.addMouseListener(new MouseList());

          theTable.addKeyListener(new KeyList(this));



          getContentPane()    . add(MiddlePanel,BorderLayout.CENTER);
     }
     private int getColSize()
     {
          int iSize =0;


          if(CCatl.isSelected()==true)
          {
               iSize = iSize+1;

          }

          if(CDraw.isSelected()==true)
          {
               iSize = iSize+1;

          }

          if(CStock.isSelected()==true)
          {
               iSize = iSize+1;

          }

          if(CRemarksExt.isSelected()==true)
          {
               iSize = iSize+1;

          }

          if(CUom.isSelected()==true)
          {
               iSize = iSize+1;
          }
          if(CUnit.isSelected()==true)
          {
               iSize = iSize+1;
          }
          if(CDepartment.isSelected()==true)
          {
               iSize = iSize+1;
          }
          if(CGroup.isSelected()==true)
          {
               iSize = iSize+1;
          }
          if(CBlock.isSelected()==true)
          {
               iSize = iSize+1;
          }
          if(CUrgent.isSelected()==true)
          {
               iSize = iSize+1;
          }
          if(CNature.isSelected()==true)
          {
               iSize = iSize+1;
          }

          if(CRate.isSelected()==true)
          {
               iSize = iSize+1;
          }
          if(CStatus.isSelected()==true)
          {
               iSize = iSize+1;
     
          }
          if(CDueDate.isSelected()==true)
          {
               iSize = iSize+1;

          }
          if(CReject.isSelected()==true)
          {
               iSize = iSize+1;

          }
          if(CRejectReason.isSelected()==true)
          {
               iSize = iSize+1;
          }
          if(CDocId.isSelected()==true)
          {
               iSize = iSize+1;
          }
          if(CDisc.isSelected()==true)
          {
               iSize = iSize+1;
          }

          if(CCenvat.isSelected()==true)
          {
               iSize = iSize+1;
          }

          if(CTax.isSelected()==true)
          {
               iSize = iSize+1;
          }
          if(CCondition.isSelected()==true)
          {
               iSize = iSize+1;
          }

          if(CSurcharge.isSelected()==true)
          {
               iSize = iSize+1;
          }
          if(CNetRate.isSelected()==true)
          {
               iSize = iSize+1;
          }
          if(COrderSource.isSelected()==true)
          {
               iSize = iSize+1;
          }

          return iSize;
     }
     private int getOrderSourceCode(String SOrderSource)
     {
          if(SOrderSource.equals("PrevOrder"))
               return 1;
          if(SOrderSource.equals("RateChart"))
               return 3;

          return 0;
     }
     private boolean isValidData()
     {
          Boolean bValue=true;

          for(int i=0; i<ratechart.theModel.getRows(); i++)
          {
               Boolean bFlag = (Boolean)ratechart.theModel.getValueAt(i,16);

               double dNet    = common.toDouble((String)ratechart.theModel.getValueAt(i,14));
               String SSupName= (String)ratechart.theModel.getValueAt(i,2);
               String SDocId  = (String)ratechart.theModel.getValueAt(i,15);


               if(bFlag)
               {
                    if(dNet<=0 )
                    {
                         JOptionPane.showMessageDialog(null," NetValue is Not Valid in Row"+(i+1));
                         bValue=false;
                         break;
                    }
                    if(SSupName.length()<=0 )
                    {
                         JOptionPane.showMessageDialog(null," Invalid Supplier Name in Row"+(i+1));
                         bValue=false;
                         break;
                    }
                    if(common.toInt(SDocId)<=0 )
                    {
                         JOptionPane.showMessageDialog(null," Invalid DocId in Row"+(i+1));
                         bValue=false;
                         break;
                    }
               }
          }
          return bValue;
     }
     private String getQS()
     {
          StringBuffer   sb = new StringBuffer();

           sb.append(" insert into ComparsionEnquiry " );
           sb.append(" (ID,ENQNO,ENQDATE,SUP_CODE,ITEM_CODE,QTY,RATE,DISCPER,CENVATPER, ");
           sb.append(" TAXPER,SURPER,NETRATE, ");
           sb.append("  USERCODE,CREATIONDATE,");
           sb.append("  CSTPER,CSTVAL,DISCVAL,CENVATVAL,TAXVAL, ");
           sb.append("  SURVAL,REMARKS, ");
           sb.append("  TOTALVALUE, ");
           sb.append("  COMPARSIONID,COMPARSIONNO,COMPARSIONDATE, ");
           sb.append("  COMPARISONENTRYUSERCODE, COMPARISONENTRYDATETIME, COMPARISONENTRYIP,Type,DocId,RateChartEntryStatus,RateChartNo) Values ( ");
           sb.append("  ?,?,?,getSupCode(?),?,?,?,?,?,?,?,?,?,to_Char(Sysdate,'YYYYMMDD'),?,?,?,?,?,?,?,?,");
           sb.append("  ComparsionEnquiry_seq.nextval,?, to_Char(Sysdate,'YYYYMMDD'), ");
           sb.append("  ?, to_Char(SysDate, 'DD.MM.YYYY HH24:MI:SS'), ?,?,?,1,RateChartNo_Seq.nextval ) ");


          return sb.toString();
     }
     private String getColor(int iValue)
     {
          String SColor="";

          if(iValue==1)
               SColor="BLUE";
          if(iValue==2)
               SColor="RED";
          if(iValue==3)
               SColor="GREEN";

          return SColor;
     }

     public void setColor()
     {
          TableColumnModel TC      = theTable.getColumnModel();


          for(int i=0;i<theModel.ColumnName.length;i++)
          {


               if(i>=SupName)
               {
                    TC             . getColumn(i).setCellRenderer(new ColoredTableCellRenderer());
               }
          }
     }


     public class ColoredTableCellRenderer extends DefaultTableCellRenderer
     {
          public Component getTableCellRendererComponent(JTable tabrep,Object value, boolean selected, boolean focused, int row, int column)
          {

               // BG Color...

               int iStatus    = common.toInt((String)VOrderStatus.elementAt(row));
               int iEnqStatus    = common.toInt((String)VEnquiryStatus.elementAt(row));

               if(iStatus == 2)
               {
                    setBackground(new Color(255, 128, 128));
               }
               else if(iStatus == 3)
               {
                    setBackground(Color.RED);
               }
               else if(iStatus == 0 && iEnqStatus==0)
               {
                    setBackground(Color.GREEN);
               }
               else if(iStatus == 0 && iEnqStatus==1)
               {
                    setBackground(Color.GRAY);
               }
               else
               {
                    setBackground(Color.WHITE);
               }

               super.getTableCellRendererComponent(tabrep,value,selected,focused,row,column);
               return this;
          }
     }  

}


