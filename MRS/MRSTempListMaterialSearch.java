package MRS;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class MRSTempListMaterialSearch implements ActionListener
{
      JLayeredPane   Layer;
      JTextField     TMatCode;
      JTextField     TMatCatl;
      JTextField     TMatDraw;
      JButton        BMatName;
      Vector         VName,VCode;
      JTextField     TIndicator;          
      JList          BrowList;
      JScrollPane    BrowScroll;
      JPanel         LeftPanel;
      JTextArea      JTA;
      JInternalFrame MaterialFrame;
      JTextField     TRate,TQty;
      JLabel         LValue;

      Vector         VNameCode;
      String SName="",SCode="";

      Connection theConnection=null;

      int iSelectedRow;
      int iFlag = 0;
      String str="";
      String SWhere="";
      ActionEvent ae;
      String SCatl="",SDraw="";
      Common common = new Common();
      JPanel BottomPanel;
      JButton BRefresh;
      int iMillCode=0;
      MRSTempListFrame mrsframe;

      MRSTempListMaterialSearch(JLayeredPane Layer,JTextField TMatCode,JButton BMatName,Vector VCode,Vector VName,Vector VNameCode,JTextField TMatCatl,JTextField TMatDraw,int iMillCode,MRSTempListFrame mrsframe,JTextField TRate,JLabel LValue,JTextField TQty)
      {
          try
          {
               this.Layer       = Layer;
               this.TMatCode    = TMatCode;
               this.BMatName    = BMatName;
               this.VCode       = VCode;
               this.VName       = VName;
               this.VNameCode   = VNameCode;
               this.TMatCatl    = TMatCatl;
               this.TMatDraw    = TMatDraw;
               this.iMillCode   = iMillCode;
               this.TRate       = TRate;
               this.LValue      = LValue;
               this.TQty        = TQty;
               this.mrsframe    = mrsframe;

               BrowList      = new JList(VNameCode);
               BrowList.setFont(new Font("monospaced", Font.PLAIN, 11));
     
               BrowScroll    = new JScrollPane(BrowList);
     
               LeftPanel     = new JPanel(true);
               TIndicator    = new JTextField();
               TIndicator.setEditable(false);
               MaterialFrame     = new JInternalFrame("Materials Selector");
               MaterialFrame.show();
               MaterialFrame.setBounds(80,100,550,350);
               MaterialFrame.setClosable(true);
               MaterialFrame.setResizable(true);
               BrowList.addKeyListener(new KeyList());
     
               BRefresh      = new JButton("Refresh");
               BottomPanel   = new JPanel(true);
               BottomPanel.setLayout(new GridLayout(1,2));
               BottomPanel.add(TIndicator);
               BottomPanel.add(BRefresh);
     
               MaterialFrame.getContentPane().setLayout(new BorderLayout());
               MaterialFrame.getContentPane().add("South",BottomPanel);
               MaterialFrame.getContentPane().add("Center",BrowScroll);
               SWhere = "MRS";      
               iFlag = 0;
               BRefresh.addActionListener(new RefreshList());
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

      }

      public void actionPerformed(ActionEvent ae)
      {
          this.ae = ae;
          removeHelpFrame();
          try
          {
               Layer.add(MaterialFrame);
               MaterialFrame.moveToFront();
               MaterialFrame.setSelected(true);
               MaterialFrame.show();
               BrowList.requestFocus();
               setCursor(BMatName.getText());
               Layer.repaint();
          }
          catch(Exception ex){}
      }
      public class RefreshList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {
                    setDataIntoVector();
//                    mrsframe.setVector1();
                    BrowList.setListData(VNameCode);
          }
      }

      public class KeyList extends KeyAdapter
      {
             public void keyReleased(KeyEvent ke)
             {
                  char lastchar=ke.getKeyChar();
                  lastchar=Character.toUpperCase(lastchar);
                  try
                  {
                     if(ke.getKeyCode()==8)
                     {
                        str=str.substring(0,(str.length()-1));
                        setCursor();
                     }
                     else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                     {
                        str=str+lastchar;
                        setCursor();
                     }
                  }
                  catch(Exception ex){}
             }
             public void keyPressed(KeyEvent ke)
             {
                  if(ke.getKeyCode()==116)    // F5 is pressed
                  {
                         mrsframe.setVector1();
                         BrowList.setListData(VNameCode);
                  }
                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                     int index = BrowList.getSelectedIndex();
                     String SMatName     = (String)VName.elementAt(index);
                     String SMatCode     = (String)VCode.elementAt(index);
                     String SNetRate     = common.getLastNetRateAll(SMatCode,iMillCode);

                     addMatDet(SMatName,SMatCode,SNetRate,index);

//                     addMatDet(SMatName,SMatCode,index);

                     str="";
                     removeHelpFrame();
                  }
             }
         }

         public void setCursor()
         {
            TIndicator.setText(str);            
            int index=0;
            for(index=0;index<VName.size();index++)
            {
                 String str1 = ((String)VName.elementAt(index)).toUpperCase();
                 if(str1.startsWith(str))
                 {
                     BrowList.setSelectedIndex(index);
                     BrowList.ensureIndexIsVisible(index+10);
                     break;
                 }
            }
         }

         public void setCursor(String xtr)
         {
            int index=0;
            for(index=0;index<VName.size();index++)
            {
                 String str1 = ((String)VName.elementAt(index)).toUpperCase();
                 xtr = xtr.toUpperCase();
                 if(str1.startsWith(xtr))
                 {
                     BrowList.setSelectedIndex(index);
                     BrowList.ensureIndexIsVisible(index+10);
                     break;
                 }
            }
         }

         public void removeHelpFrame()
         {
            try
            {
               Layer.remove(MaterialFrame);
               Layer.repaint();
               Layer.updateUI();
               BMatName.requestFocus();
            }
            catch(Exception ex) { }
         }
/*       public boolean addMatDet(String SMatName,String SMatCode,int index)
         {
               if(iFlag==0)
               {
                    TMatCode.setText(SMatCode);
                    BMatName.setText(SMatName);
                    if(SWhere=="MRS")
                    {
                        setCDM(SMatCode);
                        TMatCatl.setText(SCatl);
                        TMatDraw.setText(SDraw);
                    }
                    return true;    
                }
                return true;
         }*/


         public boolean addMatDet(String SMatName,String SMatCode,String SNetRate,int index)
         {
               if(iFlag==0)
               {
                    TMatCode.setText(SMatCode);
                    BMatName.setText(SMatName);

                    if(common.toDouble(SNetRate)>0)
                    TRate.setText(SNetRate);


                    double dQty   = common.toDouble(TQty.getText());
                    double dRate  = common.toDouble(TRate.getText());
                    double dValue = dQty*dRate;
     
                    LValue.setText(common.getRound(dValue,2));

                    if(SWhere=="MRS")
                    {
                        setCDM(SMatCode);
                        TMatCatl.setText(SCatl);
                        TMatDraw.setText(SDraw);
                    }
                    if(common.toDouble(SNetRate)>0)
                    {
                         TRate.setEditable(false);
                    }
                    else
                    {
                         TRate.setEditable(true);
                    }
                    return true;    
                }
                return true;
         }



         public void setCDM(String SMatCode)
         {
                try
                {
                         if(theConnection == null)
                         {
                              ORAConnection jdbc   = ORAConnection.getORAConnection();
                              theConnection        = jdbc.getConnection();
                         }
                         Statement stat   = theConnection.createStatement();
                        ResultSet res1 = stat.executeQuery("Select Catl,Draw From InvItems Where Item_Code='"+SMatCode+"'");
                        while(res1.next())
                        {
                                SCatl = common.parseNull(res1.getString(1));
                                SDraw = common.parseNull(res1.getString(2));
                        }
                }
                catch(Exception ex)
                {
                        System.out.println("@ setCDM");
                        System.out.println(ex);
                }
         }
         public void setDataIntoVector()
         {
               VName.removeAllElements();
               VCode.removeAllElements();
               VNameCode.removeAllElements();


               String QS = "";

               QS =  " Select Item_Name,Item_Code,InvItems.UoMCode,Catl,Draw,UomName,PaperColor,PaperSets,PaperSize,PaperSide,LastSlipNo+1 From InvItems "+
                     " Inner join Uom On Uom.UomCode=InvItems.UomCode "+
                     " Order By Item_Name";

               try
               {
                    ORAConnection  oraConnection =  ORAConnection.getORAConnection();
                    Connection     theConnection =  oraConnection.getConnection();               
                    Statement      stat          =  theConnection.createStatement();
                    ResultSet res = stat.executeQuery(QS);
                    while(res.next())
                    {

                         String str1 = res.getString(1);
                         String str2 = res.getString(2);
                         String str3 = res.getString(3);
                         String str4 = res.getString(4);
                         String str5 = res.getString(5);
                         String str6 = (String)res.getString(6);
     
                         VName.addElement(str1);
                         VCode.addElement(str2);
                         VNameCode.addElement(str1+" - "+str6+" (Code : "+str2+")"+" (Catl : "+str4+")"+" (Draw : "+str5+")");

                    }
                    res.close();
                    stat.close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
         }

}
