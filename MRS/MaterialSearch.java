package MRS;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MaterialSearch implements ActionListener
{
      JLayeredPane   Layer;
      JTextField     TMatCode;
      JButton        BMatName;
      Vector         VName,VCode;
      JTextField     TIndicator;          
      JList          BrowList;
      JScrollPane    BrowScroll;
      JPanel         LeftPanel;
      JInternalFrame MaterialFrame;

      Vector         VNameCode;
      String SName="",SCode="";

      int iSelectedRow;
      int iFlag = 0;
      String str="";
      ActionEvent ae;
      Common common = new Common();
      JPanel BottomPanel;
      JButton BRefresh;
      int iMillCode;
      String SItemTable;

      MaterialSearch(JLayeredPane Layer,JTextField TMatCode,JButton BMatName,Vector VCode,Vector VName,Vector VNameCode,int iMillCode,String SItemTable)
      {
          this.Layer       = Layer;
          this.TMatCode    = TMatCode;
          this.BMatName    = BMatName;
          this.VCode       = VCode;
          this.VName       = VName;
          this.VNameCode   = VNameCode;
          this.iMillCode   = iMillCode;
          this.SItemTable  = SItemTable;

          BrowList      = new JList(VNameCode);
          BrowList.setFont(new Font("monospaced", Font.PLAIN, 11));

          BrowScroll    = new JScrollPane(BrowList);

          LeftPanel     = new JPanel(true);
          TIndicator    = new JTextField();
          TIndicator.setEditable(false);
          MaterialFrame     = new JInternalFrame("Materials Selector");
          MaterialFrame.show();
          MaterialFrame.setBounds(80,100,550,350);
          MaterialFrame.setClosable(true);
          MaterialFrame.setResizable(true);
          BrowList.addKeyListener(new KeyList());

          BRefresh      = new JButton("Refresh");
          BottomPanel   = new JPanel(true);
          BottomPanel.setLayout(new GridLayout(1,2));
          BottomPanel.add(TIndicator);
          BottomPanel.add(BRefresh);

          MaterialFrame.getContentPane().setLayout(new BorderLayout());
          MaterialFrame.getContentPane().add("South",BottomPanel);
          MaterialFrame.getContentPane().add("Center",BrowScroll);

          iFlag = 0;

          BRefresh.addActionListener(new RefreshList());
      }

      public void actionPerformed(ActionEvent ae)
      {
          this.ae = ae;
          removeHelpFrame();
          try
          {
               Layer.add(MaterialFrame);
               MaterialFrame.moveToFront();
               MaterialFrame.setSelected(true);
               MaterialFrame.show();
               BrowList.requestFocus();
               setCursor(BMatName.getText());
               Layer.repaint();
          }
          catch(Exception ex){}
      }
      public class RefreshList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {
                    setDataIntoVector();
                    BrowList.setListData(VName);
          }
      }

      public class KeyList extends KeyAdapter
      {
             public void keyReleased(KeyEvent ke)
             {
                  char lastchar=ke.getKeyChar();
                  lastchar=Character.toUpperCase(lastchar);
                  try
                  {
                     if(ke.getKeyCode()==8)
                     {
                        str=str.substring(0,(str.length()-1));
                        setCursor();
                     }
                     else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                     {
                        str=str+lastchar;
                        setCursor();
                     }
                  }
                  catch(Exception ex){}
             }
             public void keyPressed(KeyEvent ke)
             {
                  if(ke.getKeyCode()==116)    // F5 is pressed
                  {
                         setDataIntoVector();
                         BrowList.setListData(VName);
                  }
                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                     int index = BrowList.getSelectedIndex();
                     String SMatName     = (String)VName.elementAt(index);
                     String SMatCode     = (String)VCode.elementAt(index);
                     addMatDet(SMatName,SMatCode,index);
                     str="";
                     removeHelpFrame();
                  }
             }
         }

         public void setCursor()
         {
            TIndicator.setText(str);            
            int index=0;
            for(index=0;index<VName.size();index++)
            {
                 String str1 = ((String)VName.elementAt(index)).toUpperCase();
                 if(str1.startsWith(str))
                 {
                     BrowList.setSelectedIndex(index);
                     BrowList.ensureIndexIsVisible(index+10);
                     break;
                 }
            }
         }

         public void setCursor(String xtr)
         {
            int index=0;
            for(index=0;index<VName.size();index++)
            {
                 String str1 = ((String)VName.elementAt(index)).toUpperCase();
                 if(str1.startsWith(xtr))
                 {
                     BrowList.setSelectedIndex(index);
                     BrowList.ensureIndexIsVisible(index+10);
                     break;
                 }
            }
         }

         public void removeHelpFrame()
         {
            try
            {
               Layer.remove(MaterialFrame);
               Layer.repaint();
               Layer.updateUI();
               BMatName.requestFocus();
            }
            catch(Exception ex) { }
         }
         public boolean addMatDet(String SMatName,String SMatCode,int index)
         {
               if(iFlag==0)
               {
                    TMatCode.setText(SMatCode);
                    BMatName.setText(SMatName);
                    return true;    
                }
                return true;
         }
         public void setDataIntoVector()
         {
               VName.removeAllElements();
               VCode.removeAllElements();

               String QString = "";
               if(iMillCode==0)
               {
                    QString = "Select Item_Name,Item_Code From InvItems Order By Item_Name";
               }
               else
               {
                    QString = " Select Item_Name,"+SItemTable+".Item_Code From "+SItemTable+""+
                              " Inner Join InvItems on InvItems.Item_Code = "+SItemTable+".Item_Code"+
                              " Order By Item_Name";
               }

               try
               {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
                    ResultSet res = stat.executeQuery(QString);
                    while(res.next())
                    {
                         VName.addElement(res.getString(1));
                         VCode.addElement(res.getString(2));
                    }
                    res.close();
                    stat.close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
         }
}
