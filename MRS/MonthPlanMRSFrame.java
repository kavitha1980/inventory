package MRS;

/*
     Conversion of Regular Planned Materials Into MRS
*/

import util.*;
import guiutil.*;
import jdbc.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;


public class MonthPlanMRSFrame extends JInternalFrame
{
     JPanel         TopPanel;
     JPanel         BottomPanel;
     JPanel         MiddlePanel;
     JButton        BApply,BAuthenticate;
     NextField      TMonth;
     DateField      TDate;
     JTextArea      TA;
     
     Vector         VMRSNo,VMRSDate,VTempGroup,VRefNo,VCatl,VDraw,VDueDate,VCode,VHodCode,VQty,V3Qty,VDeptCode,VGroupCode,VItemName;
     Common         common = new Common();
     
     JLayeredPane   Layer;
     int            iMillCode;
     String         SStDate,SEnDate;
     int            iUserCode;
     String         SItemTable;
     MyComboBox     JCUsers;

     String         SPColour  = "",SPSet     = "",SPSize    = "",SPSide    = "";
     
     int            iMaxMRSNo = 0,iMrsNo;

     Vector VCDeptCode,VCGroupCode,VCUnitCode,VUserCode,VUserName;

     JTable         theTable;
     MonthPlanModel theModel;


     public MonthPlanMRSFrame(JLayeredPane Layer,int iMillCode,int iUserCode,String SItemTable)
     {
          this.Layer          = Layer;
          this.iMillCode      = iMillCode;
          this.SStDate        = SStDate;
          this.SEnDate        = SEnDate;
          this.iUserCode      = iUserCode;
          this.SItemTable     = SItemTable;
          
          setUsers();
          setVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          MiddlePanel    = new JPanel();
          BApply         = new JButton("Start Fetching Planned Data");

          BAuthenticate  = new JButton("Convert to MRS");

          TMonth         = new NextField(10);
          TDate          = new DateField();
          TA             = new JTextArea(10,50);
          TDate          .     setTodayDate();

          JCUsers        = new MyComboBox(VUserName);

          theModel       = new MonthPlanModel();
          theTable       = new JTable(theModel);
     }
     
     private void setLayouts()
     {
          setTitle("Regular Plan To MRS");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,600,600);
          
          TopPanel       . setLayout(new FlowLayout(0,0,0));
          MiddlePanel    . setLayout(new BorderLayout());
          TMonth         . setBorder(new TitledBorder("Plan Month"));
     }
     
     private void addComponents()
     {
          TopPanel            . add(TMonth);
//          TopPanel            . add(TDate);
          TopPanel            . add(JCUsers);
          TopPanel            . add(BApply);
          
//		BottomPanel         . add(BAuthenticate);
		BottomPanel         . add(new JLabel(""));

          
          MiddlePanel         . add("Center", new JScrollPane(theTable));
          
          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("Center",MiddlePanel);
          getContentPane()    . add("South",BottomPanel);

     }

     private void addListeners()
     {
          BApply         . addActionListener(new ApplyList());
          BAuthenticate  . addActionListener(new ConvertList());

     }

     private class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if (common.toInt(TMonth.getText())==0)
                    return;

               if(isValidMonth())
               {
                    BApply.setEnabled(false);

                    theModel.setNumRows(0);

                    fetchData();

                    BApply         . setEnabled(true);
               }
          }
     }
     private class ConvertList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {

               if(isValidMonth())
               {

                    if(JOptionPane.showConfirmDialog(null,"Confirm To Convert this to MRS ?","Information",JOptionPane.YES_NO_OPTION) == 0)
                    {
                         setData();
                    }
               }
          }
     }


     private void fetchData()
     {
          String SPlanDate = "200399";
          
          VMRSNo       = new Vector();
          VMRSDate     = new Vector();
          VTempGroup   = new Vector();
          VRefNo       = new Vector();
          VCatl        = new Vector();
          VDraw        = new Vector();
          VDueDate     = new Vector();
          VCode        = new Vector();
          VItemName    = new Vector();
          VHodCode     = new Vector();
          VQty         = new Vector();
          V3Qty        = new Vector();
          VDeptCode    = new Vector();
          VGroupCode   = new Vector();
          
          String QS = "";


          QS = " Select RegPlanTemp.Item_Code,RegPlanTemp.HodCode, "+
               " RegPlanTemp.Group_Code,RegPlanTemp.ToOrder, "+
               " InvItems.Catl,InvItems.Draw,RegPlanTemp.Plan_Date,Hod.HodName,MatGroup.GroupName,InvItems.Item_Name,RegPlanTemp.To3Order "+
               " From ((RegPlanTemp "+
               " Inner Join InvItems On InvItems.Item_Code = RegPlanTemp.Item_Code) "+
               " INNER JOIN MatGroup ON RegPlanTemp.Group_Code=MatGroup.GroupCode) "+
               " INNER JOIN Hod ON RegPlanTemp.HodCode=Hod.HodCode "+
               " Where substr(RegPlanTemp.Plan_Date,5,2)='"+TMonth.getText()+"' "+
               " And ToOrder+To3Order > 0 "+
               " And RegPlanTemp.MillCode="+iMillCode+
               " and RegPlanTemp.UserCode="+getUserCode((String)JCUsers.getSelectedItem())+
               " ORDER BY MatGroup.GroupName,Hod.HodName,InvItems.Item_Name ";

          try
          {

               ORAConnection  oraConnection =  ORAConnection     . getORAConnection();
               Connection     theConnection =  oraConnection     . getConnection();
                              theConnection .  setAutoCommit(true);
               Statement      stat          =  theConnection     . createStatement();

               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    VMRSNo              . addElement("0");
                    VRefNo              . addElement("99999");
                    VCode               . addElement(result.getString(1));
                    VHodCode            . addElement(result.getString(2));
                    VTempGroup          . addElement(result.getString(3));
                    VQty                . addElement(result.getString(4));
                    VCatl               . addElement(common.parseNull(result.getString(5)));
                    VDraw               . addElement(common.parseNull(result.getString(6)));
                    SPlanDate           = result  . getString(7);
                    String SMrsDate     = SPlanDate;
                    VMRSDate            . addElement(SMrsDate);
                    VDueDate            . addElement(common.getDate(common.parseDate(SMrsDate),7,1));
                    V3Qty               . addElement(result.getString(11));
                    VDeptCode           . addElement("0");
                    VGroupCode          . addElement("0");
                    VItemName           . addElement(result.getString(10));

               }
               result    . close();


               setModel();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void setModel()
     {

          for(int i=0; i<VCode.size(); i++)
          {

               Vector theVect = new Vector();

               theVect.addElement(VCode.elementAt(i));
               theVect.addElement(VItemName.elementAt(i));
               theVect.addElement(VTempGroup.elementAt(i));
               theVect.addElement(VQty.elementAt(i));
               theVect.addElement(common.parseDate((String)VMRSDate.elementAt(i)));    //common.parseDate((String)VPlanDate.elementAt(i))
               theVect.addElement(common.parseDate((String)VMRSDate.elementAt(i)));
               theVect.addElement(VDueDate.elementAt(i));

               theModel.appendRow(theVect);
          }
     }
          
     private void fillBlanks()
     {
          int iPGroup   = -1;
          int iMRSNo    = iMaxMRSNo;

          for(int i=0;i<VTempGroup.size();i++)
          {
               int iGroup = common.toInt((String)VTempGroup.elementAt(i));
               if(iGroup != iPGroup)
               {
                    iPGroup = iGroup;
                    iMRSNo++;
               }
               VMRSNo    . setElementAt(String.valueOf(iMRSNo),i);
          }
     }
          
     private void setData()
     {
          int       iSlNo          = 1;
          String    SCreationDate  = common.getServerDate();

          Connection theConnection = null;

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");

               theConnection.setAutoCommit(false);

               Statement      stat1          =  theConnection     . createStatement();



               stat1.execute("Delete From Mrs Where PlanMonth > 0 And PlanMonth="+TMonth.getText()+"  And MillCode="+iMillCode+" and UserCode="+iUserCode); // And MrsDate >='"+TDate.toNormal()+"'

               String SPMrsNo = "";
               String SMrsNo  = "";

               String SUnitCode="";

               String SQS = " Select MrsNo from MrsNoConfig where UserCode="+iUserCode+" and MillCode="+iMillCode+" for Update of MrsNo nowait";
               

               if(VCUnitCode.size()>0)
               {
                    SUnitCode = (String)VCUnitCode.elementAt(0);
               }
               else
               {
                    SUnitCode = "0";
               }


               for(int i=0;i<VMRSNo.size();i++)
               {
                    String    QS = "";
                    SMrsNo = (String)VMRSNo.elementAt(i);

                    if(!SPMrsNo.equals(SMrsNo))
                    {
                        SPMrsNo    = SMrsNo;
                        iSlNo      = 1;
                    }

                    Statement      stat          =  theConnection.createStatement();
                    ResultSet      theResult     =  stat.executeQuery(SQS);
                    if(theResult.next())
                         iMrsNo = theResult.getInt(1)+1;
                    theResult.close();


                    if(!isStationary((String)VCode.elementAt(i)))
                    {
                         QS = "";
                         QS = " Insert Into Mrs (id,MRSNo,MRSDate,Item_Code,Qty,Catl,Draw,DueDate,HodCode,RefNo,Unit_Code,Dept_Code,Group_Code,BlockCode,PlanMonth,UserCode,CreationDate,Status,flag,SlNo,MillCode,MrsAuthUserCode) Values (";
                         QS = QS+"Mrs_seq.nextval,";
                         QS = QS+iMrsNo+",";
                         QS = QS+"'"+(String)VMRSDate.elementAt(i)+"',";
                         QS = QS+"'"+(String)VCode.elementAt(i)+"',";
                         QS = QS+"0"+common.toDouble((String)theModel.getValueAt(i,3))+",";   //getMaxQty(i)
                         QS = QS+"'"+(String)VCatl.elementAt(i)+"',";
                         QS = QS+"'"+(String)VDraw.elementAt(i)+"',";
                         QS = QS+"'"+common.pureDate((String)VDueDate.elementAt(i))+"',";
                         QS = QS+"0"+(String)VHodCode.elementAt(i)+",";
                         QS = QS+"0"+(String)VRefNo.elementAt(i)+",";
                         QS = QS+"0"+SUnitCode+",";
                         QS = QS+"0"+common.toInt((String)VDeptCode.elementAt(i))+",";
                         QS = QS+"0"+common.toInt((String)VGroupCode.elementAt(i))+",";
                         QS = QS+"0"+",";
                         QS = QS+TMonth.getText()+",";
                         QS = QS+iUserCode+",";
                         QS = QS+"'"+SCreationDate+"',";
                         QS = QS+"0"+",";
                         QS = QS+"0"+",";
                         QS = QS+iSlNo+",";
                         QS = QS+iMillCode+",";
                         QS = QS+iUserCode+")";

                    }
                    else
                    {
                         QS = "";
                         QS = " Insert Into Mrs (id,MRSNo,MRSDate,Item_Code,Qty,Catl,Draw,DueDate,HodCode,RefNo,Unit_Code,Dept_Code,Group_Code,BlockCode,PlanMonth,UserCode,CreationDate,Status,flag,SlNo,MillCode,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE,MrsAuthUserCode) Values (";
                         QS = QS+"Mrs_seq.nextval,";
                         QS = QS+iMrsNo+",";
                         QS = QS+"'"+(String)VMRSDate.elementAt(i)+"',";
                         QS = QS+"'"+(String)VCode.elementAt(i)+"',";
                         QS = QS+"0"+getMaxQty(i)+",";
                         QS = QS+"'"+(String)VCatl.elementAt(i)+"',";
                         QS = QS+"'"+(String)VDraw.elementAt(i)+"',";
                         QS = QS+"'"+common.pureDate((String)VDueDate.elementAt(i))+"',";
                         QS = QS+"0"+(String)VHodCode.elementAt(i)+",";
                         QS = QS+"0"+(String)VRefNo.elementAt(i)+",";
                         QS = QS+"0"+SUnitCode+",";
                         QS = QS+"0"+common.toInt((String)VDeptCode.elementAt(i))+",";
                         QS = QS+"0"+common.toInt((String)VGroupCode.elementAt(i))+",";
                         QS = QS+"0"+",";
                         QS = QS+TMonth.getText()+",";
                         QS = QS+iUserCode+",";
                         QS = QS+"'"+SCreationDate+"',";
                         QS = QS+"0"+",";
                         QS = QS+"0"+",";
                         QS = QS+iSlNo+",";
                         QS = QS+iMillCode+",";
                         QS = QS+"'"+SPColour+"',";
                         QS = QS+"'"+SPSet+"',";
                         QS = QS+"'"+SPSize+"',";
                         QS = QS+"'"+SPSide+"',";
                         QS = QS+iUserCode+")";

                    }
                    stat.execute(QS);

                    PreparedStatement thePrepare = theConnection.prepareStatement(" Update MrsNoConfig set MrsNo="+iMrsNo+" where UserCode="+iUserCode+" and MillCode="+iMillCode);
                    thePrepare.executeUpdate();

                    stat      . execute("Update RegPlanTemp Set Posted=1 Where substr(RegPlanTemp.Plan_Date,5,2)='"+TMonth.getText()+"' And RegPlanTemp.Plan_Date >='"+SStDate+"' And MillCode="+iMillCode+" and UserCode="+iUserCode);
     
                    stat.close();
                    thePrepare.close();

                    iSlNo++;
               }
               stat1.close();
               theConnection.setAutoCommit(true);
               theConnection.close();
               JOptionPane.showMessageDialog(null," Mrs Generated Successfully");


          }catch(Exception ex)
          {
               try
               {
                    JOptionPane.showMessageDialog(null," Problem in Storing Contact Mr.MohanRaj");
                    theConnection.rollback();
               }catch(Exception e){}
               System.out.println(ex);
          }
     }
     
     private String getMaxQty(int i)
     {
          double dQty    = common.toDouble((String)VQty.elementAt(i));
          double d3Qty   = common.toDouble((String)V3Qty.elementAt(i));
          double dMax    = Math.max(dQty,d3Qty);
          return common  . getRound(dMax,2);
     }

     private boolean isValidMonth()
     {
          int iCount = 0;
          try
          {
               ORAConnection  oraConnection =  ORAConnection     . getORAConnection();
               Connection     theConnection =  oraConnection     . getConnection();   
               Statement      stat          =  theConnection     . createStatement();
          
               String QS1 = "";
               
               QS1 = " Select Count(*) From Mrs Where PlanMonth > 0 And PlanMonth="+TMonth.getText()+"  And MillCode="+iMillCode+" and UserCode="+iUserCode; // And MrsDate >='"+TDate.toNormal()+"'

               ResultSet res1 = stat.executeQuery(QS1);
               while(res1.next())
               {
                    iCount = res1.getInt(1);
               }
               res1 . close();
               stat . close();

               System.out.println(" Count--->"+iCount);
               
			/*
				-- Commented on 08.10.2020..
				
				if(iCount>0)
				{
					JOptionPane.showMessageDialog(null,"Mrs Already made for this month","Error",JOptionPane.ERROR_MESSAGE);
					return false;
				}
			*/
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return true;
     }

     private boolean isStationary(String SMatCode)
     {
          boolean   bFlag          = true;
          String    SStkGroupCode  = "";
          String    QS             = "";

          try
          {
               ORAConnection  oraConnection =  ORAConnection     . getORAConnection();
               Connection     theConnection =  oraConnection     . getConnection();
               Statement      stat          =  theConnection     . createStatement();


               if(iMillCode==0)
               {
                    QS =    " select Invitems.stkgroupcode,INVITEMS.PAPERCOLOR,INVITEMS.PAPERSETS,INVITEMS.PAPERSIZE,INVITEMS.PAPERSIDE from invitems"+
                            " where invitems.item_code = '"+SMatCode+"'";
               }
               else
               {
                    QS =    " select "+SItemTable+".stkgroupcode,INVITEMS.PAPERCOLOR,INVITEMS.PAPERSETS,INVITEMS.PAPERSIZE,INVITEMS.PAPERSIDE from "+SItemTable+""+
                            " inner join invitems on invitems.item_code = "+SItemTable+".item_code"+
                            " where "+SItemTable+".item_code = '"+SMatCode+"'";
               }

               ResultSet result = stat.executeQuery(QS);

               while(result    . next())
               {
                    SStkGroupCode  = common.parseNull((String)result.getString(1));
                    SPColour       = common.parseNull((String)result.getString(2));
                    SPSet          = common.parseNull((String)result.getString(3));
                    SPSize         = common.parseNull((String)result.getString(4));
                    SPSide         = common.parseNull((String)result.getString(5));
               }
               result.close();
               stat.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

          if(SStkGroupCode.equals("B01"))
          {
               SPColour       = SPColour. toUpperCase();
               SPSet          = SPSet   . toUpperCase();
               SPSize         = SPSize  . toUpperCase();
               SPSide         = SPSide  . toUpperCase();

               return bFlag;
          }
          return false;
     }

     private void setVector()
     {
          VCDeptCode    = new Vector();
          VCGroupCode   = new Vector();
          VCUnitCode    = new Vector();
          
          String QS1 = "";
          String QS2 = "";
          String QS3 = "";
          
          QS1 = " Select Dept_Code From Dept Where Dept_Code>0 and (MillCode=2 or MillCode="+iMillCode+") Order By Dept_Code";
          QS2 = " Select Group_Code From Cata Where Group_Code>0 and (MillCode=2 or MillCode="+iMillCode+") Order By Group_Code";
          QS3 = " Select Unit_Code From Unit Where Unit_Code>0 and (MillCode=2 or MillCode="+iMillCode+") Order By Unit_Code";


          try
          {
               ORAConnection  oraConnection =  ORAConnection     . getORAConnection();
               Connection     theConnection =  oraConnection     . getConnection();
                              theConnection .  setAutoCommit(true);
               Statement      stat          =  theConnection     . createStatement();

               ResultSet result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VCDeptCode.addElement(result.getString(1));
               }
               result.close();

               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VCGroupCode.addElement(result.getString(1));
               }
               result.close();

               result = stat.executeQuery(QS3);
               while(result.next())
               {
                    VCUnitCode.addElement(result.getString(1));
               }
               result.close();
               stat.close();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setUsers()
     {
          ResultSet result = null;

          VUserCode = new Vector();
          VUserName = new Vector();

          try
          {
               String QS = " select distinct AuthUserCode,RawUser.UserName from RawUser "+
                           " inner join MrsUserAuthentication on MrsUserAuthentication.AuthUserCode=RawUser.UserCode Order by 2 ";

               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
                              result        =  stat.executeQuery(QS);
               
               while (result.next())
               {
                    VUserCode.addElement(result.getString(1));
                    VUserName.addElement(result.getString(2));
               }
               result.close();
          }
          catch(Exception ex)
          {
               System.out.println("set Users: "+ex);
          }
     }
     private int getUserCode(String SUserName)
     {
          int iIndex=-1;

          iIndex = VUserName.indexOf(SUserName);
          if(iIndex!=-1)
               return common.toInt((String)VUserCode.elementAt(iIndex));
          else
               return 0;
     }


}
