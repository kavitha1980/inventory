package MRS;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import guiutil.*;
import util.*;

public class MonthPlanModel extends DefaultTableModel 
{

     String ColumnName[] = {"Code" ,"Name","Group","Qty","PlanDate","MrsDate","DueDate"};
     String ColumnType[] = {"S"    ,"S"   ,"S"    ,"E"  ,"S"       ,"S"      ,"S"};
     String ColumnType1[]= {"S"    ,"S"   ,"S"    ,"S"  ,"S"       ,"S"      ,"S"};


     Common common = new Common();

     int iList=0;

     MonthPlanModel()
     {
          this.iList=iList;
          setDataVector(getRowData(),ColumnName);
     }
     public Class getColumnClass(int col)
     {
          return getValueAt(0,col).getClass();
     }
     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol]=="B" || ColumnType[iCol]=="E")
               return true;
          return false;
     }
     public void setValueAt(Object aValue, int row, int column)
     {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
     }
     public int getRows()
     {
          return super.dataVector.size();
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[0][ColumnName.length];
          return RowData;
     }
     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }
     public String getValueFrom(int iRow,int iCol)
     {
          return (String)getValueAt(iRow,iCol);
     }
     public void deleteRow(int index)
     {
         if(getRows()==1)
         return;
         if(index>=getRows())
         return;
         if(index==-1)
         return;
         removeRow(index);
     }

     public void appendEmptyRow(int iRow)
     {
          Vector temp = new Vector();

          insertRow(getRows(),temp);

     } 

}

