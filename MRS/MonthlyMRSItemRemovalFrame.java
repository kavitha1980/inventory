
package MRS;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;
import jdbc.*;
import java.util.*;
import java.sql.*;
import util.*;
public class MonthlyMRSItemRemovalFrame extends JInternalFrame 
{
    JPanel               pnlTop,pnlMiddle,pnlBottom,pnlNotes,pnlControls;
    JTable               theItemTable;
    JLayeredPane         Layer;
    RemovalItemListModel theItemTableModel;
    JButton              btnRemove,btnUpdate,btnExit;
    int                  iMillCode,iUserCode;
    ArrayList            theItemList,theOrderConversionList;
    Vector               VTypeCode,VTypeName,VUserCode,VUserName;
    JTextField           JTName;
    String               findstr ="";
    JComboBox         JCUserType,JCConversionType,JCOrderConversion;
    //setModelIndex
    int ItemCode =1 ,ItemName=2,MatGroup=3,OrderConversion=4,Select=5;
    //int iUpdateGroup=0;
    int iUpdateGroup[];
    String SMaterialGroupName;
    Common common = new Common();
    public MonthlyMRSItemRemovalFrame(JLayeredPane Layer,int iMillCode)
    {
        this . Layer     = Layer;
        this . iMillCode = iMillCode;

		setOrderConversion();
        SetUserName();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
        setTableData();
		
        iUpdateGroup=new int[theItemTableModel.getRowCount()];
		
        for(int i=0;i<theItemTableModel.getRowCount();i++)
        {
            iUpdateGroup[i] = 1;
		
        }
		
    }
    private void createComponents()
    {
          pnlTop      = new JPanel();
          pnlMiddle   = new JPanel();
          pnlBottom   = new JPanel();
          pnlNotes    = new JPanel();
          pnlControls = new JPanel(); 
                    
          btnRemove = new JButton("  REMOVE ITEM  ");
          btnUpdate = new JButton("  UPDATE ORDER CONVERSION ");
          btnExit   = new JButton(" EXIT ");
          JTName    = new JTextField(20);
		  
		  JCUserType          = new JComboBox(VUserName);
          JCConversionType    = new JComboBox();

        
          JCConversionType      . addItem("Direct");
          JCConversionType      . addItem("Manual");
          JCConversionType      . addItem("All ");
		
		
		  JCOrderConversion     = new JComboBox();
		 
		  
          theItemTableModel = new RemovalItemListModel();
          theItemTable      = new JTable(theItemTableModel);
		  
		  
         
 
    }
    private void setLayouts()
    {
        pnlTop     . setLayout(new GridLayout(1,8,3,3));
        pnlMiddle  . setLayout(new BorderLayout())  ;
        pnlBottom  . setLayout(new GridLayout(2,1,3,3));
        pnlNotes   . setLayout(new FlowLayout(FlowLayout.LEFT));
        pnlControls. setLayout(new FlowLayout(FlowLayout.CENTER));
        this . setTitle("Monthly Planning Item List Frame");
        this . setSize(1000,500);
        this . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this . setMaximizable(true);
        this . setClosable(true);
    } 
    private void addComponents()
    {
      
		pnlTop       . add(new JLabel("  User Name"));
        pnlTop       . add(JCUserType);
		pnlTop       . add(new JLabel("Order Conversion Type"));
		pnlTop       . add(JCConversionType);
		pnlTop       . add(new JLabel(""));
        pnlMiddle .  add(new JScrollPane(theItemTable),BorderLayout.CENTER);
        pnlMiddle .  add(JTName,BorderLayout.SOUTH);
   
        pnlControls .  add(btnUpdate);
        pnlControls .  add(btnExit);
       
        pnlBottom  . add(pnlControls);
   
        
		JCOrderConversion     . addItem("DIRECT");
        JCOrderConversion     . addItem("MANUAL");
		JCOrderConversion     . setEditable(true);
		  
		DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

        		 
		TableColumn ConversionColumn  = theItemTable.getColumn("ORDER CONVERSION");
		ConversionColumn.setCellEditor(new DefaultCellEditor(JCOrderConversion));
            
        this . add("North",pnlTop);
        this . add("Center",pnlMiddle);
        this . add("South",pnlBottom);
    }
    private void addListeners()
    {
        
        btnUpdate . addActionListener(new ActList());
		JCUserType.addItemListener(new ItemList());
		JCConversionType.addItemListener(new ItemList());
        btnExit   . addActionListener(new ActList());
        theItemTable . addKeyListener(new TableKeyList());
      
    }
	private class ItemList implements ItemListener
    {
        public void itemStateChanged(ItemEvent ie)
        {
            if(ie.getSource()==JCUserType || ie.getSource()==JCConversionType)
            {  
	            setTableData();   
		    iUpdateGroup=new int[theItemTableModel.getRowCount()];
		
		    for(int i=0;i<theItemTableModel.getRowCount();i++)
		    {
		            iUpdateGroup[i] = 1;
		
		    }

            }
        } 
    }
    private void SetUserName()
    {
        VUserName   = new Vector();
        VUserCode    = new Vector();

        String QS1 = "Select UserName,UserCode  from RawUser where MRSAuthStatus=1";

        try
        {
            ORAConnection   oraConnection =  ORAConnection.getORAConnection();
            Connection      theConnection =  oraConnection.getConnection();
            Statement       stat =  theConnection.createStatement();

            ResultSet result1 = stat.executeQuery(QS1);
            while(result1.next())
            {
                VUserName.addElement(result1.getString(1));
                VUserCode.addElement(result1.getString(2));
            }
            result1.close();
            stat.close();
        }
        catch(Exception ex)
        {
			System.out.println(ex);
        }
    }

    private void setTableData()
    {
        
        theItemTable.getColumn("SELECT").setMaxWidth(55);
        theItemTable.getColumn("SNO").setMaxWidth(35);
        theItemTable.getColumn("ITEMCODE").setMaxWidth(110);
        theItemTableModel.setNumRows(0);
        setItemList();
        
        for(int i=0;i<theItemList.size();i++)
        {
            HashMap theMap = (HashMap)theItemList.get(i);
            Vector theItemVect = new Vector();
            theItemVect . add(String.valueOf(i+1));
            theItemVect . add(theMap.get("ItemCode"));
            theItemVect . add(theMap.get("ItemName"));
            theItemVect . add(theMap.get("MatGroupName"));
            theItemVect . add(theMap.get("ConversionName"));
            theItemVect . add(new Boolean(false));
            theItemTableModel.appendRow(theItemVect);
        }
    }
    
    private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
                     
			if(ae.getSource()==btnUpdate)
            {
				int iCount=0;
           
				if(JOptionPane.showConfirmDialog(null,"Do you want to update the Order Conversion?","Confirmation",JOptionPane.YES_NO_OPTION)==0)
				{ 
					for(int i=0;i<theItemTableModel.getRowCount();i++)   
					{

						if(((Boolean)theItemTableModel.getValueAt(i, Select)).booleanValue())  
						{
                     
					 
								int iRow = theItemTable.getSelectedRow();
								int iCol = theItemTable.getSelectedColumn();
								String SItemCode = (String)theItemTable.getValueAt(i, ItemCode);
								String SItemName = (String)theItemTable.getValueAt(i, ItemName);
								int iOrderConversionName    = VTypeName.indexOf(theItemTable.getValueAt(i, OrderConversion));
								String SOrderConversionCode = (String)VTypeCode.get(iOrderConversionName);
					  					   
								int iUpdateStatus = updateMaterialGroupCode(SOrderConversionCode,SItemCode,SItemName);
								iCount = iCount+iUpdateStatus;
					  
						}
					}
				}
				if(iCount>0)
				{
					JOptionPane.showMessageDialog(null,"Order Conversion Code Updated Successfully","Info",JOptionPane.INFORMATION_MESSAGE);
				}
				setTableData();
			}
			if(ae.getSource()==btnExit)
			{
				dispose();
			}
		}
    }
    private void setMaterialGroup(String SGroupName)
    {
        this.SMaterialGroupName= SGroupName;
    }
	private int getUserCode(String SName)
    {
		int iIndex     =  VUserName.indexOf(SName);

        if(iIndex==-1)
            return -1;

        return common.toInt((String)VUserCode.elementAt(iIndex));
    }
    private void setItemList()
    {
		int iUserCode = getUserCode((String)JCUserType.getSelectedItem());

        theItemList = new ArrayList();
        StringBuffer sb = new StringBuffer();
        sb.append(" select Item_Code,Item_Name,MatGroupCode,MatGroup.GroupName,OrderConversionType.TypeName from MonthlyPlanningItem");
        sb.append(" Inner join MatGroup on MatGroup.GroupCode = MonthlyPlanningItem.MatGroupCode");
		sb.append(" Inner join OrderConversionType on OrderConversionType.TypeCode = MonthlyPlanningItem.OrderConversionTypeCode");
        sb.append(" where MonthlyPlanningItem.UserCode="+iUserCode+" and  MonthlyPlanningItem.MillCode =?");
        
		if(JCConversionType.getSelectedIndex()==0)
            sb.append("  and MonthlyPlanningItem.OrderConversionTypeCode=0");
		if(JCConversionType.getSelectedIndex()==1)
            sb.append("  and MonthlyPlanningItem.OrderConversionTypeCode=1");
             
        sb.append(" order by  Item_Name");
		
		//System.out.println("QS:"+sb.toString());

        try
        {
            ORAConnection connect          = ORAConnection.getORAConnection();
            Connection    theConnection    = connect.getConnection();
            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());

            
            theStatement.setInt(1,iUserCode);
            theStatement.setInt(1,iMillCode);
            ResultSet theResult = theStatement.executeQuery();
            while(theResult.next())
            {
                HashMap theMap = new HashMap();
                theMap . put("ItemCode",theResult.getString(1));
                theMap . put("ItemName",theResult.getString(2));
                theMap . put("MatGroupCode",theResult.getString(3));
                theMap . put("MatGroupName",theResult.getString(4));
                theMap . put("ConversionName",theResult.getString(5));
                theItemList . add(theMap);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    private void setOrderConversion()
    {
        VTypeCode = new Vector();
        VTypeName = new Vector();
		
        theOrderConversionList = new ArrayList();
		
        String QS = " select TypeCode,TypeName from OrderConversionType order by 1";
        try
        {
            ORAConnection connect          = ORAConnection.getORAConnection();
            Connection    theConnection    = connect.getConnection();
            PreparedStatement theStatement = theConnection.prepareStatement(QS);
            ResultSet         theResult    = theStatement.executeQuery();
            while(theResult.next())
            {
                 VTypeCode . addElement(theResult.getString(1));
                 VTypeName . addElement(theResult.getString(2));
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    private int getRemove()
    {
        int iRemove = -1;
        for(int i=0;i<theItemTableModel.getRowCount();i++)
        {
           if(((Boolean)theItemTableModel.getValueAt(i, Select)).booleanValue())
           {
             String SItemCode = (String)theItemTableModel.getValueAt(i,ItemCode);
             String SItemName = (String)theItemTableModel.getValueAt(i,ItemName);
             String SMatGroup = (String)theItemTableModel.getValueAt(i,MatGroup);
             iRemove = iRemove+removeMonthlyPlanningItem(SItemCode,SItemName);
             
           }
        }
        return iRemove;
    }
    private int removeMonthlyPlanningItem(String SItemCode,String SItemName)
    {

		int iUserCode = getUserCode((String)JCUserType.getSelectedItem());

     // System.out.println("ItemCode "+SItemCode+" and "+SItemName+" UserCode "+iUserCode+" MillCode "+iMillCode);
        int iRemove=-1;
        String QS = " Delete MonthlyPlanningItem where Item_Code= ? and Item_Name=?"+
                    " and UserCode = ? and MillCode = ?";
  
        ORAConnection connect          = ORAConnection.getORAConnection();
        Connection    theConnection    = connect.getConnection();
            
        try
        {
            theConnection.setAutoCommit(false);
            PreparedStatement theStatement = theConnection.prepareStatement(QS);
            theStatement.setString(1,SItemCode);
            theStatement.setString(2,SItemName);
            theStatement.setInt(3,iUserCode);
            theStatement.setInt(4,iMillCode);
            theStatement.executeQuery();
            iRemove = 1;
            theConnection . commit();
            theStatement  . close();
            theStatement  = null;
            theConnection = null;
        }
        catch(Exception ex)
        {
			iRemove=0; 
			try
			{
				theConnection .rollback(); 
				System.out.println(" remove Item "+ex);
			}
			catch(Exception e)
			{
				System.out.println(" rollback "+ex);
			}
        }
        return iRemove;
    }
    private int updateMaterialGroupCode(String SOrderConversionCode,String SItemCode,String SItemName)
    {
	
        int iUpdate=-1;
        int iUserCode = getUserCode((String)JCUserType.getSelectedItem());

        String QS = " Update MonthlyPlanningItem set OrderConversionTypeCode =?"+
                    " where Item_Code=? and Item_Name=? and UserCode=? and MillCode=?";
        ORAConnection connect          = ORAConnection.getORAConnection();
        Connection    theConnection    = connect.getConnection();
            
        try
        {
            theConnection.setAutoCommit(false);
            PreparedStatement theStatement = theConnection.prepareStatement(QS);
            theStatement.setString(1,SOrderConversionCode);
            theStatement.setString(2,SItemCode);
            theStatement.setString(3,SItemName);
            theStatement.setInt(4,iUserCode);
            theStatement.setInt(5,iMillCode);
            theStatement.executeQuery();
            iUpdate = 1;
            theConnection.commit();
            theStatement  . close();
            theStatement  = null;
            theConnection = null;
        }
        catch(Exception ex)
        {
           iUpdate=0; 
           try
           {
				theConnection .rollback(); 
				System.out.println(" remove Item "+ex);
           }
           catch(Exception e)
           {
               System.out.println(" rollback "+ex);
           }
        }
        return iUpdate;
        
    }
	private boolean checkItemSelected()
	{
		for(int i=0;i<theItemTableModel.getRows();i++)
		{
			boolean bSelect = (Boolean)theItemTableModel.getValueAt(i,Select);
			if(bSelect)
				return true;
		}
		return false;
	}
/*private class MaterialGroup extends JFrame
	{
		JList LstMaterialGroup;
		String SMaterialGroup;
		int    iRow,iCol;
		public MaterialGroup(int iRow,int iCol)
		{
			this . iRow = iRow;
			this . iCol = iCol;
          //LstMaterialGroup = new JList(VGroupName);
          //this . add(new JScrollPane(LstMaterialGroup));
          LstMaterialGroup.setVisibleRowCount(2);
          
          this . setSize(400,200);
          this . setVisible(true);
          this . setLocation(750,300);
          this . setTitle("Material Group");
          LstMaterialGroup . addKeyListener(new GroupKeyList());
      }
     
    private class GroupKeyList extends KeyAdapter
    {
        public void keyReleased(KeyEvent ke)
        {
            if(ke.getKeyCode()==KeyEvent.VK_ENTER)
            {
                SMaterialGroup = (String)LstMaterialGroup.getSelectedValue();
                setMaterialGroup(SMaterialGroup);
                theItemTable      . editCellAt(iRow, iCol);
                theItemTable      . setValueAt(SMaterialGroup,iRow,iCol);
                dispose();
            }
        }
    }
      
  } */
  
	private class TableKeyList extends KeyAdapter
	{
		public void keyReleased(KeyEvent ke)
		{
			JTable theTable     = (JTable)ke.getSource();
			int iCol            = theTable.getSelectedColumn();

            char lastchar       = ke.getKeyChar();
            lastchar            = Character.toUpperCase(lastchar);

            try
            {
                if(ke.getKeyCode() == 8)
                {
                    findstr   = findstr.substring(0,(findstr.length()-1));
                    JTName    . setText(findstr);
                    setCursor(theTable,iCol);
                }

                else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar>='0' && lastchar <= '9'))
                {
                    findstr   = findstr+lastchar;
                    JTName    . setText(findstr);
                    setCursor(theTable,iCol);
                }
            }
            catch(Exception ex){}
        }

		public void keyPressed(KeyEvent ke)
		{
			if (ke.getKeyCode()==KeyEvent.VK_ESCAPE)
			{
				findstr        = "";
				JTName.setText("");
			}
		}
	}


	private void setCursor(JTable theTable,int iCol)
	{
		int index           = -1;

		for(int i=0;i<theItemTableModel.getRows();i++)
		{
			String str     = (String)theItemTableModel.getValueAt(i,iCol);
			if(str.startsWith(findstr))
			{
				index     = i;
				break;
			}
		}
		if(index == -1)
		return;

        theTable       . setRowSelectionInterval(index,index);
        Rectangle rect = theTable.getCellRect(index,0,true);
        theTable       . scrollRectToVisible(rect);
	}

}

