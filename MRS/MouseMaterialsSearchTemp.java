
package MRS;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MouseMaterialsSearchTemp  extends MouseAdapter 
{
      JLayeredPane       Layer;
      Vector             VName,VCode;
      Vector             VNameCode;
      String             SName,SCode;
      MRSTempModel           MiddlePanel;

      JList          BrowList,SelectedList;
      JScrollPane    BrowScroll,SelectedScroll;
      JTextField     TIndicator;
      JButton        BOk;
      JPanel         LeftPanel,RightPanel;
      JInternalFrame MaterialFrame;
      JPanel         MFMPanel,MFBPanel;
      Vector VSelectedName,VSelectedCode,VSelectedCata,VSelectedDraw,VCata,VDraw;
      Vector VSelectedNameCode;
      String SFrameTitle,SWhere;
      String str="";
      int iMFSig=0;
      MouseEvent ae;
      Common common = new Common();
      int iMillCode;
      MRSTempFrame mrsframe;

      public MouseMaterialsSearchTemp(JLayeredPane Layer,Vector VCode,Vector VName,Vector VNameCode,MRSTempModel MiddlePanel,int iMillCode,Vector VCata,Vector VDraw,MRSTempFrame mrsframe)
      {
          this.Layer         = Layer;
          this.VName         = VName;
          this.VNameCode     = VNameCode;
          this.VCode         = VCode;
          this.VCata         = VCata;
          this.VDraw         = VDraw;
          this.MiddlePanel   = MiddlePanel;
          this.iMillCode     = iMillCode;
          this.mrsframe      = mrsframe;
          createComponents();
      }

      public void createComponents()
      {
          BrowList      = new JList(VNameCode);
          BrowList.setFont(new Font("monospaced", Font.PLAIN, 11));
          SelectedList  = new JList();
          BrowScroll    = new JScrollPane(BrowList);
          SelectedScroll= new JScrollPane(SelectedList);
          LeftPanel     = new JPanel(true);
          RightPanel    = new JPanel(true);
          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator.setEditable(false);
          MFMPanel      = new JPanel(true);
          MFBPanel      = new JPanel(true);
          MaterialFrame = new JInternalFrame("Materials Selector");
          MaterialFrame.show();
          MaterialFrame.setBounds(50,50,650,350);
          MaterialFrame.setClosable(true);
          MaterialFrame.setResizable(true);
          BrowList.addKeyListener(new KeyList());
          BrowList.requestFocus();
      }
      public class ActList implements ActionListener
      {
          public void actionPerformed(ActionEvent e)
          {
               if(VSelectedCode.size()==0)
               {
                    JOptionPane.showMessageDialog(null,"No Material is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                    BrowList.requestFocus();
                    return;
               }
               BOk.setEnabled(false);
               setMiddlePanel();
               removeHelpFrame();
               str="";
          }
      }
      public void mouseClicked(MouseEvent ae)
      {
          this.ae = ae;
          VSelectedName     = new Vector();
          VSelectedCode     = new Vector();
          VSelectedNameCode = new Vector();
          VSelectedCata     = new Vector();
          VSelectedDraw     = new Vector();
          TIndicator.setText(str);
          BOk.setEnabled(true);
          if(iMFSig==0)
          {
               MFMPanel.setLayout(new GridLayout(1,2));
               MFBPanel.setLayout(new GridLayout(1,2));
               MFMPanel.add(BrowScroll);
               MFMPanel.add(SelectedScroll);
               MFBPanel.add(TIndicator);
               MFBPanel.add(BOk);
               BOk.addActionListener(new ActList());
               MaterialFrame.getContentPane().add("Center",MFMPanel);
               MaterialFrame.getContentPane().add("South",MFBPanel);
               iMFSig=1;
          }
          removeHelpFrame();
          try
          {
               Layer.add(MaterialFrame);
               MaterialFrame.moveToFront();
               MaterialFrame.setSelected(true);
               MaterialFrame.show();
               BrowList.requestFocus();
               Layer.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
      }  
      public class KeyList extends KeyAdapter
      {
             public void keyReleased(KeyEvent ke)
             {
                  char lastchar=ke.getKeyChar();
                  lastchar=Character.toUpperCase(lastchar);
                  try
                  {
                     if(ke.getKeyCode()==8)
                     {
                        str=str.substring(0,(str.length()-1));
                        setCursor();
                     }
                     else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                     {
                        str=str+lastchar;
                        setCursor();
                     }
                  }
                  catch(Exception ex)
                  {
                         Toolkit.getDefaultToolkit().beep();
                  }
             }
             public void keyPressed(KeyEvent ke)
             {
                  if(ke.getKeyCode()==116)    // F5 is pressed
                  {
                         mrsframe.setVector1();
                         BrowList.setListData(VNameCode);
                  }

                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                     int index = BrowList.getSelectedIndex();
                     if(index == -1)
                        return;

                     String SMatNameCode = (String)VNameCode.elementAt(index);
                     String SMatName     = (String)VName.elementAt(index);
                     String SMatCode     = (String)VCode.elementAt(index);

                     String SCata        = (String)VCata.elementAt(index);
                     String SDraw        = (String)VDraw.elementAt(index);

                     addMatDet(SMatName,SMatCode,SMatNameCode,SCata,SDraw);
                     str="";
                     TIndicator.setText(str);
                  }
                  if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
                  {
                     try
                     {
                     setMiddlePanel();
                     removeHelpFrame();
                     str="";
                     mrsframe.setSelected(true);
                     mrsframe.theTable.requestFocus();
                     }
                     catch(Exception ex)
                     {
                         System.out.println(ex);
                     }
                  }

             }
         }

         public void setCursor()
         {
            int index=0;
            TIndicator.setText(str);
            for(index=0;index<VNameCode.size();index++)
            {
                 String str1 = ((String)VNameCode.elementAt(index)).toUpperCase();
                 if(str1.startsWith(str))
                 {
                      BrowList.setSelectedIndex(index);
                      BrowList.ensureIndexIsVisible(index+10);
                      break;
                 }
            }
            if(index >= VNameCode.size())
               Toolkit.getDefaultToolkit().beep();
         }

         public void removeHelpFrame()
         {
            try
            {
               Layer.remove(MaterialFrame);
               Layer.repaint();
               Layer.updateUI();
            }
            catch(Exception ex) { }
         }

         public boolean addMatDet(String SMatName,String SMatCode,String SMatNameCode,String SCata,String SDraw)
         {
               int iIndex=VSelectedCode.indexOf(SMatCode);
               if (iIndex==-1)
               {
                    VSelectedName.addElement(SMatName);
                    VSelectedCode.addElement(SMatCode);
                    VSelectedNameCode.addElement(SMatNameCode);
                    VSelectedCata.addElement(SCata);
                    VSelectedDraw.addElement(SDraw);
               }
               else
               {
                    VSelectedName.removeElementAt(iIndex);
                    VSelectedCode.removeElementAt(iIndex);
                    VSelectedCata.removeElementAt(iIndex);
                    VSelectedDraw.removeElementAt(iIndex);
                    VSelectedNameCode.removeElementAt(iIndex);
               }
               SelectedList.setListData(VSelectedNameCode);
               return true;
         }
         public void setMiddlePanel()
         {

           MiddlePanel.setNumRows(0);

           for(int i=0;i<VSelectedCode.size();i++)
           {
                Vector VTheVect = new Vector();

                VTheVect.addElement(String.valueOf(i+1));
                VTheVect.addElement(((String)VSelectedCode.elementAt(i)));
                VTheVect.addElement(((String)VSelectedName.elementAt(i)));
                VTheVect.addElement("");
                VTheVect.addElement("");
                VTheVect.addElement("");
                VTheVect.addElement("");
                VTheVect.addElement("");
                VTheVect.addElement("");
                VTheVect.addElement("");
                VTheVect.addElement("");
                VTheVect.addElement((common.parseNull((String)VSelectedCata.elementAt(i))));
                VTheVect.addElement((common.parseNull((String)VSelectedDraw.elementAt(i))));
                VTheVect.addElement("");
                VTheVect.addElement("");

                MiddlePanel.appendRow(VTheVect);

           }
         }


}
