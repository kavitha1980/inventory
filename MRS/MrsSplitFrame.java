package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;
import java.net.*;

public class MrsSplitFrame extends JInternalFrame
{
     JPanel    TopPanel;
	JPanel	Top1Panel,Top2Panel;
     JPanel    BottomPanel;
     JPanel    MiddlePanel;

     JTextField TMrsNo;
	NextField  TSplitNo;
     MyComboBox JCCode;
     JButton    BApply1,BApply2,BApply3,BOk,BExit;

     MyLabel    LItemName,LMrsQty,LOrderQty,LPendQty;

     Vector VItemCode,VItemName,VMrsDate,VMrsSlNo,VCodeSlNo,VMrsQty,VOrderNo,VNetRate;

     TabReport      tabreport;

     Object         RowData[][];
     String         ColumnData[] = {"Sl.No.","Split Qty"};
     String         ColumnType[] = {"N"     ,"E"        };
     
     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;
     int iUserCode,iMillCode,iAuthCode;
     String SSupTable,SItemTable;

     String SStDate = "20121001";

     Common common = new Common();
     
     Connection theConnect = null;

     boolean bComflag = true;
     
     public MrsSplitFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode,String SSupTable,String SItemTable)
     {
          try
          {
               this.DeskTop    = DeskTop;
               this.VCode      = VCode;
               this.VName      = VName;
               this.SPanel     = SPanel;
               this.iUserCode  = iUserCode;
               this.iMillCode  = iMillCode;
               this.iAuthCode  = iAuthCode;
               this.SSupTable  = SSupTable;
               this.SItemTable = SItemTable;
               
               getDBConnection();
               createComponents();
               setLayouts();
               addComponents();
               addListeners();
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }

     }

     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnect     =    oraConnection.getConnection();
     }

     public void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               Top1Panel      = new JPanel();
               Top2Panel      = new JPanel();
               BottomPanel    = new JPanel();
               MiddlePanel    = new JPanel();
     
               TMrsNo         = new JTextField();
			TSplitNo       = new NextField();
               JCCode         = new MyComboBox();
               BOk            = new JButton("Update");
               BExit          = new JButton("Exit");
               BApply1        = new JButton("Apply");
               BApply2        = new JButton("Apply");
               BApply3        = new JButton("Apply");

               LItemName      = new MyLabel("");
               LMrsQty        = new MyLabel("");
               LOrderQty      = new MyLabel("");
               LPendQty       = new MyLabel("");

               JCCode.setEnabled(false);
               BApply2.setEnabled(false);
               BOk.setEnabled(false);
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void setLayouts()
     {
          try
          {
               TopPanel       .setLayout(new BorderLayout());
               Top1Panel      .setLayout(new GridLayout(1,5,10,10));
               Top2Panel      .setLayout(new GridLayout(3,4,20,20));

               Top1Panel.setBorder(new TitledBorder(""));
               Top2Panel.setBorder(new TitledBorder(""));
               BottomPanel.setBorder(new TitledBorder(""));

               setTitle("Mrs Split Frame");
               setClosable(true);
               setMaximizable(true);
               setIconifiable(true);
               setResizable(true);
               setBounds(0,0,700,500);
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void addComponents()
     {
          try
          {
               Top1Panel.add(new JLabel("Mrs No"));
               Top1Panel.add(TMrsNo);
               Top1Panel.add(BApply1);
               Top1Panel.add(JCCode);
               Top1Panel.add(BApply2);

               Top2Panel.add(new JLabel("ItemName"));
               Top2Panel.add(LItemName);
               Top2Panel.add(new JLabel("MrsQty"));
               Top2Panel.add(LMrsQty);
               Top2Panel.add(new JLabel("Prev Order Qty"));
               Top2Panel.add(LOrderQty);
               Top2Panel.add(new JLabel("Balance Mrs Qty"));
               Top2Panel.add(LPendQty);
               Top2Panel.add(new JLabel("Noof Split"));
               Top2Panel.add(TSplitNo);
               Top2Panel.add(BApply3);
               Top2Panel.add(new JLabel(""));

               TopPanel.add(Top1Panel,"North");

               BottomPanel.add(BOk);
               BottomPanel.add(BExit);
     
               getContentPane().add(TopPanel,BorderLayout.NORTH);
               getContentPane().add(BottomPanel,BorderLayout.SOUTH);
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void addListeners()
     {
          try
          {
               BApply1.addActionListener(new ApplyList());
               BApply2.addActionListener(new ApplyList());
               BApply3.addActionListener(new ApplyList());
               BExit.addActionListener(new ApplyList());
               BOk.addActionListener(new ApplyList());
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply1)
               {
                    setItemCode();
               }

               if(ae.getSource()==BApply2)
               {
                    setMrsData();
               }

               if(ae.getSource()==BApply3)
               {
                    setRowData();
               }

               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
               if(ae.getSource()==BOk)
               {
                    if(validSplit())
                    {
                         String SMrsNo     = TMrsNo.getText();
                         String SItemCode  = (String)VItemCode.elementAt(JCCode.getSelectedIndex());
                         String SMrsSlNo   = (String)VMrsSlNo.elementAt(JCCode.getSelectedIndex());
                         String SOrderNo   = (String)VOrderNo.elementAt(JCCode.getSelectedIndex());
                         String SNetRate   = (String)VNetRate.elementAt(JCCode.getSelectedIndex());
					int iMaxSlNo	   = getMaxSlNo(SMrsNo);

			          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
			          {
			               double dQty = common.toDouble((String)tabreport.ReportTable.getValueAt(i,1));

						if(i==0)
						{
							if(common.toDouble(SOrderNo)>0)
							{
								updateMrs(SMrsNo,SItemCode,SMrsSlNo,common.toDouble(LOrderQty.getText()),SNetRate);
								iMaxSlNo++;
								insertMrs(SMrsNo,SItemCode,SMrsSlNo,dQty,iMaxSlNo,SNetRate);
							}
							else
							{
								updateMrs(SMrsNo,SItemCode,SMrsSlNo,dQty,SNetRate);
							}
						}
						else
						{
							iMaxSlNo++;
							insertMrs(SMrsNo,SItemCode,SMrsSlNo,dQty,iMaxSlNo,SNetRate);
						}
					}

                         try
                         {
                              if(bComflag)
                              {
                                   theConnect     . commit();
                                   JOptionPane.showMessageDialog(null,"Mrs Splitted Successfully ","Information",JOptionPane.INFORMATION_MESSAGE);
                                   System         . out.println("Commit");
                                   theConnect     . setAutoCommit(true);
                                   removeHelpFrame();
                              }
                              else
                              {
                                   theConnect     . rollback();
                                   JOptionPane.showMessageDialog(null,"Problem in Splitting. Contact EDP ","Error",JOptionPane.ERROR_MESSAGE);
                                   System         . out.println("RollBack");
                                   theConnect     . setAutoCommit(true);
                              }
                         }catch(Exception ex)
                         {
                              ex.printStackTrace();
                         }
                    }
               }
          }
     }

     public void setItemCode()
     {
          try
          {
               JCCode.removeAllItems();

			VItemCode		= new Vector();
			VItemName		= new Vector();
			VMrsDate		= new Vector();
			VMrsSlNo		= new Vector();
			VCodeSlNo		= new Vector();
			VMrsQty		= new Vector();
			VOrderNo		= new Vector();
			VNetRate		= new Vector();

               Statement      stat          =  theConnect.createStatement();

               String SMrsNo = TMrsNo.getText();

               String QString =    " Select Mrs.Item_Code,InvItems.Item_Name,Mrs.MrsDate,Mrs.SlNo,Mrs.Qty,Mrs.OrderNo,Mrs.NetRate "+
							" From Mrs Inner Join InvItems on Mrs.Item_Code=InvItems.Item_Code "+
							" and Mrs.MrsNo="+SMrsNo+" and Mrs.MillCode="+iMillCode+
							" Order by Mrs.Item_Code,Mrs.SlNo ";

               ResultSet res  = stat.executeQuery(QString);

               while (res.next())
               {
                    String SCode      = res.getString(1);
                    String SSlNo      = res.getString(4);
                    String SCodeSlNo  = SCode+"~"+SSlNo;

                    VItemCode      .addElement(SCode);
                    VItemName      .addElement(res.getString(2));
                    VMrsDate       .addElement(res.getString(3));
                    VMrsSlNo       .addElement(SSlNo);
                    VCodeSlNo      .addElement(SCodeSlNo);
                    VMrsQty        .addElement(res.getString(5));
                    VOrderNo       .addElement(res.getString(6));
                    VNetRate       .addElement(res.getString(7));
               }
               res.close();
               stat.close();

               if(VItemCode.size()>0)
               {
                    for(int i=0;i<VItemCode.size();i++)
                    {
                         JCCode.addItem((String)VCodeSlNo.elementAt(i));
                    }

                    TMrsNo.setEditable(false);
                    BApply1.setEnabled(false);
                    JCCode.setEnabled(true);
                    BApply2.setEnabled(true);
               }
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setMrsData()
     {
          try
          {
               String SMrsNo    = TMrsNo.getText();
               String SItemCode = (String)VItemCode.elementAt(JCCode.getSelectedIndex());
			String SItemName = (String)VItemName.elementAt(JCCode.getSelectedIndex());
			String SMrsDate  = (String)VMrsDate.elementAt(JCCode.getSelectedIndex());
               String SSlNo     = (String)VMrsSlNo.elementAt(JCCode.getSelectedIndex());
			String SMrsQty   = (String)VMrsQty.elementAt(JCCode.getSelectedIndex());
			String SOrderNo  = (String)VOrderNo.elementAt(JCCode.getSelectedIndex());

               Statement stat   = theConnect.createStatement();

			double dOrderQty = 0;

			if(common.toDouble(SOrderNo)>0)
			{
				if(common.toInt(SMrsDate)>=common.toInt(SStDate))
				{
               		String QString = " Select Sum(Qty) from PurchaseOrder "+
		                                " Where MillCode="+iMillCode+" and MrsNo="+SMrsNo+
	                                     " and Item_Code='"+SItemCode+"' and MrsSlNo="+SSlNo;

		               ResultSet res  = stat.executeQuery(QString);

		               while (res.next())
		               {
		                    dOrderQty    = res.getDouble(1);
		               }
		               res.close();
				}
				else
				{
					dOrderQty = common.toDouble(SMrsQty);
				}
			}

               stat.close();

			double dPendQty = common.toDouble(SMrsQty) - dOrderQty;


			LItemName.setText(SItemName);
			LMrsQty.setText(SMrsQty);
			LOrderQty.setText(common.getRound(dOrderQty,3));
			LPendQty.setText(common.getRound(dPendQty,3));

			TSplitNo.setText("");

               TopPanel.add(Top2Panel,"Center");

               JCCode.setEnabled(false);
               BApply2.setEnabled(false);

               DeskTop.repaint();
               DeskTop.updateUI();

			if(dPendQty<=0)
			{
				TSplitNo.setEditable(false);				
	               BApply3.setEnabled(false);
				JOptionPane.showMessageDialog(null,"No Pending Qty for Split","Information",JOptionPane.INFORMATION_MESSAGE);
			}
			else
			{
				TSplitNo.setEditable(true);				
	               BApply3.setEnabled(true);
			}
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {

          try
          {
               getContentPane().remove(tabreport);
          }
          catch(Exception ex)
          {}

		int iSplitNo = common.toInt(TSplitNo.getText());

		if(iSplitNo<=0)
		{
			TSplitNo.setEditable(true);				
               BApply3.setEnabled(true);
			JOptionPane.showMessageDialog(null,"Invalid Noof Split","Information",JOptionPane.INFORMATION_MESSAGE);
		}
		else
		{
	          try
	          {
		          RowData     = new Object[iSplitNo][ColumnData.length];
		          for(int i=0;i<iSplitNo;i++)
		          {
		               RowData[i][0]  = String.valueOf(i+1);
		               RowData[i][1]  = "";
		          }

	               tabreport = new TabReport(RowData,ColumnData,ColumnType);
	               tabreport.setBorder(new TitledBorder("Split Qty Breakup"));
	               getContentPane().add(tabreport,BorderLayout.CENTER);

				TSplitNo.setEditable(false);
				BApply3.setEnabled(false);
				BOk.setEnabled(true);

                    DeskTop.repaint();
                    DeskTop.updateUI();
	          }
	          catch(Exception e)
	          {
	               System.out.println(e);
	               e.printStackTrace();
	          }
		}
     }

     public boolean validSplit()
     {
		double dTQty = 0;
		double dPendQty = common.toDouble(LPendQty.getText());

          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
          {
               double dQty = common.toDouble((String)tabreport.ReportTable.getValueAt(i,1));

			if(dQty<=0)
	          {
	               JOptionPane.showMessageDialog(null,"Invalid Qty @Row - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
	               tabreport.ReportTable.requestFocus();
	               return false;
	          }

			dTQty = dTQty + dQty;
		}	

		if(dTQty>dPendQty || dTQty<dPendQty)
          {
               JOptionPane.showMessageDialog(null,getMismatchMessage(dTQty,dPendQty),"Error",JOptionPane.ERROR_MESSAGE);
               tabreport.ReportTable.requestFocus();
               return false;
          }

          return true;
     }

     private String getMismatchMessage(double dTQty,double dPendQty)
     {
          String str = "<html><body>";
          str = str + "<b>Quantity Mismatch</b><br>";
          str = str + "<br>Qty to Split - "+dPendQty+"<br>";
          str = str + "<br>Qty Splitted - "+dTQty+"<br>";
          str = str + "</body></html>";
          return str;
     }

     public void updateMrs(String SMrsNo,String SItemCode,String SMrsSlNo,double dQty,String SNetRate)
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat = theConnect.createStatement();

			String SNetValue = common.getRound(dQty * common.toDouble(SNetRate),2);

               String QS = " Update Mrs Set "+
                           " Qty=0"+common.getRound(dQty,3)+","+
					  " NetValue=0"+SNetValue+" "+
                           " Where MrsNo="+SMrsNo+
                           " And Item_Code='"+SItemCode+"'"+
                           " And SlNo="+SMrsSlNo+
                           " And MillCode="+iMillCode;

               stat.executeUpdate(QS);
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     public void insertMrs(String SMrsNo,String SItemCode,String SMrsSlNo,double dQty,int iSlNo,String SNetRate)
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat = theConnect.createStatement();

			String SNetValue = common.getRound(dQty * common.toDouble(SNetRate),2);

               String SSysName  = InetAddress.getLocalHost().getHostName();
               String SDateTime = common.getServerDate();

			String QS = " Insert Into Mrs (Id,Qty,OrderNo,GrnNo,SlNo,UserCode,CreationDate,NetValue,SystemName,EntryDateTime,"+
					  " MrsNo,MrsDate,Item_Code,Unit_Code,Dept_Code,Group_Code,Catl,Draw,Make,DueDate,OrderBlock,EnquiryNo,"+
					  " HodCode,RefNo,NatureCode,BlockCode,UrgOrd,Posted,PyPost,WA,DM,PlanMonth,MillCode,Remarks,Make_Code,"+
					  " Serv_Plan_No,Serv_Code,Mac_Code,IssueNo,Status,Flag,PaperColor,PaperSets,PaperSize,PaperSide,"+
					  " SlipFromNo,SlipToNo,MrsFlag,MrsTypeCode,BudgetStatus,NetRate,DeptMrsAuth,ItemName,MrsAuthUserCode,"+
					  " YearlyPlanningStatus,YearlyPlanningMonth,DocumentId,ProjectMrsTypeCode,ReasonForMrs,MrsReasonCode,"+
					  " AutoOrderConversion) "+
                           " (Select Mrs_Seq.nextVal,"+
                           "0"+common.getRound(dQty,3)+" as Qty,"+
					  " 0 as OrderNo,"+
					  " 0 as GrnNo,"+		
                           ""+iSlNo+" as SlNo,"+
                           ""+iUserCode+" as UserCode,"+
                           "'"+SDateTime+"' as CreationDate,"+
                           ""+SNetValue+" as NetValue,"+
                           "'"+SSysName+"' as SystemName,"+
                           "'"+SDateTime+"' as EntryDateTime,"+
					  " MrsNo,MrsDate,Item_Code,Unit_Code,Dept_Code,Group_Code,Catl,Draw,Make,DueDate,OrderBlock,EnquiryNo,"+
					  " HodCode,RefNo,NatureCode,BlockCode,UrgOrd,Posted,PyPost,WA,DM,PlanMonth,MillCode,Remarks,Make_Code,"+
					  " Serv_Plan_No,Serv_Code,Mac_Code,IssueNo,Status,Flag,PaperColor,PaperSets,PaperSize,PaperSide,"+
					  " SlipFromNo,SlipToNo,MrsFlag,MrsTypeCode,BudgetStatus,NetRate,DeptMrsAuth,ItemName,MrsAuthUserCode,"+
					  " YearlyPlanningStatus,YearlyPlanningMonth,DocumentId,ProjectMrsTypeCode,ReasonForMrs,MrsReasonCode,"+
					  " AutoOrderConversion from Mrs "+
                           " Where MrsNo="+SMrsNo+
                           " And Item_Code='"+SItemCode+"'"+
                           " And SlNo="+SMrsSlNo+
                           " And MillCode="+iMillCode+")";

               stat.executeUpdate(QS);
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     public int getMaxSlNo(String SMrsNo)
     {
          int iSlNo=0;
          try
          {
               String QS =" Select Max(SlNo) from Mrs Where MrsNo="+SMrsNo+" and MillCode="+iMillCode;

               Statement      stat          =  theConnect.createStatement();
               ResultSet      result        =  stat.executeQuery(QS);
               while(result.next())
               {
                    iSlNo = result.getInt(1);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
          return iSlNo;
     }

     public void removeHelpFrame()
     {
           try
           {
              DeskTop.remove(this);
              DeskTop.repaint();
              DeskTop.updateUI();
           }
           catch(Exception ex) { }
     }

}
