package MRS;

import java.awt.*;
import java.awt.event.*;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import util.*;


public class NewMRSModel extends DefaultTableModel
{

//     String         ColumnData[] = {"Date","MRS No","Code","Name","Quantity","Catl","Draw","Make","Dept","Due Date"};
//     String         ColumnType[] = {"S","N","S","S","N","S","S","S","S","S"};


     String         ColumnName[] = {"Date","MRS No","Code","Name","Stock","Pending","Quantity","Rate","Value","Catl","Draw","Make","Dept"};
     String         ColumnType[] = {"S"   ,"N"     ,"S"   ,"S"   ,"N"    ,"S"      ,"S"       ,"S"   ,"S"   ,"S"   ,"S"    ,"S"   ,"S"   };
     int            ColumnWidth[]= {30    ,20      ,30    ,30    ,20     ,20       ,50        ,20    ,20    ,20    ,20     , 20   , 20   };

     Vector theVector;
     Common common ;
     int iMillCode,iUserCode;
     String SItemTable;

     public NewMRSModel()
     {
          common         = new Common();
          setDataVector(getRowData(),ColumnName);

     }
     public Object[][] getRowData()
     {
          Object RowData[][] = new Object[0][ColumnName.length];

          return RowData;
     }
     public boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="B" || ColumnType[col]=="E")
               return true;
          return false;
     }

     public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }

     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));

          }

          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }
     public int getRows()
     {
         return super.dataVector.size(); 
     }
     public void appendEmptyRow()
     {
          Vector theVect = new Vector();
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");

          insertRow(getRows(),theVect);

     }
     public void deleteRow(int index)
     {
         if(index==-1)
         return;
         removeRow(index);
     }
     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

}

