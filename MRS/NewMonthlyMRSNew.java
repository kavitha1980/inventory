
package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class NewMonthlyMRSNew extends JInternalFrame
{
     JPanel         TopPanel,MiddlePanel,MidTop1,MidTop2;
     JPanel         BottomPanel;
     
     String         SDate;
     JButton        BApply,BPrint,BPrint1A,BPrint2E;
     JTextField     TFile;
     JTextField   JTName;
     
     String         SInt      = "  ";
     String         Strline   = "";
     String         SCatl= "",SMake= "",SDraw= "",SMrsName= "",SMrsQty= "",SDept= "",SMrsNo="",SMrsCode="",findstr="";
     int            iLen      = 0;
     String         SDepName,SOtherDepName;
     NewMRSModel    theModel;

     MRSModelForTable2 theModel1;

     JTable         theTable,theTable1;

     JComboBox      JCMonth,JCGroupName,JCDeptName,JCUserName;
     
     Vector         theVect,VMonth,VMonthCode,VGroupCode,VGroupName;
     
     Object         RowData[][];
     
//     String         ColumnData[] = {"Date","MRS No","Code","Name","Quantity","Catl","Draw","Make","Dept","Due Date"};
//     String         ColumnType[] = {"S","N","S","S","N","S","S","S","S","S"};

     JLayeredPane   DeskTop;
     Vector         VCode,VName;
     StatusPanel    SPanel;
     
     Common common = new Common();
     
     Vector         VMrsDate,VMrsNo,VMrsCode,VMrsName,VRemarks,VMake,VCatl,VDraw,VMrsQty,VReOrd,VStock,VPending,VToOrder,VDeptCode,VDeptName,VDueDate,VNature,VBlock,VUoM,VGroup,VUser;
     Vector         VDueDays,VFreq,VOrdBlock,VOrdDate,VOrdNo,VSupName;
     Vector         VOrdRate,VOrdDisc,VOrdCenVat,VOrdSalTax,VSurChg,VNetRate,VNetValue;

     TabReport1     tabreport;
     int            iMillCode;
     int            iUserCode,iOwnerCode;

     public NewMonthlyMRSNew(JLayeredPane DeskTop,int iMillCode,int iAuthCode,int iUserCode)
     {
          super("Monthly MRS List");
          this.DeskTop   = DeskTop;
          this.VCode     = VCode;
          this.VName     = VName;
          this.SPanel    = SPanel;
          this.iMillCode = iMillCode;
          this.iUserCode = iUserCode;

          setMonths();
          getGroupName();
          getUserName();
          setOwnerCode();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel1.setNumRows(0);
     }
     public void createComponents()
     {
          TopPanel       = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();

          MidTop1        = new JPanel();
          MidTop2        = new JPanel();
     
          JCMonth        = new JComboBox(VMonth);
          JCGroupName    = new JComboBox(VGroupName);
          JCDeptName     = new JComboBox(getDeptName());
          JCUserName     = new JComboBox(getUserName());


          JTName         = new JTextField();

          BApply         = new JButton("Apply");
          BPrint         = new JButton("Print");
          BPrint1A       = new JButton("Print1A");
          BPrint2E       = new JButton("Print2E");
          TFile          = new JTextField(15);

          theModel       = new NewMRSModel();
          theTable       = new JTable(theModel);

          theModel1      = new MRSModelForTable2();
          theTable1      = new JTable(theModel1);

          for(int i=0;i<theModel.ColumnName.length;i++)
          {
               (theTable.getColumnModel()).getColumn(i).setPreferredWidth(theModel.ColumnWidth[i]);
          }
          for(int j=0;j<theModel1.ColumnName.length;j++)
          {
               (theTable1.getColumnModel()).getColumn(j).setPreferredWidth(theModel1.ColumnWidth[j]);
          }
     }
     public void setLayouts()
     {
          TopPanel    .setLayout(new GridLayout(1,9));
          MidTop1     .setLayout(new BorderLayout());
          MidTop2     .setLayout(new BorderLayout());
          MiddlePanel .setLayout(new GridLayout(2,1));
     
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,700,575);
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("Month"));
          TopPanel.add(JCMonth);

          TopPanel.add(new JLabel("GroupName"));
          TopPanel.add(JCGroupName);

          TopPanel.add(new JLabel("Dept Name"));
          TopPanel.add(JCDeptName);

          TopPanel.add(new JLabel("User Name"));
          TopPanel.add(JCUserName);

          TopPanel       . add(BApply);

          MidTop1.add("Center",new JScrollPane(theTable));
          MidTop1.add("South",JTName);

          MidTop2.add("Center",new JScrollPane(theTable1));

          MiddlePanel.add(MidTop1);
          MiddlePanel.add(MidTop2);

          BottomPanel    . add(BPrint);
          BottomPanel    . add(BPrint1A);
          BottomPanel    . add(TFile);
     
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply    . addActionListener(new ApplyList());
          theTable.addKeyListener(new KeyList());
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               theModel.setNumRows(0);
               setDataIntoVector();

               theModel1.setNumRows(0);
               setDataToTable2();
          }
     }
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               JTable theTable = (JTable)ke.getSource();

               int iRow = theTable.getSelectedRow();
               int iCol = theTable.getSelectedColumn();

               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);

               try
               {
                    if(ke.getKeyCode()==8)
                    {

                         findstr=findstr.substring(0,(findstr.length()-1));
                         JTName.setText(findstr);
                         setCursor(theTable,iCol);
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar>='0' && lastchar <= '9'))
                    {
                         findstr=findstr+lastchar;
                         JTName.setText(findstr);
                         setCursor(theTable,iCol);
                    }
               }
               catch(Exception ex){}
          }
          public void keyPressed(KeyEvent ke)
          {
               if (ke.getKeyCode()==KeyEvent.VK_ESCAPE)
               {
                    findstr="";
                    JTName.setText("");
               }
          }
     }

     private void setCursor(JTable theTable,int iCol)
     {
          NewMRSModel theModel = (NewMRSModel) theTable.getModel();
          int index = -1;
          for(int i=0;i<theModel.getRows();i++)
          {
               String str = (String)theModel.getValueAt(i,iCol);
               if(str.startsWith(findstr))
               {
                    index = i;
                    break;
               }
          }
          if(index == -1)
               return;
          theTable.setRowSelectionInterval(index,index);
          Rectangle rect = theTable.getCellRect(index,0,true);
          theTable.scrollRectToVisible(rect);
     }

     /*public void setData()
     {
          VMrsDate       = new Vector();
          VMrsNo         = new Vector();
          VMrsCode       = new Vector();
          VMrsName       = new Vector();
          VUoM           = new Vector();
          VRemarks       = new Vector();
          VMake          = new Vector();
          VCatl          = new Vector();
          VDraw          = new Vector();
          VMrsQty        = new Vector();
          VReOrd         = new Vector();
          theVect        = new Vector();

          String SDate    = common.getServerPureDate();

          String SMonthCode = getMonthCode((String)JCMonth.getSelectedItem());
          String SGroupName = (String)JCGroupName.getSelectedItem();
          

           String QS1 = " create table m1 as SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, MRS.Remarks, MRS.Make, InvItems.Catl, InvItems.Draw, MRS.Qty, MRS.Dept_Code, Dept.Dept_Name,MRS.DueDate,Nature.Nature,OrdBlock.BlockName,Cata.Group_Name "+
                        " FROM ((((MRS INNER JOIN CATA on MRS.GROUP_CODE=CATA.GROUP_CODE) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) "+
                        " Inner join InvItems on InvItems.Item_Code=Mrs.Item_code) "+
                        " Inner Join MonthlyPlanningItem on MonthlyPlanningItem.Item_Code=Mrs.Item_Code "+
                        " and MonthlyPlanningItem.UserCode=Mrs.MrsAuthUserCode and MonthlyPlanningItem.MillCode=Mrs.MillCode) "+
                        " Inner join MatGroup on MatGroup.GroupCode=MonthlyPlanningItem.MatGroupCode "+
                        " Left Join Nature on MRS.NatureCode=Nature.NatureCode "+
                        " Left Join OrdBlock on MRS.BlockCode=OrdBlock.Block "+
                        " WHERE Mrs.MrsFlag=1 and MRS.Qty>0 and substr(MrsDate,1,6) = '"+SMonthCode+"'  and PlanMonth>0 "+
                        " and MrsAuthUserCode="+iOwnerCode+" and Mrs.MillCode="+iMillCode+" "+
                        " and Mrs.MillCode="+iMillCode+" ";

          if(!(JCGroupName.getSelectedItem()).equals("ALL"))
               QS1 = QS1 + " and MatGroup.GroupCode="+VGroupCode.elementAt(JCGroupName.getSelectedIndex());

          if(iMillCode!=1)
          {               
               QS1 = QS1 + " and (Mrs.MillCode is Null OR Mrs.MillCode <> 1) ";
          }
          else
          {
               QS1 = QS1 + " and Mrs.MillCode = 1  ";
          }

//          System.out.println(QS1);



          String QString =    " SELECT m1.MrsDate,m1.MrsNo,m1.Item_Code,"+
                              " m3.ITEM_NAME,m1.Remarks,m1.Make,m1.Catl,"+
                              " m1.Draw, m1.Qty, m3.ReOrd,"+
                              " (nvl(m3.OPGQTY,0)+nvl(m5.GRNQty,0)-nvl(m6.IssueQty,0)) AS Stock,"+
                              " m4.Pending, m1.Dept_Code, m1.Dept_Name,"+
                              " m1.DueDate,m1.Nature,m1.BlockName,m3.UoMName,"+
                              " m1.group_name,m7.BlockName,m7.OrderDate,"+
                              " m7.OrderNo,m7.SupName,"+
                              " m7.RATE,m7.DISCPER,m7.CENVATPER,"+
                              " m7.TAXPER,m7.SURPER"+
                              " FROM (((((m1"+
                              " INNER JOIN m3 ON m1.Item_Code = m3.Item_Code)"+
                              " LEFT JOIN m4 ON m1.Item_Code = m4.Item_Code)"+
                              " LEFT JOIN m5 ON m1.Item_Code = m5.Item_Code)"+
                              " LEFT JOIN m6 ON m1.Item_Code = m6.Item_Code)"+
                              " LEFT JOIN m7 ON m1.Item_Code = m7.Item_Code)"+
                              " Order by m3.ITEM_NAME ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet res = stat.executeQuery(QString);
               stat.executeQuery(QS1);
               
               while (res.next())
               {
                    String SMrsDate = common.parseDate(res.getString(1));
                    String SDueDate = common.parseDate(res.getString(15));
                    String SDays    = common.getDateDiff(common.parseDate(SDate),SMrsDate);

                    String SFreq    = common.getDateDiff(SDueDate,SMrsDate);
                    
                    double dReq     = common.toDouble(res.getString(9));
                    double dReOrd   = common.toDouble(res.getString(10));
                    double dStock   = common.toDouble(res.getString(11));
                    double dPending = common.toDouble(res.getString(12));
                    double dToOrder = dReq-(dStock+dPending-dReOrd);
                    
                    VMrsDate       . addElement(SMrsDate);
                    VMrsNo         . addElement(res.getString(2));
                    VMrsCode       . addElement(res.getString(3));
                    VMrsName       . addElement(res.getString(4));
                    VRemarks       . addElement(res.getString(5));
                    VMake          . addElement(res.getString(6));
                    VCatl          . addElement(res.getString(7));
                    VDraw          . addElement(res.getString(8));
                    VMrsQty        . addElement(""+dReq);
                    VReOrd         . addElement(""+dReOrd);
                    VStock         . addElement(""+dStock);
                    VPending       . addElement(""+dPending);
                    VToOrder       . addElement(""+dToOrder);
                    VDeptCode      . addElement(res.getString(13));
                    VDeptName      . addElement(res.getString(14));
                    VDueDate       . addElement(SDueDate);
                    VNature        . addElement(res.getString(16));
                    VBlock         . addElement(res.getString(17));

                    VFreq          . addElement(SFreq);
                    VUoM           . addElement(res.getString(18));
                    VGroup         . addElement(res.getString(19));

                    VOrdBlock      . addElement(common.parseNull((String)res.getString(20)));
                    VOrdDate       . addElement(common.parseNull((String)res.getString(21)));
                    VOrdNo         . addElement(common.parseNull((String)res.getString(22)));
                    VSupName       . addElement(common.parseNull((String)res.getString(23)));

                    VOrdRate       . addElement(common.parseNull((String)res.getString(24)));
                    VOrdDisc       . addElement(common.parseNull((String)res.getString(25)));
                    VOrdCenVat     . addElement(common.parseNull((String)res.getString(26)));
                    VOrdSalTax     . addElement(common.parseNull((String)res.getString(27)));
                    VSurChg        . addElement(common.parseNull((String)res.getString(28)));

                    SMrsNo      = common.parseNull((String)res.getString(2));
                    SMrsCode    = common.parseNull((String)res.getString(3));
                    SMrsName    = common.parseNull((String)res.getString(4));
                    SMrsQty     = ""+dReq;
                    SCatl       = common.parseNull((String)res.getString(7));
                    SDraw       = common.parseNull((String)res.getString(8));
                    SMake       = common.parseNull((String)res.getString(6));
                    SDept       = common.parseNull((String)res.getString(14));

                    theVect.addElement(SMrsDate);
                    theVect.addElement(SMrsNo);
                    theVect.addElement(SMrsCode);
                    theVect.addElement(SMrsName);
                    theVect.addElement(SMrsQty);
                    theVect.addElement(SCatl);
                    theVect.addElement(SDraw);
                    theVect.addElement(SMake);
                    theVect.addElement(SDept);
                    theVect.addElement(SDueDate);

                    theModel.appendRow(theVect);
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }*/

/* Kavitha--     public void setDataIntoVector()
     {
          VMrsDate       = new Vector();
          VMrsNo         = new Vector();
          VMrsCode       = new Vector();
          VMrsName       = new Vector();
          VUoM           = new Vector();
          VRemarks       = new Vector();
          VMake          = new Vector();
          VCatl          = new Vector();
          VDraw          = new Vector();
          VMrsQty        = new Vector();
          VReOrd         = new Vector();
          VStock         = new Vector();
          VPending       = new Vector();
          VToOrder       = new Vector();
          VDeptCode      = new Vector();
          VDeptName      = new Vector();
          VDueDate       = new Vector();
          VNature        = new Vector();
          VBlock         = new Vector();
          VDueDays       = new Vector();
          VFreq          = new Vector();
          VGroup         = new Vector();
          VOrdBlock      = new Vector();
          VOrdDate       = new Vector();
          VOrdNo         = new Vector();
          VSupName       = new Vector();
          VOrdRate       = new Vector();
          VOrdDisc       = new Vector();
          VOrdCenVat     = new Vector();
          VOrdSalTax     = new Vector();
          VSurChg        = new Vector();
          VNetRate       = new Vector();
          VNetValue      = new Vector();
          VUser          = new Vector();
          
          String SDate    = common.getServerPureDate();

          String SMonthCode = getMonthCode((String)JCMonth.getSelectedItem());
          String SGroupName = (String)JCGroupName.getSelectedItem();
          

           String QS1 = " create table m1 as SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, MRS.Remarks, MRS.Make, InvItems.Catl, InvItems.Draw, MRS.Qty, MRS.Dept_Code, Dept.Dept_Name,MRS.DueDate,Nature.Nature,OrdBlock.BlockName,Cata.Group_Name,MRS.NetRate,MRS.NetValue,MRS.UserCode,RawUser.UserName "+
                        " FROM (((((MonthlyPlanningItem INNER JOIN CATA on MRS.GROUP_CODE=CATA.GROUP_CODE) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) "+
                        " Inner join RawUser on RawUser.UserCode=MonthlyPlanningItem.UserCode) "+                        
				    " Left Join Mrs on MonthlyPlanningItem.Item_Code=Mrs.Item_Code "+
                        " and MonthlyPlanningItem.UserCode=Mrs.MrsAuthUserCode and MonthlyPlanningItem.MillCode=Mrs.MillCode "+
                        " and substr(MrsDate,1,6) = '"+SMonthCode+"'  and PlanMonth>0)  and Mrs.MillCode="+iMillCode+" "
                        " Left join InvItems on InvItems.Item_Code=MonthlyPlanningItem.Item_code) "+
                        " Inner join MatGroup on MatGroup.GroupCode=MonthlyPlanningItem.MatGroupCode "+
                        " Left Join Nature on MRS.NatureCode=Nature.NatureCode "+
                        " Left Join OrdBlock on MRS.BlockCode=OrdBlock.Block "+
                        
                    

          if(!(JCGroupName.getSelectedItem()).equals("ALL"))
               QS1 = QS1 + " and MatGroup.GroupCode="+VGroupCode.elementAt(JCGroupName.getSelectedIndex());
          
//           if(!(JCUserName.getSelectedItem()).equals("ALL"))
  //              System.out.println("USer"+JCUserName.getSelectedIndex());
    //           QS1=QS1+ " and RawUser.UserName = '"+VUserCode.elementAt(JCUserName.getSelectedIndex());


          if(iMillCode!=1)
          {               
               QS1 = QS1 + " and (Mrs.MillCode is Null OR Mrs.MillCode <> 1) ";
          }
          else
          {
               QS1 = QS1 + " and Mrs.MillCode = 1  ";
          }

          System.out.println(QS1);

          String QS2 =   " create table m2 as  SELECT m1.Item_Code"+
                         " FROM m1 GROUP BY m1.Item_Code ";
          
          String QS3 = " ";
          
          if(iMillCode!=1)
          {               
               QS3  =    " create table m3 as SELECT m2.Item_Code, InvItems.OPGQTY, (ROQ+MinQty) AS ReOrd, InvItems.ITEM_NAME,UOM.UoMName"+
                         " FROM m2 LEFT JOIN InvItems ON m2.Item_Code = InvItems.ITEM_CODE LEFT JOIN UOM ON UOM.UoMCode = InvItems.UOMCODE ";
          }
          else
          {
               QS3  =    " create table m3 as SELECT m2.Item_Code, DyeingInvItems.OPGQTY, (DyeingInvItems.ROQ+DyeingInvItems.MinQty) AS ReOrd, InvItems.ITEM_NAME,UOM.UoMName"+
                         " FROM (m2 LEFT JOIN InvItems ON m2.Item_Code = InvItems.ITEM_CODE  LEFT JOIN UOM ON UOM.UoMCode = InvItems.UOMCODE ) "+
                         " Left Join DyeingInvItems On InvItems.Item_Code = DyeingInvItems.Item_Code ";
          }
          
          String QS4 = " create table m4 as SELECT m2.Item_Code, Sum((Qty-InvQty)) AS Pending"+
                       " FROM m2 LEFT JOIN PurchaseOrder ON m2.Item_Code=PurchaseOrder.Item_Code ";
          
          if(iMillCode!=1)
          {               
               QS4 = QS4 +    " Where PurchaseOrder.MillCode is Null Or PurchaseOrder.MillCode <> 1 "+
                              " GROUP BY m2.Item_Code ";
          }
          else
          {
               QS4 = QS4 +    " Where PurchaseOrder.MillCode = 1 "+
                              " GROUP BY m2.Item_Code ";
          }
          
          String QS5 =   " create table m5 as SELECT m2.Item_Code, Sum(GRN.GrnQty) AS GRNQty"+
                         " FROM m2 LEFT JOIN GRN ON m2.Item_Code = GRN.Code  WHERE Grn.GrnBlock<2 ";

          if(iMillCode!=1)
          {
               QS5 = QS5 +    " and Grn.MillCode Is Null Or Grn.MillCode <> 1 "+
                              " GROUP BY m2.Item_Code ";
          }
          else
          {
               QS5 = QS5 +    " and Grn.MillCode = 1 "+
                              " GROUP BY m2.Item_Code ";
          }
          String QS6 =   " create table m6 as SELECT m2.Item_Code, Sum(Issue.Qty) AS IssueQty"+
                         " FROM m2 LEFT JOIN Issue ON m2.Item_Code = Issue.Code ";
          
          if(iMillCode!=1)
          {
               QS6 = QS6 +    " Where Issue.MillCode Is Null Or Issue.MillCode <> 1 "+
                              " GROUP BY m2.Item_Code";
          }
          else
          {
               QS6 = QS6 +    " Where Issue.MillCode = 1 "+
                              " GROUP BY m2.Item_Code";
          }

          String QS7 =   " create table m7 as (select Item_code,blockName,"+
                         " Purchaseorder.OrderNo as OrderNo,Purchaseorder.OrderDate as OrderDate,"+
                         " supplier.name as SupName,Purchaseorder.RATE,Purchaseorder.DISCPER,"+
                         " Purchaseorder.CENVATPER, Purchaseorder.TAXPER,Purchaseorder.SURPER"+
                         " from purchaseorder"+
                         " inner join supplier on supplier.ac_code = purchaseorder.sup_code"+
                         " inner join ordblock on ordblock.block   = purchaseorder.Orderblock"+
                         " where id in "+
                         " (select Ordid from (select max(id) as OrdId,ICode,OrdNo from purchaseorder"+
                         " inner join "+
                         " (select max(OrderNo) as OrdNo,item_code as ICode from"+
                         " (select purchaseOrder.Orderno,purchaseorder.item_code,max(orderdate)"+
                         " from purchaseorder"+
                         " inner join m1 on m1.item_code = purchaseorder.item_code"+
                         " group by purchaseorder.item_code,purchaseorder.Orderno)"+
                         " group by item_code) t on t.OrdNo = PurChaseOrder.orderNo and"+
                         " t.ICode = purchaseOrder.item_code"+
                         " group by Icode,OrdNo"+
                         " Order by ICode)))";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               try{stat.execute("Drop Table m1");}catch(Exception ex){}
               try{stat.execute("Drop Table m2");}catch(Exception ex){}
               try{stat.execute("Drop Table m3");}catch(Exception ex){}
               try{stat.execute("Drop Table m4");}catch(Exception ex){}
               try{stat.execute("Drop Table m5");}catch(Exception ex){}
               try{stat.execute("Drop Table m6");}catch(Exception ex){}
               try{stat.execute("Drop Table m7");}catch(Exception ex){}

               stat.execute(QS1);
               stat.execute(QS2);
               stat.execute(QS3);
               stat.execute(QS4);
               stat.execute(QS5);
               stat.execute(QS6);
               stat.execute(QS7);

               ResultSet res = stat.executeQuery(getQString());
               
               while (res.next())
               {
                    theVect = new Vector();
                    String SMrsDate = common.parseDate(res.getString(1));
                    String SDueDate = common.parseDate(res.getString(15));
                    String SDays    = common.getDateDiff(common.parseDate(SDate),SMrsDate);

                    String SFreq    = common.getDateDiff(SDueDate,SMrsDate);
                    String SNetRate = common.parseNull(res.getString(29));
                    String SNetValue= common.parseNull(res.getString(30));
                   // String SUserName= common.parseNull(res.getString(32));

                    double dReq     = common.toDouble(res.getString(9));
                    double dReOrd   = common.toDouble(res.getString(10));
                    double dStock   = common.toDouble(res.getString(11));
                    double dPending = common.toDouble(res.getString(12));
                    double dToOrder = dReq-(dStock+dPending-dReOrd);

                    
                    VMrsDate       . addElement(SMrsDate);
                    VMrsNo         . addElement(res.getString(2));
                    VMrsCode       . addElement(res.getString(3));
                    VMrsName       . addElement(res.getString(4));
                    VRemarks       . addElement(res.getString(5));
                    VMake          . addElement(res.getString(6));
                    VCatl          . addElement(res.getString(7));
                    VDraw          . addElement(res.getString(8));
                    VMrsQty        . addElement(""+dReq);
                    VReOrd         . addElement(""+dReOrd);
                    VStock         . addElement(""+dStock);
                    VPending       . addElement(""+dPending);
                    VToOrder       . addElement(""+dToOrder);
                    VDeptCode      . addElement(res.getString(13));
                    VDeptName      . addElement(res.getString(14));
                    VDueDate       . addElement(SDueDate);
                    VNature        . addElement(res.getString(16));
                    VBlock         . addElement(res.getString(17));

                    VFreq          . addElement(SFreq);
                    VUoM           . addElement(res.getString(18));
                    VGroup         . addElement(res.getString(19));

                    VOrdBlock      . addElement(common.parseNull((String)res.getString(20)));
                    VOrdDate       . addElement(common.parseNull((String)res.getString(21)));
                    VOrdNo         . addElement(common.parseNull((String)res.getString(22)));
                    VSupName       . addElement(common.parseNull((String)res.getString(23)));

                    VOrdRate       . addElement(common.parseNull((String)res.getString(24)));
                    VOrdDisc       . addElement(common.parseNull((String)res.getString(25)));
                    VOrdCenVat     . addElement(common.parseNull((String)res.getString(26)));
                    VOrdSalTax     . addElement(common.parseNull((String)res.getString(27)));
                    VSurChg        . addElement(common.parseNull((String)res.getString(28)));

                    VNetRate       . addElement(common.parseNull((String)res.getString(29)));
                    VNetValue      . addElement(common.parseNull((String)res.getString(30)));

                    SMrsNo      = common.parseNull((String)res.getString(2));
                    SMrsCode    = common.parseNull((String)res.getString(3));
                    SMrsName    = common.parseNull((String)res.getString(4));
                    SMrsQty     = ""+dReq;
                    SCatl       = common.parseNull((String)res.getString(7));
                    SDraw       = common.parseNull((String)res.getString(8));
                    SMake       = common.parseNull((String)res.getString(6));
                    SDept       = common.parseNull((String)res.getString(14));

                    theVect.addElement(SMrsDate);
                    theVect.addElement(SMrsNo);
                    theVect.addElement(SMrsCode);
                    theVect.addElement(SMrsName);
                    theVect.addElement(dStock);
                    theVect.addElement(dPending);
                    theVect.addElement(SMrsQty);
                    theVect.addElement(SNetRate);
                    theVect.addElement(SNetValue);
                    theVect.addElement(SCatl);
                    theVect.addElement(SDraw);
                    theVect.addElement(SMake);
                    theVect.addElement(SDept);

                    theModel.appendRow(theVect);
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     } */
     public void setDataIntoVector()
     {
          VMrsDate       = new Vector();
          VMrsNo         = new Vector();
          VMrsCode       = new Vector();
          VMrsName       = new Vector();
          VUoM           = new Vector();
          VRemarks       = new Vector();
          VMake          = new Vector();
          VCatl          = new Vector();
          VDraw          = new Vector();
          VMrsQty        = new Vector();
          VReOrd         = new Vector();
          VStock         = new Vector();
          VPending       = new Vector();
          VToOrder       = new Vector();
          VDeptCode      = new Vector();
          VDeptName      = new Vector();
          VDueDate       = new Vector();
          VNature        = new Vector();
          VBlock         = new Vector();
          VDueDays       = new Vector();
          VFreq          = new Vector();
          VGroup         = new Vector();
          VOrdBlock      = new Vector();
          VOrdDate       = new Vector();
          VOrdNo         = new Vector();
          VSupName       = new Vector();
          VOrdRate       = new Vector();
          VOrdDisc       = new Vector();
          VOrdCenVat     = new Vector();
          VOrdSalTax     = new Vector();
          VSurChg        = new Vector();
          VNetRate       = new Vector();
          VNetValue      = new Vector();
          VUser          = new Vector();
          
          String SDate    = common.getServerPureDate();

          String SMonthCode = getMonthCode((String)JCMonth.getSelectedItem());
          String SUserName  = (String)JCUserName.getSelectedItem();
          String SGroupName = (String)JCGroupName.getSelectedItem();
          

           String QS1 = " create table m1 as SELECT MRS.MrsDate, MRS.MrsNo, MonthlyPlanningItem.Item_Code, MRS.Remarks, MRS.Make, InvItems.Catl, InvItems.Draw, MRS.Qty, MonthlyPlanningItem.ItemDeptCode as Dept_Code, Dept.Dept_Name,MRS.DueDate,Nature.Nature,OrdBlock.BlockName,Cata.Group_Name,MRS.NetRate,MRS.NetValue,MonthlyPlanningItem.UserCode,RawUser.UserName"+          
				    " FROM  MonthlyPlanningItem Left Join CATA on MonthlyPlanningItem.MatGROUPCODE=CATA.GROUP_CODE "+
				    " Inner join RawUser on RawUser.UserCode=MonthlyPlanningItem.UserCode and monthlyPlanningItem.MillCode="+iMillCode+
				    " Left Join Mrs on MonthlyPlanningItem.Item_Code=Mrs.Item_Code and MonthlyPlanningItem.UserCode=Mrs.MrsAuthUserCode and MonthlyPlanningItem.MillCode=Mrs.MillCode and substr(MrsDate,1,6) = "+SMonthCode+" and PlanMonth>0  and MonthlyPlanningItem.MillCode="+iMillCode+
				    " Left join InvItems on InvItems.Item_Code=MonthlyPlanningItem.Item_code "+
				    " Left Join MatGroup on MatGroup.GroupCode=Mrs.Group_Code"+
				    " Left Join Dept ON Mrs.Dept_Code=Dept.Dept_code"+
				    " Left Join Nature on MRS.NatureCode=Nature.NatureCode"+
				    " Left Join OrdBlock on MRS.BlockCode=OrdBlock.Block"+
				    " where RawUser.UserName='"+ SUserName +"'";


             if(!(JCGroupName.getSelectedItem()).equals("ALL"))
               QS1 = QS1 + " and MatGroup.GroupCode="+VGroupCode.elementAt(JCGroupName.getSelectedIndex());
          
/*           if(!(JCUserName.getSelectedItem()).equals("ALL"))
                System.out.println("USer"+JCUserName.getSelectedIndex());
               QS1=QS1+ " and RawUser.UserName = '"+VUserCode.elementAt(JCUserName.getSelectedIndex());*/


          if(iMillCode!=1)
          {               
               QS1 = QS1 + " and (Mrs.MillCode is Null OR Mrs.MillCode <> 1) ";
          }
          else
          {
               QS1 = QS1 + " and Mrs.MillCode = 1  ";
          }

          System.out.println(QS1);

          String QS2 =   " create table m2 as  SELECT m1.Item_Code"+
                         " FROM m1 GROUP BY m1.Item_Code ";
          
          String QS3 = " ";
          
          if(iMillCode!=1)
          {               
               QS3  =    " create table m3 as SELECT m2.Item_Code, InvItems.OPGQTY, (ROQ+MinQty) AS ReOrd, InvItems.ITEM_NAME,UOM.UoMName"+
                         " FROM m2 LEFT JOIN InvItems ON m2.Item_Code = InvItems.ITEM_CODE LEFT JOIN UOM ON UOM.UoMCode = InvItems.UOMCODE ";
          }
          else
          {
               QS3  =    " create table m3 as SELECT m2.Item_Code, DyeingInvItems.OPGQTY, (DyeingInvItems.ROQ+DyeingInvItems.MinQty) AS ReOrd, InvItems.ITEM_NAME,UOM.UoMName"+
                         " FROM (m2 LEFT JOIN InvItems ON m2.Item_Code = InvItems.ITEM_CODE  LEFT JOIN UOM ON UOM.UoMCode = InvItems.UOMCODE ) "+
                         " Left Join DyeingInvItems On InvItems.Item_Code = DyeingInvItems.Item_Code ";
          }
          
          String QS4 = " create table m4 as SELECT m2.Item_Code, Sum((Qty-InvQty)) AS Pending"+
                       " FROM m2 LEFT JOIN PurchaseOrder ON m2.Item_Code=PurchaseOrder.Item_Code ";
          
          if(iMillCode!=1)
          {               
               QS4 = QS4 +    " Where PurchaseOrder.MillCode is Null Or PurchaseOrder.MillCode <> 1 "+
                              " GROUP BY m2.Item_Code ";
          }
          else
          {
               QS4 = QS4 +    " Where PurchaseOrder.MillCode = 1 "+
                              " GROUP BY m2.Item_Code ";
          }
          
          String QS5 =   " create table m5 as SELECT m2.Item_Code, Sum(GRN.GrnQty) AS GRNQty"+
                         " FROM m2 LEFT JOIN GRN ON m2.Item_Code = GRN.Code  WHERE Grn.GrnBlock<2 ";

          if(iMillCode!=1)
          {
               QS5 = QS5 +    " and Grn.MillCode Is Null Or Grn.MillCode <> 1 "+
                              " GROUP BY m2.Item_Code ";
          }
          else
          {
               QS5 = QS5 +    " and Grn.MillCode = 1 "+
                              " GROUP BY m2.Item_Code ";
          }
          String QS6 =   " create table m6 as SELECT m2.Item_Code, Sum(Issue.Qty) AS IssueQty"+
                         " FROM m2 LEFT JOIN Issue ON m2.Item_Code = Issue.Code ";
          
          if(iMillCode!=1)
          {
               QS6 = QS6 +    " Where Issue.MillCode Is Null Or Issue.MillCode <> 1 "+
                              " GROUP BY m2.Item_Code";
          }
          else
          {
               QS6 = QS6 +    " Where Issue.MillCode = 1 "+
                              " GROUP BY m2.Item_Code";
          }

          String QS7 =   " create table m7 as (select Item_code,blockName,"+
                         " Purchaseorder.OrderNo as OrderNo,Purchaseorder.OrderDate as OrderDate,"+
                         " supplier.name as SupName,Purchaseorder.RATE,Purchaseorder.DISCPER,"+
                         " Purchaseorder.CENVATPER, Purchaseorder.TAXPER,Purchaseorder.SURPER"+
                         " from purchaseorder"+
                         " inner join supplier on supplier.ac_code = purchaseorder.sup_code"+
                         " inner join ordblock on ordblock.block   = purchaseorder.Orderblock"+
                         " where id in "+
                         " (select Ordid from (select max(id) as OrdId,ICode,OrdNo from purchaseorder"+
                         " inner join "+
                         " (select max(OrderNo) as OrdNo,item_code as ICode from"+
                         " (select purchaseOrder.Orderno,purchaseorder.item_code,max(orderdate)"+
                         " from purchaseorder"+
                         " inner join m1 on m1.item_code = purchaseorder.item_code"+
                         " group by purchaseorder.item_code,purchaseorder.Orderno)"+
                         " group by item_code) t on t.OrdNo = PurChaseOrder.orderNo and"+
                         " t.ICode = purchaseOrder.item_code"+
                         " group by Icode,OrdNo"+
                         " Order by ICode)))";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               try{stat.execute("Drop Table m1");}catch(Exception ex){}
               try{stat.execute("Drop Table m2");}catch(Exception ex){}
               try{stat.execute("Drop Table m3");}catch(Exception ex){}
               try{stat.execute("Drop Table m4");}catch(Exception ex){}
               try{stat.execute("Drop Table m5");}catch(Exception ex){}
               try{stat.execute("Drop Table m6");}catch(Exception ex){}
               try{stat.execute("Drop Table m7");}catch(Exception ex){}

               stat.execute(QS1);
               stat.execute(QS2);
               stat.execute(QS3);
               stat.execute(QS4);
               stat.execute(QS5);
               stat.execute(QS6);
               stat.execute(QS7);

               ResultSet res = stat.executeQuery(getQString());

               
               while (res.next())
               {
                    theVect = new Vector();
                    String SMrsDate = common.parseDate(res.getString(1));
                    String SDueDate = common.parseDate(res.getString(15));
                    String SDays    = common.getDateDiff(common.parseDate(SDate),SMrsDate);

                    String SFreq    = common.getDateDiff(SDueDate,SMrsDate);
                    String SNetRate = common.parseNull(res.getString(29));
                    String SNetValue= common.parseNull(res.getString(30));
/*			     String SUserCode= common.parseNull(res.getString(31));
                    String sUserName= common.parseNull(res.getString(32));
                    System.out.println(" User"+SUserCode+" __"+sUserName);*/

                    double dReq     = common.toDouble(res.getString(9));
                    double dReOrd   = common.toDouble(res.getString(10));
                    double dStock   = common.toDouble(res.getString(11));
                    double dPending = common.toDouble(res.getString(12));
				//double dPendingQty   = dPending-common.toDouble(SMrsQty);

                    double dToOrder = dReq-(dStock+dPending-dReOrd);

                    
                    VMrsDate       . addElement(SMrsDate);
                    VMrsNo         . addElement(res.getString(2));
                    VMrsCode       . addElement(res.getString(3));
                    VMrsName       . addElement(res.getString(4));
                    VRemarks       . addElement(res.getString(5));
                    VMake          . addElement(res.getString(6));
                    VCatl          . addElement(res.getString(7));
                    VDraw          . addElement(res.getString(8));
                    VMrsQty        . addElement(""+dReq);
                    VReOrd         . addElement(""+dReOrd);
                    VStock         . addElement(""+dStock);
                    VPending       . addElement(""+dPending);
                    VToOrder       . addElement(""+dToOrder);
                    VDeptCode      . addElement(res.getString(13));
                    VDeptName      . addElement(res.getString(14));
                    VDueDate       . addElement(SDueDate);
                    VNature        . addElement(res.getString(16));
                    VBlock         . addElement(res.getString(17));

                    VFreq          . addElement(SFreq);
                    VUoM           . addElement(res.getString(18));
                    VGroup         . addElement(res.getString(19));

                    VOrdBlock      . addElement(common.parseNull((String)res.getString(20)));
                    VOrdDate       . addElement(common.parseNull((String)res.getString(21)));
                    VOrdNo         . addElement(common.parseNull((String)res.getString(22)));
                    VSupName       . addElement(common.parseNull((String)res.getString(23)));

                    VOrdRate       . addElement(common.parseNull((String)res.getString(24)));
                    VOrdDisc       . addElement(common.parseNull((String)res.getString(25)));
                    VOrdCenVat     . addElement(common.parseNull((String)res.getString(26)));
                    VOrdSalTax     . addElement(common.parseNull((String)res.getString(27)));
                    VSurChg        . addElement(common.parseNull((String)res.getString(28)));

                    VNetRate       . addElement(common.parseNull((String)res.getString(29)));
                    VNetValue      . addElement(common.parseNull((String)res.getString(30)));

                    SMrsNo      = common.parseNull((String)res.getString(2));
                    SMrsCode    = common.parseNull((String)res.getString(3));
                    SMrsName    = common.parseNull((String)res.getString(4));
                    SMrsQty     = ""+dReq;
                    SCatl       = common.parseNull((String)res.getString(7));
                    SDraw       = common.parseNull((String)res.getString(8));
                    SMake       = common.parseNull((String)res.getString(6));
                    SDept       = common.parseNull((String)res.getString(14));

                    theVect.addElement(SMrsDate);
                    theVect.addElement(SMrsNo);
                    theVect.addElement(SMrsCode);
                    theVect.addElement(SMrsName);
                    theVect.addElement(dStock);
                    theVect.addElement(dPending);
                    theVect.addElement(SMrsQty);
                    theVect.addElement(SNetRate);
                    theVect.addElement(SNetValue);
                    theVect.addElement(SCatl);
                    theVect.addElement(SDraw);
                    theVect.addElement(SMake);
                    theVect.addElement(SDept);

                    theModel.appendRow(theVect);
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     } 

     private Vector getDeptName()
     {
          Vector theDeptVect =  new Vector();
          theDeptVect   . addElement("ALL");

          StringBuffer sb     = new StringBuffer();
          sb.append(" select Dept_Code, Dept_Name from Dept order by Dept_Code ");

          try
          {
               ORAConnection oraConnection   = ORAConnection.getORAConnection();
               Connection    theConnection   = oraConnection.getConnection();
               PreparedStatement thePre      = theConnection.prepareCall(sb.toString());

               ResultSet theResult = thePre.executeQuery();

               while(theResult.next())
               {
                    theDeptVect.addElement(theResult.getString(2));
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          return theDeptVect;
     }

     private Vector getUserName()
     {
          Vector theUserVect =  new Vector();
//          theUserVect   . addElement("ALL");

          StringBuffer sb     = new StringBuffer();
          sb.append(" select UserCode, UserName from RawUser Where MRSStatus=1 order by UserName ");

          try
          {
               ORAConnection oraConnection   = ORAConnection.getORAConnection();
               Connection    theConnection   = oraConnection.getConnection();
               PreparedStatement thePre      = theConnection.prepareCall(sb.toString());

               ResultSet theResult = thePre.executeQuery();

               while(theResult.next())
               {
                    theUserVect.addElement(theResult.getString(2));
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          return theUserVect;
     }

     private void setDataToTable2()
     {
          Vector VTable2   =  new Vector();

          try
          {
               VTable2  =  getDataToTable2();

               for(int i=0;i<VTable2.size();i++)
               {
                    HashMap map     =  (HashMap)VTable2.elementAt(i);
                    Vector VtoTable2 =  new Vector();

                    VtoTable2.addElement(common.parseNull((String)map.get("DEPT_NAME")));
                    VtoTable2.addElement(common.parseNull((String)map.get("NETVALUE")));

                    theModel1.appendRow(VtoTable2);
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private Vector getDataToTable2()
     {
          String SMonthCode = getMonthCode((String)JCMonth.getSelectedItem());

          Vector       theDataVect = new Vector();
          StringBuffer sb          = new StringBuffer();

          sb.append(" Select Dept.Dept_Name,RawUser.UserName,sum(MRS.Netvalue) as NetValue ");
          sb.append(" FROM MRS INNER JOIN CATA on MRS.GROUP_CODE=CATA.GROUP_CODE ");
          sb.append(" INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code ");
          sb.append(" Left JOIN RawUser On RawUser.UserCode=MRS.UserCode ");
          sb.append(" Inner join InvItems on InvItems.Item_Code=Mrs.Item_code ");
          sb.append(" Inner Join MonthlyPlanningItem ");
          sb.append(" on MonthlyPlanningItem.Item_Code=Mrs.Item_Code ");
          sb.append(" and MonthlyPlanningItem.UserCode=Mrs.MrsAuthUserCode ");
          sb.append(" and MonthlyPlanningItem.MillCode=Mrs.MillCode ");
          sb.append(" Inner join MatGroup on MatGroup.GroupCode=MonthlyPlanningItem.MatGroupCode ");
          sb.append(" Left Join Nature on MRS.NatureCode=Nature.NatureCode ");
          sb.append(" Left Join OrdBlock on MRS.BlockCode=OrdBlock.Block ");
          sb.append(" WHERE Mrs.MrsFlag=1 and MRS.Qty>0 and substr(MrsDate,1,6) = '"+SMonthCode+"' ");
//          sb.append(" and MrsAuthUserCode="+iOwnerCode+" ");
          sb.append(" and Mrs.MillCode="+iMillCode+" ");
          sb.append(" and PlanMonth>0 "); 
      
          if(!(JCDeptName.getSelectedItem()).equals("ALL"))
               sb.append(" and Dept.Dept_Name = '"+(String)JCDeptName.getSelectedItem()+"' ");

//          if(!(JCUserName.getSelectedItem()).equals("ALL"))
               sb.append(" and RawUser.UserName = '"+(String)JCUserName.getSelectedItem()+"' ");

          sb.append(" group by Dept.Dept_Name,RawUser.UserName Order by Dept_Name "); 

          try
          {
               ORAConnection oraConnection = ORAConnection.getORAConnection();
               Connection    theConnection = oraConnection.getConnection();
               PreparedStatement thePre    = theConnection.prepareCall(sb.toString());
               ResultSet     theResult     = thePre       .executeQuery();
               ResultSetMetaData theData   = theResult    .getMetaData();

               while(theResult.next())
               {
                    HashMap theMap =  new HashMap();
                    for(int i=0;i<theData.getColumnCount();i++)
                    {
                         theMap.put(theData.getColumnName(i+1),theResult.getString(i+1));
                    }
                    theDataVect.addElement(theMap);
               }
               System.out.println(sb.toString());
          }
          catch(Exception ex)
          {
               System.out.println("DeptQry"+ex);
               ex.printStackTrace();
			
          }
          return theDataVect;
     }  
     /*public void setRowData()
     {
          RowData     = new Object[VMrsDate.size()][ColumnData.length];
          for(int i=0;i<VMrsDate.size();i++)
          {
               RowData[i][0]  = (String)VMrsDate  .elementAt(i);
               RowData[i][1]  = (String)VMrsNo    .elementAt(i);
               RowData[i][2]  = (String)VMrsCode  .elementAt(i);
               RowData[i][3]  = (String)VMrsName  .elementAt(i);
               RowData[i][4]  = (String)VMrsQty   .elementAt(i);
               RowData[i][5]  = (String)VCatl     .elementAt(i);
               RowData[i][6]  = (String)VDraw     .elementAt(i);
               RowData[i][7]  = (String)VMake     .elementAt(i);
               RowData[i][8]  = (String)VDeptName .elementAt(i);
             //  RowData[i][9]  = (String)VDueDate  .elementAt(i);
          }  
     }*/ 

     public String getQString()
     {
            
        /*  String SDate    = common.getServerPureDate();

          String SMonthCode = getMonthCode((String)JCMonth.getSelectedItem());
          String SUserName  = (String)JCUserName.getSelectedItem();
          String SGroupName = (String)JCGroupName.getSelectedItem();

		 String QS = " SELECT MRS.MrsDate, MRS.MrsNo, MonthlyPlanningItem.Item_Code,MonthlyPlanningItem.Item_Name, MRS.Remarks, MRS.Make, InvItems.Catl, InvItems.Draw, MRS.Qty, MonthlyPlanningItem.ItemDeptCode as Dept_Code, Dept.Dept_Name,MRS.DueDate,Nature.Nature,OrdBlock.BlockName,Cata.Group_Name,MRS.NetRate,MRS.NetValue,MRS.UserCode,RawUser.UserName"+          
				    " FROM  MonthlyPlanningItem Left Join CATA on MonthlyPlanningItem.MatGROUPCODE=CATA.GROUP_CODE "+
				    " Inner join RawUser on RawUser.UserCode=MonthlyPlanningItem.UserCode and monthlyPlanningItem.MillCode="+iMillCode+
				    " Left Join Mrs on MonthlyPlanningItem.Item_Code=Mrs.Item_Code and MonthlyPlanningItem.UserCode=Mrs.MrsAuthUserCode and MonthlyPlanningItem.MillCode=Mrs.MillCode and substr(MrsDate,1,6) = "+SMonthCode+" and PlanMonth>0  and MonthlyPlanningItem.MillCode="+iMillCode+
				    " Left join InvItems on InvItems.Item_Code=MonthlyPlanningItem.Item_code "+
				    " Left Join MatGroup on MatGroup.GroupCode=Mrs.Group_Code"+
				    " Left Join Dept ON Mrs.Dept_Code=Dept.Dept_code"+
				    " Left Join Nature on MRS.NatureCode=Nature.NatureCode"+
				    " Left Join OrdBlock on MRS.BlockCode=OrdBlock.Block"+
				    " where RawUser.UserName='"+ SUserName +"'";

                 if(!(JCGroupName.getSelectedItem()).equals("ALL"))
               QS = QS + " and MatGroup.GroupCode="+VGroupCode.elementAt(JCGroupName.getSelectedIndex());
          
/*           if(!(JCUserName.getSelectedItem()).equals("ALL"))
                System.out.println("USer"+JCUserName.getSelectedIndex());
               QS1=QS1+ " and RawUser.UserName = '"+VUserCode.elementAt(JCUserName.getSelectedIndex());*/


     /*     if(iMillCode!=1)
          {               
               QS = QS + " and (Mrs.MillCode is Null OR Mrs.MillCode <> 1) ";
          }
          else
          {
               QS = QS + " and Mrs.MillCode = 1  ";
          }*/

          String QString =    " SELECT m1.MrsDate,m1.MrsNo,m1.Item_Code,"+
                              " m3.ITEM_NAME,m1.Remarks,m1.Make,m1.Catl,"+
                              " m1.Draw, m1.Qty, m3.ReOrd,"+
                              " (nvl(m3.OPGQTY,0)+nvl(m5.GRNQty,0)-nvl(m6.IssueQty,0)) AS Stock,"+
                              " m4.Pending, m1.Dept_Code, m1.Dept_Name,"+
                              " m1.DueDate,m1.Nature,m1.BlockName,m3.UoMName,"+
                              " m1.group_name,m7.BlockName,m7.OrderDate,"+
                              " m7.OrderNo,m7.SupName,"+
                              " m7.RATE,m7.DISCPER,m7.CENVATPER,"+
                              " m7.TAXPER,m7.SURPER,m1.NetRate,m1.NetValue"+
                              " FROM m1"+
                              " INNER JOIN m3 ON m1.Item_Code = m3.Item_Code"+
                              " Left JOIN RawUser ON RawUser.UserCode = m1.UserCode"+
                              " LEFT JOIN m4 ON m1.Item_Code = m4.Item_Code"+
                              " LEFT JOIN m5 ON m1.Item_Code = m5.Item_Code"+
                              " LEFT JOIN m6 ON m1.Item_Code = m6.Item_Code"+
                              " LEFT JOIN m7 ON m1.Item_Code = m7.Item_Code";

                              if(!(JCDeptName.getSelectedItem()).equals("ALL"))
                 QString +=   " where m1.Dept_Name = '"+(String)JCDeptName.getSelectedItem()+"'";
                 QString +=  "  and RawUser.UserName = '"+(String)JCUserName.getSelectedItem()+"'";
                 

                 /* if(!(JCUserName.getSelectedItem()).equals("ALL"))
                         if(JCDeptName.getSelectedItem().equals("ALL")){
                               QString +=  "  Where RawUser.UserName = '"+(String)JCUserName.getSelectedItem()+"'";
                  }   
                          else{
                               QString +=  "  and RawUser.UserName = '"+(String)JCUserName.getSelectedItem()+"'";
                              }*/

                 QString +=   " Order by m1.Mrsno";

          System.out.println(QString);
          return QString;
     } 
     public void setOwnerCode()
     {
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
 //               ResultSet      res            =  stat.executeQuery(" select distinct AuthUserCode from MrsUserAuthentication where UserCode="+iUserCode);
               ResultSet      res            =  stat.executeQuery(" select distinct AuthUserCode from MrsUserAuthentication");

               System.out.println("UserCode "+iUserCode);
               if(res.next())
                iOwnerCode = res.getInt(1);
               System.out.println(" Qry OwnerCode"+iOwnerCode);

               if(iOwnerCode==5825)
                    iOwnerCode=1985;

               res.close();
               stat.close();
               System.out.println(" OwnerCode"+iOwnerCode);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     public void getGroupName()
     {
          VGroupCode   = new Vector();
          VGroupName   = new Vector();
          VGroupName   . addElement("ALL");
          VGroupCode   . addElement("8888");

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               ResultSet      res            =  stat.executeQuery(" select GroupCode,GroupName from MatGroup Order by GroupName ");

               while(res.next())
               {
                    VGroupCode  . addElement(res.getString(1));
                    VGroupName  . addElement(res.getString(2));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }


     private void setMonths()
     {
          VMonth    = new Vector();
          VMonthCode= new Vector();

          String SDate     = common.getServerPureDate();
          String SMonth    = SDate.substring(0,6);

          for(int i=0; i<12; i++)
          {
               String SMonthName = common.getMonthName(common.toInt(SMonth));

               VMonthCode.addElement(SMonth);
               VMonth.addElement(SMonthName);

               SMonth = String.valueOf(common.getPreviousMonth(common.toInt(SMonth)));
          }
     }
     private String getMonthCode(String SMonth)
     {
          int iIndex = VMonth.indexOf(SMonth);

          if(iIndex==-1)
          return "";

          return   (String)VMonthCode.elementAt(iIndex);
     }
}




