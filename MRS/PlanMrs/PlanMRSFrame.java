package MRS.PlanMrs;

/*

     Conversion of Regular Planned Materials Into MRS


*/

import util.*;
import guiutil.*;
import jdbc.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

public class PlanMRSFrame extends JInternalFrame
{
     JPanel         TopPanel;
     JPanel         BottomPanel;
     JPanel         MiddlePanel;
     JButton        BApply;
     NextField      TMonth;
     DateField      TDate;
     JTextArea      TA;
     
     Vector         VMRSNo,VMRSDate,VTempGroup,VRefNo,VCatl,VDraw,VDueDate,VCode,VHodCode,VQty,V3Qty,VDeptCode,VGroupCode;
     Common         common = new Common();
     
     JLayeredPane   Layer;
     int            iMillCode;
     String         SStDate,SEnDate;
     int            iUserCode;
     String         SItemTable;

     String         SPColour  = "",SPSet     = "",SPSize    = "",SPSide    = "";
     
     int            iMaxMRSNo = 0;

     Vector VCDeptCode,VCGroupCode,VCUnitCode;     

     public PlanMRSFrame(JLayeredPane Layer,int iMillCode,String SStDate,String SEnDate,int iUserCode,String SItemTable)
     {
          this.Layer          = Layer;
          this.iMillCode      = iMillCode;
          this.SStDate        = SStDate;
          this.SEnDate        = SEnDate;
          this.iUserCode      = iUserCode;
          this.SItemTable     = SItemTable;
          
          setVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          MiddlePanel    = new JPanel();
          BApply         = new JButton("Start Fetching Planned Data");
          TMonth         = new NextField(10);
          TDate          = new DateField();
          TA             = new JTextArea(10,50);
          TDate          .     setTodayDate();
     }
     
     private void setLayouts()
     {
          setTitle("Regular Plan To MRS");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,400,400);
          
          TopPanel       . setLayout(new FlowLayout(0,0,0));
          MiddlePanel    . setLayout(new BorderLayout());
          TMonth         . setBorder(new TitledBorder("Plan Month"));
     }
     
     private void addComponents()
     {
          TopPanel            . add(TMonth);
          TopPanel            . add(TDate);
          TopPanel            . add(BApply);
          
          MiddlePanel         . add("Center", new JScrollPane(TA));
          
          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("Center",MiddlePanel);
     }

     private void addListeners()
     {
          BApply         . addActionListener(new ApplyList());
     }

     private class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if (common.toInt(TMonth.getText())==0)
                    return;
               
               if(isValidMonth())
               {
                    BApply.setEnabled(false);

                    fetchData();
                    fillBlanks();
                    setData();

                    TA             . setText("Total Number of Materials Fetched : \n\n");
                    TA             . append ("Last MRS No                       : \n");
                    MiddlePanel    . updateUI();
                    TA             . updateUI();
                    BApply         . setEnabled(true);
               }
          }
     }

     private void fetchData()
     {
          String SPlanDate = "200399";
          
          VMRSNo       = new Vector();
          VMRSDate     = new Vector();
          VTempGroup   = new Vector();
          VRefNo       = new Vector();
          VCatl        = new Vector();
          VDraw        = new Vector();
          VDueDate     = new Vector();
          VCode        = new Vector();
          VHodCode     = new Vector();
          VQty         = new Vector();
          V3Qty        = new Vector();
          VDeptCode    = new Vector();
          VGroupCode   = new Vector();
          
          String QS = "";
          
          QS = " Select RegPlan.Item_Code,RegPlan.HodCode, "+
               " RegPlan.Group_Code,RegPlan.ToOrder, "+
               " InvItems.Catl,InvItems.Draw,RegPlan.Plan_Date,Hod.HodName,MatGroup.GroupName,InvItems.Item_Name,RegPlan.To3Order "+
               " From ((RegPlan "+
               " Inner Join InvItems On InvItems.Item_Code = RegPlan.Item_Code) "+
               " INNER JOIN MatGroup ON RegPlan.Group_Code=MatGroup.GroupCode) "+
               " INNER JOIN Hod ON RegPlan.HodCode=Hod.HodCode "+
               " Where substr(RegPlan.Plan_Date,5,2)='"+TMonth.getText()+"' "+
               " And RegPlan.Plan_Date >='"+SStDate+"' "+
               " And ToOrder+To3Order > 0 "+
               " And RegPlan.MillCode="+iMillCode+
               " ORDER BY MatGroup.GroupName,Hod.HodName,InvItems.Item_Name ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection     . getORAConnection();
               Connection     theConnection =  oraConnection     . getConnection();
                              theConnection .  setAutoCommit(true);
               Statement      stat          =  theConnection     . createStatement();

               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    VMRSNo              . addElement("0");
                    VRefNo              . addElement("99999");
                    VCode               . addElement(result.getString(1));
                    VHodCode            . addElement(result.getString(2));
                    VTempGroup          . addElement(result.getString(3));
                    VQty                . addElement(result.getString(4));
                    VCatl               . addElement(common.parseNull(result.getString(5)));
                    VDraw               . addElement(common.parseNull(result.getString(6)));
                    SPlanDate           = result  . getString(7);
                    String SMrsDate     = TDate   . toNormal();
                    VMRSDate            . addElement(SMrsDate);
                    VDueDate            . addElement(common.getDate(common.parseDate(SMrsDate),7,1));
                    V3Qty               . addElement(result.getString(11));
                    VDeptCode           . addElement("0");
                    VGroupCode          . addElement("0");

               }
               result    . close();
          
               for(int i=0;i<VCode.size();i++)
               {
                    int iId=0;
                    String QS2 = " Select Max(Id) from PurchaseOrder Where Item_Code='"+(String)VCode.elementAt(i)+"' And MillCode="+iMillCode;
                    ResultSet res = stat.executeQuery(QS2);
                    while(res.next())
                    {
                         iId = res.getInt(1);
                    }
                    res.close();

                    String QS1 = "";

                    if(iId>0)
                    {
                         QS1 = " Select Group_Code,Dept_Code From PurchaseOrder where id = "+iId;
                    }
                    else
                    {
                         QS1 = " Select Group_Code,Dept_Code From PYOrder where id = (select max(id) from pyorder Where Item_Code='"+(String)VCode.elementAt(i)+"' And MillCode="+iMillCode+") ";
                    }
                    
                    ResultSet res1 = stat.executeQuery(QS1);
                    while(res1.next())
                    {

                         String SGroupCode = common.parseNull(res1.getString(1));
                         String SDeptCode  = common.parseNull(res1.getString(2));

                         int iGIndex = common.indexOf(VCGroupCode,SGroupCode);

                         if(iGIndex>=0)
                         {
                              VGroupCode.setElementAt(SGroupCode,i);
                         }
                         else
                         {
                              VGroupCode.setElementAt("1",i);
                         }

                         int iDIndex = common.indexOf(VCDeptCode,SDeptCode);

                         if(iDIndex>=0)
                         {
                              VDeptCode.setElementAt(SDeptCode,i);
                         }
                         else
                         {
                              VDeptCode.setElementAt("9",i);
                         }
                    }
                    res1.close();
               }
               
               if(iMillCode==0)
               {
                    iMaxMRSNo = common.toInt(SPlanDate.substring(0,6)+"00");
                    stat      . execute("Update RegPlan Set Posted=1 Where substr(RegPlan.Plan_Date,5,2)='"+TMonth.getText()+"' And RegPlan.Plan_Date >='"+SStDate+"' And MillCode="+iMillCode);
               }
               else
               {
                    iMaxMRSNo = common.toInt(String.valueOf(iMillCode)+(SPlanDate.substring(0,6))+"00");
                    stat      . execute("Update RegPlan Set Posted=1 Where substr(RegPlan.Plan_Date,5,2)='"+TMonth.getText()+"' And RegPlan.Plan_Date >='"+SStDate+"' And MillCode="+iMillCode);
               }
               stat.close();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
          
     private void fillBlanks()
     {
          int iPGroup   = -1;
          int iMRSNo    = iMaxMRSNo;

          for(int i=0;i<VTempGroup.size();i++)
          {
               int iGroup = common.toInt((String)VTempGroup.elementAt(i));
               if(iGroup != iPGroup)
               {
                    iPGroup = iGroup;
                    iMRSNo++;
               }
               VMRSNo    . setElementAt(String.valueOf(iMRSNo),i);
          }
     }
          
     private void setData()
     {
          int       iSlNo          = 1;
          String    SCreationDate  = common.getServerDate();

          try
          {
               ORAConnection  oraConnection =  ORAConnection     . getORAConnection();
               Connection     theConnection =  oraConnection     . getConnection();
                              theConnection .  setAutoCommit(true);
               Statement      stat          =  theConnection     . createStatement();
     
               stat.execute(" Delete From MRS Where PlanMonth > 0 And PlanMonth="+TMonth.getText()+" And MrsDate >='"+SStDate+"' And MillCode="+iMillCode);

               String SPMrsNo = "";
               String SMrsNo  = "";

               String SUnitCode="";

               if(VCUnitCode.size()>0)
               {
                    SUnitCode = (String)VCUnitCode.elementAt(0);
               }
               else
               {
                    SUnitCode = "0";
               }

               for(int i=0;i<VMRSNo.size();i++)
               {
                    String    QS = "";
                    SMrsNo = (String)VMRSNo.elementAt(i);

                    if(!SPMrsNo.equals(SMrsNo))
                    {
                        SPMrsNo    = SMrsNo;
                        iSlNo      = 1;
                    }

                    if(!isStationary((String)VCode.elementAt(i)))
                    {
                         QS = "";
                         QS = " Insert Into MRS (id,MRSNo,MRSDate,Item_Code,Qty,Catl,Draw,DueDate,HodCode,RefNo,Unit_Code,Dept_Code,Group_Code,BlockCode,PlanMonth,UserCode,CreationDate,Status,flag,SlNo,MillCode) Values (";
                         QS = QS+"MRS_seq.nextval,";
                         QS = QS+(String)VMRSNo.elementAt(i)+",";
                         QS = QS+"'"+(String)VMRSDate.elementAt(i)+"',";
                         QS = QS+"'"+(String)VCode.elementAt(i)+"',";
                         QS = QS+"0"+getMaxQty(i)+",";
                         QS = QS+"'"+(String)VCatl.elementAt(i)+"',";
                         QS = QS+"'"+(String)VDraw.elementAt(i)+"',";
                         QS = QS+"'"+common.pureDate((String)VDueDate.elementAt(i))+"',";
                         QS = QS+"0"+(String)VHodCode.elementAt(i)+",";
                         QS = QS+"0"+(String)VRefNo.elementAt(i)+",";
                         QS = QS+"0"+SUnitCode+",";
                         QS = QS+"0"+common.toInt((String)VDeptCode.elementAt(i))+",";
                         QS = QS+"0"+common.toInt((String)VGroupCode.elementAt(i))+",";
                         QS = QS+"0"+",";
                         QS = QS+TMonth.getText()+",";
                         QS = QS+iUserCode+",";
                         QS = QS+"'"+SCreationDate+"',";
                         QS = QS+"0"+",";
                         QS = QS+"0"+",";
                         QS = QS+iSlNo+",";
                         QS = QS+iMillCode+")";
                    }
                    else
                    {
                         QS = "";
                         QS = " Insert Into MRS (id,MRSNo,MRSDate,Item_Code,Qty,Catl,Draw,DueDate,HodCode,RefNo,Unit_Code,Dept_Code,Group_Code,BlockCode,PlanMonth,UserCode,CreationDate,Status,flag,SlNo,MillCode,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE) Values (";
                         QS = QS+"MRS_seq.nextval,";
                         QS = QS+(String)VMRSNo.elementAt(i)+",";
                         QS = QS+"'"+(String)VMRSDate.elementAt(i)+"',";
                         QS = QS+"'"+(String)VCode.elementAt(i)+"',";
                         QS = QS+"0"+getMaxQty(i)+",";
                         QS = QS+"'"+(String)VCatl.elementAt(i)+"',";
                         QS = QS+"'"+(String)VDraw.elementAt(i)+"',";
                         QS = QS+"'"+common.pureDate((String)VDueDate.elementAt(i))+"',";
                         QS = QS+"0"+(String)VHodCode.elementAt(i)+",";
                         QS = QS+"0"+(String)VRefNo.elementAt(i)+",";
                         QS = QS+"0"+SUnitCode+",";
                         QS = QS+"0"+common.toInt((String)VDeptCode.elementAt(i))+",";
                         QS = QS+"0"+common.toInt((String)VGroupCode.elementAt(i))+",";
                         QS = QS+"0"+",";
                         QS = QS+TMonth.getText()+",";
                         QS = QS+iUserCode+",";
                         QS = QS+"'"+SCreationDate+"',";
                         QS = QS+"0"+",";
                         QS = QS+"0"+",";
                         QS = QS+iSlNo+",";
                         QS = QS+iMillCode+",";
                         QS = QS+"'"+SPColour+"',";
                         QS = QS+"'"+SPSet+"',";
                         QS = QS+"'"+SPSize+"',";
                         QS = QS+"'"+SPSide+"')";
                    }
                    stat.execute(QS);
                    iSlNo++;
               }
               stat.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     
     private String getMaxQty(int i)
     {
          double dQty    = common.toDouble((String)VQty.elementAt(i));
          double d3Qty   = common.toDouble((String)V3Qty.elementAt(i));
          double dMax    = Math.max(dQty,d3Qty);
          return common  . getRound(dMax,2);
     }

     private boolean isValidMonth()
     {
          int iCount = 0;
          try
          {
               ORAConnection  oraConnection =  ORAConnection     . getORAConnection();
               Connection     theConnection =  oraConnection     . getConnection();   
               Statement      stat          =  theConnection     . createStatement();
          
               String QS1 = "";
               
               QS1 = " Select Count(*) From MRS Where PlanMonth > 0 And PlanMonth="+TMonth.getText()+" And MrsDate >='"+SStDate+"' And MillCode="+iMillCode;

               System.out.println(QS1);
               
               ResultSet res1 = stat.executeQuery(QS1);
               while(res1.next())
               {
                    iCount = res1.getInt(1);
               }
               res1 . close();
               stat . close();
               
			/*
				-- Commented on 08.10.2020..
				
				if(iCount>0)
				{
					JOptionPane.showMessageDialog(null,"Mrs Already made for this month","Error",JOptionPane.ERROR_MESSAGE);
					return false;
				}
			*/
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return true;
     }

     private boolean isStationary(String SMatCode)
     {
          boolean   bFlag          = true;
          String    SStkGroupCode  = "";
          String    QS             = "";

          try
          {
               ORAConnection  oraConnection =  ORAConnection     . getORAConnection();
               Connection     theConnection =  oraConnection     . getConnection();   
               Statement      stat          =  theConnection     . createStatement();

               if(iMillCode==0)
               {
                    QS =    " select Invitems.stkgroupcode,INVITEMS.PAPERCOLOR,INVITEMS.PAPERSETS,INVITEMS.PAPERSIZE,INVITEMS.PAPERSIDE from invitems"+
                            " where invitems.item_code = '"+SMatCode+"'";
               }
               else
               {
                    QS =    " select "+SItemTable+".stkgroupcode,INVITEMS.PAPERCOLOR,INVITEMS.PAPERSETS,INVITEMS.PAPERSIZE,INVITEMS.PAPERSIDE from "+SItemTable+""+
                            " inner join invitems on invitems.item_code = "+SItemTable+".item_code"+
                            " where "+SItemTable+".item_code = '"+SMatCode+"'";
               }

               ResultSet result = stat.executeQuery(QS);

               while(result    . next())
               {
                    SStkGroupCode  = common.parseNull((String)result.getString(1));
                    SPColour       = common.parseNull((String)result.getString(2));
                    SPSet          = common.parseNull((String)result.getString(3));
                    SPSize         = common.parseNull((String)result.getString(4));
                    SPSide         = common.parseNull((String)result.getString(5));
               }
               result.close();
               stat.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

          if(SStkGroupCode.equals("B01"))
          {
               SPColour       = SPColour. toUpperCase();
               SPSet          = SPSet   . toUpperCase();
               SPSize         = SPSize  . toUpperCase();
               SPSide         = SPSide  . toUpperCase();

               return bFlag;
          }
          return false;
     }

     private void setVector()
     {
          VCDeptCode    = new Vector();
          VCGroupCode   = new Vector();
          VCUnitCode    = new Vector();
          
          String QS1 = "";
          String QS2 = "";
          String QS3 = "";
          
          QS1 = " Select Dept_Code From Dept Where Dept_Code>0 and (MillCode=2 or MillCode="+iMillCode+") Order By Dept_Code";

          QS2 = " Select Group_Code From Cata Where Group_Code>0 and (MillCode=2 or MillCode="+iMillCode+") Order By Group_Code";

          QS3 = " Select Unit_Code From Unit Where Unit_Code>0 and (MillCode=2 or MillCode="+iMillCode+") Order By Unit_Code";

          try
          {
               ORAConnection  oraConnection =  ORAConnection     . getORAConnection();
               Connection     theConnection =  oraConnection     . getConnection();
                              theConnection .  setAutoCommit(true);
               Statement      stat          =  theConnection     . createStatement();

               ResultSet result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VCDeptCode.addElement(result.getString(1));
               }
               result.close();

               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VCGroupCode.addElement(result.getString(1));
               }
               result.close();

               result = stat.executeQuery(QS3);
               while(result.next())
               {
                    VCUnitCode.addElement(result.getString(1));
               }
               result.close();
               stat.close();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }


}
