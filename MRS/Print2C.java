package MRS;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;


import util.*;
import jdbc.*;
import guiutil.*;

public class Print2C
{
     Vector VDeptCode,VDept;
     Vector VMRSNo,VDate,VCode,VName,VMake,VRemarks;
     Vector VCatl,VDraw,VUoM,VReqQty;
     Vector VBlock,VGroup;
     Vector VStock,VPending;
     Vector VRate,VDiscPer,VCenVatPer,VSTPer;
     Vector VValue,VSupplier,VPODate,VDueDate;
     
     Vector VPLCode,VPLPending,VPLRate,VPLStock;
     Vector VPLDate,VPLNetRate,VPLDiscPer,VPLCenVatPer,VPLStPer,VPLSupplier;
     
     Common common = new Common();
     
     int iLctr           = 100;
     int iPctr           = 0;
     double dDeptValue   = 0;
     double dGrandValue  = 0;
     
     String SHead1 =     "| "+common.Rad("MRS No",10)+" | "+common.Pad("MRS Date",10)+" | "+common.Pad("Material",10)+" | "+
                         common.Pad("Material Name",38)+" | "+common.Pad("Make",15)+" | "+common.Pad("Spl Info",15)+" | "+
                         common.Pad("Catl No",15)+" | "+common.Pad("Drg No",15)+" | "+common.Pad("UoM",7)+" | "+
                         common.Rad("Requested",8)+" | "+common.Rad("Stock",6)+" | "+
                         common.Rad("Pending",8)+" | "+ common.Pad("Due Date",10)+" | "+ 
                         common.Pad("Supplier",20)+" | "+
                         common.Rad("Rate",10)+" | "+common.Rad("Disc",5)+" | "+common.Rad("CenVat",5)+" | "+
                         common.Rad("ST",5)+" | "+common.Rad("MRS",10)+" | "+common.Rad("Order",10)+" | "+
                         common.Pad("Block",5)+" | "+common.Pad("Group",15)+" |";

     String SHead2 =     "| "+common.Rad(" ",10)+" | "+common.Pad(" ",10)+" | "+common.Pad("Code",10)+" | "+
                         common.Pad(" ",38)+" | "+common.Pad(" ",15)+" | "+common.Pad(" ",15)+" | "+
                         common.Pad(" ",15)+" | "+common.Pad(" ",15)+" | "+common.Pad(" ",7)+" | "+
                         common.Rad("Qty",8)+" | "+common.Rad("Qty",6)+" | "+
                         common.Rad("Qty",8)+" | "+common.Space(10)+" | "+
                         common.Pad("Name",20)+" | "+
                         common.Rad("Rs",10)+" | "+common.Rad("%",5)+" | "+common.Rad("%",5)+" | "+
                         common.Rad("%",5)+" | "+common.Rad("Value",10)+" | "+common.Rad("Date",10)+" | "+
                         common.Pad(" ",5)+" | "+common.Rad(" ",15)+" |";

     String SHead4 =     "| "+common.Rad(" ",10)+" | "+common.Pad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                         common.Pad(" ",38)+" | "+common.Pad(" ",15)+" | "+common.Pad(" ",15)+" | "+
                         common.Pad(" ",15)+" | "+common.Pad(" ",15)+" | "+common.Pad(" ",7)+" | "+
                         common.Rad("   ",8)+" | "+common.Rad("   ",6)+" | "+
                         common.Rad("   ",8)+" | "+common.Space(10)+" | "+
                         common.Pad("    ",20)+" | "+
                         common.Rad("  ",10)+" | "+common.Rad(" ",5)+" | "+common.Rad(" ",5)+" | "+
                         common.Rad(" ",5)+" | "+common.Rad("     ",10)+" | "+common.Rad("        ",10)+" | "+
                         common.Pad(" ",5)+" | "+common.Rad(" ",15)+" |";

     String SHead3 =     common.Replicate("-",SHead1.length());
     
     DateField      TStDate,TEnDate;
     JTextField     TFile;
     int            iMillCode;
     String         SMillName;
     String         SSupTable;
     
     FileWriter FW;
     
     Print2C(DateField TStDate,DateField TEnDate,JTextField TFile,int iMillCode,String SMillName,String SSupTable)
     {
          this.TStDate   = TStDate;
          this.TEnDate   = TEnDate;
          this.TFile     = TFile;
          this.iMillCode = iMillCode;
          this.SMillName = SMillName;
          this.SSupTable = SSupTable;
          
          String SFile = TFile.getText();

          if((SFile.trim()).length()==0)
               SFile = "1.prn";

          try
          {
               FW = new FileWriter(common.getPrintPath()+SFile);
               toPrint();
               FW.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               try
               {
                    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(common.getPrintPath()+"x.prn")));
                    ex.printStackTrace(out);
                    out.close();
               }
               catch(Exception e){}
          }
     }

     private void toPrint() throws Exception
     {
          iLctr=100;
          iPctr=0;
          dDeptValue=0;
          dGrandValue=0;
          
          setVectors();
          String SPDeptCode = "";
          String SDept="";
          for(int i=0;i<VDeptCode.size();i++)
          {
               setHead();
               String SDeptCode = (String)VDeptCode.elementAt(i);
               SDept            = (String)VDept.elementAt(i);
               if(!SDeptCode.equals(SPDeptCode))
               {
                    setDeptFoot(i);
                    setDeptHead(SDept);
                    SPDeptCode=SDeptCode;
                    dDeptValue=0;                      
               }
               setBody(i);
          }
          setDeptFoot(VDeptCode.size());
          setFoot();
     }
     
     private void setBody(int i) throws Exception
     {
          String str =   "| "+common.Rad((String)VMRSNo.elementAt(i),10)+" | "+
                         common.Pad(common.parseDate((String)VDate.elementAt(i)),10)+" | "+
                         common.Pad((String)VCode.elementAt(i),10)+" | "+
                         common.Pad((String)VName.elementAt(i),38)+" | "+
                         common.Pad((String)VMake.elementAt(i),15)+" | "+
                         common.Pad((String)VRemarks.elementAt(i),15)+" | "+
                         common.Pad((String)VCatl.elementAt(i),15)+" | "+
                         common.Pad((String)VDraw.elementAt(i),15)+" | "+
                         common.Pad((String)VUoM.elementAt(i),7)+" | "+
                         common.Rad((String)VReqQty.elementAt(i),8)+" | "+
                         common.Rad((String)VStock.elementAt(i),6)+" | "+
                         common.Rad((String)VPending.elementAt(i),8)+" | "+
                         common.Rad(common.parseDate((String)VDueDate.elementAt(i)),10)+" | "+
                         common.Pad((String)VSupplier.elementAt(i),20)+" | "+
                         common.Rad((String)VRate.elementAt(i),10)+" | "+
                         common.Rad((String)VDiscPer.elementAt(i),5)+" | "+
                         common.Rad((String)VCenVatPer.elementAt(i),5)+" | "+
                         common.Rad((String)VSTPer.elementAt(i),5)+" | "+
                         common.Rad((String)VValue.elementAt(i),10)+" | "+
                         common.Rad(common.parseDate((String)VPODate.elementAt(i)),10)+" | "+
                         common.Pad((String)VBlock.elementAt(i),5)+" | "+
                         common.Pad((String)VGroup.elementAt(i),15)+" |";
          
          
          FW.write(str+"\n");
          
          iLctr++;
          dDeptValue= dDeptValue+common.toDouble((String)VValue.elementAt(i));
          dGrandValue = dGrandValue+common.toDouble((String)VValue.elementAt(i));
     }
     
     private void setDeptFoot(int i) throws Exception
     {
          if(i==0)
               return;
          
          String str="";
          
          str =     "| "+common.Rad("",10)+" | "+
                    common.Pad("",10)+" | "+
                    common.Pad("",10)+" | "+
                    common.Pad("T O T A L",38)+" | "+
                    common.Pad("",15)+" | "+
                    common.Pad("",15)+" | "+
                    common.Pad("",15)+" | "+
                    common.Pad("",15)+" | "+
                    common.Pad("",7)+" | "+
                    common.Rad("",8)+" | "+
                    common.Rad("",6)+" | "+
                    common.Rad("",8)+" | "+
                    common.Rad("",10)+" | "+
                    common.Pad("",20)+" | "+
                    common.Rad("",10)+" | "+
                    common.Rad("",5)+" | "+
                    common.Rad("",5)+" | "+
                    common.Rad("",5)+" | "+
                    common.Rad(common.getRound(dDeptValue,2),10)+" | "+
                    common.Rad("",10)+" | "+
                    common.Pad("",5)+" | "+
                    common.Pad("",15)+" |";
          
          FW.write(SHead3+"\n");
          FW.write(str+"\n");
          FW.write(SHead3+"\n");
          iLctr=iLctr+3;
     }

     private void setFoot() throws Exception
     {
          String str="";
          
          str =     "| "+common.Rad("",10)+" | "+
                    common.Pad("",10)+" | "+
                    common.Pad("",10)+" | "+
                    common.Pad("G R A N DT O T A L",38)+" | "+
                    common.Pad("",15)+" | "+
                    common.Pad("",15)+" | "+
                    common.Pad("",15)+" | "+
                    common.Pad("",15)+" | "+
                    common.Pad("",7)+" | "+
                    common.Rad("",8)+" | "+
                    common.Rad("",6)+" | "+
                    common.Rad("",8)+" | "+
                    common.Rad("",10)+" | "+
                    common.Pad("",20)+" | "+
                    common.Rad("",10)+" | "+
                    common.Rad("",5)+" | "+
                    common.Rad("",5)+" | "+
                    common.Rad("",5)+" | "+
                    common.Rad(common.getRound(dGrandValue,2),10)+" | "+
                    common.Rad("",10)+" | "+
                    common.Pad("",5)+" | "+
                    common.Pad("",15)+" |";
          
          FW.write(SHead3+"\n");
          FW.write(str+"\n");
          FW.write(SHead3+"\n");
          iLctr=iLctr+3;
     }
     
     private void setDeptHead(String SDept) throws Exception
     {
          String str="";
          str =     "| "+common.Rad("",10)+" | "+
                    common.Pad("",10)+" | "+
                    common.Pad("",10)+" | "+
                    common.Pad(SDept,38)+" | "+
                    common.Pad("",15)+" | "+
                    common.Pad("",15)+" | "+
                    common.Pad("",15)+" | "+
                    common.Pad("",15)+" | "+
                    common.Pad("",7)+" | "+
                    common.Rad("",8)+" | "+
                    common.Rad("",6)+" | "+
                    common.Rad("",8)+" | "+
                    common.Rad("",10)+" | "+
                    common.Pad("",20)+" | "+
                    common.Rad("",10)+" | "+
                    common.Rad("",5)+" | "+
                    common.Rad("",5)+" | "+
                    common.Rad("",5)+" | "+
                    common.Rad("",10)+" | "+
                    common.Rad("",10)+" | "+
                    common.Pad("",5)+" | "+
                    common.Pad("",15)+" |";
                    
                    FW.write(str+"\n");
                    FW.write(SHead4+"\n");
                    iLctr=iLctr+2;
     }
     
     private void setHead() throws Exception
     {
          if(iLctr < 63)
               return ;
          
          if(iPctr > 0)
               FW.write(SHead3+"\n");
          
          iPctr++;
          
          String str1 = "";

          str1 = "gCompany  : "+SMillName;
          
          String str2 = "Document : MRS CheckList For the Period "+TStDate.toString()+"-"+TEnDate.toString()+" - 2(C)";
          String str3 = "Page     : "+iPctr+"";
          
          FW.write(str1+"\n");                  
          FW.write(str2+"\n");
          FW.write(str3+"\n");
          FW.write(SHead3+"\n");
          FW.write(SHead1+"\n");            
          FW.write(SHead2+"\n");
          FW.write(SHead3+"\n");
          
          iLctr = 7;
     }
     
     private void setVectors()
     {
          String SStDate = TStDate.toNormal();
          String SEnDate = TEnDate.toNormal();
          
          VDeptCode   = new Vector();
          VDept       = new Vector();
          VMRSNo      = new Vector();
          VDate       = new Vector();
          VCode       = new Vector();
          VName       = new Vector();
          VMake       = new Vector();
          VRemarks    = new Vector();
          VCatl       = new Vector();
          VDraw       = new Vector();
          VUoM        = new Vector();
          VReqQty     = new Vector();
          VDueDate    = new Vector();
          VPODate     = new Vector();
          VStock      = new Vector();
          VPending    = new Vector();
          VRate       = new Vector();
          VDiscPer    = new Vector();
          VCenVatPer  = new Vector();
          VSTPer      = new Vector();
          VValue      = new Vector();
          VSupplier   = new Vector();
          VBlock      = new Vector();
          VGroup      = new Vector();
          
          VPLDate     = new Vector();   
          VPLCode     = new Vector();
          VPLPending  = new Vector();
          VPLNetRate  = new Vector();
          VPLRate     = new Vector();
          VPLStock    = new Vector();
          VPLDiscPer  = new Vector();
          VPLCenVatPer= new Vector();
          VPLStPer    = new Vector();
          VPLSupplier = new Vector();

            String QS0 = " Create table Temp0 "+
                         " as (Select Distinct Item_Code as Code From MRS "+
                         " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MRS.MRSDate >='"+SStDate+"' And MRS.MRSDate <='"+SEnDate+"' "+
                         " and MRS.MillCode="+iMillCode+")";

            String QS1 = " Create table Temp1 "+
                         " as (SELECT Issue.Code, Sum(Issue.Qty) AS ConQty "+
                         " FROM Issue INNER JOIN Temp0 ON Temp0.Code = Issue.Code "+
                         " Where Issue.MillCode = "+iMillCode+
                         " GROUP BY Issue.Code) ";

            String QS2 = "  Create table Temp2 "+
                         " as (SELECT GRN.Code, Sum(GRN.Qty) AS RecQty "+
                         " FROM GRN INNER JOIN Temp0 ON GRN.Code=Temp0.Code "+
                         " Where GRN.GrnBlock < 2 "+
                         " and Grn.MillCode = "+iMillCode+
                         " GROUP BY GRN.Code) ";

            String QS3 = "  Create table Temp3 "+
                         " as (SELECT PurchaseOrder.Item_Code, Sum(PurchaseOrder.Qty-PurchaseOrder.InvQty) AS Pending "+
                         " FROM  PurchaseOrder INNER JOIN Temp0 ON Temp0.Code=PurchaseOrder.Item_Code "+
                         " Where PurchaseOrder.MillCode = "+iMillCode+
                         " GROUP BY PurchaseOrder.Item_Code) ";

            String QS4a = " create table TempMRSOrder as ("+
                          " Select PYOrder.OrderDate,PYOrder.Item_Code, "+
                          " "+SSupTable+".Name,PYOrder.Rate,PYOrder.DiscPer, "+
                          " PYOrder.CenVatPer,PYOrder.TaxPer,PYOrder.SurPer, "+
                          " PYOrder.Net/PYOrder.Qty as NetRate "+
                          " From PYOrder Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = PyOrder.Sup_Code "+
                          " Where PYOrder.Qty > 0 And PYOrder.Net > 0 "+
                          " Union All "+
                          " Select PurchaseOrder.OrderDate,PurchaseOrder.Item_Code, "+
                          " "+SSupTable+".Name,PurchaseOrder.Rate,PurchaseOrder.DiscPer, "+
                          " PurchaseOrder.CenVatPer,PurchaseOrder.TaxPer,PurchaseOrder.SurPer, " +
                          " PurchaseOrder.Net/PurchaseOrder.Qty as NetRate"+
                          " From PurchaseOrder Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = PurchaseOrder.Sup_Code "+
                          " Where PurchaseOrder.Qty > 0 And PurchaseOrder.Net > 0) ";

            String QS4 = " create table Temp4 as ("+
                         " SELECT Temp0.Code,TempMRSOrder.OrderDate, "+
                         " TempMRSOrder.Rate,TempMRSOrder.DiscPer, "+
                         " TempMRSOrder.CenVatPer,TempMRSOrder.TaxPer, "+
                         " TempMRSOrder.NetRate,TempMRSOrder.Name "+
                         " FROM Temp0 INNER JOIN TempMRSOrder ON Temp0.Code = TempMRSOrder.Item_Code )";


            String QS6 = " SELECT Code,OrderDate, "+
                         " Rate,DiscPer, CenVatPer,TaxPer, "+
                         " NetRate,Name FROM Temp4 Order By 1,2 ";

            String QS5 = " SELECT Temp0.Code,Temp1.ConQty, Temp2.RecQty, "+
                         " Temp3.Pending, InvItems.OPGQTY "+
                         " FROM (((Temp0 LEFT JOIN Temp1 ON Temp0.Code = Temp1.Code) "+
                         " LEFT JOIN Temp2 ON Temp0.Code = Temp2.Code) "+
                         " LEFT JOIN Temp3 ON Temp0.Code = Temp3.Item_Code) "+
                         " LEFT JOIN InvItems ON Temp0.Code = InvItems.Item_Code ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               try{stat.execute("Drop Table Temp0");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp1");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp2");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp3");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp4");}catch(Exception ex){}
               try{stat.execute("Drop Table TempMRSOrder");}catch(Exception ex){}
          
               stat.execute(QS0);
               stat.execute(QS1);
               stat.execute(QS2);
               stat.execute(QS3);
               stat.execute(QS4a);
               stat.execute(QS4);

               ResultSet result    = stat.executeQuery(QS5);
               while(result.next())
               {
                    double dStock   = common.toDouble(result.getString(5))+common.toDouble(result.getString(3))-common.toDouble(result.getString(2));
                    VPLCode         .addElement(result.getString(1)); 
                    VPLPending      .addElement(result.getString(4));
                    VPLStock        .addElement(common.getRound(dStock,2));
                    VPLNetRate      .addElement("0");
                    VPLRate         .addElement("0");
                    VPLDiscPer      .addElement("0");
                    VPLCenVatPer    .addElement("0");
                    VPLStPer        .addElement("0");
                    VPLSupplier     .addElement("");
                    VPLDate         .addElement("");
               }
               String SPCode="";
               ResultSet resultx = stat.executeQuery(QS6);
               while(resultx.next())
               {
                    String SCode   = resultx.getString(1);
                    int index      = VPLCode.indexOf(SCode);
                    if(index == -1)
                         continue;
                         
                    VPLNetRate     .setElementAt(resultx.getString(7),index);
                    VPLRate        .setElementAt(resultx.getString(3),index);
                    VPLDiscPer     .setElementAt(resultx.getString(4),index);
                    VPLCenVatPer   .setElementAt(resultx.getString(5),index);
                    VPLStPer       .setElementAt(resultx.getString(6),index);
                    VPLSupplier    .setElementAt(resultx.getString(8),index);
                    VPLDate        .setElementAt(resultx.getString(2),index);
               }
               
               ResultSet theResult = stat.executeQuery(getQS());
               while(theResult.next())
               {
                    VDeptCode   .addElement(theResult.getString(1));
                    VDept       .addElement(theResult.getString(2));
                    VMRSNo      .addElement(theResult.getString(3));
                    VDate       .addElement(theResult.getString(4));
                    VCode       .addElement(theResult.getString(5));
                    VName       .addElement(theResult.getString(6));
                    VMake       .addElement(theResult.getString(7));
                    VRemarks    .addElement(theResult.getString(8));
                    VCatl       .addElement(theResult.getString(9));
                    VDraw       .addElement(theResult.getString(10));
                    VUoM        .addElement(theResult.getString(11));
                    VReqQty     .addElement(theResult.getString(12));
                    VDueDate    .addElement(theResult.getString(13));
                    VBlock      .addElement(theResult.getString(14));
                    VGroup      .addElement(theResult.getString(15));
               }                                                    
               setValue();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void setValue()
     {
          for(int i=0;i<VCode.size();i++)
          {
               String SCode   = (String)VCode.elementAt(i);
               String SReqQty = (String)VReqQty.elementAt(i);
               
               int index = VPLCode.indexOf(SCode);
               
               String SNetRate = "0.00";
               String SStock   = "0.00";
               String SPending = "0.00";
               String SRate    = "0.00";
               String SDiscPer = "0.00";
               String SVatPer  = "0.00";
               String STaxPer  = "0.00";
               String SSupName = "";
               String SDate    = "";
               double dValue   = 0;
               
               if(index>-1)
               { 
                    SNetRate = (String)VPLNetRate  .elementAt(index);
                    SStock   = (String)VPLStock    .elementAt(index);
                    SPending = (String)VPLPending  .elementAt(index);
                    SRate    = (String)VPLRate     .elementAt(index);
                    SDiscPer = (String)VPLDiscPer  .elementAt(index);
                    SVatPer  = (String)VPLCenVatPer.elementAt(index);
                    STaxPer  = (String)VPLStPer    .elementAt(index);
                    SSupName = (String)VPLSupplier .elementAt(index);
                    SDate    = (String)VPLDate     .elementAt(index);
                    dValue   = common.toDouble(SNetRate)*common.toDouble(SReqQty);
               }
               
               VStock      .addElement(SStock);
               VPending    .addElement(SPending);
               VRate       .addElement(SRate);
               VDiscPer    .addElement(SDiscPer);
               VCenVatPer  .addElement(SVatPer);
               VSTPer      .addElement(STaxPer);
               VValue      .addElement(common.getRound(dValue,2));
               VSupplier   .addElement(SSupName);
               VPODate     .addElement(SDate);
          }
     }
     
     private String getQS()
     {
          String QS = " SELECT MRS.Dept_Code, Dept.Dept_Name, "+
                      " MRS.MrsNo, MRS.MrsDate, "+
                      " MRS.Item_Code, InvItems.ITEM_NAME, "+
                      " MRS.Make, MRS.Remarks, "+
                      " InvItems.Catl, InvItems.Draw, "+
                      " UoM.UOMName, MRS.Qty, MRS.DueDate, "+
                      " OrdBlock.BlockName,cata.group_name "+
                      " FROM ((((MRS INNER JOIN InvItems ON "+
                      " MRS.Item_Code=InvItems.ITEM_CODE) "+
                      " INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) "+
                      " INNER JOIN UoM ON UoM.UoMCode=InvItems.UoMCode) "+
                      " INNER JOIN OrdBlock on MRS.BlockCode = OrdBlock.Block) "+
                      " Inner Join Cata on mrs.Group_Code = Cata.Group_Code "+
                      " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) "+
                      " And MRS.MRSDate >='"+TStDate.toNormal()+"' "+
                      " And MRS.MRSDate <= '"+TEnDate.toNormal()+"' "+
                      " and MRS.MillCode ="+iMillCode+
                      " Order By 1,3,2,5 ";

          return QS;
     }
}
