package MRS;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class Print2e
{
     Vector    VDeptCode,VDept;
     Vector    VMRSNo,VDate,VCode,VName,VMake,VRemarks;
     Vector    VCatl,VDraw,VUoM,VReqQty;
     Vector    VStock,VPending,VDueDate;
     Vector    VNature,VBlock;
     Vector    VValue,VNetValue;
     Vector    VEnqNo,VEnqDate,VEnqDueDate;
     
     Vector    VPLCode,VPLPending,VPLRate,VPLStock;
     Vector    VPLDate,VPLNetRate,VPLDiscPer,VPLCenVatPer,VPLStPer,VPLSupplier;
     
     Vector    VPLEnqNo,VPLEnqDate,VPLEnqDueDate,VPLEnqCode;
     
     Common    common         = new Common();
     
     int       iLctr          = 100;
     int       iPctr          = 0;
     
     double    dDeptValue     = 0;
     double    dGrandValue    = 0;
     double    dDeptNetValue  = 0;
     double    dGrandNetValue = 0;
     
     String SHead1 =     "| "+common.Rad("MRS No",5)+" | "+common.Pad("MRS Date",10)+" | "+common.Pad("Material",10)+" | "+
                         common.Pad("Material Name",35)+" | "+common.Pad("Make",10)+" | "+common.Pad("Spl Info",10)+" | "+
                         common.Pad("Catl No",15)+" | "+common.Pad("Drg No",15)+" | "+common.Pad("UoM",7)+" | "+
                         common.Rad("Requested",8)+" | "+common.Rad("Stock",8)+" | "+
                         common.Rad("Pending",8)+" | "+ common.Pad("Due Date",10)+" | "+ 
                         common.Pad("Nature of ",20)+" | "+
                         common.Pad("Stock",5)+" | "+
                         common.Cad("Value",10+3+10)+" | "+
                         common.Cad("Enquiry",5+3+10+3+10)+" | "+
                         common.Cad("Delay Days",5+3+5)+" | "+
                         common.Pad("Remarks",10)+" |";

     String SHead2 =     "| "+common.Rad(" ",5)+" | "+common.Pad(" ",10)+" | "+common.Pad("Code",10)+" | "+
                         common.Pad(" ",35)+" | "+common.Pad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                         common.Pad(" ",15)+" | "+common.Pad(" ",15)+" | "+common.Pad(" ",7)+" | "+
                         common.Rad("Qty",8)+" | "+common.Rad("Qty",8)+" | "+
                         common.Rad("Qty",8)+" | "+common.Space(10)+" | "+
                         common.Pad("Material ",20)+" | "+
                         common.Pad("Block",5)+" |-"+
                         common.Replicate("-",10+3+10)+"-|-"+
                         common.Replicate("-",5+3+10+3+10)+"-|-"+
                         common.Replicate("-",5+3+5)+"-| "+
                         common.Space(10)+" |";

     String SHead4 =     "| "+common.Rad(" ",5)+" | "+common.Pad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                         common.Pad(" ",35)+" | "+common.Pad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                         common.Pad(" ",15)+" | "+common.Pad(" ",15)+" | "+common.Pad(" ",7)+" | "+
                         common.Rad("   ",8)+" | "+common.Rad("   ",8)+" | "+
                         common.Rad("   ",8)+" | "+common.Space(10)+" | "+
                         common.Pad(" ",20)+" | "+
                         common.Pad(" ",5)+" | "+
                         common.Rad("With CenVat",10)+" | "+
                         common.Rad("W.O CenVat",10)+" | "+
                         common.Rad("No",5)+" | "+
                         common.Pad("Date",10)+" | "+
                         common.Pad("Due Date",10)+" | "+
                         common.Rad("MRS",5)+" | "+
                         common.Rad("Enq",5)+" | "+
                         common.Space(10)+" |";
     
     String SHead5 =     "|-"+common.Replicate("-",5)+"-|-"+common.Replicate("-",10)+"-|-"+common.Replicate("-",10)+"-|-"+
                         common.Replicate("-",35)+"-|-"+common.Replicate("-",10)+"-|-"+common.Replicate("-",10)+"-|-"+
                         common.Replicate("-",15)+"-|-"+common.Replicate("-",15)+"-|-"+common.Replicate("-",7)+"-|-"+
                         common.Replicate("-",8)+"-|-"+common.Replicate("-",8)+"-|-"+
                         common.Replicate("-",8)+"-|-"+common.Replicate("-",10)+"-|-"+
                         common.Replicate("-",20)+"-|-"+
                         common.Replicate("-",5)+"-|-"+
                         common.Replicate("-",10)+"-|-"+
                         common.Replicate("-",10)+"-|-"+
                         common.Replicate("-",5)+"-|-"+
                         common.Replicate("-",10)+"-|-"+
                         common.Replicate("-",10)+"-|-"+
                         common.Replicate("-",5)+"-|-"+
                         common.Replicate("-",5)+"-|-"+
                         common.Replicate("-",10)+"-|";
     
     String SHead6 =     "| "+common.Rad(" ",5)+" | "+common.Pad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                         common.Pad(" ",35)+" | "+common.Pad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                         common.Pad(" ",15)+" | "+common.Pad(" ",15)+" | "+common.Pad(" ",7)+" | "+
                         common.Rad("   ",8)+" | "+common.Rad("   ",8)+" | "+
                         common.Rad("   ",8)+" | "+common.Space(10)+" | "+
                         common.Pad(" ",20)+" | "+
                         common.Pad(" ",5)+" | "+
                         common.Rad(" ",10)+" | "+
                         common.Rad(" ",10)+" | "+
                         common.Rad(" ",5)+" | "+
                         common.Pad(" ",10)+" | "+
                         common.Pad(" ",10)+" | "+
                         common.Rad(" ",5)+" | "+
                         common.Rad(" ",5)+" | "+
                         common.Space(10)+" |";
     
     String         SHead7 = common.Replicate("-",SHead1.length());
     
     DateField      TEnDate;
     DateField      df;
     JTextField     TFile;
     
     FileWriter     FW;
     int            iMillCode;
     String         SItemTable,SSupTable,SMillName;
     
     Print2e(DateField TEnDate,JTextField TFile,int iMillCode,String SItemTable,String SSupTable,String SMillName)
     {
          this.TEnDate    = TEnDate;
          this.TFile      = TFile;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;
          
          df           = new DateField();
          
          df.setTodayDate();
          
          String SFile = TFile.getText();

          if((SFile.trim()).length()==0)
               SFile = "1.prn";

          try
          {
               FW = new FileWriter(common.getPrintPath()+SFile);
               toPrint();
               FW.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               try
               {
                    PrintWriter    out  = new PrintWriter(new BufferedWriter(new FileWriter(common.getPrintPath()+"x.prn")));
                                   ex   . printStackTrace(out);
                    out.close();
               }
               catch(Exception e){}
          }
     }

     private void toPrint() throws Exception
     {
          iLctr          = 100;
          iPctr          = 0;
          
          dDeptValue     = 0;
          dGrandValue    = 0;
          dDeptNetValue  = 0;
          dGrandNetValue = 0;

          setVectors();
          String SPDeptCode = "";
          String SDept="";
          for(int i=0;i<VDeptCode.size();i++)
          {
               setHead();
               String    SDeptCode = (String)VDeptCode.elementAt(i);
                         SDept     = (String)VDept.elementAt(i);
               if(!SDeptCode.equals(SPDeptCode))
               {
                    setDeptFoot(i);
                    setDeptHead(SDept);
                    SPDeptCode=SDeptCode;
                    dDeptValue=0;
                    dDeptNetValue=0;
               }
               setBody(i);
          }
          setDeptFoot(VDeptCode.size());
          setFoot();
     }

     private void setBody(int i) throws Exception
     {
          String SMRSDay = common.getDateDiff(df.toString(),common.parseDate((String)VDueDate.elementAt(i)));
          String SEnqDay = common.getDateDiff(df.toString(),common.parseDate((String)VEnqDueDate.elementAt(i)));

          String str =   "| "+common.Rad((String)VMRSNo.elementAt(i),5)+" | "+
                         common.Pad(common.parseDate((String)VDate.elementAt(i)),10)+" | "+
                         common.Pad((String)VCode.elementAt(i),10)+" | "+
                         common.Pad((String)VName.elementAt(i),35)+" | "+
                         common.Pad((String)VMake.elementAt(i),10)+" | "+
                         common.Pad((String)VRemarks.elementAt(i),10)+" | "+
                         common.Pad((String)VCatl.elementAt(i),15)+" | "+
                         common.Pad((String)VDraw.elementAt(i),15)+" | "+
                         common.Pad((String)VUoM.elementAt(i),7)+" | "+
                         common.Rad((String)VReqQty.elementAt(i),8)+" | "+
                         common.Rad((String)VStock.elementAt(i),8)+" | "+
                         common.Rad((String)VPending.elementAt(i),8)+" | "+
                         common.Rad(common.parseDate((String)VDueDate.elementAt(i)),10)+" | "+
                         common.Pad((String)VNature.elementAt(i),20)+" | "+
                         common.Pad((String)VBlock.elementAt(i),5)+" | "+
                         common.Pad(common.getRound((String)VValue.elementAt(i),0),10)+" | "+
                         common.Pad(common.getRound((String)VNetValue.elementAt(i),0),10)+" | "+
                         common.Pad((String)VEnqNo.elementAt(i),5)+" | "+
                         common.Pad(common.parseDate((String)VEnqDate.elementAt(i)),10)+" | "+
                         common.Pad(common.parseDate((String)VEnqDueDate.elementAt(i)),10)+" | "+
                         common.Rad(SMRSDay,5)+" | "+
                         common.Rad(SEnqDay,5)+" | "+
                         common.Space(10)+" |";
          
          FW.write(str+"\n");
          
          iLctr++;
          dDeptValue     = dDeptValue+common.toDouble((String)VValue.elementAt(i));
          dGrandValue    = dGrandValue+common.toDouble((String)VValue.elementAt(i));
          dDeptNetValue  = dDeptValue+common.toDouble((String)VNetValue.elementAt(i));
          dGrandNetValue = dGrandValue+common.toDouble((String)VNetValue.elementAt(i));
     }

     private void setDeptFoot(int i) throws Exception
     {
          if(i==0)
               return;

          String str =   "| "+common.Rad("",5)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("DEPARTMENT TOTAL",35)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("",15)+" | "+
                         common.Pad("",15)+" | "+
                         common.Pad("",7)+" | "+
                         common.Rad("",8)+" | "+
                         common.Rad("",8)+" | "+
                         common.Rad("",8)+" | "+
                         common.Rad("",10)+" | "+
                         common.Pad("",20)+" | "+
                         common.Pad("",5)+" | "+
                         common.Pad(common.getRound(dDeptValue,0),10)+" | "+
                         common.Pad(common.getRound(dDeptNetValue,0),10)+" | "+
                         common.Pad("",5)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("",10)+" | "+
                         common.Space(5)+" | "+
                         common.Space(5)+" | "+
                         common.Space(10)+" |";

          FW.write(SHead5+"\n");
          FW.write(str+"\n");
          FW.write(SHead5+"\n");
          iLctr=iLctr+3;
     }

     private void setFoot() throws Exception
     {
          String str =   "| "+common.Rad("",5)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("GRAND TOTAL",35)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("",15)+" | "+
                         common.Pad("",15)+" | "+
                         common.Pad("",7)+" | "+
                         common.Rad("",8)+" | "+
                         common.Rad("",8)+" | "+
                         common.Rad("",8)+" | "+
                         common.Rad("",10)+" | "+
                         common.Pad("",20)+" | "+
                         common.Pad("",5)+" | "+
                         common.Pad(common.getRound(dGrandValue,0),10)+" | "+
                         common.Pad(common.getRound(dGrandNetValue,0),10)+" | "+
                         common.Pad("",5)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("",10)+" | "+
                         common.Space(5)+" | "+
                         common.Space(5)+" | "+
                         common.Space(10)+" |";
                         
          FW.write(SHead5+"\n");
          FW.write(str+"\n");
          FW.write(SHead5+"\n");
          iLctr=iLctr+3;
     }

     private void setDeptHead(String SDept) throws Exception
     {
          String  str =  "| "+common.Rad("",5)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad(SDept,35)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("",15)+" | "+
                         common.Pad("",15)+" | "+
                         common.Pad("",7)+" | "+
                         common.Rad("",8)+" | "+
                         common.Rad("",8)+" | "+
                         common.Rad("",8)+" | "+
                         common.Rad("",10)+" | "+
                         common.Pad("",20)+" | "+
                         common.Pad("",5)+" | "+
                         common.Pad(common.getRound(dGrandValue,0),10)+" | "+
                         common.Pad(common.getRound(dGrandNetValue,0),10)+" | "+
                         common.Pad("",5)+" | "+
                         common.Pad("",10)+" | "+
                         common.Pad("",10)+" | "+
                         common.Space(5)+" | "+
                         common.Space(5)+" | "+
                         common.Space(10)+" |";
               
                    FW.write(SHead6+"\n");
                    FW.write(str+"\n");
                    FW.write(SHead6+"\n");
               
                    iLctr=iLctr+3;
     }

     private void setHead() throws Exception
     {
          if(iLctr < 63)
               return;
     
          if(iPctr > 0)
               FW.write(SHead7+"\n");
     
          iPctr++;
     
          String str1 = "gCompany  : "+SMillName;
     
          String str2 = "Document : MRS Pending List as on "+TEnDate.toString()+" - 2(C)";
          String str3 = "Page     : "+iPctr+"";
     
          FW.write(str1+"\n");                  
          FW.write(str2+"\n");
          FW.write(str3+"\n");
          FW.write(SHead7+"\n");
          FW.write(SHead1+"\n");            
          FW.write(SHead2+"\n");
          FW.write(SHead4+"\n");
          FW.write(SHead5+"\n");
          iLctr = 9;
     }

     private void setVectors()
     {
          String SEnDate = TEnDate.toNormal();
          
          VDeptCode   = new Vector();
          VDept       = new Vector();
          VMRSNo      = new Vector();
          VDate       = new Vector();
          VCode       = new Vector();
          VName       = new Vector();
          VMake       = new Vector();
          VRemarks    = new Vector();
          VCatl       = new Vector();
          VDraw       = new Vector();
          VUoM        = new Vector();
          VReqQty     = new Vector();
          VDueDate    = new Vector();
          VStock      = new Vector();
          VPending    = new Vector();
          VNature     = new Vector();
          VBlock      = new Vector();
          VValue      = new Vector();
          VNetValue   = new Vector();
          VEnqNo      = new Vector();
          VEnqDate    = new Vector();
          VEnqDueDate = new Vector();
          
          VPLDate     = new Vector();   
          VPLCode     = new Vector();
          VPLPending  = new Vector();
          VPLNetRate  = new Vector();
          VPLRate     = new Vector();
          VPLStock    = new Vector();
          VPLDiscPer  = new Vector();
          VPLCenVatPer= new Vector();
          VPLStPer    = new Vector();
          VPLSupplier = new Vector();
          
          VPLEnqNo      = new Vector();
          VPLEnqDate    = new Vector();
          VPLEnqDueDate = new Vector();
          VPLEnqCode    = new Vector();
          
          String SPCode="";
          String SCode="",SNetRate="",SRate="";
          String SDiscPer="",SVatPer="",STaxPer="";
          String SName="",SDate="";
          
          Vector VECode    = new Vector();
          Vector VENetRate = new Vector();
          Vector VERate    = new Vector();
          Vector VEDiscPer = new Vector();
          Vector VEVatPer  = new Vector();
          Vector VETaxPer  = new Vector();
          Vector VEName    = new Vector();
          Vector VEDate    = new Vector();

          String QS0 =   " create table Temp0 "+
                         " AS (Select Distinct Item_Code as Code From MRS "+
                         " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MRS.Qty > 0 And MRS.OrderNo = 0 And MRS.MRSDate <='"+SEnDate+"' "+
                         " and MRS.millCode = "+iMillCode+") ";

          String QS1 =   " create table Temp1 "+
                         " AS (SELECT Issue.Code, Sum(Issue.Qty) AS ConQty "+
                         " FROM Issue INNER JOIN Temp0 ON Temp0.Code = Issue.Code "+
                         " Where Issue.MillCode = "+iMillCode+
                         " GROUP BY Issue.Code)";

          String QS2 =   " create table Temp2 "+
                         " AS (SELECT GRN.Code, Sum(GRN.GrnQty) AS RecQty "+
                         " FROM GRN INNER JOIN Temp0 ON GRN.Code=Temp0.Code "+
                         " Where GRN.GrnBlock < 2 "+
                         " And GRN.MillCode = "+iMillCode+
                         " GROUP BY GRN.Code) ";

          String QS3 =   " create table Temp3 "+
                         " AS (SELECT PurchaseOrder.Item_Code, Sum(PurchaseOrder.Qty-PurchaseOrder.InvQty) AS Pending "+
                         " FROM  PurchaseOrder INNER JOIN Temp0 ON Temp0.Code=PurchaseOrder.Item_Code "+
                         " Where PurchaseOrder.MillCode = "+iMillCode+
                         " GROUP BY PurchaseOrder.Item_Code) ";

          String QS4a  = " SELECT Temp0.Code, PYOrder.Net/PYOrder.Qty, "+
                         " PYOrder.Rate, PYOrder.DiscPer, "+
                         " PYOrder.CenVatPer, PYOrder.TaxPer, "+
                         " "+SSupTable+".Name, PYOrder.OrderDate "+
                         " FROM (Temp0 INNER JOIN PYOrder ON Temp0.Code = PYOrder.Item_Code) Inner Join "+SSupTable+" On PYOrder.Sup_Code = "+SSupTable+".Ac_Code "+
                         " Where PYOrder.Net > 0 And PYOrder.Qty > 0 and PYOrder.MillCode="+iMillCode+
                         " Union All "+
                         " SELECT Temp0.Code, PurchaseOrder.Net/PurchaseOrder.Qty, "+
                         " PurchaseOrder.Rate, PurchaseOrder.DiscPer, "+
                         " PurchaseOrder.CenVatPer, PurchaseOrder.TaxPer, "+
                         " "+SSupTable+".Name,PurchaseOrder.OrderDate "+
                         " FROM (Temp0 INNER JOIN PurchaseOrder ON Temp0.Code = PurchaseOrder.Item_Code) Inner Join "+SSupTable+" On PurchaseOrder.Sup_Code = "+SSupTable+".Ac_Code "+
                         " Where PurchaseOrder.Net > 0 And PurchaseOrder.Qty > 0 and PurchaseOrder.MillCode="+iMillCode+
                         " Order BY 1,8 ";

          String QS4   = " create table Temp4  AS SELECT Temp0.Code,"+
                         " TempMRSOrder.NetRate AS NetRate,"+
                         " TempMRSOrder.Rate as Rate,"+
                         " TempMRSOrder.DiscPer as DiscPer,"+
                         " TempMRSOrder.CenVatPer as CenVatPer,"+
                         " TempMRSOrder.TaxPer as TaxPer,"+
                         " TempMRSOrder.Name as Name,"+
                         " TempMRSOrder.OrderDate as OrderDate"+
                         " FROM TempMRSOrder"+
                         " INNER JOIN  Temp0 ON Temp0.Code = TempMRSOrder.Item_Code and"+
                         " TempMRSOrder.Rowid in"+
                         " (select id from (select max(TempMRSOrder.Rowid) as id,t.item_code from TempMRSOrder"+
                         " inner join (select max(orderdate),item_code from TempMRSOrder"+
                         " where item_code in (select code from temp0) group by item_code) t"+
                         " on TempMRSOrder.item_code=t.item_code group by t.item_code))";


          String QS5   = " SELECT Temp0.Code,"+
                         " Temp1.ConQty, Temp2.RecQty, Temp3.Pending, "+
                         " Temp4.NetRate,"+SItemTable+".OPGQTY,Temp4.Rate,Temp4.DiscPer,Temp4.CenVatPer,Temp4.TaxPer,Temp4.Name,Temp4.OrderDate "+
                         " FROM ((((Temp0 LEFT JOIN Temp1 ON Temp0.Code = Temp1.Code) "+
                         " LEFT JOIN Temp2 ON Temp0.Code = Temp2.Code) "+
                         " LEFT JOIN Temp3 ON Temp0.Code = Temp3.Item_Code) "+
                         " LEFT JOIN Temp4 ON Temp0.Code = Temp4.Code) "+
                         " LEFT JOIN "+SItemTable+" ON Temp0.Code = "+SItemTable+".Item_Code ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
          
               try{stat.execute("Drop Table Temp0");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp1");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp2");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp3");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp4");}catch(Exception ex){}
               try{stat.execute("Delete From TempMRSOrder");}catch(Exception ex){}
          
               stat.execute(QS0);
               stat.execute(QS1);
               stat.execute(QS2);
               stat.execute(QS3);
          
               ResultSet res = stat.executeQuery(QS4a);

               while(res.next())
               {
                    SCode    = res.getString(1);
                    if(!SCode.equals(SPCode))
                    {
                         VECode    . addElement(SPCode);
                         VENetRate . addElement(SNetRate);
                         VERate    . addElement(SRate);
                         VEDiscPer . addElement(SDiscPer);
                         VEVatPer  . addElement(SVatPer);
                         VETaxPer  . addElement(STaxPer);
                         VEName    . addElement(SName);
                         VEDate    . addElement(SDate);
                         SPCode    = SCode;
                    }
                    SNetRate = res.getString(2);
                    SRate    = res.getString(3);
                    SDiscPer = res.getString(4);
                    SVatPer  = res.getString(5);
                    STaxPer  = res.getString(6);
                    SName    = res.getString(7);
                    SDate    = res.getString(8);
               }
               res.close();

               VECode    . addElement(SPCode);
               VENetRate . addElement(SNetRate);
               VERate    . addElement(SRate);
               VEDiscPer . addElement(SDiscPer);
               VEVatPer  . addElement(SVatPer);
               VETaxPer  . addElement(STaxPer);
               VEName    . addElement(SName);
               VEDate    . addElement(SDate);
          
               VECode    . removeElementAt(0);
               VENetRate . removeElementAt(0);
               VERate    . removeElementAt(0);
               VEDiscPer . removeElementAt(0);
               VEVatPer  . removeElementAt(0);
               VETaxPer  . removeElementAt(0);
               VEName    . removeElementAt(0);
               VEDate    . removeElementAt(0);
          
               for(int i=0;i<VECode.size();i++)
               {
                    String    IQS = "Insert Into TempMRSOrder(Item_Code,NetRate,Rate,DiscPer,CenVatPer,TaxPer,Name,OrderDate) Values( ";
                              IQS = IQS+"'"+(String)VECode       . elementAt(i)+"',";
                              IQS = IQS+"0"+(String)VENetRate    . elementAt(i)+",";
                              IQS = IQS+"0"+(String)VERate       . elementAt(i)+",";
                              IQS = IQS+"0"+(String)VEDiscPer    . elementAt(i)+",";
                              IQS = IQS+"0"+(String)VEVatPer     . elementAt(i)+",";
                              IQS = IQS+"0"+(String)VETaxPer     . elementAt(i)+",";
                              IQS = IQS+"'"+(String)VEName       . elementAt(i)+"',";
                              IQS = IQS+"'"+(String)VEDate       . elementAt(i)+"')";
                    stat.execute(IQS);
               }
               stat.execute(QS4);
          
               ResultSet result    = stat.executeQuery(QS5);
               while(result.next())
               {
                    double    dStock         = common.toDouble(result.getString(6))+common.toDouble(result.getString(3))-common.toDouble(result.getString(2));
                              VPLCode        . addElement(result.getString(1)); 
                              VPLPending     . addElement(result.getString(4));
                              VPLNetRate     . addElement(common.getRound(result.getString(5),2));
                              VPLStock       . addElement(common.getRound(dStock,2));
                              VPLRate        . addElement(result.getString(7));
                              VPLDiscPer     . addElement(result.getString(8));
                              VPLCenVatPer   . addElement(result.getString(9));
                              VPLStPer       . addElement(result.getString(10));
                              VPLSupplier    . addElement(result.getString(11));
                              VPLDate        . addElement(result.getString(12));
               }
               result.close();

               ResultSet theResult = stat.executeQuery(getQS());
               while(theResult.next())
               {
                    VDeptCode   . addElement(theResult.getString(1));
                    VDept       . addElement(theResult.getString(2));
                    VMRSNo      . addElement(theResult.getString(3));
                    VDate       . addElement(theResult.getString(4));
                    VCode       . addElement(theResult.getString(5));
                    VName       . addElement(theResult.getString(6));
                    VMake       . addElement(theResult.getString(7));
                    VRemarks    . addElement(theResult.getString(8));
                    VCatl       . addElement(theResult.getString(9));
                    VDraw       . addElement(theResult.getString(10));
                    VUoM        . addElement(theResult.getString(11));
                    VReqQty     . addElement(theResult.getString(12));
                    VDueDate    . addElement(theResult.getString(13));
                    VNature     . addElement(theResult.getString(14));
                    VBlock      . addElement(theResult.getString(15));
                    VEnqNo      . addElement(theResult.getString(16));
               }
               theResult.close();

               ResultSet theResult1 = stat.executeQuery(getQS1());
               while(theResult1.next())
               {
                    VPLEnqNo       . addElement(theResult1.getString(1));    
                    VPLEnqDate     . addElement(theResult1.getString(2));
                    VPLEnqDueDate  . addElement(theResult1.getString(3));
                    VPLEnqCode     . addElement(theResult1.getString(4));
               }
               theResult1.close();
               stat.close();
               setValue();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void setValue()
     {
          for(int i=0;i<VCode.size();i++)
          {
               String SCode        = (String)VCode.elementAt(i);
               String SReqQty      = (String)VReqQty.elementAt(i);
               String SEnqNo       = (String)VEnqNo.elementAt(i);
          
               String SNetRate     = "0.00";
               String SStock       = "0.00";
               String SPending     = "0.00";
               String SRate        = "0.00";
               String SDiscPer     = "0.00";
               String SVatPer      = "0.00";
               String STaxPer      = "0.00";
               String SSupName     = "";
               String SDate        = "";
          
               double dValue       = 0;
               double dNetValue    = 0;
          
               String SEnqDate     = "";
               String SEnqDue      = "";
          
               int index           = VPLCode.indexOf(SCode);
          
               if(index>-1)
               {
                    SNetRate = (String)VPLNetRate      . elementAt(index);
                    SStock   = (String)VPLStock        . elementAt(index);
                    SPending = (String)VPLPending      . elementAt(index);
                    SRate    = (String)VPLRate         . elementAt(index);
                    SDiscPer = (String)VPLDiscPer      . elementAt(index);
                    SVatPer  = (String)VPLCenVatPer    . elementAt(index);
                    STaxPer  = (String)VPLStPer        . elementAt(index);
                    SSupName = (String)VPLSupplier     . elementAt(index);
                    SDate    = (String)VPLDate         . elementAt(index);
               
                    dNetValue = common.toDouble(SNetRate)*common.toDouble(SReqQty);
                    dValue    = common.toDouble(SRate)*common.toDouble(SReqQty);
               }
               VStock      . addElement(SStock);
               VPending    . addElement(SPending);
               VValue      . addElement(common.getRound(dValue,2));
               VNetValue   . addElement(common.getRound(dNetValue,2));
          
               index = indexOf(SEnqNo,SCode);
          
               if(index > -1)
               {
                    SEnqDate = (String)VPLEnqDate.elementAt(index);
                    SEnqDue  = (String)VPLEnqDueDate.elementAt(index);
               }
               VEnqDate       . addElement(SEnqDate);
               VEnqDueDate    . addElement(SEnqDue);
          }
     }

     private String getQS()
     {
          String QS = " SELECT MRS.Dept_Code, Dept.Dept_Name, "+
                      " MRS.MrsNo, MRS.MrsDate, "+
                      " MRS.Item_Code, InvItems.ITEM_NAME, "+
                      " MRS.Make, MRS.Remarks, "+
                      " InvItems.Catl, InvItems.Draw, "+
                      " UoM.UOMName, MRS.Qty, MRS.DueDate, "+
                      " Nature.Nature,OrdBlock.BlockName,MRS.EnquiryNo "+
                      " FROM (((MRS INNER JOIN InvItems ON MRS.Item_Code=InvItems.ITEM_CODE) "+
                      " INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) "+
                      " Inner Join Nature On MRS.NatureCode = Nature.NatureCode Inner Join Uom on Uom.UomCode = InvItems.UomCode) "+
                      " Inner Join OrdBlock On MRS.BlockCode = OrdBlock.Block "+
                      " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MRS.Qty > 0 And MRS.OrderNo = 0 And MRS.MRSDate <= '"+TEnDate.toNormal()+"' "+
                      " And MRS.MillCode="+iMillCode+" Order By 1,3,2,5 ";

          return QS;
     }

     private String getQS1()
     {
          String QS = " Select EnqNo,EnqDate,DueDate,Item_Code From Enquiry "+
                      " Where Enquiry.MillCode="+iMillCode+
                      " Group By EnqNo,EnqDate,DueDate,Item_Code ";

          return QS;
     }

     private int indexOf(String SEnqNo,String SCode)
     {
          int index=-1;
          for(int i=0;i<VPLEnqNo.size();i++)
          {
               int iEnqNo      = common.toInt((String)VPLEnqNo.elementAt(i));
               String SEnqCode = (String)VPLEnqCode.elementAt(i);
               if(iEnqNo == common.toInt(SEnqNo) && SEnqCode.equals(SCode))
               {
                    index=i;
                    break;
               }
          }
          return index;
     }
}
