package MRS;

import java.io.*;
import java.sql.*;

import java.util.*;
import util.*;

public class PrnGenerationWithTotal
{
     String              SMainTitle,SPitch;

     String[]            SSubTitles,SAlign;
     String[]            SHead1,SHead2,SHead3;

     int[]               iLength;

     Common              common;

     FileWriter          FW;

     int                 iLineCount,iPageNo;

     String              SHead= "",STitle="";

     public PrnGenerationWithTotal(FileWriter FW,String SMainTitle,String[] SSubTitles,String[] SAlign,int[] iLength,String SPitch,String[] SHead1,String[] SHead2,String[] SHead3)
     {
          this . FW           = FW;
          this . SMainTitle   = SMainTitle;
          this . SSubTitles   = SSubTitles;
          this . SAlign       = SAlign;
          this . iLength      = iLength;
          this . SPitch       = SPitch;
          this . SHead1       = SHead1;
          this . SHead2       = SHead2;
          this . SHead3       = SHead3;

          common              = new Common();

          initValues();
          setHeadPrn();
     }

     public PrnGenerationWithTotal(FileWriter FW,String STitle,String SHead,String[] SAlign,int[] iLength)
     {
          this . FW           = FW;
          this . STitle       = STitle;
          this . SHead        = SHead;
          this . SAlign       = SAlign;
          this . iLength      = iLength;

          common              = new Common();

          initValues();
          setHeadPrn();
     }

     /*--- Generate the Prn Format----*/

     private void initValues()
     {
          iLineCount     = 0;
          iPageNo        = 1;
     }

     private void setHeadPrn()
     {
          try
          {
               if(SHead.length() == 0)
               {
                    FW.write(SPitch+"\n");
                    FW.write("E"+SMainTitle+"F\n\n");
     
                    for(int i=0;i<SSubTitles.length;i++)
                    {
                         FW.write("E"+SSubTitles[i]+"F\n");
                    }
                    FW.write("EPage No : "+(iPageNo++)+"F\n");
                    
                    FW.write("|");
                    for(int i=0;i<iLength.length;i++)
                    {
                         FW.write(common.Replicate("-",iLength[i]));
     
                         if(iLength.length-1 != i)
                         {
                              FW.write("-");
                         }
                    }
                    FW.write("|\n");
     
                    FW.write("|");
                    for(int i=0;i<iLength.length;i++)
                    {
                         FW.write(common.Cad(SHead1[i],iLength[i])+"|");
                    }
                    FW.write("\n");
     
                    FW.write("|");
                    for(int i=0;i<iLength.length;i++)
                    {
                         FW.write(common.Cad(SHead2[i],iLength[i])+"|");
                    }
                    FW.write("\n");
     
                    FW.write("|");
                    for(int i=0;i<iLength.length;i++)
                    {
                         FW.write(common.Cad(SHead3[i],iLength[i])+"|");
                    }
                    FW.write("\n");
     
                    FW.write("|");
                    for(int i=0;i<iLength.length;i++)
                    {
                         FW.write(common.Replicate("-",iLength[i]));
     
                         if(iLength.length-1 != i)
                         {
                              FW.write("-");
                         }
                    }
                    FW.write("|\n");
               }
               else
               {
                    FW.write(STitle);
                    FW.write(SHead);
               }

               iLineCount += 10;
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setBodyPrn(String[] SBody)
     {
          try
          {
               // For Body String

               FW.write("|");
               for(int i=0;i<SBody.length;i++)
               {
                    String SBodyString  = "";

                    if(SAlign[i].equals("L"))
                    {
                         SBodyString    = common.Pad(SBody[i],iLength[i]);
                    }
                    else if(SAlign[i].equals("R"))
                    {
                         SBodyString    = common.Rad(SBody[i],iLength[i]);
                    }
                    else
                    {
                         SBodyString    = common.Cad(SBody[i],iLength[i]);
                    }

                    FW.write(SBodyString+"|");
               }
               FW.write("\n");

               // To Print line For Every Data

               if(iLineCount >= 60)
               {
                    setFootPrn();

                    iLineCount = 0;

                    setHeadPrn();
               }
               else
               {
                    FW.write("|");
                    for(int i=0;i<iLength.length;i++)
                    {
                         FW.write(common.Replicate("-",iLength[i]));
   
                         if(iLength.length-1 != i)
                         {
                              FW.write("-");
                         }
                    }
                    FW.write("|\n");
               }

               iLineCount += 2;
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setBodyPrn(String[] SBody, int iVectorSize, int iRow)
     {
          try
          {
               // For Body String

               FW.write("|");
               for(int i=0;i<SBody.length;i++)
               {
                    String SBodyString  = "";

                    if(SAlign[i].equals("L"))
                    {
                         SBodyString    = common.Pad(SBody[i],iLength[i]);
                    }
                    else if(SAlign[i].equals("R"))
                    {
                         SBodyString    = common.Rad(SBody[i],iLength[i]);
                    }
                    else
                    {
                         SBodyString    = common.Cad(SBody[i],iLength[i]);
                    }

                    FW.write(SBodyString+"|");
               }
               FW.write("\n");

               // To Print line For Every Data

               if(iLineCount >= 60)
               {
                    setFootPrn();

                    iLineCount = 0;

                    setHeadPrn();
               }
               else
               {
                    if((iRow + 1) != iVectorSize)
                    {
                         FW.write("|");
                         for(int i=0;i<iLength.length;i++)
                         {
                              FW.write(common.Replicate("-",iLength[i]));
        
                              if(iLength.length-1 != i)
                              {
                                   FW.write("-");
                              }
                         }
                         FW.write("|\n");
                    }
               }

               iLineCount += 2;
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setFootPrn()
     {
          try
          {
               FW.write("-");
               for(int i=0;i<iLength.length;i++)
               {
                    FW.write(common.Replicate("-",iLength[i]));

                    if(iLength.length-1 != i)
                    {
                         FW.write("-");
                    }
               }
               FW.write("|\n");
               FW.write("\n");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setFootPrnWithTotal(String[] STotal)
     {
          try
          {
               FW.write("|");
               for(int i=0;i<STotal.length;i++)
               {
                    String SBodyString  = common.Rad(STotal[i],iLength[i]);

                    FW.write(SBodyString+"|");
               }
               FW.write("\n");

               FW.write("-");
               for(int i=0;i<iLength.length;i++)
               {
                    FW.write(common.Replicate("-",iLength[i]));

                    if(iLength.length-1 != i)
                    {
                         FW.write("-");
                    }
               }
               FW.write("|\n");
               FW.write("");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
