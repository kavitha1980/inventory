package MRS;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import guiutil.*;
import util.*;

public class ProjectListModel extends DefaultTableModel 
{

     protected String SWhere;
     protected String SIndentNo;

     Vector theVector;

     String         ColumnName[] = {"Date","Project No","Project Name","Code","Name","EntryCode","EntryName","Make","Catl","Draw",
                                    "Remarks(Ext..)","Qty","Uom","Unit","Department",
                                    "Group","Block","Urgent","Nature","ReasonForMRs(Int..)",
                                    "Rate","T.Value","Status","Due Date","Appr.","Reject.","Reason",
                                    "ToOrder","UserName","Doc Id","SupName","PurchaseDate",
                                    "PurchaseRate","DiscPer","CenvatPer","TaxPer","SuPer",
                                    "NetRate","Id"};

     String         ColumnType[] = {"S","N","S","S","S","S","S","E","E","E","E" ,"E"  ,"E"  ,"E"   ,"E"         ,"E"    ,"E"    ,"E"    ,"E"      , "E"          ,"E"   ,"S", "S"     ,"S",      "B"     ,"B"     ,"E"     , "B"      ,  "S"     ,"S"     ,"S"      ,"S"           ,"S"           ,"S"      ,"S"        ,"S"     ,"S"    ,"S"      ,"S"};

     Common common = new Common();

     boolean bIndent=false;
     int iList=0,iSize;

     int Rate,TotalValue;

     ProjectListModel()
     {
          setDataVector(getRowData(),ColumnName);
     }
     ProjectListModel(String[] ColumnName,String[] ColumnType,int iSize)
     {
          this.ColumnName=ColumnName;
          this.ColumnType=ColumnType;
          this.iSize     =iSize;

          setDataVector(getRowData(),ColumnName);
     }
     ProjectListModel(String[] ColumnName,String[] ColumnType,int iSize,int Rate,int TotalValue)
     {
          this.ColumnName=ColumnName;
          this.ColumnType=ColumnType;
          this.iSize     =iSize;
          this.Rate      =Rate;
          this.TotalValue=TotalValue;

          setDataVector(getRowData(),ColumnName);
     }

     public Class getColumnClass(int col)

     {
          return getValueAt(0,col).getClass();
     }
     public boolean isCellEditable(int iRow,int iCol)
     {
          String SValue = (String)getValueAt(iRow,2);
/*        if(!SValue.equals("-1"))
          {
               if(iCol==12)   // 9 , 17
                   return false;
          }
          String SPrevValue = (String)getValueAt(iRow,29);

          if(SPrevValue.length()>0)
          {
             if(iCol==20)
               return false;
          } */

          if(ColumnType[iCol]=="B" || ColumnType[iCol]=="E")
               return true;
          return false;
     }

     public int getRows()
     {
          return super.dataVector.size();
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[0][ColumnName.length];

          return RowData;
     }
     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }
     public String getValueFrom(int iRow,int iCol)
     {
          return (String)getValueAt(iRow,iCol);
     }
     public void setMasterDetails(String SValue,int iRow,int iCol)
     {
          setValueAt(SValue,iRow,iCol);                        
     }
     public void deleteRow(int index)
     {
         if(getRows()==1)
         return;
         if(index>=getRows())
         return;
         if(index==-1)
         return;
         removeRow(index);
     }
     public void setValueAt(Object aValue, int row, int column)
     {
            try
            {

                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
                    if(column==Rate)
                    setTotalValue(row,column); 

            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
     }
     private void setRejectionStatus(int iRow)
     {
     }
     private void setApprovalStatus(int iRow)
     {
     }
     public void setTotalValue(int iRow,int iColumn)
     {
          double dQty = common.toDouble((String)getValueAt(iRow,11));
          double dRate = common.toDouble((String)getValueAt(iRow,Rate));


          setValueAt(common.getRound((dQty*dRate),0),iRow,TotalValue);
     }
}

