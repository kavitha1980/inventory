package MRS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class ProjectListRecordFrame extends JInternalFrame
{
    JLayeredPane Layer;
    Vector VCode,VName;
    Vector VMRSRecord;
    
    JPanel     TopPanel,BottomPanel;
    JPanel     First,Second,Third,Fourth;

    JTextField TMatCode;
    JButton    BMatName;

    JComboBox  JCUnit,JCDept,JCGroup,JCServ,JCNature,JCBlock,JCUrgOrd;

    JButton    BMachine;
    JTextField TMacCode,TDeptCode,TUnitCode,TReasonForMrs;
    JTextField TDept,TUnit;
    NextField  TQty,TRate;
    NextField  TDueDays;
    DateField  TDueDate;
    JTextField TMatCatl,TMatDraw,TMake,TRemarks;
    JLabel     LValue;
    JButton    BOk,BCancel;
    DateField  TDate;
    int        i;

    Vector VDept,VGroup,VUnit,VServ,VMac,VBlock,VNature,VUrgent;
    Vector VNameCode;
    int iMillCode;
    ProjectListFrame mrsframe;

    String SCatl,SDraw;

    Common common = new Common();     
    public ProjectListRecordFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VNameCode,int i,Vector VDept,Vector VGroup,Vector VUnit,Vector VServ,Vector VMac,Vector VBlock,Vector VNature,Vector VUrgent,DateField TDate,int iMillCode,ProjectListFrame mrsframe)
    {

             super("You are @ the record "+(i+1));
             this.Layer      = Layer;
             this.VCode      = VCode;
             this.VName      = VName;
             this.VNameCode  = VNameCode;
             this.i          = i;
             this.VDept      = VDept;
             this.VGroup     = VGroup;
             this.VUnit      = VUnit;
             this.VServ      = VServ;
             this.VMac       = VMac;
             this.VBlock     = VBlock;
             this.VNature    = VNature;
             this.VUrgent    = VUrgent;
             this.TDate      = TDate;
             this.iMillCode  = iMillCode;
             this.mrsframe   = mrsframe;

          try
          {
             createComponents();
             setLayouts();
             addComponents();
             setEditables();
             addListeners();
             setData(i);
             show();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     
    }
    public void createComponents()
    {
        TopPanel    = new JPanel();
        BottomPanel = new JPanel();

        First       = new JPanel();
        Second      = new JPanel();
        Third       = new JPanel();
        Fourth      = new JPanel();

        TMatCode   = new JTextField();
        TMatCatl   = new JTextField();
        TMake      = new JTextField();
        TMatDraw   = new JTextField();

        BMachine   = new JButton("Select a Machine");
        TUnit      = new JTextField();
        TDept      = new JTextField();

        TDeptCode  = new JTextField();
        TUnitCode  = new JTextField();
        TMacCode   = new JTextField();

        JCUnit     = new JComboBox(VUnit);
        JCDept     = new JComboBox(VDept);
        JCGroup    = new JComboBox(VGroup);
        JCServ     = new JComboBox(VServ);
        JCBlock    = new JComboBox(VBlock);
        JCNature   = new JComboBox(VNature);

        JCUrgOrd   = new JComboBox(VUrgent);

        TQty         = new NextField();
        TRate        = new NextField();
        TDueDays     = new NextField();
        TDueDate     = new DateField();
        TRemarks     = new JTextField();
        TReasonForMrs= new JTextField();

        LValue       = new JLabel();

        BOk          = new JButton("Okay");
        BCancel      = new JButton("Cancel");
        BMatName     = new JButton("Material");

        TUnit.setEditable(false);
        TDept.setEditable(false);
        TMatCatl.setEditable(false);
        TMatDraw.setEditable(false);
    }

    public void setLayouts()
    {
//         setClosable(true);
         setIconifiable(true);
         setResizable(true);
         setMaximizable(true);
         setBounds(0,0,450,425);

         First.setLayout(new GridLayout(5,2));
         Second.setLayout(new GridLayout(5,2));
         Third.setLayout(new GridLayout(4,2));
         Fourth.setLayout(new GridLayout(5,2));

         getContentPane()    .setLayout(new BorderLayout());
         TopPanel            .setLayout(new GridLayout(2,2));
         BottomPanel         .setLayout(new FlowLayout());

         First.setBorder(new TitledBorder("Material Specification"));
         Second.setBorder(new TitledBorder("Deployment"));
         Third.setBorder(new TitledBorder("Spl Info"));
         Fourth.setBorder(new TitledBorder("How Much & When ?"));
    }

    public void addComponents()
    {
         First.add(new JLabel("Material Name"));
         First.add(BMatName);
         First.add(new JLabel("Material Code"));
         First.add(TMatCode);
         First.add(new JLabel("Catalogue No"));
         First.add(TMatCatl);
         First.add(new JLabel("Drawing No"));
         First.add(TMatDraw);
         First.add(new JLabel("Make"));
         First.add(TMake);


         Second.add(new JLabel("Unit"));
         Second.add(JCUnit);
         Second.add(new JLabel("Department"));
         Second.add(JCDept);
         Second.add(new JLabel("Group"));
         Second.add(JCGroup);
         Second.add(new JLabel("Service Name"));
         Second.add(JCServ);
         Second.add(new JLabel(" Reason For Mrs"));
         Second.add(TReasonForMrs);

         Third.add(new JLabel("Block"));
         Third.add(JCBlock);
         Third.add(new JLabel("Nature"));
         Third.add(JCNature);
         Third.add(new JLabel("Urgent/Ordinary"));
         Third.add(JCUrgOrd);
         Third.add(new JLabel("Remarks/Special Instruction"));
         Third.add(TRemarks);

         Fourth.add(new JLabel("No of Qty"));
         Fourth.add(TQty);
         Fourth.add(new JLabel("Due Days "));
         Fourth.add(TDueDays);
         Fourth.add(new JLabel("Due Date"));
         Fourth.add(TDueDate);
         Fourth.add(new JLabel("Rate"));
         Fourth.add(TRate);
         Fourth.add(new JLabel("Value"));
         Fourth.add(LValue);



         BottomPanel      .add(BOk);
         BottomPanel      .add(BCancel);

         TopPanel.add(First);
         TopPanel.add(Second);
         TopPanel.add(Third);
         TopPanel.add(Fourth);
            
         getContentPane() .add("Center",TopPanel);
         getContentPane() .add("South",BottomPanel);

    }
    public void setEditables()
    {
        TMatCode.setEditable(false);
    }
    public void addListeners()
    {
        BMatName.addActionListener(new ProjectListMaterialSearch(Layer,TMatCode,BMatName,VCode,VName,VNameCode,TMatCatl,TMatDraw,iMillCode,mrsframe,TRate,LValue,TQty));
        TDueDays.addKeyListener(new KeyList());
        TDueDate.addKeyListener(new KeyList());
        TRate.addKeyListener(new KeyList());
    }
    public class KeyList extends KeyAdapter
    {
        public void keyReleased(KeyEvent ke)
        {
            if(ke.getSource()==TDueDays)
            {
                String SDate = TDate.toString();
                SDate = common.getDate(SDate,(long)common.toInt(TDueDays.getText()),1);
                TDueDate.fromString(SDate);
            }
            if(ke.getSource()==TDueDate)
            {
                String SDueDate = TDueDate.toString();
                String SDate    = TDate.toString();
                String SDays    = common.getDateDiff(SDueDate,SDate);
                TDueDays.setText(SDays);
            }
            if(ke.getSource()==TRate)
            {
                double dValue = common.toDouble(TQty.getText())*common.toDouble(TRate.getText());

                LValue.setText(common.getRound(dValue,2));
            }
        }
    }
    private void setData(int i)
    {
          TMatCode.setText("");
          BMatName.setText("");
          TMatCatl.setText("");
          TMake.setText("");
          TMatDraw.setText("");
          TQty.setText("");
          TDueDays.setText("");
          TDueDate.fromString("");

          JCUnit.setSelectedItem("");
          JCDept.setSelectedItem("");
          JCServ.setSelectedItem("");
          JCGroup.setSelectedItem("");
          JCBlock.setSelectedItem("");
          JCUrgOrd.setSelectedItem("");
          JCNature.setSelectedItem("");
          TRemarks.setText("");
          TReasonForMrs.setText("");
    }
     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }

}
