package MRS;

import javax.swing.*;
import javax.swing.table.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import java.awt.image.*;

import guiutil.*;
import util.*;
import jdbc.*;
import java.net.*;

public class RateChartDialog extends JInternalFrame
{

     JPanel  TopPanel,MiddlePanel,BottomPanel;


     JTable  theTable;

     RateChartModel theModel;
     JButton        BSave,BExit;

     MRSTempListModel theMrsModel;
     int              iAppr,ItemCode,ItemName,SupName;
     JLayeredPane     theLayer;
     Common           common  = new Common();

     public RateChartDialog(JLayeredPane theLayer,MRSTempListModel theMrsModel,int iAppr,int ItemCode,int ItemName,int SupName)
     {
          this.theLayer    = theLayer;
          this.theMrsModel = theMrsModel;
          this.iAppr       = iAppr;
          this.ItemCode    = ItemCode;
          this.ItemName    = ItemName;
          this.SupName     = SupName;



          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setRows();
     }

     private void createComponents()
     {

          TopPanel       =    new JPanel();
          MiddlePanel    =    new JPanel();
          BottomPanel    =    new JPanel();


          theModel       = new RateChartModel();
          theTable       = new JTable(theModel);
     
          BSave          = new JButton(" Save ");
          BExit          = new JButton(" Exit ");

          theModel.fireTableDataChanged();
          theTable.setShowGrid(true);
          theTable.setFillsViewportHeight(true);
     }
     private void setLayouts()
     {

          setClosable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,950,500);
          setVisible(true);
          setTitle(" Rate Chart ");

          MiddlePanel.setLayout(new BorderLayout());

     }
     private void addComponents()
     {

          MiddlePanel.add(new JScrollPane(theTable));
          BottomPanel.add(BSave);
          BottomPanel.add(BExit);

          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

     }
     private void addListeners()
     {
          BSave.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
          theTable.addKeyListener(new KeyList());

     }

     private void setRows()
     {
          String STempItemCode="";
          
          for(int i=0; i<theMrsModel.getRows(); i++)
          {

               Boolean bFlag = (Boolean)theMrsModel.getValueAt(i,iAppr);

               String SItemCode    =   (String)theMrsModel.getValueAt(i,ItemCode);
               String SItemName    =   (String)theMrsModel.getValueAt(i,ItemName);
               String SSupName     =   (String)theMrsModel.getValueAt(i,SupName);

/*             if(SSupName.length()>0 && !bFlag )
                    continue;

               if(SItemCode.equals("-1") && !bFlag)
                    continue;                               */

               if(!bFlag)
                 continue;

               if(SItemCode.equals(STempItemCode))
               {
                    STempItemCode = SItemCode;
                    continue;
               }
               HashMap theMap = new HashMap();


               theMap.put("ItemCode",SItemCode);
               theMap.put("ItemName",SItemName);

               theModel.appendRow(theMap);
          }
     }
     private class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    int iRow = theTable.getSelectedRow();
                    try
                    {

                         
                         SupplierSearchList suplist = new SupplierSearchList(theLayer,theModel,iRow);
                         theLayer.add(suplist);
                         theLayer.repaint();
                         suplist.setSelected(true);
                         theLayer.updateUI();
                         suplist.show();
                    }catch(Exception ex){}
               }
          }
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BExit)
               {
                    removeFrame();
               }
          }

     }
     public void removeFrame()
     {
          try
          {
               theLayer.remove(this);
               theLayer.updateUI();
          }
          catch(Exception ex)
          {

          }

     }

}
