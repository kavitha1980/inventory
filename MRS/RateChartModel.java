package MRS;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import guiutil.*;
import util.*;

public class RateChartModel extends DefaultTableModel 
{

     protected String SWhere;
     protected String SIndentNo;

     Vector theVector;


     String         ColumnName[] = {"Code","Name","Supplier","RatePerQty","DiscPer","Discount","Cenvat%","Cenvat","Tax%","Tax","CST%","CST","Surcharge%","Surcharge","Net","DocId","Select"};
     String         ColumnType[] = {"S"   ,"S"   ,"S","E"   ,"E"   , "S"  ,"E", "S"  ,"E","S"  ,"E","S"  ,"E","S"  ,"S" ,"E","B"};

     Common common = new Common();

     int iList=0;

     RateChartModel()
     {
          setDataVector(getRowData(),ColumnName);
     }
     public Class getColumnClass(int col)
     {
          return getValueAt(0,col).getClass();
     }
     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol]=="B" || ColumnType[iCol]=="E")
               return true;
          return false;
     }

     public int getRows()
     {
          return super.dataVector.size();
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[0][ColumnName.length];

          return RowData;
     }
     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }
     public void appendRow(HashMap theMap)
     {
          Vector theVect = new Vector();

          theVect.addElement((String)theMap.get("ItemCode"));
          theVect.addElement((String)theMap.get("ItemName"));
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement("");
          theVect.addElement(new Boolean(false));


          insertRow(getRows(),theVect);
     }

     public String getValueFrom(int iRow,int iCol)
     {
          return (String)getValueAt(iRow,iCol);
     }
     public void setMasterDetails(String SValue,int iRow,int iCol)
     {
          setValueAt(SValue,iRow,iCol);                        
     }
     public void deleteRow(int index)
     {
         if(getRows()==1)
         return;
         if(index>=getRows())
         return;
         if(index==-1)
         return;
         removeRow(index);
     }
     public void setValueAt(Object aValue, int row, int column)
     {
            try
            {

                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
                   fireTableCellUpdated(row,column);
                   if(column==3)
                   {
                      setDisc(row,column);
                      setCenVat(row,column);
                      setTax(row,column);
                      setCst(row,column);
                      setSurcharge(row,column);
                      setNetValue(row);
                   }
                   if(column==4)
                   {
                      setDisc(row,column);
                      setCenVat(row,column);
                      setTax(row,column);
                      setCst(row,column);
                      setSurcharge(row,column);
                      setNetValue(row);
                   }
                   if(column==6)
                   {
                      setCenVat(row,column);
                      setTax(row,column);
                      setCst(row,column);
                      setSurcharge(row,column);
                      setNetValue(row);
                   }
                   if(column==8)
                   {
                      setTax(row,column);
                      setCst(row,column);
                      setSurcharge(row,column);
                      setNetValue(row);
                   }
                   if(column==10)
                   {
                      setCst(row,column);
                      setSurcharge(row,column);
                      setNetValue(row);
                   }
                   if(column==12)
                   {
                      setSurcharge(row,column);
                      setNetValue(row);
                   }
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
     }

     private void  setDisc(int iRow,int iColumn)
     {
          double dPer    = common.toDouble((String)getValueAt(iRow,4));
          double dValue  = common.toDouble((String)getValueAt(iRow,3))*(dPer/100);

          setValueAt(common.getRound(dValue,2),iRow,5);

     }
     private void  setCenVat(int iRow,int iColumn)
     {
          double dRate   = common.toDouble((String)getValueAt(iRow,3));
          double dDisc   =common.toDouble((String)getValueAt(iRow,5));

          double dPer    = common.toDouble((String)getValueAt(iRow,6));
          double dValue  = (dRate-dDisc)*(dPer/100);


          setValueAt(common.getRound(dValue,2),iRow,7);

     }

     private void  setTax(int iRow,int iColumn)
     {

          double dRate   = common.toDouble((String)getValueAt(iRow,3));
          double dDisc   = common.toDouble((String)getValueAt(iRow,5));
          double dCenvat = common.toDouble((String)getValueAt(iRow,7));


          double dPer    = common.toDouble((String)getValueAt(iRow,8));
          double dValue  = ((dRate-dDisc)+dCenvat)*(dPer/100);


          setValueAt(common.getRound(dValue,2),iRow,9);

     }
     private void  setCst(int iRow,int iColumn)
     {

          double dRate   = common.toDouble((String)getValueAt(iRow,3));
          double dDisc   = common.toDouble((String)getValueAt(iRow,5));
          double dCenvat = common.toDouble((String)getValueAt(iRow,7));

          double dPer    = common.toDouble((String)getValueAt(iRow,10));
          double dValue  = ((dRate-dDisc)+dCenvat)*(dPer/100);


          setValueAt(common.getRound(dValue,2),iRow,11);

     }
     private void  setSurcharge(int iRow,int iColumn)
     {

          double dTax = common.toDouble((String)getValueAt(iRow,9));
          double dCst = common.toDouble((String)getValueAt(iRow,11));

          double dPer    = common.toDouble((String)getValueAt(iRow,12));
          double dValue  = ((dTax+dCst))*(dPer/100);

          setValueAt(common.getRound(dValue,2),iRow,13);
     }


     private void setNetValue(int iRow)
     {
          double dRate        = common.toDouble((String)getValueAt(iRow,3));
          double dDiscount    = common.toDouble((String)getValueAt(iRow,5));
          double dCenvat      = common.toDouble((String)getValueAt(iRow,7));
          double dTax         = common.toDouble((String)getValueAt(iRow,9));
          double dCst         = common.toDouble((String)getValueAt(iRow,11));
          double dSurcharge   = common.toDouble((String)getValueAt(iRow,13));

          double dNet =dRate-dDiscount+dCenvat+dTax+dCst+dSurcharge;  // put calculation here

          setValueAt(common.getRound(dNet,2),iRow,14);
     }
}

