
package MRS;
import javax.swing.table.*;
import java.util.Vector;
public class RemovalItemListModel extends DefaultTableModel{
    String ColumnName[]={"SNO","ITEMCODE","ITEMNAME","MATGROUPNAME","ORDER CONVERSION","SELECT"};
    String  ColumnType[]={"S","S","S","S","E","B"};
    int iColumnWidth[] ={30,50,30,30,40,30};
    
    public RemovalItemListModel()
    {
     setDataVector(getRowData(),ColumnName);
    }     
    
    public boolean isCellEditable(int iRow,int iCol)
    {
        if (ColumnType[iCol]=="E"||ColumnType[iCol]=="B")
            
            return true;
            return false;
    }
    public Class getColumnClass(int iCol)
    {
    return getValueAt(0,iCol).getClass();
    
    }
    private Object[][]getRowData()
    {
        Object RowData[][]=new Object[1][ColumnName.length];
        
        for(int i=0;i<ColumnName.length;i++)
            RowData[0][i]="";
        return RowData;
    }
    
    public void appendRow(Vector theVect)
    {
        insertRow(getRows(),theVect);
    }
    public int getRows()
    {
        return super.dataVector.size();
    }

}
