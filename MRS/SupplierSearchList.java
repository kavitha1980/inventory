package MRS;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;


public class SupplierSearchList extends JInternalFrame
{

     protected int iRow,iCol;
     protected String STitle;

     JDialog   dialog;
     JPanel    thePanel,TopPanel;
     JList     BrowList;
     JLabel    LIndicator;
     JTextField TStartText,TMiddleText,TEndText;

     String str="";

     Common common = new Common();

     Connection theConnection=null;
     Vector VCode,VName;

     String SSupplierTable,SCatl,SDraw;

     JTextField     TMatCode;
     JTextField     TMatCatl;
     JTextField     TMatDraw;
     JTextField     TMatName;
     JButton        BMatName;
     NextField      TQty,TRate;
     JLabel         LValue;
     String         SWhere="MRS";
     RateChartModel theModel;
     JLayeredPane   theLayer;

     
     public SupplierSearchList(JLayeredPane theLayer,RateChartModel theModel,int iRow)
     {
          this.theLayer  = theLayer;
          this.theModel  = theModel;
          this.iRow      = iRow;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          try
          {
               ORAConnection jdbc   = ORAConnection.getORAConnection();
               theConnection        = jdbc.getConnection();


          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
          
     private void createComponents()
     {
          TopPanel   = new JPanel();
          thePanel   = new JPanel(true);
          BrowList   = new JList();

          TStartText  = new JTextField(30);
          TMiddleText = new JTextField(30);
          TEndText    = new JTextField(30);

     }
               
     private void setLayouts()
     {
          setClosable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          setVisible(true);

          TopPanel.setLayout(new GridLayout(2,2));
          thePanel.setLayout(new  BorderLayout());
          BrowList.setFont(new Font("monospaced", Font.PLAIN, 11));
     }

     private void addComponents()
     {
          TopPanel.add(new JLabel("  Text ")); //Starting
          TopPanel.add(new JLabel(""));


          TopPanel.add(TMiddleText);
          TopPanel.add(new JLabel(""));


          thePanel.add("Center",new JScrollPane(BrowList));

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(thePanel,BorderLayout.CENTER);

     }

     private void addListeners()
     {
          TStartText.addKeyListener(new KeyList());
          TMiddleText.addKeyListener(new KeyList1());
          TEndText.addKeyListener(new KeyList2());

          BrowList.addKeyListener(new KeyList3());
     }
     

     private class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setSupplierList(0);
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setSupplierList(0);
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         str="";
                    }

               }
               catch(Exception ex)
               {
               }
          }

     }

     private class KeyList1 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setSupplierList(1);
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setSupplierList(1);
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         str="";
                    }

               }
               catch(Exception ex)
               {
               }
          }


     }
     private class KeyList2 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setSupplierList(2);
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setSupplierList(2);
                    }
                    if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                    {
                         str="";
                    }
               }
               catch(Exception ex)
               {
               }
          }

     }
     private class KeyList3 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               
               try
               {
                    if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                    {
                         String str=(String)BrowList.getSelectedValue();

                         int iCount=0;
                         for(int i=0; i<theModel.getRows(); i++)
                         {
                              Boolean bValue = (Boolean)theModel.getValueAt(i,16);

                              if(bValue)
                              {
                                   theModel.setValueAt(str,i,2);

                                   iCount++;
                              }
                         }
                         if(iCount==0)
                         {
                              theModel.setValueAt(str,iRow,2);
                         }
                         removeFrame();
                    }
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }

     }


     private void setSupplierList(int iStatus)
     {
          VCode = new Vector();
          VName = new Vector();

          try
          {
               String QS="";

               if(!str.equals(""))
               {
                    if(iStatus==0)
                    {
                        QS ="  Select Ac_Code,Name from Supplier "+
                            "  where Name like  '"+str+"%'";
                    }
                    if(iStatus==1)
                    {
                        QS ="  Select Ac_Code,Name from Supplier "+
                            "  where Name like  '%"+str+"%'";
                    }
                    if(iStatus==2)
                    {
                        QS ="  Select Ac_Code,Name from Supplier "+
                            "  where Name like  '%"+str+"'";
                    }
                    PreparedStatement thePrepare = theConnection.prepareStatement(QS);
     
                    ResultSet theResult = thePrepare.executeQuery();
                    while(theResult.next())
                    {

                         String SSupCode = theResult.getString(1);
                         String SSupName = theResult.getString(2);
                         VCode.addElement(SSupCode);
                         VName.addElement(SSupName);
                    }
                    theResult.close();
                    thePrepare.close();
               }
               BrowList.setListData(VName);
               BrowList.setSelectedIndex(0);


          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

     }
      public void setCDM(String SMatCode)
      {
           try
           {
                   if(theConnection == null)
                   {
                         ORAConnection jdbc   = ORAConnection.getORAConnection();
                         theConnection        = jdbc.getConnection();
                   }
                   Statement stat   = theConnection.createStatement();
                   ResultSet res1 = stat.executeQuery("Select Catl,Draw From InvItems Where Item_Code='"+SMatCode+"'");
                   while(res1.next())
                   {
                         SCatl = common.parseNull(res1.getString(1));
                         SDraw = common.parseNull(res1.getString(2));
                   }
           }
           catch(Exception ex)
           {
                   System.out.println("@ setCDM");
                   System.out.println(ex);
           }
       }
       private void removeFrame()
       {
          try
          {
               theLayer.remove(this);
               theLayer.updateUI();
          }
          catch(Exception ex)
          {

          }

       }

}
