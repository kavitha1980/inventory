package MRS;

import java.util.*;
import java.sql.*;
import java.io.*;
import javax.swing.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class Yearly1C
{
     Vector VDeptCode,VDept;
     Vector VMRSNo,VDate,VCode,VName,VMake;
     Vector VUoM,VMRSQty;
     Vector VStock,VPending,VReqQty;
     Vector VValue,VMonBudValue,VYearBudValue;
     Vector VPLCode,VPLPending,VPLRate,VPLStock;
     Vector VPLNetRate;
     Vector VMCode,VMQty,VYQty;
     
     int iLctr = 100;
     int iPctr = 0;
     
     double dMonActValue      = 0;
     double dYearActValue     = 0;
     
     double dMonBudValue      = 0;
     double dYearBudValue     = 0;            
     
     double dDMonActValue     = 0;
     double dDYearActValue    = 0;
     
     double dDMonBudValue     = 0;
     double dDYearBudValue    = 0;            
     
     double dTMonActValue     = 0;
     double dTYearActValue    = 0;
     
     double dTMonBudValue     = 0;
     double dTYearBudValue    = 0;            
     
     Common common = new Common();
     
     DateField     TEnDate;
     JTextField     TFile;
     
     FileWriter FW;
     
     String SHead1 = "*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*";
     String SHead2 = "|Sl   | MRS No |MRS Date  |Material |Material Name                 |Material       |UoM     |Present  |MRS      |Required |         |  For the Month    |   For the Year    | Excess/Shortage   |";
     String SHead3 = "|No   |        |          |Code     |                              |Make           |        |Stock    |Qty      |Qty      |Total    |-------------------|-------------------|-------------------|"; 
     String SHead4 = "|     |        |          |         |                              |               |        |  +      |         |         |Value    |Budget   |Actual   |Budget   |Actual   |For the  |For The  |";
     String SHead5 = "|     |        |          |         |                              |               |        |Pending  |         |         |After    |         |         |         |         |Month    |Year     |";
     String SHead6 = "|     |        |          |         |                              |               |        |@ Order  |         |         |ModVat   |         |         |         |         |         |         |";
     String SHead7 = "|     |        |          |         |                              |               |        |         |         |         |         |         |         |         |         |         |         |";
     String SHead8 = "|-----|--------|----------|---------|------------------------------|---------------|--------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|";
     
     int iMillCode;

     Yearly1C(DateField TEnDate,JTextField TFile,int iMillCode)
     {
          this.TEnDate   = TEnDate;
          this.TFile     = TFile;
          this.iMillCode = iMillCode;
          
          String SFile = TFile.getText();

          if((SFile.trim()).length()==0)
               SFile = "1.prn";

          try
          {
               FW = new FileWriter(common.getPrintPath()+SFile);
               toPrint();
               FW.close();
          }
          catch(Exception ex)
          {
               try
               {
                    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(common.getPrintPath()+"Raj.err")));
                    ex.printStackTrace(out);
                    out.close();
               }
               catch(Exception e){}
          }
     }
     
     private void toPrint() throws Exception
     {
          iLctr     = 100;
          iPctr     = 0;
          
          dMonActValue   = 0;
          dYearActValue  = 0;
          dMonBudValue   = 0;
          dYearBudValue  = 0;            
          dDMonActValue  = 0;
          dDYearActValue = 0;
          dDMonBudValue  = 0;
          dDYearBudValue = 0;            
          dTMonActValue  = 0;
          dTYearActValue = 0;
          dTMonBudValue  = 0;
          dTYearBudValue = 0;            
          
          setVectors();
          
          String SPDeptCode   = "";
          String SDept        = "";
          String SPCode       = "";
          String SPName       = "";

          for(int i=0;i<VDeptCode.size();i++)
          {
               setHead();
               String SDeptCode = (String)VDeptCode.elementAt(i);
               SDept            = (String)VDept.elementAt(i);
               String SCode     = (String)VCode.elementAt(i);
               String SName     = (String)VName.elementAt(i);
               
               if(!SDeptCode.equals(SPDeptCode))
               {
                    setDeptFoot(i);
                    setDeptHead(SDept);
                    SPDeptCode=SDeptCode;
                    
                    dMonActValue   = 0;
                    dYearActValue= 0;
                    dMonBudValue= 0;
                    dYearBudValue= 0;            
                    dDMonActValue= 0;
                    dDYearActValue= 0;
                    dDMonBudValue= 0;
                    dDYearBudValue= 0;
               }
               
               if(!SCode.equals(SPCode))
               {
                    setMatFoot(i);
                    SPCode=SCode;
                    
                    dMonActValue=0;
                    dYearActValue=0;
                    
                    dMonBudValue   = common.toDouble((String)VMonBudValue.elementAt(i));
                    dYearBudValue  = common.toDouble((String)VYearBudValue.elementAt(i));            
                    dDMonBudValue  = dDMonBudValue+common.toDouble((String)VMonBudValue.elementAt(i));
                    dDYearBudValue = dDYearBudValue+common.toDouble((String)VYearBudValue.elementAt(i));
                    dTMonBudValue  = dTMonBudValue+common.toDouble((String)VMonBudValue.elementAt(i));
                    dTYearBudValue = dTYearBudValue+common.toDouble((String)VYearBudValue.elementAt(i));            
               }
               setBody(i);
          }
          setMatFoot(VCode.size());
          setDeptFoot(VDeptCode.size());
          setFoot();
     }
     
     private void setBody(int i) throws Exception
     {
          int iMonth      = common.toInt(TEnDate.TMonth.getText());
          String SDate    = (String)VDate.elementAt(i);
          int iCMonth     = common.toInt(SDate.substring(4,6));
          
          double dYearAct = common.toDouble((String)VValue.elementAt(i));
          double dMonAct  = (iMonth==iCMonth?dYearAct:0);
          
          dMonActValue    = dMonActValue+dMonAct;
          dYearActValue   = dYearActValue+dYearAct;
          
          dDMonActValue   = dDMonActValue+dMonAct;
          dDYearActValue  = dDYearActValue+dYearAct;
          
          dTMonActValue   = dTMonActValue+dMonAct;
          dTYearActValue  = dTYearActValue+dYearAct;
          
          String str =   "|"+common.Rad(""+(i+1),5)+"|"+
                         common.Rad((String)VMRSNo.elementAt(i),8)+"|"+
                         common.Pad(common.parseDate((String)VDate.elementAt(i)),10)+"|"+
                         common.Pad((String)VCode.elementAt(i),9)+"|"+
                         common.Pad((String)VName.elementAt(i),30)+"|"+
                         common.Pad((String)VMake.elementAt(i),15)+"|"+
                         common.Pad((String)VUoM.elementAt(i),8)+"|"+
                         common.Rad((String)VStock.elementAt(i),9)+"|"+
                         common.Rad((String)VMRSQty.elementAt(i),9)+"|"+
                         common.Rad((String)VReqQty.elementAt(i),9)+"|"+
                         common.Rad((String)VValue.elementAt(i),9)+"|"+
                         common.Space(9)+"|"+
                         common.Rad(common.getRound(dMonAct,0),9)+"|"+
                         common.Space(9)+"|"+
                         common.Rad(common.getRound(dYearAct,0),9)+"|"+                                          
                         common.Space(9)+"|"+  
                         common.Space(9)+"|";
                         
          FW.write(str+"\n");
          iLctr++;
     }
     
     private void setFoot() throws Exception
     {
          String str =   "|"+common.Rad("",5)+"|"+
                         common.Rad("",8)+"|"+
                         common.Pad("",10)+"|"+
                         common.Pad("",9)+"|"+
                         common.Pad("Grand Total",30)+"|"+
                         common.Pad("",15)+"|"+
                         common.Pad("",8)+"|"+
                         common.Rad("",9)+"|"+
                         common.Rad("",9)+"|"+
                         common.Rad("",9)+"|"+
                         common.Rad("",9)+"|"+
                         common.Rad(common.getRound(dTMonBudValue,0),9)+"|"+
                         common.Rad(common.getRound(dTMonActValue,0),9)+"|"+
                         common.Rad(common.getRound(dTYearBudValue,0),9)+"|"+                                          
                         common.Rad(common.getRound(dTYearActValue,0),9)+"|"+                                          
                         common.Rad(common.getRound(dTMonBudValue-dTMonActValue,0),9)+"|"+
                         common.Rad(common.getRound(dTYearBudValue-dTYearBudValue,0),9)+"|";
          
          FW.write(SHead8+"\n");
          FW.write(str+"\n");
          FW.write(SHead8+"\n");
          iLctr=iLctr+3;
     }
     
     private void setDeptFoot(int i) throws Exception
     {
          if(i==0)
               return;
          
          String str = "|"+common.Rad("",5)+"|"+
          common.Rad("",8)+"|"+
          common.Pad("",10)+"|"+
          common.Pad("",9)+"|"+
          common.Pad("Department Total",30)+"|"+
          common.Pad("",15)+"|"+
          common.Pad("",8)+"|"+
          common.Rad("",9)+"|"+
          common.Rad("",9)+"|"+
          common.Rad("",9)+"|"+
          common.Rad("",9)+"|"+
          common.Rad(common.getRound(dDMonBudValue,0),9)+"|"+
          common.Rad(common.getRound(dDMonActValue,0),9)+"|"+
          common.Rad(common.getRound(dDYearBudValue,0),9)+"|"+                                          
          common.Rad(common.getRound(dDYearActValue,0),9)+"|"+                                          
          common.Rad(common.getRound(dDMonBudValue-dDMonActValue,0),9)+"|"+
          common.Rad(common.getRound(dDYearBudValue-dDYearBudValue,0),9)+"|";
          
          FW.write(SHead8+"\n");
          FW.write(str+"\n");
          FW.write(SHead8+"\n");
          iLctr=iLctr+3;
     }
     
     private void setMatFoot(int i) throws Exception
     {
          if(i==0)
               return;
          
          String str = "|"+common.Rad("",5)+"|"+
          common.Rad("",8)+"|"+
          common.Pad("",10)+"|"+
          common.Pad("",9)+"|"+
          common.Pad("Material Total",30)+"|"+
          common.Pad("",15)+"|"+
          common.Pad("",8)+"|"+
          common.Rad("",9)+"|"+
          common.Rad("",9)+"|"+
          common.Rad("",9)+"|"+
          common.Rad("",9)+"|"+
          common.Rad(common.getRound(dMonBudValue,0),9)+"|"+
          common.Rad(common.getRound(dMonActValue,0),9)+"|"+
          common.Rad(common.getRound(dYearBudValue,0),9)+"|"+                                          
          common.Rad(common.getRound(dYearActValue,0),9)+"|"+                                          
          common.Rad(common.getRound(dMonBudValue-dMonActValue,0),9)+"|"+
          common.Rad(common.getRound(dYearBudValue-dYearBudValue,0),9)+"|";
          
          FW.write(SHead8+"\n");
          FW.write(str+"\n");
          FW.write(SHead8+"\n");
          iLctr=iLctr+3;
     }
     
     private void setDeptHead(String SDept) throws Exception
     {
     }
     
     private void setHead() throws Exception
     {
          if(iLctr < 63)
               return ;
          
          if(iPctr > 0)
               FW.write(SHead3+"\n");
          
          iPctr++;
          
          String str1 = "";
          if(iMillCode==0)
          {
               str1 = "Company  : Amarjothi Spinning Mills Ltd.,";
          }
          else
          {
               str1 = "Company  : Amarjothi Spinning Mills Ltd., (Dyeing Unit) ";
          }
          
          String str2 = "Document : Requirement Analysis Report With Budget - Mechanical - Yearly";
          String str3 = "Page     : "+iPctr+"";
          
          FW.write(str1+"\n");                  
          FW.write(str2+"\n");
          FW.write(str3+"\n");
          FW.write(SHead1+"\n");            
          FW.write(SHead2+"\n");
          FW.write(SHead3+"\n");
          FW.write(SHead4+"\n");
          FW.write(SHead5+"\n");
          FW.write(SHead6+"\n");
          FW.write(SHead7+"\n");
          FW.write(SHead8+"\n");
          iLctr = 11;
     }
     
     private void setVectors()
     {
          String SEnDate = TEnDate.toNormal();

          VDeptCode      = new Vector();
          VDept          = new Vector();
          VMRSNo         = new Vector();
          VDate          = new Vector();
          VCode          = new Vector();
          VName          = new Vector();
          VMake          = new Vector();
          VUoM           = new Vector();
          VMRSQty        = new Vector();
          VReqQty        = new Vector();
          VStock         = new Vector();
          VPending       = new Vector();
          VValue         = new Vector();
          VMonBudValue   = new Vector();
          VYearBudValue  = new Vector();

          VMCode         = new Vector();
          VMQty          = new Vector();
          VYQty          = new Vector();

          VPLCode        = new Vector();
          VPLPending     = new Vector();
          VPLNetRate     = new Vector();
          VPLRate        = new Vector();
          VPLStock       = new Vector();

          String QS0 =   " create table Temp0 "+
                         " as (Select Distinct MRS.Item_Code as Code From "+
                         " MRS Inner Join YearlyBudget On "+
                         " MRS.Item_Code = YearlyBudget.Item_Code "+
                         " Where MRS.MRSDate <='"+SEnDate+"' ";

            if(iMillCode != 1)
            {
               QS0 = QS0 +" and (MRS.MillCode <> 1 Or MRS.MillCode is Null) ) ";
            }
            else
            {
               QS0 = QS0 +" and MRS.MillCode = 1 ) ";
            }

            String QS1 = " create table Temp1 "+
                         " as (SELECT Issue.Code, Sum(Issue.Qty) AS ConQty "+
                         " FROM Issue INNER JOIN Temp0 ON Temp0.Code = Issue.Code ";

            if(iMillCode != 1)
            {
               QS1 = QS1+" Where (Issue.MillCode <> 1 Or Issue.MillCode is Null) "+
                         " GROUP BY Issue.Code)";
            }
            else
            {
               QS1 = QS1+" Where Issue.MillCode = 1 "+
                         " GROUP BY Issue.Code)";
            }

            String QS2 = " create table  Temp2 "+
                         " as (SELECT GRN.Code, Sum(GRN.Qty) AS RecQty "+
                         " FROM GRN INNER JOIN Temp0 ON GRN.Code=Temp0.Code "+
                         " Where GRN.GrnBlock < 2 ";

            if(iMillCode != 1)
            {
               QS2 = QS2+" and (Grn.MillCode <> 1 Or Grn.MillCode is Null) "+
                         " GROUP BY GRN.Code) ";
            }
            else
            {
               QS2 = QS2+" and Grn.MillCode = 1 "+
                         " GROUP BY GRN.Code) ";
            }

            String QS3 = " create table  Temp3 "+
                         " as (SELECT PurchaseOrder.Item_Code, Sum(PurchaseOrder.Qty-PurchaseOrder.InvQty) AS Pending "+
                         " FROM  PurchaseOrder INNER JOIN Temp0 ON Temp0.Code=PurchaseOrder.Item_Code ";

            if(iMillCode != 1)
            {
               QS3 = QS3+" Where (purchaseOrder.MillCode <> 1 Or purchaseOrder.MillCode is Null) "+
                         " GROUP BY PurchaseOrder.Item_Code) ";
            }
            else
            {
               QS3 = QS3+" Where purchaseOrder.MillCode = 1 "+
                         " GROUP BY PurchaseOrder.Item_Code) ";
            }

            String QS4 =      " create Table Temp4 as"+
                              " (SELECT Temp0.Code, (PurchaseOrder.Net/PurchaseOrder.Qty) AS NetRate,"+
                              " PurchaseOrder.Rate as Rate,PurchaseOrder.DiscPer as DiscPer,"+
                              " PurchaseOrder.CenVatPer as CenVatPer,PurchaseOrder.TaxPer as TaxPer,"+
                              " Supplier.Name as Name"+
                              " from purchaseorder"+
                              " inner join temp0 on temp0.code = purchaseorder.Item_Code"+
                              " and purchaseorder.id in (select id from (select max(id) as id,t.item_code from purchaseorder inner join"+
                              " (select max(orderdate),item_code from purchaseorder where item_code in"+
                              " (select code from temp0) group by item_code) t on"+
                              " purchaseorder.item_code=t.item_code group by t.item_code))"+
                              " inner join supplier on supplier.Ac_Code = purchaseorder.Sup_Code"+
                              " Where PurchaseOrder.Net > 0 And PurchaseOrder.Qty > 0 )";

            String QS5 =      " SELECT Temp0.Code,"+
                              " Temp1.ConQty, Temp2.RecQty, Temp3.Pending, "+
                              " Temp4.NetRate,InvItems.OPGQTY,Temp4.Rate,Temp4.DiscPer,Temp4.CenVatPer,Temp4.TaxPer,Temp4.Name "+
                              " FROM ((((Temp0 LEFT JOIN Temp1 ON Temp0.Code = Temp1.Code) "+
                              " LEFT JOIN Temp2 ON Temp0.Code = Temp2.Code) "+
                              " LEFT JOIN Temp3 ON Temp0.Code = Temp3.Item_Code) "+
                              " LEFT JOIN Temp4 ON Temp0.Code = Temp4.Code) "+
                              " LEFT JOIN InvItems ON Temp0.Code = InvItems.Item_Code ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               try{stat.execute("Drop Table Temp0");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp1");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp2");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp3");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp4");}catch(Exception ex){}
          
               stat.execute(QS0);
               stat.execute(QS1);
               stat.execute(QS2);
               stat.execute(QS3);
               stat.execute(QS4);

               System.out.println(QS5);

               ResultSet result    = stat.executeQuery(QS5);

               while(result.next())
               {
                    double dStock = common.toDouble(result.getString(6))+common.toDouble(result.getString(3))-common.toDouble(result.getString(2));
                    
                    VPLCode        .addElement(result.getString(1));
                    VPLPending     .addElement(result.getString(4));
                    VPLNetRate     .addElement(common.getRound(result.getString(5),2));
                    VPLStock       .addElement(common.getRound(dStock,2));
                    VPLRate        .addElement(result.getString(7));
               }

               ResultSet theResult = stat.executeQuery(getQS());

               while(theResult.next())
               {
                    VDeptCode   .addElement(theResult.getString(1));
                    VDept       .addElement(theResult.getString(2));
                    VMRSNo      .addElement(theResult.getString(3));
                    VDate       .addElement(theResult.getString(4));
                    VCode       .addElement(theResult.getString(5));
                    VName       .addElement(theResult.getString(6));
                    VMake       .addElement(theResult.getString(7));
                    VUoM        .addElement(theResult.getString(8));
                    VMRSQty     .addElement(theResult.getString(9));
               }
               

               ResultSet theResult2 = stat.executeQuery(getQS2());
               while(theResult2.next())
               {
                    VMCode    .addElement(theResult2.getString(1));
                    VMQty     .addElement(theResult2.getString(2));
                    VYQty     .addElement(theResult2.getString(3));
               }

               setValue();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void setValue()
     {
          for(int i=0;i<VCode.size();i++)
          {
               String SCode   = (String)VCode     .elementAt(i);
               String SMRSQty = (String)VMRSQty   .elementAt(i);
               
               int index = VPLCode.indexOf(SCode);
               
               String SNetRate = "0.00";
               String SStock   = "0.00";
               String SPending = "0.00";
               String SRate    = "0.00";
               String SDiscPer = "0.00";
               String SVatPer  = "0.00";
               String STaxPer  = "0.00";
               
               String SMonBudValue  = "0.00";
               String SYearBudValue = "0.00";
               
               String SSupName = "";
               double dValue   = 0;
               
               if(index > -1)
               {
                    SNetRate = (String)VPLNetRate .elementAt(index);
                    SStock   = (String)VPLStock   .elementAt(index);
                    SPending = (String)VPLPending .elementAt(index);
                    dValue   = common.toDouble(SNetRate)*common.toDouble(SMRSQty);
               }
               VStock      .addElement(common.getRound(common.toDouble(SStock)+common.toDouble(SPending),0));
               VReqQty     .addElement(common.getRound(common.toDouble(SStock)+common.toDouble(SPending)-common.toDouble(SMRSQty),0));
               VPending    .addElement(SPending);
               VValue      .addElement(common.getRound(dValue,2));
               
               index = VMCode.indexOf(SCode);
               
               if(index >-1)
               {
                    String SMonBudQty    = (String)VMQty.elementAt(index);
                    String SYearBudQty   = (String)VYQty.elementAt(index);
                    SMonBudValue  = common.getRound(common.toDouble(SNetRate)*common.toDouble(SMonBudQty),0);
                    SYearBudValue = common.getRound(common.toDouble(SNetRate)*common.toDouble(SYearBudQty),0);
               }
               VMonBudValue.addElement(SMonBudValue);
               VYearBudValue.addElement(SYearBudValue);
          }
     }
     
     private String getQS2()
     {
          String Mon[] = {"AprQty","MayQty","JunQty","JulQty","AugQty","SepQty","OctQty","NovQty","DecQty","JanQty","FebQty","MarQty"};
          String QS = "Select Item_Code,";
          String str1="";
          String str2="";
          int iMonth = common.toInt(TEnDate.TMonth.getText());
          if(iMonth < 4)
          {
               for(int i=0;i<iMonth+9;i++)
               str1=str1+Mon[i]+"+";
               str2=Mon[iMonth+8];
          }
          else if(iMonth > 3)
          {
               for(int i=0;i<iMonth-3;i++)
               str1=str1+Mon[i]+"+";
               str2=Mon[iMonth-4];
          }
          str1=str1+"0";
          QS = QS+str2+","+str1+" From YearlyBudget";

          return QS;
     }
     
     private String getQS()
     {
          String QS = " ";

          try
          {
               if(iMillCode!=1)
               {
                    QS =      " SELECT YearlyBudget.Dept_Code, Dept.Dept_Name, "+
                              " MRS.MrsNo, MRS.MrsDate, "+
                              " MRS.Item_Code, InvItems.ITEM_NAME, "+
                              " MRS.Make,UOM.UOMName, MRS.Qty "+
                              " FROM (((MRS INNER JOIN InvItems ON MRS.Item_Code=InvItems.ITEM_CODE) "+
                              " Inner Join UOM On UOM.UoMCode = InvItems.UoMCode) "+
                              " Inner Join YearlyBudget On MRS.Item_Code = YearlyBudget.Item_Code) "+
                              " INNER JOIN Dept ON YearlyBudget.Dept_Code=Dept.Dept_code "+
                              " Where MRS.MRSDate <= '"+TEnDate.toNormal()+"' "+
                              " and (MRS.MillCode <> 1 Or MRS.MillCode is Null) "+
                              " Order By 1,5";
               }
               else
               {
                    QS =      " SELECT YearlyBudget.Dept_Code, Dept.Dept_Name, "+
                              " MRS.MrsNo, MRS.MrsDate, "+
                              " MRS.Item_Code, InvItems.ITEM_NAME, "+
                              " MRS.Make,UOM.UOMName, MRS.Qty "+
                              " FROM ((MRS INNER JOIN InvItems ON MRS.Item_Code=InvItems.ITEM_CODE) "+
                              " Inner Join UOM On UOM.UoMCode = InvItems.UoMCode) "+
                              " Inner Join YearlyBudget On MRS.Item_Code = YearlyBudget.Item_Code) "+
                              " INNER JOIN Dept ON YearlyBudget.Dept_Code=Dept.Dept_code "+
                              " Where MRS.MRSDate <= '"+TEnDate.toNormal()+"' "+
                              " and MRS.MillCode = 1 "+
                              " Order By 1,5";
               }
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
          return QS;
     }
}
