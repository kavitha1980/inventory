Select max(OrderDate) as OrderDate,Item_Code,OrderStatusCode from(
select  max(OrderDate) as OrderDate,PurchaseOrder.Item_Code,1 as OrderStatusCode
From PurchaseOrder 
Inner join Mrs_Temp on Mrs_Temp.Item_Code=PurchaseOrder.Item_Code
and Mrs_Temp.DeptMrsAuth=1 and MRS_TEmp.IsConverted=0  and Mrs_Temp.RejectionStatus=0 and MRS_TEmp.ApprovalStatus=0 and MRS_TEmp.ReadyForApproval=0 and MRS_TEmp.StoresRejectionStatus=0 and MRS_TEmp.IsDelete=0 and MRS_TEmp.ItemDelete=0
Left Join ComparsionEnquiry on ComparsionEnquiry.Item_code=PurchaseOrder.Item_Code
and ComparsionEnquiry.Type=3 and (ComparsionEnquiry.JmdOrderApproval=1 or ComparsionEnquiry.JmdOrderApproval=0)
where ComparsionEnquiry.Item_Code is null
Group by Purchaseorder.Item_Code
Union all
select  to_Char(max(ComparsionDate)) as OrderDate,ComparsionEnquiry.Item_Code,2 as OrderStatusCode
From  ComparsionEnquiry 
Inner join Mrs_Temp on Mrs_Temp.Item_Code=ComparsionEnquiry.Item_Code
and Mrs_Temp.DeptMrsAuth=1 and MRS_TEmp.IsConverted=0  and Mrs_Temp.RejectionStatus=0 and MRS_TEmp.ApprovalStatus=0 and MRS_TEmp.ReadyForApproval=0 and MRS_TEmp.StoresRejectionStatus=0 and MRS_TEmp.IsDelete=0 and MRS_TEmp.ItemDelete=0
where ComparsionEnquiry.Type=3 and (ComparsionEnquiry.JmdOrderApproval=1 or RateChartEntryStatus=1 )     
Group by ComparsionEnquiry.Item_Code) group by Item_Code,OrderStatusCode) 

