package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import  jdbc.*;
import  util.*;
import  guiutil.*;


//pdf


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;



public class AdvReqAbsFrame extends JInternalFrame
{
     String    SStDate,SEnDate;
     JComboBox JCOrder,JCType,JCStatus;
     JButton   BApply,BPrint,BCreatePdf;
     JPanel    TopPanel,TopLeftPanel,TopRightPanel,TopMiddlePanel;
     JPanel    BottomPanel;
     
	 AlterTabReport      tabreport;
     //TabReport tabreport;
     DateField TStDate;
     DateField TEnDate;

     JRadioButton   JRDate,JRJDate;

     
     Object RowData[][];
     String ColumnData[] = {"Sl No","Date","OrderNo","OrderDate","Supplier","Invoice Value","Advance Amount","PRNo","BPANo","Click To Print","JmdAppDate","JmdAppNo","Status"};
     String ColumnType[] = {"N","S","N","S","S","N","N","S","S","B","S","S","S"};
     Common common = new Common();
     Control control = new Control();
     
     Vector VSlNo,VDate,VSupName,VInvValue,VAdvAmt;
     Vector VSupCode,VOrderNo,VOrderDate,VJmdAppDate,VJmdAppNo,VStatus,VARDateStatus;
     Vector VPRNo,VBPANo;
     
     JLayeredPane DeskTop;
     StatusPanel SPanel;
     Vector VCode,VName;
     int iMillCode,iAuthCode;
     String SSupTable,SMillName;
     
     JScrollPane TabScroll;
     FileWriter FW;


    //pdf

     Document document;
    PdfPTable table, table1;
    int iTotalColumns = 12;
    int iWidth[] = {5,10, 10, 15,10,10,10,10,10,10,10,10};
    
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 14, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 9, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    String PDFFile = common.getPrintPath()+"/Advance.pdf";

     public AdvReqAbsFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,int iAuthCode,String SSupTable,String SMillName)
     {
          super("Abstract on Request For Advance Payment List During a Period");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.VCode     = VCode;
          this.VName     = VName;
          this.iMillCode = iMillCode;
          this.iAuthCode = iAuthCode;
          this.SSupTable = SSupTable;
          this.SMillName = SMillName;
     
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TStDate     = new DateField();
          TEnDate     = new DateField();
          BApply      = new JButton("Apply");
          BPrint      = new JButton("Print");
          BCreatePdf  = new JButton("Create PDF");
          TopPanel    = new JPanel();

          TopLeftPanel   = new JPanel();
          TopRightPanel  = new JPanel();
          TopMiddlePanel = new JPanel();

          BottomPanel = new JPanel();
          JCOrder     = new JComboBox();
          JCType      = new JComboBox();
          JCStatus    = new JComboBox();

         JRDate      = new JRadioButton(" Order Date",true);
         JRJDate     = new JRadioButton(" Jmd Appr Date",false);

          TStDate.setTodayDate();
          TEnDate.setTodayDate();
     }    
     
     public void setLayouts()
     {
          TopPanel.setLayout(new FlowLayout());

          TopPanel.setLayout(new GridLayout(1,2,3,3));

          TopLeftPanel   . setLayout(new GridLayout(3,2,1,1));         
          TopMiddlePanel . setLayout(new GridLayout(3,3,1,1));         
          TopRightPanel  . setLayout(new FlowLayout(FlowLayout.CENTER));
 
          TopLeftPanel   . setBorder(new TitledBorder(""));
          TopMiddlePanel . setBorder(new TitledBorder(""));
          TopRightPanel  . setBorder(new TitledBorder(""));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          JCOrder.addItem("Advance Request No");
          JCOrder.addItem("Supplierwise");
     

         JCType.addItem(" All ");
         JCType.addItem(" Jmd Approved");
         JCType.addItem(" Jmd Not Approved");

         JCStatus . addItem("All");
         JCStatus . addItem("Store Pending");
         JCStatus . addItem("Accounts Pending");
         JCStatus . addItem("BPA Made");



          TopLeftPanel.add(new JLabel("Sorted On"));
          TopLeftPanel.add(JCOrder);

          TopLeftPanel.add(new JLabel("JMD Status"));
          TopLeftPanel.add(JCType);

          TopLeftPanel.add(new JLabel("Pending Status"));
          TopLeftPanel.add(JCStatus);
           
          

          TopMiddlePanel.add(JRDate);
          TopMiddlePanel.add(JRJDate);
          TopMiddlePanel.add(new JLabel(""));
          TopMiddlePanel.add(new JLabel("Start Date"));
          TopMiddlePanel.add(TStDate);
          TopMiddlePanel.add(new JLabel(""));
          TopMiddlePanel.add(new JLabel("End Date"));
          TopMiddlePanel.add(TEnDate);
          TopMiddlePanel.add(BApply);

          TopPanel . add(TopLeftPanel);
          TopPanel . add(TopMiddlePanel); 


        /*  TopPanel.add(new JLabel("Sorted On"));
          TopPanel.add(JCOrder);

          TopPanel.add(JCType);

          TopPanel.add(JRDate);
          TopPanel.add(JRJDate);


          TopPanel.add(new JLabel("Start Date"));
          TopPanel.add(TStDate);
          TopPanel.add(new JLabel("End Date"));
          TopPanel.add(TEnDate);
          TopPanel.add(BApply);*/
     
          BottomPanel.add(BCreatePdf);
          BottomPanel.add(BPrint);
     
          BPrint              .setEnabled(false);
          getContentPane()    .add(TopPanel,BorderLayout.NORTH);
          getContentPane()    .add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply    .addActionListener(new Applylist());
          BPrint    .addActionListener(new PrintList());
          BCreatePdf  .addActionListener(new PrintList());

         JRDate.addActionListener(new ActList1());
         JRJDate.addActionListener(new ActList1());

     }


     private class ActList1 implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRDate)
               {
                    JRDate.setSelected(true);
                    JRJDate.setSelected(false);
               }
               if(ae.getSource()==JRJDate)
               {
                    JRJDate.setSelected(true);
                    JRDate.setSelected(false);
               }
          }
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(checkJMDAppPending())
               {
                    try
                    {
                         FileWriter FW = new FileWriter(common.getPrintPath()+"Advance.prn");
                         for(int i=0;i<RowData.length;i++)
                         {
                              Boolean BValue = (Boolean)RowData[i][9];
                              if(BValue.booleanValue())
                              {
                                   String SAdvSlNo   = (String)VSlNo.elementAt(i);
                                   String SDate      = (String)VDate.elementAt(i);
                                   String SSupCode   = (String)VSupCode.elementAt(i);
                                   String SSupName   = (String)VSupName.elementAt(i);
                                        new AdvReqPrint(FW,SAdvSlNo,SDate,SSupCode,SSupName,iMillCode,SMillName);
                              }
                         }
                         FW.close();
                         removeHelpFrame();
                    }
                    catch(Exception ex){};
               }

               //pdf

               if(ae.getSource()==BCreatePdf)
               {
                    if(checkJMDAppPending())
                    {
                         createPDFFile();
                         
                         try{
                              File theFile   = new File(PDFFile);
                              Desktop        . getDesktop() . open(theFile);
                         }catch(Exception ex){}
                    }
              } 

          }
     }

     private class Applylist implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(TabScroll);
               }
               catch(Exception ex)
               {
               }
               try
               {
                    tabreport = new AlterTabReport(RowData,ColumnData,ColumnType,VARDateStatus);
                    tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
                    TabScroll = new JScrollPane(tabreport);
                    getContentPane().add(TabScroll,BorderLayout.CENTER);

                    if(iAuthCode>1)
                    {
                         tabreport.ReportTable.addKeyListener(new KeyList());
                    }

                    setSelected(true);
                    DeskTop.repaint();
                    DeskTop.updateUI();
                    BPrint.setEnabled(true);
               }
               catch(Exception ex)
               {
                    System.out.println("1"+ex);
                    ex.printStackTrace();
               }
          }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if (ke.getKeyCode()==10)
               {
                    try
                    {
                         int i = tabreport.ReportTable.getSelectedRow();

                         String    SAdvSlNo     = (String)RowData[i][0];
                         String    SBlockSig    = control.getID("Select orderBlock From AdvRequest Where AdvSlNo = "+SAdvSlNo);

                         String    SDate        = (String)RowData[i][1];
                         String    SSupName     = (String)RowData[i][4];

                         int       iBlockSig    = common.toInt(SBlockSig);

                         AdvanceRequestFrame advanceRequestFrame = new AdvanceRequestFrame(DeskTop,SAdvSlNo,iBlockSig,SSupName,SDate,true);
                         DeskTop                  .add(advanceRequestFrame);
                         advanceRequestFrame      .show();
                         advanceRequestFrame      .moveToFront();
                         DeskTop                  .repaint();
                         DeskTop                  .updateUI();
                    }
                    catch(Exception ex)
                    {
                         System.out.println("2"+ex);
                         ex.printStackTrace();
                    }
               }
          } 
     }

     public void removeHelpFrame()
     {
          try
          {
               //DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public void setDataIntoVector()
     {
          VSlNo     = new Vector();
          VDate     = new Vector();
          VOrderNo  = new Vector();
          VOrderDate= new Vector();
          VSupName  = new Vector();
          VInvValue = new Vector();
          VAdvAmt   = new Vector();
          VSupCode  = new Vector();
          VJmdAppDate = new Vector();
          VJmdAppNo   = new Vector();
          VStatus     = new Vector();
          VARDateStatus   = new Vector();
          VPRNo           = new Vector();
          VBPANo          = new Vector();
          
                    SStDate   = TStDate.toString();
                    SEnDate   = TEnDate.toString();
          String    StDate    = TStDate.toNormal();
          String    EnDate    = TEnDate.toNormal();
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               String QString = getQString(StDate,EnDate);
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    VSlNo     .addElement(res.getString(1));
                    VDate     .addElement(common.parseDate(res.getString(2)));
                    VOrderNo  .addElement(res.getString(3));
                    VOrderDate.addElement(common.parseDate(res.getString(4)));
                    VSupName  .addElement(res.getString(5));
                    VInvValue .addElement(res.getString(6));
                    VAdvAmt   .addElement(res.getString(7));
                    VSupCode  .addElement(res.getString(8));
                    VJmdAppDate.addElement(res.getString(10));
                    VJmdAppNo .addElement(res.getString(11));   
                    
                         int iJmdStatus   = res.getInt(12);
                         int iIaStatus   = res.getInt(13);
                         int iSoStatus   = res.getInt(14);


                         String SStatus = getStatus(iJmdStatus,iIaStatus,iSoStatus);

                         VStatus.addElement(SStatus);
						 
						 String SARDateStatus="";
						 String SARDate    = res.getString(15);
						 
						 int iARDate = common.toInt(SARDate);
						 if(iARDate>0)
						 {
							SARDateStatus = "1";
						 }
						 else
						 {
							SARDateStatus = "0";
						 }
						
						 
						 VARDateStatus.addElement(SARDateStatus);
                                     VPRNo     .addElement(res.getString(16));
                                     VBPANo    .addElement(res.getString(17));

                    
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VSlNo.size()][ColumnData.length];
          for(int i=0;i<VSlNo.size();i++)
          {
               RowData[i][0]  = (String)VSlNo.elementAt(i);
               RowData[i][1]  = (String)VDate.elementAt(i);
               RowData[i][2]  = (String)VOrderNo.elementAt(i);
               RowData[i][3]  = (String)VOrderDate.elementAt(i);
               RowData[i][4]  = (String)VSupName.elementAt(i);
               RowData[i][5]  = (String)VInvValue.elementAt(i);
               RowData[i][6]  = (String)VAdvAmt.elementAt(i);
               RowData[i][7]  = (String)VPRNo.elementAt(i);
               RowData[i][8]  = (String)VBPANo.elementAt(i);
               RowData[i][9]  = new Boolean(false);
               RowData[i][10]  = common.parseNull(common.parseDate((String)VJmdAppDate.elementAt(i)));
               RowData[i][11]  = common.parseNull((String)VJmdAppNo.elementAt(i));
               RowData[i][12]  = common.parseNull((String)VStatus.elementAt(i));
               /*RowData[i][7]  = new Boolean(false);
               RowData[i][8]  = common.parseNull(common.parseDate((String)VJmdAppDate.elementAt(i)));
               RowData[i][9]  = common.parseNull((String)VJmdAppNo.elementAt(i));
               RowData[i][10]  = common.parseNull((String)VStatus.elementAt(i));*/

          }  
     }

     public String getQString(String StDate,String EnDate)
     {
          String QString  = "";
          
          QString = "SELECT AdvRequest.AdvSlNo, AdvRequest.AdvDate,AdvRequest.OrderNo,AdvRequest.OrderDate, "+SSupTable+".Name, Sum(AdvRequest.InvValue) AS SumOfInvValue, Sum(AdvRequest.Advance) AS SumOfAdvance, AdvRequest.Sup_Code, AdvRequest.MillCode,AdvRequest.JmdOrderAppDate,AdvRequest.AdvGroupNo,JmdOrderApproval,IAOrderApproval,SOOrderApproval,AdvRequest.ARDate, "+
                    " AdvRequest.ARNo,AdvRequest.BPANo FROM AdvRequest INNER JOIN "+SSupTable+" ON AdvRequest.Sup_Code = "+SSupTable+".Ac_Code ";

                  if(JCType.getSelectedIndex() == 1)
                  {
                        QString = QString+" And AdvRequest.JMDOrderApproval= 1";
                  }

                  if(JCType.getSelectedIndex() == 2)
                  {
                        QString = QString+" And AdvRequest.JMDOrderApproval= 0";
                  }

                  if(JCStatus.getSelectedIndex() == 1)
                  {
                        QString = QString+" And (AdvRequest.ARNo=0 or AdvRequest.ARNo is null) And (AdvRequest.BPANo=0 or AdvRequest.BPANo is null)";
                  }

                  if(JCStatus.getSelectedIndex() == 2)
                  {
                        QString = QString+" And AdvRequest.ARNo>0 And (AdvRequest.BPANo=0 or AdvRequest.BPANo is null)";
                  }
                  if(JCStatus.getSelectedIndex() == 3)
                  {
                        QString = QString+" And AdvRequest.BPANo>0";
                  }



                  QString= QString+  " GROUP BY AdvRequest.AdvSlNo, AdvRequest.AdvDate,AdvRequest.OrderNo,AdvRequest.OrderDate,"+SSupTable+".Name, AdvRequest.Sup_Code,AdvRequest.MillCode,AdvRequest.JmdOrderAppDate,AdvRequest.AdvGroupNo ,JmdOrderApproval,IAOrderApproval,SOOrderApproval,ARDate,AdvRequest.ARNo,AdvRequest.BPANo ";

         if(JRDate.isSelected())
         {
           QString = QString+" Having AdvRequest.AdvDate >= '"+StDate+"' and AdvRequest.AdvDate <='"+EnDate+"' and AdvRequest.MillCode="+iMillCode;
         }
         else
         {
           QString = QString+" Having AdvRequest.JmdOrderAppDate >= '"+StDate+"' and AdvRequest.JmdOrderAppDate <='"+EnDate+"' and AdvRequest.MillCode="+iMillCode;

         }


          if(JCOrder.getSelectedIndex() == 0)
               QString = QString+" Order By AdvRequest.AdvSlNo,AdvRequest.AdvDate";

          if(JCOrder.getSelectedIndex() == 1)
               QString = QString+" Order By "+SSupTable+".Name,AdvRequest.AdvDate";



          return QString;
     }
     private String getStatus(int iJmdStatus,int iIaStatus,int iSoStatus)
     {
        String SStatus="";

        if(iJmdStatus==0 && iIaStatus==0 && iSoStatus==0)
            SStatus =" Pending With S.O";

        if(iJmdStatus==0 && iIaStatus==0 && iSoStatus==1)
            SStatus =" Pending With IA";

        if(iJmdStatus==0 && iIaStatus==1 && iSoStatus==1)
            SStatus =" Pending With JMD";

        return SStatus;
     }


  //creating pdf

   
private void createPDFFile() {
        try {
          
          

             Boolean bFlag; 

            try{
		document = new Document(PageSize.A4);
		PdfWriter.getInstance(document, new FileOutputStream(PDFFile));
		document.open();
              //  document .  newPage();  

       }catch(Exception ex){}
             for(int i=0;i<RowData.length;i++)
                    {
                         Boolean BValue = (Boolean)RowData[i][9];

                         if(BValue.booleanValue())
                         {
                              String SAdvSlNo   = (String)VSlNo.elementAt(i);
                              String SDate      = (String)VDate.elementAt(i);
                              String SSupCode   = (String)VSupCode.elementAt(i);
                              String SSupName   = (String)VSupName.elementAt(i);

                              new AdvReqPrint(document,SAdvSlNo,SDate,SSupCode,SSupName,iMillCode,SMillName);
                         }
                    }

 
              document.close();
            //JOptionPane.showMessageDialog(null, "PDF File Created in " + PDFFile, "Info", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) 
        {
            e.printStackTrace();
        }
    }

     private boolean checkJMDAppPending()
     {
          String SErrorMsg    = "";

          for(int i=0;i<RowData.length;i++)
          {
               Boolean BValue = (Boolean)RowData[i][9];

               if(BValue.booleanValue())
               {
                    String SAdvSlNo     = common.parseNull((String)VSlNo.elementAt(i));
                    int iJMDAppNo       = common.toInt((String)VJmdAppNo.elementAt(i));

                    if(iJMDAppNo == 0)
                    {
                         SErrorMsg     += SAdvSlNo+", ";
                    }
               }
          }

          if(SErrorMsg.trim().length() > 0)
          {
               SErrorMsg = SErrorMsg.substring(0, SErrorMsg.length() - 2);

               SErrorMsg = "JMD Approval is Pending. So printing not allowed for the following advance sl. nos\n\n"+SErrorMsg;

               JOptionPane.showMessageDialog(null, SErrorMsg, "Info", JOptionPane.ERROR_MESSAGE);
               return false;
          }

          return true;
     }
}
