package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class AdvReqDeletionFrame extends JInternalFrame
{
     JPanel    TopPanel;
     JPanel    BottomPanel;
     JPanel    ControlPanel;
     JPanel    ListPanel;
     JPanel    First,Second,Third,Fourth,Fifth;
     
     JButton   BApply,BOk,BExit;

     JComboBox      JCFilter;
     JTextField     TOrder;
     JRadioButton   JRPeriod,JRNo;

     DateField TStDate,TEnDate;
     NextField TStNo,TEnNo;

     Object RowData[][];
     String ColumnData[] = {"Adv No","Date","Supplier","Order No","Date","Block","Code","Name","Qty","Adv Amount","Status"};
     String ColumnType[] = {"N"     ,"S"   ,"S"       ,"S"       ,"S"   ,"S"    ,"S"   ,"S"   ,"N"  ,"N"         ,"B"     };

     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;
     int iUserCode,iMillCode,iAuthCode;
     String SSupTable;

     JTextField TAdvNo;
     
     Common common = new Common();
     
     Vector VAdvNo,VAdvDate,VSupName,VOrderNo,VOrderDate,VBlock,VItemCode,VItemName,VOrdQty,VAdvAmount,VId;
     
     TabReport tabreport;
     int iOrderStatus  = 0;
     Vector VItemsStatus;
     
     public AdvReqDeletionFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode,String SSupTable)
     {
          try
          {
               this.DeskTop   = DeskTop;
               this.VCode     = VCode;
               this.VName     = VName;
               this.SPanel    = SPanel;
               this.iUserCode = iUserCode;
               this.iMillCode = iMillCode;
               this.iAuthCode = iAuthCode;
               this.SSupTable = SSupTable;
               
               createComponents();
               setLayouts();
               addComponents();
               addListeners();
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }

     }

     public void createComponents()
     {
          try
          {
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();
          First       = new JPanel();
          Second      = new JPanel();
          Third       = new JPanel();
          Fourth      = new JPanel();
          Fifth       = new JPanel();

          TStDate     = new DateField();
          TEnDate     = new DateField();

          TOrder      = new JTextField("1,2,3");
          JCFilter    = new JComboBox();
          TStNo       = new NextField(0);
          TEnNo       = new NextField(0);
     
          JRPeriod    = new JRadioButton("Periodical",true);
          JRNo        = new JRadioButton("Adv No",false);
     
          TStDate.setTodayDate();
          TEnDate.setTodayDate();

          BOk            = new JButton("Delete");
          BExit          = new JButton("Exit");
          BApply         = new JButton("Apply");
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void setLayouts()
     {
          try
          {
          TopPanel  .setLayout(new GridLayout(1,5));
          First     .setLayout(new GridLayout(1,1));
          Second    .setLayout(new GridLayout(1,1));
          Third     .setLayout(new GridLayout(2,1));
          Fourth    .setLayout(new GridLayout(2,1));
          Fifth     .setLayout(new GridLayout(1,1));

          BottomPanel.setBorder(new TitledBorder(""));

          setTitle("Advance Request Deletion Frame");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     public void addComponents()
     {
          try
          {
          JCFilter.addItem("All");
          JCFilter.addItem("Over-Due");
          JCFilter.addItem("Order Not Placed");
          
          First.setBorder(new TitledBorder("Sorted On (Column No)"));
          First.add(TOrder);
          
          Second.setBorder(new TitledBorder("Filter"));
          Second.add(JCFilter);
          
          Third.setBorder(new TitledBorder("Basis"));
          Third.add(JRPeriod);
          Third.add(JRNo);
          
          Fourth.setBorder(new TitledBorder("Periodical"));
          Fourth.add(TStDate);
          Fourth.add(TEnDate);
          
          Fifth.add(BApply);
          
          TopPanel.add(First);
          TopPanel.add(Second);
          TopPanel.add(Third);
          TopPanel.add(Fourth);
          TopPanel.add(Fifth);

          BottomPanel.add(BOk);
          BottomPanel.add(BExit);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }

     }
     
     public void addListeners()
     {
          try
          {
               JRPeriod.addActionListener(new JRList());
               JRNo.addActionListener(new JRList()); 
               BApply.addActionListener(new ApplyList());
               BExit.addActionListener(new ApplyList());
               BOk.addActionListener(new ApplyList());
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    Fourth.setBorder(new TitledBorder("Periodical"));
                    Fourth.removeAll();
                    Fourth.add(TStDate);
                    Fourth.add(TEnDate);
                    Fourth.updateUI();
                    JRNo.setSelected(false);
               }
               else
               {
                    Fourth.setBorder(new TitledBorder("Numbered"));
                    Fourth.removeAll();
                    Fourth.add(TStNo);
                    Fourth.add(TEnNo);
                    Fourth.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport = new TabReport(RowData,ColumnData,ColumnType);

                    getContentPane().add(tabreport,BorderLayout.CENTER);
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
               }
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
               if(ae.getSource()==BOk)
               {
                    for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
                    {
                         Boolean bFlag     = (Boolean)tabreport.ReportTable.getValueAt(i,10);

                         if(bFlag.booleanValue())
                         {
                              String SId = common.parseNull((String)VId.elementAt(i));
                              DeleteData(SId);
                         }
                    }
                    removeHelpFrame();
               }
          }
     }

     public void setDataIntoVector()
     {
          VAdvNo         = new Vector();
          VAdvDate       = new Vector();
          VSupName       = new Vector();
          VOrderNo       = new Vector();
          VOrderDate     = new Vector();
          VBlock         = new Vector();
          VItemCode      = new Vector();
          VItemName      = new Vector();
          VOrdQty        = new Vector();
          VAdvAmount     = new Vector();
          VId            = new Vector();

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(getQString());

               while (res.next())
               {
                    VAdvNo         .addElement(res.getString(1));
                    VAdvDate       .addElement(res.getString(2));
                    VSupName       .addElement(res.getString(3));
                    VOrderNo       .addElement(res.getString(4));
                    VOrderDate     .addElement(res.getString(5));
                    VBlock         .addElement(res.getString(6));
                    VItemCode      .addElement(res.getString(7));
                    VItemName      .addElement(res.getString(8));
                    VOrdQty        .addElement(res.getString(9));
                    VAdvAmount     .addElement(res.getString(10));
                    VId            .addElement(res.getString(11));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void DeleteData(String SId)
     {
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               String QS = " Insert into DeletedAdvRequest (Select * from AdvRequest Where Id="+SId+") ";
               stat.executeUpdate(QS);
               String QString ="Delete from AdvRequest Where Id="+SId;
               stat.executeUpdate(QString);
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          try
          {
               RowData     = new Object[VAdvNo.size()][ColumnData.length];
               for(int i=0;i<VAdvNo.size();i++)
               {
                    RowData[i][0]  = common.parseNull((String)VAdvNo         .elementAt(i));
                    RowData[i][1]  = common.parseDate((String)VAdvDate       .elementAt(i));
                    RowData[i][2]  = common.parseNull((String)VSupName       .elementAt(i));
                    RowData[i][3]  = common.parseNull((String)VOrderNo       .elementAt(i));
                    RowData[i][4]  = common.parseNull((String)VOrderDate     .elementAt(i));
                    RowData[i][5]  = common.parseNull((String)VBlock         .elementAt(i));
                    RowData[i][6]  = common.parseNull((String)VItemCode      .elementAt(i));
                    RowData[i][7]  = common.parseNull((String)VItemName      .elementAt(i));
                    RowData[i][8]  = common.parseNull((String)VOrdQty        .elementAt(i));
                    RowData[i][9]  = common.parseNull((String)VAdvAmount     .elementAt(i));
                    RowData[i][10] = new Boolean(false);
               }
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
      public void removeHelpFrame()
      {
            try
            {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
            }
            catch(Exception ex) { }
      }

     public String getQString()
     {
          String QString = "";

          if(JRPeriod.isSelected())
          {
               QString = " Select AdvRequest.AdvSlNo,AdvRequest.AdvDate,"+SSupTable+".Name,"+
                         " AdvRequest.OrderNo,AdvRequest.OrderDate,OrdBlock.BlockName,"+
                         " AdvRequest.Code,InvItems.Item_Name,AdvRequest.Qty,AdvRequest.Advance,AdvRequest.Id "+
                         " From AdvRequest "+
                         " Inner Join "+SSupTable+" on AdvRequest.Sup_Code="+SSupTable+".Ac_Code "+
                         " Inner join OrdBlock On AdvRequest.OrderBlock=OrdBlock.Block "+
                         " Inner join InvItems On AdvRequest.Code=InvItems.Item_Code "+
                         " Where AdvRequest.OrderBlock<>9 And AdvRequest.Advance > 0 "+
                         " And AdvRequest.ARNo=0 And Advrequest.AdvDate >= '"+TStDate.toNormal()+"'"+
                         " And AdvRequest.AdvDate <='"+TEnDate.toNormal()+"'"+
                         " And AdvRequest.MillCode="+iMillCode+
                         " Union All "+
                         " Select AdvRequest.AdvSlNo,AdvRequest.AdvDate,"+SSupTable+".Name,"+
                         " AdvRequest.OrderNo,AdvRequest.OrderDate,'WO' as BlockName,"+
                         " '' as Code,AdvRequest.Descript as Item_Name,AdvRequest.Qty,AdvRequest.Advance,AdvRequest.Id "+
                         " From AdvRequest "+
                         " Inner Join "+SSupTable+" on AdvRequest.Sup_Code="+SSupTable+".Ac_Code "+
                         " Where AdvRequest.OrderBlock=9 And AdvRequest.Advance > 0 "+
                         " And AdvRequest.ARNo=0 And Advrequest.AdvDate >= '"+TStDate.toNormal()+"'"+
                         " And AdvRequest.AdvDate <='"+TEnDate.toNormal()+"'"+
                         " And AdvRequest.MillCode="+iMillCode;
          }
          else
          {
               QString = " Select AdvRequest.AdvSlNo,AdvRequest.AdvDate,"+SSupTable+".Name,"+
                         " AdvRequest.OrderNo,AdvRequest.OrderDate,OrdBlock.BlockName,"+
                         " AdvRequest.Code,InvItems.Item_Name,AdvRequest.Qty,AdvRequest.Advance,AdvRequest.Id "+
                         " From AdvRequest "+
                         " Inner Join "+SSupTable+" on AdvRequest.Sup_Code="+SSupTable+".Ac_Code "+
                         " Inner join OrdBlock On AdvRequest.OrderBlock=OrdBlock.Block "+
                         " Inner join InvItems On AdvRequest.Code=InvItems.Item_Code "+
                         " Where AdvRequest.OrderBlock<>9 And AdvRequest.Advance > 0 "+
                         " And AdvRequest.ARNo=0 And AdvRequest.AdvSlNo >="+TStNo.getText()+
                         " And Advrequest.AdvSlNo <="+TEnNo.getText()+
                         " And AdvRequest.MillCode="+iMillCode+
                         " Union All "+
                         " Select AdvRequest.AdvSlNo,AdvRequest.AdvDate,"+SSupTable+".Name,"+
                         " AdvRequest.OrderNo,AdvRequest.OrderDate,'WO' as BlockName,"+
                         " '' as Code,AdvRequest.Descript as Item_Name,AdvRequest.Qty,AdvRequest.Advance,AdvRequest.Id "+
                         " From AdvRequest "+
                         " Inner Join "+SSupTable+" on AdvRequest.Sup_Code="+SSupTable+".Ac_Code "+
                         " Where AdvRequest.OrderBlock=9 And AdvRequest.Advance > 0 "+
                         " And AdvRequest.ARNo=0 And AdvRequest.AdvSlNo >="+TStNo.getText()+
                         " And Advrequest.AdvSlNo <="+TEnNo.getText()+
                         " And AdvRequest.MillCode="+iMillCode;
          }
          
          QString = QString+" Order By "+TOrder.getText()+",11 ";

          return QString;
     }

}
