package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class AdvReqInvMiddlePanel extends JPanel
{
     JTable         ReportTable;
     AdvTableModel  dataModel;  

     JLabel         LNet,LAdvance;
     NextField      TPlus,TMinus;

     String ColumnData[] = {"Proforma Inv No","Inv Date","Block","Order No","Order Date","Code","Name","Qty","Inv Value","Adv Amt (%)","Adv Amt (Rs.)"};
     String ColumnType[] = {"E","E","S","N","S","S","S","E","E","E","N"};

     JPanel         GridPanel,BottomPanel,FigurePanel;

     Common common = new Common();

     JLayeredPane DeskTop;
     Object RowData[][];

     AdvReqInvMiddlePanel(JLayeredPane DeskTop,Object RowData[][])
     {
          this.DeskTop   = DeskTop;
          this.RowData   = RowData;
     
          LNet           = new JLabel("0");
          LAdvance       = new JLabel("0");
          TPlus          = new NextField();
          TMinus         = new NextField();
          GridPanel      = new JPanel(true);
          BottomPanel    = new JPanel();
          FigurePanel    = new JPanel();
     
          GridPanel      .setLayout(new BorderLayout());
          BottomPanel    .setLayout(new BorderLayout());
          FigurePanel    .setLayout(new GridLayout(2,4));
     
          FigurePanel    .add(new JLabel("Invoice Value"));
          FigurePanel    .add(new JLabel("Plus"));
          FigurePanel    .add(new JLabel("Minus"));
          FigurePanel    .add(new JLabel("Advance Amount"));
          FigurePanel    .add(LNet);
          FigurePanel    .add(TPlus);
          FigurePanel    .add(TMinus);
          FigurePanel    .add(LAdvance);
     
          BottomPanel    .add("North",FigurePanel);
     
          TPlus          .addKeyListener(new KeyList());
          TMinus         .addKeyListener(new KeyList());
          setReportTable();
     }

     public void setReportTable()
     {
          dataModel        = new AdvTableModel(RowData,ColumnData,ColumnType,LNet,LAdvance,TPlus,TMinus);       
          ReportTable      = new JTable(dataModel);
          ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
     
          DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
          cellRenderer.setHorizontalAlignment(JLabel.RIGHT);
     
          for (int col=0;col<ReportTable.getColumnCount();col++)
          {
               if(ColumnType[col]=="N" || ColumnType[col]=="B")
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
          }
     
          //ReportTable.setShowGrid(false);
     
          setLayout(new BorderLayout());
          GridPanel.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
          GridPanel.add(new JScrollPane(ReportTable),BorderLayout.CENTER);
     
          add(GridPanel,BorderLayout.CENTER);
          add(BottomPanel,BorderLayout.SOUTH);
     }

     public Object[][] getFromVector()
     {
          return dataModel.getFromVector();     
     }

     private class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               dataModel.setTotal();               
          }
     }
}

