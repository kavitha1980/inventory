package Order;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class AdvReqMiddlePanel extends JPanel 
{
     AdvReqInvMiddlePanel MiddlePanel;
     Object RowData[][];
     
     Common common = new Common();
     
     JLayeredPane DeskTop;
     Vector VCode,VName;
     String SSupCode,SSupName;
     Vector VSelectedId,VSelectedBlock,VSelectedOrdNo,VSelectedDate,VSelectedCode,VSelectedName,VSelectedQty,VSelectedNet,VSelectedInviceNo,VSelectedInviceDate,VSelectedAdvPer,VSelectedAdvanceAmt;

     boolean   bflag = false;
     String    SAdvSlNo;
     int       iBlockSig;
     int       iMillCode;

     public AdvReqMiddlePanel(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VSelectedId,Vector VSelectedBlock,Vector VSelectedOrdNo,Vector VSelectedDate,Vector VSelectedCode,Vector VSelectedName,Vector VSelectedQty,Vector VSelectedNet)
     {
          this.DeskTop        = DeskTop;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VSelectedId    = VSelectedId;
          this.VSelectedBlock = VSelectedBlock;
          this.VSelectedOrdNo = VSelectedOrdNo;
          this.VSelectedDate  = VSelectedDate;
          this.VSelectedCode  = VSelectedCode;
          this.VSelectedName  = VSelectedName;
          this.VSelectedQty   = VSelectedQty;
          this.VSelectedNet   = VSelectedNet;
     
          setLayout(new BorderLayout());
          setRowData();
          createComponents();
     }

     public AdvReqMiddlePanel(JLayeredPane DeskTop,String SAdvSlNo,int iBlockSig, boolean bflag,int iMillCode)
     {
          this.DeskTop        = DeskTop;
          this.SAdvSlNo       = SAdvSlNo;
          this.iBlockSig      = iBlockSig;
          this.bflag          = bflag;
          this.iMillCode      = iMillCode;

          setLayout(new BorderLayout());
          getOrderDetails(SAdvSlNo,iBlockSig);
          setRowData1();
          createComponents();
     }

     public void createComponents()
     {
          MiddlePanel    = new AdvReqInvMiddlePanel(DeskTop,RowData);
          add(MiddlePanel,BorderLayout.CENTER);
     }

     public void setRowData()
     {
          RowData     = new Object[VSelectedCode.size()][11];
          for(int i=0;i<VSelectedCode.size();i++)
          {
               RowData[i][0]  = "";
               RowData[i][1]  = "";
               RowData[i][2]  = (String)VSelectedBlock                .elementAt(i);
               RowData[i][3]  = (String)VSelectedOrdNo                .elementAt(i);
               RowData[i][4]  = common.parseDate((String)VSelectedDate.elementAt(i));                 
               RowData[i][5]  = (String)VSelectedCode                 .elementAt(i);
               RowData[i][6]  = (String)VSelectedName                 .elementAt(i);
               RowData[i][7]  = (String)VSelectedQty                  .elementAt(i);
               RowData[i][8]  = (String)VSelectedNet                  .elementAt(i);
               RowData[i][9]  = "";
               RowData[i][10] = "";
          }
     }

     public void setRowData1()
     {
          RowData     = new Object[VSelectedCode.size()][11];

          for(int i=0;i<VSelectedCode.size();i++)
          {
               RowData[i][0]  = (String)VSelectedInviceNo                       .elementAt(i);
               RowData[i][1]  = common.parseDate((String)VSelectedInviceDate    .elementAt(i));
               RowData[i][2]  = (String)VSelectedBlock                          .elementAt(i);
               RowData[i][3]  = (String)VSelectedOrdNo                          .elementAt(i);
               RowData[i][4]  = common.parseDate((String)VSelectedDate          .elementAt(i));
               RowData[i][5]  = (String)VSelectedCode                           .elementAt(i);
               RowData[i][6]  = (String)VSelectedName                           .elementAt(i);
               RowData[i][7]  = (String)VSelectedQty                            .elementAt(i);
               RowData[i][8]  = (String)VSelectedNet                            .elementAt(i);
               RowData[i][9]  = (String)VSelectedAdvPer                         .elementAt(i);
               RowData[i][10] = (String)VSelectedAdvanceAmt                     .elementAt(i);
          }
     }

     public void getOrderDetails(String SSAdvSlNo,int iBlockSig)
     {
          VSelectedInviceNo   = new Vector();
          VSelectedInviceDate = new Vector();
          VSelectedBlock      = new Vector();
          VSelectedOrdNo      = new Vector();
          VSelectedDate       = new Vector();
          VSelectedCode       = new Vector();
          VSelectedName       = new Vector();
          VSelectedQty        = new Vector();
          VSelectedNet        = new Vector();
          VSelectedAdvPer     = new Vector();
          VSelectedAdvanceAmt = new Vector();
          
          String QString = iBlockSig==9?getWQString(SSAdvSlNo):getQString(SSAdvSlNo);
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(QString);

               while (res.next())
               {
                    int i = common.toInt(res.getString(14));
                    if(i!=iMillCode)
                         continue;
                    
                    VSelectedInviceNo             .addElement(common.parseNull(res.getString(1)));
                    VSelectedInviceDate           .addElement(common.parseNull(res.getString(2)));
                    VSelectedBlock                .addElement(common.parseNull(res.getString(3)));
                    VSelectedOrdNo                .addElement(common.parseNull(res.getString(4)));
                    VSelectedDate                 .addElement(common.parseNull(res.getString(5)));
                    VSelectedCode                 .addElement(common.parseNull(res.getString(6)));
                    VSelectedName                 .addElement(common.parseNull(res.getString(15)));
                    VSelectedQty                  .addElement(common.parseNull(res.getString(8)));
                    VSelectedNet                  .addElement(common.parseNull(res.getString(9)));
                    VSelectedAdvPer               .addElement(common.parseNull(res.getString(10)));
                    VSelectedAdvanceAmt           .addElement(common.parseNull(res.getString(11)));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public String getQString(String SSAdvSlNo)
     {
          String QString  = "";
          
          QString = " SELECT AdvRequest.PInvNo, AdvRequest.PInvDate, OrdBlock.BlockName, AdvRequest.OrderNo, AdvRequest.OrderDate,InvItems.ITEM_Code, UOM.UOMName, AdvRequest.Qty, AdvRequest.InvValue, AdvRequest.AdvPer, AdvRequest.Advance,AdvRequest.Plus,AdvRequest.Less,AdvRequest.MillCode,InvItems.ITEM_Name"+
                    " FROM (AdvRequest INNER JOIN OrdBlock ON AdvRequest.OrderBlock=OrdBlock.Block) "+
                    " INNER JOIN InvItems ON AdvRequest.Code=InvItems.ITEM_CODE "+
                    " INNER JOIN UOM ON UOM.UOMCode=InvItems.UOMCODE "+
                    " Where AdvRequest.AdvSlNo = "+SAdvSlNo+
                    " Order By 3,4 ";

          return QString;
     }
     
     public String getWQString(String SSAdvSlNo)
     {
          String QString  = "";
          
          QString = " SELECT AdvRequest.PInvNo, AdvRequest.PInvDate, 'WO', AdvRequest.OrderNo, AdvRequest.OrderDate,AdvRequest.Descript, ' ', AdvRequest.Qty, AdvRequest.InvValue, AdvRequest.AdvPer, AdvRequest.Advance, AdvRequest.Plus,AdvRequest.Less,AdvRequest.MillCode,AdvRequest.Descript "+
                    " FROM AdvRequest "+
                    " Where AdvRequest.AdvSlNo = "+SAdvSlNo+
                    " Order By 3,4 ";

          return QString;
     }
}
