package Order;

import java.io.*;
import java.util.*;
import java.sql.*;

import jdbc.*;
import guiutil.*;
import util.*;


//pdf


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;

public class AdvReqPrint
{
     FileWriter FW;
     String SAdvSlNo,SDate,SSupCode,SSupName;
     int iMillCode;
     String SMillName;
     int iTotalColumns = 10;
    int iWidth[] = {9,8,10,13,35, 8, 12,15,13,15};
    
     Vector VPINo,VPIDate,VBlock,VOrdNo,VOrdDate,VAName,VUom,VQty,VPIValue,VAdvPer,VAdvance;
     double dTAdvance=0,dPlus=0;
      Document document;
    PdfPTable table, table1;
     Common common = new Common();
     int Lctr      = 100;
     int Pctr      = 0;
//Georgia Regular

       private static Font bigBold = FontFactory.getFont("GEORGIS_REGULAR", 12, Font.BOLD | Font.UNDERLINE);  
     private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 9, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
       String SOrdNo="";
        String SPINo ="";
        String SPIDate="";
        String SOrdDate  = "";
     AdvReqPrint(FileWriter FW,String SAdvSlNo,String SDate,String SSupCode,String SSupName,int iMillCode,String SMillName)
     {
          this.FW         = FW;
          this.SAdvSlNo   = SAdvSlNo;
          this.SDate      = SDate;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.iMillCode  = iMillCode;
          this.SMillName  = SMillName;
          
          setDataIntoVector();
          setAdvHead();
          setAdvBody();
          setAdvFoot(0);
     }

     //pdf

     AdvReqPrint(Document document,String SAdvSlNo,String SDate,String SSupCode,String SSupName,int iMillCode,String SMillName)
     {
          this.document   = document;
          this.SAdvSlNo   = SAdvSlNo;
          this.SDate      = SDate;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.iMillCode  = iMillCode;
          this.SMillName  = SMillName;
          try{
          setDataIntoVector();
          setPDFHead();
          setPDFBody();
          setPDFFoot(0);
          document.add(table);
          }catch(Exception ex){}
     } 
     
     public void setAdvHead()
     {
          if(Lctr < 43)
          return;
          
          if(Pctr > 0)
          setAdvFoot(1);
          
          Pctr++;
          
          String Strl1 = "";
          
          Strl1 = ""+SMillName+"g";

          String Strl1a= " ";
          String Strl2  = "|-----------------------------------------------------------------------------------------------------------------------------|";
          String Strl3  = "|                                        REQUEST FOR ADVANCE PAYMENT                                                          |";
          String Strl4  = "|-----------------------------------------------------------------------------------------------------------------------------|";
          String Strl5  = "| Sl.No         |"+common.Pad(SAdvSlNo,15)+"|                                                            | Date          |"+common.Pad(SDate,16)+"|";
          String Strl6  = "|-----------------------------------------------------------------------------------------------------------------------------| ";
          String Strl7  = "| Name : "+common.Pad(SSupName,40)+common.Space(77)+"|";
          String Strl8  = "|-----------------------------------------------------------------------------------------------------------------------------|";
          String Strl9  = "| No| Proforma Invoice |     Purchase Order     |                              |     |          | Proforma  |Advance| Advance |";  
          String Strl10 = "|   |-------------------------------------------|      Description of Goods    | Uom | Quantity |  Invoice  | Amount|  Amount |";
          String Strl11 = "|   |   No  |  Date    |Block|   No  |   Date   |                              |     |          |   Value   |   %   |    Rs.P.|";
          String Strl12 = "|-----------------------------------------------------------------------------------------------------------------------------|";
          
          try
          {
               FW.write(Strl1+"\n");
               FW.write(Strl1a+"\n");
               FW.write(Strl2+"\n");
               FW.write(Strl3+"\n");
               FW.write(Strl4+"\n");
               FW.write(Strl5+"\n");
               FW.write(Strl6+"\n");
               FW.write(Strl7+"\n");
               FW.write(Strl8+"\n");
               FW.write(Strl9+"\n");
               FW.write(Strl10+"\n");
               FW.write(Strl11+"\n");
               FW.write(Strl12+"\n");
               Lctr = 13;
          }
          catch(Exception ex)
          {
          }
     }
     
     public void setAdvBody()
     {
          int ctr=0;
          for(int i=0;i<VOrdNo.size();i++)
          {
               setAdvHead();
                SPINo     = common.Pad((String)VPINo.elementAt(i),7);
                SPIDate   = common.Pad(common.parseDate((String)VPIDate.elementAt(i)),10);
               String SBlock    = common.Pad((String)VBlock.elementAt(i),5);
                SOrdNo    = common.Rad((String)VOrdNo.elementAt(i),7);
                SOrdDate  = common.Pad(common.parseDate((String)VOrdDate.elementAt(i)),10);
               String SMName    = common.Pad((String)VAName.elementAt(i),30);
               String SUom      = common.Pad((String)VUom.elementAt(i),5);
               String SQty      = common.Rad(common.getRound((String)VQty.elementAt(i),3),10);
               String SPIValue  = common.Rad(common.getRound((String)VPIValue.elementAt(i),2),11);
               String SAdvPer   = common.Rad(common.getRound((String)VAdvPer.elementAt(i),2),7);
               String SAdvance  = common.Rad(common.getRound((String)VAdvance.elementAt(i),2),9);
               dTAdvance        = dTAdvance+common.toDouble(SAdvance);
               
               try
               {
                    String Strl = "|"+common.Pad(String.valueOf(ctr+1),3)+"|"+SPINo+"|"+SPIDate+"|"+
                    SBlock+"|"+SOrdNo+"|"+SOrdDate+"|"+SMName+"|"+SUom+"|"+
                    SQty+"|"+SPIValue+"|"+SAdvPer+"|"+SAdvance+"|";
                    
                    FW.write(Strl+"\n");
                    Lctr = Lctr++;
                    ctr++;
               }
               catch(Exception ex){}
          }
     }

     public void setAdvFoot(int iSig)
     {
          String STAdvance = common.Rad(common.getRound(dTAdvance,2),9);
          String STPlus    = common.Rad(common.getRound(dPlus,2),9);
          String STGrand   = common.Rad(common.getRound(dTAdvance+dPlus,2),9);
          
          String Strl1  = "|-----------------------------------------------------------------------------------------------------------------------------|";
          String Strl2  = "|                                                                                                      Sub Total    |"+STAdvance+"|";
          String Strl3  = "|                                                                                                      Others       |"+STPlus+"|";
          String Strl4  = "|-----------------------------------------------------------------------------------------------------------------------------|"; 
          String Strl5  = "|                                                                                                      Grand Total  |"+STGrand+"|";
          String Strl6  = "|-----------------------------------------------------------------------------------------------------------------------------|"; 
          String Strl7  = "|                                   |                CHEQUE                  |                 DEMAND DRAFT                   |";
          String Strl8  = "|            Mode of Payment        |-----------------------------------------------------------------------------------------|";
          String Strl9  = "|                                   |                 BANK                   |                    DIRECT                      |";
          String Strl10 = "|-----------------------------------------------------------------------------------------------------------------------------|";
          String Strl11 = "|                                                                                                                             |";
          String Strl12 = "|                                                                                                                             |";
          String Strl13 = "|                                                                                                                             |";
          String Strl14 = "|                                                                                                                             |";
          String Strl15 = "|        S.O.                         DM(A)                            M(O)/MCA                             JMD               |";
          String Strl16 = "------------------------------------------------------------------------------------------------------------------------------|";
          
          String Strl18 = "";
          if(iSig==0)
               Strl18= "< End Of Report >";
          else
               Strl18= "(Continued on Next Page) ";
          try
          {
               FW.write( Strl1+"\n");       
               FW.write( Strl2+"\n");       
               FW.write( Strl3+"\n");
               FW.write( Strl4+"\n");       
               FW.write( Strl5+"\n");       
               FW.write( Strl6+"\n");       
               FW.write( Strl7+"\n");       
               FW.write( Strl8+"\n");       
               FW.write( Strl9+"\n");       
               FW.write( Strl10+"\n");       
               FW.write( Strl11+"\n");       
               FW.write( Strl12+"\n");       
               FW.write( Strl13+"\n");       
               FW.write( Strl14+"\n");       
               FW.write( Strl15+"\n");       
               FW.write( Strl16+"\n");
               FW.write( Strl18+"\n");       
          }
          catch(Exception ex){}
     }
     
     public void setDataIntoVector()
     {
          VPINo    = new Vector();
          VPIDate  = new Vector();
          VBlock   = new Vector();
          VOrdNo   = new Vector();
          VOrdDate = new Vector();
          VAName   = new Vector();
          VUom     = new Vector();
          VQty     = new Vector();
          VPIValue = new Vector();
          VAdvPer  = new Vector();
          VAdvance = new Vector();
          dPlus    = 0;
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();

               String QString = getQString();
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    VPINo       .addElement(res.getString(1));
                    VPIDate     .addElement(res.getString(2));
                    VBlock      .addElement(res.getString(3));
                    VOrdNo      .addElement(res.getString(4));
                    VOrdDate    .addElement(common.parseNull(res.getString(5)));
                    VAName      .addElement(res.getString(6));
                    VUom        .addElement(res.getString(7));
                    VQty        .addElement(res.getString(8));
                    VPIValue    .addElement(res.getString(9));
                    VAdvPer     .addElement(res.getString(10));
                    VAdvance    .addElement(res.getString(11));
                    dPlus       = res.getDouble(12)-res.getDouble(13);
               }

               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public String getQString()
     {
          String QString  = "";
          
          QString = " select InvNo,InvDate,BName,OrdNo,OrdDate,IName,UName,Quan,InVal,APer,"+
                    " Adv,Plus,sub,MCode"+
                    " from"+
                    " (SELECT AdvRequest.PInvNo as InvNo, AdvRequest.PInvDate as InvDate,"+
                    " OrdBlock.BlockName as BName,AdvRequest.OrderNo as OrdNo,"+
                    " AdvRequest.OrderDate as OrdDate,InvItems.ITEM_NAME as IName,"+
                    " UOM.UOMName as UName,AdvRequest.Qty as Quan, AdvRequest.InvValue as InVal,"+
                    " AdvRequest.AdvPer as APer,AdvRequest.Advance as Adv,AdvRequest.Plus as Plus,"+
                    " AdvRequest.Less as sub,AdvRequest.MillCode as MCode"+
                    " FROM (AdvRequest INNER JOIN OrdBlock ON AdvRequest.OrderBlock=OrdBlock.Block)"+
                    " INNER JOIN InvItems ON AdvRequest.Code=InvItems.ITEM_CODE"+
                    " INNER JOIN UOM ON UOM.UOMCode=InvItems.UOMCODE"+
                    " Where AdvRequest.AdvSlNo = "+SAdvSlNo+" and AdvRequest.OrderBlock<9 and AdvRequest.MillCode="+iMillCode+
                    " union all"+
                    " SELECT AdvRequest.PInvNo as InvNo,AdvRequest.PInvDate as InvDate,"+
                    " 'WO' as BName,AdvRequest.OrderNo as OrdNo,"+
                    " AdvRequest.OrderDate as OrdDate,AdvRequest.Descript as IName,"+
                    " ' ' as UName, AdvRequest.Qty as Quan,AdvRequest.InvValue as InVal,"+
                    " AdvRequest.AdvPer as APer,AdvRequest.Advance as Adv, AdvRequest.Plus as Plus,"+
                    " AdvRequest.Less as sub,AdvRequest.MillCode as MCode"+
                    " FROM AdvRequest"+
                    " Where AdvRequest.AdvSlNo = "+SAdvSlNo+" and AdvRequest.OrderBlock=9 and AdvRequest.MillCode="+iMillCode+")"+
                    " Order By 3,4";

          return QString;
     }

//pdf head

 private void setPDFHead(){
				try{
				table  = new PdfPTable(10);
				table  . setWidths(iWidth);
				table  . setWidthPercentage(100);
				document .  newPage();  
				AddCellIntoTable(SMillName, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 10,30f, 0, 0, 0, 0, bigBold);

System.out.println("SMillName"+SMillName);

                if(SMillName.equals("AMARJOTHI SPINNING MILLS LTD"))
                { 
              //	AddCellIntoTable("PUDHUSURIPALAYAM ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 7,30f, 0, 0, 0, 0, bigBold);
                AddCellIntoTable("NAMBIYUR ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 10,30f, 0, 0, 0, 0, bigBold);
                }else{

               	AddCellIntoTable("SIPCOT,PERUNDURAI", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 10,30f, 0, 0, 0, 0, bigBold);
               // AddCellIntoTable("PERUNDURAI -638 052 ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 7,30f, 0, 0, 0, 0, bigBold);
                }
             

				AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 10, 0, 0, 0, 0, bigbold);
				AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 10, 0, 0, 0, 0, bigbold);

				AddCellIntoTable("REQUEST FOR ADVANCE PAYMENT   ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 10,30f, 4, 1, 8, 2, mediumbold); 




				AddCellIntoTable("S.NO", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,20f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable(SAdvSlNo, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,20f,4, 1, 8, 2, mediumbold);  
				AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 4, 20f,4, 1, 8, 2, mediumbold); 
				AddCellIntoTable("Date ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 20f,4, 1, 8, 2, mediumbold);
				AddCellIntoTable(SDate, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 2, mediumbold);  

				AddCellIntoTable("NAME  :"+SSupName, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 10,30f, 4, 1, 8, 2, mediumbold); 



    }catch(Exception ex){}
 }

private void setPDFBody(){

String STAdvance ="";
String STPlus    ="";
String STGrand   = "";
          
				AddCellIntoTable("Proforma Invoice No", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 4,20f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable((String)VPINo.elementAt(0), table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,20f,4, 1, 8, 2, mediumbold);  
				AddCellIntoTable("Date", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,20f, 4, 1, 8, 2, mediumbold); 
				AddCellIntoTable(SPIDate, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,20f, 4, 1, 8, 2, mediumbold);

				/*AddCellIntoTable("Purchase Order No", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,20f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable((String)VOrdNo.elementAt(0), table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,20f, 4, 1, 8, 2, smallnormal);  
				AddCellIntoTable("Date", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,20f, 4, 1, 8, 2, mediumbold); 
				AddCellIntoTable(common.parseDate((String)VOrdDate.elementAt(0)), table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,20f, 4, 1, 8, 2, smallnormal);*/  


             
				AddCellIntoTable("SL. NO", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 20f,4, 1, 8, 0, mediumbold,2);
                AddCellIntoTable("Block", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 0, mediumbold,2); 
                AddCellIntoTable("Purchase Order", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 20f,4, 1, 8, 2, mediumbold,2); 
                
				AddCellIntoTable("Description of Goods", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 0, mediumbold,2);  
				AddCellIntoTable("UOM", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 0, mediumbold,2);
				
				AddCellIntoTable("Quantity", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 0, mediumbold,2);  
				AddCellIntoTable("Proforma ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 0, mediumbold,2);
				AddCellIntoTable("Advance Amount ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,20f, 4, 1, 8, 2, smallbold,2);  


                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 20f,4, 0, 8, 2, mediumbold);
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 0, 0, 8, 2, mediumbold);
				AddCellIntoTable("No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 0, 0, 8, 2, mediumbold);  
                AddCellIntoTable("Date", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 20f,4, 0, 8, 2, mediumbold);
				  
			    AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 0, 0, 8, 2, mediumbold);
				AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 0, 0, 8, 2, mediumbold);  
                AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 0, 0, 8, 2, mediumbold);  
				AddCellIntoTable(" Invoice Value", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,20f, 0, 0, 8, 2, mediumbold);
				AddCellIntoTable(" % ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 0, 0, 8, 2, smallbold);  
                AddCellIntoTable(" Rs ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,20f, 0, 0, 8, 2, smallbold);  


               
				
				
			
            for(int i=0;i<VOrdNo.size();i++)
            {

          
              // setAdvHead();
                SPINo     = common.Pad((String)VPINo.elementAt(i),7);
                SPIDate   = common.Pad(common.parseDate((String)VPIDate.elementAt(i)),10);
               String SBlock    = common.Pad((String)VBlock.elementAt(i),5);
                SOrdNo    = common.Rad((String)VOrdNo.elementAt(i),7);
                SOrdDate  = common.Pad(common.parseDate((String)VOrdDate.elementAt(i)),10);
               String SMName    = common.Pad((String)VAName.elementAt(i),30);
               String SUom      = common.Pad((String)VUom.elementAt(i),8);
               String SQty      = common.Rad(common.getRound((String)VQty.elementAt(i),3),10);
               String SPIValue  = common.Rad(common.getRound((String)VPIValue.elementAt(i),2),11);
               String SAdvPer   = common.Rad(common.getRound((String)VAdvPer.elementAt(i),2),7);
               String SAdvance  = common.Rad(common.getRound((String)VAdvance.elementAt(i),2),9);
               dTAdvance        = dTAdvance+common.toDouble(SAdvance);
  
			   STAdvance = common.Rad(common.getRound(dTAdvance,2),9);
			   STPlus    = common.Rad(common.getRound(dPlus,2),9);
		       STGrand   = common.Rad(common.getRound(dTAdvance+dPlus,2),9);

				AddCellIntoTable(String.valueOf(i+1), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
                 AddCellIntoTable(SBlock, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
                AddCellIntoTable(SOrdNo, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
               
				AddCellIntoTable(SOrdDate, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
              
				AddCellIntoTable(SMName, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(SUom, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
				 
				
				AddCellIntoTable(SQty, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
				AddCellIntoTable(SPIValue, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
				AddCellIntoTable(SAdvPer, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
				AddCellIntoTable(SAdvance, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  

          }

        
				AddCellIntoTable("Sub Total", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 9,20f, 4, 1, 8, 2, smallbold);
				AddCellIntoTable(STAdvance, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 2, smallbold);  

				AddCellIntoTable("Others", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 9,20f, 4, 1, 8, 2, smallbold);
				AddCellIntoTable(STPlus, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 2, smallbold);  

				AddCellIntoTable("GRAND TOTAL", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 9, 4, 1, 8, 2, bigbold);
				AddCellIntoTable(STGrand, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, bigbold);  

 }

private void setPDFFoot(int iSig){

				AddCellIntoTable("Mode Of Payment", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 4, 30f,4, 1, 8, 2, smallbold);


				AddCellIntoTable("CHEQUE", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,30f, 4, 1, 8, 2, smallbold);
				AddCellIntoTable("DD", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,30f, 4, 1, 8, 2, smallbold);  

				//AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 4,30f, 4, 1, 8, 2, smallbold);
				AddCellIntoTable("RTGS", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,30f, 4, 1, 8, 2, smallbold);  
				AddCellIntoTable("CASH", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,30f, 4, 1, 8, 2, smallbold);


			


				AddCellIntoTable("", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 5,30f, 4, 0, 8, 0, smallbold);
				AddCellIntoTable("", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 5,30f, 0, 0, 8, 0, smallbold);  

				AddCellIntoTable("", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 5,30f, 4, 0, 8, 0, smallbold);
				AddCellIntoTable("", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 5,30f, 0, 0, 8, 0, smallbold);  

				
    


			//	AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,30f, 4, 0, 0, 2, smallbold);
				AddCellIntoTable("S.O", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 5,30f, 4, 0, 8, 2, smallbold);  

			//	AddCellIntoTable(" ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,30f, 0, 0, 0, 2, smallbold);
				AddCellIntoTable("JMD", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 5,30f, 0, 0, 8, 2, smallbold);  


 }


public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    } 
}
