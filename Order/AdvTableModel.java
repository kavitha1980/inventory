package Order;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class AdvTableModel extends DefaultTableModel
{
     Object    RowData[][],ColumnNames[],ColumnType[];
     JLabel    LNet,LAdvance;
     NextField TPlus,TMinus;
     Common common = new Common();
     public AdvTableModel(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType,JLabel LNet,JLabel LAdvance,NextField TPlus,NextField TMinus)
     {
          super(RowData,ColumnNames);
          this.RowData     = RowData;
          this.ColumnNames = ColumnNames;
          this.ColumnType  = ColumnType;
          this.LNet        = LNet;
          this.LAdvance    = LAdvance;
          this.TPlus       = TPlus;
          this.TMinus      = TMinus;
     
          for(int i=0;i<super.dataVector.size();i++)
          {
               Vector curVector = (Vector)super.dataVector.elementAt(i);
               setMaterialAmount(curVector);
          }
     }

     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
               if(column>=8 && column<=9)
               setMaterialAmount(rowVector);
          }
          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }

     public void setMaterialAmount(Vector RowVector)
     {
          double dTNet=0,dTAdvance=0;
          double dPlus       = common.toDouble(TPlus.getText())-common.toDouble(TMinus.getText());
          double dValue      = common.toDouble((String)RowVector.elementAt(8));
          double dAdvPer     = common.toDouble((String)RowVector.elementAt(9));
     
          double dAdvance    = dValue*dAdvPer/100;
     
          RowVector.setElementAt(common.getRound(dAdvance,2),10);
          for(int i=0;i<super.dataVector.size();i++)
          {
               Vector    curVector      = (Vector)super.dataVector.elementAt(i);
                         dTNet          = dTNet+common.toDouble((String)curVector.elementAt(8));
                         dTAdvance      = dTAdvance+common.toDouble((String)curVector.elementAt(10));
          }
          dTAdvance=dTAdvance+dPlus;
          LNet.setText(common.getRound(dTNet,2));
          LAdvance.setText(common.getRound(dTAdvance,2));
     }

     public void setTotal()
     {
          double dTNet=0,dTAdvance=0;
          double dPlus       = common.toDouble(TPlus.getText())-common.toDouble(TMinus.getText());
          for(int i=0;i<super.dataVector.size();i++)
          {
               Vector    curVector = (Vector)super.dataVector.elementAt(i);
                         dTNet     = dTNet+common.toDouble((String)curVector.elementAt(8));
                         dTAdvance = dTAdvance+common.toDouble((String)curVector.elementAt(10));
          }
          dTAdvance =    dTAdvance+dPlus;
          LNet      .setText(common.getRound(dTNet,2));
          LAdvance  .setText(common.getRound(dTAdvance,2));
     }

     public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }

     public boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="B" || ColumnType[col]=="E")
               return true;
          return false;
     }

     public Object[][] getFromVector()
     {
          Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
          for(int i=0;i<super.dataVector.size();i++)
          {
               Vector curVector = (Vector)super.dataVector.elementAt(i);
               for(int j=0;j<curVector.size();j++)
                    FinalData[i][j] = (String)curVector.elementAt(j);
          }
          return FinalData;
     }

     public int getRows()
     {
          return super.dataVector.size();
     }

     public Vector getCurVector(int i)
     {
          return (Vector)super.dataVector.elementAt(i);
     }
}
