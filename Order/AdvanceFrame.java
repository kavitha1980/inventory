package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class AdvanceFrame extends JInternalFrame
{
     JButton             BSupplier,BApply,BOk,BCancel;
     JPanel              TopPanel;
     JPanel              BottomPanel;
     JTextField          TSupCode;

     JLayeredPane        DeskTop;
     StatusPanel         SPanel;
     TabReport           tabreport;
     
     Object              RowData[][];
     String              ColumnData[] = {"Order No","Date","Basic","Discount","CenVat","Tax","Surcharge","Plus","Minus","Net","Mark to Pass"};
     String              ColumnType[] = {"N","S","N","N","N","N","N","N","N","N","B"};
     Common              common       = new Common();
     
     Vector              VOrdNo,VOrdDate,VOrdFlag,VBasic,VDisc,VCenVat,VTax,VSur,VAdd,VLess,VNet;
     Vector              VSOrdNo,VSOrdFlag;
     Vector              VSelectedId,VSelectedBlockCode,VSelectedBlock,VSelectedOrdNo,VSelectedDate,VSelectedCode,VSelectedName,VSelectedQty,VSelectedNet,VSelectedOrdSlNo;
     
     Vector              VCode,VName;
     int                 iMillCode,iUserCode;
     String              SSupCode  = "",SSupName  = "";
     String              SYearCode;
     String              SItemTable,SSupTable;

     
     public AdvanceFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,int iUserCode,String SYearCode,String SItemTable,String SSupTable)
     {
          super("List of Orders Pending for this supplier");

          this.DeskTop    = DeskTop;
          this.SPanel     = SPanel;
          this.VCode      = VCode;
          this.VName      = VName;
          this.iMillCode  = iMillCode;
          this.iUserCode  = iUserCode;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          BSupplier      = new JButton("Select Supplier");
          BApply         = new JButton("Apply");
          BOk            = new JButton("Pass Request for Advance Payment");
          BCancel        = new JButton("Cancel");
          TSupCode       = new JTextField();
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();

          BOk            . setEnabled(false);
     }    
     
     public void setLayouts()
     {
          TopPanel  . setLayout(new FlowLayout());

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }    
     
     public void addComponents()
     {
          TopPanel            . add(BSupplier);
          TopPanel            . add(BApply);
          
          BottomPanel         . add(BOk);
          BottomPanel         . add(BCancel);
     
          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BSupplier . addActionListener(new SupplierSearch(DeskTop,TSupCode,SSupTable));
          BApply    . addActionListener(new ApplyList());
          BOk       . addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setSelectedRecords();
               try
               {
                    AdvanceRequestFrame ARF = new AdvanceRequestFrame(DeskTop,VCode,VName,SSupCode,SSupName,VSelectedId,VSelectedBlock,VSelectedOrdNo,VSelectedDate,VSelectedCode,VSelectedName,VSelectedBlockCode,VSelectedQty,VSelectedNet,VSelectedOrdSlNo,SPanel,iMillCode,iUserCode,SYearCode,SItemTable,SSupTable);
                    DeskTop   .add(ARF);
                    DeskTop   .repaint();
                    ARF       .setSelected(true);
                    DeskTop   .updateUI();
                    ARF       .show();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public void removeAdvanceFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex){}
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BApply    .setEnabled(false);
               BSupplier .setEnabled(false);
               BOk       .setEnabled(true);

               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport = new TabReport(RowData,ColumnData,ColumnType);
                    getContentPane().add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public void setDataIntoVector()
     {
          VOrdDate     = new Vector();
          VOrdNo       = new Vector();
          VOrdFlag     = new Vector();
          VBasic       = new Vector();
          VDisc        = new Vector();
          VCenVat      = new Vector();
          VTax         = new Vector();
          VSur         = new Vector();
          VAdd         = new Vector();
          VLess        = new Vector();
          VNet         = new Vector();
          
          SSupCode = TSupCode .getText();
          SSupName = BSupplier.getText();
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();

               String QString = getQString(SSupCode);
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    String str1  = res.getString(1);
                    String str2  = res.getString(2);
                    String str3  = res.getString(3);
                    String str4  = res.getString(4);
                    String str5  = res.getString(5);
                    String str6  = res.getString(6);
                    String str7  = res.getString(7);
                    String str8  = res.getString(8);
                    String str9  = res.getString(9);
                    String str11 = res.getString(10);
                    String str12 = res.getString(11);
               
                    str3         = common.parseDate(str3);
               
                    VOrdNo    . addElement(str1);
                    VOrdFlag  . addElement(str2);
                    VOrdDate  . addElement(str3);
                    VBasic    . addElement(str4);
                    VDisc     . addElement(str5);
                    VCenVat   . addElement(str6);
                    VTax      . addElement(str7);
                    VSur      . addElement(str8);
                    VNet      . addElement(str9);
                    VAdd      . addElement(str11);
                    VLess     . addElement(str12);
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VOrdDate.size()][ColumnData.length];
          for(int i=0;i<VOrdDate.size();i++)
          {
               RowData[i][0]  = (String)VOrdNo    .elementAt(i);
               RowData[i][1]  = (String)VOrdDate  .elementAt(i);
               RowData[i][2]  = (String)VBasic    .elementAt(i);
               RowData[i][3]  = (String)VDisc     .elementAt(i);
               RowData[i][4]  = (String)VCenVat   .elementAt(i);
               RowData[i][5]  = (String)VTax      .elementAt(i);
               RowData[i][6]  = (String)VSur      .elementAt(i);
               RowData[i][7]  = (String)VAdd      .elementAt(i);
               RowData[i][8]  = (String)VLess     .elementAt(i);
               double dRNet   = common.toDouble((String)VNet.elementAt(i))+common.toDouble((String)VAdd.elementAt(i))-common.toDouble((String)VLess.elementAt(i));
               RowData[i][9]  = ""+ dRNet;
               RowData[i][10] =  new Boolean(false);
          }  
     }

     public String getQString(String SSupCode)
     {
          String QString  = "";
          
          QString = " SELECT PurchaseOrder.OrderNo,0 as OrdFlag,PurchaseOrder.OrderDate, Sum(PurchaseOrder.qty*rate) AS Expr1, Sum(PurchaseOrder.Disc) AS SumOfDisc, Sum(PurchaseOrder.Cenvat) AS SumOfCenvat, Sum(PurchaseOrder.Tax) AS SumOfTax, Sum(PurchaseOrder.Sur) AS SumOfSur, (Sum(PurchaseOrder.qty*rate))-(Sum(PurchaseOrder.Disc))+(Sum(PurchaseOrder.Cenvat))+(Sum(PurchaseOrder.Tax))+(Sum(PurchaseOrder.Sur)) AS Expr2,PurchaseOrder.Plus, PurchaseOrder.Less, PurchaseOrder.MillCode "+
                    " FROM PurchaseOrder INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block "+
                    " Where PurchaseOrder.Qty>PurchaseOrder.InvQty and PurchaseOrder.millcode ="+iMillCode+
                    " GROUP BY PurchaseOrder.OrderNo,PurchaseOrder.OrderDate,PurchaseOrder.Plus, PurchaseOrder.Less, PurchaseOrder.Sup_Code, PurchaseOrder.MillCode "+
                    " HAVING PurchaseOrder.Sup_Code='"+SSupCode+"'"+
                    " Union All "+
                    " SELECT WorkOrder.OrderNo,1 as OrdFlag,WorkOrder.OrderDate, Sum(WorkOrder.qty*rate) AS Expr1, Sum(WorkOrder.Disc) AS SumOfDisc, Sum(WorkOrder.Cenvat) AS SumOfCenvat, Sum(WorkOrder.Tax) AS SumOfTax, Sum(WorkOrder.Sur) AS SumOfSur, (Sum(WorkOrder.qty*rate))-(Sum(WorkOrder.Disc))+(Sum(WorkOrder.Cenvat))+(Sum(WorkOrder.Tax))+(Sum(WorkOrder.Sur)) AS Expr2,WorkOrder.Plus, WorkOrder.Less, WorkOrder.MillCode "+
                    " FROM WorkOrder "+
                    " Where WorkOrder.Qty>WorkOrder.InvQty and WorkOrder.millcode ="+iMillCode+
                    " GROUP BY WorkOrder.OrderNo, WorkOrder.OrderDate, WorkOrder.Plus, WorkOrder.Less, WorkOrder.Sup_Code, WorkOrder.MillCode "+
                    " HAVING WorkOrder.Sup_Code='"+SSupCode+"'"+
                    " Order By 3";
System.out.println("QString===>"+QString);
          return QString;
     }

     public void setSelectedRecords()
     {
          VSelectedId         = new Vector();
          VSelectedBlock      = new Vector();
          VSelectedOrdNo      = new Vector();
          VSelectedDate       = new Vector();
          VSelectedCode       = new Vector();
          VSelectedName       = new Vector();
          VSelectedBlockCode  = new Vector();
          VSelectedQty        = new Vector();   
          VSelectedNet        = new Vector();
          VSelectedOrdSlNo    = new Vector();

          VSOrdNo             = new Vector();
          VSOrdFlag           = new Vector();
          
          for(int i=0;i<RowData.length;i++)
          {
               Boolean Bselected = (Boolean)RowData[i][10];
               if(Bselected.booleanValue())
               {
                              VSOrdNo   .addElement((String)RowData[i][0]);
                    String    SOrdFlag  = (String)VOrdFlag.elementAt(i);
                    VSOrdFlag           .addElement(SOrdFlag);
               }   
          }
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               for (int i=0;i<VSOrdNo.size();i++)
               {
                    String SOrdNo  = (String)VSOrdNo.elementAt(i);
                    String SBFlag  = (String)VSOrdFlag.elementAt(i);
                    String QString = getQString(SOrdNo,SBFlag);

                    ResultSet res  = stat.executeQuery(QString);
                    while (res.next())
                    {
                         VSelectedOrdNo      . addElement(res.getString(1));
                         VSelectedBlock      . addElement(res.getString(2));
                         VSelectedDate       . addElement(res.getString(3));
                         VSelectedCode       . addElement(res.getString(4));
                         VSelectedName       . addElement(res.getString(5));
                         VSelectedId         . addElement(res.getString(6));
                         VSelectedBlockCode  . addElement(res.getString(7));
                         VSelectedQty        . addElement(res.getString(8));
                         VSelectedNet        . addElement(res.getString(9));
                         VSelectedOrdSlNo    . addElement(res.getString(11));
                    }
                    res  . close();
               }
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public String getQString(String SOrdNo,String SBFlag)
     {
          String QString  = "";
          
          if(common.toInt(SBFlag)==1)
          {
               QString = " SELECT WorkOrder.OrderNo, 'WO', WorkOrder.OrderDate, ' ', WorkOrder.Descript, WorkOrder.Id, 9, WorkOrder.Qty,WorkOrder.Net+WorkOrder.Misc,WorkOrder.MillCode,SlNo"+
                         " FROM WorkOrder "+
                         " Where WorkOrder.OrderNo="+SOrdNo+" AND WorkOrder.Qty > 0 and WorkOrder.MillCode = "+iMillCode+" and WorkOrder.Sup_Code='"+SSupCode+"'"+
                         " and WorkOrder.Qty>WorkOrder.InvQty "+
                         " Order by 6 ";
          }
          else
          {
               QString = " SELECT PurchaseOrder.OrderNo, OrdBlock.BlockName, PurchaseOrder.OrderDate, PurchaseOrder.Item_Code, InvItems.Item_Name, PurchaseOrder.Id, PurchaseOrder.OrderBlock, PurchaseOrder.Qty,PurchaseOrder.Net+PurchaseOrder.Misc,PurchaseOrder.MillCode,SlNo"+
                         " FROM (PurchaseOrder INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block) INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code "+
                         " Where PurchaseOrder.OrderNo="+SOrdNo+" And PurchaseOrder.Qty > 0 and PurchaseOrder.MillCode = "+iMillCode+" and PurchaseOrder.Sup_Code='"+SSupCode+"'"+
                         " and PurchaseOrder.Qty>PurchaseOrder.InvQty "+
                         " Order by 6 ";

          }

          return QString;
     }
}
