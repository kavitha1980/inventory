package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class AdvanceRequestGSTFrame extends JInternalFrame
{
     JPanel                   TopPanel,BottomPanel;
     JButton                  BSupplier;
     JTextField               TSupCode;
     JButton                  BOk,BCancel;
     
     JLayeredPane             DeskTop;
     DateField                TDate;
     NextField                TSlNo;
     StatusPanel              SPanel;
     AdvReqMiddlePanelGST        MiddlePanel;
     Common                   common    = new Common();

     Connection               theConnection  = null;

     Vector                   VCode,VName;
     Vector                   VSelectedId,VSelectedBlock,VSelectedOrdNo,VSelectedDate,VSelectedCode,VSelectedName,VSelectedBlockCode,VSelectedQty,VSelectedNet,VSelectedOrdSlNo,VSelectedInviceNo,VSelectedInviceDate,VSelectedAdvPer,VSelectedAdvanceAmt;
     boolean                  bflag     = false;
     String                   SAdvSlNo  = "";
     String                   SSupCode  = "",SSupName  ="";
     String                   SDate     = "";

     int                      iMillCode,iUserCode;
     String                   SYearCode;
     String                   SItemTable,SSupTable;

     int                      iBlockSig;

     boolean                  bComFlag       = true;
     
     AdvanceRequestGSTFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,String SSupCode,String SSupName,Vector VSelectedId,Vector VSelectedBlock,Vector VSelectedOrdNo,Vector VSelectedDate,Vector VSelectedCode,Vector VSelectedName,Vector VSelectedBlockCode,Vector VSelectedQty,Vector VSelectedNet,Vector VSelectedOrdSlNo,StatusPanel SPanel,int iMillCode,int iUserCode,String SYearCode,String SItemTable,String SSupTable)
     {
          this.DeskTop            = DeskTop;
          this.VCode              = VCode;
          this.VName              = VName;
          this.SSupCode           = SSupCode;
          this.SSupName           = SSupName;
          this.VSelectedId        = VSelectedId;
          this.VSelectedBlock     = VSelectedBlock;
          this.VSelectedOrdNo     = VSelectedOrdNo;
          this.VSelectedDate      = VSelectedDate;
          this.VSelectedCode      = VSelectedCode;
          this.VSelectedName      = VSelectedName;
          this.VSelectedBlockCode = VSelectedBlockCode;
          this.VSelectedQty       = VSelectedQty;
          this.VSelectedNet       = VSelectedNet;
          this.SPanel             = SPanel;
          this.iMillCode          = iMillCode;
          this.iUserCode          = iUserCode;
          this.VSelectedOrdSlNo   = VSelectedOrdSlNo;
          this.SYearCode          = SYearCode;
          this.SItemTable         = SItemTable;
          this.SSupTable          = SSupTable;

          getConn();
          createComponents();   
          setLayouts();
          addComponents();
          setReference();
          addListeners(); 
          show();
     }

     AdvanceRequestGSTFrame(JLayeredPane DeskTop,String SAdvSlNo,int iBlockSig,String SSupName,String SDate,boolean bflag)
     {
          try
          {
               this.DeskTop             = DeskTop;
               this.SAdvSlNo            = SAdvSlNo;
               this.iBlockSig           = iBlockSig;
               this.SSupName            = SSupName;
               this.SDate               = SDate;
               this.bflag               = bflag;

               getConn();
               createComponents();
               setLayouts();
               addComponents();
               setReference();
               addListeners(); 
               show();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void getConn()
     {
          try
          {
               ORAConnection  oraConnection = ORAConnection.getORAConnection();
                              theConnection = oraConnection.getConnection();

          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setReference()
     {
          TSupCode  .setText(SSupCode);
          BSupplier .setText(SSupName);
     }

     public void createComponents()
     {
          BOk            = new JButton("Okay");
          BCancel        = new JButton("Abort");
          BSupplier      = new JButton("Supplier");
          
          TSlNo          = new NextField();
          TDate          = new DateField();
          TSupCode       = new JTextField();

          if(!bflag)
               MiddlePanel    = new AdvReqMiddlePanelGST(DeskTop,VCode,VName,VSelectedId,VSelectedBlock,VSelectedOrdNo,VSelectedDate,VSelectedCode,VSelectedName,VSelectedQty,VSelectedNet);
          else
               MiddlePanel    = new AdvReqMiddlePanelGST(DeskTop,SAdvSlNo,iBlockSig,true,iMillCode);

          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();

          if(!bflag)
               TDate          .setTodayDate();
          else
               TDate.fromString(SDate);

          TSlNo          .setEditable(false);
     }

     public void setLayouts()
     {
          try
          {
               setTitle("Advance Request for Orders Placed Against Selected Supplier");
               setClosable(true);
               setMaximizable(true);
               setIconifiable(true);
               setResizable(true);
               setBounds(0,0,650,500);
               
               getContentPane()    .setLayout(new BorderLayout());
               TopPanel            .setLayout(new GridLayout(3,2,5,5));
               BottomPanel         .setLayout(new FlowLayout());
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("Sl No"));
          TopPanel.add(TSlNo);
          
          TopPanel.add(new JLabel("Date"));
          TopPanel.add(TDate);
          
          TopPanel.add(new JLabel("Supplier"));
          TopPanel.add(BSupplier);
          
          if(!bflag)
          {
               BottomPanel.add(BOk);
               BottomPanel.add(BCancel);
          }
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
          
          getOrderNo();
     }

     public void addListeners()
     {
          BOk.addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
			if(validPer())
			{
                    BOk  . setEnabled(false);

                    if(!bflag)
                         getOrderNo();

                    insertAdvanceDetails();
                    updateOrderDetails();
                    getCommStatus();
                    removeHelpFrame();
			}
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public boolean validPer()
     {
          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
          
          for(int i=0;i<FinalData.length;i++)
          {
               if(common.toDouble(((String)FinalData[i][7]).trim()) == 0)
                    continue;

               if(common.toDouble(((String)FinalData[i][9]).trim())>0)
               {
                    String SOrderNo   = (String)VSelectedOrdNo.elementAt(i);
                    String SSlNo      = (String)VSelectedOrdSlNo.elementAt(i);
                    String SBlockCode = (String)VSelectedBlockCode.elementAt(i);
				double dCurPer    = common.toDouble(((String)FinalData[i][9]).trim());

                    double dOldPer    = getOldPer(SOrderNo,SSlNo,SBlockCode);

				double dTotPer = dOldPer + dCurPer;

                    if(dTotPer>100)
                    {
					String SErr = "Invalid % in Row-"+String.valueOf(i+1)+"\n"+
								"Prev % - "+dOldPer+" ; Cur % - "+dCurPer+" ; Tot % - "+dTotPer;

					JOptionPane.showMessageDialog(null,SErr,"Error",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }
               }
          }
          return true;
     }

     public double getOldPer(String SOrderNo,String SSlNo,String SBlockCode)
     {
          double dPer=0;
          try
          {
               String QS =" Select Sum(AdvPer) from AdvRequest Where OrderNo="+SOrderNo+" and OrderBlock="+SBlockCode+" and OrderSlNo="+SSlNo+" and MillCode="+iMillCode;

               Statement      stat          =  theConnection.createStatement();
               ResultSet      result        =  stat.executeQuery(QS);
               while(result.next())
               {
                    dPer = result.getDouble(1);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return dPer;
     }

     public void insertAdvanceDetails()
     {
          String QString = "Insert Into AdvRequest (id,AdvSlNo,AdvDate,OrderNo,OrderDate,OrderBlock,Code,Descript,Sup_Code,Qty,InvValue,AdvPer,Advance,Plus,Less,PInvNo,PInvDate,creationdate,OrderSlNo,UserCode,MillCode) Values (";
          try
          {
               Statement      stat           =  theConnection.createStatement();

               Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
               
               for(int i=0;i<FinalData.length;i++)
               {
                    if(common.toDouble(((String)FinalData[i][7]).trim()) == 0)
                         continue;

                    if(common.toDouble(((String)FinalData[i][9]).trim())>0)
                    {
                         String    QS1 = QString;
                                   QS1 = QS1+"0"+getNextVal("ADVREQUEST_SEQ")+",";
                                   QS1 = QS1+"0"+TSlNo.getText()+",";
                                   QS1 = QS1+"'"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                                   QS1 = QS1+"0"+((String)FinalData[i][3]).trim()+",";
                                   QS1 = QS1+"'"+(String)VSelectedDate.elementAt(i)+"',";
                                   QS1 = QS1+"0"+(String)VSelectedBlockCode.elementAt(i)+",";
                                   QS1 = QS1+"'"+((String)FinalData[i][5]).trim()+"',";
                                   QS1 = QS1+"'"+((String)FinalData[i][6]).trim()+"',";
                                   QS1 = QS1+"'"+TSupCode.getText()+"',";
                                   QS1 = QS1+"0"+((String)FinalData[i][7]).trim()+",";
                                   QS1 = QS1+"0"+((String)FinalData[i][8]).trim()+",";
                                   QS1 = QS1+"0"+((String)FinalData[i][9]).trim()+",";
                                   QS1 = QS1+"0"+((String)FinalData[i][10]).trim()+",";
                                   QS1 = QS1+"0"+MiddlePanel.MiddlePanel.TPlus.getText()+",";
                                   QS1 = QS1+"0"+MiddlePanel.MiddlePanel.TMinus.getText()+",";
                                   QS1 = QS1+"'"+((String)FinalData[i][0]).trim()+"',";
                                   QS1 = QS1+"'"+common.pureDate(((String)FinalData[i][1]).trim())+"',";
                                   QS1 = QS1+"'"+common.getServerDate()+"',";
                                   QS1 = QS1+"0"+(String)VSelectedOrdSlNo.elementAt(i)+",";
                                   QS1 = QS1+"0"+iUserCode+",";
                                   QS1 = QS1+"0"+iMillCode+")";

                         System.out.println(QS1);

                         if(theConnection    . getAutoCommit())
                                   theConnection  . setAutoCommit(false);

                         stat.executeUpdate(QS1);
                    }
               }
               stat . close();
          }
          catch(Exception ex)
          {
               bComFlag  = false;
               ex.printStackTrace();
          }
     }

     public void updateOrderDetails()
     {
          try
          {
               Statement      stat           =  theConnection.createStatement();

               Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
               String QS1="";
               for(int i=0;i<FinalData.length;i++)
               {
                    if(common.toDouble(((String)FinalData[i][7]).trim()) == 0)
                         continue;
                    int iBlockCode = common.toInt((String)VSelectedBlockCode.elementAt(i));
                    
                    if(common.toInt(((String)FinalData[i][9]).trim())>0)
                    {
                         QS1 = (iBlockCode==9?"Update WorkOrder Set ": "Update PurchaseOrder Set ");
                         QS1 = QS1+"AdvSlNo  = 0"+TSlNo.getText()+" Where ID = "+(String)VSelectedId.elementAt(i);

                         System.out.println(QS1);

                         if(theConnection    . getAutoCommit())
                                   theConnection  . setAutoCommit(false);

                         stat.executeUpdate(QS1);
                    }
               }

               if(!bflag)
               {
                    String QS2 = " Update config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1  where Id = 6";
     
                    if(theConnection  . getAutoCommit())
                         theConnection  . setAutoCommit(false);
     
                    stat.executeUpdate(QS2);
               }
               stat . close();
          }
          catch(Exception ex)
          {
               bComFlag  = false;
               ex.printStackTrace();
          }
     }

     public void getOrderNo()
     {
          if(!bflag)
          {
               String QString = "  Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 6";
     
               try
               {
                    Statement      stat           =  theConnection.createStatement();
                    ResultSet      result         =   stat.executeQuery(QString);
     
                    result.next();
                    String SSlNo = result.getString(1);
     
                    TSlNo.setText(SSlNo);

                    result    . close();
                    stat      . close();
               }catch(Exception Ex){}
          }
          else
                    TSlNo.setText(SAdvSlNo);

     }

     private int getNextVal(String SSequence) throws Exception
     {
          int       iValue    = -1;
          String    QS        = "Select "+SSequence+".NextVal from dual";
          try
          {
               Statement       stat          =  theConnection.createStatement();
               
               ResultSet result = stat.executeQuery(QS);
               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("getNextVal :"+SSequence+ex);
          }
          return iValue;
     }

     private int getBlockCode(String SSBlockName)
     {
          int iBlockCode = 0;

          String QS = "Select Block,BlockName from OrdBlock";
          try
          {
               Statement       stat          =  theConnection.createStatement();

               ResultSet result = stat.executeQuery(QS);
               if(result.next())
               {
                    if(SSBlockName.equals(((String)result.getString(2))))
                         iBlockCode     = common.toInt(((String)result.getString(1)));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
          }
          return iBlockCode;
     }

     private void getCommStatus()
     {
          try
          {
               if(bComFlag)
               {
                    theConnection  . commit();
                    System         . out.println("Commit");
                    JOptionPane    . showMessageDialog(null,"Successfully Saved","Attention",JOptionPane.INFORMATION_MESSAGE);
               }
               else
               {
                    theConnection  . rollback();
                    System         . out.println("RollBack");
                    JOptionPane    . showMessageDialog(null,"Problem in AdvRequest, the Entered Data is not Saved","Attention",JOptionPane.INFORMATION_MESSAGE);
               }
               theConnection  . setAutoCommit(true);
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
