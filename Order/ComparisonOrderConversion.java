package Order;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.*;

class CompConversion
{
     int        iMillCode,iUserCode;
     String     SItemCode     = null,SYearCode;
     Connection theConnection = null;
     Vector    VOrderGroup;

     public CompConversion(int iMillCode,int iUserCode)
     {
          this.iMillCode     = iMillCode;
          this.iUserCode     = iUserCode;

          setComparisonData();
          storeOrderData();
     }
     public boolean storeOrderData()
     {
          boolean bFlag=true;
          int iOrderNo=0;


          try
          {
               String SDate = getServerDate();

               Class.forName("oracle.jdbc.OracleDriver");
               theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
               theConnection.setAutoCommit(false);

               for(int i=0; i<VOrderGroup.size();       i++)
               {
                    int iSlNo=1;

                    OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);

                    String SYearCodeQS= " select yearCode from Invperiod"+
                                        " where (select to_Char(Sysdate,'YYYYMMDD')from dual)>=startDate"+
                                        " and (select to_Char(Sysdate,'YYYYMMDD') from dual)<=EndDate";
                    Statement theStatement       = theConnection.createStatement();

                    ResultSet theResult          = theStatement.executeQuery(SYearCodeQS);

                    String SSupCode = ordergroup.SSupCode;
                    String SSupName = ordergroup.SSupName;
                    String SMailId  = ordergroup.SMailId;


                    if(theResult.next())
                    {

                         SYearCode= theResult.getString(1);

                    }

                    theResult.close();

                    String SOrderNoQS = " Select  maxno From Config"+iMillCode+""+SYearCode+" where Id=1 for update of MaxNo noWait";
                    theResult          = theStatement.executeQuery(SOrderNoQS);

                    if(theResult.next())
                    {
                         iOrderNo = theResult.getInt(1)+1;
                    }
                    theResult.close();


                    ArrayList theItemList  = (ArrayList)ordergroup.getItems();

                    PreparedStatement thePrepare3      = theConnection.prepareStatement(getInsertSendMailQS());

                    for(int j=0; j<theItemList.size(); j++)
                    {

                         Item item = (Item)theItemList.get(j);

                         PreparedStatement thePrepare       = theConnection.prepareStatement(getInsertQS());
                         PreparedStatement theDescPrepare   = theConnection.prepareStatement(getDescInsertQS());
                         PreparedStatement thePrepare1      = theConnection.prepareStatement(getUpdateQS());
                         PreparedStatement thePrepare2      = theConnection.prepareStatement(getUpdateMrsTempQS());


                         String SRef= item.getReference();       // "SST"+"-"+iOrderNo+" Dt"+parseDate(item.getMrsDate());

                         thePrepare.setInt(1,iOrderNo);
                         thePrepare.setString(2,item.getMrsDate());
                         thePrepare.setString(3,"0");
                         thePrepare.setString(4,item.getMrsNo());
                         thePrepare.setString(5,SSupCode);
                         thePrepare.setString(6,SRef);
                         thePrepare.setString(7,"0");
                         thePrepare.setString(8,item.getPayTerms());
                         thePrepare.setString(9,item.getPayDays());
                         thePrepare.setString(10,item.getToCode());
                         thePrepare.setString(11,item.getThroCode());
                         thePrepare.setString(12,item.SItemCode);
                         thePrepare.setString(13,item.getQty());
                         thePrepare.setString(14,item.getRate());
                         thePrepare.setString(15,item.getDiscPer());
                         thePrepare.setString(16,item.getDiscount());
                         thePrepare.setString(17,item.getCenvatPer());
                         thePrepare.setString(18,item.getCenvat());
                         thePrepare.setString(19,item.getTaxPer());
                         thePrepare.setString(20,item.getTax());
                         thePrepare.setString(21,item.getSurPer());
                         thePrepare.setString(22,item.getSur());
                         thePrepare.setString(23,item.getNet());
                         thePrepare.setString(24,item.getPlus());
                         thePrepare.setString(25,item.getLess());
                         thePrepare.setString(26,item.getMisc());
                         thePrepare.setString(27,item.getUnitCode());
                         thePrepare.setString(28,item.getDeptCode());
                         thePrepare.setString(29,item.getGroupCode());
                         thePrepare.setString(30,pureDate(getDate(parseDate(item.getMrsDate()),7,0)));
                         thePrepare.setInt(31,(iSlNo));
                         thePrepare.setInt(32,0);
                         thePrepare.setInt(33,0);
                         thePrepare.setInt(34,0);
                         thePrepare.setString(35,item.getState());
                         thePrepare.setString(36,item.getPortCode());
                         thePrepare.setInt(37,Integer.parseInt(item.getMrsAuthUserCode()));
                         thePrepare.setString(38,getServerDateTime());
                         thePrepare.setInt(39,iMillCode);
                         thePrepare.setString(40,item.getMrsSlNo());
                         thePrepare.setInt(41,Integer.parseInt(item.getMrsAuthUserCode()));
                         thePrepare.setInt(42,0);
                         thePrepare.setInt(43,1);
                         thePrepare.setString(44,"0");
                         thePrepare.setInt(45,1);
                         thePrepare.setString(46,item.getPrevOrderNo());
                         thePrepare.setString(47,item.getPrevOrderBlock());
                         thePrepare.setInt(48,2);

                         thePrepare.executeUpdate();

                         theDescPrepare.setInt(1,iOrderNo);
                         theDescPrepare.setString(2,"0");
                         theDescPrepare.setString(3,item.getMrsDate());
                         theDescPrepare.setString(4,item.SItemCode);
                         theDescPrepare.setString(5,item.getMrsRemarks());
                         theDescPrepare.setString(6,item.getMake());
                         theDescPrepare.setString(7,item.getDraw());
                         theDescPrepare.setString(8,item.getCatl());
                         theDescPrepare.setInt(9,iSlNo);
                         theDescPrepare.setInt(10,iMillCode);

                         theDescPrepare.executeUpdate();


                         thePrepare1.setInt(1,iOrderNo);
                         thePrepare1.setInt(2,0);
                         thePrepare1.setString(3,item.getMrsNo());
                         thePrepare1.setString(4,item.SItemCode);


                         thePrepare1.executeUpdate();

                         thePrepare2.setInt(1,1);
                         thePrepare2.setString(2,item.getMrsNo());
                         thePrepare2.setString(3,item.SItemCode);

                         thePrepare2.executeUpdate();



                         theDescPrepare.close();
                         thePrepare.close();
                         thePrepare1.close();
                         thePrepare2.close();
                         iSlNo++;

                    }

                    thePrepare3.setInt(1,iOrderNo);
                    thePrepare3.setString(2,SSupCode);
                    thePrepare3.setInt(3,iMillCode);
                    thePrepare3.setString(4,SSupName);
                    thePrepare3.setString(5,SMailId);
                    thePrepare3.setString(6,SDate);

                    thePrepare3.executeUpdate();



                    thePrepare3.close();


                    theStatement.close();
                    UpdateOrderNo(iOrderNo);

               }

               theConnection.setAutoCommit(true);


               theConnection.close();


          }
          catch(Exception ex)
          {
               System.out.println(ex);

               ex.printStackTrace();
               bFlag = false;

               try
               {


                    theConnection.rollback();
                    theConnection.close();
               }catch(Exception e){}

          }
          return bFlag;


     }

     public void UpdateOrderNo(int iOrderNo) throws Exception
     {

          String SUString = "";


          Statement stat      = null;
          ResultSet result    = null;
          int       iId       = 0;
          stat           =  theConnection.createStatement();

          SUString       = " Update config"+iMillCode+""+SYearCode+" set MaxNo = "+iOrderNo+"  Where Id = 1";

          stat . executeUpdate(SUString);


          stat . close();

     }
     private String getInsertQS()
     {
          StringBuffer sb = new StringBuffer();
          sb.append(" Insert Into PurchaseOrder (OrderNo,OrderDate,OrderBlock,");
          sb.append(" MrsNo,Sup_Code,Reference,Advance,PayTerms,PayDays,ToCode,");
          sb.append(" ThroCode,Item_Code,Qty,Rate,DiscPer,Disc,CenvatPer,CenVat,");
          sb.append(" TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,Unit_Code,Dept_Code,");
          sb.append(" Group_Code,DueDate,SlNo,EPCG,OrderType,Project_order,state,portcode,");
          sb.append(" UserCode,ModiDate,MillCode,MRSSLNO,MrsAuthUserCode,TAXCLAIMABLE,Converted,InvQty,Authentication,ComparisionNo,PrevOrderBlock,OrderSourceTypeCode,id)");
          sb.append(" Values (?,?,?,");          // 3
          sb.append(" ?,?,?,?,?,?,?,");          // 7
          sb.append(" ?,?,?,?,?,?,?,?,");        // 8
          sb.append(" ?,?,?,?,?,?,?,?,?,?,");    // 10
          sb.append(" ?,?,?,?,?,?,?,?,");        // 8
          sb.append(" ?,?,?,?,?,?,?,?,?,?,?,?,purchaseorder_seq.nextval)");

          return sb.toString();

     }

     private String getDescInsertQS()
     {

          StringBuffer sb = new StringBuffer();

          sb.append(" insert into MatDesc(Id,OrderNo,OrderBlock,OrderDate,");
          sb.append(" Item_Code,Descr,Make,Draw,Catl,SlNo,MillCode)");
          sb.append(" values(MatDesc_Seq.nextval,?,?,?,");
          sb.append(" ?,?,?,?,?,?,?)");

          return sb.toString();

     }

     private String getUpdateQS()
     {
          String QS = " Update Mrs set OrderNo=?,OrderBlock=? where MrsNo=? and Item_Code=? ";

          return QS;
     }

     private String getUpdateMrsTempQS()
     {

          String QS = " Update Mrs_Temp  set OrderConverted=? where MrsNo=? and Item_Code=? and OrderConverted=0 and ApprovalStatus=1";

          return QS;
     }

     private String getInsertSendMailQS()
     {
          String QS = " Insert into ToSendMail(OrderNo,SupCode,MillCode,SupName,MailId,OrderDate) values(?,?,?,?,?,?)";

          return QS;
     }


     public  boolean setComparisonData()
     {

          boolean bFlag=true;
          VOrderGroup    = new Vector();

          try
          {

              Class.forName("oracle.jdbc.OracleDriver");
              theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
              Statement      stat          =  theConnection.createStatement();


              Statement theStatement = theConnection.createStatement();
              ResultSet theResult          =  theStatement.executeQuery(getQS());
              while(theResult.next())
              {
                    OrderGroup ordergroup = null;

                    String SOrderNo     = parseNull(theResult.getString(1));
                    String SOrderDate   = parseNull(theResult.getString(3));
                    String SReference   = parseNull(theResult.getString(1)+"- Enq. DT."+parseDate(SOrderDate));
                    String SItemCode    = parseNull(theResult.getString(4));
                    String SSupCode     = parseNull(theResult.getString(5));
                    double dRate        = theResult.getDouble(6);
                    double dDiscPer     = theResult.getDouble(7);
                    double dCenvatPer   = theResult.getDouble(8);
                    double dTaxPer      = theResult.getDouble(9);
                    double dSurPer      = theResult.getDouble(10);
                    String SToCode      = parseNull(theResult.getString(12));
                    String SThroCode    = parseNull(theResult.getString(13));
                    String SPayTerms    = parseNull(theResult.getString(14));
                    String SPayDays     = parseNull(theResult.getString(15));
                    String SPortCode    = parseNull(theResult.getString(16));
                    String SState       = parseNull(theResult.getString(17));
                    String STaxClaimable= parseNull(theResult.getString(18));
//                  String SReference   = parseNull(theResult.getString(19));
                    String SCatl        = parseNull(theResult.getString(20));
                    String SDraw        = parseNull(theResult.getString(21));
                    String SMrsNo       = parseNull(theResult.getString(23));
                    String SQty         = parseNull(theResult.getString(24));
                    String SMrsDate     = parseNull(getServerDate());
                    String SUnitCode    = parseNull(theResult.getString(25));
                    String SDeptCode    = parseNull(theResult.getString(26));
                    String SGroupCode   = parseNull(theResult.getString(27));
                    String SMrsSlNo     = parseNull(theResult.getString(28));
                    String SMrsAuthUserCode= parseNull(theResult.getString(29));
                    String SRemarks        = parseNull(theResult.getString(31));
                    String SPrevOrderBlock = parseNull(theResult.getString(32));
                    String SComparisionNo  = parseNull(theResult.getString(33));
                    String SSupName        = parseNull(theResult.getString(34));
                    String SMailId         = parseNull(theResult.getString(35));
		    String SMake	   = parseNull(theResult.getString(36));	


                    double dPlus        = 0;
                    double dLess        = 0;
                    double dMisc        = 0;


                    int iIndex = getIndexOf(SSupCode);

                    if(iIndex==-1)
                    {

                         ordergroup = new OrderGroup(SSupCode,SSupName,SMailId);
                         VOrderGroup.addElement(ordergroup);
                         iIndex=VOrderGroup.size()-1;
                    }


                    ordergroup = (OrderGroup)VOrderGroup.elementAt(iIndex);
                    Item item = new Item(SItemCode);

                    item.setOrderInfoDetails(SSupCode,
                                               SPayTerms,SToCode,SThroCode,
                                               SPortCode,SState,SPayDays,
                                               STaxClaimable,SReference,SCatl,SDraw,SComparisionNo,SPrevOrderBlock,SMake);

                    item.setOrderValueDetails(dRate,dDiscPer,dCenvatPer,
                                               dTaxPer,dSurPer,dPlus,dLess,
                                               dMisc);


                    item.setMrsDetails(SMrsNo,SMrsDate,SUnitCode,SDeptCode,SGroupCode,SMrsSlNo,SQty,SMrsAuthUserCode,SRemarks);


                    ordergroup.appendItem(item);
              }
              theResult.close();
              theStatement.close();
              theConnection.setAutoCommit(false);

          }

          catch(Exception ex)
          {

               try
               {

                    theConnection.rollback();
                    theConnection.close();

               }catch(Exception e){}
               System.out.println(" in selecting Orders -->"+ex);

               bFlag= false;
          }

          return bFlag;
     }

     private String getQS()
     {

           StringBuffer sb = new StringBuffer();

           sb.append(" Select ComparsionEnquiry.EnqNo,'SST' as OrderBlock,ComparsionEnquiry.EnqDate,Mrs.Item_Code,");
           sb.append(" ComparsionEnquiry.Sup_Code,ComparsionEnquiry.Rate,ComparsionEnquiry.DiscPer,");
           sb.append(" ComparsionEnquiry.CenVatPer,ComparsionEnquiry.TaxPer,ComparsionEnquiry.SurPer,0  as ItemId,0 as ToCode,");
           sb.append(" 0 as ThroCode,ComparsionEnquiry.Terms,ComparsionEnquiry.CreditDays,0 as PortCode,0 as State,");
           sb.append(" 0 as TaxClaimable,'' as Reference,InvItems.Catl,InvItems.Draw,'' as BlockName,Mrs.MrsNo,");
           sb.append(" decode(jmdMultiApproval,1,decode(jmdALLOCATEDQTYTYPE,0,AllocatedQTy,Round(Mrs.Qty*AllocatedQty/100,0))) as Qty ,Mrs.Unit_Code,Mrs.Dept_Code,Mrs.Group_Code,Mrs.SLNo,Mrs.MrsAuthUserCode,Mrs.Id,Mrs.Remarks,'' as Block,ComparsionEnquiry.ComparsionNo,Supplier.Name,Supplier.EMail,ComparsionEnquiry.Make");
      sb.append(" from Mrs");
           sb.append(" Inner join ComparsionEnquiry on ComparsionEnquiry.EnqNo=Mrs.EnquiryNo");
           sb.append(" and Mrs.Item_Code=comparsionEnquiry.Item_Code");
           sb.append(" and Mrs.Item_Code=comparsionEnquiry.Item_Code");
           sb.append(" and Mrs.OrderNo=0");
           sb.append("  and ComparsionEnquiry.ComparsionApproval=1 and ComparsionEnquiry.ComparsionCancel=0 and ComparsionEnquiry.JmdRejectionStatus=0 and Mrs.MillCode="+iMillCode);
           sb.append(" Inner join InvItems on InvItems.Item_Code=Mrs.Item_Code");
           sb.append(" Inner join Supplier on Supplier.Ac_Code=ComparsionEnquiry.Sup_Code");


           return sb.toString();
     }

     private int getIndexOf(String SSupCode)
     {

          int iIndex=-1;

          for(int i=0; i<VOrderGroup.size(); i++)
          {

               OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);

               if(SSupCode.equals(ordergroup.SSupCode))
               {
                    iIndex = i;
                    break;
               }
          }

          return iIndex;

     }

     public String parseDate(String str)
     {

         String SDate="";
         str=parseNull(str);
         str=str.trim();

         if(str.length()!=8)

            return str;

         try
         {

            SDate = str.substring(6,8)+"."+str.substring(4,6)+"."+str.substring(0,4);

         }
         catch(Exception ex)
         {
         }
         return SDate;

     }

     public String pureDate(String str)
     {

         String SDate="";
         str=str.trim();

         if(str.length()!=10)
            return str;

         try
         {

            str=str.trim();
            SDate = str.substring(6,10)+str.substring(3,5)+str.substring(0,2);
         }
         catch(Exception ex)
         {

         }
         return SDate;
     }


     public String getDate(String str1,long days,int Sig)
     {
            String SDay="",SMonth="",SYear="";
            int iYear1=0,iMonth1=0,iDay1=0;
            try
            {
               iYear1  = Integer.parseInt(str1.substring(6,10))-1900;
               iMonth1 = Integer.parseInt(str1.substring(3,5))-1;
               iDay1   = Integer.parseInt(str1.substring(0,2));

            }

            catch(Exception ex)
            {
               return "0";

            }
            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);

            long num1 = dt1.getTime();
            long num2 = (days*24*60*60*1000);

            if (Sig == -1)
                  dt1.setTime(num1-num2);
            else
                  dt1.setTime(num1+num2);

            int iDay   = dt1.getDate();
            int iMonth = dt1.getMonth()+1;
            int iYear  = dt1.getYear()+1900;

            SDay   = (iDay<10 ?"0"+iDay:""+iDay);

            SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);

            SYear  = ""+iYear;
            return SDay+"."+SMonth+"."+SYear;

     }
     public String getServerDateTime()
     {

          String SDate="";
          String QS = "Select to_Char(Sysdate,'MM/DD/YYYY HH24:MI:SS') From Dual";

          try
          {

               Statement theStatement = theConnection.createStatement();
               ResultSet theResult = theStatement.executeQuery(QS);

               if(theResult.next())
                    SDate = theResult.getString(1);

               theResult.close();

               theStatement.close();     
          }
          catch(Exception ex)
          {

               return "";
          }
          return SDate;

     }
     public String getServerDate()
     {

          String SDate="";


          String QS = "Select to_Char(Sysdate,'YYYYMMDD') From Dual";


          try
          {

               Statement theStatement = theConnection.createStatement();

               ResultSet theResult = theStatement.executeQuery(QS);

               if(theResult.next())
                    SDate = theResult.getString(1);

               theResult.close();
               theStatement.close();     

          }
          catch(Exception ex)
          {
               return "";

          }

          return SDate;

     }


     public String parseNull(String str)
     {

          str = ""+str;

          if(str.startsWith("null"))
               return "";

          return str;

     }
}

public class ComparisonOrderConversion 
{
     public ComparisonOrderConversion()
     {

     }
     public static void main(String[]  arg)
     {
          new CompConversion(Integer.parseInt(arg[0]),9991);

     }

}
