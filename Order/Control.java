package Order;

import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class Control 
{
     protected String ID="";
     Common common = new Common();
     Connection theConnection = null;

     public Control()
     {

     }
     
     public String getID(String QueryString)
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               ResultSet theResult      = theStatement.executeQuery(QueryString);
               while(theResult.next())
               {
                    ID = theResult.getString(1);
               }
               theStatement.close();
//               theConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);  
          }
          return ID;
	}
     
	public void setID(String AutoNo,String Column)
	{
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
               String ControlString      = "Update Config set "+Column+"="+AutoNo; 
               theStatement.executeUpdate(ControlString);
               theStatement.close();
//               theConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);  
          }
	}
}
