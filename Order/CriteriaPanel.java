package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class CriteriaPanel extends JPanel
{
     JPanel         DeptPanel;
     JPanel         GroupPanel;
     JPanel         UnitPanel;
     JPanel         SupplierPanel;
     JPanel         FilterPanel;
     JPanel         FigurePanel;
     JPanel         ApplyPanel;
     
     Common         common = new Common();
     Vector         VDept,VDeptCode,VGroup,VGroupCode,VUnit,VUnitCode,VSupName,VSupCode;
     JComboBox      JCDept,JCGroup,JCUnit,JCSupplier;
     JRadioButton   JRAllDept,JRSeleDept;
     JRadioButton   JRAllGroup,JRSeleGroup;
     JRadioButton   JRAllUnit,JRSeleUnit;
     JRadioButton   JRAllSup,JRSeleSup;
     JRadioButton   JRAllDoc,JRHonDoc;
     JRadioButton   JRQty,JRValue;
     
     JLayeredPane   Layer; 
     JButton        BApply;
     
     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     Vector         VCode,VName;
     int            iUserCode;
     int            iMillCode;
     String         SSupTable;

     CriteriaPanel(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable)
     {
          this.DeskTop   = DeskTop;
          this.VCode     = VCode;
          this.VName     = VName;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          
          getDeptGroupUnit();        
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          DeptPanel      = new JPanel();
          GroupPanel     = new JPanel();
          UnitPanel      = new JPanel();
          SupplierPanel  = new JPanel();
          FilterPanel    = new JPanel();
          FigurePanel    = new JPanel();
          ApplyPanel     = new JPanel();
          
          JRAllDept      = new JRadioButton("All",true);
          JRSeleDept     = new JRadioButton("Selected");
          JCDept         = new JComboBox(VDept);
          JCDept         . setEnabled(false);
          
          JRAllGroup     = new JRadioButton("All",true);
          JRSeleGroup    = new JRadioButton("Selected");
          JCGroup        = new JComboBox(VGroup);
          JCGroup        . setEnabled(false);
          
          JRAllUnit      = new JRadioButton("All",true);
          JRSeleUnit     = new JRadioButton("Selected");
          JCUnit         = new JComboBox(VUnit);
          JCUnit         . setEnabled(false);
          
          JRAllSup       = new JRadioButton("All",true);
          JRSeleSup      = new JRadioButton("Selected");
          JCSupplier     = new JComboBox(VSupName);
          JCSupplier     . setEnabled(false);
          
          JRAllDoc       = new JRadioButton("All",true);
          JRHonDoc       = new JRadioButton("Honoured");
          
          JRQty          = new JRadioButton("Qty",true);
          JRValue        = new JRadioButton("Value");
          
          BApply         = new JButton("Apply");
     }

     public void setLayouts()
     {
          setLayout(new GridLayout(1,7));
          DeptPanel      . setLayout(new GridLayout(3,1));   
          GroupPanel     . setLayout(new GridLayout(3,1));   
          UnitPanel      . setLayout(new GridLayout(3,1));
          SupplierPanel  . setLayout(new GridLayout(3,1));
          FilterPanel    . setLayout(new GridLayout(2,1));   
          FigurePanel    . setLayout(new GridLayout(2,1));
          
          DeptPanel      . setBorder(new TitledBorder("Department"));
          GroupPanel     . setBorder(new TitledBorder("Catagory"));
          UnitPanel      . setBorder(new TitledBorder("Processing Units"));
          SupplierPanel  . setBorder(new TitledBorder("Supplier"));
          FilterPanel    . setBorder(new TitledBorder("Documents"));
          FigurePanel    . setBorder(new TitledBorder("Figures are"));
     }

     public void addComponents()
     {
          add(DeptPanel);
          add(GroupPanel);
          add(UnitPanel);
          add(SupplierPanel);
          add(FilterPanel);
          add(FigurePanel);
          add(ApplyPanel);
          
          DeptPanel      . add(JRAllDept);
          DeptPanel      . add(JRSeleDept);
          DeptPanel      . add(JCDept);    
          
          GroupPanel     . add(JRAllGroup);
          GroupPanel     . add(JRSeleGroup);
          GroupPanel     . add(JCGroup);    
          
          UnitPanel      . add(JRAllUnit);  
          UnitPanel      . add(JRSeleUnit); 
          UnitPanel      . add(JCUnit);     
          
          SupplierPanel  . add(JRAllSup);  
          SupplierPanel  . add(JRSeleSup); 
          SupplierPanel  . add(JCSupplier);     
          
          FilterPanel    . add(JRAllDoc); 
          FilterPanel    . add(JRHonDoc); 
          
          FigurePanel    . add(JRQty);    
          FigurePanel    . add(JRValue);
          
          ApplyPanel     . add(BApply);
     }

     public void addListeners()
     {
          JRAllDept      . addActionListener(new DeptList());
          JRSeleDept     . addActionListener(new DeptList());
          
          JRAllGroup     . addActionListener(new GroupList());
          JRSeleGroup    . addActionListener(new GroupList());
          
          JRAllUnit      . addActionListener(new UnitList());
          JRSeleUnit     . addActionListener(new UnitList());
          
          JRAllSup       . addActionListener(new SupplierList());
          JRSeleSup      . addActionListener(new SupplierList());
          
          JRAllDoc       . addActionListener(new FilterList());
          JRHonDoc       . addActionListener(new FilterList());
          
          JRQty          . addActionListener(new FigureList());
          JRValue        . addActionListener(new FigureList());
          
     }

     public class DeptList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllDept)
               {
                    JRAllDept      . setSelected(true);
                    JRSeleDept     . setSelected(false);
                    JCDept         . setEnabled(false);
               }
               if(ae.getSource()==JRSeleDept)
               {
                    JRAllDept      . setSelected(false);
                    JRSeleDept     . setSelected(true);
                    JCDept         . setEnabled(true);
               }
          }
     }

     public class GroupList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllGroup)
               {
                    JRAllGroup     . setSelected(true);
                    JRSeleGroup    . setSelected(false);
                    JCGroup        . setEnabled(false);
               }
               if(ae.getSource()==JRSeleGroup)
               {
                    JRAllGroup     . setSelected(false);
                    JRSeleGroup    . setSelected(true);
                    JCGroup        . setEnabled(true);
               }
          }
     }

     public class UnitList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllUnit)
               {
                    JRAllUnit      . setSelected(true);
                    JRSeleUnit     . setSelected(false);
                    JCUnit         . setEnabled(false);
               }
               if(ae.getSource()==JRSeleUnit)
               {
                    JRAllUnit      . setSelected(false);
                    JRSeleUnit     . setSelected(true);
                    JCUnit         . setEnabled(true);
               }
          }
     }

     public class SupplierList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllSup)
               {
                    JRAllSup       . setSelected(true);
                    JRSeleSup      . setSelected(false);
                    JCSupplier     . setEnabled(false);
               }
               if(ae.getSource()==JRSeleSup)
               {
                    JRAllSup       . setSelected(false);
                    JRSeleSup      . setSelected(true);
                    JCSupplier     . setEnabled(true);
               }
          }
     }

     public class FilterList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllDoc)
               {
                    JRAllDoc  . setSelected(true);
                    JRHonDoc  . setSelected(false);
               }
               if(ae.getSource()==JRHonDoc)
               {
                    JRAllDoc  . setSelected(false);
                    JRHonDoc  . setSelected(true);
               }
          }
     }

     public class FigureList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRQty)
               {
                    JRQty     . setSelected(true);
                    JRValue   . setSelected(false);
               }
               if(ae.getSource()==JRValue)
               {
                    JRQty     . setSelected(false);
                    JRValue   . setSelected(true);
               }
          }
     }

     public void getDeptGroupUnit()
     {
          ResultSet  result   = null;
          VDept          = new Vector();
          VGroup         = new Vector();
          
          VDeptCode      = new Vector();
          VGroupCode     = new Vector();
          
          VUnit          = new Vector();
          VUnitCode      = new Vector();
          
          VSupName       = new Vector();
          VSupCode       = new Vector();
          
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               String QS1 = " ";
               
               QS1 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               
               result = stat.executeQuery(QS1);

               while(result.next())
               {
                    VDept     . addElement(result.getString(1));
                    VDeptCode . addElement(result.getString(2));
               }
               result    . close();

               String QS2 = " ";
               
               QS2 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               
               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VGroup    . addElement(result.getString(1));
                    VGroupCode. addElement(result.getString(2));
               }
               result    . close();
               
               String QS3 = " ";
               
               QS3 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";
               
               result = stat.executeQuery(QS3);
               while(result.next())
               {
                    VUnit     . addElement(result.getString(1));
                    VUnitCode . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Name,Ac_Code From "+SSupTable+" Order By Name");
               while(result.next())
               {
                    VSupName  . addElement(result.getString(1));
                    VSupCode  . addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept,Group & Unit :"+ex);
          }
     }
}
