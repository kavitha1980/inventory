// Used By Order,GRN  and other operations
// The Operation Varies with the state of bSig.
// Initially it is set to true once called from Non-Order activities,
// the same shall be set to false.

package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;
import MRS.*;

public class DOInvMiddlePanelGst extends JPanel
{
     JTable         ReportTable;
     JLayeredPane   DeskTop;
     JComboBox      JCDept,JCGroup,JCUnit,JCBlock,JCTax,JCUser;
     JLabel         LBasic,LDiscount,LCGST,LSGST,LIGST,LCess,LNet,LOthers;
     JPanel         GridPanel,BottomPanel,FigurePanel,ControlPanel;
     JButton        BAdd;

     Common         common = new Common();
     DOInvTableModelGst  dataModel;

     NextField      TAdd,TLess;


     Vector         VDept,VDeptCode,VGroup,VGroupCode,VUnit,VUnitCode;
     Vector         VDesc,VMake,VDraw,VCatl,VNameCode;
     Vector         VMrsStatus;
     Vector         VBlock,VBlockName;
     Vector         VTaxClaim;
     Vector         VSUserCode,VSUserName;
     Vector         VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo;
     Vector         VCode,VName;
     Vector         VItemsStatus,VOrdSlNo,VHsnType;

     Object         RowData[][];

     String         ColumnData[],ColumnType[];
     String         SMOrderNo;
     
     boolean        bflag = false;

     int            iMillCode;
     int            iOrderStatus   = 0;
     int            iTaxAuth       = 0;
     String         SItemTable;
     JTextField     TSupData;

     int iStateCheck = 0;
     int iTypeCheck  = 0;

     Connection       theConnection = null;

     // Constructor Method refferred from Order Collection Operations
     DOInvMiddlePanelGst(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,Object RowData[][],String ColumnData[],String ColumnType[],int iMillCode,String SItemTable,JTextField TSupData)
     {
          this.DeskTop        = DeskTop;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.RowData        = RowData;
          this.ColumnData     = ColumnData;
          this.ColumnType     = ColumnType;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;
          this.TSupData	      = TSupData;
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     
          setReportTable();
     }

     DOInvMiddlePanelGst(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,Object RowData[][],String ColumnData[],String ColumnType[],boolean bflag,int iOrderStatus,Vector VItemsStatus,int iMillCode,String SMOrderNo,String SItemTable,JTextField TSupData)
     {
          this.DeskTop        = DeskTop;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.RowData        = RowData;
          this.ColumnData     = ColumnData;
          this.ColumnType     = ColumnType;
          this.iMillCode      = iMillCode;
          this.bflag          = bflag;
          this.iOrderStatus   = iOrderStatus;
          this.VItemsStatus   = VItemsStatus;
          this.SMOrderNo      = SMOrderNo;
          this.SItemTable     = SItemTable;
          this.TSupData       = TSupData;
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     
          setReportTable();
     }

     public void createComponents()
     {
          LBasic         = new JLabel("0");
          LDiscount      = new JLabel("0");
          LCGST          = new JLabel("0");
          LSGST          = new JLabel("0");
          LIGST          = new JLabel("0");
          LCess          = new JLabel("0");
          TAdd           = new NextField();
          TLess          = new NextField();
          LNet           = new JLabel("0");
          LOthers        = new JLabel("0");
          GridPanel      = new JPanel(true);
          BottomPanel    = new JPanel();
          FigurePanel    = new JPanel();
          ControlPanel   = new JPanel();
         
          VOrdSlNo       = new Vector();    
          
          VDesc          = new Vector();
          VMake          = new Vector();
          VDraw          = new Vector();
          VCatl          = new Vector();

          VPColour       = new Vector();
          VPSet          = new Vector();
          VPSize         = new Vector();
          VPSide         = new Vector();
          VPSlipFrNo     = new Vector();
          VPSlipToNo     = new Vector();
          VPBookFrNo     = new Vector();
          VPBookToNo     = new Vector();
          VHsnType       = new Vector();

          VMrsStatus     = new Vector();

          BAdd           = new JButton("Add New Record");
     }

     public void setLayouts()
     {
          GridPanel      . setLayout(new BorderLayout());
          BottomPanel    . setLayout(new BorderLayout());
          FigurePanel    . setLayout(new GridLayout(2,9));
          ControlPanel   . setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          VTaxClaim      = new Vector();
          VTaxClaim      . insertElementAt("Not Claimable",0);
          VTaxClaim      . insertElementAt("Claimable",1);

          FigurePanel    . add(new JLabel("Basic"));
          FigurePanel    . add(new JLabel("Discount"));
          FigurePanel    . add(new JLabel("CGST"));
          FigurePanel    . add(new JLabel("SGST"));
          FigurePanel    . add(new JLabel("IGST"));
          FigurePanel    . add(new JLabel("Cess"));
          FigurePanel    . add(new JLabel("Plus"));
          FigurePanel    . add(new JLabel("Minus"));
          FigurePanel    . add(new JLabel("Net"));
     
          FigurePanel    . add(LBasic);
          FigurePanel    . add(LDiscount);
          FigurePanel    . add(LCGST);
          FigurePanel    . add(LSGST);
          FigurePanel    . add(LIGST);
          FigurePanel    . add(LCess);
          FigurePanel    . add(TAdd);
          FigurePanel    . add(TLess);
          FigurePanel    . add(LNet);
     
          ControlPanel   . add(new JLabel("F2--To Select HSN Code"));
          ControlPanel   . add(BAdd);
     
          BottomPanel    . add("North",FigurePanel);
          BottomPanel    . add("South",ControlPanel);
     
          getDeptGroupUnit();

          JCDept         = new JComboBox(VDept);
          JCGroup        = new JComboBox(VGroup);
          JCUnit         = new JComboBox(VUnit);
          JCBlock        = new JComboBox(VBlockName);
          JCTax          = new JComboBox(VTaxClaim);
          JCUser         = new JComboBox(VSUserName);
     }

     public void addListeners()
     {
          BAdd      . addActionListener(new ActList());
          TAdd      . addKeyListener(new KeyList());
          TLess     . addKeyListener(new KeyList());
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               calc();
          }
     }

     public void calc()
     {
               double dTNet = common.toDouble(LBasic.getText())-common.toDouble(LDiscount.getText())+common.toDouble(LCGST.getText())+common.toDouble(LSGST.getText())+common.toDouble(LIGST.getText())+common.toDouble(LCess.getText());
               dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
               LNet.setText(common.getRound(dTNet,2));                              
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BAdd)
               {
                    if(bflag)
                    {
                         VItemsStatus.addElement("0");
                    }

                    Vector VEmpty1 = new Vector();
               
                    for(int i=0;i<ColumnData.length;i++)
                    {
                         VEmpty1   . addElement(" ");
                    }
                    VDesc          . addElement(" ");
                    VMake          . addElement(" ");
                    VDraw          . addElement(" ");
                    VCatl          . addElement(" ");

                    VPColour       . addElement(" ");
                    VPSet          . addElement(" ");
                    VPSize         . addElement(" ");
                    VPSide         . addElement(" ");
                    VPSlipFrNo     . addElement(" ");
                    VPSlipToNo     . addElement(" ");
                    VPBookFrNo     . addElement(" ");
                    VPBookToNo     . addElement(" ");
             	    VHsnType.addElement("1");

                    VMrsStatus     . addElement("0");

                    dataModel           . addRow(VEmpty1);
                    ReportTable         . updateUI();
               }
          }
     }

     public void setReportTable()
     {
         dataModel        = new DOInvTableModelGst(RowData,ColumnData,ColumnType,LBasic,LDiscount,LCGST,LSGST,LIGST,LCess,TAdd,TLess,LNet,LOthers,bflag,TSupData,VHsnType,VItemsStatus);       

          ReportTable    = new JTable(dataModel);
          ReportTable    . setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
     
          DefaultTableCellRenderer cellRenderer   = new DefaultTableCellRenderer();
                                   cellRenderer   . setHorizontalAlignment(JLabel.RIGHT);
     
          for (int col=0;col<ReportTable.getColumnCount();col++)
          {
               if(ColumnType[col]=="N" || ColumnType[col]=="B"  || ColumnType[col]=="E")
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
          }
          //ReportTable.setShowGrid(false);
     
          TableColumn BlockColumn  = ReportTable.getColumn("Order Block");
          TableColumn deptColumn   = ReportTable.getColumn("Department");
          TableColumn groupColumn  = ReportTable.getColumn("Group");
          TableColumn unitColumn   = ReportTable.getColumn("Unit");
          TableColumn userColumn   = ReportTable.getColumn("User");
     
          BlockColumn              . setCellEditor(new DefaultCellEditor(JCBlock));
          deptColumn               . setCellEditor(new DefaultCellEditor(JCDept));
          groupColumn              . setCellEditor(new DefaultCellEditor(JCGroup));
          unitColumn               . setCellEditor(new DefaultCellEditor(JCUnit));
          userColumn               . setCellEditor(new DefaultCellEditor(JCUser));
     
          setLayout(new BorderLayout());
          GridPanel.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
          GridPanel.add(new JScrollPane(ReportTable),BorderLayout.CENTER);
     
          add(BottomPanel,BorderLayout.SOUTH);
          add(GridPanel,BorderLayout.CENTER);
     
          ReportTable    . addKeyListener(new ServList());
          ReportTable    . addMouseListener(new MouseList());
          ReportTable    . addKeyListener(new DescList());
          ReportTable    . addKeyListener(new StationaryList());
     }

    //hsncode

     public class ServList extends KeyAdapter
     {
               public void keyPressed(KeyEvent ke)
               {
            	    if(ke.getKeyCode()==KeyEvent.VK_F2) 
		    {
			 int i = ReportTable.getSelectedRow();
			 int iCol = ReportTable.getSelectedColumn();


			 if(iCol==2)
			 {
                     String SHsnType   = (String)VHsnType.elementAt(i);
                     String SHCode     = (String)dataModel.getValueAt(i,2);
	 	 	         String SItemCode  = (String)dataModel.getValueAt(i,0);
			         String SMasterHsn = getMasterHsn(SItemCode);
			         String SMultiHsn  = getMultiHsn(SItemCode);


                     System.out.println(SHsnType+","+SHCode+","+SMasterHsn);

                     System.out.println(SHsnType+","+SMultiHsn);


				 if(SHsnType.equals("0") && (SHCode.equals("") || SHCode.equals("0") || SMasterHsn.equals("0") || SMasterHsn.equals("")))
				 {
				      if(iStateCheck==1 && iTypeCheck==1)
				      {
		                           setHsnList(SItemCode,0);
				      }
				      else
				      {
					   JOptionPane.showMessageDialog(null,"Select Supplier before proceed","Information",JOptionPane.INFORMATION_MESSAGE);
				      }
				 }


				 else if(SHsnType.equals("0") && SMultiHsn.equals("1"))
				 {
				      if(iStateCheck==1 && iTypeCheck==1)
				      {
		                           setHsnList(SItemCode,1);
				      }
				      else
				      {
					   JOptionPane.showMessageDialog(null,"Select Supplier before proceed","Information",JOptionPane.INFORMATION_MESSAGE);
				      }
				 }
				 else
				 {
				      JOptionPane.showMessageDialog(null,"This row couldn't be changed","Information",JOptionPane.INFORMATION_MESSAGE);
				 }
			 }
		    }

            if(ke.getKeyCode()==KeyEvent.VK_HOME) 
		    {
                 int i = ReportTable.getSelectedRow();

                 int iDocId = common.toInt((String)dataModel.getValueAt(i,24));

                 System.out.println(iDocId+","+(String)dataModel.getValueAt(i,21)+","+(String)dataModel.getValueAt(i,20));

                 try
                 {
                      DocumentListFrame doclistframe = new DocumentListFrame(DeskTop,iDocId);
                      DeskTop.add(doclistframe);
                      DeskTop.repaint();
                      doclistframe.setSelected(true);
                      DeskTop.updateUI();
                      doclistframe.show();
                 }catch(Exception ex)
                 {

                 }

            }       
           }
     }

     public String getMasterHsn(String SItemCode)
     {
	String SMHsn = "";

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "Select HsnCode from InvItems where Item_Code='"+SItemCode+"'";

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    SMHsn = common.parseNull(result.getString(1));
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getMasterHsn :"+ex);
        }
	return SMHsn;
     }

     public String getMultiHsn(String SItemCode)
     {
	String SMulti = "";

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "Select nvl(MultiHsn,0) from InvItems where Item_Code='"+SItemCode+"'";

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    SMulti = common.parseNull(result.getString(1));
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getMultiHsn :"+ex);
        }
	return SMulti;
     }

     public class MouseList extends MouseAdapter
     {
          public void mouseClicked(MouseEvent me)
          {
               try
               {
                    if(bflag)
                    {
                         int i = ReportTable.getSelectedRow();
                         int iItemStatus = common.toInt((String)VItemsStatus.elementAt(i));

		         if(!(iStateCheck==1 && iTypeCheck==1))
		         {
			      JOptionPane.showMessageDialog(null,"Select Supplier before proceed","Information",JOptionPane.INFORMATION_MESSAGE);
			      return;
		         }

                         if(iItemStatus == 0)
                         {
                              if (me.getClickCount()==2)
                              {
                                   MaterialPickerGst MP = new MaterialPickerGst(DeskTop,VCode,VName,VNameCode,VDesc,VMake,VDraw,VCatl,ReportTable,dataModel,iMillCode,true,bflag,VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo,iTaxAuth,SItemTable,VMrsStatus,iItemStatus,VHsnType);

                                   try
                                   {
                                        DeskTop   . add(MP);
                                        DeskTop   . repaint();
                                        MP        . setSelected(true);
                                        DeskTop   . updateUI();
                                        MP        . show();
                                        MP        . BrowList.requestFocus();
                                   }
                                   catch(java.beans.PropertyVetoException ex){}
                              }
                         }

                         if(iItemStatus == 1)
                         {
                    	      JOptionPane.showMessageDialog(null,"Grn Process Completed for this Item","Error",JOptionPane.ERROR_MESSAGE);
                              /*if (me.getClickCount()==2)
                              {
                                   MaterialPickerGst MP = new MaterialPickerGst(DeskTop,VCode,VName,VNameCode,VDesc,VMake,VDraw,VCatl,ReportTable,dataModel,iMillCode,false,bflag,VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo,iTaxAuth,SItemTable,VMrsStatus,iItemStatus);
                                   try
                                   {
                                        DeskTop   . add(MP);
                                        DeskTop   . repaint();
                                        MP        . setSelected(true);
                                        DeskTop   . updateUI();
                                        MP        . show();
                                        MP        . BrowList.requestFocus();
                                   }
                                   catch(java.beans.PropertyVetoException ex){}
                              }*/
                         }

                         if(iItemStatus == 2)
                         {
                              if (me.getClickCount()==2)
                              {
                                   MaterialPickerGst MP = new MaterialPickerGst(DeskTop,VCode,VName,VNameCode,VDesc,VMake,VDraw,VCatl,ReportTable,dataModel,iMillCode,false,bflag,VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo,iTaxAuth,SItemTable,VMrsStatus,iItemStatus,VHsnType);
                                   try
                                   {
                                        DeskTop   . add(MP);
                                        DeskTop   . repaint();
                                        MP        . setSelected(true);
                                        DeskTop   . updateUI();
                                        MP        . show();
                                        MP        . BrowList.requestFocus();
                                   }
                                   catch(java.beans.PropertyVetoException ex){}
                              }
                         }
                    }
                    else
                    {
                         if (me.getClickCount()==2)
                         {
                              MaterialPickerGst MP = new MaterialPickerGst(DeskTop,VCode,VName,VNameCode,VDesc,VMake,VDraw,VCatl,ReportTable,dataModel,iMillCode,VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo,SItemTable,VMrsStatus);
                              try
                              {
                                   DeskTop   . add(MP);
                                   DeskTop   . repaint();
                                   MP        . setSelected(true);
                                   DeskTop   . updateUI();
                                   MP        . show();
                                   MP        . BrowList     . requestFocus();
                              }
                              catch(java.beans.PropertyVetoException ex){}
                         }
                    }
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
          }
     }

     private void  setDocumentFrame() {

	       System.out.println("comm doc frame");
			
	       int iRow = ReportTable.getSelectedRow();
	       ColumnType[24]="S";	  	

               DocumentationEntryFrameGst docFrame = new DocumentationEntryFrameGst(DeskTop,ReportTable,dataModel,iRow,true);
               try
               {
                    DeskTop.add(docFrame);
                    DeskTop.repaint();
                    docFrame.setSelected(true);
                    DeskTop.updateUI();
                    docFrame.show();
               }
               catch(java.beans.PropertyVetoException ex){}
     }

     private void  setDocumentViewFrame() {

     	int iRow = ReportTable.getSelectedRow();

	String sId= (String)dataModel. getValueAt(iRow,24); 

	System.out.println("id-->"+sId);

	       DocumentationDisplayFrameGst docDisFrame = new DocumentationDisplayFrameGst(DeskTop,ReportTable,iRow,sId);
               try
               {
                    DeskTop.add(docDisFrame);
                    DeskTop.repaint();
                    docDisFrame.setSelected(true);
                    DeskTop.updateUI();
                    docDisFrame.show();
               }
               catch(java.beans.PropertyVetoException ex){}
     }



     public Object[][] getFromVector()
     {
          return dataModel.getFromVector();     
     }

     public String getDeptCode(int i)                       // 19
     {
          Vector    VCurVector     = dataModel.getCurVector(i);
          String    str            = (String)VCurVector.elementAt(19);
          int       iid            = VDept.indexOf(str);
          return (iid==-1 ?"0":(String)VDeptCode.elementAt(iid));
     }

     public String getGroupCode(int i)                     // 20
     {
          Vector    VCurVector     = dataModel.getCurVector(i);
          String    str            = (String)VCurVector.elementAt(20);
          int       iid            = VGroup.indexOf(str);
          return (iid==-1?"0":(String)VGroupCode.elementAt(iid));
     }

     public String getUnitCode(int i)                     //  22
     {
          Vector    VCurVector     = dataModel.getCurVector(i);
          String    str            = (String)VCurVector.elementAt(22);
          int       iid            = VUnit.indexOf(str);
          return (iid==-1?"0":(String)VUnitCode.elementAt(iid));
     }

     public String getUserCode(int i)                     
     {
          Vector    VCurVector     = dataModel.getCurVector(i);
          String    str            = (String)VCurVector.elementAt(23);
          int       iid            = VSUserName.indexOf(str);
          return (iid==-1?"1":(String)VSUserCode.elementAt(iid));
     }

     public String getBlockCode(int i)                     //  3
     {
          Vector    VCurVector     = dataModel.getCurVector(i);
          String    str            = (String)VCurVector.elementAt(3);
          int       iid            = VBlockName.indexOf(str);
          return (iid==-1?"0":(String)VBlock.elementAt(iid));
     }

     public String getDueDate(int i,DateField2 TDate)      //  
     {
          Vector VCurVector = dataModel.getCurVector(i);
          try
          {
               String str = (String)VCurVector.elementAt(21);
               if((str.trim()).length()==0)
                    return TDate.toNormal();
               else
                    return common.pureDate(str);
          }
          catch(Exception ex)
          {
               return " ";
          }
     }
     
     public String getTaxClaim(int i)      //  3
     {
          Vector    VCurVector     = dataModel.getCurVector(i);
          String    str            = (String)VCurVector.elementAt(2);
          int       iid            = VTaxClaim.indexOf(str);
          return (iid==-1?"0":String.valueOf(iid));
     }
     
     public void getDeptGroupUnit()
     {
          ResultSet result = null;

          VDept          = new Vector();
          VGroup         = new Vector();
     
          VDeptCode      = new Vector();
          VGroupCode     = new Vector();
     
          VUnit          = new Vector();
          VUnitCode      = new Vector();
                             
          VBlock         = new Vector();
          VBlockName     = new Vector();

          VSUserCode     = new Vector();
          VSUserName     = new Vector();

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               String QS1 = "";
               String QS2 = "";
               String QS3 = "";
               String QS4 = "";
               String QS5 = "";
               String QS6 = "";
               
               QS1 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               QS2 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               QS3 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";
               QS4 = " Select Block,BlockName From OrdBlock Where ShowStatus=0 Order By 1";
               QS5 = " Select distinct Authentication from PurchaseOrder where MillCode="+iMillCode+" and PurchaseOrder.OrderNo = "+SMOrderNo;
               QS6 = " Select UserCode,UserName from RawUser Where UserCode in (Select Distinct(AuthUserCode) from MrsUserAuthentication) Order by UserCode ";

               result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VDept     . addElement(result.getString(1));
                    VDeptCode . addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VGroup    . addElement(result.getString(1));
                    VGroupCode. addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery(QS3);
               while(result.next())
               {
                    VUnit     . addElement(result.getString(1));
                    VUnitCode . addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery(QS4);
               while(result.next())
               {
                    VBlock    . addElement(result.getString(1));
                    VBlockName. addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery(QS5);
               while(result.next())
               {
                    iTaxAuth  = common.toInt(common.parseNull(result.getString(1)));
               }
               result.close();

               result = stat.executeQuery(QS6);
               while(result.next())
               {
                    VSUserCode. addElement(result.getString(1));
                    VSUserName. addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept,Group & Unit :"+ex);
          }
     }

     private void setDescMouse(int iItemStatus)
     {
          try
          {
               DescMouseGst MP = new DescMouseGst(DeskTop,VDesc,VMake,VDraw,VCatl,ReportTable,iItemStatus);
               DeskTop        . add(MP);
               DeskTop        . repaint();
               MP             . setSelected(true);
               DeskTop        . updateUI();
               MP             . show();
          }
          catch(java.beans.PropertyVetoException ex){ex.printStackTrace();}
     }

     private void setHsnList(String SItemCode,int iDispStatus)
     {
	   int iAmend=1;
           HsnCodePicker HP = new HsnCodePicker(DeskTop,ReportTable,dataModel,iAmend,SItemCode,iDispStatus);
           try
           {
                DeskTop   . add(HP);
                DeskTop   . repaint();
                HP        . setSelected(true);
                DeskTop   . updateUI();
                HP        . show();
                HP        . BrowList.requestFocus();
           }
           catch(java.beans.PropertyVetoException ex){}
     }

     private void setStationaryFrame()
     {
          StationaryPropertiesFrameGst  SPF   = new StationaryPropertiesFrameGst(DeskTop,ReportTable,VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo);
          try
          {
               DeskTop   . add(SPF);
               DeskTop   . repaint();
               SPF       . setSelected(true);
               DeskTop   . updateUI();
               SPF       . show();
          }
          catch(java.beans.PropertyVetoException ex){ex.printStackTrace();}
     }

     public class StationaryList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    int       i         = ReportTable.getSelectedRow();
                    String    SItem_Code= (String)ReportTable.getModel().getValueAt(i,0);

                    if(isStationary(SItem_Code))
                         setStationaryFrame();
               }    
          }
     }

     public class DescList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    if(!bflag)
                    {
                              setDescMouse(0);
                    }
                    else
                    {
                         int i          = ReportTable.getSelectedRow();
                         int iItemStatus= common.toInt((String)VItemsStatus.elementAt(i));
                         setDescMouse(iItemStatus);
                    }
               }    
               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
			setDocumentFrame();
	       }	

               if(ke.getKeyCode()==KeyEvent.VK_F1) {
                         setDocumentViewFrame();	

	        }

          }
     }

     public boolean isStationary(String SItemCode)
     {
          String SStkGroupCode     = "";
          String QS                = "";

          if(iMillCode==0)
          {
               QS =    " select stkgroupcode,invitems.item_code from invitems"+
                       " where invitems.item_code = '"+SItemCode+"'";
          }
          else
          {
               QS =    " select stkgroupcode,"+SItemTable+".item_code from "+SItemTable+""+
                       " where "+SItemTable+".item_code = '"+SItemCode+"'";
          }
          try
          {
               ORAConnection  oraConnection = ORAConnection.getORAConnection();
               Connection     theConnection = oraConnection.getConnection();
                              theConnection . setAutoCommit(true);
               Statement      stat          = theConnection.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result         . next())
               {
                    SStkGroupCode  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          if(SStkGroupCode.equals("B01"))
               return true;

          return false;
     }

     public void setSupData()
     {
          dataModel.setSupData();     

	  iStateCheck = dataModel.iStateCheck;
	  iTypeCheck  = dataModel.iTypeCheck;
     }

}

