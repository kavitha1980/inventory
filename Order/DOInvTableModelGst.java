package Order;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class DOInvTableModelGst extends DefaultTableModel
{
        Object    RowData[][],ColumnNames[],ColumnType[];
        JLabel    LBasic,LDiscount,LCGST,LSGST,LIGST,LCess,LNet,LOthers;
        NextField TAdd,TLess;
        boolean   bflag;
	JTextField TSupData;
	Vector VHsnType,VItemsStatus;
        String SSeleSupCode,SSeleSupType,SSeleStateCode;
	int iStateCheck = 0;
	int iTypeCheck  = 0;
	int iHsnCheck   = 0;
	double dCGST=0,dSGST=0,dIGST=0,dCess=0;
        Common common = new Common();

   	Connection       theConnection = null;

        public DOInvTableModelGst(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType,JLabel LBasic,JLabel LDiscount,JLabel LCGST,JLabel LSGST,JLabel LIGST,JLabel LCess,NextField TAdd,NextField TLess,JLabel LNet,JLabel LOthers,boolean bflag,JTextField TSupData,Vector VHsnType,Vector VItemsStatus)
        {
            super(RowData,ColumnNames);
            this.RowData        = RowData;
            this.ColumnNames    = ColumnNames;
            this.ColumnType     = ColumnType;
            this.LBasic         = LBasic;
            this.LDiscount      = LDiscount;
            this.LCGST          = LCGST;
            this.LSGST          = LSGST;  
            this.LIGST          = LIGST; 
            this.LCess          = LCess; 
            this.TAdd           = TAdd;
            this.TLess          = TLess;
            this.LNet           = LNet;
            this.LOthers        = LOthers;
            this.bflag          = bflag;
	    this.TSupData       = TSupData;
	    this.VHsnType	= VHsnType;
	    this.VItemsStatus   = VItemsStatus;

	    setSupData();
            /*for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
                setMaterialAmount(curVector);
            }*/
        }

       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }

       public boolean isCellEditable(int row,int col)
       {
	       if(iTypeCheck==0 || iStateCheck==0 || iHsnCheck==0 || SSeleSupCode.equals(""))
	            return false;

               if(ColumnType[col]=="B" || ColumnType[col]=="E")
                  return true;
               return false;
       }

       public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
	     	   String SHsnType  = (String)VHsnType.elementAt(row);
	     	   String SItemStatus = (String)VItemsStatus.elementAt(row);
                   if(column==21)
                   {
                     String str = ((String)aValue).trim();
                     aValue = common.getDate(str,0,1);
                   }
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
                   if(column>=6 && column<=11)
                      setMaterialAmount(rowVector,SHsnType,SItemStatus,row);
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }

      public void setMaterialAmount(Vector RowVector,String SHsnType,String SItemStatus,int iRow)
      {
              double dTGross=0,dTDisc=0,dTCGST=0,dTSGST=0,dTIGST=0,dTCess=0,dTNet=0,dTOthers=0;

              String SHsnCode    = (String)RowVector.elementAt(2);

	      dCGST=0;
	      dSGST=0;
	      dIGST=0;
	      dCess=0;

	      if(!SSeleSupType.equals("1"))
	      {
	           dCGST=0;
		   dSGST=0;
		   dIGST=0;
		   dCess=0;
	      }
	      else
	      {
   		   if((SHsnCode.equals("") || SHsnCode.equals("0")))		   
		   {
			dCGST = common.toDouble((String)RowVector.elementAt(8));
			dSGST = common.toDouble((String)RowVector.elementAt(9));
			dIGST = common.toDouble((String)RowVector.elementAt(10));
			dCess = common.toDouble((String)RowVector.elementAt(11));
		   }
		   else
		   {
		        int iRateCheck = getRateCheck(SHsnCode);
			if(iRateCheck>0)
			{
			     /*if(SItemStatus.equals("0"))
			     {
			          getGstRate(SHsnCode);
			     }
			     else*/
			     {
			          dCGST = common.toDouble((String)RowVector.elementAt(8));
				  dSGST = common.toDouble((String)RowVector.elementAt(9));
				  dIGST = common.toDouble((String)RowVector.elementAt(10));
				  dCess = common.toDouble((String)RowVector.elementAt(11));
			     }
			}
			else
			{
			     JOptionPane.showMessageDialog(null,"Gst Rate Not Updated for HsnCode-"+SHsnCode,"Information",JOptionPane.INFORMATION_MESSAGE);
			     return;
			}
		   }
	      }

	      double dQty = 0;

	      if(SHsnType.equals("0"))
	      {
                   dQty = common.toDouble((String)RowVector.elementAt(5));
	      }
	      else
	      {
                   dQty = 1;
	      }

              double dRate       = common.toDouble((String)RowVector.elementAt(6));
              double dDiscPer    = common.toDouble((String)RowVector.elementAt(7));

              double dGross   = dQty*dRate;
              double dDisc    = dGross*dDiscPer/100;
	      double dBasic   = dGross - dDisc;
              double dCGSTVal = dBasic*dCGST/100;
              double dSGSTVal = dBasic*dSGST/100;
              double dIGSTVal = dBasic*dIGST/100;
              double dCessVal = dBasic*dCess/100;
              double dNet     = dBasic+dCGSTVal+dSGSTVal+dIGSTVal+dCessVal;

	      String SBlock = ((String)RowVector.elementAt(3)).trim();
	      if(SBlock.equals(""))
	      {
                   RowVector.setElementAt("SST",3);
	      }

              /*if(!bflag)
              {
                   if(dNet>200000)
                   {
                        RowVector.setElementAt("NST",3);
                   }
                   else
                   {
                        RowVector.setElementAt("SST",3);
                   }
              }*/

              RowVector.setElementAt(common.getRound(dCGST,2),8);
              RowVector.setElementAt(common.getRound(dSGST,2),9);
              RowVector.setElementAt(common.getRound(dIGST,2),10);
              RowVector.setElementAt(common.getRound(dCess,2),11);
              RowVector.setElementAt(common.getRound(dGross,2),12);
              RowVector.setElementAt(common.getRound(dDisc,2),13);
              RowVector.setElementAt(common.getRound(dCGSTVal,2),14);
              RowVector.setElementAt(common.getRound(dSGSTVal,2),15);
              RowVector.setElementAt(common.getRound(dIGSTVal,2),16);
              RowVector.setElementAt(common.getRound(dCessVal,2),17);
              RowVector.setElementAt(common.getRound(dNet,2),18);


	      /*if(SSeleSupType.equals("1"))
	      {
	     	   if(SItemStatus.equals("0"))
		   {
			   if(SSeleStateCode.equals("33"))
			   {
				if(dIGST>0)
				{
				     RowVector.setElementAt(common.getRound((dIGST/2),2),8);
				     RowVector.setElementAt(common.getRound((dIGST/2),2),9);
				     RowVector.setElementAt(common.getRound((dIGSTVal/2),2),14);
				     RowVector.setElementAt(common.getRound((dIGSTVal/2),2),15);
	      		             RowVector.setElementAt("0",10);
	      		             RowVector.setElementAt("0",16);
				}
			   }
			   else
			   {
				if(dCGST>0 || dSGST>0)
				{
				     RowVector.setElementAt(common.getRound((dCGST+dSGST),2),10);
				     RowVector.setElementAt(common.getRound((dCGSTVal+dSGSTVal),2),16);
	      		             RowVector.setElementAt("0",8);
	      		             RowVector.setElementAt("0",9);
	      		             RowVector.setElementAt("0",14);
	      		             RowVector.setElementAt("0",15);
				}
			   }
		   }
	      }*/

              for(int i=0;i<super.dataVector.size();i++)
              {
		  String SHType = (String)VHsnType.elementAt(i);
                  Vector curVector = (Vector)super.dataVector.elementAt(i);
                  dTGross   = dTGross+common.toDouble((String)curVector.elementAt(12));
                  dTDisc    = dTDisc+common.toDouble((String)curVector.elementAt(13));
                  dTCGST    = dTCGST+common.toDouble((String)curVector.elementAt(14));
                  dTSGST    = dTSGST+common.toDouble((String)curVector.elementAt(15));
                  dTIGST    = dTIGST+common.toDouble((String)curVector.elementAt(16));
                  dTCess    = dTCess+common.toDouble((String)curVector.elementAt(17));
                  dTNet     = dTNet+common.toDouble((String)curVector.elementAt(18));

		  if(SHType.equals("1"))
		  {
                       dTOthers = dTOthers+common.toDouble((String)curVector.elementAt(18));
		  }
              }

              dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
              LBasic.setText(common.getRound(dTGross,2));
              LDiscount.setText(common.getRound(dTDisc,2));
              LCGST.setText(common.getRound(dTCGST,2));
              LSGST.setText(common.getRound(dTSGST,2));
              LIGST.setText(common.getRound(dTIGST,2));
              LCess.setText(common.getRound(dTCess,2));
              LNet.setText(common.getRound(dTNet,2));
              LOthers.setText(common.getRound(dTOthers,2));
    }

    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
                FinalData[i][j] = (String)curVector.elementAt(j);
        }
        return FinalData;
    }

    public int getRows()
    {
         return super.dataVector.size();
    }

    public Vector getCurVector(int i)
    {
         return (Vector)super.dataVector.elementAt(i);
    }

    public void setSupData()
    {
	 iStateCheck = 0;
	 iTypeCheck  = 0;

	 SSeleSupCode   = "";
	 SSeleSupType   = "";
	 SSeleStateCode = "";

	 String Str = TSupData.getText();
         StringTokenizer ST = new StringTokenizer(Str,"`");
         while(ST.hasMoreElements())
	 {
              SSeleSupCode   = ST.nextToken();
              SSeleStateCode = ST.nextToken();
              SSeleSupType   = ST.nextToken();
	 }
	 System.out.println("SupData:"+SSeleSupCode+":"+SSeleStateCode+":"+SSeleSupType);

         if(SSeleSupType.equals("") || SSeleSupType.equals("0"))
	 {
	      iTypeCheck = 0;
	 }
	 else
	 {
	      iTypeCheck = 1;
	 }

         if(SSeleStateCode.equals("") || SSeleStateCode.equals("0"))
	 {
	      iStateCheck = 0;
	 }
	 else
	 {
	      iStateCheck = 1;
	 }
    }

    public void setTaxData()
    {
	iHsnCheck = 0;

	setHsnData();

	boolean bHsnCheck = checkHsnCode();

	if(bHsnCheck)
	{
		iHsnCheck = 1;
		
		for(int i=0;i<super.dataVector.size();i++)
		{
		     Vector curVector = (Vector)super.dataVector.elementAt(i);
		     String SHsnType  = (String)VHsnType.elementAt(i);
		     String SItemStatus = (String)VItemsStatus.elementAt(i);
		     setMaterialAmount(curVector,SHsnType,SItemStatus,i);
		}
	}
	else
	{
	     iHsnCheck = 0;
	}
    }

    public void setHsnData()
    {
        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
		
	       for(int i=0;i<super.dataVector.size();i++)
	       {
		     Vector curVector   = (Vector)super.dataVector.elementAt(i);
		     String SHsnType    = (String)VHsnType.elementAt(i);
		     String SItemStatus = (String)VItemsStatus.elementAt(i);

		     if(SHsnType.equals("1"))
		          continue;

		     if(!SItemStatus.equals("0"))
			  continue;

                     String SPHsnCode = (String)curVector.elementAt(2);
		     String SItemCode = (String)curVector.elementAt(0);		

		     String SNHsnCode = "";

		     String QS = "Select HsnCode from InvItems where Item_Code='"+SItemCode+"'";

		     ResultSet result = theStatement.executeQuery(QS);
		     while(result.next())
		     {
		          SNHsnCode = common.parseNull(result.getString(1));
		     }
		     result.close();

		     if(SNHsnCode.equals("") || SNHsnCode.equals("0"))
		          continue;

		     if(SNHsnCode.equals(SPHsnCode))
		          continue;

		     if(SPHsnCode.equals("") || SPHsnCode.equals("0"))
		     {
		          curVector.setElementAt(SNHsnCode,2);
		     }
	       }
	       theStatement.close();
        }
        catch(Exception ex)
        {
               System.out.println("setHsnData :"+ex);
        }
    }

    public boolean checkHsnCode()
    {
	for(int i=0;i<super.dataVector.size();i++)
	{
	     Vector curVector = (Vector)super.dataVector.elementAt(i);
             String SHsnCode  = (String)curVector.elementAt(2);

	     int iRateCheck=0;

	     if((SHsnCode.equals("") || SHsnCode.equals("0")))
	     {
	          iRateCheck = 1;
	     }
	     else
	     {
	          if(SSeleSupType.equals("1"))
		  {
		       iRateCheck = getRateCheck(SHsnCode);
		  }
		  else
		  {
		       iRateCheck = 1;
		  }
	     }

	     if(iRateCheck<=0)
	     {
	          JOptionPane.showMessageDialog(null,"Gst Rate Not Updated for HsnCode-"+SHsnCode,"Information",JOptionPane.INFORMATION_MESSAGE);
	          return false;
	     }
	}
	return true;
    }
	
     public int getRateCheck(String SHsnCode)
     {
	int iCount=0;

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "Select Count(*) from HsnGstRate Where HsnCode='"+SHsnCode+"'";

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    iCount = result.getInt(1);
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getRateCheck :"+ex);
        }
	return iCount;
     }

     public void getGstRate(String SHsnCode)
     {
        dCGST=0;
	dSGST=0;
	dIGST=0;
	dCess=0;

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "";

	       if(SSeleStateCode.equals("33"))
	       {
		    QS = " Select nvl(CGST,0) as CGST,nvl(SGST,0) as SGST,0 as IGST,nvl(Cess,0) as Cess "+
		         " from HsnGstRate Where HsnCode='"+SHsnCode+"'";
	       }
	       else
	       {
		    QS = " Select 0 as CGST,0 as SGST,nvl(IGST,0) as IGST,nvl(Cess,0) as Cess "+
		         " from HsnGstRate Where HsnCode='"+SHsnCode+"'";
	       }

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    dCGST = result.getDouble(1);
                    dSGST = result.getDouble(2);
                    dIGST = result.getDouble(3);
                    dCess = result.getDouble(4);
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getGstRate :"+ex);
        }
     }
}
