package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class DeletionListFrame extends JInternalFrame
{
     JPanel    TopPanel;
     JPanel    BottomPanel;
     JPanel    ControlPanel;
     JPanel    ListPanel;
     
     JButton   BApply,BOk,BExit;
     
     Vector VCCode,VCName,VUCode,VUName;
     Vector VOrderQty,VReciveQty;
     
     Object RowData[][];
     String ColumnData[] = {"Order No","Date","Block","Supplier","Mrs No","Code","Name","Qty","ReciveQty","Status"};
     String ColumnType[] = {"E"       ,"S"   ,"S"    ,"S"       ,"N"     ,"S"   ,"S"   ,"N" ,"N" ,"B"};

     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;
     int iUserCode,iMillCode,iAuthCode;
     String SSupTable;

     JTextField TOrderNo;
     
     Common common = new Common();
     
     Vector VOrdDate,VBlock,VBlockCode,VOrdNo,VMrsNo,VSupName,VOrdCode,VOrdName,VOrdDeptName,VOrdCataName,VOrdUnitName,VOrdConcern,VOrdQty,VOrdRate,VOrdNet,VOrdDue,VStatus,VRecQty,VInvQty,VSlNo,VGrnStatus,VMrsSlNo;
     Vector VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup;
     
     AlterTabReport tabreport;
     int iOrderStatus  = 0;
     Vector VItemsStatus;

     int iARNo=0;
     int iBPANo=0;

     Connection theConnect = null;

     boolean bComflag = true;
     
     public DeletionListFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode,String SSupTable)
     {
          try
          {
               this.DeskTop   = DeskTop;
               this.VCode     = VCode;
               this.VName     = VName;
               this.SPanel    = SPanel;
               this.iUserCode = iUserCode;
               this.iMillCode = iMillCode;
               this.iAuthCode = iAuthCode;
               this.SSupTable = SSupTable;
               
               getDBConnection();
               createComponents();
               setLayouts();
               addComponents();
               addListeners();
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }

     }

     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnect     =    oraConnection.getConnection();
     }

     public void createComponents()
     {
          try
          {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();

          TOrderNo       = new JTextField();
          BOk            = new JButton("Delete");
          BExit          = new JButton("Exit");
          BApply         = new JButton("Apply");
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void setLayouts()
     {
          try
          {
          TopPanel       .setLayout(new GridLayout(1,3));
          BottomPanel.setBorder(new TitledBorder(""));

          setTitle("Order Deletion Frame");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void addComponents()
     {
          try
          {
          TopPanel.add(new JLabel("Order No"));
          TopPanel.add(TOrderNo);
          TopPanel.add(BApply);

          BottomPanel.add(BOk);
          BottomPanel.add(BExit);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void addListeners()
     {
          try
          {
               BApply.addActionListener(new ApplyList());
               BExit.addActionListener(new ApplyList());
               BOk.addActionListener(new ApplyList());
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
                    setDataIntoVector();
                    setRowData();
                    try
                    {
                         getContentPane().remove(tabreport);
                    }
                    catch(Exception ex){}
                    try
                    {
                         tabreport = new AlterTabReport(RowData,ColumnData,ColumnType,VGrnStatus);
     
                         getContentPane().add(tabreport,BorderLayout.CENTER);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
               if(ae.getSource()==BOk)
               {
                    if(validMrs())
                    {
                         if(validAdvance())
                         {
                              for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
                              {
                                   Boolean bFlag     = (Boolean)tabreport.ReportTable.getValueAt(i,9);
          
                                   if(bFlag.booleanValue())
                                   {
                                        String SOrderNo  = common.parseNull((String)VOrdNo.elementAt(i));
                                        String SItemCode = common.parseNull((String)VCCode.elementAt(i));
                                        int iOrdBlock    = common.toInt((String)VBlockCode.elementAt(i));
                                        int iSlNo        = common.toInt((String)VSlNo.elementAt(i));
                                        int iMrsSlNo     = common.toInt((String)VMrsSlNo.elementAt(i));
                                        int iMrsNo       = common.toInt((String)VMrsNo.elementAt(i));
          
                                        DeleteData(SOrderNo,SItemCode,iOrdBlock,iSlNo,iMrsSlNo,iMrsNo);
                                   }
                              }
    
                              try
                              {
                                   if(bComflag)
                                   {
                                        theConnect     . commit();
                                        JOptionPane.showMessageDialog(null,"Order Deleted Successfully ","Information",JOptionPane.INFORMATION_MESSAGE);
                                        System         . out.println("Commit");
                                        theConnect     . setAutoCommit(true);
                                        removeHelpFrame();
                                   }
                                   else
                                   {
                                        theConnect     . rollback();
                                        JOptionPane.showMessageDialog(null,"Problem in Deletion - 2. Contact EDP ","Error",JOptionPane.ERROR_MESSAGE);
                                        System         . out.println("RollBack");
                                        theConnect     . setAutoCommit(true);
                                   }
                              }catch(Exception ex)
                              {
                                   ex.printStackTrace();
                              }
                         }
                    }
               }
          }
     }

     public boolean validMrs()
     {
          String SCurDate   = common.parseDate(common.getServerPureDate());

          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
          {
               Boolean bFlag     = (Boolean)tabreport.ReportTable.getValueAt(i,9);

               if(bFlag.booleanValue())
               {
                    String SOrderNo  = common.parseNull((String)VOrdNo.elementAt(i));
                    String SItemCode = common.parseNull((String)VCCode.elementAt(i));
                    int iOrdBlock    = common.toInt((String)VBlockCode.elementAt(i));
                    int iMrsNo       = common.toInt((String)VMrsNo.elementAt(i));
                    String SOrdDate  = common.parseDate((String)VOrdDate.elementAt(i));

                    int iDateDiff = common.toInt(common.getDateDiff(SCurDate,SOrdDate));
    
                    /*if(iUserCode!=16)
                    {
                         if(iDateDiff>2)
                         {
                              JOptionPane.showMessageDialog(null,"Order Date Limit Exceeded - Contact EDP ","Attention",JOptionPane.INFORMATION_MESSAGE);
                              return false;
                         }
                    }*/

                    if(iMrsNo<=0)
                         continue;

                    int iCount = checkMrs(SOrderNo,SItemCode,iOrdBlock,iMrsNo);

                    if(iCount<=0)
                    {
                         JOptionPane.showMessageDialog(null,"Problem in Deletion - MRS Prob - . Contact EDP ","Error",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }
               }
          }
          return true;
     }

     public int checkMrs(String SOrderNo,String SItemCode,int iOrdBlock,int iMrsNo)
     {
          int iCount=0;
          try
          {
               String QS =" Select Count(*) from MRS Where OrderNo="+SOrderNo+" and OrderBlock="+iOrdBlock+" and MrsNo="+iMrsNo+" and Item_Code='"+SItemCode+"' and MillCode="+iMillCode;

               Statement      stat          =  theConnect.createStatement();
               ResultSet      result        =  stat.executeQuery(QS);
               while(result.next())
               {
                    iCount    = result.getInt(1);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
          return iCount;
     }

     public boolean validAdvance()
     {
          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
          {
               Boolean bFlag     = (Boolean)tabreport.ReportTable.getValueAt(i,9);

               if(bFlag.booleanValue())
               {
                    String SOrderNo  = common.parseNull((String)VOrdNo.elementAt(i));
                    String SItemCode = common.parseNull((String)VCCode.elementAt(i));
                    int iOrderSlNo   = common.toInt((String)VSlNo.elementAt(i));

                    int iCount = checkAdvance(SOrderNo,SItemCode,iOrderSlNo);

                    if(iCount<=0)
                         continue;


                    iARNo=0;
                    iBPANo=0;
                
                    getAdvanceARNo(SOrderNo,SItemCode,iOrderSlNo);

                    if(iARNo>0 || iBPANo>0)
                    {
                         JOptionPane.showMessageDialog(null,"Problem in Deletion Due to Advance Process. Contact EDP ","Error",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }
               }
          }
          return true;
     }

     public int checkAdvance(String SOrderNo,String SItemCode,int iOrderSlNo)
     {
          int iCount=0;
          try
          {
               String QS =" Select Count(*) from AdvRequest Where OrderNo="+SOrderNo+" and OrderSlNo="+iOrderSlNo+" and Code='"+SItemCode+"' and MillCode="+iMillCode;

               Statement      stat          =  theConnect.createStatement();
               ResultSet      result        =  stat.executeQuery(QS);
               while(result.next())
               {
                    iCount    = result.getInt(1);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
          return iCount;
     }

     public void getAdvanceARNo(String SOrderNo,String SItemCode,int iOrderSlNo)
     {
          iARNo=0;
          iBPANo=0;
          try
          {
               String QS =" Select Max(ARNo),Max(BPANo) from AdvRequest Where OrderNo="+SOrderNo+" and OrderSlNo="+iOrderSlNo+" and Code='"+SItemCode+"' and MillCode="+iMillCode+" Group by OrderNo,OrderSlNo ";

               Statement      stat          =  theConnect.createStatement();
               ResultSet      result        =  stat.executeQuery(QS);
               while(result.next())
               {
                    iARNo   = result.getInt(1);
                    iBPANo  = result.getInt(2);
               }
               result    . close();	
               stat      . close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     public void setDataIntoVector()
     {
          VOrdDate       = new Vector();
          VOrdNo         = new Vector();
          VBlock         = new Vector();
          VSupName       = new Vector();
          VMrsNo         = new Vector();
          VCCode         = new Vector();
          VCName         = new Vector();
          VOrdQty        = new Vector();
          VRecQty        = new Vector();
          VInvQty        = new Vector();
          VBlockCode     = new Vector();
          VSlNo          = new Vector();
          VGrnStatus     = new Vector();
          VMrsSlNo       = new Vector();

          try
          {
               Statement      stat          =  theConnect.createStatement();

               String SOrderNo = TOrderNo.getText();

               String QString =    " Select PurchaseOrder.OrderNO,OrderDate,BlockName,"+SSupTable+".Name,MRSNo,PurchaseOrder.Item_Code,Item_Name,PurchaseORder.Qty,0 as RecQty,decode(PurchaseOrder.InvQty,0,0,PurchaseOrder.InvQty,1),PurchaseOrder.OrderBlock,PurchaseOrder.SlNo,InvQty,MrsSlno from PurchaseOrder "+
                                   " Inner join InvItems on InvItems.Item_Code=PurchaseOrder.Item_Code "+
                                   " Inner join "+SSupTable+" On "+SSupTable+".Ac_Code=PurchaseOrder.Sup_Code "+
                                   " Inner Join OrdBlock On OrdBlock.Block=PurchaseOrder.OrderBlock "+
                                   " Where PurchaseOrder.OrderNo='"+SOrderNo+"' and PurchaseOrder.MillCode="+iMillCode+
                                   " Order By 1 ";

               ResultSet res  = stat.executeQuery(QString);

               while (res.next())
               {
                    VOrdNo         .addElement(res.getString(1));
                    VOrdDate       .addElement(res.getString(2));
                    VBlock         .addElement(res.getString(3));
                    VSupName       .addElement(res.getString(4));
                    VMrsNo         .addElement(res.getString(5));
                    VCCode         .addElement(res.getString(6));
                    VCName         .addElement(res.getString(7));     
                    VOrdQty        .addElement(res.getString(8));
                    VRecQty        .addElement(res.getString(9));
                    VInvQty        .addElement(res.getString(13));
                    VBlockCode     .addElement(res.getString(11));
                    VSlNo          .addElement(res.getString(12));
                    VGrnStatus     .addElement(res.getString(10));
                    VMrsSlNo       .addElement(res.getString(14));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void DeleteData(String SOrderNo,String SItemCode,int iOrdBlock,int iSlNo,int iMrsSlNo,int iMrsNo)
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat = theConnect.createStatement();

               String QS = " Insert into DeletedOrder (Select * from PurchaseOrder Where OrderNo='"+SOrderNo+"' and Item_Code='"+SItemCode+"' and OrderBlock="+iOrdBlock+" and MillCode="+iMillCode+" and SlNo="+iSlNo+" ) ";
               String QS1= " Insert into DeletedMatDesc (Select * from MatDesc Where OrderNo='"+SOrderNo+"' and Item_Code='"+SItemCode+"' and OrderBlock="+iOrdBlock+" and MillCode="+iMillCode+" and SlNo="+iSlNo+" ) ";
               stat.executeUpdate(QS);
               stat.executeUpdate(QS1);
               String QString1="Delete from MatDesc Where OrderNo='"+SOrderNo+"' and Item_Code='"+SItemCode+"' and OrderBlock="+iOrdBlock+" and MillCode="+iMillCode+" and SlNo="+iSlNo+" ";
               stat.executeUpdate(QString1);
               String QString ="Delete from PurchaseOrder Where InvQty=0 and OrderNo='"+SOrderNo+"' and Item_Code='"+SItemCode+"' and OrderBlock="+iOrdBlock+" and MillCode="+iMillCode+" and SlNo="+iSlNo+" ";
               stat.executeUpdate(QString);
               String QString2 ="Update MRS Set OrderNo=0,OrderBlock=0,AutoOrderConversion=0 Where OrderNo="+SOrderNo+" and OrderBlock="+iOrdBlock+" and MrsNo="+iMrsNo+" and Item_Code='"+SItemCode+"' and MillCode="+iMillCode;
               stat.executeUpdate(QString2);

               String QString3 = " Insert into DeletedAdvRequest (Select * from AdvRequest Where OrderNo='"+SOrderNo+"' and Code='"+SItemCode+"' and OrderBlock="+iOrdBlock+" and MillCode="+iMillCode+" and OrderSlNo="+iSlNo+" ) ";
               stat.executeUpdate(QString3);
               String QString4 ="Delete from AdvRequest Where OrderNo='"+SOrderNo+"' and Code='"+SItemCode+"' and OrderBlock="+iOrdBlock+" and MillCode="+iMillCode+" and OrderSlNo="+iSlNo+" ";
               stat.executeUpdate(QString4);
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }
     public void setRowData()
     {
          try
          {
               RowData     = new Object[VOrdDate.size()][10];
               for(int i=0;i<VOrdDate.size();i++)
               {
                    RowData[i][0]  = common.parseNull((String)VOrdNo         .elementAt(i));
                    RowData[i][1]  = common.parseDate((String)VOrdDate       .elementAt(i));
                    RowData[i][2]  = common.parseNull((String)VBlock         .elementAt(i));
                    RowData[i][3]  = common.parseNull((String)VSupName       .elementAt(i));
                    RowData[i][4]  = common.parseNull((String)VMrsNo         .elementAt(i));
                    RowData[i][5]  = common.parseNull((String)VCCode         .elementAt(i));
                    RowData[i][6]  = common.parseNull((String)VCName         .elementAt(i));
                    RowData[i][7]  = common.parseNull((String)VOrdQty        .elementAt(i));
                    RowData[i][8]  = common.parseNull((String)VInvQty        .elementAt(i));
                    RowData[i][9]  = new Boolean(false);
               }
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

}
