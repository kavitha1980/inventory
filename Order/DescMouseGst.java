package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class DescMouseGst extends JInternalFrame
{
      JTextField         TMatCode,TMatName;
      MyTextField        TDesc,TMake;
      JTextField         TDraw;
      JTextField         TCatl;

      JButton            BOk,BCancel;
                  
      JPanel             TopPanel,BottomPanel;
      JLayeredPane       Layer;
      Vector             VDesc,VMake,VDraw,VCatl;
      JTable             ReportTable;
      int                iItemStatus;

      String             str="";
      String             SCatl="",SDraw="";

      Common             common         = new Common();
      Connection         theConnection  = null;

      DescMouseGst(JLayeredPane Layer,Vector VDesc,Vector VMake,Vector VDraw,Vector VCatl,JTable ReportTable,int iItemStatus)
      {
          this.Layer       = Layer;
          this.VDesc       = VDesc;
          this.VMake       = VMake;
          this.VDraw       = VDraw;
          this.VCatl       = VCatl;
          this.ReportTable = ReportTable;
          this.iItemStatus = iItemStatus;

          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
      }

      public void createComponents()
      {
          BOk            = new JButton("Okay");
          BCancel        = new JButton("Cancel");

          TopPanel       = new JPanel(true);
          BottomPanel    = new JPanel(true);

          TMatCode       = new JTextField();
          TDesc          = new MyTextField(250);
          TMatName       = new JTextField();
          TMake          = new MyTextField(25);
          TDraw          = new JTextField();
          TCatl          = new JTextField();

          TMatCode       . setEditable(false);
          TMatName       . setEditable(false);
          TCatl          . setEditable(false);
          TDraw          . setEditable(false);

          if(iItemStatus==0)
          {
               TDesc          . setEditable(true);
               TMake          . setEditable(true);
          }
          else
          {
               TDesc          . setEditable(false);
               TMake          . setEditable(false);
          }
      }

      public void setLayouts()
      {
          setBounds(80,100,450,350);
          setResizable(true);
          setTitle("Description");
          TopPanel.setLayout(new GridLayout(6,2));

      }

      public void addComponents()
      {
          getContentPane().add("North",TopPanel);
          getContentPane().add("South",BottomPanel);

          TopPanel.add(new JLabel("Material Name"));
          TopPanel.add(TMatName);
          TopPanel.add(new JLabel("Material Code"));
          TopPanel.add(TMatCode);
          TopPanel.add(new JLabel("Description"));
          TopPanel.add(TDesc);
          TopPanel.add(new JLabel("Material Make"));
          TopPanel.add(TMake);
          TopPanel.add(new JLabel("Drawing No"));
          TopPanel.add(TDraw);
          TopPanel.add(new JLabel("Catalogue No"));
          TopPanel.add(TCatl);

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);
      }

      public void addListeners()
      {
          BOk       . addActionListener(new ActList());
          BCancel   . addActionListener(new ActList());
      }

      public void setPresets()
      {
          try
          {
          int       i    = 0;
          String    SCode= "",SName=    "";

          i         = ReportTable.getSelectedRow();

          SCode     = (String)ReportTable.getModel().getValueAt(i,0);
          SName     = (String)ReportTable.getModel().getValueAt(i,1);

          TMatCode  . setText(SCode);
          TMatName  . setText(SName);

          TDesc     . setText((String)VDesc.elementAt(i));
          TDraw     . setText((String)VDraw.elementAt(i));
          TCatl     . setText((String)VCatl.elementAt(i));
          TMake     . setText((String)VMake.elementAt(i));

          TDesc     . addKeyListener(new DescKeyList());

          }catch(Exception ex)
          {ex.printStackTrace();}
      }

      private class DescKeyList extends KeyAdapter
      {
          public void keyReleased(KeyEvent ke)
          {
               String str = TDesc.getText();
               if(str.length() > 250)
               {
                    str       = str.substring(0,250);
                    TDesc     . setText(str);
               }
          }
      }

      public class ActList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    setTableDetails();
                    removeHelpFrame();
                    ReportTable.requestFocus();
               }
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
                    ReportTable    . requestFocus();
               }
          }
      }

      private void setTableDetails()
      {
               int i = 0;
                   i = ReportTable.getSelectedRow();

               VDesc     . setElementAt(TDesc.getText(),i);
               VMake     . setElementAt(TMake.getText(),i);
               VDraw     . setElementAt(TDraw.getText(),i);
               VCatl     . setElementAt(TCatl.getText(),i);
      }

      public void removeHelpFrame()
      {
            try
            {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
            }
            catch(Exception ex) { }
      }
}
