/*
   Order Placement Utility for Materials not requested thro' MRS.
   Simply,this utility is to  handle order without any plan. 
   Hence OrderPlanMiddlePanel and PlanMiddle is replaced with a
   generalized OrderMiddlePanel component.

   This will also act like an Ammendment component for a placed
   purchase Order
*/
package Order;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.util.Date;

import util.*;
import guiutil.*;
import jdbc.*;
import java.net.*;

public class DirectOrderFrame extends JInternalFrame
{
     JPanel              TopPanel,TopLeft,TopRight,BottomPanel;
     OrderMiddlePanel    MiddlePanel;
     Connection          theConnect = null;
                         
     JButton             BOk,BCancel,BSupplier,BMaterials;
     NextField           TOrderNo,TPayDays;
     JTextField          TSupCode,TRef;
     JTextField          TAdvance,TPayTerm;
     DateField2          TDate;
     JComboBox           JCTo,JCThro,JCForm,JEPCG,JCOrderType,JCProject,JCState,JCPort;

     JLayeredPane        DeskTop;
     Vector              VCode,VName;
     StatusPanel         SPanel;

     Vector              VTo,VThro,VToCode,VThroCode,VFormCode,VForm,VEpcg,
                         VOrderType,VPort,VPortCode,VProject,VState,
                         VBlockName,VBlockCode,VNameCode;

     FileWriter          FW;

     Common              common      = new Common();
     String              SOrderNo    = "";
     String              SInvOrderNo = "";

     int                 iAmend,iUserCode;
     int                 iMillCode = 0;
     String              SYearCode;
     String              SItemTable,SSupTable;

     // to identify creation of a purchase order or its Ammendment
     // false indicates creation mode      true indicates modification mode     

     boolean             bflag;
     boolean             bComflag = true;

     int       iMaxSlNo  = 0;

     int iMSlNo=0;
     int iMUser=1;
     int iMType=0;

     Vector VMUser,VMUserCode;

     JTextField          TProformaNo,TReferenceNo;
     JComboBox           JCCurrencyType;
     Vector              VCurrencyTypeCode, VCurrencyTypeName;

     int iActualAmend=0;
     
     public DirectOrderFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,boolean bflag,int iAmend,int iUserCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable)
     {
          this.DeskTop    = DeskTop;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.SPanel     = SPanel;
          this.bflag      = bflag;
          this.iAmend     = iAmend;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
          getNextOrderNo();

          System.out.println("Year -- "+SYearCode);
     }

     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnect     =    oraConnection.getConnection();
     }

     public void createComponents()
     {
          TopPanel       = new JPanel();
          TopLeft        = new JPanel();
          TopRight       = new JPanel();
          BottomPanel    = new JPanel();
          
          BOk            = (bflag?new JButton("Update"):new JButton("Okay"));
          BCancel        = new JButton("Abort");
          
          BSupplier      = new JButton("Supplier");
          BMaterials     = new JButton("Select Materials");
          
          TOrderNo       = new NextField();
          TDate          = new DateField2();
          TSupCode       = new JTextField();
          TAdvance       = new JTextField();
          TPayTerm       = new JTextField();
          TPayDays       = new NextField();
          TRef           = new JTextField();

          getVTo();
          
          JCTo           = new JComboBox(VTo);
          JCThro         = new JComboBox(VThro);
          JCForm         = new JComboBox(VForm);
          JCPort         = new JComboBox(VPort);
          JEPCG          = new JComboBox();
          JCOrderType    = new JComboBox();
          JCProject      = new JComboBox();
          JCState        = new JComboBox();

          JCCurrencyType = new JComboBox(VCurrencyTypeName);

          String SMOrderNo = TOrderNo.getText();

          MiddlePanel    = new OrderMiddlePanel(DeskTop,VCode,VName,VNameCode,iMillCode,bflag,SMOrderNo,SItemTable);
          
          TDate          . setTodayDate();
          TOrderNo       . setEditable(false);
          TPayDays       . setEditable(false);
          TAdvance       . setEditable(false);

          TReferenceNo   = new JTextField();
          TReferenceNo   . setEditable(false);

          TProformaNo    = new JTextField();
          JCCurrencyType = new JComboBox(VCurrencyTypeName);

          TProformaNo    . setEnabled(false);
          JCCurrencyType . setEnabled(false);
           JCCurrencyType . setSelectedItem("RUPEES");
     }

     public void setLayouts()
     {
          TopPanel  . setLayout(new GridLayout(1,2));
          TopLeft   . setLayout(new GridLayout(10,2,3,3));
          TopRight  . setLayout(new GridLayout(8,2,3,3));
          
          if(bflag)
          {
               setTitle("Amendment Or Modification to Purchase Order");
          }
          else
          {
               setTitle("Direct Order Placement Without MRS");
          }
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          
          getContentPane()    . setLayout(new BorderLayout());
          BottomPanel         . setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          JEPCG.addItem("Non-EPCG Order   ");
          JEPCG.addItem("EPCG Order       ");
          
          JCOrderType.addItem("Local Order      ");
          JCOrderType.addItem("Import           ");
          
          JCProject  .addItem("Other Projects     ");
          JCProject  .addItem("Machinery Projects ");
          
          JCState    .addItem("None             ");
          JCState    .addItem("Intra State      ");
          JCState    .addItem("Inter State      ");
          
          TopLeft.add(new JLabel("Order No"));
          TopLeft.add(TOrderNo);
          
          TopLeft.add(new JLabel("Order Date"));
          TopLeft.add(TDate);

          TopLeft.add(new JLabel("Supplier"));
          TopLeft.add(BSupplier);

          TopLeft.add(new JLabel("Reference"));
          TopLeft.add(TRef);

          if(!bflag)
          {
               TopLeft       .add(new JLabel("List of Materials"));
               TopLeft       .add(BMaterials);
          }
          
          TopLeft.add(new JLabel("EPCG Order   "));
          TopLeft.add(JEPCG);

          TopLeft.add(new JLabel("Proforma No.   "));
          TopLeft.add(TProformaNo);

          TopLeft.add(new JLabel("Order Type   "));
          TopLeft.add(JCOrderType);
          
          TopLeft.add(new JLabel("Currency Type   "));
          TopLeft.add(JCCurrencyType);
          
          TopLeft.add(new JLabel("Project/Non"));
          TopLeft.add(JCProject);

          TopRight.add(new JLabel("Book To"));
          TopRight.add(JCTo);
          
          TopRight.add(new JLabel("Book Thro"));
          TopRight.add(JCThro);
          
          TopRight.add(new JLabel("Form No"));
          TopRight.add(JCForm);
          
          TopRight.add(new JLabel("Advance"));
          TopRight.add(TAdvance);
          
          TopRight.add(new JLabel("Payment Terms"));
          TopRight.add(TPayTerm);
          
//          TopRight.add(new JLabel("Payment Terms (Days)"));
//          TopRight.add(TPayDays);
          
          if (!bflag)
          {
               JCOrderType    .setEnabled(false);
               JCState        .setEnabled(false);
               JCPort         .setEnabled(false);
          }
          
          TopRight  . add(new JLabel("Intra/Inter State"));
          TopRight  . add(JCState);
          
          TopRight  . add(new JLabel("Port         "));
          TopRight  . add(JCPort);
          
          TopPanel  . add(TopLeft);
          TopPanel  . add(TopRight);
          
          TopLeft   . setBorder(new TitledBorder("Order Id Block-I"));
          TopRight  . setBorder(new TitledBorder("Order Id Block-II"));
          
          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BSupplier           . addActionListener(new SupplierSearch(DeskTop,TSupCode,SSupTable));
          JEPCG               . addFocusListener(new FocListener());
          JCOrderType         . addFocusListener(new FocListener());
          JCState             . addFocusListener(new FocListener());
          JCPort              . addFocusListener(new FocListener());

          JCOrderType         . addItemListener(new ItemList());

          if(bflag)
          {
               BOk            . addActionListener(new UpdtList());
          }
          else 
          {
               BMaterials     . addActionListener(new MaterialsSearch(DeskTop,VCode,VName,MiddlePanel,"Material Selector","Order",iMillCode,SItemTable));
               BOk            . addActionListener(new ActList());
               BCancel        . addActionListener(new CanList());
          }
     }

     public class FocListener implements FocusListener
     {
          public void focusLost(FocusEvent fe)
          {
               if (fe.getSource() == JEPCG)
               {
                    if (JEPCG.getSelectedIndex() == 0)
                    {
                         JCTo           . requestFocus();
                         JCOrderType    . setEnabled(false);
                         JCState        . setEnabled(false);
                         JCPort         . setEnabled(false);

                         TProformaNo    . setEnabled(false);
                         JCCurrencyType . setEnabled(false);
                    }
                    else
                    {
                         JCOrderType    . setEnabled(true);
                         JCState        . setEnabled(true);
                         JCPort         . setEnabled(true);

                         TProformaNo    . setEnabled(true);
                         JCCurrencyType . setEnabled(true);
                         TProformaNo    . requestFocus();

                         JCCurrencyType . setEnabled(false);
                         JCCurrencyType . setSelectedItem("RUPEES");
                    }
               }
               
               if ((fe.getSource() == JCOrderType) && (JEPCG.getSelectedIndex()==1))
               {
                    if (JCOrderType.getSelectedIndex() == 1)
                    {
                         JCState        . setEnabled(false);
                         JCPort         . setEnabled(true);
                         JCPort         . setSelectedIndex(1);
                         JCState        . setSelectedIndex(0);

                         JCCurrencyType . setEnabled(true);
                         JCCurrencyType . setSelectedIndex(0);
                    }
                    else
                    {
                         JCPort         . setSelectedIndex(0);                         
                         JCPort         . setEnabled(false);
                         JCState        . setEnabled(true);
                         JCState        . setSelectedIndex(1);                         

                         JCCurrencyType . setEnabled(false);
                         JCCurrencyType . setSelectedItem("RUPEES");
                    }
               }
          }
     
          public void focusGained(FocusEvent fe)
          {
          }
     }

     public class ItemList implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
               if (JCOrderType.getSelectedIndex() == 1)
               {
                    JCCurrencyType . setEnabled(true);
                    JCCurrencyType . setSelectedIndex(0);
               }
               else
               {
                    JCCurrencyType . setEnabled(false);
                    JCCurrencyType . setSelectedItem("RUPEES");
               }
          }
     }

     public class CanList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               removeHelpFrame();
          }
     }

     public class ActList implements ActionListener
     {
          int iAmend                    = 0;
          public void actionPerformed(ActionEvent AcE)
          {
               if(MiddlePanel.MiddlePanel == null)
               {
                    JOptionPane.showMessageDialog(null,"Please Select Materials ","Information",JOptionPane.INFORMATION_MESSAGE);
                    return;
               }

               Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();

               if((common.toInt(TOrderNo.getText())>0))
               {
                    BOk.setEnabled(false);
                    if(!isValidNew())
                    {
                         common.warn(DeskTop,SPanel);
                         BOk.setEnabled(true);
                         return;
                    }

                    if(JOptionPane.showConfirmDialog(null, "Are you sure you want to Save the Data?", "Confirm Save!", JOptionPane.YES_NO_OPTION) == 0)
                    {
                         try
                         {
                              insertOrderDetails();
                              insertDescDetails();
                              updateMrsDetails();
                              updateEnquiryDetails();
                              UpdateInvItems();
                         }
                         catch(Exception Ex)
                         {
                              Ex.printStackTrace();
                              bComflag = false;
                         }
                         try
                         {
                              if(bComflag)
                              {
                                   theConnect     . commit();
                                   System         . out.println("Commit");
                                   theConnect     . setAutoCommit(true);
                              }
                              else
                              {
                                   theConnect     . rollback();
                                   System         . out.println("RollBack-1");
                                   theConnect     . setAutoCommit(true);
                              }
                         }catch(Exception ex)
                         {
                              ex.printStackTrace();
                         }
                         removeHelpFrame();
                         DirectOrderFrame directorderframe = new DirectOrderFrame(DeskTop,VCode,VName,VNameCode,SPanel,false,iAmend,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);
                         DeskTop.add(directorderframe);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                         directorderframe.show();
                    }
                    else
                    {
                         BOk.setEnabled(true);
                    }
               }
          }
     }

     public class UpdtList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk  . setEnabled(false);
               if(!isValidNew())
               {
                    common    . warn(DeskTop,SPanel);
                    BOk       . setEnabled(true);
                    return;
               }

               if(!isValidQty())
               {
                    BOk       . setEnabled(true);
                    return;
               }

               int iSel = JOptionPane.showConfirmDialog(null, "Do you want to Update this Order. This will remove SO Approval"); 
               if (iSel == JOptionPane.YES_OPTION)
               {
                    try
                    {
                         insertAmendDetails();
                         updateDescDetails();
                         updateOrderDetails();
                         insertOrderDetails();
                         insertDescDetails();
                         updateMrsDetails();
                         updateEnquiryDetails();
                         updateOldMrsDetails();
                         UpdateInvItems();
                    }
                    catch(Exception Ex)
                    {
                         Ex.printStackTrace();
                         bComflag = false;
                    }
                    try
                    {
                         if(bComflag)
                         {
                              theConnect     . commit();
                              System         . out.println("Commit");
                              theConnect     . setAutoCommit(true);
                         }
                         else
                         {
                              theConnect     . rollback();
                              System         . out.println("RollBack");
                              theConnect     . setAutoCommit(true);
                         }
                    }catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
                    removeHelpFrame();
               }
               else
               {
                    BOk  . setEnabled(true);
               }
          }
     }

     public void insertOrderDetails()
     {
          String    QString   = "";
          int       iMRSSlno  = 0;
          int       iMrsUserCode = 1;
          int       iMRSType  = 0;
          
          if(bflag)
               QString = "Insert Into PurchaseOrder (id,OrderNo,OrderDate,OrderBlock,MrsNo,Sup_Code,Reference,Advance,PayTerms,PayDays,ToCode,ThroCode,FormCode,Item_Code,Qty,Rate,DiscPer,Disc,CenvatPer,CenVat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,Unit_Code,Dept_Code,Group_Code,DueDate,SlNo,OrderType,EPCG,portcode,state,Project_order,UserCode,ModiDate,MRSSlNo,MrsAuthUserCode,OrderTypeCode,TAXCLAIMABLE,Authentication,Amended,MillCode,ReferenceNo,YearCode,ProformaNo,Basic,DocId,CurrencyTypeCode) Values (";
          else
               QString = "Insert Into PurchaseOrder (id,OrderNo,OrderDate,OrderBlock,MrsNo,Sup_Code,Reference,Advance,PayTerms,PayDays,ToCode,ThroCode,FormCode,Item_Code,Qty,Rate,DiscPer,Disc,CenvatPer,CenVat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,Unit_Code,Dept_Code,Group_Code,DueDate,SlNo,OrderType,EPCG,portcode,state,Project_order,UserCode,ModiDate,MRSSlNo,MrsAuthUserCode,OrderTypeCode,TAXCLAIMABLE,Authentication,MillCode,ReferenceNo,YearCode,ProformaNo,Basic,DocId,CurrencyTypeCode) Values (";

          try
          {

               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat      = theConnect.createStatement();
               
                         TOrderNo  . setText(getInsertOrderNo());  // Incorporated On 23.05.2003 to enhance multi-user order numbering
               
               if(bflag)
                    iMaxSlNo = getMaxSlNo(((String)TOrderNo.getText()).trim());
               else
                    iMaxSlNo = 0;

               if(JEPCG.getSelectedIndex() == 0)
               {
                    TReferenceNo        . setText("0");
               }
               else
               {
                    TReferenceNo        . setText(getNextReferenceNo());
               }

               int k = iMaxSlNo;

               Object FinalData[][]     = MiddlePanel.MiddlePanel.getFromVector();
               String SAdd              = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess             = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm               = common.toDouble(SAdd)-common.toDouble(SLess);
               double dBasic            = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio            = dpm/dBasic;
               DateField2   date        = new DateField2();
               date.setTodayDate();
               
               for(int i=0;i<FinalData.length;i++)
               {
                    try
                    {
                         if(!isFreshRecord(i))
                              continue;
                    }
                    catch(Exception ex){}
     
                    String SMrsNo = ((String)FinalData[i][4]).trim();
                    iMrsUserCode  = common.toInt((String)MiddlePanel.MiddlePanel.getUserCode(i));

                    if(bflag)
                    {
                         String SMrsStatus = common.parseNull((String)MiddlePanel.MiddlePanel.VMrsStatus.elementAt(i)).trim();

                         if(SMrsStatus.equals("1"))
                         {
                              iMRSSlno     = common.toInt((String)MiddlePanel.VMrsSlNo.elementAt(i));
                              iMRSType     = common.toInt((String)MiddlePanel.VMrsType.elementAt(i));
                         }
                         else
                         {
                              if(common.toInt(SMrsNo)<=0)
                              {
                                   iMRSSlno     = 0;
                                   iMRSType     = 3;
                              }
                              else
                              {
                                   String SItemCode = (String)FinalData[i][0];
     
                                   getMrsSlNo(SItemCode,SMrsNo);

                                   iMRSSlno     = iMSlNo;
                                   iMRSType     = iMType;
                              }
                         }
                    }
                    else
                    {
                         if(common.toInt(SMrsNo)<=0)
                         {
                              iMRSSlno     = 0;
                              iMRSType     = 3;
                         }
                         else
                         {
                              String SItemCode = (String)FinalData[i][0];

                              getMrsSlNo(SItemCode,SMrsNo);

                              iMRSSlno     = iMSlNo;
                              iMRSType     = iMType;
                         }
                    }

                    String SBlockCode = (String)MiddlePanel.MiddlePanel.getBlockCode(i);
                    String SUnitCode  = (String)MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = (String)MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = (String)MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SDueDate   = (String)MiddlePanel.MiddlePanel.getDueDate(i,TDate);
                    String STaxClaim  = (String)MiddlePanel.MiddlePanel.getTaxClaim(i);
                       int iTaxClaim  = common.toInt(STaxClaim.trim());
                           dBasic     = common.toDouble(((String)FinalData[i][11]).trim());
                    String SMisc      = common.getRound(dBasic*dRatio,3);
                    
                    String    QS1 = QString;
                              QS1 = QS1+"0"+getNextVal("PURCHASEORDER_SEQ")+",";
                              QS1 = QS1+"0"+((String)TOrderNo.getText()).trim()+",";
                              QS1 = QS1+"'"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                              QS1 = QS1+"0"+SBlockCode+",";
                              QS1 = QS1+"0"+((String)FinalData[i][4]).trim()+",";
                              QS1 = QS1+"'"+((String)TSupCode.getText()).trim()+"',";
                              QS1 = QS1+"'"+(TRef.getText()).toUpperCase()+"',";    
                              QS1 = QS1+"0"+TAdvance.getText()+",";
                              QS1 = QS1+"'"+TPayTerm.getText()+"',";
                              QS1 = QS1+"0"+TPayDays.getText()+",";
                              QS1 = QS1+"0"+(String)VToCode.elementAt(JCTo.getSelectedIndex())+",";
                              QS1 = QS1+"0"+(String)VThroCode.elementAt(JCThro.getSelectedIndex())+",";
                              QS1 = QS1+"0"+(String)VFormCode.elementAt(JCForm.getSelectedIndex())+",";
                              QS1 = QS1+"'"+(String)FinalData[i][0]+"',";
                              QS1 = QS1+"0"+((String)FinalData[i][5]).trim()+",";
                              QS1 = QS1+"0"+common.getRound(((String)FinalData[i][6]).trim(),4)+",";
                              QS1 = QS1+"0"+((String)FinalData[i][7]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][12]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][8]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][13]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][9]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][14]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][10]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][15]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][16]).trim()+",";
                              QS1 = QS1+"0"+SAdd+",";
                              QS1 = QS1+"0"+SLess+",";
                              QS1 = QS1+"0"+SMisc+",";
                              QS1 = QS1+"0"+SUnitCode+",";
                              QS1 = QS1+"0"+SDeptCode+",";
                              QS1 = QS1+"0"+SGroupCode+",";
                              QS1 = QS1+"'"+SDueDate+"',";
                              QS1 = QS1+(k+1)+",";
                              QS1 = QS1+JCOrderType.getSelectedIndex()+",";
                              QS1 = QS1+JEPCG.getSelectedIndex()+",";
                              QS1 = QS1+"0"+(String)VPortCode.elementAt(JCPort.getSelectedIndex())+",";
                              QS1 = QS1+JCState.getSelectedIndex()+",";
                              QS1 = QS1+JCProject.getSelectedIndex()+",";
                              QS1 = QS1+iUserCode+",";
                              QS1 = QS1+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+",";
                              QS1 = QS1+iMRSSlno+",";
                              QS1 = QS1+iMrsUserCode+",";
                              QS1 = QS1+iMRSType+",";
                              QS1 = QS1+iTaxClaim+",";
                              QS1 = QS1+"1"+",";

                              if(bflag)
                                   QS1 = QS1+iActualAmend+",";

                              QS1 = QS1+iMillCode+",";
                              QS1 = QS1+"0"+((String)TReferenceNo.getText()).trim()+", ";
                              QS1 = QS1+SYearCode+", ";
                              QS1 = QS1+"'"+common.parseNull(TProformaNo.getText()).trim()+"', ";
                              QS1 = QS1+"0"+((String)FinalData[i][11]).trim()+", ";

                              QS1 = QS1+"0"+((String)FinalData[i][22]).trim()+", ";
                              QS1 = QS1+"0"+(String)VCurrencyTypeCode.elementAt(JCCurrencyType.getSelectedIndex())+") ";

                    stat.executeUpdate(QS1);
                    k++;
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("1"+ex);
               ex.printStackTrace();
          }
     }

     private int getNextVal(String SSequence) throws Exception
     {
          int iValue = -1;
          String QS = "Select "+SSequence+".NextVal from dual";
          try
          {
               Statement  stat  =  theConnect.createStatement();
               ResultSet result = stat.executeQuery(QS);
               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("getNextVal :"+SSequence+ex);
          }
          return iValue;
     }
     
     public void insertDescDetails()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);
               Statement stat      =  theConnect.createStatement();

               Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
               
               int k = iMaxSlNo;

               for(int i=0;i<FinalData.length;i++)
               {
                    String    QS1 = "";
                    try
                    {
                         if(!isFreshRecord(i))
                              continue;
                    }
                    catch(Exception ex){}

                    String SBlockCode = MiddlePanel.MiddlePanel.getBlockCode(i);
                    
                    String SDesc        = common.parseNull((String)MiddlePanel.MiddlePanel.VDesc.elementAt(i)).trim();
                    String SMake        = common.parseNull((String)MiddlePanel.MiddlePanel.VMake.elementAt(i)).trim();
                    String SDraw        = common.parseNull((String)MiddlePanel.MiddlePanel.VDraw.elementAt(i)).trim();
                    String SCatl        = common.parseNull((String)MiddlePanel.MiddlePanel.VCatl.elementAt(i)).trim();

                    String SItem_Code   = (String)FinalData[i][0];

                    if(isStationary(SItem_Code))
                    {
                         String    SPaperColor    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPColour   . elementAt(i));
                         String    SPaperSets     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSet      . elementAt(i));
                         String    SPaperSize     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSize     . elementAt(i));
                         String    SPaperSide     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSide     . elementAt(i));
                         String    SSlipFromNo    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSlipFrNo . elementAt(i));
                         String    SSlipToNo      =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSlipToNo . elementAt(i));
                         String    SBookFromNo    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPBookFrNo . elementAt(i));
                         String    SBookToNo      =    common.parseNull((String)MiddlePanel.MiddlePanel.VPBookToNo . elementAt(i));

                         QS1 = " Insert Into MatDesc (id,OrderNo,OrderDate,OrderBlock,Item_Code,Descr,Make,Draw,Catl,MillCode,SlNo,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE,SLIPFROMNO,SLIPTONO,BOOKFROMNO,BOOKTONO) Values (";
                         QS1 = QS1+"0"+getNextVal("MATDESC_SEQ")+",";
                         QS1 = QS1+"0"+TOrderNo.getText()+",";
                         QS1 = QS1+"'"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                         QS1 = QS1+"0"+SBlockCode+",";
                         QS1 = QS1+"'"+(String)FinalData[i][0]+"',";
                         QS1 = QS1+"'"+common.getNarration(SDesc)+"',";
                         QS1 = QS1+"'"+common.getNarration(SMake)+"',";
                         QS1 = QS1+"'"+SDraw+"',";
                         QS1 = QS1+"'"+SCatl+"',";
                         QS1 = QS1+iMillCode+",";
                         QS1 = QS1+(k+1)+",'";
                         QS1 = QS1+SPaperColor+"','";
                         QS1 = QS1+SPaperSets+"','";
                         QS1 = QS1+SPaperSize+"','";
                         QS1 = QS1+SPaperSide+"',";
                         QS1 = QS1+"0"+SSlipFromNo+",";
                         QS1 = QS1+"0"+SSlipToNo+",";
                         QS1 = QS1+"0"+SBookFromNo+",";
                         QS1 = QS1+"0"+SBookToNo+")";
                    }
                    else
                    {
                         QS1 = "Insert Into MatDesc (id,OrderNo,OrderDate,OrderBlock,Item_Code,Descr,Make,Draw,Catl,MillCode,SlNo) Values (";
                         QS1 = QS1+"0"+getNextVal("MATDESC_SEQ")+",";
                         QS1 = QS1+"0"+TOrderNo.getText()+",";
                         QS1 = QS1+"'"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                         QS1 = QS1+"0"+SBlockCode+",";
                         QS1 = QS1+"'"+(String)FinalData[i][0]+"',";
                         QS1 = QS1+"'"+common.getNarration(SDesc)+"',";
                         QS1 = QS1+"'"+common.getNarration(SMake)+"',";
                         QS1 = QS1+"'"+SDraw+"',";
                         QS1 = QS1+"'"+SCatl+"',";
                         QS1 = QS1+iMillCode+",";
                         QS1 = QS1+(k+1)+")";
                    }
                    stat.executeUpdate(QS1);
                    k++;
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("2"+ex);
          }
     }

     public void insertAmendDetails()
     {
          try
          {
               iActualAmend=0;

               int iAmendNo = 0;

               String SOrderNo = ((String)TOrderNo.getText()).trim();

               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat           =  theConnect.createStatement();
                                        

               iActualAmend = getActualAmend(SOrderNo);

               if(iActualAmend>0)
               {
                    iAmendNo = getAmendNo(SOrderNo);
               }

               String SSysName  = InetAddress.getLocalHost().getHostName();
               String SDateTime = common.getServerDate();

               String QS1 = " Insert Into OrderAmend (Id,NoofAmend,AmendUserCode,AmendSystemName,AmendDateTime,OrderId,OrderNo,OrderBlock,OrderDate,"+
                            " Sup_Code,Reference,Advance,MrsNo,Item_Code,Qty,Rate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,"+
                            " DueDate,PayTerms,InvQty,Dept_Code,Group_Code,Unit_Code,ToCode,ThroCode,Posted,AdvSlNo,AmendmentFlag,FormCode,Class_Code,"+
                            " SlNo,PYPost,WA,DM,Freight,PF,Others,EPCG,OrderType,LicenseNumber,Project_Order,PortCode,State,Amended,UserCode,ModiDate,"+
                            " MillCode,PayDays,Status,Flag,MrsSlNo,TaxClaimable,Authentication,IntimationNo,MailStatus,MrsAuthUserCode,MailConfirmStatus,"+
                            " Converted,JMDOrderApproval,JMDOrderAppDate,JMDOrderRejection,JMDOrderRejDate,SOOrderApproval,SOOrderAppDate,SOOrderRejection,"+
                            " SOOrderRejDate,IAOrderApproval,IAOrderAppDate,IAOrderRejection,IAOrderRejDate,IAOrderRejRemarks,OrderSourceTypeCode,"+
                            " PrevOrderNo,PrevOrderBlock,ComparisionNo,CreatedDateTime,PlanningMonth,OrderTypeCode,JMDOrderHolding,JMDOrderHoldingDate,"+
                            " POGroupNo,ReferenceNo,EPCGGroupStatus,YearCode,ProformaNo,Basic,CurrencyTypeCode,SeqId,DocId,FinalAppUserCode,JMDViewStatus,JMDFinalHoldingStatus, IAViewStatus, IAFinalHoldingStatus,PrevOrderChange) "+
                            " (Select OrderAmend_Seq.nextVal,"+
                            ""+iAmendNo+" as NoofAmend,"+
                            ""+iUserCode+" as AmendUserCode,"+
                            "'"+SSysName+"' as AmendSystemName,"+
                            "'"+SDateTime+"' as AmendDateTime,"+
                            " Id as OrderId,OrderNo,OrderBlock,OrderDate,"+
                            " Sup_Code,Reference,Advance,MrsNo,Item_Code,Qty,Rate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,"+
                            " DueDate,PayTerms,InvQty,Dept_Code,Group_Code,Unit_Code,ToCode,ThroCode,Posted,AdvSlNo,AmendmentFlag,FormCode,Class_Code,"+
                            " SlNo,PYPost,WA,DM,Freight,PF,Others,EPCG,OrderType,LicenseNumber,Project_Order,PortCode,State,Amended,UserCode,ModiDate,"+
                            " MillCode,PayDays,Status,Flag,MrsSlNo,TaxClaimable,Authentication,IntimationNo,MailStatus,MrsAuthUserCode,MailConfirmStatus,"+
                            " Converted,JMDOrderApproval,JMDOrderAppDate,JMDOrderRejection,JMDOrderRejDate,SOOrderApproval,SOOrderAppDate,SOOrderRejection,"+
                            " SOOrderRejDate,IAOrderApproval,IAOrderAppDate,IAOrderRejection,IAOrderRejDate,IAOrderRejRemarks,OrderSourceTypeCode,"+
                            " PrevOrderNo,PrevOrderBlock,ComparisionNo,CreatedDateTime,PlanningMonth,OrderTypeCode,JMDOrderHolding,JMDOrderHoldingDate, "+
                            " POGroupNo,ReferenceNo,EPCGGroupStatus,YearCode,ProformaNo,Basic,CurrencyTypeCode,SeqId,DocId,FinalAppUserCode,JMDViewStatus,JMDFinalHoldingStatus, IAViewStatus, IAFinalHoldingStatus,PrevOrderChange "+
                            " From PurchaseOrder Where OrderNo="+SOrderNo+" and MillCode="+iMillCode+")";

                    
               String QS2 = " Insert Into MatDescAmend (Id,NoofAmend,MatDescId,OrderNo,OrderBlock,OrderDate,"+
                            " Item_Code,Descr,Make,Draw,Catl,SlNo,WA,Status,Flag,MillCode,PaperColor,PaperSets,"+
                            " PaperSize,PaperSide,SlipFromNo,SlipToNo,BookFromNo,BookToNo) "+
                            " (Select MatDescAmend_Seq.nextVal,"+
                            ""+iAmendNo+" as NoofAmend,"+
                            " Id as MatDescId,OrderNo,OrderBlock,OrderDate,"+
                            " Item_Code,Descr,Make,Draw,Catl,SlNo,WA,Status,Flag,MillCode,PaperColor,PaperSets,"+
                            " PaperSize,PaperSide,SlipFromNo,SlipToNo,BookFromNo,BookToNo "+
                            " From MatDesc Where OrderNo="+SOrderNo+" and MillCode="+iMillCode+")";

               stat.executeUpdate(QS1);
               stat.executeUpdate(QS2);
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("4"+ex);
               ex.printStackTrace();
          }
     }

     public void updateOrderDetails()
     {
          try
          {
               int       iMRSSlno     = 0;
               int       iMrsUserCode = 1;
               int       iMRSType     = 0;

               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);
               Statement stat           =  theConnect.createStatement();
                                        
               Object    FinalData[][]  = MiddlePanel.MiddlePanel.getFromVector();
               String    SAdd           = MiddlePanel.MiddlePanel.TAdd.getText();
               String    SLess          = MiddlePanel.MiddlePanel.TLess.getText();
               double    dpm            = common.toDouble(SAdd)-common.toDouble(SLess);
               double    dBasic         = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double    dRatio         = dpm/dBasic;
               
               for(int i=0;i<MiddlePanel.IdData.length;i++)
               {
                    String SBlockCode = (String) MiddlePanel.MiddlePanel.getBlockCode(i);
                    String SUnitCode  = (String) MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = (String) MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = (String) MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SDueDate   = (String) MiddlePanel.MiddlePanel.getDueDate(i,TDate);
                    String STaxClaim  = (String)MiddlePanel.MiddlePanel.getTaxClaim(i);
                       int iTaxClaim  = common.toInt(STaxClaim.trim());
                    dBasic            = common.toDouble(((String)FinalData[i][11]).trim());
                    String SMisc      = common.getRound(dBasic*dRatio,3);
                    

                    String SMrsNo = ((String)FinalData[i][4]).trim();
                    iMrsUserCode  = common.toInt((String)MiddlePanel.MiddlePanel.getUserCode(i));

                    if(bflag)
                    {
                         String SMrsStatus = common.parseNull((String)MiddlePanel.MiddlePanel.VMrsStatus.elementAt(i)).trim();

                         if(SMrsStatus.equals("1"))
                         {
                              iMRSSlno     = common.toInt((String)MiddlePanel.VMrsSlNo.elementAt(i));
                              iMRSType     = common.toInt((String)MiddlePanel.VMrsType.elementAt(i));
                         }
                         else
                         {
                              if(common.toInt(SMrsNo)<=0)
                              {
                                   iMRSSlno     = 0;
                                   iMRSType     = 3;
                              }
                              else
                              {
                                   String SItemCode = (String)FinalData[i][0];
     
                                   getMrsSlNo(SItemCode,SMrsNo);

                                   iMRSSlno     = iMSlNo;
                                   iMRSType     = iMType;
                              }
                         }
                    }
                    else
                    {
                         if(common.toInt(SMrsNo)<=0)
                         {
                              iMRSSlno     = 0;
                              iMRSType     = 3;
                         }
                         else
                         {
                              String SItemCode = (String)FinalData[i][0];

                              getMrsSlNo(SItemCode,SMrsNo);

                              iMRSSlno     = iMSlNo;
                              iMRSType     = iMType;
                         }
                    }

                    String    QS1 = "Update PurchaseOrder Set ";
                              QS1 = QS1+"OrderDate  = '"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                              QS1 = QS1+"OrderBlock =0"+SBlockCode+",";
                              QS1 = QS1+"Sup_Code='"+TSupCode.getText()+"',";
                              QS1 = QS1+"Reference='"+(TRef.getText()).toUpperCase()+"',";
                              QS1 = QS1+"Advance=0"+TAdvance.getText()+",";
                              QS1 = QS1+"PayTerms='"+TPayTerm.getText()+"',";
                              QS1 = QS1+"PayDays=0"+TPayDays.getText()+",";
                              QS1 = QS1+"ToCode=0"+(String)VToCode.elementAt(JCTo.getSelectedIndex())+",";
                              QS1 = QS1+"ThroCode=0"+(String)VThroCode.elementAt(JCThro.getSelectedIndex())+",";
                              QS1 = QS1+"FormCode=0"+(String)VFormCode.elementAt(JCForm.getSelectedIndex())+",";
                              QS1 = QS1+"PortCode=0"+(String)VPortCode.elementAt(JCPort.getSelectedIndex())+",";
                              QS1 = QS1+"EPCG=0"+(JEPCG.getSelectedIndex())+",";
                              QS1 = QS1+"State=0"+(JCState.getSelectedIndex())+",";
                              QS1 = QS1+"Project_Order=0"+(JCProject.getSelectedIndex())+",";
                              QS1 = QS1+"OrderType=0"+(JCOrderType.getSelectedIndex())+",";
                              QS1 = QS1+"MrsNo=0"+((String)FinalData[i][4]).trim()+",";
                              QS1 = QS1+"Item_Code='"+(String)FinalData[i][0]+"',";
                              QS1 = QS1+"Qty=0"+((String)FinalData[i][5]).trim()+",";
                              QS1 = QS1+"Rate=0"+common.getRound(((String)FinalData[i][6]).trim(),4)+",";
                              QS1 = QS1+"DiscPer=0"+((String)FinalData[i][7]).trim()+",";
                              QS1 = QS1+"Disc=0"+((String)FinalData[i][12]).trim()+",";
                              QS1 = QS1+"CenVatPer=0"+((String)FinalData[i][8]).trim()+",";
                              QS1 = QS1+"Cenvat=0"+((String)FinalData[i][13]).trim()+",";
                              QS1 = QS1+"TaxPer=0"+((String)FinalData[i][9]).trim()+",";
                              QS1 = QS1+"Tax=0"+((String)FinalData[i][14]).trim()+",";
                              QS1 = QS1+"SurPer=0"+((String)FinalData[i][10]).trim()+",";
                              QS1 = QS1+"Sur=0"+((String)FinalData[i][15]).trim()+",";
                              QS1 = QS1+"Net=0"+((String)FinalData[i][16]).trim()+",";
                              QS1 = QS1+"Plus=0"+SAdd+",";
                              QS1 = QS1+"Less=0"+SLess+",";
                              QS1 = QS1+"Misc=0"+SMisc+",";
                              QS1 = QS1+"Unit_Code=0"+SUnitCode+",";
                              QS1 = QS1+"Dept_Code=0"+SDeptCode+",";
                              QS1 = QS1+"Group_Code=0"+SGroupCode+",";
                              QS1 = QS1+"Amended=0"+iActualAmend+",";
                              QS1 = QS1+"TAXCLAIMABLE=0"+iTaxClaim+",";
                              QS1 = QS1+"AUTHENTICATION = 1,";
                              QS1 = QS1+"MrsSlNo=0"+iMRSSlno+",";
                              QS1 = QS1+"MrsAuthUserCode=0"+iMrsUserCode+",";
                              QS1 = QS1+"OrderTypeCode=0"+iMRSType+",";

                              if(iActualAmend>0)
                              {
                                   QS1 = QS1+"SOOrderApproval = 0,";
                                   QS1 = QS1+"SOOrderRejection = 0,";
                                   QS1 = QS1+"SOOrderAppDate = '',";
                                   QS1 = QS1+"IAOrderApproval = 0,";
                                   QS1 = QS1+"IAOrderRejection = 0,";
                                   QS1 = QS1+"IAOrderAppDate = '',";
                                   QS1 = QS1+"JMDOrderApproval = 0,";
                                   QS1 = QS1+"JMDOrderRejection = 0,";
                                   QS1 = QS1+"JMDOrderAppDate = '',";
                              }

                              QS1 = QS1+"DueDate='"+SDueDate+"', ";
                              QS1 = QS1+"Basic = 0"+((String)FinalData[i][11]).trim()+", ";
                              QS1 = QS1+"ProformaNo= '"+(TProformaNo.getText()).trim()+"', ";
                              QS1 = QS1+"CurrencyTypeCode= "+((String)VCurrencyTypeCode.elementAt(JCCurrencyType.getSelectedIndex()))+" , ";

                              QS1 = QS1+"DocId = 0"+((String)FinalData[i][22]).trim()+" ";

                              QS1 = QS1+" Where ID = "+(String)MiddlePanel.IdData[i];

                      stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("4"+ex);
               ex.printStackTrace();
          }
     }

     public void updateDescDetails()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);
               Statement stat          = theConnect.createStatement();
               Object    FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
          
               for(int i=0;i<MiddlePanel.DescId.length;i++)
               {
                    String    QS1       = "";
                    String SBlockCode   = common.parseNull((String)MiddlePanel.MiddlePanel.getBlockCode(i));
                    String SDesc        = common.parseNull((String)MiddlePanel.MiddlePanel.VDesc.elementAt(i));
                    String SMake        = common.parseNull((String)MiddlePanel.MiddlePanel.VMake.elementAt(i));
                    String SDraw        = common.parseNull((String)MiddlePanel.MiddlePanel.VDraw.elementAt(i));
                    String SCatl        = common.parseNull((String)MiddlePanel.MiddlePanel.VCatl.elementAt(i));

                    String SItem_Code   = (String)FinalData[i][0];

                    if(isStationary(SItem_Code))
                    {
                         String    SPaperColor    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPColour   . elementAt(i));
                         String    SPaperSets     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSet      . elementAt(i));
                         String    SPaperSize     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSize     . elementAt(i));
                         String    SPaperSide     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSide     . elementAt(i));
                         String    SSlipFromNo    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSlipFrNo . elementAt(i));
                         String    SSlipToNo      =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSlipToNo . elementAt(i));
                         String    SBookFromNo    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPBookFrNo . elementAt(i));
                         String    SBookToNo      =    common.parseNull((String)MiddlePanel.MiddlePanel.VPBookToNo . elementAt(i));

                         QS1 = "Update MatDesc Set ";
                         QS1 = QS1+"OrderDate     = '"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                         QS1 = QS1+"OrderBlock    = 0"+SBlockCode+",";
                         QS1 = QS1+"Item_Code     = '"+(String)FinalData[i][0]+"',";
                         QS1 = QS1+"Descr         = '"+common.getNarration(SDesc)+"',";
                         QS1 = QS1+"Make          = '"+common.getNarration(SMake)+"',";
                         QS1 = QS1+"Draw          = '"+SDraw+"',";
                         QS1 = QS1+"Catl          = '"+SCatl+"',";
                         QS1 = QS1+"SLIPFROMNO    = 0"+SSlipFromNo+",";
                         QS1 = QS1+"SLIPTONO      = 0"+SSlipToNo+",";
                         QS1 = QS1+"BOOKFROMNO    = 0"+SBookFromNo+",";
                         QS1 = QS1+"BOOKTONO      = 0"+SBookToNo+" Where ID = "+(String)MiddlePanel.DescId[i];
                    }
                    else
                    {
                         QS1 = "Update MatDesc Set ";
                         QS1 = QS1+"OrderDate  = '"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                         QS1 = QS1+"OrderBlock =0"+SBlockCode+",";
                         QS1 = QS1+"Item_Code='"+(String)FinalData[i][0]+"',";
                         QS1 = QS1+"Descr    ='"+common.getNarration(SDesc)+"',";
                         QS1 = QS1+"Make     ='"+common.getNarration(SMake)+"',";
                         QS1 = QS1+"Draw     ='"+SDraw+"',";
                         QS1 = QS1+"Catl     ='"+SCatl+"' Where ID = "+(String)MiddlePanel.DescId[i];
                    }
                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("5"+ex);
          }
     }

     public void updateMrsDetails()
     {
          try
          {
               int       iMRSSlno  = 0;

               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);
               Statement stat           =  theConnect.createStatement();
                                        
               Object    FinalData[][]  = MiddlePanel.MiddlePanel.getFromVector();
               
               for(int i=0;i<FinalData.length;i++)
               {
                    String SMrsNo = ((String)FinalData[i][4]).trim();

                    String SMrsStatus = common.parseNull((String)MiddlePanel.MiddlePanel.VMrsStatus.elementAt(i)).trim();

                    if(SMrsStatus.equals("1"))
                    {
                         continue;
                    }

                    if(common.toInt(SMrsNo)<=0)
                    {
                         continue;
                    }

                    String SBlockCode = (String) MiddlePanel.MiddlePanel.getBlockCode(i);

                    String SItemCode = (String)FinalData[i][0];

                    getMrsSlNo(SItemCode,SMrsNo);

                    iMRSSlno     = iMSlNo;

                    String QS1 = "Update MRS set ";
                    QS1 = QS1+"OrderNo   ="+TOrderNo.getText()+",";
                    QS1 = QS1+"OrderBlock="+SBlockCode;
                    QS1 = QS1+" Where MrsNo="+SMrsNo;
                    QS1 = QS1+" and SlNo="+iMRSSlno;
                    QS1 = QS1+" and MillCode="+iMillCode;
                    QS1 = QS1+" and Item_Code='"+SItemCode+"'";
                    
                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("5"+ex);
               ex.printStackTrace();
          }
     }

     public void updateOldMrsDetails()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);
               Statement stat           =  theConnect.createStatement();
                                        
               Object    FinalData[][]  = MiddlePanel.MiddlePanel.getFromVector();
               
               for(int i=0;i<MiddlePanel.IdData.length;i++)
               {

                    String SMrsStatus = common.parseNull((String)MiddlePanel.MiddlePanel.VMrsStatus.elementAt(i)).trim();

                    if(SMrsStatus.equals("1"))
                         continue;


                    String SOldMrsNo = (String)MiddlePanel.VOldMrsNo.elementAt(i);

                    if(common.toInt(SOldMrsNo)<=0)
                         continue;


                    String SOldCode    = (String)MiddlePanel.VOldCode.elementAt(i);
                    String SOldMrsSlNo = (String)MiddlePanel.VMrsSlNo.elementAt(i);

                    int iCount=0;

                    String QS = " Select Count(*) from Mrs Where OrderNo="+TOrderNo.getText()+" and MillCode="+iMillCode+" and MrsNo="+SOldMrsNo+" and Item_Code='"+SOldCode+"' and SlNo="+SOldMrsSlNo;

                    ResultSet result    = stat         . executeQuery(QS);
                              result    . next();
                              iCount    = common.toInt((String)result.getString(1));
                              result    . close();

                    String QS1="";

                    if(iCount>0)
                    {
                         QS1 = "Update MRS set OrderNo=0,OrderBlock=0 ";
                         QS1 = QS1+" Where OrderNo="+TOrderNo.getText();
                         QS1 = QS1+" and MrsNo="+SOldMrsNo;
                         QS1 = QS1+" and SlNo="+SOldMrsSlNo;
                         QS1 = QS1+" and MillCode="+iMillCode;
                         QS1 = QS1+" and Item_Code='"+SOldCode+"'";
                    }
                    else
                    {
                         QS1 = "Update MRS set OrderNo=0,OrderBlock=0 ";
                         QS1 = QS1+" Where OrderNo="+TOrderNo.getText();
                         QS1 = QS1+" and MrsNo="+SOldMrsNo;
                         QS1 = QS1+" and MillCode="+iMillCode;
                         QS1 = QS1+" and Item_Code='"+SOldCode+"'";
                    }

                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("6"+ex);
               ex.printStackTrace();
          }
     }

     public void updateEnquiryDetails()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);
               Statement stat           =  theConnect.createStatement();
                                        
               Object    FinalData[][]  = MiddlePanel.MiddlePanel.getFromVector();
               
               for(int i=0;i<FinalData.length;i++)
               {
                    String SMrsNo = ((String)FinalData[i][4]).trim();

                    String SMrsStatus = common.parseNull((String)MiddlePanel.MiddlePanel.VMrsStatus.elementAt(i)).trim();

                    if(SMrsStatus.equals("1"))
                    {
                         continue;
                    }

                    if(common.toInt(SMrsNo)<=0)
                    {
                         continue;
                    }

                    String SItemCode = (String)FinalData[i][0];

                    String QS = " Select EnquiryNo from Mrs Where MrsNo="+SMrsNo+" and Item_Code='"+SItemCode+"'";
 
                    ResultSet res = stat.executeQuery(QS);
                    while(res.next())
                    {
                         int iEnqNo = common.toInt(res.getString(1));
 
                         if(iEnqNo>0)
                         {
                              String QS1 = "Update Enquiry set CloseStatus=1 ";
                              QS1 = QS1+" Where EnqNo="+iEnqNo;
                              QS1 = QS1+" and Mrs_No="+SMrsNo;
                              QS1 = QS1+" and Item_Code='"+SItemCode+"'";
                              stat.executeUpdate(QS1);
                         }
                    }
                    res.close();
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("Enq"+ex);
               ex.printStackTrace();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop   . remove(this);
               DeskTop   . repaint();
               DeskTop   . updateUI();
          }
          catch(Exception ex) { }
     }

     public void fillData(String SOrderNo,int iOrderStatus,Vector VItemsStatus,String SItemTable,String SSupTable)
     {
          MiddlePanel . getUpdateOrderNo(SOrderNo);

          try
          {
               if(iOrderStatus==2 || iOrderStatus==1)
               {
                    setEnables();
               }

               new OrderResultSet(SOrderNo,TOrderNo,TDate,BSupplier,TSupCode,TRef,TAdvance,TPayTerm,TPayDays,JCTo,JCThro,JCForm,MiddlePanel,VCode,VName,VToCode,VThroCode,VFormCode,JEPCG,JCOrderType,VEpcg,VOrderType,VPort,JCPort,VProject,JCProject,VState,JCState,iOrderStatus,VItemsStatus,iMillCode,SItemTable,SSupTable,TReferenceNo,TProformaNo,JCCurrencyType);

               setEPCGFields();

	       BOk.setEnabled(false);
          }
          catch(Exception ex)
          {
               System.out.println("6"+ex);
               ex.printStackTrace();
          }
     }

     public void setEnables()
     {
          TDate. TDay    . setEnabled(false);
          TDate. TMonth  . setEnabled(false);
          TDate. TYear   . setEnabled(false);
          TRef           . setEnabled(false);
          BSupplier      . setEnabled(false);
          JEPCG          . setEnabled(false);
          JCOrderType    . setEnabled(false);
          JCProject      . setEnabled(false);
          JCTo           . setEnabled(false);
          JCThro         . setEnabled(false);
          JCForm         . setEnabled(false);
          TAdvance       . setEnabled(false);
          TPayTerm       . setEnabled(false);
          TPayDays       . setEnabled(false);
          JCPort         . setEnabled(false);
          JCState        . setEnabled(false);
     }

     public boolean isFreshRecord(int i)
     {
          try
          {
               if(MiddlePanel.IdData==null)
                    return true;
               
               boolean bfresh = (i<MiddlePanel.IdData.length?false:true);
               return bfresh;
          }
          catch(Exception ex)
          {
               return true;
          }
     }

     public void getVTo()
     {
          ResultSet  result  = null;
          
          VTo            = new Vector();
          VToCode        = new Vector();
          VThro          = new Vector();
          VThroCode      = new Vector();
          VFormCode      = new Vector();
          VForm          = new Vector();
          VEpcg          = new Vector();
          VOrderType     = new Vector();
          VPort          = new Vector();
          VPortCode      = new Vector();
          VBlockName     = new Vector();
          VBlockCode     = new Vector();
          
          VEpcg          . addElement("Non-EPCG Order   ");
          VEpcg          . addElement("EPCG Order   ");
          
          VOrderType     . addElement("Local Order      ");
          VOrderType     . addElement("Import       ");

          VCurrencyTypeCode        = new Vector();
          VCurrencyTypeName        = new Vector();

          try
          {
               Statement stat      = theConnect.createStatement();
                         result    = stat.executeQuery("Select ToName,ToCode From BookTo Order By 1");

               while(result.next())
               {
                    VTo      . addElement(result.getString(1));
                    VToCode  . addElement(result.getString(2));
               }
               result.close();
               
               result    = stat.executeQuery("Select ThroName,ThroCode From BookThro Order By 1");
               while(result.next())
               {
                    VThro    . addElement(result.getString(1));
                    VThroCode. addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery("Select FormNo,FormCode From STForm Order By 1");
               while(result.next())
               {
                    VForm    . addElement(result.getString(1));
                    VFormCode. addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery("Select PortCode,PortName From Port Order By 1");
               while(result.next())
               {
                    String ax = result.getString(2);
                    VPort     . addElement(ax);
                    VPortCode . addElement(result.getString(1));
               }
               result.close();

               result = stat.executeQuery("Select block,blockname From OrdBlock where showstatus=0 Order By 1");
               while(result.next())
               {
                    String ax      = result.getString(2);
                    VBlockName     . addElement(ax);
                    VBlockCode     . addElement(result.getString(1));
               }
               result.close();

               ResultSet rs        = stat.executeQuery("Select CurrencyTypeCode, CurrencyTypeName from EPCGCurrencyType Order by 2");
               while(rs.next())
               {
                    VCurrencyTypeCode   . addElement(rs.getString(1));
                    VCurrencyTypeName   . addElement(rs.getString(2));
               }
               rs                       . close();

               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("7"+ex);
          }
     }

     public boolean isValidQty()
     {
          boolean bFlag = true;

          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();

          for(int i=0;i<MiddlePanel.DescId.length;i++)
          {
               double dQty    = common.toDouble(((String)FinalData[i][5]).trim());
               double dInvQty = 0;
               String QS      = "Select invqty from purchaseorder Where ID = "+(String)MiddlePanel.IdData[i];

               try
               {
                    Statement      stat           =  theConnect.createStatement();
                    ResultSet      result         = stat  . executeQuery(QS);

                                   result         . next();
                                   dInvQty        = common.toDouble((String)result.getString(1));
                                   result         . close();
                                   stat           . close();

               }catch(Exception ex)
               {
                    System.out.println(ex);
               }
               /*if(dQty<dInvQty)
               {
                    String SInf    = "Please enter the correct qty Value in Row_No"+(i+1);
                    JOptionPane    . showMessageDialog(null,SInf,"Information",JOptionPane.INFORMATION_MESSAGE);
                    return false;
               }*/
          }
          return bFlag;
     }

     public boolean isValidNew()
     {
          boolean   bFlag     = true;
          String    SDate     = TDate.toString();

          String    SPDate    = TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText();
                    SPDate    = SPDate.trim();

          if((SPDate.trim()).length()!=8)
          {
               write("Please enter the OrderDate Date Correctly \n\n");

               return false;
          } 

          try
          {
               FW = new FileWriter(common.getPrintPath()+"Error.prn");
               write("*-----------------------*\n");
               write("*    W A R N I N G      *\n");
               write("*-----------------------*\n\n");
          }
          catch(Exception ex){}
          
          // Checking for Supplier
          String str = (TSupCode.getText()).trim();
          if(str.length()==0)
          {
               write("Supplier Is Not Selected \n\n");
               bFlag = false;
          }
          
          //  Checking for Table Items

          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();

          for(int i=0;i<FinalData.length;i++)
          {
               String SItemCode    = ((String)FinalData[i][0]).trim();

               String SMrsNo       = ((String)FinalData[i][4]).trim();
               
               String SBlockName   = ((String)FinalData[i][3]).trim();
               double dQty         = common.toDouble(((String)FinalData[i][5]).trim());
               double dRate        = common.toDouble(((String)FinalData[i][6]));
               String SDept        = ((String)FinalData[i][17]).trim();
               String SGroup       = ((String)FinalData[i][18]).trim();
               String SDueDate     = ((String)FinalData[i][19]).trim();
               String SUnit        = ((String)FinalData[i][20]).trim();
               String SUser        = ((String)FinalData[i][21]).trim();
               
               if(SItemCode.length() == 0)
               {
                    write("Select Item Name for "+(i+1)+" row  \n");
                    bFlag = false;
               }

               if(SBlockName.length() == 0)
               {
                    write("BlockName for Item No "+(i+1)+" is not fed \n");
                    bFlag = false;
               }

               if(dQty <= 0)
               {
                    write("Quantity for Item No "+(i+1)+" is not fed \n");
                    bFlag = false;
               }
               if(dRate <= 0)
               {
                    write("Rate for Item No "+(i+1)+" is not fed \n");
                    bFlag = false;
               }
               if(SDept.length() == 0)
               {
                    write("Department for Item No "+(i+1)+" is not Selected \n");
                    bFlag = false;
               }
               if(SGroup.length() == 0)
               {
                    write("Group for Item No "+(i+1)+" is not Selected \n");
                    bFlag = false;
               }
               if(SUnit.length() == 0)
               {
                    write("Unit for Item No "+(i+1)+" is not Selected \n");
                    bFlag = false;
               }
               if(SUser.length() == 0)
               {
                    write("User for Item No "+(i+1)+" is not Selected \n");
                    bFlag = false;
               }
               if(SDueDate.length() != 10 || common.toInt(common.getDateDiff(SDueDate,SDate)) < 0)
               {
                    write("DueDate for Item No "+(i+1)+" is wrongly Entered \n");
                    bFlag = false;
               }

               String SMrsStatus = common.parseNull((String)MiddlePanel.MiddlePanel.VMrsStatus.elementAt(i)).trim();

               if(SMrsStatus.equals("0"))
               {
                    if(common.toInt(SMrsNo)>0)
                    {
                         int iCount = checkMrsNo(SItemCode,SMrsNo);
                         if(iCount==0)
                         {
                              write("Mrs No Mismatch for Item No "+(i+1)+" \n");
                              bFlag = false;
                         }
                         else
                         {
                              String SUserCode  = (String)MiddlePanel.MiddlePanel.getUserCode(i);

                              getMrsUser(SItemCode,SMrsNo);

                              int iIndex = common.indexOf(VMUserCode,SUserCode);
               
                              if(iIndex<0)
                              {
                                   write("User Name Mismatch for Item No "+(i+1)+" Required User = "+VMUser+" \n");
                                   bFlag = false;
                              }
                         }
                    }
               }
          }

          if(JEPCG.getSelectedIndex() == 1)
          {
               if(common.parseNull(TProformaNo.getText()).trim().length() == 0)
               {
                    write("Proforma No. should be filled for EPCG Orders \n");
                    bFlag = false;
               }

               String SOrderType        = ((String)JCOrderType.getSelectedItem()).trim();
               String SCurrencyType     = ((String)JCCurrencyType.getSelectedItem()).trim();

               if(SOrderType.equalsIgnoreCase("Import") && SCurrencyType.equalsIgnoreCase("RUPEES"))
               {
                    write("You cannot Select Rupees Currency Type for Import Orders.\n");
                    bFlag = false;
               }
          }


          try
          {
               write("*-----------------------*\n");
               FW.close();
          }
          catch(Exception ex){}
          
          return bFlag;
     }

     public void write(String str)
     {
          try
          {
               FW.write(str);
          }
          catch(Exception ex){}
     }

     public int checkMrsNo(String SItemCode,String SMrsNo)
     {
          int iCount=0;

          String QS      = "";

          QS = " Select Count(*) from Mrs Where OrderNo=0 and MillCode="+iMillCode+" and MrsNo="+SMrsNo+" and Item_Code='"+SItemCode+"'";

          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         iCount    = result.getInt(1);
                         result    . close();
                         stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
          return iCount;
     }

     public void getMrsUser(String SItemCode,String SMrsNo)
     {
          VMUser     = new Vector();
          VMUserCode = new Vector();

          String QS = " Select Mrs.MrsAuthUserCode,RawUser.UserName "+
                      " from Mrs Inner Join RawUser on Mrs.MrsAuthUserCode=RawUser.UserCode "+
                      " Where Mrs.MillCode="+iMillCode+" and Mrs.Item_Code='"+SItemCode+"'"+
                      " and Mrs.MrsNo="+SMrsNo+" Order by 1 ";

          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS);
               while(result.next())
               {
                    VMUserCode.addElement(common.parseNull((String)result.getString(1)));
                    VMUser    .addElement(common.parseNull((String)result.getString(2)));
               }
               result    . close();
               stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
     }

     public void getMrsSlNo(String SItemCode,String SMrsNo)
     {
          iMSlNo=0;
          iMUser=1;
          iMType=0;

          String QS      = "";

          QS = " Select SlNo,MrsAuthUserCode,decode(greatest(nvl(planmonth,0)),0,(decode(greatest(nvl(yearlyplanningstatus,0)),0,0,2)),1) as mrstype "+
               " from Mrs Where OrderNo=0 and MillCode="+iMillCode+" and MrsNo="+SMrsNo+" and Item_Code='"+SItemCode+"'"+
               " and SlNo=(Select Min(SlNo) from Mrs Where OrderNo=0 and MillCode="+iMillCode+" and MrsNo="+SMrsNo+" and Item_Code='"+SItemCode+"')";

          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         {
                              iMSlNo = result.getInt(1);
                              iMUser = result.getInt(2);
                              iMType = result.getInt(3);
                         }
                         result    . close();
                         stat      . close();

               if(iMUser<=0)
                    iMUser=1;

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
     }

     public int getMaxSlNo(String SOrderNo)
     {
          int iMaxNo=0;

          String QS = " Select Max(SlNo) from PurchaseOrder Where OrderNo="+SOrderNo+" and MillCode="+iMillCode;

          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         iMaxNo    = common.toInt((String)result.getString(1));
                         result    . close();
                         stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
           
          return iMaxNo;
     }

     public int getAmendNo(String SOrderNo)
     {
          int iAmendNo=0;

          String QS = " Select Max(NoofAmend) from OrderAmend Where OrderNo="+SOrderNo+" and MillCode="+iMillCode;

          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         iAmendNo  = common.toInt((String)result.getString(1))+1;
                         result    . close();
                         stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
           
          return iAmendNo;
     }

     public int getActualAmend(String SOrderNo)
     {
          int iAmendNo=0;

          int iCount=0;

          String QS1 = " Select Count(*) from PurchaseOrder Where (IAOrderApproval=1 or JMDOrderApproval=1) and OrderNo="+SOrderNo+" and MillCode="+iMillCode;

          String QS2 = " Select Count(*) from PurchaseOrder Where Amended=1 and (IAOrderApproval=0 and JMDOrderApproval=0) and OrderNo="+SOrderNo+" and MillCode="+iMillCode;

          String QS3 = " Select Count(*) from OrderAmend Where (IAOrderApproval=1 or JMDOrderApproval=1) and OrderNo="+SOrderNo+" and MillCode="+iMillCode;


          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS1);
                         result    . next();
                         iCount    = result.getInt(1);
                         result    . close();

               if(iCount>0)
               {
                    iAmendNo = checkQtyAmend(SOrderNo,false);
                    //iAmendNo=1;
               }
               else
               {
                    result    = stat         . executeQuery(QS2);
                    result    . next();
                    iCount    = result.getInt(1);
                    result    . close();

                    if(iCount>0)
                    {
                         iAmendNo = checkQtyAmend(SOrderNo,false);
                         //iAmendNo=1;
                    }
                    else
                    {
                         result    = stat         . executeQuery(QS3);
                         result    . next();
                         iCount    = result.getInt(1);
                         result    . close();
     
                         if(iCount>0)
                         {
                              iAmendNo = checkQtyAmend(SOrderNo,true);
                              //iAmendNo=1;
                         }
                    }
               }

               stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
           
          return iAmendNo;
     }

     public int checkQtyAmend(String SOrderNo,boolean bAmendFlag)
     {
          int iAmendNo=0;
          int iOtherAmend=0;
          int iQtyAmend=0;

          try
          {
               Statement stat      = theConnect   . createStatement();


               Object FinalData[][]  = MiddlePanel.MiddlePanel.getFromVector();

               double dNewAdd           = common.toDouble(MiddlePanel.MiddlePanel.TAdd.getText());
               double dNewLess          = common.toDouble(MiddlePanel.MiddlePanel.TLess.getText());

               for(int i=0;i<MiddlePanel.IdData.length;i++)
               {
                    String SOldItemCode  = "";
                    double dOldQty       = 0;
                    double dOldRate      = 0;
                    double dOldDiscPer   = 0;
                    double dOldCenvatPer = 0;
                    double dOldTaxPer    = 0;
                    double dOldSurPer    = 0;
				double dOldAdd	      = 0;
				double dOldLess	 = 0;

                    String SNewItemCode  = (String)FinalData[i][0];
                    double dNewQty       = common.toDouble(((String)FinalData[i][5]).trim());
                    double dNewRate      = common.toDouble(((String)FinalData[i][6]).trim());
                    double dNewDiscPer   = common.toDouble(((String)FinalData[i][7]).trim());
                    double dNewCenvatPer = common.toDouble(((String)FinalData[i][8]).trim());
                    double dNewTaxPer    = common.toDouble(((String)FinalData[i][9]).trim());
                    double dNewSurPer    = common.toDouble(((String)FinalData[i][10]).trim());

                    String SId           = (String)MiddlePanel.IdData[i];

                    String QS1 = "";
                    String QS2 = "";
                    String QS3 = "";

                    if(bAmendFlag)
                    {
                         QS1 = " Select Item_Code,Rate,DiscPer,CenvatPer,TaxPer,SurPer,Plus,Less from OrderAmend "+
                               " Where Id=(Select Min(Id) from OrderAmend Where OrderId="+SId+
                               " And (IAOrderApproval=1 or JMDOrderApproval=1)) ";
                    }
                    else
                    {
                         QS1 = " Select Item_Code,Rate,DiscPer,CenvatPer,TaxPer,SurPer,Plus,Less from PurchaseOrder "+
                               " Where Id="+SId;
                    }

                    QS2 = " Select Qty from OrderAmend "+
                          " Where Id=(Select Min(Id) from OrderAmend Where OrderId="+SId+
                          " And (IAOrderApproval=1 or JMDOrderApproval=1)) ";


                    QS3 = " Select Qty from PurchaseOrder Where Id="+SId;

                    ResultSet result = stat.executeQuery(QS1);
                    while(result.next())
                    {
                         SOldItemCode  = common.parseNull((String)result.getString(1));
                         dOldRate      = common.toDouble(common.parseNull((String)result.getString(2)));
                         dOldDiscPer   = common.toDouble(common.parseNull((String)result.getString(3)));
                         dOldCenvatPer = common.toDouble(common.parseNull((String)result.getString(4)));
                         dOldTaxPer    = common.toDouble(common.parseNull((String)result.getString(5)));
                         dOldSurPer    = common.toDouble(common.parseNull((String)result.getString(6)));
					dOldAdd       = common.toDouble(common.parseNull((String)result.getString(7)));
					dOldLess      = common.toDouble(common.parseNull((String)result.getString(8)));
                    }
                    result.close();

                    if((SNewItemCode.equals(SOldItemCode)) && (dNewRate==dOldRate) && (dNewDiscPer==dOldDiscPer) && (dNewCenvatPer==dOldCenvatPer) && (dNewTaxPer==dOldTaxPer) && (dNewSurPer==dOldSurPer) && (dNewAdd==dOldAdd) && (dNewLess==dOldLess))
                    {
                         iOtherAmend = 0;
                    }
                    else
                    {
                         iOtherAmend = 1;
                         iAmendNo    = 1;
                         break;
                    }

                    result = stat.executeQuery(QS2);
                    while(result.next())
                    {
                         dOldQty       = common.toDouble(common.parseNull((String)result.getString(1)));
                    }
                    result.close();

				if(dOldQty<=0)
				{
	                    result = stat.executeQuery(QS3);
	                    while(result.next())
	                    {
	                         dOldQty       = common.toDouble(common.parseNull((String)result.getString(1)));
	                    }
	                    result.close();
				}


                    if(dNewQty>dOldQty)
                    {
                         iQtyAmend = 1;
                         iAmendNo  = 1;
                         break;
                    }
                    else
                    {
                         if(dNewQty==dOldQty)
                         {
                              iQtyAmend = 0;
                         }
                         else
                         {
                              double dAllowQty = dOldQty - (dOldQty * 10 / 100);
     
                              if(dNewQty<dAllowQty)
                              {
                                   iQtyAmend = 1;
                                   iAmendNo  = 1;
                                   break;
                              }
                         }
                    }
               }

               stat.close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
           
          return iAmendNo;
     }


     public String getNextOrderNo()
     {
          // During Modification Order No Need Not to be changed
          if(bflag)
               return TOrderNo.getText();
          
          String SOrderNo= "";
          String QS      = "";
          String QS1     = "";

          QS = " Select maxno From Config"+iMillCode+""+SYearCode+" where Id=1";

          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         SOrderNo  = common.parseNull((String)result.getString(1));
                         result    . close();
                         stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
          SOrderNo  = String.valueOf(common.toInt(SOrderNo.trim())+1);
          TOrderNo  . setText(SOrderNo.trim());
           
          return TOrderNo.getText();
     }

     public String getInsertOrderNo()
     {
          // During Modification Order No Need Not to be changed
          if(bflag)
               return TOrderNo.getText();
          
          String SOrderNo= "";
          String QS      = "";
          String QS1     = "";

          QS = " Select (maxno+1) From Config"+iMillCode+""+SYearCode+" where Id=1 for update of MaxNo noWait";

          try
          {
               if(theConnect  . getAutoCommit())
                    theConnect  . setAutoCommit(false);

               Statement stat      = theConnect   . createStatement();

               PreparedStatement thePrepare = theConnect.prepareStatement(" Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 1");

               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         SOrderNo  = common.parseNull((String)result.getString(1));
                         result    . close();

               thePrepare.setInt(1,common.toInt(SOrderNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getInsertOrderNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
          TOrderNo  . setText(SOrderNo.trim());
           
          return TOrderNo.getText();
     }

     public boolean isStationary(String SItemCode)
     {
          String SStkGroupCode     = "";
          String QS                = "";

          if(iMillCode==0)
          {
               QS =    " select stkgroupcode,invitems.item_code from invitems"+
                       " where invitems.item_code = '"+SItemCode+"'";
          }
          else
          {
               QS =    " select stkgroupcode,"+SItemTable+".item_code from "+SItemTable+""+
                       " where "+SItemTable+".item_code = '"+SItemCode+"'";
          }

          try
          {
               Statement      stat          = theConnect.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result.next())
               {
                    SStkGroupCode  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }
          if(SStkGroupCode.equals("B01"))
               return true;

          return false;
     }

     public void UpdateInvItems()
     {
          String QS           = "";
          Statement stat      = null;

          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
          try
          {
               for(int i=0;i<MiddlePanel.MiddlePanel.VPSlipFrNo.size();i++)
               {
                    QS = "";
                    String SItem_Code    = ((String)FinalData[i][0]).trim();
                    if(isStationary(SItem_Code))
                    {
                         int SSlipNo = common.toInt(common.parseNull((String)MiddlePanel.MiddlePanel.VPSlipToNo.elementAt(i)));
                         int SBookNo = common.toInt(common.parseNull((String)MiddlePanel.MiddlePanel.VPBookToNo.elementAt(i)));
               
                         if(iMillCode==0)
                         {
                              QS = " Update Invitems        set LASTSLIPNO ="+SSlipNo+",LASTBOOKNO ="+SBookNo+
                                   " where invitems.item_code = '"+SItem_Code+"'";
                         }
                         else
                         {
                              QS = " Update "+SItemTable+"  set LASTSLIPNO ="+SSlipNo+",LASTBOOKNO ="+SBookNo+
                                   " where "+SItemTable+".item_code = '"+SItem_Code+"'";
                         }
     
                         if(theConnect.getAutoCommit())
                                   theConnect     . setAutoCommit(false);
     
                         stat =    theConnect. createStatement();
                                   stat      . executeUpdate(QS);
                    }
               }
               if(stat!=null)
                    stat.close();
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public String getNextReferenceNo()
     {
          // During Modification Reference No Need Not to be changed
          if(bflag)
               return TReferenceNo.getText();

          String SReferenceNo      = "";
          String QS                = "";
          String QS1               = "";

          QS = "  Select (MaxNo+1) from EPCGConfig where MillCode = "+iMillCode+" and YearCode = "+SYearCode+" and AliasName = 'REFNO' for update of MaxNo noWait ";

          try
          {
               // Get No..

               if(theConnect  . getAutoCommit())
                    theConnect  . setAutoCommit(false);

               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                    SReferenceNo   = common.parseNull((String)result.getString(1));
                         result    . close();
                         stat      . close();

               // Set No..

               PreparedStatement thePrepare = theConnect.prepareStatement(" Update EPCGConfig set MaxNo = ?  where MillCode = "+iMillCode+" and YearCode = "+SYearCode+" and AliasName = 'REFNO'");
               thePrepare          . setInt(1,common.toInt(SOrderNo));
               thePrepare          . executeUpdate();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getNextReferenceNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }

          TReferenceNo             . setText(SReferenceNo);
           
          return TReferenceNo.getText();
     }

     private void setEPCGFields()
     {
          if (JEPCG.getSelectedIndex() == 0)
          {
               JCOrderType    . setEnabled(false);
               JCState        . setEnabled(false);
               JCPort         . setEnabled(false);

               TProformaNo    . setEnabled(false);
               JCCurrencyType . setEnabled(false);
          }
          else
          {
               JCOrderType    . setEnabled(true);
               JCState        . setEnabled(true);
               JCPort         . setEnabled(true);

               TProformaNo    . setEnabled(true);
               JCCurrencyType . setEnabled(true);
          }
     }
}
