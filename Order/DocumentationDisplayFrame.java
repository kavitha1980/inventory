package Order;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.JFileChooser;
import javax.print.*;

import guiutil.*;
import util.*;

public class DocumentationDisplayFrame extends JInternalFrame
{
     JLayeredPane        Layer;
     JPanel              TopPanel,MiddlePanel;
     MyComboBox          JCParty,JCSize,JCDept,JCCategory;
     JTextArea           TA;
     JButton             BSave,BExit,BApply,BFile1,BFile2,BFile3;
     MyTextField         TDocName,TAttribute1,TAttribute2,TAttribute3,TNarr;


     Connection          theConnection;
     Statement           theStatement;
     Common              common;

     Vector              VPartyName,VPartyCode,VSize,VParty,VDeptName,VDeptCode,VCName,VCCode;

     PaintPanel          p =null;

     JScrollPane         verticalPane;

     String              SImg1="",SImg2="",SImg3="";
     int                 iUserCode;

     String sId="";	

     JTable         ReportTable;
     InvTableModel  dataModel;
     int iRow;

     DocumentationDisplayFrame(JLayeredPane Layer,JTable ReportTable,InvTableModel dataModel,int iRow,String sId)
     {
          this . Layer        	= Layer;
          this . ReportTable 	= ReportTable;
          this . dataModel   	= dataModel;
          this . iRow		= iRow;
	  this . sId		= sId;

          common              = new Common();
          setPartyVector();
          setDeptVector();
          setCategory();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
	  setImage();
     }


     DocumentationDisplayFrame(JLayeredPane Layer,int iUserCode)
     {
          this . Layer        = Layer;
          this . iUserCode    = iUserCode;

          common              = new Common();
          setPartyVector();
          setDeptVector();
          setCategory();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();

     }
     private void createComponents()
     {
          TopPanel            = new JPanel();
          MiddlePanel         = new JPanel();

          JCParty             = new MyComboBox(VPartyName);
          JCParty             . addItem("NONE");

          JCDept              = new MyComboBox(VDeptName);
          JCDept              . addItem("NONE");

          JCCategory          = new MyComboBox(VCName);
          JCCategory          . addItem("NONE");

          JCDept              . setSelectedItem("NONE");
          JCCategory          . setSelectedItem("NONE");

          VSize               = new Vector();
          VSize               . add("25%");
          VSize               . add("50%");
          VSize               . add("75%");
          VSize               . add("100%");


          TDocName            = new MyTextField();

          TAttribute1         = new MyTextField();
          TAttribute2         = new MyTextField();
          TAttribute3         = new MyTextField();
          TNarr               = new MyTextField();
          JCParty             . setSelectedItem("NONE");
          JCSize              = new MyComboBox(VSize);

          BSave               = new JButton("Save");
          BFile1              = new JButton("File1");
          BFile2              = new JButton("File2");
          BFile3              = new JButton("File3");

          BExit               = new JButton("Exit");
          BApply              = new JButton("Apply");
     }

     private void setLayouts()
     {
          TopPanel            . setLayout(new GridLayout(7,6));
          MiddlePanel         . setLayout(new BorderLayout());;


          TopPanel            . setBorder(new TitledBorder(""));
          TopPanel            . setBackground(new Color(213,234,255));
          MiddlePanel.setBorder(new TitledBorder(""));

          setTitle("Documentation Details");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,900,245);
          show();
     }

     private void addComponents()
     {

          verticalPane        = new JScrollPane(MiddlePanel,
                                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                                    JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);


          TopPanel            . add(new JLabel(" Party Name"));
          TopPanel            . add(JCParty);
          TopPanel            . add(new JLabel(" Size"));
          TopPanel            . add(JCSize);
          TopPanel            . add(new JLabel(" Doc Name"));
          TopPanel            . add(TDocName);


          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));


          TopPanel            . add(new JLabel(" Attribute1"));
          TopPanel            . add(TAttribute1);
          TopPanel            . add(new JLabel(" Attribute2"));
          TopPanel            . add(TAttribute2);
          TopPanel            . add(new JLabel(" Attribute3"));
          TopPanel            . add(TAttribute3);


          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));


          TopPanel            . add(new JLabel(" Doc Details"));
          TopPanel            . add(TNarr);
          TopPanel            . add(new JLabel("Department"));
          TopPanel            . add(JCDept);
          TopPanel            . add(new JLabel("Category"));
          TopPanel            . add(JCCategory);


          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));

          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel(""));
//          TopPanel            . add(BApply);
          TopPanel            . add(BFile1);
          TopPanel            . add(BFile2);
          TopPanel            . add(BFile3);
//          TopPanel            . add(BExit);
          TopPanel            . add(new JLabel(""));
//          TopPanel            . add(new JLabel(""));

          getContentPane(). add(TopPanel,BorderLayout.CENTER);
//          getContentPane(). add(verticalPane,BorderLayout.CENTER);
//          getContentPane(). add(new JScrollPane(MiddlePanel));
     }

     public void addListeners()
     {
//          BApply     . addActionListener(new ApplyList1(this));
          BExit      . addActionListener(new ActList());
          BFile1     . addActionListener(new ActList()); 
          BFile2     . addActionListener(new ActList()); 
          BFile3     . addActionListener(new ActList()); 
     }

     public void setPartyVector()
     {
          VPartyName                    = new Vector();
          VPartyCode                    = new Vector();

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","scm","rawmat");
               Statement stat   = theConnection.createStatement();
               ResultSet rs     = stat.executeQuery("  Select PartyNAme,PartyCode From PartyMAster Order by 1");
               while(rs.next())
               {
                    VPartyName          . addElement(common.parseNull(rs.getString(1)));
                    VPartyCode          . addElement(common.parseNull(rs.getString(2)));
               }
               rs                       . close();
	       stat			. close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public class ApplyList1 implements ActionListener
     {
          DocumentationDisplayFrame    theFrame;

          public ApplyList1(DocumentationDisplayFrame theFrame)
          {
               this.theFrame = theFrame;
          }

          public void actionPerformed(ActionEvent ae)
          {
               try
               {

                    BFile1     . setText("File1"); 
                    BFile2     . setText("File2"); 
                    BFile3     . setText("File3"); 

                    String SPartyName   =    common.parseNull((String)JCParty.getSelectedItem());
                    String SPartyCode   =    getPartyCode(SPartyName);

                    if(SPartyCode.equals("NONE"))
                    {
                         SPartyCode     ="";
                    }
                    String SDocName     =    TDocName.getText();
                    String SAtt1        =    TAttribute1.getText();
                    String SAtt2        =    TAttribute2.getText();
                    String SAtt3        =    TAttribute3.getText();
                    String SNarr        =    TNarr.getText();

                    String SDeptName    =    common.parseNull((String)JCDept.getSelectedItem());


                    if(SDeptName.equals("NONE"))
                    {
                         SDeptName = "";
                    }

                    String SDeptCode    =    getDeptCode(SDeptName);



                    String SCategory    =    common.parseNull((String)JCCategory.getSelectedItem());
                    if(SCategory.equals("NONE"))
                    {
                         SCategory = "";
                    }
                    String SCateCode    =    getCateCode(SCategory);

                    DocumentSelection DS = new DocumentSelection(Layer,theFrame,SPartyCode,SDocName,SAtt1,SAtt2,SAtt3,SNarr,SPartyName,SDeptCode,SCateCode,SDeptName,SCategory,iUserCode);
                    Layer.add(DS);
                    Layer.repaint();
                    DS.setSelected(true);
                    Layer.updateUI();
               }
               catch(Exception e)
               {
                    System.out.println(e);
                    e.printStackTrace();
               }
           }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource() == BExit)
               {
                    removeFrame();
               }
               if(ae.getSource() == BFile1)

                    try {

                         if(!BFile1.getText().equals("File1")) {
     
                              if(BFile1.getText().equals("d:\\picture1.pdf") || BFile1.getText().equals("d:\\picture1.PDF")) {
     
                                   Runtime r = Runtime.getRuntime();

//                                   String[] command = {"C:\\Program Files\\Adobe\\Reader 9.0\\Reader\\",BFile1.getText()};
                                   String[] command = {"acrord32.exe",BFile1.getText()}; 
                                   r.exec(command);




                              }
                              if(BFile1.getText().equals("d:\\picture1.txt") || BFile1.getText().equals("d:\\picture1.TXT")) {
     
                                    Runtime r = Runtime.getRuntime();
/*                                    String[] command = {
                                                      "C:\\Windows\\System32\\Notepad.EXE",
                                                       BFile1.getText()}; */

                                    String[] command = {
                                                      "Notepad.EXE",
                                                       BFile1.getText()}; 

                                    r.exec(command);
                              }
                              if(BFile1.getText().equals("d:\\picture1.jpg") || BFile1.getText().equals("d:\\picture1.JPG")) {
     
                                    Runtime r = Runtime.getRuntime();
/*                                   String[] command = {
                                                       "C:\\Program Files\\Internet Explorer\\IEXPLORE.EXE",
                                                       BFile1.getText()}; */

                                   String[] command = {
                                                       "IEXPLORE.EXE",
                                                       BFile1.getText()};
                                    r.exec(command);
                              }

                         }
                     } catch(Exception ex) {

                         ex.printStackTrace();
                     }
               {
               }
               if(ae.getSource() == BFile2)
               {
                    if(!BFile2.getText().equals("File2")) {

                         try {

                              if(BFile2.getText().equals("d:\\picture2.pdf") || BFile2.getText().equals("d:\\picture2.PDF")) {
     
                                   Runtime r = Runtime.getRuntime();
/*                                   String[] command = {
                                                       "C:\\Program Files\\Adobe\\Reader 9.0\\Reader\\AcroRd32.EXE",
                                                       BFile2.getText()}; */

                                   String[] command = {
                                                       "AcroRd32.EXE",
                                                       BFile2.getText()};

                                    r.exec(command);
                              }
                              if(BFile2.getText().equals("d:\\picture2.txt") ||  BFile2.getText().equals("d:\\picture2.TXT")) {
     
                                    Runtime r = Runtime.getRuntime();
/*                                    String[] command = {
                                                       "C:\\Windows\\System32\\Notepad.EXE",
                                                       BFile2.getText()}; */

                                    String[] command = {
                                                       "Notepad.EXE",
                                                       BFile2.getText()};

                                    r.exec(command);
                              }
                              if(BFile2.getText().equals("d:\\picture2.jpg") || BFile2.getText().equals("d:\\picture2.JPG")) {
     
                                   Runtime r = Runtime.getRuntime();
/*                                   String[] command = {
                                                       "C:\\Program Files\\Internet Explorer\\IEXPLORE.EXE",
                                                       BFile2.getText()}; */

                                   String[] command = {
                                                       "IEXPLORE.EXE",
                                                       BFile2.getText()};

                                    r.exec(command);
                              }

                         } catch(Exception ex) {

                              ex.printStackTrace();
                         }
                    }
               }
               if(ae.getSource() == BFile3)
               {
                    if(!BFile3.getText().equals("File3")) {

                         try {

                              if(BFile3.getText().equals("d:\\picture3.pdf") || BFile3.getText().equals("d:\\picture3.PDF")) {
     
                                   Runtime r = Runtime.getRuntime();
/*                                   String[] command = {
                                                       "C:\\Program Files\\Adobe\\Reader 9.0\\Reader\\AcroRd32.EXE",
                                                       BFile3.getText()}; */

                                   String[] command = {
                                                       "AcroRd32.EXE",
                                                       BFile3.getText()};

                                    r.exec(command);
                              }
                              if(BFile3.getText().equals("d:\\picture3.txt") || BFile3.getText().equals("d:\\picture3.TXT")) {
     
                                    Runtime r = Runtime.getRuntime();
/*                                    String[] command = {
                                                       "C:\\Windows\\System32\\Notepad.EXE",
                                                       BFile3.getText()}; */

                                    String[] command = {
                                                       "Notepad.EXE",
                                                       BFile3.getText()};

                                    r.exec(command);
                              }
                              if(BFile3.getText().equals("d:\\picture3.jpg") || BFile3.getText().equals("d:\\picture3.JPG")) {
     
                                    Runtime r = Runtime.getRuntime();
/*                                   String[] command = {
                                                       "C:\\Program Files\\Internet Explorer\\IEXPLORE.EXE",
                                                       BFile3.getText()}; */

                                   String[] command = {
                                                       "IEXPLORE.EXE",
                                                       BFile3.getText()};

                                    r.exec(command);
                              }

                          } catch(Exception ex) {

                              ex.printStackTrace();
                          }
                    }

               }
          }
     }


/*     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource() == BApply)
               {
                    setImage();
                    TDocName.setText("");
                    TAttribute1.setText("");
                    TAttribute2.setText("");
                    TAttribute3.setText("");
                    TNarr      .setText("");
               }

               if(ae.getSource() == BExit)
               {
                    removeFrame();
               }

          }
     } */


//     public void setImage(DocumentClass theClass)
     public void setImage()
     {
          String SPartyName   =    common.parseNull((String)JCParty.getSelectedItem());

          String SSize        = common.parseNull((String)JCSize.getSelectedItem());


          int  iSize1    =0;
          int  iSize2    =0;

          if(SSize.equals("25%"))
          {
               iSize1     =    450;
               iSize2     =    550;
          }


          if(SSize.equals("50%"))
          {
               iSize1     =    600;
               iSize2     =    700;
          }

          if(SSize.equals("75%"))
          {
               iSize1     =    900;
               iSize2     =    1000;
          }
          if(SSize.equals("100%"))
          {
               iSize1     =    1100;
               iSize2     =    1200;
          }


/*          String SPartyCode   =    theClass.SPartyCode;

          String SDocName     =    theClass.SDocName;

          String SAtt1        =    theClass.SAtt1;

          String SAtt2        =    theClass.SAtt2;

          String SAtt3        =    theClass.SAtt3;

          String SNarr        =    theClass.SNarr;

          String SDeptCode    =    theClass.SDept;
          String SCateCode    =    theClass.SCate;
          String SId          =    theClass.SId; */


          String QS1 =" Select Img1,Img2,Img3,fileformat,fileformat1,fileformat2 From Document "+
                      " where Id="+sId;


/*                      " Where  Id >=0  ";

                      if(SPartyCode.length() !=0)
                      {
                         QS1    = QS1 + " and PartyCode = '"+SPartyCode+"' ";
                      }

                      if(SDocName.length() !=0)
                      {
                         QS1 = QS1 + " and  DocName like '%"+SDocName.toUpperCase()+"%' ";
                      }

                      if(SAtt1.length() !=0)
                      {
                         QS1 = QS1 + " and  Attribute1 like '%"+SAtt1.toUpperCase()+"%' ";
                      }

                      if(SAtt2.length() !=0)
                      {
                         QS1 = QS1 + " and Attribute2 like '%"+SAtt2.toUpperCase()+"%' ";
                      }

                      if(SAtt3.length() !=0)
                      {
                         QS1 = QS1 + " and  Attribute3 like '%"+SAtt3.toUpperCase()+"%' ";
                      }

                      if(SNarr.length() !=0)
                      {
                         QS1 = QS1 + " and  TextAreaVal like '%"+SNarr.toUpperCase()+"%' ";
                      }

                      if(SDeptCode.length() !=0)
                      {
                         QS1    = QS1 + " and DeptCode = '"+SDeptCode+"' ";
                      }

                      if(SCateCode.length() !=0)
                      {
                         QS1    = QS1 + " and CateCode = '"+SCateCode+"' ";
                      } */

                      System.out.println(QS1);

          try
          {
               Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
               Connection theConnection = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");

               Statement theStatement = theConnection.createStatement();
               ResultSet theResult = theStatement.executeQuery(QS1);
               byte[] imgbytes =null;
               byte[] imgbytes1=null;
               byte[] imgbytes2=null;

               String SFor1 ="";
               String SFor2 ="";
               String SFor3 ="";


               if(theResult.next())
               {
                    imgbytes  =  getOracleBlob(theResult,"IMG1");
                    imgbytes1 =  getOracleBlob(theResult,"IMG2");
                    imgbytes2 =  getOracleBlob(theResult,"IMG3");
                    SFor1     =  common.parseNull(theResult.getString(4));
                    SFor2     =  common.parseNull(theResult.getString(5));
                    SFor3     =  common.parseNull(theResult.getString(6));
               }
               theResult.close();
               theStatement.close();

               Image   image  =null;
               Image   image1 =null;
               Image   image2 =null;


               Image  resizeimage =null;

               Image  resizeimage1=null;

               Image  resizeimage2=null;

	       String sPath = common.getPrintPath();

	       System.out.println("path==>"+sPath);	

               try
               {
                    if(imgbytes!=null)
                    {
                         String SFormat = SFor1;

                         System.out.println("format byte1-->"+SFormat);

                         image = Toolkit.getDefaultToolkit().createImage(imgbytes);

                         if(SFormat.equals("PDF")) {

//                              BFile1 . setText("d:\\picture1."+SFormat);

                              BFile1 . setText(sPath+"picture1."+SFormat);

//                              File file = new File("d:\\picture1."+SFormat);

                              File file = new File(sPath+"picture1."+SFormat);
                              FileOutputStream fos = new FileOutputStream(file);
                              fos.write(imgbytes);
                              fos.close();

                         }
                         else if(SFormat.equals("txt")) {

                               BFile1 . setText(sPath+"picture1."+SFormat);
                               File file = new File(sPath+"picture1."+SFormat);
                               FileOutputStream fos = new FileOutputStream(file);
                               fos.write(imgbytes);
                               fos.close();
                         }
                         else {

                              BFile1 . setText(sPath+"picture1."+SFormat);
                              File file = new File(sPath+"picture1."+SFormat);
                              FileOutputStream fos = new FileOutputStream(file);
                              fos.write(imgbytes);
                              fos.close();
                         }

                    }
                    else
                    {
                         image = Toolkit.getDefaultToolkit().createImage("");
                         SImg1 = "null";   

                    }


                    if(imgbytes1!=null)
                    {

                         image1 = Toolkit.getDefaultToolkit().createImage(imgbytes1);

                         String SFormat = SFor2;

                         System.out.println("format byte2-->"+SFormat);

                         if(SFormat.equals("PDF")) {

                               BFile2 . setText(sPath+"picture2."+SFormat);
                               File file = new File(sPath+"picture2."+SFormat);
                               FileOutputStream fos = new FileOutputStream(file);
                               fos.write(imgbytes1);
                               fos.close();

                         }
                         else if(SFormat.equals("txt")) {

                               BFile2 . setText(sPath+"picture2."+SFormat);
                               File file = new File(sPath+"picture2."+SFormat);

                               FileOutputStream fos = new FileOutputStream(file);
                               fos.write(imgbytes1);
                               fos.close();
                         }

                         else {

                               BFile2 . setText(sPath+"picture2."+SFormat);
                               File file = new File(sPath+"picture2."+SFormat);

                              FileOutputStream fos = new FileOutputStream(file);
                              fos.write(imgbytes1);
                              fos.close();
                         }

                    }
                    else
                    {
                         image1 = Toolkit.getDefaultToolkit().createImage("");
                         SImg2 = "null";   

                    }

                    if(imgbytes2!=null)
                    {

                         image2 = Toolkit.getDefaultToolkit().createImage(imgbytes2);

                         String SFormat = SFor3;

                         System.out.println("format byte3-->"+SFormat);


                         if(SFormat.equals("PDF")) {

                               BFile3 . setText(sPath+"picture3."+SFormat);
                               File file = new File(sPath+"picture3."+SFormat);

//                               File file = new File("d:\\picture3."+SFormat);
                               FileOutputStream fos = new FileOutputStream(file);
                               fos.write(imgbytes2);
                               fos.close();

                         }
                         else if(SFormat.equals("txt")) {

                               BFile3 . setText(sPath+"picture3."+SFormat);
                               File file = new File(sPath+"picture3."+SFormat);
                               FileOutputStream fos = new FileOutputStream(file);
                               fos.write(imgbytes2);
                               fos.close();

                         }
                         else {

                              BFile3 . setText(sPath+"picture3."+SFormat);
                              File file = new File(sPath+"picture3."+SFormat);

                              FileOutputStream fos = new FileOutputStream(file);
                              fos.write(imgbytes2);
                              fos.close();
                         }

                    }
                    else
                    {
                         image2 = Toolkit.getDefaultToolkit().createImage("");
                         SImg3 = "null";   
                    }



                    ImageFilter replicate = new ReplicateScaleFilter(iSize1,iSize2);
                    ImageProducer prod    = new FilteredImageSource(image.getSource(),replicate);


                    ImageFilter replicate1 = new ReplicateScaleFilter(iSize1,iSize2);
                    ImageProducer prod1    = new FilteredImageSource(image1.getSource(),replicate1);

                    ImageFilter replicate2 = new ReplicateScaleFilter(iSize1,iSize2);
                    ImageProducer prod2    = new FilteredImageSource(image2.getSource(),replicate2);


                    resizeimage  = createImage(prod);
                    resizeimage1 = createImage(prod1);
                    resizeimage2 = createImage(prod2); 

               }
               catch(Exception ex1)
               {
                    System.out.println(ex1);
                    ex1.printStackTrace();
               }


//               setPhotoPanel(resizeimage,resizeimage1,resizeimage2,iSize1,iSize2,SImg1,SImg2,SImg3);
               SImg1="";
               SImg2="";
               SImg3="";

               theConnection.close();

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private String getPartyCode(String SPartyName)
     {
          int iIndex  =  VPartyName.indexOf(SPartyName);

          if(iIndex!=-1)
               return (String)VPartyCode.elementAt(iIndex);

          return "";
     }
     private String getDeptCode(String SDeptName)
     {

          int iIndex =  VDeptName.indexOf(SDeptName);

          if(iIndex!=-1)
               return (String)VDeptCode.elementAt(iIndex);

          return "";
     }

     private String getCateCode(String SCateName)
     {

          int iIndex    =    VCName.indexOf(SCateName);



          if(iIndex!=-1)
               return (String)VCCode.elementAt(iIndex);

          return "";
     }



      private byte[] getOracleBlob(ResultSet result, String columnName) throws SQLException
      {
    
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        byte[] bytes = null;
        
        try {    
            oracle.sql.BLOB blob = ((oracle.jdbc.OracleResultSet)result).getBLOB(columnName);
            if(blob!=null)
            {
                 inputStream = blob.getBinaryStream();        
                 int bytesRead = 0;        
                 
                 while((bytesRead = inputStream.read()) != -1) {        
                     outputStream.write(bytesRead);    
                 }
                 
                 bytes = outputStream.toByteArray();
             }
        } catch(IOException e) {
            throw new SQLException(e.getMessage());
        } finally 
        {
        }
        return bytes;
     
     }



     class PaintPanel extends JPanel
     {
          private Image image;
          private Image image1;
          private Image image2;
          private int iSize1,iSize2;
          private String SImg1="",SImg2="",SImg3="";
          JLabel mylabel;
          JLabel mylabel1;
          JLabel mylabel2;


          public PaintPanel(Image image,Image image1,Image image2,int iSize1,int iSize2,String SImg1,String SImg2,String SImg3)
          {
               this.image = image;
               this.image1= image1;
               this.image2= image2;
               this.iSize1= iSize1;
               this.iSize2= iSize2;
               this.SImg1   = SImg1;
               this.SImg2   = SImg2;
               this.SImg3   = SImg3;


               if(!SImg1.equals("null") && SImg2.equals("null") && SImg3.equals("null"))
               {
                    System.out.println("comming layout "); 
                    setLayout(new FlowLayout());
                    mylabel = new JLabel(new ImageIcon(image));
                    add(mylabel);
               }

               if(!SImg1.equals("null") && !SImg2.equals("null") && SImg3.equals("null") )
               {
                    setLayout(new GridLayout(2,1));

                    mylabel = new JLabel(new ImageIcon(image));
                    mylabel1= new JLabel(new ImageIcon(image1));
                    add(mylabel);
                    add(mylabel1);
               } 

               if(!SImg1.equals("null") && !SImg2.equals("null") && !SImg3.equals("null") )
               {
                    setLayout(new GridLayout(3,1));
                    mylabel = new JLabel(new ImageIcon(image));
                    mylabel1= new JLabel(new ImageIcon(image1));
                    mylabel2= new JLabel(new ImageIcon(image2));

                    add(mylabel);
                    add(mylabel1);
                    add(mylabel2);
               }
               SImg1="";
               SImg2="";
               SImg3="";
          }


     }
     private void setPhotoPanel(Image image,Image image1,Image image2,int iSize1,int iSize2,String SImg1,String SImg2,String SImg3)
     {
          try
          {
               MiddlePanel.updateUI();
               JScrollPane jsp=null;
               if(p!=null)
               {
                    MiddlePanel.remove(p);
               } 

               if(image!=null)
               {
                    System.out.println("comming image");
                    p =  new PaintPanel(image,image1,image2,iSize1,iSize2,SImg1,SImg2,SImg3);
                    MiddlePanel.add(p);
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
      }

     private void removeFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex){}
     }


     public void setDeptVector()
     {
          VDeptName                     = new Vector();
          VDeptCode                     = new Vector();


          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
               Statement stat   = theConnection.createStatement();
               ResultSet rs     = stat.executeQuery(" Select DeptCode,DeptName From Hrdnew.Department Order by 2");

               while(rs.next())
               {
                    VDeptName          . addElement(common.parseNull(rs.getString(2)));
                    VDeptCode          . addElement(common.parseNull(rs.getString(1)));
               }
               rs                       . close();
	       stat			. close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setCategory()
     {
          theConnection                 = null;
          theStatement                  = null;

          VCCode                        = new Vector();
          VCName                        = new Vector();

          try
          {
               Class                    . forName("oracle.jdbc.OracleDriver");
               theConnection            = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
               theStatement             = theConnection.createStatement();
               ResultSet rs             = null;

               rs                       = theStatement.executeQuery(" Select CNAme,CCode From Category Order by 1");
               while(rs.next())
               {
                    VCName              . addElement(rs.getString(1));
                    VCCode              . addElement(rs.getString(2));
               }
               rs                       . close();
	       theStatement		. close();

          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }



}







