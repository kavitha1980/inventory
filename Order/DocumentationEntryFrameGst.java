package Order;

import java.io.*;                 
import java.util.*;              
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.JFileChooser;
import oracle.jdbc.*;
import oracle.sql.*;

import guiutil.*;
import util.*;



import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.pdfbox.TextToPDF;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.pdfbox.util.*;
import org.apache.pdfbox.util.PDFStreamEngine;


import org.apache.commons.logging.LogFactory;


public class DocumentationEntryFrameGst extends JInternalFrame
{

     JLayeredPane        Layer;

     JPanel              TopRight,TopLeft,TopPanel,BottomPanel,MiddlePanel,TopBottom1,TopBottom2;
     MyComboBox          JCParty,JCDepartment,JCCategory;
     MyTextField         TDocName,TRefNo,TAttribute1,TAttribute2,TAttribute3;
     MyLabel             LSize,LRefNo;

     JTextArea           TA;
     DateField           TDocDate;

     JButton             BSave,BExit,BUpLoad1,BUpLoad2,BUpLoad3,BUser;

     Connection          theConnection;
     private JTextField filename = new JTextField(), dir = new JTextField();

     private JButton open = new JButton("Open"), save = new JButton("Save");

     Statement           theStatement;

     Common              common;
     Vector              VPartyName,VPartyCode,VDeptName,VDeptCode,VCName,VCCode;

     int                 iUserCode;

     Vector              VUserVector,VUserName,VUserCode;

     JTable         ReportTable;
     DOInvTableModelGst  dataModel;
     InvTableModelGst    dataModel1;

     int iDocId=0;
     int iRow;

     boolean bModified=false;	


     DocumentationEntryFrameGst(JLayeredPane Layer,JTable ReportTable,InvTableModelGst dataModel1,int iRow)
     {
          this . Layer        	= Layer;
          this . ReportTable 	= ReportTable;
          this . dataModel1   	= dataModel1;
          this . iRow		= iRow;



          common              = new Common();
          setPartyVector   ();
          setDeptVector    ();
          setCategory      ();
          createComponents ();
          setLayouts       ();
          addComponents    ();
          addListeners     ();
     }
     DocumentationEntryFrameGst(JLayeredPane Layer,JTable ReportTable,DOInvTableModelGst dataModel,int iRow,boolean bModified)
     {
          this . Layer        = Layer;
          this . ReportTable 	= ReportTable;
          this . dataModel   	= dataModel;
          this . iRow		= iRow;
          this . bModified    = bModified;



          common              = new Common();
          setPartyVector   ();
          setDeptVector    ();
          setCategory      ();
          createComponents ();
          setLayouts       ();
          addComponents    ();
          addListeners     ();
     }


     private void createComponents()
     {
          TopRight            = new JPanel();
          TopLeft             = new JPanel();
          TopPanel            = new JPanel();
          BottomPanel         = new JPanel();
          MiddlePanel         = new JPanel();
          TopBottom1          = new JPanel();
          TopBottom2          = new JPanel();

          JCParty             = new MyComboBox(VPartyName);
          JCDepartment        = new MyComboBox(VDeptName);
          JCCategory          = new MyComboBox(VCName);
          JCParty             . setSelectedItem("NONE");

          JCCategory          . setSelectedItem("QUOTATION");

          JCDepartment        . setSelectedItem("STORES"); 

          TDocDate            = new DateField();
          TDocDate            . setTodayDate();

          TDocName            = new MyTextField();
          TRefNo              = new MyTextField();
          TAttribute1         = new MyTextField();
          TAttribute2         = new MyTextField();
          TAttribute3         = new MyTextField();
          LRefNo              = new MyLabel();

          LSize               = new MyLabel();
          
          BSave               = new JButton("Save");
          BExit               = new JButton("Exit");
          BUpLoad1            = new JButton("Attachment1");
          BUpLoad2            = new JButton("Attachment2");
          BUpLoad3            = new JButton("Attachment3");
          BUser               = new JButton("USER");

	  VUserVector         = new Vector();



          TA                 =    new JTextArea();

          Font font = new Font("Verdana", Font.BOLD, 12);
          TA.setFont(font);
//          TA.setForeground(Color.BLUE);
          TA.setForeground(new Color(0,0,225));
          TA                 .    setColumns(10);
          TA                 .    setRows(2);

//          setMaxRefNo();
     }

     private void setLayouts()
     {
          TopRight            . setLayout(new GridLayout(10,4));
          TopLeft             . setLayout(new GridLayout(2,1));

          TopBottom1           . setLayout(new BorderLayout());;
          TopBottom2           . setLayout(new FlowLayout());

          TopPanel             . setLayout(new GridLayout(2,1));
          BottomPanel         . setLayout(new FlowLayout());

          TopPanel            . setBorder(new TitledBorder(""));
          BottomPanel         . setBorder(new TitledBorder(""));

          TopPanel            . setBackground(new Color(213,234,255));
          TopRight            . setBackground(new Color(213,234,255));
          TopBottom2          . setBackground(new Color(213,234,255));
          BottomPanel         . setBackground(new Color(213,234,255));

          setTitle("Attach Document");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,700,500);
          show();

     }


     private void addComponents()
     {
          JScrollPane areaScrollPane = new JScrollPane(TA);
          areaScrollPane.setVerticalScrollBarPolicy(
                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
          areaScrollPane.setPreferredSize(new Dimension(250,250));

          TopRight            . add(new JLabel("Party Name"));
          TopRight            . add(JCParty);
          TopRight            . add(new JLabel("Doc Name"));
          TopRight            . add(TDocName);

          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel(""));

          TopRight            . add(new JLabel("Department"));
          TopRight            . add(JCDepartment);
          TopRight            . add(new JLabel("Category"));
          TopRight            . add(JCCategory);

          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel(""));

          TopRight            . add(new JLabel("Ref.No"));
          TopRight            . add(TRefNo);
          TopRight            . add(new JLabel("Doc Date"));
          TopRight            . add(TDocDate);

          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel(""));

          TopRight            . add(new JLabel("Attribute1"));
          TopRight            . add(TAttribute1);
          TopRight            . add(new JLabel("Attribute2"));
          TopRight            . add(TAttribute2);

          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel(""));


          TopRight            . add(new JLabel("Attribute3"));
          TopRight            . add(TAttribute3);
          TopRight            . add(new JLabel("Char Size(Miaximum 300)"));
          TopRight            . add(LSize);


//          TopRight            . add(new JLabel("Last Ref"));
//          TopRight            . add(LRefNo);

          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel(""));

          TopRight            . add(new JLabel(""));
          TopRight            . add(new JLabel("Doc Details"));

//          TopBottom2          . add(BUser);


          TopBottom2          . add(BUpLoad1);
          TopBottom2          . add(BUpLoad2);
          TopBottom2          . add(BUpLoad3);

          TopBottom1          . add(areaScrollPane,BorderLayout.CENTER);


          TopLeft             . add(TopBottom1);
          TopLeft             . add(TopBottom2);

          TopPanel            . add(TopRight);
          TopPanel            . add(TopLeft);


          BottomPanel         . add(BSave);
          BottomPanel         . add(BExit);

          getContentPane(). add(TopPanel,BorderLayout.CENTER);
          getContentPane(). add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
          BUpLoad1   . addActionListener(new ActList());
          BUpLoad2   . addActionListener(new ActList());
          BUpLoad3   . addActionListener(new ActList());
          BSave      . addActionListener(new ActList());
          BUser      . addActionListener(new ApplyList1(this));
          BExit      . addActionListener(new ActList());
          TA         . addKeyListener(new KeyList());
     }

     public class ApplyList1 implements ActionListener
     {
          DocumentationEntryFrameGst    theFrame;

          public ApplyList1(DocumentationEntryFrameGst theFrame)
          {
               this.theFrame = theFrame;
          }

          public void actionPerformed(ActionEvent ae)
          {
               try
               {

                    if((!TDocName.getText().equals("")))
                    {
                         UserSelectionGst US = new UserSelectionGst(Layer,theFrame);
                         Layer.add(US);
                         Layer.repaint();
                         US.setSelected(true);
                         Layer.updateUI();
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Fill DocName Field","Information",JOptionPane.INFORMATION_MESSAGE);
                    }


               }
               catch(Exception e)
               {
                    System.out.println(e);
                    e.printStackTrace();
               }
           }
     }


     public void setUserVector(Vector theVector)
     {
          VUserVector         = new Vector();
          VUserVector         = theVector;
     }

     

     private class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String SChar   = TA.getText();

               if(SChar != null)
               {
                    int  iNum       =  SChar.length();
                    LSize           .  setText(Integer.toString(iNum));
               }                   
               else
               {
                    LSize          . setText("");
               }
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource() == BUpLoad1)
               {
                    JFileChooser c = new JFileChooser();

                    int rVal = c.showOpenDialog(DocumentationEntryFrameGst.this);

                    if (rVal == JFileChooser.APPROVE_OPTION)
                    {

			 System.out.println("current director-->"+c.getCurrentDirectory().toString());

			 String sCurDirctory = c.getCurrentDirectory().toString();

			 if(sCurDirctory.startsWith("/")) {

				BUpLoad1.setText((c.getCurrentDirectory().toString())+"/"+(c.getSelectedFile().getName()));
			 }else {

				BUpLoad1.setText((c.getCurrentDirectory().toString())+"\\"+(c.getSelectedFile().getName()));
			 }

                         
                    }

                    if (rVal == JFileChooser.CANCEL_OPTION)
                    {
                         BUpLoad1.setText("Attachment1");
                    }
              }

               if(ae.getSource() == BUpLoad2)
               {
                    JFileChooser c = new JFileChooser();
                    int rVal = c.showOpenDialog(DocumentationEntryFrameGst.this);
                    if (rVal == JFileChooser.APPROVE_OPTION)
                    {

			 String sCurDirctory = c.getCurrentDirectory().toString();

			 if(sCurDirctory.startsWith("/")) {

				BUpLoad2.setText((c.getCurrentDirectory().toString())+"/"+(c.getSelectedFile().getName()));
			 }else {

				BUpLoad2.setText((c.getCurrentDirectory().toString())+"\\"+(c.getSelectedFile().getName()));
			 }

//                         BUpLoad2.setText((c.getCurrentDirectory().toString())+"\\"+(c.getSelectedFile().getName()));
                    }



                    if (rVal == JFileChooser.CANCEL_OPTION)
                    {
                         BUpLoad2.setText("Attachment2");
                    }


              }

               if(ae.getSource() == BUpLoad3)
               {
                    JFileChooser c = new JFileChooser();
                    int rVal = c.showOpenDialog(DocumentationEntryFrameGst.this);
                    if (rVal == JFileChooser.APPROVE_OPTION)
                    {

			 String sCurDirctory = c.getCurrentDirectory().toString();

			 if(sCurDirctory.startsWith("/")) {

				BUpLoad3.setText((c.getCurrentDirectory().toString())+"/"+(c.getSelectedFile().getName()));
			 }else {

				BUpLoad3.setText((c.getCurrentDirectory().toString())+"\\"+(c.getSelectedFile().getName()));
			 }


//                         BUpLoad3.setText((c.getCurrentDirectory().toString())+"\\"+(c.getSelectedFile().getName()));
                    }
                    if (rVal == JFileChooser.CANCEL_OPTION)
                    {
                         BUpLoad3.setText("Attachment3");
                    }
               }

               if(ae.getSource() == BSave)
               {
                    System.out.println("Length-->"+TA.getText().length());

                    if((TA.getText()).length()<=300)
                    {
//                         if( (!TDocName.getText().equals("")) && (!TAttribute1.getText().equals("")) && (!TAttribute2.getText().equals("")) && (!TAttribute3.getText().equals("")) )

                         if( (!TDocName.getText().equals(""))  )
                         {
                              if(JOptionPane.showConfirmDialog(null,"Confirm Save?","",JOptionPane.YES_NO_OPTION) == 0)
                              {
                                   try {


				       VUserVector         . addElement("1987");
				       VUserVector         . addElement("2008");
				       VUserVector         . addElement("1285");

                                        if(VUserVector.size()>0) {

					if (  (TAttribute1.getText().equals("")) ) {
						TAttribute1 . setText("0");
					}

					if (  (TAttribute2.getText().equals("")) ) {
						TAttribute2 . setText("0");
					}

					if (  (TAttribute3.getText().equals("")) ) {
						TAttribute3 . setText("0");
					}

     
                                                boolean bSave = InsertImage();
                                                setUserVector();
     
     
                                                if(bSave==true) {
                                                   insertValues();
                                                }
                                                TDocName   .setText("");
                                                TAttribute1.setText("");
                                                TAttribute2.setText("");
                                                TAttribute3.setText("");   

						if(bModified==true) {

						dataModel.setValueAt(String.valueOf(iDocId),iRow,24); 

					        dataModel.ColumnType[24]="E";	  	

						}else {

						dataModel1.setValueAt(String.valueOf(iDocId),iRow,23); 

					        dataModel1.ColumnType[23]="E";	  	


						}


						removeFrame();

//                                                setMaxRefNo();
                                        }
                                    }catch(Exception ex){
                                        JOptionPane.showMessageDialog(null,"Select Users","Information",JOptionPane.INFORMATION_MESSAGE);
                                        ex.printStackTrace();
                                    }
                              }
                              else
                              {
                                   return;
                              }
                         }

                        else
                        {
                              JOptionPane.showMessageDialog(null,"Feeding is incorrect","Information",JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                    else
                    {
                         JOptionPane    . showMessageDialog(null,"Enter Below 300 char in TextArea ","Information",JOptionPane.INFORMATION_MESSAGE);
                    }
               }

               if(ae.getSource() == BExit)
               {
                    removeFrame();
               }

          }

     }
     private boolean InsertImage()
     {
          try
          {

               String SPartyName   =    common.parseNull((String)JCParty.getSelectedItem());
               String SPartyCode   =    getPartyCode(SPartyName);

               String SDeptName    =    common.parseNull((String)JCDepartment.getSelectedItem());
               String SDeptCode    =    getDeptCode(SDeptName);

               String SCategory    =    common.parseNull((String)JCCategory.getSelectedItem());
               String SCateCode    =    getCategory(SCategory);

               String SSysDate     =    common.pureDate(common.getServerDate());

               String SAttach1     =    BUpLoad1.getText();
               String SAttach2     =    BUpLoad2.getText();
               String SAttach3     =    BUpLoad3.getText();

               String SFormat1     =SAttach1.substring(SAttach1.length()-3,SAttach1.length());
               String SFormat2     =SAttach2.substring(SAttach2.length()-3,SAttach2.length());
               String SFormat3     =SAttach3.substring(SAttach3.length()-3,SAttach3.length());

               if(SFormat1.equals("tml")) {

                    SFormat1 = "html";
               }


               if(SFormat1.equals("peg")) {

                    SFormat1 = "jpg";
               }
               if(SFormat2.equals("peg")) {

                    SFormat2 = "jpg";
               }
               if(SFormat3.equals("peg")) {

                    SFormat3 = "jpg";
               }


               System.out.println("attach 1"+SAttach1);
               System.out.println("attach 2"+SAttach2);
               System.out.println("attach 3"+SAttach3);

               System.out.println("format 1"+SFormat1);
               System.out.println("format 2"+SFormat2);
               System.out.println("format 3"+SFormat3);

               String SDocName     =    TDocName.getText().toUpperCase();
               String SRefNo       =    TRefNo.getText();
               String SDocDate     =    TDocDate.toNormal();

	       if(SRefNo.equals("")) {

               SRefNo = "0";
	       }

               String SAttr1       =    TAttribute1.getText().toUpperCase();
               String SAttr2       =    TAttribute2.getText().toUpperCase();
               String SAttr3       =    TAttribute3.getText();

               String STA          =    TA.getText().toUpperCase();

               int                 chunkSize;
               int                 chunkSize1;
               int                 chunkSize2;
               BLOB                image                   = null;
               BLOB                image1                  = null;
               BLOB                image2                  = null;
               byte[]              bytes;
               byte[]              bytes1;
               byte[]              bytes2;
               long                position;
               long                position1;
               long                position2;
               int                 bytesRead               = 0;
               int                 bytesRead1              = 0;
               int                 bytesRead2              = 0;

               String SIdQry = "select max(id) from document ";
               int iId =0;

               int iPageCount = 0;


               if(!SAttach1.equals("Attachment1")) {


                    if(SFormat3.equals("nt3")) {

                         SFormat3 = "";
                    }

                    if(SFormat2.equals("nt2")) {

                         SFormat2 = "";
                    }

                    File file           =    new File(SAttach1);
                    FileInputStream in  =    new FileInputStream(file);

                    String QS           =    " Insert into Document(ID,PartyCode,DeptCode,CateCode,RefNo,DocDate,Attribute1, "+
                                             " Attribute2,Attribute3,TextAreaVal,DocNAme,EntryDate,EntryUser,FileFormat,FileFormat1,FileFormat2,img1,img2,img3) Values ( "+
                                             " Document_Seq.nextVal,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,EMPTY_BLOB(),EMPTY_BLOB(),EMPTY_BLOB()) ";


                              Class.forName("oracle.jdbc.OracleDriver");
          
                              Connection theConnection = 
                              DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
          
                              theConnection.setAutoCommit(false);
          
                              PreparedStatement ps=theConnection.prepareStatement(QS);
               
                              ps.setString(1,SPartyCode);
                              ps.setInt(2,common.toInt(SDeptCode));
                              ps.setInt(3,common.toInt(SCateCode));
                              ps.setString(4,SRefNo);
                              ps.setInt(5,common.toInt(SDocDate));
                              ps.setString(6,SAttr1);
                              ps.setString(7,SAttr2);
                              ps.setString(8,SAttr3);
                              ps.setString(9,STA);
                              ps.setString(10,SDocName);  
                              ps.setInt(11,common.toInt(SSysDate));
                              ps.setInt(12,iUserCode);
                              ps.setString(13,SFormat1);
                              ps.setString(14,SFormat2);
                              ps.setString(15,SFormat3);

                              ps.executeUpdate();
                              ps.close();

                        Statement theStatementId = theConnection.createStatement();


                         ResultSet rsetId = theStatementId.executeQuery(SIdQry);
                         while(rsetId.next())
                              iId = common.toInt(rsetId.getString(1));
     
                         rsetId.close();
                         theStatementId.close();

                         System.out.println("Id After Insert Empty Blob"+iId);


/*                        String sqlText = "SELECT img1 from document " +
                                 " WHERE  partycode = '"+SPartyCode+"' "+
                                " and DeptCode='"+SDeptCode+"' and catecode='"+SCateCode+"' and docdate="+SDocDate+" and refno='"+SRefNo+"' "+
                                "  and DocName='"+SDocName+"' and Attribute1='"+SAttr1+"' and Attribute2='"+SAttr2+"'  FOR UPDATE "; */


                        String sqlText = "SELECT img1 from document "+
                                 " WHERE  Id = '"+iId+"' FOR UPDATE ";

                        System.out.println(sqlText);
     
                        Statement theStatement = theConnection.createStatement();
                        ResultSet rset = theStatement.executeQuery(sqlText);
                        rset.next();
                        image = ((OracleResultSet) rset).getBLOB("img1");
                        chunkSize    = image.getChunkSize();
                        bytes        = new byte[chunkSize];
     
                          position = 2;
              
                          while ((bytesRead = in.read(bytes)) != -1) {
                             image.putBytes(position, bytes);
                             position        += bytesRead;
                        
                          }
                         theConnection  .commit();
                         rset           .close();
                         theStatement   .close();

                         System.out.println("comming after first attachment");


/*                         rset = theStatement.executeQuery(SIdQry);
                         while(rset.next())
                              iId = common.toInt(rset.getString(1));
     
                         rset.close();
                         theStatement.close(); */


/*                         if(SFormat1.equals("pdf") || SFormat1.equals("PDF")) {

                              String src     = SAttach1.trim();
     
                              String sPath   = common.getPrintPath();
                         
                              System.out.println("Path :"+sPath);
     

                         try{


                         
                                   //load pdf file in the document object
          
                                   PDDocument doc=PDDocument.load(new FileInputStream(src));
          
                                   //Get all pages from document and store them in a list
          
                                   List<PDPage> pages=doc.getDocumentCatalog().getAllPages();
                                   //create iterator object so it is easy to access each page from the list
                                   Iterator<PDPage> i= pages.iterator();
                                   int count=1; //count variable used to separate each image file
                                   //Convert every page of the pdf document to a unique image file
                                   System.out.println("Please wait...");
                                   
                                   while(i.hasNext()){
          
                                        PDPage page=i.next();
                                        BufferedImage bi=page.convertToImage();
          
                                        ImageIO.write(bi, "jpg", new File(sPath+iId+count+".jpg"));
                                        count++;
          
                                        iPageCount++;
                                   }
                                   System.out.println("Conversion complete");
     
                                   doc.close();
     
                              }catch(IOException ie)
                              {
                                   ie.printStackTrace();
     
                              }
     
     
                              for(int i=0;i<iPageCount;i++) {
     
                                   String sEmptyInsert = " Insert into DocDetails (Docid,Image,Format,SlNo) "+
                                                         " Values (?,EMPTY_BLOB(),?,?) ";
     
                                   theConnection.setAutoCommit(false);
               
                                   PreparedStatement ps1=theConnection.prepareStatement(sEmptyInsert);
     
                                   ps1.setInt      (1,iId);
                                   ps1.setString   (2,"jpg");
                                   ps1.setInt      (3,(i+1));
     
                                   ps1.executeUpdate();
                                   ps1.close();
     
                              }
     
                              int count=1;
     
                              for(int i=0;i<iPageCount;i++) {
     
                                   String sFile        =    sPath+iId+count+".jpg";
     
                                   File file1           =    new File(sFile);
                                   FileInputStream in1  =    new FileInputStream(file1);
     
     
                                   String sqlText1 = "SELECT image from docdetails " +
                                           " WHERE DocId="+iId+" and slNo="+count+" FOR UPDATE   ";
     
     
     
                                  Statement theStatement1  = theConnection.createStatement();
                                  ResultSet rset1          = theStatement1.executeQuery(sqlText1);
                                  rset1.next();
                                  image = ((OracleResultSet) rset1).getBLOB("image");
                                  chunkSize    = image.getChunkSize();
                                  bytes        = new byte[chunkSize];
               
                                    position = 2;
                        
                                    while ((bytesRead = in1.read(bytes)) != -1) {
                                       image.putBytes(position, bytes);
                                       position        += bytesRead;
                                    }
                                   theConnection  .commit();
                                   rset1          .close();
                                   count++;
                              }
                         } */

                         JOptionPane.showMessageDialog(null," Data Saved  Id-->"+iId);
               }


               int iPrevPageCount  =0;

               if(!SAttach2.equals("Attachment2")) {


                         System.out.println("comming attachment 2");



/*                         if(SFormat2.equals("pdf") || SFormat2.equals("PDF")) {

                                   iPrevPageCount = iPageCount;
     
                                   String src     = SAttach2.trim();
          
                                   String sPath   = common.getPrintPath();
                              
                                   System.out.println("comming attachement 2 Path :"+sPath);
     
                              try{
                              
                                   //load pdf file in the document object
          
                                   PDDocument doc=PDDocument.load(new FileInputStream(src));
          
                                   //Get all pages from document and store them in a list
          
                                   List<PDPage> pages=doc.getDocumentCatalog().getAllPages();
                                   //create iterator object so it is easy to access each page from the list
                                   Iterator<PDPage> i= pages.iterator();
                                   int count=iPageCount+1; //count variable used to separate each image file
                                   //Convert every page of the pdf document to a unique image file
                                   System.out.println("Please wait...");
                                   
                                   while(i.hasNext()){
          
                                        PDPage page=i.next();
                                        BufferedImage bi=page.convertToImage();
          
                                        ImageIO.write(bi, "jpg", new File(sPath+iId+count+".jpg"));
                                        count++;
          
                                        iPrevPageCount++;
                                   }
                                   System.out.println("Conversion complete 2 ");
     
                                   doc .close();
     
                              }catch(IOException ie)
                              {
                                   ie.printStackTrace();
                              }
     
     
                              for(int i=iPageCount;i<iPrevPageCount;i++) {
     
                                   String sEmptyInsert = " Insert into DocDetails (Docid,Image,Format,SlNo) "+
                                                         " Values (?,EMPTY_BLOB(),?,?) ";
     
                                   theConnection.setAutoCommit(false);
               
                                   PreparedStatement ps1=theConnection.prepareStatement(sEmptyInsert);
     
                                   ps1.setInt      (1,iId);
                                   ps1.setString   (2,"jpg");
                                   ps1.setInt      (3,(i+1));
     
                                   ps1.executeUpdate();
                                   ps1.close();
     
                              }
     
                              int count=iPageCount+1;
     
                              System.out.println("Prev Page Count ==>"+iPrevPageCount);
                              System.out.println("Page Count     ==>"+iPageCount);
     
                              for(int i=iPageCount;i<iPrevPageCount;i++) {
     
                                   String sFile         =    sPath+iId+count+".jpg";
     
                                   File file1           =    new File(sFile);
                                   FileInputStream in1  =    new FileInputStream(file1);
     
                                   String sqlText1 = "SELECT image from docdetails " +
                                           " WHERE DocId="+iId+" and slNo="+count+" FOR UPDATE   ";
     
     
                                  Statement theStatement1  = theConnection.createStatement();
                                  ResultSet rset1          = theStatement1.executeQuery(sqlText1);
                                  rset1.next();
                                  image = ((OracleResultSet) rset1).getBLOB("image");
                                  chunkSize    = image.getChunkSize();
                                  bytes        = new byte[chunkSize];
               
                                    position = 2;
                        
                                    while ((bytesRead = in1.read(bytes)) != -1) {
                                       image.putBytes(position, bytes);
                                       position        += bytesRead;
                                    }
     
                                   theConnection.commit();
                                   rset1.close();
                                   count++;
                              }

                         } */
 

                         File file           =    new File(SAttach2);
                         FileInputStream in  =    new FileInputStream(file);


                        String sqlText = "SELECT img2 from document " +
                                 " WHERE  Id = '"+iId+"'   FOR UPDATE  ";

                        System.out.println("sql text2:"+sqlText);
     
                        Statement theStatement = theConnection.createStatement();
                        theConnection.setAutoCommit(false);

                        ResultSet rset = theStatement.executeQuery(sqlText);
                        rset.next();
                        image1 = ((OracleResultSet) rset).getBLOB("img2");
                        chunkSize    = image1.getChunkSize();
                        bytes        = new byte[chunkSize];
     
                          position = 2;
           
                          while ((bytesRead = in.read(bytes)) != -1) {
                             image1.putBytes(position, bytes);
                             position        += bytesRead;
                        
                          }
                         theConnection  .commit();
                         rset           .close();
                         theStatement   .close(); 
               }



               if(!SAttach3.equals("Attachment3")) {


                         System.out.println("comming attachment 3");


/*                         if(SFormat3.equals("pdf") || SFormat3.equals("PDF")) {

     
                                   System.out.println("comm 3 prev page count"+iPrevPageCount);
     
                                   iPageCount = iPrevPageCount;
     
                                   System.out.println("comm 3  page count"+iPageCount);
     
     
                                   String src     = SAttach3.trim();
          
                                   String sPath   = common.getPrintPath();
                              
                                   System.out.println("comming attachement 3 Path :"+sPath);
     
                              try{
                              
                                   //load pdf file in the document object
          
                                   PDDocument doc=PDDocument.load(new FileInputStream(src));
          
                                   //Get all pages from document and store them in a list
          
                                   List<PDPage> pages=doc.getDocumentCatalog().getAllPages();
                                   //create iterator object so it is easy to access each page from the list
                                   Iterator<PDPage> i= pages.iterator();
                                   int count=iPageCount+1; //count variable used to separate each image file
                                   //Convert every page of the pdf document to a unique image file
                                   System.out.println("Please wait...");
                                   
                                   while(i.hasNext()){
          
                                        PDPage page=i.next();
                                        BufferedImage bi=page.convertToImage();
          
                                        ImageIO.write(bi, "jpg", new File(sPath+iId+count+".jpg"));
                                        count++;
          
                                        iPrevPageCount++;
                                   }
                                   System.out.println("Conversion complete 3 ");
     
                                   doc .close();
     
                              }catch(IOException ie)
                              {
                                   ie.printStackTrace();
                              }
     
     
                              for(int i=iPageCount;i<iPrevPageCount;i++) {
     
                                   String sEmptyInsert = " Insert into DocDetails (Docid,Image,Format,SlNo) "+
                                                         " Values (?,EMPTY_BLOB(),?,?) ";
     
                                   theConnection.setAutoCommit(false);
               
                                   PreparedStatement ps1=theConnection.prepareStatement(sEmptyInsert);
     
                                   ps1.setInt      (1,iId);
                                   ps1.setString   (2,"jpg");
                                   ps1.setInt      (3,(i+1));
     
                                   ps1.executeUpdate();
                                   ps1.close();
     
                              }
     
                              int count=iPageCount+1;
     
                              System.out.println("Prev Page Count ==>"+iPrevPageCount);
                              System.out.println("Page Count     ==>"+iPageCount);
     
                              for(int i=iPageCount;i<iPrevPageCount;i++) {
     
                                   String sFile         =    sPath+iId+count+".jpg";
     
                                   File file1           =    new File(sFile);
                                   FileInputStream in1  =    new FileInputStream(file1);
     
                                   String sqlText1 = "SELECT image from docdetails " +
                                           " WHERE DocId="+iId+" and slNo="+count+" FOR UPDATE   ";
     
     
                                  Statement theStatement1  = theConnection.createStatement();
                                  ResultSet rset1          = theStatement1.executeQuery(sqlText1);
                                  rset1.next();
                                  image = ((OracleResultSet) rset1).getBLOB("image");
                                  chunkSize    = image.getChunkSize();
                                  bytes        = new byte[chunkSize];
               
                                    position = 2;
                        
                                    while ((bytesRead = in1.read(bytes)) != -1) {
                                       image.putBytes(position, bytes);
                                       position        += bytesRead;
                                    }
     
                                   theConnection.commit();
                                   rset1.close();
                                   count++;
                              }

                         }

*/

                         File file           =    new File(SAttach3);
                         FileInputStream in  =    new FileInputStream(file);

                        String sqlText = "SELECT img3 from document " +
                                 " WHERE  Id = '"+iId+"'   FOR UPDATE  ";

                        System.out.println("sql text3:"+sqlText);
     
                        Statement theStatement = theConnection.createStatement();
                        theConnection.setAutoCommit(false);

                        ResultSet rset = theStatement.executeQuery(sqlText);
                        rset.next();
                        image2 = ((OracleResultSet) rset).getBLOB("img3");
                        chunkSize    = image2.getChunkSize();
                        bytes        = new byte[chunkSize];
     
                          position = 2;
           
                          while ((bytesRead = in.read(bytes)) != -1) {
                             image2.putBytes(position, bytes);
                             position        += bytesRead;
                        
                          }
                         theConnection  .commit();
                         rset           .close();
                         theStatement   .close(); 
               }

               iDocId=iId;

               System.out.println("comming end");

          }
          catch(Exception  ex)
          {
               ex.printStackTrace();
               return false;
          }

          return true;
     }

     private void insertValues()
     {
          for(int i=0;i<VUserVector.size();i++)
          {
//               String SUserName    =    ((String)VUserVector.elementAt(i));
//               String SUserCode    =    getUserCode(SUserName);

               String SUserCode    =    ((String)VUserVector.elementAt(i));
               String SDocName     =    TDocName.getText();

               int    iId          =    getId();

               System.out.println("comming into insert values");

               insertData(SUserCode,SDocName,iId);
          }

     }
     private void insertData(String SUserCode,String SDocName,int iId)
     {
          String QS = " Insert into DocUser ( ID,UserCode,DocName,DocId) Values( DocUser_Seq.nextVal , "+SUserCode+",'"+SDocName+"',"+iId+" ) ";

          System.out.println(QS);

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
               Statement stat   = theConnection.createStatement();
               stat.executeQuery(QS);

          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
     }



     private String getUserCode(String SUser)
     {
          int  iIndex    =    0;

          iIndex         =    VUserName.indexOf(SUser);

          if(iIndex!=-1)
               return    (String)VUserCode.elementAt(iIndex);
          return    "";
     }



     private String getNarration(String Str)
     {
          String SNarr="";

          StringTokenizer ST = new StringTokenizer(Str,"'");
          while(ST.hasMoreElements())
               SNarr =SNarr+ST.nextToken()+"'";

          if(SNarr.length()==0)
               return "";

          return SNarr.substring(0,SNarr.length()-2);
     }



     private String getPartyCode(String SPartyName)
     {
          int  iIndex    =    0;

          iIndex         =    VPartyName.indexOf(SPartyName);

          if(iIndex!=-1)
               return    (String)VPartyCode.elementAt(iIndex);
          return    "";
     }
     private String getDeptCode(String SDeptName)
     {
          int  iIndex    =    0;

          iIndex         =    VDeptName.indexOf(SDeptName);

          if(iIndex!=-1)
               return    (String)VDeptCode.elementAt(iIndex);
          return    "";
     }
     private String getCategory(String SCategory)
     {
          int  iIndex    =    0;

          iIndex         =    VCName.indexOf(SCategory);

          if(iIndex!=-1)
               return    (String)VCCode.elementAt(iIndex);
          return    "";
     }

     public void setPartyVector()
     {
          VPartyName                    = new Vector();
          VPartyCode                    = new Vector();

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","scm","rawmat");
               Statement stat   = theConnection.createStatement();
               ResultSet rs     = stat.executeQuery("  Select PartyNAme,PartyCode From PartyMAster Order by 1");
               while(rs.next())
               {
                    VPartyName          . addElement(common.parseNull(rs.getString(1)));
                    VPartyCode          . addElement(common.parseNull(rs.getString(2)));
               }
               rs                       . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setDeptVector()
     {
          VDeptName                     = new Vector();
          VDeptCode                     = new Vector();


          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
               Statement stat   = theConnection.createStatement();
               ResultSet rs     = stat.executeQuery(" Select DeptCode,DeptName From hrdnew.Department Order by 2");

               while(rs.next())
               {
                    VDeptName          . addElement(common.parseNull(rs.getString(2)));
                    VDeptCode          . addElement(common.parseNull(rs.getString(1)));
               }
               rs                       . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private int getId()
     {
          int id =0;

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
               Statement stat   = theConnection.createStatement();
               ResultSet rs     = stat.executeQuery(" Select Max(id) From Document ");

               while(rs.next())
               {
                    id          =  common.toInt(rs.getString(1));
               }
               rs                       . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          return id;
     }


     public void setCategory()
     {
          theConnection                 = null;
          theStatement                  = null;

          VCCode                        = new Vector();
          VCName                        = new Vector();

          try
          {
               Class                    . forName("oracle.jdbc.OracleDriver");
               theConnection            = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
               theStatement             = theConnection.createStatement();
               ResultSet rs             = null;

               rs                       = theStatement.executeQuery(" Select CNAme,CCode From Category Order by 1");
               while(rs.next())
               {
                    VCName              . addElement(rs.getString(1));
                    VCCode              . addElement(rs.getString(2));
               }
               rs                       . close();

          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void setUserVector()
     {
          VUserName                    = new Vector();
          VUserCode                    = new Vector();

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
               Statement stat   = theConnection.createStatement();
               ResultSet rs     = stat.executeQuery("  Select UserName,UserCode From RawUser ");
               while(rs.next())
               {
                    VUserName          . addElement(common.parseNull(rs.getString(1)));
                    VUserCode          . addElement(common.parseNull(rs.getString(2)));
               }
               rs                       . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
     private void setMaxRefNo()
     {
          String SNo="";
          int    iId=0;

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
               Statement stat   = theConnection.createStatement();
               ResultSet rs     = stat.executeQuery("  Select Max(document.Id) from Document inner join docuser on document.id=docuser.docid where  usercode="+iUserCode+"  ");

               System.out.println("Select Max(document.Id) from Document inner join docuser on document.id=docuser.docid where  usercode="+iUserCode+" " );
               while(rs.next())
               {
                    iId          = rs.getInt(1);
               }
               rs                      . close();
               rs     = stat.executeQuery("  Select RefNo from Document where id="+iId+" ");
               while(rs.next())
               {
                    SNo          = rs.getString(1);
               }
               rs.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
          LRefNo . setText(SNo);
     }




     private void removeFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex){}
     }



     
}


