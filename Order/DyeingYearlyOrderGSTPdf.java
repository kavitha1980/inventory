package Order;

import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class DyeingYearlyOrderGSTPdf
{


     String         SOrdNo    = "",SOrdDate  = "",SSupCode  = "",SSupName  = "";


     String SFile,SSeqId,StDate,EnDate;
     String SItemTable;
     int    iMillCode;

     

     Document document;
     PdfPTable table;
   

     DyeingYearlyOrderGSTPdf(Document document,String SFile,String SSupCode,String SSupName,String StDate,String EnDate,String SSeqId,int iMillCode)
     {
          this.document   =document;
          this.SFile      = SFile;
          this.StDate     = StDate;
          this.EnDate     = EnDate;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.SSeqId     = SSeqId;
          this.iMillCode  = iMillCode;
          

       
          createPdfFile();
     }
     
   
     public void createPdfFile()
     {
     try
     {
        new DyeingYearlyOrderGSTPdfClass(document,table,SFile,SSupCode,SSupName,StDate,EnDate,SSeqId,iMillCode);
    
     }
       catch (Exception e)
          {
               e.printStackTrace();
          }
     }
}
