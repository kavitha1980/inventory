// Used By Order,GRN  and other operations
// The Operation Varies with the state of bSig.
// Initially it is set to true once called from Non-Order activities,
// the same shall be set to false.

package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class EPCGDOInvMiddlePanel extends JPanel
{
     JTable         ReportTable;
     JLayeredPane   DeskTop;
     JComboBox      JCDept,JCGroup,JCUnit,JCBlock,JCTax;
     JLabel         LBasic,LDiscount,LCenVat,LTax,LSur,LNet;
     JPanel         GridPanel,BottomPanel,FigurePanel,ControlPanel;
     JButton        BAdd;

     Common              common = new Common();
     EPCGInvTableModel   dataModel;

     NextField      TAdd,TLess;


     Vector         VDept,VDeptCode,VGroup,VGroupCode,VUnit,VUnitCode;
     Vector         VDesc,VMake,VDraw,VCatl,VNameCode;
     Vector         VBlock,VBlockName;
     Vector         VTaxClaim;
     Vector         VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo;
     Vector         VCode,VName;
     Vector         VItemsStatus,VOrdSlNo;

     Object         RowData[][];

     String         ColumnData[],ColumnType[];
     String         SMOrderNo, SMiReferenceNo;

     boolean        bflag = false;

     int            iMillCode;
     int            iOrderStatus   = 0;
     int            iTaxAuth       = 0;
     String         SItemTable;


     // Constructor Method refferred from Order Collection Operations
     EPCGDOInvMiddlePanel(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,Object RowData[][],String ColumnData[],String ColumnType[],int iMillCode,String SItemTable)
     {
          this.DeskTop        = DeskTop;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.RowData        = RowData;
          this.ColumnData     = ColumnData;
          this.ColumnType     = ColumnType;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     
          setReportTable();
     }

     EPCGDOInvMiddlePanel(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,Object RowData[][],String ColumnData[],String ColumnType[],boolean bflag,int iOrderStatus,Vector VItemsStatus,int iMillCode,String SMiReferenceNo,String SItemTable)
     {
          this.DeskTop        = DeskTop;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.RowData        = RowData;
          this.ColumnData     = ColumnData;
          this.ColumnType     = ColumnType;
          this.iMillCode      = iMillCode;
          this.bflag          = bflag;
          this.iOrderStatus   = iOrderStatus;
          this.VItemsStatus   = VItemsStatus;
          this.SMiReferenceNo      = SMiReferenceNo;
          this.SItemTable     = SItemTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     
          setReportTable();
     }

     public void createComponents()
     {
          LBasic         = new JLabel("0");
          LDiscount      = new JLabel("0");
          LCenVat        = new JLabel("0");
          LTax           = new JLabel("0");
          LSur           = new JLabel("0");
          TAdd           = new NextField();
          TLess          = new NextField();
          LNet           = new JLabel("0");
          GridPanel      = new JPanel(true);
          BottomPanel    = new JPanel();
          FigurePanel    = new JPanel();
          ControlPanel   = new JPanel();

          VOrdSlNo       = new Vector();    
          
          VDesc          = new Vector();
          VMake          = new Vector();
          VDraw          = new Vector();
          VCatl          = new Vector();

          VPColour       = new Vector();
          VPSet          = new Vector();
          VPSize         = new Vector();
          VPSide         = new Vector();
          VPSlipFrNo     = new Vector();
          VPSlipToNo     = new Vector();
          VPBookFrNo     = new Vector();
          VPBookToNo     = new Vector();

          BAdd           = new JButton("Add New Record");
     }

     public void setLayouts()
     {
          GridPanel      . setLayout(new BorderLayout());
          BottomPanel    . setLayout(new BorderLayout());
          FigurePanel    . setLayout(new GridLayout(2,8));
          ControlPanel   . setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          VTaxClaim      = new Vector();
          VTaxClaim      . insertElementAt("Not Claimable",0);
          VTaxClaim      . insertElementAt("Claimable",1);

          FigurePanel    . add(new JLabel("Basic"));
          FigurePanel    . add(new JLabel("Discount"));
          FigurePanel    . add(new JLabel("CenVat"));
          FigurePanel    . add(new JLabel("Tax"));
          FigurePanel    . add(new JLabel("Surcharge"));
          FigurePanel    . add(new JLabel("Plus"));
          FigurePanel    . add(new JLabel("Minus"));
          FigurePanel    . add(new JLabel("Net"));
     
          FigurePanel    . add(LBasic);
          FigurePanel    . add(LDiscount);
          FigurePanel    . add(LCenVat);
          FigurePanel    . add(LTax);
          FigurePanel    . add(LSur);
          FigurePanel    . add(TAdd);
          FigurePanel    . add(TLess);
          FigurePanel    . add(LNet);
     
          ControlPanel   . add(BAdd);
     
          BottomPanel    . add("North",FigurePanel);
          BottomPanel    . add("South",ControlPanel);
     
          getDeptGroupUnit();

          JCDept         = new JComboBox(VDept);
          JCGroup        = new JComboBox(VGroup);
          JCUnit         = new JComboBox(VUnit);
          JCBlock        = new JComboBox(VBlockName);
          JCTax          = new JComboBox(VTaxClaim);
     }

     public void addListeners()
     {
          BAdd      . addActionListener(new ActList());
          TAdd      . addKeyListener(new KeyList());
          TLess     . addKeyListener(new KeyList());
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               calc();
          }
     }

     public void calc()
     {
          double dTNet = common.toDouble(LBasic.getText())-common.toDouble(LDiscount.getText())+common.toDouble(LCenVat.getText())+common.toDouble(LTax.getText())+common.toDouble(LSur.getText());
          dTNet        = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
          LNet         . setText(common.getRound(dTNet,2));                              
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BAdd)
               {
                    if(bflag)
                    {
                         VItemsStatus.addElement("0");
                    }

                    Vector VEmpty1 = new Vector();
               
                    for(int i=0;i<ColumnData.length;i++)
                    {
                         VEmpty1   . addElement(" ");
                    }
                    VDesc          . addElement(" ");
                    VMake          . addElement(" ");
                    VDraw          . addElement(" ");
                    VCatl          . addElement(" ");

                    VPColour       . addElement(" ");
                    VPSet          . addElement(" ");
                    VPSize         . addElement(" ");
                    VPSide         . addElement(" ");
                    VPSlipFrNo     . addElement(" ");
                    VPSlipToNo     . addElement(" ");
                    VPBookFrNo     . addElement(" ");
                    VPBookToNo     . addElement(" ");

                    dataModel           . addRow(VEmpty1);
                    ReportTable         . updateUI();
               }
          }
     }

     public void setReportTable()
     {
          dataModel      = new EPCGInvTableModel(RowData,ColumnData,ColumnType,LBasic,LDiscount,LCenVat,LTax,LSur,TAdd,TLess,LNet);       
          ReportTable    = new JTable(dataModel);
          ReportTable    . setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
     
          DefaultTableCellRenderer cellRenderer   = new DefaultTableCellRenderer();
                                   cellRenderer   . setHorizontalAlignment(JLabel.RIGHT);
     
          for (int col=0;col<ReportTable.getColumnCount();col++)
          {
               if(ColumnType[col]=="N" || ColumnType[col]=="B")
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
          }
          //ReportTable.setShowGrid(false);
     
          TableColumn BlockColumn  = ReportTable.getColumn("Order Block");
          TableColumn deptColumn   = ReportTable.getColumn("Department");
          TableColumn groupColumn  = ReportTable.getColumn("Group");
          TableColumn unitColumn   = ReportTable.getColumn("Unit");
          TableColumn TaxColumn    = ReportTable.getColumn("Vat");
     
          BlockColumn              . setCellEditor(new DefaultCellEditor(JCBlock));
          deptColumn               . setCellEditor(new DefaultCellEditor(JCDept));
          groupColumn              . setCellEditor(new DefaultCellEditor(JCGroup));
          unitColumn               . setCellEditor(new DefaultCellEditor(JCUnit));
          TaxColumn                . setCellEditor(new DefaultCellEditor(JCTax));
     
          setLayout(new BorderLayout());
          GridPanel.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
          GridPanel.add(new JScrollPane(ReportTable),BorderLayout.CENTER);
     
          add(BottomPanel,BorderLayout.SOUTH);
          add(GridPanel,BorderLayout.CENTER);
     
          ReportTable    . addMouseListener(new MouseList());
          ReportTable    . addKeyListener(new DescList());
          ReportTable    . addKeyListener(new StationaryList());
     }

     public class MouseList extends MouseAdapter
     {
          public void mouseClicked(MouseEvent me)
          {
               try
               {
                    if(bflag)
                    {
                         int i = ReportTable.getSelectedRow();
                         int iItemStatus = common.toInt((String)VItemsStatus.elementAt(i));

                         if(iItemStatus == 0)
                         {
                              if (me.getClickCount()==2)
                              {
                                   EPCGMaterialPicker MP = new EPCGMaterialPicker(DeskTop,VCode,VName,VNameCode,VDesc,VMake,VDraw,VCatl,ReportTable,dataModel,iMillCode,true,bflag,VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo,iTaxAuth,SItemTable);
                                   try
                                   {
                                        DeskTop   . add(MP);
                                        DeskTop   . repaint();
                                        MP        . setSelected(true);
                                        DeskTop   . updateUI();
                                        MP        . show();
                                        MP        . BrowList.requestFocus();
                                   }
                                   catch(java.beans.PropertyVetoException ex){}
                              }
                         }

                         if(iItemStatus == 1)
                         {
                              JOptionPane.showMessageDialog(null,"The Item have already GRN","Error",JOptionPane.ERROR_MESSAGE);
                         }

                         if(iItemStatus == 2)
                         {
                              if (me.getClickCount()==2)
                              {
                                   EPCGMaterialPicker MP = new EPCGMaterialPicker(DeskTop,VCode,VName,VNameCode,VDesc,VMake,VDraw,VCatl,ReportTable,dataModel,iMillCode,false,bflag,VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo,iTaxAuth,SItemTable);
                                   try
                                   {
                                        DeskTop   . add(MP);
                                        DeskTop   . repaint();
                                        MP        . setSelected(true);
                                        DeskTop   . updateUI();
                                        MP        . show();
                                        MP        . BrowList.requestFocus();
                                   }
                                   catch(java.beans.PropertyVetoException ex){}
                              }
                         }
                    }
                    else
                    {
                         if (me.getClickCount()==2)
                         {
                              EPCGMaterialPicker MP = new EPCGMaterialPicker(DeskTop,VCode,VName,VNameCode,VDesc,VMake,VDraw,VCatl,ReportTable,dataModel,iMillCode,VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo,SItemTable);
                              try
                              {
                                   DeskTop   . add(MP);
                                   DeskTop   . repaint();
                                   MP        . setSelected(true);
                                   DeskTop   . updateUI();
                                   MP        . show();
                                   MP        . BrowList     . requestFocus();
                              }
                              catch(java.beans.PropertyVetoException ex){}
                         }
                    }
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
          }
     }

     public Object[][] getFromVector()
     {
          return dataModel.getFromVector();     
     }

     public String getDeptCode(int i)                       // 15,16
     {
          Vector    VCurVector     = dataModel.getCurVector(i);
          String    str            = (String)VCurVector.elementAt(17);
          int       iid            = VDept.indexOf(str);
          return (iid==-1 ?"0":(String)VDeptCode.elementAt(iid));
     }

     public String getGroupCode(int i)                     // 16,17
     {
          Vector    VCurVector     = dataModel.getCurVector(i);
          String    str            = (String)VCurVector.elementAt(18);
          int       iid            = VGroup.indexOf(str);
          return (iid==-1?"0":(String)VGroupCode.elementAt(iid));
     }

     public String getUnitCode(int i)                     //  18,19
     {
          Vector    VCurVector     = dataModel.getCurVector(i);
          String    str            = (String)VCurVector.elementAt(20);
          int       iid            = VUnit.indexOf(str);
          return (iid==-1?"0":(String)VUnitCode.elementAt(iid));
     }

     public String getBlockCode(int i)                     //  3,2
     {
          Vector    VCurVector     = dataModel.getCurVector(i);
          String    str            = (String)VCurVector.elementAt(3);
          int       iid            = VBlockName.indexOf(str);
          return (iid==-1?"0":(String)VBlock.elementAt(iid));
     }

     public String getDueDate(int i,DateField2 TDate)      //  17,18
     {
          Vector VCurVector = dataModel.getCurVector(i);
          try
          {
               String str = (String)VCurVector.elementAt(19);
               if((str.trim()).length()==0)
                    return TDate.toNormal();
               else
                    return common.pureDate(str);
          }
          catch(Exception ex)
          {
               return " ";
          }
     }
     
     public String getTaxClaim(int i)      //  3
     {
          Vector    VCurVector     = dataModel.getCurVector(i);
          String    str            = (String)VCurVector.elementAt(2);
          int       iid            = VTaxClaim.indexOf(str);
          return (iid==-1?"0":String.valueOf(iid));
     }
     
     public void getDeptGroupUnit()
     {
          ResultSet result = null;

          VDept          = new Vector();
          VGroup         = new Vector();
     
          VDeptCode      = new Vector();
          VGroupCode     = new Vector();
     
          VUnit          = new Vector();
          VUnitCode      = new Vector();
                             
          VBlock         = new Vector();
          VBlockName     = new Vector();


          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               String QS1 = "";
               String QS2 = "";
               String QS3 = "";
               String QS4 = "";
               String QS5 = "";
               
               QS1 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               QS2 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               QS3 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";
               QS4 = " Select Block,BlockName From OrdBlock Order By 1";
               QS5 = " Select distinct Authentication from EPCGRequestOrder where MillCode="+iMillCode+" and EPCGRequestOrder.ReferenceNo = "+SMiReferenceNo;

               result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VDept     . addElement(result.getString(1));
                    VDeptCode . addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VGroup    . addElement(result.getString(1));
                    VGroupCode. addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery(QS3);
               while(result.next())
               {
                    VUnit     . addElement(result.getString(1));
                    VUnitCode . addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery(QS4);
               while(result.next())
               {
                    VBlock    . addElement(result.getString(1));
                    VBlockName. addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery(QS5);
               while(result.next())
               {
                    iTaxAuth  = common.toInt(common.parseNull(result.getString(1)));
               }
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept,Group & Unit :"+ex);
          }
     }

     private void setDescMouse(int iItemStatus)
     {
          try
          {
               EPCGDescMouse MP   = new EPCGDescMouse(DeskTop,VDesc,VMake,VDraw,VCatl,ReportTable,dataModel,iItemStatus);
               DeskTop        . add(MP);
               DeskTop        . repaint();
               MP             . setSelected(true);
               DeskTop        . updateUI();
               MP             . show();
          }
          catch(java.beans.PropertyVetoException ex){ex.printStackTrace();}
     }

     private void setStationaryFrame()
     {
          EPCGStationaryPropertiesFrame  SPF   = new EPCGStationaryPropertiesFrame(DeskTop,ReportTable,dataModel,VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo);
          try
          {
               DeskTop   . add(SPF);
               DeskTop   . repaint();
               SPF       . setSelected(true);
               DeskTop   . updateUI();
               SPF       . show();
          }
          catch(java.beans.PropertyVetoException ex){ex.printStackTrace();}
     }

     public class StationaryList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    int       i         = ReportTable.getSelectedRow();
                    String    SItem_Code= (String)ReportTable.getModel().getValueAt(i,0);

                    if(isStationary(SItem_Code))
                         setStationaryFrame();
               }    
          }
     }

     public class DescList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    if(!bflag)
                    {
                              setDescMouse(0);
                    }
                    else
                    {
                         int i          = ReportTable.getSelectedRow();
                         int iItemStatus= common.toInt((String)VItemsStatus.elementAt(i));
                         setDescMouse(iItemStatus);
                    }
               }    
          }
     }

     public boolean isStationary(String SItemCode)
     {
          String SStkGroupCode     = "";
          String QS                = "";

          if(iMillCode==0)
          {
               QS =    " select stkgroupcode,invitems.item_code from invitems"+
                       " where invitems.item_code = '"+SItemCode+"'";
          }
          else
          {
               QS =    " select stkgroupcode,"+SItemTable+".item_code from "+SItemTable+""+
                       " where "+SItemTable+".item_code = '"+SItemCode+"'";
          }
          try
          {
               ORAConnection  oraConnection = ORAConnection.getORAConnection();
               Connection     theConnection = oraConnection.getConnection();
                              theConnection . setAutoCommit(true);
               Statement      stat          = theConnection.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result         . next())
               {
                    SStkGroupCode  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          if(SStkGroupCode.equals("B01"))
               return true;

          return false;
     }
}

