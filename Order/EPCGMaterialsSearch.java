package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class EPCGMaterialsSearch implements ActionListener
{
     JLayeredPane        Layer;
     Vector              VName,VCode;
     Vector              VNameCode;
     String              SName,SCode;
     EPCGOrderMiddlePanel    MiddlePanel;
     
     JList               BrowList,SelectedList;
     JScrollPane         BrowScroll,SelectedScroll;
     JTextField          TIndicator;
     JButton             BOk;
     JPanel              LeftPanel,RightPanel;
     JInternalFrame      MaterialFrame;
     JPanel              MFMPanel,MFBPanel;
     Vector              VSelectedName,VSelectedCode;
     Vector              VSelectedNameCode;
     String              SFrameTitle,SWhere;
     String              SItemTable;
     String              str       = "";
     int                 iMFSig    = 0;
     ActionEvent         ae;
     Common              common    = new Common();
     int                 iMillCode = 0;
     Vector              VODesc,VOMake,VODraw,VOCatl,VOPColour,VOPSet,VOPSize,VOPSide,VOPSlipFrNo,VOPSlipToNo,VOPBookFrNo,VOPBookToNo;
     String              SCatl     = "",SDraw     = "",SPColour  = "",SPSet     = "",SPSize    = "",SPSide    = "",SPSlipNo  = "",SPBookNo     = "";

     public EPCGMaterialsSearch(JLayeredPane Layer,Vector VCode,Vector VName,EPCGOrderMiddlePanel MiddlePanel,String SFrameTitle,String SWhere,int iMillCode,String SItemTable)
     {
          this.Layer          = Layer;
          this.VName          = VName;
          this.VCode          = VCode;
          this.MiddlePanel    = MiddlePanel;
          this.SFrameTitle    = SFrameTitle;
          this.SWhere         = SWhere;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;

          VODesc              = new Vector();
          VOMake              = new Vector();
          VODraw              = new Vector();
          VOCatl              = new Vector();
          VOPColour           = new Vector();
          VOPSet              = new Vector();
          VOPSize             = new Vector();
          VOPSide             = new Vector();
          VOPSlipFrNo         = new Vector();
          VOPSlipToNo         = new Vector();
          VOPBookFrNo         = new Vector();
          VOPBookToNo         = new Vector();

          createComponents();
     }

     public void createComponents()
     {
          VNameCode     = new Vector();
          for (int i=0;i<VCode.size();i++)
          {
               SName     = (String)VName.elementAt(i);
               SCode     = (String)VCode.elementAt(i);
               VNameCode . addElement(common.Pad(SName,40)+" ("+SCode+")");
          }

          BrowList       = new JList(VNameCode);
          BrowList       . setFont(new Font("monospaced", Font.PLAIN, 11));
          SelectedList   = new JList();
          BrowScroll     = new JScrollPane(BrowList);
          SelectedScroll = new JScrollPane(SelectedList);
          LeftPanel      = new JPanel(true);
          RightPanel     = new JPanel(true);
          TIndicator     = new JTextField();
          BOk            = new JButton("Selection Over");
          TIndicator     . setEditable(false);
          MFMPanel       = new JPanel(true);
          MFBPanel       = new JPanel(true);
          MaterialFrame  = new JInternalFrame(SFrameTitle);
          MaterialFrame  . show();
          MaterialFrame  . setBounds(50,50,650,350);
          MaterialFrame  . setClosable(true);
          MaterialFrame  . setResizable(true);
          BrowList       . addKeyListener(new KeyList());
          BrowList       . requestFocus();
     }
     
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent e)
          {
               if(VSelectedCode.size()==0)
               {
                    JOptionPane.showMessageDialog(null,"No Material is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                    BrowList.requestFocus();
                    return;
               }
               BOk.setEnabled(false);
               setMiddlePanel();
               removeHelpFrame();
               ((JButton)ae.getSource()).setEnabled(false);
               if(SWhere.equals("Order"))
               {
                    MiddlePanel.MiddlePanel.ReportTable.requestFocus();
               }
               str="";
          }
     }

     public void actionPerformed(ActionEvent ae)
     {
          this.ae = ae;
          VSelectedName       = new Vector();
          VSelectedCode       = new Vector();
          VSelectedNameCode   = new Vector();
          TIndicator          . setText(str);
          BOk                 . setEnabled(true);

          if(iMFSig==0)
          {
               MFMPanel       . setLayout(new GridLayout(1,2));
               MFBPanel       . setLayout(new GridLayout(1,2));
               MFMPanel       . add(BrowScroll);
               MFMPanel       . add(SelectedScroll);
               MFBPanel       . add(TIndicator);
               MFBPanel       . add(BOk);
               BOk            . addActionListener(new ActList());
               MaterialFrame  . getContentPane().add("Center",MFMPanel);
               MaterialFrame  . getContentPane().add("South",MFBPanel);
               iMFSig         = 1;
          }
          removeHelpFrame();
          try
          {
               Layer          . add(MaterialFrame);
               MaterialFrame  . moveToFront();
               MaterialFrame  . setSelected(true);
               MaterialFrame  . show();
               BrowList       . requestFocus();
               Layer          . repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
     }
     
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar  = Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex)
               {
                    Toolkit.getDefaultToolkit().beep();
               }
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==116)    // F5 is pressed
               {
                    setDataIntoVector();
                    BrowList.setListData(VNameCode);
               }
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int       index          = BrowList.getSelectedIndex();
                    String    SMatNameCode   = (String)VNameCode . elementAt(index);
                    String    SMatName       = (String)VName     . elementAt(index);
                    String    SMatCode       = (String)VCode     . elementAt(index);
                    addMatDet(SMatName,SMatCode,SMatNameCode);
                    str="";
                    TIndicator.setText(str);
               }
               if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
               {
                    setMiddlePanel();
                    removeHelpFrame();
                    ((JButton)ae.getSource()).setEnabled(false);
                    if(SWhere.equals("Order"))
                    {
                         MiddlePanel.MiddlePanel.ReportTable.requestFocus();
                    }
                    str="";
               }
          }
     }

     public void setCursor()
     {
          int index = 0;
          TIndicator. setText(str);
          for(index=0;index<VNameCode.size();index++)
          {
               String str1 = ((String)VNameCode.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
          if(index >= VNameCode.size())
               Toolkit.getDefaultToolkit().beep();
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(MaterialFrame);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

     public boolean addMatDet(String SMatName,String SMatCode,String SMatNameCode)
     {
          int iIndex     = VSelectedCode.indexOf(SMatCode);
          if (iIndex==-1)
          {
               VSelectedName       . addElement(SMatName);
               VSelectedCode       . addElement(SMatCode);
               VSelectedNameCode   . addElement(SMatNameCode);

               setStationaryData(SMatCode);

               VODesc              . addElement("");
               VOMake              . addElement("");
               VODraw              . addElement(SDraw);
               VOCatl              . addElement(SCatl);
               VOPColour           . addElement(SPColour);
               VOPSet              . addElement(SPSet);
               VOPSize             . addElement(SPSize);
               VOPSide             . addElement(SPSide);

               if(common.toInt(SPSlipNo)==0)
               {
                    VOPSlipFrNo         . addElement(String.valueOf(common.toInt(SPSlipNo)));
                    VOPSlipToNo         . addElement(String.valueOf(common.toInt(SPSlipNo)));
               }
               else
               {
                    VOPSlipFrNo         . addElement(String.valueOf(common.toInt(SPSlipNo)+1));
                    VOPSlipToNo         . addElement(String.valueOf(common.toInt(SPSlipNo)+1));
               }

               if(common.toInt(SPBookNo)==0)
               {
                    VOPBookFrNo         . addElement(String.valueOf(common.toInt(SPBookNo)));
                    VOPBookToNo         . addElement(String.valueOf(common.toInt(SPBookNo)));
               }
               else
               {
                    VOPBookFrNo         . addElement(String.valueOf(common.toInt(SPBookNo)+1));
                    VOPBookToNo         . addElement(String.valueOf(common.toInt(SPBookNo)+1));
               }
          }
          else                     
          {
               VSelectedName       . removeElementAt(iIndex);
               VSelectedCode       . removeElementAt(iIndex);
               VSelectedNameCode   . removeElementAt(iIndex);

               VODesc              . removeElementAt(iIndex);
               VOMake              . removeElementAt(iIndex);
               VODraw              . removeElementAt(iIndex);
               VOCatl              . removeElementAt(iIndex);
               VOPColour           . removeElementAt(iIndex);
               VOPSet              . removeElementAt(iIndex);
               VOPSize             . removeElementAt(iIndex);
               VOPSide             . removeElementAt(iIndex);
               VOPSlipFrNo         . removeElementAt(iIndex);
               VOPSlipToNo         . removeElementAt(iIndex);
               VOPBookFrNo         . removeElementAt(iIndex);
               VOPBookToNo         . removeElementAt(iIndex);
          }
          SelectedList.setListData(VSelectedNameCode);

          return true;
     }

     public void setMiddlePanel()
     {
          if(SWhere.equals("Order"))
          {
               MiddlePanel    . setRowData(VSelectedCode,VSelectedName);
               MiddlePanel    . createComponents();

               for(int i = 0;i<VODesc.size();i++)
               {
                    MiddlePanel.MiddlePanel.VDesc          . addElement(VODesc      . elementAt(i));
                    MiddlePanel.MiddlePanel.VMake          . addElement(VOMake      . elementAt(i));
                    MiddlePanel.MiddlePanel.VDraw          . addElement(VODraw      . elementAt(i));
                    MiddlePanel.MiddlePanel.VCatl          . addElement(VOCatl      . elementAt(i));
                    MiddlePanel.MiddlePanel.VPColour       . addElement(VOPColour   . elementAt(i));
                    MiddlePanel.MiddlePanel.VPSet          . addElement(VOPSet      . elementAt(i));
                    MiddlePanel.MiddlePanel.VPSize         . addElement(VOPSize     . elementAt(i));
                    MiddlePanel.MiddlePanel.VPSide         . addElement(VOPSide     . elementAt(i));
                    MiddlePanel.MiddlePanel.VPSlipFrNo     . addElement(VOPSlipFrNo . elementAt(i));
                    MiddlePanel.MiddlePanel.VPSlipToNo     . addElement(VOPSlipToNo . elementAt(i));
                    MiddlePanel.MiddlePanel.VPBookFrNo     . addElement(VOPBookFrNo . elementAt(i));
                    MiddlePanel.MiddlePanel.VPBookToNo     . addElement(VOPBookToNo . elementAt(i));
               }
          }
     }

     public void setDataIntoVector()
     {
          VName     . removeAllElements();
          VCode     . removeAllElements();
          VNameCode . removeAllElements();
          
          String QString = "";
          if(iMillCode==0)
          {
               QString = " Select Item_Name,Item_Code From InvItems Order By Item_Name";
          }
          else
          {
               QString = " Select Item_Name,"+SItemTable+".Item_Code From "+SItemTable+""+
                         " Inner Join InvItems on InvItems.Item_Code = "+SItemTable+".Item_Code"+
                         " Order By Item_Name";
          }

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               ResultSet res = stat.executeQuery(QString);
               while(res.next())
               {
                    String str1    = res.getString(1);
                    String str2    = res.getString(2);
                         VName     . addElement(str1);
                         VCode     . addElement(str2);
                         VNameCode . addElement(str1+" (Code : "+str2+")");
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setStationaryData(String SMatCode)
     {
          String  QString = "";
          if(iMillCode==0)
          {
               QString = "Select Catl,Draw,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE,LASTSLIPNO,LASTBOOKNO From invitems Where Item_Code='"+SMatCode+"'";
          }
          else
          {
               QString = " Select invitems.Catl,invitems.Draw,invitems.PAPERCOLOR,invitems.PAPERSETS,"+
                         " invitems.PAPERSIZE,invitems.PAPERSIDE,"+SItemTable+".LASTSLIPNO,"+
                         " "+SItemTable+".LASTBOOKNO"+
                         " From "+SItemTable+""+
                         " inner join invitems on invitems.item_code = "+SItemTable+".item_code"+
                         " where "+SItemTable+".item_code='"+SMatCode+"'";
          }
          try
          {
               ORAConnection  jdbc           = ORAConnection.getORAConnection();
               Connection     theConnection  = jdbc.getConnection();
               Statement      theStatement   = theConnection.createStatement();
               ResultSet      result         = theStatement.executeQuery(QString);

               while(result.next())
               {
                    SCatl     = common.parseNull((String)result.getString(1));
                    SDraw     = common.parseNull((String)result.getString(2));
                    SPColour  = common.parseNull((String)result.getString(3));
                    SPSet     = common.parseNull((String)result.getString(4));
                    SPSize    = common.parseNull((String)result.getString(5));
                    SPSide    = common.parseNull((String)result.getString(6));
                    SPSlipNo  = common.parseNull((String)result.getString(7));
                    SPBookNo  = common.parseNull((String)result.getString(8));
               }
               result         . close();
               theStatement   . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}
