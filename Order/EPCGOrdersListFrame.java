package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class EPCGOrdersListFrame extends JInternalFrame
{
     JPanel         TopPanel;
     JPanel         ControlPanel;
     JPanel         ListPanel;
     
     String         SStDate,SEnDate;
     JComboBox      JCOrder,JCFilter,JCConcern,JCUnit,JCShowList;
     JButton        BApply;

     Vector         VCCode,VCName,VUCode,VUName;
     Vector         VOrderQty,VOdInVQty;

     DateField2     TStDate;
     DateField2     TEnDate;

     Object         RowData[][];
     String         ColumnData[] = {"Sl.No.", "Reference No", "Proforma No","Date","Block","Supplier","Mrs No","Code","Name","Dept","Group","Unit","Concern","Qty","Rate","Amount","Due Date","Status"};
     String         ColumnType[] = {"N"     , "N"           , "S"          ,"S"   ,"S"    ,"S"       ,"N"     ,"S"   ,"S"   ,"S"   ,"S"    ,"S"   ,"S"      ,"N"  ,"N"   ,"N"     ,"S"       ,"S"     };

     JLayeredPane   DeskTop;
     Vector         VCode,VName,VNameCode;
     StatusPanel    SPanel;
     int            iUserCode,iMillCode,iAuthCode;
     String         SYearCode;
     String         SItemTable,SSupTable;

     Common         common = new Common();

     Vector         VOrdDate,VBlock,VBlockCode,VOrdNo,VProformaNo,VMrsNo,VSupName,VOrdCode,VOrdName,VOrdDeptName,VOrdCataName,VOrdUnitName,VOrdConcern,VOrdQty,VOrdRate,VOrdNet,VOrdDue,VStatus;
     Vector         VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup;

     TabReport      tabreport;
     int            iOrderStatus  = 0;
     Vector         VItemsStatus;

     public EPCGOrdersListFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode,String SYearCode,String SItemTable,String SSupTable)
     {
          super("EPCG Orders List");

          this.DeskTop    = DeskTop;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.iAuthCode  = iAuthCode;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          
          setConcernUnit();

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel       = new JPanel();
          ControlPanel   = new JPanel();
          ListPanel      = new JPanel();

          TStDate        = new DateField2();
          TEnDate        = new DateField2();
          BApply         = new JButton("Apply");
          JCOrder        = new JComboBox();
          JCFilter       = new JComboBox();
          JCConcern      = new JComboBox(VCName);
          JCUnit         = new JComboBox(VUName);

          TStDate        . setTodayDate();
          TEnDate        . setTodayDate();

          JCOrder        . addItem("Reference No");
          JCOrder        . addItem("Materialwise");
          JCOrder        . addItem("Supplierwise");
          JCOrder        . addItem("Departmentwise");
          JCOrder        . addItem("Groupwise");
          
          JCFilter       . addItem("All");
          JCFilter       . addItem("Over-Due");
          JCFilter       . addItem("All Pendings");
          JCFilter       . addItem("Before Over-Due");

          JCShowList     = new JComboBox();
          JCShowList     . addItem("All");
          JCShowList     . addItem("EPCG Orders Through Direct Order");
          JCShowList     . addItem("Direct EPCG Orders");

          JCShowList     . setToolTipText((String)JCShowList.getSelectedItem());
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);

          TopPanel            . setLayout(new GridLayout(2,1));
          ControlPanel        . setLayout(new GridLayout(1,7));
          ListPanel           . setLayout(new GridLayout(1,8));

          ControlPanel        . setBorder(new TitledBorder("Control"));
          ListPanel           . setBorder(new TitledBorder("List"));
     }

     public void addComponents()
     {
          ListPanel           . add(new JLabel("List Only", JLabel.CENTER));
          ListPanel           . add(JCFilter);
          
          ListPanel           . add(new JLabel("Concern", JLabel.CENTER));
          ListPanel           . add(JCConcern);
          
          ListPanel           . add(new JLabel("Unit", JLabel.CENTER));
          ListPanel           . add(JCUnit);

          ListPanel           . add(new JLabel("List Type", JLabel.CENTER));
          ListPanel           . add(JCShowList);

          ControlPanel        . add(new JLabel("Sorted On", JLabel.CENTER));
          ControlPanel        . add(JCOrder);
          
          ControlPanel        . add(new JLabel("Period", JLabel.CENTER));
          ControlPanel        . add(TStDate);

          ControlPanel        . add(TEnDate);

          ControlPanel        . add(new JLabel("", JLabel.CENTER));
          ControlPanel        . add(BApply);
          
          TopPanel            . add(ListPanel);
          TopPanel            . add(ControlPanel);

          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
     }
     
     public void addListeners()
     {
          BApply              . addActionListener(new ApplyList());

          JCShowList          . addItemListener(new ApplyList());
     }

     public class ApplyList implements ActionListener, ItemListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               ApplyData();
          }

          public void itemStateChanged(ItemEvent ie)
          {
               JCShowList     . setToolTipText((String)JCShowList.getSelectedItem());
          }
     }

     private void ApplyData()
     {
          setDataIntoVector();
          setRowData();

          try
          {
               getContentPane().remove(tabreport);
          }
          catch(Exception ex){}
          
          try
          {
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               getContentPane().add(tabreport,BorderLayout.CENTER);
               
               setSelected(true);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setDataIntoVector()
     {
          VOrdDate            = new Vector();
          VOrdNo              = new Vector();
          VProformaNo         = new Vector();
          VBlock              = new Vector();
          VBlockCode          = new Vector();
          VMrsNo              = new Vector();
          VSupName            = new Vector();
          VOrdCode            = new Vector();
          VOrdName            = new Vector();
          VOrdDeptName        = new Vector();
          VOrdCataName        = new Vector();
          VOrdConcern         = new Vector();
          VOrdUnitName        = new Vector();
          VOrdQty             = new Vector();
          VOrdRate            = new Vector();
          VOrdNet             = new Vector();
          VOrdDue             = new Vector();
          VStatus             = new Vector();
          
          SStDate             = TStDate.toString();
          SEnDate             = TEnDate.toString();

          String StDate       = TStDate.toNormal();
          String EnDate       = TEnDate.toNormal();
          String SShowList    = (String)JCShowList.getSelectedItem();

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();

               String         QString        = getQString(StDate, EnDate, SShowList);
               ResultSet      res            = stat.executeQuery(QString);

               while (res.next())
               {
                    String SConcern= "";
                    
                    String str1    = res.getString(1);
                    String str2    = res.getString(2);
                    String str3    = res.getString(3);
                    String str4    = res.getString(4);
                    String str5    = res.getString(5);
                    String str6    = res.getString(6);
                    String str7    = res.getString(7);
                    String str8    = res.getString(8);
                    String str9    = res.getString(9);
                    String str10   = res.getString(10);
                    String str11   = res.getString(11);
                    String str12   = res.getString(12);
                    String str13   = res.getString(13);
                    String str14   = res.getString(14);
                    String str15   = res.getString(15);
                    String str16   = common.parseNull(res.getString(16));
                    str2           = common.parseDate(str2);
                    str14          = common.parseDate(str14);
                    
                    SConcern       = (String)JCConcern.getSelectedItem();

                    VOrdNo         . addElement(str1);
                    VOrdDate       . addElement(str2);
                    VBlock         . addElement(str3);
                    VSupName       . addElement(str4);
                    VMrsNo         . addElement(str5);
                    VOrdCode       . addElement(str6);
                    VOrdName       . addElement(str7);
                    VOrdDeptName   . addElement(common.parseNull(str8));
                    VOrdCataName   . addElement(common.parseNull(str9));
                    VOrdUnitName   . addElement(common.parseNull(str10));
                    VOrdQty        . addElement(str11);
                    VOrdRate       . addElement(str12);
                    VOrdNet        . addElement(str13);
                    VOrdDue        . addElement(str14);
                    VBlockCode     . addElement(str15);
                    VOrdConcern    . addElement(SConcern);
                    VStatus        . addElement(" ");
                    VProformaNo    . addElement(common.parseNull(res.getString(19)));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VOrdDate.size()][18];
          for(int i=0;i<VOrdDate.size();i++)
          {
               RowData[i][0]  = String.valueOf(i+1);
               RowData[i][1]  = (String)VOrdNo         .elementAt(i);
               RowData[i][2]  = (String)VProformaNo    .elementAt(i);
               RowData[i][3]  = (String)VOrdDate       .elementAt(i);
               RowData[i][4]  = (String)VBlock         .elementAt(i);
               RowData[i][5]  = (String)VSupName       .elementAt(i);
               RowData[i][6]  = (String)VMrsNo         .elementAt(i);
               RowData[i][7]  = (String)VOrdCode       .elementAt(i);
               RowData[i][8]  = (String)VOrdName       .elementAt(i);
               RowData[i][9]  = (String)VOrdDeptName   .elementAt(i);
               RowData[i][10] = (String)VOrdCataName   .elementAt(i);
               RowData[i][11] = (String)VOrdUnitName   .elementAt(i);
               RowData[i][12] = (String)VOrdConcern    .elementAt(i);
               RowData[i][13] = (String)VOrdQty        .elementAt(i);
               RowData[i][14] = (String)VOrdRate       .elementAt(i);
               RowData[i][15] = (String)VOrdNet        .elementAt(i);
               RowData[i][16] = (String)VOrdDue        .elementAt(i);
               RowData[i][17] = (String)VStatus        .elementAt(i);
          }
     }

     public String getQString(String StDate, String EnDate, String SShowList)
     {
          DateField2 df  = new DateField2();

                    df   . setTodayDate();

          String SToday  = df.TYear.getText()+df.TMonth.getText()+df.TDay.getText();
          String SUnit   = (String)VUCode.elementAt(JCUnit.getSelectedIndex());

          String QString = "";

          if(SShowList.equals("Direct EPCG Orders") || SShowList.equals("All"))
          {
               QString  =     " with temp1 as (SELECT EPCGRequestOrder.ReferenceNo as ReferenceNo, EPCGRequestOrder.OrderDate,"+
                              " OrdBlock.BlockName, "+SSupTable+".Name, EPCGRequestOrder.MrsNo, EPCGRequestOrder.Item_Code,"+
                              " InvItems.Item_Name, EPCGRequestOrder.Qty, EPCGRequestOrder.Rate,"+
                              " EPCGRequestOrder.Net, EPCGRequestOrder.DueDate, EPCGRequestOrder.InvQty as InvQty,"+
                              " Dept.Dept_Name, Cata.Group_Name, EPCGRequestOrder.Unit_Code, Unit.Unit_Name,"+
                              " EPCGRequestOrder.OrderBlock,EPCGRequestOrder.MrsSlNo as SlNo,EPCGRequestOrder.MillCode,EPCGRequestOrder.ProformaNo from"+
     
                              " (((((EPCGRequestOrder INNER JOIN OrdBlock ON EPCGRequestOrder.OrderBlock=OrdBlock.Block)"+
                              " INNER JOIN "+SSupTable+" ON EPCGRequestOrder.Sup_Code="+SSupTable+".Ac_Code)"+
                              " INNER JOIN InvItems ON EPCGRequestOrder.Item_Code=InvItems.Item_Code)"+
                              " INNER JOIN Dept ON EPCGRequestOrder.Dept_Code=Dept.Dept_Code)"+
                              " INNER JOIN Cata ON EPCGRequestOrder.Group_Code=Cata.Group_Code)"+
                              " INNER JOIN Unit ON EPCGRequestOrder.Unit_Code=Unit.Unit_Code"+
     
                              " Where EPCGRequestOrder.Qty > 0 and EPCGRequestOrder.Authentication = 0 ),"+
     
                              " temp2 as (SELECT InvItems.Item_Name, MRS.MrsNo,"+
                              " MRS.Item_Code,MRS.SlNo,MRS.MillCode FROM MRS"+
                              " INNER JOIN InvItems ON MRS.Item_Code = InvItems.Item_Code)"+
     
                              " SELECT temp1.ReferenceNo, temp1.OrderDate, temp1.BlockName, temp1.Name,"+
                              " temp1.MrsNo,temp1.Item_Code, temp1.Item_Name, temp1.Dept_Name,"+
                              " temp1.Group_Name,temp1.Unit_Name, temp1.Qty, temp1.Rate, temp1.Net,"+
                              " temp1.DueDate,temp1.OrderBlock,temp1.MillCode,temp1.Unit_Code,temp1.InvQty,temp1.ProformaNo FROM temp1"+
                              " LEFT JOIN temp2 ON (temp1.MrsNo = temp2.MrsNo) AND"+
                              " (temp1.Item_Code = temp2.Item_Code) AND"+
                              " (temp1.SlNo = temp2.SlNo)"+
                              " Where temp1.OrderDate >= '"+StDate+"' and temp1.OrderDate <='"+EnDate+"' ";
     
               if(JCFilter.getSelectedIndex()==1)
                    QString = QString+" and temp1.InvQty = 0 and temp1.DueDate <= '"+SToday+"'";
     
               if(JCFilter.getSelectedIndex()==2)
                    QString = QString+" and temp1.InvQty = 0";
     
               if(JCFilter.getSelectedIndex()==3)
                    QString = QString+" and temp1.InvQty > 0 and temp1.DueDate >= '"+SToday+"'";
               /*
                    if(JCConcern.getSelectedIndex()>0)
                         QString = QString+" and temp1.MillCode="+iMillCode;
               */

               if(JCConcern.getSelectedIndex() > 0)
                    QString = QString+" and temp1.MillCode="+getMillCode();
     
               if(JCUnit.getSelectedIndex()>0)
                    QString = QString+" and temp1.Unit_Code="+SUnit;
     
               if(JCOrder.getSelectedIndex() == 0)
                    QString = QString+" Order By temp1.ReferenceNo,temp1.BlockName,temp1.OrderDate";
               if(JCOrder.getSelectedIndex() == 1)      
                    QString = QString+" Order By temp1.Item_Name,temp1.OrderDate";
               if(JCOrder.getSelectedIndex() == 2)      
                    QString = QString+" Order By temp1.Name,temp1.OrderDate";
               if(JCOrder.getSelectedIndex() == 3)      
                    QString = QString+" Order By temp1.Dept_Name,temp1.OrderDate";
               if(JCOrder.getSelectedIndex() == 4)
                    QString = QString+" Order By temp1.Group_Name,temp1.OrderDate";
          }

          if(SShowList.equals("All"))
          {
               QString      = QString + " Union All ";
          }

          if(SShowList.equals("EPCG Orders Through Direct Order") || SShowList.equals("All"))
          {
               QString  =     " with temp1 as (SELECT '' as ReferenceNo, PurchaseOrder.OrderDate,"+
                              " OrdBlock.BlockName, "+SSupTable+".Name, PurchaseOrder.MrsNo, PurchaseOrder.Item_Code,"+
                              " InvItems.Item_Name, PurchaseOrder.Qty, PurchaseOrder.Rate,"+
                              " PurchaseOrder.Net, PurchaseOrder.DueDate, PurchaseOrder.InvQty as InvQty,"+
                              " Dept.Dept_Name, Cata.Group_Name, PurchaseOrder.Unit_Code, Unit.Unit_Name,"+
                              " PurchaseOrder.OrderBlock,PurchaseOrder.MrsSlNo as SlNo,PurchaseOrder.MillCode,'' as ProformaNo from"+
     
                              " (((((PurchaseOrder INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block)"+
                              " INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code)"+
                              " INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code)"+
                              " INNER JOIN Dept ON PurchaseOrder.Dept_Code=Dept.Dept_Code)"+
                              " INNER JOIN Cata ON PurchaseOrder.Group_Code=Cata.Group_Code)"+
                              " INNER JOIN Unit ON PurchaseOrder.Unit_Code=Unit.Unit_Code"+
     
                              " Where PurchaseOrder.Qty > 0 and PurchaseOrder.EPCG = 1),"+ //PurchaseOrder.Authentication = 0
     
                              " temp2 as (SELECT InvItems.Item_Name, MRS.MrsNo,"+
                              " MRS.Item_Code,MRS.SlNo,MRS.MillCode FROM MRS"+
                              " INNER JOIN InvItems ON MRS.Item_Code = InvItems.Item_Code)"+
     
                              " SELECT temp1.ReferenceNo, temp1.OrderDate, temp1.BlockName, temp1.Name,"+
                              " temp1.MrsNo,temp1.Item_Code, temp1.Item_Name, temp1.Dept_Name,"+
                              " temp1.Group_Name,temp1.Unit_Name, temp1.Qty, temp1.Rate, temp1.Net,"+
                              " temp1.DueDate,temp1.OrderBlock,temp1.MillCode,temp1.Unit_Code,temp1.InvQty,temp1.ProformaNo FROM temp1"+
                              " LEFT JOIN temp2 ON (temp1.MrsNo = temp2.MrsNo) AND"+
                              " (temp1.Item_Code = temp2.Item_Code) AND"+
                              " (temp1.SlNo = temp2.SlNo)"+
                              " Where temp1.OrderDate >= '"+StDate+"' and temp1.OrderDate <='"+EnDate+"' ";
     
               if(JCFilter.getSelectedIndex()==1)
                    QString = QString+" and temp1.InvQty = 0 and temp1.DueDate <= '"+SToday+"'";
     
               if(JCFilter.getSelectedIndex()==2)
                    QString = QString+" and temp1.InvQty = 0";
     
               if(JCFilter.getSelectedIndex()==3)
                    QString = QString+" and temp1.InvQty > 0 and temp1.DueDate >= '"+SToday+"'";

               /*
                    if(JCConcern.getSelectedIndex()>0)
                         QString = QString+" and temp1.MillCode="+iMillCode;
               */

               if(JCConcern.getSelectedIndex()>0)
                    QString = QString+" and temp1.MillCode="+getMillCode();

               if(JCUnit.getSelectedIndex()>0)
                    QString = QString+" and temp1.Unit_Code="+SUnit;
     
               if(JCOrder.getSelectedIndex() == 0)
                    QString = QString+" Order By temp1.ReferenceNo,temp1.BlockName,temp1.OrderDate";
               if(JCOrder.getSelectedIndex() == 1)      
                    QString = QString+" Order By temp1.Item_Name,temp1.OrderDate";
               if(JCOrder.getSelectedIndex() == 2)      
                    QString = QString+" Order By temp1.Name,temp1.OrderDate";
               if(JCOrder.getSelectedIndex() == 3)      
                    QString = QString+" Order By temp1.Dept_Name,temp1.OrderDate";
               if(JCOrder.getSelectedIndex() == 4)
                    QString = QString+" Order By temp1.Group_Name,temp1.OrderDate";
          }

          if(SShowList.equals("All"))
          {
               QString      = " Select * from ("+QString+")";

               if(JCOrder.getSelectedIndex() == 0)
                    QString = QString+" Order By ReferenceNo,BlockName,OrderDate";
               if(JCOrder.getSelectedIndex() == 1)      
                    QString = QString+" Order By Item_Name,OrderDate";
               if(JCOrder.getSelectedIndex() == 2)      
                    QString = QString+" Order By Name,OrderDate";
               if(JCOrder.getSelectedIndex() == 3)      
                    QString = QString+" Order By Dept_Name,OrderDate";
               if(JCOrder.getSelectedIndex() == 4)
                    QString = QString+" Order By Group_Name,OrderDate";
          }

          return QString;
     }

     private void setConcernUnit()
     {
          VCName    = new Vector();
          VCCode    = new Vector();
          
          VUName    = new Vector();
          VUCode    = new Vector();
          
          VCCode    . addElement("2");
          VCName    . addElement("All");
          
          VUCode    . addElement("99");
          VUName    . addElement("All");
          
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               ResultSet      result        =  stat.executeQuery("Select MillCode,MillName From  Mill  Order By 1");

               while(result.next())
               {
                    VCCode    . addElement(result.getString(1));
                    VCName    . addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               System.exit(0);
          }
          
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               String QS1 = "";
               QS1 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";

               ResultSet result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VUName    . addElement(result.getString(1));
                    VUCode    . addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("7"+ex);
          }
     }

     private int getMillCode()
     {
          try
          {
               return Integer.parseInt((String)VCCode.elementAt(VCName.indexOf((String)JCConcern.getSelectedItem())));
          }
          catch(Exception ex)
          {
               return -1;
          }
     }
}
