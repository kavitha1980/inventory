/*
   Order Placement Utility for Materials not requested thro' MRS.
   Simply,this utility is to  handle order without any plan. 
   Hence OrderPlanMiddlePanel and PlanMiddle is replaced with a
   generalized EPCGOrderMiddlePanel component.

   This will also act like an Ammendment component for a placed
   purchase Order
*/

package Order;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.util.Date;

import util.*;
import guiutil.*;
import jdbc.*;

public class EPCGRequestFrame extends JInternalFrame
{
     JPanel                   TopPanel,BottomPanel;
     EPCGOrderMiddlePanel     MiddlePanel;
     Connection               theConnect = null;
                         
     JButton             BOk,BCancel,BSupplier,BMaterials;
     NextField           TOrderNo,TPayDays,TReferenceNo;
     JTextField          TSupCode,TRef,TProformaNo;
     JTextField          TAdvance,TPayTerm;
     DateField2          TDate;
     JComboBox           JCTo,JCThro,JCForm,JEPCG,JCOrderType,JCProject,JCState,JCPort,JCCurrencyType;

     JLayeredPane        DeskTop;
     Vector              VCode,VName;
     StatusPanel         SPanel;

     Vector              VTo,VThro,VToCode,VThroCode,VFormCode,VForm,VEpcg,
                         VOrderType,VPort,VPortCode,VProject,VState,
                         VBlockName,VBlockCode,VNameCode;

     Vector              VOrderTypeCode, VOrderTypeName, VStateCode, VStateName;
     Vector              VCurrencyTypeCode, VCurrencyTypeName;

     Vector              VDesc,VMake,VDraw,VCatl,VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo;

     FileWriter          FW;

     Common              common      = new Common();
     String              SOrderNo    = "";
     String              SInvOrderNo = "";

     int                 iAmend,iUserCode;
     int                 iMillCode = 0;
     String              SYearCode;
     String              SItemTable,SSupTable;

     // to identify creation of a purchase order or its Ammendment
     // false indicates creation mode      true indicates modification mode     

     boolean             bflag;
     boolean             bComflag = true;

     int                 iMaxSlNo  = 0;

     EPCGRequestListFrame     epcgrequestlistframe;
     
     public EPCGRequestFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,boolean bflag,int iAmend,int iUserCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable)
     {
          this.DeskTop    = DeskTop;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.SPanel     = SPanel;
          this.bflag      = bflag;
          this.iAmend     = iAmend;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();

          setOrderTypeListener();
          getNextReferenceNo();
     }

     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnect     =    oraConnection.getConnection();
     }

     public void createComponents()
     {
          TopPanel            = new JPanel();
          BottomPanel         = new JPanel();
          
          BOk                 = (bflag?new JButton("Update"):new JButton("Okay"));
          BCancel             = new JButton("Abort");
          
          BSupplier           = new JButton("Supplier");
          BMaterials          = new JButton("Select Materials");
          
          TOrderNo            = new NextField();
          TReferenceNo        = new NextField();
          TProformaNo         = new JTextField();

          TDate               = new DateField2();
          TSupCode            = new JTextField();
          TAdvance            = new JTextField();
          TPayTerm            = new JTextField();
          TPayDays            = new NextField();
          TRef                = new JTextField();

          getVTo();
          
          JCTo                = new JComboBox(VTo);
          JCThro              = new JComboBox(VThro);
          JCForm              = new JComboBox(VForm);
          JCPort              = new JComboBox(VPort);
          JEPCG               = new JComboBox();
          JCOrderType         = new JComboBox(VOrderTypeName);

          JCCurrencyType      = new JComboBox(VCurrencyTypeName);

          JCProject           = new JComboBox();
          JCState             = new JComboBox(VStateName);

          String SReferenceNo = TReferenceNo.getText();

          MiddlePanel         = new EPCGOrderMiddlePanel(DeskTop,VCode,VName,VNameCode,iMillCode,bflag,SReferenceNo,SItemTable);
          
          TDate               . setTodayDate();
          TOrderNo            . setEditable(false);
          TReferenceNo        . setEditable(false);
          TPayDays            . setEditable(false);
          TAdvance            . setEditable(false);

          JEPCG               . addItem("Non-EPCG Order   ");
          JEPCG               . addItem("EPCG Order       ");
          
          JCProject           . addItem("Other Projects     ");
          JCProject           . addItem("Machinery Projects ");
          
          if(!bflag)
          {
               JCPort         . setEnabled(false);
          }
          if(bflag)
          {
               TProformaNo    . setEditable(false);
          }
     }

     public void setLayouts()
     {
          if(bflag)
          {
               setTitle("EPCG Order Entry - Updation");
          }
          else
          {
               setTitle("EPCG Order Entry");
          }
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,720,520);

          TopPanel            . setLayout(new GridLayout(9,2,4,4));

          getContentPane()    . setLayout(new BorderLayout());
          BottomPanel         . setLayout(new FlowLayout());

          TopPanel            . setBorder(new TitledBorder("Basic Info"));
          BottomPanel         . setBorder(new TitledBorder("Controls"));
     }

     public void addComponents()
     {
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel("Reference No."));
          TopPanel            . add(TReferenceNo);
          TopPanel            . add(new JLabel(""));

          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel("Proforma No."));
          TopPanel            . add(TProformaNo);
          TopPanel            . add(new JLabel(""));

          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel("Entry Date"));
          TopPanel            . add(TDate);
          TopPanel            . add(new JLabel(""));

          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel("Supplier"));
          TopPanel            . add(BSupplier);
          TopPanel            . add(new JLabel(""));

          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel("List of Materials"));
          TopPanel            . add(BMaterials);
          TopPanel            . add(new JLabel(""));
          
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel("Order Type"));
          TopPanel            . add(JCOrderType);
          TopPanel            . add(new JLabel(""));
          
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel("Currency Type"));
          TopPanel            . add(JCCurrencyType);
          TopPanel            . add(new JLabel(""));
          
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel("Intra/Inter State"));
          TopPanel            . add(JCState);
          TopPanel            . add(new JLabel(""));
          
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(new JLabel("Port"));
          TopPanel            . add(JCPort);
          TopPanel            . add(new JLabel(""));

          BottomPanel         . add(BOk);
          BottomPanel         . add(BCancel);

          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(MiddlePanel,BorderLayout.CENTER);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BSupplier           . addActionListener(new EPCGSupplierSearch(DeskTop,TSupCode,SSupTable));

          JEPCG               . addFocusListener(new FocListener());
          JCState             . addFocusListener(new FocListener());
          JCPort              . addFocusListener(new FocListener());

          JCOrderType         . addItemListener(new OrderTypeListener());

          if(bflag)
          {
               BOk            . addActionListener(new UpdtList());
          }
          else 
          {
               BMaterials     . addActionListener(new EPCGMaterialsSearch(DeskTop,VCode,VName,MiddlePanel,"Material Selector","Order",iMillCode,SItemTable));
               BOk            . addActionListener(new ActList());
          }

          BCancel        . addActionListener(new CanList());
     }

     public class FocListener implements FocusListener
     {
          public void focusLost(FocusEvent fe)
          {
               if (fe.getSource() == JEPCG)
               {
                    if (JEPCG.getSelectedIndex() == 0)
                    {
                         JCTo           . requestFocus();
                         JCOrderType    . setEnabled(false);
                         JCState        . setEnabled(false);
                         JCPort         . setEnabled(false);
                    }
                    else
                    {
                         JCOrderType    . requestFocus();
                         JCOrderType    . setEnabled(true);
                         JCState        . setEnabled(true);
                         JCPort         . setEnabled(true);
                    }
               }
               
               if ((fe.getSource() == JCOrderType) && (JEPCG.getSelectedIndex()==1))
               {
                    if (JCOrderType.getSelectedIndex() == 1)
                    {
                         JCState        . setEnabled(false);
                         JCPort         . setEnabled(true);
                         JCPort         . setSelectedIndex(1);
                         JCState        . setSelectedIndex(0);                         
                    }
                    else
                    {
                         JCPort         . setSelectedIndex(0);                         
                         JCPort         . setEnabled(false);
                         JCState        . setEnabled(true);
                         JCState        . setSelectedIndex(1);                         
                    }
               }
          }
     
          public void focusGained(FocusEvent fe)
          {
          }
     }

     public class CanList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               removeHelpFrame();
          }
     }

     public class ActList implements ActionListener
     {
          int iAmend                    = 0;
          public void actionPerformed(ActionEvent AcE)
          {
               if(MiddlePanel.MiddlePanel == null)
               {
                    JOptionPane.showMessageDialog(null,"Please Select Materials ","Information",JOptionPane.INFORMATION_MESSAGE);
                    return;
               }

               Object FinalData[][]     = MiddlePanel.MiddlePanel.getFromVector();

               if(((String)JCOrderType.getSelectedItem()).startsWith("IMPORT") && ((String)JCCurrencyType.getSelectedItem()).startsWith("RUPEES"))
               {
                    JOptionPane.showMessageDialog(null, "You cannot select 'RUPEES' Currency Type for Import Orders..", "Information", JOptionPane.ERROR_MESSAGE);
                    BOk.setEnabled(true);
                    return;
               }

               if(!isValidNew())
               {
                    common.warn(DeskTop,SPanel);
                    BOk.setEnabled(true);
                    return;
               }

               if(JOptionPane.showConfirmDialog(null, "Confirm Save the Data?", "Confirm", JOptionPane.YES_NO_OPTION) == 0)
               {
                    BOk                      . setEnabled(false);

                    try
                    {
                         insertOrderDetails();
     
                         /*
                              insertDescDetails();
                              UpdateInvItems();
                         */
                    }
                    catch(Exception Ex)
                    {
                         Ex.printStackTrace();
                         bComflag = false;
                    }
                    try
                    {
                         if(bComflag)
                         {
                              theConnect     . commit();
                              System         . out.println("Commit");
                              theConnect     . setAutoCommit(true);

                              JOptionPane.showMessageDialog(null, "Data Saved Successfully.\n Reference No. "+TReferenceNo.getText(), "Information", JOptionPane.INFORMATION_MESSAGE);
                         }
                         else
                         {
                              theConnect     . rollback();
                              System         . out.println("RollBack");
                              theConnect     . setAutoCommit(true);

                              JOptionPane.showMessageDialog(null, "Data Not Saved.", "Error", JOptionPane.ERROR_MESSAGE);
                         }
                    }catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }

                    removeHelpFrame();
     
                    EPCGRequestFrame epcgRequestFrame = new EPCGRequestFrame(DeskTop,VCode,VName,VNameCode,SPanel,false,iAmend,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);
                    DeskTop.add(epcgRequestFrame);
                    DeskTop.repaint();
                    DeskTop.updateUI();
                    epcgRequestFrame.show();
               }
          }
     }

     public class UpdtList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk  . setEnabled(false);

               if(((String)JCOrderType.getSelectedItem()).startsWith("IMPORT") && ((String)JCCurrencyType.getSelectedItem()).startsWith("RUPEES"))
               {
                    JOptionPane.showMessageDialog(null, "You cannot select 'RUPEES' Currency Type for Import Orders..", "Information", JOptionPane.ERROR_MESSAGE);
                    BOk.setEnabled(true);
                    return;
               }

               if(!isValidNew())
               {
                    common    . warn(DeskTop,SPanel);
                    BOk       . setEnabled(true);
                    return;
               }

               if(JOptionPane.showConfirmDialog(null, "Confirm Update the Data?", "Confirm", JOptionPane.YES_NO_OPTION) == 0)
               {
                    try
                    {
                         updateOrderDetails();
                         insertOrderDetails();

                         /*
                              updateDescDetails();
          
                              insertDescDetails();
                              UpdateInvItems();
                         */
                    }
                    catch(Exception Ex)
                    {
                         Ex.printStackTrace();
                         bComflag = false;
                    }
                    try
                    {
                         if(bComflag)
                         {
                              theConnect     . commit();
                              System         . out.println("Commit");
                              theConnect     . setAutoCommit(true);

                              JOptionPane.showMessageDialog(null, "Data Updated Successfully", "Information", JOptionPane.INFORMATION_MESSAGE);
                         }
                         else
                         {
                              theConnect     . rollback();
                              System         . out.println("RollBack");
                              theConnect     . setAutoCommit(true);

                              JOptionPane.showMessageDialog(null, "Data Not Saved.", "Error", JOptionPane.ERROR_MESSAGE);
                         }
                    }
                    catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }

                    removeHelpFrame();
               }
          }
     }

     private class OrderTypeListener implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
               if(ie.getSource() == JCOrderType)
               {
                    setOrderTypeListener();
               }
          }
     }

     private void setOrderTypeListener()
     {
          if(JCOrderType.getSelectedIndex() == 0)
          {
               JCState        . setEnabled(true);
               JCPort         . setEnabled(false);

               JCCurrencyType . setSelectedItem("RUPEES");
               JCCurrencyType . setEnabled(false);
          }
          else
          {
               JCState        . setEnabled(false);
               JCPort         . setEnabled(true);

               JCCurrencyType . setSelectedIndex(0);
               JCCurrencyType . setEnabled(true);
          }
     }

     public void insertOrderDetails()
     {
          String    QString   = "";
          int       iMRSSlno  = 0;
          
          if(bflag)
               QString = "Insert Into EPCGRequestOrder (id,OrderNo,OrderDate,OrderBlock,MrsNo,Sup_Code,Reference,Advance,PayTerms,PayDays,ToCode,ThroCode,FormCode,Item_Code,Qty,Rate,DiscPer,Disc,CenvatPer,CenVat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,Unit_Code,Dept_Code,Group_Code,DueDate,SlNo,OrderType,EPCG,portcode,state,Project_order,UserCode,ModiDate,MRSSlNo,TAXCLAIMABLE,Amended,MillCode,ReferenceNo,YearCode,ProformaNo,Basic,CurrencyTypeCode) Values (";
          else
               QString = "Insert Into EPCGRequestOrder (id,OrderNo,OrderDate,OrderBlock,MrsNo,Sup_Code,Reference,Advance,PayTerms,PayDays,ToCode,ThroCode,FormCode,Item_Code,Qty,Rate,DiscPer,Disc,CenvatPer,CenVat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,Unit_Code,Dept_Code,Group_Code,DueDate,SlNo,OrderType,EPCG,portcode,state,Project_order,UserCode,ModiDate,MRSSlNo,TAXCLAIMABLE,MillCode,ReferenceNo,YearCode,ProformaNo,Basic,CurrencyTypeCode) Values (";

          try
          {

               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat           = theConnect.createStatement();
               
                         TReferenceNo   . setText(getNextReferenceNo());  // Incorporated On 23.05.2003 to enhance multi-user order numbering
               
               if(bflag)
                    iMaxSlNo = getMaxSlNo(((String)TReferenceNo.getText()).trim());
               else
                    iMaxSlNo = 0;

               int k = iMaxSlNo;

               Object FinalData[][]     = MiddlePanel.MiddlePanel.getFromVector();
               String SAdd              = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess             = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm               = common.toDouble(SAdd)-common.toDouble(SLess);
               double dBasic            = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio            = dpm/dBasic;
               DateField2   date        = new DateField2();
               date.setTodayDate();
               
               for(int i=0;i<FinalData.length;i++)
               {
                    try
                    {
                         if(!isFreshRecord(i))
                              continue;
                    }
                    catch(Exception ex){}
     
                    if(bflag)
                    {
                         if(MiddlePanel.VMrsSlNo.size()>i)
                              iMRSSlno   = common.toInt((String)MiddlePanel.VMrsSlNo.elementAt(i));
                         else
                              iMRSSlno   = 0;
                    }
                    else
                              iMRSSlno   = 0;
     
                    String SBlockCode = (String)MiddlePanel.MiddlePanel.getBlockCode(i);
                    String SUnitCode  = (String)MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = (String)MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = (String)MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SDueDate   = (String)MiddlePanel.MiddlePanel.getDueDate(i,TDate);
                    String STaxClaim  = (String)MiddlePanel.MiddlePanel.getTaxClaim(i);
                       int iTaxClaim  = common.toInt(STaxClaim.trim());
                           dBasic     = common.toDouble(((String)FinalData[i][11]).trim());
                    String SMisc      = common.getRound(dBasic*dRatio,3);
                    
                    String    QS1 = QString;
                              QS1 = QS1+"0"+getNextVal("EPCGRequestOrder_SEQ")+",";
                              QS1 = QS1+"0,"; //((String)TOrderNo.getText()).trim()
                              QS1 = QS1+"'"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                              QS1 = QS1+"0"+SBlockCode+",";
                              QS1 = QS1+"0"+((String)FinalData[i][4]).trim()+",";
                              QS1 = QS1+"'"+((String)TSupCode.getText()).trim()+"',";
                              QS1 = QS1+"'"+(TRef.getText()).toUpperCase()+"',";    
                              QS1 = QS1+"0"+TAdvance.getText()+",";
                              QS1 = QS1+"'"+TPayTerm.getText()+"',";
                              QS1 = QS1+"0"+TPayDays.getText()+",";
                              QS1 = QS1+"0,"; //"+(String)VToCode.elementAt(JCTo.getSelectedIndex())+"
                              QS1 = QS1+"0,"; //"+(String)VThroCode.elementAt(JCThro.getSelectedIndex())+"
                              QS1 = QS1+"0,"; //"+(String)VFormCode.elementAt(JCForm.getSelectedIndex())+"
                              QS1 = QS1+"'"+(String)FinalData[i][0]+"',";
                              QS1 = QS1+"0"+((String)FinalData[i][5]).trim()+",";
                              QS1 = QS1+"0"+common.getRound(((String)FinalData[i][6]).trim(),4)+",";
                              QS1 = QS1+"0"+((String)FinalData[i][7]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][12]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][8]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][13]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][9]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][14]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][10]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][15]).trim()+",";
                              QS1 = QS1+"0"+((String)FinalData[i][16]).trim()+",";
                              QS1 = QS1+"0"+SAdd+",";
                              QS1 = QS1+"0"+SLess+",";
                              QS1 = QS1+"0"+SMisc+",";
                              QS1 = QS1+"0"+SUnitCode+",";
                              QS1 = QS1+"0"+SDeptCode+",";
                              QS1 = QS1+"0"+SGroupCode+",";
                              QS1 = QS1+"'"+SDueDate+"',";
                              QS1 = QS1+(k+1)+",";
                              QS1 = QS1+"0"+(String)VOrderTypeCode.elementAt(JCOrderType.getSelectedIndex())+",";
                              QS1 = QS1+"1, ";  //JEPCG.getSelectedIndex()
                              QS1 = QS1+"0"+(String)VPortCode.elementAt(JCPort.getSelectedIndex())+",";
                              QS1 = QS1+"0"+(String)VStateCode.elementAt(JCState.getSelectedIndex())+",";
                              QS1 = QS1+JCProject.getSelectedIndex()+",";
                              QS1 = QS1+iUserCode+",";
                              QS1 = QS1+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+",";
                              QS1 = QS1+iMRSSlno+",";
                              QS1 = QS1+iTaxClaim+",";

                              if(bflag)
                              {
                                   QS1 = QS1+iAmend+",";
                              }

                              QS1 = QS1+iMillCode+", ";
                              QS1 = QS1+"0"+((String)TReferenceNo.getText()).trim()+", ";
                              QS1 = QS1+SYearCode+", ";
                              QS1 = QS1+"'"+common.parseNull(TProformaNo.getText()).trim()+"', ";
                              QS1 = QS1+"0"+((String)FinalData[i][11]).trim()+", ";
                              QS1 = QS1+"0"+(String)VCurrencyTypeCode.elementAt(JCCurrencyType.getSelectedIndex())+") ";

                    stat           . executeUpdate(QS1);

                    k++;
               }

               stat                . close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("1"+ex);
               ex.printStackTrace();
          }
     }

     private int getNextVal(String SSequence) throws Exception
     {
          int iValue = -1;
          String QS = "Select "+SSequence+".NextVal from dual";
          try
          {
               Statement  stat  =  theConnect.createStatement();
               ResultSet result = stat.executeQuery(QS);
               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result.close();
          }
          catch(Exception ex)
          {
               System.out.println("getNextVal :"+SSequence+ex);
          }
          return iValue;
     }
     
     public void insertDescDetails()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);
               Statement stat      =  theConnect.createStatement();

               Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
               
               int k = iMaxSlNo;

               for(int i=0;i<FinalData.length;i++)
               {
                    String    QS1 = "";
                    try
                    {
                         if(!isFreshRecord(i))
                              continue;
                    }
                    catch(Exception ex){}

                    String SBlockCode = MiddlePanel.MiddlePanel.getBlockCode(i);
                    
                    String SDesc        = common.parseNull((String)MiddlePanel.MiddlePanel.VDesc.elementAt(i)).trim();
                    String SMake        = common.parseNull((String)MiddlePanel.MiddlePanel.VMake.elementAt(i)).trim();
                    String SDraw        = common.parseNull((String)MiddlePanel.MiddlePanel.VDraw.elementAt(i)).trim();
                    String SCatl        = common.parseNull((String)MiddlePanel.MiddlePanel.VCatl.elementAt(i)).trim();

                    String SItem_Code   = (String)FinalData[i][0];

                    if(isStationary(SItem_Code))
                    {
                         String    SPaperColor    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPColour   . elementAt(i));
                         String    SPaperSets     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSet      . elementAt(i));
                         String    SPaperSize     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSize     . elementAt(i));
                         String    SPaperSide     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSide     . elementAt(i));
                         String    SSlipFromNo    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSlipFrNo . elementAt(i));
                         String    SSlipToNo      =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSlipToNo . elementAt(i));
                         String    SBookFromNo    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPBookFrNo . elementAt(i));
                         String    SBookToNo      =    common.parseNull((String)MiddlePanel.MiddlePanel.VPBookToNo . elementAt(i));

                         QS1 = " Insert Into EPCGMatDesc (id,OrderNo,OrderDate,OrderBlock,Item_Code,Descr,Make,Draw,Catl,MillCode,SlNo,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE,SLIPFROMNO,SLIPTONO,BOOKFROMNO,BOOKTONO,ReferenceNo) Values (";
                         QS1 = QS1+"0"+getNextVal("EPCGMATDESC_SEQ")+",";
                         QS1 = QS1+"0,"; //TOrderNo.getText()
                         QS1 = QS1+"'"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                         QS1 = QS1+"0"+SBlockCode+",";
                         QS1 = QS1+"'"+(String)FinalData[i][0]+"',";
                         QS1 = QS1+"'"+SDesc+"',";
                         QS1 = QS1+"'"+SMake+"',";
                         QS1 = QS1+"'"+SDraw+"',";
                         QS1 = QS1+"'"+SCatl+"',";
                         QS1 = QS1+iMillCode+",";
                         QS1 = QS1+(k+1)+",'";
                         QS1 = QS1+SPaperColor+"','";
                         QS1 = QS1+SPaperSets+"','";
                         QS1 = QS1+SPaperSize+"','";
                         QS1 = QS1+SPaperSide+"',";
                         QS1 = QS1+"0"+SSlipFromNo+",";
                         QS1 = QS1+"0"+SSlipToNo+",";
                         QS1 = QS1+"0"+SBookFromNo+",";
                         QS1 = QS1+"0"+SBookToNo+",";
                         QS1 = QS1+"0"+TReferenceNo.getText().trim()+")";
                    }
                    else
                    {
                         QS1 = "Insert Into EPCGMatDesc (id,OrderNo,OrderDate,OrderBlock,Item_Code,Descr,Make,Draw,Catl,MillCode,SlNo,ReferenceNo) Values (";
                         QS1 = QS1+"0"+getNextVal("EPCGMATDESC_SEQ")+",";
                         QS1 = QS1+"0"+TOrderNo.getText()+",";
                         QS1 = QS1+"'"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                         QS1 = QS1+"0"+SBlockCode+",";
                         QS1 = QS1+"'"+(String)FinalData[i][0]+"',";
                         QS1 = QS1+"'"+SDesc+"',";
                         QS1 = QS1+"'"+SMake+"',";
                         QS1 = QS1+"'"+SDraw+"',";
                         QS1 = QS1+"'"+SCatl+"',";
                         QS1 = QS1+iMillCode+",";
                         QS1 = QS1+(k+1)+", ";
                         QS1 = QS1+"0"+TReferenceNo.getText().trim()+")";
                    }
                    stat.executeUpdate(QS1);
                    k++;
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("2"+ex);
          }
     }

     public void updateOrderDetails()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat           =  theConnect.createStatement();
                                        
               Object    FinalData[][]  = MiddlePanel.MiddlePanel.getFromVector();
               String    SAdd           = MiddlePanel.MiddlePanel.TAdd.getText();
               String    SLess          = MiddlePanel.MiddlePanel.TLess.getText();
               double    dpm            = common.toDouble(SAdd)-common.toDouble(SLess);
               double    dBasic         = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double    dRatio         = dpm/dBasic;
               
               for(int i=0;i<MiddlePanel.IdData.length;i++)
               {
                    String SBlockCode = (String) MiddlePanel.MiddlePanel.getBlockCode(i);
                    String SUnitCode  = (String) MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = (String) MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = (String) MiddlePanel.MiddlePanel.getGroupCode(i);

                    String SDueDate   = (String) MiddlePanel.MiddlePanel.getDueDate(i,TDate);
                    String STaxClaim  = (String)MiddlePanel.MiddlePanel.getTaxClaim(i);
                       int iTaxClaim  = common.toInt(STaxClaim.trim());
                    dBasic            = common.toDouble(((String)FinalData[i][11]).trim());
                    String SMisc      = common.getRound(dBasic*dRatio,3);
                    
                    String    QS1 = "Update EPCGRequestOrder Set ";
                              QS1 = QS1+"OrderDate  = '"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                              QS1 = QS1+"OrderBlock = 0"+SBlockCode+",";
                              QS1 = QS1+"Sup_Code='"+TSupCode.getText()+"',";
                              QS1 = QS1+"Reference='"+(TRef.getText()).toUpperCase()+"',";
                              QS1 = QS1+"Advance=0"+TAdvance.getText()+",";
                              QS1 = QS1+"PayTerms='"+TPayTerm.getText()+"',";
                              QS1 = QS1+"PayDays=0"+TPayDays.getText()+",";
                              QS1 = QS1+"ToCode=0"+(String)VToCode.elementAt(JCTo.getSelectedIndex())+",";
                              QS1 = QS1+"ThroCode=0"+(String)VThroCode.elementAt(JCThro.getSelectedIndex())+",";
                              QS1 = QS1+"FormCode=0"+(String)VFormCode.elementAt(JCForm.getSelectedIndex())+",";
                              QS1 = QS1+"PortCode=0"+(String)VPortCode.elementAt(JCPort.getSelectedIndex())+",";
                              QS1 = QS1+"EPCG=0"+(JEPCG.getSelectedIndex())+",";
                              QS1 = QS1+"State=0"+((String)VStateCode.elementAt(JCState.getSelectedIndex()))+",";
                              QS1 = QS1+"Project_Order=0"+(JCProject.getSelectedIndex())+",";
                              QS1 = QS1+"OrderType=0"+((String)VOrderTypeCode.elementAt(JCOrderType.getSelectedIndex()))+",";
                              QS1 = QS1+"MrsNo=0"+((String)FinalData[i][4]).trim()+",";
                              QS1 = QS1+"Item_Code='"+(String)FinalData[i][0]+"',";
                              QS1 = QS1+"Qty=0"+((String)FinalData[i][5]).trim()+",";
                              QS1 = QS1+"Rate=0"+common.getRound(((String)FinalData[i][6]).trim(),4)+",";
                              QS1 = QS1+"DiscPer=0"+((String)FinalData[i][7]).trim()+",";
                              QS1 = QS1+"Disc=0"+((String)FinalData[i][12]).trim()+",";
                              QS1 = QS1+"CenVatPer=0"+((String)FinalData[i][8]).trim()+",";
                              QS1 = QS1+"Cenvat=0"+((String)FinalData[i][13]).trim()+",";
                              QS1 = QS1+"TaxPer=0"+((String)FinalData[i][9]).trim()+",";
                              QS1 = QS1+"Tax=0"+((String)FinalData[i][14]).trim()+",";
                              QS1 = QS1+"SurPer=0"+((String)FinalData[i][10]).trim()+",";
                              QS1 = QS1+"Sur=0"+((String)FinalData[i][15]).trim()+",";
                              QS1 = QS1+"Net=0"+((String)FinalData[i][16]).trim()+",";
                              QS1 = QS1+"Plus=0"+SAdd+",";
                              QS1 = QS1+"Less=0"+SLess+",";
                              QS1 = QS1+"Misc=0"+SMisc+",";
                              QS1 = QS1+"Unit_Code=0"+SUnitCode+",";
                              QS1 = QS1+"Dept_Code=0"+SDeptCode+",";
                              QS1 = QS1+"Group_Code=0"+SGroupCode+",";
                              QS1 = QS1+"Amended=0"+iAmend+",";
                              QS1 = QS1+"TAXCLAIMABLE=0"+iTaxClaim+",";
                              QS1 = QS1+"AUTHENTICATION = 0,";
                              QS1 = QS1+"DueDate='"+SDueDate+"', ";
                              QS1 = QS1+"Basic = 0"+((String)FinalData[i][11]).trim()+", ";
                              QS1 = QS1+"CurrencyTypeCode= "+((String)VCurrencyTypeCode.elementAt(JCCurrencyType.getSelectedIndex()))+" ";
                              QS1 = QS1+" Where ID = "+(String)MiddlePanel.IdData[i];
                    
                      stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("4"+ex);
               ex.printStackTrace();
          }
     }

     public void updateDescDetails()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);
               Statement stat          = theConnect.createStatement();
               Object    FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
          
               for(int i=0;i<MiddlePanel.DescId.length;i++)
               {
                    String    QS1       = "";
                    String SBlockCode   = common.parseNull((String)MiddlePanel.MiddlePanel.getBlockCode(i));
                    String SDesc        = common.parseNull((String)MiddlePanel.MiddlePanel.VDesc.elementAt(i));
                    String SMake        = common.parseNull((String)MiddlePanel.MiddlePanel.VMake.elementAt(i));
                    String SDraw        = common.parseNull((String)MiddlePanel.MiddlePanel.VDraw.elementAt(i));
                    String SCatl        = common.parseNull((String)MiddlePanel.MiddlePanel.VCatl.elementAt(i));

                    String SItem_Code   = (String)FinalData[i][0];

                    if(isStationary(SItem_Code))
                    {
                         String    SPaperColor    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPColour   . elementAt(i));
                         String    SPaperSets     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSet      . elementAt(i));
                         String    SPaperSize     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSize     . elementAt(i));
                         String    SPaperSide     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSide     . elementAt(i));
                         String    SSlipFromNo    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSlipFrNo . elementAt(i));
                         String    SSlipToNo      =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSlipToNo . elementAt(i));
                         String    SBookFromNo    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPBookFrNo . elementAt(i));
                         String    SBookToNo      =    common.parseNull((String)MiddlePanel.MiddlePanel.VPBookToNo . elementAt(i));

                         QS1 = "Update EPCGMatDesc Set ";
                         QS1 = QS1+"OrderDate     = '"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                         QS1 = QS1+"OrderBlock    = 0"+SBlockCode+",";
                         QS1 = QS1+"Item_Code     = '"+(String)FinalData[i][0]+"',";
                         QS1 = QS1+"Descr         = '"+SDesc+"',";
                         QS1 = QS1+"Make          = '"+SMake+"',";
                         QS1 = QS1+"Draw          = '"+SDraw+"',";
                         QS1 = QS1+"Catl          = '"+SCatl+"',";
                         QS1 = QS1+"SLIPFROMNO    = 0"+SSlipFromNo+",";
                         QS1 = QS1+"SLIPTONO      = 0"+SSlipToNo+",";
                         QS1 = QS1+"BOOKFROMNO    = 0"+SBookFromNo+",";
                         QS1 = QS1+"BOOKTONO      = 0"+SBookToNo+" Where ID = "+(String)MiddlePanel.DescId[i];
                    }
                    else
                    {
                         QS1 = "Update EPCGMatDesc Set ";
                         QS1 = QS1+"OrderDate  = '"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                         QS1 = QS1+"OrderBlock =0"+SBlockCode+",";
                         QS1 = QS1+"Item_Code='"+(String)FinalData[i][0]+"',";
                         QS1 = QS1+"Descr    ='"+SDesc+"',";
                         QS1 = QS1+"Make     ='"+SMake+"',";
                         QS1 = QS1+"Draw     ='"+SDraw+"',";
                         QS1 = QS1+"Catl     ='"+SCatl+"' Where ID = "+(String)MiddlePanel.DescId[i];
                    }
                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("5"+ex);
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop   . remove(this);
               DeskTop   . repaint();
               DeskTop   . updateUI();
          }
          catch(Exception ex) { }
     }

     public void fillData(String SReferenceNo, String SProformaNo, int iOrderStatus,Vector VItemsStatus,String SItemTable,String SSupTable)
     {
          MiddlePanel    . getUpdateOrderNo(SReferenceNo);

          TProformaNo    . setText(SProformaNo);

          try
          {
               if(iOrderStatus == 2 || iOrderStatus == 1)
               {
                    setEnables();
               }

               new EPCGOrderResultSet(SReferenceNo,TReferenceNo,TDate,BSupplier,TSupCode,TRef,TAdvance,TPayTerm,TPayDays,JCTo,JCThro,JCForm,MiddlePanel,VCode,VName,VToCode,VThroCode,VFormCode,JEPCG,JCOrderType,VEpcg,VOrderType,VPort,JCPort,VProject,JCProject,VState,JCState,iOrderStatus,VItemsStatus,iMillCode,SItemTable,SSupTable,JCCurrencyType);
          }
          catch(Exception ex)
          {
               System.out.println("6"+ex);
               ex.printStackTrace();
          }
     }

     public void setEnables()
     {
          TDate. TDay    . setEnabled(false);
          TDate. TMonth  . setEnabled(false);
          TDate. TYear   . setEnabled(false);
          TRef           . setEnabled(false);
          BSupplier      . setEnabled(false);
          JEPCG          . setEnabled(false);
          JCOrderType    . setEnabled(false);
          JCProject      . setEnabled(false);
          JCTo           . setEnabled(false);
          JCThro         . setEnabled(false);
          JCForm         . setEnabled(false);
          TAdvance       . setEnabled(false);
          TPayTerm       . setEnabled(false);
          TPayDays       . setEnabled(false);
          JCPort         . setEnabled(false);
          JCState        . setEnabled(false);
     }

     public boolean isFreshRecord(int i)
     {
          try
          {
               if(MiddlePanel.IdData==null)
                    return true;
               
               boolean bfresh = (i<MiddlePanel.IdData.length?false:true);
               return bfresh;
          }
          catch(Exception ex)
          {
               return true;
          }
     }

     public void getVTo()
     {
          ResultSet  result  = null;
          
          VTo            = new Vector();
          VToCode        = new Vector();
          VThro          = new Vector();
          VThroCode      = new Vector();
          VFormCode      = new Vector();
          VForm          = new Vector();
          VEpcg          = new Vector();
          VOrderType     = new Vector();
          VPort          = new Vector();
          VPortCode      = new Vector();
          VBlockName     = new Vector();
          VBlockCode     = new Vector();
          
          VEpcg          . addElement("Non-EPCG Order   ");
          VEpcg          . addElement("EPCG Order   ");
          
          VOrderType     . addElement("Local Order      ");
          VOrderType     . addElement("Import       ");

          VOrderTypeCode           = new Vector();
          VOrderTypeName           = new Vector();

          VStateCode               = new Vector();
          VStateName               = new Vector();

          VCurrencyTypeCode        = new Vector();
          VCurrencyTypeName        = new Vector();
          
          try
          {
               Statement stat      = theConnect.createStatement();
                         result    = stat.executeQuery("Select ToName,ToCode From BookTo Order By 1");

               while(result.next())
               {
                    VTo      . addElement(result.getString(1));
                    VToCode  . addElement(result.getString(2));
               }
               result.close();
               
               result    = stat.executeQuery("Select ThroName,ThroCode From BookThro Order By 1");
               while(result.next())
               {
                    VThro    . addElement(result.getString(1));
                    VThroCode. addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery("Select FormNo,FormCode From STForm Order By 1");
               while(result.next())
               {
                    VForm    . addElement(result.getString(1));
                    VFormCode. addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery("Select PortCode,PortName From Port Order By 1");
               while(result.next())
               {
                    String ax = result.getString(2);
                    VPort     . addElement(ax);
                    VPortCode . addElement(result.getString(1));
               }
               result.close();

               result = stat.executeQuery("Select block,blockname From OrdBlock Order By 1");
               while(result.next())
               {
                    String ax      = result.getString(2);
                    VBlockName     . addElement(ax);
                    VBlockCode     . addElement(result.getString(1));
               }
               result.close();

               ResultSet rs        = stat.executeQuery("Select OrderTypeCode, OrderTypeName from EPCGOrderType Order by 1");
               while(rs.next())
               {
                    VOrderTypeCode . addElement(rs.getString(1));
                    VOrderTypeName . addElement(rs.getString(2));
               }
               rs                  . close();

               rs                       = stat.executeQuery("Select CurrencyTypeCode, CurrencyTypeName from EPCGCurrencyType Order by 2");
               while(rs.next())
               {
                    VCurrencyTypeCode   . addElement(rs.getString(1));
                    VCurrencyTypeName   . addElement(rs.getString(2));
               }
               rs                       . close();

               rs                  = stat.executeQuery("Select StateCode, StateName from EPCGState Order by 1");

               while(rs.next())
               {
                    VStateCode     . addElement(rs.getString(1));
                    VStateName     . addElement(rs.getString(2));
               }
               rs                  . close();

               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("7"+ex);
          }
     }

     public boolean isValidNew()
     {
          boolean   bFlag     = true;
          String    SDate     = TDate.toString();

          String    SPDate    = TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText();
                    SPDate    = SPDate.trim();

          if((SPDate.trim()).length()!=8)
          {
               write("Please enter the OrderDate Date Correctly \n\n");

               return false;
          } 

          try
          {
               FW = new FileWriter(common.getPrintPath()+"Error.prn");
               write("*-----------------------*\n");
               write("*    W A R N I N G      *\n");
               write("*-----------------------*\n\n");
          }
          catch(Exception ex){}
          
          // Checking for Supplier
          String str = (TSupCode.getText()).trim();
          if(str.length()==0)
          {
               write("Supplier Is Not Selected \n\n");
               bFlag = false;
          }

          // Checking Proforma No..

          String SProformaNo       = common.parseNull(TProformaNo.getText()).trim();
          if(SProformaNo.length() == 0)
          {
               write("Proforma No. must be filled \n\n");
               bFlag = false;
          }
          
          //  Checking for Table Items

          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();

          for(int i=0;i<FinalData.length;i++)
          {
               String SItemCode    = ((String)FinalData[i][0]).trim();

               String SMrsNo       = ((String)FinalData[i][4]).trim();
               
               String SBlockName   = ((String)FinalData[i][3]).trim();
               double dQty         = common.toDouble(((String)FinalData[i][5]).trim());
               double dRate        = common.toDouble(((String)FinalData[i][6]));
               String SDept        = ((String)FinalData[i][17]).trim();
               String SGroup       = ((String)FinalData[i][18]).trim();
               String SDueDate     = ((String)FinalData[i][19]).trim();
               String SUnit        = ((String)FinalData[i][20]).trim();
               
               if(SItemCode.length() == 0)
               {
                    write("Select Item Name for "+(i+1)+" row  \n");
                    bFlag = false;
               }

               if(dQty <= 0)
               {
                    write("Quantity for Item No "+(i+1)+" is not fed \n");
                    bFlag = false;
               }
               if(dRate <= 0)
               {
                    write("Rate for Item No "+(i+1)+" is not fed \n");
                    bFlag = false;
               }

               if(SDept.length() == 0)
               {
                    write("Department for Item No "+(i+1)+" is not Selected \n");
                    bFlag = false;
               }
               if(SGroup.length() == 0)
               {
                    write("Group for Item No "+(i+1)+" is not Selected \n");
                    bFlag = false;
               }
               if(SUnit.length() == 0)
               {
                    write("Unit for Item No "+(i+1)+" is not Selected \n");
                    bFlag = false;
               }
               if(SDueDate.length() != 10 || common.toInt(common.getDateDiff(SDueDate,SDate)) < 0)
               {
                    write("DueDate for Item No "+(i+1)+" is wrongly Entered \n");
                    bFlag = false;
               }

               /*
                    if(SBlockName.length() == 0)
                    {
                         write("BlockName for Item No "+(i+1)+" is not fed \n");
                         bFlag = false;
                    }
     
                    if(common.toInt(SMrsNo)>0)
                    {
                         int iCount = checkMrsNo(SItemCode,SMrsNo);
                         if(iCount==0)
                         {
                              write("Mrs No Mismatch for Item No "+(i+1)+" \n");
                              bFlag = false;
                         }
                    }
               */
          }

          try
          {
               write("*-----------------------*\n");
               FW.close();
          }
          catch(Exception ex){}
          
          return bFlag;
     }

     public void write(String str)
     {
          try
          {
               FW.write(str);
          }
          catch(Exception ex){}
     }

     public int checkMrsNo(String SItemCode,String SMrsNo)
     {
          int iCount=0;

          String QS      = "";

          QS = " Select Count(*) from Mrs Where MillCode="+iMillCode+" and MrsNo="+SMrsNo+" and Item_Code='"+SItemCode+"'";

          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         iCount    = result.getInt(1);
                         result    . close();
                         stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
          return iCount;
     }

     public int getMaxSlNo(String SOrderNo)
     {
          int iMaxNo=0;

          String QS = " Select Max(SlNo) from EPCGRequestOrder Where ReferenceNo="+SOrderNo;

          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         iMaxNo    = common.toInt((String)result.getString(1));
                         result    . close();
                         stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
           
          return iMaxNo;
     }

     public String getNextReferenceNo()
     {
          // During Modification Reference No Need Not to be changed
          if(bflag)
               return TReferenceNo.getText();

          String SReferenceNo      = "";
          String QS                = "";
          String QS1               = "";

          QS = "  Select (MaxNo+1) from EPCGConfig where MillCode = "+iMillCode+" and YearCode = "+SYearCode+" and AliasName = 'REFNO' for update of MaxNo noWait ";

          try
          {
               // Get No..

               if(theConnect  . getAutoCommit())
                    theConnect  . setAutoCommit(false);

               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                    SReferenceNo   = common.parseNull((String)result.getString(1));
                         result    . close();
                         stat      . close();

               // Set No..

               PreparedStatement thePrepare = theConnect.prepareStatement(" Update EPCGConfig set MaxNo = ?  where where MillCode = "+iMillCode+" and YearCode = "+SYearCode+" and AliasName = 'REFNO'");
               thePrepare          . setInt(1,common.toInt(SOrderNo));
               thePrepare          . executeUpdate();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getNextReferenceNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }

          TReferenceNo             . setText(SReferenceNo);
           
          return TReferenceNo.getText();
     }

     public boolean isStationary(String SItemCode)
     {
          String SStkGroupCode     = "";
          String QS                = "";

          if(iMillCode==0)
          {
               QS =    " select stkgroupcode,invitems.item_code from invitems"+
                       " where invitems.item_code = '"+SItemCode+"'";
          }
          else
          {
               QS =    " select stkgroupcode,"+SItemTable+".item_code from "+SItemTable+""+
                       " where "+SItemTable+".item_code = '"+SItemCode+"'";
          }

          try
          {
               Statement      stat          = theConnect.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result.next())
               {
                    SStkGroupCode  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }
          if(SStkGroupCode.equals("B01"))
               return true;

          return false;
     }

     public void UpdateInvItems()
     {
          String QS           = "";
          Statement stat      = null;

          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
          try
          {
               for(int i=0;i<MiddlePanel.MiddlePanel.VPSlipFrNo.size();i++)
               {
                    QS = "";
                    String SItem_Code    = ((String)FinalData[i][0]).trim();
                    if(isStationary(SItem_Code))
                    {
                         int SSlipNo = common.toInt(common.parseNull((String)MiddlePanel.MiddlePanel.VPSlipToNo.elementAt(i)));
                         int SBookNo = common.toInt(common.parseNull((String)MiddlePanel.MiddlePanel.VPBookToNo.elementAt(i)));
               
                         if(iMillCode==0)
                         {
                              QS = " Update Invitems        set LASTSLIPNO ="+SSlipNo+",LASTBOOKNO ="+SBookNo+
                                   " where invitems.item_code = '"+SItem_Code+"'";
                         }
                         else
                         {
                              QS = " Update "+SItemTable+"  set LASTSLIPNO ="+SSlipNo+",LASTBOOKNO ="+SBookNo+
                                   " where "+SItemTable+".item_code = '"+SItem_Code+"'";
                         }
     
                         if(theConnect.getAutoCommit())
                                   theConnect     . setAutoCommit(false);
     
                         stat =    theConnect. createStatement();
                                   stat      . executeUpdate(QS);
                    }
               }
               if(stat!=null)
                    stat.close();
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
}
