package Order;
     
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class EPCGStationaryPropertiesFrame extends JInternalFrame
{
     JTextField     TMatCode,TMatName;
     JTextField     TStationaryColor,TStationarysize;
     JTextField     TStationarySlipFromNo,TStationarySlipToNo,TStationaryBookFromNo,TStationaryBookToNo;

     JComboBox      JStationarySet,JStationaryside;
     JButton        BOk,BCancel;

     JPanel         TopPanel,BottomPanel;
     JLayeredPane   Layer;
     JTable         ReportTable;

     String         SItemCode ="",SItemName  ="";
     String         SPColour  ="",SPSet      = "",SPSize    = "",SPSide    = "";
     String         SPSlipFrNo="",SPSlipToNo = "",SPBookFrNo= "",SPBookToNo= "";

     Vector         VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo;
     Vector         VPaperSet,VSetCode,VSideName,VSideCode;

     Common         common         = new Common();
     Connection     theConnection  = null;
     
     EPCGStationaryPropertiesFrame(JLayeredPane Layer,JTable ReportTable,EPCGInvTableModel dataModel,Vector VPColour,Vector VPSet,Vector VPSize,Vector VPSide,Vector VPSlipFrNo,Vector VPSlipToNo,Vector VPBookFrNo,Vector VPBookToNo)
     {
          this.Layer          = Layer;
          this.ReportTable    = ReportTable;

          this.VPColour       = VPColour;
          this.VPSet          = VPSet;
          this.VPSize         = VPSize;
          this.VPSide         = VPSide;

          this.VPSlipFrNo     = VPSlipFrNo;
          this.VPSlipToNo     = VPSlipToNo;
          this.VPBookFrNo     = VPBookFrNo;
          this.VPBookToNo     = VPBookToNo;

          getSetSide();
          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
     }

     public void createComponents()
     {
          BOk                      = new JButton("Okay");
          BCancel                  = new JButton("Cancel");
          
          TopPanel                 = new JPanel(true);
          BottomPanel              = new JPanel(true);
          
          TMatCode                 = new JTextField(SItemCode);
          TMatName                 = new JTextField(SItemName);
          
          TStationaryColor         = new JTextField();
          TStationarysize          = new JTextField();
          JStationarySet           = new JComboBox(VPaperSet);
          JStationaryside          = new JComboBox(VSideName);

          JStationarySet           . setEnabled(false);
          JStationaryside          . setEnabled(false);

          TStationarySlipFromNo    = new JTextField();
          TStationarySlipToNo      = new JTextField();
          TStationaryBookFromNo    = new JTextField();
          TStationaryBookToNo      = new JTextField();
          
          TMatCode                 . setEditable(false);
          TMatName                 . setEditable(false);
          TStationaryColor         . setEditable(false);
          JStationarySet           . setEditable(false);
          TStationarysize          . setEditable(false);
          JStationaryside          . setEditable(false);
     }
     
     public void setLayouts()
     {
          setBounds(80,100,450,350);
          setResizable(true);
          setTitle("Stationary Details");
          TopPanel  . setLayout(new GridLayout(10,2));
     }
     
     public void addComponents()
     {
          TopPanel            . add(new JLabel("Material Name"));
          TopPanel            . add(TMatName);
          TopPanel            . add(new JLabel("Material Code"));
          TopPanel            . add(TMatCode);
          TopPanel            . add(new JLabel("Paper Color"));
          TopPanel            . add(TStationaryColor);
          TopPanel            . add(new JLabel("Paper Set"));
          TopPanel            . add(JStationarySet);
          TopPanel            . add(new JLabel("Paper Size"));
          TopPanel            . add(TStationarysize);
          TopPanel            . add(new JLabel("Paper Side"));
          TopPanel            . add(JStationaryside);

          TopPanel            . add(new JLabel("Slip FromNo"));
          TopPanel            . add(TStationarySlipFromNo);
          TopPanel            . add(new JLabel("Slip ToNo"));
          TopPanel            . add(TStationarySlipToNo);
          TopPanel            . add(new JLabel("Book FromNo"));
          TopPanel            . add(TStationaryBookFromNo);
          TopPanel            . add(new JLabel("Book ToNo"));
          TopPanel            . add(TStationaryBookToNo);

          BottomPanel         . add(BOk);
          BottomPanel         . add(BCancel);
          
          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("South",BottomPanel);
     }
     
     public void addListeners()
     {
          BOk       . addActionListener(new ActList());
          BCancel   . addActionListener(new ActList());
     }

     public void setPresets()
     {
          int       i    = 0;
          String    SCode= "",SName     = "";

          i                        = ReportTable.getSelectedRow();

          SCode                    = (String)ReportTable.getModel().getValueAt(i,0);
          SName                    = (String)ReportTable.getModel().getValueAt(i,1);

          TMatCode                 . setText(SCode);
          TMatName                 . setText(SName);
          
          TStationaryColor         . setText((String)VPColour    . elementAt(i));
          TStationarysize          . setText((String)VPSize      . elementAt(i));

          JStationaryside          . setSelectedIndex(common.toInt((String)VPSide      . elementAt(i)));
          JStationarySet           . setSelectedIndex(common.toInt((String)VPSet       . elementAt(i)));

          TStationarySlipFromNo    . setText((String)VPSlipFrNo  . elementAt(i));
          TStationarySlipToNo      . setText((String)VPSlipToNo  . elementAt(i));
          TStationaryBookFromNo    . setText((String)VPBookFrNo  . elementAt(i));
          TStationaryBookToNo      . setText((String)VPBookToNo  . elementAt(i));
     }          

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    setTableDetails();
                    removeHelpFrame();
                    ReportTable    . requestFocus();
               }
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
                    ReportTable    . requestFocus();
               }
          }
     }

     private void setTableDetails()
     {
          int i     = 0;
              i     = ReportTable.getSelectedRow();
          
          VPColour  . setElementAt(TStationaryColor         . getText(),i);
          VPSet     . setElementAt((String)VSetCode         . elementAt(JStationarySet     . getSelectedIndex()),i);
          VPSize    . setElementAt(TStationarysize          . getText(),i);
          VPSide    . setElementAt((String)VSideCode        . elementAt(JStationaryside    . getSelectedIndex()),i);

          VPSlipFrNo. setElementAt(TStationarySlipFromNo    . getText(),i);
          VPSlipToNo. setElementAt(TStationarySlipToNo      . getText(),i);
          VPBookFrNo. setElementAt(TStationaryBookFromNo    . getText(),i);
          VPBookToNo. setElementAt(TStationaryBookToNo      . getText(),i);
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer     . remove(this);
               Layer     . repaint();
               Layer     . updateUI();
          }
          catch(Exception ex) { }
     }

     public void getSetSide()
     {
          ResultSet result    = null;

          VPaperSet           = new Vector();
          VSetCode            = new Vector();
          VSideName           = new Vector();
          VSideCode           = new Vector();

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();
                              theConnection  . setAutoCommit(true);
               Statement      stat           = theConnection.createStatement();

                              result         = stat.executeQuery("Select SetName,SetCode From PaperSet Order By SetCode");

               while(result.next())
               {
                    VPaperSet . addElement(result.getString(1));
                    VSetCode  . addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select SideName,SideCode From PaperSide Order By SideCode");

               while(result.next())
               {
                    VSideName . addElement(result.getString(1));
                    VSideCode . addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept & UOM :"+ex);
          }
     }
}
