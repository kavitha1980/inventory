package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class HsnCodePicker1 extends JInternalFrame
{
     JTextField     THsnCode;
     JTextField     TIndicator;
     JButton        BUpdate,BCancel;
     
     JList          BrowList;
     JScrollPane    BrowScroll;
     JPanel         LeftPanel;
     JPanel         LeftCenterPanel,LeftBottomPanel;
     
     JLayeredPane      Layer;
     JTable            ReportTable;
     JButton	       BTax,BOk;
   

     Vector            VSHsnCode;

     String         str ="";
     
     int            iLastIndex= 0;
     Common         common    = new Common();

     HsnCodePicker1(JLayeredPane Layer,JTable ReportTable,JButton BTax,JButton BOk)
     {
          this.Layer          = Layer;
          this.ReportTable    = ReportTable;
          this.BTax	      = BTax;
	      this.BOk	      = BOk;
	 

	  setDataIntoVector();
          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
     }

     public void createComponents()
     {
          BrowList            = new JList(VSHsnCode);
          BrowScroll          = new JScrollPane();
          THsnCode            = new JTextField();
          TIndicator          = new JTextField();
          TIndicator          . setEditable(false);
          
          BUpdate             = new JButton("Update");
          BCancel             = new JButton("Cancel");
          
          LeftPanel           = new JPanel(true);
          LeftCenterPanel     = new JPanel(true);
          LeftBottomPanel     = new JPanel(true);
          
          THsnCode            . setEditable(false);
     }

     public void setLayouts()
     {
          setBounds(80,50,650,500);
          setResizable(true);
          setClosable(true);
          setTitle("HsnCode Selection");
          getContentPane()    . setLayout(new GridLayout(1,1));
          LeftPanel           . setLayout(new BorderLayout());
          LeftCenterPanel     . setLayout(new BorderLayout());
          LeftBottomPanel     . setLayout(new GridLayout(4,2));
     }

     public void addComponents()
     {
          getContentPane()    . add(LeftPanel);
          
          LeftPanel           . add("Center",LeftCenterPanel);
          LeftPanel           . add("South",LeftBottomPanel);
          
          LeftCenterPanel     . add("Center",BrowScroll);
          LeftCenterPanel     . add("South",TIndicator);
          LeftBottomPanel     . add(new JLabel("Hsn Code"));
          LeftBottomPanel     . add(THsnCode);
          LeftBottomPanel     . add(new JLabel(""));
          LeftBottomPanel     . add(new JLabel(""));
          LeftBottomPanel     . add(BUpdate);
          LeftBottomPanel     . add(BCancel);
     }

     public void setPresets()
     {
          BrowList            . setAutoscrolls(true);
          BrowScroll          . getViewport().setView(BrowList);

          show();
          //ensureIndexIsVisible(SNameCode);
          LeftCenterPanel.updateUI();
     }

     public void addListeners()
     {
          BrowList  .addKeyListener(new KeyList());
          BUpdate   .addActionListener(new ActList());
          BCancel   .addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BUpdate)
               {
                         int       i = ReportTable.getSelectedRow();

                         String SItemCode = (String)ReportTable.getModel().getValueAt(i,0);
     
                         String SNewHsnCode  = THsnCode.getText();

			 updateItemMaster(SItemCode,SNewHsnCode);
     
                         ReportTable.getModel().setValueAt(THsnCode.getText(),i,2);

			

			 BTax.setEnabled(true);
			 BOk.setEnabled(false);
     
                         removeHelpFrame();
                         ReportTable.requestFocus();
               }
          }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='/') || (lastchar=='*') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==116)    // F5 is pressed
               {
                    setDataIntoVector();
                    BrowList.setListData(VSHsnCode);
               }
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SHsnCode     = (String)VSHsnCode.elementAt(index);
                    addMatDet(SHsnCode,index);
                    str="";
               }
          }
     }

     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<VSHsnCode.size();index++)
          {
               String str1 = ((String)VSHsnCode.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index);
                    break;
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

     public boolean addMatDet(String SHsnCode,int index)
     {
          THsnCode       . setText(SHsnCode);

          return true;    
     }

     public void setDataIntoVector()
     {
          VSHsnCode  = new Vector();

           String QString = " Select Distinct(hsngstrate.HsnCode) from HsnGstRate "+
                           " inner join invitems on invitems.hsncode=hsngstrate.hsncode "+
                           " and invitems.hsntype=0 "+
                           " Order By HsnCode ";
          
          
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
               ResultSet      res            = stat.executeQuery(QString);

               while(res.next())
               {
                    String    str1      = res.getString(1);
                              VSHsnCode . addElement(str1);
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void updateItemMaster(String SItemCode,String SNewHsnCode)
     {
          String QString = " Update InvItems Set HsnCode='"+SNewHsnCode+"' Where Item_Code='"+SItemCode+"'";
          
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
               stat.executeQuery(QString);
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

}
