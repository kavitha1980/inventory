package Order;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class InvAbsModel extends DefaultTableModel
{
        Object    RowData[][],ColumnNames[],ColumnType[];
        Common common = new Common();
        InvTableModel dataModel;
        JTable ReportTable;
        public InvAbsModel(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType,InvTableModel dataModel,JTable ReportTable)
        {
            super(RowData,ColumnNames);
            this.RowData     = RowData;
            this.ColumnNames = ColumnNames;
            this.ColumnType  = ColumnType;
            this.dataModel   = dataModel;
            this.ReportTable = ReportTable;

            for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
                setMaterialAmount(curVector);
            }
        }
       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }            
       public boolean isCellEditable(int row,int col)
       {
               if(ColumnType[col]=="B" || ColumnType[col]=="E")
                  return true;
               return false;
       }
       public void setValueAt(Object aValue, int row, int column)
       {
                Vector rowVector = (Vector)super.dataVector.elementAt(row);
                if(column==14)
                {
                     String str = ((String)aValue).trim();
                     aValue = common.getDate(str,0,1);
                }
                rowVector.setElementAt(aValue, column);
                setMaterialAmount(rowVector);
                fireTableChanged(new TableModelEvent(this, row, row, column,0));
                updateDataModel(rowVector);
      }
      public void setMaterialAmount(Vector RowVector)
      {
              double dQty        = common.toDouble((String)RowVector.elementAt(2));
              double dRate       = common.toDouble((String)RowVector.elementAt(3));
              double dDiscPer    = common.toDouble((String)RowVector.elementAt(4));
              double dCenVatPer  = common.toDouble((String)RowVector.elementAt(5));
              double dTaxPer     = common.toDouble((String)RowVector.elementAt(6));
              double dSurPer     = common.toDouble((String)RowVector.elementAt(7));

              double dGross  = dQty*dRate;
              double dDisc   = dGross*dDiscPer/100;
              double dCenVat = (dGross-dDisc)*dCenVatPer/100;
              double dTax    = (dGross-dDisc+dCenVat)*dTaxPer/100;
              double dSur    = (dTax)*dSurPer/100;
              double dNet    = dGross-dDisc+dCenVat+dTax+dSur;

              RowVector.setElementAt(common.getRound(dGross,2),8);
              RowVector.setElementAt(common.getRound(dDisc,2),9);
              RowVector.setElementAt(common.getRound(dCenVat,2),10);
              RowVector.setElementAt(common.getRound(dTax,2),11);
              RowVector.setElementAt(common.getRound(dSur,2),12);
              RowVector.setElementAt(common.getRound(dNet,2),13);
      }
      public void updateDataModel(Vector RowVector)
      {
              String SCCode=(String)RowVector.elementAt(0);
              SCCode = SCCode.trim();
               
              Object FinalData[][]=dataModel.getFromVector();
              for(int i=0;i<FinalData.length;i++)
              {
                  String SCode = ((String)FinalData[i][0]).trim();
                  if(SCode.equals(SCCode))
                  {
                     dataModel.setValueAt((String)RowVector.elementAt(3),i,4);
                     dataModel.setValueAt((String)RowVector.elementAt(4),i,5);
                     dataModel.setValueAt((String)RowVector.elementAt(5),i,6);
                     dataModel.setValueAt((String)RowVector.elementAt(6),i,7);
                     dataModel.setValueAt((String)RowVector.elementAt(7),i,8);
                     dataModel.setValueAt((String)RowVector.elementAt(14),i,17);
                  }
              }
      }
      public void setAbsData(Object RData[][])
      {
            super.dataVector.removeAllElements();
            for(int i=0;i<RData.length;i++)
            {
                  Vector VData = new Vector();
                  for(int j=0;j<15;j++)
                  {
                     VData.addElement((String)RData[i][j]);
                  }
                  insertRow(super.dataVector.size(),VData);
                  setMaterialAmount(VData);
                  updateDataModel(VData);
            }
            fireTableChanged(new TableModelEvent(this, 0,RData.length,-1,1));
      }
       public int getRows()
       {
            return super.dataVector.size();
       }
    public Vector getCurVector(int i)
    {
         return (Vector)super.dataVector.elementAt(i);
    }
}
