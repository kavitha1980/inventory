package Order;

import java.io.*;
import java.util.*;

import util.*;

public class ItemGst
{
     String SItemCode,SItemName,SHsnCode;

     HashMap theMap;
     Common common = new Common();
     ArrayList orderScheduleList;
     public ItemGst(String SItemCode)
     {
          this.SItemCode = SItemCode;
          theMap = new HashMap();
     }
     public ItemGst(String SItemCode,String SItemName,String SHsnCode)
     {
          this.SItemCode = SItemCode;
          this.SItemName = SItemName;
          this.SHsnCode  = SHsnCode;

          theMap = new HashMap();
          orderScheduleList = new ArrayList();
     }

    /* public void setOrderInfoDetails(String SSupCode,
                                      String SPayTerms,String SToCode,String SThroCode,
                                      String SPortCode,String SState,String SPayDays,
                                      String STaxCliamable,String SReference,String SCatl,String SDraw,String SPrevOrderNo,String SPrevOrderBlock)
     {
          theMap.put("SupCode"     ,SSupCode);
          theMap.put("PayTerms"    ,SPayTerms);
          theMap.put("ToCode"      ,SToCode);
          theMap.put("ThroCode"    ,SThroCode);
          theMap.put("PortCode"    ,SPortCode);
          theMap.put("State"       ,SState);
          theMap.put("PayDays"     ,SPayDays);
          theMap.put("TaxCliamable",STaxCliamable);
          theMap.put("Reference"   ,SReference);
          theMap.put("Catl"        ,SCatl);
          theMap.put("Draw"        ,SDraw);
          theMap.put("PrevOrderNo" ,SPrevOrderNo);
          theMap.put("PrevOrderBlock",SPrevOrderBlock);

     }*/
     public void setOrderInfoDetails(String SSupCode,
                                      String SPayTerms,String SToName,String SThroName,
                                      String SPortCode,String SState,String SPayDays,
                                      String STaxCliamable,String SReference,String SCatl,String SDraw,String SBlockName,
                                      String SUom,String SDesc,
                                      String SMake)
     {
          theMap.put("SupCode"     ,SSupCode);
          theMap.put("PayTerms"    ,SPayTerms);
          theMap.put("ToCode"      ,SToName);
          theMap.put("ThroCode"    ,SThroName);
          theMap.put("PortCode"    ,SPortCode);
          theMap.put("State"       ,SState);
          theMap.put("PayDays"     ,SPayDays);
          theMap.put("TaxCliamable",STaxCliamable);
          theMap.put("Reference"   ,SReference);
          theMap.put("Catl"        ,SCatl);
          theMap.put("Draw"        ,SDraw);
          theMap.put("Uom"           ,SUom);
          theMap.put("Desc"          ,SDesc);
          theMap.put("Make"          ,SMake);
          theMap.put("BlockName"     ,SBlockName);

     }

   
     public void setOrderValueDetails(double dRate,double dDiscPer,String SCgst,
                                      String SSgst,String SIgst,String SCess,double dPlus,
                                      double dLess,double dMisc,double dOthers)
     {
          theMap.put("Rate"        ,String.valueOf(dRate));
          theMap.put("DiscPer"     ,String.valueOf(dDiscPer));
          theMap.put("CgstPer"     ,SCgst);
          theMap.put("SgstPer"     ,SSgst);
          theMap.put("IgstPer"     ,SIgst);
          theMap.put("CessPer"     ,SCess);
           theMap.put("Plus"        ,String.valueOf(dPlus));
          theMap.put("Less"        ,String.valueOf(dLess));
          theMap.put("Misc"        ,String.valueOf(dMisc));
          theMap.put("Others"      ,String.valueOf(dOthers));

        

     }

     public void append(String SOrderNo , String SOrderDate , double dQty,
                        String SDueDate , double dDisc      , double dTax,String SCgstVal,String SSgstVal,String SIgstVal,String SCessVal,
                         double dNet)
     {
          HashMap theMap = new HashMap();

          theMap.put("OrderNo"     , SOrderNo);
          theMap.put("OrderDate"   , SOrderDate);
          theMap.put("Qty"         , String.valueOf(dQty));
          theMap.put("DueDate"     , SDueDate);
          theMap.put("Disc"        , String.valueOf(dDisc));
          theMap.put("Tax"         , String.valueOf(dTax));
          theMap.put("CgstVal"     ,SCgstVal);
          theMap.put("SgstVal"     ,SSgstVal);
          theMap.put("IgstVal"     ,SIgstVal);
          theMap.put("CessVal"     ,SCessVal);
          theMap.put("Net"         , String.valueOf(dNet));

          orderScheduleList.add(theMap);
     }
     public ArrayList getDeliveryScheduleList()
     {
          return orderScheduleList;
     }
     public String getBlockName()
     {
          return (String)theMap.get("BlockName");
     }
     public String getDesc()
     {
          return (String)theMap.get("Desc");
     }
     public String getUom()
     {
          return (String)theMap.get("Uom");
     }

     public String getMake()
     {
          return (String)theMap.get("Make");
     }

     public String getMrsNo()
     {
          return (String)theMap.get("MrsNo");
     }
     public String getMrsSlNo()
     {
          return (String)theMap.get("MrsSlNo");
     }
     public String getUnitCode()
     {
          return (String)theMap.get("UnitCode");
     }
     public String getDeptCode()
     {
          return (String)theMap.get("DeptCode");
     }
     public String getGroupCode()
     {
          return (String)theMap.get("GroupCode");
     }

     public String getMrsDate()
     {
       
          return (String)theMap.get("MrsDate");
     }
     public String getDueDate()
     {
          return (String)theMap.get("MrsDueDate");
     }

     public String getSupCode()
     {
          return (String)theMap.get("SupCode");
     }

     public String getPayTerms()
     {
          return (String)theMap.get("PayTerms");
     }
     public String getPayDays()
     {
          return (String)theMap.get("PayDays");
     }
     public String getToCode()
     {
          return (String)theMap.get("ToCode");
     }
     public String getThroCode()
     {
          return (String)theMap.get("ThroCode");
     }
     public String getPortCode()
     {
          return (String)theMap.get("PortCode");
     }
     public String getState()
     {
          return (String)theMap.get("State");
     }
     public String getTaxClaimable()
     {
          return (String)theMap.get("TaxClaimable");
     }
     public String getQty()
     {
          return (String)theMap.get("Qty");
     }
     public String getRate()
     {
          return (String)theMap.get("Rate");
     }
     public String getDiscPer()
     {
          return (String)theMap.get("DiscPer");
     }
                      
     public String  getDiscount()
     {
          double dDiscount =0;

          dDiscount = getGross()*(common.toDouble((String)theMap.get("DiscPer"))/100);

          return common.getRound(dDiscount,2);
     }

     private double getGross()
     {
          return common.toDouble((String)theMap.get("Qty"))*common.toDouble((String)theMap.get("Rate"));
     }
     public String getCenvatPer()
     {
          return (String)theMap.get("CenvatPer");
     }

      public String getCgstPer()
     {

     
          return (String)theMap.get("CgstPer");
     }
      public String getSgstPer()
     {
          return (String)theMap.get("SgstPer");
     }
      public String getIgstPer()
     {

           
          return (String)theMap.get("IgstPer");

     }
      public String getCessPer()
     {
          return (String)theMap.get("CessPer");
     }
     public String getCenvat()
     {
          double dCenvat =0;

          dCenvat =(getGross()-common.toDouble(getDiscount()))*(common.toDouble((String)theMap.get("CenvatPer"))/100);

          return common.getRound(dCenvat,2);
     }
     public String getTaxPer()
     {
          return (String)theMap.get("TaxPer");
     }

     public String getTax()
     {
          double dTax =0;

          dTax = (getGross()-common.toDouble(getDiscount())+common.toDouble(getCenvat()))*(common.toDouble((String)theMap.get("TaxPer"))/100);

//          dTax = (getGross())*(common.toDouble((String)theMap.get("TaxPer"))/100);

          return common.getRound(dTax,2);

     }
     public String getSurPer()
     {
          return (String)theMap.get("SurPer");
     }

     public String getSur()
     {
          double dSur =0;

          dSur = common.toDouble(getTax())*(common.toDouble((String)theMap.get("SurPer"))/100);

          return common.getRound(dSur,2);

     }
     public String getNet()
     {
          double dNet=0;

          dNet = getGross()-common.toDouble(getDiscount())+common.toDouble(getCenvat())+common.toDouble(getTax())+common.toDouble(getSur());

          return common.getRound(dNet,2);
     }
     public String getPlus()
     {
          return    (String)theMap.get("Plus");
     }
     public String getLess()
     {
          return    (String)theMap.get("Less");
     }
     public String getMisc()
     {
          return    (String)theMap.get("Misc");
     }
     public String getReference()
     {
          return (String)theMap.get("Reference");
     }
     public String getCatl()
     {
          return (String)theMap.get("Catl");
     }
     public String getDraw()
     {
          return (String)theMap.get("Draw");
     }
     public String getPrevOrderNo()
     {
          return (String)theMap.get("PrevOrderNo");

     }
     public String getPrevOrderBlock()
     {
          return (String)theMap.get("PrevOrderBlock");
     }

     public String getTotalQty()
     {
          double dQty =0;
          for(int i=0; i<orderScheduleList.size(); i++)
          {
               HashMap theMap = (HashMap)orderScheduleList.get(i);

               dQty = dQty+common.toDouble((String)theMap.get("Qty"));
          }
          return common.getRound(dQty,2);
     }
     public String getTotalDisc()
     {
          double dDisc =0;
          for(int i=0; i<orderScheduleList.size(); i++)
          {
               HashMap theMap = (HashMap)orderScheduleList.get(i);

               dDisc = dDisc+common.toDouble((String)theMap.get("Disc"));
          }
          return common.getRound(dDisc,2);
     }
     public String getTotalTax()
     {
          double dTax =0;
          for(int i=0; i<orderScheduleList.size(); i++)
          {
               HashMap theMap = (HashMap)orderScheduleList.get(i);

               dTax = dTax+common.toDouble((String)theMap.get("Tax"));
          }
          return common.getRound(dTax,2);
     }
     public String getTotalCenVat()
     {
          double dCenVat =0;
          for(int i=0; i<orderScheduleList.size(); i++)
          {
               HashMap theMap = (HashMap)orderScheduleList.get(i);

               dCenVat = dCenVat+common.toDouble((String)theMap.get("CenVat"));
          }
          return common.getRound(dCenVat,2);
     }

       public String getTotalCgst()
     {
          double dCgst =0;
          for(int i=0; i<orderScheduleList.size(); i++)
          {
               HashMap theMap = (HashMap)orderScheduleList.get(i);

               dCgst = dCgst+common.toDouble((String)theMap.get("CgstVal"));

          }
          return common.getRound(dCgst,2);
     }

       public String getTotalSgst()
     {
          double dSgst =0;
          for(int i=0; i<orderScheduleList.size(); i++)
          {
               HashMap theMap = (HashMap)orderScheduleList.get(i);

               dSgst = dSgst+common.toDouble((String)theMap.get("SgstVal"));
          }
          return common.getRound(dSgst,2);
     }


        public String getTotalIgst()
     {
          double dIgst =0;
          for(int i=0; i<orderScheduleList.size(); i++)
          {
               HashMap theMap = (HashMap)orderScheduleList.get(i);

               dIgst = dIgst+common.toDouble((String)theMap.get("IgstVal"));
          }
          return common.getRound(dIgst,2);
     }


        public String getTotalCess()
     {
          double dCess =0;
          for(int i=0; i<orderScheduleList.size(); i++)
          {
               HashMap theMap = (HashMap)orderScheduleList.get(i);

               dCess = dCess+common.toDouble((String)theMap.get("CessVal"));
          }
          return common.getRound(dCess,2);
     }


    
    
     public String getTotalSur()
     {
          double dSur =0;
          for(int i=0; i<orderScheduleList.size(); i++)
          {
               HashMap theMap = (HashMap)orderScheduleList.get(i);

               dSur = dSur+common.toDouble((String)theMap.get("Sur"));
          }
          return common.getRound(dSur,2);
     }
     public String getTotalNet()
     {
          double dNet =0;
          for(int i=0; i<orderScheduleList.size(); i++)
          {
               HashMap theMap = (HashMap)orderScheduleList.get(i);

               dNet = dNet+common.toDouble((String)theMap.get("Net"));
          }
          return common.getRound(dNet,2);
     }
     public String getOrderQty(String SOrderNo)
     {
          double dQty=0;
          for(int i=0; i<orderScheduleList.size(); i++)
          {
               HashMap theMap = (HashMap)orderScheduleList.get(i);

               if(((String)theMap.get("OrderNo")).equals(SOrderNo))
               {
                    dQty += common.toDouble((String)theMap.get("Qty"));
//                    break;

               }
          }
          if(dQty==0)
          return "";

          return common.getRound(dQty,2);
     }
}
