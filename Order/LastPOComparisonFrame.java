package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import javax.swing.table.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class LastPOComparisonFrame extends JInternalFrame
{
     JButton   BApply;
     JPanel    TopPanel,BottomPanel;
     JPanel    DatePanel,ApplyPanel;
     JPanel    MiddlePanel;
     
     TabReport tabreport,tabreport1;
     
     String SOrderNo="";
     String SOrderNo1="";
     
     NextField    TStNo,TEnNo;
     
     Object RowData[][];
     Object RowData1[][];
     
     String ColumnData1[] = {"Order No","Date","Block","Supplier","Code","Name"};
     String ColumnType1[] = {  "N"     , "S"  ,  "S"  ,   "S"    , "S"  , "S"  };   
     
     String ColumnData2[] = {"Qty","Rate","DiscPer","Disc","CenVatPer","CenVat","TaxPer","Tax","SurPer","Sur","Amount","Prev OrderNo","PrevDate","PrevBlock","PrevSupplier","PrevQty","PrevRate","PrevDiscPer","PrevDisc","PrevCenVatPer","PrevCenVat","PrevTaxPer","PrevTax","PrevSurPer","PrevSur","PrevAmount"};
     String ColumnType2[] = {"N"  , "N"  ,  "N"    , "N"  ,    "N"    ,  "N"   ,  "N"   , "N" ,  "N"   , "N" ,  "N"   ,     "N"      ,    "S"   ,    "S"    ,     "S"      ,   "N"   ,   "N"    ,     "N"     ,   "N"    ,      "N"      ,    "N"     ,    "N"     ,   "N"   ,   "N"      ,   "N"   ,    "N"     };
     
     Vector VOrdDate,VBlock,VBlockCode,VOrdNo,VMrsNo,VSupName,VOrdCode,VOrdName,VOrdDeptName,VOrdCataName,VOrdUnitName,VOrdConcern,VOrdQty,VOrdRate,VOrdNet,VOrdDue,VStatus;
     Vector VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup;
     Vector VDiscPer,VDisc,VCenVatPer,VCenVat,VTaxPer,VTax,VSurPer,VSur;
     
     Vector VPrevOrdDate,VPrevBlock,VPrevBlockCode,VPrevOrdNo,VPrevMrsNo,VPrevSupName,VPrevOrdCode,VPrevOrdName,VPrevOrdDeptName,VPrevOrdCataName,VPrevOrdUnitName,VPrevOrdConcern,VPrevOrdQty,VPrevOrdRate,VPrevOrdNet,VPrevOrdDue,VPrevStatus;
     Vector VPrevSelectedCode,VPrevSelectedName,VPrevSelectedMRS,VPrevSelectedQty,VPrevSelectedUnit,VPrevSelectedDept,VPrevSelectedGroup;
     Vector VPrevDiscPer,VPrevDisc,VPrevCenVatPer,VPrevCenVat,VPrevTaxPer,VPrevTax,VPrevSurPer,VPrevSur;
     
     Common common = new Common();
     JLayeredPane DeskTop;
     StatusPanel SPanel;
     Vector VCode,VName;
     int iMillCode;
     String SSupTable;
     
     public LastPOComparisonFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,String SSupTable)
     {
          super("Comparison With Previous PurchaseOrder");

          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.VCode     = VCode;
          this.VName     = VName;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TStNo          = new NextField();
          TEnNo          = new NextField();
          BApply         = new JButton("Apply");
          
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          MiddlePanel    = new JPanel();
          
          DatePanel      = new JPanel();
          ApplyPanel     = new JPanel();
     }
     
     public void setLayouts()
     {
          TopPanel       .setLayout(new GridLayout(1,3));
          
          DatePanel      .setLayout(new GridLayout(3,1));
          ApplyPanel     .setLayout(new BorderLayout());
          MiddlePanel    .setLayout(new GridLayout(1,2));
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          ApplyPanel     .add("Center",BApply);
          
          DatePanel.setBorder(new TitledBorder("Numbered"));
          DatePanel.removeAll();
          DatePanel.add(new Label("OrderNo"));
          DatePanel.add(TStNo);
          DatePanel.updateUI();
          
          TopPanel       .add(DatePanel);
          TopPanel       .add(ApplyPanel);
          
          DatePanel      .setBorder(new TitledBorder("Order No"));
          ApplyPanel     .setBorder(new TitledBorder("Control"));
          
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     
     public void addListeners()
     {
          BApply   .addActionListener(new ApplyList());
     }
     
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    MiddlePanel.remove(tabreport1);
                    MiddlePanel.remove(tabreport);
               }
               catch(Exception ex){}
               
               setDataIntoVector();
               setRowData1();
               
               setDataIntoVectorPrev();
               setRowData2();
          
               try
               {
                    tabreport1 = new TabReport(RowData,ColumnData1,ColumnType1);
                    
                    MiddlePanel.add(tabreport1);
                    setSelected(true);
                    DeskTop.repaint();
                    DeskTop.updateUI();
                    
                    tabreport1.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                    
                    tabreport = new TabReport(RowData1,ColumnData2,ColumnType2);
                    
                    tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                    tabreport.ReportTable.setGridColor(new Color( 0,225,225));
                    
                    DefaultTableCellRenderer cellRenderer1 = new DefaultTableCellRenderer();
                    cellRenderer1.setForeground( new Color(0,0,250));
                    
                    DefaultTableCellRenderer cellRenderer2 = new DefaultTableCellRenderer();
                    cellRenderer2.setForeground( new Color(250,0,0));
                    
                    for (int col=0;col<tabreport.ReportTable.getColumnCount();col++)
                    {
                         if(col>10)
                         {
                              tabreport.ReportTable.getColumn(ColumnData2[col]).setCellRenderer(cellRenderer1);
                         }
                         else
                         {                        
                              tabreport.ReportTable.getColumn(ColumnData2[col]).setCellRenderer(cellRenderer2);
                         }
                    }
                    MiddlePanel.add(tabreport);
                    
                    setSelected(true);
                    getContentPane().add(MiddlePanel,BorderLayout.CENTER);
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
          }
     }
     
     public void setDataIntoVector()
     {
          VOrdDate            = new Vector();
          VOrdNo              = new Vector();
          VBlock              = new Vector();
          VBlockCode          = new Vector();
          VMrsNo              = new Vector();
          VSupName            = new Vector();
          VOrdCode            = new Vector();
          VOrdName            = new Vector();
          VOrdDeptName        = new Vector();
          VOrdCataName        = new Vector();
          VOrdConcern         = new Vector();
          VOrdUnitName        = new Vector();
          VOrdQty             = new Vector();
          VOrdRate            = new Vector();
          VOrdNet             = new Vector();
          VOrdDue             = new Vector();
          VStatus             = new Vector();
          VDiscPer            = new Vector(); 
          VDisc               = new Vector(); 
          VCenVatPer          = new Vector(); 
          VCenVat             = new Vector(); 
          VTaxPer             = new Vector(); 
          VTax                = new Vector(); 
          VSurPer             = new Vector(); 
          VSur                = new Vector();
          VPrevOrdDate        = new Vector();
          VPrevOrdNo          = new Vector();
          VPrevBlock          = new Vector();
          VPrevBlockCode      = new Vector();
          VPrevMrsNo          = new Vector();
          VPrevSupName        = new Vector();
          VPrevOrdCode        = new Vector();
          VPrevOrdName        = new Vector();
          VPrevOrdDeptName    = new Vector();
          VPrevOrdCataName    = new Vector();
          VPrevOrdConcern     = new Vector();
          VPrevOrdUnitName    = new Vector();
          VPrevOrdQty         = new Vector();
          VPrevOrdRate        = new Vector();
          VPrevOrdNet         = new Vector();
          VPrevOrdDue         = new Vector();
          VPrevStatus         = new Vector();
          VPrevDiscPer        = new Vector(); 
          VPrevDisc           = new Vector(); 
          VPrevCenVatPer      = new Vector(); 
          VPrevCenVat         = new Vector(); 
          VPrevTaxPer         = new Vector(); 
          VPrevTax            = new Vector(); 
          VPrevSurPer         = new Vector(); 
          VPrevSur            = new Vector(); 
          

          String QString = " SELECT PurchaseOrder.OrderNo, PurchaseOrder.OrderDate, OrdBlock.BlockName, "+SSupTable+".Name, PurchaseOrder.MrsNo,"+
                           " PurchaseOrder.Item_Code, InvItems.Item_Name, Dept.Dept_Name, Cata.Group_Name, Unit.Unit_Name, "+
                           " PurchaseOrder.Qty, PurchaseOrder.Rate, PurchaseOrder.Net, PurchaseOrder.DueDate, PurchaseOrder.OrderBlock,PurchaseOrder.MillCode, PurchaseOrder.Unit_Code, "+
                           " PurchaseOrder.DiscPer,PurchaseOrder.Disc,PurchaseOrder.CenVatPer,PurchaseOrder.CenVat,PurchaseOrder.TaxPer,PurchaseOrder.Tax,"+
                           " PurchaseOrder.SurPer,PurchaseOrder.Sur,PurchaseOrder.InvQty,PurchaseOrder.SlNo "+
                           " FROM (((((PurchaseOrder INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block and PurchaseOrder.Qty>0 and PurchaseOrder.MillCode="+iMillCode+" and PurchaseOrder.OrderNo="+TStNo.getText()+") INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code) INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON PurchaseOrder.Dept_Code=Dept.Dept_Code) INNER JOIN Cata ON PurchaseOrder.Group_Code=Cata.Group_Code)  INNER JOIN Unit ON PurchaseOrder.Unit_Code=Unit.Unit_Code "+
                           " Order by PurchaseOrder.Item_Code ";

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    String str1    = res.getString(1);
                    String str2    = res.getString(2);
                    String str3    = res.getString(3);
                    String str4    = res.getString(4);
                    String str5    = res.getString(5);
                    String str6    = res.getString(6);
                    String str7    = res.getString(7);
                    String str8    = res.getString(8);
                    String str9    = res.getString(9);
                    String str10   = res.getString(10);
                    String str11   = res.getString(11);
                    String str12   = res.getString(12);
                    String str13   = res.getString(13);
                    String str14   = res.getString(14);
                    String str15   = res.getString(15);
                    String str16   = common.parseNull(res.getString(16));
                    str2           = common.parseDate(str2);
                    str14          = common.parseDate(str14);
                    
                    String str17   = res.getString(18);
                    String str18   = res.getString(19);
                    String str19   = res.getString(20);
                    String str20   = res.getString(21);
                    String str21   = res.getString(22);
                    String str22   = res.getString(23);
                    String str23   = res.getString(24);
                    String str24   = res.getString(25);
                    String str25   = res.getString(26);
                    String str26   = res.getString(27);
          
                    VOrdNo         .addElement(str1);
                    VOrdDate       .addElement(str2);
                    VBlock         .addElement(str3);
                    VSupName       .addElement(str4);
                    VMrsNo         .addElement(str5);
                    VOrdCode       .addElement(str6);
                    VOrdName       .addElement(str7);
                    VOrdDeptName   .addElement(common.parseNull(str8));
                    VOrdCataName   .addElement(common.parseNull(str9));
                    VOrdUnitName   .addElement(common.parseNull(str10));
                    VOrdQty        .addElement(str11);
                    VOrdRate       .addElement(str12);
                    VOrdNet        .addElement(str13);
                    VOrdDue        .addElement(str14);
                    VBlockCode     .addElement(str15);
                    VOrdConcern    .addElement(str16);
                    VStatus        .addElement(" ");
                    VDiscPer       .addElement(str17);
                    VDisc          .addElement(str18);
                    VCenVatPer     .addElement(str19);
                    VCenVat        .addElement(str20);
                    VTaxPer        .addElement(str21);
                    VTax           .addElement(str22);
                    VSurPer        .addElement(str23);
                    VSur           .addElement(str24);
               }
               res.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }
     
     public void setRowData1()
     {
          RowData     = new Object[VOrdDate.size()][6];
          
          for(int i=0;i<VOrdDate.size();i++)
          {
               RowData[i][0]  = (String)VOrdNo    .elementAt(i);
               RowData[i][1]  = (String)VOrdDate  .elementAt(i);
               RowData[i][2]  = (String)VBlock    .elementAt(i);
               RowData[i][3]  = (String)VSupName  .elementAt(i);
               RowData[i][4]  = (String)VOrdCode  .elementAt(i);
               RowData[i][5]  = (String)VOrdName  .elementAt(i);
          }
     }
     
     public void setRowData2()
     {
          RowData1     = new Object[VOrdDate.size()][26];
          for(int i=0;i<VOrdDate.size();i++)
          {
               if(VPrevOrdNo.size()>0)
               {
                    RowData1[i][0]      = (String)VOrdQty.elementAt(i);
                    RowData1[i][1]      = (String)VOrdRate.elementAt(i);
                    RowData1[i][2]      = (String)VDiscPer.elementAt(i);
                    RowData1[i][3]      = (String)VDisc.elementAt(i);
                    RowData1[i][4]      = (String)VCenVatPer.elementAt(i);
                    RowData1[i][5]      = (String)VCenVat.elementAt(i);
                    RowData1[i][6]      = (String)VTaxPer.elementAt(i);
                    RowData1[i][7]      = (String)VTax.elementAt(i);
                    RowData1[i][8]      = (String)VSurPer.elementAt(i);
                    RowData1[i][9]      = (String)VSur.elementAt(i);
                    RowData1[i][10]     = (String)VOrdNet.elementAt(i);
                    
                    
                    RowData1[i][11]     = common.parseNull((String)VPrevOrdNo.elementAt(i));
                    RowData1[i][12]     = (String)VPrevOrdDate.elementAt(i);
                    RowData1[i][13]     = (String)VPrevBlock.elementAt(i);
                    RowData1[i][14]     = (String)VPrevSupName.elementAt(i);
                    RowData1[i][15]     = (String)VPrevOrdQty.elementAt(i);
                    RowData1[i][16]     = (String)VPrevOrdRate.elementAt(i);
                    RowData1[i][17]     = (String)VPrevDiscPer.elementAt(i);
                    RowData1[i][18]     = (String)VPrevDisc.elementAt(i);
                    RowData1[i][19]     = (String)VPrevCenVatPer.elementAt(i);
                    RowData1[i][20]     = (String)VPrevCenVat.elementAt(i);
                    RowData1[i][21]     = (String)VPrevTaxPer.elementAt(i);
                    RowData1[i][22]     = (String)VPrevTax.elementAt(i);
                    RowData1[i][23]     = (String)VPrevSurPer.elementAt(i);
                    RowData1[i][24]     = (String)VPrevSur.elementAt(i);
                    RowData1[i][25]     = (String)VPrevOrdNet.elementAt(i);
               }
          }  
     }
     
     public void setDataIntoVectorPrev()
     {
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
          
               for(int i=0;i<RowData.length;i++)
               {
                    boolean bflag = true;

                    String SItemCode    = (String)VOrdCode.elementAt(i);
                    String SDate        = common.pureDate((String)VOrdDate.elementAt(i));
                    String QString      = getPrevOrderNoQS(SItemCode,SDate);
                    ResultSet res       = stat.executeQuery(QString);
                    while (res.next())
                    {
                         SOrderNo  = res.getString(1);
                    }
                    res.close();
                    if(common.toInt(SOrderNo)==0)
                    {
                         bflag = false;
                         QString = getPrevOrderNoQS1(SItemCode,SDate);
                         res = stat.executeQuery(QString);
                         while (res.next())
                         {
                              SOrderNo  = res.getString(1);
                         }
                         res.close();
                    }
                    if(common.toInt(SOrderNo)>0)
                    {
                         String QString1 = getPrevOrderQS1(SItemCode,SDate,SOrderNo,bflag);
                         res  = stat.executeQuery(QString1);
                         while (res.next())
                         {
                              String SConcern="";
                              SOrderNo1  = common.parseNull(res.getString(1));
                              String str1=SOrderNo1;
                              String str2    = res.getString(2);
                              String str3    = res.getString(3);
                              String str4    = res.getString(4);
                              String str5    = res.getString(5);
                              String str6    = res.getString(6);
                              String str7    = res.getString(7);
                              String str8    = res.getString(8);
                              String str9    = res.getString(9);
                              String str10   = res.getString(10);
                              String str11   = res.getString(11);
                              String str12   = res.getString(12);
                              String str13   = res.getString(13);
                              String str14   = res.getString(14);
                              String str15   = res.getString(15);
                              String str16   = common.parseNull(res.getString(16));
                                     str2    = common.parseDate(str2);
                                     str14   = common.parseDate(str14);
                              String str17   = res.getString(18);
                              String str18   = res.getString(19);
                              String str19   = res.getString(20);
                              String str20   = res.getString(21);
                              String str21   = res.getString(22);
                              String str22   = res.getString(23);
                              String str23   = res.getString(24);
                              String str24   = res.getString(25);
                              String str25   = res.getString(26);
                              String str26   = res.getString(27);

                              VPrevOrdNo          .addElement(str1);
                              VPrevOrdDate        .addElement(str2);
                              VPrevBlock          .addElement(str3);
                              VPrevSupName        .addElement(str4);
                              VPrevMrsNo          .addElement(str5);
                              VPrevOrdCode        .addElement(str6);
                              VPrevOrdName        .addElement(str7);
                              VPrevOrdDeptName    .addElement(common.parseNull(str8));
                              VPrevOrdCataName    .addElement(common.parseNull(str9));
                              VPrevOrdUnitName    .addElement(common.parseNull(str10));
                              VPrevOrdQty         .addElement(str11);
                              VPrevOrdRate        .addElement(str12);
                              VPrevOrdNet         .addElement(str13);
                              VPrevOrdDue         .addElement(str14);
                              VPrevBlockCode      .addElement(str15);
                              VPrevOrdConcern     .addElement(str16);
                              VPrevStatus         .addElement(" ");
                              VPrevDiscPer        .addElement(str17);
                              VPrevDisc           .addElement(str18);
                              VPrevCenVatPer      .addElement(str19);
                              VPrevCenVat         .addElement(str20);
                              VPrevTaxPer         .addElement(str21);
                              VPrevTax            .addElement(str22);
                              VPrevSurPer         .addElement(str23);
                              VPrevSur            .addElement(str24);
                         }
                         res.close();
                    }
                    else
                    {
                         VPrevOrdNo          .addElement("");
                         VPrevOrdDate        .addElement("");
                         VPrevBlock          .addElement("");
                         VPrevSupName        .addElement("");
                         VPrevMrsNo          .addElement("");
                         VPrevOrdCode        .addElement("");
                         VPrevOrdName        .addElement("");
                         VPrevOrdDeptName    .addElement("");
                         VPrevOrdCataName    .addElement("");
                         VPrevOrdUnitName    .addElement("");
                         VPrevOrdQty         .addElement("");
                         VPrevOrdRate        .addElement("");
                         VPrevOrdNet         .addElement("");
                         VPrevOrdDue         .addElement("");
                         VPrevBlockCode      .addElement("");
                         VPrevOrdConcern     .addElement("");
                         VPrevStatus         .addElement("");
                         VPrevDiscPer        .addElement("");
                         VPrevDisc           .addElement("");
                         VPrevCenVatPer      .addElement("");
                         VPrevCenVat         .addElement("");
                         VPrevTaxPer         .addElement("");
                         VPrevTax            .addElement("");
                         VPrevSurPer         .addElement("");
                         VPrevSur            .addElement("");
                    }
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private String getPrevOrderNoQS(String SItemCode,String SDate)
     {
          String QS = "";
          try
          {
               QS = " Select Max(OrderNo) from PurchaseOrder Where Item_Code='"+SItemCode+"' and OrderDate = ( "+
                    " Select Max(OrderDate) from PurchaseOrder Where  OrderDate < '"+SDate+"' and Item_Code='"+SItemCode+"' ) ";
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
          
          return QS;
     }
     private String getPrevOrderNoQS1(String SItemCode,String SDate)
     {
          String QS = "";
          try
          {
               QS = " Select Max(OrderNo) from PYOrder Where Item_Code='"+SItemCode+"' and OrderDate = ( "+
                    " Select Max(OrderDate) from PYOrder Where  OrderDate < '"+SDate+"' and Item_Code='"+SItemCode+"' ) ";
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
          
          return QS;
     }

     private String getPrevOrderQS1(String SItemCode,String SDate,String SOrderNo,boolean bflag)
     {
          String QS = "";
          try
          {
               if(bflag)
               {
                    QS = " SELECT PurchaseOrder.OrderNo, PurchaseOrder.OrderDate, OrdBlock.BlockName, "+SSupTable+".Name, PurchaseOrder.MrsNo,"+
                         " PurchaseOrder.Item_Code, InvItems.Item_Name, Dept.Dept_Name, Cata.Group_Name, Unit.Unit_Name, "+
                         " PurchaseOrder.Qty, PurchaseOrder.Rate, PurchaseOrder.Net, PurchaseOrder.DueDate, PurchaseOrder.OrderBlock,PurchaseOrder.MillCode, PurchaseOrder.Unit_Code, "+
                         " PurchaseOrder.DiscPer,PurchaseOrder.Disc,PurchaseOrder.CenVatPer,PurchaseOrder.CenVat,PurchaseOrder.TaxPer,PurchaseOrder.Tax,"+
                         " PurchaseOrder.SurPer,PurchaseOrder.Sur,PurchaseOrder.InvQty,PurchaseOrder.SlNo "+
                         " FROM (((((PurchaseOrder INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block) INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code) INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code) Left JOIN Dept ON PurchaseOrder.Dept_Code=Dept.Dept_Code) Left JOIN Cata ON PurchaseOrder.Group_Code=Cata.Group_Code)  INNER JOIN Unit ON PurchaseOrder.Unit_Code=Unit.Unit_Code "+
                         " Where PurchaseOrder.OrderDate < '"+SDate+"' and PurchaseOrder.Item_Code='"+SItemCode+"' and PurchaseOrder.OrderNo="+SOrderNo+
                         " and PurchaseOrder.SlNo=(Select max(SlNo) from PurchaseOrder Where PurchaseOrder.OrderDate < '"+SDate+"' and PurchaseOrder.Item_Code='"+SItemCode+"' and PurchaseOrder.OrderNo="+SOrderNo+" )";
               }
               else
               {
                    QS = " SELECT PYOrder.OrderNo, PYOrder.OrderDate, OrdBlock.BlockName, "+SSupTable+".Name, PYOrder.MrsNo,"+
                         " PYOrder.Item_Code, InvItems.Item_Name, Dept.Dept_Name, Cata.Group_Name, Unit.Unit_Name, "+
                         " PYOrder.Qty, PYOrder.Rate, PYOrder.Net, PYOrder.DueDate, PYOrder.OrderBlock,PYOrder.MillCode, PYOrder.Unit_Code, "+
                         " PYOrder.DiscPer,PYOrder.Disc,PYOrder.CenVatPer,PYOrder.CenVat,PYOrder.TaxPer,PYOrder.Tax,"+
                         " PYOrder.SurPer,PYOrder.Sur,PYOrder.InvQty,PYOrder.SlNo "+
                         " FROM (((((PYOrder INNER JOIN OrdBlock ON PYOrder.OrderBlock=OrdBlock.Block) INNER JOIN "+SSupTable+" ON PYOrder.Sup_Code="+SSupTable+".Ac_Code) INNER JOIN InvItems ON PYOrder.Item_Code=InvItems.Item_Code) Left JOIN Dept ON PYOrder.Dept_Code=Dept.Dept_Code) Left JOIN Cata ON PYOrder.Group_Code=Cata.Group_Code)  INNER JOIN Unit ON PYOrder.Unit_Code=Unit.Unit_Code "+
                         " Where PYOrder.OrderDate < '"+SDate+"' and PYOrder.Item_Code='"+SItemCode+"' and PYOrder.OrderNo="+SOrderNo+
                         " and PYOrder.Id=(Select max(Id) from PYOrder Where PYOrder.OrderDate < '"+SDate+"' and PYOrder.Item_Code='"+SItemCode+"' and PYOrder.OrderNo="+SOrderNo+" )";
               }


          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
          return QS;
     }
}
