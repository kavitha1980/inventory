package Order;

import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class MailOrder
{
     Vector VCode,VName,VUOM,VQty,VRate;
     Vector VDiscPer,VCenVatPer,VTaxPer,VSurPer;
     Vector VBasic,VDisc,VCenVat,VTax,VSur,VNet;
     Vector VDesc,VMake,VCatl,VDraw;
     String SPayTerm = "";
     String SReference = "";
     String SDueDate = "";
     String SOthers  = "";
     String SMRSNo   = "";
     String SMRSDate = "";

     double dOthers=0;
     String SAddr1="",SAddr2="",SAddr3="";
     double dTDisc=0,dTCenVat=0,dTTax=0,dTSur=0,dTNet=0;
     FileWriter FW;
     String SOrdNo,SBlock,SBlockCode,SOrdDate,SSupCode,SSupName,SForm;
     String SToName="",SThroName="";
     String SDocName="",SDocAddr1="",SDocAddr2="";
     Common common = new Common();
     int Lctr      = 100;
     int Pctr      = 0;

     MailOrder(FileWriter FW,String SOrdNo,String SBlock,String SBlockCode,String SOrdDate,String SSupCode,String SSupName) throws Exception
     {
          this.FW         = FW;
          this.SOrdNo     = SOrdNo;
          this.SBlock     = SBlock;
          this.SBlockCode = SBlockCode;
          this.SOrdDate   = SOrdDate;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          try
          {
               getAddr();
               setDataIntoVector();
               setOrderHead();
               setOrderBody();
               setOrderValueFoot();
               setOrderFoot();
               setMiscFoot();
          }
          catch(Exception ex){}
     }

     public void setDataIntoVector() throws Exception
     {
          VCode      = new Vector();
          VName      = new Vector();
          VUOM       = new Vector();
          VQty       = new Vector();
          VRate      = new Vector();
          VDiscPer   = new Vector();
          VCenVatPer = new Vector();
          VTaxPer    = new Vector();
          VSurPer    = new Vector();
          VBasic     = new Vector();
          VDisc      = new Vector();
          VCenVat    = new Vector();
          VTax       = new Vector();
          VSur       = new Vector();
          VNet       = new Vector();
          VDesc      = new Vector();
          VMake      = new Vector();
          VCatl      = new Vector();
          VDraw      = new Vector();

           try
           {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();

              String QString = getQString();
              ResultSet res  = stat.executeQuery(QString);
              while (res.next())
              {
                 String str1  = res.getString(1);  
                 String str2  = res.getString(2);
                 String str3  = res.getString(3);
                 String str4  = res.getString(4);
                 String str5  = res.getString(5);
                 String str6  = res.getString(6);
                 String str7  = res.getString(7);
                 String str8  = res.getString(8);
                 String str9  = res.getString(9);
                 String str10 = res.getString(10);
                 String str11 = res.getString(11);
                 String str12 = res.getString(12);
                 String str13 = res.getString(13);
                 SToName      = res.getString(14);
                 SThroName    = res.getString(15);
                 String str14 = res.getString(16);
                 String str15 = res.getString(17);
                 String str16 = res.getString(18);
                 String str17 = res.getString(19);
                 String str18 = res.getString(20);
                 SPayTerm     = res.getString(21);
                 SDueDate     = common.parseDate(res.getString(22));
                 dOthers      = common.toDouble(res.getString(23));
                 SReference   = res.getString(24);
                 SMRSNo       = res.getString(25);
                 SForm        = res.getString(26);


                 VCode.addElement(str1);
                 VName.addElement(str2);
                 VQty.addElement(str3);
                 VRate.addElement(str4);
                 VDiscPer.addElement(str5);
                 VDisc.addElement(str6);
                 VCenVatPer.addElement(str7);
                 VCenVat.addElement(str8);
                 VTaxPer.addElement(str9);
                 VTax.addElement(str10);
                 VNet.addElement(str11);
                 VSurPer.addElement(str12);
                 VSur.addElement(str13);
                 VUOM.addElement(str14);
                 VDesc.addElement(str15);
                 VMake.addElement(str16);
                 VCatl.addElement(str17);
                 VDraw.addElement(str18);
              }

              ResultSet res1 = stat.executeQuery("Select MRSDate From MRS Where MRSNo="+SMRSNo);
              while(res1.next())
                  SMRSDate = common.parseDate(res1.getString(1));

           }
           catch(Exception ex){System.out.println(ex);}
     }
     public String getQString()
     {
          String QString  = "";
     
          QString = " SELECT PurchaseOrder.Item_Code, InvItems.Item_Name, PurchaseOrder.Qty, PurchaseOrder.Rate, PurchaseOrder.DiscPer, PurchaseOrder.Disc, PurchaseOrder.CenVatPer, PurchaseOrder.Cenvat, PurchaseOrder.TaxPer, PurchaseOrder.Tax, PurchaseOrder.Net,PurchaseOrder.SurPer,PurchaseOrder.Sur,BookTo.ToName,BookThro.ThroName,UoM.UoMName,MatDesc.Descr,MatDesc.Make,InvItems.Catl,InvItems.Draw,PurchaseOrder.PayTerms,PurchaseOrder.DueDate,(PurchaseOrder.Plus-PurchaseOrder.Less) as Others,PurchaseOrder.Reference,PurchaseOrder.MRSNo,StForm.FormNo "+
                    " FROM ((((PurchaseOrder INNER JOIN InvItems ON PurchaseOrder.Item_Code = InvItems.Item_Code) "+
                    " Inner Join BookTo   On BookTo.ToCode = PurchaseOrder.ToCode) "+
                    " Inner Join UoM on UoM.UoMCode = InvItems.UomCode"+
                    " Inner Join BookThro On BookThro.ThroCode = PurchaseOrder.ThroCode) "+
                    " Inner Join STForm On STForm.FormCode = PurchaseOrder.FormCode) "+
                    " Left Join MatDesc On MatDesc.OrderNo = PurchaseOrder.OrderNo and MatDesc.Item_Code = PurchaseOrder.Item_Code And MatDesc.SlNo = PurchaseOrder.SlNo "+
                    " Where PurchaseOrder.Qty > 0 And PurchaseOrder.OrderNo = "+SOrdNo+" and PurchaseOrder.OrderBlock = "+SBlockCode+
                    " Order By 1";
          System.out.println(QString);
          return QString;
     }
     public void getAddr() throws Exception
     {
           String QS = "Select Supplier.Addr1,Supplier.Addr2,Supplier.Addr3,Place.PlaceName "+
                       "From Supplier LEFT Join Place On Place.PlaceCode = Supplier.City_Code "+
                       "Where Supplier.Ac_Code = '"+SSupCode+"'";
           try
           {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();

                 ResultSet res  = stat.executeQuery(QS);
                 while (res.next())
                 {
                        SAddr1 = res.getString(1);
                        SAddr2 = res.getString(2);
                        SAddr3 = common.parseNull(res.getString(3));
                 }
           }
           catch(Exception ex){System.out.println(ex);}
     }

     private void setOrderHead() throws Exception
     {

        FW.write("<html>"+"\n");
        
        FW.write("<head>"+"\n");
        FW.write("<title>Amarjothi Spinning Mills Ltd</title>"+"\n");
        FW.write("</head>"+"\n");
        
        FW.write("<body bgcolor='#FFFFCC'>"+"\n");
        
        FW.write("<p align='center'><b><font color='#000066'><font size='6'>Amarjothi Spinning Mills Ltd</font><font size='5'><br></font>Purchase Order</font></b></p>"+"\n");
        FW.write("<div align='center'>"+"\n");
        FW.write("  <center>"+"\n");
        FW.write("  <table border='1' width='100%' bgcolor='#CCFFFF'>"+"\n");
        FW.write("    <tr>"+"\n");
        FW.write("      <td width='50%' rowspan='4' valign='top'><font color='#000066'>To<br>"+"\n");
        FW.write("        "+SSupName+"<br>"+"\n");
        FW.write("        "+SAddr1+"<br>"+"\n");
        FW.write("        "+SAddr2+"<br>"+"\n");
        FW.write("        "+SAddr3+"</font>"+"\n");
        FW.write("        <p>&nbsp;</td>"+"\n");
        FW.write("      <td width='13%' ><font color='#000066'>Order No</font></td>"+"\n");
        FW.write("      <td width='37%' ><font color='#000066'>"+SBlock+"-"+SOrdNo+" Dt. "+SOrdDate+"</font></td>"+"\n");
        FW.write("    </tr>"+"\n");
        FW.write("    <tr>"+"\n");
        FW.write("      <td width='84' height='10'><font color='#000066'>Book To</font></td>"+"\n");
        FW.write("      <td width='241' height='10'><font color='#000066'>"+SToName+"</font></td>"+"\n");
        FW.write("    </tr>"+"\n");
        FW.write("    <tr>"+"\n");
        FW.write("      <td width='84' height='1'><font color='#000066'>Book Thro</font></td>"+"\n");
        FW.write("      <td width='241' height='1'><font color='#000066'>"+SThroName+"</font></td>"+"\n");
        FW.write("    </tr>"+"\n");
        FW.write("  </table>"+"\n");
        FW.write("  </center>"+"\n");
        FW.write("</div>"+"\n");
     }
     private void setOrderBody() throws Exception
     {
          FW.write("<table border='1' width='100%' bgcolor='#FFCCFF'>"+"\n");
          FW.write("  <tr>"+"\n");
          FW.write("    <td width='10%'><font color='#000066'>Material Code</font></td>"+"\n");
          FW.write("    <td width='30%'><font color='#000066'>Material Description</font></td>"+"\n");
          FW.write("    <td width='7%'>"+"\n");
          FW.write("      <p align='center'><font color='#000066'>UoM</font></td>"+"\n");
          FW.write("    <td width='10%' align='right'><font color='#000066'>Quantity</font></td>"+"\n");
          FW.write("    <td width='10%' align='right'><font color='#000066'>Rate</font></td>"+"\n");
          FW.write("    <td width='10%' align='right'><font color='#000066'>Discount</font></td>"+"\n");
          FW.write("    <td width='10%' align='right'><font color='#000066'>CenVat</font></td>"+"\n");
          FW.write("    <td width='10%' align='right'><font color='#000066'>Tax</font></td>"+"\n");
          FW.write("    <td width='10%' align='right'><font color='#000066'>Surcharge</font></td>"+"\n");
          FW.write("    <td width='10%' align='right'><font color='#000066'>Net</font></td>"+"\n");
          FW.write("  </tr>"+"\n");
          FW.write("  <tr>"+"\n"); 
          FW.write("    <td width='40%' colspan='4'>&nbsp;</td>"+"\n");
          FW.write("    <td width='10%' align='right'><font color='#000066'>Rs</font></td>"+"\n");
          FW.write("    <td width='10%' align='right'><font color='#000066'>Rs &amp; %</font></td>"+"\n");
          FW.write("    <td width='10%' align='right'><font color='#000066'>Rs &amp; %</font></td>"+"\n");
          FW.write("    <td width='10%' align='right'><font color='#000066'>Rs &amp; %</font></td>"+"\n");
          FW.write("    <td width='10%' align='right'><font color='#000066'>Rs &amp; %</font></td>"+"\n");
          FW.write("    <td width='10%' align='right'><font color='#000066'>Rs </font></td>"+"\n");
          FW.write("  </tr>"+"\n");
          for(int i=0;i<VCode.size();i++)
          {
               String SCode     = (String)VCode.elementAt(i);
               String SUOM      = (String)VUOM.elementAt(i);
               String SName     = (String)VName.elementAt(i);
               String SQty      = common.getRound((String)VQty.elementAt(i),2);
               String SRate     = common.getRound((String)VRate.elementAt(i),2);
               String SDisc     = common.getRound((String)VDisc.elementAt(i),2);
               String SDiscPer  = common.getRound((String)VDiscPer.elementAt(i),2)+"%";
               String SCenVat   = common.getRound((String)VCenVat.elementAt(i),2);
               String SCenVatPer= common.getRound((String)VCenVatPer.elementAt(i),2)+"%";
               String STax      = common.getRound((String)VTax.elementAt(i),2);
               String STaxPer   = common.getRound((String)VTaxPer.elementAt(i),2)+"%";
               String SSur      = common.getRound((String)VSur.elementAt(i),2);
               String SSurPer   = common.getRound((String)VSurPer.elementAt(i),2)+"%";
               String SNet      = common.getRound((String)VNet.elementAt(i),2);
               String SDesc     = common.parseNull((String)VDesc.elementAt(i));
               String SMake     = ""+common.parseNull((String)VMake.elementAt(i));
               String SCatl     = ""+common.parseNull((String)VCatl.elementAt(i));
               String SDraw     = ""+common.parseNull((String)VDraw.elementAt(i));

               SMake = (SMake=="null"?"":SMake);
               SCatl = (SCatl=="null"?"":SCatl);
               SDraw = (SDraw=="null"?"":SDraw);
               SDesc = SDesc.trim();

               dTDisc   = dTDisc+common.toDouble(SDisc);
               dTCenVat = dTCenVat+common.toDouble(SCenVat);
               dTTax    = dTTax   +common.toDouble(STax);
               dTSur    = dTSur   +common.toDouble(SSur);
               dTNet    = dTNet   +common.toDouble(SNet);

               SDesc = SDesc+"-"+SMake+" "+SCatl+" "+SDraw;

                  FW.write("<tr>"+"\n");
                  FW.write("  <td width='10%' bgcolor='#FFCCFF'><font color='#000066'>"+(String)VCode.elementAt(i)+"</font></td>"+"\n");
                  FW.write("  <td width='30%' bgcolor='#FFCCFF'><font color='#000066'><b>"+(String)VName.elementAt(i)+"</b></font></td>"+"\n");
                  FW.write("  <td width='7%'  bgcolor='#FFCCFF'><font color='#000066'>"+(String)VUOM.elementAt(i)+"</font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#FFCCFF'><font color='#000066'>"+SQty+"</font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#FFCCFF'><font color='#000066'>"+SRate+"</font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#FFCCFF'><font color='#000066'>"+SDisc+"</font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#FFCCFF'><font color='#000066'>"+SCenVat+"</font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#FFCCFF'><font color='#000066'>"+STax+"</font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#FFCCFF'><font color='#000066'>"+SSur+"</font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#FFCCFF'><font color='#000066'>"+SNet+"</font></td>"+"\n");
                  FW.write("</tr>"+"\n");

                  FW.write("<tr>"+"\n");
                  FW.write("  <td width='10%' bgcolor='#CCCCFF'><font color='#000066'></font></td>"+"\n");
                  FW.write("  <td width='30%' bgcolor='#CCCCFF'><font color='#000066'>"+SDesc+"</font></td>"+"\n");
                  FW.write("  <td width='7%'  bgcolor='#CCCCFF'><font color='#000066'></font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'></font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'></font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'>"+SDiscPer+"</font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'>"+SCenVatPer+"</font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'>"+STaxPer+"</font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'>"+SSurPer+"</font></td>"+"\n");
                  FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'></font></td>"+"\n");
                  FW.write("</tr>"+"\n");
          }

     }

     private void setOrderValueFoot() throws Exception
     {
            String SDisc   = common.getRound(dTDisc,2);
            String SCenVat = common.getRound(dTCenVat,2);
            String STax    = common.getRound(dTTax,2);
            String SSur    = common.getRound(dTSur,2);
            String SNet    = common.getRound(dTNet,2);
            String SOthers = common.getRound(dOthers,2);
            String SGrand  = common.getRound(dTNet+dOthers,2);

            FW.write("<tr>"+"\n");
            FW.write("  <td width='10%' bgcolor='#CCCCFF'><font color='#000066'></font></td>"+"\n");
            FW.write("  <td width='30%' align='right' bgcolor='#CCCCFF'><font color='#000066'><b>Gross</b></font></td>"+"\n");
            FW.write("  <td width='7%'  bgcolor='#CCCCFF'><font color='#000066'></font></td>"+"\n");
            FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'></font></td>"+"\n");
            FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'></font></td>"+"\n");
            FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'>"+SDisc+"</font></td>"+"\n");
            FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'>"+SCenVat+"</font></td>"+"\n");
            FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'>"+STax+"</font></td>"+"\n");
            FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'>"+SSur+"</font></td>"+"\n");
            FW.write("  <td width='10%' align='right' bgcolor='#CCCCFF'><font color='#000066'>"+SNet+"</font></td>"+"\n");
            FW.write("</tr>"+"\n");

            FW.write("<tr>"+"\n");
            FW.write("  <td width='90%' colspan='9'>"+"\n");
            FW.write("    <p align='right'><font color='#000066'>Freight/Rounding Off</font></td>"+"\n");
            FW.write("  <td width='10%' align='right'><font color='#000066'>"+SOthers+"</font></td>"+"\n");
            FW.write("</tr>"+"\n");

            FW.write("<tr>"+"\n");
            FW.write("  <td width='90%' colspan='9'>"+"\n");
            FW.write("    <p align='right'><font color='#000066'>Order Value</font></td>"+"\n");
            FW.write("  <td width='10%' align='right'><font color='#000066' size='5'><b>"+SGrand+"</b></font></td>"+"\n");
            FW.write("</tr>"+"\n");

            FW.write("</table>"+"\n");
     }
     private void setOrderFoot() throws Exception
     {
          FW.write("<table border='1' width='100%' bgcolor='#009999'>"+"\n");
          FW.write("  <tr>"+"\n");
          FW.write("    <td width='16%' rowspan='2'><font color='#000066'>Date of Delivery</font></td>"+"\n");
          FW.write("    <td width='16%' rowspan='2'><font color='#000066'>Terms of Payment</font></td>"+"\n");
          FW.write("    <td width='17%' rowspan='2'><font color='#000066'>Form Type</font></td>"+"\n");
          FW.write("    <td width='17%' rowspan='2'><font color='#000066'>Reference</font></td>"+"\n");
          FW.write("    <td width='17%' colspan='2'>"+"\n");
          FW.write("      <p align='center'><font color='#000066'>MRS</font></td>"+"\n");
          FW.write("  </tr>"+"\n");

          FW.write("  <tr>"+"\n");
          FW.write("    <td width='8%'>"+"\n");
          FW.write("      <p align='right'><font color='#000066'>No</font></td>"+"\n");
          FW.write("    <td width='9%'><font color='#000066'>Date</font></td>"+"\n");
          FW.write("  </tr>"+"\n");

          FW.write("    <tr>"+"\n");
          FW.write("      <td width='16%'><font color='#000066'>"+SDueDate+"</font></td>"+"\n");
          FW.write("      <td width='16%'><font color='#000066'>"+SPayTerm+"</font></td>"+"\n");
          FW.write("      <td width='17%'><font color='#000066'>"+SForm+"</font></td>"+"\n");
          FW.write("      <td width='17%'><font color='#000066'>"+SReference+"</font></td>"+"\n");
          FW.write("      <td width='8%'>"+"\n");
          FW.write("        <p align='right'><font color='#000066'>"+SMRSNo+"</font></td>"+"\n");
          FW.write("      <td width='9%'><font color='#000066'>"+SMRSDate+"</font></td>"+"\n");
          FW.write("    </tr>"+"\n");
          FW.write("  </table>"+"\n");
     }
     private void setMiscFoot() throws Exception
     {
          FW.write("  <table border='1' width='100%' height='79' bgcolor='#FF99CC'>"+"\n");
          FW.write("    <tr>"+"\n");
          FW.write("      <td width='100%' height='19'><font color='#000066'>Notes</font></td>"+"\n");
          FW.write("    </tr>"+"\n");
          FW.write("    <tr>"+"\n");
          FW.write("      <td width='100%' valign='top' height='48'><font color='#000066'>1. Please"+"\n");
          FW.write("        Mention the Order No and Material Code in all Correspondence with us. <br>"+"\n");
          FW.write("        2. Please Send Invoices in triplicate along with the consignment. <br>"+"\n");
          FW.write("        3. We reserve the right to increase or decrease the order quantity /"+"\n");
          FW.write("        cancel the order without assigning any&nbsp;&nbsp; reason. <br>"+"\n");
          FW.write("        4. Any legal dispute shall be within the jurisdiction of Tirupur.</font></td>"+"\n");
          FW.write("    </tr>"+"\n");
          FW.write("  </table>"+"\n");
          FW.write("  <table border='1' width='100%' bgcolor='#9999FF'>"+"\n");
          FW.write("    <tr>"+"\n");
          FW.write("      <td width='50%'><font color='#000066'>Address For Communication</font></td>"+"\n");
          FW.write("      <td width='50%'><font color='#000066'>Central Excise and other Details.</font></td>"+"\n");
          FW.write("    </tr>"+"\n");
          FW.write("    <tr>"+"\n");
          FW.write("      <td width='50%'><font size='2' color='#000066'>Pudusuripalayam, <br>"+"\n");
          FW.write("        Nambiyur - 638 458<br>"+"\n");
          FW.write("        Erode Dist,Tamil Nadu.<br>"+"\n");
          FW.write("        e-Mail :<a href='http://'> amarjoth@eth.net</a><br>"+"\n");
          FW.write("        Phone : (04285) 267201,267301 <br>"+"\n");
          FW.write("        Fax&nbsp;&nbsp;&nbsp; : (04285) 267565 </font></td>"+"\n");
          FW.write("      <td width='50%' valign='top'><font size='2' color='#000066'>CST No : 440691"+"\n");
          FW.write("        dt 24.09.1990<br>"+"\n");
          FW.write("        TNGST No :"+"\n");
          FW.write("        2960864&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; IAC No"+"\n");
          FW.write("        :    <br>"+"\n");
          FW.write("        CEX Reg &amp; ECC No : AADFA 6924 A XM 001<br>"+"\n");
          FW.write("        PLA No : 59/93&nbsp; Range - Gobichettipalayam<br>"+"\n");
          FW.write("        Division : Erode<br>"+"\n");
          FW.write("        Commissionerate :&nbsp; Coimbatore</font></td>"+"\n");
          FW.write("    </tr>"+"\n");
          FW.write("  </table>"+"\n");
          FW.write("  </body>"+"\n");
          FW.write("  </html>"+"\n");
     }
}
