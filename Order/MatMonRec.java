package Order;

import java.util.*;

public class MatMonRec
{
     String SMatCode     = "";
     String SMatName     = "";
     Vector VQty,VValue;

     public MatMonRec()
     {
          VQty      = new Vector();
          VValue    = new Vector();
          for(int i=0;i<12;i++)
          {
               VQty      . addElement("0");
               VValue    . addElement("0");
          }
     }

     public void setQty(String SValue,int iMon)
     {
          iMon = ((iMon>=4 && iMon<=12) ? iMon-4:iMon+8);
          VQty . setElementAt(SValue,iMon);
     }

    public void setValue(String SValue,int iMon)
    {
        iMon        = ((iMon>=4 && iMon<=12) ? iMon-4:iMon+8);
        VValue      . setElementAt(SValue,iMon);
    }
}
