// Consolidated Order Placement Utility From MRS //

package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;
import java.io.*;

public class MatReqFrame extends JInternalFrame
{
      JLayeredPane  Layer;
      Vector        VCode,VName;
      StatusPanel   SPanel;

      String        SStDate,SEnDate;
      JButton       BApply,BAOrder,BMOrder;
      DateField     TStDate;
      DateField     TEnDate;
      JPanel        TopPanel,BottomPanel;
      Connection    theConnection = null;

      TabReport tabreport;

      Object RowData[][];

      String ColumnData[] = {"Code","Material Name","Required (from MRS)","Stock","Pending @ Order","ROL","To be Ordered","Applicable"};
      String ColumnType[] = {"S"   ,"S"            ,"N"                  ,"N"    ,"N"              ,"N"  ,"N"            ,"B"         };

      Vector VMCode,VMName,VMReq,VMStock,VMPending,VMROL,VMAdvice;
      Vector VSelectedId,VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedDue,VSelectedDesc,VSelectedBlock,VSelectedMRSSlNo;

      Vector VSelectedColor,VSelectedSet,VSelectedSize,VSelectedSide,VSelectedSlipNo,VSelectedBookNo,VSelectedSlipFrNo,VSelectedCatl,VSelectedDraw,VSelectedMake,VSelectedUserCode,VSelectedMrsType;

      Vector VPendingItem_Code,VPendingQty;

      Common common = new Common();

      int iUserCode;
      int iMillCode;
      String SYearCode;
      String SItemTable,SSupTable;


      public MatReqFrame(JLayeredPane Layer,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable)
      {
            super("Consolidated Material Requirment against pending MRS");
            this.Layer      = Layer;
            this.VCode      = VCode;
            this.VName      = VName;
            this.SPanel     = SPanel;
            this.iUserCode  = iUserCode;
            this.iMillCode  = iMillCode;
            this.SYearCode  = SYearCode;
            this.SItemTable = SItemTable;
            this.SSupTable  = SSupTable;

            createComponents();
            setLayouts();
            addComponents();
            addListeners();
      }

      public void createComponents()
      {
         TStDate  = new DateField();
         TEnDate  = new DateField();
         BApply   = new JButton("Apply");
         TopPanel = new JPanel();

         BottomPanel = new JPanel();
         BMOrder      = new JButton("Place Order Manually");
         BAOrder      = new JButton("Place Order Automatically");

         TStDate.setTodayDate();
         TEnDate.setTodayDate();
      }

      public void setLayouts()
      {
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,650,500);
      }

      public void addComponents()
      {
            TopPanel.add(new JLabel("Start Date"));
            TopPanel.add(TStDate);
            TopPanel.add(new JLabel("End Date"));
            TopPanel.add(TEnDate);
            TopPanel.add(BApply);
   
            BottomPanel.add(BMOrder);
            BottomPanel.add(BAOrder);
   
            getContentPane().add(TopPanel,BorderLayout.NORTH);
            getContentPane().add(BottomPanel,BorderLayout.SOUTH);
      }

      public void addListeners()
      {
            BApply.addActionListener(new ApplyList());
            BMOrder.addActionListener(new ActList());
            BAOrder.addActionListener(new AutoList());
      }

      public class ApplyList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setVectorData();
                  setRowData();
                  try
                  {
                        getContentPane().remove(tabreport); 
                  }
                  catch(Exception ex){}

                  try
                  {
                        tabreport = new TabReport(RowData,ColumnData,ColumnType);
                        getContentPane().add(tabreport,BorderLayout.CENTER);
                        setSelected(true);
                        Layer.repaint();
                        Layer.updateUI();
                  }
                  catch(Exception ex)
                  {
                        System.out.println(ex);
                  }
            }
      }

      public void setVectorData()
      {
            VMCode     = new Vector();
            VMName     = new Vector();
            VMReq      = new Vector();
            VMStock    = new Vector();
            VMPending  = new Vector();
            VMROL      = new Vector();
            VMAdvice   = new Vector();

            VPendingItem_Code = new Vector();
            VPendingQty       = new Vector();


            SStDate = TStDate.toNormal();
            SEnDate = TEnDate.toNormal();

            String QS="";

              if(iMillCode==0)
                   QS =  " Select MRS.Item_code,Item_Name,Sum(Qty) as Qty, "+
                         " Sum(InvItems.OpgQty+InvItems.RecQty-InvITems.IssQty) as Stock,0 as PendingQty,"+
                         " Sum(invitems.ROQ-invitems.MinQty) as Rol"+
                         " from MRS"+
                         " Inner join InvItems on InvItems.Item_Code=MRs.Item_Code"+
                         " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MrsDate>="+SStDate+" and MRSDate <="+SEnDate+"  and Mrs.OrderNo=0 and MRS.Qty>0  and MRS.MillCode="+iMillCode+
                         " and Mrs.AutoOrderConversion=0 and Mrs.YearlyPlanningStatus=0 "+
                         " Group By MRS.Item_code,Item_Name ";
              else
                   QS =  " Select MRS.Item_code,Item_Name,Sum(Qty) as Qty, "+
                         " Sum("+SItemTable+".OpgQty+"+SItemTable+".RecQty-"+SItemTable+".IssQty) as Stock,0 as PendingQty,"+
                         " Sum("+SItemTable+".ROQ-"+SItemTable+".MinQty) as Rol"+
                         " from MRS"+
                         " Inner join "+SItemTable+" on "+SItemTable+".Item_Code=MRs.Item_Code"+
                         " Inner join InvItems on InvItems.Item_Code="+SItemTable+".Item_Code"+
                         " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MrsDate>="+SStDate+" and MRSDate <="+SEnDate+"  and Mrs.OrderNo=0 and MRS.Qty>0  and MRS.MillCode="+iMillCode+
                         " and Mrs.AutoOrderConversion=0 and Mrs.YearlyPlanningStatus=0 "+
                         " Group By MRS.Item_code,Item_Name ";

              String QS1="";

                    QS1 = " Select Item_Code,Sum(Qty)-Sum(InvQty) From PurchaseOrder Where MillCode="+iMillCode+" Group By Item_Code ";


            try
            {
                    if(theConnection == null)
                    {
                         ORAConnection jdbc   = ORAConnection.getORAConnection();
                         theConnection        = jdbc.getConnection();
                    }
     
                    Statement theStatement    = theConnection.createStatement();

                  ResultSet res = theStatement.executeQuery(QS);
                  while(res.next())
                  {
                        String str1 = res.getString(1);
                        String str2 = res.getString(2);
                        double dReq    = common.toDouble(res.getString(3));
                        double dStock  = common.toDouble(res.getString(4));
                        double dPending= common.toDouble(res.getString(5));
                        double dROL    = common.toDouble(res.getString(6));
                        double dAdvice = dReq-dStock-dPending+dROL;
                        String SAdvice = (dAdvice <0 ?"Not Necessary":""+dAdvice);

                        VMCode     .addElement(str1);
                        VMName     .addElement(str2);
                        VMReq      .addElement(""+dReq);
                        VMStock    .addElement(""+dStock);
                        VMPending  .addElement(""+dPending);
                        VMROL      .addElement(""+dROL);
                        VMAdvice   .addElement(common.getRound(SAdvice,0));
                  }
                  res.close();
                  res = theStatement.executeQuery(QS1);
                  while(res.next())
                  {
                        VPendingItem_Code.addElement(res.getString(1));
                        VPendingQty.addElement(res.getString(2));
                  }
                  res.close();
                  theStatement.close();

            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }
      public void setRowData()
      {
            RowData = new Object[VMCode.size()][ColumnData.length];
            for(int i=0;i<VMCode.size();i++)
            {

                  double dReq   = common.toDouble((String)VMReq.elementAt(i));
                  double dStk   = common.toDouble((String)VMStock.elementAt(i));
                  double dRol   = common.toDouble((String)VMROL.elementAt(i));
                  double dPen   = common.toDouble(getPendingQty((String)VMCode.elementAt(i)));

                  RowData[i][0] = (String)VMCode.elementAt(i);                  
                  RowData[i][1] = (String)VMName.elementAt(i);
                  RowData[i][2] = (String)VMReq.elementAt(i);
                  RowData[i][3] = (String)VMStock.elementAt(i);
                  RowData[i][4] = getPendingQty((String)VMCode.elementAt(i));
                  RowData[i][5] = (String)VMROL.elementAt(i);
                  RowData[i][6] = String.valueOf(((dReq+dRol)-(dStk+dPen)));
                  RowData[i][7] = new Boolean(false);
            }
      }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
              setSelectedRecords();
              OrderPlanFrame orderplanframe = new OrderPlanFrame(Layer,VCode,VName,VSelectedId,VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedDue,VSelectedDesc,SPanel,iUserCode,iMillCode,VSelectedBlock,VSelectedMRSSlNo,VSelectedColor,VSelectedSet,VSelectedSize,VSelectedSide,VSelectedSlipNo,VSelectedBookNo,VSelectedSlipFrNo,VSelectedCatl,VSelectedDraw,VSelectedMake,VSelectedUserCode,VSelectedMrsType,SYearCode,SItemTable,SSupTable);
              try
              {
                   removeOrderFrame(); 
                   Layer.add(orderplanframe);
                   Layer.repaint();
                   orderplanframe.setSelected(true);
                   Layer.updateUI();
                   orderplanframe.show();
              }
              catch(Exception ex)
              {
                 System.out.println(ex);
              }
          }
     }
     public class AutoList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
              setSelectedRecords();
              removeOrderFrame();                                                                                                                                                                                                                              
              OrderPooling orderpooling = new OrderPooling(Layer,VCode,VName,VSelectedId,VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedDue,VSelectedDesc,SPanel,iUserCode,iMillCode,VSelectedBlock,VSelectedMRSSlNo,VSelectedColor,VSelectedSet,VSelectedSize,VSelectedSide,VSelectedSlipNo,VSelectedBookNo,VSelectedSlipFrNo,VSelectedCatl,VSelectedDraw,VSelectedMake,VSelectedUserCode,VSelectedMrsType,SYearCode,SItemTable,SSupTable);
          }
     }
     public void removeOrderFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }
     public void setSelectedRecords()
     {
            VSelectedId   = new Vector();
            VSelectedCode = new Vector();
            VSelectedName = new Vector();
            VSelectedMRS  = new Vector();
            VSelectedQty  = new Vector();
            VSelectedUnit = new Vector();
            VSelectedDept = new Vector();
            VSelectedGroup= new Vector();
            VSelectedDue  = new Vector();
            VSelectedDesc = new Vector();
            VSelectedBlock= new Vector();
            VSelectedMRSSlNo= new Vector();


            VSelectedColor = new Vector();
            VSelectedSet   = new Vector();
            VSelectedSize  = new Vector();
            VSelectedSide  = new Vector();
            VSelectedSlipNo= new Vector();
            VSelectedBookNo= new Vector();

            VSelectedSlipFrNo = new Vector();
            VSelectedCatl     = new Vector();
            VSelectedDraw     = new Vector();
            VSelectedMake     = new Vector();
            VSelectedUserCode = new Vector();
            VSelectedMrsType  = new Vector();


            try
            {
                    if(theConnection == null)
                    {
                         ORAConnection jdbc   = ORAConnection.getORAConnection();
                         theConnection        = jdbc.getConnection();
                    }
     
                    Statement theStatement    = theConnection.createStatement();
                  for(int i=0;i<RowData.length;i++)
                  {
                        Boolean bAppl = (Boolean)RowData[i][7];

                        if(!bAppl.booleanValue())
                              continue;

                        double dQty    = common.toDouble(((String)RowData[i][6]).trim());
                        double dMRSQty = 0;
                        if(dQty<=0)
                              continue;

                        String SCode = (String)RowData[i][0];
                        String SName = (String)RowData[i][1];

                        String QS=""; 
                               if(iMillCode==0)
                               {
                                    QS = "Select MRS.MRSNo,MRS.Id,MRS.Qty,Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,DueDate,Mrs.Remarks,BlockName,MRS.SLNo, "+
                                         " InvItems.PaperColor,InvItems.PaperSets,InvItems.PaperSize,InvItems.PaperSide, "+
                                         " MRS.SlipToNo,InvItems.LastBookNo+1,MRS.SlipFromNo,MRS.Catl,MRS.Draw,MRS.Make,MRS.MrsAuthUserCode, "+
                                         " decode(greatest(nvl(mrs.planmonth,0)),0,(decode(greatest(nvl(mrs.yearlyplanningstatus,0)),0,0,2)),1) as mrstype "+
                                         " From ((MRS Inner Join Unit On Unit.Unit_Code = MRS.Unit_Code) "+
                                         " Inner Join Dept On Dept.Dept_Code = MRS.Dept_Code) "+
                                         " Inner Join Cata On Cata.Group_Code = MRS.Group_Code "+
                                         " Inner join OrdBlock on OrdBlock.Block=MRS.BlockCode "+
                                         " Inner Join InvItems On InvItems.Item_Code=Mrs.Item_Code "+
                                         " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MRS.OrderNo = 0 and MRS.Qty >0 and MRS.Item_Code = '"+SCode+"' and MRs.MRSDate >= '"+SStDate+"' and MRS.MRSDate <= '"+SEnDate+"' and MRS.MillCode="+iMillCode+" and Mrs.AutoOrderConversion=0 and Mrs.YearlyPlanningStatus=0 ";
                               }
                               else
                               {
                                    QS = "Select MRS.MRSNo,MRS.Id,MRS.Qty,Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,DueDate,Mrs.Remarks,BlockName,MRS.SLNo, "+
                                         " InvItems.PaperColor,InvItems.PaperSets,InvItems.PaperSize,InvItems.PaperSide, "+
                                         " MRS.SlipToNo,"+SItemTable+".LastBookNo+1,MRS.SlipFromNo,MRS.Catl,MRS.Draw,MRS.Make,MRS.MrsAuthUserCode, "+
                                         " decode(greatest(nvl(mrs.planmonth,0)),0,(decode(greatest(nvl(mrs.yearlyplanningstatus,0)),0,0,2)),1) as mrstype "+
                                         " From ((MRS Inner Join Unit On Unit.Unit_Code = MRS.Unit_Code) "+
                                         " Inner Join Dept On Dept.Dept_Code = MRS.Dept_Code) "+
                                         " Inner Join Cata On Cata.Group_Code = MRS.Group_Code "+
                                         " Inner join OrdBlock on OrdBlock.Block=MRS.BlockCode "+
                                         " Inner Join "+SItemTable+" On "+SItemTable+".Item_Code=Mrs.Item_Code "+
                                         " Inner Join InvItems On InvItems.Item_Code=Mrs.Item_Code "+
                                         " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MRS.OrderNo = 0 and MRS.Qty >0 and MRS.Item_Code = '"+SCode+"' and MRs.MRSDate >= '"+SStDate+"' and MRS.MRSDate <= '"+SEnDate+"' and MRS.MillCode="+iMillCode+" and Mrs.AutoOrderConversion=0 and Mrs.YearlyPlanningStatus=0 ";
                               }

                        ResultSet res = theStatement.executeQuery(QS);
                        while(res.next())
                        {
                              String SQty = res.getString(3);
                              VSelectedId   .addElement(res.getString(2));
                              VSelectedCode .addElement(SCode);
                              VSelectedName .addElement(SName);
                              VSelectedMRS  .addElement(res.getString(1));
                              VSelectedQty  .addElement(SQty);
                              VSelectedUnit .addElement(res.getString(4));
                              VSelectedDept .addElement(res.getString(5));
                              VSelectedGroup.addElement(res.getString(6));
                              VSelectedDue  .addElement(common.parseDate(res.getString(7)));
                              VSelectedDesc .addElement(res.getString(8));
                              VSelectedBlock.addElement(res.getString(9));
                              VSelectedMRSSlNo.addElement(res.getString(10));
                              dMRSQty = dMRSQty+common.toDouble(SQty);

                              VSelectedColor.addElement(res.getString(11));
                              VSelectedSet.addElement(res.getString(12));
                              VSelectedSize.addElement(res.getString(13));
                              VSelectedSide.addElement(res.getString(14));
                              VSelectedSlipNo.addElement(res.getString(15));
                              VSelectedBookNo.addElement(res.getString(16));
                              VSelectedSlipFrNo .addElement(res.getString(17));
                              VSelectedCatl     .addElement(res.getString(18));
                              VSelectedDraw     .addElement(res.getString(19));
                              VSelectedMake     .addElement(res.getString(20));
                              VSelectedUserCode .addElement(res.getString(21));
                              VSelectedMrsType  .addElement(res.getString(22));
                    

                              if(dMRSQty >= dQty)
                                    break;
                        }
                        res.close();

                        try
                        {
                              double dDiff = dMRSQty-dQty;
                              int index    = VSelectedQty.size()-1;
                              dDiff        = common.toDouble((String)VSelectedQty.elementAt(index))-dDiff;
                              VSelectedQty.setElementAt(String.valueOf(dDiff),index);
                         }
                         catch(Exception ex)
                         {
                              System.out.println(ex);
                              ex.printStackTrace();
                         }
                  }
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
     }
     private String getPendingQty(String SItem_Code)
     {
          int iIndex=-1;
          iIndex = VPendingItem_Code.indexOf(SItem_Code);
          if(iIndex!=-1)
               return common.parseNull((String)VPendingQty.elementAt(iIndex));
          else
               return "0";
     }
}
