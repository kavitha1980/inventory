// Consolidated Order Placement Utility From MRS //

package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;
import java.io.*;

public class MatReqMRSFrameGst extends JInternalFrame
{
      JLayeredPane  Layer;
      Vector        VCode,VName;
      StatusPanel   SPanel;

      String        SStDate,SEnDate;
      JButton       BApply,BAOrder,BMOrder;
      DateField     TStDate;
      DateField     TEnDate;
      JPanel        TopPanel,BottomPanel;
      Connection    theConnection = null;

      JComboBox JCOrder,JCFilter;

      AlterTabReport tabreport;

      Object RowData[][];

      String ColumnData[] = {"MRSDate","MRSNo","Code","Material Name","Qty","Unit","Department","Group","DueDate","Applicable","SupName","PurchaseDate","PurchaseRate","DiscPer","HSNCode"};
      String ColumnType[] = {"S"      ,"N"    ,"S"   ,"S"            ,"N"  ,"S"   ,"S"         ,"S"    ,"S"      ,"B"         ,"S"      ,"S"           ,"S"           ,"S"    ,"S"  };

      Vector VMCode,VMName,VMHsnCode,VMHsnType,VMQty,VMUnit,VMDept,VMGroup,VMDueDate,VMMRS,VMMRSSlNo,VMMRSDate,
            VSupplier,VPurchaseDate,VPurchaseRate,VDiscPer,VCenvatPer,VTaxPer,VSuPer,VNetRate;

      Vector VSelectedId,VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedDue,VSelectedDesc,VSelectedBlock,VSelectedMRSSlNo,VSelectedHsnCode,VSelectedHsnType;

      Vector VMMRSId,VMMRSDesc,VMMRSBlock,VMMRSColor,VMMRSSet,VMMRSSize,VMMRSSide;
      Vector VMMRSSlipNo,VMMRSBookNo,VMMRSSlipFrNo,VMMRSCatl,VMMRSDraw,VMMRSMake,VMMRSUserCode,VMAutoOrderStatus,VMMRSType;

      Vector VSelectedColor,VSelectedSet,VSelectedSize,VSelectedSide,VSelectedSlipNo,VSelectedBookNo,VSelectedSlipFrNo,VSelectedCatl,VSelectedDraw,VSelectedMake,VSelectedUserCode,VSelectedMrsType;

      Common common = new Common();

      int iUserCode;
      int iMillCode;
      String SYearCode;
      String SItemTable,SSupTable;

      public MatReqMRSFrameGst(JLayeredPane Layer,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable)
      {
            super(" Order For MRS");
            this.Layer      = Layer;
            this.VCode      = VCode;
            this.VName      = VName;
            this.SPanel     = SPanel;
            this.iUserCode  = iUserCode;
            this.iMillCode  = iMillCode;
            this.SYearCode  = SYearCode;
            this.SItemTable = SItemTable;
            this.SSupTable  = SSupTable;

            createComponents();
            setLayouts();
            addComponents();
            addListeners();
      }

      public void createComponents()
      {
         TStDate  = new DateField();
         TEnDate  = new DateField();
         BApply   = new JButton("Apply");
         TopPanel = new JPanel();

         JCOrder  = new JComboBox();
         JCFilter = new JComboBox();

         BottomPanel = new JPanel();
         BMOrder      = new JButton("Place Order Manually");
         BAOrder      = new JButton("Place Order Automatically");

         TStDate.setTodayDate();
         TEnDate.setTodayDate();
      }

      public void setLayouts()
      {
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,650,500);
      }

      public void addComponents()
      {

            JCOrder.addItem("Materialwise");
            JCOrder.addItem("Groupwise");
            JCOrder.addItem("Departmentwise");
            JCOrder.addItem("MRS No");
            JCOrder.addItem("SupplierWise");

            JCFilter.addItem("All Pendings");
            JCFilter.addItem("Over-Due");

            TopPanel.add(new JLabel("Sorted On"));
            TopPanel.add(JCOrder);
            TopPanel.add(new JLabel("List Only"));
            TopPanel.add(JCFilter);


            TopPanel.add(new JLabel("Start Date"));
            TopPanel.add(TStDate);
            TopPanel.add(new JLabel("End Date"));
            TopPanel.add(TEnDate);
            TopPanel.add(BApply);
   
            BottomPanel.add(BMOrder);
            BottomPanel.add(BAOrder);
   
            getContentPane().add(TopPanel,BorderLayout.NORTH);
            getContentPane().add(BottomPanel,BorderLayout.SOUTH);
      }

      public void addListeners()
      {
            BApply.addActionListener(new ApplyList());
            BMOrder.addActionListener(new ActList());
            BAOrder.addActionListener(new AutoList());
      }

      public class ApplyList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setVectorData();
                  setRowData();
                  try
                  {
                        getContentPane().remove(tabreport); 
                  }
                  catch(Exception ex){}

                  try
                  {
                        tabreport = new AlterTabReport(RowData,ColumnData,ColumnType,VMAutoOrderStatus);
                        getContentPane().add(tabreport,BorderLayout.CENTER);
                        setSelected(true);
                        Layer.repaint();
                        Layer.updateUI();
                  }
                  catch(Exception ex)
                  {
                        System.out.println(ex);
                  }
            }
      }

      public void setVectorData()
      {
            VMMRSDate  = new Vector();
            VMMRS      = new Vector();
            VMCode     = new Vector();
            VMHsnCode  = new Vector();
            VMHsnType  = new Vector();
            VMName     = new Vector();
            VMQty      = new Vector();
            VMUnit     = new Vector();
            VMDept     = new Vector();
            VMGroup    = new Vector();
            VMDueDate  = new Vector();
            VMMRSSlNo  = new Vector();


            VMMRSId    = new Vector();
            VMMRSDesc  = new Vector();
            VMMRSBlock = new Vector();
            
            VMMRSColor = new Vector();
            VMMRSSet   = new Vector();
            VMMRSSize  = new Vector();
            VMMRSSide  = new Vector();
            VMMRSSlipNo= new Vector();
            VMMRSBookNo= new Vector();
            VMMRSSlipFrNo = new Vector();
            VMMRSCatl     = new Vector();
            VMMRSDraw     = new Vector();
            VMMRSMake     = new Vector();
            VMMRSUserCode = new Vector();
            VMAutoOrderStatus = new Vector();
            VMMRSType     = new Vector();


            VSupplier     = new Vector();
            VPurchaseDate = new Vector();
            VPurchaseRate = new Vector();
            VDiscPer      = new Vector();


            SStDate = TStDate.toNormal();
            SEnDate = TEnDate.toNormal();

            DateField df  = new DateField();
            df.setTodayDate();
            String SToday = df.toNormal();

          String QSItem = " Create Table Temp1MrsItem as ("+
                          " Select distinct(Item_Code) from Mrs "+
                          " Where (MrsFlag=0 or DeptMrsAuth=1) and MrsDate>="+SStDate+" and MRSDate <="+SEnDate+"  and OrderNo=0 and Qty>0 and YearlyPlanningStatus=0 and MillCode="+iMillCode+")";

          String QSTemp   = " Create table Temp1MrsOrder as("+
                        " Select max(OrderDate) as OrderDate,Item_Code from("+
                        " select  max(OrderDate) as OrderDate,Item_Code"+
                        " From PurchaseOrder "+
                        " Where Item_Code in (Select Item_Code from Temp1MrsItem) "+
                        " Group by Item_Code,Id,MRsNo,OrderNo"+
                        " Union"+
                        " select  max(OrderDate) as OrderDate,Item_Code"+
                        " From PYOrder "+
                        " Where Item_Code in (Select Item_Code from Temp1MrsItem) "+
                        " Group by Item_Code) group by Item_Code) ";


          String QSTemp1 ="   create table TempMrsOrder22 as("+
                   "  select max(Id) as Id,OrderDate,      Item_Code from("+
                   "  select max(PYOrder.ID) as Id,Temp1MRsOrder.OrderDate as OrderDate,Temp1MrsOrder.Item_Code as Item_code from PYOrder"+
                   "  Inner join Temp1MRsOrder on Temp1MrsOrder.OrderDate=PYOrder.OrderDate"+
                   "  and Temp1MrsOrder.Item_Code=PYOrder.Item_code"+
                   "  group by temp1MrsOrder.Orderdate,Temp1MRsOrder.Item_Code"+
                   "  union All"+
                   "  select Max(Purchaseorder.Id) as Id,Temp1MRsOrder.OrderDate,Temp1MrsOrder.Item_Code as Item_Code from PurchaseOrder"+
                   "  Inner join Temp1MrsOrder on  Temp1MRsOrder.OrderDate=PurchaseOrder.OrderDate"+
                   "  and Temp1MrsOrder.Item_Code=PurchaseOrder.Item_code"+
                   "  group by temp1MrsOrder.Orderdate,Temp1MRsOrder.Item_Code) group by OrderDate,Item_Code) ";



          String QSTemp2 = " create table Temp2MRSOrder as ("+
                     " Select PYOrder.OrderDate as OrderDate,PYOrder.Item_Code, "+
                     " Supplier.Name,PYOrder.Rate,PYOrder.DiscPer "+
                     " From PYOrder Inner Join Supplier On Supplier.Ac_Code = PyOrder.Sup_Code "+
                     " Inner join TempMRSOrder22 on TempMRSOrder22.OrderDate= PYOrder.Orderdate and TempMRSOrder22.Id=PYOrder.Id "+
                     " and TempMRSOrder22.Item_Code=PYOrder.Item_Code "+
                     " Where PYOrder.Qty > 0 And PYOrder.Net > 0 "+
                     " Union All "+
                     " Select PurchaseOrder.OrderDate as OrderDate,PurchaseOrder.Item_Code, "+
                     " Supplier.Name,PurchaseOrder.Rate,PurchaseOrder.DiscPer "+
                     " From PurchaseOrder Inner Join Supplier On Supplier.Ac_Code = PurchaseOrder.Sup_Code "+
                     " Inner join TempMRSOrder22 on TempMRSOrder22.OrderDate= PurchaseOrder.Orderdate and TempMRSOrder22.Id=PurchaseOrder.Id"+
                     " and TempMRSOrder22.Item_Code=PurchaseOrder.Item_Code"+
                     " Where PurchaseOrder.Qty > 0 And PurchaseOrder.Net > 0) ";


            String QS="";
                   if(iMillCode==0)
                        QS =  " Select MRSDate,MRSNO,MRS.Item_code,Item_Name,MRS.Qty,Unit_Name,Dept_Name,Group_Name,DueDate,MRS.SLNO, "+
                              " MRS.ID,Mrs.Remarks,BlockName, "+
                              " InvItems.PaperColor,InvItems.PaperSets,InvItems.PaperSize,InvItems.PaperSide, "+
                              " MRS.SlipToNo,InvItems.LastBookNo+1,MRS.SlipFromNo,MRS.Catl,MRS.Draw,MRS.Make,MRS.MrsAuthUserCode "+
                              " ,Temp2MRsOrder.OrderDate as LastPurchaseDate,Temp2MrsOrder.Rate as LastPurchaseRate,"+
                              " Temp2MrsOrder.DiscPer,Temp2MrsOrder.Name,Mrs.AutoOrderConversion, "+
                              " decode(greatest(nvl(mrs.planmonth,0)),0,(decode(greatest(nvl(mrs.yearlyplanningstatus,0)),0,0,2)),1) as mrstype,"+
			      " nvl(mrs.enquiryno,0) as enqno,InvItems.HsnCode,InvItems.HsnType "+
                              " from MRS                                                "+
                              " Inner join InvItems on InvItems.Item_Code=MRs.Item_Code "+
                              " LEFT JOIN Temp2MRSOrder on Temp2MRSOrder.ITem_Code=MRS.Item_code"+
                              " Inner join Unit on unit.Unit_Code=MRS.Unit_COde   "+
                              " Inner join Dept on Dept.Dept_Code=MRS.Dept_COde   "+
                              " Inner join Cata on Cata.Group_Code=MRS.Group_COde "+
                              " Inner join OrdBlock on OrdBlock.Block=MRS.BlockCode "+
                              " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MrsDate>="+SStDate+" and MRSDate <="+SEnDate+"  and Mrs.OrderNo=0 and MRS.Qty>0 and MRS.YearlyPlanningStatus=0 and MRS.MillCode="+iMillCode+" ";
                   else
                        QS =  " Select MRSDate,MRSNO,MRS.Item_code,Item_Name,MRS.Qty,Unit_Name,Dept_Name,Group_Name,DueDate,MRS.SLNO, "+
                              " MRS.ID,Mrs.Remarks,BlockName, "+
                              " InvItems.PaperColor,InvItems.PaperSets,InvItems.PaperSize,InvItems.PaperSide, "+
                              " MRS.SlipToNo,"+SItemTable+".LastBookNo+1,MRS.SlipFromNo,MRS.Catl,MRS.Draw,MRS.Make,MRS.MrsAuthUserCode "+
                              " ,Temp2MRsOrder.OrderDate as LastPurchaseDate,Temp2MrsOrder.Rate as LastPurchaseRate,"+
                              " Temp2MrsOrder.DiscPer,Temp2MrsOrder.Name,Mrs.AutoOrderConversion, "+
                              " decode(greatest(nvl(mrs.planmonth,0)),0,(decode(greatest(nvl(mrs.yearlyplanningstatus,0)),0,0,2)),1) as mrstype,"+
			      " nvl(mrs.enquiryno,0) as enqno,InvItems.HsnCode,InvItems.HsnType "+
                              " from MRS                                                "+
                              " Inner Join "+SItemTable+" On "+SItemTable+".Item_Code=Mrs.Item_Code "+
                              " Inner join InvItems on InvItems.Item_Code=MRs.Item_Code "+
                              " LEFT JOIN Temp2MRSOrder on Temp2MRSOrder.ITem_Code=MRS.Item_code"+
                              " Inner join Unit on unit.Unit_Code=MRS.Unit_COde   "+
                              " Inner join Dept on Dept.Dept_Code=MRS.Dept_COde   "+
                              " Inner join Cata on Cata.Group_Code=MRS.Group_COde "+
                              " Inner join OrdBlock on OrdBlock.Block=MRS.BlockCode "+
                              " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and MrsDate>="+SStDate+" and MRSDate <="+SEnDate+"  and Mrs.OrderNo=0 and MRS.Qty>0 and MRS.YearlyPlanningStatus=0 and MRS.MillCode="+iMillCode+" ";

     
          if(JCFilter.getSelectedIndex()==1)
               QS = QS+" and Mrs.DueDate <= '"+SToday+"'";

          if(JCOrder.getSelectedIndex() == 0)      
               QS = QS+" Order By InvItems.Item_Name,Mrs.MrsDate";
          if(JCOrder.getSelectedIndex() == 1)
               QS = QS+" Order By Cata.Group_Name,Mrs.MrsDate";
          if(JCOrder.getSelectedIndex() == 2)      
               QS = QS+" Order By Dept.Dept_Name,Mrs.MrsDate";
          if(JCOrder.getSelectedIndex() == 3)      
               QS = QS+" Order By Mrs.MrsNo,Mrs.MrsDate";

          if(JCOrder.getSelectedIndex() == 4)      
               QS = QS+" Order By Temp2MrsOrder.Name,Mrs.MrsNo,Mrs.MrsDate";


            try
            {
                    if(theConnection == null)
                    {
                         ORAConnection jdbc   = ORAConnection.getORAConnection();
                         theConnection        = jdbc.getConnection();
                    }
     
                    Statement theStatement    = theConnection.createStatement();

               try{theStatement.execute("Drop Table Temp1MRSItem");}catch(Exception ex){}
               try{theStatement.execute("Drop Table Temp1MRSOrder");}catch(Exception ex){}
               try{theStatement.execute("Drop Table TempMRSOrder22");}catch(Exception ex){}
               try{theStatement.execute("Drop Table Temp2MRSOrder");}catch(Exception ex){}

               theStatement.execute(QSItem);
               theStatement.execute(QSTemp);
               theStatement.execute(QSTemp1);
               theStatement.execute(QSTemp2);

                  System.out.println("QueryQS-->"+QS);
				  ResultSet res = theStatement.executeQuery(QS);
                  while(res.next())
                  {
                        VMMRSDate  .addElement(res.getString(1));
                        VMMRS      .addElement(res.getString(2));
                        VMCode     .addElement(res.getString(3));
                        VMName     .addElement(res.getString(4));
                        VMQty      .addElement(res.getString(5));
                        VMUnit     .addElement(res.getString(6));
                        VMDept     .addElement(res.getString(7));
                        VMGroup    .addElement(res.getString(8));
                        VMDueDate  .addElement(res.getString(9));
                        VMMRSSlNo  .addElement(res.getString(10));

                        VMMRSId    .addElement(res.getString(11));
                        VMMRSDesc  .addElement(res.getString(12));
                        VMMRSBlock .addElement(res.getString(13));

                        VMMRSColor.addElement(res.getString(14));
                        VMMRSSet.addElement(res.getString(15));
                        VMMRSSize.addElement(res.getString(16));
                        VMMRSSide.addElement(res.getString(17));
                        VMMRSSlipNo.addElement(res.getString(18));
                        VMMRSBookNo.addElement(res.getString(19));
                        VMMRSSlipFrNo .addElement(res.getString(20));
                        VMMRSCatl     .addElement(res.getString(21));
                        VMMRSDraw     .addElement(res.getString(22));
                        VMMRSMake     .addElement(res.getString(23));
                        VMMRSUserCode .addElement(res.getString(24));

                        VPurchaseDate.addElement(common.parseDate(common.parseNull((String)res.getString(25))));
                        VPurchaseRate.addElement(common.parseNull((String)res.getString(26)));
                        VDiscPer     .addElement(common.parseNull((String)res.getString(27)));
                        VSupplier    .addElement(common.parseNull((String)res.getString(28)));
                        VMMRSType.addElement(common.parseNull((String)res.getString(30)));

				    String SAutoConv = common.parseNull(res.getString(29));
				    String SEnqNo    = common.parseNull(res.getString(31));		
                        VMHsnCode.addElement(common.parseNull(res.getString(32)));
                        VMHsnType.addElement(common.parseNull(res.getString(33)));

				    String SAutoStatus="";

				    //if(SAutoConv.equals("1") || common.toInt(SEnqNo)>0)
				    if(SAutoConv.equals("1"))
				    {
				         SAutoStatus = "1";
				    }
				    else
				    {
				         SAutoStatus = "0";
				    }

                        VMAutoOrderStatus.addElement(SAutoStatus);
                  }
                  res.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }
      public void setRowData()
      {
            RowData = new Object[VMCode.size()][ColumnData.length];
            for(int i=0;i<VMCode.size();i++)
            {
                  RowData[i][0] = common.parseDate((String)VMMRSDate.elementAt(i));                  
                  RowData[i][1] = (String)VMMRS.elementAt(i);
                  RowData[i][2] = (String)VMCode.elementAt(i);                  
                  RowData[i][3] = (String)VMName.elementAt(i);
                  RowData[i][4] = (String)VMQty.elementAt(i);
                  RowData[i][5] = (String)VMUnit.elementAt(i);
                  RowData[i][6] = (String)VMDept.elementAt(i);
                  RowData[i][7] = (String)VMGroup.elementAt(i);
                  RowData[i][8] = common.parseDate((String)VMDueDate.elementAt(i));
                  RowData[i][9] = new Boolean(false);

                  RowData[i][10] =     (String) VSupplier    .elementAt(i);
                  RowData[i][11] =     (String) VPurchaseDate.elementAt(i);
                  RowData[i][12] =     (String) VPurchaseRate.elementAt(i);
                  RowData[i][13] =     (String) VDiscPer     .elementAt(i);
                  RowData[i][14] = common.parseNull((String)VMHsnCode.elementAt(i));
            }
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(setSelectedRecords())
			{
				if (JOptionPane.showConfirmDialog(null, "Confirm Convert the Order?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0) {
					OrderPlanFrameGst orderplanframegst = new OrderPlanFrameGst(Layer,VCode,VName,VSelectedId,VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedDue,VSelectedDesc,SPanel,iUserCode,iMillCode,VSelectedBlock,VSelectedMRSSlNo,VSelectedColor,VSelectedSet,VSelectedSize,VSelectedSide,VSelectedSlipNo,VSelectedBookNo,VSelectedSlipFrNo,VSelectedCatl,VSelectedDraw,VSelectedMake,VSelectedUserCode,VSelectedMrsType,SYearCode,SItemTable,SSupTable,VSelectedHsnCode,VSelectedHsnType);
					try
					{
						removeOrderFrame(); 
						Layer.add(orderplanframegst);
						Layer.repaint();
						orderplanframegst.setSelected(true);
						Layer.updateUI();
						orderplanframegst.show();
					}
					catch(Exception ex)
					{
					   System.out.println(ex);
					}
				}
			}
          }
     }
     public class AutoList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(setSelectedRecords())
			{
				if (JOptionPane.showConfirmDialog(null, "Confirm Convert the Order?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0) {
					removeOrderFrame();                                                                                                                                                                                                                              
					OrderPoolingGst orderpoolinggst = new OrderPoolingGst(Layer,VCode,VName,VSelectedId,VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedDue,VSelectedDesc,SPanel,iUserCode,iMillCode,VSelectedBlock,VSelectedMRSSlNo,VSelectedColor,VSelectedSet,VSelectedSize,VSelectedSide,VSelectedSlipNo,VSelectedBookNo,VSelectedSlipFrNo,VSelectedCatl,VSelectedDraw,VSelectedMake,VSelectedUserCode,VSelectedMrsType,SYearCode,SItemTable,SSupTable,VSelectedHsnCode,VSelectedHsnType);
				}
			}
          }
     }
     public void removeOrderFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }
	
     public boolean setSelectedRecords()
     {
            VSelectedId   = new Vector();
            VSelectedCode = new Vector();
            VSelectedName = new Vector();
            VSelectedMRS  = new Vector();
            VSelectedQty  = new Vector();
            VSelectedUnit = new Vector();
            VSelectedDept = new Vector();
            VSelectedGroup= new Vector();
            VSelectedDue  = new Vector();

            VSelectedDesc = new Vector();
            VSelectedBlock= new Vector();

            VSelectedMRSSlNo= new Vector();


            VSelectedColor = new Vector();
            VSelectedSet   = new Vector();
            VSelectedSize  = new Vector();
            VSelectedSide  = new Vector();
            VSelectedSlipNo= new Vector();
            VSelectedBookNo= new Vector();
            VSelectedSlipFrNo = new Vector();

            VSelectedCatl     = new Vector();
            VSelectedDraw     = new Vector();
            VSelectedMake     = new Vector();
            VSelectedUserCode = new Vector();
            VSelectedMrsType  = new Vector();

		VSelectedHsnCode  = new Vector();
		VSelectedHsnType  = new Vector();

            try
            {
			  MRSCheckingClass theMRSClass = null;
			  theMRSClass = new MRSCheckingClass();
			  
			  String SErrorMsg = "";
			  
                  for(int i=0;i<RowData.length;i++)
                  {
                        Boolean bAppl = (Boolean)RowData[i][9];
                        if(!bAppl.booleanValue())
                              continue;

                         VSelectedId   .addElement(VMMRSId.elementAt(i));
                         VSelectedCode .addElement(VMCode.elementAt(i));
                         VSelectedHsnCode.addElement(VMHsnCode.elementAt(i));
                         VSelectedHsnType.addElement(VMHsnType.elementAt(i));
                         VSelectedName .addElement(VMName.elementAt(i));
                         VSelectedMRS  .addElement(VMMRS.elementAt(i));
                         VSelectedQty  .addElement(VMQty.elementAt(i));
                         VSelectedUnit .addElement(VMUnit.elementAt(i));
                         VSelectedDept .addElement(VMDept.elementAt(i));
                         VSelectedGroup.addElement(VMGroup.elementAt(i));
                         VSelectedDue  .addElement(common.parseDate((String)VMDueDate.elementAt(i)));

                         VSelectedDesc .addElement(VMMRSDesc.elementAt(i));
                         VSelectedBlock.addElement(VMMRSBlock.elementAt(i));
                         VSelectedMRSSlNo.addElement(VMMRSSlNo.elementAt(i));

                         VSelectedColor.addElement(VMMRSColor.elementAt(i));
                         VSelectedSet.addElement(VMMRSSet.elementAt(i));
                         VSelectedSize.addElement(VMMRSSize.elementAt(i));
                         VSelectedSide.addElement(VMMRSSide.elementAt(i));
                         VSelectedSlipNo.addElement(VMMRSSlipNo.elementAt(i));
                         VSelectedBookNo.addElement(VMMRSBookNo.elementAt(i));
                         VSelectedSlipFrNo .addElement(VMMRSSlipFrNo.elementAt(i));
                         VSelectedCatl     .addElement(VMMRSCatl.elementAt(i));
                         VSelectedDraw     .addElement(VMMRSDraw.elementAt(i));
                         VSelectedMake     .addElement(VMMRSMake.elementAt(i));
                         VSelectedUserCode .addElement(VMMRSUserCode.elementAt(i));
                         VSelectedMrsType  .addElement(VMMRSType.elementAt(i));
					
					int iMRSNo = common.toInt((String)VMMRS.elementAt(i));
					String SItemCode = common.parseNull((String)VMCode.elementAt(i));
					double dQty = common.toDouble((String)VMQty.elementAt(i));
					
					int iOrderNo = theMRSClass.getOrderNo(iMRSNo, SItemCode, dQty);
					
					if(iOrderNo > 0) {
						SErrorMsg += "Mrs No. "+iMRSNo+", Item Code "+SItemCode+" already Converted to Order..\n";
					}
					
					if(iOrderNo == -1) {
						SErrorMsg += "Problem while checking previous order count.. Mrs No. "+iMRSNo+", Item Code "+SItemCode+"..\n";
					}
                  }
			   
			   if(SErrorMsg.trim().length() == 0) {
				return true;
			   }else {
				JOptionPane.showMessageDialog(null, SErrorMsg, "Information", JOptionPane.ERROR_MESSAGE);
				return false;
			   }
            }
            catch(Exception ex)
            {
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null, "Problem while getting the data..", "Information", JOptionPane.ERROR_MESSAGE);
				return false;
            }
     }
}

