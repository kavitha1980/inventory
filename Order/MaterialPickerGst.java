package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class MaterialPickerGst extends JInternalFrame
{
     JTextField     TMatCode,TMatName,THsnCode,TMRSNo;
     JTextField     TIndicator;
     JButton        BOk,BCancel;
     
     JList          BrowList;
     JScrollPane    BrowScroll;
     JTextField     TDesc;
     JTextField     TMake;
     JTextField     TDraw;
     JTextField     TCatl;
     JPanel         LeftPanel,RightPanel;
     JPanel         LeftCenterPanel,LeftBottomPanel;
     JPanel         RightTopPanel,RightCenterPanel,RightBottomPanel;
     JPanel         FigurePanel;
     JPanel         DeptPanel;
     
     JLayeredPane   Layer;
     Vector         VName,VCode,VDesc,VMake,VDraw,VCatl;
     Vector         VMrsStatus;
     JTable         ReportTable;
     DOInvTableModelGst  dataModel;
     int            iMillCode;
     int            iItemStatus=0;
     
     Vector         VNameCode;
     String         SMName,SMCode;
     String         SItemTable;
     
     JComboBox      JCType;
     JComboBox      JCDept,JCGroup,JCUnit,JCBlock,JCUser;
     DateField2     TDueDate;
     Vector         VDept,VDeptCode,VGroup,VGroupCode,VUnit,VUnitCode;
     Vector         VSUserCode,VSUserName;
     Vector         VBlock,VBlockName;
     Vector         VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo,VHsnType;
                         
     String         str ="";
     String         SPColour="",SPSet="",SPSize="",SPSide="",SPSlipNo="",SPBookNo="";
     String         SHsnCode="",SHsnType="";

     JTextField     TQty;
     JTextField     TRate;
     JTextField     TDisPer;
     JTextField     TCGSTPer;
     JTextField     TSGSTPer;
     JTextField     TIGSTPer;
     JTextField     TCessPer;
     
     JTextField     TNet;
     JTextField     TBasic;
     JTextField     TDisc;
     JTextField     TCGST;
     JTextField     TSGST;
     JTextField     TIGST;
     JTextField     TCess;
     
     String         SCatl     = "",SDraw     = "";
     int            iLastIndex= 0;
     int            iTaxAuth;
     Common         common    = new Common();
     boolean        bCheck;
     boolean        bflag     = false;

     String         SOldCode="";
     String         SOldMrsNo="";

     double dCGST=0,dSGST=0,dIGST=0,dCess=0;

     Connection       theConnection = null;

     MaterialPickerGst(JLayeredPane Layer,Vector VCode,Vector VName,Vector VNameCode,Vector VDesc,Vector VMake,Vector VDraw,Vector VCatl,JTable ReportTable,DOInvTableModelGst dataModel,int iMillCode,Vector VPColour,Vector VPSet,Vector VPSize,Vector VPSide,Vector VPSlipFrNo,Vector VPSlipToNo,Vector VPBookFrNo,Vector VPBookToNo,String SItemTable,Vector VMrsStatus)
     {
          this.Layer          = Layer;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.VDesc          = VDesc;
          this.VMake          = VMake;
          this.VDraw          = VDraw;
          this.VCatl          = VCatl;
          this.ReportTable    = ReportTable;
          this.dataModel      = dataModel;
          this.iMillCode      = iMillCode;
          this.VPColour       = VPColour;
          this.VPSet          = VPSet;
          this.VPSize         = VPSize;
          this.VPSide         = VPSide;
          this.VPSlipFrNo     = VPSlipFrNo;
          this.VPSlipToNo     = VPSlipToNo;
          this.VPBookFrNo     = VPBookFrNo;
          this.VPBookToNo     = VPBookToNo;
          this.SItemTable     = SItemTable;
          this.VMrsStatus     = VMrsStatus;

          getDeptGroupUnit();
          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
     }

     MaterialPickerGst(JLayeredPane Layer,Vector VCode,Vector VName,Vector VNameCode,Vector VDesc,Vector VMake,Vector VDraw,Vector VCatl,JTable ReportTable,DOInvTableModelGst dataModel,int iMillCode,boolean bCheck,boolean bflag,Vector VPColour,Vector VPSet,Vector VPSize,Vector VPSide,Vector VPSlipFrNo,Vector VPSlipToNo,Vector VPBookFrNo,Vector VPBookToNo,int iTaxAuth,String SItemTable,Vector VMrsStatus,int iItemStatus,Vector VHsnType)
     {
          this.Layer          = Layer;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.VDesc          = VDesc;
          this.VMake          = VMake;
          this.VDraw          = VDraw;
          this.VCatl          = VCatl;
          this.ReportTable    = ReportTable;
          this.dataModel      = dataModel;
          this.iMillCode      = iMillCode;
          this.bCheck         = bCheck;
          this.bflag          = bflag;
          this.VPColour       = VPColour;
          this.VPSet          = VPSet;
          this.VPSize         = VPSize;
          this.VPSide         = VPSide;
          this.VPSlipFrNo     = VPSlipFrNo;
          this.VPSlipToNo     = VPSlipToNo;
          this.VPBookFrNo     = VPBookFrNo;
          this.VPBookToNo     = VPBookToNo;
          this.iTaxAuth       = iTaxAuth;
          this.SItemTable     = SItemTable;
          this.VMrsStatus     = VMrsStatus;
          this.iItemStatus    = iItemStatus;
	  this.VHsnType       = VHsnType;

          getDeptGroupUnit();
          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          setEnables(bCheck);
          addListeners();
     }

     public void  setEnables(boolean bCheck)
     {
          if(!bCheck)
          {
               BrowList            . setEnabled(false);
               TMatName            . setEditable(false);
               TMatCode            . setEditable(false);
               THsnCode            . setEditable(false);
               TMRSNo              . setEditable(false);
               TDesc               . setEditable(false);
               TMake               . setEditable(false);
               JCBlock             . setEnabled(false);
               TDraw               . setEditable(false);
               TCatl               . setEditable(false);
               JCDept              . setEnabled(false);
               JCGroup             . setEnabled(false);
               JCUnit              . setEnabled(false);
               JCUser              . setEnabled(false);
               TDueDate            . setEditable(false);
               JCType              . setEnabled(false);
               TDueDate.TDay       . setEditable(false);
               TDueDate.TMonth     . setEditable(false);
               TDueDate.TYear      . setEditable(false);
               TRate               . setEditable(false);
               TDisPer             . setEditable(false);
               TCGSTPer            . setEditable(false);
               TSGSTPer            . setEditable(false);
               TIGSTPer            . setEditable(false);
               TCessPer            . setEditable(false);
               TNet                . setEditable(false);

               if(iItemStatus==1)
               {
                    TQty.setEditable(false);
                    BOk.setEnabled(false);
                    BCancel.setEnabled(false);
                    JOptionPane.showMessageDialog(null,"The Item have already GRN","Error",JOptionPane.ERROR_MESSAGE);
               }
               else
               {
                    TQty.setEditable(true);
                    BOk.setEnabled(true);
                    BCancel.setEnabled(true);

                    TQty                . requestFocus();
               }
          }
     }

     public void createComponents()
     {
          BrowList            = new JList(VNameCode);
          BrowScroll          = new JScrollPane();
          TMatCode            = new JTextField();
          THsnCode            = new JTextField();
          TDesc               = new JTextField();
          TMake               = new JTextField();
          TDraw               = new JTextField();
          TCatl               = new JTextField();
          TMatName            = new JTextField();
          TMRSNo              = new JTextField();
          TIndicator          = new JTextField();
          TIndicator          . setEditable(false);
          
          JCDept              = new JComboBox(VDept);
          JCGroup             = new JComboBox(VGroup);

          JCUnit              = new JComboBox(VUnit);
          JCUser              = new JComboBox(VSUserName);
          JCBlock             = new JComboBox(VBlockName);
          TDueDate            = new DateField2();
          
          BOk                 = new JButton("Okay");
          BCancel             = new JButton("Cancel");
          
          LeftPanel           = new JPanel(true);
          RightPanel          = new JPanel(true);
          LeftCenterPanel     = new JPanel(true);
          LeftBottomPanel     = new JPanel(true);
          RightTopPanel       = new JPanel(true);
          RightCenterPanel    = new JPanel(true);
          RightBottomPanel    = new JPanel(true);
          FigurePanel         = new JPanel(true);
          DeptPanel           = new JPanel(true);
          
          JCType              = new JComboBox();
          
          TMatCode            . setEditable(false);
          TMatName            . setEditable(false);
          THsnCode            . setEditable(false);
          TCatl               . setEditable(false);
          TDraw               . setEditable(false);
          
          TQty                = new JTextField();
          TRate               = new JTextField();
          TDisPer             = new JTextField();
          TCGSTPer            = new JTextField();
          TSGSTPer            = new JTextField();
          TIGSTPer            = new JTextField();
          TCessPer            = new JTextField();
          
          TNet                = new JTextField();
          TBasic              = new JTextField();
          TDisc               = new JTextField();
          TCGST               = new JTextField();
          TSGST               = new JTextField();
          TIGST               = new JTextField();
          TCess               = new JTextField();
          
          JCType              . addItem("Basic");
          JCType              . addItem("Net");

	  JCType.setSelectedIndex(0);
	  JCType.setEnabled(false);

          TDesc               . addKeyListener(new DescKeyList());
     }

     public void setLayouts()
     {
          setBounds(80,50,650,500);
          setResizable(true);
          setClosable(true);
          setTitle("Material Modification");
          getContentPane()    . setLayout(new GridLayout(1,1));
          LeftPanel           . setLayout(new BorderLayout());
          RightPanel          . setLayout(new BorderLayout());
          LeftCenterPanel     . setLayout(new BorderLayout());
          LeftBottomPanel     . setLayout(new GridLayout(6,2));
          RightTopPanel       . setLayout(new BorderLayout());
          RightCenterPanel    . setLayout(new BorderLayout());
          RightBottomPanel    . setLayout(new GridLayout(5,2));
          DeptPanel           . setLayout(new GridLayout(2,4));
     }

     public void addComponents()
     {
          getContentPane()    . add(LeftPanel);
          getContentPane()    . add(RightPanel);
          
          LeftPanel           . add("Center",LeftCenterPanel);
          LeftPanel           . add("South",LeftBottomPanel);
          
          RightPanel          . add("North",RightTopPanel);
          RightPanel          . add("Center",RightCenterPanel);
          RightPanel          . add("South",RightBottomPanel);
          
          LeftCenterPanel     . add("Center",BrowScroll);
          LeftCenterPanel     . add("South",TIndicator);
          LeftBottomPanel     . add(new JLabel("Material Name"));
          LeftBottomPanel     . add(TMatName);
          LeftBottomPanel     . add(new JLabel("Material Code"));
          LeftBottomPanel     . add(TMatCode);
          LeftBottomPanel     . add(new JLabel("Hsn Code"));
          LeftBottomPanel     . add(THsnCode);
          LeftBottomPanel     . add(new JLabel("MRS No "));
          LeftBottomPanel     . add(TMRSNo);
          LeftBottomPanel     . add(new JLabel("User "));
          LeftBottomPanel     . add(JCUser);
          LeftBottomPanel     . add(new JLabel("Block "));
          LeftBottomPanel     . add(JCBlock);
          
          RightBottomPanel    . add(new JLabel("Description"));
          RightBottomPanel    . add(TDesc);
          RightBottomPanel    . add(new JLabel("Material Make"));
          RightBottomPanel    . add(TMake);
          RightBottomPanel    . add(new JLabel("Drawing No"));
          RightBottomPanel    . add(TDraw);
          RightBottomPanel    . add(new JLabel("Catalogue No"));
          RightBottomPanel    . add(TCatl);
          
          RightBottomPanel    . add(BOk);
          RightBottomPanel    . add(BCancel);
          
          DeptPanel           . add(new JLabel("Department"));
          DeptPanel           . add(JCDept);
          DeptPanel           . add(new JLabel("Group"));
          DeptPanel           . add(JCGroup);
          DeptPanel           . add(new JLabel("Unit"));
          DeptPanel           . add(JCUnit);
          DeptPanel           . add(new JLabel("DueDate"));
          DeptPanel           . add(TDueDate);
          
          RightTopPanel       . add("North",JCType);
          RightCenterPanel    . add("Center",FigurePanel);
          RightCenterPanel    . add("South",DeptPanel);
     }

     public void setPresets()
     {
          int i = ReportTable.getSelectedRow();
          String SNameCode="";
          String SCode   = (String)ReportTable.getModel().getValueAt(i,0);
          setCDM(SCode);
          String SName   = (String)ReportTable.getModel().getValueAt(i,1);
          int iindex     = common.indexOf(VCode,SCode);
          if(iindex > -1)
               SNameCode = (String)VNameCode.elementAt(iindex);
          
          String SHsnCode     = (String)ReportTable.getModel().getValueAt(i,2);
          String SBlockName   = (String)ReportTable.getModel().getValueAt(i,3);
          String SMRSNo       = (String)ReportTable.getModel().getValueAt(i,4);
          String SQty         = (String)ReportTable.getModel().getValueAt(i,5);
          String SRate        = common.getRound(((String)ReportTable.getModel().getValueAt(i,6)).trim(),4);

          String SDisPer      = ((String)ReportTable.getModel().getValueAt(i,7)).trim();
          String SCGSTPer     = ((String)ReportTable.getModel().getValueAt(i,8)).trim();
          String SSGSTPer     = ((String)ReportTable.getModel().getValueAt(i,9)).trim();
          String SIGSTPer     = ((String)ReportTable.getModel().getValueAt(i,10)).trim();
          String SCessPer     = ((String)ReportTable.getModel().getValueAt(i,11)).trim();
          String SNet         = ((String)ReportTable.getModel().getValueAt(i,18)).trim();
          String SDept        = ((String)ReportTable.getModel().getValueAt(i,19)).trim();
          String SGroup       = (String)ReportTable.getModel().getValueAt(i,20);

	  String SDueDate = "";

	  if(((String)ReportTable.getModel().getValueAt(i,21)).trim().equals(""))
	  {
          	SDueDate     = common.pureDate((String)ReportTable.getModel().getValueAt(0,21));
	  }
	  else
	  {
          	SDueDate     = common.pureDate((String)ReportTable.getModel().getValueAt(i,21));
	  }

          String SUnit        = (String)ReportTable.getModel().getValueAt(i,22);
          String SUser        = (String)ReportTable.getModel().getValueAt(i,23);

          SOldCode  = SCode;
          SOldMrsNo = SMRSNo;

          BrowList            . setAutoscrolls(true);
          BrowScroll          . getViewport().setView(BrowList);
          TMatCode            . setText(SCode);
          THsnCode            . setText(SHsnCode);
          TDesc               . setText((String)VDesc.elementAt(i));
          TDraw               . setText(SDraw);
          TCatl               . setText(SCatl);
          TMake               . setText((String)VMake.elementAt(i));
          TMatName            . setText(SName);
          JCDept              . setSelectedItem(SDept);
          JCGroup             . setSelectedItem(SGroup);
          JCUnit              . setSelectedItem(SUnit);
          JCUser              . setSelectedItem(SUser);
          JCBlock             . setSelectedItem(SBlockName);

          if(SDueDate.length()!=8)
          {
               TDueDate.TDay.setText("");
               TDueDate.TMonth.setText("");
               TDueDate.TYear.setText("");
          }
          else
          {
               TDueDate.TDay.setText(SDueDate.substring(6,8));
               TDueDate.TMonth.setText(SDueDate.substring(4,6));
               TDueDate.TYear.setText(SDueDate.substring(0,4));
          }

	  JCType.setSelectedIndex(0);
	  JCType.setEnabled(false);

          TMRSNo              . setText(SMRSNo);
          TQty                . setText(SQty.trim());
          TRate               . setText(SRate.trim());
          TDisPer             . setText(SDisPer.trim());
          TCGSTPer            . setText(SCGSTPer.trim());
          TSGSTPer            . setText(SSGSTPer.trim());
          TIGSTPer            . setText(SIGSTPer.trim());
          TCessPer            . setText(SCessPer.trim());
          TNet                . setText(SNet.trim());
          FigurePanel         . removeAll();
          FigurePanel         . setLayout(new BorderLayout());
          FigurePanel         . add("Center",getNetPane());
          setCalculation();

          if(SHsnType.equals("1"))
          {
	       TRate.setEditable(true);
	       TQty.setEditable(false);
	       TDisPer.setEditable(false);
          }

          /*if(SHsnType.equals("1") || SHsnCode.equals("") || SHsnCode.equals("0"))
          {
	       if(dataModel.SSeleSupType.equals("1"))
	       {
	            if(dataModel.SSeleStateCode.equals("33"))
		    {
	                 TCGSTPer.setEditable(true);
	            	 TSGSTPer.setEditable(true);
	            	 TIGSTPer.setEditable(false);
	            	 TCessPer.setEditable(true);
		    }
		    else
		    {
	                 TCGSTPer.setEditable(false);
	            	 TSGSTPer.setEditable(false);
	            	 TIGSTPer.setEditable(true);
	            	 TCessPer.setEditable(true);
		    }
	       }
          }*/

          TCGSTPer.setEditable(true);
    	  TSGSTPer.setEditable(true);
    	  TIGSTPer.setEditable(true);
    	  TCessPer.setEditable(true);

          show();
          ensureIndexIsVisible(SNameCode);
          LeftCenterPanel.updateUI();
     }

     public void addListeners()
     {
          BrowList  .addKeyListener(new KeyList());
          BOk       .addActionListener(new ActList());
          BCancel   .addActionListener(new ActList());
          JCType    .addItemListener(new ItList());
                    addMouseListener(new MouseList());
     }

     public class MouseList extends MouseAdapter
     {
          public void mouseEntered(MouseEvent me)
          {
               BrowList.ensureIndexIsVisible(iLastIndex);               
          }
     }

     public class ItList implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
               FigurePanel.removeAll();
               FigurePanel.setLayout(new BorderLayout());
               FigurePanel.add("Center",getNetPane());
               updateUI();
          }
     }

     public JScrollPane getNetPane()
     {
          String SType = (String)JCType.getSelectedItem();

          if((SType.trim()).equals("Net"))
          {
               TRate     .setEditable(false);
               TNet      .setEditable(true);
          }
          else
          {
               TRate     .setEditable(true);
               TNet      .setEditable(false);
          }

          JPanel thePanel    = new JPanel();
          
          thePanel  .setLayout(new GridLayout(7,4));
          thePanel  .add(new JLabel("Qty"));
          thePanel  .add(TQty);
          thePanel  .add(new JLabel("Net Value"));
          thePanel  .add(TNet);

          thePanel  .add(new JLabel("Cess (%)"));
          thePanel  .add(TCessPer);
          thePanel  .add(new JLabel("Cess (Rs)"));
          thePanel  .add(TCess);
          TCessPer  .setEditable(false);
          TCess     .setEditable(false);

          thePanel  .add(new JLabel("IGST (%)"));
          thePanel  .add(TIGSTPer);
          thePanel  .add(new JLabel("IGST (Rs)"));
          thePanel  .add(TIGST);
          TIGSTPer  .setEditable(false);
          TIGST     .setEditable(false);

          thePanel  .add(new JLabel("SGST (%)"));
          thePanel  .add(TSGSTPer);
          thePanel  .add(new JLabel("SGST (Rs)"));
          thePanel  .add(TSGST);
          TSGSTPer  .setEditable(false);
          TSGST     .setEditable(false);

          thePanel  .add(new JLabel("CGST (%)"));
          thePanel  .add(TCGSTPer);
          thePanel  .add(new JLabel("CGST (Rs)"));
          thePanel  .add(TCGST);
          TCGSTPer  .setEditable(false);
          TCGST     .setEditable(false);
          
          thePanel  .add(new JLabel("Disc (%)"));
          thePanel  .add(TDisPer);
          thePanel  .add(new JLabel("Disc (Rs)"));
          thePanel  .add(TDisc);
          TDisc     .setEditable(false);
          
          thePanel  .add(new JLabel("Basic Rate"));
          thePanel  .add(TRate);
          thePanel  .add(new JLabel("Basic Value"));
          thePanel  .add(TBasic);
          
          TBasic    .setEditable(false);
          
          TNet      .addKeyListener(new CalcList());
          TQty      .addKeyListener(new CalcList());
          TCGSTPer  .addKeyListener(new CalcList());
          TSGSTPer  .addKeyListener(new CalcList());
          TIGSTPer  .addKeyListener(new CalcList());
          TCessPer  .addKeyListener(new CalcList());
          TDisPer   .addKeyListener(new CalcList());
          TRate     .addKeyListener(new CalcList());
          
          return new JScrollPane(thePanel);
     }

     public class CalcList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               setCalculation();
          }
     }

     public void setCalculation()
     {
          String SType   = (String)JCType.getSelectedItem();

          double N  = 0;
          double R  = 0;

          double DV = 0;
          double CGV = 0;
          double SGV = 0;
          double IGV = 0;
          double CEV = 0;

          if((SType.trim()).equals("Net"))
          {
               N         = common.toDouble(TNet.getText());
          }
          else
          {
               R         = common.toDouble(TRate.getText());
          }
          double Q       = common.toDouble(TQty.getText());
          double D       = common.toDouble(TDisPer.getText());
          double CG      = common.toDouble(TCGSTPer.getText());
          double SG      = common.toDouble(TSGSTPer.getText());
          double IG      = common.toDouble(TIGSTPer.getText());
          double CE      = common.toDouble(TCessPer.getText());
          double B       = 0;

          if((SType.trim()).equals("Net"))
          {
               /*try
               {
                    B=(((N)/(1+(S/100)*(U/100+1)))/(1+C/100))/(1-D/100);
               }
               catch(Exception ex)
               {
                    B = 0;
               }*/
          }
          else
          {
               try
               {
                    B=Q*R;
               }
               catch(Exception ex)
               {
                    B = 0;
               }
          }

          DV = B*D/100;
          DV = common.toDouble(common.getRound(String.valueOf(DV),2));

          CGV = (B-DV)*(CG/100);
          CGV = common.toDouble(common.getRound(String.valueOf(CGV),2));

          SGV = (B-DV)*(SG/100);
          SGV = common.toDouble(common.getRound(String.valueOf(SGV),2));

          IGV = (B-DV)*(IG/100);
          IGV = common.toDouble(common.getRound(String.valueOf(IGV),2));

          CEV = (B-DV)*(CE/100);
          CEV = common.toDouble(common.getRound(String.valueOf(CEV),2));

          TBasic    .setText(common.getRound(B,2));
          TDisc     .setText(common.getRound(DV,2));
          TCGST     .setText(common.getRound(CGV,2));
          TSGST     .setText(common.getRound(SGV,2));
          TIGST     .setText(common.getRound(IGV,2));
          TCess     .setText(common.getRound(CEV,2));

          N = B-DV+CGV+SGV+IGV+CEV;

          if((SType.trim()).equals("Net"))
          {
               try
               {
                    TRate.setText(common.getRound(B/Q,4));
               }
               catch(Exception ex)
               {
               }
          }
          else
          {
               try
               {
                    TNet.setText(common.getRound(N,2));
               }
               catch(Exception ex)
               {
               }
          }
     }    

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    if(validUser())
                    {
                         int       i = ReportTable.getSelectedRow();
     
                         String SNewCode  = TMatCode.getText();
                         String SNewMrsNo = TMRSNo.getText();
     
                         ReportTable.getModel().setValueAt(TMatCode.getText(),i,0);
                         ReportTable.getModel().setValueAt(TMatName.getText(),i,1);
                         ReportTable.getModel().setValueAt(THsnCode.getText(),i,2);
                         ReportTable.getModel().setValueAt(JCBlock.getSelectedItem(),i,3);
                         ReportTable.getModel().setValueAt(TMRSNo.getText(),i,4);
                         ReportTable.getModel().setValueAt(TQty.getText(),i,5);
                         ReportTable.getModel().setValueAt(TRate.getText(),i,6);
                         ReportTable.getModel().setValueAt(TDisPer.getText(),i,7);
                         ReportTable.getModel().setValueAt(TCGSTPer.getText(),i,8);
                         ReportTable.getModel().setValueAt(TSGSTPer.getText(),i,9);
                         ReportTable.getModel().setValueAt(TIGSTPer.getText(),i,10);
                         ReportTable.getModel().setValueAt(TCessPer.getText(),i,11);
                         ReportTable.getModel().setValueAt(JCDept.getSelectedItem(),i,19);
                         ReportTable.getModel().setValueAt(JCGroup.getSelectedItem(),i,20);
                         String SDue = TDueDate.TYear.getText()+TDueDate.TMonth.getText()+TDueDate.TDay.getText();
                         ReportTable.getModel().setValueAt(common.parseDate(SDue),i,21);
                         ReportTable.getModel().setValueAt(JCUnit.getSelectedItem(),i,22);
                         ReportTable.getModel().setValueAt(JCUser.getSelectedItem(),i,23);
     
                         VDesc          . setElementAt(TDesc.getText(),i);
                         VMake          . setElementAt(TMake.getText(),i);
                         VDraw          . setElementAt(TDraw.getText(),i);
                         VCatl          . setElementAt(TCatl.getText(),i);
     
                         VPColour       . setElementAt(SPColour,i);
                         VPSet          . setElementAt(SPSet,i);
                         VPSize         . setElementAt(SPSize,i);
                         VPSide         . setElementAt(SPSide,i);
                                                                      
                         if((!SNewCode.equals(SOldCode)) || (!SNewMrsNo.equals(SOldMrsNo)))
                         {
                              VMrsStatus.setElementAt("0",i);
                         }
     
                         if(common.toInt(SPSlipNo)==0)
                         {
                              VPSlipFrNo     . setElementAt(String.valueOf(common.toInt(SPSlipNo)),i);
                              VPSlipToNo     . setElementAt(String.valueOf(common.toInt(SPSlipNo)),i);
                         }
                         else
                         {
                              VPSlipFrNo     . setElementAt(String.valueOf(common.toInt(SPSlipNo)+1),i);
                              VPSlipToNo     . setElementAt(String.valueOf(common.toInt(SPSlipNo)+1),i);
                         }
     
                         if(common.toInt(SPBookNo)==0)
                         {
                              VPBookFrNo     . setElementAt(String.valueOf(common.toInt(SPBookNo)),i);
                              VPBookToNo     . setElementAt(String.valueOf(common.toInt(SPBookNo)),i);
                         }
                         else
                         {
                              VPBookFrNo     . setElementAt(String.valueOf(common.toInt(SPBookNo)+1),i);
                              VPBookToNo     . setElementAt(String.valueOf(common.toInt(SPBookNo)+1),i);
                         }

                         VHsnType.setElementAt(SHsnType,i);
     
                         if(!bflag)
                         {
                              for(int j=i;j<dataModel.getRows();j++)
                              {
                                   ReportTable.getModel().setValueAt(JCDept .getSelectedItem(),j,19);
                                   ReportTable.getModel().setValueAt(JCGroup.getSelectedItem(),j,20);
                                   ReportTable.getModel().setValueAt(common .parseDate(SDue),j,21);
                                   ReportTable.getModel().setValueAt(JCUnit .getSelectedItem(),j,22);
                                   ReportTable.getModel().setValueAt(JCUser .getSelectedItem(),j,23);
                              }
                         }

                         removeHelpFrame();
                         ReportTable.requestFocus();
                    }
               }
          }
     }

     public boolean validUser()
     {
          int       i = ReportTable.getSelectedRow();

          String SNewCode  = TMatCode.getText();
          String SNewMrsNo = TMRSNo.getText().trim();

          if(common.toInt(SNewMrsNo)==0)
               return true;


          String SUserCode = (String)VSUserCode.elementAt(JCUser.getSelectedIndex());

          Vector VMrsUserCode = new Vector();
          Vector VMrsUser     = new Vector();

          String QS = " Select Mrs.MrsAuthUserCode,RawUser.UserName "+
                      " from Mrs Inner Join RawUser on Mrs.MrsAuthUserCode=RawUser.UserCode "+
                      " Where Mrs.MillCode="+iMillCode+" and Mrs.Item_Code='"+SNewCode+"'"+
                      " and Mrs.MrsNo="+SNewMrsNo+" Order by 1 ";

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
               ResultSet      result         = stat.executeQuery(QS);
               while(result.next())
               {
                    VMrsUserCode.addElement(common.parseNull((String)result.getString(1)));
                    VMrsUser    .addElement(common.parseNull((String)result.getString(2)));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }

          if(VMrsUser.size()>0)
          {
               int iIndex = common.exactIndexOf(VMrsUserCode,SUserCode);

               if(iIndex>=0)
               {
                    return true;
               }
               else
               {
                    JOptionPane.showMessageDialog(null,"User Name Mismatch. Required User = "+VMrsUser,"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }
          }
          else
          {
               JOptionPane.showMessageDialog(null,"MrsNo and Item Mismatch ","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==116)    // F5 is pressed
               {
                    setDataIntoVector();
                    BrowList.setListData(VNameCode);
               }
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SMatName     = (String)VName.elementAt(index);
                    String SMatCode     = (String)VCode.elementAt(index);
                    String SMatNameCode = (String)VNameCode.elementAt(index);
                    addMatDet(SMatName,SMatCode,SMatNameCode,index);
                    str="";
               }
          }
     }

     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<VNameCode.size();index++)
          {
               String str1 = ((String)VNameCode.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index);
                    break;
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

     public boolean addMatDet(String SMatName,String SMatCode,String SMatNameCode,int index)
     {
          TMatCode       . setText(SMatCode);
          TMatName       . setText(SMatName);
          setCDM(SMatCode);
          TCatl          . setText(SCatl);
          TDraw          . setText(SDraw);
          THsnCode       . setText(SHsnCode);

          int iRateCheck=0;

          if((SHsnCode.equals("") || SHsnCode.equals("0")))
          {
               iRateCheck = 1;
          }
          else
          {
               if(dataModel.SSeleSupType.equals("1"))
	       {
	            iRateCheck = dataModel.getRateCheck(SHsnCode);
	       }
	       else
	       {
	            iRateCheck = 1;
	       }
          }

          if(iRateCheck>0)
          {
               if(!(SHsnCode.equals("") || SHsnCode.equals("0")))
	       {
                    getGstRate(SHsnCode);
	       }

       	       if(SHsnType.equals("1"))
               {
	            TQty.setText("1");
	    	    TRate.setText("");		
	    	    TDisPer.setText("");	
	    	    TQty.setEditable(false);
	    	    TDisPer.setEditable(false);
               }
               else
               {
	            TQty.setText("");
	    	    TRate.setText("");		
	    	    TDisPer.setText("");	
	    	    TQty.setEditable(true);
	    	    TDisPer.setEditable(true);
               }

               setCalculation();

	       if(SHsnType.equals("1") || SHsnCode.equals("") || SHsnCode.equals("0"))
	       {
	            if(dataModel.SSeleSupType.equals("1"))
	            {
		         if(dataModel.SSeleStateCode.equals("33"))
		         {
		              TCGSTPer.setEditable(true);
		    	      TSGSTPer.setEditable(true);
		    	      TIGSTPer.setEditable(false);
		    	      TCessPer.setEditable(true);
		         }
		         else
		         {
		              TCGSTPer.setEditable(false);
		    	      TSGSTPer.setEditable(false);
		    	      TIGSTPer.setEditable(true);
		    	      TCessPer.setEditable(true);
		         }
	            }
	            else
	            {
	                 TCGSTPer.setEditable(false);
		         TSGSTPer.setEditable(false);
		         TIGSTPer.setEditable(false);
		         TCessPer.setEditable(false);
	            }
	       }
	       else
               {
                    TCGSTPer.setEditable(false);
	            TSGSTPer.setEditable(false);
	            TIGSTPer.setEditable(false);
	            TCessPer.setEditable(false);
               }

               BOk.setEnabled(true);
          }
          else
          {
	       JOptionPane.showMessageDialog(null,"Gst Rate Not Updated for HsnCode-"+SHsnCode,"Information",JOptionPane.INFORMATION_MESSAGE);
	       BOk.setEnabled(false);
          }

          return true;    
     }

     public void setCDM(String SMatCode)
     {
          String QS = "";
          try
          {
               if(iMillCode==0)
               {
                    QS      = " Select Catl,Draw,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE,LASTSLIPNO,LASTBOOKNO,HsnCode,HsnType "+
			      " From invitems Where Item_Code='"+SMatCode+"'";
               }
               else
               {
                    QS      = " Select invitems.Catl,invitems.Draw,invitems.PAPERCOLOR,invitems.PAPERSETS,"+
                              " invitems.PAPERSIZE,invitems.PAPERSIDE,"+SItemTable+".LASTSLIPNO,"+
                              " "+SItemTable+".LASTBOOKNO,InvItems.HsnCode,InvItems.HsnType "+
                              " From "+SItemTable+""+
                              " inner join invitems on invitems.item_code = "+SItemTable+".item_code"+
                              " where "+SItemTable+".item_code='"+SMatCode+"'";
               }

               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               ResultSet res1 = stat.executeQuery(QS);

               while(res1.next())
               {
                    SCatl     = common.parseNull(res1.getString(1));
                    SDraw     = common.parseNull(res1.getString(2));
                    SPColour  = common.parseNull(res1.getString(3));
                    SPSet     = common.parseNull(res1.getString(4));
                    SPSize    = common.parseNull(res1.getString(5));
                    SPSide    = common.parseNull(res1.getString(6));
                    SPSlipNo  = common.parseNull((String)res1.getString(7));
                    SPBookNo  = common.parseNull((String)res1.getString(8));
                    SHsnCode  = common.parseNull((String)res1.getString(9));
                    SHsnType  = common.parseNull((String)res1.getString(10));
               }
               res1.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("@ setCDM");
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public boolean ensureIndexIsVisible(String SNameCode)
     {
          SNameCode = SNameCode.trim();
          int i=0;

          for(i=0;i<VNameCode.size();i++)
          {
               String SCurName = (String)VNameCode.elementAt(i);
               SCurName = SCurName.trim();
               if(SCurName.startsWith(SNameCode))
               break;
          }

          if(i==VNameCode.size())
          {
               return false;
          }
          iLastIndex=i;
          BrowList.setSelectedIndex(i);
          BrowList.ensureIndexIsVisible(i);
          BrowList.requestFocus();
          BrowList.updateUI();
          return true;
     }

     public void setDataIntoVector()
     {
          VName     . removeAllElements();
          VCode     . removeAllElements();
          VNameCode . removeAllElements();

          String QString = "";
          if(iMillCode==0)
          {
               QString = " Select Item_Name,Item_Code From InvItems Order By Item_Name";
          }
          else
          {
               QString = " Select Item_Name,"+SItemTable+".Item_Code From "+SItemTable+""+
                         " Inner Join InvItems on InvItems.Item_Code = "+SItemTable+".Item_Code"+
                         " Order By Item_Name";
          }
          
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
               ResultSet      res            = stat.executeQuery(QString);

               while(res.next())
               {
                    String    str1      = res.getString(1);
                    String    str2      = res.getString(2);
                              VName     . addElement(str1);
                              VCode     . addElement(str2);
                              VNameCode . addElement(str1+" (Code : "+str2+")");
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void getDeptGroupUnit()
     {
          ResultSet result = null;

          VDept      = new Vector();
          VGroup     = new Vector();
          VUnit      = new Vector();
          
          VDeptCode  = new Vector();
          VGroupCode = new Vector();
          VUnitCode  = new Vector();

          VBlock     = new Vector();
          VBlockName = new Vector();

          VSUserCode = new Vector();
          VSUserName = new Vector();

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               String QS1 = "";
               String QS2 = "";
               String QS3 = "";
               String QS4 = "";
               String QS5 = "";
               
               QS1 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               QS2 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               QS3 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";
               QS4 = " Select Block,BlockName From OrdBlock Where ShowStatus=0 Order By 1";
               QS5 = " Select UserCode,UserName from RawUser Where UserCode in (Select Distinct(AuthUserCode) from MrsUserAuthentication) Order by UserCode ";
               
               result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VDept     . addElement(result.getString(1));
                    VDeptCode . addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VGroup    . addElement(result.getString(1));
                    VGroupCode. addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery(QS3);
               while(result.next())
               {
                    VUnit     . addElement(result.getString(1));
                    VUnitCode . addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery(QS4);
               while(result.next())
               {
                    VBlock    . addElement(result.getString(1));
                    VBlockName. addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery(QS5);
               while(result.next())
               {
                    VSUserCode. addElement(result.getString(1));
                    VSUserName. addElement(result.getString(2));
               }
               result. close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept,Group & Unit :"+ex);
               ex.printStackTrace();
          }
     }

     private class DescKeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String str = TDesc.getText();
               if(str.length() > 200)
               {
                    str  = str.substring(0,200);
                    TDesc. setText(str);
               }
          }
     }

     public void getGstRate(String SHsnCode)
     {
        dCGST=0;
	dSGST=0;
	dIGST=0;
	dCess=0;

        try
        {
	       if(dataModel.SSeleSupType.equals("1"))
	       {
		       if(theConnection == null)
		       {
		            ORAConnection jdbc   = ORAConnection.getORAConnection();
		            theConnection        = jdbc.getConnection();
		       }

		       Statement theStatement    = theConnection.createStatement();

		       String QS = "";

		       if(dataModel.SSeleStateCode.equals("33"))
		       {
			    QS = " Select nvl(CGST,0) as CGST,nvl(SGST,0) as SGST,0 as IGST,nvl(Cess,0) as Cess "+
				 " from HsnGstRate Where HsnCode='"+SHsnCode+"'";
		       }
		       else
		       {
			    QS = " Select 0 as CGST,0 as SGST,nvl(IGST,0) as IGST,nvl(Cess,0) as Cess "+
				 " from HsnGstRate Where HsnCode='"+SHsnCode+"'";
		       }

		       ResultSet result = theStatement.executeQuery(QS);
		       while(result.next())
		       {
		            dCGST = result.getDouble(1);
		            dSGST = result.getDouble(2);
		            dIGST = result.getDouble(3);
		            dCess = result.getDouble(4);
		       }
		       result.close();
		       theStatement.close();
	       }

               TCGSTPer            . setText(common.getRound(dCGST,2));
               TSGSTPer            . setText(common.getRound(dSGST,2));
               TIGSTPer            . setText(common.getRound(dIGST,2));
               TCessPer            . setText(common.getRound(dCess,2));

        }
        catch(Exception ex)
        {
            System.out.println("getGstRate :"+ex);
        }
     }

}
