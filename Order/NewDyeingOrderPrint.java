package Order;

import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class NewDyeingOrderPrint
{
     Vector         VCode,VName,VUOM,VQty,VRate;
     Vector         VDiscPer,VCenVatPer,VTaxPer,VSurPer;
     Vector         VBasic,VDisc,VCenVat,VTax,VSur,VNet;
     Vector         VDesc,VMake,VCatl,VDraw,VBlock;
     Vector         VPColour,VPSet,VPSize,VPSide;
     Vector         VSlipFromNo,VSlipToNo,VBookFromNo,VBookToNo;


     FileWriter     FW;
     Common         common    = new Common();

     String         SOrdNo    = "",SOrdDate  = "",SSupCode  = "",SSupName  = "";
     int            iMillCode;
     String         SSupTable,SItemTable;

     String         SAddr1    = "",SAddr2    = "",SAddr3    = "";
     String         SEMail="",SFaxNo="";
     String         SToName   = "",SThroName = "";
     String         SPayTerm  = "";
     String         SReference= "";
     String         SDueDate  = "";
     String         SOthers   = "";
     String         SMRSNo    = "";
     String         SMRSDate  = "";
     String         SPort     = "";

     double         dOthers        = 0;
     double         dTDisc         = 0,dTCenVat   = 0,dTTax      = 0,dTSur = 0,dTNet = 0;

     int            Lctr      = 100;
     int            Pctr      = 0;
     int            iEPCG          = 0;
     int            iOrderType     = 0;
     int            iProject       = 0;
     int            iState         = 0;
     int            iAmend         = 0;
     String SShortName = "";


     NewDyeingOrderPrint(FileWriter FW,String SOrdNo,String SOrdDate,String SSupCode,String SSupName,int iMillCode,String SSupTable,String SItemTable)
     {
          this.FW         = FW;
          this.SOrdNo     = SOrdNo;
          this.SOrdDate   = SOrdDate;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.iMillCode  = iMillCode;
          this.SSupTable  = SSupTable;
          this.SItemTable = SItemTable;

          getAddr();
          setDataIntoVector();
          setOrderHead();
          setOrderBody();
          setOrderFoot(0);
     }
   
     public void setOrderHead()
     {
          if(Lctr < 40)
               return;
          
          if(Pctr > 0)
               setOrderFoot(1);
          
          String SState = "";
          if (iState == 1)
          {
               SState = " Intra State";
          }
          else if (iState == 2)
          {
               SState = " Inter State";
          }
          String Strl06 = "";
          if (iAmend == 1)
          {     
               if (iEPCG == 1 && iOrderType == 0)
                    Strl06 = "| E-Mail : dyeing@amarjothi.net   TIN No. 33632960864           |"+common.Pad(SState,12)+"|   EEPCG  | AM PURCHASE ORDERF                             |";
               else if (iEPCG == 1 && iOrderType == 1)
                    Strl06 = "| E-Mail : dyeing@amarjothi.net   TIN No. 33632960864           |   EIMPORT |    EPCG   | AM PURCHASE ORDERF"+common.Pad(SPort,10)+"                   |";
               else if (iEPCG == 0 && iOrderType == 1)
                    Strl06 = "| E-Mail : dyeing@amarjothi.net   TIN No. 33632960864           |   EIMPORT |           | AM PURCHASE ORDERF"+common.Pad(SPort,10)+"                   |";
               else  
                    Strl06 = "| E-Mail : dyeing@amarjothi.net   TIN No. 33632960864           |"+common.Pad(SState,12)+"|           |EAM PURCHASE ORDERF                            |";
          }
          else
          {
               if (iEPCG == 1 && iOrderType == 0)
                    Strl06 = "| E-Mail : dyeing@amarjothi.net   TIN No. 33632960864           |"+common.Pad(SState,12)+"|   EEPCG  |    PURCHASE ORDERF                             |";
               else if (iEPCG == 1 && iOrderType == 1)
                    Strl06 = "| E-Mail : dyeing@amarjothi.net   TIN No. 33632960864           |   EIMPORT |    EPCG   |    PURCHASE ORDERF"+common.Pad(SPort,10)+"                   |";
               else if (iEPCG == 0 && iOrderType == 1)
                    Strl06 = "| E-Mail : dyeing@amarjothi.net   TIN No. 33632960864           |   EIMPORT |           |    PURCHASE ORDERF"+common.Pad(SPort,10)+"                   |";
               else  
                    Strl06 = "| E-Mail : dyeing@amarjothi.net   TIN No. 33632960864           |"+common.Pad(SState,12)+"|           |   EPURCHASE ORDERF                           |";
          }

          SShortName = common.getShortName(iMillCode);


          Pctr++;
          String Strl01 = "g|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl02 = "|      "+"E"+"AMARJOTHI SPINNING MILLS (DYEING DIVISION)"+"F"+"               |                            ECC No          : AAFCA 7082C XM 001     |";
          String Strl03 = "|              SIPCOT INDUSTRIAL AREA                           | PLA No      : 59/93        Range           : Gobichettipalayam.     |";
          String Strl04 = "|              PERUNDURAI  -  638 052                           | Division    : Erode.       Commissionerate : Salem.                 |";
          String Strl05 = "|                                                               |---------------------------------------------------------------------|";
          String Strl07 = "| Phone : 04294 - 309339          Fax : 04294 - 230392          |---------------------------------------------------------------------|";
          String Strl08 = "| Our C.S.T. No. 440691 dt. 24.09.1990                          |    NO     |E"+common.Pad(" "+SShortName+"-"+SOrdNo,20)+"F@g"+common.Space(105)+"| Date |"+common.Pad(SOrdDate,10)+"| Page |"+common.Pad(""+Pctr,3)+"|";
          String Strl09 = "|                                                               |---------------------------------------------------------------------|";
          String Strl10 = "|---------------------------------------------------------------| Book To   |E"+common.Pad(SToName,28)+"F@g"+common.Space(134)+"|";
          String Strl11 = "|To,                                                            |---------------------------------------------------------------------|";
          String Strl12 = "|"+"E"+common.Pad("M/s "+SSupName,63)+"F"+                             "| Book Thro'|"+common.Pad(SThroName,55)+                            "  |";
          String Strl13 = "|"+common.Pad(SAddr1,63)+                                      "|---------------------------------------------------------------------|";
          String Strl14 = "|"+common.Pad(SAddr2,63)+                                      "| Reference |"+common.Pad(SReference,55)+                            "  |";
          String Strl15 = "|"+common.Pad(SAddr3,63)+                                      "|           |                                                         |";
          String Strl15A= "|"+common.Pad("Fax No: "+SFaxNo,25)+common.Pad("EMail: "+SEMail,38)+"|           |                                                         |";
          String Strl16 = "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl17 = "| Mat Code  |Block| Description                             | UOM | Quantity |     Rate | Discount |   CenVat |      Tax |        Net |";
          String Strl17A= "|           |     | Make - Catalogue - Drawing Number       |     |          |       Rs |       Rs |       Rs |       Rs |         Rs |";
          String Strl18 = "|-------------------------------------------------------------------------------------------------------------------------------------|";

          try
          {
               FW.write( Strl01+"\n");       
               FW.write( Strl02+"\n");       
               FW.write( Strl03+"\n");       
               FW.write( Strl04+"\n");       
               FW.write( Strl05+"\n");       
               FW.write( Strl06+"\n");       
               FW.write( Strl07+"\n");       
               FW.write( Strl08+"\n");       
               FW.write( Strl09+"\n");       
               FW.write( Strl10+"\n");       
               FW.write( Strl11+"\n");       
               FW.write( Strl12+"\n");       
               FW.write( Strl13+"\n");       
               FW.write( Strl14+"\n");       
               FW.write( Strl15+"\n");
               FW.write( Strl15A+"\n");       
               FW.write( Strl16+"\n");       
               FW.write( Strl17+"\n");
               FW.write( Strl17A+"\n");       
               FW.write( Strl18+"\n");
               Lctr = 20;
          }
          catch(Exception ex){}
     }

     public void setOrderBody()
     {
          String Strl="";
          for(int i=0;i<VCode.size();i++)
          {
               setOrderHead();
               String SCode     = (String)VCode.elementAt(i);
               String SUOM      = (String)VUOM.elementAt(i);
               String SName     = (String)VName.elementAt(i);
               String SBkName   = (String)VBlock.elementAt(i);
               String SQty      = common.getRound((String)VQty.elementAt(i),2);
               String SRate     = common.getRound((String)VRate.elementAt(i),2);
               String SDisc     = common.getRound((String)VDisc.elementAt(i),2);
               String SDiscPer  = common.getRound((String)VDiscPer.elementAt(i),2)+"%";
               String SCenVat   = common.getRound((String)VCenVat.elementAt(i),2);
               String SCenVatPer= common.getRound((String)VCenVatPer.elementAt(i),2)+"%";
               String STax      = common.getRound((String)VTax.elementAt(i),2);
               String STaxPer   = common.getRound((String)VTaxPer.elementAt(i),2)+"%";
               String SSur      = common.getRound((String)VSur.elementAt(i),2);
               String SSurPer   = common.getRound((String)VSurPer.elementAt(i),2)+"%";
               String SNet      = common.getRound((String)VNet.elementAt(i),2);
               String SDesc     = common.parseNull((String)VDesc.elementAt(i));
               String SMake     = common.parseNull((String)VMake.elementAt(i));
               String SCatl     = common.parseNull((String)VCatl.elementAt(i));
               String SDraw     = common.parseNull((String)VDraw.elementAt(i));

               String SPColour  = common.parseNull((String)VPColour   . elementAt(i));
               String SPSets    = common.parseNull((String)VPSet      . elementAt(i));
               String SPSize    = common.parseNull((String)VPSize     . elementAt(i));
               String SPSide    = common.parseNull((String)VPSide     . elementAt(i));

               String SSlipFromNo  = common.parseNull((String)VSlipFromNo. elementAt(i));
               String SSlipToNo    = common.parseNull((String)VSlipToNo  . elementAt(i));
               String SBookFromNo  = common.parseNull((String)VBookFromNo. elementAt(i));
               String SBookToNo    = common.parseNull((String)VBookToNo  . elementAt(i));

               SDesc     = SDesc   . trim();
               SMake     = SMake   . trim();
               SPColour  = SPColour. trim();
               SPSets    = SPSets  . trim();
               SPSize    = SPSize  . trim();
               SPSide    = SPSide  . trim();
               SSlipFromNo    = SSlipFromNo  . trim();
               SSlipToNo      = SSlipToNo    . trim();
               SBookFromNo    = SBookFromNo  . trim();
               SBookToNo      = SBookToNo    . trim();

               dTDisc   = dTDisc+common.toDouble(SDisc);
               dTCenVat = dTCenVat+common.toDouble(SCenVat);
               dTTax    = dTTax   +common.toDouble(STax);
               dTSur    = dTSur   +common.toDouble(SSur);
               dTNet    = dTNet   +common.toDouble(SNet);
               
               try
               {
                    Strl =    "| "+common.Pad(SCode,9)+" | "+common.Rad(SBkName,3)+" | "+"E"+common.Pad(SName,39)+"F"+" | "+
                              common.Pad(SUOM,3)+" | "+common.Rad(SQty,8)+" | "+
                              common.Rad(SRate,8)+" | "+common.Rad(SDisc,8)+" | "+
                              common.Rad(SCenVat,8)+" | "+common.Rad(STax,8)+" | "+
                              common.Rad(SNet,10)+" |";
                    FW.write(Strl+"\n");
                    Lctr = Lctr+1;
          
                    Vector vect = common.getLines(SDesc+"` ");
          
                    for(int j=0;j<vect.size();j++)
                    {
                         if(j==0)
                         {
                              Strl =    "| "+common.Pad("",9)+" | "+common.Space(3)+" | "+common.Pad((String)vect.elementAt(j),39)+" | "+
                                        common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                        common.Space(8)+" | "+common.Rad(SDiscPer,8)+" | "+
                                        common.Rad(SCenVatPer,8)+" | "+common.Rad(STaxPer,8)+" | "+
                                        common.Space(10)+" |";
                         }
                         else
                         {
                              Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad((String)vect.elementAt(j),39)+" | "+
                                        common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                        common.Space(8)+" | "+common.Rad("",8)+" | "+
                                        common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                        common.Space(10)+" |";
                         }
                         FW.write(Strl+"\n");
                         Lctr = Lctr+1;
                    }

                    if(SMake.length() > 0)
                    {
                         Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad(SMake,39)+" | "+
                                   common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                   common.Space(8)+" | "+common.Rad("",8)+" | "+
                                   common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                   common.Space(10)+" |";
                         FW.write(Strl+"\n");
                         Lctr++;
                    }                         
          
                    if((SCatl.trim()).length() > 0)
                    {
                         Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad("Catl : "+SCatl,39)+" | "+
                                   common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                   common.Space(8)+" | "+common.Rad("",8)+" | "+
                                   common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                   common.Space(10)+" |";
                         FW.write(Strl+"\n");
                         Lctr++;
                    }

                    if((SDraw.trim()).length() > 0)
                    {
                         Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad("Drawing : "+SDraw,39)+" | "+
                                   common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                   common.Space(8)+" | "+common.Rad("",8)+" | "+
                                   common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                   common.Space(10)+" |";
                         FW.write(Strl+"\n");
                         Lctr++;
                    }

                    if(isStationary(SCode))
                    {
                         String    SSPCoSets = "Colour : "+common.Pad(SPColour,12);
                                   SSPCoSets+= "Sets: "+SPSets;
               
                         String    SSPSis    = "Size   : "+common.Pad(SPSize,12);
                                   SSPSis   += "Side: "+SPSide;

                         String    SSPBooks  = "Book No: "+SBookFromNo+" To "+SBookToNo;

                         String    SSPSlip   = "Slip No: "+SSlipFromNo+" To "+SSlipToNo;

                         if(((SPColour.trim()).length() > 0) || ((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0))
                         {
                              Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad(SSPCoSets,39)+" | "+
                                        common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                        common.Space(8)+" | "+common.Rad("",8)+" | "+
                                        common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                        common.Space(10)+" |";
                              FW.write(Strl+"\n");
                              Lctr++;
                         }
     
                         if(((SPColour.trim()).length() > 0) || ((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0))
                         {
                              Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad(SSPSis,39)+" | "+
                                        common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                        common.Space(8)+" | "+common.Rad("",8)+" | "+
                                        common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                        common.Space(10)+" |";
                              FW.write(Strl+"\n");
                              Lctr++;
                         }

                         if((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0)
                         {
                              Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad(SSPSlip,39)+" | "+
                                        common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                        common.Space(8)+" | "+common.Rad("",8)+" | "+
                                        common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                        common.Space(10)+" |";
                              FW.write(Strl+"\n");
                              Lctr++;
                         }

                         if((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0)
                         {
                              Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad(SSPBooks,39)+" | "+
                                        common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                        common.Space(8)+" | "+common.Rad("",8)+" | "+
                                        common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                        common.Space(10)+" |";
                              FW.write(Strl+"\n");
                              Lctr++;
                         }
                    }

                    Strl =    "| "+common.Space(9)+" | "+common.Space(3)+" | "+common.Space(39)+" | "+
                              common.Space(3)+" | "+common.Space(8)+" | "+
                              common.Space(8)+" | "+common.Space(8)+" | "+
                              common.Space(8)+" | "+common.Space(8)+" | "+
                              common.Space(10)+" |";
                    FW.write(Strl+"\n");
                    Lctr=Lctr+1;
               }
               catch(Exception ex){}
          }
     }

     public void setOrderFoot(int iSig)
     {
          String SDisc   = common.getRound(dTDisc,2);
          String SCenVat = common.getRound(dTCenVat,2);
          String STax    = common.getRound(dTTax,2);
          String SSur    = common.getRound(dTSur,2);
          String SNet    = common.getRound(dTNet,2);
          String SOthers = common.getRound(dOthers,2);
          String SGrand  = common.getRound(dTNet+dOthers,2);
          String Strl1 = "|-------------------------------------------------------------------------------------------------------------------------------------|";
          
          String Strl2 = "| "+common.Space(9)+" | "+common.Space(3)+" | "+common.Pad("T O T A L ",39)+" | "+
                         common.Space(3)+" | "+common.Space(8)+" | "+
                         common.Space(8)+" | "+common.Rad(SDisc,8)+" | "+
                         common.Rad(SCenVat,8)+" | "+common.Rad(STax,8)+" |E "+
                         common.Rad(SNet,10)+"F |";
          String Strl3 = "|-------------------------------------------------------------------------------------------------------------------------------------|";
          
          String Strl3a= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl3b= "| Date of Delivery | MRS No | MRS Date   | Terms of Payments                    | Freight & Others |           Order Value            |";
          String Strl3c= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl3d= "| "+common.Pad(SDueDate,16)+" | "+
                         common.Pad(SMRSNo,6)+" | "+
                         common.Pad(SMRSDate,10)+" | "+
                         common.Pad(SPayTerm,36)+" | "+
                         common.Rad(SOthers,16)+" | "+
                         "E"+common.Rad(SGrand+" ",16)+"F@g"+common.Space(134)+"|";
          String Strl3e= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl4 = "| Note : 1. EPlease mention our order No. and material code in all correspondence with us.F                                             |";
          String Strl4a= "|        2. EPlease mention our TIN Number in your invoices.F                                                                           |";
          String Strl5 = "|        3. Please send  invoices in triplicate. Original invoice to be sent direct to accounts manager.                              |";
          String Strl6 = "|        4. Please send copy of your invoice along with consignment.                                                                  |";
          String Strl7 = "|        5. We reserve the right to increase / decrease the order qty. / cancel the order without assigning any reason.               |";
          String Strl8 = "|        6. Any legal dispute shall be within the jurisdiction of Tirupur.                                                            |";
          String Strl8a= "";
          String Strl8b= "";
          String Strl8c= "";
          String Strl8d= "";
          if (iEPCG == 1)
          {
               Strl8a = "|        7. EThese materials should be supplied against EPCG Scheme  for which please send us yourF                                     |";
               Strl8b = "|           EProforma Invoice along with the catalogue immediately so that we can openF                                                 |";
               Strl8c = "|           Ethe EPCG Licence.F                                                                                                         |";
               Strl8d = "|        8. EPlease quote the EPCG License No in your all Bills.F                                                                       |";
          }
          String Strl9 = "|                                                                                               For AMARJOTHI SPINNING MILLS LTD      |";
          String Strl10= "|                                                                                                                                     |";
          String Strl11= "|                                                                                                                                     |";
          String Strl12= "| Prepared By                        Checked By                        I.A.                         Authorised Signatory              |";
          String Strl13= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl14= "| Special Instruction :-                                                                                                              |";
          String Strl15= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl16= "| Mill Address - AMARJOTHI SPINNING MILLS LIMITED, Pudusuripalayam, Nambiyur - 638458. Phone : 04285 - 267201, 267301                 |";
          String Strl16a="| E-Mail       - purchase@amarjothi.net,mill@amarjothi.net                             Fax   : 04285 - 267565                         |";
          String Strl16b="| Regd. Office - 'AMARJOTHI HOUSE' 157, Kumaran Road, Tirupur - 638 601.               Phone : 0421  - 2201980 to 2201984             |";
          String Strl16c="| E-Mail       - amarin@giasmd01.vsnl.net.in, amarjothi@vsnl.com                                                                      |";
          String Strl17= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl18= "|                           I N D I A 'S   F I N E S T  M E L A N G E  Y A R N  P R O D U C E R S                                     |";
          String Strl19= "--------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl20="";
          if(iSig==0)
               Strl20= "< End Of Report >";
          else
               Strl20= "(Continued on Next Page) ";
          try
          {
               FW.write( Strl1+"\n");       
               FW.write( Strl2+"\n");       
               FW.write( Strl3+"\n");
               if(iSig==0)
               {
                    FW.write( Strl3a+"\n");
                    FW.write( Strl3b+"\n");
                    FW.write( Strl3c+"\n");
                    FW.write( Strl3d+"\n");
                    FW.write( Strl3e+"\n");
               } 
               FW.write( Strl4+"\n");
               FW.write( Strl4a+"\n");       
               FW.write( Strl5+"\n");       
               FW.write( Strl6+"\n");       
               FW.write( Strl7+"\n");
               FW.write( Strl8+"\n");
               if (iEPCG == 1)
               {
                    FW.write( Strl8a+"\n");
                    FW.write( Strl8b+"\n");
                    FW.write( Strl8c+"\n");
                    FW.write( Strl8d+"\n");
               }
          
               FW.write( Strl9+"\n");       
               FW.write( Strl10+"\n");       
               FW.write( Strl11+"\n");       
               FW.write( Strl12+"\n");       
               FW.write( Strl13+"\n");       
               FW.write( Strl14+"\n");       
               FW.write( Strl15+"\n");       
               FW.write( Strl16+"\n");
               FW.write( Strl16a+"\n");
               FW.write( Strl16b+"\n");
               FW.write( Strl16c+"\n");       
               FW.write( Strl17+"\n");       
               FW.write( Strl18+"\n");
               FW.write( Strl19+"\n");       
               FW.write( Strl20+"\n");       
          }
          catch(Exception ex){}
     }

     public void setDataIntoVector()
     {
          VCode      = new Vector();
          VName      = new Vector();
          VBlock     = new Vector();
          VUOM       = new Vector();
          VQty       = new Vector();
          VRate      = new Vector();
          VDiscPer   = new Vector();
          VCenVatPer = new Vector();
          VTaxPer    = new Vector();
          VSurPer    = new Vector();
          VBasic     = new Vector();
          VDisc      = new Vector();
          VCenVat    = new Vector();
          VTax       = new Vector();
          VSur       = new Vector();
          VNet       = new Vector();
          VDesc      = new Vector();
          VMake      = new Vector();
          VCatl      = new Vector();
          VDraw      = new Vector();
          VPColour   = new Vector();
          VPSet      = new Vector();
          VPSize     = new Vector();
          VPSide     = new Vector();
          VSlipFromNo    = new Vector();
          VSlipToNo      = new Vector();
          VBookFromNo    = new Vector();
          VBookToNo      = new Vector();


          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
               String         QString        = getQString();
               ResultSet      res            = stat.executeQuery(QString);

               while (res.next())
               {
                    String str1    = res.getString(1);  
                    String str2    = res.getString(2);
                    String str3    = res.getString(3);
                    String str4    = res.getString(4);
                    String str5    = res.getString(5);
                    String str6    = res.getString(6);
                    String str7    = res.getString(7);
                    String str8    = res.getString(8);
                    String str9    = res.getString(9);
                    String str10   = res.getString(10);
                    String str11   = res.getString(11);
                    String str12   = res.getString(12);
                    String str13   = res.getString(13);
                    SToName        = res.getString(14);
                    SThroName      = res.getString(15);
                    String str14   = res.getString(16);
                    String str15   = res.getString(17);
                    String str16   = res.getString(18);
                    String str17   = res.getString(19);
                    String str18   = res.getString(20);
                    SPayTerm       = res.getString(21);
                    SDueDate       = common.parseDate(res.getString(22));
                    dOthers        = common.toDouble(res.getString(23));
                    SReference     = res.getString(24);
                    SMRSNo         = res.getString(25);
                    iEPCG          = res.getInt(26);
                    iOrderType     = res.getInt(27);
                    iProject       = res.getInt(28);
                    iState         = res.getInt(29);
                    SPort          = res.getString(30);
                    iAmend         = res.getInt(31);
                    String SBName  = res.getString(32);
                    
                    VBlock         . addElement(SBName);
                    VCode          . addElement(str1);
                    VName          . addElement(str2);
                    VQty           . addElement(str3);
                    VRate          . addElement(str4);
                    VDiscPer       . addElement(str5);
                    VDisc          . addElement(str6);
                    VCenVatPer     . addElement(str7);
                    VCenVat        . addElement(str8);
                    VTaxPer        . addElement(str9);
                    VTax           . addElement(str10);
                    VNet           . addElement(str11);
                    VSurPer        . addElement(str12);
                    VSur           . addElement(str13);
                    VUOM           . addElement(str14);
                    VDesc          . addElement(str15);
                    VMake          . addElement(str16);
                    VCatl          . addElement(str17);
                    VDraw          . addElement(str18);
                    VPColour       . addElement(common.parseNull(res.getString(33)));
                    VPSet          . addElement(common.parseNull(res.getString(34)));
                    VPSize         . addElement(common.parseNull(res.getString(35)));
                    VPSide         . addElement(common.parseNull(res.getString(36)));
                    VSlipFromNo    . addElement(common.parseNull(res.getString(37)));
                    VSlipToNo      . addElement(common.parseNull(res.getString(38)));
                    VBookFromNo    . addElement(common.parseNull(res.getString(39)));
                    VBookToNo      . addElement(common.parseNull(res.getString(40)));
               }
               res  . close();
               ResultSet res1 = stat.executeQuery("Select MRSDate From MRS Where MRSNo="+SMRSNo+" and MillCode="+iMillCode);
               while(res1.next())
                    SMRSDate = common.parseDate(res1.getString(1));

               res1 . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public String getQString()
     {
          String QString  = "";
          
          QString = " SELECT PurchaseOrder.Item_Code, InvItems.Item_Name, PurchaseOrder.qty,"+
                    " PurchaseOrder.Rate, PurchaseOrder.DiscPer, PurchaseOrder.Disc,"+
                    " PurchaseOrder.CenVatPer, PurchaseOrder.Cenvat, PurchaseOrder.TaxPer,"+
                    " PurchaseOrder.Tax, PurchaseOrder.Net,PurchaseOrder.SurPer,PurchaseOrder.Sur,"+
                    " BookTo.ToName,BookThro.ThroName,UoM.UoMName,MatDesc.Descr,MatDesc.Make,"+
                    " InvItems.Catl,InvItems.Draw,PurchaseOrder.PayTerms,PurchaseOrder.DueDate,"+
                    " (PurchaseOrder.Plus-PurchaseOrder.Less) as Others,PurchaseOrder.Reference,"+
                    " PurchaseOrder.MRSNo,PurchaseOrder.EPCG,PurchaseOrder.OrderType,"+
                    " PurchaseOrder.Project_order,PurchaseOrder.State,Port.PortName,PurchaseOrder.amended,OrdBlock.BlockName,"+
                    " MatDesc.PAPERCOLOR,PaperSet.SETNAME,MatDesc.PAPERSIZE,PaperSide.SIDENAME,"+
                    " MatDesc.SLIPFROMNO,MatDesc.SLIPTONO,MatDesc.BOOKFROMNO,MatDesc.BOOKTONO "+
                    " FROM ((((PurchaseOrder inner join port on purchaseorder.portcode=port.portcode)INNER JOIN InvItems ON PurchaseOrder.Item_Code = InvItems.Item_Code) "+
                    " Inner Join UoM on UoM.UoMCode = InvItems.UomCode"+
                    " Inner Join OrdBlock on OrdBlock.Block = PurchaseOrder.OrderBlock"+
                    " Inner Join BookTo   On BookTo.ToCode = PurchaseOrder.ToCode) "+
                    " Inner Join BookThro On BookThro.ThroCode = PurchaseOrder.ThroCode) "+
                    " Left Join MatDesc On MatDesc.OrderNo = PurchaseOrder.OrderNo and MatDesc.Item_Code = PurchaseOrder.Item_Code And MatDesc.SlNo = PurchaseOrder.SlNo "+
                    " inner join paperset on paperset.setcode = Matdesc.papersets inner join paperside on paperside.sidecode = Matdesc.paperside"+
                    " Where PurchaseOrder.Qty > 0 And PurchaseOrder.OrderNo = "+SOrdNo+ " and PurchaseOrder.millcode = "+iMillCode+
                    " Order By 1";

          return QString;
     }

     public void getAddr()
     {
          String QS =    " Select "+SSupTable+".Addr1,"+SSupTable+".Addr2,"+SSupTable+".Addr3,"+SSupTable+".EMail,"+SSupTable+".Fax "+
                         " From "+SSupTable+" "+
                         " Where "+SSupTable+".Ac_Code = '"+SSupCode+"'";

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    SAddr1 = res.getString(1);
                    SAddr2 = res.getString(2);
                    SAddr3 = common.parseNull(res.getString(3));
                    SEMail = common.parseNull(res.getString(4));
                    SFaxNo = common.parseNull(res.getString(5));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public boolean isStationary(String SItemCode)
     {
          String SStkGroupName     = "";
          String QS                = "";

          QS =    " select stockgroup.groupname,"+SItemTable+".item_code from "+SItemTable+""+
                  " inner join stockgroup on stockgroup.groupcode = "+SItemTable+".stkgroupcode"+
                  " where "+SItemTable+".item_code = '"+SItemCode+"'";
               
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnect    =  oraConnection.getConnection();               
               Statement      stat          = theConnect.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result.next())
               {
                    SStkGroupName  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }
          if(SStkGroupName.equals("STATIONARY"))
               return true;

          return false;
     }
}


