package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;


//pdf


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class OrderAbsFrame extends JInternalFrame
{
     JComboBox      JCOrder,JCType,JCGrnStatus,JCGSTType;
     JTextField     TFile;
     JButton        BApply,BPrint,BCancel,BMail,BCreatePDF;
     JPanel         TopPanel;
     JPanel         BottomPanel;
     JLayeredPane   DeskTop;

     NextField      TNo;
     StatusPanel    SPanel;
   //  TabReport      tabreport;
     OrderAbsTabReport      tabreport;
     DateField      TStDate;
     DateField      TEnDate;
     Common         common = new Common();

     Vector         VOrdNo,VOrdDate,VSupName,VBasic,VDisc,VCenVat,VTax,VSur,VAdd,VLess,VNet,VSupCode,VEMail;
     Vector         VContractPerson,VMobileNo,VJMDStatus,VJMDAppDate,VJMDAppNo,VStatus,VGrnNo,VGrnStatus;
     Vector         VCode,VName,VBlock,VBlockCode;
     Vector         VSupContPerson,VSupMobileNo,VSupSupCode;
    
     Object         RowData[][];

//     String         ColumnData[] = {"Mark to Print","Order No","Date","Supplier","Basic","Discount","CenVat","Tax","Surcharge","Plus","Minus","Net","EMail-Id","ContPerson","MobileNo","JMD Approval","Appr.No","Appr.Date"};
//     String         ColumnType[] = {"B","N","S","S","N","N","N","N","N","N","N","N","S","S","S","S","S","S"};


     String         ColumnData[] = {"Mark to Print","Order No","Date","Supplier","Net","EMail-Id","ContPerson","MobileNo","JMD Approval","Appr.No","Appr.Date","Status","GrnNo"};
     String         ColumnType[] = {"B","N","S","S","N","S","S","S","S","S","S","S","S"};

     String         SStDate,SEnDate;
     String         SFinYear  = "";
     boolean        bflag     = true;

     int            iUserCode,iMillCode,iMailSenderStatus;
     String         SSupTable,SItemTable;

     FileWriter     FW,FTxt;

     JRadioButton   JRDate,JRJDate;

     JRadioButton   RWithMail,RWithOutMail,RPrintMail;
     int iMailStatus=0;

     String SShortName="";
	 
    Document document;
    PdfPTable table;
    // String SPdfFile= "/root/PurchaseOrder.pdf";
     String SPdfFile = common.getPrintPath()+"PurchaseOrder.pdf";
     public OrderAbsFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,String SFinYear,int iUserCode,int iMillCode,String SSupTable,String SItemTable)
     {
         super("Abstract on Orders Placed During a Period");
         this.DeskTop    = DeskTop;
         this.VCode      = VCode;
         this.VName      = VName;
         this.SPanel     = SPanel;
         this.SFinYear   = SFinYear;
         this.iUserCode  = iUserCode;
         this.iMillCode  = iMillCode;
         this.SSupTable  = SSupTable;
         this.SItemTable = SItemTable;

         bflag = getReportYear(SFinYear);

         SShortName = getShortName();

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     public void createComponents()
     {
         TStDate     = new DateField();
         TEnDate     = new DateField();
         BApply      = new JButton("Apply");
         BPrint      = new JButton("Print Marked Orders");
         BMail       = new JButton("Mail Marked Orders");
         BCreatePDF  = new JButton(" Create PDF");
         BCancel     = new JButton("Cancel");
         TFile       = new JTextField(10);
         TNo         = new NextField(5); 
         TopPanel    = new JPanel();
         BottomPanel = new JPanel();
         JCOrder     = new JComboBox();
         JCType      = new JComboBox();
         JCGrnStatus = new JComboBox();
         JCGSTType   = new JComboBox();

         TStDate.setTodayDate();
         TEnDate.setTodayDate();

         JRDate      = new JRadioButton(" Order Date",true);
         JRJDate     = new JRadioButton(" Jmd Appr Date",false);


         RPrintMail   = new JRadioButton("Print List",true);
         RWithMail    = new JRadioButton("Sending Mail List",false);
         RWithOutMail = new JRadioButton("Pending Mail List",false);

     }

     public void setLayouts()
     {
         TopPanel.setLayout(new GridLayout(3,8));
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,750,500);
     }

     public void addComponents()
     {
          
         JCOrder.addItem("Order No");
         JCOrder.addItem("Supplierwise");
         JCOrder.addItem("Order Date");
         JCOrder.addItem("Jmd Approval Date");

         JCType.addItem(" All ");
         JCType.addItem(" Jmd Approved");
         JCType.addItem(" Jmd Not Approved");

         JCGrnStatus . addItem(" All ");
         JCGrnStatus . addItem(" Grn Made");
         JCGrnStatus . addItem(" Grn Not Made");

        
         JCGSTType  . addItem(" NEW (GST)");
         JCGSTType  . addItem(" OLD (NON GST)");  

         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(" TYPE"));
         TopPanel.add(JCGSTType);
         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(""));
        


         TopPanel.add(new JLabel(" OrderNo"));
         TopPanel.add(new JLabel("Sorted On"));
         TopPanel.add(new JLabel("Appr.Type"));
         TopPanel.add(new JLabel("Grn Status"));
         TopPanel.add(JRDate);
         TopPanel.add(TStDate);
         TopPanel.add(RPrintMail);
         TopPanel.add(RWithMail);


         TopPanel.add(TNo);
         TopPanel.add(JCOrder);
         TopPanel.add(JCType);
         TopPanel.add(JCGrnStatus);
         TopPanel.add(JRJDate);
         TopPanel.add(TEnDate);
         TopPanel.add(RWithOutMail);
         TopPanel.add(BApply);


         BottomPanel.add(TFile);
         BottomPanel.add(BCreatePDF);
        // BottomPanel.add(BPrint);
         BottomPanel.add(BMail);
         BottomPanel.add(BCancel);

         BMail.setEnabled(false);

         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
         BApply.addActionListener(new ApplyList());
         BPrint.addActionListener(new PrintList());
         BMail.addActionListener(new  MailList());
         BCreatePDF.addActionListener(new  PdfList());

         RPrintMail.addActionListener(new ActList1());
         RWithMail.addActionListener(new ActList1());
         RWithOutMail.addActionListener(new ActList1());

         JRDate.addActionListener(new ActList1());
         JRJDate.addActionListener(new ActList1());
         BCancel.addActionListener(new ExitList());
     }

     private class ActList1 implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==RPrintMail)
               {
                    RPrintMail.setSelected(true);
                    RWithMail.setSelected(false);
                    RWithOutMail.setSelected(false);
                    BMail.setEnabled(false);
                    iMailStatus = 0;
               }
               if(ae.getSource()==RWithOutMail)
               {
                    RWithOutMail.setSelected(true);
                    RPrintMail.setSelected(false);
                    RWithMail.setSelected(false);
                    BMail.setEnabled(true);
                    iMailStatus = 1;
               }
               if(ae.getSource()==RWithMail)
               {
                    RWithMail.setSelected(true);
                    RPrintMail.setSelected(false);
                    RWithOutMail.setSelected(false);
                    BMail.setEnabled(true);
                    iMailStatus = 2;
               }
               if(ae.getSource()==JRDate)
               {
                    JRDate.setSelected(true);
                    JRJDate.setSelected(false);
               }
               if(ae.getSource()==JRJDate)
               {
                    JRJDate.setSelected(true);
                    JRDate.setSelected(false);
               }
          }
     }
     private class ExitList implements ActionListener
     {
       public void actionPerformed(ActionEvent ae)
       {
          dispose();
       }

     }


/*    private class MailList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                    Connection     theConnection  =  oraConnection.getConnection();               
                    Statement      stat           =  theConnection.createStatement();

                    for(int i=0;i<RowData.length;i++)
                    {
                         Boolean Bselected = (Boolean)RowData[i][0];
                         if(Bselected.booleanValue())
                         {
                              String SOrdNo     = (String)VOrdNo.elementAt(i);
                              String SOrdDate   = (String)VOrdDate.elementAt(i);
                              String SSupCode   = (String)VSupCode.elementAt(i);
                              String SSupName   = (String)VSupName.elementAt(i);
                              try
                              {
                                   String     SFile    = "mail"+SOrdNo+".html";
                                   FileWriter FW       = new FileWriter(common.getPrintPath()+SFile);
                                   MailOrder  OP       = new MailOrder(FW,SOrdNo,SOrdDate,SSupCode,SSupName);
                                   FW.close();
                                   String    QS = "Insert Into MailDetails (Ac_Code,MailFile,OrderNo,OrderBlock,Status) Values (";
                                             QS = QS+"'"+SSupCode+"',";
                                             QS = QS+"'"+SFile+"',";
                                             QS = QS+"0"+SOrdNo+",";
                                             QS = QS+"0"+SBlockCode+",";
                                             QS = QS+"0)";
                                   stat.execute(QS);
                              }
                              catch(Exception ex)
                              {
                                   System.out.println("From OrderAbs "+ex);
                              }
                         }
                    }
               }
               catch(Exception ex){}
          }
     }*/

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
			if(isValidData())
			{
	               String SFile = TFile.getText();
                       
	              
	              
	               if((SFile.trim()).length()==0)
	                    SFile = "1.prn";

	               try
	               {
	                     FW       = new FileWriter(common.getPrintPath()+SFile);
	               }
	               catch(Exception ex){}

	               for(int i=0;i<RowData.length;i++)
	               {
	                    Boolean Bselected = (Boolean)RowData[i][0];

	                    if(Bselected.booleanValue())
	                    {
	                         String SOrdNo     = (String)VOrdNo      .elementAt(i);
	                         String SOrdDate   = (String)VOrdDate    .elementAt(i);
	                         String SSupCode   = (String)VSupCode    .elementAt(i);
	                         String SSupName   = (String)VSupName    .elementAt(i);

	                      String  SPdfFile = common.getMailPath()+SShortName+"_"+SOrdNo+".pdf";

	                         try
	                         {
	                              //FTxt = new FileWriter(common.getMailPath()+SSupName+"_"+SOrdNo+".html");
	                              //FTxt . write("<html><body><PRE>");
	                         }catch(Exception ex){ex.printStackTrace();}

	                         if(!bflag)
	                         {
	                              String SBlock     = (String)VBlock.elementAt(i);
	                              String SBlockCode = (String)VBlockCode.elementAt(i);

	                              if(iMillCode==0)
	                              {
	                                   try
	                                   {
	                                        new OrderPrint(FW,SOrdNo,SBlock,SBlockCode,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs "+ex);
	                                   }
	                              }
	                              if(iMillCode==1)
	                              {
	                                   try
	                                   {
	                                        new DyeingOrderPrint(FW,SOrdNo,SBlock,SBlockCode,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs "+ex);
	                                   }
	                              }
	                         }
	                         else
	                         {
	                              if(iMillCode==0)
	                              {
	                                   try
	                                   {
	                                        new NewOrderPrint(FW,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
	                                        //new OrderPrintPdfFile(SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);

	                                        //new OrderPrintTextFile(FTxt,SOrdNo,SOrdDate,SSupCode,SSupName);
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs "+ex);
	                                   }
	                              }
	                              if(iMillCode==1)
	                              {
	                                   try
	                                   {
	                                        new NewDyeingOrderPrint(FW,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable,SItemTable);
	                                        //new DyeingOrderPrintPdfFile(SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs "+ex);
	                                   }
	                              }
	                              if(iMillCode==3)
	                              {
	                                   try
	                                   {
	                                        //new NewMelangeOrderPrint(FW,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable,SItemTable);
	                                        //new MelangeOrderPrintPdfFile(SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs "+ex);
	                                   }
	                              }
	                              if(iMillCode==4)
	                              {
	                                   try
	                                   {
	                                        //new NewKamadhenuOrderPrint(FW,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable,SItemTable);
	                                        //new KamadhenuOrderPrintPdfFile(SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs "+ex);
	                                   }
	                              }

	                         }

	                         try
	                         {
	                              //FTxt.write("</PRE></body></html>");
	                              //FTxt.close();




	                         }catch(Exception ex){ex.printStackTrace();}
	                    }
	               }
	               try
	               {
	                    FW.close();
	               }
	               catch(Exception ex){}
	               showFileView(common.getPrintPath()+SFile);
			}
          }
     }

     public class MailList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
			if(isValidData())
			{
	               String SFile = TFile.getText();
                       int imailSend = -1;
	     
	               if((SFile.trim()).length()==0)
	                    SFile = "1.prn";
	               try
	               {
	                     FW       = new FileWriter(common.getPrintPath()+SFile);
	               }
	               catch(Exception ex){}

	               for(int i=0;i<RowData.length;i++)
	               {
	                    Boolean Bselected = (Boolean)RowData[i][0];

	                    if(Bselected.booleanValue())
	                    {
	                         String SOrdNo     = (String)VOrdNo      .elementAt(i);
	                         String SOrdDate   = (String)VOrdDate    .elementAt(i);
	                         String SSupCode   = (String)VSupCode    .elementAt(i);
	                         String SSupName   = (String)VSupName    .elementAt(i);
	                         String SMill      = "";

	                         String SMail      = (String)VEMail      .elementAt(i);
                              if(((String)VGrnStatus.elementAt(i)).equals("1"))
                                   continue;

                              //  String SPdfFile   = "/root/"+SSupName+"_"+SOrdNo+".pdf"; 
                           
	                          String SPdfFile = common.getMailPath()+SSupName+"_"+SOrdNo+".pdf";
     
	                         try
	                         {
	                              FTxt = new FileWriter(common.getMailPath()+SSupName+"_"+SOrdNo+".html");
	                              FTxt . write("<html><body><PRE>");
	                         }catch(Exception ex){ex.printStackTrace();}

	                         if(!bflag)
	                         {
	                              String SBlock     = (String)VBlock.elementAt(i);
	                              String SBlockCode = (String)VBlockCode.elementAt(i);

	                              if(iMillCode==0)
	                              {
	                                   try
	                                   {
	                                        new OrderPrint(FW,SOrdNo,SBlock,SBlockCode,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs "+ex);
	                                   }
	                              }
	                              if(iMillCode==1)
	                              {
	                                   try
	                                   {
	                                        new DyeingOrderPrint(FW,SOrdNo,SBlock,SBlockCode,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs "+ex);
	                                   }
	                              }
	                         }
	                         else
	                         {
	                              if(iMillCode==0)
	                              {
	                                   try
	                                   {
	                                        SMill = " AMARJOTHI SPINNING MILLS LTD ";
	                                        //new OrderPrintTextFile(FTxt,SOrdNo,SOrdDate,SSupCode,SSupName);
	                                       // new OrderPrintPdfFile(SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode); 
                                                if(JCGSTType.getSelectedIndex() == 0){
                                                new OrderGSTPrintPdfFile(SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode); 
                                                 }else if(JCGSTType.getSelectedIndex() == 1){
                                                new OrderPrintPdfFile(SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode); 
                                                }
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs Mail"+ex);
	                                   }
	                              }
	                              if(iMillCode==1)
	                              {
	                                   try
	                                   {
	                                   SMill = " AMARJOTHI SPINNING MILLS (DYEING DIVISION) ";
	                                        //new DyeingOrderPrintTextFile(FTxt,SOrdNo,SOrdDate,SSupCode,SSupName);
                                              
	                                        
                                                if(JCGSTType.getSelectedIndex() == 0){
                                                new DyeingOrderGSTPrintPdfFile(SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);  
                                                }else if(JCGSTType.getSelectedIndex() == 1){
                                                new DyeingOrderPrintPdfFile(SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode); 
                                                }
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs Mail"+ex);
	                                   }
	                              }
	                              if(iMillCode==3)
	                              {
	                                   try
	                                   {
	                                        SMill = " AMARJOTHI COLOUR MELANGE SPINNING MILLS LIMITED ";
	                                        //new ColorMelangeOrderPrintTextFile(FTxt,SOrdNo,SOrdDate,SSupCode,SSupName);
	                                        //new MelangeOrderPrintPdfFile(SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs Mail"+ex);
	                                   }
	                              }
	                              if(iMillCode==4)
	                              {
	                                   try
	                                   {
	                                        SMill = " SRI KAMADHENU TEXTILES ";
	                                        //new KamadhenuOrderPrintTextFile(FTxt,SOrdNo,SOrdDate,SSupCode,SSupName);
	                                        //new KamadhenuOrderPrintPdfFile(SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs Mail"+ex);
	                                   }
	                              }

                                 if(iMillCode==6)
	                              {
	                                   try
	                                   {
	                                        SMill = "RPJ TEXTILES  LTD ";
	                                             if(JCGSTType.getSelectedIndex() == 0 || JCGSTType.getSelectedIndex() == 1 ){
                                                new RPJPrintPdfFile(SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode); 
                                               // new RPJPdfFile(document,SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);  
                                                 }
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs Mail"+ex);
	                                   }
	                              }

	                         }

	                         try
	                         {
	                              FTxt.write("</PRE></body></html>");
	                              FTxt.close();


	                              String sCc=SMail;
	                              String sToMail=SMail;

	//                            sToMail = sToMail + ".readnotify.com";

	                              String sSubject="Amarjothi Purchase Order";
	                              String sText="New OrderNo "+SOrdNo+" OrderDate "+SOrdDate+" From : "+SMill+" \n For Order Details Check :"+SMail+" \n If not available please check spam folder";
	                              String   sAttachment=common.getMailPath()+SSupName+"_"+SOrdNo+".pdf";

	                              int iIndex = SMail.indexOf("@");

	                              if(iIndex!=-1)
	                              {
                                        if(iMailSenderStatus == 0)
                                        {
                                             imailSend = SendMailAttach.createMail(sCc,  sToMail,  sSubject,  sText,  sAttachment);
                                        }
                                        else
                                        {
                                             imailSend = SMTPSendMailAttach.createMail(sCc,  sToMail,  sSubject,  sText,  sAttachment);
                                        }

                                        if(imailSend==0)
                                        {
                                             updateMail(SOrdNo);
                                        }

                                        /*
                                             else
                                             {
                                                  return;
                                             }
                                        */
	                              }

	                              String SMessage = " "+SMill+" OrdNo : "+SOrdNo+" has been placed . Pls check ur E-Mail Id for further details: "+sToMail+" ";

	                              insertSMS(SSupCode,SMessage);

	                         }catch(Exception ex){ex.printStackTrace();}
	                    }
	               }
	               try
	               {
	                    FW.close();
	               }
	               catch(Exception ex){}
	//               showFileView(common.getPrintPath()+SFile);

	                /*getMobileNoAndContactPerson();

	                setDataIntoVector();
	                setRowData();
	                try
	                {
	                     getContentPane().remove(tabreport);
	                }
	                catch(Exception ex){}
	                try
	                {
					   
                        tabreport   = new OrderAbsTabReport(RowData,ColumnData,ColumnType,VGrnStatus);
	                   getContentPane().add(tabreport,BorderLayout.CENTER);
	                   setSelected(true);
	                   DeskTop     .repaint();
	                   DeskTop     .updateUI();
	                }
	                catch(Exception ex)
	                {
	                   System.out.println(ex);
	                }*/
	          }
		}

     }

     public boolean isValidData()
     {
          for(int i=0;i<RowData.length;i++)
          {
               Boolean Bselected = (Boolean)RowData[i][0];

               if(Bselected.booleanValue())
               {
//                String SJMDStatus = (String)RowData[i][15];
                String SJMDStatus = (String)RowData[i][8];

				if(SJMDStatus.equals("Not Approved"))
				{
               		String SOrderNo = (String)RowData[i][1];
					JOptionPane.showMessageDialog(null," OrderNo - "+SOrderNo+" Not Approved by JMD","Dear User",JOptionPane.INFORMATION_MESSAGE);

					return false;
				}
			}
		}

          return true;
     }

     public void showFileView(String SFileName)
     {
          JInternalFrame jf = new JInternalFrame(SFileName);
          Notepad        np = new Notepad(new File(SFileName),SPanel);

          jf.getContentPane().add("Center",np);
          jf.show();
          jf.setBounds(80,100,550,350);
          jf.setClosable(true);
          jf.setMaximizable(true);

          try
          {
               DeskTop.add(jf);
               jf.moveToFront();
               jf.setSelected(true);
               DeskTop.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {

               getMobileNoAndContactPerson();
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
			     
                  tabreport   = new OrderAbsTabReport(RowData,ColumnData,ColumnType,VGrnStatus);
                  getContentPane().add(tabreport,BorderLayout.CENTER);
                  setSelected(true);
                  DeskTop     .repaint();
                  DeskTop     .updateUI();
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
               }
          }
     }

     public void setDataIntoVector()
     {
          VOrdDate     = new Vector();
          VOrdNo       = new Vector();
          VSupName     = new Vector();
          VBasic       = new Vector();
          VDisc        = new Vector();
          VCenVat      = new Vector();
          VTax         = new Vector();
          VSur         = new Vector();
          VAdd         = new Vector();
          VLess        = new Vector();
          VNet         = new Vector();
          VSupCode     = new Vector();
          VEMail       = new Vector();
          VContractPerson = new Vector();
          VMobileNo       = new Vector();
		 VJMDStatus	 = new Vector();
          VJMDAppDate  = new Vector();
          VJMDAppNo    = new Vector();
          VStatus      = new Vector();
		  VGrnNo       = new Vector();
		  VGrnStatus   = new Vector();

          if(!bflag)
          {
               VBlock       = new Vector();
               VBlockCode   = new Vector();
          }

          SStDate = TStDate.TDay.getText()+"."+TStDate.TMonth.getText()+"."+TStDate.TYear.getText();
          SEnDate = TEnDate.TDay.getText()+"."+TEnDate.TMonth.getText()+"."+TEnDate.TYear.getText();
          String StDate  = TStDate.TYear.getText()+TStDate.TMonth.getText()+TStDate.TDay.getText();
          String EnDate  = TEnDate.TYear.getText()+TEnDate.TMonth.getText()+TEnDate.TDay.getText();

          iMailSenderStatus   = 0;

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();

              String QString = getQString(StDate,EnDate);
              ResultSet res  = stat.executeQuery(QString);
              if(bflag)
              {
                   while (res.next())
                   {
                         String str1  = res.getString(1);  
                         String str3  = res.getString(2);
                         String str4  = res.getString(3);
                         String str5  = res.getString(4);
                         String str6  = res.getString(5);
                         String str7  = res.getString(6);
                         String str8  = res.getString(7);
                         String str9  = res.getString(8);
                         String str10 = res.getString(9);
                         String str12 = res.getString(10);
                         String str13 = res.getString(11);
                         String str14 = res.getString(12);
                         String str15 = res.getString(13);
                         String str16 = res.getString(14);
                         String str17 = res.getString(15);
                         String str18 = res.getString(16);
                         String str19 = res.getString(17);
                         String str20 = res.getString(18);

                         int iJmdStatus   = res.getInt(19);
                         int iIaStatus   = res.getInt(20);
                         int iSoStatus   = res.getInt(21);
						 String SGrnNo   = res.getString(22);


                         String SStatus = getStatus(iJmdStatus,iIaStatus,iSoStatus);
                         
                         str3         = common.parseDate(str3);
                         
                         VOrdNo       .addElement(str1);
                         VOrdDate     .addElement(str3);
                         VSupName     .addElement(str4);
                         VBasic       .addElement(str5);
                         VDisc        .addElement(str6);
                         VCenVat      .addElement(str7);
                         VTax         .addElement(str8);
                         VSur         .addElement(str9);
                         VNet         .addElement(str10);
                         VSupCode     .addElement(str12);
                         VAdd         .addElement(str13);
                         VLess        .addElement(str14);
                         VEMail       .addElement(str15);

//                         VContractPerson . addElement(getContractPerson(res.getString(12)));
//                         VMobileNo       . addElement(getMobileNo(res.getString(12)));

                         VContractPerson . addElement(str16);
                         VMobileNo       . addElement(str17);
                         VJMDStatus      . addElement(str18);
                         VJMDAppDate     . addElement(common.parseDate(str19));
                         VJMDAppNo       . addElement(str20);
                         VStatus         . addElement(SStatus);
						 VGrnNo          . addElement(SGrnNo);
						 
						String SGrnStatus="";
                        int iGrnNo = common.toInt(SGrnNo);
						
						if(iGrnNo>0)
						{
							SGrnStatus = "1";
						}
						else
						{
							SGrnStatus = "0";
						}
						
                        VGrnStatus.addElement(SGrnStatus);
						 
                    }
               }
               else
               {
                    while (res.next())
                    {
                         String str1  = res.getString(1);  
                         String str2  = res.getString(2);
                         String str3  = res.getString(3);
                         String str4  = res.getString(4);
                         String str5  = res.getString(5);
                         String str6  = res.getString(6);
                         String str7  = res.getString(7);
                         String str8  = res.getString(8);
                         String str9  = res.getString(9);
                         String str10 = res.getString(10);
                         String str11 = res.getString(11);
                         String str12 = res.getString(12);
                         String str13 = res.getString(13);
                         String str14 = res.getString(14);
                         String str15 = res.getString(15);
                         String str16 = res.getString(16);
                         String str17 = res.getString(17);
                         String str18 = res.getString(18);
                         String str19 = res.getString(19);
                         String str20 = res.getString(20);


                         int iJmdStatus  = res.getInt(21);
                         int iIaStatus   = res.getInt(22);
                         int iSoStatus   = res.getInt(23);
						 String SGrnNo   = res.getString(24);

                         String SStatus = getStatus(iJmdStatus,iIaStatus,iSoStatus);

                         str3        = common.parseDate(str3);
                         
                         VOrdNo    .addElement(str1);
                         VBlock    .addElement(str2);
                         VOrdDate  .addElement(str3);
                         VSupName  .addElement(str4);
                         VBasic    .addElement(str5);
                         VDisc     .addElement(str6);
                         VCenVat   .addElement(str7);
                         VTax      .addElement(str8);
                         VSur      .addElement(str9);
                         VNet      .addElement(str10);
                         VBlockCode.addElement(str11);
                         VSupCode  .addElement(str12);
                         VAdd      .addElement(str13);
                         VLess     .addElement(str14);
                         VEMail    .addElement(str15);

//                         VContractPerson . addElement(getContractPerson(res.getString(12)));
//                         VMobileNo       . addElement(getMobileNo(res.getString(12)));

                         VContractPerson . addElement(str16);
                         VMobileNo       . addElement(str17);
                         VJMDStatus      . addElement(str18);

                         VJMDAppDate     . addElement(common.parseDate(str19));
                         VJMDAppNo       . addElement(str20);

                         VStatus         . addElement(SStatus);
						 VGrnNo          . addElement(SGrnNo);
						  String SGrnStatus="";

				        if(!SGrnNo.equals(" "))
						{
							SGrnStatus = "0";
						}
						else
						{
							SGrnStatus = "1";
						}

                        VGrnStatus.addElement(SGrnStatus);


                    }
               }
               res  . close();

               String QS      = "Select Status from SCM.MailSenderStatus ";

               ResultSet rs   = stat.executeQuery(QS);
               if(rs.next())
               {
                    iMailSenderStatus   = common.toInt(rs.getString(1));
               }
               rs.close();

               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
         RowData     = new Object[VOrdDate.size()][13];
         for(int i=0;i<VOrdDate.size();i++)
         {
/*             RowData[i][0]  = new Boolean(false);
               RowData[i][1]  = (String)VOrdNo.elementAt(i);
               RowData[i][2]  = (String)VOrdDate.elementAt(i);
               RowData[i][3]  = (String)VSupName.elementAt(i);
               RowData[i][4]  = (String)VBasic.elementAt(i);
               RowData[i][5]  = (String)VDisc.elementAt(i);
               RowData[i][6]  = (String)VCenVat.elementAt(i);
               RowData[i][7]  = (String)VTax.elementAt(i);
               RowData[i][8]  = (String)VSur.elementAt(i);
               RowData[i][9]  = (String)VAdd.elementAt(i);
               RowData[i][10] = (String)VLess.elementAt(i);
               double dRNet   = common.toDouble((String)VNet.elementAt(i))+common.toDouble((String)VAdd.elementAt(i))-common.toDouble((String)VLess.elementAt(i));
               RowData[i][11] = ""+ dRNet;
               RowData[i][12] = common.parseNull((String)VEMail.elementAt(i));
               RowData[i][13] = common.parseNull((String)VContractPerson. elementAt(i));
               RowData[i][14] = common.parseNull((String)VMobileNo      . elementAt(i));
               RowData[i][15] = common.parseNull((String)VJMDStatus     . elementAt(i));
               RowData[i][16] = common.parseNull((String)VJMDAppNo      . elementAt(i));
               RowData[i][17] = common.parseNull((String)VJMDAppDate    . elementAt(i));
*/

               RowData[i][0]  = new Boolean(false);
               RowData[i][1]  = (String)VOrdNo.elementAt(i);
               RowData[i][2]  = (String)VOrdDate.elementAt(i);
               RowData[i][3]  = (String)VSupName.elementAt(i);
               double dRNet   = common.toDouble((String)VNet.elementAt(i))+common.toDouble((String)VAdd.elementAt(i))-common.toDouble((String)VLess.elementAt(i));
               RowData[i][4]  = ""+ dRNet;
               RowData[i][5]  = common.parseNull((String)VEMail.elementAt(i));
               RowData[i][6]  = common.parseNull((String)VContractPerson. elementAt(i));
               RowData[i][7]  = common.parseNull((String)VMobileNo      . elementAt(i));
               RowData[i][8]  = common.parseNull((String)VJMDStatus     . elementAt(i));
               RowData[i][9]  = common.parseNull((String)VJMDAppNo      . elementAt(i));
               RowData[i][10] = common.parseNull((String)VJMDAppDate   . elementAt(i));
               RowData[i][11] = common.parseNull((String)VStatus     . elementAt(i));
	         RowData[i][12] = common.parseNull((String)VGrnNo      . elementAt(i));
        }  
     }

     public String getQString(String StDate,String EnDate)
     {
          int iNo = common.toInt(TNo.getText());

          DateField df = new DateField();
          df.setTodayDate();
          String SToday = df.TYear.getText()+df.TMonth.getText()+df.TDay.getText();
          String QString  = "";

          int iMail=0;

          if(iMailStatus==1)
                iMail = 0;
          if(iMailStatus==2)
                iMail = 1;



       /*   if(bflag)
          {
               QString = "SELECT PurchaseOrder.OrderNo, PurchaseOrder.OrderDate, "+SSupTable+".Name, Sum(PurchaseOrder.qty*rate) AS Expr1, Sum(PurchaseOrder.Disc) AS SumOfDisc, Sum(PurchaseOrder.Cenvat) AS SumOfCenvat, Sum(PurchaseOrder.Tax) AS SumOfTax, Sum(PurchaseOrder.Sur) AS SumOfSur, Sum(PurchaseOrder.qty*rate)-Sum(PurchaseOrder.Disc)+Sum(PurchaseOrder.Cenvat)+Sum(PurchaseOrder.Tax)+Sum(PurchaseOrder.Sur),PurchaseOrder.Sup_Code,PurchaseOrder.Plus,PurchaseOrder.Less,EMail "+
                         "FROM (PurchaseOrder INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code) "+
                         "Inner Join OrdBlock On PurchaseOrder.OrderBlock=OrdBlock.Block "+
                         "Where PurchaseOrder.MillCode="+iMillCode;


                         if(iMailStatus!=0)
                            QString = QString + " and MailStatus = "+iMail;

                         QString = QString +"GROUP BY PurchaseOrder.OrderNo, PurchaseOrder.OrderDate, "+SSupTable+".Name,PurchaseOrder.Sup_Code,PurchaseOrder.Plus,PurchaseOrder.Less,EMail ";
          }
          else
          {
               QString = "SELECT PurchaseOrder.OrderNo, OrdBlock.BlockName, PurchaseOrder.OrderDate, "+SSupTable+".Name, Sum(PurchaseOrder.qty*rate) AS Expr1, Sum(PurchaseOrder.Disc) AS SumOfDisc, Sum(PurchaseOrder.Cenvat) AS SumOfCenvat, Sum(PurchaseOrder.Tax) AS SumOfTax, Sum(PurchaseOrder.Sur) AS SumOfSur,Sum(PurchaseOrder.qty*rate)-Sum(PurchaseOrder.Disc)+Sum(PurchaseOrder.Cenvat)+Sum(PurchaseOrder.Tax)+Sum(PurchaseOrder.Sur),PurchaseOrder.OrderBlock,PurchaseOrder.Sup_Code,PurchaseOrder.Plus,PurchaseOrder.Less,EMail "+
                         "FROM (PurchaseOrder INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code) "+
                         "Inner Join OrdBlock On PurchaseOrder.OrderBlock=OrdBlock.Block "+
                         "Where PurchaseOrder.MillCode="+iMillCode;

                         if(iMailStatus!=0)
                            QString = QString + " and MailStatus = "+iMail;

                         QString = QString +"GROUP BY PurchaseOrder.OrderNo, OrdBlock.BlockName,PurchaseOrder.OrderBlock, PurchaseOrder.OrderDate, "+SSupTable+".Name,PurchaseOrder.OrderBlock,PurchaseOrder.Sup_Code,PurchaseOrder.Plus,PurchaseOrder.Less,EMail ";
          }

          */


          if(bflag)
          {
                                       QString = "SELECT PurchaseOrder.OrderNo, PurchaseOrder.OrderDate, "+SSupTable+".Name, "+
                        " Sum(PurchaseOrder.qty*rate) AS Expr1, Sum(PurchaseOrder.Disc) AS SumOfDisc, Sum(PurchaseOrder.Cenvat) AS SumOfCenvat, "+
                        " Sum(PurchaseOrder.Tax) AS SumOfTax, Sum(PurchaseOrder.Sur) AS SumOfSur, "+
                        " Sum(PurchaseOrder.qty*rate)-Sum(PurchaseOrder.Disc)+Sum(PurchaseOrder.Cenvat)+Sum(PurchaseOrder.Tax)+Sum(PurchaseOrder.Sur), "+
                        " PurchaseOrder.Sup_Code,PurchaseOrder.Plus,PurchaseOrder.Less,PartyMaster.EMail,PartyMaster.ContPerson1,PartyMaster.PurMobileNo, "+
                        " decode(PurchaseOrder.JMDOrderApproval,0,'Not Approved','Approved') as JMDApproval,JmdOrderAppDate,POGroupNo,PurchaseOrder.JmdOrderApproval,PurchaseOrder.IAOrderApproval,PurchaseOrder.SOOrderApproval ,max(Grn.GrnNo)"+
                        "FROM (PurchaseOrder INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code) "+
			            "Left join Grn on (Grn.OrderNo = PurchaseOrder.OrderNo and grn.millcode=purchaseorder.millcode)  "+
                        "Inner Join OrdBlock On PurchaseOrder.OrderBlock=OrdBlock.Block "+
                        "Inner Join PartyMaster On PartyMaster.PartyCode="+SSupTable+".Ac_Code "+
                        "Where PurchaseOrder.MillCode="+iMillCode;

                         if(iMailStatus!=0)
                            QString = QString + " and MailStatus = "+iMail;

                  if(JCType.getSelectedIndex() == 1)
                  {
                        QString = QString+" And PurchaseOrder.JMDOrderApproval= 1";
                  }

                  if(JCType.getSelectedIndex() == 2)
                  {
                        QString = QString+" And PurchaseOrder.JMDOrderApproval= 0";
                  }
                 if(JCGrnStatus.getSelectedIndex() == 1)
                  {
                        QString = QString+" And Grn.GrnNo>0";
                  }
                  if(JCGrnStatus.getSelectedIndex() == 2)
                  {
                        QString = QString+" And (Grn.GrnNo<=0 or Grn.GrnNo is null)";
                  }
                  

                  if(JCGSTType.getSelectedIndex() == 0)
                  {
                        QString = QString+" And PurchaseOrder.GSTSTATUS= 0";
                  }
                 else
                  {
                        QString = QString+" And PurchaseOrder.GSTSTATUS= 1";
                  }




                         QString = QString +"GROUP BY PurchaseOrder.OrderNo, PurchaseOrder.OrderDate, "+SSupTable+".Name, "+
                " PurchaseOrder.Sup_Code,PurchaseOrder.Plus,PurchaseOrder.Less,PartyMaster.EMail,PartyMaster.ContPerson1,PartyMaster.PurMobileNo, "+
                " decode(PurchaseOrder.JMDOrderApproval,0,'Not Approved','Approved'),JmdOrderAppDate,POGroupNo,PurchaseOrder.JmdOrderApproval,PurchaseOrder.IAOrderApproval ,PurchaseOrder.SOOrderApproval ";
                          }
                          else
                          {
                               QString = "SELECT PurchaseOrder.OrderNo, OrdBlock.BlockName, PurchaseOrder.OrderDate, "+SSupTable+".Name, "+
                                         " Sum(PurchaseOrder.qty*rate) AS Expr1, Sum(PurchaseOrder.Disc) AS SumOfDisc, Sum(PurchaseOrder.Cenvat) AS SumOfCenvat, "+
                                         " Sum(PurchaseOrder.Tax) AS SumOfTax, Sum(PurchaseOrder.Sur) AS SumOfSur, "+
                                         " Sum(PurchaseOrder.qty*rate)-Sum(PurchaseOrder.Disc)+Sum(PurchaseOrder.Cenvat)+Sum(PurchaseOrder.Tax)+Sum(PurchaseOrder.Sur), "+
                                         " PurchaseOrder.OrderBlock,PurchaseOrder.Sup_Code,PurchaseOrder.Plus,PurchaseOrder.Less,PartyMaster.EMail, "+
                                         " PartyMaster.ContPerson1, PartyMaster.PurMobileNo, "+
                                         " decode(PurchaseOrder.JMDOrderApproval,0,'Not Approved','Approved') as JMDApproval,JmdOrderAppDate ,POGroupNo,PurchaseOrder.JmdOrderApproval,PurchaseOrder.IAOrderApproval ,PurchaseOrder.SOOrderApproval,max(Grn.GrnNo) "+
                                         "FROM (PurchaseOrder INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code) "+
						     "Left join Grn on Grn.OrderNo = PurchaseOrder.OrderNo "+
                                         "Inner Join OrdBlock On PurchaseOrder.OrderBlock=OrdBlock.Block "+
                                         "Where PurchaseOrder.MillCode="+iMillCode;
                
                                         if(iMailStatus!=0)
                                            QString = QString + " and MailStatus = "+iMail;


                  if(JCType.getSelectedIndex() == 1)
                  {
                        QString = QString+" And PurchaseOrder.JMDOrderApproval= 1";
                  }

                  if(JCType.getSelectedIndex() == 2)
                  {
                        QString = QString+" And PurchaseOrder.JMDOrderApproval= 0";
                  }
                  if(JCGrnStatus.getSelectedIndex() == 1)
                  {
                        QString = QString+" And Grn.GrnNo>0";
                  }
                  if(JCGrnStatus.getSelectedIndex() == 2)
                  {
                        QString = QString+" And (Grn.GrnNo<=0 or Grn.GrnNo is null)";
                  }
                    if(JCGSTType.getSelectedIndex() == 0)
                  {
                        QString = QString+" And PurchaseOrder.GSTSTATUS= 0";
                  }
                 else
                  {
                        QString = QString+" And PurchaseOrder.GSTSTATUS= 1";
                  }


                                         QString = QString +"GROUP BY PurchaseOrder.OrderNo, OrdBlock.BlockName,PurchaseOrder.OrderBlock, "+
                " PurchaseOrder.OrderDate, "+SSupTable+".Name,PurchaseOrder.OrderBlock,PurchaseOrder.Sup_Code,PurchaseOrder.Plus, "+
                " PurchaseOrder.Less,PartyMaster.EMail,PartyMaster.ContPerson1,PartyMaster.PurMobileNo, "+
                " decode(PurchaseOrder.JMDOrderApproval,0,'Not Approved','Approved'),JmdOrderAppDate,POGroupNo,PurchaseOrder.JmdOrderApproval,PurchaseOrder.IAOrderApproval ,PurchaseOrder.SOOrderApproval ";
                          }


          if(iNo > 0)
          {
               QString = QString+" Having PurchaseOrder.OrderNo = "+iNo;
          }
          else
          {

             if(JRDate.isSelected())
             {
               QString = QString+" Having PurchaseOrder.OrderDate >= '"+StDate+"' and PurchaseOrder.OrderDate <='"+EnDate+"'";
             }
             else
             {
               QString = QString+" Having PurchaseOrder.JmdOrderAppDate >= '"+StDate+"' and PurchaseOrder.JmdOrderAppDate <='"+EnDate+"'";

             }
          }



//               QString = QString+" Having PurchaseOrder.OrderDate >= '"+StDate+"' and PurchaseOrder.OrderDate <='"+EnDate+"'";

          if(JCOrder.getSelectedIndex() == 0)      
               QString = QString+" Order By PurchaseOrder.OrderNo,PurchaseOrder.OrderDate";
          if(JCOrder.getSelectedIndex() == 1)      
               QString = QString+" Order By "+SSupTable+".Name,PurchaseOrder.OrderDate";
          if(JCOrder.getSelectedIndex() == 2)      
               QString = QString+" Order By PurchaseOrder.OrderDate";
          if(JCOrder.getSelectedIndex() == 3)      
               QString = QString+" Order By PurchaseOrder.JmdOrderAppDate";
System.out.println("Q:"+QString);
          return QString;
		  
     }

     public boolean getReportYear(String SFinYear)
     {
          SFinYear       = SFinYear.trim();
          String SYear   = SFinYear.substring(SFinYear.length()-2,SFinYear.length());
          int iYear      = common  .toInt(SYear);
          
          if(iYear>7)
               return true;
          else
               return false;
     }

     public String getShortName()
     {
          String SSName="";

          try
          {
                  String QS = " Select ShortName from Mill Where MillCode="+iMillCode;
      
                  ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                  Connection     theConnection  =  oraConnection.getConnection();               
                  Statement      stat           =  theConnection.createStatement();
                  ResultSet      res            =  stat.executeQuery(QS);

                  while(res.next())
                  {
                       SSName = res.getString(1);
                  }
                  res.close();
                  stat.close();
          }
          catch(Exception e)
          {
              System.out.println(e);
          }

          return SSName;
     }


     private void updateMail(String SOrdNo)
     {
            try
            {
                    String QS = " Update PurchaseOrder set MailStatus=1 Where OrderNo='"+SOrdNo+"' and MillCode="+iMillCode+" ";
        
        
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                    Connection     theConnection  =  oraConnection.getConnection();               
                    Statement      stat           =  theConnection.createStatement();
                                   stat.execute(QS);

                    stat.close();
            }
            catch(Exception e)
            {
                System.out.println(e);
            }

     }
     private void insertSMS(String SCustomerCode,String SMessage)
     {
            try
            {
                    String QS = " insert into Tbl_Alert_Collection (CollectionId,StatusId,AlertMessage,CustomerCode,Status,SMSType) Values(alertCollection_seq.nextval,'STATUS9','"+SMessage+"','"+SCustomerCode+"',0,1) ";
        
//                    System.out.println(QS);
                    ORAConnection3  oraConnection  =  ORAConnection3.getORAConnection();
                    Connection     theConnection  =  oraConnection.getConnection();               
                    Statement      stat           =  theConnection.createStatement();
                                   stat.execute(QS);

                    stat.close();
            }
            catch(Exception e)
            {
                System.out.println(e);
            }

     }

     public String getContractPerson(String SSupCode)
     {
        int iIndex=-1;
        iIndex = VSupSupCode.indexOf(SSupCode);
        if(iIndex!=-1)
                return common.parseNull((String)VSupContPerson.elementAt(iIndex));
        else
                return "";

     }
     public String getMobileNo(String SSupCode)
     {
        int iIndex=-1;
        iIndex = VSupSupCode.indexOf(SSupCode);
        if(iIndex!=-1)
                return common.parseNull((String)VSupMobileNo.elementAt(iIndex));
        else
                return "";

     }

     public void getMobileNoAndContactPerson()
     {
          VSupContPerson = new Vector();
          VSupMobileNo   = new Vector();
          VSupSupCode    = new Vector();


          String QS = " Select ContPerson1,PurMobileNo,PartyCode from PartyMaster ";

          System.out.println(QS);
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(QS);

               while (res.next())
               {
                    VSupContPerson.addElement(common.parseNull(res.getString(1)));
                    VSupMobileNo.addElement(common.parseNull(res.getString(2)));
                    VSupSupCode.addElement(common.parseNull(res.getString(3)));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }
     private String getStatus(int iJmdStatus,int iIaStatus,int iSoStatus)
     {
        String SStatus="";

        if(iJmdStatus==0 && iIaStatus==0 && iSoStatus==0)
            SStatus =" Pending With S.O";

        if(iJmdStatus==0 && iIaStatus==0 && iSoStatus==1)
            SStatus =" Pending With IA";

        if(iJmdStatus==0 && iIaStatus==1 && iSoStatus==1)
            SStatus =" Pending With JMD";

        return SStatus;
     }

   //pdf

    public class PdfList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {

                 
              
			if(isValidData())
			{
	             
	               try
	               {
                             
                document = new Document(PageSize.A4);
                document.setMargins(30,10,20,0); 
				PdfWriter.getInstance(document, new FileOutputStream(SPdfFile));
				document.open();
                               
				
                             
	               }
	               catch(Exception ex){ ex.printStackTrace();}

	               for(int i=0;i<RowData.length;i++)
	               {
	                    Boolean Bselected = (Boolean)RowData[i][0];

	                    if(Bselected.booleanValue())
	                    {
	                         String SOrdNo     = (String)VOrdNo      .elementAt(i);
	                         String SOrdDate   = (String)VOrdDate    .elementAt(i);
	                         String SSupCode   = (String)VSupCode    .elementAt(i);
	                         String SSupName   = (String)VSupName    .elementAt(i);
	                         String SMill      = "";

	                         String SMail      = (String)VEMail      .elementAt(i);
                         /* comment by janaki 02.06.2017    if(((String)VGrnStatus.elementAt(i)).equals("1"))
                                                            continue;*/

                               
	                         try
	                         {
 
	                            
	                         }catch(Exception ex){ex.printStackTrace();}

	                         if(!bflag)
	                         {
	                         }
	                         else
	                         {
	                              if(iMillCode==0)
	                              {
	                                   try
	                                   {
	                                        SMill = " AMARJOTHI SPINNING MILLS LTD ";
	                                        //new OrderPrintTextFile(FTxt,SOrdNo,SOrdDate,SSupCode,SSupName);
                                           
                                            if(JCGSTType.getSelectedIndex() == 0)
                                            {
                                                new OrderGSTPdfFile(document,SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);
                                            }else  if(JCGSTType.getSelectedIndex() == 1) {
	                                        new OrderPdfFile(document,SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode); } 
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs Mail"+ex);
	                                   }
	                              }
	                              if(iMillCode==1)
	                              {
	                                   try
	                                   {
	                                   SMill = " AMARJOTHI SPINNING MILLS (DYEING DIVISION) ";
	                                        //new DyeingOrderPrintTextFile(FTxt,SOrdNo,SOrdDate,SSupCode,SSupName);
                                               
                                               if(JCGSTType.getSelectedIndex() == 0){
                                                new DyeingGSTOrderPdfFile(document,SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);  
                                              
                                               }else  if(JCGSTType.getSelectedIndex() == 1)  { 
	                                        new DyeingOrderPdfFile(document,SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode); }
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs Mail"+ex);
	                                   }
	                              }

                                    if(iMillCode==6)
	                                {
	                                   try
	                                   {
	                                   SMill = " RPJ TEXTILES LTD..";
	                                          
                                               if(JCGSTType.getSelectedIndex() == 0  ||  JCGSTType.getSelectedIndex() == 1 ){
                                                new RPJPdfFile(document,SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);  
                                              
                                               }
                                        }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs Mail"+ex);
	                                   }
	                              }
	                              if(iMillCode==3)
	                              {
	                                   try
	                                   {
	                                        SMill = " AMARJOTHI COLOUR MELANGE SPINNING MILLS LIMITED ";
	                                        //new ColorMelangeOrderPrintTextFile(FTxt,SOrdNo,SOrdDate,SSupCode,SSupName);
	                                        //new MelangeOrderPdfFile(document,SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs Mail"+ex);
	                                   }
	                              }
	                              if(iMillCode==4)
	                              {
	                                   try
	                                   {
	                                        SMill = " SRI KAMADHENU TEXTILES ";
	                                        //new KamadhenuOrderPrintTextFile(FTxt,SOrdNo,SOrdDate,SSupCode,SSupName);
	                                        //new KamadhenuOrderPdfFile(document,SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);
	                                   }
	                                   catch(Exception ex)
	                                   {
	                                        System.out.println("From OrderAbs Mail"+ex);
	                                   }
	                              }

	                         }

	                       
	                    }
	               }
	               try
	               {
	                   document.close();
	               }
	               catch(Exception ex){}

                      /* try{
                File theFile   = new File(SPdfFile);
                Desktop        . getDesktop() . open(theFile);
                  

                  
               }catch(Exception ex){}*/

				try
				 {
					 Process p = Runtime.getRuntime()
					 .exec("rundll32 url.dll,FileProtocolHandler  "+SPdfFile);
					 p.waitFor(); 
				}
				catch(Exception ex)
				{
					 System.out.println(" initPDF "+ex);
				}

	            //  showFileView(common.getPrintPath()+SPdfFile);

	                getMobileNoAndContactPerson();

	                setDataIntoVector();
	                setRowData();
	                try
	                {
	                     getContentPane().remove(tabreport);
	                }
	                catch(Exception ex){}
	                try
	                {
					   
                        tabreport   = new OrderAbsTabReport(RowData,ColumnData,ColumnType,VGrnStatus);
	                   getContentPane().add(tabreport,BorderLayout.CENTER);
	                   setSelected(true);
	                   DeskTop     .repaint();
	                   DeskTop     .updateUI();
	                }
	                catch(Exception ex)
	                {
	                   System.out.println(ex);
	                }
	          }
		}

     }


}
