package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class OrderAuthFrame extends JInternalFrame
{
      JLayeredPane Layer;

      JPanel         TopPanel,BottomPanel,MiddlePanel,TopLeft,TopRight;
      AddressField   TShort;
      MyComboBox     JCOrderNo,JCTaxClaim;
      MyButton        BApply,BOk,BCancel,BPool;
      TabReport      theReport;
      JTabbedPane    thePane;
      DateField      fromDate,toDate;
      Vector VOrdDate,VOrdNo,VBlock,VSupName,VItemCode,VItemName,VQty,VRate,VDisPer,VCenvat,VSurper,VTaxClaim,VAuthOrderNo,VSlNo,VClaim,VTClaim,VTaxPer;

      Common common   = new Common();
      int iUserCode,iMillCode;
      String SSupTable;
      ORAConnection connect;
      Connection theconnect;

      public OrderAuthFrame(JLayeredPane Layer,int iUserCode,int iMillCode,String SSupTable)
      {
            this.Layer     = Layer;
            this.iUserCode = iUserCode;
            this.iMillCode = iMillCode;
            this.SSupTable = SSupTable;

            getOrderNo();
            createComponents();
            setLayouts();
            addComponents();
            addListeners();
      }
      public void createComponents()
      {
            try
            {
                 thePane     = new JTabbedPane();
                 JCOrderNo   = new MyComboBox(VAuthOrderNo);
                 JCTaxClaim  = new MyComboBox();
                 VTClaim     = new Vector();

                 VTClaim.addElement("CLAIMABLE");
                 VTClaim.addElement("NOT CLAIMABLE");

                 TopPanel    = new JPanel(true);
                 TopLeft     = new JPanel(true);
                 TopRight    = new JPanel(true);
                 BottomPanel = new JPanel(true);
                 MiddlePanel = new JPanel(true);

                 BApply      = new MyButton("Apply");
                 BOk         = new MyButton("Authenticat");
                 BCancel     = new MyButton("Cancel");

                 if(VAuthOrderNo.size()>0)
                 {
                      BApply.setEnabled(true);
                 }
                 else
                 {
                      BApply.setEnabled(false);
                 }
            }catch(Exception ex)
            {
                ex.printStackTrace();
            }
      }
      public void setLayouts()
      {
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,800,500);
            setTitle("Order TaxClaim Authentication");

            TopPanel.setLayout(new GridLayout(1,2));
            TopLeft.setLayout(new GridLayout(1,2));
            TopLeft.setBorder(new TitledBorder("OrderNo"));
            TopRight.setLayout(new GridLayout(1,1));
            TopRight.setBorder(new TitledBorder("Apply"));
            MiddlePanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());
            TopPanel.setBorder(new TitledBorder("Info"));
      }
      public void addComponents()
      {
            TopLeft.add(new MyLabel("OrderNo"));
            TopLeft.add(JCOrderNo);

            TopRight.add(BApply);

            TopPanel.add(TopLeft);
            TopPanel.add(TopRight);

            BottomPanel.add(BOk);
            BottomPanel.add(BCancel);

            getContentPane().add("North",TopPanel);
            getContentPane().add("Center",MiddlePanel);
            getContentPane().add("South",BottomPanel);
     }
     public void addListeners()
     {
          BOk         .addActionListener(new ActList());
          BCancel     .addActionListener(new ActList());
          BApply      .addActionListener(new ActList());
          JCOrderNo   .addItemListener(new ItemList());
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               String SOrderNo = ((String)JCOrderNo.getSelectedItem());

               if(ae.getSource()==BOk)
               {
                    if(!updateStatus())
                    {
                         updateData();
                         /*showList(SOrderNo);
                         setTabReport();
                         getOrderNo();
                         JCOrderNo.setValues(VAuthOrderNo);*/
                         removeHelpFrame();
                    }
               }
               if(ae.getSource()==BApply)
               {
                    showList(SOrderNo);
                    setTabReport();
               }
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
               }
          }
     }
     private class ItemList implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
               String SOrderNo = ((String)JCOrderNo.getSelectedItem());
               if(ie.getSource()==JCOrderNo)
               {
                    try
                    {
                         showList(SOrderNo);
                    }
                    catch(Exception e)
                    {
                         System.out.println(e);
                         e.printStackTrace();
                    }
               }
          }
     }
     private void setTabReport()
     {
          try
          {
               MiddlePanel.removeAll();
               thePane.removeAll();

               String ColumnName[]  = {"SlNo","OrderNo","OrderDate","Block","Supplier","Code","Name","Qty","Rate","Discount %","Cenvat %","Tax %","Surper %","TaxClaim","Status"};
               String ColumnType[]  = {  "N"   ,"N"        ,"S"      ,"S"     ,"S"     ,"S"   ,"S"   ,"N"  ,"N"    ,"N"      ,"N"      ,"N"   ,"N"     ,"E"       ,"B"  };
               int    ColumnWidth[] = {50,60,100,100,100,100,100,100,100,100,100,100,100,100};

               Object RowData[][] = new Object[VOrdNo.size()][ColumnName.length];
               Object RowData1[][] = new Object[1][ColumnName.length];

               if(VOrdNo.size()>0)
               {
                    for(int i=0;i<VOrdNo.size();i++)
                    {
                         //RowData[i][0]   = common.parseNull((String)VSlNo          .elementAt(i));
                         RowData[i][0]   = String.valueOf(i+1);
                         RowData[i][1]   = common.parseNull((String)VOrdNo         .elementAt(i));
                         RowData[i][2]   = common.parseDate((String)VOrdDate       .elementAt(i));
                         RowData[i][3]   = common.parseNull((String)VBlock         .elementAt(i));
                         RowData[i][4]   = common.parseNull((String)VSupName       .elementAt(i));
                         RowData[i][5]   = common.parseNull((String)VItemCode      .elementAt(i));
                         RowData[i][6]   = common.parseNull((String)VItemName      .elementAt(i));
                         RowData[i][7]   = common.parseNull((String)VQty           .elementAt(i));
                         RowData[i][8]   = common.parseNull((String)VRate          .elementAt(i));
                         RowData[i][9]   = common.parseNull((String)VDisPer        .elementAt(i));
                         RowData[i][10]  = common.parseNull((String)VCenvat        .elementAt(i));
                         RowData[i][11]  = common.parseNull((String)VTaxPer        .elementAt(i));
                         RowData[i][12]  = common.parseNull((String)VSurper        .elementAt(i));
                         RowData[i][13]  = common.parseNull((String)VClaim         .elementAt(i));
                         RowData[i][14]  = new Boolean(false);
                    }
                    theReport      = new TabReport(RowData,ColumnName,ColumnType);
                    theReport.setPrefferedColumnWidth(ColumnWidth);

                    TableColumn SColumnDriver     = theReport.ReportTable.getColumn("TaxClaim");
                    JCTaxClaim                    = new MyComboBox(VTClaim);
                    SColumnDriver.setCellEditor(new DefaultCellEditor(JCTaxClaim));

                    MiddlePanel.add("Center",thePane);
                    thePane.addTab("Purchase Order Details",theReport);
               }
               else
               {
                    RowData1[0][0]   = "";
                    RowData1[0][1]   = "";
                    RowData1[0][2]   = "";
                    RowData1[0][3]   = "";
                    RowData1[0][4]   = "";
                    RowData1[0][5]   = "";
                    RowData1[0][6]   = "";
                    RowData1[0][7]   = "";
                    RowData1[0][8]   = "";
                    RowData1[0][9]   = "";
                    RowData1[0][10]  = "";
                    RowData1[0][11]  = "";
                    RowData1[0][12]  = "";
                    RowData1[0][13]  = "";
                    RowData1[0][14]  = new Boolean(false);

                    theReport      = new TabReport(RowData,ColumnName,ColumnType);
                    theReport.setPrefferedColumnWidth(ColumnWidth);
                    MiddlePanel.add("Center",thePane);
                    thePane.addTab("Purchase Order Details",theReport);

               }
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }
     private void showList(String SOrderNo)
     {
          VOrdDate       = new Vector();
          VOrdNo         = new Vector();
          VBlock         = new Vector();
          VSupName       = new Vector();
          VItemCode      = new Vector();
          VItemName      = new Vector();
          VQty           = new Vector();
          VRate          = new Vector();
          VDisPer        = new Vector();
          VCenvat        = new Vector();
          VSurper        = new Vector();
          VTaxClaim      = new Vector();
          VSlNo          = new Vector();
          VClaim         = new Vector();
          VTaxPer        = new Vector();

          String QS = " Select OrderNo,OrderDate,BlockName,Name,Item_Code,Item_Name,"+
                      " Qty,Rate,DiscPer,CenVatPer,SurPer ,TaxClaimable,SlNo,decode(TaxClaimable,0,'NOT CLAIMABLE','CLAIMABLE') as Claim,TaxPer From PurchaseOrder "+
                      " Inner join "+SSupTable+" on "+SSupTable+".Ac_Code = PurchaseOrder.sup_Code "+
                      " Inner join InvItems on InvItems.Item_Code = PurchaseOrder.Item_Code "+
                      " Inner Join OrdBlock on OrdBlock.Block = PurchaseOrder.OrderBlock "+
                      " Where PurchaseOrder.OrderNo='"+SOrderNo+"' and Authentication = 0 and PurchaseOrder.MillCode="+iMillCode+" "+
                      " Order By 13 ";
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat       = theconnect.createStatement();
               ResultSet theResult  = stat.executeQuery(QS);

               while(theResult.next())
               {
                    VOrdNo      .addElement(theResult.getString(1));
                    VOrdDate    .addElement(theResult.getString(2));
                    VBlock      .addElement(theResult.getString(3));
                    VSupName    .addElement(theResult.getString(4));
                    VItemCode   .addElement(theResult.getString(5));
                    VItemName   .addElement(theResult.getString(6));
                    VQty        .addElement(theResult.getString(7));     
                    VRate       .addElement(theResult.getString(8));
                    VDisPer     .addElement(theResult.getString(9));
                    VCenvat     .addElement(theResult.getString(10));
                    VSurper     .addElement(theResult.getString(11));
                    VTaxClaim   .addElement(theResult.getString(12));
                    VSlNo       .addElement(theResult.getString(13));
                    VClaim      .addElement(theResult.getString(14));
                    VTaxPer     .addElement(theResult.getString(15));
               }
               theResult.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void getOrderNo()
     {
          VAuthOrderNo   = new Vector();

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat       = theconnect.createStatement();
               ResultSet theResult  = stat.executeQuery(" Select distinct OrderNo From PurchaseOrder Where Authentication = 0 and MillCode="+iMillCode+" Order by 1");

               while(theResult.next())
               {
                    VAuthOrderNo.addElement(theResult.getString(1)); 
               }
               theResult.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private boolean updateStatus()
     {
          Boolean bFlag;
          boolean Bflag=false;

          try
          {
               for(int i=0;i<theReport.ReportTable.getRowCount();i++)
               {
                    bFlag  = (Boolean)theReport.ReportTable.getValueAt(i,14);
                    if(!bFlag.booleanValue())
                    {
                         Bflag=true;
                    }
               }
               if(Bflag==true)
                    JOptionPane.showMessageDialog(null,"Click All ","Dear User",JOptionPane.INFORMATION_MESSAGE);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          return Bflag;
     }
     private void updateData()
     {
          Boolean bFlag;
          //String SOrderNo    = "";

          try
          {
               for(int i=0;i<theReport.ReportTable.getRowCount();i++)
               {
                    bFlag  = (Boolean)theReport.ReportTable.getValueAt(i,14);

                    if(bFlag.booleanValue())
                    {
                         String SOrderNo  = (String)theReport.ReportTable.getValueAt(i,1);
                         int    iSerialNo = common.toInt((String)theReport.ReportTable.getValueAt(i,0));
                         int    iSlNo     = common.toInt((String)VSlNo.elementAt(i));
                         String SClaim    = (String)theReport.ReportTable.getValueAt(i,13);
                         int iClaim = 0;

                         if(SClaim.startsWith("CLA"))
                         {
                              iClaim = 1;
                         }
                         else if(SClaim.startsWith("NOT"))
                         {
                              iClaim = 0;
                         }
                         updateAuth(SOrderNo,iSlNo,iClaim);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void updateAuth(String SOrderNo,int iSlNo,int iClaim)
     {
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat       = theconnect.createStatement();
               String QS  = " Update PurchaseOrder set Authentication = 1,TaxClaimable="+iClaim+" Where SlNo="+iSlNo+"  and  OrderNo="+SOrderNo+" and MillCode="+iMillCode;
               stat.executeUpdate(QS);
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private int getSlNo(int iSlNo)
     {
          int index= VSlNo.indexOf(String.valueOf(iSlNo));

          if(index==-1)
              return 0;
          else
              return (common.toInt((String)VSlNo.elementAt(index)));
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
}
