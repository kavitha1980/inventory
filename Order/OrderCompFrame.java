package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class OrderCompFrame extends JInternalFrame
{
     CriteriaPanel  criteriapanel;
     TabReport      tabreport;
     JScrollPane    TabScroll;
     Object         RowData[][];
     String         ColumnData[]= {"Material Code","Material Name","April","May","June","July","August","September","October","November","December","January","Febraury","March"}; 
     String         ColumnType[]= {"S","S","N","N","N","N","N","N","N","N","N","N","N","N"};  
     Vector         VMatRec;
     Common         common = new Common();
     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     Vector         VCode,VName;
     int            iUserCode,iMillCode;
     String         SStDate,SEnDate;
     String         SSupTable;

     public OrderCompFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iMillCode,String SStDate,String SEnDate,String SSupTable)
     {
          super("Monthly Comparison on Purchase Order's");
          this.DeskTop   = DeskTop;
          this.VCode     = VCode;
          this.VName     = VName;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SStDate   = SStDate;
          this.SEnDate   = SEnDate;
          this.SSupTable = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          criteriapanel = new CriteriaPanel(DeskTop,VCode,VName,SPanel,iUserCode,iMillCode,SSupTable);
     }

     public void setLayouts()
     {
          setClosable(true);
          setIconifiable(true);
          setResizable(true);
          setMaximizable(true);
          setBounds(0,0,650,500);
     }

     public void addComponents()
     {
          getContentPane().add(criteriapanel,BorderLayout.NORTH);
     }

     public void addListeners()
     {
          criteriapanel.BApply.addActionListener(new ApplyList());
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(TabScroll); 
               }
               catch(Exception ex)
               {
               }
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    TabScroll           = new JScrollPane(tabreport);
                    getContentPane()    . add(TabScroll,BorderLayout.CENTER);
                    tabreport           . ReportTable.setShowHorizontalLines(true);
                    setSelected(true);
                    DeskTop             . repaint();
                    DeskTop             . updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public void setDataIntoVector()
     {
                    VMatRec   = new Vector();
          MatMonRec matmonrec = new MatMonRec();
          String    SMatCode  = "";      
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
               ResultSet      res            = stat.executeQuery(getQString());

               while (res.next())
               {
                    int       iMon = res.getInt(1);  
                    String    str2 = res.getString(2);
                    String    str3 = res.getString(3);
                    String    str4 = res.getString(4);
                    String    str5 = res.getString(5);

                    if(!str2.equals(SMatCode))
                    {
                         VMatRec   . addElement(matmonrec); 
                         matmonrec = new MatMonRec();
                         matmonrec . SMatCode = str2;
                         matmonrec . SMatName = str3;
                         SMatCode  = str2;
                    }
                    matmonrec.setQty(str4,iMon);
                    matmonrec.setValue(str5,iMon);
               }
               VMatRec   . addElement(matmonrec);
               VMatRec   . removeElementAt(0);

               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VMatRec.size()][14];
          for(int i=0;i<VMatRec.size();i++)
          {
               MatMonRec matmonrec      = (MatMonRec)VMatRec.elementAt(i);
                         RowData[i][0]  = (String)matmonrec.SMatCode;
                         RowData[i][1]  = (String)matmonrec.SMatName;

               if(criteriapanel.JRQty.isSelected())
               {
                    for(int j=0;j<matmonrec.VQty.size();j++)
                         RowData[i][j+2] = (String)matmonrec.VQty.elementAt(j);
               }

               if(criteriapanel.JRValue.isSelected())
               {
                    for(int j=0;j<matmonrec.VValue.size();j++)
                         RowData[i][j+2] = (String)matmonrec.VValue.elementAt(j);
               }

          }  
     }

     public String getQString()
     {
          String QString = "",HString="",str="";
          int    iSig    = 0;

          QString   =    " SELECT substr(PurchaseOrder.OrderDate,5,2), PurchaseOrder.Item_Code, InvItems.Item_Name, sum(PurchaseOrder.Qty),sum(PurchaseOrder.net+PurChaseOrder.Misc) "+
                         " FROM PurchaseOrder INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code "+
                         " Where PurchaseOrder.OrderDate>='"+SStDate+"'"+
                         " and PurchaseOrder.OrderDate<='"+SEnDate+"'"+
                         " GROUP BY substr(PurchaseOrder.OrderDate,5,2),PurchaseOrder.Item_Code,InvItems.Item_Name ";

          if(criteriapanel.JRSeleDept.isSelected())
          {                         
               QString   = QString+", PurchaseOrder.Dept_Code ";
               HString   = "PurchaseOrder.Dept_Code="+(String)criteriapanel.VDeptCode.elementAt(criteriapanel.JCDept.getSelectedIndex());
               iSig      = 1;            
          }

          if(criteriapanel.JRSeleGroup.isSelected())
          {
               QString   = QString+", PurchaseOrder.Group_Code ";
               str       = "PurchaseOrder.Group_Code="+(String)criteriapanel.VGroupCode.elementAt(criteriapanel.JCGroup.getSelectedIndex());
               HString   = (iSig==1 ? " "+HString+" and "+str:str);
               iSig      = 1;            
          }

          if(criteriapanel.JRSeleUnit.isSelected())
          {
               QString   = QString+", PurchaseOrder.Unit_Code ";
               str       = "PurchaseOrder.Unit_Code="+(String)criteriapanel.VUnitCode.elementAt(criteriapanel.JCUnit.getSelectedIndex());
               HString   = (iSig==1 ? " "+HString+" and "+str:str);
               iSig      = 1;
          }

          if(criteriapanel.JRSeleSup.isSelected())
          {
               QString   = QString+", PurchaseOrder.Sup_Code ";
               str       = "PurchaseOrder.Sup_Code='"+criteriapanel.VSupCode.elementAt(criteriapanel.JCSupplier.getSelectedIndex())+"'";
               HString   = (iSig==1 ? " "+HString+" and "+str:str);
               iSig      = 1;
          }
          HString = (iSig>0 ? "Having "+HString : " ");

          return QString+" "+HString+" Order By PurchaseOrder.Item_Code";
     }
}
