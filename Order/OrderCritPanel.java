package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class OrderCritPanel extends JPanel
{
     JPanel    TopPanel,BottomPanel;
     JPanel    ConcernPanel,OrderTypePanel;
     JPanel    UnitPanel;
     JPanel    DeptPanel;
     JPanel    GroupPanel;
     JPanel    BlockPanel;
     JPanel    SupplierPanel;
     JPanel    ListPanel;
     JPanel    SortPanel;
     JPanel    BasePanel;
     JPanel    SourcePanel;
     JPanel    ControlPanel;
     JPanel    MrsTypePanel;
     JPanel    ApplyPanel;

     
     Common    common = new Common();
     Vector    VUnit,VUnitCode,VDept,VDeptCode,VGroup,VGroupCode,VBlock,VBlockCode,VSup,VSupCode,VOrderType,VOrderTypeCode,VOrderSourceCode,VOrderSourceName;
     Vector    VCCode,VCName,VMrsType,VMrsTypeCode;

     JComboBox JCConcern,JCUnit,JCDept,JCGroup,JCBlock,JCSup,JCList,JCOrderType,JCOrderSource,JCMrsType;
     
     JTextField TSort;
     JTextField TStNo,TEnNo;
     
     JRadioButton JRAllConc,JRSeleConc;
     JRadioButton JRAllUnit,JRSeleUnit;
     JRadioButton JRAllDept,JRSeleDept;
     JRadioButton JRAllGroup,JRSeleGroup;
     JRadioButton JRAllBlock,JRSeleBlock;
     JRadioButton JRAllSup,JRSeleSup;
     JRadioButton JRAllList,JRSeleList;
     JRadioButton JRDate,JROrdNo;
     JRadioButton JRAllType,JRType;
     JRadioButton JRAllSourceType,JRSelSourceType;
     JRadioButton JRAllMrsType,JRSelMrsType;

     
     JButton        BApply;
     DateField2      TStDate,TEnDate;
     
     boolean        bsig;
     
     int            iMillCode = 0;
     String         SSupTable;

     JComboBox      JCAuthUser;
     Vector         VAuthUserCode, VAuthUserName;

     OrderCritPanel()
     {
          try
          {
               getUDGBS();
               setConcern();
               createComponents();
               setLayouts();
               addComponents();
               addListeners();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }

     }

     OrderCritPanel(int iMillCode,String SSupTable)
     {
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          
          getUDGBS();
          setConcern();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          
          ConcernPanel   = new JPanel();
          UnitPanel      = new JPanel();
          DeptPanel      = new JPanel();
          GroupPanel     = new JPanel();
          BlockPanel     = new JPanel();
          SupplierPanel  = new JPanel();
          ListPanel      = new JPanel();
          SortPanel      = new JPanel();
          BasePanel      = new JPanel();
          ControlPanel   = new JPanel();
          ApplyPanel     = new JPanel();
          OrderTypePanel = new JPanel();
          SourcePanel    = new JPanel();
          MrsTypePanel   = new JPanel();
          
          JRAllConc      = new JRadioButton("All",true);
          JRSeleConc     = new JRadioButton("Selected");
          JCConcern      = new JComboBox(VCName);
          JCConcern      .setEnabled(false);

          JRAllType      = new JRadioButton("All",true);
          JRType         = new JRadioButton("Seleced");
          JCOrderType    = new JComboBox(VOrderType);
          JCOrderType    .setEnabled(false);
          
          JRAllUnit      = new JRadioButton("All",true);
          JRSeleUnit     = new JRadioButton("Selected");
          JCUnit         = new JComboBox(VUnit);
          JCUnit         .setEnabled(false);
          
          JRAllDept      = new JRadioButton("All",true);
          JRSeleDept     = new JRadioButton("Selected");
          JCDept         = new JComboBox(VDept);
          JCDept         .setEnabled(false);
          
          JRAllGroup     = new JRadioButton("All",true);
          JRSeleGroup    = new JRadioButton("Selected");
          JCGroup        = new JComboBox(VGroup);
          JCGroup        .setEnabled(false);
          
          JRAllBlock     = new JRadioButton("All",true);
          JRSeleBlock    = new JRadioButton("Selected");
          JCBlock        = new JComboBox(VBlock);
          JCBlock        .setEnabled(false);
          
          JRAllSup       = new JRadioButton("All",true);
          JRSeleSup      = new JRadioButton("Selected");
          JCSup          = new JComboBox(VSup);
          JCSup          .setEnabled(false);
          
          JRAllList      = new JRadioButton("All",true);
          JRSeleList     = new JRadioButton("Selected");
          JCList         = new JComboBox();
          JCList         .setEnabled(false);

          JRAllSourceType= new JRadioButton("All",true);
          JRSelSourceType= new JRadioButton("Selected");
          JCOrderSource  = new JComboBox(VOrderSourceName);
          JCOrderSource  .setEnabled(false);


          JRAllMrsType= new JRadioButton("All",true);
          JRSelMrsType= new JRadioButton("Selected");
          JCMrsType  = new JComboBox(VMrsType);
          JCMrsType  .setEnabled(false);


          JRDate         = new JRadioButton("Date",true);
          JROrdNo        = new JRadioButton("Order No");

          bsig           = true;


          TSort          = new JTextField();
          TStNo          = new JTextField();
          TEnNo          = new JTextField();
          
          TStDate        = new DateField2();
          TEnDate        = new DateField2();
          
          BApply         = new JButton("Apply");
          
          TStDate        .setTodayDate();
          TEnDate        .setTodayDate();


          setAuthUsers();

          JCAuthUser     = new JComboBox(VAuthUserName);


     }

     public void setLayouts()
     {
          setLayout(new GridLayout(2,1));
          
          TopPanel.setLayout(new GridLayout(1,5));
          
          BottomPanel    .setLayout(new GridLayout(1,8));
          ConcernPanel   .setLayout(new GridLayout(3,1));
          UnitPanel      .setLayout(new GridLayout(3,1));
          DeptPanel      .setLayout(new GridLayout(3,1));
          GroupPanel     .setLayout(new GridLayout(3,1));
          BlockPanel     .setLayout(new GridLayout(3,1));
          SupplierPanel  .setLayout(new GridLayout(3,1));
          ListPanel      .setLayout(new GridLayout(3,1));
          SortPanel      .setLayout(new GridLayout(2,1));
          BasePanel      .setLayout(new GridLayout(2,1));
          ControlPanel   .setLayout(new GridLayout(2,1));
          ApplyPanel     .setLayout(new GridLayout(3,1));
          OrderTypePanel .setLayout(new GridLayout(3,1));
          SourcePanel    .setLayout(new GridLayout(3,1));
          MrsTypePanel   .setLayout(new GridLayout(3,1));

          ConcernPanel   .setBorder(new TitledBorder("Concern"));
          UnitPanel      .setBorder(new TitledBorder("Processing Units"));
          DeptPanel      .setBorder(new TitledBorder("Department"));
          GroupPanel     .setBorder(new TitledBorder("Group"));
          BlockPanel     .setBorder(new TitledBorder("Order Block"));
          SupplierPanel  .setBorder(new TitledBorder("Supplier"));
          ListPanel      .setBorder(new TitledBorder("List Only"));
          SortPanel      .setBorder(new TitledBorder("Sort on"));
          BasePanel      .setBorder(new TitledBorder("Based on"));
          ControlPanel   .setBorder(new TitledBorder("Control"));
          ApplyPanel     .setBorder(new TitledBorder("Users & Apply"));
          OrderTypePanel .setBorder(new TitledBorder("Order Type"));
          SourcePanel    .setBorder(new TitledBorder("Order Source"));
          MrsTypePanel   .setBorder(new TitledBorder("Mrs Type"));
     }

     public void addComponents()
     {
          JCList         .addItem("Over-Due");
          JCList         .addItem("All Pendings");
          JCList         .addItem("Before Over-Due");
                          add(TopPanel);
                          add(BottomPanel);
          
          TopPanel       .add(UnitPanel);
          TopPanel       .add(DeptPanel);
          TopPanel       .add(GroupPanel);
          TopPanel       .add(BlockPanel);
          TopPanel       .add(SupplierPanel);
          BottomPanel    .add(ListPanel);
          BottomPanel    .add(OrderTypePanel);
          BottomPanel    .add(SourcePanel);
          BottomPanel    .add(MrsTypePanel);
          BottomPanel    .add(SortPanel);
          BottomPanel    .add(BasePanel);
          BottomPanel    .add(ControlPanel);
          BottomPanel    .add(ApplyPanel);
          
          UnitPanel      .add(JRAllUnit);  
          UnitPanel      .add(JRSeleUnit); 
          UnitPanel      .add(JCUnit);  
          
          DeptPanel      .add(JRAllDept);
          DeptPanel      .add(JRSeleDept);
          DeptPanel      .add(JCDept); 
          
          GroupPanel     .add(JRAllGroup);
          GroupPanel     .add(JRSeleGroup);
          GroupPanel     .add(JCGroup);
          
          BlockPanel     .add(JRAllBlock);
          BlockPanel     .add(JRSeleBlock);
          BlockPanel     .add(JCBlock);
          
          SupplierPanel  .add(JRAllSup);
          SupplierPanel  .add(JRSeleSup);
          SupplierPanel  .add(JCSup);
          
          ListPanel      .add(JRAllList);
          ListPanel      .add(JRSeleList);
          ListPanel      .add(JCList);

          OrderTypePanel  .add(JRAllType);
          OrderTypePanel  .add(JRType);
          OrderTypePanel  .add(JCOrderType);

          SourcePanel     .add(JRAllSourceType);
          SourcePanel     .add(JRSelSourceType);
          SourcePanel     .add(JCOrderSource);

          MrsTypePanel     .add(JRAllMrsType);
          MrsTypePanel     .add(JRSelMrsType);
          MrsTypePanel     .add(JCMrsType);

          
          SortPanel      .add(TSort);
          SortPanel      .add(new JLabel("eg 1,2,3"));
          
          BasePanel      .add(JRDate);
          BasePanel      .add(JROrdNo);
                         addControlPanel();
          
          ApplyPanel     .add(JCAuthUser);
          ApplyPanel     .add(new JLabel(" "));
          ApplyPanel     .add(BApply);
     }
     
     public void addControlPanel()
     {
          ControlPanel.removeAll();
          if(bsig)
          {
               ControlPanel.add(TStDate);
               ControlPanel.add(TEnDate);
          }
          else
          {
               ControlPanel.add(TStNo);
               ControlPanel.add(TEnNo);
          }
          ControlPanel.updateUI();
     }

     public void addListeners()
     {
          JRAllUnit      .addActionListener(new UnitList());
          JRSeleUnit     .addActionListener(new UnitList());
          
          JRAllDept      .addActionListener(new DeptList());
          JRSeleDept     .addActionListener(new DeptList());
          
          JRAllGroup     .addActionListener(new GroupList());
          JRSeleGroup    .addActionListener(new GroupList());
          
          JRAllBlock     .addActionListener(new BlockList());
          JRSeleBlock    .addActionListener(new BlockList());
          
          JRAllSup       .addActionListener(new SupList());
          JRSeleSup      .addActionListener(new SupList());
          
          JRAllList      .addActionListener(new SelectList());
          JRSeleList     .addActionListener(new SelectList());
          
          JRDate         .addActionListener(new BaseList());
          JROrdNo        .addActionListener(new BaseList());

          JRAllType      .addActionListener(new TypeList());
          JRType         .addActionListener(new TypeList());

          JRAllSourceType.addActionListener(new SourceTypeList());
          JRSelSourceType.addActionListener(new SourceTypeList());

          JRAllMrsType.addActionListener(new MrsTypeList());
          JRSelMrsType.addActionListener(new MrsTypeList());


     }

     
     public class UnitList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllUnit)
               {
                    JRAllUnit      .setSelected(true);
                    JRSeleUnit     .setSelected(false);
                    JCUnit         .setEnabled(false);
               }
               if(ae.getSource()==JRSeleUnit)
               {
                    JRAllUnit      .setSelected(false);
                    JRSeleUnit     .setSelected(true);
                    JCUnit         .setEnabled(true);
               }
          }
     }

     public class DeptList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllDept)
               {
                    JRAllDept      .setSelected(true);
                    JRSeleDept     .setSelected(false);
                    JCDept         .setEnabled(false);
               }
               if(ae.getSource()==JRSeleDept)
               {
                    JRAllDept      .setSelected(false);
                    JRSeleDept     .setSelected(true);
                    JCDept         .setEnabled(true);
               }
          }
     }

     public class GroupList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllGroup)
               {
                    JRAllGroup     .setSelected(true);
                    JRSeleGroup    .setSelected(false);
                    JCGroup        .setEnabled(false);
               }
               if(ae.getSource()==JRSeleGroup)
               {
                    JRAllGroup     .setSelected(false);
                    JRSeleGroup    .setSelected(true);
                    JCGroup        .setEnabled(true);
               }
          }
     }
     
     public class BlockList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllBlock)
               {
                    JRAllBlock     .setSelected(true);
                    JRSeleBlock    .setSelected(false);
                    JCBlock        .setEnabled(false);
               }                   
               if(ae.getSource()==JRSeleBlock)
               {
                    JRAllBlock     .setSelected(false);
                    JRSeleBlock    .setSelected(true);
                    JCBlock        .setEnabled(true);
               }
          }
     }

     public class SupList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllSup)
               {
                    JRAllSup       .setSelected(true);
                    JRSeleSup      .setSelected(false);
                    JCSup          .setEnabled(false);
               }
               if(ae.getSource()==JRSeleSup)
               {
                    JRAllSup       .setSelected(false);
                    JRSeleSup      .setSelected(true);
                    JCSup          .setEnabled(true);
               }
          }
     }

     public class SelectList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllList)
               {
                    JRAllList      .setSelected(true);
                    JRSeleList     .setSelected(false);
                    JCList         .setEnabled(false);
               }
               if(ae.getSource()==JRSeleList)
               {
                    JRAllList      .setSelected(false);
                    JRSeleList     .setSelected(true);
                    JCList         .setEnabled(true);
               }
          }
     }

     public class TypeList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllType)
               {
                    JRAllType      .setSelected(true);
                    JRType         .setSelected(false);
                    JCOrderType    .setEnabled(false);
               }
               if(ae.getSource()==JRType)
               {
                    JRAllType      .setSelected(false);
                    JRType         .setSelected(true);
                    JCOrderType     .setEnabled(true);
               }
          }
     }
     public class SourceTypeList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllSourceType)
               {
                    JRAllSourceType     .setSelected(true);
                    JRSelSourceType     .setSelected(false);
                    JCOrderSource       .setEnabled(false);
               }
               if(ae.getSource()==JRSelSourceType)
               {
                    JRAllSourceType      .setSelected(false);
                    JRSelSourceType      .setSelected(true);
                    JCOrderSource        .setEnabled(true);
               }
          }
     }

     public class MrsTypeList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllMrsType)
               {
                    JRAllMrsType     .setSelected(true);
                    JRSelMrsType     .setSelected(false);
                    JCMrsType       .setEnabled(false);
               }
               if(ae.getSource()==JRSelMrsType)
               {
                    JRAllMrsType      .setSelected(false);
                    JRSelMrsType      .setSelected(true);
                    JCMrsType        .setEnabled(true);
               }
          }
     }


     public class BaseList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRDate)
               {
                    bsig      = true;
                    JRDate    .setSelected(true);
                    JROrdNo   .setSelected(false);
                              addControlPanel();
               }
               if(ae.getSource()==JROrdNo)
               {
                    bsig      = false;
                    JROrdNo   .setSelected(true);
                    JRDate    .setSelected(false);
                              addControlPanel();
               }
          }
     }

     public void getUDGBS()
     {
          ResultSet result = null;
          
          VUnit      = new Vector();
          VUnitCode  = new Vector();

          VMrsType      = new Vector();
          VMrsTypeCode  = new Vector();
          
          VDept      = new Vector();
          VDeptCode  = new Vector();
          
          VGroup     = new Vector();
          VGroupCode = new Vector();
          
          VBlock     = new Vector();
          VBlockCode = new Vector();

          VOrderSourceCode   = new Vector();
          VOrderSourceName  = new Vector();
          
          VSup       = new Vector();
          VSupCode   = new Vector();

          VOrderType     = new Vector();
          VOrderTypeCode = new Vector();
          
          try
          {
               ORAConnection  oraconnection      =  ORAConnection.getORAConnection();
               Connection     theConnection      =  oraconnection.getConnection();               
               Statement      stat               =  theConnection.createStatement();
               
               String QS1 = "";
               String QS2 = "";
               String QS3 = "";
               String QS4 = "";
               
               QS1 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               QS2 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               QS3 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";
               QS4 = " Select SourceTypeCode,SourceName from OrderSource ";


               result = stat.executeQuery(QS1);
               
               while(result.next())
               {
                    VDept          .addElement(result.getString(1));
                    VDeptCode      .addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery(QS2);
               
               while(result.next())
               {
                    VGroup         .addElement(result.getString(1));
                    VGroupCode     .addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery(QS3);
               
               while(result.next())
               {
                    VUnit          .addElement(result.getString(1));
                    VUnitCode      .addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery("Select "+SSupTable+".Name,"+SSupTable+".Ac_Code From "+SSupTable+" Order By Name");
               
               while(result.next())
               {
                    VSup           .addElement(result.getString(1));
                    VSupCode       .addElement(result.getString(2));
               }
               
               result.close();

               result = stat.executeQuery("Select OrderType.TypeName,OrderType.TypeCode From OrderType Order By TypeName");
               
               while(result.next())
               {
                    VOrderType          .addElement(result.getString(1));
                    VOrderTypeCode      .addElement(result.getString(2));
               }
               
               result.close();

               result = stat.executeQuery("Select OrdBlock.BlockName,OrdBlock.Block From OrdBlock Order By BlockName");
               
               while(result.next())
               {
                    VBlock         .addElement(result.getString(1));
                    VBlockCode     .addElement(result.getString(2));
               }
               result.close();
               result = stat.executeQuery(QS4);
               
               while(result.next())
               {
                    VOrderSourceCode     .addElement(result.getString(1));
                    VOrderSourceName     .addElement(result.getString(2));

               }
               result.close();

               result = stat.executeQuery("Select MrsTYpeCOde,MrsTypeName from mrsType");
               
               while(result.next())
               {
                    VMrsTypeCode          .addElement(result.getString(1));
                    VMrsType      .addElement(result.getString(2));
               }
               result.close();


          }
          catch(Exception ex)
          {
               System.out.println("Unit,Dept,Group & Supplier :"+ex);
          }
     }

     private void setConcern()
     {
          ResultSet result = null;
          
          VCName = new Vector();
          VCCode = new Vector();
          try
          {
               ORAConnection       oraconnection         =  ORAConnection.getORAConnection();
               Connection          theConnection         =  oraconnection.getConnection();               
               Statement           theStatement          =  theConnection.createStatement();
                                   result                =  theStatement.executeQuery("Select MillCode,MillName From  Mill  Order By 1");
               while(result.next())
               {
                    VCCode.addElement(result.getString(1));
                    VCName.addElement(result.getString(2));
               }
               result.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               System.exit(0);
          }
     }


     private void setAuthUsers()
     {
          VAuthUserCode  = null;
          VAuthUserName  = null;

          VAuthUserCode  = new Vector();
          VAuthUserName  = new Vector();

          VAuthUserCode  . removeAllElements();
          VAuthUserName  . removeAllElements();

          VAuthUserCode  . addElement("All");
          VAuthUserName  . addElement("All");

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               ResultSet      result        =  stat.executeQuery(" Select UserCode, UserName from RawUser where UserCode in "+
                                                                 " (Select distinct AuthUserCode from MrsUserAuthentication) "+
                                                                 " Order by 2 ");

               while(result.next())
               {
                    VAuthUserCode  . addElement(result.getString(1));
                    VAuthUserName  . addElement(result.getString(2));
               }

               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
     
