package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class OrderDetailsFrame extends JInternalFrame
{
     Object RowData[][];
     String ColumnData[] = {"Concern","Unit","Department","Group","Block","Supplier","Order No","Date","Mrs No","Code","Name","Qty","Rate","Amount","Due Date","Status"};
     String ColumnType[] = {"S"      ,"S"   ,"S"         ,"S"    ,"S"    ,"S"       ,"N"       ,"S"   ,"N"     ,"S"   ,"S"   ,"N"  ,"N"   ,"N"     ,"S"       ,"S"     };
     
     Vector VOrdConcName,VOrdUnitName,VOrdDeptName,VOrdGroupName,VOrdBlockName,VOrdSupName,VOrdDate,VOrdNo,VMrsNo,VOrdCode,VOrdName,VOrdQty,VOrdRate,VOrdVal,VOrdDue,VStatus;
     Vector VOrdConcCode,VOrdUnitCode,VOrdDeptCode,VOrdGroupCode,VOrdBlockCode,VOrdSupCode;
     Vector VId,VItemsStatus;
     
     TabReport tabreport;
     OrderCritPanel TopPanel;
     JPanel BottomPanel;
     
     Common common = new Common();
     
     JLayeredPane DeskTop;
     Vector VCode,VName,VNameCode;
     StatusPanel SPanel;
     
     int iAmend,iUserCode,iMillCode,iAuthCode;
     String SYearCode;
     String SItemTable,SSupTable;
     int iOrderStatus  = 0;
     Vector VOrderQty,VOdInVQty;

     public OrderDetailsFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iAmend,int iUserCode,int iMillCode,int iAuthCode,String SYearCode,String SItemTable,String SSupTable)
     {
          super("Order Details with Constraints");
          this.DeskTop    = DeskTop;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.SPanel     = SPanel;
          this.iAmend     = iAmend;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.iAuthCode  = iAuthCode;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel       = new OrderCritPanel(iMillCode,SSupTable);
          BottomPanel    = new JPanel();
          TopPanel       .JCConcern.setEnabled(false);
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }

     public void addComponents()
     {
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          TopPanel.BApply.addActionListener(new ApplyList());
     }

     private void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.updateUI();
               DeskTop.repaint();
          }
          catch(Exception ex){}
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}

               try
               {
                    tabreport = new TabReport(RowData,ColumnData,ColumnType);
                    getContentPane().add(tabreport,BorderLayout.CENTER);
                    if(iAuthCode>1)
                    {
                         if (iAmend !=0)
                         {
                              tabreport.ReportTable.addKeyListener(new KeyList());
                         }
                    }
                    setSelected(true);
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if (ke.getKeyCode()==10)
               {
                    try
                    {
                         int i = tabreport.ReportTable.getSelectedRow();
                         String SOrderNo = (String)RowData[i][6];
                    
                         getOrderDetails(SOrderNo);
                         setOrderDetails();

                         DirectOrderFrame    directorderframe = new DirectOrderFrame(DeskTop,VCode,VName,VNameCode,SPanel,true,iAmend,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);
                                             directorderframe    . fillData(SOrderNo,iOrderStatus,VItemsStatus,SItemTable,SSupTable);
                         DeskTop             .add(directorderframe);
                         directorderframe    .show();
                         directorderframe    .moveToFront();
                         DeskTop             .repaint();
                         DeskTop             .updateUI();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
          } 
     }

     public void setDataIntoVector()
     {
          VOrdNo        = new Vector();
          VOrdDate      = new Vector();
          VMrsNo        = new Vector();
          VOrdCode      = new Vector();
          VOrdName      = new Vector();
          VOrdQty       = new Vector();
          VOrdRate      = new Vector();
          VOrdVal       = new Vector();
          VOrdDue       = new Vector();
          VOrdConcName  = new Vector();
          VOrdUnitName  = new Vector();
          VOrdDeptName  = new Vector();
          VOrdGroupName = new Vector();
          VOrdBlockName = new Vector();
          VOrdSupName   = new Vector();
          
          VOrdConcCode  = new Vector();
          VOrdUnitCode  = new Vector();
          VOrdDeptCode  = new Vector();
          VOrdGroupCode = new Vector();
          VOrdBlockCode = new Vector();
          VOrdSupCode   = new Vector();
          VStatus       = new Vector();
          VId           = new Vector();
          
          String SStart = "";
          String SEnd   = "";
          
          if(TopPanel.bsig==true)
          {
               SStart  = TopPanel.TStDate.toNormal();
               SEnd    = TopPanel.TEnDate.toNormal();
          }
          else
          {
               SStart  = TopPanel.TStNo.getText();
               SEnd    = TopPanel.TEnNo.getText();
          }
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
          
               String QString = getQString(SStart,SEnd);
               
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    String SConcern="";
               
                    int iMill = common.toInt(res.getString(1));
               
                    /*if(iMill==1)
                    {
                         SConcern = "Amarjothi Dyeing";
                    }
                    else
                    {
                         SConcern = "Amarjothi Spinning";
                    }*/
               
                    VOrdConcCode   .addElement(String.valueOf(iMill));
                    

                    VOrdUnitName   .addElement(res.getString(2));
                    VOrdDeptName   .addElement(res.getString(3));
                    VOrdGroupName  .addElement(res.getString(4));
                    VOrdBlockName  .addElement(res.getString(5));
                    VOrdSupName    .addElement(res.getString(6));
                    
                    VOrdNo         .addElement(res.getString(7));
                    VOrdDate       .addElement(res.getString(8));
                    VMrsNo         .addElement(res.getString(9));
                    VOrdCode       .addElement(res.getString(10));
                    VOrdName       .addElement(res.getString(11));
                    VOrdQty        .addElement(res.getString(12));
                    VOrdRate       .addElement(res.getString(13));
                    VOrdVal        .addElement(res.getString(14));
                    VOrdDue        .addElement(res.getString(15));
                    
                    VOrdUnitCode   .addElement(res.getString(16));
                    VOrdDeptCode   .addElement(res.getString(17));
                    VOrdGroupCode  .addElement(res.getString(18));
                    VOrdBlockCode  .addElement(res.getString(19));
                    VOrdSupCode    .addElement(res.getString(20));
                    VId            .addElement(res.getString(21));
                    VOrdConcName   .addElement(res.getString(22));
                    VStatus        .addElement(common.parseNull(res.getString(23)));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VOrdNo.size()][ColumnData.length];
          for(int i=0;i<VOrdNo.size();i++)
          {
               RowData[i][0]  = (String)VOrdConcName   .elementAt(i);
               RowData[i][1]  = (String)VOrdUnitName   .elementAt(i);
               RowData[i][2]  = (String)VOrdDeptName   .elementAt(i);
               RowData[i][3]  = (String)VOrdGroupName  .elementAt(i);
               RowData[i][4]  = (String)VOrdBlockName  .elementAt(i);
               RowData[i][5]  = (String)VOrdSupName    .elementAt(i);
               RowData[i][6]  = (String)VOrdNo         .elementAt(i);
               RowData[i][7]  = common.parseDate((String)VOrdDate.elementAt(i));
               RowData[i][8]  = (String)VMrsNo         .elementAt(i);
               RowData[i][9]  = (String)VOrdCode       .elementAt(i);
               RowData[i][10] = (String)VOrdName       .elementAt(i);
               RowData[i][11] = (String)VOrdQty        .elementAt(i);
               RowData[i][12] = (String)VOrdRate       .elementAt(i);
               RowData[i][13] = (String)VOrdVal        .elementAt(i);
               RowData[i][14] = common.parseDate((String)VOrdDue.elementAt(i));
               RowData[i][15] = (String)VStatus        .elementAt(i);
          }  
     }

     public String getQString(String SStart,String SEnd)
     {
          String SAuthUserCode = (String)TopPanel.VAuthUserCode.elementAt(TopPanel.VAuthUserName.indexOf((String)TopPanel.JCAuthUser.getSelectedItem()));

          String QS =    "SELECT  PurchaseOrder.MillCode,Unit.Unit_Name, Dept.Dept_Name, Cata.Group_Name, OrdBlock.BlockName, "+SSupTable+".Name, PurchaseOrder.OrderNo, PurchaseOrder.OrderDate, PurchaseOrder.MrsNo, PurchaseOrder.Item_Code, InvItems.Item_Name, PurchaseOrder.Qty, PurchaseOrder.Rate, PurchaseOrder.Net, PurchaseOrder.DueDate, PurchaseOrder.Unit_Code, PurchaseOrder.Dept_Code, PurchaseOrder.Group_Code, PurchaseOrder.OrderBlock, PurchaseOrder.Sup_Code,PurchaseOrder.Id,Mill.MillName,decode(Converted,1,'AutoConversion','Manual') as Status "+
                         " FROM ((((((PurchaseOrder "+
                         " INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code) "+
                         " INNER JOIN Unit ON PurchaseOrder.Unit_Code=Unit.Unit_Code) "+
                         " INNER JOIN Dept ON PurchaseOrder.Dept_Code=Dept.Dept_code) "+
                         " INNER JOIN Cata ON PurchaseOrder.Group_Code=Cata.Group_Code) "+
                         " INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block) "+
                         " INNER JOIN Mill ON PurchaseOrder.MillCode=Mill.MillCode) "+
                         " INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code ";

          String SHave;

          if (iAmend !=0)
          {
               SHave     =" PurchaseOrder.Qty >= 0 ";
          }
          else
          {
               SHave     =" PurchaseOrder.Qty >= 0 and amended > 0";
          }

          String str   = "";
          if(TopPanel.bsig==true)
          {
               str       = "PurchaseOrder.OrderDate >= '"+SStart+"' and PurchaseOrder.OrderDate <='"+SEnd+"' ";
               SHave     = SHave+" and "+str;
          }
          else
          {
               str       = "PurchaseOrder.OrderNo >= "+common.toInt(SStart)+" and PurchaseOrder.OrderNo <="+common.toInt(SEnd);
               SHave     = SHave+" and "+str;
          }
          
          str       = "PurchaseOrder.MillCode = "+iMillCode;
          SHave     = SHave+" and "+str;
          
          if(TopPanel.JRSeleUnit.isSelected())
          {
               str       = "PurchaseOrder.Unit_Code = "+(String)TopPanel.VUnitCode.elementAt(TopPanel.JCUnit.getSelectedIndex());
               SHave     = SHave+" and "+str;
          }

          if(TopPanel.JRSeleDept.isSelected())
          {
               str       = "PurchaseOrder.Dept_Code = "+(String)TopPanel.VDeptCode.elementAt(TopPanel.JCDept.getSelectedIndex());
               SHave     = SHave+" and "+str;
          }

          if(TopPanel.JRSeleGroup.isSelected())
          {
               str       = "PurchaseOrder.Group_Code = "+(String)TopPanel.VGroupCode.elementAt(TopPanel.JCGroup.getSelectedIndex());
               SHave     = SHave+" and "+str;
          }

          if(TopPanel.JRSeleBlock.isSelected())
          {
               str       = "PurchaseOrder.OrderBlock = "+(String)TopPanel.VBlockCode.elementAt(TopPanel.JCBlock.getSelectedIndex());
               SHave     = SHave+" and "+str;
          }

          if(TopPanel.JRSeleSup.isSelected())
          {
               str       = "PurchaseOrder.Sup_Code = '"+(String)TopPanel.VSupCode.elementAt(TopPanel.JCSup.getSelectedIndex())+"'";
               SHave     = SHave+" and "+str;
          }

          if(TopPanel.JRSelMrsType.isSelected())
          {
               str       = " getMrsTypeCode(PurchaseOrder.MrsNo) = "+(String)TopPanel.VMrsTypeCode.elementAt(TopPanel.JCMrsType.getSelectedIndex());
               SHave     = SHave+" and "+str;
          }

          if(TopPanel.JRType.isSelected())
          {
               str       = "PurchaseOrder.OrderTypeCode = "+(String)TopPanel.VOrderTypeCode.elementAt(TopPanel.JCOrderType.getSelectedIndex());
               SHave     = SHave+" and "+str;
          }

          if(TopPanel.JRSeleList.isSelected())
          {
               DateField2 df  = new DateField2();
               df             .setTodayDate();
               String SToday  = df.toNormal();
               int iSelect    = TopPanel.JCList.getSelectedIndex();

               if (iSelect==0)
               {
                    str  = "PurchaseOrder.InvQty = 0 and PurchaseOrder.DueDate <= '"+SToday+"'";
               }
               else if(iSelect==1)
               {         
                    str  = "PurchaseOrder.InvQty = 0";
               }
               else
               {
                    str  = "PurchaseOrder.InvQty > 0 and PurchaseOrder.DueDate >= '"+SToday+"'";
               }
               SHave     = SHave+" and "+str;
          }
          
          QS = QS+" Where "+SHave;

          if(!SAuthUserCode.equals("All"))
          {
               QS = QS + " and PurchaseOrder.MrsAuthUserCode = "+SAuthUserCode;
          }
          
          String SOrder=(TopPanel.TSort.getText()).trim();

          if(SOrder.length()>0)
               QS = QS+" Order By "+TopPanel.TSort.getText()+",10";
          else
               QS = QS+" Order By 7,8,5,10 ";

          return QS;
     }

     public void getOrderDetails(String SOrderNo)
     {
          VOrderQty      = new Vector();
          VOdInVQty      = new Vector();


          ResultSet result  = null;

          String QString =    " SELECT PurchaseOrder.OrderNo,PurchaseOrder.Qty,PurchaseOrder.InvQty"+
                              " FROM (((((((PurchaseOrder LEFT JOIN Port on PurchaseOrder.PortCode=Port.PortCode) INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block) INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code) INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON PurchaseOrder.Dept_Code=Dept.Dept_code) INNER JOIN Cata ON PurchaseOrder.Group_Code=Cata.Group_Code) INNER JOIN Unit ON PurchaseOrder.Unit_Code=Unit.Unit_Code) LEFT JOIN MatDesc ON (PurchaseOrder.OrderBlock=MatDesc.OrderBlock) AND (PurchaseOrder.OrderNo=MatDesc.OrderNo) AND (PurchaseOrder.Item_Code=MatDesc.Item_Code)  AND (PurchaseOrder.SlNo=MatDesc.SlNo) "+
                              " WHERE PurchaseOrder.OrderNo="+SOrderNo+" And PurchaseOrder.Qty > 0 and purchaseorder.millcode = "+iMillCode+
                              " ORDER BY PurchaseOrder.ID";
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
                              result        =  stat.executeQuery(QString);

               while(result.next())
               {
                    VOrderQty      . addElement(result.getString(2));
                    VOdInVQty      . addElement(result.getString(3));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               System.exit(0);
          }
     }

     public void setOrderDetails()
     {
          VItemsStatus   = new Vector();
          iOrderStatus   = 0;
          int iZeroCount = 0;
          int iOneCount  = 0;


          for(int i=0;i<VOrderQty.size();i++)
          {
               double iQty    = common.toDouble((String)VOrderQty.elementAt(i));
               double iInvQty = common.toDouble((String)VOdInVQty.elementAt(i));

               if(iInvQty == 0)
               {
                    VItemsStatus.addElement("0");
                    iZeroCount++;
               }

               if((iQty>iInvQty || iQty<iInvQty) && iInvQty!=0 )
               {
                    VItemsStatus.addElement("2");
                    iOrderStatus = 2;
               }

               if(iQty==iInvQty)
               {
                    VItemsStatus.addElement("1");
                    iOneCount++;
               }
          }
          if(iOrderStatus != 2)
          {
               if(iZeroCount == VOrderQty.size())
               {
                    iOrderStatus = 0;
               }
               else
               if(iOneCount  == VOrderQty.size())
               {
                    iOrderStatus = 1;
               }
               else
               {
                    iOrderStatus = 2;
               }
          }
     }
}
