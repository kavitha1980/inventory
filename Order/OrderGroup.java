package Order;

import java.io.*;
import java.util.*;

import util.*;

public class OrderGroup
{
     String    SSupCode,SMonth,SMailId,SSupName;
     ArrayList itemList,orderNoList,orderDateList,dueDateList;
     public OrderGroup(String SSupCode)
     {
          this.SSupCode  = SSupCode;
          itemList       = new ArrayList();

          orderNoList    = new ArrayList();
          orderDateList  = new ArrayList();
          dueDateList    = new ArrayList();

     }
     public OrderGroup(String SSupCode,String SMonth)
     {
          this.SSupCode = SSupCode;
          this.SMonth   = SMonth;

          itemList      = new ArrayList();
     }

     public OrderGroup(String SSupCode,String SSupName,String SMailId)
     {
          this.SSupCode = SSupCode;
          this.SSupName = SSupName;
          this.SMailId  = SMailId;

          itemList      = new ArrayList();
     }
     public void appendItem(Item item)
     {
          itemList.add(item);
     }
      public void appendItem1(ItemGst item)
     {
          itemList.add(item);
     }
     public void appendItem(Item item,String SOrderNo,String SOrderDate,String SDueDate)
     {
          itemList.add(item);

     }
     public void appendOrders(String SOrderNo,String SOrderDate,String SDueDate)
     {
          int iIndex = getIndexOf(SOrderNo);
          if(iIndex==-1)
          {
               orderNoList.add(SOrderNo);
               orderDateList.add(SOrderDate);
               dueDateList.add(SDueDate);
          }
     }

     public ArrayList getItems()
     {
          return itemList;
     }
     private int getIndexOf(String SOrderNo)
     {
          int iIndex=-1;

          for(int i=0; i<orderNoList.size(); i++)
          {
               if(SOrderNo.equals((String)orderNoList.get(i)))
               {
                    iIndex= i;
                    break;
               }
          }
          return iIndex;
     }
}
