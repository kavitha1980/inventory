package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class OrderListFrameGst extends JInternalFrame
{
     JPanel         TopPanel;
     JPanel         BottomPanel;
     JPanel         ControlPanel;
     JPanel         ListPanel;
     
     String         SStDate,SEnDate;
     JComboBox      JCOrder,JCFilter,JCConcern,JCUnit;
     JButton        BApply;

     Vector         VCCode,VCName,VUCode,VUName;
     Vector         VOrderQty,VOdInVQty;

     DateField2     TStDate;
     DateField2     TEnDate;

     Object         RowData[][];
     String         ColumnData[] = {"Order No","Date","Block","Supplier","Mrs No","Code","Name","Dept","Group","Unit","Concern","Qty","Rate","Amount","Due Date","Status"};
     String         ColumnType[] = {"N"       ,"S"   ,"S"    ,"S"       ,"N"     ,"S"   ,"S"   ,"S"   ,"S"    ,"S"   ,"S"      ,"N"  ,"N"   ,"N"     ,"S"       ,"S"};

     JLayeredPane   DeskTop;
     Vector         VCode,VName,VNameCode;
     StatusPanel    SPanel;
     int            iUserCode,iMillCode,iAuthCode;
     String         SYearCode;
     String         SItemTable,SSupTable;

     Common         common = new Common();

     Vector         VOrdDate,VBlock,VBlockCode,VOrdNo,VMrsNo,VSupName,VOrdCode,VOrdName,VOrdDeptName,VOrdCataName,VOrdUnitName,VOrdConcern,VOrdQty,VOrdRate,VOrdNet,VOrdDue,VStatus,VPStateCode,VPTypeCode,VGstStatus;
     Vector         VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup;

     TabReport      tabreport;
     int            iOrderStatus  = 0;
     Vector         VItemsStatus;

     JComboBox      JCAuthUser;
     Vector         VAuthUserCode, VAuthUserName;

     public OrderListFrameGst(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode,String SYearCode,String SItemTable,String SSupTable)
     {
          super("Orders Placed During a Period");

          this.DeskTop    = DeskTop;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.iAuthCode  = iAuthCode;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          
          setConcernUnit();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setPresets();
     }

     public void createComponents()
     {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          ControlPanel   = new JPanel();
          ListPanel      = new JPanel();

          TStDate        = new DateField2();
          TEnDate        = new DateField2();
          BApply         = new JButton("Apply");
          JCOrder        = new JComboBox();
          JCFilter       = new JComboBox();
          JCConcern      = new JComboBox(VCName);
          JCUnit         = new JComboBox(VUName);

          setAuthUsers();

          JCAuthUser     = new JComboBox(VAuthUserName);

          TStDate        . setTodayDate();
          TEnDate        . setTodayDate();
     }

     public void setLayouts()
     {
          TopPanel       . setLayout(new GridLayout(2,1));
          ControlPanel   . setLayout(new FlowLayout(FlowLayout.LEFT));
          ListPanel      . setLayout(new FlowLayout(FlowLayout.LEFT));

          ControlPanel   . setBorder(new TitledBorder("Control"));
          ListPanel      . setBorder(new TitledBorder("List"));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }

     public void addComponents()
     {
          JCOrder             . addItem("Order No");
          JCOrder             . addItem("Materialwise");
          JCOrder             . addItem("Supplierwise");
          JCOrder             . addItem("Departmentwise");
          JCOrder             . addItem("Groupwise");
          
          JCFilter            . addItem("All");
          JCFilter            . addItem("Over-Due");
          JCFilter            . addItem("All Pendings");
          JCFilter            . addItem("Before Over-Due");
          
          ListPanel           . add(new JLabel("List Only"));
          ListPanel           . add(JCFilter);
          
          ListPanel           . add(new JLabel("Concern"));
          ListPanel           . add(JCConcern);
          
          ListPanel           . add(new JLabel("Unit"));
          ListPanel           . add(JCUnit);
          
          ControlPanel        . add(new JLabel("Sorted On"));
          ControlPanel        . add(JCOrder);
          
          ControlPanel        . add(new JLabel("Period"));
          ControlPanel        . add(TStDate);
          ControlPanel        . add(TEnDate);
          ControlPanel        . add(new JLabel("User"));
          ControlPanel        . add(JCAuthUser);
          ControlPanel        . add(BApply);
          
          TopPanel            . add(ListPanel);
          TopPanel            . add(ControlPanel);
          
          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
     }
     
     public void addListeners()
     {
          BApply.addActionListener(new ApplyList());
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               
               try
               {
                    tabreport = new TabReport(RowData,ColumnData,ColumnType);
                    getContentPane().add(tabreport,BorderLayout.CENTER);
                    
                    if(iAuthCode>1)
                    {
                         tabreport.ReportTable.addKeyListener(new KeyList());
                    }
                    setSelected(true);
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if (ke.getKeyCode()==10)
               {
                    try
                    {
                         int       i         = tabreport.ReportTable.getSelectedRow();

			 String SSupType   = (String)VPTypeCode.elementAt(i);
			 String SStateCode = (String)VPStateCode.elementAt(i);
			 String SSupName   = (String)VSupName.elementAt(i);
			 String SGstStatus = (String)VGstStatus.elementAt(i);

                         String    SOrderNo  = (String)RowData[i][0];

                         getOrderDetails(SOrderNo);
                         setOrderDetails();

			 if(iOrderStatus>0 && SGstStatus.equals("1"))
			 {
                              DirectOrderFrame    directorderframe    = new DirectOrderFrame(DeskTop,VCode,VName,VNameCode,SPanel,true,1,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);
                                             directorderframe    . fillData(SOrderNo,iOrderStatus,VItemsStatus,SItemTable,SSupTable);
                                             DeskTop             . add(directorderframe);
                                             directorderframe    . show();
                                             directorderframe    . moveToFront();
                                             DeskTop             . repaint();
                                             DeskTop             . updateUI();
			 }
			 else
			 {
			      if(SSupType.equals("") || SSupType.equals("0"))
			      {
		    	           JOptionPane.showMessageDialog(null,"Gst Classification Not Updated for Supplier-"+SSupName,"Information",JOptionPane.INFORMATION_MESSAGE);
				   return;
			      }

			      if(SStateCode.equals("") || SStateCode.equals("0"))
			      {
				   JOptionPane.showMessageDialog(null,"State Data Not Updated for Supplier-"+SSupName,"Information",JOptionPane.INFORMATION_MESSAGE);
				   return;
			      }

                              DirectOrderFrameGst directorderframegst = new DirectOrderFrameGst(DeskTop,VCode,VName,VNameCode,SPanel,true,1,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable);
                                             directorderframegst . fillData(SOrderNo,iOrderStatus,VItemsStatus,SItemTable,SSupTable);
                                             DeskTop             . add(directorderframegst);
                                             directorderframegst . show();
                                             directorderframegst . moveToFront();
                                             DeskTop             . repaint();
                                             DeskTop             . updateUI();
			 }
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
               }
          } 
     }

     public void setDataIntoVector()
     {
          VOrdDate       = new Vector();
          VOrdNo         = new Vector();
          VBlock         = new Vector();
          VBlockCode     = new Vector();
          VMrsNo         = new Vector();
          VSupName       = new Vector();
          VOrdCode       = new Vector();
          VOrdName       = new Vector();
          VOrdDeptName   = new Vector();
          VOrdCataName   = new Vector();
          VOrdConcern    = new Vector();
          VOrdUnitName   = new Vector();
          VOrdQty        = new Vector();
          VOrdRate       = new Vector();
          VOrdNet        = new Vector();
          VOrdDue        = new Vector();
          VStatus        = new Vector();
	  VPStateCode	 = new Vector();
	  VPTypeCode	 = new Vector();
	  VGstStatus	 = new Vector();
          
          SStDate        = TStDate.toString();
          SEnDate        = TEnDate.toString();
          String StDate  = TStDate.toNormal();
          String EnDate  = TEnDate.toNormal();

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();

               String         QString        = getQString(StDate,EnDate);
               ResultSet      res            = stat.executeQuery(QString);

               while (res.next())
               {
                    String SConcern= "";
                    
                    String str1    = res.getString(1);
                    String str2    = res.getString(2);
                    String str3    = res.getString(3);
                    String str4    = res.getString(4);
                    String str5    = res.getString(5);
                    String str6    = res.getString(6);
                    String str7    = res.getString(7);
                    String str8    = res.getString(8);
                    String str9    = res.getString(9);
                    String str10   = res.getString(10);
                    String str11   = res.getString(11);
                    String str12   = res.getString(12);
                    String str13   = res.getString(13);
                    String str14   = res.getString(14);
                    String str15   = res.getString(15);
                    String SStatus = common.parseNull(res.getString(16));
                    str2           = common.parseDate(str2);
                    str11          = common.parseDate(str11);
                    
                    SConcern = (String)JCConcern.getSelectedItem();

                    VOrdNo         . addElement(str1);
                    VOrdDate       . addElement(str2);
                    VBlock         . addElement(str3);
                    VSupName       . addElement(str4);
                    VMrsNo         . addElement(str5);
                    VOrdCode       . addElement(str6);
                    VOrdName       . addElement(str7);
                    VOrdQty        . addElement(str8);
                    VOrdRate       . addElement(str9);
                    VOrdNet        . addElement(str10);
                    VOrdDue        . addElement(str11);
                    VOrdDeptName   . addElement(common.parseNull(str12));
                    VOrdCataName   . addElement(common.parseNull(str13));
                    VOrdUnitName   . addElement(common.parseNull(str14));
                    VBlockCode     . addElement(str15);
                    VOrdConcern    . addElement(SConcern);
                    VStatus        . addElement(SStatus);
		    VPStateCode    . addElement(res.getString(17));
		    VPTypeCode     . addElement(res.getString(18));
		    VGstStatus     . addElement(res.getString(22));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VOrdDate.size()][16];
          for(int i=0;i<VOrdDate.size();i++)
          {
               RowData[i][0]  = (String)VOrdNo         .elementAt(i);
               RowData[i][1]  = (String)VOrdDate       .elementAt(i);
               RowData[i][2]  = (String)VBlock         .elementAt(i);
               RowData[i][3]  = (String)VSupName       .elementAt(i);
               RowData[i][4]  = (String)VMrsNo         .elementAt(i);
               RowData[i][5]  = (String)VOrdCode       .elementAt(i);
               RowData[i][6]  = (String)VOrdName       .elementAt(i);
               RowData[i][7]  = (String)VOrdDeptName   .elementAt(i);
               RowData[i][8]  = (String)VOrdCataName   .elementAt(i);
               RowData[i][9]  = (String)VOrdUnitName   .elementAt(i);
               RowData[i][10] = (String)VOrdConcern    .elementAt(i);
               RowData[i][11] = (String)VOrdQty        .elementAt(i);
               RowData[i][12] = (String)VOrdRate       .elementAt(i);
               RowData[i][13] = (String)VOrdNet        .elementAt(i);
               RowData[i][14] = (String)VOrdDue        .elementAt(i);
               RowData[i][15] = (String)VStatus        .elementAt(i);
          }  
     }

     public String getQString(String StDate,String EnDate)
     {
          String SAuthUserCode = (String)VAuthUserCode.elementAt(VAuthUserName.indexOf((String)JCAuthUser.getSelectedItem()));

          DateField2 df  = new DateField2();

                    df   . setTodayDate();

          String SToday  = df.TYear.getText()+df.TMonth.getText()+df.TDay.getText();
          String QString = "";
          String SUnit   = (String)VUCode.elementAt(JCUnit.getSelectedIndex());

          QString  =     " with temp1 as (SELECT PurchaseOrder.OrderNo as orderno, PurchaseOrder.OrderDate,"+
                         " OrdBlock.BlockName, "+SSupTable+".Name, PurchaseOrder.MrsNo, PurchaseOrder.Item_Code,"+
                         " InvItems.Item_Name, PurchaseOrder.Qty, PurchaseOrder.Rate, PurchaseOrder.Net, PurchaseOrder.DueDate,"+
                         " Dept.Dept_Name, Cata.Group_Name, Unit.Unit_Name, PurchaseOrder.OrderBlock,"+
			 " decode(Converted,1,'AutoConversion','Manual') as Status,"+
		         " decode(PartyMaster.StateCode,0,'0',decode(PartyMaster.CountryCode,'61',nvl(State.GstStateCode,0),'999')) as GstStateCode,"+
			 " nvl(PartyMaster.GstPartyTypeCode,0) as GstPartyTypeCode, PurchaseOrder.InvQty as InvQty, "+
		         " PurchaseOrder.Unit_Code,PurchaseOrder.MrsSlNo as SlNo,PurchaseOrder.MillCode,PurchaseOrder.GstStatus from"+
                         " (((((((PurchaseOrder INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block)"+
                         " INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code)"+
			 " Inner Join PartyMaster on "+SSupTable+".Ac_Code=PartyMaster.PartyCode) "+
			 " Inner Join State on PartyMaster.StateCode=State.StateCode) "+
                         " INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code)"+
                         " INNER JOIN Dept ON PurchaseOrder.Dept_Code=Dept.Dept_Code)"+
                         " INNER JOIN Cata ON PurchaseOrder.Group_Code=Cata.Group_Code)"+
                         " INNER JOIN Unit ON PurchaseOrder.Unit_Code=Unit.Unit_Code"+
                         " Where PurchaseOrder.Qty > 0 ";

          if(!SAuthUserCode.equals("All"))
          {
               QString = QString + " and PurchaseOrder.MrsAuthUserCode = "+SAuthUserCode;
          }

               QString = QString + " ), "+
                         " temp2 as (SELECT InvItems.Item_Name, MRS.MrsNo,"+
                         " MRS.Item_Code,MRS.SlNo,MRS.MillCode FROM MRS"+
                         " INNER JOIN InvItems ON MRS.Item_Code = InvItems.Item_Code)"+
                         " SELECT temp1.OrderNo, temp1.OrderDate, temp1.BlockName, temp1.Name,"+
                         " temp1.MrsNo,temp1.Item_Code, temp1.Item_Name, temp1.Qty, temp1.Rate, temp1.Net,temp1.DueDate,"+
			 " temp1.Dept_Name,temp1.Group_Name,temp1.Unit_Name,temp1.OrderBlock,"+
                         " temp1.Status,temp1.GstStateCode,temp1.GstPartyTypeCode,temp1.InvQty,temp1.Unit_Code,temp1.MillCode,temp1.GstStatus FROM temp1"+
                         " LEFT JOIN temp2 ON (temp1.MrsNo = temp2.MrsNo) AND"+
                         " (temp1.Item_Code = temp2.Item_Code) AND"+
                         " (temp1.SlNo = temp2.SlNo)"+
                         " Where temp1.OrderDate >= '"+StDate+"' and temp1.OrderDate <='"+EnDate+"'";

          if(JCFilter.getSelectedIndex()==1)
               QString = QString+" and temp1.InvQty = 0 and temp1.DueDate <= '"+SToday+"'";

          if(JCFilter.getSelectedIndex()==2)
               QString = QString+" and temp1.InvQty = 0";

          if(JCFilter.getSelectedIndex()==3)
               QString = QString+" and temp1.InvQty > 0 and temp1.DueDate >= '"+SToday+"'";

          if(JCConcern.getSelectedIndex()>0)
               QString = QString+" and temp1.MillCode="+iMillCode;

          if(JCUnit.getSelectedIndex()>0)
               QString = QString+" and temp1.Unit_Code="+SUnit;

          if(JCOrder.getSelectedIndex() == 0)
               QString = QString+" Order By temp1.OrderNo,temp1.BlockName,temp1.OrderDate";
          if(JCOrder.getSelectedIndex() == 1)      
               QString = QString+" Order By temp1.Item_Name,temp1.OrderDate";
          if(JCOrder.getSelectedIndex() == 2)      
               QString = QString+" Order By temp1.Name,temp1.OrderDate";
          if(JCOrder.getSelectedIndex() == 3)      
               QString = QString+" Order By temp1.Dept_Name,temp1.OrderDate";
          if(JCOrder.getSelectedIndex() == 4)
               QString = QString+" Order By temp1.Group_Name,temp1.OrderDate";

          return QString;
     }

     private void setConcernUnit()
     {
          VCName    = new Vector();
          VCCode    = new Vector();
          
          VUName    = new Vector();
          VUCode    = new Vector();
          
          VCCode    . addElement("2");
          VCName    . addElement("All");
          
          VUCode    . addElement("99");
          VUName    . addElement("All");
          
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               ResultSet      result        =  stat.executeQuery("Select MillCode,MillName From  Mill  Order By 1");

               while(result.next())
               {
                    VCCode    . addElement(result.getString(1));
                    VCName    . addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               System.exit(0);
          }
          
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               String QS1 = "";
               QS1 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";

               ResultSet result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VUName    . addElement(result.getString(1));
                    VUCode    . addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("7"+ex);
          }
     }

     public void getOrderDetails(String SOrderNo)
     {
          VOrderQty      = new Vector();
          VOdInVQty      = new Vector();

          ResultSet result  = null;

          String QString =    " SELECT PurchaseOrder.OrderNo,PurchaseOrder.Qty,PurchaseOrder.InvQty"+
                              " FROM (((((((PurchaseOrder LEFT JOIN Port on PurchaseOrder.PortCode=Port.PortCode) INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block) INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code) INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON PurchaseOrder.Dept_Code=Dept.Dept_code) INNER JOIN Cata ON PurchaseOrder.Group_Code=Cata.Group_Code) INNER JOIN Unit ON PurchaseOrder.Unit_Code=Unit.Unit_Code) LEFT JOIN MatDesc ON (PurchaseOrder.OrderBlock=MatDesc.OrderBlock) AND (PurchaseOrder.OrderNo=MatDesc.OrderNo) AND (PurchaseOrder.Item_Code=MatDesc.Item_Code)  AND (PurchaseOrder.SlNo=MatDesc.SlNo) "+
                              " WHERE PurchaseOrder.OrderNo="+SOrderNo+" And PurchaseOrder.Qty > 0  and purchaseorder.millcode = "+iMillCode+
                              " ORDER BY PurchaseOrder.ID";
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
                              result        =  stat.executeQuery(QString);

               while(result.next())
               {
                    VOrderQty      . addElement(result.getString(2));
                    VOdInVQty      . addElement(result.getString(3));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               System.exit(0);
          }
     }

     public void setOrderDetails()
     {
               VItemsStatus   = new Vector();

               iOrderStatus   = 0;
          int  iZeroCount     = 0;
          int  iOneCount      = 0;

          for(int i=0;i<VOrderQty.size();i++)
          {
               double iQty    = common.toDouble((String)VOrderQty.elementAt(i));
               double iInvQty = common.toDouble((String)VOdInVQty.elementAt(i));
               if(iInvQty == 0)
               {
                    VItemsStatus.addElement("0");
                    iZeroCount++;
               }

               if((iQty>iInvQty || iQty<iInvQty) && iInvQty!=0 )
               {
                    VItemsStatus.addElement("2");
                    iOrderStatus = 2;
               }

               if(iQty==iInvQty)
               {
                    VItemsStatus.addElement("1");
                    iOneCount++;
               }
          }
          if(iOrderStatus != 2)
          {
               if(iZeroCount == VOrderQty.size())
               {
                    iOrderStatus = 0;
               }
               else
               if(iOneCount  == VOrderQty.size())
               {
                    iOrderStatus = 1;
               }
               else
               {
                    iOrderStatus = 2;
               }
          }
     }

     private void setPresets()
     {
          if(iMillCode<2)
          {
               JCConcern . setSelectedIndex(iMillCode+1);
               JCConcern . setEnabled(false);
          }
          else
          {
               JCConcern . setSelectedIndex(iMillCode);
               JCConcern . setEnabled(false);
          }
     }

     private void setAuthUsers()
     {
          VAuthUserCode  = null;
          VAuthUserName  = null;

          VAuthUserCode  = new Vector();
          VAuthUserName  = new Vector();

          VAuthUserCode  . removeAllElements();
          VAuthUserName  . removeAllElements();

          VAuthUserCode  . addElement("All");
          VAuthUserName  . addElement("All");

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               ResultSet      result        =  stat.executeQuery(" Select UserCode, UserName from RawUser where UserCode in "+
                                                                 " (Select distinct AuthUserCode from MrsUserAuthentication) "+
                                                                 " Order by 2 ");

               while(result.next())
               {
                    VAuthUserCode  . addElement(result.getString(1));
                    VAuthUserName  . addElement(result.getString(2));
               }

               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
