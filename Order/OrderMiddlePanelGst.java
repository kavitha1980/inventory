package Order;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class OrderMiddlePanelGst extends JPanel 
{
     DOInvMiddlePanelGst MiddlePanel;
     Object              RowData[][];
     Object              IdData[];
     Object              DescId[];

   String           ColumnData[] = {"Code","Name","HsnCode","Order Block","MRS No","Qty","Rate","Discount(%)","CGST(%)","SGST(%)","IGST(%)","Cess(%)","Basic","Discount (Rs)","CGST(Rs)","SGST(Rs)","IGST(Rs)","Cess(Rs)","Net (Rs)","Department","Group","Due Date","Unit","User","DocId"};
   //String           ColumnType[] = {"S","S","S","B","N","N","B","B","N","N","N","N","N","N","N","N","N","N","N","B","B","B","B","S","E"};

     String[]            ColumnType     = new String[25];

     Common              common;
     String              SMOrderNo;
     String              SItemTable;
     JTextField	         TSupData;
     JLayeredPane        DeskTop;
     Vector              VCode,VName,VItemsStatus,VOldCode,VOldMrsNo,VMrsSlNo,VMrsUserCode,VMrsType,VNameCode;//,VTaxClaim;
     int                 iMillCode;
     int                 iOrderStatus = 0;
     boolean             bflag;
     public OrderMiddlePanelGst(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,int iMillCode,boolean bflag,String SMOrderNo,String SItemTable,JTextField TSupData)
     {
          this.DeskTop        = DeskTop;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.iMillCode      = iMillCode;
          this.bflag          = bflag;
          this.SMOrderNo      = SMOrderNo;
          this.SItemTable     = SItemTable;
          this.TSupData       = TSupData;
          setLayout(new BorderLayout());
     }

     public void createComponents()
     {
          try
          {
               setColType();
               MiddlePanel           = new DOInvMiddlePanelGst(DeskTop,VCode,VName,VNameCode,RowData,ColumnData,ColumnType,iMillCode,SItemTable,TSupData);
               add(MiddlePanel,BorderLayout.CENTER);
               MiddlePanel.ReportTable.requestFocus();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void createComponents(Vector VCode,Vector VName,int iOrderStatus,Vector VItemsStatus,String SItemTable)
     {
          try
          {
               setColType();

               MiddlePanel         = new DOInvMiddlePanelGst(DeskTop,VCode,VName,VNameCode,RowData,ColumnData,ColumnType,bflag,iOrderStatus,VItemsStatus,iMillCode,SMOrderNo,SItemTable,TSupData);
               add(MiddlePanel,BorderLayout.CENTER);
               MiddlePanel         . ReportTable.requestFocus();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     /*public void setRowData(Vector VSelectedCode,Vector VSelectedName,Vector VSelectedMRS,Vector VSelectedQty,Vector VSelectedUnit,Vector VSelectedDept,Vector VSelectedGroup,Vector VSelectedDue,Vector VSelectedUser)
     {
          RowData     = new Object[VSelectedCode.size()][ColumnData,length];
          for(int i=0;i<VSelectedCode.size();i++)
          {
               RowData[i][0]  = (String)VSelectedCode  .elementAt(i);
               RowData[i][1]  = (String)VSelectedName  .elementAt(i);
               RowData[i][2]  = " ";
               RowData[i][3]  = " ";
               RowData[i][4]  = (String)VSelectedMRS   .elementAt(i);                 
               RowData[i][5]  = (String)VSelectedQty   .elementAt(i);
               RowData[i][6]  = " ";
               RowData[i][7]  = " ";
               RowData[i][8]  = " ";
               RowData[i][9]  = " ";
               RowData[i][10] = " ";
               RowData[i][11] = " ";
               RowData[i][12] = " ";
               RowData[i][13] = " ";
               RowData[i][14] = " ";
               RowData[i][15] = " ";
               RowData[i][16] = " ";
               RowData[i][17] = (String)VSelectedDept  . elementAt(i);
               RowData[i][18] = (String)VSelectedGroup . elementAt(i);
               RowData[i][19] = (String)VSelectedDue   . elementAt(i);
               RowData[i][20] = (String)VSelectedUnit  . elementAt(i);
               RowData[i][21] = (String)VSelectedUser  . elementAt(i);
               RowData[i][22] = "0";
          }
     }

     public void setRowData(Vector VSelectedCode,Vector VSelectedName)
     {
          RowData     = new Object[VSelectedCode.size()][23];
          for(int i=0;i<VSelectedCode.size();i++)
          {
               RowData[i][0]  = (String)VSelectedCode.elementAt(i);
               RowData[i][1]  = (String)VSelectedName.elementAt(i);
               RowData[i][2]  = " ";
               RowData[i][3]  = " ";
               RowData[i][4]  = " ";
               RowData[i][5]  = " ";
               RowData[i][6]  = " ";
               RowData[i][7]  = " ";
               RowData[i][8]  = " ";
               RowData[i][9]  = " ";
               RowData[i][10] = " ";
               RowData[i][11] = " ";
               RowData[i][12] = " ";
               RowData[i][13] = " ";
               RowData[i][14] = " ";
               RowData[i][15] = " ";
               RowData[i][16] = " ";
               RowData[i][17] = " ";
               RowData[i][18] = " ";
               RowData[i][19] = " ";
               RowData[i][20] = " ";
               RowData[i][21] = " ";
               RowData[i][22] = "0";
          }
     }*/

     public void setColType()
     {

          ColumnType[0]  = "S";
          ColumnType[1]  = "S";
          ColumnType[2]  = "S";

          if(bflag)
               ColumnType[3]  = "S";
          else
               ColumnType[3]  = "B";
          if(bflag)
               ColumnType[4]  = "S";
          else
               ColumnType[4]  = "B";
          if(bflag)
               ColumnType[5]  = "N";
          else
               ColumnType[5]  = "B";
          if(bflag)
               ColumnType[6]  = "N";
          else
               ColumnType[6]  = "B";
          if(bflag)
               ColumnType[7]  = "N";
          else
               ColumnType[7]  = "B";
          if(bflag)
               ColumnType[8]  = "N";
          else
               ColumnType[8]  = "B";
          if(bflag)
               ColumnType[9]  = "N";
          else
               ColumnType[9]  = "B";
          if(bflag)
               ColumnType[10]  = "N";
          else
               ColumnType[10]  = "B";
          if(bflag)
               ColumnType[11]  = "N";
          else
               ColumnType[11]  = "B";

          ColumnType[12] = "N";
          ColumnType[13] = "N";
          ColumnType[14] = "N";
          ColumnType[15] = "N";
          ColumnType[16] = "N";
          ColumnType[17] = "N";
          ColumnType[18] = "N";

          if(bflag)
               ColumnType[19] = "S";
          else
               ColumnType[19] = "B";
          if(bflag)
               ColumnType[20] = "S";
          else
               ColumnType[20] = "B";
          if(bflag)
               ColumnType[21] = "S";
          else
               ColumnType[21] = "B";
          if(bflag)
               ColumnType[22] = "S";
          else
               ColumnType[22] = "B";
          if(bflag)
               ColumnType[23] = "S";
          else
               ColumnType[23] = "B";
          if(bflag)
               ColumnType[24] = "E";
          else
               ColumnType[24] = "E";

     }

     public void getUpdateOrderNo(String SMTOrderNo)
     {
          SMOrderNo = SMTOrderNo;
     }
}
