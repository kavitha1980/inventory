package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class OrderPendingList extends JInternalFrame
{
     JPanel    TopPanel;
     JPanel    Top1Panel,Top2Panel;
     JPanel    BottomPanel;
     JPanel    ListPanel,DateTypePanel,DatePanel;
     JPanel    ControlPanel;
     
     JComboBox JCFilter;
     String    SDate;
     JButton   BApply,BPrint,BStock,BSave;
     JRadioButton   JRPeriod,JRAsOn;
     JTextField TFile;
     
     DateField TDate,TStDate,TEnDate;
     
     String SInt    = "  ";
     String Strline = "";
     int iLen       = 0;
     String SName,SOName;
     int    iDateStatus=-1;
     Object RowData[][];
     String ColumnData[] = {"Order Date","Order No","Block","Supplier","Mrs No","Code","Name","Ordered Qty","Recd Qty","Pending Qty","Rate","Basic","Advance","Due Date","Delay Days","User Name","Concern","JMD App Date","App. Delay Days","PR No","PR Date","Urgent","Next FollowUp Date","Current Status","Select"};
     String ColumnType[] = {"S"         ,"N"       ,"S"    ,"S"       ,"N"     ,"S"   ,"S"   ,"N"          ,"N"       ,"N"          ,"N"   ,"N"    ,"N"      ,"S"       ,"N"         ,"S"        ,"S"      ,"S"           ,"N"              ,"S"    ,"S"      ,"B"     ,"E"                 , "E"            , "B"};
     
     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;
     int iUserCode,iMillCode;
     String SSupTable,SItemTable;
     
     Common common = new Common();
     
     Vector VOrdDate,VOrdNo,VBlock,VOrdCode,VOrdName,VDescr,VMake,VMrsNo,VMrsDate,VOrdQty,VRecdQty,VPenQty,VRate,VBasic,VAdvance,VDueDate,VPId,VSupName,VSupCode,VUnitName,VConcName,VNextFollowUpDate,VCurrentStatus,VOrderID;
     Vector VDueDays,VFreq,VProjectName,VProjectCode;
	Vector VJMDAppDate,VAppDueDays;
	 Vector VJMDAppStatus,VSOAppStatus,VSOAppDate,VIAAppStatus,VIAAppDate,VUserName;
     Vector VCatl,VDraw,VUoM;
     
     TabReport tabreport;
     
     String Head1[] = {"Order","Order", "Material","Material Name ","Special Information ","Item Make","Catl","Draw ","UoM ","MRS No","Mrs Date","Ordered","Received","Pending","Rate ","Basic","Due Date","Delay","User","JMD App.","App.    ","P.R.No","P.R.Date","Over  "};
     String Head2[] = {"Date", "Number","Code    ","              ","                    ","         ","    ","     ","    ","      ","        ","Qty    ","Qty     ","Qty    ","     ","Price","        ","Days ","Name","Date    ","Delay   ","      ","        ","All   "};
     String Head3[] = {"    ", "      ","        ","              ","                    ","         ","    ","     ","    ","      ","        ","       ","        ","       ","Rs   ","Rs   ","        ","     ","    ","        ","Days    ","      ","        ","Delay "};
     String Width[] = {"10  ", "10    ","9      ","33            ","24                  ","8         ","14  ","14   ","5   ","6     ","10      ","7      ","7       ","7      ","9    ","10   ","10      ","4    ","10   ","25      ","4      ","10    ","10      ","4     "};
     String Align[] = {"Left", "Left  ","Left    ","Left          ","Left                ","Left     ","Left","Left ","Left","Right ","Left    ","Right  ","Right   ","Right  ","Right","Right","Left    ","Right","Left","Left    ","Right   ","Left  ","Left    ","Left  "};
     
     
     JComboBox JCConcern,JCUnit,JCDept,JCProject,JCStock,JCType,JCApprove;
     Vector VCCode,VCName,VUnitCode,VUnit,VDept,VDeptCode,VOrderType,VOrderTypeCode;
     Vector VPRNo,VPRDate;
     String SPRNo="",SPRDate="";

     Vector         VSeleStkGroupCode;
     Vector         VStkGroupCode,VStkGroupName;

     JTextArea      AStock;
     JScrollPane    StockScroll;

     JComboBox      JCAuthUser;
     Vector         VAuthUserCode, VAuthUserName;

     public OrderPendingList(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable,String SItemTable)
     {
          super("Orders Pending As On");
          this.DeskTop    = DeskTop;
          this.VCode      = VCode;
          this.VName      = VName;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.SSupTable  = SSupTable;
          this.SItemTable = SItemTable;
        //System.out.println("SItemTable:"+SItemTable);  
          setConcernCombo();
          setUnitCombo();
          setDeptCombo();
		  setTypeCombo();
          setProjectCombo();
          setStockGroup();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setPresets();
          setStockGroupMenu(0);
		 
     }

     public void createComponents()
     {
          TopPanel       = new JPanel();
          Top1Panel      = new JPanel();
          Top2Panel      = new JPanel();
          BottomPanel    = new JPanel();
          ListPanel      = new JPanel();
          ControlPanel   = new JPanel();
          DatePanel      = new JPanel();
          DateTypePanel  = new JPanel();

          JCFilter       = new JComboBox();
          TDate          = new DateField();
          TStDate        = new DateField();
          TEnDate        = new DateField();

          JCConcern      = new JComboBox(VCName);
          JCUnit         = new JComboBox(VUnit);
          JCDept         = new JComboBox(VDept);
	  JCType         = new JComboBox(VOrderType);
          JCProject      = new JComboBox(VProjectName);
          JCStock        = new JComboBox(); 
	  JCApprove       = new JComboBox();
          BApply         = new JButton("Apply");
          BPrint         = new JButton("Print");
          BStock         = new JButton("StockGroup");
	  BSave          = new JButton("Save");
          AStock         = new JTextArea(2,40);
          StockScroll    = new JScrollPane(AStock);
          TFile          = new JTextField(15);

          JRPeriod    = new JRadioButton("Periodical",false);
          JRAsOn      = new JRadioButton("As On",true);

          setAuthUsers();

          JCAuthUser     = new JComboBox(VAuthUserName);

          TDate          .setTodayDate();
          TStDate        .setTodayDate();
          TEnDate        .setTodayDate();

          JCStock        . addItem("All StockGroup");
          JCStock        . addItem("Selected StockGroup");

	  JCApprove      . addItem("All Orders");
	  JCApprove      . addItem("JMD Approved Orders");
	  JCApprove      . addItem("JMD UnApproved Orders");

          VSeleStkGroupCode = new Vector();
     }
     
     public void setLayouts()
     {
          TopPanel       .setLayout(new BorderLayout());
          Top1Panel      .setLayout(new GridLayout(2,1));
          ListPanel      .setLayout(new FlowLayout(FlowLayout.LEFT));
          ControlPanel   .setLayout(new FlowLayout(FlowLayout.LEFT));
          ListPanel      .setBorder(new TitledBorder(""));
          ControlPanel   .setBorder(new TitledBorder(""));
          Top2Panel      .setBorder(new TitledBorder(""));
          //DatePanel      .setLayout(new FlowLayout(FlowLayout.LEFT));          
          DatePanel      .setLayout(new GridLayout(2,5,3,3));          
          //DatePanel      .setBorder(new TitledBorder("Date"));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,800,500);
     }
     
     public void addComponents()
     {
          JCFilter       .addItem("All");
          JCFilter       .addItem("After DueDate");
          JCFilter       .addItem("After DueDate(>20 Days)");
          JCFilter       .addItem("Before DueDate");
          JCFilter       .addItem("Advance Paid-Goods Not Recd");
          
          ControlPanel   .add(new JLabel(" Concern "));
          ControlPanel   .add(JCConcern);
          
          ControlPanel   .add(new JLabel(" Unit "));
          ControlPanel   .add(JCUnit);
          
          ControlPanel   .add(new JLabel(" Dept "));
          ControlPanel   .add(JCDept);
          
          ControlPanel   .add(new JLabel(" Type "));
          ControlPanel   .add(JCType);
          
          ListPanel      .add(new JLabel(" Project "));
          ListPanel      .add(JCProject);
          
          ListPanel      .add(new JLabel("List"));
          ListPanel      .add(JCApprove);

          ListPanel      .add(new JLabel("List Only"));
          ListPanel      .add(JCFilter);
          
          DatePanel      .add(JRAsOn);
          DatePanel      .add(JRPeriod);

          DatePanel      .add(TStDate);
          DatePanel      .add(TEnDate);

          //ListPanel      .add(DateTypePanel);

         // ListPanel      .add(DatePanel);

          ListPanel      .add(new JLabel("User"));
          ListPanel      .add(JCAuthUser);

          Top1Panel      .add(ControlPanel);
          Top1Panel      .add(ListPanel);
          Top2Panel      .add(DatePanel);

          Top2Panel      .add(JCStock);
          Top2Panel      .add(BStock);
          Top2Panel      .add(StockScroll);
          Top2Panel      .add(BApply);
          
          TopPanel       .add("Center",Top2Panel);
          TopPanel       .add("North",Top1Panel);
          
          BottomPanel    .add(BPrint);
          BottomPanel    .add(TFile);
		  BottomPanel    .add(BSave);
          
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply.addActionListener(new ApplyList());
          BPrint.addActionListener(new PrintList());
          JCStock.addItemListener(new ItemList());
	  BSave.addActionListener(new SaveList());
          JCConcern.addItemListener(new ItemList());
          BStock.addActionListener(new StockGroupSearch(DeskTop,VSeleStkGroupCode,AStock,VStkGroupCode,VStkGroupName));
          JRPeriod.addActionListener(new JRList());
          JRAsOn.addActionListener(new JRList());
     }

     private class ItemList implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
               if(ie.getSource()==JCStock)
               {
                    int iIndex = JCStock.getSelectedIndex();
                    setStockGroupMenu(iIndex);
               }
               if(ie.getSource()==JCConcern)
               {
                    int iIndex = JCConcern.getSelectedIndex();
                    if(iIndex==0)
                    {
                         JCStock.setSelectedIndex(0);
                         JCStock.setEnabled(false);
                    }
                    else
                    {
                         JCStock.setSelectedIndex(0);
                         JCStock.setEnabled(true);
                    }
               }
          }
     }

     private void setStockGroupMenu(int iIndex)
     {
          if(iIndex==0)
          {
               AStock.setText("");
               BStock.setEnabled(false);
               VSeleStkGroupCode.removeAllElements();
          }
          else
          {
               AStock.setText("");
               BStock.setEnabled(true);
               VSeleStkGroupCode.removeAllElements();
          }
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
              String STitle="";
	      if(iDateStatus==1)	  
     	         STitle = "Report On "+JCFilter.getSelectedItem()+" Pending Orders List ("+JCApprove.getSelectedItem()+") From "+TStDate.toString()+" To "+TEnDate.toString();
              else
     	         STitle = "Report On "+JCFilter.getSelectedItem()+" Pending Orders List ("+JCApprove.getSelectedItem()+") As On "+TStDate.toString();

               String SFile  = TFile.getText();
               String SConcern = (String)JCConcern.getSelectedItem();
               new DocPrint(getPendingBody(),getPendingHead(),STitle,SFile,iMillCode,SConcern);
               removeHelpFrame();
          }
     }
	 public class SaveList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
			for(int i=0;i<RowData.length;i++)
	        {
				Boolean Bselected = (Boolean)RowData[i][24];

                if(Bselected.booleanValue())
	            {
					boolean  bUrgent  = (Boolean)RowData[i][21];
					if(bUrgent==true)
					{
						if(JOptionPane.showConfirmDialog(null, "Confirm Save the Data?", "Information", JOptionPane.YES_NO_OPTION) == 0)
							{
								SaveDetails();
								
							}
					}
					else
					{
						String SDate = (String)RowData[i][22];
						if(setValidation(SDate))
						{
							if(JOptionPane.showConfirmDialog(null, "Confirm Save the Data?", "Information", JOptionPane.YES_NO_OPTION) == 0)
							{
								SaveDetails();
								
							}
						}
					}
				}
			}	
							
          }
     }

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    JRAsOn.setSelected(false);
                    iDateStatus=1;
               }
               else
               {
   
                    JRPeriod.setSelected(false);
                    iDateStatus=0;
               }
          }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public void setStockGroup()
     {
          VStkGroupCode= new Vector();
          VStkGroupName= new Vector();

          try
          {
               ORAConnection oraconnection = ORAConnection.getORAConnection();
               Connection    theConnection = oraconnection.getConnection();               
               Statement     stat          = theConnection.createStatement();
               
               ResultSet result = stat.executeQuery("Select GroupCode,GroupName From StockGroup Order By GroupName");
               
               while(result.next())
               {
                    VStkGroupCode.addElement(result.getString(1));
                    VStkGroupName.addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public Vector getPendingHead()
     {
          Vector vect = new Vector();
          String Strh1="",Strh2="",Strh3="";
          for(int i=0;i<Head1.length;i++)
          {
               int iWidth   = common.toInt(Width[i].trim());
               String SType = Align[i].trim();
               if(SType.startsWith("Left"))
               {
                    Strh1 = Strh1+common.Pad(Head1[i].trim(),iWidth)+common.Space(2);
                    Strh2 = Strh2+common.Pad(Head2[i].trim(),iWidth)+common.Space(2);
                    Strh3 = Strh3+common.Pad(Head3[i].trim(),iWidth)+common.Space(2);
               }
               else
               {
                    Strh1 = Strh1+common.Rad(Head1[i].trim(),iWidth)+common.Space(2);
                    Strh2 = Strh2+common.Rad(Head2[i].trim(),iWidth)+common.Space(2);
                    Strh3 = Strh3+common.Rad(Head3[i].trim(),iWidth)+common.Space(2);
               }
          }
          vect.addElement(Strh1+"\n");
          vect.addElement(Strh2+"\n");
          vect.addElement(Strh3+"\n");
          return vect;
     }

     public Vector getPendingBody()
     {
          Vector vect = new Vector();
          
          for(int i=0;i<VOrdDate.size();i++)
          {
               if(i>0)
               {
                    SName  = ((String)VSupName.elementAt(i)).trim();
                    SOName = ((String)VSupName.elementAt(i-1)).trim();
               }
              
			   String SOverAllDelay = common.getDateDiff(TDate.toString(),common.parseDate((String)VMrsDate.elementAt(i)));
			   
               String Sda1    =    common.parseDate((String)VOrdDate.elementAt(i));
               String Sda2    =    (String)VBlock.elementAt(i)+"-"+(String)VOrdNo.elementAt(i);
               String Sda3    =    (String)VOrdCode.elementAt(i);
               String Sda4    =    (String)VOrdName.elementAt(i);
               String Sda5    =    (String)VDescr.elementAt(i);
               String Sda6    =    (String)VMake.elementAt(i);
               String Sda7    =    (String)VCatl.elementAt(i);
               String Sda8    =    (String)VDraw.elementAt(i);
               String Sda9    =    (String)VUoM.elementAt(i);
               String Sda10   =    (String)VMrsNo.elementAt(i);
               String Sda11   =    common.parseDate((String)VMrsDate.elementAt(i));
               String Sda12   =    common.getRound((String)VOrdQty.elementAt(i),3);
               String Sda13   =    common.getRound((String)VRecdQty.elementAt(i),3);
               String Sda14   =    common.getRound((String)VPenQty.elementAt(i),3);
               String Sda15   =    common.getRound((String)VRate.elementAt(i),2);
               String Sda16   =    common.getRound((String)VBasic.elementAt(i),2);
               String Sda17   =    common.parseDate((String)VDueDate.elementAt(i));
               String Sda18   =    (String)VDueDays.elementAt(i);
               String Sda19   =    (String)VUserName.elementAt(i);
			   
			   String SJMDAppStatus = (String)VJMDAppStatus.elementAt(i);
			   String SIAAppStatus = (String)VIAAppStatus.elementAt(i);
			   String SSOAppStatus = (String)VSOAppStatus.elementAt(i);
			   String Sda20 = "";
			   
			   if(SJMDAppStatus.equals("1"))
				{
					Sda20   =    (String)VJMDAppDate.elementAt(i)+"(JMD)";
				}
				else if(SIAAppStatus.equals("1"))
				{
					Sda20 = "JMD Pend..("+common.parseDate((String)VIAAppDate.elementAt(i))+"-IA)";
				}
				else if(SSOAppStatus.equals("1"))
				{
					Sda20 = "IA Pending("+common.parseDate((String)VSOAppDate.elementAt(i))+"-SO)";
				}
				else
				{
					 Sda20 = "SO Pending";
				}
               //String Sda20   =    (String)VJMDAppDate.elementAt(i);
               String Sda21   =    (String)VAppDueDays.elementAt(i);
               String Sda22   =    (String)VPRNo.elementAt(i);
               String Sda23   =    (String)VPRDate.elementAt(i);
			   String Sda24   =    SOverAllDelay;
			   
                      Sda1    = common.Pad(Sda1,common.toInt(Width[0].trim()))+common.Space(2);
                      Sda2    = common.Pad(Sda2,common.toInt(Width[1].trim()))+common.Space(2);
                      Sda3    = common.Pad(Sda3,common.toInt(Width[2].trim()))+common.Space(2);
                      Sda4    = common.Pad(Sda4,common.toInt(Width[3].trim()))+common.Space(2);
                      Sda5    = common.Pad(Sda5,common.toInt(Width[4].trim()))+common.Space(2);
                      Sda6    = common.Pad(Sda6,common.toInt(Width[5].trim()))+common.Space(2);
                      Sda7    = common.Pad(Sda7,common.toInt(Width[6].trim()))+common.Space(2);
                      Sda8    = common.Pad(Sda8,common.toInt(Width[7].trim()))+common.Space(2);
                      Sda9    = common.Pad(Sda9,common.toInt(Width[8].trim()))+common.Space(2);
                      Sda10   = common.Rad(Sda10,common.toInt(Width[9].trim()))+common.Space(2);
                      Sda11   = common.Rad(Sda11,common.toInt(Width[10].trim()))+common.Space(2);
                      Sda12   = common.Rad(Sda12,common.toInt(Width[11].trim()))+common.Space(2);
                      Sda13   = common.Rad(Sda13,common.toInt(Width[12].trim()))+common.Space(2);
                      Sda14   = common.Rad(Sda14,common.toInt(Width[13].trim()))+common.Space(2);
                      Sda15   = common.Rad(Sda15,common.toInt(Width[14].trim()))+common.Space(2);
                      Sda16   = common.Rad(Sda16,common.toInt(Width[15].trim()))+common.Space(2);
                      Sda17   = common.Rad(Sda17,common.toInt(Width[16].trim()))+common.Space(2);
                      Sda18   = common.Rad(Sda18,common.toInt(Width[17].trim()))+common.Space(2);
                      Sda19   = common.Rad(Sda19,common.toInt(Width[18].trim()))+common.Space(2);
                      Sda20   = common.Rad(Sda20,common.toInt(Width[19].trim()))+common.Space(2);
                      Sda21   = common.Pad(Sda21,common.toInt(Width[20].trim()))+common.Space(2);
                      Sda22   = common.Pad(Sda22,common.toInt(Width[21].trim()))+common.Space(2);
                      Sda23   = common.Pad(Sda23,common.toInt(Width[22].trim()))+common.Space(2);
					  Sda24   = common.Pad(Sda24,common.toInt(Width[23].trim()))+common.Space(2);
					  
					  
                 String Sdb1  = (String)VSupName.elementAt(i);
                 String Strd1 = "E"+common.Rad("Name : ",20)+" "+Sdb1+"F\n";
                 String Strd  = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+Sda16+Sda17+Sda18+Sda19+Sda20+Sda21+Sda22+Sda23+Sda24+"\n";

               if(i==0)
               {
                    vect.addElement("\n");
                    vect.addElement(Strd1);
                    vect.addElement("\n");
                    vect.addElement(Strd);
               }
               else
               {
                    if(SName.equals(SOName))
                         vect.addElement(Strd);
                    else
                    {
                         vect.addElement(common.Replicate("-",Strd.length())+"\n");
                         vect.addElement(Strd1);
                         vect.addElement("\n");
                         vect.addElement(Strd);
                    }
               }
          }
          return vect;
     }
     
	public boolean setValidation(String SDate)
	{
	     boolean bVal = false;
		try
		{
						
					
			if(!SDate.trim().equals(""))  
			{
				
				if((SDate.length()==10) )
				{
					int iDay = common.toInt(SDate.substring(0,2));
					int iMon = common.toInt(SDate.substring(3,5));
					int iMonEnd =common.toInt(common.getMonthEndDate(common.pureDate(SDate)));
			
					if(iDay>0 && iDay<=iMonEnd) 
					{
						if(iMon >=1 && iMon<=12)
						{
							bVal = true ;
							return bVal;
						}
						else
						{
							JOptionPane.showMessageDialog(null,"Invalid Month ","Error",JOptionPane.ERROR_MESSAGE);
							bVal = false ;
							return bVal;
						}
					}
					else
					{
						JOptionPane.showMessageDialog(null,"Invalid Day  ","Error",JOptionPane.ERROR_MESSAGE);
						bVal = false ;
						return bVal;
					}
				}
				else
				{	
					JOptionPane.showMessageDialog(null,"Date Format like DD.MM.YYYY ","Error",JOptionPane.ERROR_MESSAGE);
					bVal =  false ;	
					return bVal;
				}
			}
			else
			{
				JOptionPane.showMessageDialog(null,"Next Follow Up Date Field is Empty ","Error",JOptionPane.ERROR_MESSAGE);
				bVal = false ;
				return bVal;
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		return bVal;
	}
	
	
	public void SaveDetails()
	{
		int  iExCount =0,iCount=0,iExecuteCount=0,iUpdateCount=0,iUpCount=0;
	    Connection theConnection           = null;
          
        try
        {
            ORAConnection oraConnection     =   ORAConnection.getORAConnection();
            theConnection                   =   oraConnection.getConnection();
               
            theConnection                 . setAutoCommit(false);
              
            for(int i=0; i<RowData.length; i++)
            {
                  
				Boolean Bselected = (Boolean)RowData[i][24];

	            if(Bselected.booleanValue())
	            {  
				//String   SOrderID = (String)RowData[i][1];	
				String   SStatus  = (String)RowData[i][23];
				String   SDate    = (String)RowData[i][22];
				boolean  bUrgent  = (Boolean)RowData[i][21];
				int iDate         = common.toInt(common.pureDate(SDate));
				String SOrderID   = (String)VOrderID.elementAt(i);
				
				int iUrgentStatus = -1;
				if(bUrgent==true)
				{
					iUrgentStatus =1;
				}
				else
				{
					iUrgentStatus =0;
				} 
								
				if(!SStatus.equals(""))  
				{
					
					PreparedStatement thePS  = theConnection.prepareStatement("Select Count(*) from PurchaseOrderStatus Where OrderID = "+SOrderID+"  ");
					ResultSet rs             = thePS.executeQuery();
					if(rs.next())
					{
						iCount              = rs.getInt(1);
						//System.out.println("Count:"+iCount);
					}
					rs                       . close();
					thePS                    . close();
                         
					if(iCount > 0)
					{
						//System.out.println("Execute Update");
						PreparedStatement ps     = null;
						ps                       = theConnection.prepareStatement(getUpdateQS());
                         
						ps    .    setInt(1,1);
						ps    .    setString(2,SOrderID);
                     
						iUpCount =  ps               . executeUpdate();
                         
						ps         . close();
						ps         = null;
						iUpdateCount     = iUpdateCount+iUpCount;
							
					}
		
					//System.out.println("Execute Insert");		
					PreparedStatement ps     = null;
					ps                       = theConnection.prepareStatement(getInsertQS());
					
					ps    .    setInt(1,common.toInt(SOrderID));
					ps    .    setString(2,SStatus);
					ps    .    setInt(3,0);
					if(iDate>0)
					ps    .    setInt(4,iDate);
					else
					ps    .    setString(4,"");
					ps    .    setInt(5,iUrgentStatus);
							
                     
					iExCount =  ps               . executeUpdate();
                            
					ps         . close();
					ps         = null;
					iExecuteCount     = iExecuteCount+iExCount;
												
				}
				}
            }
            if(iExecuteCount>0)
			{          
					JOptionPane.showMessageDialog(null, "Data Saved Succesfully");
            }
			else if(iUpdateCount>0)
			{          
				JOptionPane.showMessageDialog(null, "Data Updated Succesfully");
            }
			else
			{
				JOptionPane.showMessageDialog(null,"Next Follow Up Date Or Current Status Must be Filled");     
			}
			theConnection          . commit();
			theConnection          = null;
               
		}
		catch (Exception ex)
		{
			System.out.println(ex);
			ex.printStackTrace();
			JOptionPane.showMessageDialog(null, "Problem in  Saving Data", "Error", 
			JOptionPane.ERROR_MESSAGE,new ImageIcon("D:/HRDGUI/Warning.gif"));
			try
			{
				theConnection      . rollback();   
			} catch(Exception e){}
        }
    }

	private String getInsertQS()
     {
          StringBuffer SB = null;
          SB = new StringBuffer();
          
          SB.append(" Insert into PurchaseOrderStatus(ID,OrderID,Status,EntryDate,EntryDateTime,ActiveStatus,NextFollowUpDate,UrgentStatus) ");
          SB.append(" values(PurchaseOrderStatus_seq.nextval,?,?,to_Char(SysDate,'YYYYMMDD'),to_Char(SysDate,'DD.MM.YYYY HH24:MI:SS'),?,?,?) ");
        //  System.out.println("Insert QS:"+SB.toString());
          return SB.toString();
     }
     private String getUpdateQS()
     {
          StringBuffer SB = null;
          SB = new StringBuffer();
          
          SB.append(" Update PurchaseOrderStatus set ActiveStatus=? where ID = (Select Max(ID) from PurchaseOrderStatus where OrderID =? )");
         
          
         // System.out.println("UpdateQS:"+SB.toString());
          return SB.toString();
     }
	
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(JCStock.getSelectedIndex()==0 || VSeleStkGroupCode.size()>0)
               {
                    setDataIntoVector();
                    setRowData();
					
                    try
                    {
                         getContentPane().remove(tabreport);
                    }
                    catch(Exception ex){}
                    
                    try
                    {
                         tabreport = new TabReport(RowData,ColumnData,ColumnType);
                         getContentPane().add(tabreport,BorderLayout.CENTER);
                         setSelected(true);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               else
               {
                    JOptionPane.showMessageDialog(null,"Choose the StockGroup","Error",JOptionPane.ERROR_MESSAGE);
               }
          }
     }
     public void setDataIntoVector()
     {
          VOrdDate    = new Vector();
          VOrdNo      = new Vector();
          VBlock      = new Vector();
          VOrdCode    = new Vector();
          VOrdName    = new Vector();
          VDescr      = new Vector();
          VMake       = new Vector();
          VCatl       = new Vector();
          VDraw       = new Vector();
          VUoM        = new Vector();
          VMrsNo      = new Vector();
          VMrsDate    = new Vector();
          VOrdQty     = new Vector();
          VRecdQty    = new Vector();
          VPenQty     = new Vector();
          VRate       = new Vector();
          VBasic      = new Vector();
          VAdvance    = new Vector();
          VDueDate    = new Vector();
          VPId        = new Vector();
          VSupName    = new Vector();
          VSupCode    = new Vector();
          VDueDays    = new Vector();
          VFreq       = new Vector();
          VUnitName   = new Vector();
          VConcName   = new Vector();
          VJMDAppDate = new Vector();
          VAppDueDays = new Vector();
          VPRNo       = new Vector();
          VPRDate     = new Vector();
		  VNextFollowUpDate = new Vector();
		  VCurrentStatus = new Vector();
		  VOrderID    = new Vector();
                  VJMDAppStatus = new Vector();
		  VSOAppStatus  = new Vector();
		  VSOAppDate    = new Vector();
		  VIAAppStatus  = new Vector();
		  VIAAppDate   = new Vector();
		  VUserName    = new Vector();
		  
          

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               Statement      stat2          =  theConnection.createStatement();

               String QString="";  
               String SStDate = TStDate.toNormal();
               String SEnDate = TEnDate.toNormal();             
               if(JRPeriod.isSelected())
               {
                   QString = getQString(SStDate,SEnDate,1);
               }
               else 
               {
                  // String SDate   = TDate.toNormal();
                   QString = getQString(SStDate,SEnDate,0);
               }
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    String str1  = res.getString(1);
                    String str2  = res.getString(2);                     // OrderNo
                    String str3  = res.getString(3);
                    String str4  = res.getString(4);                     // ItemCode
                    String str5  = res.getString(5);
                    String str6  = res.getString(6);
                    String str7  = res.getString(7);
                    String str8  = res.getString(8);
                    String str9  = res.getString(9);
                    String str10 = res.getString(10);
                    String str11 = res.getString(11);
                    String str12 = res.getString(12);
                    String str13 = res.getString(13);
                    String str14 = res.getString(14);
                    String str15 = res.getString(15);
                    String str16 = res.getString(16);
                    String str17 = common.parseNull(res.getString(17));  // phone
                    String str18 = common.parseNull(res.getString(18));  // contact
                    String str19 = common.parseNull(res.getString(19));  // email
                    String str20 = common.parseNull(res.getString(20));  // Description
                    String str21 = common.parseNull(res.getString(21));  // Make
                    String str22 = common.parseNull(res.getString(22));  // Catl
                    String str23 = common.parseNull(res.getString(23));  // Draw
                    String str24 = common.parseNull(res.getString(24));  // UoM
                    String str25 = common.parseNull(res.getString(25));  // Unit
                    String str26 = common.parseNull(res.getString(26));  // MillCode
                    String str27 = common.parseNull(res.getString(27));  // JMDAppDate

                    String SBlockCode = common.parseNull(res.getString(28));  // BlockCode
                    String SOrdSlNo   = common.parseNull(res.getString(29));  // OrderSlNo
					String SNextDate  = common.parseDate(common.parseNull(res.getString(30))); //Next Follow Up Date
                    String SCurrStatus = common.parseNull(res.getString(31));  // Current Status
					String SOrderID   = common.parseNull(res.getString(32));
					String SJMDAppStatus = common.parseNull(res.getString(33));
					String SSOAppStatus = common.parseNull(res.getString(34));
					String SSOAppDate  = common.parseNull(res.getString(35));
					String SIAAppStatus = common.parseNull(res.getString(36));
					String SIAAppDate = common.parseNull(res.getString(37));
					String SUserName  = common.parseNull(res.getString(38));
					
					
                    str1        = common.parseDate(str1); //Order Date
                    str13       = common.parseDate(str13);//Due Date
                    str27       = common.parseDate(str27);//JMD AppDate
                    
                    String SDays    = common.getDateDiff(TDate.toString(),str13);

					String SAppDueDays="";
                    if(SJMDAppStatus.equals("1"))
					{
						SAppDueDays = common.getDateDiff(TDate.toString(),str27);
					}
    				else if(SIAAppStatus.equals("1"))
					{
						SAppDueDays = common.getDateDiff(TDate.toString(),common.parseDate(SIAAppDate));
					}
					else if(SSOAppStatus.equals("1")  )
					{
						SAppDueDays = common.getDateDiff(TDate.toString(),common.parseDate(SSOAppDate));
					}
					else
					{
						//SAppDueDays = common.getDateDiff(TDate.toString(),str1);
						
						SAppDueDays = SDays;
					}
					
					
                    if(JCFilter.getSelectedIndex()==2)
                    {
                         if(common.toDouble(SDays)<=20)
                              continue;
                    }

                    String SConcern="";

                    SConcern = (String)JCConcern.getSelectedItem();
				

                    getPRNo(str2,SBlockCode,str4,SOrdSlNo,str26,stat2);

					
					
					
                    VOrdDate  .addElement(str1);
                    VOrdNo    .addElement(str2);  
                    VBlock    .addElement(str3);  
                    VOrdCode  .addElement(str4);
                    VOrdName  .addElement(str5);
                    VDescr    .addElement(str20);  
                    VMake     .addElement(str21);   
                    VMrsNo    .addElement(str6);  
                    VMrsDate  .addElement("");
                    VOrdQty   .addElement(str7); 
                    VRecdQty  .addElement(str8);
                    VPenQty   .addElement(str9); 
                    VRate     .addElement(str10);   
                    VBasic    .addElement(str11);  
                    VAdvance  .addElement(str12);
                    VDueDate  .addElement(str13);
                    VPId      .addElement(str14);    
                    VSupName  .addElement(str15+"  "+str17+"  "+str18+"  "+str19);
                    VSupCode  .addElement(str16);
                    VDueDays  .addElement(SDays);
                    VFreq     .addElement("");
                    VCatl     .addElement(str22);
                    VDraw     .addElement(str23);
                    VUoM      .addElement(str24);
                    VUserName .addElement(SUserName);
                    VConcName .addElement(SConcern);
                    VJMDAppDate.addElement(str27);
                    VAppDueDays.addElement(SAppDueDays);
                    VPRNo      .addElement(SPRNo);
                    VPRDate    .addElement(SPRDate);
					VNextFollowUpDate.addElement(SNextDate);
					VCurrentStatus.addElement(SCurrStatus);
					VOrderID.addElement(SOrderID);
					VJMDAppStatus.addElement(SJMDAppStatus);
					VSOAppStatus.addElement(SSOAppStatus);
					VSOAppDate.addElement(SSOAppDate);
					VIAAppStatus.addElement(SIAAppStatus);
					VIAAppDate.addElement(SIAAppDate);
               }
               setMRSDate(stat);
               res.close();
               stat.close();
               stat2.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     private void getPRNo(String SOrderNo,String SBlockCode,String SItemCode,String SOrdSlNo,String SMillCode,Statement theStatement) throws Exception
     {
          String str1="";
          String str2="";
          String str3="";
          String str4="";

          SPRNo="";
          SPRDate="";

          ResultSet result = theStatement.executeQuery("Select AdvSlNo,AdvDate,BPANo,BPAdate From AdvRequest Where OrderNo="+SOrderNo+" and OrderBlock="+SBlockCode+" and Code='"+SItemCode+"' and OrderSlNo="+SOrdSlNo+" and MillCode="+SMillCode);

          int iCtr = 0;

          while(result.next())
          {
               iCtr++;
               if(iCtr==1)
               {
                    str1="";
                    str2="";
                    str3="";
                    str4="";
                    str1=result.getString(1);
                    str2=result.getString(2);
                    str3=result.getString(3);
                    str4=common.parseDate(result.getString(4));

                    SPRNo = str1;

                    if(common.toInt(str3)>0)
                    {
                         SPRDate = "Paid("+str4+")";
                    }
                    else
                    {
                         SPRDate = str2;
                    }
               }
               else
               {
                    str1=result.getString(1);
                    str2=result.getString(2);
                    str3=result.getString(3);
                    str4=common.parseDate(result.getString(4));

                    SPRNo = SPRNo+","+str1;

                    if(common.toInt(str3)>0)
                    {
                         SPRDate = str4+","+"Paid";  // SPRDate
                    }
                    else
                    {
                         SPRDate = SPRDate+","+str2;
                    }
               }
          }
          result.close();
     }

     private void setMRSDate(Statement theStatement) throws Exception
     {
          for(int i=0;i<VMrsNo.size();i++)
          {
               String str="";
               ResultSet result = theStatement.executeQuery("Select MRSDate From MRS_Temp Where MillCode="+iMillCode+" and MRSNo = "+(String)VMrsNo.elementAt(i));

               while(result.next())
				{
                    str=result.getString(1);
					
				}
               
			   if(str.equals(""))
				{
					ResultSet result1 = theStatement.executeQuery("Select MRSDate From MRS Where MillCode="+iMillCode+" and MRSNo = "+(String)VMrsNo.elementAt(i));
					while(result1.next())
					{
						str=result1.getString(1);
					}
					result1.close();
					VMrsDate.setElementAt(str,i);
					
				}
				else
				{
					VMrsDate.setElementAt(str,i);
					
				}
				result.close();
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VOrdDate.size()][ColumnData.length];
          for(int i=0;i<VOrdDate.size();i++)
          {
               RowData[i][0]  = (String)VOrdDate.elementAt(i);
               RowData[i][1]  = (String)VOrdNo.elementAt(i);
               RowData[i][2]  = (String)VBlock.elementAt(i);
               RowData[i][3]  = (String)VSupName.elementAt(i);
               RowData[i][4]  = (String)VMrsNo.elementAt(i);
               RowData[i][5]  = (String)VOrdCode.elementAt(i);
               RowData[i][6]  = (String)VOrdName.elementAt(i);
               RowData[i][7]  = (String)VOrdQty.elementAt(i);
               RowData[i][8]  = (String)VRecdQty.elementAt(i);
               RowData[i][9]  = (String)VPenQty.elementAt(i);
               RowData[i][10] = (String)VRate.elementAt(i);
               RowData[i][11] = (String)VBasic.elementAt(i);
               RowData[i][12] = (String)VAdvance.elementAt(i);
               RowData[i][13] = (String)VDueDate.elementAt(i);
               RowData[i][14] = (String)VDueDays.elementAt(i);
               RowData[i][15] = (String)VUserName.elementAt(i);
             //RowData[i][16] = (String)VConcName.elementAt(i);
               RowData[i][16] = "";
			   
			   String SJMDAppStatus = (String)VJMDAppStatus.elementAt(i);
			   String SIAAppStatus = (String)VIAAppStatus.elementAt(i);
			   String SSOAppStatus = (String)VSOAppStatus.elementAt(i);
			   
			   if(SJMDAppStatus.equals("1"))
				{
					RowData[i][17] = common.parseDate((String)VJMDAppDate.elementAt(i))+"(JMD)";
				}
				else if(SIAAppStatus.equals("1"))
				{
					RowData[i][17] = "JMD Pend..("+common.parseDate((String)VIAAppDate.elementAt(i))+"-IA)";
				}
				else if(SSOAppStatus.equals("1"))
				{
					RowData[i][17] = "IA Pending("+common.parseDate((String)VSOAppDate.elementAt(i))+"-SO)";
				}
				else
				{
					RowData[i][17] = "SO Pending";
				}
			
               RowData[i][18] = (String)VAppDueDays.elementAt(i);
               RowData[i][19] = (String)VPRNo.elementAt(i);
               RowData[i][20] = (String)VPRDate.elementAt(i);
			   RowData[i][21] = new Boolean(false);
			   RowData[i][22] = (String)VNextFollowUpDate.elementAt(i);
			   RowData[i][23] = (String)VCurrentStatus.elementAt(i);
			   RowData[i][24] = new Boolean(false);
          }  
     }
	 
     public String getQString(String SStDate,String SEnDate,int iStatus)
     {
          String SAuthUserCode = (String)VAuthUserCode.elementAt(VAuthUserName.indexOf((String)JCAuthUser.getSelectedItem()));

          String QString = "";

          int iCount=0;

          if(iMillCode==0)
          {
               QString = " SELECT PurchaseOrder.OrderDate, PurchaseOrder.OrderNo, OrdBlock.BlockName, PurchaseOrder.Item_Code, InvItems.Item_Name,PurchaseOrder.MrsNo,PurchaseOrder.Qty,PurchaseOrder.InvQty,PurchaseOrder.Qty-PurchaseOrder.InvQty,PurchaseOrder.Rate,PurchaseOrder.Qty*PurchaseOrder.Rate,PurchaseOrder.Advance,PurchaseOrder.DueDate,PurchaseOrder.Id,"+SSupTable+".Name,PurchaseOrder.Sup_Code,"+SSupTable+".contact,"+SSupTable+".phone,"+SSupTable+".email,MatDesc.Descr,MatDesc.Make,MatDesc.Catl,MatDesc.Draw,Uom.UomName,Unit.Unit_Name,PurchaseOrder.MillCode,PurchaseOrder.JMDOrderAppDate,PurchaseOrder.OrderBlock,PurchaseOrder.SlNo,PurchaseOrderStatus.NextFollowUpDate,PurchaseOrderStatus.Status,PurchaseOrder.Id,PurchaseOrder.JMDOrderApproval,"+
						 " PurchaseOrder.SOORDERAPPROVAL ,PurchaseOrder.SOORDERAPPDATE,PurchaseOrder.IAORDERAPPROVAL,PurchaseOrder.IAORDERAPPDATE,RawUser.UserName  "+
                         " FROM (((((PurchaseOrder INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block) "+
                         " INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code) "+
                         " INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code) "+
                         " INNER JOIN Uom ON UOM.UOMCode=InvItems.UOMCode) "+
                         " INNER JOIN Unit On PurchaseOrder.Unit_Code = Unit.Unit_Code) "+ 
						 " Left join RawUSer on RawUSer.UserCode = PurchaseOrder.MrsAuthUserCode "+
						 " Left join PurchaseOrderStatus on PurchaseOrder.ID = PurchaseOrderStatus.OrderID and ActiveStatus=0 "+
                         " Left  JOIN MatDesc  ON PurchaseOrder.Item_Code=MatDesc.Item_Code And PurchaseOrder.OrderNo = MatDesc.OrderNo And PurchaseOrder.SlNo = MatDesc.SlNo ";
     
               if(JCFilter.getSelectedIndex()==0)
               {
                   if(iStatus==0)
                     QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.OrderDate <= '"+SStDate+"' ";
                   else
                    QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.OrderDate >= '"+SStDate+"' and PurchaseOrder.OrderDate <= '"+SEnDate+"' ";
               }
               else
               if(JCFilter.getSelectedIndex()==1 || JCFilter.getSelectedIndex()==2)
               {
                    if(iStatus==0)
                      QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.OrderDate <= '"+SDate+"' and PurchaseOrder.DueDate < '"+SDate+"' ";
                    else
                      QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.OrderDate >= '"+SStDate+"' and PurchaseOrder.OrderDate <= '"+SEnDate+"' and PurchaseOrder.DueDate < '"+SEnDate+"'";
               }
               else
               if(JCFilter.getSelectedIndex()==3)
               {
                   if(iStatus==0)
                      QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.OrderDate <= '"+SDate+"' and PurchaseOrder.DueDate >= '"+SDate+"' "; 
                   else
                      QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.OrderDate >= '"+SStDate+"' and PurchaseOrder.OrderDate <= '"+SEnDate+"' and PurchaseOrder.DueDate >= '"+SEnDate+"' ";
               }
               else
               {
                   if(iStatus==0)
                     QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.Advance > 0 and PurchaseOrder.OrderDate <= '"+SDate+"'  and PurchaseOrder.DueDate < '"+SDate+"' ";
                   else
                     QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.Advance > 0 and PurchaseOrder.OrderDate >= '"+SStDate+"' and PurchaseOrder.OrderDate <= '"+SEnDate+"' and PurchaseOrder.DueDate < '"+SEnDate+"' ";
               }
     
               if(JCConcern.getSelectedIndex()>0)
                    QString = QString+" and PurchaseOrder.MillCode="+iMillCode;
     
               if(!(JCUnit.getSelectedItem()).equals("ALL"))
                    QString = QString + " and purchaseOrder.Unit_code="+VUnitCode.elementAt(JCUnit.getSelectedIndex());
               
               if(!(JCDept.getSelectedItem()).equals("ALL"))
                    QString = QString + " and purchaseOrder.Dept_code="+VDeptCode.elementAt(JCDept.getSelectedIndex());
               
               if(!(JCType.getSelectedItem()).equals("ALL"))
                    QString = QString + " and purchaseOrder.OrderTypeCode="+VOrderTypeCode.elementAt(JCType.getSelectedIndex());

               if (JCProject.getSelectedIndex()==1)
                    QString = QString + " and purchaseOrder.Project_Order=0";
               else if (JCProject.getSelectedIndex()==2)
                    QString = QString + " and purchaseOrder.Project_Order=1";
     
               if(!SAuthUserCode.equals("All"))
               {
                    QString = QString + " and PurchaseOrder.MrsAuthUserCode = "+SAuthUserCode;
               }

               if(JCApprove.getSelectedIndex()==1)
                    QString = QString + " and purchaseOrder.JMDOrderApproval=1";

			  if(JCApprove.getSelectedIndex()==2)
                    QString = QString + " and purchaseOrder.JMDOrderApproval=0";
					
               if(JCStock.getSelectedIndex()>0 && VSeleStkGroupCode.size()>0)
               {
                    QString = QString + " and InvItems.StkGroupCode in (";


                    for(int i=0;i<VSeleStkGroupCode.size();i++)
                    {
                         String SStkGroupCode = (String)VSeleStkGroupCode.elementAt(i);
     
                         iCount++;
     
                         if(iCount==1)
                         {
                              QString = QString + "'"+SStkGroupCode+"'";
                         }
                         else
                         {
                              QString = QString + ",'"+SStkGroupCode+"'";
                         }
                    }
     
                    if(iCount==0)
                         QString = QString + "''";

                    QString = QString + ")";
               }

               QString = QString + " Order By 15,1,2,3,4";
          }
          else
          {
               QString = " SELECT PurchaseOrder.OrderDate, PurchaseOrder.OrderNo, OrdBlock.BlockName, PurchaseOrder.Item_Code, InvItems.Item_Name,PurchaseOrder.MrsNo,PurchaseOrder.Qty,PurchaseOrder.InvQty,PurchaseOrder.Qty-PurchaseOrder.InvQty,PurchaseOrder.Rate,PurchaseOrder.Qty*PurchaseOrder.Rate,PurchaseOrder.Advance,PurchaseOrder.DueDate,PurchaseOrder.Id,"+SSupTable+".Name,PurchaseOrder.Sup_Code,"+SSupTable+".contact,"+SSupTable+".phone,"+SSupTable+".email,MatDesc.Descr,MatDesc.Make,MatDesc.Catl,MatDesc.Draw,Uom.UomName,Unit.Unit_Name,PurchaseOrder.MillCode,PurchaseOrder.JMDOrderAppDate,PurchaseOrder.OrderBlock,PurchaseOrder.SlNo,PurchaseOrderStatus.NextFollowUpDate,PurchaseOrderStatus.Status,PurchaseOrder.Id,PurchaseOrder.JMDOrderApproval, "+
						 " PurchaseOrder.SOORDERAPPROVAL ,PurchaseOrder.SOORDERAPPDATE,PurchaseOrder.IAORDERAPPROVAL,PurchaseOrder.IAORDERAPPDATE,RawUser.UserName  "+
                         " FROM ((((((PurchaseOrder INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block) "+
                         " INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code) "+
                         " INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code) "+
                         " INNER JOIN "+SItemTable+" ON PurchaseOrder.Item_Code="+SItemTable+".Item_Code) "+
                         " INNER JOIN Uom ON UOM.UOMCode=InvItems.UOMCode) "+
                         " INNER JOIN Unit On PurchaseOrder.Unit_Code = Unit.Unit_Code) "+ 
						 " Left join RawUSer on RawUSer.UserCode = PurchaseOrder.MrsAuthUserCode "+
						 " Left join PurchaseOrderStatus on PurchaseOrder.ID = PurchaseOrderStatus.OrderID and ActiveStatus = 0 "+
                         " Left  JOIN MatDesc  ON PurchaseOrder.Item_Code=MatDesc.Item_Code And PurchaseOrder.OrderNo = MatDesc.OrderNo And PurchaseOrder.SlNo = MatDesc.SlNo ";
     
               if(JCFilter.getSelectedIndex()==0)
               {
                   if(iStatus==0)
                    QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.OrderDate <= '"+SDate+"' ";
                   else
                    QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.OrderDate >= '"+SStDate+"' and PurchaseOrder.OrderDate <= '"+SEnDate+"' ";
               }
               else
               if(JCFilter.getSelectedIndex()==1 || JCFilter.getSelectedIndex()==2)
               {
                  if(iStatus==0)
                     QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.OrderDate <= '"+SDate+"' and PurchaseOrder.DueDate < '"+SDate+"' ";
                  else
                      QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.OrderDate >= '"+SStDate+"' and PurchaseOrder.OrderDate <= '"+SEnDate+"' and PurchaseOrder.DueDate < '"+SEnDate+"' ";
               }
               else
               if(JCFilter.getSelectedIndex()==3)
               {
                    if(iStatus==0)
                     QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.OrderDate <= '"+SDate+"' and  PurchaseOrder.DueDate >= '"+SDate+"' ";
                    else
                       QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.OrderDate >= '"+SStDate+"' and PurchaseOrder.OrderDate <= '"+SEnDate+"' and PurchaseOrder.DueDate >= '"+SDate+"' ";
               }
               else
               {
                   if(iStatus==0)
                      QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.Advance > 0 and PurchaseOrder.OrderDate <= '"+SDate+"' and PurchaseOrder.DueDate < '"+SDate+"' ";
                   else
                      QString = QString+" Where PurchaseOrder.Qty > 0 And PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.Advance > 0 and PurchaseOrder.OrderDate >= '"+SStDate+"' and PurchaseOrder.OrderDate <= '"+SEnDate+"' and PurchaseOrder.DueDate < '"+SDate+"' ";
               }
     
               if(JCConcern.getSelectedIndex()>0)
                    QString = QString+" and PurchaseOrder.MillCode="+iMillCode;
     
               if(!(JCUnit.getSelectedItem()).equals("ALL"))
                    QString = QString + " and purchaseOrder.Unit_code="+VUnitCode.elementAt(JCUnit.getSelectedIndex());
               
               if(!(JCDept.getSelectedItem()).equals("ALL"))
                    QString = QString + " and purchaseOrder.Dept_code="+VDeptCode.elementAt(JCDept.getSelectedIndex());
               
               if(!(JCType.getSelectedItem()).equals("ALL"))
                    QString = QString + " and purchaseOrder.OrderTypeCode="+VOrderTypeCode.elementAt(JCType.getSelectedIndex());

               if (JCProject.getSelectedIndex()==1)
                    QString = QString + " and purchaseOrder.Project_Order=0";
               else if (JCProject.getSelectedIndex()==2)
                    QString = QString + " and purchaseOrder.Project_Order=1";
     
               if(!SAuthUserCode.equals("All"))
               {
                    QString = QString + " and PurchaseOrder.MrsAuthUserCode = "+SAuthUserCode;
               }

               if(JCApprove.getSelectedIndex()==1)
                    QString = QString + " and purchaseOrder.JMDOrderApproval=1";

				if(JCApprove.getSelectedIndex()==2)
                    QString = QString + " and purchaseOrder.JMDOrderApproval=0";

               if(JCStock.getSelectedIndex()>0 && VSeleStkGroupCode.size()>0)
               {
                    QString = QString + " and "+SItemTable+".StkGroupCode in (";


                    for(int i=0;i<VSeleStkGroupCode.size();i++)
                    {
                         String SStkGroupCode = (String)VSeleStkGroupCode.elementAt(i);
     
                         iCount++;
     
                         if(iCount==1)
                         {
                              QString = QString + "'"+SStkGroupCode+"'";
                         }
                         else
                         {
                              QString = QString + ",'"+SStkGroupCode+"'";
                         }
                    }
     
                    if(iCount==0)
                         QString = QString + "''";

                    QString = QString + ")";
               }

               QString = QString + " Order By 15,1,2,3,4";
          }
		 
System.out.println("QS:"+QString);
          return QString;
     }
     
     private void setConcernCombo()
     {
          VCName = new Vector();
          VCCode = new Vector();
          
          VCCode.addElement("2");
          VCName.addElement("All");
          
          try
          {
               ORAConnection          oraconnection         =  ORAConnection.getORAConnection();
               Connection             theConnection         =  oraconnection.getConnection();               
               Statement              theStatement          =  theConnection.createStatement();
               ResultSet              result1               =  theStatement.executeQuery("Select MillCode,MillName From  Mill  Order By 1");
               while(result1.next())
               {
                    VCCode.addElement(result1.getString(1));
                    VCName.addElement(result1.getString(2));
               }
               result1.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               System.exit(0);
          }
     }
     
     private void setUnitCombo()
     {
          VUnit       = new Vector();
          VUnitCode   = new Vector();
          
          VUnit     .addElement("ALL");
          VUnitCode .addElement("9999");
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               String QS1 = "";

               QS1 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";
               
               ResultSet result3 = stat.executeQuery(QS1);
               while(result3.next())
               {
                    VUnit     .addElement(result3.getString(1));
                    VUnitCode .addElement(result3.getString(2));
               }
               result3.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept,Group & Unit :"+ex);
               System.exit(0);
          }
     }

     private void setDeptCombo()
     {
          VDept          = new Vector();
          VDeptCode      = new Vector();
          
          VDept          .addElement("ALL");
          VDeptCode      .addElement("9999");
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               String QS1 = "";
               QS1 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               
               ResultSet result3 = stat.executeQuery(QS1);
               while(result3.next())
               {
                    VDept     .addElement(result3.getString(1));
                    VDeptCode .addElement(result3.getString(2));
               }
               result3.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept:"+ex);
               System.exit(0);
          }
     }

     private void setTypeCombo()
     {
          VOrderType     = new Vector();
          VOrderTypeCode = new Vector();
          
          VOrderType     .addElement("ALL");
          VOrderTypeCode .addElement("9999");
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               String QS1 = "";
               QS1 = " Select TypeName,TypeCode From OrderType Order By TypeName";
               
               ResultSet result1 = stat.executeQuery(QS1);
               while(result1.next())
               {
                    VOrderType     .addElement(result1.getString(1));
                    VOrderTypeCode .addElement(result1.getString(2));
               }
               result1.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("Type:"+ex);
               System.exit(0);
          }
     }
     
     private void setProjectCombo()
     {
          VProjectCode = new Vector();
          VProjectName = new Vector();
          
          VProjectName.addElement("All");
          VProjectCode.addElement("1");
          
          VProjectName.addElement("Other Projects    ");
          VProjectCode.addElement("2");
          
          VProjectName.addElement("Machinery Projects");
          VProjectCode.addElement("3");
     }
     private void setPresets()
     {
          if(iMillCode<2)
          {
               JCConcern . setSelectedIndex(iMillCode+1);
          }
          else
          {
               JCConcern . setSelectedIndex(iMillCode);
          }

          if(iMillCode==0)
          {
               JCConcern . setEnabled(true);
          }
          else
          {
               JCConcern . setEnabled(false);
          }
     }


     private void setAuthUsers()
     {
          VAuthUserCode  = null;
          VAuthUserName  = null;

          VAuthUserCode  = new Vector();
          VAuthUserName  = new Vector();

          VAuthUserCode  . removeAllElements();
          VAuthUserName  . removeAllElements();

          VAuthUserCode  . addElement("All");
          VAuthUserName  . addElement("All");

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               ResultSet      result        =  stat.executeQuery(" Select UserCode, UserName from RawUser where UserCode in "+
                                                                 " (Select distinct AuthUserCode from MrsUserAuthentication) "+
                                                                 " Order by 2 ");

               while(result.next())
               {
                    VAuthUserCode  . addElement(result.getString(1));
                    VAuthUserName  . addElement(result.getString(2));
               }

               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
