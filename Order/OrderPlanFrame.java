package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class OrderPlanFrame extends JInternalFrame
{

     // The three Major Partions of OrderPlanFrame
     JPanel TopPanel,TopLeftPanel,TopRightPanel,BottomPanel;
     OrderPlanMiddlePanel MiddlePanel;

     // Components for TopPanel
     JButton    BSupplier;
     NextField  TOrderNo,TPayDays;
     JTextField TSupCode;
     JTextField TAdvance,TPayTerm,TRefArea;
     DateField  TDate;
     JComboBox  JCBlock,JCTo,JCThro,JEPCG,JCOrderType,JCProject,JCState,JCPort;

     // Components for BottomPanel
     JButton    BOk,BCancel;
     
     Connection theConnection = null;

     // Parameters Common for Both Manual and Auto Mode
     JLayeredPane DeskTop;
     StatusPanel SPanel;
     Vector VCode,VName;

     // Counterpart of Parameters from MatReqFrame Manual Mode
     Vector VSelectedId,VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedRate,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedDue,VSelectedDesc,VSelectedBlock,VSelectedMRSSlNo;
     Vector VSelectedColor,VSelectedSet,VSelectedSize,VSelectedSide,VSelectedSlipNo,VSelectedBookNo,VSelectedSlipFrNo,VSelectedCatl,VSelectedDraw,VSelectedMake,VSelectedUserCode,VSelectedMrsType;
     Vector VTo,VThro,VToCode,VThroCode,VPortCode,VPortName;

     // Packed Vectors for Automatic Pooling Technology
     OrderRec orderrec;

     // Utilities 
     Common common = new Common();
     Control control = new Control();

     int iMrsLink=0;    // Identifier of Manual Mode and Auto Mode
                        // 0 indicates Manual Mode Setting
                        // 1 indicates Auto   Mode Setting

     FileWriter FW;

     int iUserCode;
     int iMillCode=0;

     String SYearCode;
     String SItemTable,SSupTable;


     boolean bDescFlag = true;
     boolean bComflag  = true;

	String SToName,SThroName;

     // Called When Manual Order Placement Option is clicked
     OrderPlanFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VSelectedId,Vector VSelectedCode,Vector VSelectedName,Vector VSelectedMRS,Vector VSelectedQty,Vector VSelectedUnit,Vector VSelectedDept,Vector VSelectedGroup,Vector VSelectedDue,Vector VSelectedDesc,StatusPanel SPanel,int iUserCode,int iMillCode,Vector VSelectedBlock,Vector VSelectedMRSSlNo,Vector VSelectedColor,Vector VSelectedSet,Vector VSelectedSize,Vector VSelectedSide,Vector VSelectedSlipNo,Vector VSelectedBookNo,Vector VSelectedSlipFrNo,Vector VSelectedCatl,Vector VSelectedDraw,Vector VSelectedMake,Vector VSelectedUserCode,Vector VSelectedMrsType,String SYearCode,String SItemTable,String SSupTable)
     {
          this.DeskTop           = DeskTop;
          this.VCode             = VCode;
          this.VName             = VName;
          this.VSelectedId       = VSelectedId;
          this.VSelectedCode     = VSelectedCode;
          this.VSelectedName     = VSelectedName;
          this.VSelectedMRS      = VSelectedMRS;
          this.VSelectedQty      = VSelectedQty;
          this.VSelectedUnit     = VSelectedUnit;
          this.VSelectedDept     = VSelectedDept;
          this.VSelectedGroup    = VSelectedGroup;
          this.VSelectedDue      = VSelectedDue;
          this.VSelectedDesc     = VSelectedDesc;
          this.SPanel            = SPanel;
          this.iUserCode         = iUserCode;
          this.iMillCode         = iMillCode;
          this.VSelectedBlock    = VSelectedBlock;
          this.VSelectedMRSSlNo  = VSelectedMRSSlNo;
          this.VSelectedColor    = VSelectedColor;
          this.VSelectedSet      = VSelectedSet;
          this.VSelectedSize     = VSelectedSize;
          this.VSelectedSide     = VSelectedSide;
          this.VSelectedSlipNo   = VSelectedSlipNo;
          this.VSelectedBookNo   = VSelectedBookNo;
          this.VSelectedSlipFrNo = VSelectedSlipFrNo;
          this.VSelectedCatl     = VSelectedCatl;
          this.VSelectedDraw     = VSelectedDraw;
          this.VSelectedMake     = VSelectedMake;
          this.VSelectedUserCode = VSelectedUserCode;
          this.VSelectedMrsType  = VSelectedMrsType;
          this.SYearCode         = SYearCode;
          this.SItemTable        = SItemTable;
          this.SSupTable         = SSupTable;


          iMrsLink=0;                              // Indicates Manual Mode

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }

     // Called From OrderPooling Class When Auto Mode is selected
     OrderPlanFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,OrderRec orderrec,StatusPanel SPanel,int iUserCode,int iMillCode,Vector VSelectedMRSSlNo,Vector VSelectedColor,Vector VSelectedSet,Vector VSelectedSize,Vector VSelectedSide,Vector VSelectedSlipNo,Vector VSelectedBookNo,Vector VSelectedSlipFrNo,Vector VSelectedCatl,Vector VSelectedDraw,Vector VSelectedMake,Vector VSelectedUserCode,Vector VSelectedMrsType,String SYearCode,String SItemTable,String SSupTable)
     {
          this.DeskTop           = DeskTop;
          this.VCode             = VCode;
          this.VName             = VName;
          this.orderrec          = orderrec;
          this.SPanel            = SPanel;
          this.iUserCode         = iUserCode;
          this.iMillCode         = iMillCode;
          this.VSelectedMRSSlNo  = VSelectedMRSSlNo;
          this.VSelectedColor    = VSelectedColor;
          this.VSelectedSet      = VSelectedSet;
          this.VSelectedSize     = VSelectedSize;
          this.VSelectedSide     = VSelectedSide;
          this.VSelectedSlipNo   = VSelectedSlipNo;
          this.VSelectedBookNo   = VSelectedBookNo;
          this.VSelectedSlipFrNo = VSelectedSlipFrNo;
          this.VSelectedCatl     = VSelectedCatl;
          this.VSelectedDraw     = VSelectedDraw;
          this.VSelectedMake     = VSelectedMake;
          this.VSelectedUserCode = VSelectedUserCode;
          this.VSelectedMrsType  = VSelectedMrsType;
          this.SYearCode         = SYearCode;
          this.SItemTable        = SItemTable;
          this.SSupTable         = SSupTable;


          iMrsLink=1;                           // indicates Auto Mode Setting

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          setReference();
          addListeners();
          show();
     }

     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnection  =    oraConnection.getConnection();
     }

     public void setReference()
     {
          TSupCode  .setText(orderrec.SSupCode);
          BSupplier .setText(orderrec.SSupName);

          /*int iToId   = VToCode.indexOf(orderrec.SToCode);
          int iThroId = VThroCode.indexOf(orderrec.SThroCode);

          JCTo      .setSelectedIndex((iToId==-1 ? 0:iToId));
          JCThro    .setSelectedIndex((iThroId==-1 ? 0:iThroId));*/

          TPayTerm  .setText(orderrec.SPayTerm);
          TPayDays  .setText(orderrec.SPayDays);

          for(int i=0;i<orderrec.VOrdNo.size();i++)
          {
               String SOrdNo  = (String)orderrec.VOrdNo.elementAt(i);
               JCBlock        .setSelectedIndex(common.toInt((String)orderrec.VBlock.elementAt(i)));
               getNextOrderNo();
               String SBlock  = (String)orderrec.VBlock.elementAt(i);
               String SDate   = common.parseDate((String)orderrec.VDate.elementAt(i));
               TRefArea       .setText(SBlock+"-"+SOrdNo+" Dt."+SDate+"\n");
          }
     }

     public void createComponents()
     {
          BOk            = new JButton("Okay");
          BCancel        = new JButton("Abort");
          BSupplier      = new JButton("Supplier");

          TOrderNo       = new NextField();
          TDate          = new DateField();
          TSupCode       = new JTextField();
          TAdvance       = new JTextField();
          TPayTerm       = new JTextField();
          TPayDays       = new NextField();
          TRefArea       = new JTextField();
          JCBlock        = new JComboBox();
          JEPCG          = new JComboBox();
          JCOrderType    = new JComboBox();
          JCProject      = new JComboBox();
          JCState        = new JComboBox();

          getVTo();
          JCPort         = new JComboBox(VPortName);
          JCTo           = new JComboBox(VTo);
          JCThro         = new JComboBox(VThro);

          if(iMrsLink == 1)
             MiddlePanel = new OrderPlanMiddlePanel(DeskTop,VCode,VName,orderrec,iMillCode,VSelectedColor,VSelectedSet,VSelectedSize,VSelectedSide,VSelectedSlipNo,VSelectedBookNo,VSelectedSlipFrNo,VSelectedCatl,VSelectedDraw,VSelectedMake,SItemTable);
          else
             MiddlePanel = new OrderPlanMiddlePanel(DeskTop,VCode,VName,VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedDue,VSelectedDesc,iMillCode,VSelectedBlock,VSelectedColor,VSelectedSet,VSelectedSize,VSelectedSide,VSelectedSlipNo,VSelectedBookNo,VSelectedSlipFrNo,VSelectedCatl,VSelectedDraw,VSelectedMake,SItemTable,VSelectedMRSSlNo,VSelectedUserCode,VSelectedMrsType);

          JCPort.setSelectedItem("Unknown");
		JCTo.setSelectedItem(SToName);
		JCThro.setSelectedItem(SThroName);

          TopPanel       = new JPanel();
          TopLeftPanel   = new JPanel();
          TopRightPanel  = new JPanel();
          BottomPanel    = new JPanel();

          TDate          .setTodayDate();
          TOrderNo       .setEditable(false);
          TPayDays       .setEditable(false);
          TAdvance       .setEditable(false);
     }

     public void setLayouts()
     {
          setTitle("Order Placement Against Selected MRS Materials");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,550);

          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(1,2));
          TopLeftPanel        .setLayout(new GridLayout(7,2));
          TopRightPanel       .setLayout(new GridLayout(8,2));
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          JCBlock.addItem("Stockable Stores     - SST");
          JCBlock.addItem("Stockable Spares     - SSP");
          JCBlock.addItem("Non-Stockable Stores - NST");
          JCBlock.addItem("Non-Stockable Spares - NSP");
          JCBlock.addItem("Capital/Project      - NPR");

          JEPCG.addItem("Non-EPCG Order   ");
          JEPCG.addItem("EPCG Order       ");

          JCOrderType.addItem("Local Order      ");
          JCOrderType.addItem("Import           ");

          TopLeftPanel.add(new JLabel("Order No"));
          TopLeftPanel.add(TOrderNo);

          TopLeftPanel.add(new JLabel("Order Date"));
          TopLeftPanel.add(TDate);

          TopLeftPanel.add(new JLabel("Reference"));
          TopLeftPanel.add(TRefArea);

          TopLeftPanel.add(new JLabel("EPCG Order   "));
          TopLeftPanel.add(JEPCG);

          TopLeftPanel.add(new JLabel("Order Type   "));
          TopLeftPanel.add(JCOrderType);

          TopLeftPanel.add(new JLabel("Project/Non"));
          TopLeftPanel.add(JCProject);

          TopRightPanel.add(new JLabel("Supplier"));
          TopRightPanel.add(BSupplier);

          TopRightPanel.add(new JLabel("Book To"));
          TopRightPanel.add(JCTo);

          TopRightPanel.add(new JLabel("Book Thro"));
          TopRightPanel.add(JCThro);

          TopRightPanel.add(new JLabel("Advance"));
          TopRightPanel.add(TAdvance);

          TopRightPanel.add(new JLabel("Payment Terms"));
          TopRightPanel.add(TPayTerm);

          TopRightPanel.add(new JLabel("Intra/Inter State"));
          TopRightPanel.add(JCState);

          JCProject  .addItem("Other Projects    ");
          JCProject  .addItem("Machinery Projects");

          JCState    .addItem("None             ");
          JCState    .addItem("Intra Stete      ");
          JCState    .addItem("Inter State      ");

          JCOrderType.setEnabled(false);
          JCProject.setEnabled(false);
          JCState.setEnabled(false);
          JCPort.setEnabled(false);

          TopRightPanel.add(new JLabel("Port         "));
          TopRightPanel.add(JCPort);

          TopPanel.add(TopLeftPanel);
          TopPanel.add(TopRightPanel);

          TopLeftPanel.setBorder(new TitledBorder("Order Id Block-I"));
          TopRightPanel.setBorder(new TitledBorder("Order Id Block-II"));

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          getNextOrderNo();
     }

     public void addListeners()
     {
           JCOrderType.addFocusListener(new FocListener());
           JEPCG.addFocusListener(new FocListener());
           JCBlock.addFocusListener(new FocListener());
           JCState.addFocusListener(new FocListener());
           JCPort.addFocusListener(new FocListener());
           BSupplier.addActionListener(new SupplierSearch(DeskTop,TSupCode,SSupTable));
           BOk.addActionListener(new ActList());
           BCancel.addActionListener(new ActList());
     }

     public class FocListener implements FocusListener
     {
          public void focusLost(FocusEvent fe)
          {
               if (fe.getSource() == JEPCG)
               {
                    if (JEPCG.getSelectedIndex() == 0)
                    {
                         JCTo.requestFocus();
                         JCOrderType.setEnabled(false);
                         JCState.setEnabled(false);
                         JCPort.setEnabled(false);
                    }
                    else
                    {
                         JCOrderType.requestFocus();
                         JCOrderType.setEnabled(true);
                         JCState.setEnabled(true);
                         JCPort.setEnabled(true);
                    }
               }

               if (fe.getSource() == JCBlock)
               {
                    if (JCBlock.getSelectedIndex() == 4)
                    {
                         JCProject.setEnabled(true);
                    }
                    else
                    {
                         JCProject.setEnabled(false);
                         JCProject.setSelectedIndex(0);
                    }
               }

               if ((fe.getSource() == JCOrderType) && (JEPCG.getSelectedIndex()==1))
               {
                    if (JCOrderType.getSelectedIndex() == 1)
                    {
                         JCState.setEnabled(false);
                         JCPort.setEnabled(true);
                         JCPort.setSelectedIndex(1);
                         JCState.setSelectedIndex(0);                         
                    }
                    else
                    {
                         JCPort.setSelectedIndex(0);                         
                         JCPort.setEnabled(false);
                         JCState.setEnabled(true);
                         JCState.setSelectedIndex(1);                         
                    }
               }
          }
          public void focusGained(FocusEvent fe)
          {
          }
     }

     public class ActList implements ActionListener
     {
            public void actionPerformed(ActionEvent ae)
            {

                  if(ae.getSource()==BOk)
                  {
                       if(common.toInt(TOrderNo.getText())>0)
                       {

                             if(bDescFlag==true)
                              BOk.setEnabled(false);

                             if(!isValidNew())
                             {
                                 common.warn(DeskTop,SPanel);
                                 BOk.setEnabled(true);
                                 return;
                             }

                             try
                             {
                                  getInsertOrderNo();
     
                                  insertOrderDetails();
                                  if(iMrsLink==1)
                                  {
                                        setPoolMrsLink();   // Updating MRS Ids from OrderRec Object
                                        setPoolEnquiryLink();
                                  }
                                  else
                                  {
                                        setMrsLink();      // Updating MRS Ids from Selected Vectors
                                        setEnquiryLink();
                                  }
     
                                  UpdateInvItems();
                             }
                             catch(Exception Ex)
                             {
                                  Ex.printStackTrace();
                                  bComflag = false;
                             }

                             try
                             {
                                   if(bComflag)
                                   {
                                        theConnection  . commit();
                                        System         . out.println("Commit");
                                        theConnection  . setAutoCommit(true);

                                        if(bDescFlag==true)
                                             removeHelpFrame();
                                   }
                                   else
                                   {
                                        theConnection  . rollback();
                                        System         . out.println("RollBack");
                                        theConnection  . setAutoCommit(true);
                                        BOk.setEnabled(true);
                                   }
                             }catch(Exception ex)
                             {
                                   ex.printStackTrace();
                             }
                       }
                  }
                  if(ae.getSource()==BCancel)
                  {
                       removeHelpFrame();
                  }     
            }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
     public void insertOrderDetails()
     {
          String QString = "Insert Into PurchaseOrder (OrderNo,OrderDate,OrderBlock,MrsNo,Sup_Code,Reference,Advance,PayTerms,PayDays,ToCode,ThroCode,Item_Code,Qty,Rate,DiscPer,Disc,CenvatPer,CenVat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,Unit_Code,Dept_Code,Group_Code,DueDate,SlNo,EPCG,OrderType,Project_order,state,portcode,UserCode,ModiDate,MillCode,MRSSLNO,MrsAuthUserCode,OrderTypeCode,PrevOrderNo,OrderSourceTypeCode,PrevOrderBlock,TAXCLAIMABLE,Authentication,PrevOrderChange,docid,id) Values (";
          try
          {
               if(theConnection.getAutoCommit())
                         theConnection.setAutoCommit(false);

               Statement theStatement    = theConnection.createStatement();

               Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
               String SAdd       = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess      = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm        = common.toDouble(SAdd)-common.toDouble(SLess);
               double dBasic     = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio     = dpm/dBasic;

               for(int i=0;i<FinalData.length;i++)
               {
                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SBlockCode = MiddlePanel.MiddlePanel.getBlockCode(i);
                    String SDueDate   = MiddlePanel.MiddlePanel.getDueDate(i,TDate);
                    dBasic            = common.toDouble(((String)FinalData[i][11]).trim());
                    String SMisc      = common.getRound(dBasic*dRatio,3);

                    String STaxClaim  = (String)MiddlePanel.MiddlePanel.getTaxClaim(i);
                       int iTaxClaim  = common.toInt(STaxClaim.trim());


                    int iMRSSlNo       = common.toInt((String)MiddlePanel.MiddlePanel.VMRSSlNo.elementAt(i));
                    int iMRSUserCode   = common.toInt((String)MiddlePanel.MiddlePanel.VMRSUserCode.elementAt(i));
                    int iMRSType       = common.toInt((String)MiddlePanel.MiddlePanel.VMRSType.elementAt(i));
                    int iPrevOrderNo   = common.toInt((String)MiddlePanel.MiddlePanel.VPOrderNo.elementAt(i));
                    int iPrevSource    = common.toInt((String)MiddlePanel.MiddlePanel.VPSource.elementAt(i));
                    int iPrevBlockCode = common.toInt((String)MiddlePanel.MiddlePanel.VPBlockCode.elementAt(i));

                    int iDeptCode  = common.toInt(SDeptCode);
                    int iGroupCode = common.toInt(SGroupCode);

				String SSupCode   = TSupCode.getText();
				String SRate      = common.getRound(common.toDouble(((String)FinalData[i][6]).trim()),4);
				String SDiscPer   = common.getRound(common.toDouble(((String)FinalData[i][7]).trim()),2);
				String SCenvatPer = common.getRound(common.toDouble(((String)FinalData[i][8]).trim()),2);
				String STaxPer    = common.getRound(common.toDouble(((String)FinalData[i][9]).trim()),2);
				String SSurPer    = common.getRound(common.toDouble(((String)FinalData[i][10]).trim()),2);

				int iPrevOrderChange=0;
				
				if(iMrsLink==1)
				{
					iPrevOrderChange = getPrevOrderChangeStatus(i,SSupCode,SRate,SDiscPer,SCenvatPer,STaxPer,SSurPer);
				}
                    
                    String QS1 = QString;
                    QS1 = QS1+"0"+TOrderNo.getText()+",";
                    QS1 = QS1+"'"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                    QS1 = QS1+""+SBlockCode+",";
                    QS1 = QS1+"0"+((String)FinalData[i][4]).trim()+",";
                    QS1 = QS1+"'"+TSupCode.getText()+"',";
                    QS1 = QS1+"'"+(TRefArea.getText()).toUpperCase()+"',";    
                    QS1 = QS1+"0"+TAdvance.getText()+",";
                    QS1 = QS1+"'"+TPayTerm.getText()+"',";
                    QS1 = QS1+"0"+TPayDays.getText()+",";
                    QS1 = QS1+"0"+VToCode.elementAt(JCTo.getSelectedIndex())+",";
                    QS1 = QS1+"0"+VThroCode.elementAt(JCThro.getSelectedIndex())+",";
                    QS1 = QS1+"'"+(String)FinalData[i][0]+"',";
                    QS1 = QS1+"0"+((String)FinalData[i][5]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][6]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][7]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][12]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][8]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][13]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][9]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][14]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][10]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][15]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][16]).trim()+",";
                    QS1 = QS1+"0"+SAdd+",";
                    QS1 = QS1+"0"+SLess+",";
                    QS1 = QS1+"0"+SMisc+",";
                    QS1 = QS1+"0"+SUnitCode+",";
                    QS1 = QS1+iDeptCode+",";
                    QS1 = QS1+iGroupCode+",";
                    QS1 = QS1+"'"+SDueDate+"',";
                    QS1 = QS1+(i+1)+",";
                    QS1 = QS1+JEPCG.getSelectedIndex()+",";
                    QS1 = QS1+JCOrderType.getSelectedIndex()+",";
                    QS1 = QS1+JCProject.getSelectedIndex()+",";
                    QS1 = QS1+JCState.getSelectedIndex()+",";
                    QS1 = QS1+VPortCode.elementAt(JCPort.getSelectedIndex())+",";
                    QS1 = QS1+iUserCode+",";
                    QS1 = QS1+"'"+common.getServerDateTime()+"',";
                    QS1 = QS1+iMillCode+",";
                    QS1 = QS1+iMRSSlNo+",";
                    QS1 = QS1+iMRSUserCode+",";
                    QS1 = QS1+iMRSType+",";
                    QS1 = QS1+iPrevOrderNo+",";
                    QS1 = QS1+iPrevSource+",";
                    QS1 = QS1+iPrevBlockCode+",";
                    QS1 = QS1+iTaxClaim+",";
                    QS1 = QS1+"1"+",";
                    QS1 = QS1+iPrevOrderChange+",";
                    QS1 = QS1+"0"+((String)FinalData[i][21]).trim()+",";
                    QS1 = QS1+" purchaseorder_seq.nextval) " ;

                    theStatement.executeUpdate(QS1);
               }                        
               theStatement.close();
               insertDescDetails();

//	       updateDocumentSupplier();	
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     public int getPrevOrderChangeStatus(int i,String SSupCode,String SRate,String SDiscPer,String SCenvatPer,String STaxPer,String SSurPer)
     {
		int iStatus=0;

		if(!SSupCode.equals(orderrec.SSupCode))
		{
			iStatus = 1;
			return iStatus;		
		}

		String SPRate      = common.getRound(common.toDouble((String)orderrec.VRate.elementAt(i)),4);
		String SPDiscPer   = common.getRound(common.toDouble((String)orderrec.VDiscPer.elementAt(i)),2);
		String SPCenvatPer = common.getRound(common.toDouble((String)orderrec.VCenVatPer.elementAt(i)),2);
		String SPTaxPer    = common.getRound(common.toDouble((String)orderrec.VTaxPer.elementAt(i)),2);
		String SPSurPer    = common.getRound(common.toDouble((String)orderrec.VSurPer.elementAt(i)),2);

		if((!SRate.equals(SPRate)) || (!SDiscPer.equals(SPDiscPer)) || (!SCenvatPer.equals(SPCenvatPer)) || (!STaxPer.equals(SPTaxPer)) || (!SSurPer.equals(SPSurPer)))
		{
			iStatus = 1;
		}

          return iStatus;
     }

     public void insertDescDetails()
     {
          String QString = "Insert Into MatDesc (OrderNo,OrderDate,OrderBlock,Item_Code,Descr,Make,Draw,Catl,SlNo,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE,SLIPFROMNO,SLIPTONO,BOOKFROMNO,BOOKTONO,id,MillCode) Values (";
          String QString1= "Insert Into MatDesc (OrderNo,OrderDate,OrderBlock,Item_Code,Descr,Make,Draw,Catl,SlNo,id,MillCode) Values (";
          String QS1="";

          try
          {
               if(theConnection.getAutoCommit())
                         theConnection.setAutoCommit(false);
     
               Statement theStatement    = theConnection.createStatement();
               Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();

               for(int i=0;i<FinalData.length;i++)
               {
                    String SDesc = common.parseNull((String)MiddlePanel.MiddlePanel.VDesc.elementAt(i));
                    String SMake = common.parseNull((String)MiddlePanel.MiddlePanel.VMake.elementAt(i));
                    String SDraw = (String)MiddlePanel.MiddlePanel.VDraw.elementAt(i);
                    String SCatl = (String)MiddlePanel.MiddlePanel.VCatl.elementAt(i);
                    String SBlockCode = MiddlePanel.MiddlePanel.getBlockCode(i);

                    String SItem_Code   = (String)FinalData[i][0];

                    if(isStationary(SItem_Code))
                    {
                         String    SPaperColor    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPColour   . elementAt(i));
                         String    SPaperSets     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSet      . elementAt(i));
                         String    SPaperSize     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSize     . elementAt(i));
                         String    SPaperSide     =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSide     . elementAt(i));
                         String    SSlipFromNo    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSlipFrNo . elementAt(i));
                         String    SSlipToNo      =    common.parseNull((String)MiddlePanel.MiddlePanel.VPSlipToNo . elementAt(i));
                         String    SBookFromNo    =    common.parseNull((String)MiddlePanel.MiddlePanel.VPBookFrNo . elementAt(i));
                         String    SBookToNo      =    common.parseNull((String)MiddlePanel.MiddlePanel.VPBookToNo . elementAt(i));


                         SSlipFromNo = SSlipFromNo.trim();
                         SSlipToNo   = SSlipToNo.trim();
                         SBookFromNo = SBookFromNo.trim();
                         SBookToNo   = SBookToNo.trim();


                         int iSlipToNo = common.toInt(SSlipToNo);
                         int iBookToNo = common.toInt(SBookToNo);

                         if(iSlipToNo<=0)
                         {
                              SBookFromNo = "0";
                              SBookToNo   = "0";
                         }

                         if(iSlipToNo>0)
                         {
                              if(iBookToNo<=0)
                              {
                                   JOptionPane.showMessageDialog(null,"Please Entered The BookNo","Dear User",JOptionPane.INFORMATION_MESSAGE);
                                   bDescFlag=false;
                              }        
                         }

                                   QS1 = QString;
                                   QS1 = QS1+"0"+TOrderNo.getText()+",";
                                   QS1 = QS1+"'"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                                   QS1 = QS1+""+SBlockCode+",";
                                   QS1 = QS1+"'"+(String)FinalData[i][0]+"',";
                                   QS1 = QS1+"'"+common.getNarration(SDesc)+"',";
                                   QS1 = QS1+"'"+common.getNarration(SMake)+"',";
                                   QS1 = QS1+"'"+SDraw+"',";
                                   QS1 = QS1+"'"+SCatl+"',";
                                   QS1 = QS1+(i+1)+",";
                                   QS1 = QS1+"'"+SPaperColor+"',";
                                   QS1 = QS1+"'"+SPaperSets+"',";
                                   QS1 = QS1+"'"+SPaperSize+"',";
                                   QS1 = QS1+"'"+SPaperSide+"',";
                                   QS1 = QS1+"0"+SSlipFromNo+",";
                                   QS1 = QS1+"0"+SSlipToNo+",";
                                   QS1 = QS1+"0"+SBookFromNo+",";
                                   QS1 = QS1+"0"+SBookToNo+",";
                                   QS1 = QS1+" matdesc_seq.nextval , ";
                                   QS1 = QS1+"0"+iMillCode+")";
                    }
                    else
                    {

                         QS1 = QString1;
                         QS1 = QS1+"0"+TOrderNo.getText()+",";
                         QS1 = QS1+"'"+TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText()+"',";
                         QS1 = QS1+""+SBlockCode+",";
                         QS1 = QS1+"'"+(String)FinalData[i][0]+"',";
                         QS1 = QS1+"'"+common.getNarration(SDesc)+"',";
                         QS1 = QS1+"'"+common.getNarration(SMake)+"',";
                         QS1 = QS1+"'"+SDraw+"',";
                         QS1 = QS1+"'"+SCatl+"',";
                         QS1 = QS1+(i+1)+",";
                         QS1 = QS1+" matdesc_seq.nextval , ";
                         QS1 = QS1+"0"+iMillCode+" )";
                    }
                    theStatement.executeUpdate(QS1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     public void setMrsLink()
     {
          try
          {

               if(theConnection.getAutoCommit())
                         theConnection.setAutoCommit(false);

               Statement theStatement    = theConnection.createStatement();
               for(int i=0;i<VSelectedId.size();i++)
               {
                        String SBlockCode = MiddlePanel.MiddlePanel.getBlockCode(i);


                        String QS1 = "Update MRS set ";
                        QS1 = QS1+"OrderNo   ="+TOrderNo.getText()+",";
                        QS1 = QS1+"OrderBlock="+SBlockCode;
                        QS1 = QS1+" Where Id ="+(String)VSelectedId.elementAt(i);
                        theStatement.executeUpdate(QS1);
                }
                theStatement.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }

      }
     public void setPoolMrsLink()
     {
          try
          {
               if(theConnection.getAutoCommit())
                         theConnection.setAutoCommit(false);

               Statement theStatement    = theConnection.createStatement();
               for(int i=0;i<orderrec.VMRSId.size();i++)
               {

                        String SBlockCode = MiddlePanel.MiddlePanel.getBlockCode(i);

                        String QS1 = "Update MRS set ";
                        QS1 = QS1+"OrderNo   ="+TOrderNo.getText()+",";
                        QS1 = QS1+"OrderBlock="+SBlockCode;
                        QS1 = QS1+" Where Id ="+(String)orderrec.VMRSId.elementAt(i);
                        theStatement.executeUpdate(QS1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }

     }

     public void setPoolEnquiryLink()
     {
          try
          {
               if(theConnection.getAutoCommit())
                         theConnection.setAutoCommit(false);

               Statement theStatement    = theConnection.createStatement();
               for(int i=0;i<orderrec.VMRSId.size();i++)
               {
                        String QS = " Select EnquiryNo,MrsNo,Item_Code from Mrs Where Id="+(String)orderrec.VMRSId.elementAt(i);

                        ResultSet res = theStatement.executeQuery(QS);
                        while(res.next())
                        {
                             int iEnqNo    = common.toInt(res.getString(1));
                             String SMrsNo = res.getString(2);
                             String SCode  = res.getString(3);

                             if(iEnqNo>0)
                             {
                                  String QS1 = "Update Enquiry set CloseStatus=1 ";
                                  QS1 = QS1+" Where EnqNo="+iEnqNo;
                                  QS1 = QS1+" and Mrs_No="+SMrsNo;
                                  QS1 = QS1+" and Item_Code='"+SCode+"'";
                                  theStatement.executeUpdate(QS1);
                             }
                        }
                        res.close();
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     public void setEnquiryLink()
     {
          try
          {

               if(theConnection.getAutoCommit())
                         theConnection.setAutoCommit(false);

               Statement theStatement    = theConnection.createStatement();
               for(int i=0;i<VSelectedId.size();i++)
               {
                   String QS = " Select EnquiryNo,MrsNo,Item_Code from Mrs Where Id="+(String)VSelectedId.elementAt(i);

                   ResultSet res = theStatement.executeQuery(QS);
                   while(res.next())
                   {
                        int iEnqNo    = common.toInt(res.getString(1));
                        String SMrsNo = res.getString(2);
                        String SCode  = res.getString(3);

                        if(iEnqNo>0)
                        {
                             String QS1 = "Update Enquiry set CloseStatus=1 ";
                             QS1 = QS1+" Where EnqNo="+iEnqNo;
                             QS1 = QS1+" and Mrs_No="+SMrsNo;
                             QS1 = QS1+" and Item_Code='"+SCode+"'";
                             theStatement.executeUpdate(QS1);
                        }
                   }
                   res.close();
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }

      }

     public void getVTo()
     {
          VTo       = new Vector();
          VToCode   = new Vector();
          VThro     = new Vector();
          VThroCode = new Vector();
          VPortCode = new Vector();
          VPortName = new Vector();

          try
          {
               Statement theStatement    = theConnection.createStatement();
               ResultSet res1 = theStatement.executeQuery("Select ToName,ToCode From BookTo Order By ToName");
               while(res1.next())
               {
                     VTo.addElement(res1.getString(1));
                     VToCode.addElement(res1.getString(2));
               }
               res1.close();

               ResultSet res2 = theStatement.executeQuery("Select ThroName,ThroCode From BookThro Order By ThroName");
               while(res2.next())
               {
                     VThro.addElement(res2.getString(1));
                     VThroCode.addElement(res2.getString(2));
               }
               res2.close();

               ResultSet res3 = theStatement.executeQuery("Select PortName,PortCode From Port Order By PortName");
               while(res3.next())
               {
                     VPortName.addElement(res3.getString(1));
                     VPortCode.addElement(res3.getString(2));
               }
               res3.close();

               ResultSet res4 = theStatement.executeQuery("Select ToName From BookTo Where DivCode="+iMillCode);
               while(res4.next())
               {
                     SToName = common.parseNull(res4.getString(1));
               }
               res4.close();

               ResultSet res5 = theStatement.executeQuery("Select ThroName From BookThro Where DivCode="+iMillCode);
               while(res5.next())
               {
                     SThroName = common.parseNull(res5.getString(1));
               }
               res5.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     public boolean isValidNew()
     {
          boolean bFlag = true;

          try
          {
                FW = new FileWriter(common.getPrintPath()+"Error.prn");
                write("*-----------------------*\n");
                write("*    W A R N I N G      *\n");
                write("*-----------------------*\n\n");
          }
          catch(Exception ex){}

          // Checking for Supplier
          String str = common.parseNull(TSupCode.getText());
          if(str.length()==0)
          {
             write("Supplier Is Not Selected \n\n");
             bFlag = false;
          }

         //  Checking for Table Items 
         Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
         for(int i=0;i<FinalData.length;i++)
         {
                    double dQty     = common.toDouble(((String)FinalData[i][5]).trim());
                    double dRate    = common.toDouble(((String)FinalData[i][6]));
                    String SDept    = ((String)FinalData[i][17]).trim();
                    String SGroup   = ((String)FinalData[i][18]).trim();
                    String SUnit    = ((String)FinalData[i][20]).trim();
                    String SDueDate = ((String)FinalData[i][19]).trim();
                    String SBlock   = ((String)FinalData[i][3]).trim();
                    String SDate    = TDate.toString();

                    if(dQty <= 0)
                    {
                      write("Quantity for Item No "+(i+1)+" is not fed \n");
                      bFlag = false;
                    }
                    if(dRate <= 0)
                    {
                      write("Rate for Item No "+(i+1)+" is not fed \n");
                      bFlag = false;
                    }
                    if(SDept.length() == 0)
                    {
                      write("Department for Item No "+(i+1)+" is not Selected \n");
                      bFlag = false;
                    }
                    if(SGroup.length() == 0)
                    {
                      write("Group for Item No "+(i+1)+" is not Selected \n");
                      bFlag = false;
                    }
                    if(SUnit.length() == 0)
                    {
                      write("Unit for Item No "+(i+1)+" is not Selected \n");
                      bFlag = false;
                    }

                    if(SBlock.length() == 0)
                    {
                      write("Block for Item No "+(i+1)+" is not Selected \n");
                      bFlag = false;
                    }

                    if(SDueDate.length() != 10 || common.toInt(common.getDateDiff(SDueDate,SDate)) < 0)
                    {
                      write("DueDate for Item No "+(i+1)+" is wrongly Entered \n");
                      bFlag = false;
                    }
         }

         try
         {
            write("*-----------------------*\n");
            FW.close();
         }
         catch(Exception ex){}

         return bFlag;
     }
     public void write(String str)
     {
          try
          {
                   FW.write(str);
          }
          catch(Exception ex){}
     }

     public void getNextOrderNo()
     {
          String SOrderNo= "";
          String QS      = "";

          QS = " Select maxno From Config"+iMillCode+""+SYearCode+" where Id=1";

          try
          {

               Statement stat      = theConnection. createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         SOrderNo  = common.parseNull((String)result.getString(1));
                         result    . close();
                         stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
          SOrderNo  = String.valueOf(common.toInt(SOrderNo.trim())+1);
          TOrderNo  . setText(SOrderNo.trim());
     }

     public String getInsertOrderNo()
     {
          String SOrderNo= "";
          String QS      = "";

          QS = " Select (maxno+1) From Config"+iMillCode+""+SYearCode+" where Id=1 for update of MaxNo noWait";

          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);

               Statement stat      = theConnection   . createStatement();

               PreparedStatement thePrepare = theConnection.prepareStatement(" Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 1");

               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         SOrderNo  = common.parseNull((String)result.getString(1));
                         result    . close();

               thePrepare.setInt(1,common.toInt(SOrderNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getInsertOrderNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
          TOrderNo  . setText(SOrderNo.trim());
           
          return TOrderNo.getText();
     }

     public boolean isStationary(String SItemCode)
     {
          String SStkGroupName     = "";
          String QS                = "";

          if(iMillCode==0)
          {
               QS =    " select InvItems.stkgroupcode,stockgroup.groupname,invitems.item_code from invitems"+
                       " inner join stockgroup on stockgroup.groupcode = invitems.stkgroupcode"+
                       " where invitems.item_code = '"+SItemCode+"'";
          }
          else
          {
               QS =    " select "+SItemTable+".stkgroupcode,stockgroup.groupname,"+SItemTable+".item_code from "+SItemTable+""+
                       " inner join stockgroup on stockgroup.groupcode = "+SItemTable+".stkgroupcode"+
                       " where "+SItemTable+".item_code = '"+SItemCode+"'";
          }
          try
          {

               if(theConnection.getAutoCommit())
                         theConnection.setAutoCommit(false);

               Statement      stat          = theConnection.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result.next())
               {
                    SStkGroupName  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }

          if(SStkGroupName.equals("B01"))
               return true;

          return false;
     }

     public void UpdateInvItems()
     {
          String QS = "";
          Statement stat      = null;

          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
          try
          {

               for(int i=0;i<FinalData.length;i++)
               {
                    QS = "";

                    String SItem_Code    = common.parseNull(((String)FinalData[i][0]).trim());

                    if(isStationary(SItem_Code))
                    {
                         int SSlipNo = common.toInt(common.parseNull((String)MiddlePanel.MiddlePanel.VPSlipToNo.elementAt(i)));
                         int SBookNo = common.toInt(common.parseNull((String)MiddlePanel.MiddlePanel.VPBookToNo.elementAt(i)));
               
                         if(iMillCode==0)
                         {
                              QS = " Update Invitems set LASTSLIPNO ='"+SSlipNo+"',LASTBOOKNO ='"+SBookNo+"' "+
                                   " where invitems.item_code = '"+SItem_Code+"'";
                         }
                         else
                         {
                              QS = " Update "+SItemTable+" set LASTSLIPNO ='"+SSlipNo+"',LASTBOOKNO ='"+SBookNo+"' "+
                                   " where "+SItemTable+".item_code = '"+SItem_Code+"'";
                         }

                         if(theConnection.getAutoCommit())
                                   theConnection.setAutoCommit(false);
          
                         stat =    theConnection. createStatement();

                         stat      . executeUpdate(QS);
                    }
               }
               if(stat!=null)
                    stat.close();
          }catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }


}
