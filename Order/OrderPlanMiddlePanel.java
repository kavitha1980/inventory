package Order;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class OrderPlanMiddlePanel extends JPanel 
{

      // The Table Filled Component 
      PlanMiddlePanel MiddlePanel;
      Object RowData[][];

      Common common=new Common();

      // Counterparts of Parameters
      JLayeredPane DeskTop;
      Vector VCode,VName;
      Vector VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedDue,VSelectedDesc,VSelectedBlock;

      Vector VSelectedColor,VSelectedSet,VSelectedSize,VSelectedSide,VSelectedSlipNo,VSelectedBookNo,VSelectedSlipFrNo,VSelectedCatl,VSelectedDraw,VSelectedMake;
      Vector VSelectedMRSSlNo,VSelectedUserCode,VSelectedMrsType;

      OrderRec orderrec;
      int iMillCode;
      String SItemTable;

      // Constructor for Manual Mode 
      public OrderPlanMiddlePanel(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VSelectedCode,Vector VSelectedName,Vector VSelectedMRS,Vector VSelectedQty,Vector VSelectedUnit,Vector VSelectedDept,Vector VSelectedGroup,Vector VSelectedDue,Vector VSelectedDesc,int iMillCode,Vector VSelectedBlock,Vector VSelectedColor,Vector VSelectedSet,Vector VSelectedSize,Vector VSelectedSide,Vector VSelectedSlipNo,Vector VSelectedBookNo,Vector VSelectedSlipFrNo,Vector VSelectedCatl,Vector VSelectedDraw,Vector VSelectedMake,String SItemTable,Vector VSelectedMRSSlNo,Vector VSelectedUserCode,Vector VSelectedMrsType)
      {
              this.DeskTop           = DeskTop;
              this.VCode             = VCode;
              this.VName             = VName;
              this.VSelectedCode     = VSelectedCode;
              this.VSelectedName     = VSelectedName;
              this.VSelectedMRS      = VSelectedMRS;
              this.VSelectedQty      = VSelectedQty;
              this.VSelectedUnit     = VSelectedUnit;
              this.VSelectedDept     = VSelectedDept;
              this.VSelectedGroup    = VSelectedGroup;
              this.VSelectedDue      = VSelectedDue;
              this.VSelectedDesc     = VSelectedDesc;
              this.iMillCode         = iMillCode;
              this.VSelectedBlock    = VSelectedBlock;
              this.VSelectedColor    = VSelectedColor;
              this.VSelectedSet      = VSelectedSet;
              this.VSelectedSize     = VSelectedSize;
              this.VSelectedSide     = VSelectedSide;
              this.VSelectedSlipNo   = VSelectedSlipNo;
              this.VSelectedBookNo   = VSelectedBookNo;
              this.VSelectedSlipFrNo = VSelectedSlipFrNo;
              this.VSelectedCatl     = VSelectedCatl;
              this.VSelectedDraw     = VSelectedDraw;
              this.VSelectedMake     = VSelectedMake;
              this.SItemTable        = SItemTable;
              this.VSelectedMRSSlNo  = VSelectedMRSSlNo;
              this.VSelectedUserCode = VSelectedUserCode;
              this.VSelectedMrsType  = VSelectedMrsType;

              setLayout(new BorderLayout());
              setRowData(VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedDue,VSelectedBlock);
              createComponents();

              MiddlePanel.VDesc.removeAllElements();
              MiddlePanel.VCatl.removeAllElements();
              MiddlePanel.VDraw.removeAllElements();
              MiddlePanel.VMake.removeAllElements();

              MiddlePanel.VPColour.removeAllElements();
              MiddlePanel.VPSet.removeAllElements();
              MiddlePanel.VPSize.removeAllElements();
              MiddlePanel.VPSide.removeAllElements();

              MiddlePanel.VPSlipFrNo.removeAllElements();
              MiddlePanel.VPSlipToNo.removeAllElements();
              MiddlePanel.VPBookFrNo.removeAllElements();
              MiddlePanel.VPBookToNo.removeAllElements();

              MiddlePanel.VMRSSlNo.removeAllElements();
              MiddlePanel.VMRSUserCode.removeAllElements();
              MiddlePanel.VMRSType.removeAllElements();
              MiddlePanel.VPOrderNo.removeAllElements();
              MiddlePanel.VPSource.removeAllElements();
              MiddlePanel.VPBlockCode.removeAllElements();

              for(int i=0;i<VSelectedDesc.size();i++)
              {
                    MiddlePanel.VDesc.addElement((String)VSelectedDesc.elementAt(i));
                    MiddlePanel.VCatl.addElement((String)VSelectedCatl.elementAt(i));
                    MiddlePanel.VDraw.addElement((String)VSelectedDraw.elementAt(i));
                    MiddlePanel.VMake.addElement((String)VSelectedMake.elementAt(i));

                    MiddlePanel.VPColour.addElement((String)VSelectedColor.elementAt(i));
                    MiddlePanel.VPSet.addElement((String)VSelectedSet.elementAt(i));
                    MiddlePanel.VPSize.addElement((String)VSelectedSize.elementAt(i));
                    MiddlePanel.VPSide.addElement((String)VSelectedSide.elementAt(i));

                    MiddlePanel.VPSlipFrNo.addElement((String)VSelectedSlipFrNo.elementAt(i));
                    MiddlePanel.VPSlipToNo.addElement((String)VSelectedSlipNo.elementAt(i));
                    MiddlePanel.VPBookFrNo.addElement((String)VSelectedBookNo.elementAt(i));
                    MiddlePanel.VPBookToNo.addElement((String)VSelectedBookNo.elementAt(i));

                    MiddlePanel.VMRSSlNo.addElement((String)VSelectedMRSSlNo.elementAt(i));
                    MiddlePanel.VMRSUserCode.addElement((String)VSelectedUserCode.elementAt(i));
                    MiddlePanel.VMRSType.addElement((String)VSelectedMrsType.elementAt(i));
                    MiddlePanel.VPOrderNo.addElement("0");
                    MiddlePanel.VPSource.addElement("0");
                    MiddlePanel.VPBlockCode.addElement("0");
              }
      }
      // Constructor for Auto Mode
      public OrderPlanMiddlePanel(JLayeredPane DeskTop,Vector VCode,Vector VName,OrderRec orderrec,int iMillCode,Vector VSelectedColor,Vector VSelectedSet,Vector VSelectedSize,Vector VSelectedSide,Vector VSelectedSlipNo,Vector VSelectedBookNo,Vector VSelectedSlipFrNo,Vector VSelectedCatl,Vector VSelectedDraw,Vector VSelectedMake,String SItemTable)
      {
              this.DeskTop           = DeskTop;
              this.VCode             = VCode;
              this.VName             = VName;
              this.orderrec          = orderrec;
              this.iMillCode         = iMillCode;
              this.VSelectedColor    = VSelectedColor;
              this.VSelectedSet      = VSelectedSet;
              this.VSelectedSize     = VSelectedSize;
              this.VSelectedSide     = VSelectedSide;
              this.VSelectedSlipNo   = VSelectedSlipNo;
              this.VSelectedBookNo   = VSelectedBookNo;
              this.VSelectedSlipFrNo = VSelectedSlipFrNo;
              this.VSelectedCatl     = VSelectedCatl;
              this.VSelectedDraw     = VSelectedDraw;
              this.VSelectedMake     = VSelectedMake;
              this.SItemTable        = SItemTable;

              setLayout(new BorderLayout());
              setRowData();
              createComponents();

              MiddlePanel.VDesc.removeAllElements();
              MiddlePanel.VMake.removeAllElements();
              MiddlePanel.VCatl.removeAllElements();
              MiddlePanel.VDraw.removeAllElements();

              MiddlePanel.VPColour.removeAllElements();
              MiddlePanel.VPSet.removeAllElements();
              MiddlePanel.VPSize.removeAllElements();
              MiddlePanel.VPSide.removeAllElements();
              MiddlePanel.VPSlipFrNo.removeAllElements();
              MiddlePanel.VPSlipToNo.removeAllElements();
              MiddlePanel.VPBookFrNo.removeAllElements();
              MiddlePanel.VPBookToNo.removeAllElements();

              MiddlePanel.VMRSSlNo.removeAllElements();
              MiddlePanel.VMRSUserCode.removeAllElements();
              MiddlePanel.VMRSType.removeAllElements();
              MiddlePanel.VPOrderNo.removeAllElements();
              MiddlePanel.VPSource.removeAllElements();
              MiddlePanel.VPBlockCode.removeAllElements();

              for(int i=0;i<orderrec.VDescr.size();i++)
              {
                    MiddlePanel.VDesc.addElement((String)orderrec.VDescr.elementAt(i));
                    MiddlePanel.VMake.addElement((String)orderrec.VMake.elementAt(i));
                    MiddlePanel.VCatl.addElement((String)orderrec.VCatl.elementAt(i));
                    MiddlePanel.VDraw.addElement((String)orderrec.VDraw.elementAt(i));

                    MiddlePanel.VPColour.addElement((String)orderrec.VPColor.elementAt(i));
                    MiddlePanel.VPSet.addElement((String)orderrec.VPSet.elementAt(i));
                    MiddlePanel.VPSize.addElement((String)orderrec.VPSize.elementAt(i));
                    MiddlePanel.VPSide.addElement((String)orderrec.VPSide.elementAt(i));

                    MiddlePanel.VPSlipFrNo.addElement((String)orderrec.VPSlipFrNo.elementAt(i));
                    MiddlePanel.VPSlipToNo.addElement((String)orderrec.VPSlipToNo.elementAt(i));
                    MiddlePanel.VPBookFrNo.addElement((String)orderrec.VPBookFrNo.elementAt(i));
                    MiddlePanel.VPBookToNo.addElement((String)orderrec.VPBookToNo.elementAt(i));

                    MiddlePanel.VMRSSlNo.addElement((String)orderrec.VMRSSlNo.elementAt(i));
                    MiddlePanel.VMRSUserCode.addElement((String)orderrec.VMRSUserCode.elementAt(i));
                    MiddlePanel.VMRSType.addElement((String)orderrec.VMRSType.elementAt(i));
                    MiddlePanel.VPOrderNo.addElement((String)orderrec.VOrdNo.elementAt(i));
                    MiddlePanel.VPSource.addElement("1");
                    MiddlePanel.VPBlockCode.addElement((String)orderrec.VBlockCode.elementAt(i));
              }
      }
      public void createComponents()
      {
               MiddlePanel           = new PlanMiddlePanel(DeskTop,RowData,iMillCode,SItemTable);
               add(MiddlePanel,BorderLayout.CENTER);
      }

      // Invoked when Manual Mode is set
      public void setRowData(Vector VSelectedCode,Vector VSelectedName,Vector VSelectedMRS,Vector VSelectedQty,Vector VSelectedUnit,Vector VSelectedDept,Vector VSelectedGroup,Vector VSelectedDue,Vector VSelectedBlock)
      {
         RowData     = new Object[VSelectedCode.size()][22];
         for(int i=0;i<VSelectedCode.size();i++)
         {
                RowData[i][0] = common.parseNull((String)VSelectedCode.elementAt(i));
                RowData[i][1] = common.parseNull((String)VSelectedName.elementAt(i));
                RowData[i][2] = " ";
                //RowData[i][3] = common.parseNull((String)VSelectedBlock.elementAt(i));
                RowData[i][3] = " ";
                RowData[i][4] = common.parseNull((String)VSelectedMRS.elementAt(i));                 
                RowData[i][5] = common.parseNull((String)VSelectedQty.elementAt(i));
                RowData[i][6] = " ";
                RowData[i][7] = " ";
                RowData[i][8] = " ";
                RowData[i][9] = " ";
                RowData[i][10] = " ";
                RowData[i][11] = " ";
                RowData[i][12] = " ";
                RowData[i][13] = " ";
                RowData[i][14] = " ";
                RowData[i][15] = " ";
                RowData[i][16] = " ";
                RowData[i][17] = common.parseNull((String)VSelectedDept.elementAt(i));
                RowData[i][18] = common.parseNull((String)VSelectedGroup.elementAt(i));
                RowData[i][19] = common.parseNull((String)VSelectedDue.elementAt(i));
                RowData[i][20] = common.parseNull((String)VSelectedUnit.elementAt(i));
                RowData[i][21] = " ";
         }
      }

      // Invoked when Auto Mode is set
      public void setRowData()
      {
         RowData     = new Object[orderrec.VCode.size()][22];
         for(int i=0;i<orderrec.VCode.size();i++)
         {
                RowData[i][0] = common.parseNull((String)orderrec.VCode.elementAt(i));
                RowData[i][1] = common.parseNull((String)orderrec.VName.elementAt(i));
                RowData[i][2] = "";
                //RowData[i][3] = common.parseNull((String)orderrec.VBlock.elementAt(i));
                RowData[i][3] = " ";
                RowData[i][4] = common.parseNull((String)orderrec.VMRS.elementAt(i));                 
                RowData[i][5] = common.parseNull((String)orderrec.VQty.elementAt(i));
                RowData[i][6] = common.parseNull((String)orderrec.VRate.elementAt(i));
                RowData[i][7] = common.parseNull((String)orderrec.VDiscPer.elementAt(i));
                RowData[i][8] = common.parseNull((String)orderrec.VCenVatPer.elementAt(i));
                RowData[i][9] = common.parseNull((String)orderrec.VTaxPer.elementAt(i));
                RowData[i][10] = common.parseNull((String)orderrec.VSurPer.elementAt(i));
                RowData[i][11] = " ";
                RowData[i][12] = " ";
                RowData[i][13] = " ";
                RowData[i][14] = " ";
                RowData[i][15] = " ";
                RowData[i][16] = " ";
                RowData[i][17] = common.parseNull((String)orderrec.VDept.elementAt(i));
                RowData[i][18] = common.parseNull((String)orderrec.VGroup.elementAt(i));
                RowData[i][19] = common.parseNull((String)orderrec.VDue.elementAt(i));
                RowData[i][20] = common.parseNull((String)orderrec.VUnit.elementAt(i));
                RowData[i][21] = common.parseNull((String)orderrec.VDocId.elementAt(i));
         }
      }
}
