package Order;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class OrderPlanMiddlePanelGst extends JPanel 
{

      // The Table Filled Component 
      PlanMiddlePanelGst MiddlePanel;
      Object RowData[][];

      Common common=new Common();

      // Counterparts of Parameters
      JLayeredPane DeskTop;
      Vector VCode,VName;
      Vector VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedDue,VSelectedDesc,VSelectedBlock;

      Vector VSelectedColor,VSelectedSet,VSelectedSize,VSelectedSide,VSelectedSlipNo,VSelectedBookNo,VSelectedSlipFrNo,VSelectedCatl,VSelectedDraw,VSelectedMake;
      Vector VSelectedMRSSlNo,VSelectedUserCode,VSelectedMrsType,VSelectedHsnCode,VSelectedHsnType;
      JTextField TSupData;

      OrderRecGst orderrecgst;
      int iMillCode;
      String SItemTable;

      // Constructor for Manual Mode 
      public OrderPlanMiddlePanelGst(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VSelectedCode,Vector VSelectedName,Vector VSelectedMRS,Vector VSelectedQty,Vector VSelectedUnit,Vector VSelectedDept,Vector VSelectedGroup,Vector VSelectedDue,Vector VSelectedDesc,int iMillCode,Vector VSelectedBlock,Vector VSelectedColor,Vector VSelectedSet,Vector VSelectedSize,Vector VSelectedSide,Vector VSelectedSlipNo,Vector VSelectedBookNo,Vector VSelectedSlipFrNo,Vector VSelectedCatl,Vector VSelectedDraw,Vector VSelectedMake,String SItemTable,Vector VSelectedMRSSlNo,Vector VSelectedUserCode,Vector VSelectedMrsType,Vector VSelectedHsnCode,Vector VSelectedHsnType,JTextField TSupData)
      {
              this.DeskTop           = DeskTop;
              this.VCode             = VCode;
              this.VName             = VName;
              this.VSelectedCode     = VSelectedCode;
              this.VSelectedName     = VSelectedName;
              this.VSelectedMRS      = VSelectedMRS;
              this.VSelectedQty      = VSelectedQty;
              this.VSelectedUnit     = VSelectedUnit;
              this.VSelectedDept     = VSelectedDept;
              this.VSelectedGroup    = VSelectedGroup;
              this.VSelectedDue      = VSelectedDue;
              this.VSelectedDesc     = VSelectedDesc;
              this.iMillCode         = iMillCode;
              this.VSelectedBlock    = VSelectedBlock;
              this.VSelectedColor    = VSelectedColor;
              this.VSelectedSet      = VSelectedSet;
              this.VSelectedSize     = VSelectedSize;
              this.VSelectedSide     = VSelectedSide;
              this.VSelectedSlipNo   = VSelectedSlipNo;
              this.VSelectedBookNo   = VSelectedBookNo;
              this.VSelectedSlipFrNo = VSelectedSlipFrNo;
              this.VSelectedCatl     = VSelectedCatl;
              this.VSelectedDraw     = VSelectedDraw;
              this.VSelectedMake     = VSelectedMake;
              this.SItemTable        = SItemTable;
              this.VSelectedMRSSlNo  = VSelectedMRSSlNo;
              this.VSelectedUserCode = VSelectedUserCode;
              this.VSelectedMrsType  = VSelectedMrsType;
	      this.VSelectedHsnCode  = VSelectedHsnCode;
	      this.VSelectedHsnType  = VSelectedHsnType;
	      this.TSupData          = TSupData;

              setLayout(new BorderLayout());
              setRowData(VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedDue,VSelectedBlock);
              createComponents();

              MiddlePanel.VDesc.removeAllElements();
              MiddlePanel.VCatl.removeAllElements();
              MiddlePanel.VDraw.removeAllElements();
              MiddlePanel.VMake.removeAllElements();

              MiddlePanel.VPColour.removeAllElements();
              MiddlePanel.VPSet.removeAllElements();
              MiddlePanel.VPSize.removeAllElements();
              MiddlePanel.VPSide.removeAllElements();

              MiddlePanel.VPSlipFrNo.removeAllElements();
              MiddlePanel.VPSlipToNo.removeAllElements();
              MiddlePanel.VPBookFrNo.removeAllElements();
              MiddlePanel.VPBookToNo.removeAllElements();

              MiddlePanel.VMRSSlNo.removeAllElements();
              MiddlePanel.VMRSUserCode.removeAllElements();
              MiddlePanel.VMRSType.removeAllElements();
              MiddlePanel.VPOrderNo.removeAllElements();
              MiddlePanel.VPSource.removeAllElements();
              MiddlePanel.VPBlockCode.removeAllElements();
              MiddlePanel.VHsnType.removeAllElements();
              MiddlePanel.VRateStatus.removeAllElements();

              for(int i=0;i<VSelectedDesc.size();i++)
              {
                    MiddlePanel.VDesc.addElement((String)VSelectedDesc.elementAt(i));
                    MiddlePanel.VCatl.addElement((String)VSelectedCatl.elementAt(i));
                    MiddlePanel.VDraw.addElement((String)VSelectedDraw.elementAt(i));
                    MiddlePanel.VMake.addElement((String)VSelectedMake.elementAt(i));

                    MiddlePanel.VPColour.addElement((String)VSelectedColor.elementAt(i));
                    MiddlePanel.VPSet.addElement((String)VSelectedSet.elementAt(i));
                    MiddlePanel.VPSize.addElement((String)VSelectedSize.elementAt(i));
                    MiddlePanel.VPSide.addElement((String)VSelectedSide.elementAt(i));

                    MiddlePanel.VPSlipFrNo.addElement((String)VSelectedSlipFrNo.elementAt(i));
                    MiddlePanel.VPSlipToNo.addElement((String)VSelectedSlipNo.elementAt(i));
                    MiddlePanel.VPBookFrNo.addElement((String)VSelectedBookNo.elementAt(i));
                    MiddlePanel.VPBookToNo.addElement((String)VSelectedBookNo.elementAt(i));

                    MiddlePanel.VMRSSlNo.addElement((String)VSelectedMRSSlNo.elementAt(i));
                    MiddlePanel.VMRSUserCode.addElement((String)VSelectedUserCode.elementAt(i));
                    MiddlePanel.VMRSType.addElement((String)VSelectedMrsType.elementAt(i));
                    MiddlePanel.VPOrderNo.addElement("0");
                    MiddlePanel.VPSource.addElement("0");
                    MiddlePanel.VPBlockCode.addElement("0");
                    MiddlePanel.VHsnType.addElement((String)VSelectedHsnType.elementAt(i));

		    String SHsnCode = (String)VSelectedHsnCode.elementAt(i);
		    if((SHsnCode.equals("") || SHsnCode.equals("0")))
		    {
                         MiddlePanel.VRateStatus.addElement("1");
		    }
		    else
		    {
                         MiddlePanel.VRateStatus.addElement("0");
		    }
              }
      }
      // Constructor for Auto Mode
      public OrderPlanMiddlePanelGst(JLayeredPane DeskTop,Vector VCode,Vector VName,OrderRecGst orderrecgst,int iMillCode,Vector VSelectedColor,Vector VSelectedSet,Vector VSelectedSize,Vector VSelectedSide,Vector VSelectedSlipNo,Vector VSelectedBookNo,Vector VSelectedSlipFrNo,Vector VSelectedCatl,Vector VSelectedDraw,Vector VSelectedMake,String SItemTable,Vector VSelectedHsnCode,Vector VSelectedHsnType,JTextField TSupData)
      {
              this.DeskTop           = DeskTop;
              this.VCode             = VCode;
              this.VName             = VName;
              this.orderrecgst       = orderrecgst;
              this.iMillCode         = iMillCode;
              this.VSelectedColor    = VSelectedColor;
              this.VSelectedSet      = VSelectedSet;
              this.VSelectedSize     = VSelectedSize;
              this.VSelectedSide     = VSelectedSide;
              this.VSelectedSlipNo   = VSelectedSlipNo;
              this.VSelectedBookNo   = VSelectedBookNo;
              this.VSelectedSlipFrNo = VSelectedSlipFrNo;
              this.VSelectedCatl     = VSelectedCatl;
              this.VSelectedDraw     = VSelectedDraw;
              this.VSelectedMake     = VSelectedMake;
              this.SItemTable        = SItemTable;
	      this.VSelectedHsnCode  = VSelectedHsnCode;
	      this.VSelectedHsnType  = VSelectedHsnType;
	      this.TSupData          = TSupData;

              setLayout(new BorderLayout());
              setRowData();
              createComponents();

              MiddlePanel.VDesc.removeAllElements();
              MiddlePanel.VMake.removeAllElements();
              MiddlePanel.VCatl.removeAllElements();
              MiddlePanel.VDraw.removeAllElements();

              MiddlePanel.VPColour.removeAllElements();
              MiddlePanel.VPSet.removeAllElements();
              MiddlePanel.VPSize.removeAllElements();
              MiddlePanel.VPSide.removeAllElements();
              MiddlePanel.VPSlipFrNo.removeAllElements();
              MiddlePanel.VPSlipToNo.removeAllElements();
              MiddlePanel.VPBookFrNo.removeAllElements();
              MiddlePanel.VPBookToNo.removeAllElements();

              MiddlePanel.VMRSSlNo.removeAllElements();
              MiddlePanel.VMRSUserCode.removeAllElements();
              MiddlePanel.VMRSType.removeAllElements();
              MiddlePanel.VPOrderNo.removeAllElements();
              MiddlePanel.VPSource.removeAllElements();
              MiddlePanel.VPBlockCode.removeAllElements();
              MiddlePanel.VHsnType.removeAllElements();
              MiddlePanel.VRateStatus.removeAllElements();

              for(int i=0;i<orderrecgst.VDescr.size();i++)
              {
                    MiddlePanel.VDesc.addElement((String)orderrecgst.VDescr.elementAt(i));
                    MiddlePanel.VMake.addElement((String)orderrecgst.VMake.elementAt(i));
                    MiddlePanel.VCatl.addElement((String)orderrecgst.VCatl.elementAt(i));
                    MiddlePanel.VDraw.addElement((String)orderrecgst.VDraw.elementAt(i));

                    MiddlePanel.VPColour.addElement((String)orderrecgst.VPColor.elementAt(i));
                    MiddlePanel.VPSet.addElement((String)orderrecgst.VPSet.elementAt(i));
                    MiddlePanel.VPSize.addElement((String)orderrecgst.VPSize.elementAt(i));
                    MiddlePanel.VPSide.addElement((String)orderrecgst.VPSide.elementAt(i));

                    MiddlePanel.VPSlipFrNo.addElement((String)orderrecgst.VPSlipFrNo.elementAt(i));
                    MiddlePanel.VPSlipToNo.addElement((String)orderrecgst.VPSlipToNo.elementAt(i));
                    MiddlePanel.VPBookFrNo.addElement((String)orderrecgst.VPBookFrNo.elementAt(i));
                    MiddlePanel.VPBookToNo.addElement((String)orderrecgst.VPBookToNo.elementAt(i));

                    MiddlePanel.VMRSSlNo.addElement((String)orderrecgst.VMRSSlNo.elementAt(i));
                    MiddlePanel.VMRSUserCode.addElement((String)orderrecgst.VMRSUserCode.elementAt(i));
                    MiddlePanel.VMRSType.addElement((String)orderrecgst.VMRSType.elementAt(i));
                    MiddlePanel.VPOrderNo.addElement((String)orderrecgst.VOrdNo.elementAt(i));
                    MiddlePanel.VPSource.addElement("1");
                    MiddlePanel.VPBlockCode.addElement((String)orderrecgst.VBlockCode.elementAt(i));
                    MiddlePanel.VHsnType.addElement((String)orderrecgst.VHType.elementAt(i));

		    String SHsnCode = (String)orderrecgst.VHCode.elementAt(i);
		    if((SHsnCode.equals("") || SHsnCode.equals("0")))
		    {
                         MiddlePanel.VRateStatus.addElement("1");
		    }
		    else
		    {
                         MiddlePanel.VRateStatus.addElement("0");
		    }
              }
      }
      public void createComponents()
      {
               MiddlePanel           = new PlanMiddlePanelGst(DeskTop,RowData,iMillCode,SItemTable,TSupData);
               add(MiddlePanel,BorderLayout.CENTER);
      }

      // Invoked when Manual Mode is set
      public void setRowData(Vector VSelectedCode,Vector VSelectedName,Vector VSelectedMRS,Vector VSelectedQty,Vector VSelectedUnit,Vector VSelectedDept,Vector VSelectedGroup,Vector VSelectedDue,Vector VSelectedBlock)
      {
         RowData     = new Object[VSelectedCode.size()][24];
         for(int i=0;i<VSelectedCode.size();i++)
         {
                RowData[i][0] = common.parseNull((String)VSelectedCode.elementAt(i));
                RowData[i][1] = common.parseNull((String)VSelectedName.elementAt(i));
                RowData[i][2] = common.parseNull((String)VSelectedHsnCode.elementAt(i));
                //RowData[i][3] = common.parseNull((String)VSelectedBlock.elementAt(i));
                RowData[i][3] = " ";
                RowData[i][4] = common.parseNull((String)VSelectedMRS.elementAt(i));                 
                RowData[i][5] = common.parseNull((String)VSelectedQty.elementAt(i));
                RowData[i][6] = " ";
                RowData[i][7] = " ";
                RowData[i][8] = " ";
                RowData[i][9] = " ";
                RowData[i][10] = " ";
                RowData[i][11] = " ";
                RowData[i][12] = " ";
                RowData[i][13] = " ";
                RowData[i][14] = " ";
                RowData[i][15] = " ";
                RowData[i][16] = " ";
                RowData[i][17] = " ";
                RowData[i][18] = " ";
                RowData[i][19] = common.parseNull((String)VSelectedDept.elementAt(i));
                RowData[i][20] = common.parseNull((String)VSelectedGroup.elementAt(i));
                RowData[i][21] = common.parseNull((String)VSelectedDue.elementAt(i));
                RowData[i][22] = common.parseNull((String)VSelectedUnit.elementAt(i));
                RowData[i][23] = " ";
         }
      }

      // Invoked when Auto Mode is set
      public void setRowData()
      {
         RowData     = new Object[orderrecgst.VCode.size()][24];
         for(int i=0;i<orderrecgst.VCode.size();i++)
         {
		String SHCode = common.parseNull((String)orderrecgst.VHCode.elementAt(i));
		String SGstStatus = common.parseNull((String)orderrecgst.VGstStatus.elementAt(i));

                RowData[i][0] = common.parseNull((String)orderrecgst.VCode.elementAt(i));
                RowData[i][1] = common.parseNull((String)orderrecgst.VName.elementAt(i));
                RowData[i][2] = common.parseNull((String)orderrecgst.VHCode.elementAt(i));
                //RowData[i][3] = common.parseNull((String)orderrecgst.VBlock.elementAt(i));
                RowData[i][3] = " ";
                RowData[i][4] = common.parseNull((String)orderrecgst.VMRS.elementAt(i));                 
                RowData[i][5] = common.parseNull((String)orderrecgst.VQty.elementAt(i));
                RowData[i][6] = common.parseNull((String)orderrecgst.VRate.elementAt(i));
                RowData[i][7] = common.parseNull((String)orderrecgst.VDiscPer.elementAt(i));

		if((SHCode.equals("") || SHCode.equals("0")) && SGstStatus.equals("0"))
		{
                     RowData[i][8]  = common.parseNull((String)orderrecgst.VCGST.elementAt(i));
                     RowData[i][9]  = common.parseNull((String)orderrecgst.VSGST.elementAt(i));
                     RowData[i][10] = common.parseNull((String)orderrecgst.VIGST.elementAt(i));
                     RowData[i][11] = common.parseNull((String)orderrecgst.VCess.elementAt(i));
		}
		else
		{
                     RowData[i][8] = " ";
                     RowData[i][9] = " ";
                     RowData[i][10] = " ";
                     RowData[i][11] = " ";
		}

                RowData[i][12] = " ";
                RowData[i][13] = " ";
                RowData[i][14] = " ";
                RowData[i][15] = " ";
                RowData[i][16] = " ";
                RowData[i][17] = " ";
                RowData[i][18] = " ";
                RowData[i][19] = common.parseNull((String)orderrecgst.VDept.elementAt(i));
                RowData[i][20] = common.parseNull((String)orderrecgst.VGroup.elementAt(i));
                RowData[i][21] = common.parseNull((String)orderrecgst.VDue.elementAt(i));
                RowData[i][22] = common.parseNull((String)orderrecgst.VUnit.elementAt(i));
                RowData[i][23] = common.parseNull((String)orderrecgst.VDocId.elementAt(i));
         }
      }
}
