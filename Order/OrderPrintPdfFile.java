package Order;

import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class OrderPrintPdfFile
{
     Vector         VCode,VName,VUOM,VQty,VRate;
     Vector         VDiscPer,VCenVatPer,VTaxPer,VSurPer;
     Vector         VBasic,VDisc,VCenVat,VTax,VSur,VNet;
     Vector         VDesc,VMake,VCatl,VDraw,VBlock;
     Vector         VPColour,VPSet,VPSize,VPSide;
     Vector         VSlipFromNo,VSlipToNo,VBookFromNo,VBookToNo;
     Common         common    = new Common();
     FileWriter     FW;

     String         SOrdNo    = "",SOrdDate  = "",SSupCode  = "",SSupName  = "";
     String         SAddr1    = "",SAddr2    = "",SAddr3    = "";
     String         SEMail="",SFaxNo="";
     String         SToName   = "",SThroName = "";
     String         SPayTerm       = "";
     String         SReference     = "";
     String         SDueDate       = "";
     String         SOthers        = "";
     String         SMRSNo         = "";
     String         SMRSDate       = "";
     String         SPort          = "";
     
     double         dOthers   = 0;
     double         dTDisc    = 0,dTCenVat   = 0,dTTax      = 0,dTSur = 0,dTNet = 0;

     int            Lctr      = 100;
     int            Pctr      = 0;
     int            iEPCG          = 0;
     int            iOrderType     = 0;
     int            iProject       = 0;
     int            iState         = 0;
     int            iAmend         = 0;

     String         SSupplierEMail,SSupplierMobile;
     String         SContact;

     String SFile;
     String SItemTable;
     int    iMillCode;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font tinyBold   = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
     private static Font tinyNormal = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

     
     String SHead[]  = {"Mat Code","Block","Description","UOM","Quantity","Rate","Discount","Cenvat","Tax","Net"};
     String SHead2[] = {"","","Make - Catalogue - Drawing Number","","","Rs","Rs","Rs","Rs","Rs"};
     int    iWidth[] = {8,5,30,6,9,8,7,8,8,11};

     Document document;
     PdfPTable table;
        String SPdfFile= "/root/PurchaseOrder.pdf";

     OrderPrintPdfFile(String SFile,String SOrdNo,String SOrdDate,String SSupCode,String SSupName,String SItemTable,int iMillCode)
     {
          this.SFile      = SFile;
          this.SOrdNo     = SOrdNo;
          this.SOrdDate   = SOrdDate;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.SItemTable = SItemTable;
          this.iMillCode  = iMillCode;

          getAddr();
          setDataIntoVector();
          createPdfFile();
     }
     
     public void setDataIntoVector()
     {
          VCode          = new Vector();
          VName          = new Vector();
          VBlock         = new Vector();
          VUOM           = new Vector();
          VQty           = new Vector();
          VRate          = new Vector();
          VDiscPer       = new Vector();
          VCenVatPer     = new Vector();
          VTaxPer        = new Vector();
          VSurPer        = new Vector();
          VBasic         = new Vector();
          VDisc          = new Vector();
          VCenVat        = new Vector();
          VTax           = new Vector();
          VSur           = new Vector();
          VNet           = new Vector();
          VDesc          = new Vector();
          VMake          = new Vector();
          VCatl          = new Vector();
          VDraw          = new Vector();
          VPColour       = new Vector();
          VPSet          = new Vector();
          VPSize         = new Vector();
          VPSide         = new Vector();
          VSlipFromNo    = new Vector();
          VSlipToNo      = new Vector();
          VBookFromNo    = new Vector();
          VBookToNo      = new Vector();

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();

               String         QString        = getQString();
               ResultSet      res            = stat.executeQuery(QString);

               while (res.next())
               {
                    String str1    = res.getString(1);  
                    String str2    = res.getString(2);
                    String str3    = res.getString(3);
                    String str4    = res.getString(4);
                    String str5    = res.getString(5);
                    String str6    = res.getString(6);
                    String str7    = res.getString(7);
                    String str8    = res.getString(8);
                    String str9    = res.getString(9);
                    String str10   = res.getString(10);
                    String str11   = res.getString(11);
                    String str12   = res.getString(12);
                    String str13   = res.getString(13);

                    SToName        = res.getString(14);
                    SThroName      = res.getString(15);
                    String str14   = res.getString(16);
                    String str15   = res.getString(17);
                    String str16   = res.getString(18);
                    String str17   = res.getString(19);
                    String str18   = res.getString(20);
                    SPayTerm       = res.getString(21);
                    SDueDate       = common.parseDate(res.getString(22));
                    dOthers        = common.toDouble(res.getString(23));
                    SReference     = res.getString(24);

                    SMRSNo         = res.getString(25);
                    iEPCG          = res.getInt(26);
                    iOrderType     = res.getInt(27);
                    iProject       = res.getInt(28);
                    iState         = res.getInt(29);
                    SPort          = res.getString(30);
                    iAmend         = res.getInt(31);
                    String SBName  = res.getString(32);
                    
                    VBlock         . addElement(SBName);
                    VCode          . addElement(str1);
                    VName          . addElement(str2);
                    VQty           . addElement(str3);
                    VRate          . addElement(str4);
                    VDiscPer       . addElement(str5);
                    VDisc          . addElement(str6);
                    VCenVatPer     . addElement(str7);
                    VCenVat        . addElement(str8);
                    VTaxPer        . addElement(str9);
                    VTax           . addElement(str10);
                    VNet           . addElement(str11);
                    VSurPer        . addElement(str12);
                    VSur           . addElement(str13);
                    VUOM           . addElement(str14);
                    VDesc          . addElement(str15);
                    VMake          . addElement(str16);
                    VCatl          . addElement(str17);
                    VDraw          . addElement(str18);

                    VPColour       . addElement(common.parseNull(res.getString(33)));
                    VPSet          . addElement(common.parseNull(res.getString(34)));
                    VPSize         . addElement(common.parseNull(res.getString(35)));
                    VPSide         . addElement(common.parseNull(res.getString(36)));
                    VSlipFromNo    . addElement(common.parseNull(res.getString(37)));
                    VSlipToNo      . addElement(common.parseNull(res.getString(38)));
                    VBookFromNo    . addElement(common.parseNull(res.getString(39)));
                    VBookToNo      . addElement(common.parseNull(res.getString(40)));
               }
               res  . close();

               ResultSet res1 = stat.executeQuery("Select MRSDate From MRS Where MRSNo="+SMRSNo+" and (MillCode=0 or MillCode is Null)");
               while(res1.next())
                    SMRSDate = common.parseDate(res1.getString(1));

               res1 . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);ex.printStackTrace();}
     }

     public String getQString()
     {
          String QString  = "";
          
          QString = " SELECT PurchaseOrder.Item_Code, InvItems.Item_Name, PurchaseOrder.qty,"+
                    " PurchaseOrder.Rate, PurchaseOrder.DiscPer, PurchaseOrder.Disc,"+
                    " PurchaseOrder.CenVatPer, PurchaseOrder.Cenvat, PurchaseOrder.TaxPer,"+
                    " PurchaseOrder.Tax, PurchaseOrder.Net,PurchaseOrder.SurPer,PurchaseOrder.Sur,"+
                    " BookTo.ToName,BookThro.ThroName,UoM.UoMName,MatDesc.Descr,MatDesc.Make,"+
                    " InvItems.Catl,InvItems.Draw,PurchaseOrder.PayTerms,PurchaseOrder.DueDate,"+
                    " (PurchaseOrder.Plus-PurchaseOrder.Less) as Others,PurchaseOrder.Reference,"+
                    " PurchaseOrder.MRSNo,PurchaseOrder.EPCG,PurchaseOrder.OrderType,"+
                    " PurchaseOrder.Project_order,PurchaseOrder.State,Port.PortName,PurchaseOrder.amended,OrdBlock.BlockName,"+
                    " MatDesc.PAPERCOLOR,PaperSet.SETNAME,MatDesc.PAPERSIZE,PaperSide.SIDENAME,"+
                    " MatDesc.SLIPFROMNO,MatDesc.SLIPTONO,MatDesc.BOOKFROMNO,MatDesc.BOOKTONO "+
                    " FROM ((((PurchaseOrder inner join port on purchaseorder.portcode=port.portcode)INNER JOIN InvItems ON PurchaseOrder.Item_Code = InvItems.Item_Code) "+
                    " Inner Join UoM on UoM.UoMCode = InvItems.UomCode"+
                    " Inner Join OrdBlock on OrdBlock.Block = PurchaseOrder.OrderBlock"+
                    " Inner Join BookTo   On BookTo.ToCode = PurchaseOrder.ToCode) "+
                    " Inner Join BookThro On BookThro.ThroCode = PurchaseOrder.ThroCode) "+
                    " Left Join MatDesc On MatDesc.OrderNo = PurchaseOrder.OrderNo and MatDesc.OrderBlock = PurchaseOrder.OrderBlock and MatDesc.Item_Code = PurchaseOrder.Item_Code And MatDesc.SlNo = PurchaseOrder.SlNo "+
                    " inner join paperset on paperset.setcode = Matdesc.papersets inner join paperside on paperside.sidecode = Matdesc.paperside"+
                    " Where PurchaseOrder.Qty > 0 And PurchaseOrder.OrderNo = "+SOrdNo+" and PurchaseOrder.millcode = 0"+
                    " Order By 1";

          return QString;
     }
     
     public void getAddr()
     {
          String QS =    " Select Supplier.Addr1,Supplier.Addr2,Supplier.Addr3,Supplier.EMail,Supplier.Fax,Place.PlaceName,Supplier.Contact "+
                         " From Supplier LEFT Join Place On Place.PlaceCode = Supplier.City_Code "+
                         " Where Supplier.Ac_Code = '"+SSupCode+"'";

          SSupplierEMail      = "";
          SSupplierMobile     = "";
          SContact            = "";

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               ResultSet      res            = stat.executeQuery(QS);

               while (res.next())
               {
                    SAddr1    = res.getString(1);
                    SAddr2    = res.getString(2);
                    SAddr3    = common.parseNull(res.getString(3));
                    SEMail    = common.parseNull(res.getString(4));
                    SFaxNo    = common.parseNull(res.getString(5));
                    SContact  = common.parseNull(res.getString(7));
               }

               res  . close();

               ResultSet rs                  = stat.executeQuery("Select EMail,PurMobileNo from PartyMaster where PartyCode = '"+SSupCode+"' ");
               if(rs.next())
               {
                    SSupplierEMail           = common.parseNull(rs.getString(1));
                    SSupplierMobile          = common.parseNull(rs.getString(2));
               }
               rs                            . close();

               stat . close();
          }
          catch(Exception ex){System.out.println(ex);ex.printStackTrace();}
     }

     public boolean isStationary(String SItemCode)
     {
          String SStkGroupName     = "";
          String QS                = "";

          QS =    " select stockgroup.groupname,invitems.item_code from invitems"+
                  " inner join stockgroup on stockgroup.groupcode = invitems.stkgroupcode"+
                  " where invitems.item_code = '"+SItemCode+"'";
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnect    =  oraConnection.getConnection();               
               Statement      stat          = theConnect.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result.next())
               {
                    SStkGroupName  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }
          if(SStkGroupName.equals("STATIONARY"))
               return true;

          return false;
     }

     public void createPdfFile()
     {
          try
          {
               document = new Document(PageSize.A4);
               PdfWriter.getInstance(document, new FileOutputStream(SFile));
               document.open();

               new OrderPdfClass(document,table,SFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);


            /*   table = new PdfPTable(10);
               table.setWidths(iWidth);
               table.setWidthPercentage(100);

               addHead(document,table);
               addBody(document,table);
               addFoot(document,table,0);*/

               document.close();
          }
          catch (Exception e)
          {
               e.printStackTrace();
          }
     }

     private void addHead(Document document,PdfPTable table) throws BadElementException
     {
          try
          {
               if(Lctr < 42)
                    return;
               
               if(Pctr > 0)
                    addFoot(document,table,1);
             

               Pctr++;

               document.newPage();

               table.flushContent();

               table = new PdfPTable(10);
               table.setWidths(iWidth);
               table.setWidthPercentage(100);


               String SState      = "";
               String SEpcg       = "";
               String SOrderTitle = "";
     
               if (iState == 1)
               {
                    SState = " Intra State";
               }
               else if (iState == 2)
               {
                    SState = " Inter State";
               }
     
               if (iAmend == 1)
               {     
                    if (iEPCG == 1 && iOrderType == 0)
                    {
                         SEpcg       = "EPCG";
                         SOrderTitle = "AM PURCHASE ORDER";
                    }
                    else if (iEPCG == 1 && iOrderType == 1)
                    {
                         SState      = "IMPORT";
                         SEpcg       = "EPCG";
                         SOrderTitle = "AM PURCHASE ORDER"+common.Pad(SPort,10);
                    }
                    else if (iEPCG == 0 && iOrderType == 1)
                    {
                         SState      = "IMPORT";
                         SOrderTitle = "AM PURCHASE ORDER"+common.Pad(SPort,10);
                    }
                    else
                    {
                         SOrderTitle = "AM PURCHASE ORDER";
                    }
               }
               else
               {
                    if (iEPCG == 1 && iOrderType == 0)
                    {
                         SEpcg       = "EPCG";
                         SOrderTitle = "PURCHASE ORDER";
                    }
                    else if (iEPCG == 1 && iOrderType == 1)
                    {
                         SState      = "IMPORT";
                         SEpcg       = "EPCG";
                         SOrderTitle = "PURCHASE ORDER"+common.Pad(SPort,10);
                    }
                    else if (iEPCG == 0 && iOrderType == 1)
                    {
                         SState      = "IMPORT";
                         SOrderTitle = "PURCHASE ORDER"+common.Pad(SPort,10);
                    }
                    else
                    {
                         SOrderTitle = "PURCHASE ORDER";
                    }
               }


               PdfPCell c1;

               String Str1   = "AMARJOTHI SPINNING MILLS LIMITED";
               String Str2   = "PUDUSURIPALAYAM";
               String Str3   = "NAMBIYUR  -  638 458";
               String Str4   = "";
               String Str5   = "E-Mail: purchase@amarjothi.net,mill@amarjothi.net";
               String Str6   = "Phones               : 04285 - 267201,267301, Fax - 04285-267565";
               String Str7   = "Our C.S.T. No. : 440691 dt. 24.09.1990  ";
               String Str8   = "TIN No.             : 33632960864 ";
               String Str9   = "To,";
               String Str10  = "M/s. "+SSupName;
               String Str11  = SAddr1;
               String Str12  = SAddr2;
               String Str13  = SAddr3;
               String Str14  = common.Pad("Fax No: "+SFaxNo,25)+common.Pad("EMail: "+SSupplierEMail,38);

               String Str1a  = "";
               String Str2a  = "PLA No    : 59/93";
               String Str3a  = "Division    : Erode";
               String Str4a  = SState;
               String Str6a  = "NO";
               String Str8a  = "Book To";
               String Str10a = "Book Thro";
               String Str12a = "Reference";

               
               String Str1b  = "ECC No                  :  AAFCA 7082C XM 001";
               String Str2b  = "Range                     :  Gobichettipalayam";
               String Str3b  = "Commissionerate  : Salem";
               String Str4b  = SEpcg;
               String Str6b  = common.Pad(SOrdNo,27);
               String Str8b  = common.Pad(SToName,55);
               String Str10b = common.Pad(SThroName,55);
               String Str12b = common.Pad(SReference,55);

               String Str4c  = SOrderTitle;
               String Str6c  = "Date";
               String Str6d  = common.Pad(SOrdDate,10);
               String Str6e  = "Page";
               String Str6f  = common.Pad(""+Pctr,3);



               c1 = new PdfPCell(new Phrase(Str1,mediumBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.TOP | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str1a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str1b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.TOP | Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str2,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str2a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(2);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str2b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str3,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str3a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(2);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str3b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);



               c1 = new PdfPCell(new Phrase(Str4,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str4a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str4b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str4c,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str5,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str6,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);
               
               c1 = new PdfPCell(new Phrase(Str6c,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6d,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6e,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6f,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str7,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str8,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str8a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str8b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str9,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

     
               c1 = new PdfPCell(new Phrase(Str10,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str10a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str10b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str11,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str12,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str12a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(3);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str12b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(3);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str13,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str14,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
               table.addCell(c1);


               for(int i=0;i<SHead.length;i++)
               {
                    c1 = new PdfPCell(new Phrase(SHead[i],smallBold));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                    table.addCell(c1);
               }

               for(int i=0;i<SHead2.length;i++)
               {
                    c1 = new PdfPCell(new Phrase(SHead2[i],smallBold));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                    table.addCell(c1);
               }

               document.add(table);
             
               Lctr = 16;
          }    
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void addBody(Document document,PdfPTable table) throws BadElementException
     {
          try
          {
               int iCount=0;

               PdfPCell c1;

               for(int i=0;i<VCode.size();i++)
               {
                    addHead(document,table);

                    String SCode        = (String)VCode   . elementAt(i);
                    String SUOM         = (String)VUOM    . elementAt(i);
                    String SName        = (String)VName   . elementAt(i);
                    String SBkName      = (String)VBlock  . elementAt(i);
     
                    String SQty         = common.getRound((String)VQty        . elementAt(i),2);
                    String SRate        = common.getRound((String)VRate       . elementAt(i),2);
                    String SDisc        = common.getRound((String)VDisc       . elementAt(i),2);
                    String SDiscPer     = common.getRound((String)VDiscPer    . elementAt(i),2)+"%";
                    String SCenVat      = common.getRound((String)VCenVat     . elementAt(i),2);
                    String SCenVatPer   = common.getRound((String)VCenVatPer  . elementAt(i),2)+"%";
                    String STax         = common.getRound((String)VTax        . elementAt(i),2);
                    String STaxPer      = common.getRound((String)VTaxPer     . elementAt(i),2)+"%";
                    String SSur         = common.getRound((String)VSur        . elementAt(i),2);
                    String SSurPer      = common.getRound((String)VSurPer     . elementAt(i),2)+"%";
                    String SNet         = common.getRound((String)VNet        . elementAt(i),2);
                    String SDesc        = common.parseNull((String)VDesc      . elementAt(i));
                    String SMake        = common.parseNull((String)VMake      . elementAt(i));
                    String SCatl        = common.parseNull((String)VCatl      . elementAt(i));
                    String SDraw        = common.parseNull((String)VDraw      . elementAt(i));
     
                    String SPColour     = common.parseNull((String)VPColour   . elementAt(i));
                    String SPSets       = common.parseNull((String)VPSet      . elementAt(i));
                    String SPSize       = common.parseNull((String)VPSize     . elementAt(i));
                    String SPSide       = common.parseNull((String)VPSide     . elementAt(i));
     
                    String SSlipFromNo  = common.parseNull((String)VSlipFromNo. elementAt(i));
                    String SSlipToNo    = common.parseNull((String)VSlipToNo  . elementAt(i));
                    String SBookFromNo  = common.parseNull((String)VBookFromNo. elementAt(i));
                    String SBookToNo    = common.parseNull((String)VBookToNo  . elementAt(i));
     
                    SDesc          = SDesc        . trim();
                    SMake          = SMake        . trim();
                    SPColour       = SPColour     . trim();
                    SPSets         = SPSets       . trim();
                    SPSize         = SPSize       . trim();
                    SPSide         = SPSide       . trim();
                    SSlipFromNo    = SSlipFromNo  . trim();
                    SSlipToNo      = SSlipToNo    . trim();
                    SBookFromNo    = SBookFromNo  . trim();
                    SBookToNo      = SBookToNo    . trim();
     
                    dTDisc   = dTDisc   + common.toDouble(SDisc);
                    dTCenVat = dTCenVat + common.toDouble(SCenVat);
                    dTTax    = dTTax    + common.toDouble(STax);
                    dTSur    = dTSur    + common.toDouble(SSur);
                    dTNet    = dTNet    + common.toDouble(SNet);
                    
                    try
                    {
                         c1 = new PdfPCell(new Phrase(common.Pad(SCode,9),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SBkName,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);
                    
                         c1 = new PdfPCell(new Phrase(common.Pad(SName,37),tinyNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.Pad(SUOM,5),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SQty,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SRate,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SDisc,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SCenVat,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(STax,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SNet,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         Lctr = Lctr+1;

                         Vector vect = common.getLines(SDesc+"` ");
                         
                         for(int j=0;j<vect.size();j++)
                         {
                              if(j==0)
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   c1 = new PdfPCell(new Phrase(common.Pad((String)vect.elementAt(j),37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   c1 = new PdfPCell(new Phrase(SDiscPer,smallNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   c1 = new PdfPCell(new Phrase(SCenVatPer,smallNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   c1 = new PdfPCell(new Phrase(STaxPer,smallNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   addEmptyCell(table,"Left","Right");

                                   Lctr = Lctr+1;
                              }
                              else
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   c1 = new PdfPCell(new Phrase(common.Pad((String)vect.elementAt(j),37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   Lctr = Lctr+1;
                              }
                         }
                         
                         if(SMake.length() > 0)
                         {
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              c1 = new PdfPCell(new Phrase(common.Pad(SMake,37),tinyNormal));
                              c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                              c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                              table.addCell(c1);

                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              Lctr = Lctr+1;
                         }                         
                         
                         if((SCatl.trim()).length() > 0)
                         {
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              c1 = new PdfPCell(new Phrase(common.Pad("Catl   : "+SCatl,37),tinyNormal));
                              c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                              c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                              table.addCell(c1);

                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              Lctr = Lctr+1;
                         }                         
                         
                         if((SDraw.trim()).length() > 0)
                         {
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              c1 = new PdfPCell(new Phrase(common.Pad("Drawing: "+SDraw,37),tinyNormal));
                              c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                              c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                              table.addCell(c1);

                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              Lctr = Lctr+1;
                         }
     
                         String    SSPCoSets = "Colour : "+common.Pad(SPColour,12);
                                   SSPCoSets+= "Sets: "+SPSets;
               
                         String    SSPSis    = "Size   : "+common.Pad(SPSize,12);
                                   SSPSis   += "Side: "+SPSide;
     
                         String    SSPBooks  = "Book No: "+SBookFromNo+" To "+SBookToNo;
                         String    SSPSlip   = "Slip No: "+SSlipFromNo+" To "+SSlipToNo;
     
                         if(isStationary(SCode))
                         {
                              if((SPColour.trim()).length() > 0 || ((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0))
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
     
                                   c1 = new PdfPCell(new Phrase(common.Pad(SSPCoSets,37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);
     
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   Lctr++;
                              }
          
                              if(((SPColour.trim()).length() > 0) || ((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0))
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
     
                                   c1 = new PdfPCell(new Phrase(common.Pad(SSPSis,37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);
     
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   Lctr++;
                              }
     
                              if((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0)
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
     
                                   c1 = new PdfPCell(new Phrase(common.Pad(SSPSlip,37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);
     
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   Lctr++;
                              }
     
                              if((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0)
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
     
                                   c1 = new PdfPCell(new Phrase(common.Pad(SSPBooks,37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);
     
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   Lctr++;
                              }
                         }
                    }
                    catch(Exception ex){}
               }
          }    
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void addFoot(Document document,PdfPTable table,int iSig) throws BadElementException
     {
          try
          {
               PdfPCell c1;

               String SDisc   = common.getRound(dTDisc,2);
               String SCenVat = common.getRound(dTCenVat,2);
               String STax    = common.getRound(dTTax,2);
               String SSur    = common.getRound(dTSur,2);
               String SNet    = common.getRound(dTNet,2);
               String SOthers = common.getRound(dOthers,2);
               String SGrand  = common.getRound(dTNet+dOthers,2);
               
               float f = 16.0f;

               addEmptyCell(table);
               addEmptyCell(table);

               c1 = new PdfPCell(new Phrase(" T O T A L ",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setFixedHeight(f);
               table.addCell(c1);

               addEmptyCell(table);
               addEmptyCell(table);
               addEmptyCell(table);

               c1 = new PdfPCell(new Phrase(SDisc,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SCenVat,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(STax,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SNet,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);


               
               if(iSig==0)
               {
                    c1 = new PdfPCell(new Phrase("Date of Delivery",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setFixedHeight(f);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase("Terms of Payments",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase("MRS No",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase("MRS Date",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase("Freight & Others",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase("Order Value",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
     
     
                    c1 = new PdfPCell(new Phrase(SDueDate,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setFixedHeight(f);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase(SPayTerm,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase(SMRSNo,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase(SMRSDate,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase(SOthers,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase(SGrand,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
               }

               String Str1  = " Note  :   1. Please mention our order No. and material code in all correspondence with us.";
               String Str2  = common.Space(14)+" 2. Please mention our TIN Number in your invoices.";
               String Str3  = common.Space(14)+" 3. Please send  invoices in triplicate. Original invoice to be sent direct to accounts manager.";
               String Str4  = common.Space(14)+" 4. Please send copy of your invoice along with consignment.";
               String Str5  = common.Space(14)+" 5. We reserve the right to increase / decrease the order qty. / cancel the order without assigning any reason";
               String Str6  = common.Space(14)+" 6. Any legal dispute shall be within the jurisdiction of Tirupur.";

               String Str7  = common.Space(14)+" 7. These materials should be supplied against EPCG Scheme  for which please send us your Proforma Invoice along with the catalogue immediately so that we can open the EPCG Licence.";
               String Str8  = common.Space(14)+" 8. Please quote the EPCG License No in your all Bills.";

               String Str9  = "For AMARJOTHI SPINNING MILLS LTD";
               String Str10 = common.Space(10)+"Prepared By"+common.Space(50)+"Checked By"+common.Space(50)+"I.A."+common.Space(50)+"Authorised Signatory";
               String Str11 = " Special Instruction :-";

               String Str12 = " Regd. Office - 'AMARJOTHI HOUSE' 157, Kumaran Road, Tirupur - 638 601. Phone : 0421- 2201980 to 2201984";
               String Str13 = " ";

               String Str14 = "I N D I A 'S   F I N E S T  M E L A N G E  Y A R N  P R O D U C E R S";


	       String SGstNote1 = " GST Note : "; 
	       String SGstNote2 = common.Space(15)+" ITC shall be eligible only on the basis of Credit available in GSTN Portal. The Supplier shall be responsible for proper filing of GST Return in the  ";
	       String SGstNote3 = common.Space(15)+" GSTN  Portal.  If for any reason,  the GST as indicated in the Invoice is not reflected in the  GSTN  Portal,  the tax component  shall be debited to ";
	       String SGstNote4 = common.Space(15)+" your account with interest. ";



               c1 = new PdfPCell(new Phrase(Str1,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str2,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str3,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str4,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str5,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               if (iEPCG == 1)
               {
                    c1 = new PdfPCell(new Phrase(Str7,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    c1.setColspan(10);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(Str8,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    c1.setColspan(10);
                    table.addCell(c1);
               }

               addEmptySpanRow(table,"LEFT","RIGHT");

               c1 = new PdfPCell(new Phrase(SGstNote1,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SGstNote2,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SGstNote3,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SGstNote4,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               addEmptySpanRow(table,"LEFT","RIGHT");

               c1 = new PdfPCell(new Phrase(Str9,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               addEmptySpanRow(table,"LEFT","RIGHT");
               addEmptySpanRow(table,"LEFT","RIGHT");
               addEmptySpanRow(table,"LEFT","RIGHT");

               c1 = new PdfPCell(new Phrase(Str10,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str11,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(10);
               c1.setFixedHeight(f);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str12,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str13,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str14,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(10);
               c1.setFixedHeight(f);
               table.addCell(c1);

               document.add(table);


               String Str15="";

               if(iSig==0)
                    Str15 = "<End Of Report>";
               else
                    Str15 = "(Continued on Next Page)";


               Paragraph paragraph = new Paragraph(Str15,smallNormal);
               document.add(paragraph);

          }    
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void addEmptyCell(PdfPTable table)
     {
          PdfPCell c1 = new PdfPCell();
          table.addCell(c1);
     }

     private void addEmptyCell(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
          table.addCell(c1);
     }

     private void addEmptyRow(PdfPTable table)
     {
          for(int i=0;i<SHead.length;i++)
          {
               PdfPCell c1 = new PdfPCell();
               table.addCell(c1);
          }
     }

     private void addEmptySpanRow(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
          c1.setColspan(SHead.length);
          table.addCell(c1);
     }

     private static void addEmptyLine(Paragraph paragraph, int number)
     {
          for (int i = 0; i < number; i++)
          {
               paragraph.add(new Paragraph(" "));
          }
     }


}
