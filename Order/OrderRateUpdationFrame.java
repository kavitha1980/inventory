package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class OrderRateUpdationFrame extends JInternalFrame
{
     JPanel    TopPanel;
     JPanel    BottomPanel;
     JPanel    MiddlePanel;

     JPanel    OldPanel,NewPanel;
     
     JTextField TOrderNo;
     MyComboBox JCCode;
     JButton    BApply1,BApply2,BOk,BExit;

     FractionNumberField  TRate,TDiscPer,TCenvatPer,TTaxPer,TSurPer;

     MyLabel    LQty,LRate,LDiscPer,LDisc,LCenvatPer,LCenvat,LTaxPer,LTax,LSurPer,LSur,LNet;
     MyLabel    LQtyNew,LDiscNew,LCenvatNew,LTaxNew,LSurNew,LNetNew;

     Vector VItemCode,VItemSlNo,VCodeSlNo,VBlockCode;
     
     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;
     int iUserCode,iMillCode,iAuthCode;
     String SSupTable,SItemTable;

     String SStDate = "";
     Vector VRecDate,VRecQty,VRecValue,VIssDate,VIssQty,VIssRate,VIssId,VIssOldVal;

     Common common = new Common();
     
     Connection theConnect = null;

     boolean bComflag = true;
     
     public OrderRateUpdationFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode,String SSupTable,String SItemTable)
     {
          try
          {
               this.DeskTop    = DeskTop;
               this.VCode      = VCode;
               this.VName      = VName;
               this.SPanel     = SPanel;
               this.iUserCode  = iUserCode;
               this.iMillCode  = iMillCode;
               this.iAuthCode  = iAuthCode;
               this.SSupTable  = SSupTable;
               this.SItemTable = SItemTable;
               
               getDBConnection();
               createComponents();
               setLayouts();
               addComponents();
               addListeners();
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }

     }

     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnect     =    oraConnection.getConnection();
     }

     public void createComponents()
     {
          try
          {
               TopPanel       = new JPanel();
               BottomPanel    = new JPanel();
               MiddlePanel    = new JPanel();
               OldPanel       = new JPanel();
               NewPanel       = new JPanel();
     
               TOrderNo       = new JTextField();
               JCCode         = new MyComboBox();
               BOk            = new JButton("Update");
               BExit          = new JButton("Exit");
               BApply1        = new JButton("Apply");
               BApply2        = new JButton("Apply");

               TRate          = new FractionNumberField(15,4);
               TDiscPer       = new FractionNumberField(6,2);
               TCenvatPer     = new FractionNumberField(6,2);
               TTaxPer        = new FractionNumberField(6,2);
               TSurPer        = new FractionNumberField(6,2);

               LQty           = new MyLabel("");
               LRate          = new MyLabel("");
               LDiscPer       = new MyLabel("");
               LDisc          = new MyLabel("");
               LCenvatPer     = new MyLabel("");
               LCenvat        = new MyLabel("");
               LTaxPer        = new MyLabel("");
               LTax           = new MyLabel("");
               LSurPer        = new MyLabel("");
               LSur           = new MyLabel("");
               LNet           = new MyLabel("");

               LQtyNew        = new MyLabel("");
               LDiscNew       = new MyLabel("");
               LCenvatNew     = new MyLabel("");
               LTaxNew        = new MyLabel("");
               LSurNew        = new MyLabel("");
               LNetNew        = new MyLabel("");


               JCCode.setEnabled(false);
               BApply2.setEnabled(false);
               BOk.setEnabled(false);
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void setLayouts()
     {

          try
          {
               TopPanel       .setLayout(new GridLayout(1,5,10,10));
               MiddlePanel    .setLayout(new GridLayout(1,2,10,10));
               OldPanel       .setLayout(new GridLayout(8,3,10,10));
               NewPanel       .setLayout(new GridLayout(8,3,10,10));
               BottomPanel.setBorder(new TitledBorder(""));

               OldPanel.setBorder(new TitledBorder("Existing Rate Details"));
               NewPanel.setBorder(new TitledBorder("New Rate Details"));
     
               setTitle("Order RateUpdation Frame");
               setClosable(true);
               setMaximizable(true);
               setIconifiable(true);
               setResizable(true);
               setBounds(0,0,650,500);
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void addComponents()
     {
          try
          {
               TopPanel.add(new JLabel("Order No"));
               TopPanel.add(TOrderNo);
               TopPanel.add(BApply1);
               TopPanel.add(JCCode);
               TopPanel.add(BApply2);

               OldPanel.add(new JLabel("Particulars"));
               OldPanel.add(new JLabel("%"));
               OldPanel.add(new JLabel("Value"));

               OldPanel.add(new JLabel("Qty"));
               OldPanel.add(new JLabel(""));
               OldPanel.add(LQty);

               OldPanel.add(new JLabel("Rate"));
               OldPanel.add(new JLabel(""));
               OldPanel.add(LRate);

               OldPanel.add(new JLabel("Discount"));
               OldPanel.add(LDiscPer);
               OldPanel.add(LDisc);

               OldPanel.add(new JLabel("Cenvat"));
               OldPanel.add(LCenvatPer);
               OldPanel.add(LCenvat);

               OldPanel.add(new JLabel("Tax"));
               OldPanel.add(LTaxPer);
               OldPanel.add(LTax);

               OldPanel.add(new JLabel("Surcharge"));
               OldPanel.add(LSurPer);
               OldPanel.add(LSur);

               OldPanel.add(new JLabel("Net Value"));
               OldPanel.add(new JLabel(""));
               OldPanel.add(LNet);


               NewPanel.add(new JLabel("Particulars"));
               NewPanel.add(new JLabel("%"));
               NewPanel.add(new JLabel("Value"));

               NewPanel.add(new JLabel("Qty"));
               NewPanel.add(new JLabel(""));
               NewPanel.add(LQtyNew);

               NewPanel.add(new JLabel("Rate"));
               NewPanel.add(TRate);
               NewPanel.add(new JLabel(""));

               NewPanel.add(new JLabel("Discount"));
               NewPanel.add(TDiscPer);
               NewPanel.add(LDiscNew);

               NewPanel.add(new JLabel("Cenvat"));
               NewPanel.add(TCenvatPer);
               NewPanel.add(LCenvatNew);

               NewPanel.add(new JLabel("Tax"));
               NewPanel.add(TTaxPer);
               NewPanel.add(LTaxNew);

               NewPanel.add(new JLabel("Surcharge"));
               NewPanel.add(TSurPer);
               NewPanel.add(LSurNew);

               NewPanel.add(new JLabel("Net Value"));
               NewPanel.add(new JLabel(""));
               NewPanel.add(LNetNew);

               MiddlePanel.add(OldPanel);
               MiddlePanel.add(NewPanel);


               BottomPanel.add(BOk);
               BottomPanel.add(BExit);
     
               getContentPane().add(TopPanel,BorderLayout.NORTH);
               getContentPane().add(BottomPanel,BorderLayout.SOUTH);
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void addListeners()
     {
          try
          {
               BApply1.addActionListener(new ApplyList());
               BApply2.addActionListener(new ApplyList());
               BExit.addActionListener(new ApplyList());
               BOk.addActionListener(new ApplyList());
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply1)
               {
                    setItemCode();
               }

               if(ae.getSource()==BApply2)
               {
                    setRateData();
               }

               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
               if(ae.getSource()==BOk)
               {
                    if(validRate())
                    {
                         String SOrderNo   = TOrderNo.getText();
                         String SItemCode  = (String)VItemCode.elementAt(JCCode.getSelectedIndex());
                         String SOrderSlNo = (String)VItemSlNo.elementAt(JCCode.getSelectedIndex());
                         int iBlockCode    = common.toInt((String)VBlockCode.elementAt(JCCode.getSelectedIndex()));

                         updateOrderData(SOrderNo,SItemCode,SOrderSlNo);
                         updateGrnData(SOrderNo,SItemCode,SOrderSlNo,iBlockCode);

                         if(iBlockCode<2)
                         {
                              updateIssueData(SItemCode);
                         }
                         updateItemStock(SItemCode);

                         try
                         {
                              if(bComflag)
                              {
                                   theConnect     . commit();
                                   JOptionPane.showMessageDialog(null,"Order Updated Successfully ","Information",JOptionPane.INFORMATION_MESSAGE);
                                   System         . out.println("Commit");
                                   theConnect     . setAutoCommit(true);
                                   removeHelpFrame();
                              }
                              else
                              {
                                   theConnect     . rollback();
                                   JOptionPane.showMessageDialog(null,"Problem in Order Update. Contact EDP ","Error",JOptionPane.ERROR_MESSAGE);
                                   System         . out.println("RollBack");
                                   theConnect     . setAutoCommit(true);
                              }
                         }catch(Exception ex)
                         {
                              ex.printStackTrace();
                         }
                    }
               }
          }
     }

     public void setItemCode()
     {
          try
          {
               JCCode.removeAllItems();

               VItemCode      = new Vector();
               VItemSlNo      = new Vector();
               VCodeSlNo      = new Vector();
               VBlockCode     = new Vector();

               Statement      stat          =  theConnect.createStatement();

               String SOrderNo = TOrderNo.getText();

               String QString =    " Select Item_Code,SlNo,OrderBlock from PurchaseOrder Where MillCode="+iMillCode+" and OrderNo="+SOrderNo+" Order by 1,2 ";

               ResultSet res  = stat.executeQuery(QString);

               while (res.next())
               {
                    String SCode      = res.getString(1);
                    String SSlNo      = res.getString(2);
                    String SBlockCode = res.getString(3);
                    String SCodeSlNo  = SCode+"~"+SSlNo;

                    VItemCode      .addElement(SCode);
                    VItemSlNo      .addElement(SSlNo);
                    VCodeSlNo      .addElement(SCodeSlNo);
                    VBlockCode     .addElement(SBlockCode);
               }
               res.close();
               stat.close();

               if(VItemCode.size()>0)
               {
                    for(int i=0;i<VItemCode.size();i++)
                    {
                         JCCode.addItem((String)VCodeSlNo.elementAt(i));
                    }

                    TOrderNo.setEditable(false);
                    BApply1.setEnabled(false);
                    JCCode.setEnabled(true);
                    BApply2.setEnabled(true);
               }
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRateData()
     {
          try
          {
               String SQty="",SRate="",SDiscPer="",SDisc="",SCenvatPer="",SCenvat="";
               String STaxPer="",STax="",SSurPer="",SSur="",SNet="";

               String SOrderNo  = TOrderNo.getText();
               String SItemCode = (String)VItemCode.elementAt(JCCode.getSelectedIndex());
               String SSlNo     = (String)VItemSlNo.elementAt(JCCode.getSelectedIndex());


               Statement      stat          =  theConnect.createStatement();


               String QString =    " Select Qty,Rate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,Net "+
                                   " from PurchaseOrder Where MillCode="+iMillCode+" and OrderNo="+SOrderNo+
                                   " and Item_Code='"+SItemCode+"' and SlNo="+SSlNo;

               ResultSet res  = stat.executeQuery(QString);

               while (res.next())
               {
                    SQty         = res.getString(1);
                    SRate        = res.getString(2);
                    SDiscPer     = res.getString(3);
                    SDisc        = res.getString(4);
                    SCenvatPer   = res.getString(5);
                    SCenvat      = res.getString(6);
                    STaxPer      = res.getString(7);
                    STax         = res.getString(8);
                    SSurPer      = res.getString(9);
                    SSur         = res.getString(10);
                    SNet         = res.getString(11);
               }
               res.close();
               stat.close();

               getContentPane().add(MiddlePanel,BorderLayout.CENTER);

               LQty.setText(SQty);
               LRate.setText(SRate);
               LDiscPer.setText(SDiscPer);
               LDisc.setText(SDisc);
               LCenvatPer.setText(SCenvatPer);
               LCenvat.setText(SCenvat);
               LTaxPer.setText(STaxPer);
               LTax.setText(STax);
               LSurPer.setText(SSurPer);
               LSur.setText(SSur);
               LNet.setText(SNet);

               LQtyNew.setText(SQty);
               TRate.setText(SRate);
               TDiscPer.setText(SDiscPer);
               LDiscNew.setText(SDisc);
               TCenvatPer.setText(SCenvatPer);
               LCenvatNew.setText(SCenvat);
               TTaxPer.setText(STaxPer);
               LTaxNew.setText(STax);
               TSurPer.setText(SSurPer);
               LSurNew.setText(SSur);
               LNetNew.setText(SNet);

               JCCode.setEnabled(false);
               BApply2.setEnabled(false);
               BOk.setEnabled(true);

               TRate     .addKeyListener(new CalcList());
               TDiscPer  .addKeyListener(new CalcList());
               TCenvatPer.addKeyListener(new CalcList());
               TTaxPer   .addKeyListener(new CalcList());
               TSurPer   .addKeyListener(new CalcList());

               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public class CalcList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               setCalculation();
          }
     }

     public void setCalculation()
     {
          double N  = 0;

          double DV = 0;
          double CV = 0;
          double SV = 0;
          double UV = 0;

          double Q       = common.toDouble(LQtyNew.getText());
          double R       = common.toDouble(TRate.getText());
          double D       = common.toDouble(TDiscPer.getText());
          double C       = common.toDouble(TCenvatPer.getText());
          double S       = common.toDouble(TTaxPer.getText());
          double U       = common.toDouble(TSurPer.getText());
          double B       = 0;

          try
          {
               B=Q*R;
          }
          catch(Exception ex)
          {
               B = 0;
          }

          DV = B*D/100;
          DV = common.toDouble(common.getRound(DV,2));

          CV = (B-DV)*(C/100);
          CV = common.toDouble(common.getRound(CV,2));

          SV = (B-DV+CV)*(S/100);
          SV = common.toDouble(common.getRound(SV,2));

          UV = SV*U/100;
          UV = common.toDouble(common.getRound(UV,2));

          LDiscNew  .setText(common.getRound(DV,2));
          LCenvatNew.setText(common.getRound(CV,2));
          LTaxNew   .setText(common.getRound(SV,2));
          LSurNew   .setText(common.getRound(UV,2));

          N = B-DV+CV+SV+UV;

          try
          {
               LNetNew.setText(common.getRound(N,2));
          }
          catch(Exception ex)
          {
          }
     }    

     public boolean validRate()
     {
          if(common.toDouble(TRate.getText())<=0)
          {
               JOptionPane.showMessageDialog(null,"Invalid Rate","Error",JOptionPane.ERROR_MESSAGE);
               TRate.requestFocus();
               return false;
          }

          if(common.toDouble(LNetNew.getText())<=0)
          {
               JOptionPane.showMessageDialog(null,"Invalid NetValue","Error",JOptionPane.ERROR_MESSAGE);
               TRate.requestFocus();
               return false;
          }

          if(common.toDouble(TDiscPer.getText())>=100)
          {
               JOptionPane.showMessageDialog(null,"Invalid Discount","Error",JOptionPane.ERROR_MESSAGE);
               TDiscPer.requestFocus();
               return false;
          }

          if(common.toDouble(TCenvatPer.getText())>=100)
          {
               JOptionPane.showMessageDialog(null,"Invalid Cenvat","Error",JOptionPane.ERROR_MESSAGE);
               TCenvatPer.requestFocus();
               return false;
          }

          if(common.toDouble(TTaxPer.getText())>=100)
          {
               JOptionPane.showMessageDialog(null,"Invalid Tax","Error",JOptionPane.ERROR_MESSAGE);
               TTaxPer.requestFocus();
               return false;
          }

          if(common.toDouble(TSurPer.getText())>=100)
          {
               JOptionPane.showMessageDialog(null,"Invalid Surcharge","Error",JOptionPane.ERROR_MESSAGE);
               TSurPer.requestFocus();
               return false;
          }

          return true;
     }

     public void updateOrderData(String SOrderNo,String SItemCode,String SOrderSlNo)
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat = theConnect.createStatement();

               String QS = " Update PurchaseOrder Set "+
                           "Rate=0"+common.getRound(TRate.getText(),4)+","+
                           "DiscPer=0"+common.getRound(TDiscPer.getText(),2)+","+
                           "Disc=0"+common.getRound(LDiscNew.getText(),2)+","+
                           "CenvatPer=0"+common.getRound(TCenvatPer.getText(),2)+","+
                           "Cenvat=0"+common.getRound(LCenvatNew.getText(),2)+","+
                           "TaxPer=0"+common.getRound(TTaxPer.getText(),2)+","+
                           "Tax=0"+common.getRound(LTaxNew.getText(),2)+","+
                           "SurPer=0"+common.getRound(TSurPer.getText(),2)+","+
                           "Sur=0"+common.getRound(LSurNew.getText(),2)+","+
                           "Net=0"+common.getRound(LNetNew.getText(),2)+" "+
                           " Where OrderNo="+SOrderNo+
                           " And Item_Code='"+SItemCode+"'"+
                           " And SlNo="+SOrderSlNo+
                           " And MillCode="+iMillCode;


               stat.executeUpdate(QS);
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     public void updateGrnData(String SOrderNo,String SItemCode,String SOrderSlNo,int iBlockCode)
     {
          try
          {
               int iCount = checkGrn(SOrderNo,SItemCode,SOrderSlNo);

               if(iCount>0)
               {
                    Vector VGrnId     = new Vector();
                    Vector VGrnQty    = new Vector();
                    Vector VGrnOldVal = new Vector();

                    SStDate = "";

                    Statement stat = theConnect.createStatement();


                    String QS1 = " Select Id,GrnQty,GrnValue from Grn "+
                                 " Where OrderNo="+SOrderNo+
                                 " And Code='"+SItemCode+"'"+
                                 " And OrderSlNo="+SOrderSlNo+
                                 " And MillCode="+iMillCode+
                                 " Order by Id ";

                    String QS2 = " Select Min(GrnDate) from Grn "+
                                 " Where OrderNo="+SOrderNo+
                                 " And Code='"+SItemCode+"'"+
                                 " And OrderSlNo="+SOrderSlNo+
                                 " And MillCode="+iMillCode;


                    ResultSet res  = stat.executeQuery(QS1);
     
                    while (res.next())
                    {
                         VGrnId.addElement(common.parseNull(res.getString(1)));
                         VGrnQty.addElement(common.parseNull(res.getString(2)));
                         VGrnOldVal.addElement(common.parseNull(res.getString(3)));
                    }
                    res.close();

                    res  = stat.executeQuery(QS2);
     
                    while (res.next())
                    {
                         SStDate = common.parseNull(res.getString(1));
                    }
                    res.close();

                    double dOrderQty = common.toDouble(LQty.getText());

                    for(int i=0;i<VGrnId.size();i++)
                    {
                         String SGrnId     = (String)VGrnId.elementAt(i);
                         double dGrnQty    = common.toDouble((String)VGrnQty.elementAt(i));
                         double dGrnOldVal = common.toDouble((String)VGrnOldVal.elementAt(i));

                         if(dGrnQty>dOrderQty || dGrnQty<dOrderQty)
                         {
                              double N  = 0;
                    
                              double DV = 0;
                              double CV = 0;
                              double SV = 0;
                              double UV = 0;
                    
                              double Q       = dGrnQty;
                              double R       = common.toDouble(TRate.getText());
                              double D       = common.toDouble(TDiscPer.getText());
                              double C       = common.toDouble(TCenvatPer.getText());
                              double S       = common.toDouble(TTaxPer.getText());
                              double U       = common.toDouble(TSurPer.getText());
                              double B       = 0;
                    
                              try
                              {
                                   B=Q*R;
                              }
                              catch(Exception ex)
                              {
                                   B = 0;
                              }
                    
                              DV = B*D/100;
                              DV = common.toDouble(common.getRound(DV,2));
                    
                              CV = (B-DV)*(C/100);
                              CV = common.toDouble(common.getRound(CV,2));
                    
                              SV = (B-DV+CV)*(S/100);
                              SV = common.toDouble(common.getRound(SV,2));
                    
                              UV = SV*U/100;
                              UV = common.toDouble(common.getRound(UV,2));
                    
                              N = B-DV+CV+SV+UV;


                              String QS3 = " Update Grn Set "+
                                           "InvRate=0"+common.getRound(TRate.getText(),4)+","+
                                           "DiscPer=0"+common.getRound(TDiscPer.getText(),2)+","+
                                           "Disc=0"+common.getRound(DV,2)+","+
                                           "CenvatPer=0"+common.getRound(TCenvatPer.getText(),2)+","+
                                           "Cenvat=0"+common.getRound(CV,2)+","+
                                           "TaxPer=0"+common.getRound(TTaxPer.getText(),2)+","+
                                           "Tax=0"+common.getRound(SV,2)+","+
                                           "SurPer=0"+common.getRound(TSurPer.getText(),2)+","+
                                           "Sur=0"+common.getRound(UV,2)+","+
                                           "InvAmount=0"+common.getRound(N,2)+","+
                                           "InvNet=0"+common.getRound(N,2)+","+
                                           "GrnValue=0"+common.getRound(N,2)+" "+
                                           " Where Id="+SGrnId+
                                           " And Code='"+SItemCode+"'"+
                                           " And OrderSlNo="+SOrderSlNo+
                                           " And MillCode="+iMillCode;
               
               
                              double dGrnNewVal = common.toDouble(common.getRound(N,2));

                              String QS4 = "";

                              if(iBlockCode<2)
                              {
                                   QS4 = " Update "+SItemTable+" Set "+
                                         " RecVal=RecVal-("+dGrnOldVal+")+("+dGrnNewVal+")"+
                                         " Where Item_Code='"+SItemCode+"'";
                              }
                              else
                              {
                                   QS4 = " Update "+SItemTable+" Set "+
                                         " RecVal=RecVal-("+dGrnOldVal+")+("+dGrnNewVal+"),"+
                                         " IssVal=IssVal-("+dGrnOldVal+")+("+dGrnNewVal+")"+
                                         " Where Item_Code='"+SItemCode+"'";
                              }

                              if(theConnect.getAutoCommit())
                                        theConnect     . setAutoCommit(false);

               
                              stat.executeUpdate(QS3);
                              stat.executeUpdate(QS4);
                         }
                         else
                         {
                              String QS3 = " Update Grn Set "+
                                           "InvRate=0"+common.getRound(TRate.getText(),4)+","+
                                           "DiscPer=0"+common.getRound(TDiscPer.getText(),2)+","+
                                           "Disc=0"+common.getRound(LDiscNew.getText(),2)+","+
                                           "CenvatPer=0"+common.getRound(TCenvatPer.getText(),2)+","+
                                           "Cenvat=0"+common.getRound(LCenvatNew.getText(),2)+","+
                                           "TaxPer=0"+common.getRound(TTaxPer.getText(),2)+","+
                                           "Tax=0"+common.getRound(LTaxNew.getText(),2)+","+
                                           "SurPer=0"+common.getRound(TSurPer.getText(),2)+","+
                                           "Sur=0"+common.getRound(LSurNew.getText(),2)+","+
                                           "InvAmount=0"+common.getRound(LNetNew.getText(),2)+","+
                                           "InvNet=0"+common.getRound(LNetNew.getText(),2)+","+
                                           "GrnValue=0"+common.getRound(LNetNew.getText(),2)+" "+
                                           " Where Id="+SGrnId+
                                           " And Code='"+SItemCode+"'"+
                                           " And OrderSlNo="+SOrderSlNo+
                                           " And MillCode="+iMillCode;
               

                              double dGrnNewVal = common.toDouble(common.getRound(LNetNew.getText(),2));

                              String QS4 = "";

                              if(iBlockCode<2)
                              {
                                   QS4 = " Update "+SItemTable+" Set "+
                                         " RecVal=RecVal-("+dGrnOldVal+")+("+dGrnNewVal+")"+
                                         " Where Item_Code='"+SItemCode+"'";
                              }
                              else
                              {
                                   QS4 = " Update "+SItemTable+" Set "+
                                         " RecVal=RecVal-("+dGrnOldVal+")+("+dGrnNewVal+"),"+
                                         " IssVal=IssVal-("+dGrnOldVal+")+("+dGrnNewVal+")"+
                                         " Where Item_Code='"+SItemCode+"'";
                              }

                              if(theConnect.getAutoCommit())
                                        theConnect     . setAutoCommit(false);

                              stat.executeUpdate(QS3);
                              stat.executeUpdate(QS4);
                         }
                    }
                    stat.close();
               }
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     public int checkGrn(String SOrderNo,String SItemCode,String SOrderSlNo)
     {
          int iCount=0;
          try
          {
               String QS =" Select Count(*) from Grn Where OrderNo="+SOrderNo+" and OrderSlNo="+SOrderSlNo+" and Code='"+SItemCode+"' and MillCode="+iMillCode;

               Statement      stat          =  theConnect.createStatement();
               ResultSet      result        =  stat.executeQuery(QS);
               while(result.next())
               {
                    iCount    = result.getInt(1);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
          return iCount;
     }

     public void updateIssueData(String SItemCode)
     {
          try
          {
               String SEnDate = common.getServerPureDate();

               VRecDate   = new Vector();
               VRecQty    = new Vector();
               VRecValue  = new Vector();
               
               VIssDate   = new Vector();
               VIssQty    = new Vector();
               VIssRate   = new Vector();
               VIssId     = new Vector();
               VIssOldVal = new Vector();


               Item1 item = new Item1(SItemCode,SStDate,SEnDate,iMillCode,SItemTable,SSupTable);

               String SOpgQty = item.getOpgStock();
               String SOpgVal = item.getOpgValue();

               Statement stat = theConnect.createStatement();


               String QS1 = " Select IssueDate,Qty,Id,IssueValue From Issue Where IssueDate>="+SStDate+" And Code='"+SItemCode+"' And MillCode="+iMillCode+" Order By 1,3";


               String QS2 = " Select GrnDate,GrnQty,GrnValue From Grn "+
                            " Where (GrnBlock < 2 or GrnType = 2) And GrnDate>="+SStDate+" And Code='"+SItemCode+"' And Grn.MillCode="+iMillCode+
                            " Order By 1 ";

               ResultSet res  = stat.executeQuery(QS1);

               while (res.next())
               {
                    VIssDate.addElement(common.parseNull(res.getString(1)));
                    VIssQty.addElement(common.parseNull(res.getString(2)));
                    VIssRate.addElement("0");
                    VIssId.addElement(common.parseNull(res.getString(3)));
                    VIssOldVal.addElement(common.parseNull(res.getString(4)));
               }
               res.close();

               VRecDate.addElement(SStDate);
               VRecQty.addElement(SOpgQty);
               VRecValue.addElement(SOpgVal);

               res  = stat.executeQuery(QS2);

               while (res.next())
               {
                    VRecDate.addElement(common.parseNull(res.getString(1)));
                    VRecQty.addElement(common.parseNull(res.getString(2)));
                    VRecValue.addElement(common.parseNull(res.getString(3)));
               }
               res.close();


               for(int i=0;i<VIssDate.size();i++)
               {
                    double dIssRate=getIssueRate((String)VIssDate.elementAt(i));
                    VIssRate.setElementAt(common.getRound(dIssRate,4),i);
               }

               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);


               for(int j=0;j<VIssId.size();j++)
               {
                    double dQty  = common.toDouble((String)VIssQty.elementAt(j));
                    double dRate = common.toDouble((String)VIssRate.elementAt(j));
                    double dIssVal = dQty * dRate;

                    String QS3 = " Update Issue Set ";
                    QS3 = QS3+"IssRate = 0"+common.getRound(dRate,4)+",";
                    QS3 = QS3+"IssueValue = 0"+common.getRound(dIssVal,2);
                    QS3 = QS3+" Where Id = "+(String)VIssId.elementAt(j);


                    double dIssOldVal = common.toDouble((String)VIssOldVal.elementAt(j));
                    double dIssNewVal = common.toDouble(common.getRound(dIssVal,2));


                    String QS4 = " Update "+SItemTable+" Set "+
                                 " IssVal=IssVal-("+dIssOldVal+")+("+dIssNewVal+")"+
                                 " Where Item_Code='"+SItemCode+"'";


                    stat.execute(QS3);
                    stat.execute(QS4);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     private double getIssueRate(String SDate) 
     {
          double dRecQty=0,dRecValue=0,dLastRate=0;
          for(int i=0;i<VRecDate.size();i++)
          {
               int iDate = common.toInt((String)VRecDate.elementAt(i));
               if(common.toInt(SDate) < iDate)
                    break;
               dRecQty   = dRecQty+common.toDouble((String)VRecQty.elementAt(i));
               dRecValue = dRecValue+common.toDouble((String)VRecValue.elementAt(i));
               try
               {
                    dLastRate = common.toDouble((String)VRecValue.elementAt(i))/common.toDouble((String)VRecQty.elementAt(i));
               }
               catch(Exception ex)
               {
               }
          }
          for(int i=0;i<VIssDate.size();i++)
          {
               int iDate = common.toInt((String)VIssDate.elementAt(i));
               if(common.toInt(SDate) < iDate)
                    break;
               double dQty   =  common.toDouble((String)VIssQty.elementAt(i));
               double dRate  =  common.toDouble((String)VIssRate.elementAt(i));
               if(dRate == 0)
                    break;
               dRecQty   = dRecQty-dQty;
               dRecValue = dRecValue-(dQty*dRate);
          }

          if(dRecQty == 0)
               return dLastRate;

          return dRecValue/dRecQty;
     }

     private void updateItemStock(String SItemCode)
     {
          try
          {
               Item1 IC = new Item1(SItemCode,iMillCode,SItemTable,SSupTable);
     
               double dAllStock = common.toDouble(IC.getClStock());
               double dAllValue = common.toDouble(IC.getClValue());
     
               double dRate   = 0;
               try
               {
                    dRate = dAllValue/dAllStock;
               }
               catch(Exception ex)
               {
                    dRate=0;
               }
     
               if(dAllStock==0)
               {
                    dRate = common.toDouble(IC.SRate);
               }

               Statement stat = theConnect.createStatement();

               String QS1 = " Update ItemStock set StockValue="+common.getRound(dRate,4)+
                            " Where ItemCode='"+SItemCode+"' and MillCode="+iMillCode;


               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);


               stat.execute(QS1);
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println("E7"+e);
               bComflag  = false;
          }
     }

     public void removeHelpFrame()
     {
           try
           {
              DeskTop.remove(this);
              DeskTop.repaint();
              DeskTop.updateUI();
           }
           catch(Exception ex) { }
     }

}
