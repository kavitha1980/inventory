package Order;

import java.util.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class OrderRecGst
{
     String SSupCode  ="";
     String SSupName  ="";
     String SToCode   ="";
     String SThroCode ="";
     Vector VOrdNo,VBlock,VDate,VBlockCode,VDocId;
     Vector VCode,VRate,VDiscPer;
     Vector VId;
     Vector VDescr,VMake,VCatl,VDraw;
     Vector VCGST,VSGST,VIGST,VCess,VGstStatus;
     String SPayTerm = "";
     String SPayDays = "";

     Vector VName,VMRS,VQty,VUnit,VDept,VGroup,VDue,VMRSId,VMRSBlock,VMRSSlNo,VMRSUserCode,VMRSType;
     Vector VHCode,VHType;
     Control control = new Control();
     Common  common  = new Common();

     Vector VPColor,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo;

     OrderRecGst()
     {
          VId       = new Vector();
          VOrdNo    = new Vector();
          VBlock    = new Vector();
          VBlockCode= new Vector();
          VDate     = new Vector();
          VCode     = new Vector();
          VRate     = new Vector();
          VDiscPer  = new Vector();
          VName     = new Vector();
          VMRS      = new Vector();
          VQty      = new Vector();
          VUnit     = new Vector();
          VDept     = new Vector();
          VGroup    = new Vector();
          VDue      = new Vector();
          VMRSId    = new Vector();
          VDescr    = new Vector();
          VMake     = new Vector();
          VCatl     = new Vector();
          VDraw     = new Vector();
          VMRSBlock = new Vector();
          VMRSSlNo  = new Vector();
          VMRSUserCode = new Vector();
          VMRSType  = new Vector();
	  VHCode    = new Vector();
	  VHType    = new Vector();

          VPColor   = new Vector();
          VPSet     = new Vector();
          VPSize    = new Vector();
          VPSide    = new Vector();
          VPSlipFrNo= new Vector();
          VPSlipToNo= new Vector();
          VPBookFrNo= new Vector();
          VPBookToNo= new Vector();
	  VDocId    = new Vector();	
	  VCGST	    = new Vector();
	  VSGST	    = new Vector();
	  VIGST	    = new Vector();
	  VCess	    = new Vector();
	  VGstStatus= new Vector();
     }

     public void setMiscDetails(Vector VSelectedId,Vector VSelectedName,Vector VSelectedMRS,Vector VSelectedQty,Vector VSelectedUnit,Vector VSelectedDept,Vector VSelectedGroup,Vector VSelectedDue,Vector VSelectedDesc,Vector VSelectedBlock,Vector VSelectedColor,Vector VSelectedSet,Vector VSelectedSize,Vector VSelectedSide,Vector VSelectedSlipNo,Vector VSelectedBookNo,Vector VSelectedSlipFrNo,Vector VSelectedCatl,Vector VSelectedDraw,Vector VSelectedMake,String SSupTable,Vector VSelectedMRSSlNo,Vector VSelectedUserCode,Vector VSelectedMrsType,Vector VSelectedHsnCode,Vector VSelectedHsnType)
     {
          SSupName = control.getID("Select Name From "+SSupTable+" Where Ac_Code = '"+SSupCode+"'");
          
          for(int i=0;i<VId.size();i++)
          {
               int index = common.toInt((String)VId.elementAt(i));
               VName     .addElement(common.parseNull((String)VSelectedName.elementAt(index)));
               VMRS      .addElement(common.parseNull((String)VSelectedMRS.elementAt(index)));
               VQty      .addElement(common.parseNull((String)VSelectedQty.elementAt(index)));
               VUnit     .addElement(common.parseNull((String)VSelectedUnit.elementAt(index)));
               VDept     .addElement(common.parseNull((String)VSelectedDept.elementAt(index)));
               VGroup    .addElement(common.parseNull((String)VSelectedGroup.elementAt(index)));
               VDue      .addElement(common.parseNull((String)VSelectedDue.elementAt(index)));
               VMRSId    .addElement(common.parseNull((String)VSelectedId.elementAt(index)));
               VDescr    .addElement(common.parseNull((String)VSelectedDesc.elementAt(index)));
               VMake     .addElement(common.parseNull((String)VSelectedMake.elementAt(index)));
               VCatl     .addElement(common.parseNull((String)VSelectedCatl.elementAt(index)));
               VDraw     .addElement(common.parseNull((String)VSelectedDraw.elementAt(index)));
               VMRSBlock .addElement(common.parseNull((String)VSelectedBlock.elementAt(index)));
               VMRSSlNo  .addElement(common.parseNull((String)VSelectedMRSSlNo.elementAt(index)));
               VMRSUserCode.addElement(common.parseNull((String)VSelectedUserCode.elementAt(index)));
               VMRSType .addElement(common.parseNull((String)VSelectedMrsType.elementAt(index)));
               VHCode   .addElement(common.parseNull((String)VSelectedHsnCode.elementAt(index)));
               VHType   .addElement(common.parseNull((String)VSelectedHsnType.elementAt(index)));

               VPColor   .addElement(common.parseNull((String)VSelectedColor.elementAt(index)));
               VPSet     .addElement(common.parseNull((String)VSelectedSet.elementAt(index)));
               VPSize    .addElement(common.parseNull((String)VSelectedSize.elementAt(index)));
               VPSide    .addElement(common.parseNull((String)VSelectedSide.elementAt(index)));
               VPSlipFrNo.addElement(common.parseNull((String)VSelectedSlipFrNo.elementAt(index)));
               VPSlipToNo.addElement(common.parseNull((String)VSelectedSlipNo.elementAt(index)));
               VPBookFrNo.addElement(common.parseNull((String)VSelectedBookNo.elementAt(index)));
               VPBookToNo.addElement(common.parseNull((String)VSelectedBookNo.elementAt(index)));
          }
     }

}
