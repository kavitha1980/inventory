
package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class OrderResultSet
{
     NextField      TOrderNo;
     NextField      TPayDays;

     JTextField     TSupCode;
     JTextField     TRef;
     JTextField     TAdvance;
     JTextField     TPayTerm;

     JComboBox      JCTo,JCThro,JCForm,JTClaim;
     JComboBox      JEPCG,JCOrderType,JCProject,JCPort,JCState;

     DateField2     TDate;
     JButton        BSupplier;

     String         SOrderNo       = "";
     String         SOrderDate     = "";
     String         SToCode        = "0";
     String         SThroCode      = "0";
     String         SFormCode      = "0";

     String         SOrderBlock    = "";
     String         SSupName       = "";
     String         SSupCode       = "";
     String         SRef           = "";
     String         SAdvance       = "";
     String         SPayTerm       = "";
     String         SPayDays       = "";
     String         SAdd           = "";
     String         SLess          = "";
     String         SPort          = "";

     int            iBlock         = -1;
     int            iEpcg          = 0;
     int            iOrderType     = 0;
     int            iProject       = 0;
     int            iState         = 0;
     int            iMillCode      = 0;
     int            iOrderStatus   = 0;
     
     Vector         VOCode,VOName,VOMrsNo,VOQty,VORate,VODiscPer,VOCenVatPer,VOTaxPer,VOSurPer,VOrdBlockName;
     Vector         VODeptName,VOGroupName,VOUnitName,VODueDate,VOUserName,VODocId;
     Vector         VOId,VDId;
     Vector         VODesc,VOMake,VODraw,VOCatl,VInvQty;

     Vector         VCode,VName;
     Vector         VToCode,VThroCode,VFormCode,VEpcg,VOrderType,VPort,VState,VProject,VBlockName;
     Vector         VItemsStatus;
     Vector         VTaxClaim1;
     Vector         VMrsSlNo,VTaxClaim,VMrsUserCode,VMrsType;
     Vector         VOOrdSlNo,VOPColour,VOPSet,VOPSize,VOPSide,VOPSlipFrNo,VOPSlipToNo,VOPBookFrNo,VOPBookToNo;
     Common         common = new Common();

     OrderMiddlePanel MiddlePanel;
     String SItemTable,SSupTable;

     JTextField     TProformaNo,TReferenceNo;
     JComboBox      JCCurrencyType;

     String         SReferenceNo= "", SProformaNo = "", SCurrencyTypeName= "";

     OrderResultSet(String SOrderNo,NextField TOrderNo,DateField2 TDate,JButton BSupplier,JTextField TSupCode,JTextField TRef,JTextField TAdvance,JTextField TPayTerm,NextField TPayDays,JComboBox JCTo,JComboBox JCThro,JComboBox JCForm,OrderMiddlePanel MiddlePanel,Vector VCode,Vector VName,Vector VToCode,Vector VThroCode,Vector VFormCode,JComboBox JEPCG,JComboBox JCOrderType,Vector VEpcg,Vector VOrderType,Vector VPort,JComboBox JCPort,Vector VProject,JComboBox JCProject,Vector VState,JComboBox JCState,int iOrderStatus,Vector VItemsStatus,int iMillCode,String SItemTable,String SSupTable,JTextField TReferenceNo, JTextField TProformaNo, JComboBox JCCurrencyType)
     {
          this.SOrderNo       = SOrderNo;
          this.TOrderNo       = TOrderNo;
          this.TDate          = TDate;
          this.BSupplier      = BSupplier;
          this.TSupCode       = TSupCode;
          this.TRef           = TRef;
          this.TAdvance       = TAdvance;
          this.TPayTerm       = TPayTerm;
          this.TPayDays       = TPayDays;
          this.MiddlePanel    = MiddlePanel;
          this.JCTo           = JCTo;
          this.JCThro         = JCThro;
          this.JCForm         = JCForm;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VToCode        = VToCode;
          this.VThroCode      = VThroCode;
          this.VFormCode      = VFormCode;
          this.JEPCG          = JEPCG;
          this.JCOrderType    = JCOrderType;
          this.VEpcg          = VEpcg;
          this.VOrderType     = VOrderType;
          this.VPort          = VPort;
          this.JCPort         = JCPort;
          this.VState         = VState;
          this.JCState        = JCState;
          this.VProject       = VProject;
          this.JCProject      = JCProject;
          this.VItemsStatus   = VItemsStatus;
          this.iOrderStatus   = iOrderStatus;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;
          this.TReferenceNo   = TReferenceNo;
          this.TProformaNo    = TProformaNo;
          this.JCCurrencyType = JCCurrencyType;

          VOrdBlockName       = new Vector();
          VOCode              = new Vector();
          VOName              = new Vector();
          VOMrsNo             = new Vector();
          VOQty               = new Vector();
          VORate              = new Vector();
          VODiscPer           = new Vector();
          VOCenVatPer         = new Vector();
          VOTaxPer            = new Vector();
          VOSurPer            = new Vector();
          VODeptName          = new Vector();
          VOGroupName         = new Vector();
          VOUnitName          = new Vector();
          VODueDate           = new Vector();
          VOUserName          = new Vector();
	  VODocId	      = new Vector();

          VOId                = new Vector();
          VDId                = new Vector();
          VODesc              = new Vector();
          VOMake              = new Vector();
          VODraw              = new Vector();
          VOCatl              = new Vector();
          VInvQty             = new Vector();
          VMrsSlNo            = new Vector();
          VMrsUserCode        = new Vector();
          VMrsType            = new Vector();
          VTaxClaim           = new Vector();
          VTaxClaim1          = new Vector();
          VTaxClaim1          . insertElementAt("Not Claimable",0);
          VTaxClaim1          . insertElementAt("Claimable",1);
          VOOrdSlNo           = new Vector();
          VOPColour           = new Vector();
          VOPSet              = new Vector();
          VOPSize             = new Vector();
          VOPSide             = new Vector();
          VOPSlipFrNo         = new Vector();
          VOPSlipToNo         = new Vector();
          VOPBookFrNo         = new Vector();
          VOPBookToNo         = new Vector();

          JTClaim             = new JComboBox(VTaxClaim1);
          getData();
          setData();
     }

     public void setData()
     {
          try
          {
               MiddlePanel.RowData           = new Object[VOCode.size()][23];
               MiddlePanel.IdData            = new Object[VOCode.size()];
               MiddlePanel.VOldCode          = new Vector();
               MiddlePanel.VOldMrsNo         = new Vector();
               MiddlePanel.VMrsSlNo          = new Vector();
               MiddlePanel.VMrsUserCode      = new Vector();
               MiddlePanel.VMrsType          = new Vector();

               for(int i=0;i<VOCode.size();i++)
               {
                    MiddlePanel.IdData[i]      = (String)VOId.elementAt(i);
                    MiddlePanel.VOldCode       . addElement((String)VOCode.elementAt(i));
                    MiddlePanel.VOldMrsNo      . addElement((String)VOMrsNo.elementAt(i));
                    MiddlePanel.VMrsSlNo       . addElement((String)VMrsSlNo.elementAt(i));
                    MiddlePanel.VMrsUserCode   . addElement((String)VMrsUserCode.elementAt(i));
                    MiddlePanel.VMrsType       . addElement((String)VMrsType.elementAt(i));
                    MiddlePanel.RowData[i][0]  = (String)VOCode.elementAt(i);
                    MiddlePanel.RowData[i][1]  = (String)VOName.elementAt(i);
                    MiddlePanel.RowData[i][2]  = (String)VTaxClaim1.elementAt(common.toInt((String)VTaxClaim.elementAt(i)));
                    MiddlePanel.RowData[i][3]  = (String)VOrdBlockName.elementAt(i);
                    MiddlePanel.RowData[i][4]  = (String)VOMrsNo.elementAt(i);
                    MiddlePanel.RowData[i][5]  = (String)VOQty.elementAt(i);
                    MiddlePanel.RowData[i][6]  = (String)VORate.elementAt(i);
                    MiddlePanel.RowData[i][7]  = (String)VODiscPer.elementAt(i);
                    MiddlePanel.RowData[i][8]  = (String)VOCenVatPer.elementAt(i);
                    MiddlePanel.RowData[i][9]  = (String)VOTaxPer.elementAt(i);
                    MiddlePanel.RowData[i][10] = (String)VOSurPer.elementAt(i);
                    MiddlePanel.RowData[i][11] = " ";
                    MiddlePanel.RowData[i][12] = " ";
                    MiddlePanel.RowData[i][13] = " ";
                    MiddlePanel.RowData[i][14] = " ";
                    MiddlePanel.RowData[i][15] = " ";
                    MiddlePanel.RowData[i][16] = " ";
                    MiddlePanel.RowData[i][17] = (String)VODeptName.elementAt(i);
                    MiddlePanel.RowData[i][18] = (String)VOGroupName.elementAt(i);
                    MiddlePanel.RowData[i][19] = common.parseDate((String)VODueDate.elementAt(i));
                    MiddlePanel.RowData[i][20] = (String)VOUnitName.elementAt(i);
                    MiddlePanel.RowData[i][21] = (String)VOUserName.elementAt(i);
		    MiddlePanel.RowData[i][22] = common.parseNull((String)VODocId.elementAt(i));
               }
              
               MiddlePanel.createComponents(VCode,VName,iOrderStatus,VItemsStatus,SItemTable);
               // Setting Common Data
     
               TOrderNo  . setText(SOrderNo);
               TDate     . fromString1(SOrderDate);
               TDate     . TDay.setText(SOrderDate.substring(6,8));
               TDate     . TMonth.setText(SOrderDate.substring(4,6));
               TDate     . TYear.setText(SOrderDate.substring(0,4));
               BSupplier . setText(SSupName);
               TSupCode  . setText(SSupCode);
               TRef      . setText(SRef.trim());
               TAdvance  . setText(SAdvance);
               TPayTerm  . setText(SPayTerm);
               TPayDays  . setText(SPayDays);
               JCTo      . setSelectedIndex(VToCode.indexOf(SToCode));
               JCThro    . setSelectedIndex(VThroCode.indexOf(SThroCode));
               JCForm    . setSelectedIndex(VFormCode.indexOf(SFormCode));
               JEPCG     . setSelectedIndex(iEpcg);
               JCProject . setSelectedIndex(iProject);
               JCState   . setSelectedIndex(iState);
               JCPort    . setSelectedIndex(VPort.indexOf(SPort));

               TReferenceNo   . setText(SReferenceNo);
               TProformaNo    . setText(SProformaNo);
               if(SCurrencyTypeName.length() == 0)
               {
                    JCCurrencyType . setSelectedItem("RUPEES");
               }
               else
               {
                    JCCurrencyType . setSelectedItem(SCurrencyTypeName);
               }

               JCOrderType    . setSelectedIndex(iOrderType);
               MiddlePanel    . MiddlePanel.TAdd.setText(SAdd);
               MiddlePanel    . MiddlePanel.TLess.setText(SLess);
               MiddlePanel    . MiddlePanel.calc();
               MiddlePanel    . MiddlePanel.VDesc.removeAllElements();
               MiddlePanel    . MiddlePanel.VMake.removeAllElements();
               MiddlePanel    . MiddlePanel.VDraw.removeAllElements();
               MiddlePanel    . MiddlePanel.VCatl.removeAllElements();
               MiddlePanel    . MiddlePanel.VMrsStatus.removeAllElements();
               MiddlePanel    . DescId = new Object[VODesc.size()];
     
               for(int i=0;i<VODesc.size();i++)
               {
                    MiddlePanel.MiddlePanel.VDesc.addElement((String)VODesc.elementAt(i));
                    MiddlePanel.MiddlePanel.VMake.addElement((String)VOMake.elementAt(i));
                    MiddlePanel.MiddlePanel.VDraw.addElement((String)VODraw.elementAt(i));
                    MiddlePanel.MiddlePanel.VCatl.addElement((String)VOCatl.elementAt(i));
                    MiddlePanel.MiddlePanel.VMrsStatus.addElement("1");
                    MiddlePanel.DescId[i]      = (String)VDId.elementAt(i);

               }

               for(int j=0;j<VOOrdSlNo.size();j++)
               {
                    MiddlePanel    . MiddlePanel. VOrdSlNo  . addElement(VOOrdSlNo   .elementAt(j));
                    MiddlePanel    . MiddlePanel. VPColour  . addElement(VOPColour   .elementAt(j));
                    MiddlePanel    . MiddlePanel. VPSet     . addElement(VOPSet      .elementAt(j));
                    MiddlePanel    . MiddlePanel. VPSize    . addElement(VOPSize     .elementAt(j));
                    MiddlePanel    . MiddlePanel. VPSide    . addElement(VOPSide     .elementAt(j));
                    MiddlePanel    . MiddlePanel. VPSlipFrNo. addElement(VOPSlipFrNo .elementAt(j));
                    MiddlePanel    . MiddlePanel. VPSlipToNo. addElement(VOPSlipToNo .elementAt(j));
                    MiddlePanel    . MiddlePanel. VPBookFrNo. addElement(VOPBookFrNo .elementAt(j));
                    MiddlePanel    . MiddlePanel. VPBookToNo. addElement(VOPBookToNo .elementAt(j));
               }

               TOrderNo.setEditable(false);
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

     public void getData()
     {
          String QString =    " SELECT PurchaseOrder.OrderNo, PurchaseOrder.OrderDate, PurchaseOrder.OrderBlock, "+
                              " OrdBlock.BlockName, "+SSupTable+".Name, PurchaseOrder.Item_Code, InvItems.Item_Name, "+
                              " PurchaseOrder.MrsNo, PurchaseOrder.Qty, PurchaseOrder.Rate, PurchaseOrder.DiscPer, "+
                              " PurchaseOrder.CenVatPer, PurchaseOrder.TaxPer, Dept.Dept_Name, Cata.Group_Name, "+
                              " Unit.Unit_Name, PurchaseOrder.DueDate, PurchaseOrder.ID, PurchaseOrder.Sup_Code, "+
                              " PurchaseOrder.SurPer, PurchaseOrder.Advance, PurchaseOrder.ToCode, PurchaseOrder.ThroCode, "+
                              " PurchaseOrder.plus, PurchaseOrder.Less, MatDesc.Descr,MatDesc.Make,MatDesc.Draw,MatDesc.Catl, "+
                              " MatDesc.ID,PurchaseOrder.PayTerms,PurchaseOrder.FormCode,PurchaseOrder.Reference, "+
                              " PurchaseOrder.EPCG,PurchaseOrder.OrderType,PurchaseOrder.Project_order,PurchaseOrder.State, "+
                              " Port.PortName,PurchaseOrder.PayDays,PurchaseOrder.InvQty,nvl(PurchaseOrder.MrsSlno,0), "+
                              " Purchaseorder.TAXCLAIMABLE,Purchaseorder.slno,"+
                              " MatDesc.PAPERCOLOR,MatDesc.PAPERSETS,MatDesc.PAPERSIZE,MatDesc.PAPERSIDE, "+
                              " MatDesc.SLIPFROMNO,MatDesc.SLIPTONO,MatDesc.BOOKFROMNO,MatDesc.BOOKTONO, "+
                              " PurchaseOrder.MrsAuthUserCode,RawUser.UserName,PurchaseOrder.OrderTypeCode, "+
                              " PurchaseOrder.ReferenceNo, PurchaseOrder.ProformaNo, EPCGCurrencyType.CurrencyTypeName,PurchaseOrder.Docid "+

                              " FROM ((((((((PurchaseOrder LEFT JOIN Port on PurchaseOrder.PortCode=Port.PortCode) "+
                              " INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block) "+
                              " INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code) "+
                              " INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code) "+
                              " INNER JOIN Dept ON PurchaseOrder.Dept_Code=Dept.Dept_code) "+
                              " INNER JOIN Cata ON PurchaseOrder.Group_Code=Cata.Group_Code) "+
                              " INNER JOIN Unit ON PurchaseOrder.Unit_Code=Unit.Unit_Code) "+
                              " INNER JOIN RawUser ON PurchaseOrder.MrsAuthUserCode=RawUser.UserCode) "+
                              " LEFT JOIN MatDesc ON (PurchaseOrder.OrderBlock=MatDesc.OrderBlock) AND (PurchaseOrder.OrderNo=MatDesc.OrderNo) AND (PurchaseOrder.Item_Code=MatDesc.Item_Code)  AND (PurchaseOrder.SlNo=MatDesc.SlNo) "+
                              " Left Join EPCGCurrencyType on EPCGCurrencyType.CurrencyTypeCode = PurchaseOrder.CurrencyTypeCode "+

                              " WHERE PurchaseOrder.OrderNo="+SOrderNo+" And PurchaseOrder.Qty > 0 "+
                              " and purchaseorder.millcode = "+iMillCode+
                              " ORDER BY PurchaseOrder.ID";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet result = stat.executeQuery(QString);

               while(result.next())
               {
                    SOrderDate     = result.getString(2);
                    iBlock         = result.getInt(3);
                    SOrderBlock    = result.getString(4);
                    SSupName       = result.getString(5);

                    VOrdBlockName  . addElement(SOrderBlock);

                    VOCode         . addElement(result.getString(6));
                    VOName         . addElement(result.getString(7));
                    VOMrsNo        . addElement(result.getString(8));
                    VOQty          . addElement(result.getString(9));
                    VORate         . addElement(result.getString(10));
                    VODiscPer      . addElement(result.getString(11));
                    VOCenVatPer    . addElement(result.getString(12));
                    VOTaxPer       . addElement(result.getString(13));
                    VODeptName     . addElement(result.getString(14));
                    VOGroupName    . addElement(result.getString(15));
                    VOUnitName     . addElement(result.getString(16));
                    VODueDate      . addElement(result.getString(17));
                    VOId           . addElement(result.getString(18));
               
                    SSupCode       = result.getString(19);
               
                    VOSurPer       . addElement(result.getString(20));
               
                    SAdvance       = common.parseNull(result.getString(21));
                    SToCode        = common.parseNull(result.getString(22));
                    SThroCode      = common.parseNull(result.getString(23));
                    SAdd           = common.parseNull(result.getString(24));
                    SLess          = common.parseNull(result.getString(25));
               
                    VODesc         . addElement(result.getString(26));
                    VOMake         . addElement(result.getString(27));
                    VODraw         . addElement(result.getString(28));
                    VOCatl         . addElement(result.getString(29));
                    VDId           . addElement(result.getString(30));
               
                    SPayTerm       = common.parseNull(result.getString(31));
                    SFormCode      = common.parseNull(result.getString(32));
                    SRef           = common.parseNull(result.getString(33));
                    iEpcg          = result.getInt(34);
                    iOrderType     = result.getInt(35);
                    iProject       = result.getInt(36);
                    iState         = result.getInt(37);
                    SPort          = common.parseNull(result.getString(38));
                    SPayDays       = common.parseNull(result.getString(39));

                    VInvQty        . addElement(common.parseNull((String)result.getString(40)));
                    VMrsSlNo       . addElement(common.parseNull((String)result.getString(41)));
                    VTaxClaim      . addElement(common.parseNull((String)result.getString(42)));

                    VOOrdSlNo  . addElement(common.parseNull((String)result.getString(43)));
                    VOPColour  . addElement(common.parseNull((String)result.getString(44)));
                    VOPSet     . addElement(common.parseNull((String)result.getString(45)));
                    VOPSize    . addElement(common.parseNull((String)result.getString(46)));
                    VOPSide    . addElement(common.parseNull((String)result.getString(47)));
                    VOPSlipFrNo. addElement(common.parseNull((String)result.getString(48)));
                    VOPSlipToNo. addElement(common.parseNull((String)result.getString(49)));
                    VOPBookFrNo. addElement(common.parseNull((String)result.getString(50)));
                    VOPBookToNo. addElement(common.parseNull((String)result.getString(51)));

                    VMrsUserCode . addElement(common.parseNull((String)result.getString(52)));
                    VOUserName   . addElement(common.parseNull((String)result.getString(53)));
                    VMrsType     . addElement(common.parseNull((String)result.getString(54)));

		    VODocId      . addElement(common.parseNull((String)result.getString(58)));

                    SReferenceNo        = common.parseNull((String)result.getString(55));
                    SProformaNo         = common.parseNull((String)result.getString(56));
                    SCurrencyTypeName   = common.parseNull((String)result.getString(57));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
}
