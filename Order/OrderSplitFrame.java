package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import java.net.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class OrderSplitFrame extends JInternalFrame
{
     JPanel    TopPanel;
     JPanel    BottomPanel;
     JPanel    ControlPanel;
     JPanel    ListPanel;
     
     JButton   BApply,BOk,BExit;
     
     Object RowData[][];
     String ColumnData[] = {"Order No","Date","Block","Supplier","Mrs No","Code","Name","Qty","ReciveQty","Status"};
     String ColumnType[] = {"E"       ,"S"   ,"S"    ,"S"       ,"N"     ,"S"   ,"S"   ,"N" ,"N" ,"B"};

     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;
     int iUserCode,iMillCode,iAuthCode;
     String SSupTable,SYearCode;

     JTextField TOrderNo;
     
     Common common = new Common();
     
     Vector VOrdNo,VOrdDate,VBlock,VSupName,VMrsNo,VCCode,VCName,VOrdQty,VMrsQty,VInvQty,VSlNo,VMrsSlNo,VOrderId,VMrsId,VGrnStatus,VOrdStatus,VMrsStatus;
     
     AlterTabReport tabreport;

     int iARNo=0;
     int iBPANo=0;

     String SNewOrderNo= "";
     int iNewOrderSlNo=0;
     String SNewOrderDate="";
     Vector VCheckMrsNo,VCheckSlNo;

     Connection theConnect = null;

     boolean bComflag = true;
     
     public OrderSplitFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode,String SSupTable,String SYearCode)
     {
          try
          {
               this.DeskTop   = DeskTop;
               this.VCode     = VCode;
               this.VName     = VName;
               this.SPanel    = SPanel;
               this.iUserCode = iUserCode;
               this.iMillCode = iMillCode;
               this.iAuthCode = iAuthCode;
               this.SSupTable = SSupTable;
               this.SYearCode = SYearCode;
               
               getDBConnection();
               createComponents();
               setLayouts();
               addComponents();
               addListeners();
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }

     }

     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnect     =    oraConnection.getConnection();
     }

     public void createComponents()
     {
          try
          {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();

          TOrderNo       = new JTextField();
          BOk            = new JButton("Split");
          BExit          = new JButton("Exit");
          BApply         = new JButton("Apply");
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void setLayouts()
     {
          try
          {
          TopPanel       .setLayout(new GridLayout(1,3));
          BottomPanel.setBorder(new TitledBorder(""));

          setTitle("Order Splitting Frame");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void addComponents()
     {
          try
          {
          TopPanel.add(new JLabel("Order No"));
          TopPanel.add(TOrderNo);
          TopPanel.add(BApply);

          BottomPanel.add(BOk);
          BottomPanel.add(BExit);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     
     public void addListeners()
     {
          try
          {
               BApply.addActionListener(new ApplyList());
               BExit.addActionListener(new ApplyList());
               BOk.addActionListener(new ApplyList());
          }catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
		    if(validOrder())
		    {
		            setDataIntoVector();
		            setRowData();
		            try
		            {
		                 getContentPane().remove(tabreport);
		            }
		            catch(Exception ex){}
		            try
		            {
		                 tabreport = new AlterTabReport(RowData,ColumnData,ColumnType,VGrnStatus);
	     
		                 getContentPane().add(tabreport,BorderLayout.CENTER);
		                 DeskTop.repaint();
		                 DeskTop.updateUI();
		            }
		            catch(Exception ex)
		            {
		                 System.out.println(ex);
		            }
		    }
		    else
		    {
			JOptionPane.showMessageDialog(null,"No Need of Order Splitting for this Order","Information",JOptionPane.INFORMATION_MESSAGE);
		    }
               }
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
               if(ae.getSource()==BOk)
               {
                    if(validMrs())
                    {
                         //if(validAdvance())
                         {
			      int iNewCount = 0;
		              iNewOrderSlNo=0;
			      SNewOrderDate = common.getServerPureDate();

			      String SSysName  = "";
			      String SDateTime = "";

			      try
			      {
			           SSysName  = InetAddress.getLocalHost().getHostName();
			           SDateTime = common.getServerDate();
			      }
			      catch(Exception ex)
			      {
			           System.out.println(ex);
			      }

			      VCheckMrsNo = new Vector();
			      VCheckSlNo  = new Vector();

                              for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
                              {
                                   Boolean bFlag     = (Boolean)tabreport.ReportTable.getValueAt(i,9);
          
                                   if(bFlag.booleanValue())
                                   {
					if(iNewCount==0)
					{
					     SNewOrderNo = getInsertOrderNo();
					}

					iNewOrderSlNo++;

                                        String SOrdStatus = common.parseNull((String)VOrdStatus.elementAt(i));

					if(SOrdStatus.equals("0"))
					{
					     splitOrderData(i,SSysName,SDateTime);
					}
					else if(SOrdStatus.equals("1"))
					{
					     splitPartialOrderData(i,SSysName,SDateTime);
					}
					else
					{
					     continue;
					}

					iNewCount++;
                                   }
                              }
    
                              try
                              {
                                   if(bComflag)
                                   {
                                        theConnect     . commit();
                                        JOptionPane.showMessageDialog(null,"Order Splitted Successfully. Split OrderNo-"+SNewOrderNo,"Information",JOptionPane.INFORMATION_MESSAGE);
                                        System         . out.println("Commit");
                                        theConnect     . setAutoCommit(true);
                                        removeHelpFrame();
                                   }
                                   else
                                   {
                                        theConnect     . rollback();
                                        JOptionPane.showMessageDialog(null,"Problem in Splitting. Contact EDP ","Error",JOptionPane.ERROR_MESSAGE);
                                        System         . out.println("RollBack");
                                        theConnect     . setAutoCommit(true);
                                   }
                              }catch(Exception ex)
                              {
                                   ex.printStackTrace();
                              }
                         }
                    }
               }
          }
     }

     public boolean validMrs()
     {
          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
          {
               Boolean bFlag     = (Boolean)tabreport.ReportTable.getValueAt(i,9);

               if(bFlag.booleanValue())
               {
		    String SMrsStatus = common.parseNull((String)VMrsStatus.elementAt(i));

                    if(SMrsStatus.equals("0"))
		    {
                         continue;
		    }
		    else
                    {
                         JOptionPane.showMessageDialog(null,"Mrs and Order Qty Mismatch @Row-"+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }
               }
          }
          return true;
     }

     public boolean validAdvance()
     {
          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
          {
               Boolean bFlag     = (Boolean)tabreport.ReportTable.getValueAt(i,9);

               if(bFlag.booleanValue())
               {
                    String SOrderNo  = common.parseNull((String)VOrdNo.elementAt(i));
                    String SItemCode = common.parseNull((String)VCCode.elementAt(i));
                    int iOrderSlNo   = common.toInt((String)VSlNo.elementAt(i));

                    int iCount = checkAdvance(SOrderNo,SItemCode,iOrderSlNo);

                    if(iCount<=0)
                         continue;


                    iARNo=0;
                    iBPANo=0;
                
                    getAdvanceARNo(SOrderNo,SItemCode,iOrderSlNo);

                    if(iARNo>0 || iBPANo>0)
                    {
                         JOptionPane.showMessageDialog(null,"Advance Process Made for Item @Row-"+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }
               }
          }
          return true;
     }

     public int checkAdvance(String SOrderNo,String SItemCode,int iOrderSlNo)
     {
          int iCount=0;
          try
          {
               String QS =" Select Count(*) from AdvRequest Where OrderNo="+SOrderNo+" and OrderSlNo="+iOrderSlNo+" and Code='"+SItemCode+"' and MillCode="+iMillCode;

               Statement      stat          =  theConnect.createStatement();
               ResultSet      result        =  stat.executeQuery(QS);
               while(result.next())
               {
                    iCount    = result.getInt(1);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
          return iCount;
     }

     public void getAdvanceARNo(String SOrderNo,String SItemCode,int iOrderSlNo)
     {
          iARNo=0;
          iBPANo=0;
          try
          {
               String QS =" Select Max(ARNo),Max(BPANo) from AdvRequest Where OrderNo="+SOrderNo+" and OrderSlNo="+iOrderSlNo+" and Code='"+SItemCode+"' and MillCode="+iMillCode+" Group by OrderNo,OrderSlNo ";

               Statement      stat          =  theConnect.createStatement();
               ResultSet      result        =  stat.executeQuery(QS);
               while(result.next())
               {
                    iARNo   = result.getInt(1);
                    iBPANo  = result.getInt(2);
               }
               result    . close();	
               stat      . close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     public boolean validOrder()
     {
          int iPendCount = 0;
          int iRecdCount = 0;

          try
          {
               String SOrderNo = TOrderNo.getText();


               String QS1 = " Select Count(*) from PurchaseOrder Where Qty>InvQty and OrderNo="+SOrderNo+" and MillCode="+iMillCode;
               String QS2 = " Select Count(*) from PurchaseOrder Where InvQty>0 and OrderNo="+SOrderNo+" and MillCode="+iMillCode;

               Statement      stat          =  theConnect.createStatement();
               ResultSet      result        =  stat.executeQuery(QS1);
               while(result.next())
               {
                    iPendCount = result.getInt(1);
               }
               result    . close();

               result        =  stat.executeQuery(QS2);
               while(result.next())
               {
                    iRecdCount = result.getInt(1);
               }
               result    . close();
               stat      . close();

	       if(iPendCount>0 && iRecdCount>0)
	            return true;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
	       return false;
          }
          return false;
     }

     public void setDataIntoVector()
     {
          VOrdNo         = new Vector();
          VOrdDate       = new Vector();
          VBlock         = new Vector();
          VSupName       = new Vector();
          VMrsNo         = new Vector();
          VCCode         = new Vector();
          VCName         = new Vector();
          VOrdQty        = new Vector();
          VMrsQty        = new Vector();
          VInvQty        = new Vector();
          VSlNo          = new Vector();
          VMrsSlNo       = new Vector();
	  VOrderId	 = new Vector();
	  VMrsId	 = new Vector();
          VGrnStatus     = new Vector();
          VOrdStatus     = new Vector();
          VMrsStatus     = new Vector();

          try
          {
               Statement      stat          =  theConnect.createStatement();

               String SOrderNo = TOrderNo.getText();

               String QString =    " Select PurchaseOrder.OrderNO,PurchaseOrder.OrderDate,BlockName,"+SSupTable+".Name,PurchaseOrder.MRSNo, "+
			           " PurchaseOrder.Item_Code,InvItems.Item_Name, PurchaseORder.Qty,Mrs.Qty as MrsQty,PurchaseOrder.InvQty, "+
				   " PurchaseOrder.SlNo,PurchaseOrder.MrsSlno,PurchaseOrder.Id,Mrs.Id from PurchaseOrder "+
                                   " Inner join InvItems on InvItems.Item_Code=PurchaseOrder.Item_Code "+
                                   " Inner join "+SSupTable+" On "+SSupTable+".Ac_Code=PurchaseOrder.Sup_Code "+
                                   " Inner Join OrdBlock On OrdBlock.Block=PurchaseOrder.OrderBlock "+
				   " Left join Mrs on (purchaseorder.orderno=mrs.orderno and purchaseorder.mrsno=mrs.mrsno "+
				   " and purchaseorder.item_code=mrs.item_code and purchaseorder.mrsslno=mrs.slno and purchaseorder.millcode=mrs.millcode) "+
                                   " Where PurchaseOrder.OrderNo='"+SOrderNo+"' and PurchaseOrder.MillCode="+iMillCode+
                                   " Order By 1 ";

               ResultSet res  = stat.executeQuery(QString);

               while (res.next())
               {
		    double dOrdQty = common.toDouble(res.getString(8));
		    double dMrsQty = common.toDouble(res.getString(9));
		    double dInvQty = common.toDouble(res.getString(10));

                    VOrdNo         .addElement(res.getString(1));
                    VOrdDate       .addElement(res.getString(2));
                    VBlock         .addElement(res.getString(3));
                    VSupName       .addElement(res.getString(4));
                    VMrsNo         .addElement(res.getString(5));
                    VCCode         .addElement(res.getString(6));
                    VCName         .addElement(res.getString(7));     
                    VOrdQty        .addElement(res.getString(8));
                    VMrsQty        .addElement(res.getString(9));
                    VInvQty        .addElement(res.getString(10));
                    VSlNo          .addElement(res.getString(11));
                    VMrsSlNo       .addElement(res.getString(12));
                    VOrderId       .addElement(res.getString(13));
                    VMrsId         .addElement(res.getString(14));

		    if(dOrdQty>dInvQty)
		    {
		         VGrnStatus.addElement("0");

			 if(dInvQty<=0)
			 {
			      VOrdStatus.addElement("0");
			 }
			 else
			 {
			      VOrdStatus.addElement("1");
			 }
		    }
		    else
		    {
		         VGrnStatus.addElement("1");
		         VOrdStatus.addElement("2");
		    }

		    if(dOrdQty>dMrsQty || dOrdQty<dMrsQty)
		    {
		         VMrsStatus.addElement("1");	
		    }
		    else
		    {
		         VMrsStatus.addElement("0");	
		    }
               }
               res.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          try
          {
               RowData     = new Object[VOrdDate.size()][10];
               for(int i=0;i<VOrdDate.size();i++)
               {
                    RowData[i][0]  = common.parseNull((String)VOrdNo         .elementAt(i));
                    RowData[i][1]  = common.parseDate((String)VOrdDate       .elementAt(i));
                    RowData[i][2]  = common.parseNull((String)VBlock         .elementAt(i));
                    RowData[i][3]  = common.parseNull((String)VSupName       .elementAt(i));
                    RowData[i][4]  = common.parseNull((String)VMrsNo         .elementAt(i));
                    RowData[i][5]  = common.parseNull((String)VCCode         .elementAt(i));
                    RowData[i][6]  = common.parseNull((String)VCName         .elementAt(i));
                    RowData[i][7]  = common.parseNull((String)VOrdQty        .elementAt(i));
                    RowData[i][8]  = common.parseNull((String)VInvQty        .elementAt(i));
                    RowData[i][9]  = new Boolean(false);
               }
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public void splitOrderData(int iRow,String SSysName,String SDateTime)
     {
          try
          {
                String SOrderId    = common.parseNull((String)VOrderId.elementAt(iRow));
                String SMrsId      = common.parseNull((String)VMrsId.elementAt(iRow));
                String SOldOrderNo = common.parseNull((String)VOrdNo.elementAt(iRow));
                String SOldOrdSlNo = common.parseNull((String)VSlNo.elementAt(iRow));

               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat = theConnect.createStatement();


               String QS1 = " Insert Into PurchaseOrder (Id,OrderNo,SlNo,InvQty,SplitStatus,SplitOrderNo,OrderDate,OrderBlock,"+
                            " Sup_Code,Reference,Advance,MrsNo,Item_Code,Qty,Rate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,"+
                            " DueDate,PayTerms,Dept_Code,Group_Code,Unit_Code,ToCode,ThroCode,Posted,AdvSlNo,AmendmentFlag,FormCode,Class_Code,"+
                            " PYPost,WA,DM,Freight,PF,Others,EPCG,OrderType,LicenseNumber,Project_Order,PortCode,State,Amended,UserCode,ModiDate,"+
                            " MillCode,PayDays,Status,Flag,MrsSlNo,TaxClaimable,Authentication,IntimationNo,MailStatus,MrsAuthUserCode,MailConfirmStatus,"+
                            " Converted,JMDOrderApproval,JMDOrderAppDate,JMDOrderRejection,JMDOrderRejDate,SOOrderApproval,SOOrderAppDate,SOOrderRejection,"+
                            " SOOrderRejDate,IAOrderApproval,IAOrderAppDate,IAOrderRejection,IAOrderRejDate,IAOrderRejRemarks,OrderSourceTypeCode,"+
                            " PrevOrderNo,PrevOrderBlock,ComparisionNo,CreatedDateTime,PlanningMonth,OrderTypeCode,JMDOrderHolding,JMDOrderHoldingDate,"+
                            " ReferenceNo,EPCGGroupStatus,YearCode,ProformaNo,Basic,CurrencyTypeCode,POGroupNo,SeqId,DocId,FinalAppUserCode,"+
			    " JMDViewStatus,JMDFinalHoldingStatus, IAViewStatus, IAFinalHoldingStatus,PrevOrderChange,RateChartNo,"+
			    " CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,Cess,CessVal,GstStatus) "+
                            " (Select PurchaseOrder_Seq.nextVal,"+
                            ""+SNewOrderNo+" as OrderNo,"+
                            ""+iNewOrderSlNo+" as SlNo,"+
			    " 0 as InvQty, "+
			    " 1 as SplitStatus, "+
                            ""+SOldOrderNo+" as SplitOrderNo,"+
                            " OrderDate,OrderBlock,"+
                            " Sup_Code,Reference,Advance,MrsNo,Item_Code,Qty,Rate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,"+
                            " DueDate,PayTerms,Dept_Code,Group_Code,Unit_Code,ToCode,ThroCode,Posted,AdvSlNo,AmendmentFlag,FormCode,Class_Code,"+
                            " PYPost,WA,DM,Freight,PF,Others,EPCG,OrderType,LicenseNumber,Project_Order,PortCode,State,Amended,UserCode,ModiDate,"+
                            " MillCode,PayDays,Status,Flag,MrsSlNo,TaxClaimable,Authentication,IntimationNo,MailStatus,MrsAuthUserCode,MailConfirmStatus,"+
                            " Converted,JMDOrderApproval,JMDOrderAppDate,JMDOrderRejection,JMDOrderRejDate,SOOrderApproval,SOOrderAppDate,SOOrderRejection,"+
                            " SOOrderRejDate,IAOrderApproval,IAOrderAppDate,IAOrderRejection,IAOrderRejDate,IAOrderRejRemarks,OrderSourceTypeCode,"+
                            " PrevOrderNo,PrevOrderBlock,ComparisionNo,CreatedDateTime,PlanningMonth,OrderTypeCode,JMDOrderHolding,JMDOrderHoldingDate, "+
                            " ReferenceNo,EPCGGroupStatus,YearCode,ProformaNo,Basic,CurrencyTypeCode,POGroupNo,SeqId,DocId,FinalAppUserCode,"+
			    " JMDViewStatus,JMDFinalHoldingStatus, IAViewStatus, IAFinalHoldingStatus,PrevOrderChange,RateChartNo, "+
			    " CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,Cess,CessVal,GstStatus "+
                            " From PurchaseOrder Where Id="+SOrderId+" and MillCode="+iMillCode+")";

                    
               String QS2 = " Insert Into MatDesc (Id,OrderNo,SlNo,OrderDate,OrderBlock,"+
                            " Item_Code,Descr,Make,Draw,Catl,WA,Status,Flag,MillCode,PaperColor,PaperSets,"+
                            " PaperSize,PaperSide,SlipFromNo,SlipToNo,BookFromNo,BookToNo) "+
                            " (Select MatDesc_Seq.nextVal,"+
                            ""+SNewOrderNo+" as OrderNo,"+
                            ""+iNewOrderSlNo+" as SlNo,"+
                            " OrderDate,OrderBlock,"+
                            " Item_Code,Descr,Make,Draw,Catl,WA,Status,Flag,MillCode,PaperColor,PaperSets,"+
                            " PaperSize,PaperSide,SlipFromNo,SlipToNo,BookFromNo,BookToNo "+
                            " From MatDesc Where OrderNo="+SOldOrderNo+" and SlNo="+SOldOrdSlNo+" and MillCode="+iMillCode+")";

               String QS3 = " Update MRS Set OrderNo="+SNewOrderNo+" Where Id="+SMrsId+" and MillCode="+iMillCode;

               stat.executeUpdate(QS1);
               stat.executeUpdate(QS2);
               stat.executeUpdate(QS3);

               String QS4 = "Delete from MatDesc Where OrderNo='"+SOldOrderNo+"' and SlNo="+SOldOrdSlNo+" and MillCode="+iMillCode;
               stat.executeUpdate(QS4);

               String QS5 = " Delete from PurchaseOrder Where InvQty=0 and Id="+SOrderId+" and MillCode="+iMillCode;
               stat.executeUpdate(QS5);
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     public void splitPartialOrderData(int iRow,String SSysName,String SDateTime)
     {
          try
          {
                String SOrderId    = common.parseNull((String)VOrderId.elementAt(iRow));
                String SMrsNo      = common.parseNull((String)VMrsNo.elementAt(iRow));
                String SMrsId      = common.parseNull((String)VMrsId.elementAt(iRow));
                String SOldOrderNo = common.parseNull((String)VOrdNo.elementAt(iRow));
                String SOldOrdSlNo = common.parseNull((String)VSlNo.elementAt(iRow));
                String SOrdQty     = common.parseNull((String)VOrdQty.elementAt(iRow));
                String SInvQty     = common.parseNull((String)VInvQty.elementAt(iRow));
                String SBalQty     = common.getRound(common.toDouble(SOrdQty) - common.toDouble(SInvQty),3);

		int iNewMrsSlNo = 0;

		int iMIndex = common.exactIndexOf(VCheckMrsNo,SMrsNo);

		if(iMIndex>=0)
		{
		     int iLastSlNo = common.toInt((String)VCheckSlNo.elementAt(iMIndex));
		     iNewMrsSlNo   = iLastSlNo+1;
		     VCheckSlNo.setElementAt(String.valueOf(iNewMrsSlNo),iMIndex);
		}
		else
		{
		     iNewMrsSlNo = getNewMrsSlNo(SMrsNo);
		     VCheckMrsNo.addElement(SMrsNo);
		     VCheckSlNo.addElement(String.valueOf(iNewMrsSlNo));
		}

		double dOldRatio   = common.toDouble(SInvQty) / common.toDouble(SOrdQty);
		double dNewRatio   = common.toDouble(SBalQty) / common.toDouble(SOrdQty);

               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat = theConnect.createStatement();


               String QS1 = " Insert Into PurchaseOrder (Id,OrderNo,SlNo,Qty,InvQty,Disc,Cenvat,Tax,Sur,Net,Plus,Less,Misc,MrsSlNo,Basic,"+
			    " CGSTVal,SGSTVal,IGSTVal,CessVal,SplitStatus,SplitOrderNo, "+
                            " OrderDate,OrderBlock,Sup_Code,Reference,Advance,MrsNo,Item_Code,Rate,DiscPer,CenvatPer,TaxPer,SurPer,"+
                            " DueDate,PayTerms,Dept_Code,Group_Code,Unit_Code,ToCode,ThroCode,Posted,AdvSlNo,AmendmentFlag,FormCode,Class_Code,"+
                            " PYPost,WA,DM,Freight,PF,Others,EPCG,OrderType,LicenseNumber,Project_Order,PortCode,State,Amended,UserCode,ModiDate,"+
                            " MillCode,PayDays,Status,Flag,TaxClaimable,Authentication,IntimationNo,MailStatus,MrsAuthUserCode,MailConfirmStatus,"+
                            " Converted,JMDOrderApproval,JMDOrderAppDate,JMDOrderRejection,JMDOrderRejDate,SOOrderApproval,SOOrderAppDate,SOOrderRejection,"+
                            " SOOrderRejDate,IAOrderApproval,IAOrderAppDate,IAOrderRejection,IAOrderRejDate,IAOrderRejRemarks,OrderSourceTypeCode,"+
                            " PrevOrderNo,PrevOrderBlock,ComparisionNo,CreatedDateTime,PlanningMonth,OrderTypeCode,JMDOrderHolding,JMDOrderHoldingDate,"+
                            " ReferenceNo,EPCGGroupStatus,YearCode,ProformaNo,CurrencyTypeCode,POGroupNo,SeqId,DocId,FinalAppUserCode,"+
			    " JMDViewStatus,JMDFinalHoldingStatus, IAViewStatus, IAFinalHoldingStatus,PrevOrderChange,RateChartNo,"+
			    " CGST,SGST,IGST,Cess,GstStatus) "+
                            " (Select PurchaseOrder_Seq.nextVal,"+
                            ""+SNewOrderNo+" as OrderNo,"+
                            ""+iNewOrderSlNo+" as SlNo,"+
                            ""+SBalQty+" as Qty,"+
			    " 0 as InvQty, "+
                            " round(((Qty-InvQty)/Qty) * Disc,2) as Disc,"+
                            " round(((Qty-InvQty)/Qty) * Cenvat,2) as Cenvat,"+
                            " round(((Qty-InvQty)/Qty) * Tax,2) as Tax,"+
                            " round(((Qty-InvQty)/Qty) * Sur,2) as Sur,"+
                            " round(((Qty-InvQty)/Qty) * Net,2) as Net,"+
                            " round(((Qty-InvQty)/Qty) * Plus,2) as Plus,"+
                            " round(((Qty-InvQty)/Qty) * Less,2) as Less,"+
                            " round(((Qty-InvQty)/Qty) * Misc,2) as Misc,"+
                            ""+iNewMrsSlNo+" as MrsSlNo,"+
                            " round(((Qty-InvQty)/Qty) * Basic,2) as Basic,"+
                            " round(((Qty-InvQty)/Qty) * CGSTVal,2) as CGSTVal,"+
                            " round(((Qty-InvQty)/Qty) * SGSTVal,2) as SGSTVal,"+
                            " round(((Qty-InvQty)/Qty) * IGSTVal,2) as IGSTVal,"+
                            " round(((Qty-InvQty)/Qty) * CessVal,2) as CessVal,"+
			    " 1 as SplitStatus, "+
                            ""+SOldOrderNo+" as SplitOrderNo,"+
                            " OrderDate,OrderBlock,Sup_Code,Reference,Advance,MrsNo,Item_Code,Rate,DiscPer,CenvatPer,TaxPer,SurPer,"+
                            " DueDate,PayTerms,Dept_Code,Group_Code,Unit_Code,ToCode,ThroCode,Posted,AdvSlNo,AmendmentFlag,FormCode,Class_Code,"+
                            " PYPost,WA,DM,Freight,PF,Others,EPCG,OrderType,LicenseNumber,Project_Order,PortCode,State,Amended,UserCode,ModiDate,"+
                            " MillCode,PayDays,Status,Flag,TaxClaimable,Authentication,IntimationNo,MailStatus,MrsAuthUserCode,MailConfirmStatus,"+
                            " Converted,JMDOrderApproval,JMDOrderAppDate,JMDOrderRejection,JMDOrderRejDate,SOOrderApproval,SOOrderAppDate,SOOrderRejection,"+
                            " SOOrderRejDate,IAOrderApproval,IAOrderAppDate,IAOrderRejection,IAOrderRejDate,IAOrderRejRemarks,OrderSourceTypeCode,"+
                            " PrevOrderNo,PrevOrderBlock,ComparisionNo,CreatedDateTime,PlanningMonth,OrderTypeCode,JMDOrderHolding,JMDOrderHoldingDate, "+
                            " ReferenceNo,EPCGGroupStatus,YearCode,ProformaNo,CurrencyTypeCode,POGroupNo,SeqId,DocId,FinalAppUserCode,"+
			    " JMDViewStatus,JMDFinalHoldingStatus, IAViewStatus, IAFinalHoldingStatus,PrevOrderChange,RateChartNo, "+
			    " CGST,SGST,IGST,Cess,GstStatus "+
                            " From PurchaseOrder Where Id="+SOrderId+" and MillCode="+iMillCode+")";

                    
               String QS2 = " Insert Into MatDesc (Id,OrderNo,SlNo,OrderDate,OrderBlock,"+
                            " Item_Code,Descr,Make,Draw,Catl,WA,Status,Flag,MillCode,PaperColor,PaperSets,"+
                            " PaperSize,PaperSide,SlipFromNo,SlipToNo,BookFromNo,BookToNo) "+
                            " (Select MatDesc_Seq.nextVal,"+
                            ""+SNewOrderNo+" as OrderNo,"+
                            ""+iNewOrderSlNo+" as SlNo,"+
                            " OrderDate,OrderBlock,"+
                            " Item_Code,Descr,Make,Draw,Catl,WA,Status,Flag,MillCode,PaperColor,PaperSets,"+
                            " PaperSize,PaperSide,SlipFromNo,SlipToNo,BookFromNo,BookToNo "+
                            " From MatDesc Where OrderNo="+SOldOrderNo+" and SlNo="+SOldOrdSlNo+" and MillCode="+iMillCode+")";

	       String QS3 = " Insert Into Mrs (Id,Qty,OrderNo,GrnNo,SlNo,UserCode,CreationDate,NetValue,SystemName,EntryDateTime,"+
			    " AutoOrderConversion,MrsNo,MrsDate,Item_Code,Unit_Code,Dept_Code,Group_Code,Catl,Draw,Make,DueDate,OrderBlock,EnquiryNo,"+
			    " HodCode,RefNo,NatureCode,BlockCode,UrgOrd,Posted,PyPost,WA,DM,PlanMonth,MillCode,Remarks,Make_Code,"+
			    " Serv_Plan_No,Serv_Code,Mac_Code,IssueNo,Status,Flag,PaperColor,PaperSets,PaperSize,PaperSide,"+
			    " SlipFromNo,SlipToNo,MrsFlag,MrsTypeCode,BudgetStatus,NetRate,DeptMrsAuth,ItemName,MrsAuthUserCode,"+
			    " YearlyPlanningStatus,YearlyPlanningMonth,DocumentId,ProjectMrsTypeCode,ReasonForMrs,MrsReasonCode,"+
			    " ProjectNo,OrderSourceCode) "+
			    " (Select Mrs_Seq.nextVal,"+
 			    "0"+SBalQty+" as Qty,"+
                            ""+SNewOrderNo+" as OrderNo,"+
			    " 0 as GrnNo,"+		
			    ""+iNewMrsSlNo+" as SlNo,"+
			    ""+iUserCode+" as UserCode,"+
			    "'"+SDateTime+"' as CreationDate,"+
			    "round("+SBalQty+"*NetRate,2) as NetValue,"+
			    "'"+SSysName+"' as SystemName,"+
			    "'"+SDateTime+"' as EntryDateTime,"+
			    " AutoOrderConversion,"+
			    " MrsNo,MrsDate,Item_Code,Unit_Code,Dept_Code,Group_Code,Catl,Draw,Make,DueDate,OrderBlock,EnquiryNo,"+
			    " HodCode,RefNo,NatureCode,BlockCode,UrgOrd,Posted,PyPost,WA,DM,PlanMonth,MillCode,Remarks,Make_Code,"+
			    " Serv_Plan_No,Serv_Code,Mac_Code,IssueNo,Status,Flag,PaperColor,PaperSets,PaperSize,PaperSide,"+
			    " SlipFromNo,SlipToNo,MrsFlag,MrsTypeCode,BudgetStatus,NetRate,DeptMrsAuth,ItemName,MrsAuthUserCode,"+
			    " YearlyPlanningStatus,YearlyPlanningMonth,DocumentId,ProjectMrsTypeCode,ReasonForMrs,MrsReasonCode,"+
			    " ProjectNo,OrderSourceCode from Mrs "+
			    " Where Id="+SMrsId+" And MillCode="+iMillCode+")";


               String QS4 = " Update MRS Set Qty="+SInvQty+",NetValue=round("+SInvQty+"*NetRate,2) Where Id="+SMrsId+" and MillCode="+iMillCode;

               stat.executeUpdate(QS1);
               stat.executeUpdate(QS2);
               stat.executeUpdate(QS3);
               stat.executeUpdate(QS4);

	       String QS5 = " Update PurchaseOrder Set "+
			    " Qty="+SInvQty+","+
			    " Disc=round((InvQty/Qty) * Disc,2),"+
			    " Cenvat=round((InvQty/Qty) * Cenvat,2),"+
			    " Tax=round((InvQty/Qty) * Tax,2),"+
			    " Sur=round((InvQty/Qty) * Sur,2),"+
			    " Net=round((InvQty/Qty) * Net,2),"+
			    " Plus=round((InvQty/Qty) * Plus,2),"+
			    " Less=round((InvQty/Qty) * Less,2),"+
			    " Misc=round((InvQty/Qty) * Misc,2),"+
			    " Basic=round((InvQty/Qty) * Basic,2),"+
			    " CGSTVal=round((InvQty/Qty) * CGSTVal,2),"+
			    " SGSTVal=round((InvQty/Qty) * SGSTVal,2),"+
			    " IGSTVal=round((InvQty/Qty) * IGSTVal,2),"+
			    " CessVal=round((InvQty/Qty) * CessVal,2)"+
			    " Where Id="+SOrderId+" and MillCode="+iMillCode;

               stat.executeUpdate(QS5);
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     public String getInsertOrderNo()
     {
          String SNewNo= "";
          String QS      = "";

          QS = " Select (maxno+1) From Config"+iMillCode+""+SYearCode+" where Id=1 for update of MaxNo noWait";

          try
          {
               if(theConnect  . getAutoCommit())
                    theConnect  . setAutoCommit(false);

               Statement stat      = theConnect   . createStatement();

               PreparedStatement thePrepare = theConnect.prepareStatement(" Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 1");

               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         SNewNo    = common.parseNull((String)result.getString(1));
                         result    . close();

               thePrepare.setInt(1,common.toInt(SNewNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getInsertOrderNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
           
          return SNewNo.trim();
     }

     public int getNewMrsSlNo(String SMrsNo)
     {
          int iNewNo=0;
          try
          {
               String QS =" Select Max(SlNo)+1 from Mrs Where MrsNo="+SMrsNo+" and MillCode="+iMillCode;

               Statement      stat          =  theConnect.createStatement();
               ResultSet      result        =  stat.executeQuery(QS);
               while(result.next())
               {
                    iNewNo    = result.getInt(1);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
          return iNewNo;
     }



}
