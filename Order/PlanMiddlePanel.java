package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class PlanMiddlePanel extends JPanel
{

   JTable           ReportTable;    // The Bottom Table and Top Tables
   InvTableModel    dataModel;               // The Detailed View Table Model

   Vector           VRCode,VRName,VRQty,VRRate,VRDis,VRVat,VRTax,VRSur,VRDue;  

   Connection       theConnection = null;


   Vector           VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo;
   Vector           VMRSSlNo,VMRSUserCode,VMRSType,VPOrderNo,VPSource,VPBlockCode;


   // For Details View of P.O
   String           ColumnData[] = {"Code","Name","Vat","Block","MRS No","Qty","Rate","Discount(%)","CenVat(%)","Tax(%)","Surcharge(%)","Basic","Discount (Rs)","CenVat(Rs)","Tax(Rs)","Surcharge(Rs)","Net (Rs)","Department","Group","Due Date","Unit","DocId"};
   String           ColumnType[] = {"S","S","B","B","N","N","B","B","B","B","B","N","N","N","N","N","N","B","B","B","B","E"};

   JLabel           LBasic,LDiscount,LCenVat,LTax,LSur,LNet;
   NextField        TAdd,TLess;

   JPanel           FigurePanel;                     // The South Panel 
   JPanel           GridPanel,GridTop,GridBottom;    // The Center Panel and its two Constituents


   Vector           VDept,VDeptCode,VGroup,VGroupCode,VUnit,VUnitCode,VBlock,VBlockCode,VTaxClaim;
   Vector           VDesc,VMake,VDraw,VCatl;
   JComboBox        JCDept,JCGroup,JCUnit,JCBlock,JCTax;

   Common           common = new Common();
   // Parameter Counterparts
   JLayeredPane     DeskTop;
   Object           RowData[][];
   int              iMillCode;
   String           SItemTable;

   PlanMiddlePanel(JLayeredPane DeskTop,Object RowData[][],int iMillCode,String SItemTable)
   {
         this.DeskTop    = DeskTop;
         this.RowData    = RowData;
         this.iMillCode  = iMillCode;
         this.SItemTable = SItemTable;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
         setReportTable();
    }
    public void createComponents()
    {
         LBasic           = new JLabel("0");
         LDiscount        = new JLabel("0");
         LCenVat          = new JLabel("0");
         LTax             = new JLabel("0");
         LSur             = new JLabel("0");
         TAdd             = new NextField();
         TLess            = new NextField();
         LNet             = new JLabel("0");
         GridPanel        = new JPanel(true);
         GridTop          = new JPanel(true);
         GridBottom       = new JPanel(true);
         FigurePanel      = new JPanel();
         VDesc            = new Vector();
         VMake            = new Vector();
         VDraw            = new Vector();
         VCatl            = new Vector();

         VPColour       = new Vector();
         VPSet          = new Vector();
         VPSize         = new Vector();
         VPSide         = new Vector();
         VPSlipFrNo     = new Vector();
         VPSlipToNo     = new Vector();
         VPBookFrNo     = new Vector();
         VPBookToNo     = new Vector();

         VMRSSlNo       = new Vector();
         VMRSUserCode   = new Vector();
         VMRSType       = new Vector();
         VPOrderNo      = new Vector();
         VPSource       = new Vector();
         VPBlockCode    = new Vector();

         for(int i=0;i<RowData.length;i++)
         {
             VDesc.addElement(" ");
             VMake.addElement(" ");
             VDraw.addElement(" ");
             VCatl.addElement(" ");

             VPColour       . addElement(" ");
             VPSet          . addElement(" ");
             VPSize         . addElement(" ");
             VPSide         . addElement(" ");
             VPSlipFrNo     . addElement(" ");
             VPSlipToNo     . addElement(" ");
             VPBookFrNo     . addElement(" ");
             VPBookToNo     . addElement(" ");

             VMRSSlNo.addElement(" ");
             VMRSUserCode.addElement(" ");
             VMRSType.addElement(" ");
             VPOrderNo.addElement(" ");
             VPSource.addElement(" ");
             VPBlockCode.addElement(" ");
         }
     }
     public void setLayouts()
     {
         GridPanel.setLayout(new GridLayout(1,1));
         FigurePanel.setLayout(new GridLayout(2,8));
         GridTop.setLayout(new BorderLayout());
         GridBottom.setLayout(new BorderLayout());         
     }
     public void addComponents()
     {
         FigurePanel.add(new JLabel("Basic"));
         FigurePanel.add(new JLabel("Discount"));
         FigurePanel.add(new JLabel("CenVat"));
         FigurePanel.add(new JLabel("Tax"));
         FigurePanel.add(new JLabel("Surcharge"));
         FigurePanel.add(new JLabel("Plus"));
         FigurePanel.add(new JLabel("Minus"));
         FigurePanel.add(new JLabel("Net"));

         FigurePanel.add(LBasic);
         FigurePanel.add(LDiscount);
         FigurePanel.add(LCenVat);
         FigurePanel.add(LTax);
         FigurePanel.add(LSur);
         FigurePanel.add(TAdd);
         FigurePanel.add(TLess);
         FigurePanel.add(LNet);

         getDeptGroupUnit();

         VTaxClaim     = new Vector();
         VTaxClaim.insertElementAt("Not Claimable",0);
         VTaxClaim.insertElementAt("Claimable",1);

         JCDept  = new JComboBox(VDept);
         JCGroup = new JComboBox(VGroup);
         JCUnit  = new JComboBox(VUnit);
         JCBlock = new JComboBox(VBlock);

         JCTax   = new JComboBox(VTaxClaim);
     }
     public void addListeners()
     {
         TAdd.addKeyListener(new KeyList());
         TLess.addKeyListener(new KeyList());
     }
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               calc();
          }
     }
     public void calc()
     {

               double dTNet = common.toDouble(LBasic.getText())-common.toDouble(LDiscount.getText())+common.toDouble(LCenVat.getText())+common.toDouble(LTax.getText())+common.toDouble(LSur.getText());
               dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
               LNet.setText(common.getRound(dTNet,2));                              
     }


     public void setReportTable()
     {
         dataModel        = new InvTableModel(RowData,ColumnData,ColumnType,LBasic,LDiscount,LCenVat,LTax,LSur,TAdd,TLess,LNet,false);       
         ReportTable      = new JTable(dataModel);
         ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<ReportTable.getColumnCount();col++)
         {
               if(ColumnType[col]=="N" || ColumnType[col]=="B" || ColumnType[col]=="E")
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
         }


         TableColumn deptColumn  = ReportTable.getColumn("Department");
         TableColumn groupColumn = ReportTable.getColumn("Group");
         TableColumn unitColumn  = ReportTable.getColumn("Unit");
         TableColumn orderColumn = ReportTable.getColumn("Block");

         TableColumn TaxColumn = ReportTable.getColumn("Vat");

         deptColumn.setCellEditor(new DefaultCellEditor(JCDept));
         groupColumn.setCellEditor(new DefaultCellEditor(JCGroup));
         unitColumn.setCellEditor(new DefaultCellEditor(JCUnit));
         orderColumn.setCellEditor(new DefaultCellEditor(JCBlock));
         TaxColumn.setCellEditor(new DefaultCellEditor(JCTax));

         setLayout(new BorderLayout());
         GridBottom.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
         GridBottom.add(new JScrollPane(ReportTable),BorderLayout.CENTER);

         GridPanel.add(GridBottom);

         add(FigurePanel,BorderLayout.SOUTH);
         add(GridPanel,BorderLayout.CENTER);
         ReportTable.addMouseListener(new MouseList());
         ReportTable.addKeyListener(new DescList());
         ReportTable    . addKeyListener(new StationaryList());
     }
     public class MouseList extends MouseAdapter
     {
               public void mouseClicked(MouseEvent me)
               {
                    if (me.getClickCount()==2)
                         setRateMouse();
               }
     }
     private void setRateMouse()
     {
               RateMouse MP = new RateMouse(DeskTop,VDesc,VMake,VDraw,VCatl,ReportTable,dataModel);
               try
               {
                    DeskTop.add(MP);
                    DeskTop.repaint();
                    MP.setSelected(true);
                    DeskTop.updateUI();
                    MP.show();
               }
               catch(java.beans.PropertyVetoException ex){}
     }
     private void  setDocumentViewFrame() {

     	int iRow = ReportTable.getSelectedRow();

	String sId= (String)dataModel. getValueAt(iRow,21); 

	System.out.println("id-->"+sId);

	       DocumentationDisplayFrame docDisFrame = new DocumentationDisplayFrame(DeskTop,ReportTable,dataModel,iRow,sId);
               try
               {
                    DeskTop.add(docDisFrame);
                    DeskTop.repaint();
                    docDisFrame.setSelected(true);
                    DeskTop.updateUI();
                    docDisFrame.show();
               }
               catch(java.beans.PropertyVetoException ex){}




     }

     private void  setDocumentFrame() {

	       System.out.println("comm doc frame");
			
	       int iRow = ReportTable.getSelectedRow();
	       ColumnType[21]="S";

               DocumentationEntryFrame docFrame = new DocumentationEntryFrame(DeskTop,ReportTable,dataModel,iRow);
               try
               {
                    DeskTop.add(docFrame);
                    DeskTop.repaint();
                    docFrame.setSelected(true);
                    DeskTop.updateUI();
                    docFrame.show();
               }
               catch(java.beans.PropertyVetoException ex){}


     }

     private void setDescMouse()
     {
               DescMouse MP = new DescMouse(DeskTop,VDesc,VMake,VDraw,VCatl,ReportTable,dataModel,0);
               try
               {
                    DeskTop.add(MP);
                    DeskTop.repaint();
                    MP.setSelected(true);
                    DeskTop.updateUI();
                    MP.show();
               }
               catch(java.beans.PropertyVetoException ex){}
     }
     public class DescList extends KeyAdapter
     {
               public void keyPressed(KeyEvent ke)
               {
                    if(ke.getKeyCode()==KeyEvent.VK_INSERT) 
                         setDescMouse();
                    if(ke.getKeyCode()==KeyEvent.VK_F3)
                         setDocumentFrame();
                    if(ke.getKeyCode()==KeyEvent.VK_F1)
                         setDocumentViewFrame();
               }
     }

     private void setStationaryFrame()
     {

          StationaryPropertiesFrame  SPF   = new StationaryPropertiesFrame(DeskTop,ReportTable,dataModel,VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo);
          try
          {
               DeskTop   . add(SPF);
               DeskTop   . repaint();
               SPF       . setSelected(true);
               DeskTop   . updateUI();
               SPF       . show();
          }
          catch(java.beans.PropertyVetoException ex){ex.printStackTrace();}
     }

     public class StationaryList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    int       i         = ReportTable.getSelectedRow();

                    String    SItem_Code= (String)ReportTable.getModel().getValueAt(i,0);

                    if(isStationary(SItem_Code))
                         setStationaryFrame();
               }    
          }
     }
     public Object[][] getFromVector()
     {
          return dataModel.getFromVector();     
     }
     public String getDeptCode(int i)                       // 15,16 //16,17 MohanJr
     {
         Vector VCurVector = dataModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(17);
         int iid = VDept.indexOf(str);
         return (iid==-1 ?"0":(String)VDeptCode.elementAt(iid));
     }
     public String getGroupCode(int i)                     // 16,17 //17,18 MohanJr
     {
         Vector VCurVector = dataModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(18);
         int iid = VGroup.indexOf(str);
         return (iid==-1?"0":(String)VGroupCode.elementAt(iid));
     }
     public String getUnitCode(int i)                     //  19,20
     {
         Vector VCurVector = dataModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(20);
         int iid = VUnit.indexOf(str);
         return (iid==-1?"0":(String)VUnitCode.elementAt(iid));
     }

     public String getBlockCode(int i)                     //  2,3
     {
         Vector VCurVector = dataModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(3);
         int iid = VBlock.indexOf(str);
         return (iid==-1?"0":(String)VBlockCode.elementAt(iid));
     }

     public String getTaxClaim(int i)      //  2
     {
          Vector VCurVector = dataModel.getCurVector(i);
          String str = (String)VCurVector.elementAt(2);
          int iid = VTaxClaim.indexOf(str);
          return (iid==-1?"0":String.valueOf(iid));
     }
     
     public String getDueDate(int i,DateField TDate)             //  18,19
     {
         Vector VCurVector = dataModel.getCurVector(i);
         try
         {
            String str = (String)VCurVector.elementAt(19);
            if((str.trim()).length()==0)
               return TDate.toNormal();
            else
               return common.pureDate(str);
         }
         catch(Exception ex)
         {
            return " ";
         }
     }
     public void getDeptGroupUnit()
     {
        VDept  = new Vector();
        VGroup = new Vector();

        VDeptCode  = new Vector();
        VGroupCode = new Vector();

        VUnit      = new Vector();
        VUnitCode  = new Vector();

        VBlock     = new Vector();
        VBlockCode = new Vector();

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS1 = "";
               String QS2 = "";
               String QS3 = "";

               QS1 = " Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               QS2 = " Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               QS3 = " Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";

               String QS4 = " Select BlockName,Block from OrdBlock Where ShowStatus=0 Order By 2";

               ResultSet result1 = theStatement.executeQuery(QS1);
               while(result1.next())
               {
                    VDept.addElement(result1.getString(1));
                    VDeptCode.addElement(result1.getString(2));
               }
               result1.close();

               ResultSet result2 = theStatement.executeQuery(QS2);
               while(result2.next())
               {
                    VGroup.addElement(result2.getString(1));
                    VGroupCode.addElement(result2.getString(2));
               }
               result2.close();

               ResultSet result3 = theStatement.executeQuery(QS3);
               while(result3.next())
               { 
                    VUnit.addElement(result3.getString(1));
                    VUnitCode.addElement(result3.getString(2));
               }
               result3.close();

               ResultSet result4 = theStatement.executeQuery(QS4);
               while(result4.next())
               { 
                    VBlock.addElement(result4.getString(1));
                    VBlockCode.addElement(result4.getString(2));
               }
               result4.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("Dept,Group,Unit & Block :"+ex);
        }
     }
     private void parseZero()
     {
          for(int i=0;i<VRRate.size();i++)
          {
               String SRate = (String)VRRate.elementAt(i);
               if(common.toDouble(SRate)==0)
                    VRRate.setElementAt("",i);
          }
          for(int i=0;i<VRDis.size();i++)
          {
               String SDis = (String)VRDis.elementAt(i);
               if(common.toDouble(SDis)==0)
                    VRDis.setElementAt("",i);
          }
          for(int i=0;i<VRVat.size();i++)
          {
               String SVat = (String)VRVat.elementAt(i);
               if(common.toDouble(SVat)==0)
                    VRVat.setElementAt("",i);
          }
          for(int i=0;i<VRTax.size();i++)
          {
               String STax = (String)VRTax.elementAt(i);
               if(common.toDouble(STax)==0)
                    VRTax.setElementAt("",i);
          }
          for(int i=0;i<VRSur.size();i++)
          {
               String SSur = (String)VRSur.elementAt(i);
               if(common.toDouble(SSur)==0)
                    VRSur.setElementAt("",i);
          }
     }
     public boolean isStationary(String SItemCode)
     {
          String SStkGroupName     = "";
          String QS                = "";

          if(iMillCode==0)
          {
               QS =    " select InvItems.stkgroupcode,stockgroup.groupname,invitems.item_code from invitems"+
                       " inner join stockgroup on stockgroup.groupcode = invitems.stkgroupcode"+
                       " where invitems.item_code = '"+SItemCode+"'";
          }
          else
          {
               QS =    " select "+SItemTable+".stkgroupcode,stockgroup.groupname,"+SItemTable+".item_code from "+SItemTable+""+
                       " inner join stockgroup on stockgroup.groupcode = "+SItemTable+".stkgroupcode"+
                       " where "+SItemTable+".item_code = '"+SItemCode+"'";
          }
          try
          {
               ORAConnection  oraConnection = ORAConnection.getORAConnection();
               Connection     theConnection = oraConnection.getConnection();
                              theConnection . setAutoCommit(true);
               Statement      stat          = theConnection.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result         . next())
               {
                    SStkGroupName  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          if(SStkGroupName.equals("B01"))
               return true;

          return false;
     }
}
