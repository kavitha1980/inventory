package Order;

import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class RPJPdfFile
{


     String         SOrdNo    = "",SOrdDate  = "",SSupCode  = "",SSupName  = "";


     String SFile;
     String SItemTable;
     int    iMillCode;

     

     Document document;
     PdfPTable table;
     String SPdfFile= "/root/PurchaseOrder.pdf"; 

     RPJPdfFile(Document document,String SFile,String SOrdNo,String SOrdDate,String SSupCode,String SSupName,String SItemTable,int iMillCode)
     {
          this.document   =document;
          this.SFile      = SFile;
          this.SOrdNo     = SOrdNo;
          this.SOrdDate   = SOrdDate;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.SItemTable = SItemTable;
          this.iMillCode  = iMillCode;

         // getAddr();
          //setDataIntoVector();
          createPdfFile();
     }
     
   
     public void createPdfFile()
     {
          try
     {
                 System.out.println("Pdf ===>Enter into this");
		

               
               new RPJPdfClass(document,table,SFile,SOrdNo,SOrdDate,SSupCode,SSupName,SItemTable,iMillCode);

             
            
              
          }
          catch (Exception e)
          {
               e.printStackTrace();
          }
     }
}
