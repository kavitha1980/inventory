package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class RateMouse extends JInternalFrame
{
      JTextField     TMatCode,TMatName,TDesc;
      JTextField     TDraw,TMake;
      JTextField     TCatl;

      JButton        BOk,BCancel;
                  
      JPanel         TopPanel,FigurePanel,BottomPanel;
      JPanel         TopBottomPanel;
      JLayeredPane   Layer;
      Vector         VDesc,VMake,VDraw,VCatl;
      JTable         ReportTable;
      InvTableModel  dataModel;

      JComboBox      JCType;

      String str="";

      JTextField TQty    ;
      JTextField TRate   ;
      JTextField TDisPer ;
      JTextField TVatPer ;
      JTextField TTaxPer ;
      JTextField TSurPer ;

      JTextField TNet   ;
      JTextField TBasic ;
      JTextField TDisc  ;
      JTextField TVat   ;
      JTextField TTax   ;
      JTextField TSur   ;

      String SCatl="",SDraw="";

      Common common = new Common();
      Connection theConnection = null;


      RateMouse(JLayeredPane Layer,Vector VDesc,Vector VMake,Vector VDraw,Vector VCatl,JTable ReportTable,InvTableModel dataModel)
      {
          this.Layer       = Layer;
          this.VDesc       = VDesc;
          this.VMake       = VMake;
          this.VDraw       = VDraw;
          this.VCatl       = VCatl;
          this.ReportTable = ReportTable;
          this.dataModel   = dataModel;

          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
      }
      public void createComponents()
      {
          BOk           = new JButton("Okay");
          BCancel       = new JButton("Cancel");

          TopPanel      = new JPanel(true);
          TopBottomPanel= new JPanel(true);
          FigurePanel   = new JPanel(true);
          BottomPanel   = new JPanel(true);

          JCType        = new JComboBox();

          TMatCode      = new JTextField();
          TDesc         = new JTextField();
          TMatName      = new JTextField();
          TDraw         = new JTextField();
          TCatl         = new JTextField();
          TMake         = new JTextField();

          TQty    = new JTextField();
          TRate   = new JTextField();
          TDisPer = new JTextField();
          TVatPer = new JTextField();
          TTaxPer = new JTextField();
          TSurPer = new JTextField();

          TNet   = new JTextField();
          TBasic = new JTextField();
          TDisc  = new JTextField();
          TVat   = new JTextField();
          TTax   = new JTextField();
          TSur   = new JTextField();

          JCType.addItem("Basic");
          JCType.addItem("Net");

          TMatCode.setEditable(false);
          TMatName.setEditable(false);
          TCatl.setEditable(false);
          TDraw.setEditable(false);

          TQty.setEditable(false);
      }
      public void setLayouts()
      {
          setBounds(80,100,650,450);
          setResizable(true);
          setClosable(true);
          setTitle("Net Rate Calculation");
          TopPanel.setLayout(new BorderLayout());
          TopBottomPanel.setLayout(new GridLayout(6,2));

      }
      public void addComponents()
      {
          getContentPane().add("Center",TopPanel);
          getContentPane().add("South",BottomPanel);

          TopBottomPanel.add(new JLabel("Material Name"));
          TopBottomPanel.add(TMatName);
          TopBottomPanel.add(new JLabel("Material Code"));
          TopBottomPanel.add(TMatCode);
          TopBottomPanel.add(new JLabel("Description"));
          TopBottomPanel.add(TDesc);
          TopBottomPanel.add(new JLabel("Material Make"));
          TopBottomPanel.add(TMake);
          TopBottomPanel.add(new JLabel("Drawing No"));
          TopBottomPanel.add(TDraw);
          TopBottomPanel.add(new JLabel("Catalogue No"));
          TopBottomPanel.add(TCatl);

          TopPanel.add("North",JCType);
          TopPanel.add("Center",FigurePanel);
          TopPanel.add("South",TopBottomPanel);

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

      }
      public void setPresets()
      {
          int i = ReportTable.getSelectedRow();

          String SCode   = (String)ReportTable.getModel().getValueAt(i,0);
          setCDM(SCode);
          String SName   = (String)ReportTable.getModel().getValueAt(i,1);
          String SQty    = (String)ReportTable.getModel().getValueAt(i,5);
          String SRate   = (String)ReportTable.getModel().getValueAt(i,6);
          String SDisPer = (String)ReportTable.getModel().getValueAt(i,7);
          String SVatPer = (String)ReportTable.getModel().getValueAt(i,8);
          String STaxPer = (String)ReportTable.getModel().getValueAt(i,9);
          String SSurPer = (String)ReportTable.getModel().getValueAt(i,10);
          String SNet    = (String)ReportTable.getModel().getValueAt(i,16);

          TMatCode.setText(SCode);
          TMatName.setText(SName);
          TDesc.setText((String)VDesc.elementAt(i));
          TDraw.setText(SDraw);
          TCatl.setText(SCatl);
          TMake.setText((String)VMake.elementAt(i));
          TQty.setText(SQty);
          TRate.setText(SRate);
          TDisPer.setText(SDisPer);
          TVatPer.setText(SVatPer);
          TTaxPer.setText(STaxPer);
          TSurPer.setText(SSurPer);
          TNet   .setText(SNet);
          FigurePanel.removeAll();
          FigurePanel.setLayout(new BorderLayout());
          FigurePanel.add("Center",getNetPane());
          setCalculation();
      }
      public void addListeners()
      {
          BOk.addActionListener(new ActList());
          BCancel.addActionListener(new ActList());
          JCType.addItemListener(new ItList());
      }
      public class ItList implements ItemListener
      {
          public void itemStateChanged(ItemEvent ie)
          {
               FigurePanel.removeAll();
               FigurePanel.setLayout(new BorderLayout());
               FigurePanel.add("Center",getNetPane());
               updateUI();
          }
     }
     public JScrollPane getNetPane()
     {
          JPanel thePanel    = new JPanel();

          thePanel.setLayout(new GridLayout(6,4));

          thePanel.add(new JLabel("Qty"));
          thePanel.add(TQty);
          thePanel.add(new JLabel("Net Value"));
          thePanel.add(TNet);

          thePanel.add(new JLabel("Surcharge (%)"));
          thePanel.add(TSurPer);
          thePanel.add(new JLabel("Surcharge (Rs)"));
          thePanel.add(TSur);
          TSur.setEditable(false);

          thePanel.add(new JLabel("Tax (%)"));
          thePanel.add(TTaxPer);
          thePanel.add(new JLabel("Tax (Rs)"));
          thePanel.add(TTax);
          TTax.setEditable(false);

          thePanel.add(new JLabel("Vat (%)"));
          thePanel.add(TVatPer);
          thePanel.add(new JLabel("Vat (Rs)"));
          thePanel.add(TVat);
          TVat.setEditable(false);

          thePanel.add(new JLabel("Disc (%)"));
          thePanel.add(TDisPer);
          thePanel.add(new JLabel("Disc (Rs)"));
          thePanel.add(TDisc);
          TDisc.setEditable(false);

          thePanel.add(new JLabel("Basic Rate"));
          thePanel.add(TRate);
          thePanel.add(new JLabel("Basic Value"));
          thePanel.add(TBasic);

          TRate.setEditable(false);
          TBasic.setEditable(false);

          TNet.addKeyListener(new CalcList());
          TQty.addKeyListener(new CalcList());
          TTaxPer.addKeyListener(new CalcList());
          TSurPer.addKeyListener(new CalcList());
          TVatPer.addKeyListener(new CalcList());
          TDisPer.addKeyListener(new CalcList());

          return new JScrollPane(thePanel);
     }
     public class CalcList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               setCalculation();
          }
     }
     public void setCalculation()
     {
               double Q      = common.toDouble(TQty.getText());
               double N      = common.toDouble(TNet.getText());
               double D      = common.toDouble(TDisPer.getText());
               double C      = common.toDouble(TVatPer.getText());
               double S      = common.toDouble(TTaxPer.getText());
               double U      = common.toDouble(TSurPer.getText());
               double B      = 0;
               try
               {
                    B=(((N)/(1+(S/100)*(U/100+1)))/(1+C/100))/(1-D/100);
               }
               catch(Exception ex)
               {
                    B = 0;
               }
               double DV = B*D/100;
               double CV = (B-DV)*(C/100);
               double SV = (B-DV+CV)*(S/100);
               double UV = SV*U/100;

               TBasic.setText(common.getRound(B,2));
               TDisc.setText(common.getRound(DV,2));
               TVat.setText(common.getRound(CV,2));
               TTax.setText(common.getRound(SV,2));
               TSur.setText(common.getRound(UV,2));
               try
               {
                    TRate.setText(common.getRound(B/Q,3));
               }
               catch(Exception ex)
               {

               }
      }
      public class ActList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    int i = ReportTable.getSelectedRow();
                    ReportTable.getModel().setValueAt(TMatCode.getText(),i,0);
                    ReportTable.getModel().setValueAt(TMatName.getText(),i,1);
                    ReportTable.getModel().setValueAt(TRate.getText(),i,6);
                    ReportTable.getModel().setValueAt(TDisPer.getText(),i,7);
                    ReportTable.getModel().setValueAt(TVatPer.getText(),i,8);
                    ReportTable.getModel().setValueAt(TTaxPer.getText(),i,9);
                    ReportTable.getModel().setValueAt(TSurPer.getText(),i,10);
                    VDesc.setElementAt(TDesc.getText(),i);
                    VMake.setElementAt(TMake.getText(),i);
                    VDraw.setElementAt(TDraw.getText(),i);
                    VCatl.setElementAt(TCatl.getText(),i);

                    try
                    {
                    }
                    catch(Exception ex)
                    {
                        System.out.println("@ :"+ex);
                    }
               removeHelpFrame();
               }
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
               }     
          }
      }
      public void removeHelpFrame()
      {
            try
            {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
            }
            catch(Exception ex) { }
      }
      public void setCDM(String SMatCode)
      {
             try
             {
                    if(theConnection == null)
                    {
                         ORAConnection jdbc   = ORAConnection.getORAConnection();
                         theConnection        = jdbc.getConnection();
                    }
     
                    Statement theStatement    = theConnection.createStatement();
                     ResultSet res1 = theStatement.executeQuery("Select Catl,Draw From InvItems Where Item_Code='"+SMatCode+"'");
                     while(res1.next())
                     {
                             SCatl = common.parseNull(res1.getString(1));
                             SDraw = common.parseNull(res1.getString(2));
                     }
                     res1.close();
                     theStatement.close();
             }
             catch(Exception ex)
             {
                     System.out.println("@ setCDM");
                     System.out.println(ex);
             }
      }
}
