//package com.amarjothi.alert;
package Order;


/******************************************************************************************************************************
 *
 ******************************************************************************************************************************
 *  File Name             :  SSLMail
 *  Project Name          :  Amarjothi
 *  Customer Name	  :
 *  Author		  :  Tirupathi Rao
 *  Version & Date        :  v1.0 &  Jul 20, 2009
 *  File Description      :
 *  Modification History  :
 *----------------------------------------------------------------------------------------------------------------------------
 *    Version		   Date			  Description			                  Changed By
 *----------------------------------------------------------------------------------------------------------------------------
 *
 ******************************************************************************************************************************/

import javax.mail.*;
import javax.mail.internet.*;
import java.util.*;

public class SSLMail {

//    static ResourceBundle rb = ResourceBundle.getBundle("ApplicationProperties");
    static String sFromMail ="info@amarjothi.net";// rb.getString("MAIL_USER");
    static String sFromMailPwd ="master";// rb.getString("MAIL_PWD");
    static String sMailHost ="smtp.bizmail.yahoo.com";// rb.getString("MAIL_HOST");
    static String sMailPort ="465";// rb.getString("MAIL_PORT");
  
    public static void main(String[] args) {
            
        
        SSLMail.sendMail("tirupathi.rao@acropetal.com",   "Amarjothi-Order Approve Notification",
                "Your Order is Approved Successfully");
        
    }

    public synchronized static boolean sendMail(String userName,
            String passWord,
            String host,
            String port,
            String starttls,
            String auth,
            boolean debug,
            String socketFactoryClass,
            String fallback,
            String[] to,
            String[] cc,
            String[] bcc,
            String subject,
            String text) {
        Properties props = new Properties();
        //Properties props=System.getProperties();
        props.put("mail.smtp.user", userName);
        props.put("mail.smtp.host", host);
        if (!"".equals(port)) {
            props.put("mail.smtp.port", port);
        }
        if (!"".equals(starttls)) {
            props.put("mail.smtp.starttls.enable", starttls);
        }
        props.put("mail.smtp.auth", auth);
        if (debug) {
            props.put("mail.smtp.debug", "true");
        } else {
            props.put("mail.smtp.debug", "false");
        }
        if (!"".equals(port)) {
            props.put("mail.smtp.socketFactory.port", port);
        }
        if (!"".equals(socketFactoryClass)) {
            props.put("mail.smtp.socketFactory.class", socketFactoryClass);
        }
        if (!"".equals(fallback)) {
            props.put("mail.smtp.socketFactory.fallback", fallback);
        }
        try {
            Session session = Session.getDefaultInstance(props, null);
            session.setDebug(debug);
            MimeMessage msg = new MimeMessage(session);
            msg.setText(text);
            msg.setSubject(subject);
            msg.setFrom(new InternetAddress("ao@amarjothi.net"));
            for (int i = 0; i < to.length; i++) {
                msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to[i]));
            }
            for (int i = 0; i < cc.length; i++) {
                msg.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[i]));
            }
            for (int i = 0; i < bcc.length; i++) {
                msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc[i]));
            }
            msg.saveChanges();
            Transport transport = session.getTransport("smtp");
            transport.connect(host, userName, passWord);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
            return true;
        } catch (Exception mex) {
            mex.printStackTrace();
            return false;
        }
    }

    public synchronized static boolean sendMail(
            String to,
            String cc,
            String subject,
            String text) {
        String starttls = "true";
        String auth = "true";
        boolean debug = true;

        String socketFactoryClass = "javax.net.ssl.SSLSocketFactory";
        String fallback = "false";
        Properties props = new Properties();
        //Properties props=System.getProperties();
        props.put("mail.smtp.user", sFromMail);
        props.put("mail.smtp.host", sMailHost);
        if (!"".equals(sMailPort)) {
            props.put("mail.smtp.port", sMailPort);
        }
        if (!"".equals(starttls)) {
            props.put("mail.smtp.starttls.enable", starttls);
        }
        props.put("mail.smtp.auth", auth);
        if (debug) {
            props.put("mail.smtp.debug", "true");
        } else {
            props.put("mail.smtp.debug", "false");
        }
        if (!"".equals(sMailPort)) {
            props.put("mail.smtp.socketFactory.port", sMailPort);
        }
        if (!"".equals(socketFactoryClass)) {
            props.put("mail.smtp.socketFactory.class", socketFactoryClass);
        }
        if (!"".equals(fallback)) {
            props.put("mail.smtp.socketFactory.fallback", fallback);
        }
        try {
            Session session = Session.getDefaultInstance(props, null);
            session.setDebug(debug);
            MimeMessage msg = new MimeMessage(session);
            msg.setText(text);
            msg.setSubject(subject);
            msg.setFrom(new InternetAddress("ao@amarjothi.net"));

            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));


            msg.addRecipient(Message.RecipientType.CC, new InternetAddress(cc));

            msg.saveChanges();
            Transport transport = session.getTransport("smtp");
            transport.connect(sMailHost, sFromMail, sFromMailPwd);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
            return true;
        } catch (Exception mex) {
            mex.printStackTrace();
            return false;
        }
    }
    
     public synchronized static boolean sendMail(
            String to,
            String subject,
            String text) {
        String starttls = "true";
        String auth = "true";
        boolean debug = true;

        String socketFactoryClass = "javax.net.ssl.SSLSocketFactory";
        String fallback = "false";
        Properties props = new Properties();

  //       props.setProperty("proxySet","true");
//         props.setProperty("socksProxyHost","192.168.1.102");
//       props.setProperty("socksProxyPort","8080");

        
        props.put("mail.smtp.user", sFromMail);
        props.put("mail.smtp.host", sMailHost);

        if (!"".equals(sMailPort)) {
            props.put("mail.smtp.port", sMailPort);
        }
        if (!"".equals(starttls)) {
            props.put("mail.smtp.starttls.enable", starttls);
        }
        props.put("mail.smtp.auth", auth);
        if (debug) {
            props.put("mail.smtp.debug", "true");
        } else {
            props.put("mail.smtp.debug", "false");
        }
        if (!"".equals(sMailPort)) {
            props.put("mail.smtp.socketFactory.port", sMailPort);
        }
        if (!"".equals(socketFactoryClass)) {
            props.put("mail.smtp.socketFactory.class", socketFactoryClass);
        }
        if (!"".equals(fallback)) {
            props.put("mail.smtp.socketFactory.fallback", fallback);
        }

        

        try {
            Session session = Session.getDefaultInstance(props, null);
            session.setDebug(debug);
            MimeMessage msg = new MimeMessage(session);
            msg.setText(text);
            msg.setSubject(subject);
            msg.setFrom(new InternetAddress(sFromMail));

            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));



            msg.saveChanges();
            Transport transport = session.getTransport("smtp");
            transport.connect(sMailHost, sFromMail, sFromMailPwd);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
            return true;
        } catch (Exception mex) {
            mex.printStackTrace();
            return false;
        }
    }
}
