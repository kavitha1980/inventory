package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class StockGroupSearch implements ActionListener
{
     JLayeredPane   Layer;
     Vector         VSeleStkGroupCode,VStkGroupName,VStkGroupCode;
     JTextArea      AStock;

     JTextField     TIndicator;
     JButton        BOk;
     JList          BrowList,SelectedList;
     JScrollPane    BrowScroll,SelectedScroll;
     JPanel         LeftPanel,RightPanel;
     JInternalFrame SupplierFrame;
     JPanel         SFMPanel,SFBPanel;

     Vector VSelectedName,VSelectedCode;
     String str="";
     int iSFSig=0;
     ActionEvent ae;
     Common common = new Common();

     StockGroupSearch(JLayeredPane Layer,Vector VSeleStkGroupCode,JTextArea AStock,Vector VStkGroupCode,Vector VStkGroupName)
     {
          this.Layer             = Layer;
          this.VSeleStkGroupCode = VSeleStkGroupCode;
          this.AStock            = AStock;
          this.VStkGroupCode     = VStkGroupCode;
          this.VStkGroupName     = VStkGroupName;

          createComponents();
     }

     public void createComponents()
     {
          VSelectedName = new Vector();
          VSelectedCode = new Vector();
          BrowList      = new JList(VStkGroupName);
          SelectedList  = new JList();
          BrowScroll    = new JScrollPane(BrowList);
          SelectedScroll= new JScrollPane(SelectedList);
          LeftPanel     = new JPanel(true);
          RightPanel    = new JPanel(true);
          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator.setEditable(false);
          SFMPanel      = new JPanel(true);
          SFBPanel      = new JPanel(true);
          SupplierFrame = new JInternalFrame("List of StockGroup");
          SupplierFrame.show();
          SupplierFrame.setBounds(80,100,550,350);
          SupplierFrame.setClosable(true);
          SupplierFrame.setResizable(true);
          BrowList.addKeyListener(new KeyList());
          BrowList.requestFocus();
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent e)
          {
               if(VSelectedCode.size()==0)
               {
                    JOptionPane.showMessageDialog(null,"No StockGroup is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                    BrowList.requestFocus();
                    return;
               }
               BOk.setEnabled(false);
               setStockGroup();
               removeHelpFrame();
               str="";
               //((JButton)ae.getSource()).setEnabled(false);
          }
     }

     public void actionPerformed(ActionEvent ae)
     {
          this.ae = ae;

          VSelectedName = new Vector();
          VSelectedCode = new Vector();

          SelectedList.setListData(VSelectedName);

          TIndicator.setText(str);
          BOk.setEnabled(true);
          if(iSFSig==0)
          {
               SFMPanel.setLayout(new GridLayout(1,2));
               SFBPanel.setLayout(new GridLayout(1,2));
               SFMPanel.add(BrowScroll);
               SFMPanel.add(SelectedScroll);
               SFBPanel.add(TIndicator);
               SFBPanel.add(BOk);
               BOk.addActionListener(new ActList());
               SupplierFrame.getContentPane().add("Center",SFMPanel);
               SupplierFrame.getContentPane().add("South",SFBPanel);
               iSFSig=1;
          }
          removeHelpFrame();
          try
          {
               Layer.add(SupplierFrame);
               SupplierFrame.moveToFront();
               SupplierFrame.setSelected(true);
               SupplierFrame.show();
               BrowList.requestFocus();
               Layer.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
     }  

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SStkGroupName = (String)VStkGroupName.elementAt(index);
                    String SStkGroupCode = (String)VStkGroupCode.elementAt(index);
                    addStkGroupDet(SStkGroupName,SStkGroupCode);
                    str="";
                    TIndicator.setText(str);
               }

               if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
               {
                    if(VSelectedCode.size()==0)
                    {
                         JOptionPane.showMessageDialog(null,"No StockGroup is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                         BrowList.requestFocus();
                         return;
                    }

                    BOk.setEnabled(false);
                    setStockGroup();
                    removeHelpFrame();
                    str="";
                    //((JButton)ae.getSource()).setEnabled(false);
               }
          }
     }

     public void setCursor()
     {
          int index=0;
          TIndicator.setText(str);
          for(index=0;index<VStkGroupName.size();index++)
          {
               String str1 = ((String)VStkGroupName.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedValue(str1,true);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(SupplierFrame);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

     public boolean addStkGroupDet(String SStkGroupName,String SStkGroupCode)
     {
          int iIndex=VSelectedCode.indexOf(SStkGroupCode);
          if (iIndex==-1)
          {
               VSelectedName.addElement(SStkGroupName);
               VSelectedCode.addElement(SStkGroupCode);
          }
          else
          {
               VSelectedName.removeElementAt(iIndex);
               VSelectedCode.removeElementAt(iIndex);
          }
          SelectedList.setListData(VSelectedName);
          return true;
     }

     public void setStockGroup()
     {
          VSeleStkGroupCode.removeAllElements();
          AStock.setText("");
          for(int i=0;i<VSelectedCode.size();i++)
          {
               VSeleStkGroupCode.addElement((String)VSelectedCode.elementAt(i));
               AStock.append((String)VSelectedName.elementAt(i)+"\n");
          }
     }

}
