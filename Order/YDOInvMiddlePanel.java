// Used By Order,GRN  and other operations
// The Operation Varies with the state of bSig.
// Initially it is set to true once called from Non-Order activities,
// the same shall be set to false.

package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class YDOInvMiddlePanel extends JPanel
{
     JTable         ReportTable;
     JLayeredPane   DeskTop;
     JComboBox      JCDept,JCGroup,JCUnit,JCBlock,JCTax,JCUser;
     JLabel         LBasic,LDiscount,LCenVat,LTax,LSur,LNet;
     JPanel         GridPanel,BottomPanel,FigurePanel,ControlPanel;

     Common         common = new Common();
     YearlyTableModel  dataModel;

     NextField      TAdd,TLess;


     Vector         VDept,VDeptCode,VGroup,VGroupCode,VUnit,VUnitCode;
     Vector         VDesc,VMake,VDraw,VCatl,VNameCode;
     Vector         VMrsStatus;
     Vector         VBlock,VBlockName;
     Vector         VTaxClaim;
     Vector         VSUserCode,VSUserName;
     Vector         VPColour,VPSet,VPSize,VPSide,VPSlipFrNo,VPSlipToNo,VPBookFrNo,VPBookToNo;
     Vector         VCode,VName;
     Vector         VItemsStatus,VOrdSlNo;

	String 		SSupCode,SSeqId;
	Vector		theItemVector;
	String		SOrderDate;

     Object         RowData[][];

     String         ColumnData[],ColumnType[];

     boolean        bflag = false;

     int            iMillCode;
     int            iOrderStatus   = 0;
     int            iTaxAuth       = 0;
     String         SItemTable;

	YearlyQtyAmendFrame  yearlyQtyAmendFrame;

     // Constructor Method refferred from Order Collection Operations
     YDOInvMiddlePanel(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,Object RowData[][],String ColumnData[],String ColumnType[],int iMillCode,String SItemTable)
     {
          this.DeskTop        = DeskTop;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.RowData        = RowData;
          this.ColumnData     = ColumnData;
          this.ColumnType     = ColumnType;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     
          setReportTable();
     }

     YDOInvMiddlePanel(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,Object RowData[][],String ColumnData[],String ColumnType[],boolean bflag,int iOrderStatus,Vector VItemsStatus,int iMillCode,String SItemTable,String SSupCode,String SSeqId,Vector theItemVector,String SOrderDate)
     {
          this.DeskTop        = DeskTop;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.RowData        = RowData;
          this.ColumnData     = ColumnData;
          this.ColumnType     = ColumnType;
          this.iMillCode      = iMillCode;
          this.bflag          = bflag;
          this.iOrderStatus   = iOrderStatus;
          this.VItemsStatus   = VItemsStatus;
          this.SItemTable     = SItemTable;
		this.SSupCode		= SSupCode;
		this.SSeqId		= SSeqId;
		this.theItemVector	= theItemVector;
		this.SOrderDate     = SOrderDate;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     
          setReportTable();
     }

     public void createComponents()
     {
          LBasic         = new JLabel("0");
          LDiscount      = new JLabel("0");
          LCenVat        = new JLabel("0");
          LTax           = new JLabel("0");
          LSur           = new JLabel("0");
          TAdd           = new NextField();
          TLess          = new NextField();
          LNet           = new JLabel("0");
          GridPanel      = new JPanel(true);
          BottomPanel    = new JPanel();
          FigurePanel    = new JPanel();
          ControlPanel   = new JPanel();

          VOrdSlNo       = new Vector();    
          
          VDesc          = new Vector();
          VMake          = new Vector();
          VDraw          = new Vector();
          VCatl          = new Vector();

          VPColour       = new Vector();
          VPSet          = new Vector();
          VPSize         = new Vector();
          VPSide         = new Vector();
          VPSlipFrNo     = new Vector();
          VPSlipToNo     = new Vector();
          VPBookFrNo     = new Vector();
          VPBookToNo     = new Vector();

          VMrsStatus     = new Vector();
     }

     public void setLayouts()
     {
          GridPanel      . setLayout(new BorderLayout());
          BottomPanel    . setLayout(new BorderLayout());
          FigurePanel    . setLayout(new GridLayout(2,8));
          ControlPanel   . setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          VTaxClaim      = new Vector();
          VTaxClaim      . insertElementAt("Not Claimable",0);
          VTaxClaim      . insertElementAt("Claimable",1);

          FigurePanel    . add(new JLabel("Basic"));
          FigurePanel    . add(new JLabel("Discount"));
          FigurePanel    . add(new JLabel("CenVat"));
          FigurePanel    . add(new JLabel("Tax"));
          FigurePanel    . add(new JLabel("Surcharge"));
          FigurePanel    . add(new JLabel("Plus"));
          FigurePanel    . add(new JLabel("Minus"));
          FigurePanel    . add(new JLabel("Net"));
     
          FigurePanel    . add(LBasic);
          FigurePanel    . add(LDiscount);
          FigurePanel    . add(LCenVat);
          FigurePanel    . add(LTax);
          FigurePanel    . add(LSur);
          FigurePanel    . add(TAdd);
          FigurePanel    . add(TLess);
          FigurePanel    . add(LNet);
     
          BottomPanel    . add("North",FigurePanel);
          BottomPanel    . add("South",ControlPanel);
     }

     public void addListeners()
     {
          TAdd      . addKeyListener(new KeyList());
          TLess     . addKeyListener(new KeyList());
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               calc();
          }
     }

     public void calc()
     {
          double dTNet = common.toDouble(LBasic.getText())-common.toDouble(LDiscount.getText())+common.toDouble(LCenVat.getText())+common.toDouble(LTax.getText())+common.toDouble(LSur.getText());
          dTNet        = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
          LNet         . setText(common.getRound(dTNet,2));                              
     }

     public void setReportTable()
     {
          dataModel      = new YearlyTableModel(RowData,ColumnData,ColumnType,LBasic,LDiscount,LCenVat,LTax,LSur,TAdd,TLess,LNet,bflag);       
          ReportTable    = new JTable(dataModel);
          ReportTable    . setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
     
          DefaultTableCellRenderer cellRenderer   = new DefaultTableCellRenderer();
                                   cellRenderer   . setHorizontalAlignment(JLabel.RIGHT);
     
          for (int col=0;col<ReportTable.getColumnCount();col++)
          {
               if(ColumnType[col]=="N" || ColumnType[col]=="B")
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
          }
          //ReportTable.setShowGrid(false);
     
          TableColumn BlockColumn  = ReportTable.getColumn("Order Block");
          TableColumn TaxColumn    = ReportTable.getColumn("Vat");
     
          //BlockColumn              . setCellEditor(new DefaultCellEditor(JCBlock));
          //TaxColumn                . setCellEditor(new DefaultCellEditor(JCTax));
     
          setLayout(new BorderLayout());
          GridPanel.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
          GridPanel.add(new JScrollPane(ReportTable),BorderLayout.CENTER);
     
          add(BottomPanel,BorderLayout.SOUTH);
          add(GridPanel,BorderLayout.CENTER);
     
          ReportTable    . addMouseListener(new MouseList());
		ReportTable    . addKeyListener(new QtyList());
     }

     public class MouseList extends MouseAdapter
     {
          public void mouseClicked(MouseEvent me)
          {
               try
               {
                    if(bflag)
                    {
                         int i = ReportTable.getSelectedRow();
                         int iItemStatus = common.toInt((String)VItemsStatus.elementAt(i));

                         if(iItemStatus == 0)
                         {
                              if (me.getClickCount()==2)
                              {
                                   YearlyRateMouse MP = new YearlyRateMouse(DeskTop,ReportTable,dataModel);
                                   try
                                   {
                                        DeskTop   . add(MP);
                                        DeskTop   . repaint();
                                        MP        . setSelected(true);
                                        DeskTop   . updateUI();
                                        MP        . show();
                                        MP        . TRate.requestFocus();
                                   }
                                   catch(java.beans.PropertyVetoException ex){}
                              }
                         }
					else
					{
						JOptionPane.showMessageDialog(null,"The Item have already GRN","Error",JOptionPane.ERROR_MESSAGE);
					}
                    }
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
          }
     }

     public class QtyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    if(bflag)
                    {
                    	showQtyAmendFrame();
                    }
               }    
          }
     }

     private void showQtyAmendFrame()
     {
          try
          {
               DeskTop.remove(yearlyQtyAmendFrame);
               DeskTop.updateUI();
          }
          catch(Exception ex){}

          try
          {
               int iRow     = ReportTable.getSelectedRow();

               String SItemCode     = (String)ReportTable.getValueAt(iRow,0);
               String SItemName     = (String)ReportTable.getValueAt(iRow,1);
			String SOrdQty       = (String)ReportTable.getValueAt(iRow,5);

			int iIndex = getItemIndexOf(SItemCode);

			if(iIndex>=0)
			{
               	yearlyQtyAmendFrame  = new YearlyQtyAmendFrame(DeskTop,SItemCode,SItemName,SOrdQty,iMillCode,theItemVector,iIndex,ReportTable,dataModel,SOrderDate);
                    DeskTop.add(yearlyQtyAmendFrame);
                    yearlyQtyAmendFrame.show();
                    yearlyQtyAmendFrame.moveToFront();
                    DeskTop.repaint();
                    DeskTop.updateUI();
			}
          }
          catch(Exception ex){}
     }

     private int getItemIndexOf(String SItemCode)
     {
          int iIndex=-1;

          for(int i=0; i<theItemVector.size(); i++)
          {
			YearlyItemClass yearlyItemClass = (YearlyItemClass)theItemVector.elementAt(i);	

               if(SItemCode.equals(yearlyItemClass.SItemCode) && SSupCode.equals(yearlyItemClass.SSupCode) && SSeqId.equals(yearlyItemClass.SSeqId) && iMillCode==yearlyItemClass.iMillCode)
               {
                    iIndex = i;
                    break;
               }
          }
          return iIndex;
     }

     public Object[][] getFromVector()
     {
          return dataModel.getFromVector();     
     }

     public String getBlockCode(int i)                     //  3,2
     {
          Vector    VCurVector     = dataModel.getCurVector(i);
          String    str            = (String)VCurVector.elementAt(3);
          int       iid            = VBlockName.indexOf(str);
          return (iid==-1?"0":(String)VBlock.elementAt(iid));
     }

     public String getTaxClaim(int i)      //  3
     {
          Vector    VCurVector     = dataModel.getCurVector(i);
          String    str            = (String)VCurVector.elementAt(2);
          int       iid            = VTaxClaim.indexOf(str);
          return (iid==-1?"0":String.valueOf(iid));
     }
     
}

