/*
*/
package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.util.*;

import guiutil.*;
import util.*;
import java.io.*;


public class YearlyItemClass
{
     String SSupCode,SSeqId,SItemCode;
     int    iMillCode;

     Vector VPId,VPOrderNo,VPOrdQty,VPInvQty,VPDueDate;

     Common common = new Common();

     YearlyItemClass(String SSupCode,String SSeqId,String SItemCode,int iMillCode)
     {
          this.SSupCode   = SSupCode;
          this.SSeqId     = SSeqId;
          this.SItemCode  = SItemCode;
          this.iMillCode  = iMillCode;

          VPId		       = new Vector();
          VPOrderNo        = new Vector();
          VPOrdQty         = new Vector();
          VPInvQty         = new Vector();
		VPDueDate	       = new Vector();
     }

     public void setData(String SPId,String SPOrderNo,String SPOrdQty,String SPInvQty,String SPDueDate)
     {
          VPId.addElement(SPId);
          VPOrderNo.addElement(SPOrderNo);
          VPOrdQty.addElement(SPOrdQty);
          VPInvQty.addElement(SPInvQty);
		VPDueDate.addElement(SPDueDate);
     }

}

