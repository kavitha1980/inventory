/*
*/
package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.util.*;

import guiutil.*;
import util.*;
import java.io.*;


public class YearlyMrsClass
{
     String SItemCode;
     int    iMillCode;

     Vector VMrsNo,VQty,VUnitCode,VDeptCode,VGroupCode,VCatl,VDraw,VMake,VDueDate,VSlNo,VRemarks,VMrsAuthUserCode,VMrsPlanMonth,VEnquiryNo;

     Common common = new Common();

     YearlyMrsClass(String SItemCode,int iMillCode)
     {
          this.SItemCode = SItemCode;
          this.iMillCode = iMillCode;

          VMrsNo           = new Vector();
          VQty             = new Vector();
          VUnitCode        = new Vector();
          VDeptCode        = new Vector();
          VGroupCode       = new Vector();
          VCatl            = new Vector();
          VDraw            = new Vector();
          VMake            = new Vector();
          VDueDate         = new Vector();
          VSlNo            = new Vector();
          VRemarks         = new Vector();
          VMrsAuthUserCode = new Vector();
          VMrsPlanMonth    = new Vector();
          VEnquiryNo       = new Vector();
     }
     public void setData(String SMrsNo,String SQty,String SUnitCode,String SDeptCode,String SGroupCode,String SCatl,String SDraw,String SMake,String SDueDate,String SSlNo,String SRemarks,String SMrsAuthUserCode,String SMrsPlanMonth,String SEnquiryNo)
     {
          VMrsNo.addElement(SMrsNo);
          VQty.addElement(SQty);
          VUnitCode.addElement(SUnitCode);
          VDeptCode.addElement(SDeptCode);
          VGroupCode.addElement(SGroupCode);
          VCatl.addElement(SCatl);
          VDraw.addElement(SDraw);
          VMake.addElement(SMake);
          VDueDate.addElement(SDueDate);
          VSlNo.addElement(SSlNo);
          VRemarks.addElement(SRemarks);
          VMrsAuthUserCode.addElement(SMrsAuthUserCode);
          VMrsPlanMonth.addElement(SMrsPlanMonth);
          VEnquiryNo.addElement(SEnquiryNo);
     }

     public double getTotalMrsQty()
     {
         double dTQty = 0;
         
         for(int i=0; i<VMrsNo.size(); i++)
         {
              dTQty+=common.toDouble((String)VQty.elementAt(i));
         }
         return dTQty;
     }

}

