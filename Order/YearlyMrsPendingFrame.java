// Consolidated Order Placement Utility From Yearly Planning MRS //

package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;
import java.io.*;

public class YearlyMrsPendingFrame extends JInternalFrame
{
      JLayeredPane  Layer;
      Vector        VCode,VName;
      StatusPanel   SPanel;
      int           iUserCode;
      int           iMillCode;
      String        SYearCode;
      String        SItemTable,SSupTable;

      JButton       BAOrder,BMOrder,BApply;
      JComboBox     JCOrder,JCDept;
      JPanel        TopPanel,BottomPanel;
      Connection    theConnection = null;

      Vector VSelectedId,VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedDue,VSelectedDesc,VSelectedBlock,VSelectedMRSSlNo;

      Vector VMMRSId,VMMRSDesc,VMMRSBlock,VMMRSColor,VMMRSSet,VMMRSSize,VMMRSSide;
      Vector VMMRSSlipNo,VMMRSBookNo,VMMRSSlipFrNo,VMMRSCatl,VMMRSDraw,VMMRSMake,VMMRSUserCode,VMAutoOrderStatus,VMMRSType;
      Vector VSupplier,VPurchaseDate,VPurchaseRate,VDiscPer,VCenvatPer,VTaxPer,VSuPer,VNetRate,VFCatl,VFDraw,VFMake,VDept,VEntryDate;

      Vector VSelectedColor,VSelectedSet,VSelectedSize,VSelectedSide,VSelectedSlipNo,VSelectedBookNo,VSelectedSlipFrNo,VSelectedCatl,VSelectedDraw,VSelectedMake,VSelectedUserCode,VSelectedMrsType;

      Vector VMonthCode,VMonth;

      Common common = new Common();


      Vector theVector;
      Vector VItemCode,VItemName;

      Vector VColumnName,VColumnType;

      TabReport tabreport;

      Object RowData[][];

      String [] ColumnData;
      String [] ColumnType;

      ArrayList theDeptList;
      public YearlyMrsPendingFrame(JLayeredPane Layer,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable)
      {
            super(" Order For Yearly MRS");
            this.Layer      = Layer;
            this.VCode      = VCode;
            this.VName      = VName;
            this.SPanel     = SPanel;
            this.iUserCode  = iUserCode;
            this.iMillCode  = iMillCode;
            this.SYearCode  = SYearCode;
            this.SItemTable = SItemTable;
            this.SSupTable  = SSupTable;

            setMonths();
            createComponents();
            setLayouts();
            addComponents();
            addListeners();
            setDept();

      }

      public void createComponents()
      {
         BApply   = new JButton("Apply");
         TopPanel = new JPanel();

         JCDept         = new JComboBox();
         JCOrder  = new JComboBox();

         BottomPanel = new JPanel();
         BMOrder      = new JButton("Place Order Manually");
         BAOrder      = new JButton("Place Order Automatically");

         BMOrder.setEnabled(false);
         BAOrder.setEnabled(false);

      }

      public void setLayouts()
      {
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,1000,500);
      }

      public void addComponents()
      {
            JCOrder.addItem("Materialwise");
            JCOrder.addItem("SupplierWise");

            TopPanel       . add(new JLabel("Department "));
            TopPanel       . add(JCDept);


            TopPanel.add(new JLabel("Sorted On"));
            TopPanel.add(JCOrder);


            TopPanel.add(BApply);


            BottomPanel.add(BMOrder);
            BottomPanel.add(BAOrder);
   
            getContentPane().add(TopPanel,BorderLayout.NORTH);
            getContentPane().add(BottomPanel,BorderLayout.SOUTH);
      }

      public void addListeners()
      {
            BApply.addActionListener(new ApplyList());
            BMOrder.addActionListener(new ActList());
            BAOrder.addActionListener(new AutoList());
      }

      public class ApplyList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  try
                  {
                        getContentPane().remove(tabreport); 
                  }
                  catch(Exception ex){}

                  setMrsData();
                  setTabReport();


                  if(VItemCode.size()>0)
                  {
                       BMOrder.setEnabled(true);
                       BAOrder.setEnabled(true);
                  }
                  else
                  {
                       BMOrder.setEnabled(false);
                       BAOrder.setEnabled(false);
                  }
            }
      }

      public void setTabReport()
      {
            VColumnName  = new Vector();
            VColumnType  = new Vector();

            VColumnName.addElement("ItemCode");
            VColumnName.addElement("ItemName");

            VColumnName.addElement("DeptName");
            VColumnName.addElement("Catl");
            VColumnName.addElement("Draw");
            VColumnName.addElement("Make");
            VColumnName.addElement("EntryDate");



            VColumnType.addElement("S");
            VColumnType.addElement("S");

            VColumnType.addElement("S");
            VColumnType.addElement("S");
            VColumnType.addElement("S");
            VColumnType.addElement("S");
            VColumnType.addElement("S");


            for(int i=0;i<VMonth.size();i++)
            {
                VColumnName.addElement((String)VMonth.elementAt(i));
                VColumnType.addElement("N");
            }

            VColumnName.addElement("Total");
            VColumnName.addElement("Click");
            VColumnName.addElement("SupName");
            VColumnName.addElement("PurchaseDate");
            VColumnName.addElement("PurchaseRate");
            VColumnName.addElement("DiscPer");
            VColumnName.addElement("CenvatPer");
            VColumnName.addElement("TaxPer");
            VColumnName.addElement("SurPer");
            VColumnName.addElement("NetRate");

            VColumnType.addElement("N");
            VColumnType.addElement("B");
            VColumnType.addElement("S");
            VColumnType.addElement("S");
            VColumnType.addElement("N");
            VColumnType.addElement("N");
            VColumnType.addElement("N");
            VColumnType.addElement("N");
            VColumnType.addElement("N");
            VColumnType.addElement("N");

            ColumnData = getColumnData();
            ColumnType = getColumnType();

            setRowData();
            try
            {
                  getContentPane().remove(tabreport); 
            }
            catch(Exception ex){}

            try
            {
                  tabreport = new TabReport(RowData,ColumnData,ColumnType);
                  getContentPane().add(tabreport,BorderLayout.CENTER);
                  setSelected(true);
                  Layer.repaint();
                  Layer.updateUI();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      public String[] getColumnData()
      {
 
           String[] SColumnName = new String[VColumnName.size()];
           VColumnName.toArray(SColumnName);
 
           return SColumnName;
      }
 
      public String[] getColumnType()
      {
 
           String[] SColumnType = new String[VColumnType.size()];
           VColumnType.toArray(SColumnType);
 
           return SColumnType;
      }

      public void setRowData()
      {
            RowData = new Object[VItemCode.size()][ColumnData.length];
            for(int i=0;i<VItemCode.size();i++)
            {
                  String SItemCode = (String)VItemCode.elementAt(i);

                  RowData[i][0] = (String)VItemCode.elementAt(i);
                  RowData[i][1] = (String)VItemName.elementAt(i);
                  RowData[i][2]  = (String)VDept.elementAt(i);
                  RowData[i][3]  = (String)VFCatl.elementAt(i);
                  RowData[i][4]  = (String)VFDraw.elementAt(i);
                  RowData[i][5]  = (String)VFMake.elementAt(i);
                  RowData[i][6]  = (String)VEntryDate.elementAt(i);


                  int iIndex = getIndexOf(SItemCode);

                  YearlyMrsClass yearlyMrsClass = (YearlyMrsClass)theVector.elementAt(iIndex);

                  for(int k=0;k<VMonth.size();k++)
                  {
                       String SMonthCode = (String)VMonthCode.elementAt(k);

                       int iMonIndex = common.exactIndexOf(yearlyMrsClass.VMrsPlanMonth,SMonthCode);

                       if(iMonIndex==-1)
                       {
                            RowData[i][k+7] = "";
                       }
                       else
                       {
					   double dTotal = 0;
					   for(int m=0;m<yearlyMrsClass.VMrsPlanMonth.size();m++)
					   {		
                                 if(((String)yearlyMrsClass.VMrsPlanMonth.elementAt(m)).equals(SMonthCode))
						   {	
                                      dTotal += common.toDouble((String)yearlyMrsClass.VQty.elementAt(m));
						   }
					   }
                            RowData[i][k+7] = common.getRound(dTotal,3);
                       }
                  }

                  RowData[i][ColumnData.length-10] = common.getRound(yearlyMrsClass.getTotalMrsQty(),3);
                  RowData[i][ColumnData.length-9] = new Boolean(false);
                  RowData[i][ColumnData.length-8] = (String)VSupplier.elementAt(i);
                  RowData[i][ColumnData.length-7] = (String)VPurchaseDate.elementAt(i);
                  RowData[i][ColumnData.length-6]  = (String)VPurchaseRate.elementAt(i);
                  RowData[i][ColumnData.length-5]  = (String)VDiscPer.elementAt(i);
                  RowData[i][ColumnData.length-4]  = (String)VCenvatPer.elementAt(i);
                  RowData[i][ColumnData.length-3]  = (String)VTaxPer.elementAt(i);
                  RowData[i][ColumnData.length-2]  = (String)VSuPer.elementAt(i);
                  RowData[i][ColumnData.length-1]  = (String)VNetRate.elementAt(i);
            }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setSelectedRecords();
               YearlyOrderPlanFrame yearlyorderplanframe = new YearlyOrderPlanFrame(Layer,VCode,VName,VSelectedCode,VSelectedName,VSelectedQty,SPanel,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable,theVector);
               try
               {
                    removeOrderFrame(); 
                    Layer.add(yearlyorderplanframe);
                    Layer.repaint();
                    yearlyorderplanframe.setSelected(true);
                    Layer.updateUI();
                    yearlyorderplanframe.show();
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
               }
          }
     }
     public class AutoList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setSelectedRecords();
               removeOrderFrame();                                                                                                                                                                                                                              
               YearlyOrderPooling yearlyorderpooling = new YearlyOrderPooling(Layer,VCode,VName,VSelectedCode,VSelectedName,VSelectedQty,SPanel,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable,theVector);
          }
     }
     public void removeOrderFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }
     public void setSelectedRecords()
     {
            VSelectedCode = new Vector();
            VSelectedName = new Vector();
            VSelectedQty  = new Vector();

            try
            {
                  for(int i=0;i<RowData.length;i++)
                  {
                        Boolean bAppl = (Boolean)RowData[i][ColumnData.length-9];
                        if(!bAppl.booleanValue())
                              continue;

                         VSelectedCode .addElement(VItemCode.elementAt(i));
                         VSelectedName .addElement(VItemName.elementAt(i));
                         VSelectedQty  .addElement((String)RowData[i][ColumnData.length-10]);
                  }
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
     }

     private void setMonths()
     {
          try
          {
               VMonth    = new Vector();
               VMonthCode= new Vector();
               theDeptList = new ArrayList();

     
               String SMinMonth = "";
               String SMaxMonth = "";

               String QS = " Select Min(YearlyPlanningMonth),Max(YearlyPlanningMonth) from Mrs Where YearlyPlanningStatus=1 and OrderNo=0 and MillCode="+iMillCode;
     
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
     
               Statement theStatement    = theConnection.createStatement();
               ResultSet res             = theStatement.executeQuery(QS);
               while(res.next())
               {
                    SMinMonth = common.parseNull(res.getString(1));
                    SMaxMonth = common.parseNull(res.getString(2));
               }
               res.close();
     
               if(!SMinMonth.equals(SMaxMonth))
               {
                    setMonthData(SMinMonth,SMaxMonth);
               }
               else
               {
                    String SMonthName = common.getMonthName(common.toInt(SMaxMonth));
     
                    VMonthCode.addElement(SMaxMonth);
                    VMonth.addElement(SMonthName);
               }
               String QS2= " Select Dept_Code,Dept_Name from Dept";

               
               res  = theStatement.executeQuery(QS2);
               while(res.next())
               {
                    HashMap theMap =new HashMap();

                    theMap.put("DeptCode",res.getString(1));
                    theMap.put("DeptName",res.getString(2));

                    theDeptList.add(theMap);
               }
               res.close();
               theStatement.close();

          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setMonthData(String SMinMonth,String SMaxMonth)
     {
          int iDiff=0;

          iDiff = common.toInt(SMaxMonth) - common.toInt(SMinMonth);

          if(iDiff>=0)
          {
               String SMonthName = common.getMonthName(common.toInt(SMinMonth));

               VMonthCode.addElement(SMinMonth);
               VMonth.addElement(SMonthName);

               SMinMonth = String.valueOf(common.getNextMonth(common.toInt(SMinMonth)));
               setMonthData(SMinMonth,SMaxMonth);
          }
     }

     private void setMrsData()
     {
          try
          {
               VItemCode = new Vector();
               VItemName = new Vector();

               VSupplier     = new Vector();
               VPurchaseDate = new Vector();
               VPurchaseRate = new Vector();
               VDiscPer      = new Vector();
               VCenvatPer    = new Vector();
               VTaxPer       = new Vector();
               VSuPer        = new Vector();
               VNetRate      = new Vector();
               VFCatl        = new Vector();
               VFDraw        = new Vector();
               VFMake        = new Vector();
               VDept         = new Vector();
               VEntryDate    = new Vector();


               YearlyMrsClass yearlyMrsClass=null;
               theVector = new Vector();


               String QSTemp   = " Create or replace view Temp1MrsOrderV as("+
                             " Select max(OrderDate) as OrderDate,Item_Code from("+
                             " select  max(OrderDate) as OrderDate,Item_Code"+
                             " From PurchaseOrder "+        
                             " Group by Item_Code,Id,MRsNo,OrderNo"+
                             " Union"+
                             " select  max(OrderDate) as OrderDate,Item_Code"+
                             " From PYOrder "+             
                             " Group by Item_Code) group by Item_Code) ";


               String QSTemp1 ="   create or replace view TempMrsOrder22V as("+
                        "  select max(Id) as Id,OrderDate,      Item_Code from("+
                        "  select max(PYOrder.ID) as Id,Temp1MRsOrderV.OrderDate as OrderDate,Temp1MrsOrderV.Item_Code as Item_code from PYOrder"+
                        "  Inner join Temp1MRsOrderV on Temp1MrsOrderV.OrderDate=PYOrder.OrderDate"+
                        "  and Temp1MrsOrderV.Item_Code=PYOrder.Item_code"+
                        "  group by temp1MrsOrderV.Orderdate,Temp1MRsOrderV.Item_Code"+
                        "  union All"+
                        "  select Max(Purchaseorder.Id) as Id,Temp1MRsOrderV.OrderDate,Temp1MrsOrderV.Item_Code as Item_Code from PurchaseOrder"+
                        "  Inner join Temp1MrsOrderV on  Temp1MRsOrderV.OrderDate=PurchaseOrder.OrderDate"+
                        "  and Temp1MrsOrderV.Item_Code=PurchaseOrder.Item_code"+
                        "  group by temp1MrsOrderV.Orderdate,Temp1MRsOrderV.Item_Code) group by OrderDate,Item_Code) ";



               String QSTemp2 = " create or replace view Temp2MRSOrderV as ("+
                          " Select PYOrder.OrderDate as OrderDate,PYOrder.Item_Code, "+
                          " Supplier.Name,PYOrder.Rate,PYOrder.DiscPer, "+
                          " PYOrder.CenVatPer,PYOrder.TaxPer,PYOrder.SurPer, "+
                          " PYOrder.Net/PYOrder.Qty as NetRate "+
                          " From PYOrder Inner Join Supplier On Supplier.Ac_Code = PyOrder.Sup_Code "+
                          " Inner join TempMRSOrder22V on TempMRSOrder22V.OrderDate= PYOrder.Orderdate and TempMRSOrder22V.Id=PYOrder.Id "+
                          " and TempMRSOrder22V.Item_Code=PYOrder.Item_Code "+
                          " Where PYOrder.Qty > 0 And PYOrder.Net > 0 "+
                          " Union All "+
                          " Select PurchaseOrder.OrderDate as OrderDate,PurchaseOrder.Item_Code, "+
                          " Supplier.Name,PurchaseOrder.Rate,PurchaseOrder.DiscPer, "+
                          " PurchaseOrder.CenVatPer,PurchaseOrder.TaxPer,PurchaseOrder.SurPer, " +
                          " PurchaseOrder.Net/PurchaseOrder.Qty as NetRate"+
                          " From PurchaseOrder Inner Join Supplier On Supplier.Ac_Code = PurchaseOrder.Sup_Code "+
                          " Inner join TempMRSOrder22V on TempMRSOrder22V.OrderDate= PurchaseOrder.Orderdate and TempMRSOrder22V.Id=PurchaseOrder.Id"+
                          " and TempMRSOrder22V.Item_Code=PurchaseOrder.Item_Code"+
                          " Where PurchaseOrder.Qty > 0 And PurchaseOrder.Net > 0) ";

     
               String QS = " Select Mrs.Item_Code,InvItems.Item_Name,Mrs.MrsNo,Mrs.Qty,Mrs.Unit_Code,Mrs.Dept_Code,Mrs.Group_Code,"+
                           " InvItems.Catl,InvItems.Draw,Mrs.Make,Mrs.DueDate,Mrs.SlNo,Mrs.Remarks,Mrs.MrsAuthUserCode,Mrs.YearlyPlanningMonth,Mrs.EnquiryNo, "+
                           " Temp2MRsOrderV.OrderDate as LastPurchaseDate,Temp2MrsOrderV.Rate as LastPurchaseRate,"+
                           " Temp2MrsOrderV.DiscPer,Temp2MrsOrderV.CenvatPer,Temp2MrsOrderV.TaxPer,Temp2MrsOrderV.SurPer,"+
                           " Temp2MrsOrderV.Netrate,Temp2MrsOrderV.Name,Dept_Name,Mrs.MrsDate "+
                           " from Mrs "+
                           " LEFT JOIN Temp2MRSOrderV on Temp2MRSOrderV.ITem_Code=MRS.Item_code "+
                           " Inner Join InvItems on Mrs.Item_Code = InvItems.Item_Code "+
                           " Inner Join Dept on Dept.Dept_Code = Mrs.dept_Code "+

                           " and Mrs.YearlyPlanningStatus=1 and Mrs.OrderNo=0 and Mrs.MillCode="+iMillCode;

               if(!(JCDept.getSelectedItem()).equals("All"))
                    QS = QS + " and mrs.Dept_code="+getDeptCode((String)JCDept.getSelectedItem());


               if(JCOrder.getSelectedIndex()==0)
               {
                    QS = QS + " Order by Dept.Dept_Name,InvItems.Item_Name,Mrs.YearlyPlanningMonth,Mrs.MrsNo ";
               }
               else
               {
                    QS = QS + " Order by Dept.Dept_Name,Temp2MrsOrderV.Name,InvItems.Item_Name,Mrs.YearlyPlanningMonth,Mrs.MrsNo ";
               }


     
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
     
               Statement theStatement    = theConnection.createStatement();


               theStatement.executeUpdate(QSTemp);
               theStatement.executeUpdate(QSTemp1);
               theStatement.executeUpdate(QSTemp2);

               ResultSet res             = theStatement.executeQuery(QS);
               while(res.next())
               {
                    String SItemCode        = common.parseNull(res.getString(1));
                    String SItemName        = common.parseNull(res.getString(2));
                    String SMrsNo           = common.parseNull(res.getString(3));
                    String SQty             = common.parseNull(res.getString(4));
                    String SUnitCode        = common.parseNull(res.getString(5));
                    String SDeptCode        = common.parseNull(res.getString(6));
                    String SGroupCode       = common.parseNull(res.getString(7));
                    String SCatl            = common.parseNull(res.getString(8));
                    String SDraw            = common.parseNull(res.getString(9));
                    String SMake            = common.parseNull(res.getString(10));
                    String SDueDate         = common.parseNull(res.getString(11));
                    String SSlNo            = common.parseNull(res.getString(12));
                    String SRemarks         = common.parseNull(res.getString(13));
                    String SMrsAuthUserCode = common.parseNull(res.getString(14));
                    String SMrsPlanMonth    = common.parseNull(res.getString(15));
                    String SEnquiryNo       = common.parseNull(res.getString(16));
                    String SPurcDate        = common.parseNull(res.getString(17));
                    String SPurcRate        = common.parseNull(res.getString(18));
                    String SDiscPer         = common.parseNull(res.getString(19));
                    String SCenvatPer       = common.parseNull(res.getString(20));
                    String STaxPer          = common.parseNull(res.getString(21));
                    String SSurPer          = common.parseNull(res.getString(22));
                    String SNetRate         = common.parseNull(res.getString(23));
                    String SSupName         = common.parseNull(res.getString(24));
                    String SDept            = common.parseNull(res.getString(25));
                    String SEntryDate       = common.parseDate(res.getString(26));


                    int iIndex = getIndexOf(SItemCode);

                    if(iIndex==-1)
                    {
                         yearlyMrsClass = new YearlyMrsClass(SItemCode,iMillCode);
                         theVector.addElement(yearlyMrsClass);
                         iIndex = theVector.size()-1;
                         VItemCode.addElement(SItemCode);
                         VItemName.addElement(SItemName);
                         VPurchaseDate.addElement(common.parseDate(SPurcDate));
                         VPurchaseRate.addElement(SPurcRate);
                         VDiscPer     .addElement(SDiscPer);
                         VCenvatPer   .addElement(SCenvatPer);
                         VTaxPer      .addElement(STaxPer);
                         VSuPer       .addElement(SSurPer);
                         VNetRate     .addElement(SNetRate);
                         VSupplier    .addElement(SSupName);
                         VFCatl       .addElement(SCatl);
                         VFDraw       .addElement(SDraw);
                         VFMake       .addElement(SMake);
                         VDept        .addElement(SDept);
                         VEntryDate   .addElement(SEntryDate);
                    }
                    yearlyMrsClass = (YearlyMrsClass)theVector.elementAt(iIndex);
                    yearlyMrsClass.setData(SMrsNo,SQty,SUnitCode,SDeptCode,SGroupCode,SCatl,SDraw,SMake,SDueDate,SSlNo,SRemarks,SMrsAuthUserCode,SMrsPlanMonth,SEnquiryNo);
               }
               res.close();
               theStatement.close();
     
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private int getIndexOf(String SItemCode)
     {
          int iIndex =-1;

          for(int i=0; i<theVector.size(); i++)
          {
               YearlyMrsClass yearlyMrsClass = (YearlyMrsClass)theVector.elementAt(i);
               if(((yearlyMrsClass.SItemCode).equals(SItemCode)) && (yearlyMrsClass.iMillCode==iMillCode))
               {
                    iIndex=i;
                    break;
               }
          }
          return iIndex;
     }
     public void setDept()
     {
          JCDept.addItem("All");

          for(int i=0; i<theDeptList.size(); i++)
          {
               HashMap theMap =(HashMap)theDeptList.get(i);

               JCDept.addItem((String)theMap.get("DeptName"));
          }
     }

     private String getDeptCode(String  SDeptName)
     {
          String SDeptCode="-1";

          for(int i=0; i<theDeptList.size(); i++)
          {
               HashMap theMap =(HashMap)theDeptList.get(i);
               if(((String)theMap.get("DeptName")).equals(SDeptName))
               {
                    SDeptCode = (String)theMap.get("DeptCode");
                    break;
               }
          }
          return SDeptCode;
     }


}

