package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class YearlyOrderAbsFrameGst extends JInternalFrame
{
     JComboBox      JCOrder;
     JTextField     TFile;
     JButton        BApply,BCancel,BMail,BCreatePDF;
     JPanel         TopPanel;
     JPanel         BottomPanel;
     JLayeredPane   DeskTop;

     NextField      TNo;
     StatusPanel    SPanel;
     TabReport      tabreport;
     DateField      TStDate;
     DateField      TEnDate;
     Common         common = new Common();
     String         SStateName,SStateCode,GSTId;
     Vector         VSupName,VBasic,VNet,VSupCode,VEMail,VContractPerson,VMobileNo,VSeqId;
     Vector         VCode,VName,VNameCode;
     Vector         VSupContPerson,VSupMobileNo,VSupSupCode;
    
     Object         RowData[][];
     String         ColumnData[] = {"Mark to Print","Supplier","Basic","Net","EMail-Id","ContPerson","MobileNo","Id"};
     String         ColumnType[] = {"B","S","N","N","S","S","S","S"};

     String         SFinYear  = "";

     int            iUserCode,iMillCode,iAuthCode;	
     String         SSupTable,SItemTable,SYearCode;

     FileWriter     FW,FTxt;

     JRadioButton   RWithMail,RWithOutMail,RPrintMail;
     int iMailStatus=0;

     String SShortName="";

     Vector         VOrderQty,VOdInVQty;

     int            iOrderStatus  = 0;
     Vector         VItemsStatus;
     String SOrdNo;
     Document document;
     PdfPTable table;
  // String SPdfFile= "/root/YearlyOrder.pdf";
     String SPdfFile = common.getPrintPath()+"/YearlyOrder.pdf";
     public YearlyOrderAbsFrameGst(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,String SFinYear,int iUserCode,int iAuthCode,int iMillCode,String SSupTable,String SItemTable,String SYearCode)
     {
         super("Abstract on Orders Placed During a Period");
         this.DeskTop    = DeskTop;
         this.VCode      = VCode;
         this.VName      = VName;
         this.VNameCode  = VNameCode;
         this.SPanel     = SPanel;
         this.SFinYear   = SFinYear;
         this.iUserCode  = iUserCode;
         this.iAuthCode  = iAuthCode;
         this.iMillCode  = iMillCode;
         this.SSupTable  = SSupTable;
         this.SItemTable = SItemTable;
         this.SYearCode  = SYearCode;

         SShortName = getShortName();

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     public void createComponents()
     {
         TStDate     = new DateField();
         TEnDate     = new DateField();
         BApply      = new JButton("Apply");
         BMail       = new JButton("Mail Marked Orders");
         BCreatePDF  = new JButton(" Create PDF");
         BCancel     = new JButton("Cancel");
         TFile       = new JTextField(10);
         TNo         = new NextField(5); 
         TopPanel    = new JPanel();
         BottomPanel = new JPanel();
         JCOrder     = new JComboBox();

         TStDate.setTodayDate();
         TEnDate.setTodayDate();

         RPrintMail   = new JRadioButton("Print List",true);
         RWithMail    = new JRadioButton("Sending Mail List",false);
         RWithOutMail = new JRadioButton("Pending Mail List",false);
     }

     public void setLayouts()
     {
         TopPanel.setLayout(new GridLayout(1,5));
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,850,500);
     }

     public void addComponents()
     {
         JCOrder.addItem("Supplierwise");
          
         TopPanel.add(new JLabel("Sorted On"));
         TopPanel.add(JCOrder);
         TopPanel.add(TStDate);
         TopPanel.add(TEnDate);
         TopPanel.add(BApply);
         //TopPanel.add(RPrintMail);
         //TopPanel.add(RWithOutMail);
         //TopPanel.add(RWithMail);

	 BottomPanel.add(new JLabel("Press F3 for Amend"));
         BottomPanel.add(TFile);
         BottomPanel.add(BCreatePDF); 
         //BottomPanel.add(BMail);
         BottomPanel.add(BCancel);

         //BMail.setEnabled(false);

         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
         BApply.addActionListener(new ApplyList(this));
         BCreatePDF.addActionListener(new  PdfList());

         //BMail.addActionListener(new MailList());

         //RPrintMail.addActionListener(new ActList1());
         //RWithMail.addActionListener(new ActList1());
         //RWithOutMail.addActionListener(new ActList1());
     }

     /*private class ActList1 implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==RPrintMail)
               {
                    RPrintMail.setSelected(true);
                    RWithMail.setSelected(false);
                    RWithOutMail.setSelected(false);
                    BMail.setEnabled(false);
                    iMailStatus = 0;
               }
               if(ae.getSource()==RWithOutMail)
               {
                    RWithOutMail.setSelected(true);
                    RPrintMail.setSelected(false);
                    RWithMail.setSelected(false);
                    BMail.setEnabled(true);
                    iMailStatus = 1;
               }
               if(ae.getSource()==RWithMail)
               {
                    RWithMail.setSelected(true);
                    RPrintMail.setSelected(false);
                    RWithOutMail.setSelected(false);
                    BMail.setEnabled(true);
                    iMailStatus = 2;
               }
          }
     }*/

     /*public class MailList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               String StDate  = TStDate.TYear.getText()+TStDate.TMonth.getText()+TStDate.TDay.getText();
               String EnDate  = TEnDate.TYear.getText()+TEnDate.TMonth.getText()+TEnDate.TDay.getText();

               String SFile = TFile.getText();
     
               if((SFile.trim()).length()==0)
                    SFile = "1.prn";
               try
               {
                       FW       = new FileWriter(common.getPrintPath()+SFile);

               }
               catch(Exception ex){}

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][0];

                    if(Bselected.booleanValue())
                    {
                         String SSupCode   = (String)VSupCode    .elementAt(i);
                         String SSupName   = (String)VSupName    .elementAt(i);
                         String SSeqId     = (String)VSeqId      .elementAt(i);

                         String SMill      = "";

                         String SMail      =  (String)VEMail      .elementAt(i);

//                         String SPdfFile = common.getMailPath()+SSupName+"_"+SSeqId+".pdf";

                         try                        
                         {
                              FTxt = new FileWriter(common.getMailPath()+SSupName+"_"+SSeqId+".html");
                              FTxt . write("<html><body><PRE>");
                         }catch(Exception ex){ex.printStackTrace();}


                         if(iMillCode==0)
                         {
                              try
                              {
                                    new YearlyOrderPrint(FTxt,SSupCode,SSupName,StDate,EnDate,SSeqId);
                              }
                              catch(Exception ex)
                              {
                                   System.out.println("From OrderAbs "+ex);
                              }
                         }
                         try
                         {
                              FTxt.write("</PRE></body></html>");
                              FTxt.close();

                              String sCc=SMail;
                              String sToMail=SMail;

                              String sSubject="Amarjothi Purchase Order";
                              String sText="Yearly Order with Delivery Schedule  From : "+SMill+" \n For Order Details Check :"+SMail+" \n If not available please check spam folder";
                              String   sAttachment= common.getMailPath()+SSupName+"_"+SSeqId+".html";

                              int iIndex = SMail.indexOf("@");

                              if(iIndex!=-1)
                              {
                                   SendMailAttach.createMail(sCc,  sToMail,  sSubject,  sText,  sAttachment);
                                   updateMail(SSeqId);
                              }

                              String SMessage = " "+SMill+" Yearly Order  has been placed . Pls check ur E-Mail Id for further details: "+sToMail+" ";

                              insertSMS(SSupCode,SMessage);

                         }catch(Exception ex){ex.printStackTrace();}
                    }
               }
               try
               {
                    FW.close();
               }
               catch(Exception ex){}

                getMobileNoAndContactPerson();

                setDataIntoVector();
                setRowData();
                try
                {
                     getContentPane().remove(tabreport);
                }
                catch(Exception ex){}
                try
                {
                   tabreport   = new TabReport(RowData,ColumnData,ColumnType);
                   getContentPane().add(tabreport,BorderLayout.CENTER);
                   setSelected(true);
                   DeskTop     .repaint();
                   DeskTop     .updateUI();
                }
                catch(Exception ex)
                {
                   System.out.println(ex);
                }
          }

     }*/

     public class ApplyList implements ActionListener
     {
	  YearlyOrderAbsFrameGst yearlyOrderAbsFrameGst;

          public ApplyList(YearlyOrderAbsFrameGst yearlyOrderAbsFrameGst)
          {
               this.yearlyOrderAbsFrameGst = yearlyOrderAbsFrameGst;
          }

          public void actionPerformed(ActionEvent ae)
          {
		setApplyData(yearlyOrderAbsFrameGst);
          }
     }

     public void setApplyData(YearlyOrderAbsFrameGst yearlyOrderAbsFrameGst)
     {
          getMobileNoAndContactPerson();
          setDataIntoVector();
          setRowData();
          try
          {
               getContentPane().remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
               tabreport   = new TabReport(RowData,ColumnData,ColumnType);
               getContentPane().add(tabreport,BorderLayout.CENTER);
               if(iAuthCode>1)
               {
                    tabreport.ReportTable.addKeyListener(new KeyList(yearlyOrderAbsFrameGst));
               }
               setSelected(true);
               DeskTop     .repaint();
               DeskTop     .updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public class KeyList extends KeyAdapter
     {
	  YearlyOrderAbsFrameGst yearlyOrderAbsFrameGst;

          public KeyList(YearlyOrderAbsFrameGst yearlyOrderAbsFrameGst)
          {
               this.yearlyOrderAbsFrameGst  = yearlyOrderAbsFrameGst;
          }

          public void keyPressed(KeyEvent ke)
          {
	       if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    try	
                    {
                         int       i         = tabreport.ReportTable.getSelectedRow();
                         String SSupCode     = (String)VSupCode    .elementAt(i);
                         String SSupName     = (String)VSupName    .elementAt(i);
                         String SSeqId       = (String)VSeqId      .elementAt(i);

			 int iCheck = getGstCheck(SSupCode,SSeqId);
			 if(iCheck>0)
			 {
	    	    	      JOptionPane.showMessageDialog(null,"This Row Can't be Amended. Use Order Amendment","Information",JOptionPane.INFORMATION_MESSAGE);
		    	      return;
			 }
			 else
			 {
                              getOrderDetails(SSupCode,SSeqId);
                              setOrderDetails();
                              YearlyOrderAmendFrameGst yearlyorderamendframegst = new YearlyOrderAmendFrameGst(DeskTop,VCode,VName,VNameCode,SPanel,true,1,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable,SSupCode,SSeqId,yearlyOrderAbsFrameGst);
                                             yearlyorderamendframegst	.fillData(SSupCode,SSeqId,iOrderStatus,VItemsStatus,SItemTable,SSupTable);
                                             DeskTop             	. add(yearlyorderamendframegst);
                                             yearlyorderamendframegst   . show();
                                             yearlyorderamendframegst   . moveToFront();
                                             DeskTop             	. repaint();
                                             DeskTop             	. updateUI();
			 }
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
               }
          } 
     }

     public int getGstCheck(String SSupCode,String SSeqId)
     {
	  int iChk = 0;

          ResultSet result  = null;

          String QString = " Select Count(*) from PurchaseOrder Where GstStatus=1 and OrderTypeCode=2 and Sup_Code='"+SSupCode+"'"+
			   " and SeqId="+SSeqId+" And Qty > 0 and MillCode = "+iMillCode;

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
                              result        =  stat.executeQuery(QString);

               while(result.next())
               {
                    iChk = result.getInt(1);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

	  return iChk;
     }

     public void getOrderDetails(String SSupCode,String SSeqId)
     {
          VOrderQty      = new Vector();
          VOdInVQty      = new Vector();

          ResultSet result  = null;

          String QString =    " SELECT PurchaseOrder.Item_Code,sum(PurchaseOrder.Qty),sum(PurchaseOrder.InvQty) "+
                              " FROM PurchaseOrder INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code "+
                              " WHERE PurchaseOrder.OrderTypeCode=2 and PurchaseOrder.Sup_Code='"+SSupCode+"'"+
			      " and PurchaseOrder.SeqId="+SSeqId+" And PurchaseOrder.Qty > 0 "+
			      " and purchaseorder.millcode = "+iMillCode+
			      " group by PurchaseOrder.Item_Code "+
                              " ORDER BY PurchaseOrder.Item_Code ";
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
                              result        =  stat.executeQuery(QString);

               while(result.next())
               {
                    VOrderQty      . addElement(result.getString(2));
                    VOdInVQty      . addElement(result.getString(3));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setOrderDetails()
     {
               VItemsStatus   = new Vector();

               iOrderStatus   = 0;
          int  iZeroCount     = 0;
          int  iOneCount      = 0;

          for(int i=0;i<VOrderQty.size();i++)
          {
               double iQty    = common.toDouble((String)VOrderQty.elementAt(i));
               double iInvQty = common.toDouble((String)VOdInVQty.elementAt(i));
               if(iInvQty == 0)
               {
                    VItemsStatus.addElement("0");
                    iZeroCount++;
               }

               if((iQty>iInvQty || iQty<iInvQty) && iInvQty!=0 )
               {
                    VItemsStatus.addElement("2");
                    iOrderStatus = 2;
               }

               if(iQty==iInvQty)
               {
                    VItemsStatus.addElement("1");
                    iOneCount++;
               }
          }
          if(iOrderStatus != 2)
          {
               if(iZeroCount == VOrderQty.size())
               {
                    iOrderStatus = 0;
               }
               else
               if(iOneCount  == VOrderQty.size())
               {
                    iOrderStatus = 1;
               }
               else
               {
                    iOrderStatus = 2;
               }
          }
     }

     public void setDataIntoVector()
     {
          VSupName     = new Vector();
          VBasic       = new Vector();
          VNet         = new Vector();
          VSupCode     = new Vector();
          VEMail       = new Vector();
          VSeqId       = new Vector();
          VContractPerson = new Vector();
          VMobileNo       = new Vector();

          String StDate  = TStDate.toNormal();
          String EnDate  = TEnDate.toNormal();

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();

              String QString = getQString(StDate,EnDate);
              ResultSet res  = stat.executeQuery(QString);
              while (res.next())
              {
  
                    String SSupCode=common.parseNull(res.getString(4)); 
                    String SSeqId=common.parseNull(res.getString(8)); 

                    int iCheck = getGstCheck(SSupCode,SSeqId);
					 if(iCheck>0)
                      continue;

                   VSupName       .addElement(common.parseNull(res.getString(1)));
                   VBasic         .addElement(common.parseNull(res.getString(2)));
                   VNet           .addElement(common.parseNull(res.getString(3)));
                   VSupCode       .addElement(common.parseNull(res.getString(4)));
                   VEMail         .addElement(common.parseNull(res.getString(5)));
                   VContractPerson.addElement(common.parseNull(res.getString(6)));
                   VMobileNo      .addElement(common.parseNull(res.getString(7)));
                   VSeqId         .addElement(common.parseNull(res.getString(8)));

                   
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
         RowData     = new Object[VSupName.size()][8];
         for(int i=0;i<VSupName.size();i++)
         {

               RowData[i][0] = new Boolean(false);
               RowData[i][1] = (String)VSupName.elementAt(i);
               RowData[i][2] = (String)VBasic.elementAt(i);
               RowData[i][3] = (String)VNet.elementAt(i);
               RowData[i][4] = common.parseNull((String)VEMail.elementAt(i));
               RowData[i][5] = common.parseNull((String)VContractPerson.elementAt(i));
               RowData[i][6] = common.parseNull((String)VMobileNo.elementAt(i));
               RowData[i][7] = (String)VSeqId.elementAt(i);       

         }  
     }

     public String getQString(String StDate,String EnDate)
     {
          String QString  = "";

          QString = " SELECT "+SSupTable+".Name, Sum(PurchaseOrder.qty*rate) AS Expr1, Sum(PurchaseOrder.Net),PurchaseOrder.Sup_Code,"+
		    " PartyMaster.EMail,PartyMaster.ContPerson1,PartyMaster.PurMobileNo,PurchaseOrder.SeqId "+
                    " FROM PurchaseOrder INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code "+
                    " Inner Join PartyMaster On PartyMaster.PartyCode="+SSupTable+".Ac_Code "+
                    " Where PurchaseOrder.MillCode="+iMillCode+" and purchaseOrder.OrderTypeCode=2 "+
               	    " and PurchaseOrder.OrderDate >= '"+StDate+"' and PurchaseOrder.OrderDate <='"+EnDate+"'"+
                    " GROUP BY  "+SSupTable+".Name,PurchaseOrder.Sup_Code,PartyMaster.EMail,PartyMaster.ContPerson1, "+
		    " PartyMaster.PurMobileNo,PurchaseOrder.SeqId Order by "+SSupTable+".Name ";

          return QString;
     }

     public String getShortName()
     {
          String SSName="";

          try
          {
                  String QS = " Select ShortName from Mill Where MillCode="+iMillCode;
      
                  ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                  Connection     theConnection  =  oraConnection.getConnection();               
                  Statement      stat           =  theConnection.createStatement();
                  ResultSet      res            =  stat.executeQuery(QS);

                  while(res.next())
                  {
                       SSName = res.getString(1);
                  }
                  res.close();
                  stat.close();
          }
          catch(Exception e)
          {
              System.out.println(e);
          }

          return SSName;
     }

     private void updateMail(String SSeqId)
     {
            try
            {
                    String QS = " Update PurchaseOrder set MailStatus=1 Where SeqId='"+SSeqId+"' and MillCode="+iMillCode+" and OrderTypeCode=2 ";
        
        
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                    Connection     theConnection  =  oraConnection.getConnection();               
                    Statement      stat           =  theConnection.createStatement();
                                   stat.execute(QS);

                    stat.close();
            }
            catch(Exception e)
            {
                System.out.println(e);
            }

     }
     private void insertSMS(String SCustomerCode,String SMessage)
     {
            try
            {
                    String QS = " insert into Tbl_Alert_Collection (CollectionId,StatusId,AlertMessage,CustomerCode,Status,SMSType) Values(alertCollection_seq.nextval,'STATUS9','"+SMessage+"','"+SCustomerCode+"',0,1) ";
        

                    ORAConnection3  oraConnection  =  ORAConnection3.getORAConnection();
                    Connection     theConnection  =  oraConnection.getConnection();               
                    Statement      stat           =  theConnection.createStatement();
                                   stat.execute(QS);

                    stat.close();
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
     }

     public String getContractPerson(String SSupCode)
     {
        int iIndex=-1;
        iIndex = VSupSupCode.indexOf(SSupCode);
        if(iIndex!=-1)
                return common.parseNull((String)VSupContPerson.elementAt(iIndex));
        else
                return "";

     }
     public String getMobileNo(String SSupCode)
     {
        int iIndex=-1;
        iIndex = VSupSupCode.indexOf(SSupCode);
        if(iIndex!=-1)
                return common.parseNull((String)VSupMobileNo.elementAt(iIndex));
        else
                return "";

     }

     public void getMobileNoAndContactPerson()
     {
          VSupContPerson = new Vector();
          VSupMobileNo   = new Vector();
          VSupSupCode    = new Vector();


          String QS = " Select ContPerson1,PurMobileNo,PartyCode from PartyMaster ";

          System.out.println(QS);
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(QS);

               while (res.next())
               {
                    VSupContPerson.addElement(common.parseNull(res.getString(1)));
                    VSupMobileNo.addElement(common.parseNull(res.getString(2)));
                    VSupSupCode.addElement(common.parseNull(res.getString(3)));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

 //pdf List

    public class PdfList implements ActionListener
    {
         public void actionPerformed(ActionEvent ae)
         {
               String SFile = TFile.getText();

               String StDate  = TStDate.toNormal();
               String EnDate  = TEnDate.toNormal();

               try
               {
                    document = new Document(PageSize.A4);
		    PdfWriter.getInstance(document, new FileOutputStream(SPdfFile));
	            document.open();
               }
               catch(Exception ex){}

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][0];

                    if(Bselected.booleanValue())
                    {
                         String SSupCode   = (String)VSupCode    .elementAt(i);
                         String SSupName   = (String)VSupName    .elementAt(i);
                         String SSeqId     = (String)VSeqId      .elementAt(i);

                       

					/* int iCheck = getGstCheck(SSupCode,SSeqId);
					 if(iCheck>0)
                         {  
                         JOptionPane.showMessageDialog(null,"PDF Does Not Generated ","Information",JOptionPane.INFORMATION_MESSAGE);
		    	         continue;
                         }*/
                         //String SPdfFile = common.getMailPath()+SShortName+"_"+SSeqId+".pdf";

                         if(iMillCode==0)
                         {
                              try
                              {
                                   new YearlyOrderGSTPdf(document,SOrdNo,SPdfFile,SSupCode,SSupName,StDate,EnDate,SSeqId);
                                   //new YearlyOrderPdf(document,SPdfFile,SSupCode,SSupName,StDate,EnDate,SSeqId);
                              }
                              catch(Exception ex)
                              {
                                   System.out.println("From YearlyOrderAbs "+ex);
                              }
                         }

                         if(iMillCode==1)
                         {
                              try
                              {
                                   new DyeingYearlyOrderGSTPdf(document,SPdfFile,SSupCode,SSupName,StDate,EnDate,SSeqId,iMillCode);
                              }
                              catch(Exception ex)
                              {
                                   System.out.println("From YearlyOrderAbs "+ex);
                              }
                         }
                         try
                         {
                              //document.close();
                         }catch(Exception ex){ex.printStackTrace();}

                       
                    }

                

               }

             
                    try{
                                   document.close();
				File theFile   = new File(SPdfFile);
				Desktop        . getDesktop() . open(theFile);
               		 }catch(Exception ex){}
         }

    }
 

}


               
          
     




