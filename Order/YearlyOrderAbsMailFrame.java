package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class YearlyOrderAbsMailFrame extends JInternalFrame
{
     JComboBox      JCOrder;
     JTextField     TFile;
     JButton        BApply,BPrint,BCancel,BMail;
     JPanel         TopPanel;
     JPanel         BottomPanel;
     JLayeredPane   DeskTop;

     NextField      TNo;
     StatusPanel    SPanel;
     TabReport      tabreport;
     DateField      TStDate;
     DateField      TEnDate;
     Common         common = new Common();

     Vector         VSupName,VBasic,VDisc,VCenVat,VTax,VSur,VAdd,VLess,VNet,VSupCode,VSeq;
     Vector         VCode,VName,VBlock,VBlockCode;
    
     Object         RowData[][];
     String         ColumnData[] = {"Mark to Print","Supplier","Basic","Discount","CenVat","Tax","Surcharge","Plus","Minus","Net","Id"};
     String         ColumnType[] = {"B","S","N","N","N","N","N","N","N","N","S","S"};
     String         SStDate,SEnDate;
     String         SFinYear  = "";
     boolean        bflag     = true;

     int            iUserCode,iMillCode;

     FileWriter     FW,FTxt;

     public YearlyOrderAbsMailFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,String SFinYear,int iUserCode,int iMillCode)
     {
         super("Abstract on Orders Placed During a Period");
         this.DeskTop    = DeskTop;
         this.VCode      = VCode;
         this.VName      = VName;
         this.SPanel     = SPanel;
         this.iUserCode  = iUserCode;
         this.iMillCode  = iMillCode;
         this.SFinYear   = SFinYear;

         bflag = getReportYear(SFinYear);
         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     public void createComponents()
     {
         TStDate     = new DateField();
         TEnDate     = new DateField();
         BApply      = new JButton("Apply");
         BPrint      = new JButton("Print Marked Orders");
         BMail       = new JButton("Mail Marked Orders");
         BCancel     = new JButton("Cancel");
         TFile       = new JTextField(10);
         TNo         = new NextField(5); 
         TopPanel    = new JPanel();
         BottomPanel = new JPanel();
         JCOrder     = new JComboBox();

         TStDate.setTodayDate();
         TEnDate.setTodayDate();
     }

     public void setLayouts()
     {
         TopPanel.setLayout(new FlowLayout());
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,650,500);
     }

     public void addComponents()
     {
          
         JCOrder.addItem("Order No");
         JCOrder.addItem("Supplierwise");

         TopPanel.add(TNo);
         TopPanel.add(new JLabel("Sorted On"));
         TopPanel.add(JCOrder);
         TopPanel.add(TStDate);
         TopPanel.add(TEnDate);
         TopPanel.add(BApply);

         BottomPanel.add(TFile);
         BottomPanel.add(BPrint);
         BottomPanel.add(BMail);
         BottomPanel.add(BCancel);

         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
         BApply.addActionListener(new ApplyList());
         BPrint.addActionListener(new PrintList());
//         BMail.addActionListener(new MailList());
     }

/*   private class MailList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                    Connection     theConnection  =  oraConnection.getConnection();               
                    Statement      stat           =  theConnection.createStatement();

                    for(int i=0;i<RowData.length;i++)
                    {
                         Boolean Bselected = (Boolean)RowData[i][0];
                         if(Bselected.booleanValue())
                         {
                              String SOrdNo     = "";
                              String SOrdDate   = "";
                              String SSupCode   = (String)VSupCode.elementAt(i);
                              String SSupName   = (String)VSupName.elementAt(i);
                              String SSeq       = (String)VSeq.elementAt(i);

                              try
                              {
                                   String     SFile    = "mail"+SOrdNo+".html";
                                   FileWriter FW       = new FileWriter(common.getPrintPath()+SFile);
                                   MailOrder  OP       = new MailOrder(FW,SOrdNo,SOrdDate,SSupCode,SSupName);
                                   FW.close();
                                   String    QS = "Insert Into MailDetails (Ac_Code,MailFile,OrderNo,OrderBlock,Status,Seq) Values (";
                                             QS = QS+"'"+SSupCode+"',";
                                             QS = QS+"'"+SFile+"',";
                                             QS = QS+"0"+SOrdNo+",";
                                             QS = QS+"0"+SBlockCode+",";
                                             QS = QS+"0)";
                                   stat.execute(QS);
                              }
                              catch(Exception ex)
                              {
                                   System.out.println("From OrderAbs "+ex);
                              }
                         }
                    }
               }
               catch(Exception ex){}
          }
     } */

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               String SFile = TFile.getText();
     
               if((SFile.trim()).length()==0)
                    SFile = "1.prn";

               try
               {
                     FW       = new FileWriter(common.getPrintPath()+SFile);
               }
               catch(Exception ex){}

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][0];

                    if(Bselected.booleanValue())
                    {
                         String SOrdNo     = "";
                         String SOrdDate   = "";

                         String SSupCode   = (String)VSupCode    .elementAt(i);
                         String SSupName   = (String)VSupName    .elementAt(i);
                         try
                         {
                              FTxt = new FileWriter(common.getMailPath()+SSupName+"_"+SOrdNo+".html");
                              FTxt . write("<html><body><PRE>");
                         }catch(Exception ex){ex.printStackTrace();}

                         if(!bflag)
                         {
                              String SBlock     = (String)VBlock.elementAt(i);
                              String SBlockCode = (String)VBlockCode.elementAt(i);

                              if(iMillCode==0)
                              {
                                   try
                                   {
                                        new OrderPrint(FW,SOrdNo,SBlock,SBlockCode,SOrdDate,SSupCode,SSupName);
                                   }
                                   catch(Exception ex)
                                   {
                                        System.out.println("From OrderAbs "+ex);
                                   }
                              }
                              else
                              {
                                   try
                                   {
                                        new DyeingOrderPrint(FW,SOrdNo,SBlock,SBlockCode,SOrdDate,SSupCode,SSupName);
                                   }
                                   catch(Exception ex)
                                   {
                                        System.out.println("From OrderAbs "+ex);
                                   }
                              }
                         }
                         else
                         {
                              if(iMillCode==0)
                              {
                                   try
                                   {
                                        new NewOrderPrint(FW,SOrdNo,SOrdDate,SSupCode,SSupName);

                                        new OrderPrintTextFile(FTxt,SOrdNo,SOrdDate,SSupCode,SSupName);
                                   }
                                   catch(Exception ex)
                                   {
                                        System.out.println("From OrderAbs "+ex);
                                   }
                              }
                              else
                              {
                                   try
                                   {
                                        new NewDyeingOrderPrint(FW,SOrdNo,SOrdDate,SSupCode,SSupName);
                                   }
                                   catch(Exception ex)
                                   {
                                        System.out.println("From OrderAbs "+ex);
                                   }
                              }
                         }

                         try
                         {
                              FTxt.write("</PRE></body></html>");
                              FTxt.close();


                              String sCc="smohanr2006@yahoo.co.in";
                              String sToMail="smohanr2006@yahoo.co.in";
                              String sSubject="Amarjothi Testing";
                              String sText="Amarjothi";
                              String   sAttachment= common.getMailPath()+SSupName+"_"+SOrdNo+".html";
            
                              SendMailAttach.createMail(sCc,  sToMail,  sSubject,  sText,  sAttachment);


                         }catch(Exception ex){ex.printStackTrace();}
                    }
               }
               try
               {
                    FW.close();
               }
               catch(Exception ex){}
               showFileView(common.getPrintPath()+SFile);
          }
     }

     public void showFileView(String SFileName)
     {
          JInternalFrame jf = new JInternalFrame(SFileName);
          Notepad        np = new Notepad(new File(SFileName),SPanel);

          jf.getContentPane().add("Center",np);
          jf.show();
          jf.setBounds(80,100,550,350);
          jf.setClosable(true);
          jf.setMaximizable(true);

          try
          {
               DeskTop.add(jf);
               jf.moveToFront();
               jf.setSelected(true);
               DeskTop.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                  tabreport   = new TabReport(RowData,ColumnData,ColumnType);
                  getContentPane().add(tabreport,BorderLayout.CENTER);
                  setSelected(true);
                  DeskTop     .repaint();
                  DeskTop     .updateUI();
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
               }
          }
     }

     public void setDataIntoVector()
     {
          VSupName     = new Vector();
          VBasic       = new Vector();
          VDisc        = new Vector();
          VCenVat      = new Vector();
          VTax         = new Vector();
          VSur         = new Vector();
          VAdd         = new Vector();
          VLess        = new Vector();
          VNet         = new Vector();
          VSupCode     = new Vector();
          VSeq         = new Vector();

          if(!bflag)
          {
               VBlock       = new Vector();
               VBlockCode   = new Vector();
          }

          SStDate = TStDate.TDay.getText()+"."+TStDate.TMonth.getText()+"."+TStDate.TYear.getText();
          SEnDate = TEnDate.TDay.getText()+"."+TEnDate.TMonth.getText()+"."+TEnDate.TYear.getText();
          String StDate  = TStDate.TYear.getText()+TStDate.TMonth.getText()+TStDate.TDay.getText();
          String EnDate  = TEnDate.TYear.getText()+TEnDate.TMonth.getText()+TEnDate.TDay.getText();

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();

              String QString = getQString(StDate,EnDate);
              ResultSet res  = stat.executeQuery(QString);
              while (res.next())
              {
                    String str4  = res.getString(1);
                    String str5  = res.getString(2);
                    String str6  = res.getString(3);
                    String str7  = res.getString(4);
                    String str8  = res.getString(5);
                    String str9  = res.getString(6);
                    String str10 = res.getString(7);
                    String str12 = res.getString(8);
                    String str13 = res.getString(9);
                    String str14 = res.getString(10);
                    String str15 = res.getString(11);
                    
                    
                    VSupName     .addElement(str4);
                    VBasic       .addElement(str5);
                    VDisc        .addElement(str6);
                    VCenVat      .addElement(str7);
                    VTax         .addElement(str8);
                    VSur         .addElement(str9);
                    VNet         .addElement(str10);
                    VSupCode     .addElement(str12);
                    VAdd         .addElement(str13);
                    VLess        .addElement(str14);
                    VSeq         .addElement(str15);
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
         RowData     = new Object[VSupName.size()][13];
         for(int i=0;i<VSupName.size();i++)
         {
               RowData[i][0]  = new Boolean(false);
               RowData[i][3]  = (String)VSupName.elementAt(i);
               RowData[i][4]  = (String)VBasic.elementAt(i);
               RowData[i][5]  = (String)VDisc.elementAt(i);
               RowData[i][6]  = (String)VCenVat.elementAt(i);
               RowData[i][7]  = (String)VTax.elementAt(i);
               RowData[i][8]  = (String)VSur.elementAt(i);
               RowData[i][9] = (String)VAdd.elementAt(i);
               RowData[i][10] = (String)VLess.elementAt(i);
               double dRNet   = common.toDouble((String)VNet.elementAt(i))+common.toDouble((String)VAdd.elementAt(i))-common.toDouble((String)VLess.elementAt(i));
               RowData[i][11] = ""+ dRNet;
               RowData[i][12] = (String)VSeq.elementAt(i);
        }  
     }

     public String getQString(String StDate,String EnDate)
     {
          String QString ="";

          QString = " SELECT Supplier.Name, Sum(PurchaseOrder.qty*rate) AS Expr1, Sum(PurchaseOrder.Disc) AS SumOfDisc, Sum(PurchaseOrder.Cenvat) AS SumOfCenvat, Sum(PurchaseOrder.Tax) AS SumOfTax, Sum(PurchaseOrder.Sur) AS SumOfSur, Sum(PurchaseOrder.qty*rate)-Sum(PurchaseOrder.Disc)+Sum(PurchaseOrder.Cenvat)+Sum(PurchaseOrder.Tax)+Sum(PurchaseOrder.Sur),PurchaseOrder.Sup_Code,"+
                    " PurchaseOrder.Plus,PurchaseOrder.Less,PartyMaster.EMail,"+
                    " PartyMaster.ContPerson1,PartyMaster.PurMobileNo,PurchaseOrder.SeqId "+
                    " FROM (PurchaseOrder INNER JOIN Supplier ON PurchaseOrder.Sup_Code=Supplier.Ac_Code) "+
                    " Inner Join OrdBlock On PurchaseOrder.OrderBlock=OrdBlock.Block "+
                    " Inner Join PartyMaster On PartyMaster.PartyCode=Supplier.Ac_Code "+
                    " Where PurchaseOrder.MillCode="+iMillCode+
                    " and purchaseOrder.OrderTypeCode=2";

               QString = QString+"  and PurchaseOrder.OrderDate >= '"+StDate+"' and PurchaseOrder.OrderDate <='"+EnDate+"'";

               QString = QString+" GROUP BY  Supplier.Name,PurchaseOrder.Sup_Code,PurchaseOrder.Plus,PurchaseOrder.Less,PartyMaster.EMail,PartyMaster.ContPerson1,PartyMaster.PurMobileNo,PurchaseOrder.SeqId ";


              // System.out.println(QString);
          return QString;
     }

     public boolean getReportYear(String SFinYear)
     {
          SFinYear       = SFinYear.trim();
          String SYear   = SFinYear.substring(SFinYear.length()-2,SFinYear.length());
          int iYear      = common  .toInt(SYear);
          
          if(iYear>7)
               return true;
          else
               return false;
     }
}
