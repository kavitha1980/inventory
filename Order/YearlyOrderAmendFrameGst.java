/*
   Order Placement Utility for Materials not requested thro' MRS.
   Simply,this utility is to  handle order without any plan. 
   Hence OrderPlanMiddlePanel and PlanMiddle is replaced with a
   generalized OrderMiddlePanel component.

   This will also act like an Ammendment component for a placed
   purchase Order
*/
package Order;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.util.Date;

import util.*;
import guiutil.*;
import jdbc.*;
import java.net.*;

public class YearlyOrderAmendFrameGst extends JInternalFrame
{
     JPanel              TopPanel,TopLeft,TopRight,BottomPanel;
     YearlyOrderMiddlePanelGst MiddlePanel;
     Connection          theConnect = null;
                         
     JButton             BOk,BCancel,BSupplier,BMaterials;
     NextField           TOrderNo,TPayDays;
     JTextField          TSupCode,TSupData,TRef;
     JTextField          TAdvance,TPayTerm;
     DateField2          TDate;
     JComboBox           JCTo,JCThro,JCForm,JEPCG,JCOrderType,JCProject,JCState,JCPort;

     JLayeredPane        DeskTop;
     Vector              VCode,VName;
     StatusPanel         SPanel;

     Vector              VTo,VThro,VToCode,VThroCode,VFormCode,VForm,VEpcg,
                         VOrderType,VPort,VPortCode,VProject,VState,
                         VBlockName,VBlockCode,VNameCode;

     FileWriter          FW;

     Common              common      = new Common();
     String              SOrderNo    = "";
     String              SInvOrderNo = "";

     int                 iAmend,iUserCode;
     int                 iMillCode = 0;
     String              SYearCode;
     String              SItemTable,SSupTable,SSupCode,SSeqId;
     YearlyOrderAbsFrameGst yearlyOrderAbsFrameGst;

     // to identify creation of a purchase order or its Ammendment
     // false indicates creation mode      true indicates modification mode     

     boolean             bflag;
     boolean             bComflag = true;

     int       iMaxSlNo  = 0;

     int iMSlNo=0;
     int iMUser=1;
     int iMType=0;

     Vector VMUser,VMUserCode;

     JTextField          TProformaNo,TReferenceNo;
     JComboBox           JCCurrencyType;
     Vector              VCurrencyTypeCode, VCurrencyTypeName;

     int iActualAmend=0;
     int iTaxCheck = 0;

     String SSeleSupType="";
     String SSeleStateCode="";
     
     public YearlyOrderAmendFrameGst(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,boolean bflag,int iAmend,int iUserCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable,String SSupCode,String SSeqId,YearlyOrderAbsFrameGst yearlyOrderAbsFrameGst)
     {
          this.DeskTop    = DeskTop;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.SPanel     = SPanel;
          this.bflag      = bflag;
          this.iAmend     = iAmend;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
	  this.SSupCode   = SSupCode;
	  this.SSeqId	 = SSeqId;
	  this.yearlyOrderAbsFrameGst = yearlyOrderAbsFrameGst;

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
          getNextOrderNo();

          System.out.println("Year -- "+SYearCode);
     }

     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnect     =    oraConnection.getConnection();
     }

     public void createComponents()
     {
          TopPanel       = new JPanel();
          TopLeft        = new JPanel();
          TopRight       = new JPanel();
          BottomPanel    = new JPanel();
          
          BOk            = new JButton("Update");
          BCancel        = new JButton("Abort");
          
          BSupplier      = new JButton("Supplier");
          BMaterials     = new JButton("Select Materials");
          
          TOrderNo       = new NextField();
          TDate          = new DateField2();
          TSupCode       = new JTextField();
          TSupData       = new JTextField();
          TAdvance       = new JTextField();
          TPayTerm       = new JTextField();
          TPayDays       = new NextField();
          TRef           = new JTextField();

          getVTo();
          
          JCTo           = new JComboBox(VTo);
          JCThro         = new JComboBox(VThro);
          JCForm         = new JComboBox(VForm);
          JCPort         = new JComboBox(VPort);
          JEPCG          = new JComboBox();
          JCOrderType    = new JComboBox();
          JCProject      = new JComboBox();
          JCState        = new JComboBox();

          JCCurrencyType = new JComboBox(VCurrencyTypeName);

          String SMOrderNo = TOrderNo.getText();

          MiddlePanel    = new YearlyOrderMiddlePanelGst(DeskTop,VCode,VName,VNameCode,iMillCode,bflag,SItemTable);
          
          TDate          . setTodayDate();
          TOrderNo       . setEditable(false);
          TPayDays       . setEditable(false);
          TAdvance       . setEditable(false);

          TReferenceNo   = new JTextField();
          TReferenceNo   . setEditable(false);

          TProformaNo    = new JTextField();
          JCCurrencyType = new JComboBox(VCurrencyTypeName);

          TProformaNo    . setEnabled(false);
          JCCurrencyType . setEnabled(false);
          JCCurrencyType . setSelectedItem("RUPEES");
     }

     public void setLayouts()
     {
          TopPanel  . setLayout(new GridLayout(1,2));
          TopLeft   . setLayout(new GridLayout(10,2,3,3));
          TopRight  . setLayout(new GridLayout(8,2,3,3));
          
          setTitle("Amendment Or Modification to Yearly Order");
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,850,500);
          
          getContentPane()    . setLayout(new BorderLayout());
          BottomPanel         . setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          JEPCG.addItem("Non-EPCG Order   ");
          JEPCG.addItem("EPCG Order       ");
          
          JCOrderType.addItem("Local Order      ");
          JCOrderType.addItem("Import           ");
          
          JCProject  .addItem("Other Projects     ");
          JCProject  .addItem("Machinery Projects ");
          
          JCState    .addItem("None             ");
          JCState    .addItem("Intra State      ");
          JCState    .addItem("Inter State      ");
          
          TopLeft.add(new JLabel("Order No"));
          TopLeft.add(TOrderNo);
          
          TopLeft.add(new JLabel("Order Date"));
          TopLeft.add(TDate);

          TopLeft.add(new JLabel("Supplier"));
          TopLeft.add(BSupplier);

          TopLeft.add(new JLabel("Reference"));
          TopLeft.add(TRef);

          TopLeft.add(new JLabel("EPCG Order   "));
          TopLeft.add(JEPCG);

          TopLeft.add(new JLabel("Proforma No.   "));
          TopLeft.add(TProformaNo);

          TopLeft.add(new JLabel("Order Type   "));
          TopLeft.add(JCOrderType);
          
          TopLeft.add(new JLabel("Currency Type   "));
          TopLeft.add(JCCurrencyType);
          
          TopLeft.add(new JLabel("Project/Non"));
          TopLeft.add(JCProject);

          TopRight.add(new JLabel("Book To"));
          TopRight.add(JCTo);
          
          TopRight.add(new JLabel("Book Thro"));
          TopRight.add(JCThro);
          
          TopRight.add(new JLabel("Form No"));
          TopRight.add(JCForm);
          
          TopRight.add(new JLabel("Advance"));
          TopRight.add(TAdvance);
          
          TopRight.add(new JLabel("Payment Terms"));
          TopRight.add(TPayTerm);
          
//          TopRight.add(new JLabel("Payment Terms (Days)"));
//          TopRight.add(TPayDays);
          
          TopRight  . add(new JLabel("Intra/Inter State"));
          TopRight  . add(JCState);
          
          TopRight  . add(new JLabel("Port         "));
          TopRight  . add(JCPort);
          
          TopPanel  . add(TopLeft);
          TopPanel  . add(TopRight);
          
          TopLeft   . setBorder(new TitledBorder("Order Id Block-I"));
          TopRight  . setBorder(new TitledBorder("Order Id Block-II"));
          
          BottomPanel.add(new JLabel("Double Click - Rate Amend          "));
	  BottomPanel.add(new JLabel("F3 - Qty Amend          "));
          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BSupplier           . addActionListener(new SupplierSearch(DeskTop,TSupCode,SSupTable));
          JEPCG               . addFocusListener(new FocListener());
          JCOrderType         . addFocusListener(new FocListener());
          JCState             . addFocusListener(new FocListener());
          JCPort              . addFocusListener(new FocListener());

          JCOrderType         . addItemListener(new ItemList());

          BOk            . addActionListener(new UpdtList());
          BCancel        . addActionListener(new CanList());
     }

     public class FocListener implements FocusListener
     {
          public void focusLost(FocusEvent fe)
          {
               if (fe.getSource() == JEPCG)
               {
                    if (JEPCG.getSelectedIndex() == 0)
                    {
                         JCTo           . requestFocus();
                         JCOrderType    . setEnabled(false);
                         JCState        . setEnabled(false);
                         JCPort         . setEnabled(false);

                         TProformaNo    . setEnabled(false);
                         JCCurrencyType . setEnabled(false);
                    }
                    else
                    {
                         JCOrderType    . setEnabled(true);
                         JCState        . setEnabled(true);
                         JCPort         . setEnabled(true);

                         TProformaNo    . setEnabled(true);
                         JCCurrencyType . setEnabled(true);
                         TProformaNo    . requestFocus();

                         JCCurrencyType . setEnabled(false);
                         JCCurrencyType . setSelectedItem("RUPEES");
                    }
               }
               
               if ((fe.getSource() == JCOrderType) && (JEPCG.getSelectedIndex()==1))
               {
                    if (JCOrderType.getSelectedIndex() == 1)
                    {
                         JCState        . setEnabled(false);
                         JCPort         . setEnabled(true);
                         JCPort         . setSelectedIndex(1);
                         JCState        . setSelectedIndex(0);

                         JCCurrencyType . setEnabled(true);
                         JCCurrencyType . setSelectedIndex(0);
                    }
                    else
                    {
                         JCPort         . setSelectedIndex(0);                         
                         JCPort         . setEnabled(false);
                         JCState        . setEnabled(true);
                         JCState        . setSelectedIndex(1);                         

                         JCCurrencyType . setEnabled(false);
                         JCCurrencyType . setSelectedItem("RUPEES");
                    }
               }
          }
     
          public void focusGained(FocusEvent fe)
          {
          }
     }

     public class ItemList implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
               if (JCOrderType.getSelectedIndex() == 1)
               {
                    JCCurrencyType . setEnabled(true);
                    JCCurrencyType . setSelectedIndex(0);
               }
               else
               {
                    JCCurrencyType . setEnabled(false);
                    JCCurrencyType . setSelectedItem("RUPEES");
               }
          }
     }

     public class CanList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               removeHelpFrame();
          }
     }

     public class UpdtList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk  . setEnabled(false);
               if(!isValidNew())
               {
                    common    . warn(DeskTop,SPanel);
                    BOk       . setEnabled(true);
                    return;
               }

               int iSel = JOptionPane.showConfirmDialog(null, "Do you want to Update this Order. This will remove SO Approval"); 
               if (iSel == JOptionPane.YES_OPTION)
               {
                    try
                    {
                         insertAmendDetails();
                         updateOrderDetails();
			 updateRateDetails();
                    }
                    catch(Exception Ex)
                    {
                         Ex.printStackTrace();
                         bComflag = false;
                    }
                    try
                    {
                         if(bComflag)
                         {
                              theConnect     . commit();
                              System         . out.println("Commit");
                              theConnect     . setAutoCommit(true);
			      yearlyOrderAbsFrameGst.setApplyData(yearlyOrderAbsFrameGst);
                         }
                         else
                         {
                              theConnect     . rollback();
                              System         . out.println("RollBack");
                              theConnect     . setAutoCommit(true);
                         }
                    }catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
                    removeHelpFrame();
               }
               else
               {
                    BOk  . setEnabled(true);
               }
          }
     }

     public void insertAmendDetails()
     {
          try
          {
               iActualAmend=0;

               int iAmendNo = 0;

               String SOrderNo = ((String)TOrderNo.getText()).trim();

               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat           =  theConnect.createStatement();
                                        

               iActualAmend = getActualAmend();

               if(iActualAmend>0)
               {
                    iAmendNo = getAmendNo();
               }

               String SSysName  = InetAddress.getLocalHost().getHostName();
               String SDateTime = common.getServerDate();

               String QS1 = " Insert Into OrderAmend (Id,NoofAmend,AmendUserCode,AmendSystemName,AmendDateTime,OrderId,OrderNo,OrderBlock,OrderDate,"+
                            " Sup_Code,Reference,Advance,MrsNo,Item_Code,Qty,Rate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,"+
                            " DueDate,PayTerms,InvQty,Dept_Code,Group_Code,Unit_Code,ToCode,ThroCode,Posted,AdvSlNo,AmendmentFlag,FormCode,Class_Code,"+
                            " SlNo,PYPost,WA,DM,Freight,PF,Others,EPCG,OrderType,LicenseNumber,Project_Order,PortCode,State,Amended,UserCode,ModiDate,"+
                            " MillCode,PayDays,Status,Flag,MrsSlNo,TaxClaimable,Authentication,IntimationNo,MailStatus,MrsAuthUserCode,MailConfirmStatus,"+
                            " Converted,JMDOrderApproval,JMDOrderAppDate,JMDOrderRejection,JMDOrderRejDate,SOOrderApproval,SOOrderAppDate,SOOrderRejection,"+
                            " SOOrderRejDate,IAOrderApproval,IAOrderAppDate,IAOrderRejection,IAOrderRejDate,IAOrderRejRemarks,OrderSourceTypeCode,"+
                            " PrevOrderNo,PrevOrderBlock,ComparisionNo,CreatedDateTime,PlanningMonth,OrderTypeCode,JMDOrderHolding,JMDOrderHoldingDate,"+
                            " POGroupNo,ReferenceNo,EPCGGroupStatus,YearCode,ProformaNo,Basic,CurrencyTypeCode,SeqId,DocId,FinalAppUserCode,"+
			    " JMDViewStatus,JMDFinalHoldingStatus, IAViewStatus, IAFinalHoldingStatus,PrevOrderChange,RateChartNo,"+
			    " CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,Cess,CessVal,GstStatus,SplitStatus,SplitOrderNo,HsnCode,GstRate) "+
                            " (Select OrderAmend_Seq.nextVal,"+
                            ""+iAmendNo+" as NoofAmend,"+
                            ""+iUserCode+" as AmendUserCode,"+
                            "'"+SSysName+"' as AmendSystemName,"+
                            "'"+SDateTime+"' as AmendDateTime,"+
                            " Id as OrderId,OrderNo,OrderBlock,OrderDate,"+
                            " Sup_Code,Reference,Advance,MrsNo,Item_Code,Qty,Rate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,"+
                            " DueDate,PayTerms,InvQty,Dept_Code,Group_Code,Unit_Code,ToCode,ThroCode,Posted,AdvSlNo,AmendmentFlag,FormCode,Class_Code,"+
                            " SlNo,PYPost,WA,DM,Freight,PF,Others,EPCG,OrderType,LicenseNumber,Project_Order,PortCode,State,Amended,UserCode,ModiDate,"+
                            " MillCode,PayDays,Status,Flag,MrsSlNo,TaxClaimable,Authentication,IntimationNo,MailStatus,MrsAuthUserCode,MailConfirmStatus,"+
                            " Converted,JMDOrderApproval,JMDOrderAppDate,JMDOrderRejection,JMDOrderRejDate,SOOrderApproval,SOOrderAppDate,SOOrderRejection,"+
                            " SOOrderRejDate,IAOrderApproval,IAOrderAppDate,IAOrderRejection,IAOrderRejDate,IAOrderRejRemarks,OrderSourceTypeCode,"+
                            " PrevOrderNo,PrevOrderBlock,ComparisionNo,CreatedDateTime,PlanningMonth,OrderTypeCode,JMDOrderHolding,JMDOrderHoldingDate, "+
                            " POGroupNo,ReferenceNo,EPCGGroupStatus,YearCode,ProformaNo,Basic,CurrencyTypeCode,SeqId,DocId,FinalAppUserCode,"+
			    " JMDViewStatus,JMDFinalHoldingStatus, IAViewStatus, IAFinalHoldingStatus,PrevOrderChange,RateChartNo, "+
			    " CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,Cess,CessVal,GstStatus,SplitStatus,SplitOrderNo,HsnCode,GstRate "+
                            " From PurchaseOrder "+
                            " Where Sup_Code='"+SSupCode+"' and SeqId="+SSeqId+
	 	            " and OrderTypeCode=2 "+
	                    " and millcode = "+iMillCode+")";

               stat.executeUpdate(QS1);
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("4"+ex);
               ex.printStackTrace();
          }
     }

     public void updateOrderDetails()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);
               Statement stat           =  theConnect.createStatement();
                                        
               String    QS1 = "Update PurchaseOrder Set ";
                         QS1 = QS1+"Sup_Code='"+TSupCode.getText()+"',";
                         QS1 = QS1+"Advance=0"+TAdvance.getText()+",";
                         QS1 = QS1+"PayTerms='"+TPayTerm.getText()+"',";
                         QS1 = QS1+"PayDays=0"+TPayDays.getText()+",";
                         QS1 = QS1+"ToCode=0"+(String)VToCode.elementAt(JCTo.getSelectedIndex())+",";
                         QS1 = QS1+"ThroCode=0"+(String)VThroCode.elementAt(JCThro.getSelectedIndex())+",";
                         QS1 = QS1+"FormCode=0"+(String)VFormCode.elementAt(JCForm.getSelectedIndex())+",";
                         QS1 = QS1+"PortCode=0"+(String)VPortCode.elementAt(JCPort.getSelectedIndex())+",";
                         QS1 = QS1+"EPCG=0"+(JEPCG.getSelectedIndex())+",";
                         QS1 = QS1+"State=0"+(JCState.getSelectedIndex())+",";
                         QS1 = QS1+"Project_Order=0"+(JCProject.getSelectedIndex())+",";
                         QS1 = QS1+"OrderType=0"+(JCOrderType.getSelectedIndex())+",";
                         QS1 = QS1+"Amended=0"+iActualAmend+",";
                         QS1 = QS1+"AUTHENTICATION = 1,";

                         if(iActualAmend>0)
                         {
                              QS1 = QS1+"SOOrderApproval = 0,";
                              QS1 = QS1+"SOOrderRejection = 0,";
                              QS1 = QS1+"SOOrderAppDate = '',";
                              QS1 = QS1+"IAOrderApproval = 0,";
                              QS1 = QS1+"IAOrderRejection = 0,";
                              QS1 = QS1+"IAOrderAppDate = '',";
                              QS1 = QS1+"JMDOrderApproval = 0,";
                              QS1 = QS1+"JMDOrderRejection = 0,";
                              QS1 = QS1+"JMDOrderAppDate = '',";
                         }

                         QS1 = QS1+"ProformaNo= '"+(TProformaNo.getText()).trim()+"', ";
                         QS1 = QS1+"CurrencyTypeCode= "+((String)VCurrencyTypeCode.elementAt(JCCurrencyType.getSelectedIndex()))+" ";
                         QS1 = QS1+" Where OrderTypeCode=2 and MillCode="+iMillCode;
			 QS1 = QS1+" and Sup_Code='"+SSupCode+"' and SeqId="+SSeqId;

               stat.executeUpdate(QS1);
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("4"+ex);
               ex.printStackTrace();
          }
     }

     public void updateRateDetails()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);
               Statement stat           =  theConnect.createStatement();
                                        
               Object    FinalData[][]  = MiddlePanel.MiddlePanel.getFromVector();
               String    SAdd           = MiddlePanel.MiddlePanel.TAdd.getText();
               String    SLess          = MiddlePanel.MiddlePanel.TLess.getText();
               double    dpm            = common.toDouble(SAdd)-common.toDouble(SLess);
               double    dTBasic        = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double    dRatio         = dpm/dTBasic;
               
               for(int i=0;i<MiddlePanel.RowData.length;i++)
               {
			String SItemCode  = (String)FinalData[i][0];
			double dRate      = common.toDouble((String)FinalData[i][3]);
			double dDiscPer   = common.toDouble((String)FinalData[i][4]);
			double dCGST      = common.toDouble((String)FinalData[i][5]);
			double dSGST      = common.toDouble((String)FinalData[i][6]);
			double dIGST      = common.toDouble((String)FinalData[i][7]);
			double dCess      = common.toDouble((String)FinalData[i][8]);

			int iIndex = getItemIndexOf(SItemCode);

			if(iIndex>=0)
			{
				YearlyItemClass yearlyItemClass = (YearlyItemClass)MiddlePanel.MiddlePanel.theItemVector.elementAt(iIndex);

				for(int k=0;k<yearlyItemClass.VPId.size();k++)
				{
					String SId  = (String)yearlyItemClass.VPId.elementAt(k);
					double dQty = common.toDouble((String)yearlyItemClass.VPOrdQty.elementAt(k));
					String SDueDate = (String)yearlyItemClass.VPDueDate.elementAt(k);
					
				        double dGross   = dQty*dRate;
				        double dDisc    = dGross*dDiscPer/100;
				        double dBasic   = dGross - dDisc;
				        double dCGSTVal = dBasic*dCGST/100;
				        double dSGSTVal = dBasic*dSGST/100;
				        double dIGSTVal = dBasic*dIGST/100;
				        double dCessVal = dBasic*dCess/100;
				        double dNet     = dBasic+dCGSTVal+dSGSTVal+dIGSTVal+dCessVal;

	                    		String SMisc    = common.getRound(dGross*dRatio,3);

			            	String    QS1 = "Update PurchaseOrder Set ";
			                	  QS1 = QS1+"Qty=0"+common.getRound(dQty,3)+",";
			                      	  QS1 = QS1+"Rate=0"+common.getRound(dRate,4)+",";
			                      	  QS1 = QS1+"DiscPer=0"+common.getRound(dDiscPer,2)+",";
			                      	  QS1 = QS1+"Disc=0"+common.getRound(dDisc,2)+",";
			                      	  QS1 = QS1+"CGST=0"+common.getRound(dCGST,2)+",";
			                      	  QS1 = QS1+"CGSTVal=0"+common.getRound(dCGSTVal,2)+",";
			                      	  QS1 = QS1+"SGST=0"+common.getRound(dSGST,2)+",";
			                      	  QS1 = QS1+"SGSTVal=0"+common.getRound(dSGSTVal,2)+",";
			                      	  QS1 = QS1+"IGST=0"+common.getRound(dIGST,2)+",";
			                      	  QS1 = QS1+"IGSTVal=0"+common.getRound(dIGSTVal,2)+",";
			                      	  QS1 = QS1+"Cess=0"+common.getRound(dCess,2)+",";
			                      	  QS1 = QS1+"CessVal=0"+common.getRound(dCessVal,2)+",";
			                      	  QS1 = QS1+"Net=0"+common.getRound(dNet,2)+",";
			                      	  QS1 = QS1+"Plus=0"+SAdd+",";
			                      	  QS1 = QS1+"Less=0"+SLess+",";
			                      	  QS1 = QS1+"Misc=0"+SMisc+",";
			                      	  QS1 = QS1+"DueDate='"+SDueDate+"', ";	
			                      	  QS1 = QS1+"Basic = 0"+common.getRound(dGross,2)+" ";
			                      	  QS1 = QS1+" Where ID = "+SId;

			            	stat.executeUpdate(QS1);
				}
			}

               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("4"+ex);
               ex.printStackTrace();
          }
     }

     private int getItemIndexOf(String SItemCode)
     {
          int iIndex=-1;

          for(int i=0; i<MiddlePanel.MiddlePanel.theItemVector.size(); i++)
          {
	       YearlyItemClass yearlyItemClass = (YearlyItemClass)MiddlePanel.MiddlePanel.theItemVector.elementAt(i);	

               if(SItemCode.equals(yearlyItemClass.SItemCode) && SSupCode.equals(yearlyItemClass.SSupCode) && SSeqId.equals(yearlyItemClass.SSeqId) && iMillCode==yearlyItemClass.iMillCode)
               {
                    iIndex = i;
                    break;
               }
          }
          return iIndex;
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop   . remove(this);
               DeskTop   . repaint();
               DeskTop   . updateUI();
          }
          catch(Exception ex) { }
     }

     public void fillData(String SSupCode,String SSeqId,int iOrderStatus,Vector VItemsStatus,String SItemTable,String SSupTable)
     {
          try
          {
               if(iOrderStatus==2 || iOrderStatus==1)
               {
                    setEnables();
               }

               new YearlyOrderResultSetGst(SSupCode,SSeqId,TOrderNo,TDate,BSupplier,TSupCode,TRef,TAdvance,TPayTerm,TPayDays,JCTo,JCThro,JCForm,MiddlePanel,VCode,VName,VToCode,VThroCode,VFormCode,JEPCG,JCOrderType,VEpcg,VOrderType,VPort,JCPort,VProject,JCProject,VState,JCState,iOrderStatus,VItemsStatus,iMillCode,SItemTable,SSupTable,TReferenceNo,TProformaNo,JCCurrencyType);

               setEPCGFields();
	       //checkTaxData();

	       if(iOrderStatus==1)
	       {
		    BOk.setEnabled(false);
	       }
	       else
	       {
		    BOk.setEnabled(true);
	       }
          }
          catch(Exception ex)
          {
               System.out.println("6"+ex);
               ex.printStackTrace();
          }
     }

     public void setEnables()
     {
          TDate. TDay    . setEnabled(false);
          TDate. TMonth  . setEnabled(false);
          TDate. TYear   . setEnabled(false);
          TRef           . setEnabled(false);
          BSupplier      . setEnabled(false);
          JEPCG          . setEnabled(false);
          JCOrderType    . setEnabled(false);
          JCProject      . setEnabled(false);
          JCTo           . setEnabled(false);
          JCThro         . setEnabled(false);
          JCForm         . setEnabled(false);
          TAdvance       . setEnabled(false);
          TPayTerm       . setEnabled(false);
          TPayDays       . setEnabled(false);
          JCPort         . setEnabled(false);
          JCState        . setEnabled(false);
     }

     public boolean isFreshRecord(int i)
     {
          try
          {
               if(MiddlePanel.IdData==null)
                    return true;
               
               boolean bfresh = (i<MiddlePanel.IdData.length?false:true);
               return bfresh;
          }
          catch(Exception ex)
          {
               return true;
          }
     }

     public void getVTo()
     {
          ResultSet  result  = null;
          
          VTo            = new Vector();
          VToCode        = new Vector();
          VThro          = new Vector();
          VThroCode      = new Vector();
          VFormCode      = new Vector();
          VForm          = new Vector();
          VEpcg          = new Vector();
          VOrderType     = new Vector();
          VPort          = new Vector();
          VPortCode      = new Vector();
          VBlockName     = new Vector();
          VBlockCode     = new Vector();
          
          VEpcg          . addElement("Non-EPCG Order   ");
          VEpcg          . addElement("EPCG Order   ");
          
          VOrderType     . addElement("Local Order      ");
          VOrderType     . addElement("Import       ");

          VCurrencyTypeCode        = new Vector();
          VCurrencyTypeName        = new Vector();

          try
          {
               Statement stat      = theConnect.createStatement();
                         result    = stat.executeQuery("Select ToName,ToCode From BookTo Order By 1");

               while(result.next())
               {
                    VTo      . addElement(result.getString(1));
                    VToCode  . addElement(result.getString(2));
               }
               result.close();
               
               result    = stat.executeQuery("Select ThroName,ThroCode From BookThro Order By 1");
               while(result.next())
               {
                    VThro    . addElement(result.getString(1));
                    VThroCode. addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery("Select FormNo,FormCode From STForm Order By 1");
               while(result.next())
               {
                    VForm    . addElement(result.getString(1));
                    VFormCode. addElement(result.getString(2));
               }
               result.close();
               
               result = stat.executeQuery("Select PortCode,PortName From Port Order By 1");
               while(result.next())
               {
                    String ax = result.getString(2);
                    VPort     . addElement(ax);
                    VPortCode . addElement(result.getString(1));
               }
               result.close();

               result = stat.executeQuery("Select block,blockname From OrdBlock where showstatus=0 Order By 1");
               while(result.next())
               {
                    String ax      = result.getString(2);
                    VBlockName     . addElement(ax);
                    VBlockCode     . addElement(result.getString(1));
               }
               result.close();

               ResultSet rs        = stat.executeQuery("Select CurrencyTypeCode, CurrencyTypeName from EPCGCurrencyType Order by 2");
               while(rs.next())
               {
                    VCurrencyTypeCode   . addElement(rs.getString(1));
                    VCurrencyTypeName   . addElement(rs.getString(2));
               }
               rs                       . close();

               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("7"+ex);
          }
     }

     public boolean isValidNew()
     {
          boolean   bFlag     = true;

          // Checking for Supplier
          String SSupCode = (TSupCode.getText()).trim();
          if(SSupCode.length()==0)
          {
	       JOptionPane.showMessageDialog(null,"Supplier Is Not Selected","Information",JOptionPane.INFORMATION_MESSAGE);
	       return false;
          }
          
	  getSupData(SSupCode);

          if(SSeleSupType.equals("") || SSeleSupType.equals("0"))
	  {
	       JOptionPane.showMessageDialog(null,"Supplier Gst Classification Not Updated","Information",JOptionPane.INFORMATION_MESSAGE);
	       return false;
	  }

	  if(SSeleStateCode.equals("") || SSeleStateCode.equals("0"))
	  {
	       JOptionPane.showMessageDialog(null,"Supplier State Data Not Updated","Information",JOptionPane.INFORMATION_MESSAGE);
	       return false;
	  }

          String    SDate     = TDate.toString();

          String    SPDate    = TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText();
                    SPDate    = SPDate.trim();

          if((SPDate.trim()).length()!=8)
          {
               write("Please enter the OrderDate Correctly \n\n");

               return false;
          } 

          try
          {
               FW = new FileWriter(common.getPrintPath()+"Error.prn");
               write("*-----------------------*\n");
               write("*    W A R N I N G      *\n");
               write("*-----------------------*\n\n");
          }
          catch(Exception ex){}
          
          //  Checking for Table Items

          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();

          for(int i=0;i<FinalData.length;i++)
          {
               String SItemCode    = ((String)FinalData[i][0]).trim();

               double dQty         = common.toDouble(((String)FinalData[i][2]).trim());
               double dRate        = common.toDouble(((String)FinalData[i][3]));
               
               if(dQty <= 0)
               {
                    write("Quantity for Item No "+(i+1)+" is not fed \n");
                    bFlag = false;
               }
               if(dRate <= 0)
               {
                    write("Rate for Item No "+(i+1)+" is not fed \n");
                    bFlag = false;
               }

               double dCGST    = common.toDouble(((String)FinalData[i][5]));
               double dSGST    = common.toDouble(((String)FinalData[i][6]));
               double dIGST    = common.toDouble(((String)FinalData[i][7]));

	       if(SSeleSupType.equals("1"))
	       {
	            if(dCGST<=0 && dSGST<=0 && dIGST<=0)
	            {
	                 write("Tax Data for Item No "+(i+1)+" Not Updated \n");
	                 bFlag = false;
	            }

	            if(dCGST<dSGST || dCGST>dSGST)
	            {
	                 write("CGST and SGST Tax Data for Item No "+(i+1)+" Wrongly Updated \n");
	                 bFlag = false;
	            }

	            if(dCGST>0 && dIGST>0)
	            {
	                 write("CGST and IGST Tax Data for Item No "+(i+1)+" Wrongly Updated \n");
	                 bFlag = false;
	            }

	            if(SSeleStateCode.equals("33"))
	            {
		         if(dIGST>0)
		         {
			      write("IGST Tax Data not applicable for this Party for Item No "+(i+1)+" \n");
			      bFlag = false;
		         }
	            }
	            else
	            {
		         if(dCGST>0 || dSGST>0)
		         {
			      write("CGST/SGST Tax Data not applicable for this Party for Item No "+(i+1)+" \n");
			      bFlag = false;
		         }
	            }
	       }
	       else
	       {
	            if(dCGST>0 || dSGST>0 || dIGST>0)
	            {
	                 write("Tax Data for Item No "+(i+1)+" Wrongly Updated \n");
	                 bFlag = false;
	            }
	       }
          }

          if(JEPCG.getSelectedIndex() == 1)
          {
               if(common.parseNull(TProformaNo.getText()).trim().length() == 0)
               {
                    write("Proforma No. should be filled for EPCG Orders \n");
                    bFlag = false;
               }

               String SOrderType        = ((String)JCOrderType.getSelectedItem()).trim();
               String SCurrencyType     = ((String)JCCurrencyType.getSelectedItem()).trim();

               if(SOrderType.equalsIgnoreCase("Import") && SCurrencyType.equalsIgnoreCase("RUPEES"))
               {
                    write("You cannot Select Rupees Currency Type for Import Orders.\n");
                    bFlag = false;
               }
          }

          try
          {
               write("*-----------------------*\n");
               FW.close();
          }
          catch(Exception ex){}
          
          return bFlag;
     }

     public void write(String str)
     {
          try
          {
               FW.write(str);
          }
          catch(Exception ex){}
     }

      public void getSupData(String SSupCode)
      {
	       SSeleSupType   = "";
	       SSeleStateCode = "";

               String QString = " Select decode(PartyMaster.StateCode,0,'0',decode(PartyMaster.CountryCode,'61',nvl(State.GstStateCode,0),'999')) as GstStateCode, "+
			        " nvl(PartyMaster.GstPartyTypeCode,0) as GstPartyTypeCode From "+SSupTable+
				" Inner Join PartyMaster on "+SSupTable+".Ac_Code=PartyMaster.PartyCode and "+SSupTable+".Ac_Code='"+SSupCode+"'"+
				" Inner Join State on PartyMaster.StateCode=State.StateCode";
               try
               {
                    Statement theStatement    = theConnect.createStatement();

                    ResultSet res = theStatement.executeQuery(QString);
                    while(res.next())
                    {
			 SSeleStateCode = common.parseNull(res.getString(1));
                         SSeleSupType   = common.parseNull(res.getString(2));
                    }
                    res            . close();
                    theStatement   . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
      }

     public int getAmendNo()
     {
          int iAmendNo=0;

          String QS = " Select Max(NoofAmend) from OrderAmend "+
                      " Where SeqId="+SSeqId+
 	              " and OrderTypeCode=2 and Qty > 0 "+
                      " and millcode = "+iMillCode;

          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         iAmendNo  = common.toInt((String)result.getString(1))+1;
                         result    . close();
                         stat      . close();

          }catch(Exception Ex)
          {
	       bComflag = false;
               System.out.println(Ex);
               Ex.printStackTrace();
          }
           
          return iAmendNo;
     }

     public int getActualAmend()
     {
          int iAmendNo=0;

          int iCount=0;

          String QS1 = " Select Count(*) from ( "+
		       " Select Count(*),nvl(sum(IAOrderApproval),0) as IAOrderApproval, "+
		       " nvl(sum(JMDOrderApproval),0) as JMDOrderApproval,Max(Amended) as Amended from PurchaseOrder "+
                       " Where PurchaseOrder.Sup_Code='"+SSupCode+"' and PurchaseOrder.SeqId="+SSeqId+
	               " and PurchaseOrder.OrderTypeCode=2 and PurchaseOrder.Qty > 0 "+
                       " and purchaseorder.millcode = "+iMillCode+")"+
	               " Where Amended=1 and IAOrderApproval=0 and JMDOrderApproval=0 ";

          String QS2 = " Select Count(*) from ( "+
	               " Select Count(*),nvl(sum(IAOrderApproval),0) as IAOrderApproval, "+
	               " nvl(sum(JMDOrderApproval),0) as JMDOrderApproval from OrderAmend "+
                       " Where OrderAmend.Sup_Code='"+SSupCode+"' and OrderAmend.SeqId="+SSeqId+
	               " and OrderAmend.OrderTypeCode=2 and OrderAmend.Qty > 0 "+
                       " and OrderAmend.millcode = "+iMillCode+")"+
	               " Where IAOrderApproval>0 or JMDOrderApproval>0 ";

          String QS3 = " Select Count(*) from ( "+
	               " Select Count(*),nvl(sum(IAOrderApproval),0) as IAOrderApproval, "+
	               " nvl(sum(JMDOrderApproval),0) as JMDOrderApproval from PurchaseOrder "+
                       " Where PurchaseOrder.Sup_Code='"+SSupCode+"' and PurchaseOrder.SeqId="+SSeqId+
	               " and PurchaseOrder.OrderTypeCode=2 and PurchaseOrder.Qty > 0 "+
                       " and purchaseorder.millcode = "+iMillCode+")"+
	               " Where IAOrderApproval>0 or JMDOrderApproval>0 ";

          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS1);
                         result    . next();
                         iCount    = result.getInt(1);
                         result    . close();

               if(iCount>0)
               {
                    iAmendNo = checkQtyAmend(false);
               }
               else
               {
                    result    = stat         . executeQuery(QS2);
                    result    . next();
                    iCount    = result.getInt(1);
                    result    . close();

                    if(iCount>0)
                    {
                         iAmendNo = checkQtyAmend(true);
                    }
                    else
                    {
                         result    = stat         . executeQuery(QS3);
                         result    . next();
                         iCount    = result.getInt(1);
                         result    . close();
     
                         if(iCount>0)
                         {
                              iAmendNo = checkQtyAmend(false);
                         }
                    }
               }

               stat      . close();

          }catch(Exception Ex)
          {
	       bComflag = false;
               System.out.println(Ex);
               Ex.printStackTrace();
          }

          return iAmendNo;
     }

     public int checkQtyAmend(boolean bAmendFlag)
     {
          int iAmendNo=0;
          int iOtherAmend=0;

          try
          {
               Statement stat      = theConnect   . createStatement();


               Object FinalData[][]  = MiddlePanel.MiddlePanel.getFromVector();

               double dNewAdd           = common.toDouble(MiddlePanel.MiddlePanel.TAdd.getText());
               double dNewLess          = common.toDouble(MiddlePanel.MiddlePanel.TLess.getText());

               for(int i=0;i<MiddlePanel.RowData.length;i++)
               {
                    double dOldRate      = 0;
                    double dOldDiscPer   = 0;
                    double dOldCGSTPer   = 0;
                    double dOldSGSTPer   = 0;
                    double dOldIGSTPer   = 0;
                    double dOldCessPer   = 0;
		    double dOldAdd	 = 0;
	            double dOldLess	 = 0;

		    String SItemCode  = (String)FinalData[i][0];

                    double dNewRate      = common.toDouble(((String)FinalData[i][3]).trim());
                    double dNewDiscPer   = common.toDouble(((String)FinalData[i][4]).trim());
                    double dNewCGSTPer   = common.toDouble(((String)FinalData[i][5]).trim());
                    double dNewSGSTPer   = common.toDouble(((String)FinalData[i][6]).trim());
                    double dNewIGSTPer   = common.toDouble(((String)FinalData[i][7]).trim());
                    double dNewCessPer   = common.toDouble(((String)FinalData[i][8]).trim());

                    String QS1 = "";

                    if(bAmendFlag)
                    {
                         QS1 = " Select Rate,DiscPer,CGST,SGST,IGST,Cess,Plus,Less from OrderAmend "+
                               " Where Id=(Select Max(Id) from OrderAmend "+
	                       " Where Sup_Code='"+SSupCode+"' and SeqId="+SSeqId+
			       " and OrderTypeCode=2 and Qty > 0 "+
		               " and millcode = "+iMillCode+" and Item_Code='"+SItemCode+"'"+
                               " having (nvl(Max(IAOrderApproval),0)=1 or nvl(Max(JMDOrderApproval),0)=1)) ";
                    }
                    else
                    {
                         QS1 = " Select Rate,DiscPer,CGST,SGST,IGST,Cess,Plus,Less from PurchaseOrder "+
                               " Where Id=(Select Max(Id) from PurchaseOrder "+
		               " Where Sup_Code='"+SSupCode+"' and SeqId="+SSeqId+
			       " and OrderTypeCode=2 and Qty > 0 "+
		               " and millcode = "+iMillCode+" and Item_Code='"+SItemCode+"')";
                    }

                    ResultSet result = stat.executeQuery(QS1);
                    while(result.next())
                    {
                         dOldRate      = common.toDouble(common.parseNull((String)result.getString(1)));
                         dOldDiscPer   = common.toDouble(common.parseNull((String)result.getString(2)));
                         dOldCGSTPer   = common.toDouble(common.parseNull((String)result.getString(3)));
                         dOldSGSTPer   = common.toDouble(common.parseNull((String)result.getString(4)));
                         dOldIGSTPer   = common.toDouble(common.parseNull((String)result.getString(5)));
                         dOldCessPer   = common.toDouble(common.parseNull((String)result.getString(6)));
			 dOldAdd       = common.toDouble(common.parseNull((String)result.getString(7)));
			 dOldLess      = common.toDouble(common.parseNull((String)result.getString(8)));
                    }
                    result.close();

                    if((dNewRate==dOldRate) && (dNewDiscPer==dOldDiscPer) && (dNewCGSTPer==dOldCGSTPer) && (dNewSGSTPer==dOldSGSTPer) && (dNewIGSTPer==dOldIGSTPer) && (dNewCessPer==dOldCessPer) && (dNewAdd==dOldAdd) && (dNewLess==dOldLess))
                    {
                         iOtherAmend = 0;
                    }
                    else
                    {
                         iOtherAmend = 1;
                         iAmendNo    = 1;
                         break;
                    }
               }

               stat.close();

          }catch(Exception Ex)
          {
	       bComflag = false;
               System.out.println(Ex);
               Ex.printStackTrace();
          }
           
          return iAmendNo;
     }

     public String getNextOrderNo()
     {
          // During Modification Order No Need Not to be changed
          if(bflag)
               return TOrderNo.getText();
          
          String SOrderNo= "";
          String QS      = "";
          String QS1     = "";

          QS = " Select maxno From Config"+iMillCode+""+SYearCode+" where Id=1";

          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         SOrderNo  = common.parseNull((String)result.getString(1));
                         result    . close();
                         stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
          SOrderNo  = String.valueOf(common.toInt(SOrderNo.trim())+1);
          TOrderNo  . setText(SOrderNo.trim());
           
          return TOrderNo.getText();
     }

     private void setEPCGFields()
     {
          if (JEPCG.getSelectedIndex() == 0)
          {
               JCOrderType    . setEnabled(false);
               JCState        . setEnabled(false);
               JCPort         . setEnabled(false);

               TProformaNo    . setEnabled(false);
               JCCurrencyType . setEnabled(false);
          }
          else
          {
               JCOrderType    . setEnabled(true);
               JCState        . setEnabled(true);
               JCPort         . setEnabled(true);

               TProformaNo    . setEnabled(true);
               JCCurrencyType . setEnabled(true);
          }
     }
}
