/*
*/
package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.util.*;

import guiutil.*;
import util.*;
import java.io.*;


public class YearlyOrderClassGst
{
     String SSupCode;
     String SMonthCode;
     int    iMillCode;

     Vector VItemCode,VMrsNo,VQty,VUnitCode,VDeptCode,VGroupCode,VCatl,VDraw,VMake,VDueDate,VMrsSlNo,VRemarks,VMrsAuthUserCode,VEnquiryNo;
     Vector VRate,VDiscPer,VCGSTPer,VSGSTPer,VIGSTPer,VCessPer;
     Vector VPOrderNo,VPSource,VPBlock,VPBlockCode,VPOrderDate,VHsnCode;

     Common common = new Common();

     YearlyOrderClassGst(String SSupCode,String SMonthCode,int iMillCode)
     {
          this.SSupCode   = SSupCode;
          this.SMonthCode = SMonthCode;
          this.iMillCode  = iMillCode;

          VItemCode        = new Vector();
          VMrsNo           = new Vector();
          VQty             = new Vector();
          VUnitCode        = new Vector();
          VDeptCode        = new Vector();
          VGroupCode       = new Vector();
          VCatl            = new Vector();
          VDraw            = new Vector();
          VMake            = new Vector();
          VDueDate         = new Vector();
          VMrsSlNo         = new Vector();
          VRemarks         = new Vector();
          VMrsAuthUserCode = new Vector();
          VEnquiryNo       = new Vector();

          VRate            = new Vector();
          VDiscPer         = new Vector();
          VCGSTPer         = new Vector();
          VSGSTPer         = new Vector();
          VIGSTPer         = new Vector();
          VCessPer         = new Vector();

          VPOrderNo        = new Vector();
          VPSource         = new Vector();
          VPBlock          = new Vector();
          VPBlockCode      = new Vector();
          VPOrderDate      = new Vector();
	  VHsnCode         = new Vector();
     }
     public void setData(String SItemCode,String SMrsNo,String SQty,String SUnitCode,String SDeptCode,String SGroupCode,String SCatl,String SDraw,String SMake,String SDueDate,String SMrsSlNo,String SRemarks,String SMrsAuthUserCode,String SEnquiryNo,String SRate,String SDiscPer,String SCGSTPer,String SSGSTPer,String SIGSTPer,String SCessPer,String SPOrderNo,String SPSource,String SPBlock,String SPBlockCode,String SPOrderDate,String SHsnCode)
     {
          VItemCode.addElement(SItemCode);
          VMrsNo.addElement(SMrsNo);
          VQty.addElement(SQty);
          VUnitCode.addElement(SUnitCode);
          VDeptCode.addElement(SDeptCode);
          VGroupCode.addElement(SGroupCode);
          VCatl.addElement(SCatl);
          VDraw.addElement(SDraw);
          VMake.addElement(SMake);
          VDueDate.addElement(SDueDate);
          VMrsSlNo.addElement(SMrsSlNo);
          VRemarks.addElement(SRemarks);
          VMrsAuthUserCode.addElement(SMrsAuthUserCode);
          VEnquiryNo.addElement(SEnquiryNo);
          VRate.addElement(SRate);
          VDiscPer.addElement(SDiscPer);
          VCGSTPer.addElement(SCGSTPer);
          VSGSTPer.addElement(SSGSTPer);
          VIGSTPer.addElement(SIGSTPer);
          VCessPer.addElement(SCessPer);
          VPOrderNo.addElement(SPOrderNo);
          VPSource.addElement(SPSource);
          VPBlock.addElement(SPBlock);
          VPBlockCode.addElement(SPBlockCode);
          VPOrderDate.addElement(SPOrderDate);
          VHsnCode.addElement(SHsnCode);
     }

}

