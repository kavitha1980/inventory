package Order;

import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class YearlyOrderGSTPdfClass
{
     Vector         VCode,VName,VUOM,VQty,VRate;
     Vector         VDiscPer,VCenVatPer,VTaxPer,VSurPer;
     Vector         VBasic,VDisc,VCenVat,VTax,VSur,VNet;
     Vector         VDesc,VMake,VCatl,VDraw,VBlock;
     Vector         VPColour,VPSet,VPSize,VPSide;
     Vector         VSlipFromNo,VSlipToNo,VBookFromNo,VBookToNo,VOrderNos,VOrderGroup;
     Common         common    = new Common();
     FileWriter     FW;
       Vector         VItem;
     String         SOrdNo    = "",SOrdDate  = "",SSupCode  = "",SSupName  = "";
     String         SAddr1    = "",SAddr2    = "",SAddr3    = "";
     String         SEMail="",SFaxNo="";
     String         SToName   = "",SThroName = "";
     String         SPayTerm       = "";
     String         SReference     = "";
     String         SDueDate       = "";
     String         SOthers        = "";
     String         SMRSNo         = "";
     String         SMRSDate       = "";
     String         SPort          = "";
     String SLine = "";
     double         dOthers   = 0;
     double         dTDisc    = 0,dTCenVat   = 0,dTTax      = 0,dTSur = 0,dTNet = 0;
      String         SCgst="",SCgstVal="",SSgst="",SSgstVal="",SIgst="",SIgstVal="",SCess="",SCessVal="",SHsnCode="";   
    
     int            Lctr      = 100;
     int            Pctr      = 0;
     int            iEPCG          = 0;
     int            iOrderType     = 0;
     int            iProject       = 0;
     int            iState         = 0;
     int            iAmend         = 0;
      String SOrderDate="";
     String         SSupplierEMail,SSupplierMobile;
     String         SContact;
      String         SStateName,SStateCode,GSTId;

     String SFile,SStDate,SEnDate,SSeqId;
     String SItemTable;
     int    iMillCode;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 6.9f, Font.NORMAL);

     private static Font tinyBold   = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
     private static Font tinyNormal = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);
      Vector VCGST,VCGSTVal,VSgst,VSgstVal,VIgst,VIgstVal,VCess,VCessVal,VHsnCode;
     double         dTCgst   =0,dTSgst=0,dTIgst=0,dTCess=0 ;

     
     String SHead[]  = {"Mat Code","Block","Description","HSN/SAC Code","UOM","Quantity","Rate","Discount","CGST","SGST","IGST","Cess","Net"};
     String SHead2[] = {"","","Make - Catalogue - Drawing Number","","","","Rs","Rs","Rs","Rs","Rs","Rs","Rs"};
     int    iWidth[] = {10,5,30,9,6,9,8,7,8,8,8,5,9}; int    iWidth1[];
     Document document;
     PdfPTable table,table1;

     int iTotalColumns=0;
     YearlyOrderGSTPdfClass(Document document,PdfPTable table,String SFile,String SSupCode,String SSupName,String SStDate,String SEnDate,String SSeqId)
     {
          this.document   = document;
          this.table      = table;
          this.SFile      = SFile;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.SStDate     = SStDate;
          this.SEnDate     = SEnDate;
          this.SSeqId     = SSeqId;
      
         
          getAddr();
          setDataIntoVector();
          setwidth() ;
          createPdfFile();
     }
     
     public void setDataIntoVector()
     {
          VCode          = new Vector();
          VName          = new Vector();
          VBlock         = new Vector();
          VUOM           = new Vector();
          VQty           = new Vector();
          VRate          = new Vector();
          VDiscPer       = new Vector();
          VCenVatPer     = new Vector();
          VTaxPer        = new Vector();
          VSurPer        = new Vector();
          VBasic         = new Vector();
          VDisc          = new Vector();
          VCenVat        = new Vector();
          VTax           = new Vector();
          VSur           = new Vector();
          VNet           = new Vector();
          VDesc          = new Vector();
          VMake          = new Vector();
          VCatl          = new Vector();
          VDraw          = new Vector();
          VPColour       = new Vector();
          VPSet          = new Vector();
          VPSize         = new Vector();
          VPSide         = new Vector();
          VSlipFromNo    = new Vector();
          VSlipToNo      = new Vector();
          VBookFromNo    = new Vector();
          VBookToNo      = new Vector();

          VOrderGroup    = new Vector();
          VItem          = new Vector();

           VCGST           = new Vector();
          VCGSTVal        = new Vector();
	      VSgst           = new Vector();
          VSgstVal        = new Vector();
          VIgst           = new Vector();
          VIgstVal        = new Vector();
          VCess           = new Vector();
          VCessVal        = new Vector();
          VHsnCode        = new Vector();

  try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection     theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
               Statement      stat           = theConnection.createStatement();

               String         QString        = getQString();
               ResultSet      res            = stat.executeQuery(QString);

               while (res.next())
               {


                    
                    OrderGroup ordergroup = null;


                    String SItemCode    = res.getString(1);  
                    String SItemName    = res.getString(2);
                    double dQty         = res.getDouble(3);
                    double dRate        = res.getDouble(4);
                    double dDiscPer     = res.getDouble(5);
                    double dDisc        = res.getDouble(6);
                    double dCenVatPer   = res.getDouble(7);
                    double dCenVat      = res.getDouble(8);
                    double dTaxPer      = res.getDouble(9);
                    double dTax         = res.getDouble(10);
                    double dNet         = res.getDouble(11);
                    double dSurPer      = res.getDouble(12);
                    double dSur         = res.getDouble(13);

                    SToName        = common.parseNull(res.getString(14));
                    SThroName      = common.parseNull(res.getString(15));
                    String SUom    = common.parseNull(res.getString(16));
                    String SDesc   = common.parseNull(res.getString(17));
                    String SMake   = common.parseNull(res.getString(18));
                    String SCatl   = common.parseNull(res.getString(19));
                    String SDraw   = common.parseNull(res.getString(20));
                    SPayTerm       = common.parseNull(res.getString(21));
                    SDueDate       = common.parseDate(res.getString(22));
                    dOthers        = common.toDouble(res.getString(23));
                    SReference     = common.parseNull(res.getString(24));


                    SMRSNo         = res.getString(25);
                    iEPCG          = res.getInt(26);
                    iOrderType     = res.getInt(27);
                    iProject       = res.getInt(28);
                    iState         = res.getInt(29);
                    SPort          = res.getString(30);
                    iAmend         = res.getInt(31);
                    String SBName  = res.getString(32);


                    String SPColour     =  common.parseNull(res.getString(33));
                    String SPSet        =  common.parseNull(res.getString(34));
                    String SPSize       =  common.parseNull(res.getString(35));
                    String SPSide       =  common.parseNull(res.getString(36));
                    String SPSlipFromNo =  common.parseNull(res.getString(37));
                    String SPSlipToNo   =  common.parseNull(res.getString(38));
                    String SBookFromNo  =  common.parseNull(res.getString(39));
                    String SBookToNo    =  common.parseNull(res.getString(40));



                     String  SOrderNo     =  res.getString(41);
                    SOrderDate   =  res.getString(42);


                   
                    //janaki added 27.06.2017
          
                    SCgst       =   res.getString(43); 
                    SCgstVal    =   res.getString(44);
                    SSgst       =   res.getString(45);
                    SSgstVal    =   res.getString(46); 
                    SIgst       =   res.getString(47); 
                    SIgstVal    =   res.getString(48); 
                    SCess       =   res.getString(49); 
                    SCessVal    =   res.getString(50); 
                    SHsnCode    =   res.getString(51); 

                   


                    int iIndex = getIndexOf(SSupCode);

                    if(iIndex==-1)
                    {
                         ordergroup = new OrderGroup(SSupCode);
                         VOrderGroup.addElement(ordergroup);
                         iIndex=VOrderGroup.size()-1;
                    }
                    ordergroup = (OrderGroup)VOrderGroup.elementAt(iIndex);

                    ItemGst item = null;

                    int iItemIndex  = getItemIndexOf(SItemCode);


                    if(iItemIndex==-1)
                    {

                         item = new ItemGst(SItemCode,SItemName,SHsnCode);
     
                         item.setOrderInfoDetails(SSupCode,
                                                    SPayTerm,SToName,SThroName,
                                                    "","","",
                                                    "",SReference,SCatl,SDraw,
                                                    SBName,SUom,SDesc,
                                                    SMake);


                         item.setOrderValueDetails(dRate,dDiscPer,SCgst,SSgst,SIgst,SCess,0,0,0,dOthers);

                         VItem.addElement(item);

                         iItemIndex = VItem.size()-1;
                         ordergroup.appendItem1(item);

                    }
                    ordergroup.appendOrders(SOrderNo,SOrderDate,SDueDate);

                    item = (ItemGst)VItem.elementAt(iItemIndex);
                    item.append(SOrderNo,SOrderDate,dQty,SDueDate,dDisc,dTax,SCgstVal,SSgstVal,SIgstVal,SCessVal,dNet);

               }
               res  . close();

               ResultSet res1 = stat.executeQuery("Select MRSDate From MRS Where MRSNo="+SMRSNo+" and (MillCode=0 or MillCode is Null)");
               while(res1.next())
                    SMRSDate = common.parseDate(res1.getString(1));

               res1 . close();
               stat . close();
               theConnection.close();
          }
          catch(Exception ex){System.out.println(ex);ex.printStackTrace();}
     }

     public String getQString()
     {
          String QString  = "";
          
         QString = " SELECT PurchaseOrder.Item_Code, InvItems.Item_Name, PurchaseOrder.qty,"+
                    " PurchaseOrder.Rate, PurchaseOrder.DiscPer, PurchaseOrder.Disc,"+
                    " PurchaseOrder.CenVatPer,PurchaseOrder.Cenvat, PurchaseOrder.TaxPer,"+
                    " PurchaseOrder.Tax,PurchaseOrder.Net,PurchaseOrder.SurPer,PurchaseOrder.Sur,"+
                    " BookTo.ToName,BookThro.ThroName,UoM.UoMName,MatDesc.Descr,MatDesc.Make,"+
                    " InvItems.Catl,InvItems.Draw,PurchaseOrder.PayTerms,PurchaseOrder.DueDate,"+
                    " (PurchaseOrder.Plus-PurchaseOrder.Less) as Others,PurchaseOrder.Reference,"+
                    " PurchaseOrder.MRSNo,PurchaseOrder.EPCG,PurchaseOrder.OrderType,"+
                    " PurchaseOrder.Project_order,PurchaseOrder.State,Port.PortName,PurchaseOrder.amended,OrdBlock.BlockName,"+
                    " MatDesc.PAPERCOLOR,PaperSet.SETNAME,MatDesc.PAPERSIZE,PaperSide.SIDENAME,"+
                    " MatDesc.SLIPFROMNO,MatDesc.SLIPTONO,MatDesc.BOOKFROMNO,MatDesc.BOOKTONO,PurchaseOrder.OrderNo,PurchaseOrder.OrderDate "+
                    " ,purchaseorder.CGST,purchaseorder.cgstval,purchaseorder.SGST,purchaseorder.sgstval,purchaseorder.IGST,purchaseorder.IGSTVAL,purchaseorder.CESS,purchaseorder.cessval,decode (Purchaseorder.hsncode,null,invitems.hsncode,purchaseorder.hsncode) as hsncode "+//invitems.hsncode
                    " FROM ((((PurchaseOrder inner join port on purchaseorder.portcode=port.portcode)INNER JOIN InvItems ON PurchaseOrder.Item_Code = InvItems.Item_Code) "+
                    " Inner Join UoM on UoM.UoMCode = InvItems.UomCode"+
                    " Inner Join OrdBlock on OrdBlock.Block = PurchaseOrder.OrderBlock"+
                    " Inner Join BookTo   On BookTo.ToCode = PurchaseOrder.ToCode) "+
                    " Inner Join BookThro On BookThro.ThroCode = PurchaseOrder.ThroCode) "+
                    " Left Join MatDesc On MatDesc.OrderNo = PurchaseOrder.OrderNo and MatDesc.OrderBlock = PurchaseOrder.OrderBlock and MatDesc.Item_Code = PurchaseOrder.Item_Code And MatDesc.SlNo = PurchaseOrder.SlNo "+
                    " inner join paperset on paperset.setcode = Matdesc.papersets inner join paperside on paperside.sidecode = Matdesc.paperside"+
                    " Where PurchaseOrder.Qty > 0 And PurchaseOrder.Sup_Code = '"+SSupCode+"' and PurchaseOrder.millcode = 0 and PurchaseOrder.OrderDate>="+SStDate+" and PurchaseOrder.Orderdate<="+SEnDate+  //and PurchaseOrder.OrderTypeCode=2
                    " and PurchaseOrder.SeqId="+SSeqId+
                    " and OrderTypeCode=2"+
                    " Order By DueDate ";

          return QString;
     }
     
     public void getAddr()
     {
          String QS =    " Select Supplier.Addr1,Supplier.Addr2,Supplier.Addr3,Supplier.EMail,Supplier.Fax,Place.PlaceName,Supplier.Contact "+
                         " From Supplier LEFT Join Place On Place.PlaceCode = Supplier.City_Code "+
                         " Where Supplier.Ac_Code = '"+SSupCode+"'";


         String QS1=" select distinct state.statename,state.GSTSTATECODE,GSTINID from partymaster "+
	             " inner join PURCHASEORDER on purchaseorder.SUP_CODE=partymaster.PARTYCODE "+
                     " inner join state on state.STATECODE=partymaster.statecode "+
                     " Where partymaster.PARTYCODE= '"+SSupCode+"'"; 

          SSupplierEMail      = "";
          SSupplierMobile     = "";
          SContact            = "";


         
         SStateName ="";
         SStateCode="";
         GSTId="";

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               ResultSet      res            = stat.executeQuery(QS);

               while (res.next())
               {
                    SAddr1    = res.getString(1);
                    SAddr2    = res.getString(2);
                    SAddr3    = common.parseNull(res.getString(3));
                    SEMail    = common.parseNull(res.getString(4));
                    SFaxNo    = common.parseNull(res.getString(5));
                    SContact  = common.parseNull(res.getString(7));
               }

               res  . close();

               ResultSet rs                  = stat.executeQuery("Select EMail,PurMobileNo from PartyMaster where PartyCode = '"+SSupCode+"' ");
               if(rs.next())
               {
                    SSupplierEMail           = common.parseNull(rs.getString(1));
                    SSupplierMobile          = common.parseNull(rs.getString(2));
               }
               rs                            . close();


               ResultSet rs1                  = stat.executeQuery(QS1);
               if(rs1.next())
               {
                    SStateName           = common.parseNull(rs1.getString(1));
                    SStateCode           = common.parseNull(rs1.getString(2));
                    GSTId                = common.parseNull(rs1.getString(3));

   System.out.println("State==>"+SStateName+"=="+SStateCode);
               }
               rs1                           . close();

               stat . close();
          }
          catch(Exception ex){System.out.println(ex);ex.printStackTrace();}
     }

     public boolean isStationary(String SItemCode)
     {
          String SStkGroupName     = "";
          String QS                = "";

          QS =    " select stockgroup.groupname,invitems.item_code from invitems"+
                  " inner join stockgroup on stockgroup.groupcode = invitems.stkgroupcode"+
                  " where invitems.item_code = '"+SItemCode+"'";
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnect    =  oraConnection.getConnection();               
               Statement      stat          = theConnect.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result.next())
               {
                    SStkGroupName  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }
          if(SStkGroupName.equals("STATIONARY"))
               return true;

          return false;
     }

     public void createPdfFile()
     {
          try
          {
              table = new PdfPTable(13);
               table.setWidths(iWidth);
               table.setWidthPercentage(100);

                 
               addHead(document,table);
               addBody(document,table);
               addFoot(document,table,0);


             

          }
          catch (Exception e)
          {
               e.printStackTrace();
          }
     }

    private void setwidth() 
    {
       
         for(int i=0; i<VOrderGroup.size(); i++)
               {
               OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);

       iTotalColumns = ordergroup.orderNoList.size()+1;
           }
	iWidth1 = new int[iTotalColumns];
 	for (int j = 0; j < iTotalColumns; j++)
	{
           
                 iWidth1[j] = 20;
        }
     

        }
 
     private void addHead(Document document,PdfPTable table) throws BadElementException
     {
          try
          {
               if(Lctr < 37)
                    return;
               
               if(Pctr > 0)
                    addFoot(document,table,1);
             

               Pctr++;

               document.newPage();

               table.flushContent();

               table = new PdfPTable(13);
               table.setWidths(iWidth);
               table.setWidthPercentage(100);


               String SState      = "";
               String SEpcg       = "";
               String SOrderTitle = "";
     
               if (iState == 1)
               {
                    SState = " Intra State";
               }
               else if (iState == 2)
               {
                    SState = " Inter State";
               }
     
               if (iAmend == 1)
               {     
                    if (iEPCG == 1 && iOrderType == 0)
                    {
                         SEpcg       = "EPCG";
                         SOrderTitle = "AM PURCHASE ORDER";
                    }
                    else if (iEPCG == 1 && iOrderType == 1)
                    {
                         SState      = "IMPORT";
                         SEpcg       = "EPCG";
                         SOrderTitle = "AM PURCHASE ORDER"+common.Pad(SPort,10);
                    }
                    else if (iEPCG == 0 && iOrderType == 1)
                    {
                         SState      = "IMPORT";
                         SOrderTitle = "AM PURCHASE ORDER"+common.Pad(SPort,10);
                    }
                    else
                    {
                         SOrderTitle = "AM PURCHASE ORDER";
                    }
               }
               else
               {
                    if (iEPCG == 1 && iOrderType == 0)
                    {
                         SEpcg       = "EPCG";
                         SOrderTitle = "PURCHASE ORDER";
                    }
                    else if (iEPCG == 1 && iOrderType == 1)
                    {
                         SState      = "IMPORT";
                         SEpcg       = "EPCG";
                         SOrderTitle = "PURCHASE ORDER"+common.Pad(SPort,10);
                    }
                    else if (iEPCG == 0 && iOrderType == 1)
                    {
                         SState      = "IMPORT";
                         SOrderTitle = "PURCHASE ORDER"+common.Pad(SPort,10);
                    }
                    else
                    {
                         SOrderTitle = "PURCHASE ORDER";
                    }
               }


               PdfPCell c1;
 String Str1   = "AMARJOTHI SPINNING MILLS LIMITED";
               String Str2   = "PUDUSURIPALAYAM";
               String Str3   = "NAMBIYUR  -  638 458,";
               String Str3aa = " Gobi Taluk,Erode Dt,";
               String Str31  = "TAMILNADU   "; 
               String Str4   = "";
               String Str5   = "E-Mail: purchase@amarjothi.net,mill@amarjothi.net";
               String Str6   = "Phones               : 04285 - 267201,267301";
             //  String Str7   = "Our C.S.T. No. : 440691 dt. 24.09.1990  ";
              // String Str8   = "TIN No.             : 33632960864 ";
               String Str7    = "GSTIN               :  33AAFCA7082C1Z0 ";
               String Str9   = "To,";
               String Str10  = "M/s. "+SSupName;
               String Str11  = SAddr1;
               String Str12  = SAddr2;
               String Str13  = SAddr3;
              // String SState1 = SState; 
               //String SCode  = SStateCode; 
               String Str14  = common.Pad("Fax No:04285 "+SFaxNo,25);
               String StrEmail=  common.Pad("EMail: "+SSupplierEMail,38);

               String Str1a  = "";
               String Str2a  = "PLA No    : 59/93";
               String Str3a  = "Division    : Erode Division II";
               String Str4a  = SStateName;
               String PGstNo = GSTId;
               String Str6a  = "NO";
               String Str8a  = "Book To";
               String Str10a = "Book Thro";
               String Str12a = "Reference";

               
               String Str1b  = "ECC No   : AAFCA 7082C XM 001";
               String Str2b  = "Range                     :  Gobichettipalayam";
               String Str3b  = "Commissionerate  : Salem";
               String Str4b  = SEpcg;
               String Str6b  = common.Pad(getOrderNo(),27);
               String Str8b  = common.Pad(SToName,55);
               String Str10b = common.Pad(SThroName,55);
               String Str12b = common.Pad(SReference,55);

               String Str4c  = SOrderTitle;
               String Str6c  = "Date";
               String Str6d  =common.Pad(common.parseDate(SOrderDate),10);
               String Str6e  = "Page";
               String Str6f  = common.Pad(""+Pctr,3);

System.out.println("SOrdNo===>"+SOrdNo);
      c1 = new PdfPCell(new Phrase(Str1,mediumBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.TOP | Rectangle.RIGHT);
               table.addCell(c1);

              /* c1 = new PdfPCell(new Phrase(Str1a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
               table.addCell(c1);*/

               c1 = new PdfPCell(new Phrase(Str1b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(8);
               c1.setBorder(Rectangle.TOP | Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str2,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str2a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str2b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str3,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str3a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str3b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str3aa,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str1a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(2);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str1a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(6);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);



               c1 = new PdfPCell(new Phrase(Str31,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

            /*   c1 = new PdfPCell(new Phrase(Str4,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

              

               c1 = new PdfPCell(new Phrase(Str4b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);*/

               c1 = new PdfPCell(new Phrase(Str4c,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setColspan(8);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);
 


               c1 = new PdfPCell(new Phrase(Str5,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str6,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(3);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(3);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);
               
               c1 = new PdfPCell(new Phrase(Str6c,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(3);
                c1.setColspan(2); 
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6d,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(3);
                c1.setColspan(2); 
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6e,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(3);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6f,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(3);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str7,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase("",smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT  |Rectangle.BOTTOM);
               table.addCell(c1);

                c1 = new PdfPCell(new Phrase("33",smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(1);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP |Rectangle.BOTTOM);
               table.addCell(c1);

              //added

               c1 = new PdfPCell(new Phrase(Str9,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);
   
               c1 = new PdfPCell(new Phrase(Str8a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                c1.setColspan(1);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);


              
               c1 = new PdfPCell(new Phrase(Str8b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(7);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str10,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);
             

               c1 = new PdfPCell(new Phrase(Str10a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                c1.setColspan(1);
               c1.setRowspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str10b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(4);
               c1.setColspan(7);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

                
               c1 = new PdfPCell(new Phrase(Str11,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str12,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str13,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str4a  ,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str12a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str12b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(4);
               c1.setColspan(7);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("GSTIN  :"+PGstNo,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT );
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str14,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(3);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT );
               table.addCell(c1);

            if(SStateCode.equals("")){
              c1 = new PdfPCell(new Phrase("",smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP  );
               table.addCell(c1);
               }else{
                 c1 = new PdfPCell(new Phrase("State Code",smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP  );
               table.addCell(c1);

               }
             
               c1 = new PdfPCell(new Phrase(StrEmail,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(3);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT );
               table.addCell(c1);


    
              c1 = new PdfPCell(new Phrase(SStateCode,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM );
               table.addCell(c1);
       


               for(int i=0;i<SHead.length;i++)
               {
                    c1 = new PdfPCell(new Phrase(SHead[i],smallBold));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                    table.addCell(c1);
               }

               for(int i=0;i<SHead2.length;i++)
               {
                    c1 = new PdfPCell(new Phrase(SHead2[i],smallBold));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                    table.addCell(c1);
               }

               document.add(table);
             
               Lctr = 16;
          }    
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void addBody(Document document,PdfPTable table) throws BadElementException
     {
        
               int iCount=0;

               PdfPCell c1;

             String Strl="";
          for(int i=0;i<VOrderGroup.size();i++)
          {
                    addHead(document,table);
                OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);
                
               ArrayList theItemList  = (ArrayList)ordergroup.getItems();

                int iItemCount = 0;

               for(int k=0; k<theItemList.size(); k++)
               {

                  

                    iItemCount++;


                      /*if(iItemCount>10)
			           {
                           try{ 
                           document.add(table);
                           table  = new PdfPTable(13); 
                           addFoot(document,table,1);
                           document.newPage(); 
				
                           addHead(document,table);
                           iItemCount=0;
                           }catch(Exception ex){}
                         }*/
                    ItemGst item           = (ItemGst)theItemList.get(k);

                    String SCode        = item.SItemCode;
                    String SUOM         = item.getUom();
                    String SName        = item.SItemName;
                    
                    String SBkName      = item.getBlockName();
     
                    String SQty         = item.getTotalQty();
                    String SRate        = item.getRate();
                    String SDisc        = item.getTotalDisc();
                    String SDiscPer     = item.getDiscPer()+"%";
                    String SCenVat      = item.getTotalCenVat();
                    String SCenVatPer   = item.getCenvatPer()+"%";
                    String STax         = item.getTotalTax();
                    String STaxPer      = item.getTaxPer()+"%";
                    String SSur         = item.getTotalSur();
                    String SSurPer      = item.getSurPer()+"%";
                    String SNet         = item.getTotalNet();
                    String SDesc        = item.getDesc();
                    String SMake        = item.getMake();
                    String SCatl        = item.getCatl();
                    String SDraw        = item.getDraw();
     
                    String SPColour     = "";
                    String SPSets       = "";
                    String SPSize       = "";
                    String SPSide       = "";
     
                    String SSlipFromNo  = "";
                    String SSlipToNo    = "";
                    String SBookFromNo  = item.getThroCode();
                    String SBookToNo    = item.getToCode();

                    SDesc          = SDesc        . trim();
                    SMake          = SMake        . trim();
                    SPColour       = SPColour     . trim();
                    SPSets         = SPSets       . trim();
                    SPSize         = SPSize       . trim();
                    SPSide         = SPSide       . trim();
                    SSlipFromNo    = SSlipFromNo  . trim();
                    SSlipToNo      = SSlipToNo    . trim();
                    SBookFromNo    = SBookFromNo  . trim();
                    SBookToNo      = SBookToNo    . trim();

                    
                     String SCGST     =item.getCgstPer();
                    String SCGSTVal    =item.getTotalCgst();     
                    String Ssgst       =item.getSgstPer();
                    String SsgstVal    =item.getTotalSgst();   
                    String SIgst       =item.getIgstPer();
                    String SIgstVal    =item.getTotalIgst();   
                    String SCess       =item.getCessPer();
                    String SCessVal    =item.getTotalCess();   
                    String SHsnCode     = item.SHsnCode;



                 /* String SCGST         = common.parseNull((String)VCGST. elementAt(k));
                    String SCGSTVal      = common.parseNull((String)VCGSTVal. elementAt(k));
                    String Ssgst         = common.parseNull((String)VSgst. elementAt(k));
                    String SsgstVal      = common.parseNull((String)VSgstVal. elementAt(k));
                    String SIgst         = common.parseNull((String)VIgst. elementAt(k));
                    String SIgstVal      = common.parseNull((String)VIgstVal. elementAt(k));
                    String SCess         = common.parseNull((String)VCess. elementAt(k));
                    String SCessVal      = common.parseNull((String)VCessVal. elementAt(k));
                    String SHsnCode     = common.parseNull((String)VHsnCode. elementAt(k));*/


                    dTDisc   = dTDisc   + common.toDouble(SDisc);
                    dTCenVat = dTCenVat + common.toDouble(SCenVat);
                    dTTax    = dTTax    + common.toDouble(STax);
                    dTSur    = dTSur    + common.toDouble(SSur);
                    dTNet    = dTNet    + common.toDouble(SNet);
                    
                

                   dTCgst  =dTCgst+common.toDouble(SCGSTVal);
                   dTSgst =dTSgst+ common.toDouble(SsgstVal);
                   dTIgst =dTIgst+ common.toDouble(SIgstVal);
                   dTCess =dTCess+ common.toDouble(SCessVal);

                  
                     try
                    {
                         c1 = new PdfPCell(new Phrase(common.Pad(SCode,9),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SBkName,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);
                    
                         c1 = new PdfPCell(new Phrase(SName,tinyNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SHsnCode,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1); 

                         c1 = new PdfPCell(new Phrase(common.Pad(SUOM,5),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.getRound(SQty,3),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SRate,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SDisc,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

				
                         c1 = new PdfPCell(new Phrase(common.getRound(SCGSTVal,2),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1); 

                         c1 = new PdfPCell(new Phrase(common.getRound(SsgstVal,2),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1); 

                          c1 = new PdfPCell(new Phrase(common.getRound(SIgstVal,2),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1); 

                         c1 = new PdfPCell(new Phrase(common.getRound(SCessVal,2),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1); 

                        /* c1 = new PdfPCell(new Phrase(SCenVat,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(STax,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);*/

                         c1 = new PdfPCell(new Phrase(SNet,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         Lctr = Lctr+1;

                         Vector vect = common.getLines(SDesc+"` ");
                         
                         for(int j=0;j<vect.size();j++)
                         {
                              if(j==0)
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   c1 = new PdfPCell(new Phrase(common.Pad((String)vect.elementAt(j),37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                  addEmptyCell(table,"Left","Right");

                                   c1 = new PdfPCell(new Phrase(SDiscPer,smallNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

  

                            


                                   c1 = new PdfPCell(new Phrase(SCGST+"%",smallNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                 
                                   c1 = new PdfPCell(new Phrase(Ssgst+"%",smallNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);


                                   c1 = new PdfPCell(new Phrase(SIgst+"%",smallNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   c1 = new PdfPCell(new Phrase(SCess+"%",smallNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                 
                                 /*  c1 = new PdfPCell(new Phrase(SCenVatPer,smallNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   c1 = new PdfPCell(new Phrase(STaxPer,smallNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);*/

                                   addEmptyCell(table,"Left","Right");

                                   Lctr = Lctr+1;
                              }
                              else
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   c1 = new PdfPCell(new Phrase(common.Pad((String)vect.elementAt(j),37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   Lctr = Lctr+1;
                              }
                         }
                         
                         if(SMake.length() > 0)
                         {
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              c1 = new PdfPCell(new Phrase(common.Pad(SMake,37),tinyNormal));
                              c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                              c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                              table.addCell(c1);

                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                               addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                              Lctr = Lctr+1;
                         }                         
                         
                         if((SCatl.trim()).length() > 0)
                         {
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              c1 = new PdfPCell(new Phrase(common.Pad("Catl   : "+SCatl,37),tinyNormal));
                              c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                              c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                              table.addCell(c1);

                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                              Lctr = Lctr+1;
                         }                         
                         
                         if((SDraw.trim()).length() > 0)
                         {
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              c1 = new PdfPCell(new Phrase(common.Pad("Drawing: "+SDraw,37),tinyNormal));
                              c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                              c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                              table.addCell(c1);

                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                               addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");  

                              Lctr = Lctr+1;
                         }
     
                         String    SSPCoSets = "Colour : "+common.Pad(SPColour,12);
                                   SSPCoSets+= "Sets: "+SPSets;
               
                         String    SSPSis    = "Size   : "+common.Pad(SPSize,12);
                                   SSPSis   += "Side: "+SPSide;
     
                         String    SSPBooks  = "Book No: "+SBookFromNo+" To "+SBookToNo;
                         String    SSPSlip   = "Slip No: "+SSlipFromNo+" To "+SSlipToNo;
     
                         if(isStationary(SCode))
                         {
                              if((SPColour.trim()).length() > 0 || ((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0))
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
     
                                   c1 = new PdfPCell(new Phrase(common.Pad(SSPCoSets,37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);
     
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   Lctr++;
                              }
          
                              if(((SPColour.trim()).length() > 0) || ((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0))
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
     
                                   c1 = new PdfPCell(new Phrase(common.Pad(SSPSis,37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);
     
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right"); 


                                   Lctr++;
                              }
     
                              if((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0)
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
     
                                   c1 = new PdfPCell(new Phrase(common.Pad(SSPSlip,37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);
     
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   Lctr++;
                              }
     
                              if((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0)
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
     
                                   c1 = new PdfPCell(new Phrase(common.Pad(SSPBooks,37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);
     
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right"); 

                                   Lctr++;
                              }
                         }
                    }
                    catch(Exception ex){}
               }
          }    

     }

     private void addFoot(Document document,PdfPTable table,int iSig) throws BadElementException
     {
          try
          {
                PdfPCell c1;

               String SDisc   = common.getRound(dTDisc,2);
               String SCenVat = common.getRound(dTCenVat,2);
               String STax    = common.getRound(dTTax,2);
               String SSur    = common.getRound(dTSur,2);
               String SNet    = common.getRound(dTNet,2);
               String SOthers = common.getRound(dOthers,2);
               String SGrand  = common.getRound(dTNet+dOthers,2);

		
		String scgst   = common.getRound(dTCgst  ,2);
		String ssgst   = common.getRound( dTSgst ,2);
		String sigst   =common.getRound( dTIgst ,2);
		String scess   =common.getRound( dTCess ,2);

               float f = 16.0f;

             

               c1 = new PdfPCell(new Phrase(" T O T A L ",smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(6);
               c1.setFixedHeight(f);
               table.addCell(c1);

              
               addEmptyCell(table);

               c1 = new PdfPCell(new Phrase(SDisc,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(scgst,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(ssgst,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);

              c1 = new PdfPCell(new Phrase(sigst,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(scess,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SNet,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);


               
               if(iSig==0)
               {
                    c1 = new PdfPCell(new Phrase("Date of Delivery",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                   // c1.setFixedHeight(f);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase("Terms of Payments",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase("MRS No",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase("MRS Date",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase("Freight & Others",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(4);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase("Order Value",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(4);
                    table.addCell(c1);
     
     
                    c1 = new PdfPCell(new Phrase(SDueDate,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setFixedHeight(f);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase(SPayTerm,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase(SMRSNo,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase(SMRSDate,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase(SOthers,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(4);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase(SGrand,mediumBold));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(4);
                    table.addCell(c1);
               }

               String Str1  = " Note  :   1. Please mention our order No. and material code in all correspondence with us.";
               String Str2  = common.Space(14)+" 2. Please mention our GSTIN  in your invoices.";
               String Str3  = common.Space(14)+" 3. Please send  invoices in triplicate.";
               String Str4  = common.Space(14)+" 4. Please send copy of your invoice along with consignment.";
               String Str5  = common.Space(14)+" 5. We reserve the right to increase / decrease the order qty. / cancel the order without assigning any reason";
               String Str6  = common.Space(14)+" 6. Any legal dispute shall be within the jurisdiction of Tirupur.";

               String Str7  = common.Space(14)+" 7. These materials should be supplied against EPCG Scheme  for which please send us your Proforma Invoice along with the catalogue immediately so that we can open the EPCG Licence.";
               String Str8  = common.Space(14)+" 8. Please quote the EPCG License No in your all Bills.";

               String Str9  = "For AMARJOTHI SPINNING MILLS LTD";
               String Str10 = common.Space(10)+"Prepared By"+common.Space(50)+"Checked By"+common.Space(50)+"I.A."+common.Space(100)+"Authorised Signatory";
               String Str11 = " Special Instruction :-";

               String Str12 = " Regd. Office - 'AMARJOTHI HOUSE' 157, Kumaran Road, Tirupur - 638 601. Phone : 0421- 2201980 to 2201984";
               String Str13 = " ";

               String Str14 = "I N D I A 'S   F I N E S T  M E L A N G E  Y A R N  P R O D U C E R S";


	       String SGstNote1 = " GST Note : "; 
	       String SGstNote2 = common.Space(15)+" ITC shall be eligible only on the basis of Credit available in GSTN Portal. The Supplier shall be responsible for proper filing of GST Return in the  ";
	       String SGstNote3 = common.Space(15)+" GSTN  Portal.  If for any reason,  the GST as indicated in the Invoice is not reflected in the  GSTN  Portal,  the tax component  shall be debited to ";
	       String SGstNote4 = common.Space(15)+" your account with interest. ";



               c1 = new PdfPCell(new Phrase(Str1,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(13);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str2,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(13);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str3,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(13);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str4,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(13);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str5,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(13);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(13);
               table.addCell(c1);

               if (iEPCG == 1)
               {
                    c1 = new PdfPCell(new Phrase(Str7,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    c1.setColspan(13);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(Str8,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    c1.setColspan(13);
                    table.addCell(c1);
               }

               addEmptySpanRow(table,"LEFT","RIGHT");

               c1 = new PdfPCell(new Phrase(SGstNote1,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(13);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SGstNote2,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(13);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SGstNote3,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(13);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SGstNote4,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(13);
               table.addCell(c1);

               addEmptySpanRow(table,"LEFT","RIGHT");

               c1 = new PdfPCell(new Phrase(Str9,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(13);
               table.addCell(c1);

               addEmptySpanRow(table,"LEFT","RIGHT");
               addEmptySpanRow(table,"LEFT","RIGHT");
               addEmptySpanRow(table,"LEFT","RIGHT");
               addEmptySpanRow(table,"LEFT","RIGHT");

               c1 = new PdfPCell(new Phrase(Str10,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
               c1.setColspan(13);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str11,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(13);
               c1.setFixedHeight(f);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str12,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(13);
               table.addCell(c1);

               /*c1 = new PdfPCell(new Phrase(Str13,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
               c1.setColspan(13);
               table.addCell(c1);*/

               c1 = new PdfPCell(new Phrase(Str14,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(13);
               c1.setFixedHeight(f);
               table.addCell(c1);

               document.add(table);
               Lctr = Lctr+1;


               setDeliverySchedules(0);
               setDeliveryQty();


             

               String Str15="";

               if(iSig==0)
                    Str15 = "<End Of Report>";
               else
                    Str15 = "(Continued on Next Page)";


               Paragraph paragraph = new Paragraph(Str15,smallNormal);
               document.add(paragraph);

          }    
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }


 //

     private void setDeliverySchedules(int iFlag)
     {
          try
          {
                PdfPCell c1;

               
             for(int i=0; i<VOrderGroup.size(); i++)
               {
               OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);

               
               table1 = new PdfPTable(ordergroup.orderNoList.size()+1);

               System.out.println(ordergroup.orderNoList.size());
	       System.out.println(ordergroup.orderNoList);
	       table1.setWidths(iWidth1);
               table1.setWidthPercentage(100);	

               }	


               c1 = new PdfPCell(new Phrase(" ",bigBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_LEFT);
               c1.setBorder(0 );
               c1.setColspan(  iTotalColumns);
               table1.addCell(c1);
             

               c1 = new PdfPCell(new Phrase(" ",bigBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_LEFT);
               c1.setBorder(0);
               c1.setColspan(  iTotalColumns);
               table1.addCell(c1);

               c1 = new PdfPCell(new Phrase(" DELIVERY SCHEDULE",bigBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_LEFT);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP) ;
               c1.setColspan(  iTotalColumns);
               table1.addCell(c1);

        
           
              

               c1 = new PdfPCell(new Phrase(" Order No",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_LEFT);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
              c1.setRowspan(1);
               table1.addCell(c1);



               for(int i=0; i<VOrderGroup.size(); i++)
               {
               OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);

                    for(int k=0; k<ordergroup.orderNoList.size(); k++)
                    {

                  c1 = new PdfPCell(new Phrase((String)ordergroup.orderNoList.get(k),smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP); 
               c1.setColspan(1);
               table1.addCell(c1); 
             
                    }
		}

		
         
               c1 = new PdfPCell(new Phrase(" Due Date",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_LEFT);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM); 
               c1.setRowspan(1);
               table1.addCell(c1);

           for(int i=0; i<VOrderGroup.size(); i++)
               {
               OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);


                    for(int k=0; k<ordergroup.orderNoList.size(); k++)
                    {
               c1 = new PdfPCell(new Phrase((String)ordergroup.dueDateList.get(k),smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP); 
                c1.setColspan(1);
                table1.addCell(c1); 
             
                    }
		}


          
          }catch(Exception ex){}
     }


     private void setDeliveryPrint(String Str,int iFlag)
     {
          try
          {
             
               if(Lctr < 37)
               {
                    Lctr = Lctr + 1;
                    return;
               }
               if(Pctr > 0)             
               {
                    if(iFlag==0)
                    {
                         FW.write("(Continued on Next Page) ");
                         Lctr=0;
                         setDeliverySchedules(0);
                         return;
                    }
                    else
                    {
                         FW.write("");
                         return;
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void setDeliveryQty()
     {

           PdfPCell c1;

          try
          {
               String SBody ="";
               String SEmptySpace="";
               String SLastLine = "";


               for(int i=0; i<VOrderGroup.size(); i++)
               {

                    OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);
                  
                    ArrayList theItemList  = (ArrayList)ordergroup.getItems();
                     
                     for(int j=0; j<theItemList.size(); j++)
                    {
                    
                         ItemGst item = (ItemGst)theItemList.get(j);

                                

		       c1 = new PdfPCell(new Phrase(item.SItemCode,smallNormal));
		       c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		       c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		       c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
		       table1.addCell(c1); 
              
                   

                
                 for(int k=0; k<VOrderGroup.size(); k++)
                   {
                   
                    for(int l=0; l<ordergroup.orderNoList.size(); l++)
                    {
                            
                              String SOrderNo = (String)ordergroup.orderNoList.get(l);

                              String SOrderQty       = item.getOrderQty(SOrderNo);

                             


				c1 = new PdfPCell(new Phrase(SOrderQty,smallNormal));
				c1.setHorizontalAlignment(Element.ALIGN_LEFT);
				c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
				table1.addCell(c1);  
              



                    }
                    
               }

 
          }

           }

           document.add(table1);
         
         
           }catch(Exception ex){}

     }



 //

     private void addEmptyCell(PdfPTable table)
     {
          PdfPCell c1 = new PdfPCell();
          table.addCell(c1);
     }

     private void addEmptyCell(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
          table.addCell(c1);
     }

     private void addEmptyRow(PdfPTable table)
     {
          for(int i=0;i<SHead.length;i++)
          {
               PdfPCell c1 = new PdfPCell();
               table.addCell(c1);
          }
     }

     private void addEmptySpanRow(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
          c1.setColspan(SHead.length);
          table.addCell(c1);
     }

     private static void addEmptyLine(Paragraph paragraph, int number)
     {
          for (int i = 0; i < number; i++)
          {
               paragraph.add(new Paragraph(" "));
          }
     }

  private String getOrderNo()
     {
          String SOrderNo= "";
          VOrderNos      = new Vector();


          for(int i=0; i<VOrderGroup.size(); i++)
          {
               String SFirstOrderNo="";
               String SLastOrderNo="";

               OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);

               for(int k=0; k<ordergroup.orderNoList.size(); k++)
               {
                    VOrderNos.addElement((String)ordergroup.orderNoList.get(k));
               }
          }
          Collections.sort(VOrderNos);
          int iSize = VOrderNos.size();

          return VOrderNos.elementAt(0)+" To "+VOrderNos.elementAt(iSize-1);
     }

   private int getIndexOf(String SSupCode)
     {
          int iIndex=-1;

          for(int i=0; i<VOrderGroup.size(); i++)
          {
               OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);

               if(SSupCode.equals(ordergroup.SSupCode) )
               {
                    iIndex = i;
                    break;
               }
          }
          return iIndex;
     }
  private int getItemIndexOf(String SItemCode)
     {
          int iIndex=-1;

          for(int i=0; i<VItem.size(); i++)
          {
               ItemGst item = (ItemGst)VItem.elementAt(i);

               if(SItemCode.equals(item.SItemCode))
               {
                    iIndex = i;
                    break;
               }
          }
          return iIndex;
     }

  public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iRIGHTBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iRIGHTBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iRIGHTBorder, int iTopBorder, int iRightBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        //c1.setRowspan(iRowSpan);
        c1.setBorder(iRIGHTBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iRIGHTBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iRIGHTBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iRIGHTBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iRIGHTBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }


}
