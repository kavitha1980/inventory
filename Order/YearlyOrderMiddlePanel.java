package Order;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class YearlyOrderMiddlePanel extends JPanel 
{
     YDOInvMiddlePanel   MiddlePanel;
     Object              RowData[][];
     Object              IdData[];
     Object              DescId[];

     String              ColumnData[]   = {"Code","Name","Vat","Order Block","MRS No","Qty","Rate","Discount(%)","CenVat(%)","Tax(%)","Surcharge(%)","Basic","Discount (Rs)","CenVat(Rs)","Tax(Rs)","Surcharge(Rs)","Net (Rs)"};
     String[]            ColumnType     = new String[22];

     Common              common;
     String              SItemTable;
     JLayeredPane        DeskTop;
     Vector              VCode,VName,VItemsStatus,VOldCode,VOldMrsNo,VMrsSlNo,VMrsUserCode,VMrsType,VNameCode;//,VTaxClaim;
     int                 iMillCode;
     int                 iOrderStatus = 0;
     boolean             bflag;

     public YearlyOrderMiddlePanel(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VNameCode,int iMillCode,boolean bflag,String SItemTable)
     {
          this.DeskTop        = DeskTop;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VNameCode      = VNameCode;
          this.iMillCode      = iMillCode;
          this.bflag          = bflag;
          this.SItemTable     = SItemTable;

          setLayout(new BorderLayout());
     }

     public void createComponents()
     {
          try
          {
               setColType();
               MiddlePanel           = new YDOInvMiddlePanel(DeskTop,VCode,VName,VNameCode,RowData,ColumnData,ColumnType,iMillCode,SItemTable);
               add(MiddlePanel,BorderLayout.CENTER);
               MiddlePanel.ReportTable.requestFocus();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void createComponents(Vector VCode,Vector VName,int iOrderStatus,Vector VItemsStatus,String SItemTable,String SSupCode,String SSeqId,Vector theItemVector,String SOrderDate)
     {
          try
          {
               setColType();

               MiddlePanel         = new YDOInvMiddlePanel(DeskTop,VCode,VName,VNameCode,RowData,ColumnData,ColumnType,bflag,iOrderStatus,VItemsStatus,iMillCode,SItemTable,SSupCode,SSeqId,theItemVector,SOrderDate);
               add(MiddlePanel,BorderLayout.CENTER);
               MiddlePanel         . ReportTable.requestFocus();	
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setRowData(Vector VSelectedCode,Vector VSelectedName,Vector VSelectedMRS,Vector VSelectedQty,Vector VSelectedUnit,Vector VSelectedDept,Vector VSelectedGroup,Vector VSelectedDue,Vector VSelectedUser)
     {
          RowData     = new Object[VSelectedCode.size()][17];
          for(int i=0;i<VSelectedCode.size();i++)
          {
               RowData[i][0]  = (String)VSelectedCode  .elementAt(i);
               RowData[i][1]  = (String)VSelectedName  .elementAt(i);
               RowData[i][2]  = " ";
               RowData[i][3]  = " ";
               RowData[i][4]  = (String)VSelectedMRS   .elementAt(i);                 
               RowData[i][5]  = (String)VSelectedQty   .elementAt(i);
               RowData[i][6]  = " ";
               RowData[i][7]  = " ";
               RowData[i][8]  = " ";
               RowData[i][9]  = " ";
               RowData[i][10] = " ";
               RowData[i][11] = " ";
               RowData[i][12] = " ";
               RowData[i][13] = " ";
               RowData[i][14] = " ";
               RowData[i][15] = " ";
               RowData[i][16] = " ";
          }
     }

     public void setRowData(Vector VSelectedCode,Vector VSelectedName)
     {
          RowData     = new Object[VSelectedCode.size()][17];
          for(int i=0;i<VSelectedCode.size();i++)
          {
               RowData[i][0]  = (String)VSelectedCode.elementAt(i);
               RowData[i][1]  = (String)VSelectedName.elementAt(i);
               RowData[i][2]  = " ";
               RowData[i][3]  = " ";
               RowData[i][4]  = " ";
               RowData[i][5]  = " ";
               RowData[i][6]  = " ";
               RowData[i][7]  = " ";
               RowData[i][8]  = " ";
               RowData[i][9]  = " ";
               RowData[i][10] = " ";
               RowData[i][11] = " ";
               RowData[i][12] = " ";
               RowData[i][13] = " ";
               RowData[i][14] = " ";
               RowData[i][15] = " ";
               RowData[i][16] = " ";
          }
     }

     public void setColType()
     {
          ColumnType[0]  = "S";
          ColumnType[1]  = "S";

          if(bflag)
               ColumnType[2]  = "S";
          else
               ColumnType[2]  = "B";
          if(bflag)
               ColumnType[3]  = "S";
          else
               ColumnType[3]  = "B";
          if(bflag)
               ColumnType[4]  = "S";
          else
               ColumnType[4]  = "B";
          if(bflag)
               ColumnType[5]  = "S";
          else
               ColumnType[5]  = "B";
          if(bflag)
               ColumnType[6]  = "N";
          else
               ColumnType[6]  = "B";
          if(bflag)
               ColumnType[7]  = "N";
          else
               ColumnType[7]  = "B";
          if(bflag)
               ColumnType[8]  = "N";
          else
               ColumnType[8]  = "B";
          if(bflag)
               ColumnType[9]  = "N";
          else
               ColumnType[9]  = "B";
          if(bflag)
               ColumnType[10]  = "N";
          else
               ColumnType[10]  = "B";

          ColumnType[11] = "N";
          ColumnType[12] = "N";
          ColumnType[13] = "N";
          ColumnType[14] = "N";
          ColumnType[15] = "N";
          ColumnType[16] = "N";
     }

}
