package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class YearlyOrderPlanFrame extends JInternalFrame
{

     // The three Major Partions of OrderPlanFrame
     JPanel TopPanel,TopLeftPanel,TopRightPanel,BottomPanel;

     TabReport tabreport;

     Object RowData[][];

     String ColumnData[] = {"Material Code","Material Name","Total Mrs Qty","Rate","Discount (%)","Cenvat (%)","Tax (%)","Surcharge (%)"};
     String ColumnType[] = {"S"            ,"S"            ,"N"            ,"E"   ,"E"           ,"E"         ,"E"      ,"E"            };


     // Components for TopPanel
     JButton    BSupplier;
     NextField  TOrderNo,TPayDays;
     JTextField TSupCode;
     JTextField TAdvance,TPayTerm,TRefArea;
     DateField  TDate;
     JComboBox  JCTo,JCThro,JEPCG,JCOrderType,JCProject,JCState,JCPort;

     // Components for BottomPanel
     JButton    BOk,BCancel;
     
     Connection theConnection = null;

     // Parameters Common for Both Manual and Auto Mode
     JLayeredPane DeskTop;
     StatusPanel SPanel;
     Vector VCode,VName;

     // Counterpart of Parameters from MatReqFrame Manual Mode
     Vector VSelectedCode,VSelectedName,VSelectedQty;

     Vector VTo,VThro,VToCode,VThroCode,VPortCode,VPortName;

     // Packed Vectors for Automatic Pooling Technology
     YearlyOrderRec orderrec;

     // Utilities 
     Common common = new Common();
     Control control = new Control();

     int iMrsLink=0;    // Identifier of Manual Mode and Auto Mode
                        // 0 indicates Manual Mode Setting
                        // 1 indicates Auto   Mode Setting

     FileWriter FW;

     int iUserCode;
     int iMillCode=0;

     String SYearCode;
     String SItemTable,SSupTable;
     Vector theMrsVector;

     Vector theOrderVector;

	String SToName,SThroName;

     boolean bComflag  = true;

     // Called When Manual Order Placement Option is clicked
     YearlyOrderPlanFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VSelectedCode,Vector VSelectedName,Vector VSelectedQty,StatusPanel SPanel,int iUserCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable,Vector theMrsVector)
     {
          this.DeskTop           = DeskTop;
          this.VCode             = VCode;
          this.VName             = VName;
          this.VSelectedCode     = VSelectedCode;
          this.VSelectedName     = VSelectedName;
          this.VSelectedQty      = VSelectedQty;
          this.SPanel            = SPanel;
          this.iUserCode         = iUserCode;
          this.iMillCode         = iMillCode;
          this.SYearCode         = SYearCode;
          this.SItemTable        = SItemTable;
          this.SSupTable         = SSupTable;
          this.theMrsVector      = theMrsVector;


          iMrsLink=0;                              // Indicates Manual Mode

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setTabReport();
          show();
     }

     // Called From OrderPooling Class When Auto Mode is selected
     YearlyOrderPlanFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,YearlyOrderRec orderrec,StatusPanel SPanel,int iUserCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable,Vector theMrsVector)
     {
          this.DeskTop           = DeskTop;
          this.VCode             = VCode;
          this.VName             = VName;
          this.orderrec          = orderrec;
          this.SPanel            = SPanel;
          this.iUserCode         = iUserCode;
          this.iMillCode         = iMillCode;
          this.SYearCode         = SYearCode;
          this.SItemTable        = SItemTable;
          this.SSupTable         = SSupTable;
          this.theMrsVector      = theMrsVector;

          iMrsLink=1;                           // indicates Auto Mode Setting

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          setReference();
          addListeners();
          setTabReport();
          show();
     }

     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnection  =    oraConnection.getConnection();
     }

     public void setReference()
     {
          TSupCode  .setText(orderrec.SSupCode);
          BSupplier .setText(orderrec.SSupName);

          /*int iToId   = VToCode.indexOf(orderrec.SToCode);
          int iThroId = VThroCode.indexOf(orderrec.SThroCode);

          JCTo      .setSelectedIndex((iToId==-1 ? 0:iToId));
          JCThro    .setSelectedIndex((iThroId==-1 ? 0:iThroId));*/

          TPayTerm  .setText(orderrec.SPayTerm);
          TPayDays  .setText(orderrec.SPayDays);

          for(int i=0;i<orderrec.VOrdNo.size();i++)
          {
               String SOrdNo  = (String)orderrec.VOrdNo.elementAt(i);
               getNextOrderNo();
               String SBlock  = (String)orderrec.VBlock.elementAt(i);
               String SDate   = common.parseDate((String)orderrec.VDate.elementAt(i));
               TRefArea       .setText(SBlock+"-"+SOrdNo+" Dt."+SDate+"\n");
          }
     }

     public void createComponents()
     {
          BOk            = new JButton("Okay");
          BCancel        = new JButton("Abort");
          BSupplier      = new JButton("Supplier");

          TOrderNo       = new NextField();
          TDate          = new DateField();
          TSupCode       = new JTextField();
          TAdvance       = new JTextField();
          TPayTerm       = new JTextField();
          TPayDays       = new NextField();
          TRefArea       = new JTextField();
          JEPCG          = new JComboBox();
          JCOrderType    = new JComboBox();
          JCProject      = new JComboBox();
          JCState        = new JComboBox();

          getVTo();
          JCPort         = new JComboBox(VPortName);
          JCTo           = new JComboBox(VTo);
          JCThro         = new JComboBox(VThro);

          JCPort.setSelectedItem("Unknown");
		JCTo.setSelectedItem(SToName);
		JCThro.setSelectedItem(SThroName);

          TopPanel       = new JPanel();
          TopLeftPanel   = new JPanel();
          TopRightPanel  = new JPanel();
          BottomPanel    = new JPanel();

          TDate          .setTodayDate();
          TOrderNo       .setEditable(false);
          TPayDays       .setEditable(false);
          TAdvance       .setEditable(false);
     }

     public void setLayouts()
     {
          setTitle("Order Placement Against Selected MRS Materials");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,550);

          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(1,2));
          TopLeftPanel        .setLayout(new GridLayout(7,2));
          TopRightPanel       .setLayout(new GridLayout(8,2));
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          JEPCG.addItem("Non-EPCG Order   ");
          JEPCG.addItem("EPCG Order       ");

          JCOrderType.addItem("Local Order      ");
          JCOrderType.addItem("Import           ");

          TopLeftPanel.add(new JLabel("Order No"));
          TopLeftPanel.add(TOrderNo);

          TopLeftPanel.add(new JLabel("Order Date"));
          TopLeftPanel.add(TDate);

          TopLeftPanel.add(new JLabel("Reference"));
          TopLeftPanel.add(TRefArea);

          TopLeftPanel.add(new JLabel("EPCG Order   "));
          TopLeftPanel.add(JEPCG);

          TopLeftPanel.add(new JLabel("Order Type   "));
          TopLeftPanel.add(JCOrderType);

          TopLeftPanel.add(new JLabel("Project/Non"));
          TopLeftPanel.add(JCProject);

          TopRightPanel.add(new JLabel("Supplier"));
          TopRightPanel.add(BSupplier);

          TopRightPanel.add(new JLabel("Book To"));
          TopRightPanel.add(JCTo);

          TopRightPanel.add(new JLabel("Book Thro"));
          TopRightPanel.add(JCThro);

          TopRightPanel.add(new JLabel("Advance"));
          TopRightPanel.add(TAdvance);

          TopRightPanel.add(new JLabel("Payment Terms"));
          TopRightPanel.add(TPayTerm);

          TopRightPanel.add(new JLabel("Intra/Inter State"));
          TopRightPanel.add(JCState);

          JCProject  .addItem("Other Projects    ");
          JCProject  .addItem("Machinery Projects");

          JCState    .addItem("None             ");
          JCState    .addItem("Intra Stete      ");
          JCState    .addItem("Inter State      ");

          JCOrderType.setEnabled(false);
          JCProject.setEnabled(false);
          JCState.setEnabled(false);
          JCPort.setEnabled(false);

          TopRightPanel.add(new JLabel("Port         "));
          TopRightPanel.add(JCPort);

          TopPanel.add(TopLeftPanel);
          TopPanel.add(TopRightPanel);

          TopLeftPanel.setBorder(new TitledBorder("Order Id Block-I"));
          TopRightPanel.setBorder(new TitledBorder("Order Id Block-II"));

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          JCProject.setEnabled(false);
          JCProject.setSelectedIndex(0);

          getNextOrderNo();
     }

     public void addListeners()
     {
           JCOrderType.addFocusListener(new FocListener());
           JEPCG.addFocusListener(new FocListener());
           JCState.addFocusListener(new FocListener());
           JCPort.addFocusListener(new FocListener());
           BSupplier.addActionListener(new SupplierSearch(DeskTop,TSupCode,SSupTable));
           BOk.addActionListener(new ActList());
           BCancel.addActionListener(new ActList());
     }

     public class FocListener implements FocusListener
     {
          public void focusLost(FocusEvent fe)
          {
               if (fe.getSource() == JEPCG)
               {
                    if (JEPCG.getSelectedIndex() == 0)
                    {
                         JCTo.requestFocus();
                         JCOrderType.setEnabled(false);
                         JCState.setEnabled(false);
                         JCPort.setEnabled(false);
                    }
                    else
                    {
                         JCOrderType.requestFocus();
                         JCOrderType.setEnabled(true);
                         JCState.setEnabled(true);
                         JCPort.setEnabled(true);
                    }
               }

               if ((fe.getSource() == JCOrderType) && (JEPCG.getSelectedIndex()==1))
               {
                    if (JCOrderType.getSelectedIndex() == 1)
                    {
                         JCState.setEnabled(false);
                         JCPort.setEnabled(true);
                         JCPort.setSelectedIndex(1);
                         JCState.setSelectedIndex(0);                         
                    }
                    else
                    {
                         JCPort.setSelectedIndex(0);                         
                         JCPort.setEnabled(false);
                         JCState.setEnabled(true);
                         JCState.setSelectedIndex(1);                         
                    }
               }
          }
          public void focusGained(FocusEvent fe)
          {
          }
     }

     public class ActList implements ActionListener
     {
            public void actionPerformed(ActionEvent ae)
            {

                  if(ae.getSource()==BOk)
                  {
                       BOk.setEnabled(false);

                       if(!isValidNew())
                       {
                           common.warn(DeskTop,SPanel);
                           BOk.setEnabled(true);
                           return;
                       }

                       try
                       {
                            setOrderClass();

                            insertOrderDetails();
                       }
                       catch(Exception Ex)
                       {
                            Ex.printStackTrace();
                            bComflag = false;
                       }

                       try
                       {
                             if(bComflag)
                             {
                                  theConnection  . commit();
                                  System         . out.println("Commit");
                                  theConnection  . setAutoCommit(true);
                                  removeHelpFrame();
                             }
                             else
                             {
                                  theConnection  . rollback();
                                  System         . out.println("RollBack");
                                  theConnection  . setAutoCommit(true);
                                  BOk.setEnabled(true);
                             }
                       }catch(Exception ex)
                       {
                             ex.printStackTrace();
                       }
                  }
                  if(ae.getSource()==BCancel)
                  {
                       removeHelpFrame();
                  }     
            }
     }

     public void setOrderClass()
     {
          try
          {
               YearlyOrderClass yearlyOrderClass=null;
               theOrderVector = new Vector();

               String SSupCode = common.parseNull(TSupCode.getText());

               for(int i=0;i<RowData.length;i++)
               {
                    String SItemCode  = (String)RowData[i][0];

                    String SRate      = (String)RowData[i][3];
                    String SDiscPer   = (String)RowData[i][4];
                    String SCenvatPer = (String)RowData[i][5];
                    String STaxPer    = (String)RowData[i][6];
                    String SSurPer    = (String)RowData[i][7];

                    String SPOrderNo   = "0";
                    String SPSource    = "0";
                    String SPBlockCode = "0";
                    String SPBlock     = "";
                    String SPOrderDate = "";

                    if(iMrsLink==1)
                    {
                         SPOrderNo   = (String)orderrec.VOrdNo.elementAt(i);
                         SPSource    = "1";
                         SPBlock     = (String)orderrec.VBlock.elementAt(i);
                         SPBlockCode = (String)orderrec.VBlockCode.elementAt(i);
                         SPOrderDate = (String)orderrec.VDate.elementAt(i);
                    }

                    int iMIndex = getMrsIndexOf(SItemCode);
  
                    YearlyMrsClass yearlyMrsClass = (YearlyMrsClass)theMrsVector.elementAt(iMIndex);

                    for(int k=0;k<yearlyMrsClass.VMrsNo.size();k++)
                    {
                         String SMonthCode = (String)yearlyMrsClass.VMrsPlanMonth.elementAt(k);

                         String SMrsNo           = (String)yearlyMrsClass.VMrsNo.elementAt(k);
                         String SQty             = (String)yearlyMrsClass.VQty.elementAt(k);
                         String SUnitCode        = (String)yearlyMrsClass.VUnitCode.elementAt(k);
                         String SDeptCode        = (String)yearlyMrsClass.VDeptCode.elementAt(k);
                         String SGroupCode       = (String)yearlyMrsClass.VGroupCode.elementAt(k);
                         String SCatl            = (String)yearlyMrsClass.VCatl.elementAt(k);
                         String SDraw            = (String)yearlyMrsClass.VDraw.elementAt(k);
                         String SMake            = (String)yearlyMrsClass.VMake.elementAt(k);
                         String SDueDate         = (String)yearlyMrsClass.VDueDate.elementAt(k);
                         String SMrsSlNo         = (String)yearlyMrsClass.VSlNo.elementAt(k);
                         String SRemarks         = (String)yearlyMrsClass.VRemarks.elementAt(k);
                         String SMrsAuthUserCode = (String)yearlyMrsClass.VMrsAuthUserCode.elementAt(k);
                         String SEnquiryNo       = (String)yearlyMrsClass.VEnquiryNo.elementAt(k);

                         int iOIndex = getOrderIndexOf(SSupCode,SMonthCode);

                         if(iOIndex==-1)
                         {
                              yearlyOrderClass = new YearlyOrderClass(SSupCode,SMonthCode,iMillCode);
                              theOrderVector.addElement(yearlyOrderClass);
                              iOIndex = theOrderVector.size()-1;
                         }
                         yearlyOrderClass = (YearlyOrderClass)theOrderVector.elementAt(iOIndex);
                         yearlyOrderClass.setData(SItemCode,SMrsNo,SQty,SUnitCode,SDeptCode,SGroupCode,SCatl,SDraw,SMake,SDueDate,SMrsSlNo,SRemarks,SMrsAuthUserCode,SEnquiryNo,SRate,SDiscPer,SCenvatPer,STaxPer,SSurPer,SPOrderNo,SPSource,SPBlock,SPBlockCode,SPOrderDate);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private int getMrsIndexOf(String SItemCode)
     {
          int iIndex =-1;

          for(int i=0; i<theMrsVector.size(); i++)
          {
               YearlyMrsClass yearlyMrsClass = (YearlyMrsClass)theMrsVector.elementAt(i);
               if(((yearlyMrsClass.SItemCode).equals(SItemCode)) && (yearlyMrsClass.iMillCode==iMillCode))
               {
                    iIndex=i;
                    break;
               }
          }
          return iIndex;
     }

     private int getOrderIndexOf(String SSupCode,String SMonthCode)
     {
          int iIndex =-1;

          for(int i=0; i<theOrderVector.size(); i++)
          {
               YearlyOrderClass yearlyOrderClass = (YearlyOrderClass)theOrderVector.elementAt(i);
               if(((yearlyOrderClass.SSupCode).equals(SSupCode)) && ((yearlyOrderClass.SMonthCode).equals(SMonthCode)) && (yearlyOrderClass.iMillCode==iMillCode))
               {
                    iIndex=i;
                    break;
               }
          }
          return iIndex;
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public void insertOrderDetails()
     {
          String QString1 = "Insert Into PurchaseOrder (OrderNo,OrderDate,OrderBlock,MrsNo,Sup_Code,Reference,Advance,PayTerms,PayDays,ToCode,ThroCode,Item_Code,Qty,Rate,DiscPer,Disc,CenvatPer,CenVat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,Unit_Code,Dept_Code,Group_Code,DueDate,SlNo,EPCG,OrderType,Project_order,state,portcode,UserCode,ModiDate,MillCode,MRSSLNO,MrsAuthUserCode,OrderTypeCode,PrevOrderNo,OrderSourceTypeCode,PrevOrderBlock,TAXCLAIMABLE,Authentication,SeqId,id) Values (";
          String QString2 = "Insert Into MatDesc (OrderNo,OrderDate,OrderBlock,Item_Code,Descr,Make,Draw,Catl,SlNo,id,MillCode) Values (";

          try
          {
               if(theConnection.getAutoCommit())
                         theConnection.setAutoCommit(false);

               Statement theStatement    = theConnection.createStatement();

               String SDate  = common.getServerPureDate();
               int iSeqId    = getSeqCode("Purchase_SeqId");

               for(int i=0;i<theOrderVector.size();i++)
               {
                    YearlyOrderClass yearlyOrderClass = (YearlyOrderClass)theOrderVector.elementAt(i);

                    String SSupCode   = yearlyOrderClass.SSupCode;
                    String SMonthCode = yearlyOrderClass.SMonthCode;

                    int iSlNo=0;

                    String SOrderNo = getInsertOrderNo();

                    for(int k=0;k<yearlyOrderClass.VItemCode.size();k++)
                    {
                         iSlNo++;

                         String SItemCode  = (String)yearlyOrderClass.VItemCode.elementAt(k);

                         double dQty       = common.toDouble((String)yearlyOrderClass.VQty.elementAt(k));
                         double dRate      = common.toDouble((String)yearlyOrderClass.VRate.elementAt(k));
                         double dDiscPer   = common.toDouble((String)yearlyOrderClass.VDiscPer.elementAt(k));
                         double dCenvatPer = common.toDouble((String)yearlyOrderClass.VCenvatPer.elementAt(k));
                         double dTaxPer    = common.toDouble((String)yearlyOrderClass.VTaxPer.elementAt(k));
                         double dSurPer    = common.toDouble((String)yearlyOrderClass.VSurPer.elementAt(k));

                         double dGross  = dQty*dRate;
                         double dDisc   = dGross*dDiscPer/100;
                         double dCenvat = (dGross-dDisc)*dCenvatPer/100;
                         double dTax    = (dGross-dDisc+dCenvat)*dTaxPer/100;
                         double dSur    = (dTax)*dSurPer/100;
                         double dNet    = dGross-dDisc+dCenvat+dTax+dSur;

                         String SAdd    = "0";
                         String SLess   = "0";
                         String SMisc   = "0";

                         String SUnitCode  = (String)yearlyOrderClass.VUnitCode.elementAt(k);
                         String SDeptCode  = (String)yearlyOrderClass.VDeptCode.elementAt(k);
                         String SGroupCode = (String)yearlyOrderClass.VGroupCode.elementAt(k);
                         String SBlockCode = "0";
                         String SDueDate   = (String)yearlyOrderClass.VDueDate.elementAt(k);
     
                         int iTaxClaim     = 0;
     
     
                         String SMrsNo      = (String)yearlyOrderClass.VMrsNo.elementAt(k);
                         int iMRSSlNo       = common.toInt((String)yearlyOrderClass.VMrsSlNo.elementAt(k));
                         int iMRSUserCode   = common.toInt((String)yearlyOrderClass.VMrsAuthUserCode.elementAt(k));
                         String SEnqNo      = (String)yearlyOrderClass.VEnquiryNo.elementAt(k);
                         int iMRSType       = 2;
                         int iPrevOrderNo   = common.toInt((String)yearlyOrderClass.VPOrderNo.elementAt(k));
                         int iPrevSource    = common.toInt((String)yearlyOrderClass.VPSource.elementAt(k));
                         int iPrevBlockCode = common.toInt((String)yearlyOrderClass.VPBlockCode.elementAt(k));
                         String SPrevBlock  = (String)yearlyOrderClass.VPBlock.elementAt(k);
                         String SPOrderDate = (String)yearlyOrderClass.VPOrderDate.elementAt(k);
     
                         int iDeptCode  = common.toInt(SDeptCode);
                         int iGroupCode = common.toInt(SGroupCode);

                         String SReference = "";

                         if(iMrsLink==0)
                         {
                              SReference   = (TRefArea.getText()).toUpperCase();
                         }
                         else
                         {
                              SReference   = SPrevBlock+"-"+iPrevOrderNo+" DT."+common.parseDate(SPOrderDate);
                         }

                         String SDesc = common.parseNull((String)yearlyOrderClass.VRemarks.elementAt(k));
                         String SMake = common.parseNull((String)yearlyOrderClass.VMake.elementAt(k));
                         String SDraw = common.parseNull((String)yearlyOrderClass.VDraw.elementAt(k));
                         String SCatl = common.parseNull((String)yearlyOrderClass.VCatl.elementAt(k));

                         
                         String QS1 = QString1;
                         QS1 = QS1+"0"+SOrderNo+",";
                         QS1 = QS1+"'"+SDate+"',";
                         QS1 = QS1+""+SBlockCode+",";
                         QS1 = QS1+"0"+SMrsNo+",";
                         QS1 = QS1+"'"+SSupCode+"',";
                         QS1 = QS1+"'"+SReference+"',";    
                         QS1 = QS1+"0"+TAdvance.getText()+",";
                         QS1 = QS1+"'"+(TPayTerm.getText()).toUpperCase()+"',";
                         QS1 = QS1+"0"+TPayDays.getText()+",";
                         QS1 = QS1+"0"+VToCode.elementAt(JCTo.getSelectedIndex())+",";
                         QS1 = QS1+"0"+VThroCode.elementAt(JCThro.getSelectedIndex())+",";
                         QS1 = QS1+"'"+SItemCode+"',";
                         QS1 = QS1+"0"+common.getRound(dQty,3)+",";
                         QS1 = QS1+"0"+common.getRound(dRate,4)+",";
                         QS1 = QS1+"0"+common.getRound(dDiscPer,2)+",";
                         QS1 = QS1+"0"+common.getRound(dDisc,2)+",";
                         QS1 = QS1+"0"+common.getRound(dCenvatPer,2)+",";
                         QS1 = QS1+"0"+common.getRound(dCenvat,2)+",";
                         QS1 = QS1+"0"+common.getRound(dTaxPer,2)+",";
                         QS1 = QS1+"0"+common.getRound(dTax,2)+",";
                         QS1 = QS1+"0"+common.getRound(dSurPer,2)+",";
                         QS1 = QS1+"0"+common.getRound(dSur,2)+",";
                         QS1 = QS1+"0"+common.getRound(dNet,2)+",";
                         QS1 = QS1+"0"+SAdd+",";
                         QS1 = QS1+"0"+SLess+",";
                         QS1 = QS1+"0"+SMisc+",";
                         QS1 = QS1+"0"+SUnitCode+",";
                         QS1 = QS1+iDeptCode+",";
                         QS1 = QS1+iGroupCode+",";
                         QS1 = QS1+"'"+SDueDate+"',";
                         QS1 = QS1+iSlNo+",";
                         QS1 = QS1+JEPCG.getSelectedIndex()+",";
                         QS1 = QS1+JCOrderType.getSelectedIndex()+",";
                         QS1 = QS1+JCProject.getSelectedIndex()+",";
                         QS1 = QS1+JCState.getSelectedIndex()+",";
                         QS1 = QS1+VPortCode.elementAt(JCPort.getSelectedIndex())+",";
                         QS1 = QS1+iUserCode+",";
                         QS1 = QS1+"'"+common.getServerDateTime()+"',";
                         QS1 = QS1+iMillCode+",";
                         QS1 = QS1+iMRSSlNo+",";
                         QS1 = QS1+iMRSUserCode+",";
                         QS1 = QS1+iMRSType+",";
                         QS1 = QS1+iPrevOrderNo+",";
                         QS1 = QS1+iPrevSource+",";
                         QS1 = QS1+iPrevBlockCode+",";
                         QS1 = QS1+iTaxClaim+",";
                         QS1 = QS1+"1"+",";
                         QS1 = QS1+iSeqId+",";
                         QS1 = QS1+" purchaseorder_seq.nextval) " ;

                         theStatement.executeUpdate(QS1);

                         String QS2 = QString2;
                         QS2 = QS2+"0"+SOrderNo+",";
                         QS2 = QS2+"'"+SDate+"',";
                         QS2 = QS2+""+SBlockCode+",";
                         QS2 = QS2+"'"+SItemCode+"',";
                         QS2 = QS2+"'"+common.getNarration(SDesc)+"',";
                         QS2 = QS2+"'"+common.getNarration(SMake)+"',";
                         QS2 = QS2+"'"+SDraw+"',";
                         QS2 = QS2+"'"+SCatl+"',";
                         QS2 = QS2+iSlNo+",";
                         QS2 = QS2+" matdesc_seq.nextval , ";
                         QS2 = QS2+"0"+iMillCode+" )";
     
                         theStatement.executeUpdate(QS2);


                         String QS3 = "Update MRS set ";
                         QS3 = QS3+"OrderNo   ="+SOrderNo+",";
                         QS3 = QS3+"OrderBlock="+SBlockCode;
                         QS3 = QS3+" Where MrsNo="+SMrsNo;
                         QS3 = QS3+" And Item_Code='"+SItemCode+"'";
                         QS3 = QS3+" And YearlyPlanningMonth="+SMonthCode;

                         theStatement.executeUpdate(QS3);

                         if(common.toInt(SEnqNo)>0)
                         {
                              String QS4 = "Update Enquiry set CloseStatus=1 ";
                              QS4 = QS4+" Where EnqNo="+SEnqNo;
                              QS4 = QS4+" and Mrs_No="+SMrsNo;
                              QS4 = QS4+" and Item_Code='"+SItemCode+"'";

                              theStatement.executeUpdate(QS4);
                         }


                         String QS5 = " Update YearlyPlanning set OrderConverted=1 ";
                         QS5 = QS5+" Where Month="+SMonthCode;
                         QS5 = QS5+" And ItemCode='"+SItemCode+"'";

                         theStatement.executeUpdate(QS5);
                    }
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     private int getSeqCode(String SSeqName)
     {
           int iSeqValue=0;
           try
           {
                 Statement   theStatement      =     theConnection.createStatement();
                 ResultSet   theResult         =     theStatement.executeQuery("Select "+SSeqName+".nextval from dual");
                 while(theResult.next())
                 {
                       iSeqValue               =     theResult.getInt(1);
                 }
                 theResult.close();
                 theStatement.close();
           }
           catch(Exception e)
           {
                 bComflag = false;
                 e.printStackTrace();
           }
           return iSeqValue;
     }

     public boolean isValidNew()
     {
          boolean bFlag = true;

          try
          {
                FW = new FileWriter(common.getPrintPath()+"Error.prn");
                write("*-----------------------*\n");
                write("*    W A R N I N G      *\n");
                write("*-----------------------*\n\n");
          }
          catch(Exception ex){}

          // Checking for Supplier
          String str = common.parseNull(TSupCode.getText());
          if(str.length()==0)
          {
             write("Supplier Is Not Selected \n\n");
             bFlag = false;
          }

          String str1 = common.parseNull(TPayTerm.getText());
          if(str1.length()==0)
          {
             write("PaymentTerm Is Not Filled \n\n");
             bFlag = false;
          }

         //  Checking for Table Items 
         for(int i=0;i<RowData.length;i++)
         {
               double dQty     = common.toDouble(((String)RowData[i][2]).trim());
               double dRate    = common.toDouble(((String)RowData[i][3]));

               if(dQty <= 0)
               {
                 write("Quantity for Item No "+(i+1)+" is not fed \n");
                 bFlag = false;
               }
               if(dRate <= 0)
               {
                 write("Rate for Item No "+(i+1)+" is not fed \n");
                 bFlag = false;
               }
         }

         try
         {
            write("*-----------------------*\n");
            FW.close();
         }
         catch(Exception ex){}

         return bFlag;
     }
     public void write(String str)
     {
          try
          {
                   FW.write(str);
          }
          catch(Exception ex){}
     }

     public void getNextOrderNo()
     {
          String SOrderNo= "";
          String QS      = "";

          QS = " Select maxno From Config"+iMillCode+""+SYearCode+" where Id=1";

          try
          {

               Statement stat      = theConnection. createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         SOrderNo  = common.parseNull((String)result.getString(1));
                         result    . close();
                         stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
          SOrderNo  = String.valueOf(common.toInt(SOrderNo.trim())+1);
          TOrderNo  . setText(SOrderNo.trim());
     }

     public String getInsertOrderNo()
     {
          String SOrderNo= "";
          String QS      = "";

          QS = " Select (maxno+1) From Config"+iMillCode+""+SYearCode+" where Id=1 for update of MaxNo noWait";

          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);

               Statement stat      = theConnection   . createStatement();

               PreparedStatement thePrepare = theConnection.prepareStatement(" Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 1");

               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         SOrderNo  = common.parseNull((String)result.getString(1));
                         result    . close();

               thePrepare.setInt(1,common.toInt(SOrderNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getInsertOrderNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
          return SOrderNo;
     }

     public void getVTo()
     {
          VTo       = new Vector();
          VToCode   = new Vector();
          VThro     = new Vector();
          VThroCode = new Vector();
          VPortCode = new Vector();
          VPortName = new Vector();

          try
          {
               Statement theStatement    = theConnection.createStatement();
               ResultSet res1 = theStatement.executeQuery("Select ToName,ToCode From BookTo Order By ToName");
               while(res1.next())
               {
                     VTo.addElement(res1.getString(1));
                     VToCode.addElement(res1.getString(2));
               }
               res1.close();

               ResultSet res2 = theStatement.executeQuery("Select ThroName,ThroCode From BookThro Order By ThroName");
               while(res2.next())
               {
                     VThro.addElement(res2.getString(1));
                     VThroCode.addElement(res2.getString(2));
               }
               res2.close();

               ResultSet res3 = theStatement.executeQuery("Select PortName,PortCode From Port Order By PortName");
               while(res3.next())
               {
                     VPortName.addElement(res3.getString(1));
                     VPortCode.addElement(res3.getString(2));
               }
               res3.close();

               ResultSet res4 = theStatement.executeQuery("Select ToName From BookTo Where DivCode="+iMillCode);
               while(res4.next())
               {
                     SToName = common.parseNull(res4.getString(1));
               }
               res4.close();

               ResultSet res5 = theStatement.executeQuery("Select ThroName From BookThro Where DivCode="+iMillCode);
               while(res5.next())
               {
                     SThroName = common.parseNull(res5.getString(1));
               }
               res5.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

      public void setTabReport()
      {
            setRowData();
            try
            {
                  getContentPane().remove(tabreport); 
            }
            catch(Exception ex){}

            try
            {
                  tabreport = new TabReport(RowData,ColumnData,ColumnType);
                  getContentPane().add(tabreport,BorderLayout.CENTER);
                  setSelected(true);
                  DeskTop.repaint();
                  DeskTop.updateUI();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      public void setRowData()
      {
            if(iMrsLink==0)
            {
                 RowData = new Object[VSelectedCode.size()][ColumnData.length];
                 for(int i=0;i<VSelectedCode.size();i++)
                 {
                       RowData[i][0] = (String)VSelectedCode.elementAt(i);
                       RowData[i][1] = (String)VSelectedName.elementAt(i);
                       RowData[i][2] = (String)VSelectedQty.elementAt(i);
                       RowData[i][3] = "";
                       RowData[i][4] = "";
                       RowData[i][5] = "";
                       RowData[i][6] = "";
                       RowData[i][7] = "";
                 }
            }
            else
            {
                 RowData = new Object[orderrec.VCode.size()][ColumnData.length];
                 for(int i=0;i<orderrec.VCode.size();i++)
                 {
                       RowData[i][0] = common.parseNull((String)orderrec.VCode.elementAt(i));
                       RowData[i][1] = common.parseNull((String)orderrec.VName.elementAt(i));
                       RowData[i][2] = common.parseNull((String)orderrec.VQty.elementAt(i));
                       RowData[i][3] = common.parseNull((String)orderrec.VRate.elementAt(i));
                       RowData[i][4] = common.parseNull((String)orderrec.VDiscPer.elementAt(i));
                       RowData[i][5] = common.parseNull((String)orderrec.VCenVatPer.elementAt(i));
                       RowData[i][6] = common.parseNull((String)orderrec.VTaxPer.elementAt(i));
                       RowData[i][7] = common.parseNull((String)orderrec.VSurPer.elementAt(i));
                 }
            }
     }


}
