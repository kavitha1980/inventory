package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class YearlyOrderPlanFrameGst extends JInternalFrame
{

     // The three Major Partions of OrderPlanFrame
     JPanel TopPanel,TopLeftPanel,TopRightPanel,BottomPanel;

     TabReport tabreport;

     Object RowData[][];

     String ColumnData[] = {"Material Code","Material Name","Total Mrs Qty","Rate","Discount (%)","CGST (%)","SGST (%)","IGST (%)","Cess (%)"};
     String ColumnType[] = {"S"            ,"S"            ,"N"            ,"E"   ,"E"           ,"E"       ,"E"       ,"E"       ,"E"       };


     // Components for TopPanel
     JButton    BSupplier;
     NextField  TOrderNo,TPayDays;
     JTextField TSupCode,TSupData;
     JTextField TAdvance,TPayTerm,TRefArea;
     DateField  TDate;
     JComboBox  JCTo,JCThro,JEPCG,JCOrderType,JCProject,JCState,JCPort;

     // Components for BottomPanel
     JButton    BOk,BCancel;
     
     Connection theConnection = null;

     // Parameters Common for Both Manual and Auto Mode
     JLayeredPane DeskTop;
     StatusPanel SPanel;
     Vector VCode,VName;

     // Counterpart of Parameters from MatReqFrame Manual Mode
     Vector VSelectedCode,VSelectedName,VSelectedQty,VSelectedHsnCode;

     Vector VTo,VThro,VToCode,VThroCode,VPortCode,VPortName;

     // Packed Vectors for Automatic Pooling Technology
     YearlyOrderRecGst orderrecgst;

     // Utilities 
     Common common = new Common();
     Control control = new Control();

     int iMrsLink=0;    // Identifier of Manual Mode and Auto Mode
                        // 0 indicates Manual Mode Setting
                        // 1 indicates Auto   Mode Setting

     FileWriter FW;

     int iUserCode;
     int iMillCode=0;

     String SYearCode;
     String SItemTable,SSupTable;
     Vector theMrsVector;

     Vector theOrderVector;

     String SToName,SThroName;

     String SSeleSupCode,SSeleSupType,SSeleStateCode;
     int iStateCheck = 0;
     int iTypeCheck  = 0;
     int iTaxCheck = 0;

     boolean bComflag  = true;

     // Called When Manual Order Placement Option is clicked
     YearlyOrderPlanFrameGst(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VSelectedCode,Vector VSelectedName,Vector VSelectedQty,Vector VSelectedHsnCode,StatusPanel SPanel,int iUserCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable,Vector theMrsVector)
     {
          this.DeskTop           = DeskTop;
          this.VCode             = VCode;
          this.VName             = VName;
          this.VSelectedCode     = VSelectedCode;
          this.VSelectedName     = VSelectedName;
          this.VSelectedQty      = VSelectedQty;
          this.VSelectedHsnCode  = VSelectedHsnCode;
          this.SPanel            = SPanel;
          this.iUserCode         = iUserCode;
          this.iMillCode         = iMillCode;
          this.SYearCode         = SYearCode;
          this.SItemTable        = SItemTable;
          this.SSupTable         = SSupTable;
          this.theMrsVector      = theMrsVector;


          iMrsLink=0;                              // Indicates Manual Mode

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setTabReport();
          show();
     }

     // Called From OrderPooling Class When Auto Mode is selected
     YearlyOrderPlanFrameGst(JLayeredPane DeskTop,Vector VCode,Vector VName,YearlyOrderRecGst orderrecgst,StatusPanel SPanel,int iUserCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable,Vector theMrsVector)
     {
          this.DeskTop           = DeskTop;
          this.VCode             = VCode;
          this.VName             = VName;
          this.orderrecgst       = orderrecgst;
          this.SPanel            = SPanel;
          this.iUserCode         = iUserCode;
          this.iMillCode         = iMillCode;
          this.SYearCode         = SYearCode;
          this.SItemTable        = SItemTable;
          this.SSupTable         = SSupTable;
          this.theMrsVector      = theMrsVector;

          iMrsLink=1;                           // indicates Auto Mode Setting

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          setReference();
          addListeners();
          setTabReport();
          show();
     }

     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnection  =    oraConnection.getConnection();
     }

     public void setReference()
     {
          TSupCode  .setText(orderrecgst.SSupCode);
          BSupplier .setText(orderrecgst.SSupName);

	  String SSupData = getSupData(TSupCode.getText());
          TSupData.setText(SSupData);

          /*int iToId   = VToCode.indexOf(orderrecgst.SToCode);
          int iThroId = VThroCode.indexOf(orderrecgst.SThroCode);

          JCTo      .setSelectedIndex((iToId==-1 ? 0:iToId));
          JCThro    .setSelectedIndex((iThroId==-1 ? 0:iThroId));*/

          TPayTerm  .setText(orderrecgst.SPayTerm);
          TPayDays  .setText(orderrecgst.SPayDays);

          for(int i=0;i<orderrecgst.VOrdNo.size();i++)
          {
               String SOrdNo  = (String)orderrecgst.VOrdNo.elementAt(i);
               getNextOrderNo();
               String SBlock  = (String)orderrecgst.VBlock.elementAt(i);
               String SDate   = common.parseDate((String)orderrecgst.VDate.elementAt(i));
               TRefArea       .setText(SBlock+"-"+SOrdNo+" Dt."+SDate+"\n");
          }
     }

     public void createComponents()
     {
          BOk            = new JButton("Okay");
          BCancel        = new JButton("Abort");
          BSupplier      = new JButton("Supplier");

          TOrderNo       = new NextField();
          TDate          = new DateField();
          TSupCode       = new JTextField();
          TSupData       = new JTextField();
          TAdvance       = new JTextField();
          TPayTerm       = new JTextField();
          TPayDays       = new NextField();
          TRefArea       = new JTextField();
          JEPCG          = new JComboBox();
          JCOrderType    = new JComboBox();
          JCProject      = new JComboBox();
          JCState        = new JComboBox();

          getVTo();
          JCPort         = new JComboBox(VPortName);
          JCTo           = new JComboBox(VTo);
          JCThro         = new JComboBox(VThro);

          JCPort.setSelectedItem("Unknown");
	  JCTo.setSelectedItem(SToName);
	  JCThro.setSelectedItem(SThroName);

          TopPanel       = new JPanel();
          TopLeftPanel   = new JPanel();
          TopRightPanel  = new JPanel();
          BottomPanel    = new JPanel();

          TDate          .setTodayDate();
          TOrderNo       .setEditable(false);
          TPayDays       .setEditable(false);
          TAdvance       .setEditable(false);
     }

     public void setLayouts()
     {
          setTitle("Order Placement Against Selected MRS Materials");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,550);

          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(1,2));
          TopLeftPanel        .setLayout(new GridLayout(7,2));
          TopRightPanel       .setLayout(new GridLayout(8,2));
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          JEPCG.addItem("Non-EPCG Order   ");
          JEPCG.addItem("EPCG Order       ");

          JCOrderType.addItem("Local Order      ");
          JCOrderType.addItem("Import           ");

          TopLeftPanel.add(new JLabel("Order No"));
          TopLeftPanel.add(TOrderNo);

          TopLeftPanel.add(new JLabel("Order Date"));
          TopLeftPanel.add(TDate);

          TopLeftPanel.add(new JLabel("Reference"));
          TopLeftPanel.add(TRefArea);

          TopLeftPanel.add(new JLabel("EPCG Order   "));
          TopLeftPanel.add(JEPCG);

          TopLeftPanel.add(new JLabel("Order Type   "));
          TopLeftPanel.add(JCOrderType);

          TopLeftPanel.add(new JLabel("Project/Non"));
          TopLeftPanel.add(JCProject);

          TopRightPanel.add(new JLabel("Supplier"));
          TopRightPanel.add(BSupplier);

          TopRightPanel.add(new JLabel("Book To"));
          TopRightPanel.add(JCTo);

          TopRightPanel.add(new JLabel("Book Thro"));
          TopRightPanel.add(JCThro);

          TopRightPanel.add(new JLabel("Advance"));
          TopRightPanel.add(TAdvance);

          TopRightPanel.add(new JLabel("Payment Terms"));
          TopRightPanel.add(TPayTerm);

          TopRightPanel.add(new JLabel("Intra/Inter State"));
          TopRightPanel.add(JCState);

          JCProject  .addItem("Other Projects    ");
          JCProject  .addItem("Machinery Projects");

          JCState    .addItem("None             ");
          JCState    .addItem("Intra Stete      ");
          JCState    .addItem("Inter State      ");

          JCOrderType.setEnabled(false);
          JCProject.setEnabled(false);
          JCState.setEnabled(false);
          JCPort.setEnabled(false);

          TopRightPanel.add(new JLabel("Port         "));
          TopRightPanel.add(JCPort);

          TopPanel.add(TopLeftPanel);
          TopPanel.add(TopRightPanel);

          TopLeftPanel.setBorder(new TitledBorder("Order Id Block-I"));
          TopRightPanel.setBorder(new TitledBorder("Order Id Block-II"));

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          JCProject.setEnabled(false);
          JCProject.setSelectedIndex(0);

          getNextOrderNo();
     }

     public void addListeners()
     {
           JCOrderType.addFocusListener(new FocListener());
           JEPCG.addFocusListener(new FocListener());
           JCState.addFocusListener(new FocListener());
           JCPort.addFocusListener(new FocListener());
           BSupplier.addActionListener(new YearlyOrderSupplierSearch(DeskTop,TSupCode,SSupTable,TSupData,this));
           BOk.addActionListener(new ActList());
           BCancel.addActionListener(new ActList());
     }

     public class FocListener implements FocusListener
     {
          public void focusLost(FocusEvent fe)
          {
               if (fe.getSource() == JEPCG)
               {
                    if (JEPCG.getSelectedIndex() == 0)
                    {
                         JCTo.requestFocus();
                         JCOrderType.setEnabled(false);
                         JCState.setEnabled(false);
                         JCPort.setEnabled(false);
                    }
                    else
                    {
                         JCOrderType.requestFocus();
                         JCOrderType.setEnabled(true);
                         JCState.setEnabled(true);
                         JCPort.setEnabled(true);
                    }
               }

               if ((fe.getSource() == JCOrderType) && (JEPCG.getSelectedIndex()==1))
               {
                    if (JCOrderType.getSelectedIndex() == 1)
                    {
                         JCState.setEnabled(false);
                         JCPort.setEnabled(true);
                         JCPort.setSelectedIndex(1);
                         JCState.setSelectedIndex(0);                         
                    }
                    else
                    {
                         JCPort.setSelectedIndex(0);                         
                         JCPort.setEnabled(false);
                         JCState.setEnabled(true);
                         JCState.setSelectedIndex(1);                         
                    }
               }
          }
          public void focusGained(FocusEvent fe)
          {
          }
     }

     public class ActList implements ActionListener
     {
            public void actionPerformed(ActionEvent ae)
            {

                  if(ae.getSource()==BOk)
                  {
                       BOk.setEnabled(false);

		       checkTaxData();

		       if(iTaxCheck==0)
		       {
                           BOk.setEnabled(true);
                           return;
                       }

                       if(!isValidNew())
                       {
                           common.warn(DeskTop,SPanel);
                           BOk.setEnabled(true);
                           return;
                       }

                       try
                       {
                            setOrderClass();

                            insertOrderDetails();
                       }
                       catch(Exception Ex)
                       {
                            Ex.printStackTrace();
                            bComflag = false;
                       }

                       try
                       {
                             if(bComflag)
                             {
                                  theConnection  . commit();
                                  System         . out.println("Commit");
                                  theConnection  . setAutoCommit(true);
                                  removeHelpFrame();
                             }
                             else
                             {
                                  theConnection  . rollback();
                                  System         . out.println("RollBack");
                                  theConnection  . setAutoCommit(true);
                                  BOk.setEnabled(true);
                             }
                       }catch(Exception ex)
                       {
                             ex.printStackTrace();
                       }
                  }
                  if(ae.getSource()==BCancel)
                  {
                       removeHelpFrame();
                  }     
            }
     }

     public void setOrderClass()
     {
          try
          {
               YearlyOrderClassGst yearlyOrderClassGst=null;
               theOrderVector = new Vector();

               String SSupCode = common.parseNull(TSupCode.getText());

               for(int i=0;i<RowData.length;i++)
               {
                    String SItemCode  = (String)RowData[i][0];

                    String SRate      = (String)RowData[i][3];
                    String SDiscPer   = (String)RowData[i][4];
                    String SCGSTPer   = (String)RowData[i][5];
                    String SSGSTPer   = (String)RowData[i][6];
                    String SIGSTPer   = (String)RowData[i][7];
                    String SCessPer   = (String)RowData[i][8];

                    String SPOrderNo   = "0";
                    String SPSource    = "0";
                    String SPBlockCode = "0";
                    String SPBlock     = "";
                    String SPOrderDate = "";
		    String SHsnCode    = "";

                    if(iMrsLink==1)
                    {
                         SPOrderNo   = (String)orderrecgst.VOrdNo.elementAt(i);
                         SPSource    = "1";
                         SPBlock     = (String)orderrecgst.VBlock.elementAt(i);
                         SPBlockCode = (String)orderrecgst.VBlockCode.elementAt(i);
                         SPOrderDate = (String)orderrecgst.VDate.elementAt(i);
                         SHsnCode    = (String)orderrecgst.VHCode.elementAt(i);
                    }
		    else
                    {
                         SHsnCode    = (String)VSelectedHsnCode.elementAt(i);
                    }

                    int iMIndex = getMrsIndexOf(SItemCode);
  
                    YearlyMrsClass yearlyMrsClass = (YearlyMrsClass)theMrsVector.elementAt(iMIndex);

                    for(int k=0;k<yearlyMrsClass.VMrsNo.size();k++)
                    {
                         String SMonthCode = (String)yearlyMrsClass.VMrsPlanMonth.elementAt(k);

                         String SMrsNo           = (String)yearlyMrsClass.VMrsNo.elementAt(k);
                         String SQty             = (String)yearlyMrsClass.VQty.elementAt(k);
                         String SUnitCode        = (String)yearlyMrsClass.VUnitCode.elementAt(k);
                         String SDeptCode        = (String)yearlyMrsClass.VDeptCode.elementAt(k);
                         String SGroupCode       = (String)yearlyMrsClass.VGroupCode.elementAt(k);
                         String SCatl            = (String)yearlyMrsClass.VCatl.elementAt(k);
                         String SDraw            = (String)yearlyMrsClass.VDraw.elementAt(k);
                         String SMake            = (String)yearlyMrsClass.VMake.elementAt(k);
                         String SDueDate         = (String)yearlyMrsClass.VDueDate.elementAt(k);
                         String SMrsSlNo         = (String)yearlyMrsClass.VSlNo.elementAt(k);
                         String SRemarks         = (String)yearlyMrsClass.VRemarks.elementAt(k);
                         String SMrsAuthUserCode = (String)yearlyMrsClass.VMrsAuthUserCode.elementAt(k);
                         String SEnquiryNo       = (String)yearlyMrsClass.VEnquiryNo.elementAt(k);

                         int iOIndex = getOrderIndexOf(SSupCode,SMonthCode);

                         if(iOIndex==-1)
                         {
                              yearlyOrderClassGst = new YearlyOrderClassGst(SSupCode,SMonthCode,iMillCode);
                              theOrderVector.addElement(yearlyOrderClassGst);
                              iOIndex = theOrderVector.size()-1;
                         }
                         yearlyOrderClassGst = (YearlyOrderClassGst)theOrderVector.elementAt(iOIndex);
                         yearlyOrderClassGst.setData(SItemCode,SMrsNo,SQty,SUnitCode,SDeptCode,SGroupCode,SCatl,SDraw,SMake,SDueDate,SMrsSlNo,SRemarks,SMrsAuthUserCode,SEnquiryNo,SRate,SDiscPer,SCGSTPer,SSGSTPer,SIGSTPer,SCessPer,SPOrderNo,SPSource,SPBlock,SPBlockCode,SPOrderDate,SHsnCode);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private int getMrsIndexOf(String SItemCode)
     {
          int iIndex =-1;

          for(int i=0; i<theMrsVector.size(); i++)
          {
               YearlyMrsClass yearlyMrsClass = (YearlyMrsClass)theMrsVector.elementAt(i);
               if(((yearlyMrsClass.SItemCode).equals(SItemCode)) && (yearlyMrsClass.iMillCode==iMillCode))
               {
                    iIndex=i;
                    break;
               }
          }
          return iIndex;
     }

     private int getOrderIndexOf(String SSupCode,String SMonthCode)
     {
          int iIndex =-1;

          for(int i=0; i<theOrderVector.size(); i++)
          {
               YearlyOrderClassGst yearlyOrderClassGst = (YearlyOrderClassGst)theOrderVector.elementAt(i);
               if(((yearlyOrderClassGst.SSupCode).equals(SSupCode)) && ((yearlyOrderClassGst.SMonthCode).equals(SMonthCode)) && (yearlyOrderClassGst.iMillCode==iMillCode))
               {
                    iIndex=i;
                    break;
               }
          }
          return iIndex;
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public void insertOrderDetails()
     {
          String QString1 = "Insert Into PurchaseOrder (OrderNo,OrderDate,OrderBlock,MrsNo,Sup_Code,Reference,Advance,PayTerms,PayDays,ToCode,ThroCode,Item_Code,Qty,Rate,DiscPer,Disc,CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,Cess,CessVal,Net,Plus,Less,Misc,Unit_Code,Dept_Code,Group_Code,DueDate,SlNo,EPCG,OrderType,Project_order,state,portcode,UserCode,ModiDate,MillCode,MRSSLNO,MrsAuthUserCode,OrderTypeCode,PrevOrderNo,OrderSourceTypeCode,PrevOrderBlock,TAXCLAIMABLE,Authentication,SeqId,HsnCode,GstRate,id) Values (";
          String QString2 = "Insert Into MatDesc (OrderNo,OrderDate,OrderBlock,Item_Code,Descr,Make,Draw,Catl,SlNo,id,MillCode) Values (";

          try
          {
               if(theConnection.getAutoCommit())
                         theConnection.setAutoCommit(false);

               Statement theStatement    = theConnection.createStatement();

               String SDate  = common.getServerPureDate();
               int iSeqId    = getSeqCode("Purchase_SeqId");

               for(int i=0;i<theOrderVector.size();i++)
               {
                    YearlyOrderClassGst yearlyOrderClassGst = (YearlyOrderClassGst)theOrderVector.elementAt(i);

                    String SSupCode   = yearlyOrderClassGst.SSupCode;
                    String SMonthCode = yearlyOrderClassGst.SMonthCode;

                    int iSlNo=0;

                    String SOrderNo = getInsertOrderNo();

                    for(int k=0;k<yearlyOrderClassGst.VItemCode.size();k++)
                    {
                         iSlNo++;

                         String SItemCode  = (String)yearlyOrderClassGst.VItemCode.elementAt(k);

                         double dQty       = common.toDouble((String)yearlyOrderClassGst.VQty.elementAt(k));
                         double dRate      = common.toDouble((String)yearlyOrderClassGst.VRate.elementAt(k));
                         double dDiscPer   = common.toDouble((String)yearlyOrderClassGst.VDiscPer.elementAt(k));
                         double dCGST      = common.toDouble((String)yearlyOrderClassGst.VCGSTPer.elementAt(k));
                         double dSGST      = common.toDouble((String)yearlyOrderClassGst.VSGSTPer.elementAt(k));
                         double dIGST      = common.toDouble((String)yearlyOrderClassGst.VIGSTPer.elementAt(k));
                         double dCess      = common.toDouble((String)yearlyOrderClassGst.VCessPer.elementAt(k));
		    	 String SGstRate   = common.getRound((dCGST+dSGST+dIGST),2);

		      	 double dGross   = dQty*dRate;
		      	 double dDisc    = dGross*dDiscPer/100;
		      	 double dBasic   = dGross - dDisc;
		      	 double dCGSTVal = dBasic*dCGST/100;
		      	 double dSGSTVal = dBasic*dSGST/100;
		      	 double dIGSTVal = dBasic*dIGST/100;
		      	 double dCessVal = dBasic*dCess/100;
		      	 double dNet     = dBasic+dCGSTVal+dSGSTVal+dIGSTVal+dCessVal;

                         String SAdd    = "0";
                         String SLess   = "0";
                         String SMisc   = "0";

                         String SUnitCode  = (String)yearlyOrderClassGst.VUnitCode.elementAt(k);
                         String SDeptCode  = (String)yearlyOrderClassGst.VDeptCode.elementAt(k);
                         String SGroupCode = (String)yearlyOrderClassGst.VGroupCode.elementAt(k);
                         String SBlockCode = "0";
                         String SDueDate   = (String)yearlyOrderClassGst.VDueDate.elementAt(k);
                         String SHsnCode   = (String)yearlyOrderClassGst.VHsnCode.elementAt(k);
     
                         int iTaxClaim     = 0;
     
     
                         String SMrsNo      = (String)yearlyOrderClassGst.VMrsNo.elementAt(k);
                         int iMRSSlNo       = common.toInt((String)yearlyOrderClassGst.VMrsSlNo.elementAt(k));
                         int iMRSUserCode   = common.toInt((String)yearlyOrderClassGst.VMrsAuthUserCode.elementAt(k));
                         String SEnqNo      = (String)yearlyOrderClassGst.VEnquiryNo.elementAt(k);
                         int iMRSType       = 2;
                         int iPrevOrderNo   = common.toInt((String)yearlyOrderClassGst.VPOrderNo.elementAt(k));
                         int iPrevSource    = common.toInt((String)yearlyOrderClassGst.VPSource.elementAt(k));
                         int iPrevBlockCode = common.toInt((String)yearlyOrderClassGst.VPBlockCode.elementAt(k));
                         String SPrevBlock  = (String)yearlyOrderClassGst.VPBlock.elementAt(k);
                         String SPOrderDate = (String)yearlyOrderClassGst.VPOrderDate.elementAt(k);
     
                         int iDeptCode  = common.toInt(SDeptCode);
                         int iGroupCode = common.toInt(SGroupCode);

                         String SReference = "";

                         if(iMrsLink==0)
                         {
                              SReference   = (TRefArea.getText()).toUpperCase();
                         }
                         else
                         {
                              SReference   = SPrevBlock+"-"+iPrevOrderNo+" DT."+common.parseDate(SPOrderDate);
                         }

                         String SDesc = common.parseNull((String)yearlyOrderClassGst.VRemarks.elementAt(k));
                         String SMake = common.parseNull((String)yearlyOrderClassGst.VMake.elementAt(k));
                         String SDraw = common.parseNull((String)yearlyOrderClassGst.VDraw.elementAt(k));
                         String SCatl = common.parseNull((String)yearlyOrderClassGst.VCatl.elementAt(k));

                         
                         String QS1 = QString1;
                         QS1 = QS1+"0"+SOrderNo+",";
                         QS1 = QS1+"'"+SDate+"',";
                         QS1 = QS1+""+SBlockCode+",";
                         QS1 = QS1+"0"+SMrsNo+",";
                         QS1 = QS1+"'"+SSupCode+"',";
                         QS1 = QS1+"'"+SReference+"',";    
                         QS1 = QS1+"0"+TAdvance.getText()+",";
                         QS1 = QS1+"'"+(TPayTerm.getText()).toUpperCase()+"',";
                         QS1 = QS1+"0"+TPayDays.getText()+",";
                         QS1 = QS1+"0"+VToCode.elementAt(JCTo.getSelectedIndex())+",";
                         QS1 = QS1+"0"+VThroCode.elementAt(JCThro.getSelectedIndex())+",";
                         QS1 = QS1+"'"+SItemCode+"',";
                         QS1 = QS1+"0"+common.getRound(dQty,3)+",";
                         QS1 = QS1+"0"+common.getRound(dRate,4)+",";
                         QS1 = QS1+"0"+common.getRound(dDiscPer,2)+",";
                         QS1 = QS1+"0"+common.getRound(dDisc,2)+",";
                         QS1 = QS1+"0"+common.getRound(dCGST,2)+",";
                         QS1 = QS1+"0"+common.getRound(dCGSTVal,2)+",";
                         QS1 = QS1+"0"+common.getRound(dSGST,2)+",";
                         QS1 = QS1+"0"+common.getRound(dSGSTVal,2)+",";
                         QS1 = QS1+"0"+common.getRound(dIGST,2)+",";
                         QS1 = QS1+"0"+common.getRound(dIGSTVal,2)+",";
                         QS1 = QS1+"0"+common.getRound(dCess,2)+",";
                         QS1 = QS1+"0"+common.getRound(dCessVal,2)+",";
                         QS1 = QS1+"0"+common.getRound(dNet,2)+",";
                         QS1 = QS1+"0"+SAdd+",";
                         QS1 = QS1+"0"+SLess+",";
                         QS1 = QS1+"0"+SMisc+",";
                         QS1 = QS1+"0"+SUnitCode+",";
                         QS1 = QS1+iDeptCode+",";
                         QS1 = QS1+iGroupCode+",";
                         QS1 = QS1+"'"+SDueDate+"',";
                         QS1 = QS1+iSlNo+",";
                         QS1 = QS1+JEPCG.getSelectedIndex()+",";
                         QS1 = QS1+JCOrderType.getSelectedIndex()+",";
                         QS1 = QS1+JCProject.getSelectedIndex()+",";
                         QS1 = QS1+JCState.getSelectedIndex()+",";
                         QS1 = QS1+VPortCode.elementAt(JCPort.getSelectedIndex())+",";
                         QS1 = QS1+iUserCode+",";
                         QS1 = QS1+"'"+common.getServerDateTime()+"',";
                         QS1 = QS1+iMillCode+",";
                         QS1 = QS1+iMRSSlNo+",";
                         QS1 = QS1+iMRSUserCode+",";
                         QS1 = QS1+iMRSType+",";
                         QS1 = QS1+iPrevOrderNo+",";
                         QS1 = QS1+iPrevSource+",";
                         QS1 = QS1+iPrevBlockCode+",";
                         QS1 = QS1+iTaxClaim+",";
                         QS1 = QS1+"1"+",";
                         QS1 = QS1+iSeqId+",";
                    	 QS1 = QS1+"'"+SHsnCode+"',";
                    	 QS1 = QS1+"0"+SGstRate+",";
                         QS1 = QS1+" purchaseorder_seq.nextval) " ;

                         theStatement.executeUpdate(QS1);

                         String QS2 = QString2;
                         QS2 = QS2+"0"+SOrderNo+",";
                         QS2 = QS2+"'"+SDate+"',";
                         QS2 = QS2+""+SBlockCode+",";
                         QS2 = QS2+"'"+SItemCode+"',";
                         QS2 = QS2+"'"+common.getNarration(SDesc)+"',";
                         QS2 = QS2+"'"+common.getNarration(SMake)+"',";
                         QS2 = QS2+"'"+SDraw+"',";
                         QS2 = QS2+"'"+SCatl+"',";
                         QS2 = QS2+iSlNo+",";
                         QS2 = QS2+" matdesc_seq.nextval , ";
                         QS2 = QS2+"0"+iMillCode+" )";
     
                         theStatement.executeUpdate(QS2);


                         String QS3 = "Update MRS set ";
                         QS3 = QS3+"OrderNo   ="+SOrderNo+",";
                         QS3 = QS3+"OrderBlock="+SBlockCode;
                         QS3 = QS3+" Where MrsNo="+SMrsNo;
                         QS3 = QS3+" And Item_Code='"+SItemCode+"'";
                         QS3 = QS3+" And YearlyPlanningMonth="+SMonthCode;

                         theStatement.executeUpdate(QS3);

                         if(common.toInt(SEnqNo)>0)
                         {
                              String QS4 = "Update Enquiry set CloseStatus=1 ";
                              QS4 = QS4+" Where EnqNo="+SEnqNo;
                              QS4 = QS4+" and Mrs_No="+SMrsNo;
                              QS4 = QS4+" and Item_Code='"+SItemCode+"'";

                              theStatement.executeUpdate(QS4);
                         }


                         String QS5 = " Update YearlyPlanning set OrderConverted=1 ";
                         QS5 = QS5+" Where Month="+SMonthCode;
                         QS5 = QS5+" And ItemCode='"+SItemCode+"'";

                         theStatement.executeUpdate(QS5);
                    }
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }

     private int getSeqCode(String SSeqName)
     {
           int iSeqValue=0;
           try
           {
                 Statement   theStatement      =     theConnection.createStatement();
                 ResultSet   theResult         =     theStatement.executeQuery("Select "+SSeqName+".nextval from dual");
                 while(theResult.next())
                 {
                       iSeqValue               =     theResult.getInt(1);
                 }
                 theResult.close();
                 theStatement.close();
           }
           catch(Exception e)
           {
                 bComflag = false;
                 e.printStackTrace();
           }
           return iSeqValue;
     }

     public boolean isValidNew()
     {
          boolean bFlag = true;

          try
          {
                FW = new FileWriter(common.getPrintPath()+"Error.prn");
                write("*-----------------------*\n");
                write("*    W A R N I N G      *\n");
                write("*-----------------------*\n\n");
          }
          catch(Exception ex){}

          // Checking for Supplier
          String str = common.parseNull(TSupCode.getText());
          if(str.length()==0)
          {
             write("Supplier Is Not Selected \n\n");
             bFlag = false;
          }

          String str1 = common.parseNull(TPayTerm.getText());
          if(str1.length()==0)
          {
             write("PaymentTerm Is Not Filled \n\n");
             bFlag = false;
          }

         //  Checking for Table Items 
         for(int i=0;i<RowData.length;i++)
         {
               double dQty     = common.toDouble(((String)RowData[i][2]).trim());
               double dRate    = common.toDouble(((String)RowData[i][3]));
               double dCGST    = common.toDouble(((String)RowData[i][5]));
               double dSGST    = common.toDouble(((String)RowData[i][6]));
               double dIGST    = common.toDouble(((String)RowData[i][7]));

               if(dQty <= 0)
               {
                 write("Quantity for Item No "+(i+1)+" is not fed \n");
                 bFlag = false;
               }
               if(dRate <= 0)
               {
                 write("Rate for Item No "+(i+1)+" is not fed \n");
                 bFlag = false;
               }

	       if(SSeleSupType.equals("1"))
	       {
		      if(dCGST<=0 && dSGST<=0 && dIGST<=0)
		      {
		           write("Tax Data for Item No "+(i+1)+" Not Updated \n");
		           bFlag = false;
		      }

		      if(dCGST<dSGST || dCGST>dSGST)
		      {
		           write("CGST and SGST Tax Data for Item No "+(i+1)+" Wrongly Updated \n");
		           bFlag = false;
		      }

		      if(dCGST>0 && dIGST>0)
		      {
		           write("CGST and IGST Tax Data for Item No "+(i+1)+" Wrongly Updated \n");
		           bFlag = false;
		      }

	              if(SSeleStateCode.equals("33"))
		      {
		           if(dIGST>0)
		           {
		                write("IGST Tax Data not applicable for this Party for Item No "+(i+1)+" \n");
		                bFlag = false;
		           }
		      }
		      else
		      {
		           if(dCGST>0 || dSGST>0)
		           {
		                write("CGST/SGST Tax Data not applicable for this Party for Item No "+(i+1)+" \n");
		                bFlag = false;
		           }
		      }
	       }
	       else
	       {
		      if(dCGST>0 || dSGST>0 || dIGST>0)
		      {
		           write("Tax Data for Item No "+(i+1)+" Wrongly Updated \n");
		           bFlag = false;
		      }
	       }
         }

         try
         {
            write("*-----------------------*\n");
            FW.close();
         }
         catch(Exception ex){}

         return bFlag;
     }
     public void write(String str)
     {
          try
          {
                   FW.write(str);
          }
          catch(Exception ex){}
     }

     public void getNextOrderNo()
     {
          String SOrderNo= "";
          String QS      = "";

          QS = " Select maxno From Config"+iMillCode+""+SYearCode+" where Id=1";

          try
          {

               Statement stat      = theConnection. createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         SOrderNo  = common.parseNull((String)result.getString(1));
                         result    . close();
                         stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
          SOrderNo  = String.valueOf(common.toInt(SOrderNo.trim())+1);
          TOrderNo  . setText(SOrderNo.trim());
     }

     public String getInsertOrderNo()
     {
          String SOrderNo= "";
          String QS      = "";

          QS = " Select (maxno+1) From Config"+iMillCode+""+SYearCode+" where Id=1 for update of MaxNo noWait";

          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);

               Statement stat      = theConnection   . createStatement();

               PreparedStatement thePrepare = theConnection.prepareStatement(" Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 1");

               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         SOrderNo  = common.parseNull((String)result.getString(1));
                         result    . close();

               thePrepare.setInt(1,common.toInt(SOrderNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getInsertOrderNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
          return SOrderNo;
     }

     public void getVTo()
     {
          VTo       = new Vector();
          VToCode   = new Vector();
          VThro     = new Vector();
          VThroCode = new Vector();
          VPortCode = new Vector();
          VPortName = new Vector();

          try
          {
               Statement theStatement    = theConnection.createStatement();
               ResultSet res1 = theStatement.executeQuery("Select ToName,ToCode From BookTo Order By ToName");
               while(res1.next())
               {
                     VTo.addElement(res1.getString(1));
                     VToCode.addElement(res1.getString(2));
               }
               res1.close();

               ResultSet res2 = theStatement.executeQuery("Select ThroName,ThroCode From BookThro Order By ThroName");
               while(res2.next())
               {
                     VThro.addElement(res2.getString(1));
                     VThroCode.addElement(res2.getString(2));
               }
               res2.close();

               ResultSet res3 = theStatement.executeQuery("Select PortName,PortCode From Port Order By PortName");
               while(res3.next())
               {
                     VPortName.addElement(res3.getString(1));
                     VPortCode.addElement(res3.getString(2));
               }
               res3.close();

               ResultSet res4 = theStatement.executeQuery("Select ToName From BookTo Where DivCode="+iMillCode);
               while(res4.next())
               {
                     SToName = common.parseNull(res4.getString(1));
               }
               res4.close();

               ResultSet res5 = theStatement.executeQuery("Select ThroName From BookThro Where DivCode="+iMillCode);
               while(res5.next())
               {
                     SThroName = common.parseNull(res5.getString(1));
               }
               res5.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

      public void setTabReport()
      {
            setRowData();
            try
            {
                  getContentPane().remove(tabreport); 
            }
            catch(Exception ex){}

            try
            {
                  tabreport = new TabReport(RowData,ColumnData,ColumnType);
                  getContentPane().add(tabreport,BorderLayout.CENTER);
                  setSelected(true);
                  DeskTop.repaint();
                  DeskTop.updateUI();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      public void setRowData()
      {
            if(iMrsLink==0)
            {
                 RowData = new Object[VSelectedCode.size()][ColumnData.length];
                 for(int i=0;i<VSelectedCode.size();i++)
                 {
                       RowData[i][0] = (String)VSelectedCode.elementAt(i);
                       RowData[i][1] = (String)VSelectedName.elementAt(i);
                       RowData[i][2] = (String)VSelectedQty.elementAt(i);
                       RowData[i][3] = "";
                       RowData[i][4] = "";
                       RowData[i][5] = "";
                       RowData[i][6] = "";
                       RowData[i][7] = "";
                       RowData[i][8] = "";
                 }
            }
            else
            {
                 RowData = new Object[orderrecgst.VCode.size()][ColumnData.length];
                 for(int i=0;i<orderrecgst.VCode.size();i++)
                 {
		       String SGstStatus = common.parseNull((String)orderrecgst.VGstStatus.elementAt(i));

                       RowData[i][0] = common.parseNull((String)orderrecgst.VCode.elementAt(i));
                       RowData[i][1] = common.parseNull((String)orderrecgst.VName.elementAt(i));
                       RowData[i][2] = common.parseNull((String)orderrecgst.VQty.elementAt(i));
                       RowData[i][3] = common.parseNull((String)orderrecgst.VRate.elementAt(i));
                       RowData[i][4] = common.parseNull((String)orderrecgst.VDiscPer.elementAt(i));

		       if(SGstStatus.equals("0"))
		       {
		             RowData[i][5] = common.parseNull((String)orderrecgst.VCGST.elementAt(i));
		             RowData[i][6] = common.parseNull((String)orderrecgst.VSGST.elementAt(i));
		             RowData[i][7] = common.parseNull((String)orderrecgst.VIGST.elementAt(i));
		             RowData[i][8] = common.parseNull((String)orderrecgst.VCess.elementAt(i));
		       }
		       else
		       {
		             RowData[i][5] = " ";
		             RowData[i][6] = " ";
		             RowData[i][7] = " ";
		             RowData[i][8] = " ";
		       }
                 }
            }
     }

     public void checkTaxData()
     {
	       iTaxCheck = 0;

	       if(TSupCode.getText().equals(""))
	       {
	    	    JOptionPane.showMessageDialog(null,"Supplier Not Selected","Information",JOptionPane.INFORMATION_MESSAGE);
		    return;
	       }

	       setSupData();

	       if(iStateCheck==0)
	       {
	    	    JOptionPane.showMessageDialog(null,"Supplier State Data Not Updated","Information",JOptionPane.INFORMATION_MESSAGE);
		    return;
	       }

	       if(iTypeCheck==0)
	       {
	    	    JOptionPane.showMessageDialog(null,"Supplier Gst Classification Not Updated","Information",JOptionPane.INFORMATION_MESSAGE);
		    return;
	       }

	       iTaxCheck = 1;
     }

    public void setSupData()
    {
	 iStateCheck = 0;
	 iTypeCheck  = 0;

	 SSeleSupCode   = "";
	 SSeleSupType   = "";
	 SSeleStateCode = "";

	 String Str = TSupData.getText();
         StringTokenizer ST = new StringTokenizer(Str,"`");
         while(ST.hasMoreElements())
	 {
              SSeleSupCode   = ST.nextToken();
              SSeleStateCode = ST.nextToken();
              SSeleSupType   = ST.nextToken();
	 }
	 System.out.println("SupData:"+SSeleSupCode+":"+SSeleStateCode+":"+SSeleSupType);

         if(SSeleSupType.equals("") || SSeleSupType.equals("0"))
	 {
	      iTypeCheck = 0;
	 }
	 else
	 {
	      iTypeCheck = 1;
	 }

         if(SSeleStateCode.equals("") || SSeleStateCode.equals("0"))
	 {
	      iStateCheck = 0;
	 }
	 else
	 {
	      iStateCheck = 1;
	 }
    }

      public String getSupData(String SSupCode)
      {
               String SData = "";

               String QString = " Select "+SSupTable+".Ac_Code,decode(PartyMaster.StateCode,0,'0',decode(PartyMaster.CountryCode,'61',nvl(State.GstStateCode,0),'999')) as GstStateCode, "+
			        " nvl(PartyMaster.GstPartyTypeCode,0) as GstPartyTypeCode From "+SSupTable+
				" Inner Join PartyMaster on "+SSupTable+".Ac_Code=PartyMaster.PartyCode and "+SSupTable+".Ac_Code='"+SSupCode+"'"+
				" Inner Join State on PartyMaster.StateCode=State.StateCode";
               try
               {
                    if(theConnection == null)
                    {
                         ORAConnection jdbc   = ORAConnection.getORAConnection();
                         theConnection        = jdbc.getConnection();
                    }

                    Statement theStatement    = theConnection.createStatement();

                    ResultSet res = theStatement.executeQuery(QString);
                    while(res.next())
                    {
                         SData = res.getString(1)+"`"+res.getString(2)+"`"+res.getString(3);
                    }
                    res            . close();
                    theStatement   . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
	       return SData;
      }


}
