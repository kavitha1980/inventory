package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;
import java.io.*;

public class YearlyOrderPooling
{

     // Parameter Counterpart
     JLayeredPane DeskTop;
     Vector VCode,VName;
     Vector VSelectedCode,VSelectedName,VSelectedQty;
     StatusPanel SPanel;


     // The Vectors are to be packed based on Supplier

     Vector VOrderRec;
     int iUserCode;
     int iMillCode;
     String SYearCode;
     String SItemTable,SSupTable;
     Vector theMrsVector;

     Common common = new Common();

     Connection theConnection = null;

     YearlyOrderPooling(JLayeredPane DeskTop,Vector VCode,Vector VName,Vector VSelectedCode,Vector VSelectedName,Vector VSelectedQty,StatusPanel SPanel,int iUserCode,int iMillCode,String SYearCode,String SItemTable,String SSupTable,Vector theMrsVector)
     {
          this.DeskTop           = DeskTop;
          this.VCode             = VCode;
          this.VName             = VName;
          this.VSelectedCode     = VSelectedCode;
          this.VSelectedName     = VSelectedName;
          this.VSelectedQty      = VSelectedQty;
          this.SPanel            = SPanel;
          this.iUserCode         = iUserCode;
          this.iMillCode         = iMillCode;
          this.SYearCode         = SYearCode;
          this.SItemTable        = SItemTable;
          this.SSupTable         = SSupTable;
          this.theMrsVector      = theMrsVector;


          VOrderRec           = new Vector();

          setData();
          setOrderPool();
          setMiscDetails();
          setOrderPlanFrame();
     }
     public void setOrderPlanFrame()
     {
          int ctr=0;
          try
          {
               for(int i=0;i<VOrderRec.size();i++)
               {
                    YearlyOrderRec orderrec = (YearlyOrderRec)VOrderRec.elementAt(i);
                    YearlyOrderPlanFrame yearlyorderplanframe = new YearlyOrderPlanFrame(DeskTop,VCode,VName,orderrec,SPanel,iUserCode,iMillCode,SYearCode,SItemTable,SSupTable,theMrsVector);
                    try
                    {
                         DeskTop.add(yearlyorderplanframe);
                         DeskTop.repaint();
                         yearlyorderplanframe.setSelected(true);
                         DeskTop.updateUI();
                         yearlyorderplanframe.show();
                         ctr++;
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
          }
          catch(Exception e)
          {
               System.out.println("Order Pooling"+e);
               e.printStackTrace();
          }
     }
     public void setMiscDetails()
     {
          try
          {
               for(int i=0;i<VOrderRec.size();i++)
               {
                    YearlyOrderRec orderrec = (YearlyOrderRec)VOrderRec.elementAt(i);
                    orderrec  .setMiscDetails(VSelectedName,VSelectedQty,SSupTable);
                    VOrderRec .setElementAt(orderrec,i);
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setOrderPool()
     {
          String SOSupCode="";
          YearlyOrderRec orderrec = new YearlyOrderRec();
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               ResultSet res   = theStatement.executeQuery("Select Id,Sup_Code,Pool.OrderNo,Pool.OrderBlock,OrderDate,Item_Code,Rate,DiscPer,CenvatPer,TaxPer,SurPer,ToCode,ThroCode,Descr,Make,Catl,Draw,PayTerms,PayDays,BlockName From Pool Inner Join OrdBlock on OrdBlock.block=Pool.OrderBlock Order By Sup_Code");

               while(res.next())
               {
                    int iOrdNo      = res.getInt(3);
                    if (iOrdNo==0)
                         continue;

                    int iId          = res.getInt(1);
                    String SSupCode  = common.parseNull(res.getString(2));
                    String SToCode   = common.parseNull(res.getString(12));
                    String SThroCode = common.parseNull(res.getString(13));
                    String SPayTerm  = common.parseNull(res.getString(18));
                    String SPayDays  = common.parseNull(res.getString(19));

                    if(!SOSupCode.equals(SSupCode))
                    {
                         SOSupCode = SSupCode;
                         VOrderRec.addElement(orderrec);
                         orderrec = new YearlyOrderRec();
                         orderrec.SSupCode  = SSupCode;
                         orderrec.SToCode   = SToCode;
                         orderrec.SThroCode = SThroCode;
                         orderrec.SPayTerm  = SPayTerm;
                         orderrec.SPayDays  = SPayDays;
                    }
                    orderrec.VId       .addElement(String.valueOf(iId));
                    orderrec.VOrdNo    .addElement(String.valueOf(iOrdNo));
                    orderrec.VBlock    .addElement(common.parseNull(res.getString(20)));
                    orderrec.VBlockCode.addElement(common.parseNull(res.getString(4)));
                    orderrec.VDate     .addElement(common.parseNull(res.getString(5)));
                    String SItemCode   = common.parseNull(res.getString(6));
                    orderrec.VCode     .addElement(common.parseNull(SItemCode));
                    orderrec.VRate     .addElement(common.parseNull(res.getString(7)));
                    orderrec.VDiscPer  .addElement(common.parseNull(res.getString(8)));
                    orderrec.VCenVatPer.addElement(common.parseNull(res.getString(9)));
                    orderrec.VTaxPer   .addElement(common.parseNull(res.getString(10)));
                    orderrec.VSurPer   .addElement(common.parseNull(res.getString(11)));
               }
               res.close();
               theStatement.close();

               VOrderRec.addElement(orderrec);

               VOrderRec.removeElementAt(0);

          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     public void setData()
     {

          String QS1="",QS2="";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();
               theStatement.executeUpdate("Delete From Pool");
               for(int i=0;i<VSelectedCode.size();i++)
               {
                     QS1 =" Select PYOrder.OrderNo,PYOrder.OrderBlock,PYOrder.OrderDate,PYOrder.Item_Code, "+
                          " PYOrder.Sup_Code,PYOrder.Rate,PYOrder.DiscPer, "+
                          " decode(getcenvatper(PYOrder.CenVatPer),null,PYOrder.CenVatPer,getcenvatper(PYOrder.CenVatPer)) as CenvatPer,"+
                          " decode(gettaxper(PYOrder.TaxPer),null,PYOrder.TaxPer,gettaxper(PYOrder.TaxPer)) as TaxPer,"+
                          " decode(getsurchargeper(PYOrder.SurPer),null,PYOrder.SurPer,getsurchargeper(PYOrder.SurPer)) as SurPer,"+
                          " "+String.valueOf(i)+" as ItemID,PYOrder.ToCode,PYOrder.ThroCode,PYMatDesc.Descr, PYMatDesc.Make, PYMatDesc.Catl, PYMatDesc.Draw, PYOrder.PayTerms, PYOrder.PayDays "+
                          " From PYOrder Left JOIN PYMatDesc ON (PYOrder.OrderNo = PYMatDesc.OrderNo) AND (PYOrder.OrderBlock = PYMatDesc.OrderBlock) AND (PYOrder.Item_Code = PYMatDesc.Item_Code) AND (PYOrder.SlNo = PYMatDesc.SlNo)  "+
                          " Where PYOrder.Item_Code='"+(String)VSelectedCode.elementAt(i)+"'"+
                          " Union All "+
                          " Select PurchaseOrder.OrderNo,PurchaseOrder.OrderBlock,PurchaseOrder.OrderDate,PurchaseOrder.Item_Code, "+
                          " PurchaseOrder.Sup_Code,PurchaseOrder.Rate,PurchaseOrder.DiscPer, "+
                          " decode(getcenvatper(PurchaseOrder.CenVatPer),null,PurchaseOrder.CenVatPer,getcenvatper(PurchaseOrder.CenVatPer)) as CenvatPer,"+
                          " decode(gettaxper(PurchaseOrder.TaxPer),null,PurchaseOrder.TaxPer,gettaxper(PurchaseOrder.TaxPer)) as TaxPer,"+
                          " decode(getsurchargeper(PurchaseOrder.SurPer),null,PurchaseOrder.SurPer,getsurchargeper(PurchaseOrder.SurPer)) as SurPer,"+
                          " "+String.valueOf(i)+" as ItemID,PurchaseOrder.ToCode,PurchaseOrder.ThroCode,MatDesc.Descr, MatDesc.Make, MatDesc.Catl, MatDesc.Draw, PurchaseOrder.PayTerms, PurchaseOrder.PayDays "+
                          " From PurchaseOrder Left JOIN MatDesc ON (PurchaseOrder.OrderNo = MatDesc.OrderNo) AND (PurchaseOrder.OrderBlock = MatDesc.OrderBlock) AND (PurchaseOrder.Item_Code = MatDesc.Item_Code) AND (PurchaseOrder.SlNo = MatDesc.SlNo)  "+
                          " Where PurchaseOrder.Item_Code='"+(String)VSelectedCode.elementAt(i)+"' Order By OrderDate";

                    ResultSet result = theStatement.executeQuery(QS1);

                    String SOrdNo   = "";
                    String SBlock   = "";
                    String SDate    = "";
                    String SCode    = "";
                    String SSupCode = "";
                    String SRate    = "";
                    String SDiscPer = "";
                    String SVatPer  = "";
                    String STaxPer  = "";
                    String SSurPer  = "";
                    String SId      = "";
                    String SToCode  = "";
                    String SThroCode= "";
                    String SDescr   = "";
                    String SMake    = "";
                    String SCatl    = "";
                    String SDraw    = "";
                    String STerms   = "";
                    String SDays    = "";

                    while(result.next())
                    {
                            SOrdNo    = common.parseNull(result.getString(1));
                            SBlock    = common.parseNull(result.getString(2));
                            SDate     = common.parseNull(result.getString(3));
                            SCode     = common.parseNull(result.getString(4));
                            SSupCode  = common.parseNull(result.getString(5));
                            SRate     = common.parseNull(result.getString(6));
                            SDiscPer  = common.parseNull(result.getString(7));
                            SVatPer   = common.parseNull(result.getString(8));
                            STaxPer   = common.parseNull(result.getString(9));
                            SSurPer   = common.parseNull(result.getString(10));
                            SId       = common.parseNull(result.getString(11));
                            SToCode   = common.parseNull(result.getString(12));
                            SThroCode = common.parseNull(result.getString(13));
                            SDescr    = common.parseNull(result.getString(14));
                            SMake     = common.parseNull(result.getString(15));
                            SCatl     = common.parseNull(result.getString(16));
                            SDraw     = common.parseNull(result.getString(17));
                            STerms    = common.parseNull(result.getString(18));
                            SDays     = common.parseNull(result.getString(19));
                    }
                    result.close();

                    String QS3 = "Insert Into Pool ("+
                                 "OrderNo,OrderBlock,OrderDate,Item_Code,"+
                                 "Sup_Code,Rate,DiscPer,CenVatPer,TaxPer,"+
                                 "SurPer,Id,ToCode,ThroCode,Descr,"+
                                 "Make,Catl,Draw,PayTerms,PayDays) Values ("+
                                 "0"+SOrdNo+","+
                                 "0"+SBlock+","+
                                 "'"+SDate+"'"+","+
                                 "'"+SCode+"'"+","+
                                 "'"+SSupCode+"'"+","+
                                 "0"+SRate   +","+
                                 "0"+SDiscPer+","+ 
                                 "0"+SVatPer+","+  
                                 "0"+STaxPer +"," +
                                 "0"+SSurPer +"," +
                                 "0"+SId     +","+ 
                                 "0"+SToCode +","+ 
                                 "0"+SThroCode+","+ 
                                  "'"+SDescr+"'"+","+
                                  "'"+SMake+"'"+","+     
                                  "'"+SCatl+"'"+","+     
                                  "'"+SDraw+"'"+","+     
                                  "'"+STerms+"',"+
                                  "0"+SDays+")";

                        theStatement.execute(QS3);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
}
