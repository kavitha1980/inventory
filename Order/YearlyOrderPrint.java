package Order;

import java.io.*;
import java.util.*;
import java.sql.*;
import util.*;


public class YearlyOrderPrint
{

     Vector         VCode,VName,VUOM,VQty,VRate;
     Vector         VDiscPer,VCenVatPer,VTaxPer,VSurPer;
     Vector         VBasic,VDisc,VCenVat,VTax,VSur,VNet;
     Vector         VDesc,VMake,VCatl,VDraw,VBlock;
     Vector         VPColour,VPSet,VPSize,VPSide;
     Vector         VSlipFromNo,VSlipToNo,VBookFromNo,VBookToNo,VOrderNos;
     Common         common    = new Common();
     FileWriter     FW;

     String         SOrdNo    = "",SOrdDate  = "",SSupCode  = "",SSupName  = "";
     String         SAddr1    = "",SAddr2    = "",SAddr3    = "";
     String         SEMail="",SFaxNo="";
     String         SToName   = "",SThroName = "";
     String         SPayTerm       = "";
     String         SReference     = "";
     String         SDueDate       = "";
     String         SOthers        = "";
     String         SMRSNo         = "0";
     String         SMRSDate       = "";
     String         SPort          = "";
     
     double         dOthers   = 0;
     double         dTDisc    = 0,dTCenVat   = 0,dTTax      = 0,dTSur = 0,dTNet = 0;

     int            Lctr      = 100;
     int            Pctr      = 0;
     int            iEPCG          = 0;
     int            iOrderType     = 0;
     int            iProject       = 0;
     int            iState         = 0;
     int            iAmend         = 0;

     String         SSupplierEMail,SSupplierMobile,SStDate,SEnDate;
     String         SContact;
     Vector         VOrderGroup,VItem;
     String SLine = "";
     String SOrderDate="";
     String SSeqId="0";
     YearlyOrderPrint(FileWriter FW,String SSupCode,String SSupName)
     {
          this.FW         = FW;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;

          getAddr();
          setDataIntoVector();
          setOrderHead();
          setOrderBody();
          setOrderFoot(0);
     }
     YearlyOrderPrint(FileWriter FW,String SSupCode,String SSupName,String SStDate,String SEnDate)
     {
          this.FW         = FW;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.SStDate    = SStDate;
          this.SEnDate    = SEnDate;

          getAddr();
          setDataIntoVector();
          setOrderHead();
          setOrderBody();
          setOrderFoot(0);
     }
     YearlyOrderPrint(FileWriter FW,String SSupCode,String SSupName,String SStDate,String SEnDate,String SSeqId)
     {
          this.FW         = FW;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.SStDate    = SStDate;
          this.SEnDate    = SEnDate;
          this.SSeqId      = SSeqId;

          getAddr();
          setDataIntoVector();
          setOrderHead();
          setOrderBody();
          setOrderFoot(0);
     }
     
     public void setOrderHead()
     {
          if(Lctr < 37)
               return;

          if(Pctr > 0)
               setOrderFoot(1);
          
          String SState = "";
          if (iState == 1)
          {
               SState = " Intra State";
          }
          else if (iState == 2)
          {
               SState = " Inter State";
          }
          String Strl06 = "";
          if (iAmend == 1)
          {     
               if (iEPCG == 1 && iOrderType == 0)

                    Strl06 = "| E-Mail: purchase@amarjothi.net,mill@amarjothi.net              |"+common.Pad(SState,12)+"|   EPCG  | AM PURCHASE ORDER                             |";
               else if (iEPCG == 1 && iOrderType == 1)
                    Strl06 = "| E-Mail: purchase@amarjothi.net,mill@amarjothi.net              |   IMPORT |    EPCG   | AM PURCHASE ORDER"+common.Pad(SPort,10)+"                   |";
               else if (iEPCG == 0 && iOrderType == 1)
                    Strl06 = "| E-Mail: purchase@amarjothi.net,mill@amarjothi.net              |   IMPORT |           | AM PURCHASE ORDER"+common.Pad(SPort,10)+"                   |";
               else  
                    Strl06 = "| E-Mail: purchase@amarjothi.net,mill@amarjothi.net              |"+common.Pad(SState,12)+"|           |AM PURCHASE ORDER                            |";
          }
          else
          {
               if (iEPCG == 1 && iOrderType == 0)
                    Strl06 = "| E-Mail: purchase@amarjothi.net,mill@amarjothi.net              |"+common.Pad(SState,12)+"|   EPCG  |    PURCHASE ORDER                             |";
               else if (iEPCG == 1 && iOrderType == 1)
                    Strl06 = "| E-Mail: purchase@amarjothi.net,mill@amarjothi.net              |   IMPORT |    EPCG   |    PURCHASE ORDER"+common.Pad(SPort,10)+"                   |";
               else if (iEPCG == 0 && iOrderType == 1)
                    Strl06 = "| E-Mail:purchase@amarjothi.net,mill@amarjothi.net              |   IMPORT |           |    PURCHASE ORDER"+common.Pad(SPort,10)+"                   |";
               else  
                    Strl06 = "| E-Mail:purchase@amarjothi.net,mill@amarjothi.net              |"+common.Pad(SState,12)+"|           |   PURCHASE ORDER                           |";
          }


          Pctr++;

          String STitle=  "      E   YEARLY ORDER WITH DELIVERY SCHEDULEF                                                                                    ";
          String Strl01 = "|-------------------------------------------------------------------------------------------------------------------------------------";
          String Strl02 = "|         "+""+"AMARJOTHI SPINNING MILLS LIMITED"+""+"                      |                            ECC No          : AAFCA 7082C XM 001     |";
          String Strl03 = "|                 PUDUSURIPALAYAM                               | PLA No      : 59/93        Range           : Gobichettipalayam.     |";
          String Strl04 = "|               NAMBIYUR  -  638 458                            | Division    : Erode.       Commissionerate : Salem.                 |";
          String Strl05 = "|                                                               |---------------------------------------------------------------------|";
          String Strl07 = "| Phones        : 04285 - 267201,267301, Fax - 04285-267565     |---------------------------------------------------------------------|";
          String Strl08 = "| Our C.S.T. No.: 440691 dt. 24.09.1990                         | Order No:  "+common.Pad(getOrderNo(),27)+"Order Dt"+common.Pad(common.parseDate(SOrderDate),10)+"| Page | "+common.Pad(""+Pctr,3)+"|";// "Ref.Delivery Schedule"
          String Strl09 = "| TIN No.       : 33632960864                                   |---------------------------------------------------------------------|";
          String Strl10 = "|---------------------------------------------------------------| Book To   |"+common.Pad(SToName,57)+"|";
          String Strl11 = "|To,                                                            |---------------------------------------------------------------------|";
          String Strl12 = "|"+""+common.Pad("M/s "+SSupName,63)+""+                             "| Book Thro'|"+common.Pad(SThroName,55)+                            "  |";
          String Strl13 = "|"+common.Pad(SAddr1,63)+                                      "|---------------------------------------------------------------------|";
          String Strl14 = "|"+common.Pad(SAddr2,63)+                                      "| Reference |"+common.Pad(SReference,55)+                            "  |";
          String Strl15 = "|"+common.Pad(SAddr3,63)+                                      "|           |                                                         |";
          String Strl15A= "|"+common.Pad("Fax No: "+SFaxNo,25)+common.Pad("EMail: "+SSupplierEMail,38)+"|           |                                                         |";
          String Strl15B= "|"+common.Pad("Mobile: "+SSupplierMobile,13)+common.Space(10)+common.Pad("Contact Person : "+SContact,40)+"|           |                                                         |";
          String Strl16 = "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl17 = "| Mat Code  |Block| Description                             | UOM | Quantity |     Rate | Discount |   CenVat |      Tax |        Net |";
          String Strl17A= "|           |     | Make - Catalogue - Drawing Number       |     |          |       Rs |       Rs |       Rs |       Rs |         Rs |";
          String Strl18 = "|-------------------------------------------------------------------------------------------------------------------------------------|";
     
          try
          {

               FW.write(STitle+"\n");       
               FW.write("g"+Strl01+"\n");
               FW.write(Strl02+"\n");       
               FW.write(Strl03+"\n");       
               FW.write(Strl04+"\n");       
               FW.write(Strl05+"\n");       
               FW.write(Strl06+"\n");       
               FW.write(Strl07+"\n");       
               FW.write(Strl08+"\n");       
               FW.write(Strl09+"\n");       
               FW.write(Strl10+"\n");       
               FW.write(Strl11+"\n");       
               FW.write(Strl12+"\n");       
               FW.write(Strl13+"\n");       
               FW.write(Strl14+"\n");       
               FW.write(Strl15+"\n");
               FW.write(Strl15A+"\n");       
               FW.write(Strl15B+"\n");       
               FW.write(Strl16+"\n");       
               FW.write(Strl17+"\n");
               FW.write(Strl17A+"\n");       
               FW.write(Strl18+"\n");
               Lctr = 22;
          }
          catch(Exception ex){}
     }
     
     public void setOrderBody()
     {
          String Strl="";
          for(int i=0;i<VOrderGroup.size();i++)
          {
               setOrderHead();
               OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);

               ArrayList theItemList  = (ArrayList)ordergroup.getItems();

               for(int k=0; k<theItemList.size(); k++)
               {
                    Item item           = (Item)theItemList.get(k);

                    String SCode        = item.SItemCode;
                    String SUOM         = item.getUom();
                    String SName        = item.SItemName;
                    
                    String SBkName      = item.getBlockName();
     
                    String SQty         = item.getTotalQty();
                    String SRate        = item.getRate();
                    String SDisc        = item.getTotalDisc();
                    String SDiscPer     = item.getDiscPer()+"%";
                    String SCenVat      = item.getTotalCenVat();
                    String SCenVatPer   = item.getCenvatPer()+"%";
                    String STax         = item.getTotalTax();
                    String STaxPer      = item.getTaxPer()+"%";
                    String SSur         = item.getTotalSur();
                    String SSurPer      = item.getSurPer()+"%";
                    String SNet         = item.getTotalNet();
                    String SDesc        = item.getDesc();
                    String SMake        = item.getMake();
                    String SCatl        = item.getCatl();
                    String SDraw        = item.getDraw();
     
                    String SPColour     = "";
                    String SPSets       = "";
                    String SPSize       = "";
                    String SPSide       = "";
     
                    String SSlipFromNo  = "";
                    String SSlipToNo    = "";
                    String SBookFromNo  = item.getThroCode();
                    String SBookToNo    = item.getToCode();

                    SDesc          = SDesc        . trim();
                    SMake          = SMake        . trim();
                    SPColour       = SPColour     . trim();
                    SPSets         = SPSets       . trim();
                    SPSize         = SPSize       . trim();
                    SPSide         = SPSide       . trim();
                    SSlipFromNo    = SSlipFromNo  . trim();
                    SSlipToNo      = SSlipToNo    . trim();
                    SBookFromNo    = SBookFromNo  . trim();
                    SBookToNo      = SBookToNo    . trim();

                    dTDisc   = dTDisc   + common.toDouble(SDisc);
                    dTCenVat = dTCenVat + common.toDouble(SCenVat);
                    dTTax    = dTTax    + common.toDouble(STax);
                    dTSur    = dTSur    + common.toDouble(SSur);
                    dTNet    = dTNet    + common.toDouble(SNet);
               
                    try
                    {
                         Strl =    "| "+common.Pad(SCode,9)+" | "+common.Rad(SBkName,3)+" | "+""+common.Pad(SName,39)+""+" | "+
                                   common.Pad(SUOM,3)+" | "+common.Rad(SQty,8)+" | "+
                                   common.Rad(SRate,8)+" | "+common.Rad(SDisc,8)+" | "+
                                   common.Rad(SCenVat,8)+" | "+common.Rad(STax,8)+" | "+
                                   common.Rad(SNet,10)+" |";
                         FW.write(Strl+"\n");
                         Lctr = Lctr+1;
                    
                         Vector vect = common.getLines(SDesc+"` ");
                         
                         for(int j=0;j<vect.size();j++)
                         {
                              if(j==0)
                              {
                                   Strl =    "| "+common.Pad("",9)+" | "+common.Space(3)+" | "+common.Pad((String)vect.elementAt(j),39)+" | "+
                                             common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                             common.Space(8)+" | "+common.Rad(SDiscPer,8)+" | "+
                                             common.Rad(SCenVatPer,8)+" | "+common.Rad(STaxPer,8)+" | "+
                                             common.Space(10)+" |";
                              }
                              else
                              {
                                   Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad((String)vect.elementAt(j),39)+" | "+
                                             common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                             common.Space(8)+" | "+common.Rad("",8)+" | "+
                                             common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                             common.Space(10)+" |";
                              }
                              FW.write(Strl+"\n");
                              Lctr = Lctr+1;
                         }
                         
                         if(SMake.length() > 0)
                         {
                              Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad(SMake,39)+" | "+
                                        common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                        common.Space(8)+" | "+common.Rad("",8)+" | "+
                                        common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                        common.Space(10)+" |";
                              FW.write(Strl+"\n");
                              Lctr++;
                         }                         
                         
                         if((SCatl.trim()).length() > 0)
                         {
                              Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad("Catl   : "+SCatl,39)+" | "+
                                        common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                        common.Space(8)+" | "+common.Rad("",8)+" | "+
                                        common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                        common.Space(10)+" |";
                              FW.write(Strl+"\n");
                              Lctr++;
                         }                         
                         
                         if((SDraw.trim()).length() > 0)
                         {
                              Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad("Drawing: "+SDraw,39)+" | "+
                                        common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                        common.Space(8)+" | "+common.Rad("",8)+" | "+
                                        common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                        common.Space(10)+" |";
                              FW.write(Strl+"\n");
                              Lctr++;
                         }
     
                         String    SSPCoSets = "Colour : "+common.Pad(SPColour,12);
                                   SSPCoSets+= "Sets: "+SPSets;
               
                         String    SSPSis    = "Size   : "+common.Pad(SPSize,12);
                                   SSPSis   += "Side: "+SPSide;
     
                         String    SSPBooks  = "Book No: "+SBookFromNo+" To "+SBookToNo;
                         String    SSPSlip   = "Slip No: "+SSlipFromNo+" To "+SSlipToNo;
     
                         if(isStationary(SCode))
                         {
                              if((SPColour.trim()).length() > 0 || ((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0))
                              {
                                   Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad(SSPCoSets,39)+" | "+
                                             common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                             common.Space(8)+" | "+common.Rad("",8)+" | "+
                                             common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                             common.Space(10)+" |";
                                   FW.write(Strl+"\n");
                                   Lctr++;
                              }
          
                              if(((SPColour.trim()).length() > 0) || ((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0))
                              {
                                   Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad(SSPSis,39)+" | "+
                                             common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                             common.Space(8)+" | "+common.Rad("",8)+" | "+
                                             common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                             common.Space(10)+" |";
                                   FW.write(Strl+"\n");
                                   Lctr++;
                              }
     
                              if((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0)
                              {
                                   Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad(SSPSlip,39)+" | "+
                                             common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                             common.Space(8)+" | "+common.Rad("",8)+" | "+
                                             common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                             common.Space(10)+" |";
                                   FW.write(Strl+"\n");
                                   Lctr++;
                              }
     
                              if((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0)
                              {
                                   Strl =    "| "+common.Pad("",9)+" | "+common.Rad("",3)+" | "+common.Pad(SSPBooks,39)+" | "+
                                             common.Pad("",3)+" | "+common.Rad("",8)+" | "+
                                             common.Space(8)+" | "+common.Rad("",8)+" | "+
                                             common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                             common.Space(10)+" |";
                                   FW.write(Strl+"\n");
                                   Lctr++;
                              }
                         }
     
                         Strl =    "| "+common.Space(9)+" | "+common.Space(3)+" | "+common.Space(39)+" | "+
                                   common.Space(3)+" | "+common.Space(8)+" | "+
                                   common.Space(8)+" | "+common.Space(8)+" | "+
                                   common.Space(8)+" | "+common.Space(8)+" | "+
                                   common.Space(10)+" |";
     
                         FW   . write(Strl+"\n");
                         Lctr = Lctr+1;
                    }
                    catch(Exception ex){}
               }
          }

     }
     
     public void setOrderFoot(int iSig)
     {
          String SDisc   = common.getRound(dTDisc,2);
          String SCenVat = common.getRound(dTCenVat,2);
          String STax    = common.getRound(dTTax,2);
          String SSur    = common.getRound(dTSur,2);
          String SNet    = common.getRound(dTNet,2);
          String SOthers = common.getRound(dOthers,2);
          String SGrand  = common.getRound(dTNet+dOthers,2);
          String Strl1 = "|-------------------------------------------------------------------------------------------------------------------------------------|";
          
          String Strl2 = "| "+common.Space(9)+" | "+common.Space(3)+" | "+common.Pad("T O T A L ",39)+" | "+
          common.Space(3)+" | "+common.Space(8)+" | "+
          common.Space(8)+" | "+common.Rad(SDisc,8)+" | "+
          common.Rad(SCenVat,8)+" | "+common.Rad(STax,8)+" | "+
          common.Rad(SNet,10)+" |";
          String Strl3 = "|-------------------------------------------------------------------------------------------------------------------------------------|";
          
          String Strl3a= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl3b= "| Date of Delivery | MRS No | MRS Date   | Terms of Payments                    | Freight & Others |           Order Value            |";
          String Strl3c= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl3d= "| "+common.Pad("Ref.Deliv.Schdle",16)+" | "+
          common.Pad(SMRSNo,6)+" | "+
          common.Pad(SMRSDate,10)+" | "+
          common.Pad(SPayTerm,36)+" | "+
          common.Rad(SOthers,16)+" | "+
          ""+common.Rad(SGrand+" ",33)+"|";
          String Strl3e= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl4 = "| Note : 1. Please mention our order No. and material code in all correspondence with us.                                             |";
          String Strl4a= "|        2. Please mention our TIN Number in your invoices.                                                                           |";
          String Strl5 = "|        3. Please send  invoices in triplicate. Original invoice to be sent direct to accounts manager.                              |";
          String Strl6 = "|        4. Please send copy of your invoice along with consignment.                                                                  |";
          String Strl7 = "|        5. We reserve the right to increase / decrease the order qty. / cancel the order without assigning any reason.               |";
          String Strl8 = "|        6. Any legal dispute shall be within the jurisdiction of Tirupur.                                                            |";
          String Strl8a= "";
          String Strl8b= "";
          String Strl8c= "";
          String Strl8d= "";
          if (iEPCG == 1)
          {
               Strl8a = "|        7. These materials should be supplied against EPCG Scheme  for which please send us your                                     |";
               Strl8b = "|           Proforma Invoice along with the catalogue immediately so that we can open                                                 |";
               Strl8c = "|           the EPCG Licence.                                                                                                         |";
               Strl8d = "|        8. Please quote the EPCG License No in your all Bills.                                                                       |";
          }
          String Strl9 = "|                                                                                               For AMARJOTHI SPINNING MILLS LTD      |";
          String Strl10= "|                                                                                                                                     |";
          String Strl11= "|                                                                                                                                     |";
          String Strl12= "| Prepared By                        Checked By                        I.A.                         Authorised Signatory              |";
          String Strl13= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl14= "| Special Instruction :-                                                                                                              |";
          String Strl15= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl16= "| Regd. Office - 'AMARJOTHI HOUSE' 157, Kumaran Road, Tirupur - 638 601. Phone : 0421- 2201980 to 2201984                             |";
          String Strl16a="| E-Mail       - amarin@giasmd01.vsnl.net.in, amarjothi@vsnl.com                                                                      |";
          String Strl17= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl18= "|                           I N D I A 'S   F I N E S T  M E L A N G E  Y A R N  P R O D U C E R S                                     |";
          String Strl19= " -------------------------------------------------------------------------------------------------------------------------------------";
          String Strl20="";
          try
          {
               FW.write( Strl1+"\n");       
               FW.write( Strl2+"\n");       
               FW.write( Strl3+"\n");
               if(iSig==0)
               {
                    FW.write( Strl3a+"\n");
                    FW.write( Strl3b+"\n");
                    FW.write( Strl3c+"\n");
                    FW.write( Strl3d+"\n");
                    FW.write( Strl3e+"\n");
               } 
               FW.write( Strl4+"\n");
               FW.write( Strl4a+"\n");       
               FW.write( Strl5+"\n");       
               FW.write( Strl6+"\n");       
               FW.write( Strl7+"\n");
               FW.write( Strl8+"\n");
               if (iEPCG == 1)
               {
                    FW.write( Strl8a+"\n");
                    FW.write( Strl8b+"\n");
                    FW.write( Strl8c+"\n");
                    FW.write( Strl8d+"\n");
               }
               FW.write( Strl9+"\n");       
               FW.write( Strl10+"\n");       
               FW.write( Strl11+"\n");       
               FW.write( Strl12+"\n");       
               FW.write( Strl13+"\n");       
               FW.write( Strl14+"\n");       
               FW.write( Strl15+"\n");       
               FW.write( Strl16+"\n");
               FW.write( Strl16a+"\n");       
               FW.write( Strl17+"\n");       
               FW.write( Strl18+"\n");
               FW.write( Strl19+"\n");       
               FW.write( Strl20+"\n");

               String str2 ="";
               String SStatusId="Pur Order : "+SOrdNo+"";
               String SCollId="";
               str2 += " \n ";
//               str2 += " You have received this email because you subscribed to email alert notifications from Amarjothi spinning mills ltd. If you do not wish to receive email alerts from Amarjothi spinning mills ltd,";
//               str2 += " <a href='http://www.melangeonline.net:8070/StoreEnquiry/MailUnSubscribe.jsp?EMail="+SEMail+"&SupCode="+SSupCode+"&StatusId="+SStatusId+"&CollId="+SCollId+" '><font color=blue>Unsubscribe</font></a> ";

               FW.write(str2);
               Lctr = Lctr+1;


               setDeliverySchedules(0);
               setDeliveryQty();


               if(iSig==0)
               Strl20= "<End Of Report>";
               else
               Strl20= "(Continued on Next Page)";

               FW.write(Strl20+"@\n");


          }
          catch(Exception ex){}
     }
     private void setDeliverySchedules(int iFlag)
     {
          try
          {

               SLine = "";
               String SLine2 = "";
               String SLine3 = "";
               String SEmptySpace1="";
               String SEmptySpace2="";
                         
               SLine = " "+common.Replicate("-",12)+"-";
               SLine3 = "|"+common.Pad("DueDate",12)+"|";
               SEmptySpace1="|"+common.Space(12)+"|";
               SLine2 = "|"+common.Pad("OrderNo",12)+"|";
               SEmptySpace2="|"+common.Pad("ItemCode",12)+"|";

               String STitle=  " E  DELIVERY SCHEDULEF  ";

               for(int i=0; i<VOrderGroup.size(); i++)
               {
                    OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);
     
                    for(int k=0; k<ordergroup.orderNoList.size(); k++)
                    {
                         SLine = SLine+  common.Replicate("-",12)+"-";
                         SLine2 = SLine2+  common.Cad((String)ordergroup.orderNoList.get(k),12)+"|";
                         SLine3 = SLine3+  common.Cad((String)ordergroup.dueDateList.get(k),12)+"|";
                         SEmptySpace1=SEmptySpace1+common.Space(12)+"|";
                         SEmptySpace2=SEmptySpace2+common.Space(12)+"|";
                    }

                    setDeliveryPrint(STitle+"\n",0);
                    setDeliveryPrint("M"+SLine+"\n",0);
                    setDeliveryPrint(SLine2+"\n",0);
                    setDeliveryPrint(SEmptySpace1+"\n",0);
                    setDeliveryPrint(SLine3+"\n",0);
                    setDeliveryPrint(SEmptySpace2+"\n",0);
                    setDeliveryPrint(SLine+"\n",0);
               }

          }catch(Exception ex){}
     }


     private void setDeliveryPrint(String Str,int iFlag)
     {
          try
          {
               FW.write(Str);

               if(Lctr < 37)
               {
                    Lctr = Lctr + 1;
                    return;
               }
               if(Pctr > 0)             
               {
                    if(iFlag==0)
                    {
                         FW.write("(Continued on Next Page) ");
                         Lctr=0;
                         setDeliverySchedules(0);
                         return;
                    }
                    else
                    {
                         FW.write("");
                         return;
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void setDeliveryQty()
     {
          try
          {
               String SBody ="";
               String SEmptySpace="";
               String SLastLine = "";


               for(int i=0; i<VOrderGroup.size(); i++)
               {

                    OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);

                    ArrayList theItemList  = (ArrayList)ordergroup.getItems();

                    for(int j=0; j<theItemList.size(); j++)
                    {
                         Item item = (Item)theItemList.get(j);

                         SBody ="|"+common.Pad(item.SItemCode,12)+"|";
                         SEmptySpace="|"+common.Space(12)+"|";
                         SLastLine = " "+common.Replicate("-",12)+"-";

     
                         for(int k=0; k<ordergroup.orderNoList.size(); k++)
                         {
                              String SOrderNo = (String)ordergroup.orderNoList.get(k);

                              String SOrderQty       = item.getOrderQty(SOrderNo);

                              SLastLine = SLastLine+  common.Replicate("-",12)+"-";
                              SBody = SBody+common.Rad(SOrderQty,12)+"|";
                              SEmptySpace=SEmptySpace+common.Space(12)+"|";

                         }

                         SBody = SBody+"\n";
                         SEmptySpace=SEmptySpace+"\n";
                         setDeliveryPrint(SBody,0);
                         setDeliveryPrint(SEmptySpace,0);
                    }
                    setDeliveryPrint(SLastLine+"\n",1);
               }

           }catch(Exception ex){}

     }
     
     public void setDataIntoVector()
     {
          VCode          = new Vector();
          VName          = new Vector();
          VBlock         = new Vector();
          VUOM           = new Vector();
          VQty           = new Vector();
          VRate          = new Vector();
          VDiscPer       = new Vector();
          VCenVatPer     = new Vector();
          VTaxPer        = new Vector();
          VSurPer        = new Vector();
          VBasic         = new Vector();
          VDisc          = new Vector();
          VCenVat        = new Vector();
          VTax           = new Vector();
          VSur           = new Vector();
          VNet           = new Vector();
          VDesc          = new Vector();
          VMake          = new Vector();
          VCatl          = new Vector();
          VDraw          = new Vector();
          VPColour       = new Vector();
          VPSet          = new Vector();
          VPSize         = new Vector();
          VPSide         = new Vector();
          VSlipFromNo    = new Vector();
          VSlipToNo      = new Vector();
          VBookFromNo    = new Vector();
          VBookToNo      = new Vector();


          VOrderGroup    = new Vector();
          VItem          = new Vector();

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection     theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
               Statement      stat           = theConnection.createStatement();

               String         QString        = getQString();
               ResultSet      res            = stat.executeQuery(QString);

               while (res.next())
               {


                    
                    OrderGroup ordergroup = null;


                    String SItemCode    = res.getString(1);  
                    String SItemName    = res.getString(2);
                    double dQty         = res.getDouble(3);
                    double dRate        = res.getDouble(4);
                    double dDiscPer     = res.getDouble(5);
                    double dDisc        = res.getDouble(6);
                    double dCenVatPer   = res.getDouble(7);
                    double dCenVat      = res.getDouble(8);
                    double dTaxPer      = res.getDouble(9);
                    double dTax         = res.getDouble(10);
                    double dNet         = res.getDouble(11);
                    double dSurPer      = res.getDouble(12);
                    double dSur         = res.getDouble(13);

                    SToName        = common.parseNull(res.getString(14));
                    SThroName      = common.parseNull(res.getString(15));
                    String SUom    = common.parseNull(res.getString(16));
                    String SDesc   = common.parseNull(res.getString(17));
                    String SMake   = common.parseNull(res.getString(18));
                    String SCatl   = common.parseNull(res.getString(19));
                    String SDraw   = common.parseNull(res.getString(20));
                    SPayTerm       = common.parseNull(res.getString(21));
                    SDueDate       = common.parseDate(res.getString(22));
                    dOthers        = common.toDouble(res.getString(23));
                    SReference     = common.parseNull(res.getString(24));


                    SMRSNo         = res.getString(25);
                    iEPCG          = res.getInt(26);
                    iOrderType     = res.getInt(27);
                    iProject       = res.getInt(28);
                    iState         = res.getInt(29);
                    SPort          = res.getString(30);
                    iAmend         = res.getInt(31);
                    String SBName  = res.getString(32);


                    String SPColour     =  common.parseNull(res.getString(33));
                    String SPSet        =  common.parseNull(res.getString(34));
                    String SPSize       =  common.parseNull(res.getString(35));
                    String SPSide       =  common.parseNull(res.getString(36));
                    String SPSlipFromNo =  common.parseNull(res.getString(37));
                    String SPSlipToNo   =  common.parseNull(res.getString(38));
                    String SBookFromNo  =  common.parseNull(res.getString(39));
                    String SBookToNo    =  common.parseNull(res.getString(40));

                    String  SOrderNo     =  res.getString(41);
                    SOrderDate   =  res.getString(42);

                    int iIndex = getIndexOf(SSupCode);

                    if(iIndex==-1)
                    {
                         ordergroup = new OrderGroup(SSupCode);
                         VOrderGroup.addElement(ordergroup);
                         iIndex=VOrderGroup.size()-1;
                    }
                    ordergroup = (OrderGroup)VOrderGroup.elementAt(iIndex);

                    Item item = null;

                    int iItemIndex  = getItemIndexOf(SItemCode);


                    if(iItemIndex==-1)
                    {

                         item = new Item(SItemCode,SItemName);
     
                         item.setOrderInfoDetails(SSupCode,
                                                    SPayTerm,SToName,SThroName,
                                                    "","","",
                                                    "",SReference,SCatl,SDraw,
                                                    SBName,SUom,SDesc,
                                                    SMake);


                         item.setOrderValueDetails(dRate,dDiscPer,dCenVatPer,
                                                    dTaxPer,dSurPer,0,0,
                                                    0,dOthers);

                         VItem.addElement(item);

                         iItemIndex = VItem.size()-1;
                         ordergroup.appendItem(item);

                    }
                    ordergroup.appendOrders(SOrderNo,SOrderDate,SDueDate);

                    item = (Item)VItem.elementAt(iItemIndex);
                    item.append(SOrderNo,SOrderDate,dQty,SDueDate,dDisc,dTax,dCenVat,dSur,dNet);

               }
               res  . close();

               ResultSet res1 = stat.executeQuery("Select MRSDate From MRS Where MRSNo="+SMRSNo+" and (MillCode=0 or MillCode is Null)");
               while(res1.next())
                    SMRSDate = common.parseDate(res1.getString(1));

               res1 . close();
               stat . close();
               theConnection.close();
          }
          catch(Exception ex){System.out.println(ex);ex.printStackTrace();}
     }

     public String getQString()
     {
          String QString  = "";

          QString = " SELECT PurchaseOrder.Item_Code, InvItems.Item_Name, PurchaseOrder.qty,"+
                    " PurchaseOrder.Rate, PurchaseOrder.DiscPer, PurchaseOrder.Disc,"+
                    " PurchaseOrder.CenVatPer,PurchaseOrder.Cenvat, PurchaseOrder.TaxPer,"+
                    " PurchaseOrder.Tax,PurchaseOrder.Net,PurchaseOrder.SurPer,PurchaseOrder.Sur,"+
                    " BookTo.ToName,BookThro.ThroName,UoM.UoMName,MatDesc.Descr,MatDesc.Make,"+
                    " InvItems.Catl,InvItems.Draw,PurchaseOrder.PayTerms,PurchaseOrder.DueDate,"+
                    " (PurchaseOrder.Plus-PurchaseOrder.Less) as Others,PurchaseOrder.Reference,"+
                    " PurchaseOrder.MRSNo,PurchaseOrder.EPCG,PurchaseOrder.OrderType,"+
                    " PurchaseOrder.Project_order,PurchaseOrder.State,Port.PortName,PurchaseOrder.amended,OrdBlock.BlockName,"+
                    " MatDesc.PAPERCOLOR,PaperSet.SETNAME,MatDesc.PAPERSIZE,PaperSide.SIDENAME,"+
                    " MatDesc.SLIPFROMNO,MatDesc.SLIPTONO,MatDesc.BOOKFROMNO,MatDesc.BOOKTONO,PurchaseOrder.OrderNo,PurchaseOrder.OrderDate "+
                    " FROM ((((PurchaseOrder inner join port on purchaseorder.portcode=port.portcode)INNER JOIN InvItems ON PurchaseOrder.Item_Code = InvItems.Item_Code) "+
                    " Inner Join UoM on UoM.UoMCode = InvItems.UomCode"+
                    " Inner Join OrdBlock on OrdBlock.Block = PurchaseOrder.OrderBlock"+
                    " Inner Join BookTo   On BookTo.ToCode = PurchaseOrder.ToCode) "+
                    " Inner Join BookThro On BookThro.ThroCode = PurchaseOrder.ThroCode) "+
                    " Left Join MatDesc On MatDesc.OrderNo = PurchaseOrder.OrderNo and MatDesc.OrderBlock = PurchaseOrder.OrderBlock and MatDesc.Item_Code = PurchaseOrder.Item_Code And MatDesc.SlNo = PurchaseOrder.SlNo "+
                    " inner join paperset on paperset.setcode = Matdesc.papersets inner join paperside on paperside.sidecode = Matdesc.paperside"+
                    " Where PurchaseOrder.Qty > 0 And PurchaseOrder.Sup_Code = '"+SSupCode+"' and PurchaseOrder.millcode = 0 and PurchaseOrder.OrderDate>="+SStDate+" and PurchaseOrder.Orderdate<="+SEnDate+  //and PurchaseOrder.OrderTypeCode=2
                    " and PurchaseOrder.SeqId="+SSeqId+
                    " and OrderTypeCode=2"+
                    " Order By DueDate ";



          return QString;
     }
     
     public void getAddr()
     {
          String QS =    " Select Supplier.Addr1,Supplier.Addr2,Supplier.Addr3,Supplier.EMail,Supplier.Fax,Place.PlaceName,Supplier.Contact "+
                         " From Supplier LEFT Join Place On Place.PlaceCode = Supplier.City_Code "+
                         " Where Supplier.Ac_Code = '"+SSupCode+"'";

          SSupplierEMail      = "";
          SSupplierMobile     = "";
          SContact            = "";

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection     theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
               Statement      stat           =  theConnection.createStatement();
               ResultSet      res            = stat.executeQuery(QS);

               while (res.next())
               {
                    SAddr1    = res.getString(1);
                    SAddr2    = res.getString(2);
                    SAddr3    = common.parseNull(res.getString(3));
                    SEMail    = common.parseNull(res.getString(4));
                    SFaxNo    = common.parseNull(res.getString(5));
                    SContact  = common.parseNull(res.getString(7));
               }

               res  . close();

               ResultSet rs                  = stat.executeQuery("Select EMail,PurMobileNo from PartyMaster where PartyCode = '"+SSupCode+"' ");
               if(rs.next())
               {
                    SSupplierEMail           = common.parseNull(rs.getString(1));
                    SSupplierMobile          = common.parseNull(rs.getString(2));
               }
               rs                            . close();

               stat . close();
               theConnection.close();
          }
          catch(Exception ex){System.out.println(ex);ex.printStackTrace();}
     }

     public boolean isStationary(String SItemCode)
     {
          String SStkGroupName     = "";
          String QS                = "";

          QS =    " select stockgroup.groupname,invitems.item_code from invitems"+
                  " inner join stockgroup on stockgroup.groupcode = invitems.stkgroupcode"+
                  " where invitems.item_code = '"+SItemCode+"'";

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection     theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
               Statement      stat          = theConnection.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result.next())
               {
                    SStkGroupName  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
               theConnection  . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }
          if(SStkGroupName.equals("STATIONARY"))
               return true;

          return false;
     }
     private int getIndexOf(String SSupCode)
     {
          int iIndex=-1;

          for(int i=0; i<VOrderGroup.size(); i++)
          {
               OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);

               if(SSupCode.equals(ordergroup.SSupCode) )
               {
                    iIndex = i;
                    break;
               }
          }
          return iIndex;
     }
     private int getItemIndexOf(String SItemCode)
     {
          int iIndex=-1;

          for(int i=0; i<VItem.size(); i++)
          {
               Item item = (Item)VItem.elementAt(i);

               if(SItemCode.equals(item.SItemCode))
               {
                    iIndex = i;
                    break;
               }
          }
          return iIndex;
     }
     private String getOrderNo()
     {
          String SOrderNo= "";
          VOrderNos      = new Vector();


          for(int i=0; i<VOrderGroup.size(); i++)
          {
               String SFirstOrderNo="";
               String SLastOrderNo="";

               OrderGroup ordergroup = (OrderGroup)VOrderGroup.elementAt(i);

               for(int k=0; k<ordergroup.orderNoList.size(); k++)
               {
                    VOrderNos.addElement((String)ordergroup.orderNoList.get(k));
               }
          }
          Collections.sort(VOrderNos);
          int iSize = VOrderNos.size();

          return VOrderNos.elementAt(0)+" To "+VOrderNos.elementAt(iSize-1);
     }

}
