package Order;

import java.util.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class YearlyOrderRecGst
{
     String SSupCode  ="";
     String SSupName  ="";
     String SToCode   ="";
     String SThroCode ="";
     Vector VOrdNo,VBlock,VDate,VBlockCode;
     Vector VId;
     Vector VCode,VName,VQty,VHCode,VRate,VDiscPer;
     Vector VCGST,VSGST,VIGST,VCess,VGstStatus;
     String SPayTerm = "";
     String SPayDays = "";

     Control control = new Control();
     Common  common  = new Common();

     YearlyOrderRecGst()
     {
          VId        = new Vector();
          VOrdNo     = new Vector();
          VBlock     = new Vector();
          VDate      = new Vector();
          VBlockCode = new Vector();
          VCode      = new Vector();
          VName      = new Vector();
          VQty       = new Vector();
          VHCode     = new Vector();
          VRate      = new Vector();
          VDiscPer   = new Vector();
	  VCGST	     = new Vector();
	  VSGST	     = new Vector();
	  VIGST	     = new Vector();
	  VCess	     = new Vector();
	  VGstStatus = new Vector();
     }

     public void setMiscDetails(Vector VSelectedName,Vector VSelectedQty,Vector VSelectedHsnCode,String SSupTable)
     {
          SSupName = control.getID("Select Name From "+SSupTable+" Where Ac_Code = '"+SSupCode+"'");
          
          for(int i=0;i<VOrdNo.size();i++)
          {
               int index = common.toInt((String)VId.elementAt(i));
               VName     .addElement(common.parseNull((String)VSelectedName.elementAt(index)));
               VQty      .addElement(common.parseNull((String)VSelectedQty.elementAt(index)));
               VHCode    .addElement(common.parseNull((String)VSelectedHsnCode.elementAt(index)));
          }
     }

}
