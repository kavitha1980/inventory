
package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class YearlyOrderResultSet
{
     NextField      TOrderNo;
     NextField      TPayDays;

     JTextField     TSupCode;
     JTextField     TRef;
     JTextField     TAdvance;
     JTextField     TPayTerm;

     JComboBox      JCTo,JCThro,JCForm,JTClaim;
     JComboBox      JEPCG,JCOrderType,JCProject,JCPort,JCState;

     DateField2     TDate;
     JButton        BSupplier;

	String 		SSupCode,SSeqId;

     String         SOrderNo       = "";
     String         SOrderDate     = "";
     String         SToCode        = "0";
     String         SThroCode      = "0";
     String         SFormCode      = "0";

     String         SOrderBlock    = "";
     String         SSupName       = "";
     String         SRef           = "";
     String         SAdvance       = "";
     String         SPayTerm       = "";
     String         SPayDays       = "";
     String         SAdd           = "";
     String         SLess          = "";
     String         SPort          = "";

     int            iBlock         = -1;
     int            iEpcg          = 0;
     int            iOrderType     = 0;
     int            iProject       = 0;
     int            iState         = 0;
     int            iMillCode      = 0;
     int            iOrderStatus   = 0;
     
     Vector         VOCode,VOName,VOMrsNo,VOQty,VORate,VODiscPer,VOCenVatPer,VOTaxPer,VOSurPer,VOrdBlockName;
     Vector         VODeptName,VOGroupName,VOUnitName,VODueDate,VOUserName;
     Vector         VOId,VDId;
     Vector         VODesc,VOMake,VODraw,VOCatl,VInvQty;

     Vector         VCode,VName;
     Vector         VToCode,VThroCode,VFormCode,VEpcg,VOrderType,VPort,VState,VProject,VBlockName;
     Vector         VItemsStatus;
     Vector         VTaxClaim1;
     Vector         VTaxClaim,VMrsUserCode,VMrsType;
     Vector         VOOrdSlNo,VOPColour,VOPSet,VOPSize,VOPSide,VOPSlipFrNo,VOPSlipToNo,VOPBookFrNo,VOPBookToNo;

	Vector         theItemVector;
	YearlyItemClass yearlyItemClass=null;

     Common         common = new Common();

     YearlyOrderMiddlePanel MiddlePanel;
     String SItemTable,SSupTable;

     JTextField     TProformaNo,TReferenceNo;
     JComboBox      JCCurrencyType;

     String         SReferenceNo= "", SProformaNo = "", SCurrencyTypeName= "";

     YearlyOrderResultSet(String SSupCode,String SSeqId,NextField TOrderNo,DateField2 TDate,JButton BSupplier,JTextField TSupCode,JTextField TRef,JTextField TAdvance,JTextField TPayTerm,NextField TPayDays,JComboBox JCTo,JComboBox JCThro,JComboBox JCForm,YearlyOrderMiddlePanel MiddlePanel,Vector VCode,Vector VName,Vector VToCode,Vector VThroCode,Vector VFormCode,JComboBox JEPCG,JComboBox JCOrderType,Vector VEpcg,Vector VOrderType,Vector VPort,JComboBox JCPort,Vector VProject,JComboBox JCProject,Vector VState,JComboBox JCState,int iOrderStatus,Vector VItemsStatus,int iMillCode,String SItemTable,String SSupTable,JTextField TReferenceNo, JTextField TProformaNo, JComboBox JCCurrencyType)
     {
          this.SSupCode       = SSupCode;
		this.SSeqId		= SSeqId;
          this.TOrderNo       = TOrderNo;
          this.TDate          = TDate;
          this.BSupplier      = BSupplier;
          this.TSupCode       = TSupCode;
          this.TRef           = TRef;
          this.TAdvance       = TAdvance;
          this.TPayTerm       = TPayTerm;
          this.TPayDays       = TPayDays;
          this.MiddlePanel    = MiddlePanel;
          this.JCTo           = JCTo;
          this.JCThro         = JCThro;
          this.JCForm         = JCForm;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VToCode        = VToCode;
          this.VThroCode      = VThroCode;
          this.VFormCode      = VFormCode;
          this.JEPCG          = JEPCG;
          this.JCOrderType    = JCOrderType;
          this.VEpcg          = VEpcg;
          this.VOrderType     = VOrderType;
          this.VPort          = VPort;
          this.JCPort         = JCPort;
          this.VState         = VState;
          this.JCState        = JCState;
          this.VProject       = VProject;
          this.JCProject      = JCProject;
          this.VItemsStatus   = VItemsStatus;
          this.iOrderStatus   = iOrderStatus;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;
          this.TReferenceNo   = TReferenceNo;
          this.TProformaNo    = TProformaNo;
          this.JCCurrencyType = JCCurrencyType;

          VOrdBlockName       = new Vector();
          VOCode              = new Vector();
          VOName              = new Vector();
          VOMrsNo             = new Vector();
          VOQty               = new Vector();
          VORate              = new Vector();
          VODiscPer           = new Vector();
          VOCenVatPer         = new Vector();
          VOTaxPer            = new Vector();
          VOSurPer            = new Vector();
          VODesc              = new Vector();
          VOMake              = new Vector();
          VODraw              = new Vector();
          VOCatl              = new Vector();
          VInvQty             = new Vector();
		VMrsType            = new Vector();
          VTaxClaim           = new Vector();
          VTaxClaim1          = new Vector();
          VTaxClaim1          . insertElementAt("Not Claimable",0);
          VTaxClaim1          . insertElementAt("Claimable",1);

          JTClaim             = new JComboBox(VTaxClaim1);
          getData();
		setQtyData();
          setData();
     }

     public void setData()
     {
          try
          {
               MiddlePanel.RowData           = new Object[VOCode.size()][17];
               MiddlePanel.IdData            = new Object[VOCode.size()];
               MiddlePanel.VMrsType          = new Vector();

               for(int i=0;i<VOCode.size();i++)
               {
                    MiddlePanel.VMrsType       . addElement((String)VMrsType.elementAt(i));
                    MiddlePanel.RowData[i][0]  = (String)VOCode.elementAt(i);
                    MiddlePanel.RowData[i][1]  = (String)VOName.elementAt(i);
                    MiddlePanel.RowData[i][2]  = (String)VTaxClaim1.elementAt(common.toInt((String)VTaxClaim.elementAt(i)));
                    MiddlePanel.RowData[i][3]  = (String)VOrdBlockName.elementAt(i);
                    MiddlePanel.RowData[i][4]  = (String)VOMrsNo.elementAt(i);
                    MiddlePanel.RowData[i][5]  = (String)VOQty.elementAt(i);
                    MiddlePanel.RowData[i][6]  = (String)VORate.elementAt(i);
                    MiddlePanel.RowData[i][7]  = (String)VODiscPer.elementAt(i);
                    MiddlePanel.RowData[i][8]  = (String)VOCenVatPer.elementAt(i);
                    MiddlePanel.RowData[i][9]  = (String)VOTaxPer.elementAt(i);
                    MiddlePanel.RowData[i][10] = (String)VOSurPer.elementAt(i);
                    MiddlePanel.RowData[i][11] = " ";
                    MiddlePanel.RowData[i][12] = " ";
                    MiddlePanel.RowData[i][13] = " ";
                    MiddlePanel.RowData[i][14] = " ";
                    MiddlePanel.RowData[i][15] = " ";
                    MiddlePanel.RowData[i][16] = " ";
               }
              
               MiddlePanel.createComponents(VCode,VName,iOrderStatus,VItemsStatus,SItemTable,SSupCode,SSeqId,theItemVector,SOrderDate);
               // Setting Common Data
     
               TOrderNo  . setText("Multiple Orders");
               TDate     . fromString1(SOrderDate);
               TDate     . TDay.setText(SOrderDate.substring(6,8));
               TDate     . TMonth.setText(SOrderDate.substring(4,6));
               TDate     . TYear.setText(SOrderDate.substring(0,4));
               BSupplier . setText(SSupName);
               TSupCode  . setText(SSupCode);
               TRef      . setText("Multiple Ref");
               TAdvance  . setText(SAdvance);
               TPayTerm  . setText(SPayTerm);
               TPayDays  . setText(SPayDays);
               JCTo      . setSelectedIndex(VToCode.indexOf(SToCode));
               JCThro    . setSelectedIndex(VThroCode.indexOf(SThroCode));
               JCForm    . setSelectedIndex(VFormCode.indexOf(SFormCode));
               JEPCG     . setSelectedIndex(iEpcg);
               JCProject . setSelectedIndex(iProject);
               JCState   . setSelectedIndex(iState);
               JCPort    . setSelectedIndex(VPort.indexOf(SPort));

               TReferenceNo   . setText("");
               TProformaNo    . setText(SProformaNo);
               if(SCurrencyTypeName.length() == 0)
               {
                    JCCurrencyType . setSelectedItem("RUPEES");
               }
               else
               {
                    JCCurrencyType . setSelectedItem(SCurrencyTypeName);
               }

               JCOrderType    . setSelectedIndex(iOrderType);
               MiddlePanel    . MiddlePanel.TAdd.setText(SAdd);
               MiddlePanel    . MiddlePanel.TLess.setText(SLess);
               MiddlePanel    . MiddlePanel.calc();
               MiddlePanel    . MiddlePanel.VMrsStatus.removeAllElements();
     
               TOrderNo.setEditable(false);
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

     public void getData()
     {
          String QString =    " SELECT PurchaseOrder.OrderDate, PurchaseOrder.OrderBlock, "+
                              " OrdBlock.BlockName, "+SSupTable+".Name, PurchaseOrder.Item_Code, InvItems.Item_Name, "+
                              " PurchaseOrder.MrsNo, sum(PurchaseOrder.Qty), Max(PurchaseOrder.Rate), Max(PurchaseOrder.DiscPer), "+
                              " Max(PurchaseOrder.CenVatPer), Max(PurchaseOrder.TaxPer), Max(PurchaseOrder.SurPer), "+
                              " Max(PurchaseOrder.Advance), Min(PurchaseOrder.ToCode), Min(PurchaseOrder.ThroCode), "+
                              " Max(PurchaseOrder.plus), Max(PurchaseOrder.Less), "+
                              " Min(PurchaseOrder.PayTerms),Min(PurchaseOrder.FormCode), "+
                              " 0 as EPCG,PurchaseOrder.OrderType,PurchaseOrder.Project_order,PurchaseOrder.State, "+
                              " Port.PortName,PurchaseOrder.PayDays,sum(PurchaseOrder.InvQty), "+
                              " Max(PurchaseOrder.TAXCLAIMABLE),"+
                              " PurchaseOrder.OrderTypeCode, "+
                              " PurchaseOrder.ProformaNo, EPCGCurrencyType.CurrencyTypeName  "+
                              " FROM PurchaseOrder LEFT JOIN Port on PurchaseOrder.PortCode=Port.PortCode "+
                              " INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block "+
                              " INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code "+
                              " INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code "+
                              " Inner Join EPCGCurrencyType on EPCGCurrencyType.CurrencyTypeCode = "+
						" (select nvl(Max(PurchaseOrder.CurrencyTypeCode),1) from PurchaseOrder "+
						" WHERE PurchaseOrder.Sup_Code='"+SSupCode+"' and PurchaseOrder.SeqId="+SSeqId+
						" and PurchaseOrder.OrderTypeCode=2 and PurchaseOrder.Qty > 0 "+
                              " and purchaseorder.millcode = "+iMillCode+")"+
                              " WHERE PurchaseOrder.Sup_Code='"+SSupCode+"' and PurchaseOrder.SeqId="+SSeqId+
						" and PurchaseOrder.OrderTypeCode=2 and PurchaseOrder.Qty > 0 "+
                              " and purchaseorder.millcode = "+iMillCode+
						" group by PurchaseOrder.OrderDate, PurchaseOrder.OrderBlock, "+
                              " OrdBlock.BlockName, "+SSupTable+".Name, PurchaseOrder.Item_Code, InvItems.Item_Name, "+
                              " PurchaseOrder.MrsNo, PurchaseOrder.OrderType,PurchaseOrder.Project_order,PurchaseOrder.State, "+
                              " Port.PortName,PurchaseOrder.PayDays, "+
                              " PurchaseOrder.OrderTypeCode, "+
                              " PurchaseOrder.ProformaNo, EPCGCurrencyType.CurrencyTypeName "+
                              " ORDER BY PurchaseOrder.Item_Code ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

			ResultSet result = stat.executeQuery(QString);

               while(result.next())
               {
                    SOrderDate     = result.getString(1);
                    iBlock         = result.getInt(2);
                    SOrderBlock    = result.getString(3);
                    SSupName       = result.getString(4);

                    VOrdBlockName  . addElement(SOrderBlock);

                    VOCode         . addElement(result.getString(5));
                    VOName         . addElement(result.getString(6));
                    VOMrsNo        . addElement(result.getString(7));
                    VOQty          . addElement(result.getString(8));
                    VORate         . addElement(result.getString(9));
                    VODiscPer      . addElement(result.getString(10));
                    VOCenVatPer    . addElement(result.getString(11));
                    VOTaxPer       . addElement(result.getString(12));
                    VOSurPer       . addElement(result.getString(13));
               
                    SAdvance       = common.parseNull(result.getString(14));
                    SToCode        = common.parseNull(result.getString(15));
                    SThroCode      = common.parseNull(result.getString(16));
                    SAdd           = common.parseNull(result.getString(17));
                    SLess          = common.parseNull(result.getString(18));
               
                    SPayTerm       = common.parseNull(result.getString(19));
                    SFormCode      = common.parseNull(result.getString(20));
                    iEpcg          = result.getInt(21);
                    iOrderType     = result.getInt(22);
                    iProject       = result.getInt(23);
                    iState         = result.getInt(24);
                    SPort          = common.parseNull(result.getString(25));
                    SPayDays       = common.parseNull(result.getString(26));

                    VInvQty        . addElement(common.parseNull((String)result.getString(27)));
                    VTaxClaim      . addElement(common.parseNull((String)result.getString(28)));
                    VMrsType       . addElement(common.parseNull((String)result.getString(29)));

                    SProformaNo         = common.parseNull((String)result.getString(30));
                    SCurrencyTypeName   = common.parseNull((String)result.getString(31));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setQtyData()
     {
          try
          {
			theItemVector = new Vector();

			yearlyItemClass=null;

               for(int i=0;i<VOCode.size();i++)
               {
                    String SItemCode = (String)VOCode.elementAt(i);
				
				int iIndex = getItemIndexOf(SItemCode);

				if(iIndex==-1)
				{
	                    yearlyItemClass = new YearlyItemClass(SSupCode,SSeqId,SItemCode,iMillCode);
     	               theItemVector.addElement(yearlyItemClass);
          	          iIndex = theItemVector.size()-1;
				}
				
				getOrderDetails(SItemCode,iIndex);
               }
              
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

     private int getItemIndexOf(String SItemCode)
     {
          int iIndex=-1;

          for(int i=0; i<theItemVector.size(); i++)
          {
			yearlyItemClass = (YearlyItemClass)theItemVector.elementAt(i);	

               if(SItemCode.equals(yearlyItemClass.SItemCode) && SSupCode.equals(yearlyItemClass.SSupCode) && SSeqId.equals(yearlyItemClass.SSeqId) && iMillCode==yearlyItemClass.iMillCode)
               {
                    iIndex = i;
                    break;
               }
          }
          return iIndex;
     }

     public void getOrderDetails(String SItemCode,int iIndex)
     {
		yearlyItemClass = (YearlyItemClass)theItemVector.elementAt(iIndex);

          String QString =    " SELECT PurchaseOrder.Id, PurchaseOrder.OrderNo, PurchaseOrder.Qty, PurchaseOrder.InvQty, PurchaseOrder.DueDate "+
                              " FROM PurchaseOrder "+
                              " WHERE PurchaseOrder.Sup_Code='"+SSupCode+"' and PurchaseOrder.SeqId="+SSeqId+
						" and PurchaseOrder.OrderTypeCode=2 and PurchaseOrder.Qty > 0 "+
                              " and purchaseorder.millcode = "+iMillCode+
						" and PurchaseOrder.Item_Code='"+SItemCode+"'"+
                              " ORDER BY PurchaseOrder.Id ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet result = stat.executeQuery(QString);

               while(result.next())
               {
                    String SPId	  = result.getString(1);
				String SPOrderNo = result.getString(2);
				String SPOrdQty  = result.getString(3);
				String SPInvQty  = result.getString(4);
				String SPDueDate = result.getString(5);

                    yearlyItemClass.setData(SPId,SPOrderNo,SPOrdQty,SPInvQty,SPDueDate);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }


}
