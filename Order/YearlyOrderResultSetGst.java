
package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class YearlyOrderResultSetGst
{
     NextField      TOrderNo;
     NextField      TPayDays;

     JTextField     TSupCode;
     JTextField     TRef;
     JTextField     TAdvance;
     JTextField     TPayTerm;

     JComboBox      JCTo,JCThro,JCForm;
     JComboBox      JEPCG,JCOrderType,JCProject,JCPort,JCState;

     DateField2     TDate;
     JButton        BSupplier;

     String 	    SSupCode,SSeqId;

     String         SOrderNo       = "";
     String         SOrderDate     = "";
     String         SToCode        = "0";
     String         SThroCode      = "0";
     String         SFormCode      = "0";

     String         SSupName       = "";
     String         SRef           = "";
     String         SAdvance       = "";
     String         SPayTerm       = "";
     String         SPayDays       = "";
     String         SAdd           = "";
     String         SLess          = "";
     String         SPort          = "";

     int            iEpcg          = 0;
     int            iOrderType     = 0;
     int            iProject       = 0;
     int            iState         = 0;
     int            iMillCode      = 0;
     int            iOrderStatus   = 0;
     
     Vector         VOCode,VOName,VOQty,VORate,VODiscPer,VOCGSTPer,VOSGSTPer,VOIGSTPer,VOCessPer;
     Vector         VInvQty;

     Vector         VCode,VName;
     Vector         VToCode,VThroCode,VFormCode,VEpcg,VOrderType,VPort,VState,VProject;
     Vector         VItemsStatus;

     Vector         theItemVector;
     YearlyItemClass yearlyItemClass=null;

     Common         common = new Common();

     YearlyOrderMiddlePanelGst MiddlePanel;
     String SItemTable,SSupTable;

     JTextField     TProformaNo,TReferenceNo;
     JComboBox      JCCurrencyType;

     String         SReferenceNo= "", SProformaNo = "", SCurrencyTypeName= "";

     YearlyOrderResultSetGst(String SSupCode,String SSeqId,NextField TOrderNo,DateField2 TDate,JButton BSupplier,JTextField TSupCode,JTextField TRef,JTextField TAdvance,JTextField TPayTerm,NextField TPayDays,JComboBox JCTo,JComboBox JCThro,JComboBox JCForm,YearlyOrderMiddlePanelGst MiddlePanel,Vector VCode,Vector VName,Vector VToCode,Vector VThroCode,Vector VFormCode,JComboBox JEPCG,JComboBox JCOrderType,Vector VEpcg,Vector VOrderType,Vector VPort,JComboBox JCPort,Vector VProject,JComboBox JCProject,Vector VState,JComboBox JCState,int iOrderStatus,Vector VItemsStatus,int iMillCode,String SItemTable,String SSupTable,JTextField TReferenceNo, JTextField TProformaNo, JComboBox JCCurrencyType)
     {
          this.SSupCode       = SSupCode;
	  this.SSeqId	      = SSeqId;
          this.TOrderNo       = TOrderNo;
          this.TDate          = TDate;
          this.BSupplier      = BSupplier;
          this.TSupCode       = TSupCode;
          this.TRef           = TRef;
          this.TAdvance       = TAdvance;
          this.TPayTerm       = TPayTerm;
          this.TPayDays       = TPayDays;
          this.MiddlePanel    = MiddlePanel;
          this.JCTo           = JCTo;
          this.JCThro         = JCThro;
          this.JCForm         = JCForm;
          this.VCode          = VCode;
          this.VName          = VName;
          this.VToCode        = VToCode;
          this.VThroCode      = VThroCode;
          this.VFormCode      = VFormCode;
          this.JEPCG          = JEPCG;
          this.JCOrderType    = JCOrderType;
          this.VEpcg          = VEpcg;
          this.VOrderType     = VOrderType;
          this.VPort          = VPort;
          this.JCPort         = JCPort;
          this.VState         = VState;
          this.JCState        = JCState;
          this.VProject       = VProject;
          this.JCProject      = JCProject;
          this.VItemsStatus   = VItemsStatus;
          this.iOrderStatus   = iOrderStatus;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;
          this.TReferenceNo   = TReferenceNo;
          this.TProformaNo    = TProformaNo;
          this.JCCurrencyType = JCCurrencyType;

          VOCode              = new Vector();
          VOName              = new Vector();
          VOQty               = new Vector();
          VORate              = new Vector();
          VODiscPer           = new Vector();
	  VOCGSTPer	      = new Vector();
	  VOSGSTPer	      = new Vector();
	  VOIGSTPer	      = new Vector();
	  VOCessPer	      = new Vector();
          VInvQty             = new Vector();
          getData();
	  setQtyData();
          setData();
     }

     public void setData()
     {
          try
          {
               MiddlePanel.RowData           = new Object[VOCode.size()][16];
               MiddlePanel.IdData            = new Object[VOCode.size()];

               for(int i=0;i<VOCode.size();i++)
               {
                    MiddlePanel.RowData[i][0]  = (String)VOCode.elementAt(i);
                    MiddlePanel.RowData[i][1]  = (String)VOName.elementAt(i);
                    MiddlePanel.RowData[i][2]  = (String)VOQty.elementAt(i);
                    MiddlePanel.RowData[i][3]  = (String)VORate.elementAt(i);
                    MiddlePanel.RowData[i][4]  = (String)VODiscPer.elementAt(i);
                    MiddlePanel.RowData[i][5]  = (String)VOCGSTPer.elementAt(i);
                    MiddlePanel.RowData[i][6]  = (String)VOSGSTPer.elementAt(i);
                    MiddlePanel.RowData[i][7]  = (String)VOIGSTPer.elementAt(i);
                    MiddlePanel.RowData[i][8]  = (String)VOCessPer.elementAt(i);
                    MiddlePanel.RowData[i][9]  = " ";
                    MiddlePanel.RowData[i][10] = " ";
                    MiddlePanel.RowData[i][11] = " ";
                    MiddlePanel.RowData[i][12] = " ";
                    MiddlePanel.RowData[i][13] = " ";
                    MiddlePanel.RowData[i][14] = " ";
                    MiddlePanel.RowData[i][15] = " ";
               }
              
               MiddlePanel.createComponents(VCode,VName,iOrderStatus,VItemsStatus,SItemTable,SSupCode,SSeqId,theItemVector,SOrderDate);
               // Setting Common Data
     
               TOrderNo  . setText("Multiple Orders");
               TDate     . fromString1(SOrderDate);
               TDate     . TDay.setText(SOrderDate.substring(6,8));
               TDate     . TMonth.setText(SOrderDate.substring(4,6));
               TDate     . TYear.setText(SOrderDate.substring(0,4));
               BSupplier . setText(SSupName);
               TSupCode  . setText(SSupCode);
               TRef      . setText("Multiple Ref");
               TAdvance  . setText(SAdvance);
               TPayTerm  . setText(SPayTerm);
               TPayDays  . setText(SPayDays);
               JCTo      . setSelectedIndex(VToCode.indexOf(SToCode));
               JCThro    . setSelectedIndex(VThroCode.indexOf(SThroCode));
               JCForm    . setSelectedIndex(VFormCode.indexOf(SFormCode));
               JEPCG     . setSelectedIndex(iEpcg);
               JCProject . setSelectedIndex(iProject);
               JCState   . setSelectedIndex(iState);
               JCPort    . setSelectedIndex(VPort.indexOf(SPort));

               TReferenceNo   . setText("");
               TProformaNo    . setText(SProformaNo);
               if(SCurrencyTypeName.length() == 0)
               {
                    JCCurrencyType . setSelectedItem("RUPEES");
               }
               else
               {
                    JCCurrencyType . setSelectedItem(SCurrencyTypeName);
               }

               JCOrderType    . setSelectedIndex(iOrderType);
               MiddlePanel    . MiddlePanel.TAdd.setText(SAdd);
               MiddlePanel    . MiddlePanel.TLess.setText(SLess);
               MiddlePanel    . MiddlePanel.calc();
     
               TOrderNo.setEditable(false);
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

     public void getData()
     {
          String QString =    " SELECT PurchaseOrder.OrderDate,  "+
                              " "+SSupTable+".Name, PurchaseOrder.Item_Code, InvItems.Item_Name, "+
                              " sum(PurchaseOrder.Qty), Max(PurchaseOrder.Rate), Max(PurchaseOrder.DiscPer), "+
                              " Max(PurchaseOrder.CGST), Max(PurchaseOrder.SGST), Max(PurchaseOrder.IGST), Max(PurchaseOrder.Cess), "+
                              " Max(PurchaseOrder.Advance), Min(PurchaseOrder.ToCode), Min(PurchaseOrder.ThroCode), "+
                              " Max(PurchaseOrder.plus), Max(PurchaseOrder.Less), "+
                              " Min(PurchaseOrder.PayTerms),Min(PurchaseOrder.FormCode), "+
                              " 0 as EPCG,PurchaseOrder.OrderType,PurchaseOrder.Project_order,PurchaseOrder.State, "+
                              " Port.PortName,PurchaseOrder.PayDays,sum(PurchaseOrder.InvQty), "+
                              " PurchaseOrder.OrderTypeCode, "+
                              " PurchaseOrder.ProformaNo, EPCGCurrencyType.CurrencyTypeName  "+
                              " FROM PurchaseOrder LEFT JOIN Port on PurchaseOrder.PortCode=Port.PortCode "+
                              " INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block "+
                              " INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code="+SSupTable+".Ac_Code "+
                              " INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code "+
                              " Inner Join EPCGCurrencyType on EPCGCurrencyType.CurrencyTypeCode = "+
			      " (select nvl(Max(PurchaseOrder.CurrencyTypeCode),1) from PurchaseOrder "+
			      " WHERE PurchaseOrder.Sup_Code='"+SSupCode+"' and PurchaseOrder.SeqId="+SSeqId+
			      " and PurchaseOrder.OrderTypeCode=2 and PurchaseOrder.Qty > 0 "+
                              " and purchaseorder.millcode = "+iMillCode+")"+
                              " WHERE PurchaseOrder.Sup_Code='"+SSupCode+"' and PurchaseOrder.SeqId="+SSeqId+
			      " and PurchaseOrder.OrderTypeCode=2 and PurchaseOrder.Qty > 0 "+
                              " and purchaseorder.millcode = "+iMillCode+
			      " group by PurchaseOrder.OrderDate, "+
                              " "+SSupTable+".Name, PurchaseOrder.Item_Code, InvItems.Item_Name, "+
                              " PurchaseOrder.OrderType,PurchaseOrder.Project_order,PurchaseOrder.State, "+
                              " Port.PortName,PurchaseOrder.PayDays, "+
                              " PurchaseOrder.OrderTypeCode, "+
                              " PurchaseOrder.ProformaNo, EPCGCurrencyType.CurrencyTypeName "+
                              " ORDER BY PurchaseOrder.Item_Code ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

			ResultSet result = stat.executeQuery(QString);

               while(result.next())
               {
                    SOrderDate     = result.getString(1);
                    SSupName       = result.getString(2);

                    VOCode         . addElement(result.getString(3));
                    VOName         . addElement(result.getString(4));
                    VOQty          . addElement(result.getString(5));
                    VORate         . addElement(result.getString(6));
                    VODiscPer      . addElement(result.getString(7));
                    VOCGSTPer      . addElement(result.getString(8));
                    VOSGSTPer      . addElement(result.getString(9));
                    VOIGSTPer      . addElement(result.getString(10));
                    VOCessPer      . addElement(result.getString(11));
               
                    SAdvance       = common.parseNull(result.getString(12));
                    SToCode        = common.parseNull(result.getString(13));
                    SThroCode      = common.parseNull(result.getString(14));
                    SAdd           = common.parseNull(result.getString(15));
                    SLess          = common.parseNull(result.getString(16));
               
                    SPayTerm       = common.parseNull(result.getString(17));
                    SFormCode      = common.parseNull(result.getString(18));
                    iEpcg          = result.getInt(19);
                    iOrderType     = result.getInt(20);
                    iProject       = result.getInt(21);
                    iState         = result.getInt(22);
                    SPort          = common.parseNull(result.getString(23));
                    SPayDays       = common.parseNull(result.getString(24));

                    VInvQty        . addElement(common.parseNull((String)result.getString(25)));

                    SProformaNo         = common.parseNull((String)result.getString(27));
                    SCurrencyTypeName   = common.parseNull((String)result.getString(28));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setQtyData()
     {
          try
          {
			theItemVector = new Vector();

			yearlyItemClass=null;

               for(int i=0;i<VOCode.size();i++)
               {
                    String SItemCode = (String)VOCode.elementAt(i);
				
				int iIndex = getItemIndexOf(SItemCode);

				if(iIndex==-1)
				{
	                    yearlyItemClass = new YearlyItemClass(SSupCode,SSeqId,SItemCode,iMillCode);
     	               theItemVector.addElement(yearlyItemClass);
          	          iIndex = theItemVector.size()-1;
				}
				
				getOrderDetails(SItemCode,iIndex);
               }
              
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

     private int getItemIndexOf(String SItemCode)
     {
          int iIndex=-1;

          for(int i=0; i<theItemVector.size(); i++)
          {
			yearlyItemClass = (YearlyItemClass)theItemVector.elementAt(i);	

               if(SItemCode.equals(yearlyItemClass.SItemCode) && SSupCode.equals(yearlyItemClass.SSupCode) && SSeqId.equals(yearlyItemClass.SSeqId) && iMillCode==yearlyItemClass.iMillCode)
               {
                    iIndex = i;
                    break;
               }
          }
          return iIndex;
     }

     public void getOrderDetails(String SItemCode,int iIndex)
     {
		yearlyItemClass = (YearlyItemClass)theItemVector.elementAt(iIndex);

          String QString =    " SELECT PurchaseOrder.Id, PurchaseOrder.OrderNo, PurchaseOrder.Qty, PurchaseOrder.InvQty, PurchaseOrder.DueDate "+
                              " FROM PurchaseOrder "+
                              " WHERE PurchaseOrder.Sup_Code='"+SSupCode+"' and PurchaseOrder.SeqId="+SSeqId+
 			      " and PurchaseOrder.OrderTypeCode=2 and PurchaseOrder.Qty > 0 "+
                              " and purchaseorder.millcode = "+iMillCode+
			      " and PurchaseOrder.Item_Code='"+SItemCode+"'"+
                              " ORDER BY PurchaseOrder.Id ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet result = stat.executeQuery(QString);

               while(result.next())
               {
                    String SPId	  = result.getString(1);
		    String SPOrderNo = result.getString(2);
	            String SPOrdQty  = result.getString(3);
		    String SPInvQty  = result.getString(4);
		    String SPDueDate = result.getString(5);

                    yearlyItemClass.setData(SPId,SPOrderNo,SPOrdQty,SPInvQty,SPDueDate);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }


}
