package Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class YearlyQtyAmendFrame extends JInternalFrame
{
     
     JLayeredPane   Layer;
	String 		SItemCode,SItemName,SOrdQty;
	int 			iMillCode,iIndex;
	Vector		theItemVector;
     JTable         ReportTable;
     YearlyTableModel  dataModel;
	String		SOrderDate;

     JLabel         LItemCode,LItemName,LTOrdQty,LTNewQty;
	JButton		BOk;

	Vector		VPOrderNo,VPOrdQty,VPInvQty,VPStatus,VPDueDate;

	AlterTabReport	tabreport;

     JPanel         TopPanel,MiddlePanel,BottomPanel;

     Common common = new Common();

     Object RowData[][];
     String ColumnData[] = {"S.No.","OrderNo","Order Qty","Recd Qty","New Order Qty","Due Date"};
     String ColumnType[] = {"N"    ,"N"      ,"N"        ,"N"       ,"E"            ,"E"       };

     ORAConnection connect;
     Connection theconnect;

     YearlyQtyAmendFrame(JLayeredPane Layer,String SItemCode,String SItemName,String SOrdQty,int iMillCode,Vector theItemVector,int iIndex,JTable ReportTable,YearlyTableModel dataModel,String SOrderDate)
     {
          this.Layer         = Layer;
          this.SItemCode     = SItemCode;
          this.SItemName     = SItemName;
          this.SOrdQty       = SOrdQty;
          this.iMillCode     = iMillCode;
          this.theItemVector = theItemVector;
          this.iIndex        = iIndex;
          this.ReportTable   = ReportTable;
          this.dataModel     = dataModel;
		this.SOrderDate    = SOrderDate;

          createComponents();
          setLayouts();
          addComponents();
		addListeners();
     }

     private void createComponents()
     {
          LItemCode      = new JLabel("");
          LItemName      = new JLabel("");
          LTOrdQty       = new JLabel("");
          LTNewQty       = new JLabel("");

		BOk			= new JButton("Ok");

          TopPanel       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BottomPanel    = new JPanel(true);
     }

     private void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(4,2,10,10));
          MiddlePanel.setLayout(new BorderLayout());

          TopPanel.setBorder(new TitledBorder("Selected Item Details"));
          MiddlePanel.setBorder(new TitledBorder("Order Breakup Details"));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,850,500);
     }

     private void addComponents()
     {
          TopPanel       .add(new JLabel("Item Code"));
          TopPanel       .add(LItemCode);
          TopPanel       .add(new JLabel("Item Name"));
          TopPanel       .add(LItemName);
          TopPanel       .add(new JLabel("Total Ordered Qty"));
          TopPanel       .add(LTOrdQty);
          TopPanel       .add(new JLabel("Total New Ordered Qty"));
          TopPanel       .add(LTNewQty);

		BottomPanel	.add(BOk);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

          setPresets();
     }

	private void addListeners()
	{
		BOk.addActionListener(new ActList());
	}

     private void setPresets()
     {
          LItemCode .setText(SItemCode);
          LItemName .setText(SItemName);
          LTOrdQty  .setText(common.getRound(common.toDouble(SOrdQty),3));
          LTNewQty  .setText(common.getRound(common.toDouble(SOrdQty),3));

          setDataVector();
          setRowData();

          try
          {
             tabreport = new AlterTabReport(RowData,ColumnData,ColumnType,VPStatus);
             MiddlePanel.add("Center",tabreport);
             tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		   tabreport.ReportTable.addKeyListener(new KeyList());
             setSelected(true);
             Layer.repaint();
             Layer.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }
     }

     private void setDataVector()
     {
          VPOrderNo      = new Vector();
          VPOrdQty       = new Vector();
          VPInvQty       = new Vector();
		VPStatus		= new Vector();
		VPDueDate      = new Vector();

		YearlyItemClass yearlyItemClass = (YearlyItemClass)theItemVector.elementAt(iIndex);

		VPOrderNo = yearlyItemClass.VPOrderNo;
		VPOrdQty  = yearlyItemClass.VPOrdQty;
		VPInvQty  = yearlyItemClass.VPInvQty;
		VPDueDate = yearlyItemClass.VPDueDate;
     }

     public void setRowData()
     {
            RowData = new Object[VPOrderNo.size()][ColumnData.length];
            for(int i=0;i<VPOrderNo.size();i++)
            {
 			    double dOrdQty = common.toDouble((String)VPOrdQty.elementAt(i));
			    double dInvQty = common.toDouble((String)VPInvQty.elementAt(i));

	              if(dInvQty<dOrdQty)
			    {
			         VPStatus.addElement("0");
			    }
			    else
			    {
			         VPStatus.addElement("1");
			    }
			
                   RowData[i][0] = String.valueOf(i+1);
			    RowData[i][1] = common.parseNull((String)VPOrderNo.elementAt(i));
                   RowData[i][2] = common.parseNull((String)VPOrdQty.elementAt(i));
                   RowData[i][3] = common.parseNull((String)VPInvQty.elementAt(i));
                   RowData[i][4] = common.parseNull((String)VPOrdQty.elementAt(i));
                   RowData[i][5] = common.parseDate((String)VPDueDate.elementAt(i));
            }
     }

	public class KeyList extends KeyAdapter
	{
		public void keyReleased(KeyEvent ke)
		{
			setNewQty();
		}
	}

	public void setNewQty()
	{
		double dTNewQty=0;
		for(int i=0;i<RowData.length;i++)
		{
			double dNewQty = common.toDouble((String)tabreport.ReportTable.getValueAt(i,4));
			dTNewQty = dTNewQty + dNewQty;
		}
		LTNewQty.setText(common.getRound(dTNewQty,3));
	}

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    if(validData())
                    {
					setNewQty();
                         saveData();
                    }
               }
          }
     }

     private boolean validData()
     {
		for(int i=0;i<RowData.length;i++)
		{
			double dOrdQty  = common.toDouble((String)VPOrdQty.elementAt(i));
			double dInvQty  = common.toDouble((String)VPInvQty.elementAt(i));
			double dNewQty  = common.toDouble((String)tabreport.ReportTable.getValueAt(i,4));
			String SDueDate = ((String)tabreport.ReportTable.getValueAt(i,5)).trim();

			if(dNewQty<=0 || dNewQty<dInvQty)
			{
				JOptionPane.showMessageDialog(null,"Invalid Quantity @Row-"+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
				return false;
			}

               if(SDueDate.length() != 10 || common.toInt(common.getDateDiff(SDueDate,common.parseDate(SOrderDate))) < 0)
               {
				JOptionPane.showMessageDialog(null,"Invalid DueDate @Row-"+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
				return false;

               }
		}

         return true;
     }

     private void saveData()
     {
		YearlyItemClass yearlyItemClass = (YearlyItemClass)theItemVector.elementAt(iIndex);

		for(int i=0;i<RowData.length;i++)
     	{
			double dOrdQty  = common.toDouble((String)VPOrdQty.elementAt(i));
			double dNewQty  = common.toDouble((String)tabreport.ReportTable.getValueAt(i,4));
			String SOldDueDate = (String)VPDueDate.elementAt(i);
			String SNewDueDate = common.pureDate(((String)tabreport.ReportTable.getValueAt(i,5)).trim());

			if(dNewQty>dOrdQty || dNewQty<dOrdQty)
			{
				yearlyItemClass.VPOrdQty.setElementAt(common.getRound(dNewQty,3),i);
			}

			if(!SOldDueDate.equals(SNewDueDate))
			{
				yearlyItemClass.VPDueDate.setElementAt(SNewDueDate,i);
			}
	    	}

		ReportTable.setValueAt(LTNewQty.getText(),iIndex,5);

  	     removeHelpFrame();
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }


}
