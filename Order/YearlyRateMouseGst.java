package Order;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class YearlyRateMouseGst extends JInternalFrame
{
      JTextField     TMatCode,TMatName;

      JButton        BOk,BCancel;
                  
      JPanel         TopPanel,FigurePanel,BottomPanel;
      JPanel         TopBottomPanel;
      JLayeredPane   Layer;
      JTable         ReportTable;
      YearlyTableModelGst  dataModel;

      JComboBox      JCType;

      String str="";

      JTextField TQty    ;
      JTextField TRate   ;
      JTextField TDisPer ;
      JTextField TCGSTPer ;
      JTextField TSGSTPer ;
      JTextField TIGSTPer ;
      JTextField TCessPer ;

      JTextField TNet   ;
      JTextField TBasic ;
      JTextField TDisc  ;
      JTextField TCGST  ;
      JTextField TSGST  ;
      JTextField TIGST  ;
      JTextField TCess  ;

      Common common = new Common();
      Connection theConnection = null;

      YearlyRateMouseGst(JLayeredPane Layer,JTable ReportTable,YearlyTableModelGst dataModel)
      {
          this.Layer       = Layer;
          this.ReportTable = ReportTable;
          this.dataModel   = dataModel;

          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
      }
      public void createComponents()
      {
          BOk           = new JButton("Okay");
          BCancel       = new JButton("Cancel");

          TopPanel      = new JPanel(true);
          TopBottomPanel= new JPanel(true);
          FigurePanel   = new JPanel(true);
          BottomPanel   = new JPanel(true);

          JCType        = new JComboBox();

          TMatCode      = new JTextField();
          TMatName      = new JTextField();

          TQty     = new JTextField();
          TRate    = new JTextField();
          TDisPer  = new JTextField();
          TCGSTPer = new JTextField();
          TSGSTPer = new JTextField();
          TIGSTPer = new JTextField();
          TCessPer = new JTextField();

          TNet    = new JTextField();
          TBasic  = new JTextField();
          TDisc   = new JTextField();
          TCGST   = new JTextField();
          TSGST   = new JTextField();
          TIGST   = new JTextField();
          TCess   = new JTextField();

          TMatCode.setEditable(false);
          TMatName.setEditable(false);

          TQty.setEditable(false);
      }
      public void setLayouts()
      {
          setBounds(80,100,650,450);
          setResizable(true);
          setClosable(true);
          setTitle("Net Rate Calculation");
          TopPanel.setLayout(new BorderLayout());
          TopBottomPanel.setLayout(new GridLayout(2,2));
      }
      public void addComponents()
      {
          getContentPane().add("Center",TopPanel);
          getContentPane().add("South",BottomPanel);

          TopBottomPanel.add(new JLabel("Material Name"));
          TopBottomPanel.add(TMatName);
          TopBottomPanel.add(new JLabel("Material Code"));
          TopBottomPanel.add(TMatCode);

          TopPanel.add("Center",FigurePanel);
          TopPanel.add("South",TopBottomPanel);

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);
      }
      public void setPresets()
      {
          int i = ReportTable.getSelectedRow();

          String SCode    = (String)ReportTable.getModel().getValueAt(i,0);
          String SName    = (String)ReportTable.getModel().getValueAt(i,1);
          String SQty     = (String)ReportTable.getModel().getValueAt(i,2);
          String SRate    = (String)ReportTable.getModel().getValueAt(i,3);
          String SDisPer  = (String)ReportTable.getModel().getValueAt(i,4);
          String SCGSTPer = (String)ReportTable.getModel().getValueAt(i,5);
          String SSGSTPer = (String)ReportTable.getModel().getValueAt(i,6);
          String SIGSTPer = (String)ReportTable.getModel().getValueAt(i,7);
          String SCessPer = (String)ReportTable.getModel().getValueAt(i,8);
          String SNet     = (String)ReportTable.getModel().getValueAt(i,15);

          TMatCode.setText(SCode);
          TMatName.setText(SName);
          TQty.setText(SQty);
          TRate.setText(SRate);
          TDisPer.setText(SDisPer);
          TCGSTPer.setText(SCGSTPer);
          TSGSTPer.setText(SSGSTPer);
          TIGSTPer.setText(SIGSTPer);
          TCessPer.setText(SCessPer);
          TNet.setText(SNet);
          FigurePanel.removeAll();
          FigurePanel.setLayout(new BorderLayout());
          FigurePanel.add("Center",getNetPane());
          setCalculation();
      }
      public void addListeners()
      {
          BOk.addActionListener(new ActList());
          BCancel.addActionListener(new ActList());
      }

     public JScrollPane getNetPane()
     {
          JPanel thePanel    = new JPanel();

          thePanel.setLayout(new GridLayout(7,4));

          thePanel.add(new JLabel("Qty"));
          thePanel.add(TQty);
          thePanel.add(new JLabel("Net Value"));
          thePanel.add(TNet);

          thePanel.add(new JLabel("Cess (%)"));
          thePanel.add(TCessPer);
          thePanel.add(new JLabel("Cess Value (Rs)"));
          thePanel.add(TCess);
          TCess.setEditable(false);

          thePanel.add(new JLabel("IGST (%)"));
          thePanel.add(TIGSTPer);
          thePanel.add(new JLabel("IGST Value (Rs)"));
          thePanel.add(TIGST);
          TIGST.setEditable(false);

          thePanel.add(new JLabel("SGST (%)"));
          thePanel.add(TSGSTPer);
          thePanel.add(new JLabel("SGST Value (Rs)"));
          thePanel.add(TSGST);
          TSGST.setEditable(false);

          thePanel.add(new JLabel("CGST (%)"));
          thePanel.add(TCGSTPer);
          thePanel.add(new JLabel("CGST Value (Rs)"));
          thePanel.add(TCGST);
          TCGST.setEditable(false);

          thePanel.add(new JLabel("Disc (%)"));
          thePanel.add(TDisPer);
          thePanel.add(new JLabel("Disc (Rs)"));
          thePanel.add(TDisc);
          TDisc.setEditable(false);

          thePanel.add(new JLabel("Basic Rate"));
          thePanel.add(TRate);
          thePanel.add(new JLabel("Basic Value"));
          thePanel.add(TBasic);

          TBasic.setEditable(false);
	  TNet.setEditable(false);		

          TRate.addKeyListener(new CalcList());
          TQty.addKeyListener(new CalcList());
          TCGSTPer.addKeyListener(new CalcList());
          TSGSTPer.addKeyListener(new CalcList());
          TIGSTPer.addKeyListener(new CalcList());
          TCessPer.addKeyListener(new CalcList());
          TDisPer.addKeyListener(new CalcList());

          return new JScrollPane(thePanel);
     }
     public class CalcList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               setCalculation();
          }
     }

     public void setCalculation()
     {
          double N  = 0;

          double DV = 0;
          double CGV = 0;
          double SGV = 0;
          double IGV = 0;
          double CEV = 0;

          double Q       = common.toDouble(TQty.getText());
          double R       = common.toDouble(TRate.getText());
          double D       = common.toDouble(TDisPer.getText());
          double CG      = common.toDouble(TCGSTPer.getText());
          double SG      = common.toDouble(TSGSTPer.getText());
          double IG      = common.toDouble(TIGSTPer.getText());
          double CE      = common.toDouble(TCessPer.getText());
          double B       = 0;

          try
          {
               B=Q*R;
          }
          catch(Exception ex)
          {
               B = 0;
          }

          DV = B*D/100;
          DV = common.toDouble(common.getRound(String.valueOf(DV),2));

          CGV = (B-DV)*(CG/100);
          CGV = common.toDouble(common.getRound(String.valueOf(CGV),2));

          SGV = (B-DV)*(SG/100);
          SGV = common.toDouble(common.getRound(String.valueOf(SGV),2));

          IGV = (B-DV)*(IG/100);
          IGV = common.toDouble(common.getRound(String.valueOf(IGV),2));

          CEV = (B-DV)*(CE/100);
          CEV = common.toDouble(common.getRound(String.valueOf(CEV),2));

          TBasic    .setText(common.getRound(B,2));
          TDisc     .setText(common.getRound(DV,2));
          TCGST     .setText(common.getRound(CGV,2));
          TSGST     .setText(common.getRound(SGV,2));
          TIGST     .setText(common.getRound(IGV,2));
          TCess     .setText(common.getRound(CEV,2));

          N = B-DV+CGV+SGV+IGV+CEV;

          TNet.setText(common.getRound(N,2));
      }

      public class ActList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    int i = ReportTable.getSelectedRow();
                    ReportTable.getModel().setValueAt(TRate.getText(),i,3);
                    ReportTable.getModel().setValueAt(TDisPer.getText(),i,4);
                    ReportTable.getModel().setValueAt(TCGSTPer.getText(),i,5);
                    ReportTable.getModel().setValueAt(TSGSTPer.getText(),i,6);
                    ReportTable.getModel().setValueAt(TIGSTPer.getText(),i,7);
                    ReportTable.getModel().setValueAt(TCessPer.getText(),i,8);

               	    removeHelpFrame();
               }
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
               }     
          }
      }
      public void removeHelpFrame()
      {
            try
            {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
            }
            catch(Exception ex) { }
      }
}
