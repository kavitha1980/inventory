package Order;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class YearlyTableModelGst extends DefaultTableModel
{
        Object    RowData[][],ColumnNames[],ColumnType[];
        JLabel    LBasic,LDiscount,LCGST,LSGST,LIGST,LCess,LNet;
        NextField TAdd,TLess;
        boolean   bflag;
        Common common = new Common();
        public YearlyTableModelGst(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType,JLabel LBasic,JLabel LDiscount,JLabel LCGST,JLabel LSGST,JLabel LIGST,JLabel LCess,NextField TAdd,NextField TLess,JLabel LNet,boolean bflag)
        {
            super(RowData,ColumnNames);
            this.RowData     = RowData;
            this.ColumnNames = ColumnNames;
            this.ColumnType  = ColumnType;
            this.LBasic      = LBasic;
            this.LDiscount   = LDiscount;
            this.LCGST       = LCGST;
            this.LSGST       = LSGST; 
            this.LIGST       = LIGST;  
            this.LCess       = LCess;
            this.TAdd        = TAdd;
            this.TLess       = TLess;
            this.LNet        = LNet;
            this.bflag       = bflag;

            for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
                setMaterialAmount(curVector);
            }
        }

       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }

       public boolean isCellEditable(int row,int col)
       {
               if(ColumnType[col]=="B" || ColumnType[col]=="E")
                  return true;
               return false;
       }

       public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
                   if(column>=2 && column<=8)
                      setMaterialAmount(rowVector);
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }

      public void setMaterialAmount(Vector RowVector)
      {
              double dTGross=0,dTDisc=0,dTCGST=0,dTSGST=0,dTIGST=0,dTCess=0,dTNet=0;

              double dQty        = common.toDouble((String)RowVector.elementAt(2));
              double dRate       = common.toDouble((String)RowVector.elementAt(3));
              double dDiscPer    = common.toDouble((String)RowVector.elementAt(4));
              double dCGST       = common.toDouble((String)RowVector.elementAt(5));
              double dSGST       = common.toDouble((String)RowVector.elementAt(6));
              double dIGST       = common.toDouble((String)RowVector.elementAt(7));
              double dCess       = common.toDouble((String)RowVector.elementAt(8));

              double dGross   = dQty*dRate;
              double dDisc    = dGross*dDiscPer/100;
	      double dBasic   = dGross - dDisc;
              double dCGSTVal = dBasic*dCGST/100;
              double dSGSTVal = dBasic*dSGST/100;
              double dIGSTVal = dBasic*dIGST/100;
              double dCessVal = dBasic*dCess/100;
              double dNet     = dBasic+dCGSTVal+dSGSTVal+dIGSTVal+dCessVal;

              RowVector.setElementAt(common.getRound(dGross,2),9);
              RowVector.setElementAt(common.getRound(dDisc,2),10);
              RowVector.setElementAt(common.getRound(dCGSTVal,2),11);
              RowVector.setElementAt(common.getRound(dSGSTVal,2),12);
              RowVector.setElementAt(common.getRound(dIGSTVal,2),13);
              RowVector.setElementAt(common.getRound(dCessVal,2),14);
              RowVector.setElementAt(common.getRound(dNet,2),15);

              for(int i=0;i<super.dataVector.size();i++)
              {
                  Vector curVector = (Vector)super.dataVector.elementAt(i);
                  dTGross   = dTGross+common.toDouble((String)curVector.elementAt(9));
                  dTDisc    = dTDisc+common.toDouble((String)curVector.elementAt(10));
                  dTCGST    = dTCGST+common.toDouble((String)curVector.elementAt(11));
                  dTSGST    = dTSGST+common.toDouble((String)curVector.elementAt(12));
                  dTIGST    = dTIGST+common.toDouble((String)curVector.elementAt(13));
                  dTCess    = dTCess+common.toDouble((String)curVector.elementAt(14));
                  dTNet     = dTNet+common.toDouble((String)curVector.elementAt(15));
              }

              dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
              LBasic.setText(common.getRound(dTGross,2));
              LDiscount.setText(common.getRound(dTDisc,2));
              LCGST.setText(common.getRound(dTCGST,2));
              LSGST.setText(common.getRound(dTSGST,2));
              LIGST.setText(common.getRound(dTIGST,2));
              LCess.setText(common.getRound(dTCess,2));
              LNet.setText(common.getRound(dTNet,2));
    }

    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
                FinalData[i][j] = (String)curVector.elementAt(j);
        }
        return FinalData;
    }

    public int getRows()
    {
         return super.dataVector.size();
    }

    public Vector getCurVector(int i)
    {
         return (Vector)super.dataVector.elementAt(i);
    }
}
