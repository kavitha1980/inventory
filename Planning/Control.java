package Planning;

import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class Control 
{
     protected      String         ID             = "";
                    Connection     theConnection  = null;
     Common         common = new Common();

     public Control()
     {

     }
     
     public String getID(String QueryString)
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               ResultSet theResult      = theStatement.executeQuery(QueryString);
               while(theResult.next())
               {
                    ID = theResult.getString(1);
               }
               theResult      . close();
               theStatement   . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);  
          }
          return ID;
	}
}
