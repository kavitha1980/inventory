package Planning;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class GroupPlanPrint
{
     String Head1 = "*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*";
     String Head2 = "|         |                                        |    |         |       |      LAST SIX MONTH CONSUMPTION         |   LAST |                               |      |       |         Monthly planning       |         3 Months Planning      |";
     String Head3 = "|         |                                        |    |         |       |                                         |   SIX  |   AVERAGE CONSUMPTION FOR     |      |       |--------------------------------|--------------------------------|";
     String Head4 = "|  ITEM   | MATERIAL AND DESCRIPTION               |UOM | OPENING |RATE / |-----------------------------------------| MONTHS |------------------------------ |MINIMU|ORDER  |BALANCE |QTY    |VALUE  |VALUE  |BALANCE |QTY    |VALUE  |VALUE  |";
     String Head5 = "|  CODE   |                                        |    |  STOCK  |  UNIT |      |      |      |      |      |      |CON.TION| MONTH |   DAY |   45  |  105  |LEVEL |PEND   |ORDER TO|RECOMEN|FOR ORD|RECD   |ORDER TO|RECOMEN|FOR ORD|RECD   |";
     String Head6 = "|         |                                        |    |         |       |      |      |      |      |      |      |        |       |       |  DAYS |  DAYS |15 DAY|QTY    |BE PLACE|TO PLAC|TO BE  |TO     |BE PLACE|TO PLAC|TO BE  |TO     |";
     String Head7 = "|         |                                        |    |         |       |      |      |      |      |      |      | TOTAL  |       |       |       |       |STOCK |       |        |ORDER  |PLACED |ORDER  |        |ORDER  |PLACED |ORDER  |";
     String Head8 = "|---------|----------------------------------------|----|---------|-------|------|------|------|------|------|------|--------|-------|-------|-------|-------|------|-------|--------|-------|-------|-------|--------|-------|-------|-------|";
     
     Common         common = new Common();
     String         SStDate,SEnDate;
     int            iMillCode;
     String         SMillName;
     
     int            Lctr      = 100;
     int            Pctr      = 0;
     int            iLen;
     
     double         dTOrder=0,dTProj=0,d3TProj=0,d3TOrder=0;
     double         dGOrder=0,dGProj=0,d3GProj=0,d3GOrder=0;
     double         dG1=0,dG2=0,dG3=0,dG4=0,dG5=0,dG6=0,dGQ=0;
     FileWriter     FW;
     
     Vector         VHName,VGName,VCode,VName,VUoM,VStock,VLRate,VPending;
     Vector         VOQty,VO3Qty,VHCode,VGCode;
     Vector         VMonthPlanClass;
     Vector         vect;
     
     boolean        flag;

     GroupPlanPrint(String SStDate,String SEnDate,Vector VMonthPlanClass,int iMillCode,String SMillName)
     {
          this.SStDate         = SStDate;
          this.SEnDate         = SEnDate;
          this.VMonthPlanClass = VMonthPlanClass;
          this.iMillCode       = iMillCode;
          this.SMillName       = SMillName;
          
          try
          {
               createVectors();
               setVectors();
               setHeader();
               prtVectors();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void createVectors()
     {
          VHName     = new Vector();    
          VGName     = new Vector();
          VCode      = new Vector();    
          VName      = new Vector();
          VUoM       = new Vector();
          VStock     = new Vector();
          VLRate     = new Vector();    
          VPending   = new Vector();    
          VOQty      = new Vector();
          VO3Qty     = new Vector();
          VHCode     = new Vector();
          VGCode     = new Vector();
          
          vect       = new Vector();
     }
     
     private void setHeader()
     {
          setVect();
          Head6 = "|         |                                        |    |         |       |";
          for(int i=0;i<vect.size();i++)
          {
               Head6 = Head6+common.Pad((String)vect.elementAt(i),6)+"|";
          }
          Head6 = Head6 +"        |       |       |  DAYS |  DAYS |15 DAY|QTY    |BE PLACE|TO PLAC|TO BE  |TO     |BE PLACE|TO PLAC|TO BE  |TO     |";
          
     }
     private void setVectors()
     {
          ResultSet result    = null;
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
                              result         = stat.executeQuery(getQS());

               while(result.next())
               {
                    VHName  .addElement(result.getString(1));
                    VGName  .addElement(result.getString(2));
                    VCode   .addElement(result.getString(3));
                    VName   .addElement(result.getString(4));
                    VUoM    .addElement(result.getString(5));
                    VStock  .addElement(result.getString(6));
                    VLRate  .addElement(common.getRound(result.getString(7),4));
                    VPending.addElement(result.getString(8));
                    VOQty   .addElement(common.getRound(result.getString(9),2));
                    VHCode  .addElement(result.getString(10));
                    VGCode  .addElement(result.getString(11));
                    VO3Qty  .addElement(common.getRound(result.getString(12),2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
          }
     }

     private String getQS()
     {
          String QS = "";

          QS = " SELECT Hod.HodName, MatGroup.GroupName, RegPool.Item_Code,  InvItems.Item_Name, "+
               " UOM.UoMName, RegPool.Stock, RegPool.Rate, RegPool.Pending, "+
               " RegPool.ToOrder, RegPool.HodCode, RegPool.Group_Code,RegPool.To3Order "+
               " FROM RegPool INNER JOIN InvItems ON "+
               " RegPool.Item_Code=InvItems.Item_Code "+
               " INNER JOIN MatGroup ON RegPool.Group_Code=MatGroup.GroupCode "+
               " INNER JOIN UOM ON UOM.UOMCODE = INVITEMS.UOMCODE"+
               " INNER JOIN Hod ON RegPool.HodCode=Hod.HodCode "+
               " Where RegPool.MillCode = "+iMillCode+
               " ORDER BY Hod.HodName, MatGroup.GroupName, InvItems.Item_Name ";

          return QS;
     }

     private void prtVectors()
     {
          String SOGroupCode = "";
          String SOHodCode   = "";
          try
          {
               FW = new FileWriter(common.getPrintPath()+"Plan.Prn");
               
               dTOrder=0;dTProj=0;d3TProj=0;d3TOrder=0;
               dGOrder=0;dGProj=0;d3GProj=0;d3GOrder=0;
               
               if(VGCode.size()!=0)
               {
                    SOGroupCode = (String)VGCode     .elementAt(0);
                    SOHodCode   = (String)VHCode     .elementAt(0);
               }

               for(int i=0;i<VCode.size();i++)
               {
                    String SGroupCode = (String)VGCode.elementAt(i);
                    String SHodCode   = (String)VHCode.elementAt(i);
                    
                    if(!SHodCode.equals(SOHodCode))
                    {
                         if(i > 0)
                              setSubFoot();
                         setFoot(1);
                         setHead((String)VHName.elementAt(i),""); 
                         SOHodCode   = SHodCode;
                         SOGroupCode = "";
                         dG1=0;dG2 = 0;dG3   = 0;dG4    = 0;dG5=0;dG6= 0;dGQ = 0;
                         dTOrder   = 0;dTProj= 0;d3TProj= 0;d3TOrder = 0;
                         dGOrder   = 0;dGProj= 0;d3GProj= 0;d3GOrder = 0;
                    }
               
                    if(!SGroupCode.equals(SOGroupCode))
                    {
                         if(i > 0)
                              setSubFoot();
                         String HeadZ = "|         |"+common.Pad(" "+(String)VGName.elementAt(i),40)+"|"+
                         common.Pad("",4)+"|"+
                         common.Rad("",9)+"|"+
                         common.Rad("",7)+"|";
                         
                         for(int j=0;j<vect.size();j++)
                              HeadZ = HeadZ+common.Rad("",6)+"|";
                         
                         HeadZ = HeadZ+common.Rad("",8)+"|"+
                         common.Rad("",7)+"|"+
                         common.Rad("",7)+"|"+
                         common.Rad("",7)+"|"+
                         common.Rad("",7)+"|"+
                         common.Rad("",6)+"|"+
                         common.Rad("",7)+"|"+
                         common.Rad("",8)+"|"+
                         common.Rad("",7)+"|"+  
                         common.Rad("",7)+"|"+
                         common.Rad("",7)+"|"+
                         common.Rad("",8)+"|"+
                         common.Rad("",7)+"|"+  
                         common.Rad("",7)+"|"+
                         common.Rad("",7)+"|";
                         
                         String HeadY = "|         |"+common.Space(40);
                         HeadY = HeadY+"|    |         |       |      |      |      |      |      |      |        |       |       |       |       |      |       |        |       |       |       |        |       |       |       |";
                         
                         FW.write(HeadY+"\n");
                         FW.write(HeadZ+"\n");
                         FW.write(HeadY+"\n");
                         Lctr = Lctr+5;
                         
                         dGOrder=0;dGProj=0;d3GProj=0;d3GOrder=0;
                         dG1=0;dG2=0;dG3=0;dG4=0;dG5=0;dG6=0;dGQ=0;
                         
                         SOGroupCode = SGroupCode;
                    }
                         
                    int index          = indexOf((String)VCode.elementAt(i));
                    vect               = new Vector();
                    if(index > -1)
                    {
                         MonthPlanClass MPC = (MonthPlanClass)VMonthPlanClass.elementAt(index);
                         vect               = MPC.getSortedQty();
                    }
                    else
                    {
                         for(int j=0;j<6;j++)
                         vect.addElement("0");
                    }
                    
                    double dP60        = 0;
                    for(int j=0;j<vect.size();j++)
                    dP60  = dP60+common.toDouble((String)vect.elementAt(j));
                    
                    dG1 = dG1+common.toDouble((String)vect.elementAt(0));
                    dG2 = dG2+common.toDouble((String)vect.elementAt(1));
                    dG3 = dG3+common.toDouble((String)vect.elementAt(2));
                    dG4 = dG4+common.toDouble((String)vect.elementAt(3));
                    dG5 = dG5+common.toDouble((String)vect.elementAt(4));
                    dG6 = dG6+common.toDouble((String)vect.elementAt(5));
                    
                    double    dStock         = common.toDouble((String)VStock.elementAt(i));
                    double    dPDCon         = common.toDouble(common.getRound(dP60/180,3));
                    double    dP30           = dPDCon*30;
                    double    dP45           = dPDCon*45;
                    double    dP105          = dPDCon*105;
                    double    dPMini         = dPDCon*15;
                    double    dPending       = common.toDouble((String)VPending.elementAt(i));
                    
                    double    dBalance       = (dPDCon*60)-(dPending+dStock);
                              dBalance       = common.toDouble(common.getRound(dBalance < 0?0:dBalance,0));
                    double    dRate          = common.toDouble((String)VLRate.elementAt(i));
                    double    dOrdQty        = common.toDouble(common.getRound((String)VOQty.elementAt(i),0));
                    double    d3OrdQty       = common.toDouble(common.getRound((String)VO3Qty.elementAt(i),0));
                              dOrdQty        = (d3OrdQty > 0?0:dOrdQty);
                    
                    double dOrderValue       = common.toDouble(common.getRound(dRate*dOrdQty,0));
                    double dBalanceValue     = common.toDouble(common.getRound(dRate*dBalance,0));
                    
                    double    d3Balance      = (dPDCon*120)-(dPending+dStock);
                              d3Balance      = common.toDouble(common.getRound(d3Balance < 0?0:d3Balance,0));
                    double    d3Rate         = common.toDouble((String)VLRate.elementAt(i));
                    double    d3OrderValue   = common.toDouble(common.getRound(dRate*d3OrdQty,0));
                    double    d3BalanceValue = common.toDouble(common.getRound(dRate*d3Balance,0));
                    
                              dTOrder        = dTOrder+dOrderValue;
                              dTProj         = dTProj+dBalanceValue;
                              d3TProj        = d3TProj+d3BalanceValue;
                              d3TOrder       = d3TOrder+d3OrderValue;
                    
                              dGOrder        = dGOrder+dOrderValue;
                              dGProj         = dGProj+dBalanceValue;
                              d3GProj        = d3GProj+d3BalanceValue;
                              d3GOrder       = d3GOrder+d3OrderValue;

                    String SXCode = (String)VCode.elementAt(i);

                    String Strl  = "|"+common.Pad((String)VCode.elementAt(i),9)+"|"+
                                   common.Pad(" "+(String)VName.elementAt(i),40)+"|"+
                                   common.Pad((String)VUoM.elementAt(i),4)+"|"+
                                   common.Rad(common.getRound(dStock,0),9)+"|"+
                                   common.Rad(common.getRound(dRate,2),7)+"|";
                    
                    for(int j=0;j<vect.size();j++)
                    Strl = Strl+common.Rad(common.getRound((String)vect.elementAt(j),0),6)+"|";
                    
                    Strl = Strl +  common.Rad(common.getRound(dP60,0),8)+"|"+
                                   common.Rad(common.getRound(dP30,0),7)+"|"+
                                   common.Rad(common.getRound(dPDCon,2),7)+"|"+
                                   common.Rad(common.getRound(dP45,0),7)+"|"+
                                   common.Rad(common.getRound(dP105,0),7)+"|"+
                                   common.Rad(common.getRound(dPMini,0),6)+"|"+
                                   common.Rad(common.getRound(dPending,0),7)+"|"+
                                   common.Rad(common.getRound(dBalance,0),8)+"|"+
                                   common.Rad(common.getRound(dOrdQty,0),7)+"|"+
                                   common.Rad(common.getRound(dOrderValue,0),7)+"|"+
                                   common.Rad(common.getRound(dBalanceValue,0),7)+"|"+
                                   common.Rad(common.getRound(d3Balance,0),8)+"|"+
                                   common.Rad(common.getRound(d3OrdQty,0),7)+"|"+        
                                   common.Rad(common.getRound(d3OrderValue,0),7)+"|"+
                                   common.Rad(common.getRound(d3BalanceValue,0),7)+"|";
                    
                    setHead((String)VHName.elementAt(i),(String)VGName.elementAt(i)); 
                    FW.write(Strl+"\n");
                    Lctr = Lctr+1;
               }
               setSubFoot();
               setFoot(1);
               FW.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     
     public void setHead(String SHod,String SGName)
     {
          if(Lctr < 63)
               return;
          
          Pctr++;
          String str1 = "Company : "+SMillName+"\n";
          String str2 = "Document: Groupwise Plan for the Period "+SStDate+"-"+SEnDate+"\n";
          String str3 = "Catagory: Regular Planning\n";
          String str4 = "HOD     : "+SHod+"\n";
          String str5 = "Page    : "+Pctr+"\n";
          
          if(SHod.equals("PRODUCTION"))
               flag = true;
          
          try
          {
               if(Pctr > 1)
               setFoot(0);
               
               FW.write(str1);
               FW.write(str2);
               FW.write(str3);
               FW.write(str4);
               FW.write(str5);
               FW.write(Head1+"\n");
               FW.write(Head2+"\n");
               FW.write(Head3+"\n");
               FW.write(Head4+"\n");
               FW.write(Head5+"\n");
               FW.write(Head6+"\n");
               FW.write(Head7+"\n");
               FW.write(Head8+"\n");

               Lctr = 13;

               if(SGName.length()>0)
               {
                    String HeadX = "|         |"+common.Pad(SGName,40);
                    HeadX = HeadX+"|    |         |       |      |      |      |      |      |      |        |       |       |       |       |      |       |        |       |       |       |        |       |       |       |";
                    FW.write(HeadX+"\n");
                    
                    String HeadY = "|         |"+common.Space(40);
                    HeadY = HeadY+"|    |         |       |      |      |      |      |      |      |        |       |       |       |       |      |       |        |       |       |       |        |       |       |       |";
                    FW.write(HeadY+"\n");
                    
                    Lctr = Lctr+2;
               }
          }
          catch(Exception ex){}
     }

     private void setSubFoot() throws Exception
     {
          dGQ=dG1+dG2+dG3+dG4+dG5+dG6;

          String HeadX = "|         |"+common.Rad("T O T A L",40)+"|"+
          common.Pad("",4)+"|"+
          common.Rad("",9)+"|"+
          common.Rad("",7)+"|";
          
          HeadX = HeadX+common.Rad(common.getRound(dG1,0),6)+"|";
          HeadX = HeadX+common.Rad(common.getRound(dG2,0),6)+"|";
          HeadX = HeadX+common.Rad(common.getRound(dG3,0),6)+"|";
          HeadX = HeadX+common.Rad(common.getRound(dG4,0),6)+"|";
          HeadX = HeadX+common.Rad(common.getRound(dG5,0),6)+"|";
          HeadX = HeadX+common.Rad(common.getRound(dG6,0),6)+"|";
          HeadX = HeadX+common.Rad(common.getRound(dGQ,0),8)+"|"+
                       common.Rad("",7)+"|"+
                       common.Rad("",7)+"|"+
                       common.Rad("",7)+"|"+
                       common.Rad("",7)+"|"+
                       common.Rad("",6)+"|"+
                       common.Rad("",7)+"|"+
                       common.Rad("",8)+"|"+
                       common.Rad("",7)+"|"+        
                       common.Rad(common.getRound(dGOrder,0),7)+"|"+
                       common.Rad(common.getRound(dGProj,0),7)+"|"+
                       common.Rad("",8)+"|"+
                       common.Rad("",7)+"|"+        
                       common.Rad(common.getRound(d3GOrder,0),7)+"|"+
                       common.Rad(common.getRound(d3GProj,0),7)+"|";
                    
                    String HeadY = "|         |"+common.Space(40);
                    HeadY = HeadY+"|    |         |       |      |      |      |      |      |      |        |       |       |       |       |      |       |        |       |       |       |        |       |       |       |";
                    
                    FW.write(HeadY+"\n");
                    FW.write(HeadX+"\n");
     }

     public void setFoot(int i) throws Exception
     {
          String str1 = common.Replicate("-",Head5.length())+"\n\n\n\n";
          String str2 = "Prepared By                  Verified By\n\n\n";
          String str3 = "< End of Report >\n";
          String str2a = "Plan for Paper Cone Stock:\n";
          String str2b = "Non Rainy Season ( Jan to June ) - Store Stock 45 days & Lead time 15 days.\n";
          String str2c = "Rainy Season ( July to Dec ) - Store Stock 70 days & Lead time 20 days.\n";
          
          Lctr = 100;
          if(i==0)
          {
               FW.write(str1+"\n");
               return;
          }
          FW.write(common.Replicate("-",Head1.length())+"\n");
          String HeadX = "|         |"+common.Pad("H O D  T O T A L",40)+"|"+
          common.Pad("",4)+"|"+
          common.Rad("",9)+"|"+
          common.Rad("",7)+"|";
          
          for(int j=0;j<vect.size();j++)
               HeadX = HeadX+common.Rad("",6)+"|";
          
          HeadX = HeadX+ common.Rad("",8)+"|"+
                         common.Rad("",7)+"|"+
                         common.Rad("",7)+"|"+
                         common.Rad("",7)+"|"+
                         common.Rad("",7)+"|"+
                         common.Rad("",6)+"|"+
                         common.Rad("",7)+"|"+
                         common.Rad("",8)+"|"+
                         common.Rad("",7)+"|"+        
                         common.Rad(common.getRound(dTOrder,0),7)+"|"+
                         common.Rad(common.getRound(dTProj,0),7)+"|"+
                         common.Rad("",8)+"|"+
                         common.Rad("",7)+"|"+        
                         common.Rad(common.getRound(d3TOrder,0),7)+"|"+
                         common.Rad(common.getRound(d3TProj,0),7)+"|";
          
          FW.write(HeadX+"\n");
          FW.write(common.Replicate("-",Head1.length())+"\n");
          FW.write(str2);
          if(flag)
          {
               FW.write("\n");
               FW.write("\n");
               FW.write(str2a);
               FW.write(str2b);
               FW.write(str2c);
          }
          flag = false;
          FW.write(str3);
          Pctr=0;
          dTOrder=0;
          dTProj=0;
     }

     private void setVect()
     {
          int index = 0;
          for(int i=0;i<VCode.size();i++)
          {
               index = indexOf((String)VCode.elementAt(i));
               if (index > -1)
               break;
          }
          
          if(index > -1)
          {
               MonthPlanClass MPC = (MonthPlanClass)VMonthPlanClass.elementAt(index);
               vect               = MPC.getSortedMonth();
          }
          else
          {
               for(int i=0;i<6;i++)
                    vect.addElement("");
          }    
     }

     private int indexOf(String SCode)
     {
          for(int i=0;i<VMonthPlanClass.size();i++)
          {
               MonthPlanClass MPC = (MonthPlanClass)VMonthPlanClass.elementAt(i);
               if(MPC.SCode.equals(SCode))
                    return i;
          }
          return -1;
     }
}
