package Planning;

import java.util.Vector;

import util.*;
import guiutil.*;
import jdbc.*;

public class MonthPlanClass
{
     String    SCode;
     Vector    VQty;
     int       iForMonth = 0;
     
     Common    common = new Common();
     
     String    ColumnData[]=new String[6];
     int       iindex[]=new int[6];
     
     MonthPlanClass()
     {
          SCode     = "";
          VQty      = new Vector();
     }
     
     MonthPlanClass(String SCode,int iForMonth)
     {
          this.SCode     = SCode;
          this.iForMonth = iForMonth;
          VQty           = new Vector();
          setMonthDet(iForMonth);
          for(int i=0;i<6;i++)
               VQty.addElement("0");
     }

     public Vector getSortedQty()
     {
          return VQty;
     }

     public Vector getSortedMonth()
     {
          Vector vect = new Vector();
          for(int i=0;i<ColumnData.length;i++)
               vect.addElement(ColumnData[i]);
          return vect;
     }

     public void append(double dQty,int iMonth)
     {
          if(iMonth == 0)
               return;

          int index = indexOf(iMonth);
          if(index==-1)
               return;
          double dTQty = common.toDouble((String)VQty.elementAt(index));
          dTQty = dTQty+dQty;
          
          VQty.setElementAt(common.getRound(dTQty,2),index);
     }

     private int indexOf(int iMonth)
     {
          int index=-1;
          for(int i=0;i<iindex.length;i++)
          {
               if(iindex[i]==iMonth)
               {
                    index=i;
                    break;
               }
          }
          return index;
     }

     private void setMonthDet(int iForMonth)
     {
          if(iForMonth==4)
          {
               ColumnData[0] = "Oct";
               ColumnData[1] = "Nov";
               ColumnData[2] = "Dec";
               ColumnData[3] = "Jan";
               ColumnData[4] = "Feb";
               ColumnData[5] = "Mar";
               
               iindex[0] = 10;
               iindex[1] = 11;
               iindex[2] = 12;
               iindex[3] = 1;
               iindex[4] = 2;
               iindex[5] = 3;
          }
          else if(iForMonth==5)
          {
               ColumnData[0] = "Nov";
               ColumnData[1] = "Dec";
               ColumnData[2] = "Jan";
               ColumnData[3] = "Feb";
               ColumnData[4] = "Mar";
               ColumnData[5] = "Apr";
               
               iindex[0] = 11;
               iindex[1] = 12;
               iindex[2] = 1;
               iindex[3] = 2;
               iindex[4] = 3;
               iindex[5] = 4;
          }
          else if(iForMonth==6)
          {
               ColumnData[0] = "Dec";
               ColumnData[1] = "Jan";
               ColumnData[2] = "Feb";
               ColumnData[3] = "Mar";
               ColumnData[4] = "Apr";
               ColumnData[5] = "May";
               
               iindex[0] = 12;
               iindex[1] = 1;
               iindex[2] = 2;
               iindex[3] = 3;
               iindex[4] = 4;
               iindex[5] = 5;
          }
          else if(iForMonth==7)
          {
               ColumnData[0] = "Jan";
               ColumnData[1] = "Feb";
               ColumnData[2] = "Mar";
               ColumnData[3] = "Apr";
               ColumnData[4] = "May";
               ColumnData[5] = "Jun";
               
               iindex[0] = 1;
               iindex[1] = 2;
               iindex[2] = 3;
               iindex[3] = 4;
               iindex[4] = 5;
               iindex[5] = 6;
          }
          else if(iForMonth==8)
          {
               ColumnData[0] = "Feb";
               ColumnData[1] = "Mar";
               ColumnData[2] = "Apr";
               ColumnData[3] = "May";
               ColumnData[4] = "Jun";
               ColumnData[5] = "Jul";
               
               iindex[0] = 2;
               iindex[1] = 3;
               iindex[2] = 4;
               iindex[3] = 5;
               iindex[4] = 6;
               iindex[5] = 7;
          }
          else if(iForMonth==9)
          {
               ColumnData[0] = "Mar";
               ColumnData[1] = "Apr";
               ColumnData[2] = "May";
               ColumnData[3] = "Jun";
               ColumnData[4] = "Jul";
               ColumnData[5] = "Aug";
               
               iindex[0] = 3;
               iindex[1] = 4;
               iindex[2] = 5;
               iindex[3] = 6;
               iindex[4] = 7;
               iindex[5] = 8;
          }
          else if(iForMonth==10)
          {
               ColumnData[0] = "Apr";
               ColumnData[1] = "May";
               ColumnData[2] = "Jun";
               ColumnData[3] = "Jul";
               ColumnData[4] = "Aug";
               ColumnData[5] = "Sep";
               
               iindex[0] = 3;
               iindex[1] = 5;
               iindex[2] = 6;
               iindex[3] = 7;
               iindex[4] = 8;
               iindex[5] = 9;
          }
          else if(iForMonth==11)
          {
               ColumnData[0] = "May";
               ColumnData[1] = "Jun";
               ColumnData[2] = "Jul";
               ColumnData[3] = "Aug";
               ColumnData[4] = "Sep";
               ColumnData[5] = "Oct";
               
               iindex[0] = 5;
               iindex[1] = 6;
               iindex[2] = 7;
               iindex[3] = 8;
               iindex[4] = 9;
               iindex[5] = 10;
          }
          else if(iForMonth==12)
          {
               ColumnData[0] = "Jun";
               ColumnData[1] = "Jul";
               ColumnData[2] = "Aug";
               ColumnData[3] = "Sep";
               ColumnData[4] = "Oct";
               ColumnData[5] = "Nov";
               
               iindex[0] = 6;
               iindex[1] = 7;
               iindex[2] = 8;
               iindex[3] = 9;
               iindex[4] = 10;
               iindex[5] = 11;
          }
          else if(iForMonth==1)
          {
               ColumnData[0] = "Jul";
               ColumnData[1] = "Aug";
               ColumnData[2] = "Sep";
               ColumnData[3] = "Oct";
               ColumnData[4] = "Nov";
               ColumnData[5] = "Dec";
               
               iindex[0] = 7;
               iindex[1] = 8;
               iindex[2] = 9;
               iindex[3] = 10;
               iindex[4] = 11;
               iindex[5] = 12;
          }
          else if(iForMonth==2)
          {
               ColumnData[0] = "Aug";
               ColumnData[1] = "Sep";
               ColumnData[2] = "Oct";
               ColumnData[3] = "Nov";
               ColumnData[4] = "Dec";
               ColumnData[5] = "Jan";
               
               iindex[0] = 8;
               iindex[1] = 9;
               iindex[2] = 10;
               iindex[3] = 11;
               iindex[4] = 12;
               iindex[5] = 1;
          }
          else if(iForMonth==3)
          {
               ColumnData[0] = "Sep";
               ColumnData[1] = "Oct";
               ColumnData[2] = "Nov";
               ColumnData[3] = "Dec";
               ColumnData[4] = "Jan";
               ColumnData[5] = "Feb";
               
               iindex[0] = 9;
               iindex[1] = 10;
               iindex[2] = 11;
               iindex[3] = 12;
               iindex[4] = 1;
               iindex[5] = 2;
          }
     }
}
