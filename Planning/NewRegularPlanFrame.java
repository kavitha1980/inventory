package Planning;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class NewRegularPlanFrame extends JInternalFrame
{
     Common         common    = new Common();
     Control        control   = new Control();

     JLayeredPane   Layer;
     RegPlanTab     tabreport1;
     TabReport      tabreport2;
     RegCritPanel   TopPanel;
     JPanel         BottomPanel,MiddlePanel;
     JButton        BOk,BPrint,BInsert;
     
     Vector         VMonthPlanClass,VMCode,VMName,VHodCode,VHod,VGroupCode,VGroup;
     Vector         VPCon,VPDCon,VProjCon,VStock,VPending,VCDays,VOQty,VO3Qty;
     Vector         VLRate,VValue,VId;
     Vector         VCode,VName,VNameCode;
     Vector         VTemp0,VTemp1;
     Vector         VItem_Code,VItem_Name;
     
     Object         RowData[][];
     
     String         ColumnData[]   = {"HoD","Material Code","Material Name","Prev 6 Months Cons","Per Day Cons.","Projected For This Month","Stock on Hand","@ Pending","One Month Order ","3 Months Order","Last PO Rate","Value","Applicable","Covering Days","Group"};
     String         ColumnType[]   = {"S"  ,           "S" ,          "S"  ,            "N"     ,            "N",                     "N"  ,            "N",        "N",          "E",                "E",         "N",          "N",   "B",              "N"  ,    "S"};

     String         SOStDate       = "", SOEnDate = "";
     String         SStDate        = "", SEnDate  = "";
     String         SServPlanNo    = "0";
     String         SFinYear       = "",SFStDate   = "",SFEnDate   = "";     
     int            iApplyctr      = 0;
     int            iMillCode,iUserCode;
     String         SItemTable,SMillName;
     boolean        bComflag       = true;

     boolean        bMonth = false;

     Connection     theConnection  = null;
                    
     public NewRegularPlanFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VNameCode,int iMillCode,String SFinYear,String SFStDate,String SFEnDate,int iUserCode,String SItemTable,String SMillName)
     {
          super("Regular Material Requirement Planning");
          this . Layer        = Layer;
          this . VCode        = VCode;
          this . VName        = VName;
          this . VNameCode    = VNameCode;
          this . iMillCode    = iMillCode;
          this . SFinYear     = SFinYear;
          this . SFStDate     = SFStDate;
          this . SFEnDate     = SFEnDate;
          this . iUserCode    = iUserCode;
          this . SItemTable   = SItemTable;
          this . SMillName    = SMillName;
     
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          searchPlan();
     }

     public void createComponents()
     {
          TopPanel       = new RegCritPanel();
          
          BottomPanel    = new JPanel();
          MiddlePanel    = new JPanel(true);
          
          BOk            = new JButton("Save Schedule (Finalization)");
          BPrint         = new JButton("Print");
          BInsert        = new JButton("Insert New Materials");

          BOk            . setEnabled(false);
          BPrint         . setEnabled(false);
          BInsert        . setEnabled(false);
          
          VMCode         = new Vector();
          VMName         = new Vector();
          VHod           = new Vector();
          VHodCode       = new Vector();
          VGroup         = new Vector();
          VGroupCode     = new Vector();
          VPCon          = new Vector();
          VPDCon         = new Vector();
          VStock         = new Vector();
          VPending       = new Vector();
          VCDays         = new Vector();
          VOQty          = new Vector();
          VO3Qty         = new Vector();
          VLRate         = new Vector();
          VValue         = new Vector();
          VId            = new Vector();
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          setTitle("Regular Material Planner for a period ");
          
          TopPanel       . BApply.setEnabled(false);
          MiddlePanel    . setLayout(new BorderLayout());
     }

     public void addComponents()
     {
          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
          getContentPane()    . add(MiddlePanel,BorderLayout.CENTER);

          BottomPanel         . add(BInsert);
          BottomPanel         . add(BOk);
          BottomPanel         . add(BPrint);
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               new GroupPlanPrint(TopPanel.TStDate.toString(),TopPanel.TDate.toString(),VMonthPlanClass,iMillCode,SMillName);
          }
     }

     public void addListeners()
     {
          TopPanel  . BPool    . addActionListener(new PoolList());
          TopPanel  . BApply   . addActionListener(new ActList());

          BOk       . addActionListener(new SaveList());
          BPrint    . addActionListener(new PrintList());
          BInsert   . addActionListener(new InsertList());
     }

     public void searchPlan()
     {
          int iRec = common.toInt(control.getID("Select count(*) From RegPool Where MillCode="+iMillCode));
          if (iRec==0)
               return;

          String SStDate=control.getID("Select StDate From TPHistory Where MillCode="+iMillCode);
          String SEnDate=control.getID("Select EnDate From TPHistory Where MillCode="+iMillCode);
          
          TopPanel  . TStDate  . setEditable(false);
          TopPanel  . TDate    . setEditable(false);
          TopPanel  . BPool    . setEnabled(false);
          TopPanel  . setAfterPooling();
          TopPanel  . BApply   . setEnabled(true);
          TopPanel  . TStDate  . fromString(SStDate);
          TopPanel  . TDate    . fromString(SEnDate);
          
          SStDate   = TopPanel. TStDate . toString();
          SEnDate   = TopPanel. TDate   . toString();
          
          SOEnDate  = common.getDate(SStDate,1,-1);
          SOStDate  = common.getDate(SOEnDate,180,-1);
          bMonth    = getMonthFlag(SOStDate);
     }

     public boolean getMonthFlag(String SOStDate)
     {
          int iFromDate = common.toInt(common.pureDate(SOStDate));
          int iIEnDate  = common.toInt(control.getID("Select Max(IssueDate) From PYIssue Where Qty>0 "));
          if(iIEnDate>=iFromDate)
               return true;

          int iGEnDate = common.toInt(control.getID("Select Max(GrnDate) From PYGrn Where Qty>0 "));

          if(iGEnDate>=iFromDate)
               return true;

          return false;
     }

     public class PoolList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               TopPanel.TStDate .setEditable(false);
               TopPanel.TDate   .setEditable(false);
               TopPanel.BPool   .setEnabled(false);
               
               if(found())
               {
                    JOptionPane.showMessageDialog(null,"Regular Orders Were Finalized","Information",JOptionPane.INFORMATION_MESSAGE);
                    return;
               }
               SStDate  = TopPanel .TStDate  .toString();
               SEnDate  = TopPanel .TDate    .toString();
               SOEnDate = common   .getDate(SStDate,1,-1);
               SOStDate = common   .getDate(SOEnDate,180,-1);
               bMonth   = getMonthFlag(SOStDate);

               setDoneData();
               setTaskPool();
               TopPanel  .setAfterPooling();
               TopPanel  .BApply.setEnabled(true);
          }
     }

     public boolean found()
     {
          String XStDate = control.getID("Select StDate From RPHistory Where StDate ='"+TopPanel.TStDate.toString()+"' and MillCode="+iMillCode);
          if(XStDate.equals(TopPanel.TStDate.toString()))
               return true;
          return false;
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(iApplyctr>0)
               {
                    try
                    {
                         repool();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         return;
                    }
               }
               try
               {
                    BOk       . setEnabled(true);
                    BPrint    . setEnabled(true);
                    BInsert   . setEnabled(true);

                    setVectorData();
                    setMonthlyConsumption();
                    setProjectedConsumption();
                    setRowData();
                    iApplyctr++;
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    try
                    {
                         PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(common.getPrintPath()+"Raj.prn")));
                         ex.printStackTrace(out);
                         out.close();
                    }
                    catch(Exception e){}
               }
          }
     }
     
     public void repool()
     {
          String QS = "";
          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();               
               }
               Statement      stat           =  theConnection.createStatement();

               for(int i=0;i<RowData.length;i++)
               {
                    String    SCDays    = (String)RowData[i][13];
                    String    SOQty     = (String)RowData[i][8];
                    String    SO3Qty    = (String)RowData[i][9];
                    String    SOValue   = (String)RowData[i][11];
                    boolean   bsig      = ((Boolean)RowData[i][12]).booleanValue();
                              SCDays    = SCDays.trim();
                              SOQty     = common.getRound(SOQty.trim(),3);
                              SO3Qty    = common.getRound(SO3Qty.trim(),3);
                              SOValue   = SOValue.trim();
                    
                    if(!bsig)
                         QS = "Delete From RegPool Where Id = "+(String)VId.elementAt(i);
                    else                                                    
                    {
                         QS = "Update RegPool Set " ;
                         QS = QS+" CDays  =0"+SCDays+",";
                         QS = QS+" ToOrder=0"+SOQty+",";
                         QS = QS+" To3Order=0"+SO3Qty+",";
                         QS = QS+" OrdVal =0"+SOValue;
                         QS = QS+" Where Id = "+(String)VId.elementAt(i);
                    }
                    stat.executeUpdate(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     
     public void setVectorData()
     {
          VMCode       . removeAllElements();
          VMName       . removeAllElements();
          VHodCode     . removeAllElements();
          VHod         . removeAllElements();
          VGroupCode   . removeAllElements();
          VGroup       . removeAllElements();
          VPCon        . removeAllElements();
          VPDCon       . removeAllElements();
          VStock       . removeAllElements();
          VPending     . removeAllElements();
          VCDays       . removeAllElements();
          VOQty        . removeAllElements();
          VO3Qty       . removeAllElements();
          VLRate       . removeAllElements();
          VValue       . removeAllElements();
          VId          . removeAllElements();
          
          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }

               Statement      stat           = theConnection.createStatement();
               ResultSet      res            = stat         .executeQuery(getQString());
               
               while(res.next())
               {
                    VHod           . addElement(res.getString(1));
                    VMCode         . addElement(res.getString(2));
                    VMName         . addElement(res.getString(3));
                    VHodCode       . addElement(res.getString(4));
                    VPCon          . addElement(res.getString(5));
                    VPDCon         . addElement(res.getString(6));
                    VStock         . addElement(res.getString(7));
                    VPending       . addElement(res.getString(8));
                    VCDays         . addElement(res.getString(9));
                    VOQty          . addElement(res.getString(10));
                    VLRate         . addElement(res.getString(11));
                    VValue         . addElement(res.getString(12));
                    VId            . addElement(res.getString(13));
                    VGroup         . addElement(res.getString(14));
                    VGroupCode     . addElement(res.getString(15));
                    VO3Qty         . addElement(res.getString(16));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     
     public void setRowData()
     {
               RowData   = new Object[VId.size()][ColumnData.length];
          
          int  iForMonth = common.toInt(TopPanel.TStDate.TMonth.getText());
          int  iSig      = (iForMonth > 3 && iForMonth < 10?1:0);
          
          for(int i=0;i<VId.size();i++)
          {
               RowData[i][0]  = (String)VHod      . elementAt(i);
               RowData[i][1]  = (String)VMCode    . elementAt(i);
               RowData[i][2]  = (String)VMName    . elementAt(i);
               
               RowData[i][3]  = (String)VPCon     . elementAt(i);
               RowData[i][4]  = (String)VPDCon    . elementAt(i);
               RowData[i][5]  = (String)VProjCon  . elementAt(i);
               RowData[i][6]  = (String)VStock    . elementAt(i);
               RowData[i][7]  = (String)VPending  . elementAt(i);
               RowData[i][8]  = common.toDouble((String)VOQty.elementAt(i))==0?"":(String)VOQty.elementAt(i);
               RowData[i][9]  = common.toDouble((String)VO3Qty.elementAt(i))==0?"":(String)VO3Qty.elementAt(i);
               RowData[i][10] = common.getRound((String)VLRate.elementAt(i),4);
               RowData[i][11] = (String)VValue    . elementAt(i);
               RowData[i][12] = new Boolean(true);
               RowData[i][13] = (String)VCDays    . elementAt(i);
               RowData[i][14] = (String)VGroup    . elementAt(i);
          }
          try
          {
               MiddlePanel.remove(tabreport1); 
          }
          catch(Exception ex){}
          try
          {
               tabreport1     = new RegPlanTab(RowData,ColumnData,ColumnType);
               MiddlePanel    . add("Center",tabreport1);
               setSelected(true);
               Layer          . repaint();
               Layer          . updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setDoneData()
     {
          VMCode         . removeAllElements();
          VMName         . removeAllElements();
          VHodCode       . removeAllElements();
          VHod           . removeAllElements();
          VGroupCode     . removeAllElements();
          VGroup         . removeAllElements();
          VPCon          . removeAllElements();
          VPDCon         . removeAllElements();
          VStock         . removeAllElements();
          VPending       . removeAllElements();
          VCDays         . removeAllElements();
          VOQty          . removeAllElements();
          VO3Qty         . removeAllElements();
          VLRate         . removeAllElements();
          VValue         . removeAllElements();
//          VId            . removeAllElements();

          
          String QS0     = "";
          String QS1     = "";
          String QS2     = "";
          String QS3     = "";
          String QS4     = "";
          String QS5     = "";
          String QS6     = "";
          String QS7     = "";
          String QS      = "";
          
          QS0 =     " create table Temp0 as"+
                    " (select item_code,orderdate,max(rate) as lastrate from (select item_code,"+
                    " orderdate,net/qty as rate from purchaseorder"+
                    " inner join (select item_code as code,max(orderdate) as orddate from purchaseorder"+
                    " where PurchaseOrder.Net> 0 And PurchaseOrder.Qty > 0"+
                    " And PurchaseOrder.MillCode="+iMillCode+
                    " group by item_code) on code = purchaseorder.item_code and orddate=purchaseorder.orderdate"+
                    " where millcode="+iMillCode+" and net>0 and qty>0)"+
                    " group by item_code,orderdate"+
                    " Union all"+
                    " select item_code,orderdate,max(rate) as lastrate from (select item_code,"+
                    " orderdate,net/qty as rate from PYOrder"+
                    " inner join (select item_code as code,max(orderdate) as orddate from PYOrder"+
                    " where PYOrder.Net> 0 And PYOrder.Qty > 0"+
                    " And PYOrder.MillCode="+iMillCode+
                    " group by item_code) on code = PYOrder.item_code and orddate = PYOrder.orderdate"+
                    " where millcode="+iMillCode+" and net>0 and qty>0)"+
                    " group by item_code,orderdate)";

          QS1 =     " create table Temp1 as"+
                    " (Select Code,sum(ConQty) as ConQty from "+
                    " (SELECT Issue.Code, Sum(Issue.Qty) AS ConQty "+
                    " FROM "+SItemTable+" INNER JOIN Issue ON "+SItemTable+".Item_Code = Issue.Code "+
                    " WHERE Issue.IssueDate>='"+common.pureDate(SOStDate)+"' And Issue.IssueDate<'"+TopPanel.TStDate.toNormal()+"' And Issue.MillCode="+iMillCode+
                    " GROUP BY Issue.Code,"+SItemTable+".Regular HAVING "+SItemTable+".Regular=1 ";

                    if(bMonth)
                    {
                         QS1 = QS1 + " Union All "+
                               " SELECT PYIssue.Code, Sum(PYIssue.Qty) AS ConQty "+
                               " FROM "+SItemTable+" INNER JOIN PYIssue ON "+SItemTable+".Item_Code = PYIssue.Code "+
                               " WHERE PYIssue.IssueDate>='"+common.pureDate(SOStDate)+"' And PYIssue.IssueDate<'"+TopPanel.TStDate.toNormal()+"' And PYIssue.MillCode="+iMillCode+
                               " GROUP BY PYIssue.Code,"+SItemTable+".Regular HAVING "+SItemTable+".Regular=1 ";
                    }
                    QS1 = QS1 + ") "+
                          " Group by Code) ";

          QS7 =     " create table Temp7 as "+
                    " (Select Code,Sum(ConQty) as ConQty From "+
                    " (SELECT GRN.Code as Code, Sum(GRN.GrnQty) AS ConQty  FROM GRN "+
                    " INNER JOIN "+SItemTable+" ON GRN.Code="+SItemTable+".Item_Code "+
                    " WHERE (Grn.GrnBlock > 1 and Grn.GrnType<>2) "+
                    " And "+SItemTable+".Regular=1 AND Grn.GrnDate>='"+common.pureDate(SOStDate)+"'"+
                    " And Grn.GrnDate<'"+TopPanel.TStDate.toNormal()+"'"+
                    " And GRN.MillCode="+iMillCode+
                    " GROUP BY GRN.Code, "+SItemTable+".Regular HAVING "+SItemTable+".Regular=1 ";

                    if(bMonth)
                    {
                         QS7 = QS7 + " Union All "+
                               " SELECT PYGRN.Code as Code, Sum(PYGRN.GrnQty) AS ConQty  FROM PYGRN "+
                               " INNER JOIN "+SItemTable+" ON PYGRN.Code="+SItemTable+".Item_Code "+
                               " WHERE (PYGrn.GrnBlock > 1 and PYGrn.GrnType<>2) "+
                               " And "+SItemTable+".Regular=1 AND PYGrn.GrnDate>='"+common.pureDate(SOStDate)+"'"+
                               " And PYGrn.GrnDate<'"+TopPanel.TStDate.toNormal()+"'"+
                               " And PYGRN.MillCode="+iMillCode+
                               " GROUP BY PYGRN.Code, "+SItemTable+".Regular HAVING "+SItemTable+".Regular=1 ";
                    }
                    QS7 = QS7 + ") "+
                          " Group by Code) ";


          QS2 =     " create table Temp2 as"+
                    " (SELECT PurchaseOrder.Item_Code, Sum(PurchaseOrder.Qty-PurchaseOrder.InvQty) AS Pending "+
                    " FROM "+SItemTable+" INNER JOIN PurchaseOrder ON "+SItemTable+".Item_Code=PurchaseOrder.Item_Code "+
                    " Where PurchaseOrder.MillCode="+iMillCode+
                    " GROUP BY PurchaseOrder.Item_Code, "+SItemTable+".Regular "+
                    " HAVING "+SItemTable+".Regular=1)";

          QS3 =     " create table Temp3 as"+
                    " (SELECT Issue.Code, Sum(Issue.Qty) AS IssueQty "+
                    " FROM Issue INNER JOIN "+SItemTable+" ON Issue.Code = "+SItemTable+".Item_Code "+
                    " Where Issue.IssueDate<'"+TopPanel.TStDate.toNormal()+"' And Issue.MillCode="+iMillCode+
                    " GROUP BY Issue.Code, "+SItemTable+".Regular "+
                    " HAVING "+SItemTable+".Regular=1) ";

          QS4 =     " create table temp4 as "+
                    " SELECT GRN.Code as Code, Sum(GRN.GrnQty) AS RecQty  FROM GRN "+
                    " INNER JOIN "+SItemTable+" ON GRN.Code="+SItemTable+".Item_Code "+
                    " Where Grn.GrnDate<'"+TopPanel.TStDate.toNormal()+"' And GRN.MillCode="+iMillCode+
                    " GROUP BY GRN.Code, "+SItemTable+".Regular HAVING "+SItemTable+".Regular=1 ";

          QS5 =     " create table Temp5 as"+
                    " (SELECT "+SItemTable+".Item_Code, max(Temp0.LastRate) AS LastRate "+
                    " FROM "+SItemTable+" INNER JOIN Temp0 ON "+SItemTable+".Item_Code = Temp0.Item_Code "+
                    " GROUP BY "+SItemTable+".Item_Code, "+SItemTable+".Regular "+
                    " HAVING "+SItemTable+".Regular=1) ";

          QS6 =     " Create table Temp6 as  "+
                    " SELECT GRN.Code as Code, Sum(GRN.GrnQty) AS IssueQty  FROM GRN "+
                    " INNER JOIN "+SItemTable+" ON GRN.Code="+SItemTable+".Item_Code "+
                    " Where (Grn.GrnBlock > 1 and Grn.GrnType<>2) And "+SItemTable+".Regular = 1 "+
                    " And Grn.GrnDate < '"+TopPanel.TStDate.toNormal()+"' And GRN.MillCode="+iMillCode+
                    " GROUP BY GRN.Code, "+SItemTable+".Regular  HAVING "+SItemTable+".Regular=1 ";

          if(iMillCode==0)
          {
               QS  =     " SELECT InvItems.Item_Code, InvItems.Item_Name, "+
                         " Temp1.ConQty, Temp2.Pending, Temp3.IssueQty, "+
                         " Temp4.RecQty, Temp5.LastRate, InvItems.OPGQTY, "+
                         " InvItems.HodCode,Temp7.ConQty,Temp6.IssueQty,InvItems.MatGroupCode "+
                         " FROM ((((((InvItems LEFT JOIN Temp1 ON InvItems.Item_Code = Temp1.Code) "+
                         " LEFT JOIN Temp2 ON InvItems.Item_Code = Temp2.Item_Code) "+
                         " LEFT JOIN Temp3 ON InvItems.Item_Code = Temp3.Code) "+
                         " LEFT JOIN Temp4 ON InvItems.Item_Code = Temp4.Code) "+
                         " LEFT JOIN Temp5 ON InvItems.Item_Code = Temp5.Item_Code) "+
                         " LEFT JOIN Temp6 ON InvItems.Item_Code = Temp6.Code) "+
                         " LEFT JOIN Temp7 ON InvItems.Item_Code = Temp7.Code "+
                         " WHERE InvItems.Regular=1 ";
          }
          else
          {
               QS  =     " SELECT "+SItemTable+".Item_Code, InvItems.Item_Name, "+
                         " Temp1.ConQty, Temp2.Pending, Temp3.IssueQty, "+
                         " Temp4.RecQty, Temp5.LastRate, "+SItemTable+".OPGQTY, "+
                         " "+SItemTable+".HodCode,Temp7.ConQty,Temp6.IssueQty,"+SItemTable+".MatGroupCode "+
                         " FROM ((((((("+SItemTable+" LEFT JOIN Temp1 ON "+SItemTable+".Item_Code = Temp1.Code) "+
                         " INNER JOIN InvItems ON "+SItemTable+".Item_Code = InvItems.Item_Code) "+
                         " LEFT JOIN Temp2 ON "+SItemTable+".Item_Code = Temp2.Item_Code) "+
                         " LEFT JOIN Temp3 ON "+SItemTable+".Item_Code = Temp3.Code) "+
                         " LEFT JOIN Temp4 ON "+SItemTable+".Item_Code = Temp4.Code) "+
                         " LEFT JOIN Temp5 ON "+SItemTable+".Item_Code = Temp5.Item_Code) "+
                         " LEFT JOIN Temp6 ON "+SItemTable+".Item_Code = Temp6.Code) "+
                         " LEFT JOIN Temp7 ON "+SItemTable+".Item_Code = Temp7.Code "+
                         " WHERE "+SItemTable+".Regular=1 ";
          }

          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }

               Statement      stat           =  theConnection.createStatement();

               try{stat.execute("Drop Table Temp0");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp1");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp2");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp3");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp4");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp5");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp6");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp7");}catch(Exception ex){}

               stat.execute(QS0);
               stat.execute(QS1);
               stat.execute(QS2);
               stat.execute(QS3);
               stat.execute(QS4);
               stat.execute(QS5);
               stat.execute(QS6);
               stat.execute(QS7);

               ResultSet  res  = stat.executeQuery(QS);

               while(res.next())
               {
                    VMCode    . addElement(res.getString(1));
                    VMName    . addElement(res.getString(2));
                    
                    double    dVal1     = common.toDouble(res.getString(3));
                    double    dVal2     = common.toDouble(res.getString(10));
                    double    dPCon     = dVal1+dVal2;
                    double    dPDCon    = common.toDouble(common.getRound(dPCon/180,2));

                    VPCon     . addElement(common.getRound(dPCon,2));
                    VPDCon    . addElement(common.getRound(dPDCon,2));

                    double    dOpQty    = common.toDouble(res.getString(8));
                    double    dRQty     = common.toDouble(res.getString(6));
                    double    dIQty     = common.toDouble(res.getString(5));

                              dIQty     = dIQty+common.toDouble(res.getString(11));

                    double    dSQty     = dOpQty + dRQty - dIQty;
                    double    dPQty     = common.toDouble(res.getString(4));
                    double    dLRate    = common.toDouble(res.getString(7));
                    double    dOQty     = (dPDCon*60)-dSQty-dPQty;
                    double    dOVal     = dOQty*dLRate;

                    VStock    . addElement(""+dSQty);

                    //by GSK 20040305

                              dPQty     = (dPQty <= 0 ? 0:dPQty);

                    VPending  . addElement(""+dPQty);
                    VCDays    . addElement("60");

                              dOQty     = (dOQty <= 0 ? 0:dOQty);
                              dOVal     = (dOQty <= 0 ? 0:dOVal);

                    VOQty     . addElement("");
                    VO3Qty    . addElement("");
                    VLRate    . addElement(""+dLRate);
                    VValue    . addElement(""+dOVal);
                    VHodCode  . addElement(res.getString(9));
                    VGroupCode. addElement(res.getString(12));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println("setDoneData : "+ex);
               ex.printStackTrace();
          }
     }
     
     public void setTaskPool()
     {
          String QS = " Insert into RegPool(id,Item_Code,HodCode,Group_Code,ConQty,PerDayCon,Stock,Pending,CDays,ToOrder,To3Order,Rate,OrdVal,Plan_Date,MillCode) Values (";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }

               Statement      stat           =  theConnection.createStatement();

               for(int i=0;i<VMCode.size();i++)
               {
                    String    QS1 = QS+ "REGPOOL_SEQ.nextVal,";
                              QS1 = QS1+"'"+(String)VMCode.elementAt(i)+"',";
                              QS1 = QS1+"0"+common.toInt((String)VHodCode.elementAt(i))+",";
                              QS1 = QS1+"0"+common.toInt((String)VGroupCode.elementAt(i))+",";
                              QS1 = QS1+"0"+(String)VPCon.elementAt(i)+",";
                              QS1 = QS1+"0"+(String)VPDCon.elementAt(i)+",";
                              QS1 = QS1+"0"+(String)VStock.elementAt(i)+",";
                              QS1 = QS1+"0"+(String)VPending.elementAt(i)+",";
                              QS1 = QS1+"0"+common.toInt((String)VCDays.elementAt(i))+",";
                              QS1 = QS1+"0"+(String)VOQty.elementAt(i)+",";
                              QS1 = QS1+"0"+(String)VO3Qty.elementAt(i)+",";
                              QS1 = QS1+"0"+(String)VLRate.elementAt(i)+",";
                              QS1 = QS1+"0"+(String)VValue.elementAt(i)+",";
                              QS1 = QS1+"'"+(String)TopPanel.TStDate.toNormal()+"',";
                              QS1 = QS1+"0"+iMillCode+")";

                    stat.execute(QS1);
               }
               stat.execute("Delete From TPHistory Where MillCode="+iMillCode);
               QS = "Insert Into TPHistory (id,StDate,EnDate,MillCode) Values( TPHistory_SEQ.nextVal,"+"'"+TopPanel.TStDate.toString()+"','"+TopPanel.TDate.toString()+"',"+iMillCode+")";
               stat . execute(QS);
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public String getQString()
     {
          String QS =    " Select Hod.HodName,RegPool.Item_Code, "+
                         " InvItems.Item_Name,RegPool.HodCode, "+
                         " RegPool.ConQty,RegPool.PerDayCon, "+
                         " RegPool.Stock,RegPool.Pending, "+
                         " RegPool.CDays,RegPool.ToOrder, "+
                         " RegPool.Rate,RegPool.OrdVal, "+
                         " RegPool.Id,MatGroup.GroupName, "+
                         " RegPool.Group_Code,RegPool.To3Order "+
                         " From ((RegPool Inner Join Hod On Hod.HodCode = RegPool.HodCode) "+
                         " Inner Join InvItems on InvItems.Item_Code = RegPool.Item_Code) "+
                         " Inner Join MatGroup on MatGroup.GroupCode = RegPool.Group_Code "+
                         " Where RegPool.MillCode="+iMillCode;
          
          String SHave   = "",str  = "";

          if(TopPanel.JRSeleHod.isSelected())
          {
               str  = "RegPool.HodCode = "+(String)TopPanel.VHodCode.elementAt(TopPanel.JCHod.getSelectedIndex());
               SHave= SHave+" and "+str;
          }
          if(TopPanel.JRSeleGroup.isSelected())
          {
               str  = "RegPool.Group_Code = "+(String)TopPanel.VGroupCode.elementAt(TopPanel.JCGroup.getSelectedIndex());
               SHave= SHave+" and "+str;
          }
          QS = QS+" "+SHave;
          
          String SOrder  = (TopPanel.TSort.getText()).trim();
          if(SOrder.length()>0)
               QS = QS+" Order By "+TopPanel.TSort.getText()+",5";
          else
               QS = QS+" Order By 1,14,3 ";

          return QS;
     }

     public class SaveList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               repool();
               savePlan();

               if(bComflag)
                    removeHelpFrame();
               else
                    BOk            . setEnabled(true);
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer . remove(this);
               Layer . repaint();
               Layer . updateUI();
          }
          catch(Exception ex) { }
     }

     public void savePlan()
     {
          SServPlanNo =  ""+(common.toInt(control.getID("Select maxNo From ConfigAll where Id = 1"))+1);

          String QS =    " Insert Into RegPlan(id,Item_Code,HodCode,Group_Code,ConQty,PerDayCon,Stock,Pending,CDays,ToOrder,To3Order,Rate,OrdVal,MillCode) "+
                         " Select RegPlan_seq.nextVal,Item_Code,HodCode,Group_Code,ConQty,PerDayCon,Stock,Pending,CDays,ToOrder,To3Order,Rate,OrdVal,MillCode From RegPool "+
                         " Where RegPool.MillCode="+iMillCode;
          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }
                              theConnection  .  setAutoCommit(false);
               Statement      stat           =  theConnection.createStatement();

               stat . executeUpdate("Update RegPlan Set Valid=1 where millcode ="+iMillCode);
               stat . execute(QS);
               stat . executeUpdate("Update RegPlan Set Serv_Plan_No="+SServPlanNo+",Plan_Date='"+TopPanel.TStDate.toNormal()+"' Where Valid=0 and millcode ="+iMillCode);
               stat . executeUpdate("Update RegPlan Set Valid=0 where millcode  ="+iMillCode);
               stat . execute("Delete From RegPool Where MillCode="+iMillCode);
               stat . execute("Delete From TPHistory Where MillCode="+iMillCode);
               stat . execute("Insert Into RPHistory (id,Serv_Plan_No,StDate,EnDate,MillCode) Values(RPHistory_SEQ.nextVal,"+SServPlanNo+",'"+TopPanel.TStDate.toString()+"','"+TopPanel.TDate.toString()+"',"+iMillCode+")");
               stat . close();
          }
          catch(Exception ex)
          {
               bComflag  = false;
               System    . out.println(ex);
               ex        . printStackTrace();
          }

          if(bComflag)
          {
               try
               {
                    UpdatedSerPlanNo();
                    theConnection  . commit();
                    System.out.println("commit");
                    theConnection  .  setAutoCommit(true);
                    JOptionPane    . showMessageDialog(null,"Data Saved","InforMation",JOptionPane.INFORMATION_MESSAGE);
               }catch(Exception ex)
               {
                    System.out.println("New Regular Planframe actionPerformed ->"+ex);ex.printStackTrace();
               }
          }
          else
          {
               try
               {
                    theConnection  . rollback();
                    System.out.println("Roll Back");
                    theConnection  .  setAutoCommit(true);
                    JOptionPane    . showMessageDialog(null,"The Given Data is not Saved Please Enter Correct data","InforMation",JOptionPane.INFORMATION_MESSAGE);
               }
               catch(Exception ex)
               {
                    System.out.println("New Regular Plan frame actionPerformed ->"+ex);ex.printStackTrace();
               }
          }
     }

     private String getPYStDate()
     {
          String SDate  = "";
          int iMonth       = common.toInt(TopPanel.TStDate.TMonth.getText())+6;

          iMonth           = iMonth > 12?iMonth-12:iMonth;
          
          int iPYear       = common.toInt((SFStDate).substring(0,4))-1;

          iPYear           = iMonth >= 1 && iMonth <=3?iPYear+1:iPYear;
          
          SDate = String.valueOf(iPYear)+(iMonth < 10 ?"0"+iMonth:String.valueOf(iMonth))+"01";

          return SDate;
     }

     private String getPYEnDate()
     {
          String SDate   = "";
          SDate = (SFEnDate).substring(0,4)+"0331";
          return SDate;
     }

     private void setMonthlyConsumption()
     {
          try
          {
               String    SPYStDate = getPYStDate();
               String    SPYEnDate = getPYEnDate();
               int       iForMonth = common.toInt(TopPanel.TStDate.TMonth.getText());
               int       iSig      = (iForMonth > 3 && iForMonth < 10?1:0);
               
               VMonthPlanClass     = new Vector();
               MonthPlanClass MPC  = new MonthPlanClass();
               
               String QS8 = "";

               QS8 =     " Select Code,Sum(ConQty) as ConQty,SubGrnDate from ( "+
                         " SELECT Issue.Code, Sum(Issue.Qty) AS ConQty, substr(Issue.IssueDate,5,2) as SubGrnDate "+
                         " FROM "+SItemTable+" "+
                         " INNER JOIN Issue ON "+SItemTable+".Item_Code = Issue.Code "+
                         " Where Issue.MillCode="+iMillCode+
                         " and Issue.IssueDate>='"+common.pureDate(SOStDate)+"'"+
                         " GROUP BY Issue.Code,"+SItemTable+".Regular, substr(Issue.IssueDate,5,2) "+
                         " HAVING "+SItemTable+".Regular=1 "+
                         " Union All "+
                         " SELECT GRN.Code as Code, Sum(GRN.GrnQty) AS ConQty, substr(Grn.GrnDate,5,2) as SubGrnDate "+
                         " FROM GRN "+
                         " INNER JOIN "+SItemTable+" ON GRN.Code="+SItemTable+".Item_Code "+
                         " WHERE (Grn.GrnBlock > 1 and Grn.GrnType<>2) And "+SItemTable+".Regular=1 "+
                         " And Grn.MillCode="+iMillCode+" and Grn.GrnDate>='"+common.pureDate(SOStDate)+"'"+
                         " GROUP BY GRN.Code, "+SItemTable+".Regular, substr(Grn.GrnDate,5,2) "+
                         " HAVING "+SItemTable+".Regular=1 ";

                         if(bMonth)
                         {
                              QS8 = QS8 + " Union All "+
                                    " SELECT PYIssue.Code, Sum(PYIssue.Qty) AS ConQty, substr(PYIssue.IssueDate,5,2) as SubGrnDate "+
                                    " FROM "+SItemTable+" "+
                                    " INNER JOIN PYIssue ON "+SItemTable+".Item_Code = PYIssue.Code "+
                                    " Where PYIssue.MillCode="+iMillCode+
                                    " and PYIssue.IssueDate>='"+common.pureDate(SOStDate)+"'"+
                                    " GROUP BY PYIssue.Code,"+SItemTable+".Regular, substr(PYIssue.IssueDate,5,2) "+
                                    " HAVING "+SItemTable+".Regular=1 "+
                                    " Union All "+
                                    " SELECT PYGRN.Code as Code, Sum(PYGRN.GrnQty) AS ConQty, substr(PYGrn.GrnDate,5,2) as SubGrnDate "+
                                    " FROM PYGRN "+
                                    " INNER JOIN "+SItemTable+" ON PYGRN.Code="+SItemTable+".Item_Code "+
                                    " WHERE (PYGrn.GrnBlock > 1 and PYGrn.GrnType<>2) And "+SItemTable+".Regular=1 "+
                                    " And PYGrn.MillCode="+iMillCode+" and PYGrn.GrnDate>='"+common.pureDate(SOStDate)+"'"+
                                    " GROUP BY PYGRN.Code, "+SItemTable+".Regular, substr(PYGrn.GrnDate,5,2) "+
                                    " HAVING "+SItemTable+".Regular=1 ";
                         }

                         QS8 = QS8 + " ) "+
                               " Group by Code,SubGrnDate "+
                               " Order by 1 ";
               
               try
               {
                    if(theConnection == null)
                    {
                         ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                        theConnection  =  oraConnection.getConnection();
                    }
                    Statement      stat           =  theConnection.createStatement();
     
                    ResultSet result = stat.executeQuery(QS8);
                    String    SPCode = "";
                    
                    VTemp0    = new Vector();
                    VTemp1    = new Vector();
                    
                    while(result.next())
                    {
                         String    SCode     = result.getString(1);
                         double    dQty      = result.getDouble(2);
                         int       iMonth    = result.getInt(3);
     
                         if (!SPCode.equals(SCode))
                         {
                              VMonthPlanClass     . addElement(MPC);
                              MPC                 = new MonthPlanClass(SCode,iForMonth);
                              SPCode              = SCode;
                         }
                         MPC       . append(dQty,iMonth);
     
                         VTemp0    . add(SCode);
                         VTemp1    . add(String.valueOf(dQty));
                    }
                    result.close();
                    stat           . close();
                    VMonthPlanClass. addElement(MPC);
                    VMonthPlanClass. removeElementAt(0);
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
                    System.out.println(ex);
               }
          }catch(Exception ex)
          {
               ex        . printStackTrace();
               System    . out     . println(ex);
          }
     }
     
     private void setProjectedConsumption()
     {
          VProjCon = new Vector();
          for(int i=0;i<VMCode.size();i++)
               VProjCon  . addElement("0");

          for(int i=0;i<VMonthPlanClass.size();i++)
          {
               MonthPlanClass MPC  = (MonthPlanClass)VMonthPlanClass.elementAt(i);
               double         dProj= common.getProjection(MPC.getSortedQty());
               int index = VMCode.indexOf(MPC.SCode);
               if(index > -1)
                    VProjCon  . setElementAt(common.getRound(dProj,2),index);
          }
     }

     public void UpdatedSerPlanNo()
     {
          String    SUString  = "";

          Statement stat      = null;
          ResultSet result    = null;

          int iId=0;
          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }
                              stat           = theConnection.createStatement();

               SUString  =  " Update ConfigAll set MaxNo = MaxNo+1 where Id = 1";

               stat .executeUpdate(SUString);
               stat .close();
          }
          catch(Exception e)
          {
               System.out.println("New RegularPlan Frame UpdatedSerPlanNo() ->"+e);
               bComflag  = false;
               e.printStackTrace();
          }
     }

     private class InsertList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               trfdOldData();
               setDoneData();
               setTaskPool();
               setOldOrder();
          }
     }

     private void trfdOldData()
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }
               Statement           stat           = theConnection.createStatement();

               PreparedStatement   psmt           = theConnection.prepareStatement("Update regpool set toOrder = ?,to3Order = ? where item_code = ? and MillCode = ?"); 

               try{stat.execute("Delete from TempRegPool Where MillCode="+iMillCode);}catch(Exception e){}

               for(int i=0;i<RowData.length;i++)
               {
                    psmt .setDouble(1,common.toDouble((String)RowData[i][8]));
                    psmt .setDouble(2,common.toDouble((String)RowData[i][9]));
                    psmt .setString(3,(String)RowData[i][1]);
                    psmt .setString(4,String.valueOf(iMillCode));
                    psmt .execute();
               }
               psmt.close();

               stat.execute("Insert Into TempRegPool (Select * From RegPool Where MillCode="+iMillCode+")");
               stat.execute("Delete From RegPool Where MillCode="+iMillCode);
               stat.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               System.out.println(ex);
          }
     }

     private void setOldOrder()
     {
          String SQuery =" Update RegPool Set"+
                         " ToOrder = (select ToOrder from TempRegPool where TempRegPool.item_code  = RegPool.item_code and millcode ="+iMillCode+"),"+
                         " To3Order= (select To3Order from TempRegPool where TempRegPool.item_code = RegPool.item_code and millcode ="+iMillCode+")"+
                         " Where MillCode = "+iMillCode;
          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }

               Statement      stat           = theConnection.createStatement();
                              stat           . execute(SQuery);
                              stat           . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               System.out.println(ex);
          }
     }
}
