package Planning;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class RegCritPanel extends JPanel
{
     JPanel         PoolPanel;
     JPanel         HodPanel;
     JPanel         GroupPanel;
     JPanel         SortPanel;
     JPanel         ApplyPanel;

     Common         common = new Common();
     Vector         VGroup,VGroupCode,VHod,VHodCode;
     JComboBox      JCGroup,JCHod;
     JTextField     TSort;
     NextField      TDelay;

     JRadioButton   JRAllGroup,JRSeleGroup;
     JRadioButton   JRAllHod,JRSeleHod;

     JButton        BPool,BApply,BValue;
     DateField      TDate;
     DateField      TStDate;

     RegCritPanel()
     {
          getHodGroup();
          createComponents();
          setLayouts();

          PoolPanel . add(TStDate);
          PoolPanel . add(TDate);
          PoolPanel . add(BPool);

          add(PoolPanel);
          add(HodPanel);
          add(GroupPanel);
          add(SortPanel);
          add(ApplyPanel);
     }

     public void setAfterPooling()
     {
          addComponents();
          addListeners();
          updateUI();
     }

     public void createComponents()
     {
          PoolPanel      = new JPanel();
          HodPanel       = new JPanel();
          GroupPanel     = new JPanel();
          SortPanel      = new JPanel();
          ApplyPanel     = new JPanel();
     
          JRAllHod       = new JRadioButton("All");
          JRSeleHod      = new JRadioButton("Selected",true);
          JCHod          = new JComboBox(VHod);
     
          JRAllGroup     = new JRadioButton("All");
          JRSeleGroup    = new JRadioButton("Selected",true);
          JCGroup        = new JComboBox(VGroup);
     
          TSort          = new JTextField();
     
          BApply         = new JButton("Apply");
          BValue         = new JButton("Valuation");
     
          TStDate        = new DateField();
          TDate          = new DateField();
          BPool          = new JButton("Pool");
          TDate          . setTodayDate();
          TStDate        . setTodayDate();
     }

     public void setLayouts()
     {
          setLayout(new GridLayout(1,5));
          PoolPanel      . setLayout(new GridLayout(3,1));      
          HodPanel       . setLayout(new GridLayout(3,1));
          GroupPanel     . setLayout(new GridLayout(3,1));   
          SortPanel      . setLayout(new GridLayout(2,1));
          ApplyPanel     . setLayout(new GridLayout(2,1));
     
          PoolPanel      . setBorder(new TitledBorder("Task Schedule"));
          HodPanel       . setBorder(new TitledBorder("Dept Head"));
          GroupPanel     . setBorder(new TitledBorder("Classification"));
          SortPanel      . setBorder(new TitledBorder("Sort on"));
          ApplyPanel     . setBorder(new TitledBorder("Control"));
     }

     public void addComponents()
     {
          HodPanel       . add(JRAllHod);
          HodPanel       . add(JRSeleHod);
          HodPanel       . add(JCHod);    
     
          GroupPanel     . add(JRAllGroup);
          GroupPanel     . add(JRSeleGroup);
          GroupPanel     . add(JCGroup);    
     
          SortPanel      . add(TSort);
          SortPanel      . add(new JLabel("eg 1,2,3"));
     
          ApplyPanel     . add(BApply);
     }

     public void addListeners()
     {
          JRAllHod       . addActionListener(new HodList());
          JRSeleHod      . addActionListener(new HodList());
          JRAllGroup     . addActionListener(new GroupList());
          JRSeleGroup    . addActionListener(new GroupList());
     }

     public class HodList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllHod)
               {
                    JRAllHod  . setSelected(true);
                    JRSeleHod . setSelected(false);
                    JCHod     . setEnabled(false);
               }
               if(ae.getSource()==JRSeleHod)
               {
                    JRAllHod  . setSelected(false);
                    JRSeleHod . setSelected(true);
                    JCHod     . setEnabled(true);
               }
          }
     }

     public class GroupList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllGroup)
               {
                    JRAllGroup     . setSelected(true);
                    JRSeleGroup    . setSelected(false);
                    JCGroup        . setEnabled(false);
               }
               if(ae.getSource()==JRSeleGroup)
               {
                    JRAllGroup     . setSelected(false);
                    JRSeleGroup    . setSelected(true);
                    JCGroup        . setEnabled(true);
               }
          }
     }

     public void getHodGroup()
     {
          ResultSet result    = null;

          VHod       = new Vector();
          VHodCode   = new Vector();
          VGroup     = new Vector();
          VGroupCode = new Vector();
     
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();

               result         = stat.executeQuery("Select HodName,HodCode From Hod Order By 1");
               while(result.next())
               {
                    VHod      .addElement(result.getString(1));
                    VHodCode  .addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery("Select GroupName,GroupCode From MatGroup Order By 1");

               while(result.next())
               {
                    VGroup    .addElement(result.getString(1));
                    VGroupCode.addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("Hod & Class :"+ex);
          }
     }
}
