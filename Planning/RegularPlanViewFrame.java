package Planning;

import javax.swing.table.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;


public class RegularPlanViewFrame extends JInternalFrame
{
     Common         common    = new Common();
     Control        control   = new Control();

     JLayeredPane   Layer;
     RegularPlanViewTab	tabreport1;
     TabReport      tabreport2;
     RegularPlanViewPanel   TopPanel;
     
     JPanel         BottomPanel,MiddlePanel;
     JButton        BOk,BPrint,BInsert,BAuthenticate;
     
     Vector         VMonthPlanClass,VMCode,VMName,VHodCode,VHod,VGroupCode,VGroup,VPlusOrMinus;
     Vector         VPCon,VPDCon,VProjCon,VStock,VPending,VCDays,VOQty,VO3Qty;
     Vector         VDeptCode,VDeptName,VBudgetDeptCode,VBudgetDeptName,VBudgetDeptGroupCode,VDeptGroupCode;
     Vector         VLRate,VValue,VId;
     Vector         VCode,VName,VNameCode;
     Vector         VTemp0,VTemp1;
     Vector         VItem_Code,VItem_Name;
     JLabel        LTotalValue;
	 
	 Vector			VMrsCode,VMrsQty;
	 
	 
     String         SOStDate       = "", SOEnDate = "";
     String         SStDate        = "", SEnDate  = "";
     String         SServPlanNo    = "0";
     String         SFinYear       = "",SFStDate   = "",SFEnDate   = "";     
     int            iApplyctr      = 0,iAuthStatus =0;
     int            iMillCode,iUserCode;
     String         SItemTable,SMillName;
     boolean        bComflag       = true;

     boolean        bMonth = false;

     Connection     theConnection  = null;

     int iCount=0;
     int iOwnerCode=0;

     JTable         theTable;
     RegularPlanViewModel   theModel;
     JTextField          JTName;
     String findstr="",findstr1="";

     //int            ColumnWidth[]  = {5,20,175,10,10,10,35,35,35,2,5,10,10,10};
	 int            ColumnWidth[]  = {5,20,130,10,10,10,50,50,35,40,45,35,2,5,10,10,10};
	 
     Connection     theConnection1;
     Vector         VBudget;
	 
	 double 		dServiceStock,dNonStock,dOtherStock;
	 
     public RegularPlanViewFrame(JLayeredPane Layer,int iMillCode,int iUserCode,String SItemTable,String SMillName,String SFStDate,String SFEnDate)
     {
          super("Regular Material Requirement Planning");
          this . Layer        = Layer;
          this . iMillCode    = iMillCode;
          this . SFStDate     = SFStDate;
          this . SFEnDate     = SFEnDate;
          this . iUserCode    = iUserCode;
          this . SItemTable   = SItemTable;
          this . SMillName    = SMillName;

          try
          {
     
              createComponents();
              setLayouts();
              addComponents();
              addListeners();
              checkAuthenticator();
              setDeptBudget();
              searchPlan();
              setTotalValue();
          }catch(Exception ex)
          {
              ex.printStackTrace();
          }
     }

     public void createComponents()
     {
          TopPanel       = new RegularPlanViewPanel();
          
          BottomPanel    = new JPanel();
          MiddlePanel    = new JPanel(true);

          BAuthenticate  = new JButton(" Authenticate  (Finalization)");
          BOk            = new JButton("Save Schedule");
          BPrint         = new JButton("Print");
          BInsert        = new JButton("Insert New Materials");

          LTotalValue    = new JLabel();

          theModel       = new RegularPlanViewModel();
          theTable       = new JTable(theModel)  // ;
          {
                public Component prepareRenderer
                           (TableCellRenderer renderer, int index_row, int index_col){
                  Component comp = super.prepareRenderer(renderer, index_row, index_col);
          
                  /*if(index_col == 6 ){
                    comp.setBackground(Color.lightGray);
                  }
                  if(index_col == 7 ){
                    comp.setBackground(Color.red);
                  }
                  if(index_col == 8 ){
                    comp.setBackground(Color.green);
                  }
                  if(index_col == 9 ){
                    comp.setBackground(Color.white);
                  } */
				  
				if(index_col == 6 || index_col == 7 || index_col == 8  || index_col == 9 ){
                    //comp.setBackground(Color.lightGray);
					comp.setBackground(Color.lightGray);
                  }
                  if(index_col == 10 ){
                    comp.setBackground(Color.red);
                  }
                  if(index_col == 11 ){
                    comp.setBackground(Color.green);
                  }
                  if(index_col == 12 ){
                    comp.setBackground(Color.white);
                  }
				  
                  
                  return comp;
                }
          };

          /*for(int i=0; i<14; i++)
          {
               TableColumn name = theTable.getColumnModel().getColumn(i);
               name.setPreferredWidth(ColumnWidth[i]);
          }*/
		  
          for(int i=0; i<16; i++)
          {
               TableColumn name = theTable.getColumnModel().getColumn(i);
               name.setPreferredWidth(ColumnWidth[i]);
          }
		  

          theTable.getTableHeader().setReorderingAllowed(false);
          
          BOk            . setEnabled(false);
          BPrint         . setEnabled(false);
          BInsert        . setEnabled(false);
          BAuthenticate  . setEnabled(false);

          JTName            = new JTextField(20);

          VMCode         = new Vector();
          VMName         = new Vector();
          VHod           = new Vector();
          VHodCode       = new Vector();
          VDeptCode      = new Vector();
          VDeptGroupCode = new Vector();
          VDeptName      = new Vector();

          VGroup         = new Vector();
          VGroupCode     = new Vector();
          VPCon          = new Vector();
          VPDCon         = new Vector();
          VStock         = new Vector();
          VPending       = new Vector();
          VCDays         = new Vector();
          VOQty          = new Vector();
          VO3Qty         = new Vector();
          VLRate         = new Vector();
          VValue         = new Vector();
          VId            = new Vector();

          VPlusOrMinus   = new Vector();

          BAuthenticate.setEnabled(false);
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,850,900);
          setTitle(" Regular Material Planner for a period ");
          
          TopPanel       . BApply.setEnabled(false);
          MiddlePanel    . setLayout(new BorderLayout());
          
     }

     public void addComponents()
     {
          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
          getContentPane()    . add(MiddlePanel,BorderLayout.CENTER);


//        BottomPanel         . add(BInsert);


//        BottomPanel         . add(BOk);
//		BottomPanel         . add(BPrint);


          MiddlePanel         . add(new JScrollPane(theTable));
          MiddlePanel.add("South",JTName);


       //   BottomPanel         . add(BAuthenticate);
          BottomPanel         . add(new JLabel(" Total Value :"));
          BottomPanel         . add(LTotalValue);

          theTable.addKeyListener(new TableKeyList());

     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               new GroupPlanPrint(TopPanel.TStDate.toString(),TopPanel.TDate.toString(),VMonthPlanClass,iMillCode,SMillName);
          }
     }

     public void addListeners()
     {
          TopPanel  . BPool    . addActionListener(new PoolList());
          TopPanel  . BApply   . addActionListener(new ActList());

          BOk       . addActionListener(new SaveList());
          BPrint    . addActionListener(new PrintList());
          BInsert   . addActionListener(new InsertList());

          BAuthenticate.addActionListener(new AuthenticateList());

     }

     public void searchPlan()
     {
          String QS="Select count(*) From RegPoolTemp Where MillCode="+iMillCode+" and OwnerCode="+iOwnerCode;
          int iRec = common.toInt(control.getID(QS));

          if (iRec==0)
               return;


		QS = null;
		QS = "Select StDate From TPHistoryTemp Where MillCode="+iMillCode+" and UserCode="+iOwnerCode;
		
          String SStDate=control.getID(QS);
          String SEnDate=control.getID("Select EnDate From TPHistoryTemp Where MillCode="+iMillCode+" and UserCode="+iOwnerCode);

          TopPanel  . BPool    . setEnabled(false);
          TopPanel  . setAfterPooling();
          TopPanel  . BApply   . setEnabled(true);

          if(common.toInt(common.pureDate(SStDate).substring(0,6))<common.toInt(common.getServerPureDate().substring(0,6)))
          {

               TopPanel  . TStDate        . setTodayDate();
               TopPanel  . TDate          . setTodayDate();
          }
          else
          {
               TopPanel  . TStDate  . fromString(SStDate);
               TopPanel  . TDate    . fromString(SEnDate);
          }

          SStDate   = TopPanel. TStDate . toString();
          SEnDate   = TopPanel. TDate   . toString();

          String SDate = common.parseDate(common.pureDate(SStDate).substring(0,6)+"01");


          SOEnDate  = common.getDate(SDate,1,-1);  // SStDate
          SOStDate  = common.getDate(SOEnDate,180,-1);
          SOStDate  = common.parseDate(common.pureDate(SOStDate).substring(0,6)+"01");

          bMonth    = getMonthFlag(SOStDate);
     }

     public boolean getMonthFlag(String SOStDate)
     {
          int iFromDate = common.toInt(common.pureDate(SOStDate));
          int iIEnDate  = common.toInt(control.getID("Select Max(IssueDate) From PYIssue Where Qty>0 "));

          if(iIEnDate>=iFromDate)
               return true;

          int iGEnDate = common.toInt(control.getID("Select Max(GrnDate) From PYGrn Where Qty>0 "));

          if(iGEnDate>=iFromDate)
               return true;

          return false;
     }

     public class PoolList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               TopPanel.BPool   .setEnabled(false);
           
			/*
				String SFinalDate = getMonthlyMrsFinalDate();

				if(found())
				{
					JOptionPane.showMessageDialog(null,"Planning  Were Finalized","Information",JOptionPane.INFORMATION_MESSAGE);
					return;
				}

				if(common.toInt(SFinalDate)<common.toInt(common.getServerPureDate()))
				{
					JOptionPane.showMessageDialog(null," 7th of Every Month is FinalDate for Monthly Mrs");
					return;
				}

				SStDate  = TopPanel .TStDate  .toString();
				SEnDate  = TopPanel .TDate    .toString();

				String SDate = common.parseDate(common.pureDate(SStDate).substring(0,6)+"01");


				SOEnDate = common   .getDate(SDate,1,-1); /// SStDate
				SOStDate = common   .getDate(SOEnDate,180,-1);
				bMonth   = getMonthFlag(SOStDate);

				setDoneData();
				setTaskPool();
			*/
			
               TopPanel  .setAfterPooling();
               TopPanel  .BApply.setEnabled(true);
          }
     }

     public boolean found()
     {

/*
          String XStDate = control.getID("Select StDate From RPHistoryTemp Where StDate ='"+TopPanel.TStDate.toString()+"' and MillCode="+iMillCode+"  and UserCode="+iOwnerCode);

          if(XStDate.substring(0,6).equals(common.pureDate(TopPanel.TStDate.toString()).substring(0,6)))
               return true;
          return false

          */

          int iCount=0;

          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }
               Statement      stat           =  theConnection.createStatement();

               String SPlanMonth = TopPanel.TStDate.toNormal().substring(0,6);

               String QS     = " Select count(*) from PoolingCompleted where PoolingMonth="+SPlanMonth+
                               " and UserCode="+iOwnerCode+" and MillCode="+iMillCode;

               ResultSet res  = stat.executeQuery(QS);
               if(res.next())
               {
                    iCount = res.getInt(1);
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
          if(iCount==0)
               return false;

          return true;

     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
			setOSOEDate();
			setUserAndOwnerCode();
			setVectorData();
			setMrsData();
			setMonthlyConsumption();
			setProjectedConsumption();
			setRowData();
			setTotalValue();

			/*
				String SFinalDate = getMonthlyMrsFinalDate();
				

				if(common.toInt(SFinalDate)<common.toInt(common.getServerPureDate()))
				{
					JOptionPane.showMessageDialog(null," 7th of Every Month is FinalDate for Monthly Mrs");
					return;
				}

				if(iApplyctr>0)
				{
					try
					{
						trfdOldData();
						setDoneData();
						setTaskPool();
						setOldOrder();

						repool();
					}
					catch(Exception ex)
					{
						return;
					}
				}
				try
				{
					BOk       . setEnabled(true);
					BPrint    . setEnabled(true);
					BInsert   . setEnabled(true);

					if(found())
					{
						JOptionPane.showMessageDialog(null,"Planning  Were Finalized","Information",JOptionPane.INFORMATION_MESSAGE);
						return;
					}

					setVectorData();
					setMrsData();
					setMonthlyConsumption();
					setProjectedConsumption();
					setRowData();
					setTotalValue();
					iApplyctr++;

				}
				catch(Exception ex)
				{
					try
					{
						PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(common.getPrintPath()+"/Raj.prn")));
						ex.printStackTrace(out);
						out.close();
					}
					catch(Exception e){}
				}
			*/
          }
     }
	
	private void setOSOEDate() {
          SStDate   = TopPanel. TStDate . toString();
          SEnDate   = TopPanel. TDate   . toString();

          String SDate = common.parseDate(common.pureDate(SStDate).substring(0,6)+"01");

          SOEnDate  = common.getDate(SDate,1,-1);  // SStDate
          SOStDate  = common.getDate(SOEnDate,180,-1);
          SOStDate  = common.parseDate(common.pureDate(SOStDate).substring(0,6)+"01");

          bMonth    = getMonthFlag(SOStDate);
	}
     
     public void repool()
     {
          String QS = "";
          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();               
               }
               Statement      stat           =  theConnection.createStatement();

               for(int i=0;i<theModel.getRows();i++)
               {
                    //String    SCDays    = (String)theModel.getValueAt(i,12); 17.04.2020
					String    SCDays    = (String)theModel.getValueAt(i,15);
                    //String    SOQty     = (String)theModel.getValueAt(i,8); 17.04.2020
					String    SOQty     = (String)theModel.getValueAt(i,11);
					
                    //String    SO3Qty    = (String)theModel.getValueAt(i,9);17.04.2020
					String    SO3Qty    = (String)theModel.getValueAt(i,12); 
					
                    //String    SOValue   = (String)theModel.getValueAt(i,11); 17.04.2020
					String    SOValue   = (String)theModel.getValueAt(i,14);
					
                              SCDays    = SCDays.trim();
                              SOQty     = common.getRound(SOQty.trim(),3);
                              SO3Qty    = common.getRound(SO3Qty.trim(),3);
                              SOValue   = SOValue.trim();


                         QS = " Update RegPoolTemp Set " ;
                         QS = QS+" CDays  =0"+SCDays+",";
                         QS = QS+" ToOrder=0"+SOQty+",";
                         QS = QS+" To3Order=0"+SO3Qty+",";
                         QS = QS+" OrdVal =0"+SOValue;
                         QS = QS+" Where Id = "+(String)VId.elementAt(i);
					
					if(common.toDouble(SOQty) > 0)
					{
						System.out.println("Updated Id : "+(String)VId.elementAt(i));
					}

                    stat.executeUpdate(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private class AuthenticateList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(theModel.getRows()==0)
                    return;

               repool();

               try
               {

                    Class.forName("oracle.jdbc.OracleDriver");
                    theConnection1 = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");

                    theConnection1  .  setAutoCommit(false);
     
                    savePlan();

                    if(checkBudget())
                    {
     
                         Authenticate();
                         theModel.setNumRows(0);
                         //new MonthMRSConversion(iMillCode,iUserCode,TopPanel.TStDate.toNormal(),SItemTable,theConnection1);
                         theConnection1.commit();
                         theConnection1.setAutoCommit(true);

                         if(bComflag)
                              removeHelpFrame();

                    }
                    else
                    {

                         theConnection1.commit();
                         theConnection1.setAutoCommit(true);
                         theConnection1.close();
                    }

               }
               catch(Exception ex)
               {
                    try
                    {
					theConnection1  . rollback();

                    }catch(Exception e){}
               }
          }

     }
     public void Authenticate()
     {
          try
          {
               bComflag  = true;

               String SPlanQS    = " Select maxNo From MonthlyPlanningConfig where MillCode = "+iMillCode+" and UserCode="+iUserCode+" for update of MaxNo nowait ";

               String SUString   = " Update MonthlyPlanningConfig set MaxNo = ? where  MillCode=? and UserCode=?";

               String SPlanMonth =  TopPanel.TStDate.toNormal().substring(0,6);

               String SInsQS     = " Insert into PoolingCompleted(PoolingMonth,UserCode,MillCode) values("+SPlanMonth+","+iOwnerCode+","+iMillCode+")";

               Statement      stat           =  theConnection1.createStatement();
               PreparedStatement thePrepare  =  theConnection1.prepareStatement(SUString);
               ResultSet      res            =  stat.executeQuery(SPlanQS);
               if(res.next())
               {
                    SServPlanNo=String.valueOf(res.getInt(1)+1);
               }
               stat . execute(" Update RegPlanTemp set AuthStatus=1,AuthUserCode="+iUserCode+",Serv_Plan_No="+SServPlanNo+",AuthDateTime='"+common.getServerDateTime2()+"' Where MillCode="+iMillCode+" and OwnerCode="+iOwnerCode+" and AuthStatus=0 ");
               stat . execute(" Delete From RegPoolTemp Where MillCode="+iMillCode+" and OwnerCode="+iOwnerCode);
               stat . execute(" Delete From TempRegPool Where MillCode="+iMillCode+" and OwnerCode="+iOwnerCode);
               stat . execute(" Insert Into RPHistoryTemp (id,Serv_Plan_No,StDate,EnDate,MillCode,Usercode) Values(RPHistory_SEQ.nextVal,"+SServPlanNo+",'"+TopPanel.TStDate.toString()+"','"+TopPanel.TDate.toString()+"',"+iMillCode+","+iOwnerCode+")");
               stat . execute(SInsQS);                                                                 

               thePrepare.setString(1,SServPlanNo);
               thePrepare.setInt(2,iMillCode);
               thePrepare.setInt(3,iUserCode);

               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();
          }
          catch(Exception ex)
          {                             

               bComflag  = false;
               try
               {
               theConnection1  . rollback();
               }catch(Exception e){}
               System    . out.println(ex);
               ex        . printStackTrace();
          }

          if(bComflag)
          {                        
               try
               {
//                  JOptionPane    . showMessageDialog(null,"Data Authenticated Sucessfully","InforMation",JOptionPane.INFORMATION_MESSAGE);

               }catch(Exception ex)
               {
               }
          }
          else
          {
               try
               {
                    theConnection1  . rollback();
                    JOptionPane    . showMessageDialog(null,"The Given Data is not Saved Please Save Once Again or Problem in Storing Data ","InforMation",JOptionPane.INFORMATION_MESSAGE);
               }
               catch(Exception ex)
               {
               }
          }
     }
     public void setVectorData()
     {
          VMCode       . removeAllElements();
          VMName       . removeAllElements();
          VHodCode     . removeAllElements();
          VHod         . removeAllElements();
          VDeptCode    . removeAllElements();
          VDeptGroupCode.removeAllElements();
          VDeptName    . removeAllElements();

          VGroupCode   . removeAllElements();
          VGroup       . removeAllElements();
          VPCon        . removeAllElements();
          VPDCon       . removeAllElements();
          VStock       . removeAllElements();
          VPending     . removeAllElements();
          VCDays       . removeAllElements();
          VOQty        . removeAllElements();
          VO3Qty       . removeAllElements();
          VLRate       . removeAllElements();
          VValue       . removeAllElements();
          VId          . removeAllElements();

          VPlusOrMinus . removeAllElements();


          try
          {

               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }

               Statement      stat           = theConnection.createStatement();

               String QS = " Select count(*) from RegPoolTemp where "+
                           " (UserCode="+iUserCode+" or UserCode in(Select UserCode from MrsUserAuthentication where AuthUserCode="+iUserCode+"))"+
                           " and MillCode="+iMillCode+
                           " and Plan_Date="+TopPanel.TStDate.toNormal();

               ResultSet      res            = stat.executeQuery(QS);


               if(res.next())
               {
                    iCount= res.getInt(1);
               }

               res            = stat.executeQuery(getQString());
               
               while(res.next())
               {
                    VHod           . addElement(res.getString(1));
                    VMCode         . addElement(res.getString(2));
                    VMName         . addElement(res.getString(3));
                    VHodCode       . addElement(res.getString(4));
                    VPCon          . addElement(res.getString(5));
                    VPDCon         . addElement(res.getString(6));
                    VStock         . addElement(res.getString(7));
                    VPending       . addElement(res.getString(8));
                    VCDays         . addElement(res.getString(9));
                    VOQty          . addElement(res.getString(10));
                    VLRate         . addElement(res.getString(11));
                    VValue         . addElement(res.getString(12));
                    VId            . addElement(res.getString(13));
                    VGroup         . addElement(res.getString(14));
                    VGroupCode     . addElement(res.getString(15));
                    VO3Qty         . addElement(res.getString(16));
                    VPlusOrMinus   . addElement(res.getString(17));
                    VDeptCode      . addElement(res.getString(18));
                    VDeptName      . addElement(res.getString(19));
                    VDeptGroupCode . addElement(res.getString(20));

               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
          }
     }
     
     public void setRowData()
     {

          theModel.setNumRows(0);

          int  iForMonth = common.toInt(TopPanel.TStDate.TMonth.getText());
          int  iSig      = (iForMonth > 3 && iForMonth < 10?1:0);

          for(int i=0;i<VId.size();i++)
          {
               Vector theVect = new Vector();

/*             if(common.toDouble((String)VOQty.elementAt(i))<=0 && TopPanel . COnlyOrderd.isSelected())
                         continue;  */
					 
					 
				setItemStock(common.parseNull((String)VMCode    . elementAt(i)));
					 
				double dMrsPending = getMrsPending(common.parseNull((String)VMCode    . elementAt(i)));

               theVect.addElement(common.parseNull((String)VDeptName . elementAt(i)));
               theVect.addElement(common.parseNull((String)VMCode    . elementAt(i)));
               theVect.addElement(common.parseNull((String)VMName    . elementAt(i)));
               
               theVect.addElement(common.parseNull((String)VPCon     . elementAt(i)));
               theVect.addElement(common.parseNull((String)VPDCon    . elementAt(i)));
               theVect.addElement(common.parseNull((String)VProjCon  . elementAt(i)));
               theVect.addElement(common.parseNull((String)VStock    . elementAt(i)));
			   
			   theVect.addElement(String.valueOf(dServiceStock));
			   theVect.addElement(String.valueOf(dNonStock));
			   theVect.addElement(String.valueOf(dOtherStock));
			   
			   if(dMrsPending>0) {
			   }
			   
			   double dPending = dMrsPending + common.toDouble((String)VPending  . elementAt(i));
			   
			   theVect.addElement(String.valueOf(dPending));
			   
               //theVect.addElement(common.parseNull((String)VPending  . elementAt(i)));
			   
			   
               theVect.addElement(common.parseNull((String)VOQty.elementAt(i)));
               theVect.addElement(common.parseNull((String)VO3Qty.elementAt(i)));
               theVect.addElement(common.getRound((String)VLRate.elementAt(i),4));
               theVect.addElement(common.parseNull((String)VValue    . elementAt(i)));
               theVect.addElement(common.parseNull((String)VCDays    . elementAt(i)));
               theVect.addElement(common.parseNull((String)VGroup    . elementAt(i)));

               theModel.appendRow(theVect);
          }
     }
	 
	 private double getMrsPending(String SItemCode) {
		 
		 double dQty=0;
		 
		 for(int i=0;i<VMrsCode.size();i++) {
			 
			if( ((String)VMrsCode.elementAt(i)).equals(SItemCode)) {
				
				dQty += common.toDouble((String)VMrsQty.elementAt(i));
			}
		 }
		 
	     return dQty;		
	 }

     public void setDoneData()
     {
          VMCode         . removeAllElements();
          VMName         . removeAllElements();
          VHodCode       . removeAllElements();
          VHod           . removeAllElements();
          VGroupCode     . removeAllElements();
          VGroup         . removeAllElements();
          VPCon          . removeAllElements();
          VPDCon         . removeAllElements();
          VStock         . removeAllElements();
          VPending       . removeAllElements();
          VCDays         . removeAllElements();
          VOQty          . removeAllElements();
          VO3Qty         . removeAllElements();
          VLRate         . removeAllElements();
          VValue         . removeAllElements();
//           VId            . removeAllElements();
          VPlusOrMinus   . removeAllElements();
          
          String QS0     = "";
          String QS1     = "";
          String QS2     = "";
          String QS3     = "";
          String QS4     = "";
          String QS5     = "";
          String QS6     = "";
          String QS7     = "";
          String QS      = "";
          
          QS0 =     " create or replace view Temp0"+iUserCode+" as "+    //
                    " (select item_code,orderdate,max(rate) as lastrate from (select item_code,"+
                    " orderdate,net/qty as rate from purchaseorder"+
                    " inner join (select item_code as code,max(orderdate) as orddate from purchaseorder"+
                    " where PurchaseOrder.Net> 0 And PurchaseOrder.Qty > 0"+
                    " And PurchaseOrder.MillCode="+iMillCode+
                    " group by item_code) on code = purchaseorder.item_code and orddate=purchaseorder.orderdate"+
                    " where millcode="+iMillCode+" and net>0 and qty>0)"+
                    " group by item_code,orderdate"+
                    " Union all"+                   
                    " select item_code,orderdate,max(rate) as lastrate from (select item_code,"+
                    " orderdate,net/qty as rate from PYOrder"+
                    " inner join (select item_code as code,max(orderdate) as orddate from PYOrder"+
                    " where PYOrder.Net> 0 And PYOrder.Qty > 0"+
                    " And PYOrder.MillCode="+iMillCode+
                    " group by item_code) on code = PYOrder.item_code and orddate = PYOrder.orderdate"+
                    " where millcode="+iMillCode+" and net>0 and qty>0)"+
                    " group by item_code,orderdate)";

          QS1 =     " create or replace view Temp1"+iUserCode+" as "+    //
                    " (Select Code,sum(ConQty) as ConQty from "+
                    " (SELECT Issue.Code, Sum(Issue.Qty) AS ConQty "+
                    " FROM MonthlyPlanningItem INNER JOIN Issue ON MonthlyPlanningItem.Item_Code = Issue.Code "+
                    " and MonthlyPlanningItem.UserCode="+iOwnerCode+"  and MonthlyPlanningItem.MillCode="+iMillCode+
                    " WHERE Issue.IssueDate>='"+common.pureDate(SOStDate)+"' And Issue.IssueDate<'"+TopPanel.TStDate.toNormal()+"' And Issue.MillCode="+iMillCode+
                    " GROUP BY Issue.Code";

                    if(bMonth)
                    {
                         QS1 = QS1 + " Union All "+
                               " SELECT PYIssue.Code, Sum(PYIssue.Qty) AS ConQty "+
                               " FROM MonthlyPlanningItem INNER JOIN PYIssue ON MonthlyPlanningItem.Item_Code = PYIssue.Code "+
                               " and MonthlyPlanningItem.UserCode="+iOwnerCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                               " WHERE PYIssue.IssueDate>='"+common.pureDate(SOStDate)+"' And PYIssue.IssueDate<'"+TopPanel.TStDate.toNormal()+"' And PYIssue.MillCode="+iMillCode+
                               " GROUP BY PYIssue.Code";
                    }
                    QS1 = QS1 + ") "+
                          " Group by Code) ";

          QS7 =     " create or replace view Temp7"+iUserCode+" as  "+   //
                    " (Select Code,Sum(ConQty) as ConQty From "+
                    " (SELECT GRN.Code as Code, Sum(GRN.GrnQty) AS ConQty  FROM GRN "+
                    " INNER JOIN MonthlyPlanningItem ON GRN.Code=MonthlyPlanningItem.Item_Code "+
                    " and MonthlyPlanningItem.UserCode="+iOwnerCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                    " WHERE (Grn.GrnBlock > 1 and Grn.GrnType<>2) "+
                    " AND Grn.GrnDate>='"+common.pureDate(SOStDate)+"'"+
                    " And Grn.GrnDate<'"+TopPanel.TStDate.toNormal()+"'"+
                    " And GRN.MillCode="+iMillCode+
                    " GROUP BY GRN.Code";

                    if(bMonth)
                    {
                         QS7 = QS7 + " Union All "+
                               " SELECT PYGRN.Code as Code, Sum(PYGRN.GrnQty) AS ConQty  FROM PYGRN "+
                               " INNER JOIN MonthlyPlanningItem ON PYGRN.Code=MonthlyPlanningItem.Item_Code "+
                               " and MonthlyPlanningItem.UserCode="+iOwnerCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                               " WHERE (PYGrn.GrnBlock > 1 and PYGrn.GrnType<>2) "+
                               " AND PYGrn.GrnDate>='"+common.pureDate(SOStDate)+"'"+
                               " And PYGrn.GrnDate<'"+TopPanel.TStDate.toNormal()+"'"+
                               " And PYGRN.MillCode="+iMillCode+
                               " GROUP BY PYGRN.Code ";
                    }
                    QS7 = QS7 + ") "+
                          " Group by Code) ";


          QS2 =     " create or replace view Temp2"+iUserCode+" as "+    //
                    " (SELECT PurchaseOrder.Item_Code, Sum(PurchaseOrder.Qty-PurchaseOrder.InvQty) AS Pending "+
                    " FROM MonthlyPlanningItem INNER JOIN PurchaseOrder ON MonthlyPlanningItem.Item_Code=PurchaseOrder.Item_Code "+
                    " and MonthlyPlanningItem.UserCode="+iOwnerCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                    " and PurchaseOrder.Usercode="+iOwnerCode+
                    " Where PurchaseOrder.MillCode="+iMillCode+" and PurchaseOrder.Qty>PurchaseOrder.InvQty "+
                    " GROUP BY PurchaseOrder.Item_Code)";

          QS3 =     " create or replace view Temp3"+iUserCode+" as "+    //
                    " (SELECT Issue.Code, Sum(Issue.Qty) AS IssueQty "+
                    " FROM Issue INNER JOIN MonthlyPlanningItem ON Issue.Code = MonthlyPlanningItem.Item_Code "+
                    " and MonthlyPlanningItem.UserCode="+iOwnerCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                    " Where Issue.IssueDate<'"+TopPanel.TStDate.toNormal()+"' And Issue.MillCode="+iMillCode+
                    " GROUP BY Issue.Code) ";

          QS4 =     " create or replace view temp4"+iUserCode+" as  "+  //
                    " SELECT GRN.Code as Code, Sum(GRN.GrnQty) AS RecQty  FROM GRN "+
                    " INNER JOIN MonthlyPlanningItem ON GRN.Code=MonthlyPlanningItem.Item_Code "+
                    " and MonthlyPlanningItem.UserCode="+iOwnerCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                    " Where Grn.GrnDate<'"+TopPanel.TStDate.toNormal()+"' And GRN.MillCode="+iMillCode+
                    " GROUP BY GRN.Code";

          QS5 =     " create or replace view Temp5"+iUserCode+" as "+    //
                    " (SELECT MonthlyPlanningItem.Item_Code, max(Temp0"+iUserCode+".LastRate) AS LastRate "+
                    " FROM MonthlyPlanningItem INNER JOIN Temp0"+iUserCode+" ON MonthlyPlanningItem.Item_Code = Temp0"+iUserCode+".Item_Code "+
                    " and MonthlyPlanningItem.UserCode="+iOwnerCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                    " GROUP BY MonthlyPlanningItem.Item_Code) ";


          QS6 =     " Create or replace view Temp6"+iUserCode+" as (  "+ //
                    " SELECT GRN.Code as Code, Sum(GRN.GrnQty) AS IssueQty  FROM GRN "+
                    " INNER JOIN MonthlyPlanningItem ON GRN.Code=MonthlyPlanningItem.Item_Code "+
                    " and MonthlyPlanningItem.UserCode="+iOwnerCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                    " Where (Grn.GrnBlock > 1 and Grn.GrnType<>2)  "+
                    " And Grn.GrnDate < '"+TopPanel.TStDate.toNormal()+"' And GRN.MillCode="+iMillCode+
                    " GROUP BY GRN.Code)";


               QS  =     " SELECT MonthlyPlanningItem.Item_Code, MonthlyPlanningItem.Item_Name, "+
                         " Temp1"+iUserCode+".ConQty, Temp2"+iUserCode+".Pending, Temp3"+iUserCode+".IssueQty, "+
                         " Temp4"+iUserCode+".RecQty, Temp5"+iUserCode+".LastRate, "+SItemTable+".OPGQTY, "+
                         " "+SItemTable+".HodCode,Temp7"+iUserCode+".ConQty,Temp6"+iUserCode+".IssueQty,"+
                         " MonthlyPlanningItem.MatGroupCode,MonthlyPlanningItem.PlusOrMinusPer,Dept.Dept_Code,Dept.Dept_Name,getTotalStockNew(MonthlyPlanningItem.Item_Code,"+iMillCode+","+iOwnerCode+") ,Dept.GroupCode"+
                         " FROM (((((((MonthlyPlanningItem Inner join "+SItemTable+" on"+
                         " "+SItemTable+".Item_Code=MonthlyPlanningItem.Item_Code)"+
                         " LEFT JOIN Temp1"+iUserCode+" ON MonthlyPlanningItem.Item_Code = Temp1"+iUserCode+".Code) "+
                         " LEFT JOIN Temp2"+iUserCode+" ON MonthlyPlanningItem.Item_Code = Temp2"+iUserCode+".Item_Code) "+
                         " LEFT JOIN Temp3"+iUserCode+" ON MonthlyPlanningItem.Item_Code = Temp3"+iUserCode+".Code) "+
                         " LEFT JOIN Temp4"+iUserCode+" ON MonthlyPlanningItem.Item_Code = Temp4"+iUserCode+".Code) "+
                         " LEFT JOIN Temp5"+iUserCode+" ON MonthlyPlanningItem.Item_Code = Temp5"+iUserCode+".Item_Code) "+
                         " LEFT JOIN Temp6"+iUserCode+" ON MonthlyPlanningItem.Item_Code = Temp6"+iUserCode+".Code) "+
                         " LEFT JOIN Temp7"+iUserCode+" ON MonthlyPlanningItem.Item_Code = Temp7"+iUserCode+".Code "+
                         " Inner join Dept on Dept.Dept_Code=MonthlyPlanningItem.ItemDeptCode"+
                         " and MonthlyPlanningItem.UserCode="+iOwnerCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                         " and MonthlyPlanningItem.ItemDeptCode in(Select distinct DeptCode from Budget where Budget>0  and UserCode="+iOwnerCode+
                         " and to_Char(sysdate,'YYYYMMDD')>=WithEffectFrom"+
                         " and to_char(Sysdate,'YYYYMMDD')<=WithEffectTo"+
                         " and MillCode="+iMillCode+")"+
                         " Left join ItemStock on ItemStock.itemCode=MonthlyPlanningItem.Item_Code"+
                         " and ItemStock.HodCode="+iOwnerCode+" and ItemStock.MillCode="+iMillCode+
                         " WHERE MonthlyPlanningItem.UserCode="+iOwnerCode+
                         " and MonthlyPlanningItem.MillCode="+iMillCode;


          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }

               Statement      stat           =  theConnection.createStatement();


/*             try{stat.execute("Drop Table Temp0"+iUserCode);}catch(Exception ex){}
               try{stat.execute("Drop Table Temp1"+iUserCode);}catch(Exception ex){}
               try{stat.execute("Drop Table Temp2"+iUserCode);}catch(Exception ex){}
               try{stat.execute("Drop Table Temp3"+iUserCode);}catch(Exception ex){}
               try{stat.execute("Drop Table Temp4"+iUserCode);}catch(Exception ex){}
               try{stat.execute("Drop Table Temp5"+iUserCode);}catch(Exception ex){}
               try{stat.execute("Drop Table Temp6"+iUserCode);}catch(Exception ex){}
               try{stat.execute("Drop Table Temp7"+iUserCode);}catch(Exception ex){} */


               stat.execute(QS0);
               stat.execute(QS1);
               stat.execute(QS2);
               stat.execute(QS3);
               stat.execute(QS4);
               stat.execute(QS5);
               stat.execute(QS6);
               stat.execute(QS7);

               ResultSet  res  = stat.executeQuery(QS);

               while(res.next())
               {
                    VMCode    . addElement(res.getString(1));
                    VMName    . addElement(res.getString(2));

                    double    dVal1     = common.toDouble(res.getString(3));
                    double    dVal2     = common.toDouble(res.getString(10));
                    double    dPCon     = dVal1+dVal2;
                    double    dPDCon    = common.toDouble(common.getRound(dPCon/180,2));

                    VPCon     . addElement(common.getRound(dPCon,2));
                    VPDCon    . addElement(common.getRound(dPDCon,2));

                    double    dOpQty    = common.toDouble(res.getString(8));
                    double    dRQty     = common.toDouble(res.getString(6));
                    double    dIQty     = common.toDouble(res.getString(5));

                              dIQty     = dIQty+common.toDouble(res.getString(11));

                    double    dSQty     = dOpQty + dRQty - dIQty;
                    double    dPQty     = common.toDouble(res.getString(4));
                    double    dLRate    = common.toDouble(res.getString(7));
                    double    dOQty     = (dPDCon*60)-dSQty-dPQty;
                    double    dOVal     = 0;             // dOQty*dLRate;

                    VStock    . addElement(res.getString(16));

                    //by GSK 20040305

                              dPQty     = (dPQty <= 0 ? 0:dPQty);


                    VPending  . addElement(""+dPQty);
                    VCDays    . addElement("60");

                              dOQty     = (dOQty <= 0 ? 0:dOQty);
                              dOVal     = (dOQty <= 0 ? 0:dOVal);

                    VOQty     . addElement("");
                    VO3Qty    . addElement("");
                    VLRate    . addElement(""+dLRate);
                    VValue    . addElement(""+dOVal);
                    VHodCode  . addElement(res.getString(9));
                    VGroupCode. addElement(res.getString(12));
                    VPlusOrMinus. addElement(res.getString(13));

                    VDeptCode.addElement(res.getString(14));
                    VDeptName.addElement(res.getString(15));
                    VDeptGroupCode.addElement(res.getString(17));


               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
     
     public void setTaskPool()
     {
          String QS = " Insert into RegPoolTemp(id,Item_Code,HodCode,Group_Code,ConQty,PerDayCon,Stock,Pending,CDays,ToOrder,To3Order,Rate,OrdVal,Plan_Date,MillCode,UserCode,OwnerCode,Dept_Code) Values (";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }

               Statement      stat           =  theConnection.createStatement();

               for(int i=0;i<VMCode.size();i++)
               {
                    String    QS1 = QS+ "REGPOOLTemp_SEQ.nextVal,";
                              QS1 = QS1+"'"+(String)VMCode.elementAt(i)+"',";
                              QS1 = QS1+"0"+common.toInt((String)VHodCode.elementAt(i))+",";
                              QS1 = QS1+"0"+common.toInt((String)VGroupCode.elementAt(i))+",";
                              QS1 = QS1+"0"+(String)VPCon.elementAt(i)+",";
                              QS1 = QS1+"0"+(String)VPDCon.elementAt(i)+",";
                              QS1 = QS1+"0"+(String)VStock.elementAt(i)+",";
                              QS1 = QS1+"0"+(String)VPending.elementAt(i)+",";
                              QS1 = QS1+"0"+common.toInt((String)VCDays.elementAt(i))+",";
                              QS1 = QS1+"0"+(String)VOQty.elementAt(i)+",";
                              QS1 = QS1+"0"+(String)VO3Qty.elementAt(i)+",";
                              QS1 = QS1+"0"+(String)VLRate.elementAt(i)+",";
                              QS1 = QS1+"0"+(String)VValue.elementAt(i)+",";
                              QS1 = QS1+"'"+(String)TopPanel.TStDate.toNormal()+"',";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"0"+iOwnerCode+",";
                              QS1 = QS1+"0"+common.parseNull((String)VDeptCode.elementAt(i))+")";


                    stat.execute(QS1);
               }
               stat.execute("Delete From TPHistoryTemp Where MillCode="+iMillCode+" and UserCode="+iOwnerCode);
               QS = "Insert Into TPHistoryTemp (id,StDate,EnDate,MillCode,UserCode) Values( TPHistory_SEQ.nextVal,"+"'"+TopPanel.TStDate.toString()+"','"+TopPanel.TDate.toString()+"',"+iMillCode+","+iOwnerCode+")";
               stat . execute(QS);
               stat . close();
          }
          catch(Exception ex)
          {
//               ex.printStackTrace();

          }
     }
     public String getQString()
     {


          String QS =    " Select decode(Dept.Dept_Name,null,'',Dept.Dept_Name) as  HodName,PlanPoolTemp.Item_Code, "+
                         " MonthlyPlanningItem.Item_Name,PlanPoolTemp.HodCode, "+
                         " PlanPoolTemp.ConQty,PlanPoolTemp.PerDayCon, "+
                         " PlanPoolTemp.Stock,PlanPoolTemp.Pending, "+
                         " PlanPoolTemp.CDays,decode(PlanPoolTemp.ToOrder,0,'',PlanPoolTemp.ToOrder) ,"+
                         " PlanPoolTemp.Rate,Round((PlanPoolTemp.TOOrder*PlanPoolTemp.Rate),0), "+
                         " PlanPoolTemp.Id,MatGroup.GroupName, "+
                         " PlanPoolTemp.Group_Code,decode(PlanPoolTemp.To3Order,0,'',PlanPoolTemp.To3Order),MonthlyPlanningItem.PlusOrMinusPer,"+
                         " MonthlyPlanningItem.ItemDeptCode,Dept.Dept_Name,Dept.GroupCode"+
                         " From (PlanPoolTemp  "+
                         " Inner Join MonthlyPlanningItem on MonthlyPlanningItem.Item_Code = PlanPoolTemp.Item_Code "+
                         " and MonthlyPlanningItem.UserCode="+iOwnerCode+" and MonthlyPlanningItem.MillCode="+iMillCode+")"+
                         " Inner join Dept on Dept.Dept_Code=MonthlyPlanningItem.ItemDeptCode"+
                         " and MonthlyPlanningItem.UserCode="+iOwnerCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                         " Inner Join MatGroup on MatGroup.GroupCode = PlanPoolTemp.Group_Code "+
                         " Where PlanPoolTemp.MillCode="+iMillCode+
                         " and OwnerCode="+iOwnerCode+

					// Date Added..
					" And PlanPoolTemp.PLAN_DATE >= '"+TopPanel.TStDate.toNormal()+"' "+
					" And PlanPoolTemp.PLAN_DATE <= '"+TopPanel.TStDate.toNormal()+"' ";
          

          String SHave   = "",str  = "";

          if(TopPanel.JRSeleGroup.isSelected())
          {
               str  = "PlanPoolTemp.Group_Code = "+(String)TopPanel.VGroupCode.elementAt(TopPanel.JCGroup.getSelectedIndex());
               SHave= SHave+" and "+str;
          }
          QS = QS+" "+SHave;

          String SOrder  = (TopPanel.TSort.getText()).trim();

          if(SOrder.length()>0)
               QS = QS+" Order By "+TopPanel.TSort.getText()+",5";
          else
               QS = QS+" Order By 1,14,3 ";
		
          return QS;
     }



     public class SaveList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {

               repool();

/*             if(!validData())
                    return; */

               try
               {
                    Class.forName("oracle.jdbc.OracleDriver");
                    theConnection1 = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
                    theConnection1  .  setAutoCommit(false);
                    savePlan();
                    theConnection1  . setAutoCommit(true);
                    theConnection1  . close();
                    checkBudget();
               }
               catch(Exception ex)
               {
                    try
                    {
                    theConnection1  . rollback();

                    }catch(Exception e){}

               }
               if(bComflag)
                    removeHelpFrame();
               else
                    BOk            . setEnabled(true);

          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer . remove(this);
               Layer . repaint();
               Layer . updateUI();
          }
          catch(Exception ex) { }
     }

     public void savePlan()
     {

          String QS =    " Insert Into RegPlanTemp(id,Item_Code,HodCode,Group_Code,ConQty,PerDayCon,Stock,Pending,CDays,ToOrder,To3Order,Rate,OrdVal,MillCode,UserCode,OwnerCode,Dept_Code) "+
                         " Select RegPlanTemp_seq.nextVal,Item_Code,HodCode,Group_Code,ConQty,PerDayCon,Stock,Pending,CDays,ToOrder,To3Order,Rate,OrdVal,MillCode,UserCode,Ownercode,Dept_Code From RegPoolTemp "+
                         " Where RegPoolTemp.MillCode="+iMillCode+
                         " and OwnerCode="+iOwnerCode+
                         " and TOOrder>0";


          String SDelQS = " Delete from RegPlanTemp where MillCode="+iMillCode+
                          "  and OwnerCode="+iOwnerCode+
                          "  and Plan_Date="+TopPanel.TStDate.toNormal()+
                          " and AuthStatus=0";


          try
          {
               Statement      stat           =  theConnection1.createStatement();

               stat . execute(SDelQS);
               stat . execute(QS);
               stat . executeUpdate("Update RegPlanTemp Set Plan_Date='"+TopPanel.TStDate.toNormal()+"' Where AuthStatus=0 and millcode ="+iMillCode+" and OwnerCode="+iOwnerCode);
//               stat . execute("Delete From TPHistoryTemp Where MillCode="+iMillCode+" and UserCode="+iOwnerCode);
               stat . close();
          }
          catch(Exception ex)
          {

               try
               {
                    theConnection1  . rollback();
               }catch(Exception e){}

               bComflag  = false;
               System    . out.println(ex);
               ex        . printStackTrace();
          }

          if(bComflag)
          {
               try
               {
                    if(iAuthStatus==0)
                       JOptionPane    . showMessageDialog(null,"Data Saved Successfully ","InforMation",JOptionPane.INFORMATION_MESSAGE);
               }
			catch(Exception ex)
               {
               }
          }
          else
          {
               try
               {
                    theConnection1  . rollback();
                    JOptionPane    . showMessageDialog(null,"The Given Data is not Saved Please Enter Correct data","InforMation",JOptionPane.INFORMATION_MESSAGE);
               }
               catch(Exception ex)
               {
               }
          }
     }

     private String getPYStDate()
     {
          String SDate  = "";
          int iMonth       = common.toInt(TopPanel.TStDate.TMonth.getText())+6;

          iMonth           = iMonth > 12?iMonth-12:iMonth;
          
          int iPYear       = common.toInt((SFStDate).substring(0,4))-1;

          iPYear           = iMonth >= 1 && iMonth <=3?iPYear+1:iPYear;
          
          SDate = String.valueOf(iPYear)+(iMonth < 10 ?"0"+iMonth:String.valueOf(iMonth))+"01";

          return SDate;
     }

     private String getPYEnDate()
     {
          String SDate   = "";
          SDate = (SFEnDate).substring(0,4)+"0331";
          return SDate;
     }

     private void setMonthlyConsumption()
     {
          try
          {
               String    SPYStDate = getPYStDate();
               String    SPYEnDate = getPYEnDate();
               int       iForMonth = common.toInt(TopPanel.TStDate.TMonth.getText());
               int       iSig      = (iForMonth > 3 && iForMonth < 10?1:0);
               
               VMonthPlanClass     = new Vector();
               MonthPlanClass MPC  = new MonthPlanClass();
               
               String QS8 = "";

               QS8 =     " Select Code,Sum(ConQty) as ConQty,SubGrnDate from ( "+
                         " SELECT Issue.Code, Sum(Issue.Qty) AS ConQty, substr(Issue.IssueDate,5,2) as SubGrnDate "+
                         " FROM MonthlyPlanningItem "+
                         " INNER JOIN Issue ON MonthlyPlanningItem.Item_Code = Issue.Code "+
                         " and MonthlyPlanningItem.UserCode="+iUserCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                         " Where Issue.MillCode="+iMillCode+
                         " and Issue.IssueDate>='"+common.pureDate(SOStDate)+"'"+
                         " GROUP BY Issue.Code, substr(Issue.IssueDate,5,2) "+
                         " Union All "+
                         " SELECT GRN.Code as Code, Sum(GRN.GrnQty) AS ConQty, substr(Grn.GrnDate,5,2) as SubGrnDate "+
                         " FROM GRN "+
                         " INNER JOIN MonthlyPlanningItem ON GRN.Code=MonthlyPlanningItem.Item_Code "+
                         " and MonthlyPlanningItem.UserCode="+iUserCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                         " WHERE (Grn.GrnBlock > 1 and Grn.GrnType<>2) "+
                         " And Grn.MillCode="+iMillCode+" and Grn.GrnDate>='"+common.pureDate(SOStDate)+"'"+
                         " GROUP BY GRN.Code, substr(Grn.GrnDate,5,2) ";

                         if(bMonth)
                         {
                              QS8 = QS8 + " Union All "+
                                    " SELECT PYIssue.Code, Sum(PYIssue.Qty) AS ConQty, substr(PYIssue.IssueDate,5,2) as SubGrnDate "+
                                    " FROM MonthlyPlanningItem "+
                                    " INNER JOIN PYIssue ON MonthlyPlanningItem.Item_Code = PYIssue.Code "+
                                    " and MonthlyPlanningItem.UserCode="+iUserCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                                    " Where PYIssue.MillCode="+iMillCode+
                                    " and PYIssue.IssueDate>='"+common.pureDate(SOStDate)+"'"+
                                    " GROUP BY PYIssue.Code, substr(PYIssue.IssueDate,5,2) "+
                                    " Union All "+
                                    " SELECT PYGRN.Code as Code, Sum(PYGRN.GrnQty) AS ConQty, substr(PYGrn.GrnDate,5,2) as SubGrnDate "+
                                    " FROM PYGRN "+
                                    " INNER JOIN MonthlyPlanningItem ON PYGRN.Code=MonthlyPlanningItem.Item_Code "+
                                    " and MonthlyPlanningItem.UserCode="+iUserCode+" and MonthlyPlanningItem.MillCode="+iMillCode+
                                    " WHERE (PYGrn.GrnBlock > 1 and PYGrn.GrnType<>2)"+
                                    " And PYGrn.MillCode="+iMillCode+" and PYGrn.GrnDate>='"+common.pureDate(SOStDate)+"'"+
                                    " GROUP BY PYGRN.Code, substr(PYGrn.GrnDate,5,2) ";
                         }

                         QS8 = QS8 + " ) "+
                               " Group by Code,SubGrnDate "+
                               " Order by 1 ";
						 
               try
               {
                    if(theConnection == null)
                    {
                         ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                        theConnection  =  oraConnection.getConnection();
                    }
                    Statement      stat           =  theConnection.createStatement();
     
                    ResultSet result = stat.executeQuery(QS8);
                    String    SPCode = "";
                    
                    VTemp0    = new Vector();
                    VTemp1    = new Vector();
                    
                    while(result.next())
                    {
                         String    SCode     = result.getString(1);
                         double    dQty      = result.getDouble(2);
                         int       iMonth    = result.getInt(3);
     
                         if (!SPCode.equals(SCode))
                         {
                              VMonthPlanClass     . addElement(MPC);
                              MPC                 = new MonthPlanClass(SCode,iForMonth);
                              SPCode              = SCode;
                         }
                         MPC       . append(dQty,iMonth);
     
                         VTemp0    . add(SCode);
                         VTemp1    . add(String.valueOf(dQty));
                    }
                    result.close();
                    stat           . close();
                    VMonthPlanClass. addElement(MPC);
                    VMonthPlanClass. removeElementAt(0);
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
          }catch(Exception ex)
          {
               ex        . printStackTrace();
               System    . out     . println(ex);
          }
     }
     
     private void setProjectedConsumption()
     {
          VProjCon = new Vector();
          for(int i=0;i<VMCode.size();i++)
               VProjCon  . addElement("0");

          for(int i=0;i<VMonthPlanClass.size();i++)
          {
               MonthPlanClass MPC  = (MonthPlanClass)VMonthPlanClass.elementAt(i);
               double         dProj= common.getProjection(MPC.getSortedQty());
               int index = VMCode.indexOf(MPC.SCode);
               if(index > -1)
                    VProjCon  . setElementAt(common.getRound(dProj,2),index);
          }
     }


     private class InsertList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               //new ItemPlanList(iMillCode,iOwnerCode,SItemTable);
               trfdOldData();
               setDoneData();
               setTaskPool();
               setOldOrder();
          }
     }

     private void trfdOldData()
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }
               Statement           stat           = theConnection.createStatement();

               PreparedStatement   psmt           = theConnection.prepareStatement("Update regpoolTemp set toOrder = ?,to3Order = ? where item_code = ? and MillCode = ? and OwnerCode =?"); 

               try{stat.execute("Delete from TempRegPool Where MillCode="+iMillCode+" and OwnerCode="+iOwnerCode);}catch(Exception e){} 

               for(int i=0;i<theModel.getRows();i++)
               {
                    //psmt .setDouble(1,common.toDouble((String)theModel.getValueAt(i,8))); 17.04.2020
					
					psmt .setDouble(1,common.toDouble((String)theModel.getValueAt(i,11)));
					
                    //psmt .setDouble(2,common.toDouble((String)theModel.getValueAt(i,9)));17.04.2020
					psmt .setDouble(2,common.toDouble((String)theModel.getValueAt(i,12)));
					
                    psmt .setString(3,(String)theModel.getValueAt(i,1));
                    psmt .setString(4,String.valueOf(iMillCode));
                    psmt .setString(5,String.valueOf(iOwnerCode));
                    psmt .execute();
               }
               psmt.close();

               stat.execute("Insert Into TempRegPool (Select * From RegPoolTemp Where MillCode="+iMillCode+" and OwnerCode="+iOwnerCode+")");
               stat.execute("Delete From RegPoolTemp Where MillCode="+iMillCode+" and OwnerCode="+iOwnerCode);
               stat.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();

          }
     }

     private void setOldOrder()
     {
          String SQuery =" Update RegPoolTemp Set"+
                         " ToOrder = (select ToOrder from TempRegPool where item_code  = RegPoolTemp.item_code and millcode ="+iMillCode+" and OwnerCode="+iOwnerCode+"),"+
                         " To3Order= (select To3Order from TempRegPool where TempRegPool.item_code = RegPoolTemp.item_code and millcode ="+iMillCode+" and OwnerCode="+iOwnerCode+")"+
                         " Where MillCode = "+iMillCode+
                         " and OwnerCode="+iOwnerCode;


          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }

               Statement      stat           = theConnection.createStatement();


                              stat           . execute(SQuery);
                              stat           . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();

          }
     }

     public void checkAuthenticator()
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }
               Statement      stat           =  theConnection.createStatement();
               ResultSet      res            =  stat.executeQuery(" select UserCode from MrsUserAuthentication where AuthUserCode="+iUserCode+" and UserCode not in(select UserCode from OtherDeptAuthentication)");
               if(res.next())
               {
                    BAuthenticate.setEnabled(true);

                    TopPanel  .JRAllGroup.setSelected(true);
                    TopPanel  .JRSeleGroup.setSelected(false);
                    TopPanel  .JRSeleGroup.setEnabled(false);
                    TopPanel  .JCGroup.setEnabled(false);
                    iAuthStatus = 1;
               }
               res.close();
               res            =  stat.executeQuery(" select AuthUserCode from MrsUserAuthentication where UserCode="+iUserCode);
               if(res.next())
                iOwnerCode = res.getInt(1);

               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
	
     private void setUserAndOwnerCode()
     {
		iUserCode = 0;
		iOwnerCode = 0;
		
          try
          {
			String SUserName = common.parseNull((String)TopPanel.JCHOD.getSelectedItem());
			
			int iUserCode = common.toInt((String)TopPanel.VHodCode.elementAt(TopPanel.VHod.indexOf(SUserName)));
			
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }
               Statement      stat           =  theConnection.createStatement();
               ResultSet      res            =  null;
               res            =  stat.executeQuery(" select AuthUserCode from MrsUserAuthentication where UserCode="+iUserCode);
               if(res.next())
                iOwnerCode = res.getInt(1);

               res.close();
               stat.close();
			
			System.out.println("User & Owner Code : "+iUserCode+", "+iOwnerCode);
          }
          catch(Exception ex)
          {
              ex.printStackTrace();
          }
     }
	
     private class TableKeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               JTable theTable = (JTable)ke.getSource();
               int iRow = theTable.getSelectedRow();
               int iCol = theTable.getSelectedColumn();
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(iCol==2)
                    {
                         if(ke.getKeyCode()==8)
                         {
                              findstr=findstr.substring(0,(findstr.length()-1));
                              JTName.setText(findstr);
                              setCursor(theTable,iCol);
                         }
                         else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar>='0' && lastchar <= '9'))
                         {
                              findstr=findstr+lastchar;
                              JTName.setText(findstr);
                              setCursor(theTable,iCol);
                         }
                    }
                    //if(iCol==8)
						if(iCol==11)
                    {
                         setTotalValue();
                    }

/*                  if(ke.getKeyCode()==KeyEvent.VK_F3)
                    {
                         
                         if(iCol==0)
                         {
                             new NewDepartmentSearch(Layer,VBudgetDeptName,VBudgetDeptCode,theModel,iRow,iCol,"Department");
                         }
                    }
*/

               }
               catch(Exception ex){}
          }
          public void keyPressed(KeyEvent ke)
          {
               if (ke.getKeyCode()==KeyEvent.VK_ESCAPE)
               {
                    findstr="";
               }
          }
     }

     private void setCursor(JTable theTable,int iCol)
     {
          RegularPlanViewModel theModel = (RegularPlanViewModel) theTable.getModel();
          int index = -1;
          for(int i=0;i<theModel.getRows();i++)
          {
               String str = (String)theModel.getValueAt(i,iCol);
               if(str.startsWith(findstr))
               {
                    index = i;
                    break;
               }
          }
          if(index == -1)
          {
               JTName.setBackground(Color.RED);
               return;
          }
          else
               JTName.setBackground(Color.WHITE);


          theTable.setRowSelectionInterval(index,index);
          Rectangle rect = theTable.getCellRect(index,0,true);
          theTable.scrollRectToVisible(rect);
     }
     private void setTotalValue()
     {
          double dValue=0;
          for(int i=0; i<theModel.getRows(); i++)
          {
               //if(common.toDouble((String)theModel.getValueAt(i,8))>0)  17.04.2020
                    //dValue= dValue+common.toDouble((String)theModel.getValueAt(i,11)); 17.04.2020
				
				if(common.toDouble((String)theModel.getValueAt(i,11))>0)
					dValue= dValue+common.toDouble((String)theModel.getValueAt(i,14));					
          }
          LTotalValue.setText(""+common.getRound(dValue,2));
     }
     private boolean validData()
     {
          for(int i=0; i<theModel.getRows(); i++)
          {
               double dMonthCons = common.toDouble((String)theModel.getValueAt(i,4))*30;

               //double dOrder     = common.toDouble((String)theModel.getValueAt(i,8)); 17.04.2020
			   
			   double dOrder     = common.toDouble((String)theModel.getValueAt(i,11));

               if(dOrder>0)
               {
                    double dPer        = ((dOrder/dMonthCons)*100)-100;
                    double dAllowedPer = common.toDouble((String)VPlusOrMinus.elementAt(i));

                    if( dPer>dAllowedPer)  // (dPer)<=-dAllowedPer ||
                    {
                         JOptionPane.showMessageDialog(null," More than or less Than Allowed Per For the Item"+(String)theModel.getValueAt(i,2));
                         return false;
                    }
               }
          }
          return true;
     }
     private String getMonthlyMrsFinalDate()
     {
          String SDate="";

          
          try
          {
               if(theConnection == null)
               {
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                                   theConnection  =  oraConnection.getConnection();
               }
               Statement      stat           =  theConnection.createStatement();
               ResultSet      res            =  stat.executeQuery(" Select FinalDate from MonthlyMrsFinalDate");
               if(res.next())
               {
                    SDate = res.getString(1);
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
          return (common.getServerPureDate().substring(0,6)+SDate);
     }

     private void setDeptBudget()
     {
          VBudget = new Vector();

          VBudgetDeptCode = new Vector();
          VBudgetDeptName = new Vector();
          VBudgetDeptGroupCode= new Vector();

          String QS = " Select DeptGroupCode,sum(Budget+CarriedOver) as Budget from Budget "+
                      " Where MillCode="+iMillCode+" and UserCode="+iOwnerCode+
                      " and to_Char(sysdate,'YYYYMMDD')>=WithEffectFrom"+
                      " and to_char(Sysdate,'YYYYMMDD')<=WithEffectTo"+
                      " and TypeCode<>2"+
                      " Group by DeptGroupCode";

          String QS1="";
          if(iMillCode!=1)
               QS1 = "Select DeptGroupName,Dept_Code,GroupCode From Dept where (MillCode=0 OR MillCode = 2) Order By Dept_Name";
          else
               QS1 = "Select DeptGroupName,Dept_Code,GroupCode From Dept where (MillCode=1 OR MillCode = 2) Order By Dept_Name";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
     
               Statement      stat          = theConnection.createStatement();
               ResultSet      result        = stat.executeQuery(QS);

               while(result.next())
               {
                    HashMap theMap = new HashMap();

                    theMap.put("DeptCode",result.getString(1));
                    theMap.put("Budget",result.getString(2));

                    VBudget.add(theMap);
               }
               result.close();
               result                       = stat.executeQuery(QS1);
               while(result.next())
               {
                         VBudgetDeptName.addElement(result.getString(1));
                         VBudgetDeptCode.addElement(result.getString(2));
                         VBudgetDeptGroupCode.addElement(result.getString(3));

               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }
     }
     private boolean checkBudget()
     {
          boolean bFlag=true;
          for(int i=0; i<VBudget.size();i++)
          {
               HashMap theMap = (HashMap)VBudget.elementAt(i);

               int  iDeptCode= common.toInt((String)theMap.get("DeptCode"));
               double dBudget= common.toDouble((String)theMap.get("Budget"));

               String SPlanMonth = TopPanel.TStDate.toNormal().substring(0,6);

               String SStDate= SPlanMonth+"01";
               String SEnDate= SPlanMonth+"31";

               double dValue=0 ;

               dValue = getValue(iDeptCode)+getNonOrderedMrsValue(SStDate,SEnDate,iOwnerCode,iDeptCode,"0");

               if(dValue>dBudget)
               {
                    JOptionPane.showMessageDialog(null,getWarningMessage(iDeptCode,dBudget,dValue));
                    bFlag=false;
                    break;
               }
          }
          return bFlag;
     }


     private double getValue(int iBudgetDeptCode)
     {
          double dValue=0;
          for(int i=0; i<theModel.getRows(); i++)
          {
               int iDeptCode = common.toInt((String)VDeptGroupCode.elementAt(i));

               if(iDeptCode==iBudgetDeptCode)
               {

                    //dValue += common.toDouble((String)theModel.getValueAt(i,11));17.04.2020
					dValue += common.toDouble((String)theModel.getValueAt(i,14));
               }
          }
          return dValue;
     }
     private String getDeptName(int iDeptCode)
     {
          int iIndex = VBudgetDeptGroupCode.indexOf(String.valueOf(iDeptCode));

          if(iIndex==-1)
          return "";

          return (String)VBudgetDeptName.elementAt(iIndex);

     }
     private String getWarningMessage(int iDeptCode,double dBudget,double dValue)
     {
          String str = "<html><body><b>";
          str = str + "Mrs Value Exceeds Budget For Dept:"+getDeptName(iDeptCode)+"<br>";
          str = str + " Budget : "+dBudget+" Mrs Value :"+dValue+"<br>";
          str = str + "</body></html>";

          return str;
     }
     private double getNonOrderedMrsValue(String SStDate,String SEnDate,int iAuthUserCode,int iDeptCode,String SMrsNo)
     {
          double dValue = 0;

          String QS = " Select Mrs_Temp.Item_Code,Sum(Mrs_Temp.Qty) as Qty,Sum(Mrs_Temp.NetValue) as NetValue "+
                      " From Mrs_Temp "+
                      " Inner join Dept on Dept.Dept_Code=Mrs_Temp.Dept_Code"+
                      " Where Mrs_Temp.MillCode="+iMillCode+" and Mrs_Temp.AuthUserCode="+iAuthUserCode+
                      " and Dept.GroupCode="+iDeptCode+        // " and MrsNo<>"+SMrsNo+
                      " and Mrs_temp.BudgetStatus=1 and substr(Mrs_Temp.CreationDate,1,8)>='"+SStDate+"' and substr(Mrs_Temp.CreationDate,1,8)<='"+SEnDate+"'"+
                      " and Mrs_Temp.RejectionStatus=0 and Mrs_Temp.ItemDelete=0 and Mrs_Temp.isdelete=0"+
                      " and Mrs_Temp.StoresRejectionStatus=0"+
                      " and Mrs_Temp.UserCode in(select UserCode from MrsUserAuthentication where AuthUserCode="+iAuthUserCode+") "+
                      " Group by Item_Code ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
     
               Statement      stat          = theConnection.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result.next())
               {
                    String SItemCode = result.getString(1);
                    double dQty      = result.getDouble(2);
                    double dNetValue = result.getDouble(3);
                    double dRate     = common.toDouble(common.getLastNetRate(SItemCode,iMillCode));

                    if(dRate>0)
                    {
                         dValue = dValue + (dQty * dRate);
                    }
                    else
                    {
                         dValue = dValue + dNetValue;
                    }
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }

          return dValue;
     }
	 
	 private void setItemStock(String SItemCode) {
		 
          dServiceStock	=0;
		  dNonStock		=0;
		  dOtherStock	=0;

		  String QS =   " select sum(serivestock),sum(nonstock),sum(otherstock) from ( "+
						" select stock as serivestock,0 as nonstock,0 as otherstock from itemstock where itemcode='"+SItemCode+"' and hodcode=10371 "+
						" union all "+
						" select 0 as stock,stock as  nonstock,0 as otherstock from itemstock where itemcode='"+SItemCode+"' and hodcode=6125 "+
						" union all "+
						" select 0 as stock,0 as  nonstock,stock as otherstock from itemstock where itemcode='"+SItemCode+"' and hodcode not in(10371,6125,1,"+iOwnerCode+") ) ";
		  
		  

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
     
               Statement      stat          = theConnection.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result.next())
               {
                     dServiceStock	= result.getDouble(1);
					 dNonStock		= result.getDouble(2);
					 dOtherStock	= result.getDouble(3);
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }
	 }
	 
     public void setMrsData()
     {
          VMrsCode       = new Vector();
          VMrsQty        = new Vector();

          
          String SDate   = common.getServerPureDate();
		  
          String QS1 =   " SELECT MRS.MrsDate, MRS.MrsNo, MRS.Item_Code, MRS.Remarks, MRS.Make, InvItems.Catl, InvItems.Draw, MRS.Qty, MRS.Dept_Code, Dept.Dept_Name,MRS.DueDate,Nature.Nature,OrdBlock.BlockName,Cata.Group_Name"+
                         " FROM ((((MRS INNER JOIN CATA on MRS.GROUP_CODE=CATA.GROUP_CODE) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) "+
                         " Inner Join InvItems on Mrs.Item_Code=InvItems.Item_Code) "+
                         " Left Join Nature on MRS.NatureCode=Nature.NatureCode) "+
                         " Left Join OrdBlock on MRS.BlockCode=OrdBlock.Block "+
                         " WHERE Mrs.MrsFlag=1 and MRS.Qty>0 And MRS.OrderNo=0 and MRS.MrsDate <= '"+SDate+"' "+
                         "  and Mrs.MillCode="+iMillCode+" "+
                         " and Mrs.MRSAuthUserCode in "+
                         " (Select distinct AuthUserCode from MRSUserAuthentication where UserCode = "+iOwnerCode+" )  ";


          

          if(iMillCode!=1)
          {               
               QS1 = QS1 + " and (Mrs.MillCode is Null OR Mrs.MillCode <> 1) ";
          }
          else
          {
               QS1 = QS1 + " and Mrs.MillCode = 1 ";
          }
          
          String QS2 =   " SELECT m1.Item_Code"+
                         " FROM ("+QS1+") m1 GROUP BY m1.Item_Code ";
          
          String QS3 = " ";
          
          if(iMillCode!=1)
          {               
               QS3  =    " SELECT m2.Item_Code, InvItems.OPGQTY, (ROQ+MinQty) AS ReOrd, InvItems.ITEM_NAME,UOM.UoMName"+
                         " FROM ("+QS2+") m2 LEFT JOIN InvItems ON m2.Item_Code = InvItems.ITEM_CODE LEFT JOIN UOM ON UOM.UoMCode = InvItems.UOMCODE ";
          }
          else
          {
               QS3  =    " SELECT m2.Item_Code, DyeingInvItems.OPGQTY, (DyeingInvItems.ROQ+DyeingInvItems.MinQty) AS ReOrd, InvItems.ITEM_NAME,UOM.UoMName"+
                         " FROM ("+QS2+") m2 LEFT JOIN InvItems ON m2.Item_Code = InvItems.ITEM_CODE  "+
                         " LEFT JOIN UOM ON UOM.UoMCode = InvItems.UOMCODE  "+
                         " Left Join DyeingInvItems On InvItems.Item_Code = DyeingInvItems.Item_Code ";
          }
          
          String QS4 = " SELECT m2.Item_Code, Sum((Qty-InvQty)) AS Pending"+
                       " FROM ("+QS2+") m2 LEFT JOIN PurchaseOrder ON m2.Item_Code=PurchaseOrder.Item_Code ";
          
          if(iMillCode!=1)
          {               
               QS4 = QS4 +    " Where PurchaseOrder.MillCode is Null Or PurchaseOrder.MillCode <> 1 "+
                              " GROUP BY m2.Item_Code ";
          }
          else
          {
               QS4 = QS4 +    " Where PurchaseOrder.MillCode = 1 "+
                              " GROUP BY m2.Item_Code ";
          }
          
          String QS5 =   " SELECT m2.Item_Code, Sum(GRN.GrnQty) AS GRNQty"+
                         " FROM ("+QS2+") m2 LEFT JOIN GRN ON m2.Item_Code = GRN.Code  WHERE Grn.GrnBlock<2 ";

          if(iMillCode!=1)
          {
//               QS5 = QS5 +    " where Grn.MillCode Is Null Or Grn.MillCode <> 1 "+
               QS5 = QS5 +    " and Grn.MillCode Is Null Or Grn.MillCode <> 1 "+
                              " GROUP BY m2.Item_Code ";
          }
          else
          {
//               QS5 = QS5 +    " Where Grn.MillCode = 1 "+
               QS5 = QS5 +    " and Grn.MillCode = 1 "+
                              " GROUP BY m2.Item_Code ";
          }
          String QS6 =   " SELECT m2.Item_Code, Sum(Issue.Qty) AS IssueQty"+
                         " FROM ("+QS2+") m2 LEFT JOIN Issue ON m2.Item_Code = Issue.Code ";
          
          if(iMillCode!=1)
          {
               QS6 = QS6 +    " Where Issue.MillCode Is Null Or Issue.MillCode <> 1 "+
                              " GROUP BY m2.Item_Code";
          }
          else
          {
               QS6 = QS6 +    " Where Issue.MillCode = 1 "+
                              " GROUP BY m2.Item_Code";
          }

          String QS7 =   " (select Item_code,blockName,"+
                         " Purchaseorder.OrderNo as OrderNo,Purchaseorder.OrderDate as OrderDate,"+
                         " supplier.name as SupName,Purchaseorder.RATE,Purchaseorder.DISCPER,"+
                         " Purchaseorder.CENVATPER, Purchaseorder.TAXPER,Purchaseorder.SURPER"+
                         " from purchaseorder"+
                         " inner join supplier on supplier.ac_code = purchaseorder.sup_code"+
                         " inner join ordblock on ordblock.block   = purchaseorder.Orderblock"+
                         " where id in "+
                         " (select Ordid from (select max(id) as OrdId,ICode,OrdNo from purchaseorder"+
                         " inner join "+
                         " (select max(OrderNo) as OrdNo,item_code as ICode from"+
                         " (select purchaseOrder.Orderno,purchaseorder.item_code,max(orderdate)"+
                         " from purchaseorder"+
                         " inner join ("+QS1+") m1 on m1.item_code = purchaseorder.item_code"+
                         " group by purchaseorder.item_code,purchaseorder.Orderno)"+
                         " group by item_code) t on t.OrdNo = PurChaseOrder.orderNo and"+
                         " t.ICode = purchaseOrder.item_code"+
                         " group by Icode,OrdNo"+
                         " Order by ICode)))";


          String QString =    " SELECT m1.MrsDate,m1.MrsNo,m1.Item_Code,"+
                              " m3.ITEM_NAME,m1.Remarks,m1.Make,m1.Catl,"+
                              " m1.Draw, m1.Qty, m3.ReOrd,"+
                              " (nvl(m3.OPGQTY,0)+nvl(m5.GRNQty,0)-nvl(m6.IssueQty,0)) AS Stock,"+
                              " m4.Pending, m1.Dept_Code, m1.Dept_Name,"+
                              " m1.DueDate,m1.Nature,m1.BlockName,m3.UoMName,"+
                              " m1.group_name,m7.BlockName,m7.OrderDate,"+
                              " m7.OrderNo,m7.SupName,"+
                              " m7.RATE,m7.DISCPER,m7.CENVATPER,"+
                              " m7.TAXPER,m7.SURPER"+
                              " FROM ("+QS1+") m1"+
                              " INNER JOIN ("+QS3+") m3 ON m1.Item_Code = m3.Item_Code"+
                              " LEFT JOIN ("+QS4+") m4 ON m1.Item_Code = m4.Item_Code"+
                              " LEFT JOIN ("+QS5+") m5 ON m1.Item_Code = m5.Item_Code"+
                              " LEFT JOIN ("+QS6+") m6 ON m1.Item_Code = m6.Item_Code"+
                              " LEFT JOIN ("+QS7+") m7 ON m1.Item_Code = m7.Item_Code"+
                              " ORDER BY 14,1,2,3 ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               ResultSet res = stat.executeQuery(QString);
               
               while (res.next())
               {
                    double dReq     = common.toDouble(res.getString(9));
					
					VMrsQty        . addElement(""+dReq);
					VMrsCode       . addElement(res.getString(3));
                    
               }
			   res.close();
			   stat.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
