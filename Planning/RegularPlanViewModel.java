package Planning;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import guiutil.*;
import util.*;

public class RegularPlanViewModel extends DefaultTableModel 
{
                         //             0         1              2              3                   4              5                             6                7          8                    9            10            11       12             13

//     String         ColumnName[]   = {"HoD","Material Code","Material Name","Prev 6 Months Cons","Per Day Cons.","Projected For This Month","Stock on Hand","@ Pending","One Month Order ","3 Months Order","Last PO Rate","Value","Covering Days","Group"};
//     String         ColumnType[]   = {"S"  ,           "S" ,          "S"  ,            "N"     ,            "N",                     "N"  ,            "N",        "N",          "E",                "S",         "N",          "N",     "N"  ,    "S"};
	 
     String         ColumnName[]   = {"HoD","Material Code","Material Name","Prev 6 Months Cons","Per Day Cons.","Projected For This Month","Stock on Hand","Service Stock","Non Stock","Dyeing Unit Stock",  "Mrs/Order Pending","One Month Order ","3 Months Order","Last PO Rate","Value","Covering Days","Group"};
     String         ColumnType[]   = {"S"  ,           "S" ,          "S"  ,            "N"     ,            "N",                     "N"  ,            "N",			"N",        "N",           "N",  "N",          "E",                "S",         "N",          "N",     "N"  ,    "S"};
	 

     Common common = new Common();

     int iList=0;

     RegularPlanViewModel()
     {
          this.iList=iList;
          setDataVector(getRowData(),ColumnName);
     }
     public Class getColumnClass(int col)
     {
          return getValueAt(0,col).getClass();
     }
     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol]=="B" || ColumnType[iCol]=="E")
               return true;
          return false;
     }
     public void setValueAt(Object aValue, int row, int column)
     {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
                   reSchedule(row,column);

            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
     }
     public int getRows()
     {
          return super.dataVector.size();
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[0][ColumnName.length];
          return RowData;
     }
     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }
     public String getValueFrom(int iRow,int iCol)
     {
          return (String)getValueAt(iRow,iCol);
     }
     public void deleteRow(int index)
     {
         if(getRows()==1)
         return;
         if(index>=getRows())
         return;
         if(index==-1)
         return;
         removeRow(index);
     }

     public void appendEmptyRow(int iRow)
     {
          Vector temp = new Vector();

          insertRow(getRows(),temp);

     } 

     /*public void reSchedule(int Row,int Col)
     {
          try
          {
               if(!(Col>=8 && Col<=9))
                    return;
               double dRate     = common.toDouble((String)getValueAt(Row,10));
               double d3Qty     = common.toDouble((String)getValueAt(Row,9));
               double dQty      = common.toDouble((String)getValueAt(Row,8));
               
               String SValue    = common.getRound((dRate*dQty),2);
               setValueAt( common.parseNull(SValue),Row,11);
               double dStock    = common.toDouble((String)getValueAt(Row,6));
               double dPending  = common.toDouble((String)getValueAt(Row,7));
               dStock           = dStock+dPending+Math.max(dQty,d3Qty);
               double dPDCon    = common.toDouble((String)getValueAt(Row,4));
               double dCover    = 0;
               try
               {
                    dCover = dStock/dPDCon;            
               }
               catch(Exception ex)
               {
                    dCover = 0;
               }
               setValueAt(common.parseNull(common.getRound(dCover,0)),Row,12) ;
          }catch(Exception ex)
          {
               System.out.println(" In Reschedule"+ex);
               ex.printStackTrace();
          }
     }*/

     public void reSchedule(int Row,int Col)
     {
          try
          {
               if(!(Col>=11 && Col<=12))
                    return;
               double dRate     = common.toDouble((String)getValueAt(Row,13));
               double d3Qty     = common.toDouble((String)getValueAt(Row,12));
               double dQty      = common.toDouble((String)getValueAt(Row,11));
               
               String SValue    = common.getRound((dRate*dQty),2);
               setValueAt( common.parseNull(SValue),Row,14);
               double dStock    = common.toDouble((String)getValueAt(Row,6));
               double dPending  = common.toDouble((String)getValueAt(Row,10));
               dStock           = dStock+dPending+Math.max(dQty,d3Qty);
               double dPDCon    = common.toDouble((String)getValueAt(Row,4));
               double dCover    = 0;
               try
               {
                    dCover = dStock/dPDCon;            
               }
               catch(Exception ex)
               {
                    dCover = 0;
               }
               setValueAt(common.parseNull(common.getRound(dCover,0)),Row,15) ;
			   
          }catch(Exception ex)
          {
               System.out.println(" In Reschedule"+ex);
               ex.printStackTrace();
          }
     }
	 
	 
}

