package Planning;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class RegularPlanViewPanel extends JPanel
{
     JPanel         PoolPanel;
     JPanel         HodPanel;
     JPanel         GroupPanel, HODPanel;
     JPanel         SortPanel;
     JPanel         ApplyPanel;

     Common         common = new Common();
     Vector         VGroup,VGroupCode,VHod,VHodCode;
     JComboBox      JCGroup, JCHOD;
     JTextField     TSort;
     NextField      TDelay;

     JRadioButton   JRAllGroup,JRSeleGroup;
     JRadioButton   JRAllHod,JRSeleHod;

     JButton        BPool,BApply,BValue;
     DateField      TDate;
     DateField      TStDate;
//     JCheckBox      COnlyOrderd;

     RegularPlanViewPanel()
     {
          getHodGroup();
          createComponents();
          setLayouts();

          //TStDate.setEditable(false);
          //TDate.setEditable(false);


          PoolPanel . add(TStDate);
          PoolPanel . add(TDate);
          PoolPanel . add(BPool);

          add(PoolPanel);
          add(GroupPanel);
          add(HODPanel);
          add(SortPanel);
          add(ApplyPanel);
     }

     public void setAfterPooling()
     {
          addComponents();
          addListeners();
          updateUI();
     }

     public void createComponents()
     {
          PoolPanel      = new JPanel();
          GroupPanel     = new JPanel();
		HODPanel		= new JPanel();
          SortPanel      = new JPanel();
          ApplyPanel     = new JPanel();
     
          JRAllHod       = new JRadioButton("All");
          JRSeleHod      = new JRadioButton("Selected",true);
     
          JRAllGroup     = new JRadioButton("All",true);
          JRSeleGroup    = new JRadioButton("Selected");
          JCGroup        = new JComboBox(VGroup);
		JCHOD		= new JComboBox(VHod);
//          COnlyOrderd   = new JCheckBox(" Qty Feeded ",true);
     
          TSort          = new JTextField();
     
          BApply         = new JButton("Apply");
          BValue         = new JButton("Valuation");
     
          TStDate        = new DateField();
          TDate          = new DateField();
          BPool          = new JButton("Pool");
          TDate          . setTodayDate();
          TStDate        . setTodayDate();
     }

     public void setLayouts()  
     {
          setLayout(new GridLayout(1,5));
          PoolPanel      . setLayout(new GridLayout(3,1));      
          GroupPanel     . setLayout(new GridLayout(3,1));   
          SortPanel      . setLayout(new GridLayout(2,1));
          ApplyPanel     . setLayout(new GridLayout(2,1));
		HODPanel		. setLayout(new GridLayout(2,1));
     
          PoolPanel      . setBorder(new TitledBorder("Task Schedule"));
          GroupPanel     . setBorder(new TitledBorder("Classification"));
          SortPanel      . setBorder(new TitledBorder("Sort on"));
          ApplyPanel     . setBorder(new TitledBorder("Control"));
		HODPanel		. setBorder(new TitledBorder("User"));
     }

     public void addComponents()
     {
          GroupPanel     . add(JRAllGroup);
          GroupPanel     . add(JRSeleGroup);
          GroupPanel     . add(JCGroup);    
     
          SortPanel      . add(TSort);
/*        SortPanel      . add(new JLabel("eg 1,2,3"));
          SortPanel      . add(COnlyOrderd);*/

          ApplyPanel     . add(BApply);
		
		HODPanel		. add(new JLabel("User"));
		HODPanel		. add(JCHOD);
     }

     public void addListeners()
     {
          JRAllGroup     . addActionListener(new GroupList());
          JRSeleGroup    . addActionListener(new GroupList());
     }


     public class GroupList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllGroup)
               {
                    JRAllGroup     . setSelected(true);
                    JRSeleGroup    . setSelected(false);
                    JCGroup        . setEnabled(false);
               }
               if(ae.getSource()==JRSeleGroup)
               {
                    JRAllGroup     . setSelected(false);
                    JRSeleGroup    . setSelected(true);
                    JCGroup        . setEnabled(true);
               }
          }
     }

     public void getHodGroup()
     {
          ResultSet result    = null;

          VGroup     = new Vector();
          VGroupCode = new Vector();
		
		VHod = new Vector();
		VHodCode = new Vector();
     
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();

               result = stat.executeQuery("Select GroupName,GroupCode From MatGroup Order By 1");

               while(result.next())
               {
                    VGroup    .addElement(result.getString(1));
                    VGroupCode.addElement(result.getString(2));
               }
               result    . close();
			
               result = stat.executeQuery(" Select UserCode, UserName from RawUser where MRSAuthStatus = 1 Order by 2 ");

               while(result.next())
               {
                    VHodCode  .addElement(result.getString(1));
                    VHod		.addElement(result.getString(2));
               }
               result    . close();
			
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("Hod & Class :"+ex);
          }
     }
}
