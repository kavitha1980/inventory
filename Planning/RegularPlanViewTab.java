package Planning;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class RegularPlanViewTab extends JPanel
{
     JTable         ReportTable;
     Object         RowData[][];
     String         ColumnData[],ColumnType[];
     boolean        Flag;
     Common         common = new Common();

     RegularPlanViewTab(Object RowData[][],String ColumnData[],String ColumnType[])
     {
          this.RowData     = RowData;
          this.ColumnData  = ColumnData;
          this.ColumnType  = ColumnType;
          try
          {
               setReportTable();

               for(int i=0;i<RowData.length;i++)
               {
                    reSchedule(i,7);
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);

          }
     }

     public void setReportTable()
     {
          try
          {
               AbstractTableModel dataModel = new AbstractTableModel()
               {
                    public int getColumnCount(){return ColumnData.length;}
                    public int getRowCount(){return RowData.length;}
                    public Object getValueAt(int row,int col){
                    return RowData[row][col];}
                    public String getColumnName(int col){ return ColumnData[col];}
                    public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }
                    public boolean isCellEditable(int row,int col)
                    {
                         if(ColumnType[col]=="B" || ColumnType[col]=="E")
                              return true;
                         return false;
                    }
                    public void setValueAt(Object element,int row,int col)
                    {
                         RowData[row][col]=common.parseNull(String.valueOf(element));
                         reSchedule(row,col);
                    }
               };
               ReportTable  = new JTable(dataModel);
               ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
               DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
               cellRenderer.setHorizontalAlignment(JLabel.RIGHT);
     
               for (int col=0;col<ReportTable.getColumnCount();col++)
               {
                    if(ColumnType[col]=="N" || ColumnType[col]=="E") 
                         ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
               }
//               ReportTable     .setShowGrid(false);
               setLayout(new BorderLayout());
               add(ReportTable.getTableHeader(),BorderLayout.NORTH);
               add(new JScrollPane(ReportTable),BorderLayout.CENTER);
//             ReportTable     .addKeyListener(new KeyList());
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }    

/*   public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_DELETE)
               {
                    nullify();                    
               }
          }
     }*/

     public void reSchedule(int Row,int Col)
     {
          try
          {
               if(!(Col>=8 && Col<=9))
                    return;
               double dRate     = common.toDouble((String)RowData[Row][10]);
               double d3Qty     = common.toDouble((String)RowData[Row][9]);
               double dQty      = common.toDouble((String)RowData[Row][8]);
               
               String SValue    = common.getRound(dRate*Math.max(dQty,d3Qty),2);
               RowData[Row][11] = common.parseNull(SValue);
               double dStock    = common.toDouble((String)RowData[Row][6]);
               double dPending  = common.toDouble((String)RowData[Row][7]);
               dStock           = dStock+dPending+Math.max(dQty,d3Qty);
               double dPDCon    = common.toDouble((String)RowData[Row][4]);
               double dCover    = 0;
               try
               {
                    dCover = dStock/dPDCon;            
               }
               catch(Exception ex)
               {
                    dCover = 0;
               }
               RowData[Row][13] = common.parseNull(common.getRound(dCover,0));
               try
               {
//                    ReportTable.updateUI();
               }catch(Exception e)
               {
               }
          }catch(Exception ex)
          {
               System.out.println(" In Reschedule"+ex);
               ex.printStackTrace();
          }
     }

     public void nullify()
     {
          int Row = ReportTable.getSelectedRow();
          RowData[Row][8]  = "0";
          RowData[Row][9]  = "0";
          RowData[Row][11] = "0";
          RowData[Row][13] = "0";
          try
          {
               try
               {
//                    ReportTable.updateUI();
               }catch(Exception e)
               {
               }
          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}

