import java.io.PrintStream;
import java.sql.*;
import java.util.ArrayList;

public class PurchaseOrder
{

    public PurchaseOrder()
    {
        common = new Common();
    }

    public static String getServerDate()
        throws Exception
    {
        String s = "";
        Class.forName("oracle.jdbc.OracleDriver");
        Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "scm", "rawmat");
        Statement statement = connection.createStatement();
        ResultSet resultset = statement.executeQuery(getDateQS());
        if(resultset.next())
            s = resultset.getString(1);
        resultset.close();
        statement.close();
        return s;
    }

    private static String getDateQS()
    {
        String s = "Select to_Char(sysdate,'yyyymmdd') from Dual";
        return s;
    }

    public static ArrayList setDataintoVector()
    {
        ArrayList arraylist = new ArrayList();
        String s = "";
        try
        {
            String s1 = " Select distinct Sup_Code from   (SELECT PurchaseOrder.OrderDate, PurchaseOrder.OrderNo, OrdBlock.BlockName, PurchaseOrder.Item_Code, InvItems.Item_Name,MatDesc.Descr,MatDesc.Make,PurchaseOrder.MrsNo, Mrs.MrsDate,PurchaseOrder.Qty,PurchaseOrder.InvQty,PurchaseOrder.Qty-PurchaseOrder.InvQty,PurchaseOrder.Rate,PurchaseOrder.Qty*PurchaseOrder.Rate,PurchaseOrder.Advance,PurchaseOrder.DueDate,PurchaseOrder.Id,Supplier.Name,PurchaseOrder.Sup_Code  FROM ((((PurchaseOrder INNER JOIN OrdBlock ON PurchaseOrder.OrderBlock=OrdBlock.Block)  INNER JOIN Supplier ON PurchaseOrder.Sup_Code=Supplier.Ac_Code)  INNER JOIN InvItems ON PurchaseOrder.Item_Code=InvItems.Item_Code)  Left  JOIN MatDesc  ON PurchaseOrder.Item_Code=MatDesc.Item_Code And PurchaseOrder.OrderNo = MatDesc.OrderNo and PurchaseOrder.SlNo = MatDesc.SlNo)  Left  Join MRS      On PurchaseOrder.Item_Code=MRS.Item_Code And PurchaseOrder.MRSNo = MRs.MRSNo and PurchaseOrder.MrsSlNo = MRs.SlNo  Where MailStatus=1 and PurchaseOrder.InvQty < PurchaseOrder.Qty and PurchaseOrder.DueDate < " + getServerDate() + "  Order By 18,17 " + " ) ";
            System.out.println(" QS " + s1);
            Class.forName("oracle.jdbc.OracleDriver");
            Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "inventory", "stores");
            Statement statement = connection.createStatement();
            ResultSet resultset;
            for(resultset = statement.executeQuery(s1); resultset.next(); arraylist.add(resultset.getString(1)));
            resultset.close();
        }
        catch(Exception exception)
        {
            System.out.println(exception);
            exception.printStackTrace();
        }
        return arraylist;
    }

    public static ArrayList setDataintoVector1()
    {
        ArrayList arraylist = new ArrayList();
        String s = "";
        try
        {
            String s1 = " Select distinct Sup_Code from (  Select RDC.RDCDate,RDC.RDCNo,Supplier.Name,Descript,  RDC.Remarks,RDC.Thro,RDC.Qty,RDC.RecQty,  (RDC.Qty-RDC.RecQty) as PendQty,RDC.DueDate,RDC.Sup_Code,RDC.MillCode,  trunc(to_Date(to_char(sysdate,'YYYYMMDD'),'YYYYMMDD'))-trunc(to_Date(RDC.RDCDate,'YYYYMMDD')) as DelayDays  From RDC  Inner Join Supplier On Supplier.Ac_Code = RDC.Sup_Code  Where RDC.RDCDate<=" + getServerDate() + " And RDC.RecQty < RDC.Qty " + " And RDC.AssetFlag=0 and DocType=0 " + " and trunc(to_Date(to_char(sysdate,'YYYYMMDD'),'YYYYMMDD'))-trunc(to_Date(RDC.DueDate,'YYYYMMDD'))>0 " + " Order By 3,1 ) ";
            System.out.println(" QS " + s1);
            Class.forName("oracle.jdbc.OracleDriver");
            Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "inventory", "stores");
            Statement statement = connection.createStatement();
            ResultSet resultset;
            for(resultset = statement.executeQuery(s1); resultset.next(); arraylist.add(resultset.getString(1)));
            resultset.close();
        }
        catch(Exception exception)
        {
            System.out.println(exception);
            exception.printStackTrace();
        }
        return arraylist;
    }

    public static void InsertDetails(String s, String s1, Statement statement, String s2)
    {
        try
        {
            statement.execute(getInsertDetailsQS(s, s1, s2));
        }
        catch(Exception exception)
        {
            System.out.println(exception);
        }
    }

    private static String getInsertDetailsQS(String s, String s1, String s2)
    {
        String s3 = "";
        try
        {
            s3 = " Insert into TBL_ALERT_RECIPIENT_COLLECTION(Collectionid,StatusId,AlertMessage,CustomerCode,Status,Type) Values (alertrecipientcollection_seq.nextval,'" + s2 + "','" + s1 + "','" + s + "',0,1) ";
        }
        catch(Exception exception)
        {
            System.out.println("Insert Details " + exception);
        }
        return s3;
    }

    public static void getData()
    {
        try
        {
            Class.forName("oracle.jdbc.OracleDriver");
            Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "scm", "rawmat");
            Statement statement = connection.createStatement();
            ArrayList arraylist = setDataintoVector();
            ArrayList arraylist1 = setDataintoVector1();
            String s = "testing";
            String s1 = "rdc";
            String s2 = "STATUS12";
            String s3 = "STATUS34";
            for(int i = 0; i < arraylist.size(); i++)
            {
                String s4 = (String)arraylist.get(i);
                InsertDetails(s4, s, statement, s2);
            }

            for(int j = 0; j < arraylist1.size(); j++)
            {
                String s5 = (String)arraylist1.get(j);
                InsertDetails(s5, s1, statement, s3);
            }

            statement.close();
        }
        catch(Exception exception)
        {
            System.out.println(exception);
            exception.printStackTrace();
        }
    }

    public static void main(String args[])
    {
        try
        {
            PurchaseOrder purchaseorder = new PurchaseOrder();
            PurchaseOrder _tmp = purchaseorder;
            getData();
        }
        catch(Exception exception)
        {
            System.out.println(exception);
            exception.printStackTrace();
        }
    }

    Common common;
}