package RDC;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class Accept extends JFrame
{
     JButton        BAccept,BDeny;

     JComboBox      JCUsers,JCMill,JCYear;
     
     JPasswordField TA;
     JPanel         TopPanel,MiddlePanel,BottomPanel;
     Vector         VUserCode,VUserName,VPassword,VEntryCode,VAuthCode,VMCode;
     Vector         VMillCode,VMillName,VItemTable,VSupTable;
     Vector         VYearCode,VYearName,VStDate,VEnDate;
     Common common = new Common();

     Accept()
     {
          VMillCode  = new Vector();
          VMillName  = new Vector();
          VItemTable = new Vector();
          VSupTable  = new Vector();

          setVectors();

          TopPanel    = new JPanel(true);
          MiddlePanel = new JPanel(true);
          BottomPanel = new JPanel(true);

          BAccept  = new JButton("Accept");
          BDeny    = new JButton("Deny");

          JCUsers  = new JComboBox(VUserName);
          JCMill   = new JComboBox();
          JCYear   = new JComboBox(VYearName);

          TA       = new JPasswordField();
          JCMill.setEnabled(false);
          //JCYear.setEnabled(false);
          TA.setEditable(false);

          MiddlePanel.setLayout(new GridLayout(4,1));

          BottomPanel.add(BAccept);
          BottomPanel.add(BDeny);

          MiddlePanel.add(JCUsers);
          MiddlePanel.add(JCMill);
          MiddlePanel.add(JCYear);

          MiddlePanel.add(TA);
          MiddlePanel.setBorder(new TitledBorder("Login"));

          BAccept.setBackground(Color.cyan);
          BAccept.setForeground(Color.blue);
          BDeny.setBackground(Color.red);
          BDeny.setForeground(Color.white);
          TA.setFont(new Font("monospaced",Font.PLAIN,12));
          TA.setForeground(new Color(130,30,60));
          TA.setBackground(Color.pink);

          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          double x             = screenSize.getWidth();
          double y             = screenSize.getHeight();

          x=x/2;
          y=y/2;
          setSize(280,175);
          setLocation(new Double(x-150).intValue(),new Double(y-90).intValue());

          JCUsers.addItemListener(new ITList());
          setMillData(0);
     }

     private class ITList implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
               int index = JCUsers.getSelectedIndex();
               setMillData(index);
          }
     }

     private void setVectors()
     {
          VUserName  = new Vector();
          VUserCode  = new Vector();
          VPassword  = new Vector();
          VEntryCode = new Vector();
          VAuthCode  = new Vector();
          VMCode     = new Vector();

          VYearCode  = new Vector();
          VYearName  = new Vector();
          VStDate    = new Vector();
          VEnDate    = new Vector();
     
          try
          {
               ORAConnection          oraconnection         =  ORAConnection.getORAConnection();
               Connection            theConnection1         =  oraconnection.getConnection();               
               Statement              theStatement          =  theConnection1.createStatement();
               ResultSet              result1               =  theStatement.executeQuery("Select UserName,password,id,EntryLevel,AuthLevel,MillCode From InvLogin Order By 1");
               while(result1.next())
               {
                    VUserName.addElement(result1.getString(1));
                    VPassword.addElement(result1.getString(2));
                    VUserCode.addElement(result1.getString(3));
                    VEntryCode.addElement(result1.getString(4));
                    VAuthCode.addElement(result1.getString(5));
                    VMCode.addElement(result1.getString(6));
               }
               result1.close();

               ResultSet result3 = theStatement.executeQuery("Select YearCode,YearName,StartDate,EndDate From InvPeriod Where YearCode>2 Order By 1 Desc");
               while(result3.next())
               {
                    VYearCode .addElement(result3.getString(1));
                    VYearName .addElement(result3.getString(2));
                    VStDate   .addElement(result3.getString(3));
                    VEnDate   .addElement(result3.getString(4));
               }
               result3.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               System.exit(0);
          }
     }

     private void setMillData(int index)
     {
          Statement              theStatement     = null;
          ResultSet              result           = null;

          JCMill.removeAllItems();

          VMillCode .removeAllElements();
          VMillName .removeAllElements();
          VItemTable.removeAllElements();
          VSupTable .removeAllElements();

          int iUserMillCode = common.toInt((String)VMCode.elementAt(index));

          String QS = "";

          if(iUserMillCode==2)
          {
               QS = "Select MillCode,MillName,Item_Table,Sup_Table From Mill Order By 1";
          }
          else
          if(iUserMillCode==1)
          {
               QS = "Select MillCode,MillName,Item_Table,Sup_Table From Mill Where MillCode=1 Order By 1";
          }
          else
          {
               QS = "Select MillCode,MillName,Item_Table,Sup_Table From Mill Where MillCode<>1 Order By 1";
          }

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               theStatement                  =  theConnection.createStatement();
               result               =  theStatement.executeQuery(QS);
               while(result.next())
               {
                    VMillCode.addElement(result.getString(1));
                    VMillName.addElement(result.getString(2));
                    VItemTable.addElement(result.getString(3));
                    VSupTable.addElement(result.getString(4));
               }
               result.close();
               theStatement.close();

               for(int i=0;i<VMillCode.size();i++)
               {
                    JCMill.addItem((String)VMillName.elementAt(i));
               }
               JCMill.setEnabled(true);
               TA.setEditable(true);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               System.exit(0);
          }
     }

     public boolean isPassValid()
     {
          boolean bFlag = false;

          String SPass = (String)VPassword.elementAt(JCUsers.getSelectedIndex());

          if(SPass.equals(TA.getText()))
               return true;
          return false;
     }
     public String getPassCode()
     {
          return (String)VPassword.elementAt(JCUsers.getSelectedIndex());
     }
     public int getOffIndex()
     {
          return JCUsers.getSelectedIndex();
     }
     public int getHRCode()
     {
          int index = JCUsers.getSelectedIndex();
          return common.toInt((String)VUserCode.elementAt(index));
     }
     public int getAuthCode()
     {
          int index = JCUsers.getSelectedIndex();
          return common.toInt((String)VAuthCode.elementAt(index));
     }

     public int getMillCode()
     {
          int index = JCMill.getSelectedIndex();
          return common.toInt((String)VMillCode.elementAt(index));
     }

     public String getStDate()
     {
          int index = JCYear.getSelectedIndex();
          return (String)VStDate.elementAt(index);
     }

     public String getEnDate()
     {
          int index = JCYear.getSelectedIndex();
          return (String)VEnDate.elementAt(index);
     }

     public String getYear()
     {
          int index = JCYear.getSelectedIndex();
          return (String)VYearName.elementAt(index);
     }
     public String getYearCode()
     {
          int index = JCYear.getSelectedIndex();
          return (String)VYearCode.elementAt(index);
     }
     public String getItemTable()
     {
          int index = JCMill.getSelectedIndex();
          return (String)VItemTable.elementAt(index);
     }
     public String getSupTable()
     {
          int index = JCMill.getSelectedIndex();
          return (String)VSupTable.elementAt(index);
     }
     public String getMillName()
     {
          int index = JCMill.getSelectedIndex();
          return (String)VMillName.elementAt(index);
     }


}
