package RDC;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.JFileChooser;

import util.*;

public class DWOGstDocumentSelection extends JInternalFrame
{

     protected     JLayeredPane Layer;

     JPanel        TopPanel,MiddlePanel,BottomPanel;
     JButton       BApply,BExit;
     Common        common = new Common();

     JTable        theTable;

     Vector        theVector,VPartyName,VPartyCode;

     String        SPartyCode="",SDocName="",SAtt1="",SAtt2="",SAtt3="",SNarr="",SPartyName="",SDeptCode="",SCateCode="",SDeptName="",SCategory="";
     int           iUserCode; 

     DocumentModel  theModel;
     DWOGstDocumentationDisplayFrame theFrame;

     public  DWOGstDocumentSelection(JLayeredPane Layer,DWOGstDocumentationDisplayFrame theFrame,String SPartyCode,String SDocName,String SAtt1,String SAtt2,String SAtt3,String SNarr,String SPartyName,String SDeptCode,String SCateCode,String SDeptName,String SCategory,int iUserCode)
     {

          this.Layer     = Layer;
          this.theFrame  = theFrame;
          this.SPartyCode= SPartyCode;
          this.SDocName  = SDocName;
          this.SAtt1     = SAtt1;
          this.SAtt2     = SAtt2;
          this.SAtt3     = SAtt3;
          this.SNarr     = SNarr;
          this.SPartyName= SPartyName;
          this.SDeptCode = SDeptCode;
          this.SCateCode = SCateCode;
          this.SDeptName = SDeptName;
          this.SCategory = SCategory;
          this.iUserCode = iUserCode;

          setVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setTabReport();
     }
     private void createComponents()
     {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();

          theModel       = new DocumentModel();
          theTable       = new JTable(theModel);

          BExit          = new JButton("Exit");
          BApply         = new JButton("Apply");    
     }

     private void setLayouts()
     {
          setTitle("Document Selection Frame ");
          setClosable(true);                    
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,485);

          TopPanel.setLayout(new BorderLayout());
          TopPanel.setBorder(new TitledBorder("Doc Details"));

          BottomPanel.setLayout(new FlowLayout());
          show();
     }
     private void addComponents()
     {

          TopPanel.add(new JScrollPane(theTable));

          BottomPanel.add(BApply);
          BottomPanel.add(BExit);

          getContentPane().add("Center",TopPanel);
          getContentPane().add("South",BottomPanel);
          BApply.setMnemonic('S');
          BExit.setMnemonic('E');
     }

     public void addListeners()
     {
          BApply      . addActionListener(new ActList());
          BExit       . addActionListener(new ActList());
     }


     private void setTabReport()
     {
          try
          {
               theModel.setNumRows(0);
     
               for(int i=0;i<theVector.size();i++)
               {
                    HashMap   theMap    =(HashMap)theVector.elementAt(i);
     
                    Vector Vect    =    new Vector();
                    Vect           .    addElement(common.parseNull((String)theMap.get("ID")));
                    Vect           .    addElement(common.parseDate(common.parseNull((String)theMap.get("ENTRYDATE"))));

                    String SNames  =    getUsers(common.parseNull((String)theMap.get("ID")));
                    Vect           .    addElement(SNames);
                    Vect           .    addElement(common.parseNull((String)theMap.get("PARTYNAME")));
                    Vect           .    addElement(common.parseNull((String)theMap.get("DOCNAME")));
                    Vect           .    addElement(common.parseNull((String)theMap.get("ATTRIBUTE1")));
                    Vect           .    addElement(common.parseNull((String)theMap.get("ATTRIBUTE2")));
                    Vect           .    addElement(common.parseNull((String)theMap.get("ATTRIBUTE3")));
                    Vect           .    addElement(common.parseNull((String)theMap.get("TEXTAREAVAL")));
                    Vect           .    addElement(common.parseNull((String)theMap.get("DEPTNAME")));
                    Vect           .    addElement(common.parseNull((String)theMap.get("CNAME")));

                    Vect           .    addElement(new Boolean(false));
     
                    theModel            . appendRow(Vect);
               }
          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BExit)
               {
                   removeFrame();
               }
               if(ae.getSource()==BApply)               
               {
                    if(CheckAnyOneSelected())
                    {
                         setData();
                    }
               }
          }
     }

     public void setData()
     {
          try
          {
               DocumentClass  theClass=null;

               String SParty="",SDoc="",SAttri1="",SAttri2="",SAttri3="",SNarration="",SDept="",SCate="",SId="";

               for(int i=0;i<theVector.size();i++)
               {
                    Boolean bValue      = (Boolean)theModel.getValueAt(i,11);


                    if(bValue.booleanValue())
                    {
                         HashMap   theMap   = (HashMap)theVector.elementAt(i);
                         SParty      = common.parseNull((String)theMap.get("PARTYCODE"));
                         SDoc        = common.parseNull((String)theMap.get("DOCNAME"));
                         SAttri1     = common.parseNull((String)theMap.get("ATTRIBUTE1"));
                         SAttri2     = common.parseNull((String)theMap.get("ATTRIBUTE2"));
                         SAttri3     = common.parseNull((String)theMap.get("ATTRIBUTE3"));
                         SNarration  = common.parseNull((String)theMap.get("TEXTAREAVAL"));
                         SDept       = common.parseNull((String)theMap.get("DEPTCODE"));
                         SCate       = common.parseNull((String)theMap.get("CATECODE"));
                         SId         = common.parseNull((String)theMap.get("ID"));

                    }
               }

               System.out.println("PartyCode--->"+SParty);
               System.out.println("DocName-->"+SDoc);
               System.out.println("Att1-->"+SAttri1);
               System.out.println("Att2-->"+SAttri2);
               System.out.println("Att3-->"+SAttri3);
//               theClass            = new DocumentClass(SParty,SDoc,SAttri1,SAttri2,SAttri3,SNarration,SDept,SCate,SId);
                              
//               theFrame            . setImage(theClass);
               removeFrame();
          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
     }

     private boolean CheckAnyOneSelected()
     {
          int iCount = 0;

          for(int i=0;i<theVector.size();i++)
          {
               Boolean bValue      = (Boolean)theModel.getValueAt(i,11);

               if(bValue.booleanValue())
               {
                    iCount        += 1;
               }
          }

          if(iCount == 1)
          {
               return true;               
          }

          JOptionPane.showMessageDialog(null,"Please Select Any one Check Box for Print","Information",JOptionPane.ERROR_MESSAGE);
          return false;
     }



     private Vector setVector()
     {
          theVector      =    new Vector();


          String QS1     =    " Select Id,EntryDate,PartyName,PartyCode,DocName,Attribute1,Attribute2,Attribute3,TextAreaVal,DeptName,DeptCode,CName,CateCode from ( "+
                              " Select distinct (Id) as Id,EntryDate,PartyName,PartyCode,DocName,Attribute1,Attribute2,Attribute3,TextAreaVal,DeptName,DeptCode,CName,CateCode from ( "+
                              " Select Document.Id as Id,Document.EntryDate,PartyMaster.PartyName,Document.PartyCode,Document.DocName,Document.Attribute1, "+
                              " Document.Attribute2,Document.Attribute3,Document.TextAreaVal,Hrdnew.Department.DeptName,Document.DeptCode,Category.CName,Document.CateCode from Document "+
                              " Inner join PartyMaster on PartyMaster.PartyCode=Document.PartyCode "+
                              " Inner join HrdNew.Department on HrdNew.Department.DeptCode=Document.DeptCode "+
                              " Inner join DocUser on DocUser.DocId=Document.Id "+
                              " Inner join Category on Category.CCode=Document.CateCode "+
                              " Where Document.Id>=0  ";     


                           if(!SPartyName.equals("NONE"))
                           {
                              QS1    = QS1 + " and  Document.PartyCode = '"+SPartyCode+"'";
                           }
     
                           if(SDocName.length() !=0)
                           {
                              QS1 = QS1 + " and  Document.DocName like '%"+SDocName.toUpperCase()+"%' ";
                           }
     
                           if(SAtt1.length() !=0)
                           {
                              QS1 = QS1 + " and  Document.Attribute1 like '%"+SAtt1.toUpperCase()+"%' ";
                           }
     
                           if(SAtt2.length() !=0)
                           {
                              QS1 = QS1 + " and  Document.Attribute2 like '%"+SAtt2.toUpperCase()+"%' ";
                           }
     
                           if(SAtt3.length() !=0)
                           {
                              QS1 = QS1 + " and  Document.Attribute3 like '%"+SAtt3.toUpperCase()+"%' ";
                           }
     
                           if(SNarr.length() !=0)
                           {
                              QS1 = QS1 + " and  Document.TextAreaVal like '%"+SNarr.toUpperCase()+"%' ";
                           }

                           if(!SDeptName.equals(""))
                           {
                              QS1    = QS1 + " and  Document.DeptCode = '"+SDeptCode+"'";
                           }

                           if(!SCategory.equals(""))
                           {
                              QS1    = QS1 + " and  Document.CateCode = '"+SCateCode+"'";
                           }

//                           QS1     =    QS1 + " and  DocUser.UserCode="+iUserCode+"   Order by 2 ";

                           QS1     =    QS1 + " and  (DocUser.UserCode="+iUserCode+"  Or Document.EntryUser="+iUserCode+" ) ) Order by 1,2) ";


                         System.out.println(QS1);


          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
               Statement stat      = theConnection.createStatement();
               ResultSet result1   = stat.executeQuery(QS1);
               ResultSetMetaData rsmd = result1.getMetaData();

               while(result1.next())
               {
                    HashMap   theMap    =    new HashMap();

                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         theMap.put(rsmd.getColumnName(i+1),result1.getString(i+1));
                    }
                    theVector.addElement(theMap);
               }
                    result1        .close();
                    stat           .close();

          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
          return theVector;
     }

     private void removeFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex){}
     }

     private String getUsers(String SId) {

      String SQry = " select username from docuser inner join rawuser on rawuser.usercode=docuser.usercode where Docid="+SId;

      String SName="";

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
               Statement stat      = theConnection.createStatement();
               ResultSet result1   = stat.executeQuery(SQry);
               while(result1.next())
               {
                  SName += result1.getString(1)+",";
               }
               result1        .close();
               stat           .close();

          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
          return SName.substring(0,SName.length()-1);
     }

}
