package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGRNInvMiddlePanel extends JPanel
{
   JTable               ReportTable;
   DirectGRNTableModel  dataModel;

   JLabel         LBasic,LDiscount,LCenVat,LTax,LSur,LNet;
   NextField      TAdd,TLess,TModVat;
   JPanel         GridPanel,BottomPanel,FigurePanel;
   JPanel         GridBottom;

   Vector    VDept,VDeptCode,VGroup,VGroupCode,VUnit,VUnitCode;
   JComboBox JCDept,JCGroup,JCUnit;

   JLayeredPane DeskTop;
   Object         RowData[][];
   String         ColumnData[],ColumnType[];
   int iMillCode;

   Common common = new Common();
   ORAConnection connect;
   Connection theconnect;


   // Constructor method referred in GRN Collection Frame Operations
    DirectGRNInvMiddlePanel(JLayeredPane DeskTop,Object RowData[][],String ColumnData[],String ColumnType[],int iMillCode)
    {
         this.DeskTop     = DeskTop;
         this.RowData     = RowData;
         this.ColumnData  = ColumnData;
         this.ColumnType  = ColumnType;
         this.iMillCode   = iMillCode;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
         setReportTable();
    }
    public void createComponents()
    {
         LBasic           = new JLabel("0");
         LDiscount        = new JLabel("0");
         LCenVat          = new JLabel("0");
         LTax             = new JLabel("0");
         LSur             = new JLabel("0");
         TAdd             = new NextField();
         TLess            = new NextField();
         TModVat          = new NextField();
         LNet             = new JLabel("0");
         GridPanel        = new JPanel(true);
         GridBottom       = new JPanel(true);
         BottomPanel      = new JPanel();
         FigurePanel      = new JPanel();
     }
          
     public void setLayouts()
     {
         GridPanel.setLayout(new GridLayout(1,1));
         BottomPanel.setLayout(new BorderLayout());
         FigurePanel.setLayout(new GridLayout(2,9));
         GridBottom.setLayout(new BorderLayout());         
     }
     public void addComponents()
     {
         FigurePanel.add(new JLabel("Basic"));
         FigurePanel.add(new JLabel("Discount"));
         FigurePanel.add(new JLabel("CenVat"));
         FigurePanel.add(new JLabel("Tax"));
         FigurePanel.add(new JLabel("Surcharge"));
         FigurePanel.add(new JLabel("Plus"));
         FigurePanel.add(new JLabel("Minus"));
         FigurePanel.add(new JLabel("Net"));
         FigurePanel.add(new JLabel("Vat Claimable"));

         FigurePanel.add(LBasic);
         FigurePanel.add(LDiscount);
         FigurePanel.add(LCenVat);
         FigurePanel.add(LTax);
         FigurePanel.add(LSur);
         FigurePanel.add(TAdd);
         FigurePanel.add(TLess);
         FigurePanel.add(LNet);
         FigurePanel.add(TModVat);

         BottomPanel.add("North",FigurePanel);

         getDeptGroupUnit();
         JCDept  = new JComboBox(VDept);
         JCGroup = new JComboBox(VGroup);
         JCUnit  = new JComboBox(VUnit);
     }
     public void addListeners()
     {
         TAdd.addKeyListener(new KeyList());
         TLess.addKeyListener(new KeyList());
     }
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               calc();
          }
     }
     public void calc()
     {
               double dTNet = common.toDouble(LBasic.getText())-common.toDouble(LDiscount.getText())+common.toDouble(LCenVat.getText())+common.toDouble(LTax.getText())+common.toDouble(LSur.getText());
               dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
               LNet.setText(common.getRound(dTNet,2));                              
     }
     public void setReportTable()
     {
         dataModel        = new DirectGRNTableModel(RowData,ColumnData,ColumnType,LBasic,LDiscount,LCenVat,LTax,LSur,TAdd,TLess,LNet,TModVat);       
         ReportTable      = new JTable(dataModel);
         ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<ReportTable.getColumnCount();col++)
         {
               if(ColumnType[col]=="N" || ColumnType[col]=="B")
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
         }
         //ReportTable.setShowGrid(false);

         TableColumn deptColumn  = ReportTable.getColumn("Department");
         TableColumn groupColumn = ReportTable.getColumn("Group");
         TableColumn unitColumn  = ReportTable.getColumn("Unit");

         deptColumn.setCellEditor(new DefaultCellEditor(JCDept));
         groupColumn.setCellEditor(new DefaultCellEditor(JCGroup));
         unitColumn.setCellEditor(new DefaultCellEditor(JCUnit));

         setLayout(new BorderLayout());
         GridBottom.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
         GridBottom.add(new JScrollPane(ReportTable),BorderLayout.CENTER);

         GridPanel.add(GridBottom);

         add(BottomPanel,BorderLayout.SOUTH);
         add(GridPanel,BorderLayout.CENTER);
     }
     public Object[][] getFromVector()
     {
          return dataModel.getFromVector();     
     }

     public String getDeptCode(int i)                       // 19
     {
         Vector VCurVector = dataModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(17);
         int iid = VDept.indexOf(str);
         return (iid==-1 ?"0":(String)VDeptCode.elementAt(iid));
     }
     public String getGroupCode(int i)                     // 20
     {
         Vector VCurVector = dataModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(18);
         int iid = VGroup.indexOf(str);
         return (iid==-1?"0":(String)VGroupCode.elementAt(iid));
     }
     public String getUnitCode(int i)                     //  21
     {
         Vector VCurVector = dataModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(19);
         int iid = VUnit.indexOf(str);
         return (iid==-1?"0":(String)VUnitCode.elementAt(iid));
     }
     public double getModVat()                     //  Actual Modvat Claimable
     {
         return common.toDouble(TModVat.getText());
     }
     public double getVatBasic()
     {
          return dataModel.getVatBasic();
     }
     public void getDeptGroupUnit()
     {
        VDept  = new Vector();
        VGroup = new Vector();

        VDeptCode  = new Vector();
        VGroupCode = new Vector();

        VUnit      = new Vector();
        VUnitCode  = new Vector();

        try
        {
               if(theconnect==null)
               {
                     connect=ORAConnection.getORAConnection();
                     theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();

               String QS1 = "";
               String QS2 = "";
               String QS3 = "";

               QS1 = "Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               QS2 = "Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               QS3 = "Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";

               ResultSet result1 = stat.executeQuery(QS1);
               while(result1.next())
               {
                    VDept.addElement(result1.getString(1));
                    VDeptCode.addElement(result1.getString(2));
               }
               result1.close();

               ResultSet result2 = stat.executeQuery(QS2);
               while(result2.next())
               {
                    VGroup.addElement(result2.getString(1));
                    VGroupCode.addElement(result2.getString(2));
               }
               result2.close();

               ResultSet result3 = stat.executeQuery(QS3);
               while(result3.next())
               {
                    VUnit.addElement(result3.getString(1));
                    VUnitCode.addElement(result3.getString(2));
               }
               result3.close();
               stat.close();
        }
        catch(Exception ex)
        {
            System.out.println("Dept,Group & Unit :"+ex);
        }
     }
}

