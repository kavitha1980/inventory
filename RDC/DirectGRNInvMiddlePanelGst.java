package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGRNInvMiddlePanelGst extends JPanel
{
   JTable                  ReportTable;
   DirectGRNTableModelGst  dataModel;
  MyArrayList<String>    MaterialCodeList;
   JLabel         LBasic,LDiscount,LCenVat,LTax,LSur,LNet,LCGST,LSGST,LIGST,LCESS,LOthers,LInfo;
   NextField      TAdd,TLess,TModVat;
   JPanel         GridPanel,BottomPanel,FigurePanel,ControlPanel;
   JPanel         GridBottom;

   Vector    VDept,VDeptCode,VGroup,VGroupCode,VUnit,VUnitCode;
   JComboBox JCDept,JCGroup,JCUnit;
   JButton        BAdd,BDel;
   JLayeredPane DeskTop;
   Object         RowData[][];
   String         ColumnData[],ColumnType[];
   int iMillCode;
   String SSupCode="";
   Common common = new Common();
   ORAConnection connect;
   Connection theconnect;
   Vector         VHsnType,VRateStatus;

   // Constructor method referred in GRN Collection Frame Operations
    DirectGRNInvMiddlePanelGst(JLayeredPane DeskTop,Object RowData[][],String ColumnData[],String ColumnType[],int iMillCode,String SSupCode,Vector VRateStatus)
    {
         this.DeskTop     = DeskTop;
         this.RowData     = RowData;
         this.ColumnData  = ColumnData;
         this.ColumnType  = ColumnType;
         this.iMillCode   = iMillCode;
         this.SSupCode    = SSupCode;
         this.VRateStatus  =VRateStatus;

          MaterialCodeList = new MyArrayList<String>(); 
         createComponents();
         setLayouts();
         addComponents();
         addListeners();
         setReportTable();
    }
    public void createComponents()
    {
         LBasic           = new JLabel("0");
         LDiscount        = new JLabel("0");
       //  LCenVat          = new JLabel("0");
        // LTax             = new JLabel("0");
         //LSur             = new JLabel("0");
         LCGST            = new JLabel("0");
         LSGST            = new JLabel("0");
         LIGST            = new JLabel("0");
         LCESS            = new  JLabel("0");  
         LOthers          = new JLabel("0");    
         TAdd             = new NextField();
         TLess            = new NextField();
         TModVat          = new NextField();
         LNet             = new JLabel("0");
         GridPanel        = new JPanel(true);
         GridBottom       = new JPanel(true);
         BottomPanel      = new JPanel();
         FigurePanel      = new JPanel();
         ControlPanel     = new JPanel();
         BAdd             = new JButton("Add New Record");
         BDel             = new JButton("Delete Record");  
         LInfo           =  new JLabel("Press F5 to select service");
         
         VHsnType         = new Vector();
       
	    // VRateStatus      = new Vector();
     }
          
     public void setLayouts()
     {
         GridPanel.setLayout(new GridLayout(1,1));
         BottomPanel.setLayout(new BorderLayout());
         FigurePanel.setLayout(new GridLayout(2,10));
         GridBottom.setLayout(new BorderLayout());  
         ControlPanel.setLayout(new FlowLayout());      
     }
     public void addComponents()
     {
         FigurePanel.add(new JLabel("Basic"));
         FigurePanel.add(new JLabel("Discount"));
         FigurePanel.add(new JLabel("CGST"));
         FigurePanel.add(new JLabel("SGST"));
         FigurePanel.add(new JLabel("IGST"));
         FigurePanel.add(new JLabel("CESS")); 
      //   FigurePanel.add(new JLabel("Others")); 
         FigurePanel.add(new JLabel("Plus"));
         FigurePanel.add(new JLabel("Minus"));
         FigurePanel.add(new JLabel("Net"));
       //  FigurePanel.add(new JLabel("Vat Claimable"));

         FigurePanel.add(LBasic);
         FigurePanel.add(LDiscount);
         FigurePanel.add(LCGST);
         FigurePanel.add(LSGST);
         FigurePanel.add(LIGST);
         FigurePanel.add(LCESS);
       //  FigurePanel.add(LOthers);
         FigurePanel.add(TAdd);
         FigurePanel.add(TLess);
         FigurePanel.add(LNet);
        // FigurePanel.add(TModVat);

          ControlPanel.add(BAdd);
          ControlPanel.add(BDel);
          ControlPanel.add(LInfo);

         BottomPanel.add("North",FigurePanel);
         BottomPanel.add("South",ControlPanel);

         getDeptGroupUnit();
         JCDept  = new JComboBox(VDept);
         JCGroup = new JComboBox(VGroup);
         JCUnit  = new JComboBox(VUnit);
     }
     public void addListeners()
     {
         TAdd.addKeyListener(new KeyList());
         TLess.addKeyListener(new KeyList());
         BAdd   .addActionListener(new ActList());
         BDel.addActionListener(new ActList());
     }
 
      public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BAdd)
               {
                     int iRows = dataModel.getRows();

           if(iRows>=1)
		    {
				
                    Vector  VEmpty1 = new Vector();
                    for(int i=0;i<ColumnData.length;i++){
                    VEmpty1.addElement(" ");
                     }

                     
			          VRateStatus.addElement("1");
                      dataModel.addRow(VEmpty1);
                      ReportTable.updateUI();
                      ReportTable.requestFocus();

             }
                
               }

               if(ae.getSource()==BDel)
               {
		   
                    int iRows = dataModel.getRows();
		    if(iRows>1)
		    {
		     int i = ReportTable.getSelectedRow();

			 String SRateStatus = (String)VRateStatus.elementAt(i);
			 if(SRateStatus.equals("1"))
			 {
				 dataModel.removeRow(i);

				 
                 VRateStatus.removeElementAt(i);
				
				 ReportTable.updateUI();
			 }
			 else
			 {
		              JOptionPane.showMessageDialog(null,"This row couldn't be Deleted","Information",JOptionPane.INFORMATION_MESSAGE);
			 }
		    }
               }


          }
     }
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               calc();
          }
     }
     public void calc()
     {
               double dTNet = common.toDouble(LBasic.getText())-common.toDouble(LDiscount.getText())+common.toDouble(LCGST.getText())+common.toDouble(LSGST.getText())+common.toDouble(LIGST.getText()+common.toDouble(LCESS.getText()));
               dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
               LNet.setText(common.getRound(dTNet,2));                              
     }
     public void setReportTable()
     {
         dataModel        = new DirectGRNTableModelGst(RowData,ColumnData,ColumnType,LBasic,LDiscount,LCGST,LSGST,LIGST,LCESS,TAdd,TLess,LNet,TModVat,VRateStatus,VHsnType,LOthers);       
         ReportTable      = new JTable(dataModel);
         ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<ReportTable.getColumnCount();col++)
         {
               if(ColumnType[col]=="N" || ColumnType[col]=="B")
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
         }
         //ReportTable.setShowGrid(false);

         TableColumn deptColumn  = ReportTable.getColumn("Department");
         TableColumn groupColumn = ReportTable.getColumn("Group");
         TableColumn unitColumn  = ReportTable.getColumn("Unit");

         deptColumn.setCellEditor(new DefaultCellEditor(JCDept));
         groupColumn.setCellEditor(new DefaultCellEditor(JCGroup));
         unitColumn.setCellEditor(new DefaultCellEditor(JCUnit));

         setLayout(new BorderLayout());
         GridBottom.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
         GridBottom.add(new JScrollPane(ReportTable),BorderLayout.CENTER);

         GridPanel.add(GridBottom);

         add(BottomPanel,BorderLayout.SOUTH);
         add(GridPanel,BorderLayout.CENTER);

          ReportTable    .  addKeyListener(new KeyList1());
     }

       public class KeyList1 extends KeyAdapter
       {
          public void keyPressed(KeyEvent ke)
          {
	       int iSelectedRow  = ReportTable.getSelectedRow();
	       int iCol = ReportTable.getSelectedColumn();
            
           String SRateStatus = (String)VRateStatus.elementAt(iSelectedRow);

	       if(ke.getKeyCode()==KeyEvent.VK_F5)
               {	
            if(SRateStatus.equals("1"))
			 {		
			if (iCol == 0 || iCol == 1) {
               
             
				setServList(iSelectedRow);
               
			}

             }else{
                JOptionPane.showMessageDialog(null,"This row couldn't be changed","Information",JOptionPane.INFORMATION_MESSAGE);
              } 

	       }

       }

    }

    private void setServList(int iSelectedRow)
     {

            String  sRdcDesc = common.parseNull((String)ReportTable.getValueAt(iSelectedRow, 2)).trim();
           
            if(sRdcDesc.equals("")){
           WorkGrnGstServiceAndMaterialPickerFrame SP = new WorkGrnGstServiceAndMaterialPickerFrame(DeskTop,iSelectedRow,ReportTable,LBasic,LDiscount,TAdd,TLess,LNet,MaterialCodeList,iMillCode,SSupCode,VHsnType);
           try
           {
                DeskTop   . add(SP);
                DeskTop   . repaint();
                SP        . setSelected(true);
                DeskTop   . updateUI();
                SP        . show();
                SP        . BrowList.requestFocus();			    				
           }
           catch(java.beans.PropertyVetoException ex){}
         }
     }
     public Object[][] getFromVector()
     {
          return dataModel.getFromVector();     
     }

     public String getDeptCode(int i)                       // 21
     {
         Vector VCurVector = dataModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(21);
         int iid = VDept.indexOf(str);
         return (iid==-1 ?"0":(String)VDeptCode.elementAt(iid));
     }
     public String getGroupCode(int i)                     // 22
     {
         Vector VCurVector = dataModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(22);
         int iid = VGroup.indexOf(str);
         return (iid==-1?"0":(String)VGroupCode.elementAt(iid));
     }
     public String getUnitCode(int i)                     //  23
     {
         Vector VCurVector = dataModel.getCurVector(i);
         String str = (String)VCurVector.elementAt(23);
         int iid = VUnit.indexOf(str);
         return (iid==-1?"0":(String)VUnitCode.elementAt(iid));
     }
     public double getModVat()                     //  Actual Modvat Claimable
     {
         return common.toDouble(TModVat.getText());
     }
   /*  public double getVatBasic()
     {
          return dataModel.getVatBasic();
     }*/
     public void getDeptGroupUnit()
     {
        VDept  = new Vector();
        VGroup = new Vector();

        VDeptCode  = new Vector();
        VGroupCode = new Vector();

        VUnit      = new Vector();
        VUnitCode  = new Vector();

        try
        {
               if(theconnect==null)
               {
                     connect=ORAConnection.getORAConnection();
                     theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();

               String QS1 = "";
               String QS2 = "";
               String QS3 = "";

               QS1 = "Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               QS2 = "Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               QS3 = "Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";

               ResultSet result1 = stat.executeQuery(QS1);
               while(result1.next())
               {
                    VDept.addElement(result1.getString(1));
                    VDeptCode.addElement(result1.getString(2));
               }
               result1.close();

               ResultSet result2 = stat.executeQuery(QS2);
               while(result2.next())
               {
                    VGroup.addElement(result2.getString(1));
                    VGroupCode.addElement(result2.getString(2));
               }
               result2.close();

               ResultSet result3 = stat.executeQuery(QS3);
               while(result3.next())
               {
                    VUnit.addElement(result3.getString(1));
                    VUnitCode.addElement(result3.getString(2));
               }
               result3.close();
               stat.close();
        }
        catch(Exception ex)
        {
            System.out.println("Dept,Group & Unit :"+ex);
        }
     }
}

