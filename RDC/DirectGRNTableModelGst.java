package RDC;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGRNTableModelGst extends DefaultTableModel
{
       Object    RowData[][],ColumnNames[],ColumnType[];
       JLabel    LBasic,LDiscount,LCenVat,LTax,LSur;
       NextField TAdd,TLess,TModVat;
       JLabel         LCGST,LSGST,LIGST,LCess,LNet,LOthers;
       double dCGST=0,dSGST=0,dIGST=0,dCess=0;
       Common common = new Common();
        Vector VHsnType,VRateStatus;
       public DirectGRNTableModelGst(Object[][] RowData, Object[] ColumnNames, Object[] ColumnType, JLabel LBasic, JLabel LDiscount, JLabel LCGST, JLabel LSGST, JLabel LIGST,JLabel LCess, NextField TAdd, NextField TLess, JLabel LNet, NextField TModVat,Vector VRateStatus,Vector VHsnType,JLabel LOthers)
       {
           super(RowData,ColumnNames);
           this.RowData     = RowData;
           this.ColumnNames = ColumnNames;
           this.ColumnType  = ColumnType;
           this.LBasic      = LBasic;
           this.LDiscount   = LDiscount;
           this.LCGST       = LCGST; 
           this.LSGST       = LSGST;
           this.LIGST        = LIGST;
           this.LCess       = LCess;
           this.TAdd        = TAdd;
           this.TLess       = TLess;
           this.LNet        = LNet;
           this.TModVat     = TModVat;
           this.VHsnType    = VHsnType;
	       this.VRateStatus = VRateStatus; 
           this.LOthers=LOthers;
           for(int i=0;i<super.dataVector.size();i++)
           {
               Vector curVector = (Vector)super.dataVector.elementAt(i);
              // setMaterialAmount(curVector);
               // setMaterialAmount(curVector,SRateStatus,row);
           }
       }
       
       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }     
              
       public boolean isCellEditable(int row,int col)
       {

         //  System.out.println("VHsnType"+VHsnType);
            /*  String SHsnType    = (String)VHsnType.elementAt(row);
     	       String SRateStatus = (String)VRateStatus.elementAt(row);

               if(col==8)
	           {
					   if(SHsnType.equals("1"))
					   {*/

				       if(ColumnType[col]=="B" || ColumnType[col]=="E")
				          return true;
                        //}
			    
		                 else
			             return false;

	     /*  }
	       else
	       {

              if(ColumnType[col]=="B" || ColumnType[col]=="E")
		          return true;

	       	       if(SHsnType.equals("1") && SRateStatus.equals("1") )
			    return true;

		       return false;
	       }*/
              //return false;
       }
       
       public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   String SRateStatus = (String)VRateStatus.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
                   if(column>=6 && column<=13)
                      setMaterialAmount(rowVector,SRateStatus,row);
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }
      
      public void setMaterialAmount(Vector RowVector,String SRateStatus,int iRow)
      {
           

				double dTGross=0,dTDisc=0,dTCGST=0,dTSGST=0,dTIGST=0,dTCess=0,dTNet=0,dTOthers=0;

                double dQty  =0;
                if(SRateStatus.equals("0"))
	            {
				 dQty        = common.toDouble((String)RowVector.elementAt(6));
                }else{
                    dQty = 1;
                }


				double dRate       = common.toDouble((String)RowVector.elementAt(8));
				double dDiscPer    = common.toDouble((String)RowVector.elementAt(9));
				double dCGSTPer    = common.toDouble((String)RowVector.elementAt(10));
				double dSGSTPer    = common.toDouble((String)RowVector.elementAt(11));
				double dIGSTPer    = common.toDouble((String)RowVector.elementAt(12));
				double dCESSPer    = common.toDouble((String)RowVector.elementAt(13));




				double dGross   = dQty*dRate;
				double dDisc    = dGross*dDiscPer/100;
				double dBasic   = dGross - dDisc;
				double dCGSTVal = dBasic*dCGSTPer/100;
				double dSGSTVal = dBasic*dSGSTPer/100;
				double dIGSTVal = dBasic*dIGSTPer/100;
				double dCessVal = dBasic*dCESSPer/100;
				double dNet     = dBasic+dCGSTVal+dSGSTVal+dIGSTVal+dCessVal;



				RowVector.setElementAt(common.getRound(dDisc,2),15);
				RowVector.setElementAt(common.getRound(dBasic,2),14);
				RowVector.setElementAt(common.getRound(dCGSTVal,2),16);
				RowVector.setElementAt(common.getRound(dSGSTVal,2),17);
				RowVector.setElementAt(common.getRound(dIGSTVal,2),18);
				RowVector.setElementAt(common.getRound(dCessVal,2),19);
				RowVector.setElementAt(common.getRound(dNet,2),20);




              for(int i=0;i<super.dataVector.size();i++)
              {
               String SRStatus = (String)VRateStatus.elementAt(i);
			Vector curVector = (Vector)super.dataVector.elementAt(i);

			dTGross   = dTGross+common.toDouble((String)curVector.elementAt(14));
			dTDisc    = dTDisc +common.toDouble((String)curVector.elementAt(15));
			dTCGST    = dTCGST +common.toDouble((String)curVector.elementAt(16));
			dTSGST    = dTSGST +common.toDouble((String)curVector.elementAt(17));
			dTIGST    = dTIGST +common.toDouble((String)curVector.elementAt(18));
			dTCess    = dTCess +common.toDouble((String)curVector.elementAt(19));
			dTNet     = dTNet  +common.toDouble((String)curVector.elementAt(20));
            if(SRStatus.equals("1"))
		    {
                       dTOthers = dTOthers+common.toDouble((String)curVector.elementAt(20));
		    }
		    System.out.println("Inside:"+dTOthers);

			}

		    System.out.println("outside:"+dTOthers);
			dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
			LBasic.setText(common.getRound(dTGross,2));
			LDiscount.setText(common.getRound(dTDisc,2));
			LCGST.setText(common.getRound(dTCGST,2));
			LSGST.setText(common.getRound(dTSGST,2));
			LIGST.setText(common.getRound(dTIGST,2));
			LCess.setText(common.getRound(dTCess,2));
			LNet.setText(common.getRound(dTNet,2));
            LOthers.setText(common.getRound(dTOthers,2));
    }
    
    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
                FinalData[i][j] = ((String)curVector.elementAt(j)).trim();
        }
        return FinalData;
    }
    
    public int getRows()
    {
         return super.dataVector.size();
    }
    
    public Vector getCurVector(int i)
    {
         return (Vector)super.dataVector.elementAt(i);
    }
    
}
