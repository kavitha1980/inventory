package RDC;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectRDCInvMiddlePanel extends JPanel
{
     JTable               ReportTable;
     DirectRDCTableModel  dataModel;
  
     JPanel         GridPanel,BottomPanel;
     JPanel         GridBottom;
  
     JLayeredPane   DeskTop;
     Object         RowData[][];
     String         ColumnData[],ColumnType[];
     int            iColWidth[];
     int            iMillCode;
     String         SSupCode;
  
     Common common = new Common();
 
     DirectRDCInvMiddlePanel(JLayeredPane DeskTop,Object RowData[][],String ColumnData[],String ColumnType[],int iColWidth[],int iMillCode,String SSupCode)
     {
          this.DeskTop     = DeskTop;
          this.RowData     = RowData;
          this.ColumnData  = ColumnData;
          this.ColumnType  = ColumnType;
          this.iColWidth   = iColWidth;
          this.iMillCode   = iMillCode;
          this.SSupCode    = SSupCode;
 
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setReportTable();
     }
     public void createComponents()
     {
         GridPanel        = new JPanel(true);
         GridBottom       = new JPanel(true);
         BottomPanel      = new JPanel();
     }
          
     public void setLayouts()
     {
         GridPanel.setLayout(new GridLayout(1,1));
         BottomPanel.setLayout(new BorderLayout());
         GridBottom.setLayout(new BorderLayout());         
     }
     public void addComponents()
     {
     }
     public void addListeners()
     {
     }

     public void setReportTable()
     {
         dataModel        = new DirectRDCTableModel(RowData,ColumnData,ColumnType);       
         ReportTable      = new JTable(dataModel);
         ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
         setPrefferedColumnWidth();

         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<ReportTable.getColumnCount();col++)
         {
               if(ColumnType[col]=="N" || ColumnType[col]=="B")
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
         }
         ReportTable.setShowGrid(false);

         setLayout(new BorderLayout());
         GridBottom.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
         GridBottom.add(new JScrollPane(ReportTable),BorderLayout.CENTER);

         GridPanel.add(GridBottom);

         add(BottomPanel,BorderLayout.SOUTH);
         add(GridPanel,BorderLayout.CENTER);

         dataModel.removeRow(0);
     }

     public void setPrefferedColumnWidth()
     {
          if(iColWidth.length != ColumnData.length)
               return;
          for(int i=0;i<ColumnData.length;i++)
          {
               if(iColWidth[i]>0)
                    (ReportTable.getColumn(ColumnData[i])).setPreferredWidth(iColWidth[i]);
          }
     }

     public Object[][] getFromVector()
     {
         return dataModel.getFromVector();     
     }

}

