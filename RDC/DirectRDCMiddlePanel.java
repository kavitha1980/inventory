package RDC;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class DirectRDCMiddlePanel extends JTabbedPane 
{
     DirectRDCInvMiddlePanel MiddlePanel;
     DirectRDCPanel RdcPanel;
     DirectGatePanel GatePanel;
     Object RowData[][];
     Object RData[][];
     Object GData[][];

     String ColumnData[] = {"Description","RDC No","RDC Qty","Pending Qty","Inv/DC Qty","Recd Qty","Department","Unit"};
     String ColumnType[] = {"S"          ,"N"     ,"N"      ,"N"          ,"N"         ,"N"       ,"S"         ,"S"   };
     int    iColWidth[]  = {250          ,80      ,80       ,80           ,80          ,80        ,80          ,80    };

/*     String CData[]   = {"Description","RDC No","RDC Date","RDC Qty","Pending Qty","Recd Qty","Direct WO Qty","Prev WO Qty","Prev Free Qty","Free Qty","WO Qty","Select","Purpose"};
     String CType[]   = {"S"          ,"N"     ,"N"       ,"N"      ,"N"          ,"E"       ,"N"            ,"N"          ,"N"            ,"E"       ,"N"     ,"E"     ,"S"      };
     int    iCWidth[] = {250          ,60      ,70        ,70       ,70           ,70        ,70             ,70           ,70             ,75        ,70      ,60      ,150      };*/

	 
/* 02.07.2020    String CData[]   = {"Description","RDC No","RDC Date","RDC Qty","Pending Qty","Recd Qty","Direct WO Qty","Prev WO Qty","Prev Free Qty","Free Qty","WO Qty","Select","Scrap Qty","ItemName","ItemCode" ,"Purpose","Bin"};
     String CType[]   = {"S"          ,"N"     ,"N"       ,"N"      ,"N"          ,"E"       ,"N"            ,"N"          ,"N"            ,"E"       ,"N"     ,"E"     ,"E"      	,"S" 		 ,"S"		 ,"S"	 ,"E"};
     int    iCWidth[] = {250          ,60      ,70        ,70       ,70           ,70        ,70             ,70           ,70             ,75        ,70      ,60      ,90      	,120		 ,70		 ,100	 ,90};*/
	 

     String CData[]   = {"Description","RDC No","RDC Date","RDC Qty","Pending Qty","Recd Qty","Direct WO Qty","Prev WO Qty","Prev Free Qty","Free Qty","WO Qty","Select","Scrap Qty","ItemName","ItemCode" ,"Purpose","Bin","Service Stock",  "Non Stock","Without Stock"};
     String CType[]   = {"S"          ,"N"     ,"N"       ,"N"      ,"N"          ,"E"       ,"N"            ,"N"          ,"N"            ,"E"       ,"N"     ,"E"     ,"E"      	,"S" 		 ,"S"		 ,"S"	 ,"E"  ,"E" 		   ,  "E"		 ,"E"};
     int    iCWidth[] = {250          ,60      ,70        ,70       ,70           ,70        ,70             ,70           ,70             ,75        ,70      ,60      ,90      	,120		 ,70		 ,80	 ,90   ,90			   ,  70		 ,80};
	 
	 int iJmdAppStatus=0;
	 

     String CGData[] = {"Description","Inv/DC Qty"};
     String CGType[] = {"E"          ,"E"         };

     JLayeredPane DeskTop;
     String SSupCode,SSupName;

     Vector VGDesc,VGRdcNo,VGRdcDate;
     Vector VGPendQty,VGRecQty;
     Vector VGDept,VGDeptCode,VGUnit,VGUnitCode;
     Vector VId,VGRdcQty,VRdcSlNo,VDirectWOQty,VPrevWOQty,VPrevFreeQty,VRemarks,VMemoUserCode,VItemName,VItemCode,VBinName,VJMDStatus;

     Vector VSeleId;

     Common common   = new Common();
     int iMillCode;
     int iSortIndex;
	 
	 String SSupTable;

     public DirectRDCMiddlePanel(JLayeredPane DeskTop,int iMillCode,String SSupCode,String SSupName,int iSortIndex)
     {
          this.DeskTop    = DeskTop;
          this.iMillCode  = iMillCode;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.iSortIndex = iSortIndex;

          VSeleId      = new Vector();

          createComponents();
     }
	 
     public DirectRDCMiddlePanel(JLayeredPane DeskTop,int iMillCode,String SSupCode,String SSupName,int iSortIndex,String SSupTable)
     {
          this.DeskTop    = DeskTop;
          this.iMillCode  = iMillCode;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.iSortIndex = iSortIndex;
		  this.SSupTable  = SSupTable;
		  

          VSeleId      = new Vector();

          createComponents();
     }
	 

     public void createComponents()
     {
          setVectorData();
          setRowData();
          setRData();
          setGData();
          try
          {
                GatePanel   = new DirectGatePanel(DeskTop,GData,CGData,CGType);
                RdcPanel    = new DirectRDCPanel(DeskTop,RData,CData,CType,iCWidth,iMillCode,SSupCode,SSupName,this,iJmdAppStatus);
                MiddlePanel = new DirectRDCInvMiddlePanel(DeskTop,RowData,ColumnData,ColumnType,iColWidth,iMillCode,SSupCode);
                addTab("Gate Inward Without RDC",GatePanel);
                addTab("Gate Inward against RDC",RdcPanel);
                addTab("RDC Inward",MiddlePanel);
                GatePanel.setBorder(new TitledBorder(""));
                RdcPanel.setBorder(new TitledBorder(""));
                MiddlePanel.setBorder(new TitledBorder("Pending RDC"));
                setSelectedIndex(1);
          }
          catch(Exception ex)
          {
                System.out.println("RDCInvMiddlePanel: "+ex);
          }
     }

     public void setVectorData()
     {
         VGDesc       = new Vector();
         VGRdcNo      = new Vector(); 
         VGRdcDate    = new Vector();
         VGPendQty    = new Vector();
         VGRecQty     = new Vector();
         VGDept       = new Vector();
         VGDeptCode   = new Vector(); 
         VGUnit       = new Vector();
         VGUnitCode   = new Vector();
         VId          = new Vector();
         VGRdcQty     = new Vector();
         VRdcSlNo     = new Vector();
         VDirectWOQty = new Vector();
         VPrevWOQty   = new Vector();
         VPrevFreeQty = new Vector();
         VRemarks     = new Vector();
         VMemoUserCode= new Vector();
		 
		 VItemCode	  = new Vector()	;
		 VItemName	  = new Vector()	;
		 
		 VBinName	  = new Vector()	;
		 
	     iJmdAppStatus=0;



         String QS1= "";

         QS1 =   " SELECT RDC.Descript, RDC.RDCNo, RDC.RdcDate, "+
                 " RDC.Qty-RDC.RecQty AS Pending, "+
                 " Dept.Dept_Name, RDC.Dept_Code, Unit.Unit_Name, "+
                 " RDC.Unit_Code,RDC.Id, RDC.Qty, RDC.SlNo, "+
                 " RDC.DirectWOQty,RDC.WOQty,RDC.FreeQty,RDC.Remarks, "+
                 " RDC.MemoAuthUserCode,InvItems.Item_Code,InvItems.Item_Name ,InvItems.ItemLocName "+
                 " FROM RDC "+
				 " Inner Join InvItems on RDC.HSNMATCODE = InvItems.Item_Code "+
                 " INNER JOIN Dept ON RDC.Dept_Code = Dept.Dept_code "+
                 " And RDC.Sup_Code = '"+SSupCode+"' "+
                 " And RDC.RecQty < RDC.Qty  "+
                 " And RDC.DocType=0 "+
                 " And RDC.MillCode="+iMillCode+
                 " INNER JOIN Unit ON RDC.Unit_Code = Unit.Unit_Code ";

         if(iSortIndex==0)
         {
              QS1 = QS1 + " Order By 2,1 ";
         }
         if(iSortIndex==1)
         {
              QS1 = QS1 + " Order By 1,2 ";
         }

         try
         {
                ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                Connection      theConnection =  oraConnection.getConnection();
				
				
				
				
				String QS2= " select activestatus from ( "+
							" select ACTIVESTATUS,id from RDC_JMD_APPROVAL  order by id desc  "+
							" )where rownum=1 ";

				
				
                Statement       stat2 =  theConnection.createStatement();

                ResultSet res2 = stat2.executeQuery(QS2);

                while(res2.next())
                {
					iJmdAppStatus = res2.getInt(1);
				
				}
				res2.close();
				stat2.close();
				
				
				
                Statement       stat =  theConnection.createStatement();

                ResultSet res1 = stat.executeQuery(QS1);

                while(res1.next())
                {
                     VGDesc      .addElement(res1.getString(1));
                     VGRdcNo     .addElement(res1.getString(2)); 
                     VGRdcDate   .addElement(res1.getString(3));
                     VGPendQty   .addElement(res1.getString(4));
                     VGRecQty    .addElement("0");
                     VGDept      .addElement(res1.getString(5));
                     VGDeptCode  .addElement(res1.getString(6)); 
                     VGUnit      .addElement(res1.getString(7));
                     VGUnitCode  .addElement(res1.getString(8));
                     VId         .addElement(res1.getString(9));
                     VGRdcQty    .addElement(res1.getString(10));
                     VRdcSlNo    .addElement(res1.getString(11));
                     VDirectWOQty.addElement(res1.getString(12));
                     VPrevWOQty  .addElement(res1.getString(13));
                     VPrevFreeQty.addElement(res1.getString(14));
                     VRemarks    .addElement(common.parseNull(res1.getString(15)));
                     VMemoUserCode.addElement(common.parseNull(res1.getString(16)));

                     VItemCode    	.addElement(common.parseNull(res1.getString(17)));
                     VItemName		.addElement(common.parseNull(res1.getString(18)));
					 
					 VBinName		.addElement(common.parseNull(res1.getString(19)));
					 
                }
                res1.close();
                stat.close();
         }
         catch(Exception ex)
         {
             System.out.println("E1"+ex );
         }
     }

     public boolean setRowData()
     {
         RowData = new Object[1][ColumnData.length];

         RowData[0][0]  = "";
         RowData[0][1]  = "";
         RowData[0][2]  = "";
         RowData[0][3]  = "";
         RowData[0][4]  = "";
         RowData[0][5]  = "";
         RowData[0][6]  = "";
         RowData[0][7]  = "";

         return true;
     }

     public boolean setRData()
     {
         RData = new Object[VGDesc.size()][CData.length];
		 
		 System.out.println("VGDesc-->"+VGDesc.size());

         for(int i=0;i<VGDesc.size();i++)
         {
              RData[i][0]  = (String)VGDesc.elementAt(i);
              RData[i][1]  = (String)VGRdcNo.elementAt(i);
              RData[i][2]  = common.parseDate((String)VGRdcDate.elementAt(i));
              RData[i][3]  = (String)VGRdcQty.elementAt(i);
              RData[i][4]  = (String)VGPendQty.elementAt(i);
              RData[i][5]  = "";
              RData[i][6]  = (String)VDirectWOQty.elementAt(i);
              RData[i][7]  = (String)VPrevWOQty.elementAt(i);
              RData[i][8]  = (String)VPrevFreeQty.elementAt(i);
              RData[i][9]  = "";
              RData[i][10] = "";
              RData[i][11] = new Boolean(false);
              RData[i][12] = "";
			  
			  RData[i][13] = (String)VItemName.elementAt(i);
			  RData[i][14] = (String)VItemCode.elementAt(i);
			  RData[i][15] = (String)VRemarks.elementAt(i);
			  RData[i][16] = (String)VBinName.elementAt(i);
			  
			  RData[i][17] = new Boolean(false);
			  RData[i][18] = new Boolean(false);
			  
			  RData[i][19] = new Boolean(false);
			  
         }
         return true;
     }

     public boolean setGData()
     {
         GData = new Object[1][CGData.length];

         GData[0][0] = "";
         GData[0][1] = "";

         return true;
     }

     public String getRdcQty(int i)
     {
            return (String)VGRdcQty.elementAt(i);
     }
     public String getRdcSLNo(int i)
     {
            return (String)VRdcSlNo.elementAt(i);
     }
     public String getMemoUserCode(int i)
     {
            return (String)VMemoUserCode.elementAt(i);
     }


}
