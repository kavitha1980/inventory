package RDC;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectRDCPanel extends JPanel
{
     JTable         RdcTable;
     DirectRDCModel RdcModel;
  
     JPanel         GridPanel,BottomPanel,ControlPanel;
     JPanel         GridBottom;
  
     JLayeredPane   DeskTop;
     Object         RData[][];
     String         CData[],CType[];
     int            iCWidth[];
     int            iMillCode;
     String         SSupCode,SSupName;
     Vector         VSeleGINo;
     DirectRDCMiddlePanel  MiddlePanel;
  
     JTextField     TFind,TFind1;

     Common common = new Common();
	 
	 int iJmdStatus=0;
  
     DirectRDCPanel(JLayeredPane DeskTop,Object RData[][],String CData[],String CType[],int iCWidth[],int iMillCode,String SSupCode,String SSupName,DirectRDCMiddlePanel MiddlePanel)
     {
          this.DeskTop      = DeskTop;
          this.RData        = RData;
          this.CData        = CData;
          this.CType        = CType;
          this.iCWidth      = iCWidth;
          this.iMillCode    = iMillCode;
          this.SSupCode     = SSupCode;
          this.SSupName     = SSupName;
          this.MiddlePanel  = MiddlePanel;
 
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setReportTable();
     }
	 
     DirectRDCPanel(JLayeredPane DeskTop,Object RData[][],String CData[],String CType[],int iCWidth[],int iMillCode,String SSupCode,String SSupName,DirectRDCMiddlePanel MiddlePanel,int iJmdStatus)
     {
          this.DeskTop      = DeskTop;
          this.RData        = RData;
          this.CData        = CData;
          this.CType        = CType;
          this.iCWidth      = iCWidth;
          this.iMillCode    = iMillCode;
          this.SSupCode     = SSupCode;
          this.SSupName     = SSupName;
          this.MiddlePanel  = MiddlePanel;
		  this.iJmdStatus	= iJmdStatus;
		  
  		  System.out.println("iJmdStatus"+iJmdStatus);

 
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setReportTable();
  }
	 
     public void createComponents()
     {
          GridPanel        = new JPanel(true);
          GridBottom       = new JPanel(true);
          BottomPanel      = new JPanel();
          ControlPanel     = new JPanel();
 
          TFind            = new JTextField();
          TFind1           = new JTextField();
     }
          
     public void setLayouts()
     {
         //GridPanel.setLayout(new GridLayout(1,1));
         GridPanel.setLayout(new BorderLayout());
         BottomPanel.setLayout(new BorderLayout());
         ControlPanel.setLayout(new GridLayout(1,3));
         GridBottom.setLayout(new BorderLayout());         
     }
     public void addComponents()
     {
         ControlPanel.add(new JLabel("Name Find"));
         ControlPanel.add(TFind);
         ControlPanel.add(new JLabel(""));
     }
     public void addListeners()
     {
          TFind   . addKeyListener(new NameKeyList());
     }
     public class ActList implements ActionListener
     {
            public void actionPerformed(ActionEvent ae)
            {
            }
     }
     public void setReportTable()
     {
         RdcModel      = new DirectRDCModel(RData,CData,CType);
         RdcTable      = new JTable(RdcModel);
         RdcTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
         setPrefferedColumnWidth();

         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<RdcTable.getColumnCount();col++)
         {
               if(CType[col]=="N" || CType[col]=="B")
                    RdcTable.getColumn(CData[col]).setCellRenderer(cellRenderer);
         }
         //RdcTable.setShowGrid(false);

         setLayout(new BorderLayout());
         GridBottom.add(RdcTable.getTableHeader(),BorderLayout.NORTH);
         GridBottom.add(new JScrollPane(RdcTable),BorderLayout.CENTER);

         GridPanel.add(ControlPanel,BorderLayout.NORTH);
         GridPanel.add(GridBottom,BorderLayout.CENTER);

         add(BottomPanel,BorderLayout.SOUTH);
         add(GridPanel,BorderLayout.CENTER);

         RdcTable.addKeyListener(new KeyList1());
         RdcTable.addMouseListener(new MouseList1());
     }

     public void setPrefferedColumnWidth()
     {
          if(iCWidth.length != CData.length)
               return;
          for(int i=0;i<CData.length;i++)
          {
               if(iCWidth[i]>0)
                    (RdcTable.getColumn(CData[i])).setPreferredWidth(iCWidth[i]);
          }
     }

     public class NameKeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String    SFind     = TFind.getText();
               int       i         = nameIndexOf(SFind);
               if (i>-1)
               {
                              RdcTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = RdcTable.getCellRect(i,0,true);
                              RdcTable.scrollRectToVisible(cellRect);
               }
          }
     }
     public int nameIndexOf(String SFind)
     {
          SFind = SFind.toUpperCase();

          for(int i=0;i<RdcModel.getRows();i++)
          {
               String str  = (String)RdcModel.getValueAt(i,0);

               int iIndex = str.indexOf(SFind);

               if(iIndex>=0)
                    return i;
          }
          return -1;
     }

     private class KeyList1 extends KeyAdapter
     {
         public void keyPressed(KeyEvent ke)
         {
            if(ke.getKeyCode()==KeyEvent.VK_SPACE)
            {
                if(validSeleData())
                {
                     setKeyRDCData();
                }
            }
         }
     }

     public class MouseList1 extends MouseAdapter
     {
          public void mouseReleased(MouseEvent me)
          {
               if(me.getClickCount()==1)
               {
                    if(validSeleData())
                    {
                         setMouseRDCData();
                    }
               }
          }
     }

/*     private boolean validSeleData()
     {
         int iCol     = RdcTable.getSelectedColumn();

         if(iCol!=11)
             return false;
         
         int iRow     = RdcTable.getSelectedRow();

         String SQty    = (String)RdcModel.getValueAt(iRow,5);

         if(common.toDouble(SQty)<=0)
         {
             RdcModel.setValueAt(new Boolean(false),iRow,11);
             JOptionPane.showMessageDialog(null,"Invalid Recd Qty","Error",JOptionPane.ERROR_MESSAGE);
             return false;
         }

         double dPendQty   = common.toDouble((String)RdcModel.getValueAt(iRow,4));
         double dCurRecQty = common.toDouble(SQty);

         if(dCurRecQty>dPendQty)
         {
             RdcModel.setValueAt(new Boolean(false),iRow,11);
             JOptionPane.showMessageDialog(null,"Quantity Mismatch","Error",JOptionPane.ERROR_MESSAGE);
             return false;
         }

         return true;
     }
*/	 
     private boolean validSeleData()
     {
         int iCol     = RdcTable.getSelectedColumn();
		 
		 		 System.out.println("iCol"+iCol);


         if(iCol!=11 && iCol!=19)
             return false;
		 
		 int iRow     	= RdcTable.getSelectedRow();
		 
		 System.out.println("iJmdStatus"+iJmdStatus);
		 
		 if(iJmdStatus==1 && iCol==19) {
			 
             RdcModel.setValueAt(new Boolean(false),iRow,19);
             JOptionPane.showMessageDialog(null,"This Option Blocked by JMD..","Error",JOptionPane.ERROR_MESSAGE);
             return false;
		 }
		 
         
         if(iCol==11) {
			 
			 
			 String SQty    = (String)RdcModel.getValueAt(iRow,5);

			 if(common.toDouble(SQty)<=0)
			 {
				 RdcModel.setValueAt(new Boolean(false),iRow,11);
				 JOptionPane.showMessageDialog(null,"Invalid Recd Qty","Error",JOptionPane.ERROR_MESSAGE);
				 return false;
			 }

			 double dPendQty   = common.toDouble((String)RdcModel.getValueAt(iRow,4));
			 double dCurRecQty = common.toDouble(SQty);

			 if(dCurRecQty>dPendQty)
			 {
				 RdcModel.setValueAt(new Boolean(false),iRow,11);
				 JOptionPane.showMessageDialog(null,"Quantity Mismatch","Error",JOptionPane.ERROR_MESSAGE);
				 return false;
			 }
			 
		 }


         return true;
     }
	 

     private void setKeyRDCData()
     {
          try
          {
               int iRow     = RdcTable.getSelectedRow();

               Boolean bValue = (Boolean)RdcModel.getValueAt(iRow,11);

               if(!bValue.booleanValue())
               {
                    addRdcRow(iRow);
               }
               else
               {
                    deleteRdcRow(iRow);
               }
          }
          catch(Exception ex){}
     }

     private void setMouseRDCData()
     {
          try
          {
               int iRow     = RdcTable.getSelectedRow();

               Boolean bValue = (Boolean)RdcModel.getValueAt(iRow,11);

               if(bValue.booleanValue())
               {
                    addRdcRow(iRow);
               }
               else
               {
                    deleteRdcRow(iRow);
               }
          }
          catch(Exception ex){}
     }


     private void addRdcRow(int iRow)
     {
          try
          {
               String SDesc      = (String)RdcModel.getValueAt(iRow,0);
               String SPendQty   = (String)RdcModel.getValueAt(iRow,4);
               String SQty       = (String)RdcModel.getValueAt(iRow,5);

               double dRecQty     = common.toDouble(SQty);
               double dPendQty    = common.toDouble(SPendQty);
               double dPrevRecQty = common.toDouble((String)MiddlePanel.VGRecQty.elementAt(iRow));
               double dTotRecQty  = dRecQty + dPrevRecQty;
               MiddlePanel.VGRecQty.setElementAt(String.valueOf(dTotRecQty),iRow);
 
               Vector VEmpty = new Vector();
 
               VEmpty.addElement((String)MiddlePanel.VGDesc.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGRdcNo.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGRdcQty.elementAt(iRow));
               VEmpty.addElement(""+dPendQty);
               VEmpty.addElement(SQty);
               VEmpty.addElement(""+dRecQty);
               VEmpty.addElement((String)MiddlePanel.VGDept.elementAt(iRow));
               VEmpty.addElement((String)MiddlePanel.VGUnit.elementAt(iRow));

               MiddlePanel.MiddlePanel.dataModel.addRow(VEmpty);
               MiddlePanel.VSeleId.addElement((String)MiddlePanel.VId.elementAt(iRow));
          }
          catch(Exception ex){}
     }

     private void deleteRdcRow(int iRow)
     {
          try
          {
               String SDesc      = (String)RdcModel.getValueAt(iRow,0);
               String SId        = (String)MiddlePanel.VId.elementAt(iRow);

               Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();

               int iRdcRow        = getRowIndex(SId,RowData);

               if(iRdcRow>=0)
               {
                    String  SRecQty = (String)RowData[iRdcRow][5];

                    double dRecQty     = common.toDouble(SRecQty);
                    double dPrevRecQty = common.toDouble((String)MiddlePanel.VGRecQty.elementAt(iRow));
                    double dTotRecQty  = dPrevRecQty - dRecQty;
                    MiddlePanel.VGRecQty.setElementAt(String.valueOf(dTotRecQty),iRow);
      
                    MiddlePanel.VSeleId.removeElementAt(iRdcRow);

                    MiddlePanel.MiddlePanel.dataModel.removeRow(iRdcRow);
               }
          }
          catch(Exception ex){}
     }

     private int getRowIndex(String SId,Object RowData[][])
     {
          int iIndex=-1;
          
          for(int i=0;i<RowData.length;i++)
          {
               String SSeleId = (String)MiddlePanel.VSeleId.elementAt(i);
               
               if(!SSeleId.equals(SId))
                    continue;
               
               iIndex=i;
               return iIndex;
          }
          return iIndex;
     }

     public Object[][] getFromVector()
     {
          return RdcModel.getFromVector();     
     }


}

