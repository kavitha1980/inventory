package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectWorkOrderFrame extends JInternalFrame
{
     
     JPanel           TopPanel,TopLeft,TopRight,BottomPanel;
     WOMiddlePanel    MiddlePanel;

     JButton          BOk,BCancel,BSupplier;
     JButton          BApply;

     NextField        TOrderNo;
     JTextField       TSupCode;
     MyTextField      TRef,TPayTerm;
     NextField        TAdvance;
     DateField        TDate;
     MyComboBox       JCTo,JCThro,JCForm;
     String           SOrderNo="";

     JLayeredPane     DeskTop;
     StatusPanel      SPanel;
     int              iUserCode;
     int              iMillCode;
     String           SSupTable,SYearCode;

     boolean          bflag;                 

     Common common    = new Common();
     ORAConnection connect;
     Connection theConnect = null;

     Vector           VTo,VThro,VToCode,VThroCode,VFormCode,VForm;

     Vector           VPendCode,VPendName,VRdcNo,VPendDetails,VSlNo,VRdcQty,VPrevQty,VPendQty,VWOQty;
     Vector           VRDept,VRGroup,VRUnit,VMemoUserCode;

     int       iMaxSlNo  = 0;
     int       iCount=0;

     boolean             bComflag = true;

     DirectWorkOrderFrame(JLayeredPane DeskTop,StatusPanel SPanel,boolean bflag,int iUserCode,int iMillCode,String SSupTable,String SYearCode)
     {
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.bflag     = bflag;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }

     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnect     =    oraConnection.getConnection();
     }

     public void createComponents()
     {
          TopPanel    = new JPanel();
          TopLeft     = new JPanel();
          TopRight    = new JPanel();
          BottomPanel = new JPanel();

          BOk         = (bflag?new JButton("Update"):new JButton("Save"));
          BCancel     = new JButton("Abort");

          BSupplier   = new JButton("Supplier");
          BApply      = new JButton("Apply");

          TOrderNo    = new NextField();
          TDate       = new DateField();
          TSupCode    = new JTextField();
          TAdvance    = new NextField(10);
          TPayTerm    = new MyTextField(40);
          TRef        = new MyTextField(30);

          getVTo();

          JCTo        = new MyComboBox(VTo);
          JCThro      = new MyComboBox(VThro);
          JCForm      = new MyComboBox(VForm);

          TDate.setTodayDate();
          
          TDate.setEditable(false);
          TOrderNo.setEditable(false);
          BSupplier.setMnemonic('U');
          BApply.setMnemonic('A');
          BCancel.setMnemonic('C');
          if(bflag)
          {
               BOk.setMnemonic('U');
          }
          else
          {
               BOk.setMnemonic('S');
          }
     }


     public void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(1,2));
          TopRight.setLayout(new GridLayout(5,2));

          if(bflag)
          {
               setTitle("Amendment Or Modification to Work Order");
               TopLeft   .setLayout(new GridLayout(4,2));
          }
          else
          {
               setTitle("Work Order Placement");
               TopLeft.setLayout(new GridLayout(5,2));
          }

          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,790,500);

          getContentPane()    .setLayout(new BorderLayout());
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          TopLeft   .add(new JLabel("Order No"));
          TopLeft   .add(TOrderNo);

          TopLeft   .add(new JLabel("Order Date"));
          TopLeft   .add(TDate);

          TopLeft   .add(new JLabel("Reference"));
          TopLeft   .add(TRef);

          TopLeft   .add(new JLabel("Supplier"));
          TopLeft   .add(BSupplier);

          if(!bflag)
          {
               TopLeft   .add(new JLabel("Materials"));
               TopLeft   .add(BApply);
          }

          TopRight  .add(new JLabel("Book To"));
          TopRight  .add(JCTo);

          TopRight  .add(new JLabel("Book Thro"));
          TopRight  .add(JCThro);

          TopRight  .add(new JLabel("Form No"));
          TopRight  .add(JCForm);

          TopRight  .add(new JLabel("Advance"));
          TopRight  .add(TAdvance);

          TopRight  .add(new JLabel("Payment Terms"));
          TopRight  .add(TPayTerm);

          TopPanel  .add(TopLeft);
          TopPanel  .add(TopRight);

          TopLeft   .setBorder(new TitledBorder("Order Id Block-I"));
          TopRight  .setBorder(new TitledBorder("Order Id Block-II"));

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          TOrderNo.setText(getNextOrderNo());  
     }

     public void addListeners()
     {
          BSupplier.addActionListener(new SupplierSearch(DeskTop,TSupCode,SSupTable));
          BApply.addActionListener(new ApplyList());

          if(bflag)
          {
               //BOk       .addActionListener(new UpdtList());
          }
          else 
          {
               BOk       .addActionListener(new ActList());
          }
          BCancel   .addActionListener(new CanList());
     }
     public class CanList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               removeHelpFrame();
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setMiddlePanel();
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(common.toInt(TOrderNo.getText())>0)
               {
                    if(checkData())
                    {
                         iCount=0;
                         BOk.setEnabled(true);
                         insertOrderDetails();
                         if(iCount>0)
                             updateOrderNo();

                         getACommit();
                         removeHelpFrame();
                    }
               }
          }
     }

     public void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnect     . commit();
                    System         . out.println("Commit");
                    theConnect     . setAutoCommit(true);
               }
               else
               {
                    theConnect     . rollback();
                    JOptionPane.showMessageDialog(null,"Problem in Saving Data","Error",JOptionPane.ERROR_MESSAGE);
                    System         . out.println("RollBack");
                    theConnect     . setAutoCommit(true);
               }
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void insertOrderDetails()
     {

          String QString = "Insert Into WorkOrder (ID,OrderNo,OrderDate,Sup_Code,Reference,Advance,PayTerms,ToCode,ThroCode,FormCode,Descript,RDCNo,Qty,Rate,DiscPer,Disc,CenvatPer,CenVat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,Unit_Code,Dept_Code,Group_Code,DueDate,SlNo,UserCode,ModiDate,RDCSlNo,MillCode,LogBookNo,Remarks,WODesc,EntryStatus,DocId,MemoAuthUserCode) Values (";

          int iSlNo = 0;
          int iMemoUserCode = 1;

          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat      = theConnect.createStatement();

               TOrderNo.setText(getNextOrderNo());  

               if(bflag)
                    iMaxSlNo = getMaxSlNo(((String)TOrderNo.getText()).trim());
               else
                    iMaxSlNo = 0;


               int k = iMaxSlNo;

               Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
               String SAdd       = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess      = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm        = common.toDouble(SAdd)-common.toDouble(SLess);
               double dBasic     = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio     = dpm/dBasic;

               for(int i=0;i<FinalData.length;i++)
               {
                    try
                    {
                         if(!isFreshRecord(i))
                              continue;
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
                  
                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SDueDate   = MiddlePanel.MiddlePanel.getDueDate(i,TDate);

                    dBasic            = common.toDouble(((String)FinalData[i][12]).trim());

                    if(dBasic == 0)
                         continue;

                    String SMisc      = common.getRound(dBasic*dRatio,3);

                    if(MiddlePanel.SlData.length>i)
                    {
                         iSlNo = common.toInt(common.parseNull((String)MiddlePanel.SlData[i]));
                    }
                    else
                    {
                         iSlNo = 0;
                    }

                    String SRDCNo = ((String)FinalData[i][1]).trim();

                    String SEntryStatus="2";

                    String SRDCDesc = "";
                    String SWODesc  = "";

                    SRDCDesc = (((String)FinalData[i][0]).trim()).toUpperCase();
                    SWODesc  = (((String)FinalData[i][2]).trim()).toUpperCase();


                    if(SRDCDesc.equals(""))
                    {
                         SRDCDesc = SWODesc;
                         SWODesc  = "";
                    }

                    iMemoUserCode = common.toInt((String)VMemoUserCode.elementAt(i));

                    if(iMemoUserCode<=0)
                    {
                         iMemoUserCode = 1;
                    }

                    String QS1 = QString;
                    QS1 = QS1+"WorkOrder_Seq.nextval,";
                    QS1 = QS1+"0"+TOrderNo.getText()+",";
                    QS1 = QS1+"'"+TDate.toNormal()+"',";
                    QS1 = QS1+"'"+TSupCode.getText()+"',";
                    QS1 = QS1+"'"+(TRef.getText()).toUpperCase()+"',";    
                    QS1 = QS1+"0"+TAdvance.getText()+",";
                    QS1 = QS1+"'"+(TPayTerm.getText()).toUpperCase()+"',";
                    QS1 = QS1+"0"+(String)VToCode.elementAt(JCTo.getSelectedIndex())+",";
                    QS1 = QS1+"0"+(String)VThroCode.elementAt(JCThro.getSelectedIndex())+",";
                    QS1 = QS1+"0"+(String)VFormCode.elementAt(JCForm.getSelectedIndex())+",";
                    QS1 = QS1+"'"+common.getNarration(SRDCDesc)+"',";
                    QS1 = QS1+"0"+SRDCNo+",";
                    QS1 = QS1+"0"+((String)FinalData[i][6]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][7]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][8]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][13]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][9]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][14]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][10]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][15]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][11]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][16]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][17]).trim()+",";
                    QS1 = QS1+"0"+SAdd+",";
                    QS1 = QS1+"0"+SLess+",";
                    QS1 = QS1+"0"+SMisc+",";
                    QS1 = QS1+"0"+SUnitCode+",";
                    QS1 = QS1+"0"+SDeptCode+",";
                    QS1 = QS1+"0"+SGroupCode+",";
                    QS1 = QS1+"'"+SDueDate+"',";
                    QS1 = QS1+(k+1)+",";
                    QS1 = QS1+"0"+iUserCode+",";
                    QS1 = QS1+"'"+common.getServerDateTime2()+"',";
                    QS1 = QS1+iSlNo+",";
                    QS1 = QS1+iMillCode+",";
                    QS1 = QS1+"0"+((String)FinalData[i][23]).trim()+",";
                    QS1 = QS1+"'"+(((String)FinalData[i][22]).trim()).toUpperCase()+"',";
                    QS1 = QS1+"'"+common.getNarration(SWODesc)+"',";
                    QS1 = QS1+"0"+SEntryStatus+",";
                    QS1 = QS1+"0"+((String)FinalData[i][24]).trim()+",";
                    QS1 = QS1+"0"+iMemoUserCode+")";

                    stat.executeUpdate(QS1);

                    String QS2 = " Update RDC set WOStatus=1,DirectWOQty=DirectWOQty+("+((String)FinalData[i][6]).trim()+")"+
                                 " Where RdcNo="+SRDCNo+" and SlNo="+iSlNo+
                                 " and MillCode="+iMillCode+" and Descript='"+((String)FinalData[i][0]).trim()+"'";

                    stat.executeUpdate(QS2);

                    k++;
                    iCount++;
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void updateOrderNo()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat      = theConnect.createStatement();

               String QS1 = " Update Config"+iMillCode+""+SYearCode+" Set MaxNo=MaxNo+1 Where ID=11 ";
               stat.executeUpdate(QS1);
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public boolean isFreshRecord(int i)
     {
          try
          {
               if(MiddlePanel.IdData==null)
                    return true;

               boolean bfresh = (i<MiddlePanel.IdData.length?false:true);
               return bfresh;
          }
          catch(Exception ex)
          {
               return true;
          }
     }

     public int getMaxSlNo(String SOrderNo)
     {
          int iMaxNo=0;

          String QS = " Select Max(SlNo) from WorkOrder Where OrderNo="+SOrderNo+" and MillCode="+iMillCode;

          iMaxNo = common.toInt(common.getID(QS));
           
          return iMaxNo;
     }

     public String getNextOrderNo()
     {
          if(bflag)
               return TOrderNo.getText();
               
          String QS = "";

          QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" "+
               " Where ID=11 ";

          String SOrderNo = String.valueOf(common.toInt(common.getID(QS))+1);
          return SOrderNo;
     }
     public void getVTo()
     {
          VTo       = new Vector();
          VToCode   = new Vector();
          VThro     = new Vector();
          VThroCode = new Vector();
          VFormCode = new Vector();
          VForm     = new Vector();

          try
          {
               Statement stat   = theConnect.createStatement();
               ResultSet res = stat.executeQuery("Select ToName,ToCode From BookTo Order By 1");
               while(res.next())
               {
                    VTo.addElement(res.getString(1));
                    VToCode.addElement(res.getString(2));
               }
               res.close();
               res = stat.executeQuery("Select ThroName,ThroCode From BookThro Order By 1");
               while(res.next())
               {
                    VThro.addElement(res.getString(1));
                    VThroCode.addElement(res.getString(2));
               }
               res.close();
               res = stat.executeQuery("Select FormNo,FormCode From STForm Order By 1");
               while(res.next())
               {
                    VForm.addElement(res.getString(1));
                    VFormCode.addElement(res.getString(2));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void setMiddlePanel()
     {
          String SSupCode = TSupCode.getText();

          if(SSupCode.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Please Select the Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
          }
          else
          {
               setDataVector();

               if(VPendName.size()<=0)
               {
                    JOptionPane.showMessageDialog(null,"There is no Pending for this Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
               }
               else
               {
                    BSupplier.setEnabled(false);
                    BApply.setEnabled(false);

                    MiddlePanel = new WOMiddlePanel(DeskTop,VPendName,VRdcNo,VSlNo,VRdcQty,VPrevQty,VPendQty,VWOQty,VRDept,VRGroup,VRUnit,iMillCode,true);

                    getContentPane().add(MiddlePanel,BorderLayout.CENTER);
                    DeskTop.updateUI();
               }
          }

     }

     public void setDataVector()
     {
          VPendName     = new Vector();
          VRdcNo        = new Vector();
          VSlNo         = new Vector();
          VRdcQty       = new Vector();
          VPrevQty      = new Vector();
          VPendQty      = new Vector();
          VWOQty        = new Vector();
          VRDept        = new Vector();
          VRUnit        = new Vector();
          VRGroup       = new Vector();
          VMemoUserCode = new Vector();

          String QS = " Select Descript,RNo,SlNo,Qty,WOQty,Dept_Name,Unit_Name,MemoAuthUserCode from ( "+
                      " Select Descript,RDCNo as RNo,SlNo,Sum(Qty) as Qty, "+
                      " Sum(WOQty+DirectWOQty+FreeQty) as WOQty,Dept.Dept_Name,"+
                      " Unit.Unit_Name,RDC.MemoAuthUserCode From RDC "+
                      " Inner Join Dept on RDC.Dept_Code=Dept.Dept_Code "+
                      " and DocType=0 and RDC.MillCode="+iMillCode+
                      " and Sup_Code = '"+TSupCode.getText()+"' "+
                      " and RDC.Qty>RDC.RecQty "+
                      " Inner Join Unit on RDC.Unit_Code=Unit.Unit_Code "+
                      " Group by Descript,RDCNo,SlNo,Dept.Dept_Name,Unit.Unit_Name,RDC.MemoAuthUserCode) "+
                      " Where Qty>WOQty "+
                      " Order By Descript,RNo ";

          try
          {
               Statement stat      = theConnect.createStatement();
               ResultSet theResult = stat.executeQuery(QS);

               while(theResult.next())
               {
                    double dRDCQty  = theResult.getDouble(4);
                    double dPrevQty = theResult.getDouble(5);
                    double dPendQty = dRDCQty - dPrevQty;

                    VPendName    .addElement(theResult.getString(1));
                    VRdcNo       .addElement(common.parseNull(String.valueOf(theResult.getInt(2))));
                    VSlNo        .addElement(common.parseNull(String.valueOf(theResult.getInt(3))));
                    VRdcQty      .addElement(String.valueOf(dRDCQty));
                    VPrevQty     .addElement(String.valueOf(dPrevQty));
                    VPendQty     .addElement(String.valueOf(dPendQty));
                    VWOQty       .addElement("");
                    VRDept       .addElement(common.parseNull(theResult.getString(6)));
                    VRUnit       .addElement(common.parseNull(theResult.getString(7)));
                    VRGroup      .addElement("REPAIRS AND RENEWALS");
                    VMemoUserCode.addElement(common.parseNull(theResult.getString(8)));
               }
               theResult.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void fillData(String SOrderNo)
     {
          try
          {
               /*MiddlePanel = new WOMiddlePanel(DeskTop,iMillCode);
               new WorkOrderResultSet(SOrderNo,TOrderNo,TDate,BSupplier,TSupCode,TRef,TAdvance,TPayTerm,JCTo,JCThro,JCForm,MiddlePanel,VToCode,VThroCode,VFormCode,this,iMillCode,SSupTable);
               getContentPane().add(MiddlePanel,BorderLayout.CENTER);
               DeskTop.updateUI();*/
          }
          catch(Exception ex)
          {
               System.out.println("6"+ex);
               ex.printStackTrace();
          }
     }

     public boolean checkData()
     {
          String SOrderDate = TDate.toNormal();
          String SSupCode   = TSupCode.getText();
	    int iRDCAuthStatus= 0;

          int iRows=0;

          if(SOrderDate.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Order Date Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TDate.TDay.requestFocus();
               return false;
          }
          if(SSupCode.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Select Supplier","Error",JOptionPane.ERROR_MESSAGE);
               BSupplier.setEnabled(true);
               BSupplier.requestFocus();
               return false;
          }
          try
          {
               iRows    = MiddlePanel.MiddlePanel.getRows();
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"No Materials Selected","Error",JOptionPane.ERROR_MESSAGE);
               BApply.setEnabled(true);
               BApply.requestFocus();
               return false;
          }

          int iRowCount=0;

          Object FData[][] = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<FData.length;i++)
          {
               double dPendQty = common.toDouble(((String)FData[i][5]).trim());
               double dQty     = common.toDouble(((String)FData[i][6]).trim());
               double dRate    = common.toDouble(((String)FData[i][7]).trim());
               String SDesc1   = ((String)FData[i][0]).trim();
		   String SRDCNo   = (common.parseNull((String)FData[i][1])).trim();
	         iRDCAuthStatus	 = getRDCAuthStatus(SDesc1.trim(),SRDCNo);

               if(dQty<=0 && dRate<=0)
                    continue;


               if(dQty>dPendQty)
               {
                    JOptionPane.showMessageDialog(null,"Invalid Qty @Row - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(dQty>0 && dRate<=0)
               {
                    JOptionPane.showMessageDialog(null,"Invalid Rate @Row - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(dQty<=0 && dRate>0)
               {
                    JOptionPane.showMessageDialog(null,"Invalid Qty @Row - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }


               String SDueDate   = MiddlePanel.MiddlePanel.getDueDate(i,TDate);

               if(common.toInt(SDueDate)<common.toInt((String)TDate.toNormal()))
               {
                    JOptionPane.showMessageDialog(null,"Due Date Greater than Order Date","Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

//               String SDesc1 = ((String)FData[i][0]).trim();
               String SDesc2 = (common.parseNull((String)FData[i][2])).trim();

               if(SDesc1.equals("") && SDesc2.equals("") && dQty>0)
               {
                    JOptionPane.showMessageDialog(null,"Invalid Description @Row - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(bflag)
               {
                    if(i<MiddlePanel.IdData.length && dQty<=0)
                    {
                         JOptionPane.showMessageDialog(null,"Invalid Quantity @Row-"+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }
               }
    	     /*  if(iRDCAuthStatus==0){
				System.out.println("false");
			System.out.println("Inside Else ----------->"+iRDCAuthStatus+"--"+SRDCNo);
			JOptionPane.showMessageDialog(null," This RDC No ("+SRDCNo+" )  is Not Authenticated","Error",JOptionPane.ERROR_MESSAGE);
	            return false;
		   }*/
               iRowCount++;
          }

          if(iRowCount<=0)
          {
               JOptionPane.showMessageDialog(null,"No Rows Filled","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          return true;
     }

private int getRDCAuthStatus(String sDescription, String sRDCNo){
int iRDCAuthStatus=-1;
try{
        StringBuffer sb     =new StringBuffer(); 
        
        sb.append("  Select RDCAuthStatus from RDC ");
	  sb.append("  Where RDCNo='"+ sRDCNo+"'  and Descript ='"+sDescription+"'");
        
        System.out.println("GetRDCAuthStatus Qry:"+sb.toString());
        if (theConnect==null)
              {
                  ORAConnection jdbc  = ORAConnection.getORAConnection();
                  theConnect         = jdbc.getConnection();
              }
        PreparedStatement ps          = theConnect.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
    
       while (rst.next())
         {
                    iRDCAuthStatus    = rst.getInt(1);
         }
            rst.close();
            ps.close();
            ps=null;
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("getNoofEmp:"+e);
    }
    return iRDCAuthStatus;

}



}
