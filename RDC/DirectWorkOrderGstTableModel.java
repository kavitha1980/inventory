package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.sql.*;
import jdbc.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class DirectWorkOrderGstTableModel extends DefaultTableModel
{
        Object    RowData[][],ColumnNames[],ColumnType[];
        JLabel    LBasic,LDiscount,LCGST,LSGST,LIGST,LCess,LNet;
        NextField TAdd,TLess;
        Common common = new Common();
        
        
        
        public DirectWorkOrderGstTableModel(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType,JLabel LBasic,JLabel LDiscount,JLabel LCGST,JLabel LSGST,JLabel LIGST, JLabel LCess,NextField TAdd,NextField TLess,JLabel LNet)
        {
            super(RowData,ColumnNames);
            this.RowData     = RowData;
            this.ColumnNames = ColumnNames;
            this.ColumnType  = ColumnType;
            this.LBasic      = LBasic;
            this.LDiscount   = LDiscount;
            this.LCGST       = LCGST;
            this.LSGST       = LSGST;  
            this.LIGST       = LIGST;
            this.LCess       = LCess;
            this.TAdd        = TAdd;
            this.TLess       = TLess;
            this.LNet        = LNet;

            for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
                setMaterialAmount(curVector);
            }

        }
        public DirectWorkOrderGstTableModel(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType)
        {
            super(RowData,ColumnNames);
            this.RowData     = RowData;
            this.ColumnNames = ColumnNames;
            this.ColumnType  = ColumnType;
        }

       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }            
       public boolean isCellEditable(int row,int col)
       {
               if(ColumnType[col]=="B" || ColumnType[col]=="E")
                  return true;
               return false;
       }
       public void setValueAt(Object aValue, int row, int column)
       {
       
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   if(column==24) // 
                   {
                        String str = ((String)aValue).trim();
                        aValue = common.getDate(str,0,1);
                   }
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
                   if(column>=6 && column<=11)
                      setMaterialAmount(rowVector);
                      
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }
      public void setMaterialAmount(Vector RowVector)
      {
              if(LBasic==null)
                    return;
              
              double dTGross = 0, dTDisc = 0, dTCgst = 0, dTSgst = 0, dTIgst = 0, dTCess = 0, dTNet = 0;

              double dQty        = common.toDouble(common.parseNull((String)RowVector.elementAt(8)));
              double dRate       = common.toDouble(common.parseNull((String)RowVector.elementAt(9)));
              double dDiscPer    = common.toDouble(common.parseNull((String)RowVector.elementAt(10)));
              double dCgstPer    = common.toDouble(common.parseNull((String)RowVector.elementAt(11)));
              double dSgstPer    = common.toDouble(common.parseNull((String)RowVector.elementAt(12)));
              double dIgstPer    = common.toDouble(common.parseNull((String)RowVector.elementAt(13)));
              double dCessPer    = common.toDouble(common.parseNull((String)RowVector.elementAt(14)));

              double dGross  = dQty*dRate;
              double dDisc   = dGross*dDiscPer/100;
              double dCgst   = (dGross-dDisc)*dCgstPer/100;
              double dSgst   = (dGross-dDisc)*dSgstPer/100;
              double dIgst   = (dGross-dDisc)*dIgstPer/100;
              double dCess   = (dGross-dDisc)*dCessPer/100;
              double dNet    = dGross-dDisc+dCgst+dSgst+dIgst+dCess;

              RowVector.setElementAt(common.parseNull(common.getRound(dGross,2)),15);
              RowVector.setElementAt(common.parseNull(common.getRound(dDisc,2)),16);
              RowVector.setElementAt(common.parseNull(common.getRound(dCgst,2)),17);
              RowVector.setElementAt(common.parseNull(common.getRound(dSgst,2)),18);
              RowVector.setElementAt(common.parseNull(common.getRound(dIgst,2)),19);
              RowVector.setElementAt(common.parseNull(common.getRound(dCess,2)),20);
              RowVector.setElementAt(common.parseNull(common.getRound(dNet,2)),21);

              for(int i=0;i<super.dataVector.size();i++)
              {
                  Vector curVector = (Vector)super.dataVector.elementAt(i);
                  dTGross   = dTGross+common.toDouble((String)curVector.elementAt(15));
                  dTDisc    = dTDisc+common.toDouble((String)curVector.elementAt(16));
                  dTCgst    = dTCgst+common.toDouble((String)curVector.elementAt(17));
                  dTSgst    = dTSgst+common.toDouble((String)curVector.elementAt(18));
                  dTIgst    = dTIgst+common.toDouble((String)curVector.elementAt(19));
                  dTCess    = dTCess+common.toDouble((String)curVector.elementAt(20));
                  dTNet     = dTNet+common.toDouble((String)curVector.elementAt(21));
              }
              
              dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
              
              LBasic.setText(common.parseNull(common.getRound(dTGross,2)));
              LDiscount.setText(common.parseNull(common.getRound(dTDisc,2)));
              LCGST.setText(common.parseNull(common.getRound(dTCgst,2)));
              LSGST.setText(common.parseNull(common.getRound(dTSgst,2)));
              LIGST.setText(common.parseNull(common.getRound(dTIgst,2)));
              LCess.setText(common.parseNull(common.getRound(dTCess,2)));
              LNet.setText(common.parseNull(common.getRound(dTNet,2)));
    }
    
    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
                FinalData[i][j] = (String)curVector.elementAt(j);
        }
        return FinalData;
    }
    
    public int getRows()
    {
         return super.dataVector.size();
    }
    
    public Vector getCurVector(int i)
    {
         return (Vector)super.dataVector.elementAt(i);
    }
}
