package RDC;

import java.io.*;
import java.util.*;
import util.*;
public class DocPrint
{
      Vector VBody,VHead;
      String STitle,SFile;
      int iMillCode;
      String SMillName;

      int ipctr=0,ilctr=100,iLen=0;

      FileWriter FW;

      Common common = new Common();

      public DocPrint(Vector VBody,Vector VHead,String STitle,String SFile,int iMillCode,String SMillName)
      {
            this.VBody     = VBody;
            this.VHead     = VHead;
            this.STitle    = STitle;
            this.SFile     = SFile;
            this.iMillCode = iMillCode;
            this.SMillName = SMillName;

            if((SFile.trim()).length()==0)
               SFile = "1.prn";
            try
            {
                  iLen = ((String)VHead.elementAt(0)).length();
                  FW = new FileWriter(common.getPrintPath()+SFile);
                  for(int i=0;i<VBody.size();i++)
                  {
                        setHead();
                        String strl = (String)VBody.elementAt(i);
                        FW.write(strl);
                        ilctr++;
                  }
                  setFoot();
                  FW.close();
            }
            catch(Exception ex)
            {
                  System.out.println("From DocPrint"+ex);
            }
      }
      public void setHead()
      {
            if(ilctr < 63)
                  return;
            if(ipctr > 0)
                  setFoot();
            ipctr++;

            String str1 = set130Pitch(iLen)+"Company : "+SMillName+"\n";

            String str2 = "Document : "+STitle+"\n";
//            String str4 = "Page     : "+ipctr+"\n";
            String str4 = "Page     : "+ipctr+"\n";
            String str3 = common.Replicate("-",iLen)+"\n";
            try
            {
                  FW.write(str1);
                  FW.write(str2);
                  FW.write(str4);
                  FW.write(str3);
      
                  for(int i=0;i<VHead.size();i++)
                        FW.write((String)VHead.elementAt(i));
                  FW.write(str3);
                  ilctr = VHead.size()+5;
            }
            catch(Exception ex){}
      }
      public void setFoot()
      {
            try
            {
                  String str3 = common.Replicate("-",iLen)+"\n";
                  FW.write(str3);
            }
            catch(Exception ex){}
      }

     private String set80Pitch(int iPageLength)
     {
          String SPitch  = "";
          if(iPageLength<=86)
               return SPitch="P\n";
          if(iPageLength>86 && iPageLength<=107)
               return SPitch="M\n";
          if(iPageLength>107 && iPageLength<=136)
               return SPitch="g\n";
          if(iPageLength>136 && iPageLength<=153)
               return SPitch="P\n";
          if(iPageLength>153 && iPageLength<=173)
               return SPitch="M\n";
          if(iPageLength>173 && iPageLength<=192)
               return SPitch="g\n";

          return "";
     }

     private String set130Pitch(int iPageLength)
     {
          String SPitch  = "";

          if(iPageLength<=134)
               return SPitch="P\n";
          if(iPageLength>134 && iPageLength<=163)
               return SPitch="M\n";
          if(iPageLength>163 && iPageLength<=204)
               return SPitch="g\n";
          if(iPageLength>204 && iPageLength<=217)
               return SPitch="P\n";
          if(iPageLength>217 && iPageLength<=261)
               return SPitch="M\n";
          if(iPageLength>261)
               return SPitch="g\n";

          return "";
     }

}
