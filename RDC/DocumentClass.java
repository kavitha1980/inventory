package RDC;

import java.io.*;

public class DocumentClass
{
     String SPartyCode="",SDocName="",SAtt1="",SAtt2="",SAtt3="",SNarr="",SDept="",SCate="",SId="";

     public DocumentClass(String SPartyCode,String SDocName,String SAtt1,String SAtt2,String SAtt3,String SNarr,String SDept,String SCate,String SId)
     {
          this.SPartyCode     =    SPartyCode;
          this.SDocName       =    SDocName;
          this.SAtt1          =    SAtt1;
          this.SAtt2          =    SAtt2;
          this.SAtt3          =    SAtt3;
          this.SNarr          =    SNarr;
          this.SDept          =    SDept;
          this.SCate          =    SCate;
          this.SId            =    SId;
     }
}

