package RDC;

import java.awt.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

import util.*;
public class DocumentModel extends DefaultTableModel
{
     String ColumnName[] = {"ID"   ,"ENTRYDATE" ,"ALLOWEDUSERS"   ,"PARTYNAME","DOCNAME","ATTRIBUTE1","ATTRIBUTE2","ATTRIBUTE3","NARR","DEPARTMENT","CATEGORY","Click"};
     String ColumnType[] = {"N"    ,"N"         ,"S"              ,"S" ,"S"    ,"S" ,"S"      ,"S" ,"S" ,"S","S","B"};
     int  iColumnWidth[] = {25     ,60          ,90               ,120  ,80    ,80  ,80      ,80  ,100 ,80,80,50  };

     public DocumentModel()
     {
          setDataVector(getRowData(),ColumnName);
     }

     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";

          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }

}
