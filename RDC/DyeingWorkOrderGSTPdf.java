package RDC;

import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class DyeingWorkOrderGSTPdf
{


     String         SOrdNo    = "",SOrdDate  = "",SSupCode  = "",SSupName  = "";


     String SFile;
     String SSupTable;
     int    iMillCode;

     

     Document document;
     PdfPTable table;
  
                          
     DyeingWorkOrderGSTPdf(Document document,String SFile,String SOrdNo,String SOrdDate,String SSupCode,String SSupName,int iMillCode,String SSupTable)
     {
          this.document   =document;
          this.SFile      = SFile;
          this.SOrdNo     = SOrdNo;
          this.SOrdDate   = SOrdDate;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.SSupTable = SSupTable;
          this.iMillCode  = iMillCode;

         // getAddr();
          //setDataIntoVector();
          createPdfFile();
     }
     
   
     public void createPdfFile()
     {
          try
     {
                
               
               new DyeingOrderGSTPdfClass(document,table,SFile,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);

             
            
              
          }
          catch (Exception e)
          {
               e.printStackTrace();
          }
     }
}
