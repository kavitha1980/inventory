package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import jdbc.*;
import guiutil.*;



// pdf import
import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;

public class FinPendingList extends JInternalFrame
{
      JPanel     TopPanel;
      JPanel     MiddlePanel;
      JPanel     BottomPanel;
 
      String     SDate;
      JButton    BApply,BPrint,BCreatePdf;
      JTextField TFile;
      DateField  TDate;

      JLayeredPane DeskTop;
      StatusPanel SPanel;
      int iMillCode;
      String SSupTable,SMillName;

      Common common = new Common();
      ORAConnection connect;
      Connection theconnect;
      TabReport tabreport;

      Vector VAcCode,VAcName,VGrnNo,VGrnDate,VGateNo,VGateDate,VOrderNo;
      Vector VInvNo,VInvDate;
      Vector VMatName,VInvQty,VRecQty,VRejQty,VAcQty;
      Vector VInvValue,VOrdQty,VFinDelay,VGrnDelay;

      Vector VXOrderDate,VXOrderNo,VXIndex,VPaymentTerms;   

      String ColumnData[] = {"Supplier","Work Grn No","Work Grn Date","Work Order No","Order Date","Invoice No","Invoice Date","Material Name","Qty Accepted","Invoice Value","GRN Vs Finance","GI Vs GRN"};
      String ColumnType[] = {"S","S","S","S","S","S","S","S","N","V","N","N"};
      Object RowData[][];

      String SHead1 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String SHead2 = "|        WORK GRN         |          Gate           |        Work Order       |        Invoice          |                                          |                        Q U A N T I T Y                         |      Invoice |   Payment Terms     | Delayed Days  |";
      String SHead3 = "|-------------------------------------------------------------------------------------------------------|              Material Name               |----------------------------------------------------------------|        Value |                     |---------------|";
      String SHead4 = "|         No |       Date |         No |       Date |         No |       Date |         No |       Date |                                          |  As Per DC |   Received |   Rejected |   Accepted |    Ordered |           Rs |                     |  Grn | GI-GRN |";
      String SHead5 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String SHead6 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String SHead7 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String SHead8 = "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String SHead9 = "|            |            |            |            |            |            |            |            |                                          |            |            |            |            |            |              |                     |      |        |";

      int Lctr=100,Pctr=0;
      FileWriter FW;
      double dInvValue=0;


     //pdf

    Document document;
    PdfPTable table;
    int iTotalColumns = 18;
    int iWidth[] = {10, 12, 12, 12, 10, 12, 8, 14, 20, 8,8, 8, 8, 8, 12, 11, 10, 10};
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font bigNormal = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
    String SFile="";   
    String PDFFile = common.getPrintPath()+"/RDCFinPending.pdf";


      FinPendingList(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode,String SSupTable,String SMillName)
      {
          super("Grn's Pending @ Finance As On ");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SMillName = SMillName;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
      }

      public void createComponents()
      {
          TopPanel    = new JPanel();
          MiddlePanel = new JPanel();
          BottomPanel = new JPanel();
 
          TDate    = new DateField();
          BApply   = new JButton("Apply");
          BPrint   = new JButton("Print");
          TFile    = new JTextField(15);
          BCreatePdf   = new JButton("Create pdf"); 
          TDate.setTodayDate();
          TFile.setText("Finance.prn");
      }

      public void setLayouts()
      {
          TopPanel.setLayout(new GridLayout(1,5));
          MiddlePanel.setLayout(new BorderLayout());
            
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
      }
                                                                        
      public void addComponents()
      {
          TopPanel.add(new JLabel("As On "));
          TopPanel.add(TDate);
          TopPanel.add(BApply);
 
          BottomPanel.add(BPrint);
          BottomPanel.add(TFile);
           BottomPanel. add(BCreatePdf);
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
      }
      public void addListeners()
      {
          BApply.addActionListener(new ApplyList());
          BPrint.addActionListener(new PrintList());
          BCreatePdf.addActionListener(new PrintList1()); 
      }
      public class ApplyList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setDataIntoVector();
                  setVectorIntoRowData();
                  try
                  {
                        MiddlePanel.remove(tabreport);
                  }
                  catch(Exception ex){}

                  tabreport = new TabReport(RowData,ColumnData,ColumnType);                        
                  MiddlePanel.add("Center",tabreport);
                  MiddlePanel.updateUI();
            }
      }
      public class PrintList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setPendingReport();
                  removeHelpFrame();
            }
      }

       public class PrintList1 implements ActionListener
      {

              public void actionPerformed(ActionEvent ae)
            {
               if(ae.getSource() == BCreatePdf)
               {
                        createPDFFile();
                     
                      try{
                File theFile   = new File(PDFFile);
                Desktop        . getDesktop() . open(theFile);
                  

                  
               }catch(Exception ex){}

                       
               }
                  setPendingReport();
                  removeHelpFrame();
            }
            
            
      }  
      public void removeHelpFrame()
      {
            try
            {
                  DeskTop.remove(this);
                  DeskTop.repaint();
                  DeskTop.updateUI();
            }
            catch(Exception ex) { }
      }

      private void setDataIntoVector()
      {
            VAcCode        = new Vector();
            VAcName        = new Vector();
            VGrnNo         = new Vector();
            VGrnDate       = new Vector();
            VGateNo        = new Vector();
            VGateDate      = new Vector();
            VOrderNo       = new Vector();
            VInvNo         = new Vector();
            VInvDate       = new Vector();
            VMatName       = new Vector();
            VInvQty        = new Vector();
            VRecQty        = new Vector();
            VRejQty        = new Vector();
            VAcQty         = new Vector();
            VInvValue      = new Vector();
            VOrdQty        = new Vector();

            VXOrderDate    = new Vector();
            VXOrderNo      = new Vector();
            VPaymentTerms  = new Vector();


            String QS  = "";
            String QS1 = "";

            QS  = " SELECT WorkGRN.Sup_Code, "+SSupTable+".NAME, WorkGRN.GrnNo, WorkGRN.GrnDate, WorkGRN.GateInNo, WorkGRN.GateInDate, WorkGRN.OrderNo, WorkGRN.InvNo, WorkGRN.InvDate, WorkGrn.Descript, WorkGRN.InvQty, WorkGRN.MillQty, WorkGRN.RejQty, WorkGRN.Qty, WorkGRN.InvNet,WorkGrn.Pending "+
                  " FROM WorkGRN INNER JOIN "+SSupTable+" ON WorkGRN.Sup_Code = "+SSupTable+".AC_CODE "+
                  " WHERE WorkGRN.GRNDate <= '"+TDate.toNormal()+"' And WorkGRN.SPJNo=0 And WorkGrn.Qty > 0 And WorkGrn.MillCode="+iMillCode+" ORDER BY "+SSupTable+".NAME,WorkGrn.GrnNo,WorkGRN.OrderNo,WorkGRN.GrnDate,WorkGrn.Descript ";

            QS1 = " Select WorkOrder.OrderDate,WorkGrn.OrderNo,WorkOrder.PayTerms From WorkOrder "+
                  " Inner Join WorkGRN On WorkOrder.OrderNo = WorkGrn.OrderNo And WorkGRN.MillCode=WorkOrder.MillCode "+
                  " Where WorkGRN.SPJNo=0  And WorkGrn.Qty > 0 And WorkGrn.MillCode="+iMillCode+" Order By 2,3 ";

            try
            {
                  if(theconnect==null)
                  {
                       connect=ORAConnection.getORAConnection();
                       theconnect=connect.getConnection();
                  }
                  Statement stat  = theconnect.createStatement();
                  ResultSet theResult = stat.executeQuery(QS);
                  while(theResult.next())
                  {
                        VAcCode        .addElement(theResult.getString(1));
                        VAcName        .addElement(theResult.getString(2));
                        VGrnNo         .addElement(theResult.getString(3));
                        VGrnDate       .addElement(theResult.getString(4));
                        VGateNo        .addElement(theResult.getString(5));
                        VGateDate      .addElement(theResult.getString(6));
                        VOrderNo       .addElement(theResult.getString(7));
                        VInvNo         .addElement(theResult.getString(8));
                        VInvDate       .addElement(theResult.getString(9));
                        VMatName       .addElement(theResult.getString(10));
                        VInvQty        .addElement(theResult.getString(11));
                        VRecQty        .addElement(theResult.getString(12));
                        VRejQty        .addElement(theResult.getString(13));
                        VAcQty         .addElement(theResult.getString(14));
                        VInvValue      .addElement(theResult.getString(15));
                        VOrdQty        .addElement(common.getRound(theResult.getString(16),2));
                  }
                  theResult.close();

                  ResultSet theResult1 = stat.executeQuery(QS1);
                  while(theResult1.next())
                  {
                      VXOrderDate  .addElement(theResult1.getString(1));    
                      VXOrderNo    .addElement(theResult1.getString(2));
                      VPaymentTerms.addElement(theResult1.getString(3));
                  }
                  theResult1.close();
                  stat.close();
                  setIndexTable();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      private void setVectorIntoRowData()
      {
            RowData = new Object[VAcCode.size()][ColumnData.length];
            for(int i=0;i<VAcCode.size();i++)
            {
                  String SFinDelay  = common.getDateDiff(TDate.toString(),common.parseDate((String)VGrnDate.elementAt(i)));
                  String SGateDelay = common.getDateDiff(common.parseDate((String)VGrnDate.elementAt(i)),common.parseDate((String)VGateDate.elementAt(i)));

                  RowData[i][0] = ""+(String)VAcName.elementAt(i);
                  RowData[i][1] = ""+(String)VGrnNo.elementAt(i);
                  RowData[i][2] = ""+common.parseDate((String)VGrnDate.elementAt(i));
                  RowData[i][3] = ""+(String)VOrderNo.elementAt(i);
                  RowData[i][4] = " ";
                  RowData[i][5] = ""+(String)VInvNo.elementAt(i);
                  RowData[i][6] = common.parseDate((String)VInvDate.elementAt(i));
                  RowData[i][7] = ""+(String)VMatName.elementAt(i);
                  RowData[i][8] = ""+(String)VAcQty.elementAt(i);
                  RowData[i][9] = ""+(String)VInvValue.elementAt(i);
                  RowData[i][10] =""+SFinDelay;
                  RowData[i][11] =""+SGateDelay;
            }
      }

     private void setPendingReport()
     {
          if(VAcCode.size() == 0)
               return;

          Lctr = 100;Pctr=0;

          String PAcCode   = (String)VAcCode.elementAt(0);
          String SPGrnNo   = "";
          String SFile     = TFile.getText();
          dInvValue = 0;

          if((SFile.trim()).length()==0)
             SFile = "1.prn";

          try
          {
               FW = new FileWriter(common.getPrintPath()+SFile);
               for(int i=0;i<VAcCode.size();i++)
               {
                    setHead((String)VAcName.elementAt(i));
                    String SAcCode = (String)VAcCode.elementAt(i);
                    String SGrnNo  = (String)VGrnNo.elementAt(i);
                    if(!SAcCode.equals(PAcCode))
                    {
                         setContBreak();
                         PAcCode = SAcCode;
                         setFirstLine(0,(String)VAcName.elementAt(i));
                         dInvValue = 0;
                    }
                    if(SPGrnNo.equals(SGrnNo))
                         setBody(i,false);
                    else
                    {
                         setBody(i,true);
                    }
                    SPGrnNo = SGrnNo;
               }
               setContBreak();
               FW.write(SHead8+"\n");
               FW.write("< End of Report >\n\n");
               FW.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void setHead(String SName) throws Exception
     {
          if (Lctr < 60)
               return;

          if (Pctr > 0)
               setPageBreak();

          Pctr++;
          String str1 = "Company  : "+SMillName;
          String str2 = "Document : Report on WorkGRN's Pending @ Finpro as On "+TDate.toString();
          String str3 = "Page No  : "+Pctr+"";
          FW.write("M"+str1+"\n");
          FW.write(str2+"\n");
          FW.write(str3+"\n");
          FW.write(SHead1+"\n");
          FW.write(SHead2+"\n");
          FW.write(SHead3+"\n");
          FW.write(SHead4+"\n");
          FW.write(SHead5+"\n");
          Lctr = 8;
          if(Pctr == 1)
            setFirstLine(0,SName);
     }

     private void setFirstLine(int i,String SName) throws Exception 
     {
          String str="";
          if(i==1)
               SName = SName+"(Contd...)";               

          
          String strx = "| "+common.Pad(SName,10+3+10+3+10+3+10+3+10+3+10+3)+
                        common.Rad(" ",10)+"   "+common.Pad(" ",10)+"   "+
                        common.Pad(" ",40)+"   "+
                        common.Rad(" ",10)+"   "+common.Rad(" ",10)+"   "+
                        common.Rad(" ",10)+"   "+common.Rad(" ",10)+"   "+
                        common.Rad(" ",10)+"   "+common.Rad(" ",12)+"   "+
                        common.Space(19)+" | "+
                        common.Rad(" ",4)+"   "+common.Rad(" ",6)+" |";

          FW.write(SHead6+"\n");
          FW.write(strx+"\n");
          FW.write(SHead7+"\n");
          Lctr = Lctr+3;
     }

     private void setContBreak() throws Exception
     {
          if(Lctr == 8)
            return;
          FW.write(SHead5+"\n");

          String str  = "| "+common.Rad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                        common.Rad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                        common.Rad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                        common.Rad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                        common.Pad("T O T A L",40)+" | "+
                        common.Rad(" ",10)+" | "+common.Rad(" ",10)+" | "+
                        common.Rad(" ",10)+" | "+common.Rad(" ",10)+" | "+
                        common.Rad(" ",10)+" | "+common.Rad(common.getRound(dInvValue,2),12)+" | "+
                        common.Space(19)+" | "+
                        common.Rad(" ",4)+" | "+common.Rad(" ",6)+" |";

          FW.write(str+"\n");

          FW.write(SHead5+"\n");
          Lctr=Lctr+3;
     }
     private void setPageBreak() throws Exception
     {
          FW.write(SHead8+"\n");
     }
     private void setBody(int i,boolean bFlag) throws Exception
     {
          if(Lctr == 8)
          {
               setFirstLine(1,(String)VAcName.elementAt(i));
               bFlag = true;
          }
          if(bFlag && Lctr > 8)
          {
               FW.write(SHead9+"\n");
               Lctr++;
          }
          String SGrnNo = "",SGrnDate="",SGINo="",SGIDate="",SInvNo="",SInvDate="";
          if(bFlag)
          {
               SGrnNo     = (String)VGrnNo.elementAt(i);
               SGrnDate   = common.parseDate((String)VGrnDate.elementAt(i));
               SGINo      = (String)VGateNo.elementAt(i);
               SGIDate    = common.parseDate((String)VGateDate.elementAt(i));
               SInvNo     = (String)VInvNo.elementAt(i);
               SInvDate   = common.parseDate((String)VInvDate.elementAt(i));
          }
          String SOrderNo   = (String)VOrderNo.elementAt(i);
          String SOrderDate = common.parseDate(getOrdDate(i));
          String SPaymentTerms = getPaymentTerms(i);
          String SMatName   = (String)VMatName.elementAt(i);

          String SInvQty    = common.getRound((String)VInvQty.elementAt(i),3);
          String SRecQty    = common.getRound((String)VRecQty.elementAt(i),3);        
          String SRejQty    = common.getRound((String)VRejQty.elementAt(i),3);        
          String SAcQty     = common.getRound((String)VAcQty.elementAt(i),3);         
          String SOrdQty    = common.getRound((String)VOrdQty.elementAt(i),3);         

          String SInvValue  = common.getRound((String)VInvValue.elementAt(i),2);
          String SFinDelay  = common.getDateDiff(TDate.toString(),common.parseDate((String)VGrnDate.elementAt(i)));
          String SGateDelay = common.getDateDiff(common.parseDate((String)VGrnDate.elementAt(i)),common.parseDate((String)VGateDate.elementAt(i)));

          dInvValue         = dInvValue+common.toDouble(SInvValue);

          String str  = "| "+common.Rad(SGrnNo,10)+" | "+common.Pad(SGrnDate,10)+" | "+
                        common.Rad(SGINo,10)+" | "+common.Pad(SGIDate,10)+" | "+
                        common.Rad(SOrderNo,10)+" | "+common.Pad(SOrderDate,10)+" | "+
                        common.Rad(SInvNo,10)+" | "+common.Pad(SInvDate,10)+" | "+
                        common.Pad(SMatName,40)+" | "+
                        common.Rad(SInvQty,10)+" | "+common.Rad(SRecQty,10)+" | "+
                        common.Rad(SRejQty,10)+" | "+common.Rad(SAcQty,10)+" | "+
                        common.Rad(SOrdQty,10)+" | "+common.Rad(SInvValue,12)+" | "+
                        common.Pad(SPaymentTerms,19)+" | "+
                        common.Rad(SFinDelay,4)+" | "+common.Rad(SGateDelay,6)+" |";

          FW.write(str+"\n");
          Lctr++;
     }
     private void setIndexTable()
     {
          VXIndex = new Vector();
          for(int i=0;i<VXOrderNo.size();i++)
          {
               String str1 = (String)VXOrderNo.elementAt(i);
               VXIndex.addElement(str1);
          }
     }
     private int getOrderIndex(int i)
     {
          String str1  = (String)VOrderNo.elementAt(i);
          return VXIndex.indexOf(str1);
     }
     private String getOrdDate(int i)
     {
          int iIndex = getOrderIndex(i);
          if (iIndex == -1)
               return " ";
          return (String)VXOrderDate.elementAt(iIndex);
     }
     private String getPaymentTerms(int i)
     {
          int iIndex = getOrderIndex(i);
          if (iIndex == -1)
               return " ";
          return (String)VPaymentTerms.elementAt(iIndex);

     }

  //pdf Part

  
private void createPDFFile() {

if(VAcCode.size() == 0)
               return;
          
        //  Lctr = 100;
         // Pctr = 0;
          
          String    PAcCode   = (String)VAcCode.elementAt(0);
          String    SPGrnNo   = "";
          String    SPBlock   = "";
          String    SFile     = TFile.getText();
                    dInvValue = 0;
          
          if((SFile.trim()).length()==0)
               SFile = "1.pdf";
          
          try
          {

             
             document = new Document(PageSize.A4.rotate());
              PdfWriter.getInstance(document, new FileOutputStream(PDFFile));
              document.open();
              document .  newPage();  
              

                        table  = new PdfPTable(18);
			table  . setWidths(iWidth);
			table  . setWidthPercentage(100);
                        table.setHeaderRows(4);

                AddCellIntoTable("Company : "+SMillName+" ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 18, 0, 0, 0, 0, smallbold);


           AddCellIntoTable( "Document : Report on WorkGRN's Pending @ Finpro as On "+TDate.toString()+"", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 18, 0, 0, 0, 0, smallbold);           
  
       
	


      setHeadpdf();
               for(int i=0;i<VAcCode.size();i++)
               {
                   // setHead((String)VAcName.elementAt(i));

                    String    SAcCode   = (String)VAcCode   . elementAt(i);
                    String    SGrnNo    = (String)VGrnNo    . elementAt(i);
                    

                    if(!SAcCode.equals(PAcCode) || i==0)
                    {
                         setContBreakpdf();
                         PAcCode   = SAcCode;
                         setFirstLinepdf((String)VAcName.elementAt(i));
                         dInvValue = 0;
                    }
                    if(SPGrnNo.equals(SGrnNo))
                         setBodypdf(i,false);
                    else
                    {
                         setBodypdf(i,true);
                    }
                    SPGrnNo = SGrnNo;
                   
               }
              // setContBreak();
              
               //new ControlFigures(FW,iMillCode);
              document.add(table); 
              document   . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

}

 private void setHeadpdf() throws Exception
{

        AddCellIntoTable("WorkGRN  ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 4, 1, 2, 2, mediumbold);
	AddCellIntoTable("GATE", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("WorkOrder ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("Invoice", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 2, mediumbold);
	AddCellIntoTable("Material Name", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
        AddCellIntoTable("  QUANTITY     ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 5, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("Invoice Value ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("Payment Terms", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
	AddCellIntoTable("Delayed Days", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 2, mediumbold);

	AddCellIntoTable("No ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 2, 2, mediumbold);
	AddCellIntoTable("Date", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("No ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 2, 2, mediumbold);
	AddCellIntoTable("Date", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("No ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 2, 2, mediumbold);
	AddCellIntoTable("Date", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("Date", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	
	AddCellIntoTable("Material Name ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("As Per DC", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
	AddCellIntoTable(" Received", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("Rejected", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("Accepted", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("Ordered", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("Rs.", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
	AddCellIntoTable("grn", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("GI-GRN", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
 }
 private void setFirstLinepdf(String SName) throws Exception 
     {
          String str="";
         /* if(i==1)
               str = "Name : "+SName+" (Contd...)";               
          else*/
               str = "Name : "+SName+" ";

          AddCellIntoTable(str, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 18, 0, 0, 0, 0, smallbold); 
         
     }

 private void setBodypdf(int i,boolean bFlag) throws Exception
     {
         /* if(Lctr == 8)
          {
               setFirstLinepdf((String)VAcName.elementAt(i));
               bFlag = true;
          }

          if(bFlag && Lctr > 8)
          {
               FW.write(SHead9+"\n");
               Lctr++;
          }*/

        String SGrnNo = "",SGrnDate="",SGINo="",SGIDate="",SInvNo="",SInvDate="";
          if(bFlag)
          {
               SGrnNo     = (String)VGrnNo.elementAt(i);
               SGrnDate   = common.parseDate((String)VGrnDate.elementAt(i));
               SGINo      = (String)VGateNo.elementAt(i);
               SGIDate    = common.parseDate((String)VGateDate.elementAt(i));
               SInvNo     = (String)VInvNo.elementAt(i);
               SInvDate   = common.parseDate((String)VInvDate.elementAt(i));
          }
          String SOrderNo   = (String)VOrderNo.elementAt(i);
          String SOrderDate = common.parseDate(getOrdDate(i));
          String SPaymentTerms = getPaymentTerms(i);
          String SMatName   = (String)VMatName.elementAt(i);

          String SInvQty    = common.getRound((String)VInvQty.elementAt(i),3);
          String SRecQty    = common.getRound((String)VRecQty.elementAt(i),3);        
          String SRejQty    = common.getRound((String)VRejQty.elementAt(i),3);        
          String SAcQty     = common.getRound((String)VAcQty.elementAt(i),3);         
          String SOrdQty    = common.getRound((String)VOrdQty.elementAt(i),3);         

          String SInvValue  = common.getRound((String)VInvValue.elementAt(i),2);
          String SFinDelay  = common.getDateDiff(TDate.toString(),common.parseDate((String)VGrnDate.elementAt(i)));
          String SGateDelay = common.getDateDiff(common.parseDate((String)VGrnDate.elementAt(i)),common.parseDate((String)VGateDate.elementAt(i)));

          dInvValue         = dInvValue+common.toDouble(SInvValue);

          String str  = "| "+common.Rad(SGrnNo,10)+" | "+common.Pad(SGrnDate,10)+" | "+
                        common.Rad(SGINo,10)+" | "+common.Pad(SGIDate,10)+" | "+
                        common.Rad(SOrderNo,10)+" | "+common.Pad(SOrderDate,10)+" | "+
                        common.Rad(SInvNo,10)+" | "+common.Pad(SInvDate,10)+" | "+
                        common.Pad(SMatName,40)+" | "+
                        common.Rad(SInvQty,10)+" | "+common.Rad(SRecQty,10)+" | "+
                        common.Rad(SRejQty,10)+" | "+common.Rad(SAcQty,10)+" | "+
                        common.Rad(SOrdQty,10)+" | "+common.Rad(SInvValue,12)+" | "+
                        common.Pad(SPaymentTerms,19)+" | "+
                        common.Rad(SFinDelay,4)+" | "+common.Rad(SGateDelay,6)+" |";

		AddCellIntoTable(SGrnNo, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 2, 2, smallnormal);
		AddCellIntoTable(SGrnDate, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
		AddCellIntoTable(SGINo, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SGIDate, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(SOrderNo, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
		AddCellIntoTable(SOrderDate, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SInvNo, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SInvDate, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
		AddCellIntoTable(SMatName, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 2, 2, smallnormal);
		AddCellIntoTable(SInvQty, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
		AddCellIntoTable(SRecQty, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SRejQty, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
		AddCellIntoTable(SAcQty, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
		AddCellIntoTable(SOrdQty, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SInvValue, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SPaymentTerms, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
		AddCellIntoTable(SFinDelay, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
		AddCellIntoTable(SGateDelay, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 


                
        
     }
 private void setContBreakpdf() throws Exception
     {

 AddCellIntoTable("TOTAL", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 14, 4, 1, 8, 2, mediumbold);  
		AddCellIntoTable(String.valueOf(common.getRound(dInvValue,2)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
               AddCellIntoTable("", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 3, 4, 1, 8, 2, smallnormal); 
     }

  public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }



}
