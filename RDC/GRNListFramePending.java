package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GRNListFramePending extends JInternalFrame
{

     String    SStDate,SEnDate;
     JComboBox JCOrder;
     JButton   BApply;
     JPanel    TopPanel,BottomPanel;
     JPanel    DatePanel,FilterPanel,SortPanel,BasisPanel,ApplyPanel;

     TabReport tabreport;
     DateField TStDate;
     DateField TEnDate;

     JRadioButton JRPeriod,JRNo;
     NextField    TStNo,TEnNo;

     Object RowData[][];
     String ColumnData[] = {"GRN No","Date","Supplier","Order No","G.I. No","G.I. Date","DC No","DC Date","Inv No","Inv Date","Description","Inv Qty","Recd Qty","Inv Rate","Amount","Accepted Qty","Dept","Group","Unit","Status"};
     String ColumnType[] = {"N","S","S","N","N","S","S","S","S","S","S","N","N","N","N","N","S","S","S","S"};

     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;
     Vector VGrnNo,VGrnDate,VSupName,VOrdNo,VDcNo,VDcDate,VGno,VGDate,VINo,VIDate,VGrnName,VIQty,VRQty,VIRate,VIAmt,VAccQty,VGrnDeptName,VGrnCataName,VGrnUnitName,VStatus,VSPJNo,VId;
     JLayeredPane DeskTop;
     StatusPanel SPanel;
     int iUserCode,iMillCode;
     String SSupTable;

     Vector VCode,VName;

     GRNListFramePending(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable)
     {
         super("GRN Pending List During a Period");
         this.DeskTop = DeskTop;
         this.SPanel  = SPanel;
         this.iUserCode = iUserCode;
         this.iMillCode = iMillCode;
         this.SSupTable = SSupTable;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }

     public void createComponents()
     {
         TStDate  = new DateField();
         TStNo    = new NextField();
         TEnNo    = new NextField();
         BApply   = new JButton("Apply");

         TopPanel    = new JPanel();
         BottomPanel = new JPanel();

         JCOrder  = new JComboBox();

         JRPeriod = new JRadioButton("Periodical",true);
         JRNo     = new JRadioButton("GRN No");


         DatePanel   = new JPanel();
         FilterPanel = new JPanel();
         SortPanel   = new JPanel();
         BasisPanel  = new JPanel();
         ApplyPanel  = new JPanel();

         TStDate.setTodayDate();
         BApply.setMnemonic('A');
     }

     public void setLayouts()
     {
         TopPanel.setLayout(new GridLayout(1,5));

         DatePanel.setLayout(new GridLayout(3,1));
         SortPanel.setLayout(new BorderLayout());
         FilterPanel.setLayout(new BorderLayout());
         BasisPanel.setLayout(new GridLayout(2,1));
         ApplyPanel.setLayout(new BorderLayout());
       
         setClosable(true);
         setIconifiable(true);
         setMaximizable(true);
         setResizable(true);
         setBounds(0,0,790,500);
     }

     public void addComponents()
     {
         JCOrder.addItem("GRN No");
         JCOrder.addItem("Materialwise");
         JCOrder.addItem("Supplierwise");
         JCOrder.addItem("Departmentwise");
         JCOrder.addItem("Groupwise");
         JCOrder.addItem("Processing Unitwise");

         SortPanel.add("Center",JCOrder);
         DatePanel.add(TStDate);
         ApplyPanel.add("Center",BApply);
         BasisPanel.add(JRPeriod);
         BasisPanel.add(JRNo);

         TopPanel.add(SortPanel);
         TopPanel.add(BasisPanel);
         TopPanel.add(DatePanel);
         TopPanel.add(ApplyPanel);

         SortPanel.setBorder(new TitledBorder("Sorting"));
         BasisPanel.setBorder(new TitledBorder("Basis"));
         DatePanel.setBorder(new TitledBorder("Period"));
         ApplyPanel.setBorder(new TitledBorder("Control"));

         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
         BApply.addActionListener(new ApplyList(this));
         JRPeriod.addActionListener(new JRList());
         JRNo.addActionListener(new JRList());
     }

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    DatePanel.setBorder(new TitledBorder("Periodical"));
                    DatePanel.removeAll();
                    DatePanel.add(TStDate);
                    DatePanel.updateUI();
                    JRNo.setSelected(false);
               }
               else
               {
                    DatePanel.setBorder(new TitledBorder("Numbered"));
                    DatePanel.removeAll();
                    DatePanel.add(TStNo);
                    DatePanel.add(TEnNo);
                    DatePanel.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }


     public class ApplyList implements ActionListener
     {
          GRNListFramePending grnlistframepending;
          public ApplyList(GRNListFramePending grnlistframepending)
          {
               this.grnlistframepending = grnlistframepending;
          }
          public void actionPerformed(ActionEvent ae)
          {
               setTabReport(grnlistframepending);
          }
     }
     public void setTabReport(GRNListFramePending grnlistframepending)
     {
          setDataIntoVector();
          setRowData();
          try
          {
             getContentPane().remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             getContentPane().add(tabreport,BorderLayout.CENTER);
             setSelected(true);
             DeskTop.repaint();
             DeskTop.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }
     }
     public void setDataIntoVector()
     {
           VGrnNo       = new Vector();
           VGrnDate     = new Vector();
           VSupName     = new Vector();
           VOrdNo       = new Vector();
           VGno         = new Vector();
           VGDate       = new Vector();
           VDcNo        = new Vector();
           VDcDate      = new Vector();
           VINo         = new Vector();
           VIDate       = new Vector();
           VGrnName     = new Vector();
           VIQty        = new Vector();
           VRQty        = new Vector();
           VIRate       = new Vector();
           VIAmt        = new Vector();
           VAccQty      = new Vector();
           VGrnDeptName = new Vector();
           VGrnCataName = new Vector();
           VGrnUnitName = new Vector();
           VStatus      = new Vector();
           VSPJNo       = new Vector();
           VId          = new Vector();

           SStDate = TStDate.TDay.getText()+"."+TStDate.TMonth.getText()+"."+TStDate.TYear.getText();
           String StDate  = TStDate.TYear.getText()+TStDate.TMonth.getText()+TStDate.TDay.getText();

           String QString = getQString(StDate);
           try
           {
              if(theconnect==null)
              {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
              }
              Statement stat  = theconnect.createStatement();
              ResultSet res  = stat.executeQuery(QString);
              while (res.next())
              {
                     String str1  = res.getString(1);  
                     String str2  = res.getString(2);
                     String str4  = res.getString(3);
                     String str5  = res.getString(4);
                     String str6  = res.getString(5);
                     String str7  = res.getString(6);
                     String str8  = res.getString(7);
                     String str9  = res.getString(8);
                     String str10 = res.getString(9);
                     String str11 = res.getString(10);
                     String str13 = res.getString(11);
                     String str14 = res.getString(12);
                     String str15 = res.getString(13);
                     String str16 = res.getString(14);
                     String str17 = res.getString(15);
                     String str18 = res.getString(16);
                     String str19 = res.getString(17);
                     String str20 = res.getString(18);
                     String str21 = res.getString(19);
                     String str22 = res.getString(20);
                     String str23 = res.getString(21);
                                                    
                     VGrnNo       .addElement(str1);
                     VGrnDate     .addElement(common.parseDate(str2));
                     VSupName     .addElement(str4);
                     VOrdNo       .addElement(str5);
                     VGno         .addElement(str6);
                     VGDate       .addElement(common.parseDate(str7));
                     VDcNo        .addElement(common.parseNull(str8));
                     VDcDate      .addElement(common.parseDate(str9));
                     VINo         .addElement(common.parseNull(str10));
                     VIDate       .addElement(common.parseDate(str11));
                     VGrnName     .addElement(str13);
                     VIQty        .addElement(str14);
                     VRQty        .addElement(str15);
                     VIRate       .addElement(str16);
                     VIAmt        .addElement(str17);
                     VAccQty      .addElement(str18);
                     VGrnDeptName .addElement(common.parseNull(str19));
                     VGrnCataName .addElement(common.parseNull(str20));
                     VGrnUnitName .addElement(common.parseNull(str21));
                     VSPJNo       .addElement(str22);
                     VStatus      .addElement(" ");
                     VId          .addElement(str23);
              }
              res.close();
              stat.close();
           }
           catch(Exception ex)
           {
               try
               {
                    FileWriter FW = new FileWriter(common.getPrintPath()+"q.txt");
                    FW.write(QString);
                    FW.close();
               }
               catch(Exception e){}
               System.out.println(ex);
           }
     }
     public void setRowData()
     {
         RowData     = new Object[VGrnNo.size()][ColumnData.length];
         for(int i=0;i<VGrnNo.size();i++)
         {
               RowData[i][0]  = (String)VGrnNo.elementAt(i);
               RowData[i][1]  = (String)VGrnDate.elementAt(i);
               RowData[i][2]  = (String)VSupName.elementAt(i);
               RowData[i][3]  = (String)VOrdNo.elementAt(i);
               RowData[i][4]  = (String)VGno.elementAt(i);
               RowData[i][5]  = (String)VGDate.elementAt(i);
               RowData[i][6]  = (String)VDcNo.elementAt(i);
               RowData[i][7]  = (String)VDcDate.elementAt(i);
               RowData[i][8]  = (String)VINo.elementAt(i);
               RowData[i][9]  = (String)VIDate.elementAt(i);
               RowData[i][10] = (String)VGrnName.elementAt(i);
               RowData[i][11] = (String)VIQty.elementAt(i);
               RowData[i][12] = (String)VRQty.elementAt(i);
               RowData[i][13] = (String)VIRate.elementAt(i);
               RowData[i][14] = (String)VIAmt.elementAt(i);
               RowData[i][15] = (String)VAccQty.elementAt(i);
               RowData[i][16] = (String)VGrnDeptName.elementAt(i);
               RowData[i][17] = (String)VGrnCataName.elementAt(i);
               RowData[i][18] = (String)VGrnUnitName.elementAt(i);
               RowData[i][19] = (String)VStatus.elementAt(i);
        }
     }

     public String getQString(String StDate)
     {
          String QString  = "";

          if(JRPeriod.isSelected())
          {
               QString  = " SELECT WorkGRN.GrnNo, WorkGRN.GrnDate, "+
                          " "+SSupTable+".Name, WorkGRN.OrderNo, "+
                          " WorkGRN.GateInNo, WorkGRN.GateInDate, "+
                          " WorkGRN.DcNo, WorkGRN.DcDate, "+
                          " WorkGRN.InvNo, WorkGRN.InvDate, "+
                          " WorkGRN.Descript, WorkGRN.InvQty, "+
                          " WorkGRN.MillQty, WorkGRN.InvRate, "+
                          " WorkGRN.InvAmount, WorkGRN.Qty, "+
                          " Dept.Dept_Name, Cata.Group_Name, "+
                          " Unit.Unit_Name, WorkGRN.SPJNo, WorkGRN.Id "+
                          " FROM (((WorkGRN INNER JOIN "+SSupTable+" ON WorkGRN.Sup_Code = "+SSupTable+".Ac_Code) "+
                          " INNER JOIN Dept ON WorkGRN.Dept_Code = Dept.Dept_code) "+
                          " INNER JOIN Cata ON WorkGRN.Group_Code = Cata.Group_Code) "+
                          " INNER JOIN Unit ON WorkGRN.Unit_Code = Unit.Unit_Code "+
                          " Where WorkGRN.GrnDate <= '"+StDate+"' and WorkGRN.spjno=0 and WorkGRN.MillCode="+iMillCode;              
          }
          else
          {
               QString  = " SELECT WorkGRN.GrnNo, WorkGRN.GrnDate, "+
                          " "+SSupTable+".Name, WorkGRN.OrderNo, "+
                          " WorkGRN.GateInNo, WorkGRN.GateInDate, "+
                          " WorkGRN.DcNo, WorkGRN.DcDate, "+
                          " WorkGRN.InvNo, WorkGRN.InvDate, "+
                          " WorkGRN.Descript, WorkGRN.InvQty, "+
                          " WorkGRN.MillQty, WorkGRN.InvRate, "+
                          " WorkGRN.InvAmount, WorkGRN.Qty, "+
                          " Dept.Dept_Name, Cata.Group_Name, "+
                          " Unit.Unit_Name, WorkGRN.SPJNo, WorkGRN.Id "+
                          " FROM (((WorkGRN INNER JOIN "+SSupTable+" ON WorkGRN.Sup_Code = "+SSupTable+".Ac_Code) "+
                          " INNER JOIN Dept ON WorkGRN.Dept_Code = Dept.Dept_code) "+
                          " INNER JOIN Cata ON WorkGRN.Group_Code = Cata.Group_Code) "+
                          " INNER JOIN Unit ON WorkGRN.Unit_Code = Unit.Unit_Code "+
                          " Where WorkGRN.GRNNo >="+TStNo.getText()+" and WorkGRN.GrnNo <= "+TEnNo.getText()+" and WorkGRN.spjno=0 and WorkGRN.MillCode="+iMillCode;
          }

          if(JCOrder.getSelectedIndex() == 0)
               QString = QString+" Order By WorkGRN.GrnNo,WorkGRN.GrnDate";
          if(JCOrder.getSelectedIndex() == 1)
               QString = QString+" Order By WorkGRN.Descript,WorkGRN.GrnDate";
          if(JCOrder.getSelectedIndex() == 2)
               QString = QString+" Order By "+SSupTable+".Name,WorkGRN.GrnDate";
          if(JCOrder.getSelectedIndex() == 3)
               QString = QString+" Order By Dept.Dept_Name,WorkGRN.GrnDate";
          if(JCOrder.getSelectedIndex() == 4)
               QString = QString+" Order By Cata.Group_Name,WorkGRN.GrnDate";
          if(JCOrder.getSelectedIndex() == 5)
               QString = QString+" Order By Unit.Unit_Name,WorkGRN.GrnDate";

          return QString;
     }
}
