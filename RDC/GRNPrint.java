package RDC;

import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GRNPrint
{
     FileWriter FW;
     String SGrnNo,SGrnDate,SSupCode,SSupName,SNet;
     int iMillCode;
     String SMillName;

     String SGINo="",SGIDate="";
     String SInvNo="",SInvDate="";
     String SDCNo="",SDCDate="";

     Vector VGName,VGOrdNo,VGDCQty,VGRecQty,VGRejQty,VGAcQty,VGPendQty;

     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;

     int Lctr      = 100;
     int Pctr      = 0;
     GRNPrint(FileWriter FW,String SGrnNo,String SGrnDate,String SSupCode,String SSupName,String SNet,int iMillCode,String SMillName)
     {
          this.FW         = FW;
          this.SGrnNo     = SGrnNo;
          this.SGrnDate   = SGrnDate;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.SNet       = SNet;
          this.iMillCode  = iMillCode;
          this.SMillName  = SMillName;

          setDataIntoVector();
          setGrnHead();
          setGrnBody();
          setGrnFoot(0);
     }
   
     public void setGrnHead()
     {
          if(Lctr < 43)
            return;

          if(Pctr > 0)
            setGrnFoot(1);

          Pctr++;


               String Strl1 = ""+SMillName+"";
               String Strl1a= " ";
               String Strl1b= "ERDC Goods Receipt NoteF";
               String Strl2 = " ";
               String Strl3 = " ";
               String Strl4 = "G.R.N No       : "+SGrnNo+"/"+SGrnDate; 
               String Strl5 = "";
               String Strl6 = "Supplier Name  : "+SSupName;
               String Strl7 = "";
               String Strl8  = "";
               String Strl9  = "|------------------------------------------------------------------------------------|";
               String Strl10 = "|          GATE INWARD          |            INVOICE            |                    |";
               String Strl11 = "|---------------------------------------------------------------|     GRN VALUE      |";
               String Strl12 = "| No            | Date          | No            | Date          |                    |";
               String Strl13 = "|------------------------------------------------------------------------------------| ";
               String Strl14 = "| "+common.Pad(SGINo,14)+"| "+common.Pad(SGIDate,14)+"| "+common.Pad(SInvNo,14)+"| "+common.Pad(SInvDate,14)+"| "+common.Cad(common.getRound(SNet,2),14+4)+" |";
               String Strl15 = "|------------------------------------------------------------------------------------|";
               String Strl16 = "|------------------------------------------------------------------------------------|";
               String Strl17 = "|Material Description          |Order No/Date       |      Q U A N T I T Y           |";
               String Strl18 = "|                              |                    |--------------------------------|";
               String Strl19 = "|                              |                    |    DC Qty|  Accepted|   Ordered|";
               String Strl20 = "|------------------------------------------------------------------------------------|";

               try
               {
                    FW.write(Strl1+"\n");
                    FW.write(Strl1a+"\n");
                    FW.write(Strl1b+"\n");
                    FW.write(Strl2+"\n");
                    FW.write(Strl3+"\n");
                    FW.write(Strl4+"\n");
                    FW.write(Strl5+"\n");
                    FW.write(Strl6+"\n");
                    FW.write(Strl7+"\n");
                    FW.write(Strl8+"\n");
                    FW.write(Strl9+"\n");
                    FW.write(Strl10+"\n");
                    FW.write(Strl11+"\n");
                    FW.write(Strl12+"\n");
                    FW.write(Strl13+"\n");
                    FW.write(Strl14+"\n");
                    FW.write(Strl15+"\n");
                    FW.write(Strl16+"\n");
                    FW.write(Strl17+"\n");
                    FW.write(Strl18+"\n");
                    FW.write(Strl19+"\n");
                    FW.write(Strl20+"\n");
                    Lctr = 22;
               }
               catch(Exception ex)
               {
               }
     }

     public void setGrnBody()
     {
          double dDCQty=0,dAcQty=0,dPendQty=0;
          String SCode="",SPCode="";
          int ctr=0;
          for(int i=0;i<VGName.size();i++)
          {
               SCode     = (String)VGName.elementAt(i);
               if(!SCode.equals(SPCode))
               {
                    if(i>0)
                         setDCFoot(0,ctr,dDCQty,dAcQty,dPendQty);                    
                    SPCode = SCode;
                    ctr=0;
                    dDCQty=0;dAcQty=0;dPendQty=0;
               }
               setGrnHead();
               String SName     = (String)VGName.elementAt(i);
               String SGOrdNo   = common.Pad((String)VGOrdNo.elementAt(i),20);

               String SGDCQty   = common.getRound((String)VGDCQty.elementAt(i),2);
               String SGAcQty   = common.getRound((String)VGAcQty.elementAt(i),2);
               String SGPendQty = common.getRound((String)VGPendQty.elementAt(i),2);

               dDCQty   = dDCQty+common.toDouble(SGDCQty);
               dAcQty   = dAcQty+common.toDouble(SGAcQty);
               dPendQty = dPendQty+common.toDouble(SGPendQty);

               SGDCQty   = common.Rad(SGDCQty,10);
               SGAcQty   = common.Rad(SGAcQty,10);
               SGPendQty = common.Rad(SGPendQty,10);
               Vector vect = common.getLines(SName);
               String Strl = "";
               try
               {
                    for(int j=0;j<vect.size();j++)
                    {
                         if (j==0)
                              Strl = "|"+(ctr==0?common.Pad((String)vect.elementAt(j),30):common.Space(30))+"|"+
                                       SGOrdNo+"|"+SGDCQty+"|"+
                                       SGAcQty+"|"+SGPendQty+"|";
                         else
                              Strl = "|"+(ctr==0?common.Pad((String)vect.elementAt(j),30):common.Space(30))+"|"+
                                       common.Space(20)+"|"+common.Space(10)+"|"+
                                       common.Space(10)+"|"+common.Space(10)+"|";

                         FW.write(Strl+"\n");
                         Lctr = Lctr++;
                    }
                    ctr++;
               }
               catch(Exception ex){}
          }
          setDCFoot(1,ctr,dDCQty,dAcQty,dPendQty);                    
     }
     public void setDCFoot(int iEnd,int ctr,double dDCQty,double dAcQty,double dPendQty)                    
     {
          if(ctr==1)
          {
               try
               {
                    if(iEnd==0)
                         FW.write("|------------------------------------------------------------------------------------|\n");
                    else
                         FW.write("|------------------------------------------------------------------------------------|\n");

                    Lctr++;
               }
               catch(Exception ex){}
               return;
          }

          String SDGDCQty    = common.Rad(common.getRound(dDCQty,2),10);
          String SDGAcQty    = common.Rad(common.getRound(dAcQty,2),10);
          String SDGPendQty  = common.Rad(common.getRound(dPendQty,2),10);

          try
          {
               FW.write("|------------------------------------------------------------------------------------|\n");

               String Strl = "|"+common.Space(30)+"|"+
                             common.Space(20)+"|"+SDGDCQty+"|"+
                             SDGAcQty+"|"+SDGPendQty+"|";

               FW.write(Strl+"\n");
               if(iEnd==0)
                    FW.write("|------------------------------------------------------------------------------------|\n");
               else
                    FW.write("|------------------------------------------------------------------------------------|\n");

               Lctr=Lctr+3;
          }
          catch(Exception ex){}
     }
     public void setGrnFoot(int iSig)
     {
               String Strl1 = "|------------------------------------------------------------------------------------|";
               String Strl2 = "| Remarks :                                                                          |";
               String Strl3 = "|                                                                                    |";
               String Strl4 = "|                                                                                    |";
               String Strl5 = "|------------------------------------------------------------------------------------|";
               String Strl6 = "|Prepared    |Binned      |Inspected   |Store-Keeper|Received   |Issued     |Passed  |";
               String Strl7 = "|            |            |            |            |           |           |        |";
               String Strl8 = "|            |            |            |            |           |           |        |";
               String Strl9 = "-------------------------------------------------------------------------------------|";

               try
               {
                    FW.write(Strl1+"\n");
                    FW.write(Strl2+"\n");
                    FW.write(Strl3+"\n");
                    FW.write(Strl4+"\n");
                    FW.write(Strl5+"\n");
                    FW.write(Strl6+"\n");
                    FW.write(Strl7+"\n");
                    FW.write(Strl8+"\n");
                    FW.write(Strl9+"\n");
                    if(iSig==0)
                    {
                       String SDateTime = common.getServerDateTime2();
                       String SEnd = "Report Taken on "+SDateTime+"\n";
                       FW.write(SEnd);
                    }
                    else
                       FW.write("(Continued on Next Page) \n");
               }
               catch(Exception ex){}
     }
     public void setDataIntoVector()
     {
          VGName      = new Vector();
          VGOrdNo     = new Vector();
          VGDCQty     = new Vector();
          VGAcQty     = new Vector();
          VGPendQty   = new Vector();

           try
           {
              if(theconnect==null)
              {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
              }
              Statement stat  = theconnect.createStatement();
              String QString = getQString();
              ResultSet res  = stat.executeQuery(QString);
              while (res.next())
              {
                    VGName      .addElement(res.getString(1));
                    VGOrdNo     .addElement(common.parseNull(res.getString(11))+"/"+common.parseDate(res.getString(12)));
                    VGDCQty     .addElement(res.getString(2));
                    VGAcQty     .addElement(res.getString(3));
                    VGPendQty   .addElement(res.getString(4));
                    SGINo      = res.getString(5);
                    SGIDate    = common.parseDate(res.getString(6));
                    SInvNo     = common.parseDate(res.getString(7));
                    SInvDate   = common.parseDate(res.getString(8));
                    SDCNo      = common.parseDate(res.getString(9));
                    SDCDate    = common.parseDate(res.getString(10));
              }
              res.close();
              stat.close();
           }
           catch(Exception ex){System.out.println(ex);}
     }
     public String getQString()
     {
          String QString  = "";

          QString = " Select WorkGRN.Descript, WorkGRN.InvQty, "+
                    " WorkGRN.Qty,WorkGRN.Pending, "+
                    " WorkGRN.GateInNo, WorkGRN.GateInDate, "+
                    " WorkGRN.InvNo,WorkGRN.InvDate, WorkGRN.DCNo, "+
                    " WorkGRN.DCDate,WorkGRN.OrderNo,WorkOrder.OrderDate "+
                    " From WorkGRN "+
                    " Inner Join WorkOrder On WorkGRN.OrderNo = WorkOrder.OrderNo "+
                    " And WorkGRN.Descript=WorkOrder.Descript And WorkGRN.MillCode = WorkOrder.MillCode "+
                    " and WorkGRN.OrderSlNo=WorkOrder.SlNo "+
                    " Where WorkGRN.GrnNo = "+SGrnNo+" And WorkGrn.MillCode="+iMillCode+ 
                    " Order By 1";

          return QString;
     }

}
