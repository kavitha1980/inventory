package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.sql.*;
import jdbc.*;

import util.*;
import jdbc.*;
import guiutil.*;


public class InvTableModelGst extends DefaultTableModel
{
        Object    RowData[][],ColumnNames[],ColumnType[];
        JLabel    LBasic,LDiscount,LCGST,LSGST,LIGST,LCess,LNet;
        NextField TAdd,TLess;
        Common common = new Common();
        public InvTableModelGst(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType,JLabel LBasic,JLabel LDiscount,JLabel LCGST,JLabel LSGST,JLabel LIGST,JLabel LCess,NextField TAdd,NextField TLess,JLabel LNet)
        {
            super(RowData,ColumnNames);
            this.RowData     = RowData;
            this.ColumnNames = ColumnNames;
            this.ColumnType  = ColumnType;
            this.LBasic      = LBasic;
            this.LDiscount   = LDiscount;
            this.LCGST       = LCGST; 
            this.LSGST       = LSGST;
            this.LIGST       = LIGST;
	    this.LCess       = LCess;
            this.TAdd        = TAdd;
            this.TLess       = TLess;
            this.LNet        = LNet;

            for(int i=0;i<super.dataVector.size();i++)
            {
                Vector curVector = (Vector)super.dataVector.elementAt(i);
                //setMaterialAmount(curVector);
            }

        }
        public InvTableModelGst(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType)
        {
            super(RowData,ColumnNames);
            this.RowData     = RowData;
            this.ColumnNames = ColumnNames;
            this.ColumnType  = ColumnType;
        }

       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }            
       public boolean isCellEditable(int row,int col)
       {
               if(ColumnType[col]=="B" || ColumnType[col]=="E")
                  return true;
               return false;
       }
       /*public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   if(column==20)
                   {
                        String str = ((String)aValue).trim();
                        aValue = common.getDate(str,0,1);
                   }
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
                   //if(column>=2 && column<=7)
                      //setMaterialAmount(rowVector);
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }*/
      /*public void setMaterialAmount(Vector RowVector)
      {
              if(LBasic==null)
                    return;
              double dTGross=0,dTDisc=0,dTCenVat=0,dTTax=0,dTSur=0,dTNet=0;

              double dQty        = common.toDouble(common.parseNull((String)RowVector.elementAt(2)));
              double dRate       = common.toDouble(common.parseNull((String)RowVector.elementAt(3)));
              double dDiscPer    = common.toDouble(common.parseNull((String)RowVector.elementAt(4)));
              double dCenVatPer  = common.toDouble(common.parseNull((String)RowVector.elementAt(5)));
              double dTaxPer     = common.toDouble(common.parseNull((String)RowVector.elementAt(6)));
              double dSurPer     = common.toDouble(common.parseNull((String)RowVector.elementAt(7)));

              double dGross  = dQty*dRate;
              double dDisc   = dGross*dDiscPer/100;
              double dCenVat = (dGross-dDisc)*dCenVatPer/100;
              double dTax    = (dGross-dDisc+dCenVat)*dTaxPer/100;
              double dSur    = (dTax)*dSurPer/100;
              double dNet    = dGross-dDisc+dCenVat+dTax+dSur;

              RowVector.setElementAt(common.parseNull(common.getRound(dGross,2)),8);
              RowVector.setElementAt(common.parseNull(common.getRound(dDisc,2)),9);
              RowVector.setElementAt(common.parseNull(common.getRound(dCenVat,2)),10);
              RowVector.setElementAt(common.parseNull(common.getRound(dTax,2)),11);
              RowVector.setElementAt(common.parseNull(common.getRound(dSur,2)),12);
              RowVector.setElementAt(common.parseNull(common.getRound(dNet,2)),13);

              for(int i=0;i<super.dataVector.size();i++)
              {
                  Vector curVector = (Vector)super.dataVector.elementAt(i);
                  dTGross   = dTGross+common.toDouble((String)curVector.elementAt(8));
                  dTDisc    = dTDisc+common.toDouble((String)curVector.elementAt(9));
                  dTCenVat  = dTCenVat+common.toDouble((String)curVector.elementAt(10));
                  dTTax     = dTTax+common.toDouble((String)curVector.elementAt(11));
                  dTSur     = dTSur+common.toDouble((String)curVector.elementAt(12));
                  dTNet     = dTNet+common.toDouble((String)curVector.elementAt(13));
              }
              dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
              LBasic.setText(common.parseNull(common.getRound(dTGross,2)));
              LDiscount.setText(common.parseNull(common.getRound(dTDisc,2)));
              LCenVat.setText(common.parseNull(common.getRound(dTCenVat,2)));
              LTax.setText(common.parseNull(common.getRound(dTTax,2)));
              LSur.setText(common.parseNull(common.getRound(dTSur,2)));
              LNet.setText(common.parseNull(common.getRound(dTNet,2)));
    }*/
    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
                FinalData[i][j] = (String)curVector.elementAt(j);
        }
        return FinalData;
    }
    public int getRows()
    {
         return super.dataVector.size();
    }
    public Vector getCurVector(int i)
    {
         return (Vector)super.dataVector.elementAt(i);
    }
}
