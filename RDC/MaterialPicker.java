package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class MaterialPicker extends JInternalFrame
{
     JTextField     TMatCode,TMatName,THsnCode,TRate,TValue;
     JTextField     TIndicator;
     JButton        BOk,BCancel,BRefresh;
     
     JList          BrowList;
     JScrollPane    BrowScroll;
     JPanel         LeftPanel;
     JPanel         LeftCenterPanel,LeftBottomPanel;
     
     JLayeredPane      Layer;
     JTable            ReportTable;

     Vector            VSName,VSCode;
     Vector            VSNameCode,VSHsnCode;

     String         str ="";
     int iMillCode;
     int            iLastIndex= 0;
     Common         common    = new Common();
     String SItemTable;
     MaterialPicker(JLayeredPane Layer,JTable ReportTable,int iMillCode,String SItemTable)
     {
          this.Layer          = Layer;
          this.ReportTable    = ReportTable;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;

	  setDataIntoVector();
          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
     }

     public void createComponents()
     {
          BrowList            = new JList(VSNameCode);
          BrowScroll          = new JScrollPane();
          TMatCode            = new JTextField();
          TMatName            = new JTextField();
          THsnCode            = new JTextField();
          TIndicator          = new JTextField();
          TRate               = new JTextField();
          TValue              = new JTextField();
          TIndicator          . setEditable(false);
          
          BOk                 = new JButton("Okay");
          BCancel             = new JButton("Cancel");
          BRefresh            = new JButton("Refresh");
          
          LeftPanel           = new JPanel(true);
          LeftCenterPanel     = new JPanel(true);
          LeftBottomPanel     = new JPanel(true);
          
          TMatCode            . setEditable(false);
          TMatName            . setEditable(false);
          THsnCode            . setEditable(false);
          TRate               . setEditable(false);
          TValue              . setEditable(false);
     }

     public void setLayouts()
     {
          setBounds(80,50,650,500);
          setResizable(true);
          setClosable(true);
          setTitle("Material Selection");
          getContentPane()    . setLayout(new GridLayout(1,1));
          LeftPanel           . setLayout(new BorderLayout());
          LeftCenterPanel     . setLayout(new BorderLayout());
          LeftBottomPanel     . setLayout(new GridLayout(5,2));
     }

     public void addComponents()
     {
          getContentPane()    . add(LeftPanel);
          
          LeftPanel           . add("Center",LeftCenterPanel);
          LeftPanel           . add("South",LeftBottomPanel);
          
          LeftCenterPanel     . add("Center",BrowScroll);
          LeftCenterPanel     . add("South",TIndicator);
          LeftBottomPanel     . add(new JLabel("Material Name"));
          LeftBottomPanel     . add(TMatName);
          LeftBottomPanel     . add(new JLabel("Material Code"));
          LeftBottomPanel     . add(TMatCode);
          LeftBottomPanel     . add(new JLabel("HSN Code"));
          LeftBottomPanel     . add(THsnCode);
          LeftBottomPanel     . add(new JLabel("To Refresh")); 
          LeftBottomPanel     . add(BRefresh);
          LeftBottomPanel     . add(BOk);
          LeftBottomPanel     . add(BCancel);
     }

     public void setPresets()
     {
          int i = ReportTable.getSelectedRow();
          String SNameCode="";
          String SCode   = (String)ReportTable.getModel().getValueAt(i,1);
          String SName   = (String)ReportTable.getModel().getValueAt(i,2);
          String SHSNCode = (String)ReportTable.getModel().getValueAt(i,3);
          int iindex     = common.indexOf(VSCode,SCode);
          if(iindex > -1)
               SNameCode = (String)VSNameCode.elementAt(iindex);
          
          BrowList            . setAutoscrolls(true);
          BrowScroll          . getViewport().setView(BrowList);
          TMatCode            . setText(SCode);
          TMatName            . setText(SName);
          THsnCode            . setText(SHSNCode);

          show();
          ensureIndexIsVisible(SNameCode);
          LeftCenterPanel.updateUI();
     }

     public void addListeners()
     {
          BrowList  .addKeyListener(new KeyList());
          BOk       .addActionListener(new ActList());
          BCancel   .addActionListener(new ActList());
          BRefresh   .addActionListener(new ActList());
                    //addMouseListener(new MouseList());
     }

     /*public class MouseList extends MouseAdapter
     {
          public void mouseEntered(MouseEvent me)
          {
               BrowList.ensureIndexIsVisible(iLastIndex);               
          }
     }*/

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                         int       i = ReportTable.getSelectedRow();
     
                         String SNewCode  = TMatCode.getText();

                         String SHSNCode  = THsnCode.getText();

                        if(SHSNCode.equals(""))

                        {

                              JOptionPane.showMessageDialog(null,"HSNCode not Updated","Information",JOptionPane.INFORMATION_MESSAGE);
                              


                         }else

                         {
                         double dRate =setRateandValue(SNewCode,iMillCode);
                         ReportTable.getModel().setValueAt(TMatCode.getText(),i,1);
                         ReportTable.getModel().setValueAt(TMatName.getText(),i,2);
                         ReportTable.getModel().setValueAt(THsnCode.getText(),i,3);
                         String SQty   = (String)ReportTable.getModel().getValueAt(i,10);
                         ReportTable.getModel().setValueAt(String.valueOf(dRate),i,11);
 
                         double  dValue =common.toDouble(SQty)*dRate;

                        ReportTable.getModel().setValueAt(String.valueOf(dValue),i,12);
                            removeHelpFrame();
                            ReportTable.requestFocus();


                       }
               }
  
               if(ae.getSource()==BCancel)
               {

                  removeHelpFrame();
               }

               if(ae.getSource()==BRefresh)
              {

                  setDataIntoVector();
                    BrowList.setListData(VSNameCode);

              }
          }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==116)    // F5 is pressed
               {
                    setDataIntoVector();
                    BrowList.setListData(VSNameCode);
               }
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SMatName     = (String)VSName.elementAt(index);
                    String SMatCode     = (String)VSCode.elementAt(index);
                    String SMatNameCode = (String)VSNameCode.elementAt(index);
                    String SHsnCode     = (String)VSHsnCode.elementAt(index);
                    addMatDet(SMatName,SMatCode,SMatNameCode,SHsnCode,index);
                    str="";
               }
          }
     }

     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<VSNameCode.size();index++)
          {
               String str1 = ((String)VSNameCode.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index);
                    break;
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

     public boolean addMatDet(String SMatName,String SMatCode,String SMatNameCode,String SHsnCode,int index)
     {
          TMatCode       . setText(SMatCode);
          TMatName       . setText(SMatName);
          THsnCode       . setText(SHsnCode);

          return true;    
     }

     public boolean ensureIndexIsVisible(String SNameCode)
     {
          SNameCode = SNameCode.trim();
          int i=0;

          for(i=0;i<VSNameCode.size();i++)
          {
               String SCurName = (String)VSNameCode.elementAt(i);
               SCurName = SCurName.trim();
               if(SCurName.startsWith(SNameCode))
               break;
          }

          if(i==VSNameCode.size())
          {
               return false;
          }
          iLastIndex=i;
          BrowList.setSelectedIndex(i);
          BrowList.ensureIndexIsVisible(i);
          BrowList.requestFocus();
          BrowList.updateUI();
          return true;
     }

     public void setDataIntoVector()
     {
          VSName     = new Vector();
          VSCode     = new Vector();
          VSNameCode = new Vector();
          VSHsnCode  = new Vector();
            String QString = "";
          if(iMillCode==0)
          {

           QString = " Select Item_Name,Item_Code,HsnCode From InvItems Where HsnType=0 Order By Item_Name";
           }
           else
           {
           QString ="  Select Item_Name,"+SItemTable+".ITEM_CODE,HsnCode From "+SItemTable+" "+
                    "   inner join  InvItems on InvItems.ITEM_CODE="+SItemTable+".ITEM_CODE  "+
                    "    Where HsnType=0 Order By Item_Name";
   
            }           
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
               ResultSet      res            = stat.executeQuery(QString);

               while(res.next())
               {
                    String    str1      = res.getString(1);
                    String    str2      = res.getString(2);
                    String    str3      = res.getString(3);
                              VSName    . addElement(str1);
                              VSCode    . addElement(str2);
                              VSNameCode. addElement(str1+" (Code : "+str2+")"+" (HSNCode : "+str3+")");
                              VSHsnCode . addElement(str3);
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

//rate and value


   private double setRateandValue(String SNewCode,int iMillCode){
    
            double dQty=0.0;
            double dNet =0.0;
            double dOriginalRate=0.0;
	
                try{
                  

                    String QS=" select qty,net,max(orderdate) from purchaseorder where item_code='"+SNewCode+"'  and millcode="+iMillCode+" "+
                               " group by qty,net ";


		System.out.println("QS=>"+QS);	
			       ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
			       Connection     theConnection  =  oraConnection.getConnection();               
			       Statement      stat           =  theConnection.createStatement();
			       ResultSet      rs1            = stat.executeQuery(QS);


			     
			       if(rs1.next())
			       {
				   
				    dQty          = rs1.getDouble(1);
				    dNet           = rs1.getDouble(2);
				  

			       }
			       rs1                           . close();

			       stat . close();
			
  
              }catch(Exception ex){}


                  return dNet/dQty;

            
 
          }


 


}
