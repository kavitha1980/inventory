package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

import java.net.*;

public class MemoDeletionFrame extends JInternalFrame
{

     JPanel         MiddlePanel,BottomPanel;
     JTabbedPane    JTP;
     TabReport      tabreport;

     JButton        BDelete;

     Object         RowData[][];
     String         ColumnData[] = {"WorkShop/Supplier","Hod","Memo Type","Memo Date","Memo No","Select"};
     String         ColumnType[] = {"S"                ,"S"  ,"S"        ,"S"        ,"S"      ,"B"     };
     
     JLayeredPane   DeskTop;
     
     Common common = new Common();
     
     int            iMillCode,iUserCode;
     String         SSupTable,SYearCode;

     Vector         VMemoDate,VMemoNo,VHodName,VMemoType,VHodCode,VMemoTypeCode,VSupName,VSupCode;

     ORAConnection connect;
     Connection theconnect;

     public MemoDeletionFrame(JLayeredPane DeskTop,int iUserCode,int iMillCode,String SSupTable,String SYearCode)
     {
          super(" MEMOs Pending For RDC Conversion");
          this.DeskTop        = DeskTop;
          this.iUserCode      = iUserCode;
          this.iMillCode      = iMillCode;
          this.SSupTable      = SSupTable;
          this.SYearCode      = SYearCode;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          MiddlePanel  = new JPanel();
          BottomPanel  = new JPanel();
          JTP          = new JTabbedPane();

          BDelete      = new JButton("Delete");
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,850,475);
          MiddlePanel.setLayout(new BorderLayout());
     }

     public void addComponents()
     {
          try
          {
               JTP.removeAll();
               MiddlePanel.removeAll();
               getContentPane().remove(MiddlePanel);
               getContentPane().remove(BottomPanel);
          }
          catch(Exception ex){}

          getContentPane().add("Center",MiddlePanel);
          MiddlePanel.add("Center",JTP);

          BottomPanel.add(BDelete);

          setDataIntoVector();
          setRowData();

          try
          {
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

               JTP.addTab("Pending Memos",tabreport);

               if(RowData.length>0)
               {
                    getContentPane().add("South",BottomPanel);
               }

               setSelected(true);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void addListeners()
     {
          BDelete.addActionListener(new DeleteList());
     }

     public class DeleteList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               updateStatus();
          }
     }

     public void updateStatus()
     {
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }

               theconnect.setAutoCommit(false);

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean Bdelete   = (Boolean)RowData[i][5];   

                    String QS = "";

                    if(Bdelete.booleanValue())
                    {
                         if(JOptionPane.showConfirmDialog(null,"Are You Sure to Delete Memo?","Information",JOptionPane.YES_NO_OPTION) == 0)
                         {
                              String SMemoNo = (String)RowData[i][4];
                              String SMemoTypeCode = (String)VMemoTypeCode.elementAt(i);

                              QS = " Update RDCMemo set IsDelete=1 where MemoNo="+SMemoNo+" and DocType="+SMemoTypeCode+" and MillCode="+iMillCode;

                              PreparedStatement thePrepare1 = theconnect.prepareStatement(QS);
                              thePrepare1.executeUpdate();
                              thePrepare1.close();

                              JOptionPane.showMessageDialog(null," Memo Deleted Sucessfully ---->  "+SMemoNo);
                         }
                    }
               }
               theconnect.commit();
               theconnect.setAutoCommit(true);
               removeHelpFrame();
          }
          catch(Exception ex)
          {
               try
               {
                    JOptionPane.showMessageDialog(null," Problem in Deletion");
                    theconnect.rollback();
               }catch(Exception e){}
               System.out.println(ex);
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public void setDataIntoVector()
     {
          VMemoDate      = new Vector();
          VMemoNo        = new Vector();
          VHodName       = new Vector();
          VMemoType      = new Vector();
          VHodCode       = new Vector();
          VMemoTypeCode  = new Vector();
          VSupName       = new Vector();
          VSupCode       = new Vector();

          String QS = " Select distinct(RDCMemo.MemoNo),RDCMemo.MemoDate,Hod.HodName,"+
                      " decode(RDCMemo.DocType,0,'Returnable','Non-Returnable') as MemoTypeName,"+
                      " RDCMemo.HodCode,RDCMemo.DocType,"+SSupTable+".Name,RDCMemo.Sup_Code from RDCMemo "+
                      " Left Join Hod on RDCMemo.HodCode=Hod.HodCode "+
                      " Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = RDCMemo.Sup_Code "+
                      " Where RDCMemo.RDCStatus=0 and RDCMemo.AuthStatus=1 "+
                      " and RDCMemo.MillCode="+iMillCode+" and RDCMemo.isDelete=0 "+
                      " Order by "+SSupTable+".Name,RDCMemo.MemoDate,RDCMemo.MemoNo ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               ResultSet res = stat.executeQuery(QS);
               
               while (res.next())
               {
                    VMemoNo        . addElement(res.getString(1));
                    VMemoDate      . addElement(common.parseDate(res.getString(2)));
                    VHodName       . addElement(res.getString(3));
                    VMemoType      . addElement(res.getString(4));
                    VHodCode       . addElement(res.getString(5));
                    VMemoTypeCode  . addElement(res.getString(6));
                    VSupName       . addElement(res.getString(7));
                    VSupCode       . addElement(res.getString(8));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VMemoDate.size()][ColumnData.length];
          for(int i=0;i<VMemoDate.size();i++)
          {
               RowData[i][0]  = (String)VSupName  .elementAt(i);
               RowData[i][1]  = (String)VHodName  .elementAt(i);
               RowData[i][2]  = (String)VMemoType .elementAt(i);
               RowData[i][3]  = (String)VMemoDate .elementAt(i);
               RowData[i][4]  = (String)VMemoNo   .elementAt(i);
               RowData[i][5]  = new Boolean(false);
          }  
     }


}
