package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import jdbc.*;
import util.*;
import guiutil.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;


public class MonthlyGrnProvisionFrame extends JInternalFrame
{
     JPanel         TopPanel,MiddlePanel,BottomPanel;
     
     JTabbedPane    JTP;
     
     JButton        BSave;
     JLabel         LMonth,LNet;

     JTextField     TMatCode;
     
     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     int            iUserCode,iMillCode;

     TabReport      tabreport1,tabreport2,tabreport3,tabreport4;
     Object         RowData1[][],RowData2[][],RowData3[][],RowData4[][];
          
     String    ColumnData[]   = {"Grn No","Grn Date","Supplier Name","DC No/Date","Bill No","Bill Date","Amount","Remarks","Xerox Copy"};
     String    ColumnType[]   = {"N"     ,"S"       ,"S"            ,"S"         ,"S"      ,"S"        ,"N"     ,"B"      ,"B"         };
     int iColWidth[]          = {70      ,80        ,150            ,80          ,80       ,80         ,80      ,150      ,80          };

     Vector VGrnNo1,VGrnDate1,VSupName1,VDCNo1,VDCDate1,VInvNo1,VInvDate1,VValue1,VRemarks1,VXerox1;
     Vector VGrnNo2,VGrnDate2,VSupName2,VDCNo2,VDCDate2,VInvNo2,VInvDate2,VValue2,VRemarks2,VXerox2;
     Vector VGrnNo3,VGrnDate3,VSupName3,VDCNo3,VDCDate3,VInvNo3,VInvDate3,VValue3,VRemarks3,VXerox3;
     Vector VGrnNo4,VGrnDate4,VSupName4,VDCNo4,VDCDate4,VInvNo4,VInvDate4,VValue4,VRemarks4,VXerox4;
     
     Common         common   = new Common();

     Connection     theConnection = null;

     boolean bComflag = true;
     String SMonthName1;
     Document document;
     PdfPTable table,table1,table2;
     PdfPCell c1;

     private static Font mediumbold=FontFactory.getFont("TIMES_ROMAN",10,Font.BOLD);
     private static Font smallnormal=FontFactory.getFont("TIMES_ROMAN",8,Font.NORMAL);
     private static Font smallbold=FontFactory.getFont("TIMES_ROMAN",8,Font.BOLD);
     private static Font smallbold1=FontFactory.getFont("TIMES_ROMAN",6,Font.BOLD);

     int iWidth[] = {20,25,30,70,30,50,30,30,40,30};

     
     public MonthlyGrnProvisionFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode)
     {
          this.DeskTop    = DeskTop;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          
          ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                          theConnection =  oraConnection.getConnection();

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          showList();
     }
     
     public void createComponents()
     {
          TopPanel       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BottomPanel    = new JPanel(true);
          
          BSave  = new JButton("Save & Print");
          LMonth = new JLabel("");
          LNet   = new JLabel("");

          BSave.setMnemonic('S');
     }
     
     public void setLayouts()
     {
          MiddlePanel    . setLayout(new BorderLayout());
          
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setTitle("Monthrly Grn Provision");
          setBounds(0,0,950,550);
     }
     
     public void addComponents()
     {
          TopPanel.add(new JLabel("For Month     "));
          TopPanel.add(LMonth);

          BottomPanel.add(BSave);
          BottomPanel.add(new JLabel("     Total Value     "));
          BottomPanel.add(LNet);
          
          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("Center",MiddlePanel);
          getContentPane()    . add("South",BottomPanel);
     }
     
     public void addListeners()
     {
          BSave.addActionListener(new ActList());
     }
     
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validData())
               {
                    BSave.setEnabled(false);
                    updateValue();
                    getACommit();
                    removeHelpFrame();
               }
          }
     }

     public void showList()
     {
          try
          {
               MiddlePanel.remove(JTP);
          }
          catch(Exception ex){}


          String SDate     = common.getServerPureDate();
          String SMonth    = SDate.substring(0,6);

          SMonth = String.valueOf(common.getPreviousMonth(common.toInt(SMonth)));
          SDate  = SMonth+"01";

          String SStDate = SDate;

          String SDays = common.getMonthEndDate(SDate);

          String SEnDate = SMonth+SDays;

          String SMonthName = common.getMonthName(common.toInt(SEnDate));

          SMonthName1 = SMonthName;

          LMonth.setText(SMonthName);
          LMonth.setForeground(Color.red);

          setDataIntoVector(SStDate,SEnDate);
          setRowData();

          double dValue1 = common.toDouble(common.getSum(VValue1,"S",2));
          double dValue2 = common.toDouble(common.getSum(VValue2,"S",2));
          double dValue3 = common.toDouble(common.getSum(VValue3,"S",2));
          double dValue4 = common.toDouble(common.getSum(VValue4,"S",2));
          double dTValue = dValue1 + dValue2 + dValue3 + dValue4;

          LNet.setText(common.getRound(dTValue,2));
          LNet.setForeground(Color.red);

          JTP            = new JTabbedPane(JTabbedPane.TOP);
          JTP            .addTab("Previous Month Pending - GRN",getPreviousMonthGrn());
          JTP            .addTab("Previous Month Pending - Work GRN",getPreviousMonthWorkGrn());
          JTP            .addTab("Current Month Pending - GRN",getCurrentMonthGrn());
          JTP            .addTab("Current Month Pending - Work GRN",getCurrentMonthWorkGrn());
          MiddlePanel    .add("Center",JTP);
          DeskTop        .updateUI();
     }
     
     public JPanel getPreviousMonthGrn()
     {
          JPanel    panel     = new JPanel(true);
                    panel     .setLayout(new BorderLayout());
                    panel     .setBorder(new TitledBorder("List of Pending Grns"));
                    panel     .setBackground(new Color(80,160,160));
          
          tabreport1 = new TabReport(RowData1,ColumnData,ColumnType);
          tabreport1.setPrefferedColumnWidth1(iColWidth);

          panel.add(tabreport1);
          return panel;
     }

     public JPanel getPreviousMonthWorkGrn()
     {
          JPanel    panel     = new JPanel(true);
                    panel     .setLayout(new BorderLayout());
                    panel     .setBorder(new TitledBorder("List of Pending WorkGrns"));
                    panel     .setBackground(new Color(160,160,200));
          
          tabreport2 = new TabReport(RowData2,ColumnData,ColumnType);
          tabreport2.setPrefferedColumnWidth1(iColWidth);

          panel.add(tabreport2);
          return panel;
     }

     public JPanel getCurrentMonthGrn()
     {
          JPanel    panel     = new JPanel(true);
                    panel     .setLayout(new BorderLayout());
                    panel     .setBorder(new TitledBorder("List of Pending Grns"));
                    panel     .setBackground(new Color(70,160,250));
          
          tabreport3 = new TabReport(RowData3,ColumnData,ColumnType);
          tabreport3.setPrefferedColumnWidth1(iColWidth);

          panel.add(tabreport3);
          return panel;
     }

     public JPanel getCurrentMonthWorkGrn()
     {
          JPanel    panel     = new JPanel(true);
                    panel     .setLayout(new BorderLayout());
                    panel     .setBorder(new TitledBorder("List of Pending WorkGrns"));
                    panel     .setBackground(new Color(250,250,190));
          
          tabreport4 = new TabReport(RowData4,ColumnData,ColumnType);
          tabreport4.setPrefferedColumnWidth1(iColWidth);

          panel.add(tabreport4);
          return panel;
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();

          }
          catch(Exception ex){}
     }

     public void setDataIntoVector(String SStDate,String SEnDate)
     {
          VGrnNo1    = new Vector();
          VGrnDate1  = new Vector();
          VSupName1  = new Vector();
          VDCNo1     = new Vector();
          VDCDate1   = new Vector();
          VInvNo1    = new Vector();
          VInvDate1  = new Vector();
          VValue1    = new Vector();
          VRemarks1  = new Vector();
          VXerox1    = new Vector();

          VGrnNo2    = new Vector();
          VGrnDate2  = new Vector();
          VSupName2  = new Vector();
          VDCNo2     = new Vector();
          VDCDate2   = new Vector();
          VInvNo2    = new Vector();
          VInvDate2  = new Vector();
          VValue2    = new Vector();
          VRemarks2  = new Vector();
          VXerox2    = new Vector();

          VGrnNo3    = new Vector();
          VGrnDate3  = new Vector();
          VSupName3  = new Vector();
          VDCNo3     = new Vector();
          VDCDate3   = new Vector();
          VInvNo3    = new Vector();
          VInvDate3  = new Vector();
          VValue3    = new Vector();
          VRemarks3  = new Vector();
          VXerox3    = new Vector();

          VGrnNo4    = new Vector();
          VGrnDate4  = new Vector();
          VSupName4  = new Vector();
          VDCNo4     = new Vector();
          VDCDate4   = new Vector();
          VInvNo4    = new Vector();
          VInvDate4  = new Vector();
          VValue4    = new Vector();
          VRemarks4  = new Vector();
          VXerox4    = new Vector();



          String QS1 = " SELECT Grn.GrnNo,Grn.GrnDate,Supplier.Name,Grn.DCNo,Grn.DCDate,Grn.InvNo,Grn.InvDate,Sum(Grn.GrnValue) as GrnValue,Grn.ProvRemarks,Grn.ProvXerox,Max(Grn.SpjNo) "+
                       " FROM Grn "+
                       " Inner Join Supplier on Grn.Sup_Code=Supplier.Ac_Code "+
                       " and Grn.MillCode="+iMillCode+
                       " and Grn.GrnDate<'"+SStDate+"'"+
                       " Group by Grn.GrnNo,Grn.GrnDate,Supplier.Name,Grn.DCNo,Grn.DCDate,Grn.InvNo,Grn.InvDate,Grn.ProvRemarks,Grn.ProvXerox "+
                       " Having Max(Grn.SpjNo)=0 and Sum(Grn.GrnValue)>0 "+
                       " Order by Grn.GrnDate,Grn.GrnNo ";

          String QS2 = " SELECT WorkGrn.GrnNo,WorkGrn.GrnDate,Supplier.Name,WorkGrn.DCNo,WorkGrn.DCDate,WorkGrn.InvNo,WorkGrn.InvDate,Sum(WorkGrn.InvNet) as GrnValue,WorkGrn.ProvRemarks,WorkGrn.ProvXerox,Max(WorkGrn.SpjNo) "+
                       " FROM WorkGrn "+
                       " Inner Join Supplier on WorkGrn.Sup_Code=Supplier.Ac_Code "+
                       " and WorkGrn.MillCode="+iMillCode+
                       " and WorkGrn.GrnDate<'"+SStDate+"'"+
                       " Group by WorkGrn.GrnNo,WorkGrn.GrnDate,Supplier.Name,WorkGrn.DCNo,WorkGrn.DCDate,WorkGrn.InvNo,WorkGrn.InvDate,WorkGrn.ProvRemarks,WorkGrn.ProvXerox "+
                       " Having Max(WorkGrn.SpjNo)=0 and Sum(WorkGrn.InvNet)>0 "+
                       " Order by WorkGrn.GrnDate,WorkGrn.GrnNo ";

          String QS3 = " SELECT Grn.GrnNo,Grn.GrnDate,Supplier.Name,Grn.DCNo,Grn.DCDate,Grn.InvNo,Grn.InvDate,Sum(Grn.GrnValue) as GrnValue,Grn.ProvRemarks,Grn.ProvXerox,Max(Grn.SpjNo) "+
                       " FROM Grn "+
                       " Inner Join Supplier on Grn.Sup_Code=Supplier.Ac_Code "+
                       " and Grn.MillCode="+iMillCode+
                       " and Grn.GrnDate>='"+SStDate+"' and Grn.GrnDate<='"+SEnDate+"'"+
                       " Group by Grn.GrnNo,Grn.GrnDate,Supplier.Name,Grn.DCNo,Grn.DCDate,Grn.InvNo,Grn.InvDate,Grn.ProvRemarks,Grn.ProvXerox "+
                       " Having Max(Grn.SpjNo)=0 and Sum(Grn.GrnValue)>0 "+
                       " Order by Grn.GrnDate,Grn.GrnNo ";

          String QS4 = " SELECT WorkGrn.GrnNo,WorkGrn.GrnDate,Supplier.Name,WorkGrn.DCNo,WorkGrn.DCDate,WorkGrn.InvNo,WorkGrn.InvDate,Sum(WorkGrn.InvNet) as GrnValue,WorkGrn.ProvRemarks,WorkGrn.ProvXerox,Max(WorkGrn.SpjNo) "+
                       " FROM WorkGrn "+
                       " Inner Join Supplier on WorkGrn.Sup_Code=Supplier.Ac_Code "+
                       " and WorkGrn.MillCode="+iMillCode+
                       " and WorkGrn.GrnDate>='"+SStDate+"' and WorkGrn.GrnDate<='"+SEnDate+"'"+
                       " Group by WorkGrn.GrnNo,WorkGrn.GrnDate,Supplier.Name,WorkGrn.DCNo,WorkGrn.DCDate,WorkGrn.InvNo,WorkGrn.InvDate,WorkGrn.ProvRemarks,WorkGrn.ProvXerox "+
                       " Having Max(WorkGrn.SpjNo)=0 and Sum(WorkGrn.InvNet)>0 "+
                       " Order by WorkGrn.GrnDate,WorkGrn.GrnNo ";


          try
          {
               Statement stat = theConnection.createStatement();
               ResultSet res  = stat.executeQuery(QS1);

               while (res.next())
               {
                    VGrnNo1   .addElement(common.parseNull(res.getString(1)));
                    VGrnDate1 .addElement(common.parseDate(res.getString(2)));
                    VSupName1 .addElement(common.parseNull(res.getString(3)));
                    VDCNo1    .addElement(common.parseNull(res.getString(4)));
                    VDCDate1  .addElement(common.parseDate(res.getString(5)));
                    VInvNo1   .addElement(common.parseNull(res.getString(6)));
                    VInvDate1 .addElement(common.parseDate(res.getString(7)));
                    VValue1   .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(8))),2));
                    VRemarks1 .addElement(common.parseNull(res.getString(9)));
                    VXerox1   .addElement(common.parseNull(res.getString(10)));
               }
               res.close();

               res  = stat.executeQuery(QS2);

               while (res.next())
               {
                    VGrnNo2   .addElement(common.parseNull(res.getString(1)));
                    VGrnDate2 .addElement(common.parseDate(res.getString(2)));
                    VSupName2 .addElement(common.parseNull(res.getString(3)));
                    VDCNo2    .addElement(common.parseNull(res.getString(4)));
                    VDCDate2  .addElement(common.parseDate(res.getString(5)));
                    VInvNo2   .addElement(common.parseNull(res.getString(6)));
                    VInvDate2 .addElement(common.parseDate(res.getString(7)));
                    VValue2   .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(8))),2));
                    VRemarks2 .addElement(common.parseNull(res.getString(9)));
                    VXerox2   .addElement(common.parseNull(res.getString(10)));
               }
               res.close();

               res  = stat.executeQuery(QS3);

               while (res.next())
               {
                    VGrnNo3   .addElement(common.parseNull(res.getString(1)));
                    VGrnDate3 .addElement(common.parseDate(res.getString(2)));
                    VSupName3 .addElement(common.parseNull(res.getString(3)));
                    VDCNo3    .addElement(common.parseNull(res.getString(4)));
                    VDCDate3  .addElement(common.parseDate(res.getString(5)));
                    VInvNo3   .addElement(common.parseNull(res.getString(6)));
                    VInvDate3 .addElement(common.parseDate(res.getString(7)));
                    VValue3   .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(8))),2));
                    VRemarks3 .addElement(common.parseNull(res.getString(9)));
                    VXerox3   .addElement(common.parseNull(res.getString(10)));
               }
               res.close();

               res  = stat.executeQuery(QS4);

               while (res.next())
               {
                    VGrnNo4   .addElement(common.parseNull(res.getString(1)));
                    VGrnDate4 .addElement(common.parseDate(res.getString(2)));
                    VSupName4 .addElement(common.parseNull(res.getString(3)));
                    VDCNo4    .addElement(common.parseNull(res.getString(4)));
                    VDCDate4  .addElement(common.parseDate(res.getString(5)));
                    VInvNo4   .addElement(common.parseNull(res.getString(6)));
                    VInvDate4 .addElement(common.parseDate(res.getString(7)));
                    VValue4   .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(8))),2));
                    VRemarks4 .addElement(common.parseNull(res.getString(9)));
                    VXerox4   .addElement(common.parseNull(res.getString(10)));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
          RowData1 = new Object[VGrnNo1.size()][ColumnData.length];

          for(int i=0;i<VGrnNo1.size();i++)
          {
               RowData1[i][0]  = (String)VGrnNo1    .elementAt(i);
               RowData1[i][1]  = (String)VGrnDate1  .elementAt(i);
               RowData1[i][2]  = (String)VSupName1  .elementAt(i);
               RowData1[i][3]  = (String)VDCNo1.elementAt(i)+"/"+(String)VDCDate1.elementAt(i);
               RowData1[i][4]  = (String)VInvNo1    .elementAt(i);
               RowData1[i][5]  = (String)VInvDate1  .elementAt(i);
               RowData1[i][6]  = (String)VValue1    .elementAt(i);
               RowData1[i][7]  = (String)VRemarks1  .elementAt(i);
               RowData1[i][8]  = (String)VXerox1    .elementAt(i);
          }

          RowData2 = new Object[VGrnNo2.size()][ColumnData.length];

          for(int i=0;i<VGrnNo2.size();i++)
          {
               RowData2[i][0]  = (String)VGrnNo2    .elementAt(i);
               RowData2[i][1]  = (String)VGrnDate2  .elementAt(i);
               RowData2[i][2]  = (String)VSupName2  .elementAt(i);
               RowData2[i][3]  = (String)VDCNo2.elementAt(i)+"/"+(String)VDCDate2.elementAt(i);
               RowData2[i][4]  = (String)VInvNo2    .elementAt(i);
               RowData2[i][5]  = (String)VInvDate2  .elementAt(i);
               RowData2[i][6]  = (String)VValue2    .elementAt(i);
               RowData2[i][7]  = (String)VRemarks2  .elementAt(i);
               RowData2[i][8]  = (String)VXerox2    .elementAt(i);
          }

          RowData3 = new Object[VGrnNo3.size()][ColumnData.length];

          for(int i=0;i<VGrnNo3.size();i++)
          {
               RowData3[i][0]  = (String)VGrnNo3    .elementAt(i);
               RowData3[i][1]  = (String)VGrnDate3  .elementAt(i);
               RowData3[i][2]  = (String)VSupName3  .elementAt(i);
               RowData3[i][3]  = (String)VDCNo3.elementAt(i)+"/"+(String)VDCDate3.elementAt(i);
               RowData3[i][4]  = (String)VInvNo3    .elementAt(i);
               RowData3[i][5]  = (String)VInvDate3  .elementAt(i);
               RowData3[i][6]  = (String)VValue3    .elementAt(i);
               RowData3[i][7]  = (String)VRemarks3  .elementAt(i);
               RowData3[i][8]  = (String)VXerox3    .elementAt(i);
          }

          RowData4 = new Object[VGrnNo4.size()][ColumnData.length];

          for(int i=0;i<VGrnNo4.size();i++)
          {
               RowData4[i][0]  = (String)VGrnNo4    .elementAt(i);
               RowData4[i][1]  = (String)VGrnDate4  .elementAt(i);
               RowData4[i][2]  = (String)VSupName4  .elementAt(i);
               RowData4[i][3]  = (String)VDCNo4.elementAt(i)+"/"+(String)VDCDate4.elementAt(i);
               RowData4[i][4]  = (String)VInvNo4    .elementAt(i);
               RowData4[i][5]  = (String)VInvDate4  .elementAt(i);
               RowData4[i][6]  = (String)VValue4    .elementAt(i);
               RowData4[i][7]  = (String)VRemarks4  .elementAt(i);
               RowData4[i][8]  = (String)VXerox4    .elementAt(i);
          }
     }

     private boolean validData()
     {
          for(int i=0;i<RowData1.length;i++)
          {
               String SRemarks = ((String)RowData1[i][7]).trim();
               String SXerox   = ((String)RowData1[i][8]).trim();

               if(SRemarks.equals("") || SXerox.equals(""))
               {
                    JTP.setSelectedIndex(0);
                    JOptionPane.showMessageDialog(null,"Remarks and Xerox Column Not Filled in Row No - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(SRemarks.length()>100)
               {
                    JTP.setSelectedIndex(0);
                    JOptionPane.showMessageDialog(null,"Remarks Can't Exceed 100 Char. Current Length - "+SRemarks.length()+" in Row No - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(SXerox.length()>50)
               {
                    JTP.setSelectedIndex(0);
                    JOptionPane.showMessageDialog(null,"Xerox Can't Exceed 50 Char. Current Length - "+SXerox.length()+" in Row No - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }
          }

          for(int i=0;i<RowData2.length;i++)
          {
               String SRemarks = ((String)RowData2[i][7]).trim();
               String SXerox   = ((String)RowData2[i][8]).trim();

               if(SRemarks.equals("") || SXerox.equals(""))
               {
                    JTP.setSelectedIndex(1);
                    JOptionPane.showMessageDialog(null,"Remarks and Xerox Column Not Filled in Row No - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(SRemarks.length()>100)
               {
                    JTP.setSelectedIndex(1);
                    JOptionPane.showMessageDialog(null,"Remarks Can't Exceed 100 Char. Current Length - "+SRemarks.length()+" in Row No - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(SXerox.length()>50)
               {
                    JTP.setSelectedIndex(1);
                    JOptionPane.showMessageDialog(null,"Xerox Can't Exceed 50 Char. Current Length - "+SXerox.length()+" in Row No - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }
          }

          for(int i=0;i<RowData3.length;i++)
          {
               String SRemarks = ((String)RowData3[i][7]).trim();
               String SXerox   = ((String)RowData3[i][8]).trim();

               if(SRemarks.equals("") || SXerox.equals(""))
               {
                    JTP.setSelectedIndex(2);
                    JOptionPane.showMessageDialog(null,"Remarks and Xerox Column Not Filled in Row No - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(SRemarks.length()>100)
               {
                    JTP.setSelectedIndex(2);
                    JOptionPane.showMessageDialog(null,"Remarks Can't Exceed 100 Char. Current Length - "+SRemarks.length()+" in Row No - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(SXerox.length()>50)
               {
                    JTP.setSelectedIndex(2);
                    JOptionPane.showMessageDialog(null,"Xerox Can't Exceed 50 Char. Current Length - "+SXerox.length()+" in Row No - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }
          }

          for(int i=0;i<RowData4.length;i++)
          {
               String SRemarks = ((String)RowData4[i][7]).trim();
               String SXerox   = ((String)RowData4[i][8]).trim();

               if(SRemarks.equals("") || SXerox.equals(""))
               {
                    JTP.setSelectedIndex(3);
                    JOptionPane.showMessageDialog(null,"Remarks and Xerox Column Not Filled in Row No - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(SRemarks.length()>100)
               {
                    JTP.setSelectedIndex(3);
                    JOptionPane.showMessageDialog(null,"Remarks Can't Exceed 100 Char. Current Length - "+SRemarks.length()+" in Row No - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(SXerox.length()>50)
               {
                    JTP.setSelectedIndex(3);
                    JOptionPane.showMessageDialog(null,"Xerox Can't Exceed 50 Char. Current Length - "+SXerox.length()+" in Row No - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }
          }

          return true;
     }

     public void updateValue()
     {
          try
          {                                                          
               Statement stat = theConnection.createStatement();
               
               for(int i=0;i<RowData1.length;i++)
               {
                    String SGrnNo = (String)VGrnNo1.elementAt(i);

                    String SRemarks = ((String)RowData1[i][7]).toUpperCase();
                    String SXerox   = ((String)RowData1[i][8]).toUpperCase();

                    String QString = "Update Grn Set ";
                    QString = QString+" ProvRemarks='"+SRemarks+"',ProvXerox='"+SXerox+"'";
                    QString = QString+" Where GrnNo="+SGrnNo+" and MillCode="+iMillCode;
               
                    if(theConnection   . getAutoCommit())
                         theConnection   . setAutoCommit(false);

                    stat.execute(QString);
               }

               for(int i=0;i<RowData2.length;i++)
               {
                    String SGrnNo = (String)VGrnNo2.elementAt(i);

                    String SRemarks = ((String)RowData2[i][7]).toUpperCase();
                    String SXerox   = ((String)RowData2[i][8]).toUpperCase();

                    String QString = "Update WorkGrn Set ";
                    QString = QString+" ProvRemarks='"+SRemarks+"',ProvXerox='"+SXerox+"'";
                    QString = QString+" Where GrnNo="+SGrnNo+" and MillCode="+iMillCode;
               
                    if(theConnection   . getAutoCommit())
                         theConnection   . setAutoCommit(false);

                    stat.execute(QString);
               }

               for(int i=0;i<RowData3.length;i++)
               {
                    String SGrnNo = (String)VGrnNo3.elementAt(i);

                    String SRemarks = ((String)RowData3[i][7]).toUpperCase();
                    String SXerox   = ((String)RowData3[i][8]).toUpperCase();

                    String QString = "Update Grn Set ";
                    QString = QString+" ProvRemarks='"+SRemarks+"',ProvXerox='"+SXerox+"'";
                    QString = QString+" Where GrnNo="+SGrnNo+" and MillCode="+iMillCode;
               
                    if(theConnection   . getAutoCommit())
                         theConnection   . setAutoCommit(false);

                    stat.execute(QString);
               }

               for(int i=0;i<RowData4.length;i++)
               {
                    String SGrnNo = (String)VGrnNo4.elementAt(i);

                    String SRemarks = ((String)RowData4[i][7]).toUpperCase();
                    String SXerox   = ((String)RowData4[i][8]).toUpperCase();

                    String QString = "Update WorkGrn Set ";
                    QString = QString+" ProvRemarks='"+SRemarks+"',ProvXerox='"+SXerox+"'";
                    QString = QString+" Where GrnNo="+SGrnNo+" and MillCode="+iMillCode;
               
                    if(theConnection   . getAutoCommit())
                         theConnection   . setAutoCommit(false);

                    stat.execute(QString);
               }

               stat.close();
          }
          catch(Exception e)
          {
               System.out.println(e);
               bComflag  = false;
          }
     }

     private void getACommit()
     {
          try
          {
                 if(bComflag)
                {
                    theConnection  . commit();
                    createPDF();
                    JOptionPane    . showMessageDialog(null,"Data Saved and PDF File Created","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("Commit");
               }
                else
               {
                    theConnection  . rollback();
                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theConnection   . setAutoCommit(true);
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

        private void createPDF()
        {

           try

            {

      int rowcount     = 0;
      double dTotvalue =0.0;
      int Slcount      = 0;

      document = new Document(PageSize.A4);
      PdfWriter. getInstance(document,new FileOutputStream(common.getPrintPath()+"MonthlyGrnProvisionList.pdf"));
      // PdfWriter. getInstance(document,new FileOutputStream("d:\\MonthlyGrnProvisionList.pdf"));
      document . open();

     Paragraph paragraph;

     table  = new PdfPTable(10);
     table  . setWidths(iWidth);
     table  . setWidthPercentage(100);

        
          if(iMillCode==0)
          {
               c1         = new PdfPCell(new Phrase("AMARJOTHI SPINNING MILLS LTD",mediumbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . hasMinimumHeight();
               c1         . setColspan(10);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.TOP);
               table      . addCell(c1);

  
          }

          if(iMillCode==1)
          {
               c1         = new PdfPCell(new Phrase("AMARJOTHI SPINNING MILLS LTD (DYEING DIVISION)",mediumbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . hasMinimumHeight();
               c1         . setColspan(10);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.TOP);
               table      . addCell(c1);

    
          }

               c1         = new PdfPCell(new Phrase("PROVISION FOR THE MONTH OF "+SMonthName1+"  ",mediumbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . hasMinimumHeight();
               c1         . setColspan(10);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.BOTTOM);
               table      . addCell(c1);



                AddCellIntoTable("SL.NO",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("GRN.NO",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("GRN.DATE",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("SUPPLIER NAME",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("DCNO/DCDATE ",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("BILL NO",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("BILL DATE",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("AMOUNT",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("REMARKS",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("XEROX COPY",table,Element.ALIGN_CENTER,smallbold);

              //Before pending GRN
   
              //  AddCellIntoTable("PREVIOUS MONTH PENDING BEFORE  "+SMonthName1+" ",table,Element.ALIGN_CENTER,smallbold,10);
               
               c1         = new PdfPCell(new Phrase(" ",smallbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . setFixedHeight(20f);
               c1         . setColspan(10);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.TOP);
               table      . addCell(c1);

               c1         = new PdfPCell(new Phrase("PREVIOUS MONTH PENDING BEFORE "+SMonthName1+" (GRN) ",smallbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . setFixedHeight(20f);
               c1         . setColspan(10);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.BOTTOM);
               table      . addCell(c1);


                rowcount = VGrnNo1.size();

                 System.out.println( "rowcount1-->"+rowcount);

                if(rowcount >0 )
                {
                
                for(int i=0;i<VGrnNo1.size();i++)
                  {

                double dValue = common.toDouble((String)VValue1.elementAt(i));
                Slcount = Slcount +1 ;
                
               String SRemarks = ((String)RowData1[i][7]).trim();
               String SXerox   = ((String)RowData1[i][8]).trim();

                AddCellIntoTable(""+Slcount+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VGrnNo1.elementAt(i)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VGrnDate1.elementAt(i)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VSupName1.elementAt(i)+"",table,Element.ALIGN_LEFT,smallnormal);
                AddCellIntoTable(""+VDCNo1.elementAt(i)+"/"+common.parseDate((String)VDCDate1.elementAt(i))+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VInvNo1.elementAt(i)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+common.parseDate((String)VInvDate1.elementAt(i))+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VValue1.elementAt(i)+"",table,Element.ALIGN_RIGHT,smallnormal);
                AddCellIntoTable(""+SRemarks+"",table,Element.ALIGN_LEFT,smallnormal);
                AddCellIntoTable(""+SXerox+"",table,Element.ALIGN_LEFT,smallnormal);

                dTotvalue = dTotvalue + dValue;

                  }

                }

                else

                {  AddCellIntoTable("No Pending",table,Element.ALIGN_LEFT,smallnormal,10); }


              //   before pending work GRN

              //   AddCellIntoTable("WORK GRN",table,Element.ALIGN_CENTER,smallbold,10);

      



               c1         = new PdfPCell(new Phrase(" ",smallbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . setFixedHeight(15f);
               c1         . setColspan(10);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.TOP);
               table      . addCell(c1);

               c1         = new PdfPCell(new Phrase("WORK GRN",smallbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . setFixedHeight(15f);
               c1         . setColspan(10);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.BOTTOM);
               table      . addCell(c1);


                 rowcount = VGrnNo2.size();
                 System.out.println( "rowcount2-->"+rowcount);

                if(rowcount >0 )
                {
                
                for(int j=0;j<VGrnNo2.size();j++)
                  {

                  double dValue = common.toDouble((String)VValue2.elementAt(j));
                Slcount = Slcount +1 ;
                  
               String SRemarks = ((String)RowData2[j][7]).trim();
               String SXerox   = ((String)RowData2[j][8]).trim();


                AddCellIntoTable(""+Slcount+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VGrnNo2.elementAt(j)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VGrnDate2.elementAt(j)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VSupName2.elementAt(j)+"",table,Element.ALIGN_LEFT,smallnormal);
                AddCellIntoTable(""+VDCNo2.elementAt(j)+"/"+common.parseDate((String)VDCDate2.elementAt(j))+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VInvNo2.elementAt(j)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+common.parseDate((String)VInvDate2.elementAt(j))+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VValue2.elementAt(j)+"",table,Element.ALIGN_RIGHT,smallnormal);
                AddCellIntoTable(""+SRemarks+"",table,Element.ALIGN_LEFT,smallnormal);
                AddCellIntoTable(""+SXerox+"",table,Element.ALIGN_LEFT,smallnormal);
                dTotvalue = dTotvalue + dValue;

                


                  }

                }

                else

                {  AddCellIntoTable("No Pending",table,Element.ALIGN_LEFT,smallnormal,10); }



             

               // Current Month pending GRN

               // AddCellIntoTable("CURRENT MONTH PENDING ("+SMonthName1+") ",table,Element.ALIGN_LEFT,smallbold,10);

               c1         = new PdfPCell(new Phrase(" ",smallbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . setFixedHeight(20f);
               c1         . setColspan(10);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.TOP);
               table      . addCell(c1);

               c1         = new PdfPCell(new Phrase("CURRENT MONTH PENDING (GRN) ",smallbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . setFixedHeight(20f);
               c1         . setColspan(10);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.BOTTOM);
               table      . addCell(c1);



                rowcount = VGrnNo3.size();
                 System.out.println( "rowcount3-->"+rowcount);

                if(rowcount >0 )
                {
                
                for(int k=0;k<VGrnNo3.size();k++)
                  {
                  double dValue = common.toDouble((String)VValue3.elementAt(k));
                Slcount = Slcount +1 ;

                     
               String SRemarks = ((String)RowData3[k][7]).trim();
               String SXerox   = ((String)RowData3[k][8]).trim();


                AddCellIntoTable(""+Slcount+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VGrnNo3.elementAt(k)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VGrnDate3.elementAt(k)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VSupName3.elementAt(k)+"",table,Element.ALIGN_LEFT,smallnormal);
                AddCellIntoTable(""+VDCNo3.elementAt(k)+"/"+common.parseDate((String)VDCDate3.elementAt(k))+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VInvNo3.elementAt(k)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+common.parseDate((String)VInvDate3.elementAt(k))+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VValue3.elementAt(k)+"",table,Element.ALIGN_RIGHT,smallnormal);
                AddCellIntoTable(""+SRemarks+"",table,Element.ALIGN_LEFT,smallnormal);
                AddCellIntoTable(""+SXerox+"",table,Element.ALIGN_LEFT,smallnormal);
                dTotvalue = dTotvalue + dValue;


                  }

                }

                else

                {  AddCellIntoTable("No Pending",table,Element.ALIGN_LEFT,smallnormal,10); }


                // Current Month pending Work-GRN

                //AddCellIntoTable("WORK GRN ",table,Element.ALIGN_CENTER,smallbold,10);



               c1         = new PdfPCell(new Phrase(" ",smallbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . setFixedHeight(15f);
               c1         . setColspan(10);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.TOP);
               table      . addCell(c1);

               c1         = new PdfPCell(new Phrase("WORK GRN",smallbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . setFixedHeight(15f);
               c1         . setColspan(10);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.BOTTOM);
               table      . addCell(c1);

                rowcount = VGrnNo4.size();
                System.out.println( "rowcount4-->"+rowcount);

                if(rowcount >0 )
                {
                
                for(int l=0;l<VGrnNo4.size();l++)
                  {

                double dValue = common.toDouble((String)VValue4.elementAt(l));
                Slcount = Slcount +1 ;
 
 
                String SRemarks = ((String)RowData4[l][7]).trim();
                String SXerox   = ((String)RowData4[l][8]).trim();




                AddCellIntoTable(""+Slcount+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VGrnNo4.elementAt(l)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VGrnDate4.elementAt(l)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VSupName4.elementAt(l)+"",table,Element.ALIGN_LEFT,smallnormal);
                AddCellIntoTable(""+VDCNo4.elementAt(l)+"/"+common.parseDate((String)VDCDate4.elementAt(l))+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VInvNo4.elementAt(l)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+common.parseDate((String)VInvDate4.elementAt(l))+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VValue4.elementAt(l)+"",table,Element.ALIGN_RIGHT,smallnormal);
                AddCellIntoTable(""+SRemarks+"",table,Element.ALIGN_LEFT,smallnormal);
                AddCellIntoTable(""+SXerox+"",table,Element.ALIGN_LEFT,smallnormal);
                dTotvalue = dTotvalue + dValue;


                  }

                }

                else

                {  AddCellIntoTable("No Pending",table,Element.ALIGN_LEFT,smallnormal,10); }
 

       //      Total


                 AddCellIntoTable(" ",table,Element.ALIGN_CENTER,smallnormal);
                 AddCellIntoTable(" ",table,Element.ALIGN_CENTER,smallnormal);
                 AddCellIntoTable(" ",table,Element.ALIGN_CENTER,smallnormal);
                 AddCellIntoTable("TOTAL",table,Element.ALIGN_CENTER,smallbold);
                 AddCellIntoTable(" ",table,Element.ALIGN_CENTER,smallnormal);
                 AddCellIntoTable(" ",table,Element.ALIGN_CENTER,smallnormal);
                 AddCellIntoTable(" ",table,Element.ALIGN_CENTER,smallnormal);
                 AddCellIntoTable(""+common.getRound(dTotvalue,2)+"",table,Element.ALIGN_RIGHT,smallbold);
                 AddCellIntoTable(" ",table,Element.ALIGN_CENTER,smallnormal);
                 AddCellIntoTable(" ",table,Element.ALIGN_CENTER,smallnormal);


           //     Sign

 
          PdfPCell c1 = new PdfPCell(new Phrase(" ",smallbold1));  
          c1.setHorizontalAlignment(Element.ALIGN_CENTER);
          c1.setFixedHeight(20f);
          c1.setColspan(5);
          c1.setBorder(Rectangle.LEFT|Rectangle.TOP);
          table.addCell(c1);

          c1 = new PdfPCell(new Phrase(" ",smallbold1));  
          c1.setHorizontalAlignment(Element.ALIGN_CENTER);
          c1.setFixedHeight(20f);
          c1.setColspan(5);
          c1.setBorder(Rectangle.RIGHT | Rectangle.TOP);
          table.addCell(c1); 


          
          c1 = new PdfPCell(new Phrase("                SK ",smallnormal));  
          c1.setHorizontalAlignment(Element.ALIGN_LEFT);
          c1.hasMinimumHeight();
          c1.setColspan(5);
          c1.setBorder(Rectangle.LEFT|Rectangle.BOTTOM);
          table.addCell(c1);

          c1 = new PdfPCell(new Phrase("       SO",smallnormal));  
          c1.setHorizontalAlignment(Element.ALIGN_CENTER);
          c1.hasMinimumHeight();
          c1.setColspan(5);
          c1.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
          table.addCell(c1); 
                
                
                document . add(table);
                document . close();
                 
                dTotvalue =0;
                rowcount  =0;
                Slcount   =0;

                 }


             catch(Exception e)
                 { e.printStackTrace(); }



          }

          
        private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,Font docFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,docFont));  
          c1.setHorizontalAlignment(iHorizontal);
          c1.hasMinimumHeight();
          c1.setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.TOP|Rectangle.BOTTOM);
          table.addCell(c1);
      }
          
        private void AddCellIntoTable(int iRowspan,String Str,PdfPTable table,int iHorizontal,Font docFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,docFont));  
          c1.setHorizontalAlignment(iHorizontal);
          c1.hasMinimumHeight();
          c1.setRowspan(iRowspan);
          c1.setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.TOP|Rectangle.BOTTOM);
          table.addCell(c1);
      }
          private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,Font docFont,int iColspan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,docFont));  
          c1.setHorizontalAlignment(iHorizontal);
          c1.hasMinimumHeight();
          c1.setColspan(iColspan);
          c1.setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.TOP|Rectangle.BOTTOM);
          table.addCell(c1);
      }


}
