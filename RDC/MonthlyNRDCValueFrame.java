package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class MonthlyNRDCValueFrame extends JInternalFrame
{
     JPanel         TopPanel,BottomPanel;

     JButton        BSave;
     JLabel         LMonth,LNet;

     TabReport      tabreport;
     Object         RowData[][];
          
     String ColumnData[] = {"Rdc No","Rdc Date","Description of Goods","Uom","Send Qty","Rate","Value","Purpose","Select"};
     String ColumnType[] = {"N"     ,"S"       ,"S"                   ,"S"  ,"N"       ,"E"   ,"E"    ,"B"      ,"B"     };
     int iColWidth[]     = {70      ,80        ,230                   ,50   ,80        ,80    ,80     ,230      ,60      };
     
     Common common   = new Common();

     Connection     theConnection = null;

     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     int            iMillCode,iUserCode;
     String         SSupTable,SYearCode,SMillName;

     Vector         VRDCNo,VRDCDate,VDescript,VRemarks,VQty,VUom,VId;

     boolean bComflag = true;

     MonthlyNRDCValueFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable,String SYearCode,String SMillName)
     {
          super("NRDC List");

          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;
          this.SMillName = SMillName;

          ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                          theConnection =  oraConnection.getConnection();

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setTabReport();
     }

     public void createComponents()
     {
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          BSave  = new JButton("Save");
          LMonth = new JLabel("");
          LNet   = new JLabel("");

          BSave.setEnabled(false);
          BSave.setMnemonic('S');

          LMonth.setFont(new Font("monospaced", Font.BOLD, 14));
          LNet.setFont(new Font("monospaced", Font.BOLD, 14));
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,980,500);
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("For Month     "));
          TopPanel.add(LMonth);

          BottomPanel.add(BSave);
          BottomPanel.add(new JLabel("     Total Value     "));
          BottomPanel.add(LNet);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BSave.addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validData())
               {
                    BSave.setEnabled(false);
                    updateValue();
                    getACommit();
                    removeHelpFrame();
               }
          }
     }

     public void setTabReport()
     {
          String SDate     = common.getServerPureDate();
          String SMonth    = SDate.substring(0,6);

          SMonth = String.valueOf(common.getPreviousMonth(common.toInt(SMonth)));
          SDate  = SMonth+"01";

          String SDays = common.getMonthEndDate(SDate);

          String SEnDate = SMonth+SDays;

          String SMonthName = common.getMonthName(common.toInt(SEnDate));

          LMonth.setText(SMonthName);
          LMonth.setForeground(Color.red);

          setDataIntoVector(SEnDate);
          setRowData();

          try
          {
               getContentPane().remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.setPrefferedColumnWidth1(iColWidth);

               getContentPane().add(tabreport,BorderLayout.CENTER);
               DeskTop.repaint();
               DeskTop.updateUI();
               if(RowData.length>0)
               {
                    BSave.setEnabled(true);
               }
               tabreport.ReportTable.addKeyListener(new KeyList());
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();

          }
          catch(Exception ex){}
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               int iRow = tabreport.ReportTable.getSelectedRow();
               int iCol = tabreport.ReportTable.getSelectedColumn();

               if(iCol==5)
               {
                   setValue();
               }

               if(iCol==6)
               {
                   setRate();
               }
          }
     }

     private void setValue()
     {
          double dValue  = 0;
          double dTValue = 0;

          for(int i=0;i<RowData.length;i++)
          {
               double dQty   = common.toDouble((String)tabreport.ReportTable.getValueAt(i,4));
               double dRate  = common.toDouble((String)tabreport.ReportTable.getValueAt(i,5));
               dValue        = dQty*dRate;
               dTValue       = dTValue + dValue;
     
               tabreport.ReportTable.setValueAt(common.getRound(dValue,2),i,6);

               if(dValue>0)
               {
                    tabreport.ReportTable.setValueAt(new Boolean(true),i,8);
               }
          }

          LNet.setText(common.getRound(dTValue,2));
          LNet.setForeground(Color.red);

          DeskTop.repaint();
          DeskTop.updateUI();
     }

     private void setRate()
     {
          double dRate  = 0;
          double dTValue = 0;

          for(int i=0;i<RowData.length;i++)
          {
               double dQty   = common.toDouble((String)tabreport.ReportTable.getValueAt(i,4));
               double dValue = common.toDouble((String)tabreport.ReportTable.getValueAt(i,6));
               dRate         = dValue / dQty;
               dTValue       = dTValue + dValue;
     
               tabreport.ReportTable.setValueAt(common.getRound(dRate,3),i,5);

               if(dValue>0)
               {
                    tabreport.ReportTable.setValueAt(new Boolean(true),i,8);
               }
          }

          LNet.setText(common.getRound(dTValue,2));
          LNet.setForeground(Color.red);

          DeskTop.repaint();
          DeskTop.updateUI();
     }

     public void setDataIntoVector(String SEnDate)
     {
          String SSupCode="";

          if(iMillCode==0)
          {
               SSupCode = "720725";
          }

          if(iMillCode==1)
          {
               SSupCode = "X1012";
          }

          String QS = " SELECT RDC.RDCNo, RDC.RDCDate, RDC.Descript, RDC.Remarks, RDC.Qty, RDC.Id, UOM.UomName "+
                      " FROM RDC "+
                      " Inner Join UOM on RDC.UomCode=UOM.UomCode "+
                      " Where RDC.DocType=1 and RDC.ValueStatus=0 and RDC.MillCode = "+iMillCode+
                      " and RDC.Sup_Code='"+SSupCode+"' and RDC.RDCDate<='"+SEnDate+"'"+
                      " Order by RDC.RDCDate,RDC.RdcNo,Rdc.Id ";

          VRDCNo   = new Vector();
          VRDCDate = new Vector();
          VDescript= new Vector();
          VRemarks = new Vector();
          VQty     = new Vector();
          VId      = new Vector();
          VUom     = new Vector();

          try
          {
               Statement stat = theConnection.createStatement();
               ResultSet res  = stat.executeQuery(QS);

               while (res.next())
               {
                    VRDCNo   .addElement(common.parseNull(res.getString(1)));
                    VRDCDate .addElement(common.parseDate(res.getString(2)));
                    VDescript.addElement(common.parseNull(res.getString(3)));
                    VRemarks .addElement(common.parseNull(res.getString(4)));
                    VQty     .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(5))),3));
                    VId      .addElement(common.parseNull(res.getString(6)));
                    VUom      .addElement(common.parseNull(res.getString(7)));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VRDCNo.size()][ColumnData.length];

          for(int i=0;i<VRDCNo.size();i++)
          {
               RowData[i][0]  = (String)VRDCNo    .elementAt(i);
               RowData[i][1]  = (String)VRDCDate  .elementAt(i);
               RowData[i][2]  = (String)VDescript .elementAt(i);
               RowData[i][3]  = (String)VUom.elementAt(i);
               RowData[i][4]  = (String)VQty      .elementAt(i);
               RowData[i][5]  = "";
               RowData[i][6]  = "";
               RowData[i][7]  = (String)VRemarks  .elementAt(i);
               RowData[i][8]  = new Boolean(false);
          }
     }

     private boolean validData()
     {
          int iCount=0;

          for(int i=0;i<RowData.length;i++)
          {
               Boolean Bselected = (Boolean)RowData[i][8];

               if(!Bselected.booleanValue())
               {
                    JOptionPane.showMessageDialog(null,"Row No - "+String.valueOf(i+1)+" Not Selected","Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               double dValue = common.toDouble((String)RowData[i][6]);

               if(dValue<0)
               {
                    JOptionPane.showMessageDialog(null,"Wrong Value for Row - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }
          }

          return true;
     }

     public void updateValue()
     {
          try
          {                                                          
               Statement stat = theConnection.createStatement();
               
               for(int i=0;i<RowData.length;i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][8];
                    if(Bselected.booleanValue())
                    {
                         String SId    = (String)VId.elementAt(i);
                         String SRDCNo = (String)VRDCNo.elementAt(i);

                         String SRate    = common.getRound(common.toDouble((String)RowData[i][5]),3);
                         String SValue   = common.getRound(common.toDouble((String)RowData[i][6]),2);
                         String SRemarks = ((String)RowData[i][7]).toUpperCase();

                         String QString = "Update RDC Set ";
                         QString = QString+" ValueStatus = 1,Rate="+SRate+",NetValue="+SValue+",ValueRemarks='"+SRemarks+"'";
                         QString = QString+" Where Id = "+SId+" and RDCNo="+SRDCNo+" and MillCode="+iMillCode;
                    
                         if(theConnection   . getAutoCommit())
                              theConnection   . setAutoCommit(false);

                         stat.execute(QString);
                    }
               }
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println(e);
               bComflag  = false;
          }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnection  . commit();
                    JOptionPane    . showMessageDialog(null,"The Entered Data is Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("Commit");
               }
               else
               {
                    theConnection  . rollback();
                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theConnection   . setAutoCommit(true);
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }


}
