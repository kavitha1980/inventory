
package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

public class MonthlyNRDCValuePrintFrame extends JInternalFrame
{
     JPanel         TopPanel,BottomPanel;

     JButton        BPrint,BExit;
     JLabel         LMonth,LNet;

     TabReport      tabreport;
     Object         RowData[][];
          
     String ColumnData[] = {"Sl.No.","NRDC No","DATE","MATERIAL NAME","UOM","QTY","VALUE","REASON"};
     String ColumnType[] = {"N"     ,"N"      ,"S"   ,"S"            ,"S"  ,"N"  ,"N"    ,"B"     };
     int iColWidth[]     = {70      ,70       ,80    ,230            ,60   ,80   ,80     ,250     };
     
     Common common   = new Common();

     Connection     theConnection = null;

     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     int            iMillCode,iUserCode;
     String         SSupTable,SYearCode,SMillName,SMonthName1;

     Vector         VRDCNo,VRDCDate,VDescript,VRemarks,VQty,VValue,VUom;

     Document document;
     PdfPTable table,table1,table2;
     PdfPCell c1;

     private static Font mediumbold=FontFactory.getFont("TIMES_ROMAN",10,Font.BOLD);
     private static Font smallnormal=FontFactory.getFont("TIMES_ROMAN",8,Font.NORMAL);
     private static Font smallbold=FontFactory.getFont("TIMES_ROMAN",8,Font.BOLD);

     int iWidth[] = {10,15,25,50,20,20,30,50};


     MonthlyNRDCValuePrintFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable,String SYearCode,String SMillName)
     {
          super("NRDC Valuation List");

          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;
          this.SMillName = SMillName;

          ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                          theConnection =  oraConnection.getConnection();

          createComponents();
          setLayouts();
          addComponents();
          setTabReport();
        

          addListeners();
          
     }

     public void createComponents()
     {
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          BPrint = new JButton("Print");
          BExit  = new JButton("EXit");

          LMonth = new JLabel("");
          LNet   = new JLabel("");

          BPrint.setEnabled(false);
          BPrint.setMnemonic('P');

         // LMonth.setFont(new Font("monospaced", Font.BOLD, 14));
         // LNet.setFont(new Font("monospaced", Font.BOLD, 14));
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,950,500);
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("For Month     "));
          TopPanel.add(LMonth);

       
          BottomPanel.add(BPrint);
          BottomPanel.add(new JLabel("     Total Value     "));
          BottomPanel.add(LNet);
          BottomPanel.add(BExit);
         

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BPrint.addActionListener(new PrintList());
          BExit .addActionListener(new PrintList());
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
              if(ae.getSource() == BExit)
                {
                   removeHelpFrame();
                }
 
               if(ae.getSource() == BPrint)
                {

                 createPDF();
                 JOptionPane.showMessageDialog(null,"Print Generated","Information",JOptionPane.INFORMATION_MESSAGE);     

                }
          }
     }

     public void setTabReport()
     {
          String SDate     = common.getServerPureDate();
          String SMonth    = SDate.substring(0,6);

          SMonth = String.valueOf(common.getPreviousMonth(common.toInt(SMonth)));
          SDate  = SMonth+"01";

          String SDays = common.getMonthEndDate(SDate);

          String SStDate = SDate;
          String SEnDate = SMonth+SDays;

          String SMonthName = common.getMonthName(common.toInt(SEnDate));
          SMonthName1 =  SMonthName;

          LMonth.setText(SMonthName);
          LMonth.setForeground(Color.red);

          setDataIntoVector(SStDate,SEnDate);
          setRowData();
          setTotalValue();

          try
          {
               getContentPane().remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.setPrefferedColumnWidth1(iColWidth);

               getContentPane().add(tabreport,BorderLayout.CENTER);
               DeskTop.repaint();
               DeskTop.updateUI();
               if(RowData.length>0)
               {
                    BPrint.setEnabled(true);
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex){}
     }

     private void setTotalValue()
     {
          double dTValue = common.toDouble(common.getSum(VValue,"S"));

          LNet.setText(common.getRound(dTValue,2));
          LNet.setForeground(Color.red);
     }

     public void setDataIntoVector(String SStDate,String SEnDate)
     {
          String SSupCode="";

          if(iMillCode==0)
          {
               SSupCode = "720725";
          }

          if(iMillCode==1)
          {
               SSupCode = "X1012";
          }

          String QS = " SELECT RDC.RDCNo, RDC.RDCDate, RDC.Descript, RDC.ValueRemarks, RDC.Qty, RDC.NetValue, Uom.UomName "+
                      " FROM RDC "+
                      " Inner Join UOM on RDC.UomCode=UOM.UomCode "+
                      " Where RDC.DocType=1 and RDC.ValueStatus=1 and RDC.MillCode = "+iMillCode+
                      " and RDC.Sup_Code='"+SSupCode+"' and RDC.RDCDate>='"+SStDate+"' and RDC.RDCDate<='"+SEnDate+"'"+
                      " Order by RDC.RDCDate,RDC.RdcNo,Rdc.Id ";

          VRDCNo   = new Vector();
          VRDCDate = new Vector();
          VDescript= new Vector();
          VRemarks = new Vector();
          VQty     = new Vector();
          VValue   = new Vector();
          VUom     = new Vector();

          try
          {
               Statement stat = theConnection.createStatement();
               ResultSet res  = stat.executeQuery(QS);

               while (res.next())
               {
                    VRDCNo   .addElement(common.parseNull(res.getString(1)));
                    VRDCDate .addElement(common.parseDate(res.getString(2)));
                    VDescript.addElement(common.parseNull(res.getString(3)));
                    VRemarks .addElement(common.parseNull(res.getString(4)));
                    VQty     .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(5))),3));
                    VValue   .addElement(common.parseNull(res.getString(6)));
                    VUom   .addElement(common.parseNull(res.getString(7)));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VRDCNo.size()][ColumnData.length];

          for(int i=0;i<VRDCNo.size();i++)
          {
               RowData[i][0]  = String.valueOf(i+1);
               RowData[i][1]  = (String)VRDCNo    .elementAt(i);
               RowData[i][2]  = (String)VRDCDate  .elementAt(i);
               RowData[i][3]  = (String)VDescript .elementAt(i);
               RowData[i][4]  = (String)VUom      .elementAt(i);
               RowData[i][5]  = (String)VQty      .elementAt(i);
               RowData[i][6]  = (String)VValue    .elementAt(i);
               RowData[i][7]  = (String)VRemarks  .elementAt(i);
          }
     }

     private void createPDF()

      {

             try 
                {
         document = new Document(PageSize.A4);
         PdfWriter. getInstance(document,new FileOutputStream(common.getPrintPath()+"NRDCValuationList.pdf"));
         document . open();

         Paragraph paragraph;
         table  = new PdfPTable(8);
         table  . setWidths(iWidth);
         table  . setWidthPercentage(100);

         double dTotvalue = 0; 

          if(iMillCode==0)
          {

              

               c1         = new PdfPCell(new Phrase("AMARJOTHI SPINNING MILLS LTD",mediumbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . hasMinimumHeight();
               c1         . setColspan(8);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.TOP);
               table      . addCell(c1);

               c1         = new PdfPCell(new Phrase("MATERIAL SENT FOR DYING UNIT FOR THE MONTH OF "+SMonthName1+" ",mediumbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . hasMinimumHeight();
               c1         . setColspan(8);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.BOTTOM);
               table      . addCell(c1);

          }

          if(iMillCode==1)
          {
               c1         = new PdfPCell(new Phrase("AMARJOTHI SPINNING MILLS LTD (DYEING DIVISION)",mediumbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . hasMinimumHeight();
               c1         . setColspan(8);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.TOP);
               table      . addCell(c1);

               c1         = new PdfPCell(new Phrase("MATERIAL SENT FOR MILL FOR THE MONTH OF "+SMonthName1+"",mediumbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . hasMinimumHeight();
               c1         . setColspan(8);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.BOTTOM);
               table      . addCell(c1);
          }


                AddCellIntoTable("Sl No",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("NRDC NO",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("DATE",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("MATERIAL NAME",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("UOM",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("QTY",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("VALUE",table,Element.ALIGN_CENTER,smallbold);
                AddCellIntoTable("REASON",table,Element.ALIGN_CENTER,smallbold);



            for(int i=0; i<VRDCNo.size();i++)

                {

                double dvalue = common.toDouble((String)VValue.elementAt(i));

                AddCellIntoTable(""+(i+1)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VRDCNo.elementAt(i)+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+common.parseDate((String)VRDCDate.elementAt(i))+"",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(""+VDescript.elementAt(i)+"",table,Element.ALIGN_LEFT,smallnormal);
                AddCellIntoTable(""+VUom.elementAt(i)+"",table,Element.ALIGN_LEFT,smallnormal);
                AddCellIntoTable(""+VQty.elementAt(i)+"",table,Element.ALIGN_RIGHT,smallnormal);
                AddCellIntoTable(""+VValue.elementAt(i)+"",table,Element.ALIGN_RIGHT,smallnormal);
                AddCellIntoTable(""+VRemarks.elementAt(i)+"",table,Element.ALIGN_LEFT,smallnormal);
               
                dTotvalue = dTotvalue + dvalue;


                }

                 
                AddCellIntoTable(" ",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable(" ",table,Element.ALIGN_RIGHT,smallnormal);
                AddCellIntoTable(" ",table,Element.ALIGN_CENTER,smallnormal);
                AddCellIntoTable("Grand Total",table,Element.ALIGN_LEFT,smallbold);
                AddCellIntoTable(" ",table,Element.ALIGN_LEFT,smallnormal);
                AddCellIntoTable(" ",table,Element.ALIGN_LEFT,smallnormal);
                AddCellIntoTable(""+common.getRound(dTotvalue,2)+"",table,Element.ALIGN_RIGHT,smallbold);
                AddCellIntoTable(" ",table,Element.ALIGN_LEFT,smallnormal);

               
               c1         = new PdfPCell(new Phrase(" ",smallbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . setFixedHeight(20f);
               c1         . setColspan(8);
               c1         . setBorder(Rectangle.LEFT|Rectangle.RIGHT);
               table      . addCell(c1);

               c1         = new PdfPCell(new Phrase("SK",smallbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . hasMinimumHeight();
               c1         . setColspan(4);
               c1         . setBorder(Rectangle.LEFT|Rectangle.BOTTOM);
               table      . addCell(c1);

               c1         = new PdfPCell(new Phrase("SO",smallbold));
               c1         . setHorizontalAlignment(Element.ALIGN_CENTER);
               c1         . hasMinimumHeight();
               c1         . setColspan(4);
               c1         . setBorder(Rectangle.BOTTOM|Rectangle.RIGHT);
               table      . addCell(c1);

 
         document  . add(table);
         document  . close();

               }


         catch(Exception e)

               {  e.printStackTrace(); }

       }

        private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,Font docFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,docFont));  
          c1.setHorizontalAlignment(iHorizontal);
          c1.hasMinimumHeight();
          c1.setBorder(Rectangle.LEFT|Rectangle.RIGHT|Rectangle.TOP|Rectangle.BOTTOM);
          table.addCell(c1);
      }



}
