package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

 
//pdf


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class OrderAbsFrame extends JInternalFrame
{
     String      SStDate,SEnDate;
     JComboBox   JCOrder,JCGSTType;
     MyTextField TFile;
     NextField   TNo;

     JButton   BApply,BPrint,BCancel,BCreatePDF;
     JPanel    TopPanel;
     JPanel    BottomPanel;
    
     TabReport tabreport;
     DateField TStDate;
     DateField TEnDate;
    
     Object RowData[][];
     String ColumnData[] = {"Mark to Print","Order No","Date","Supplier","Basic","Discount","CenVat","Tax","Surcharge","Plus","Minus","Net","Status"};
     String ColumnType[] = {"B","N","S","S","N","N","N","N","N","N","N","N","S"};
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;

     Vector VOrdNo,VOrdDate,VSupName,VBasic,VDisc,VCenVat,VTax,VSur,VAdd,VLess,VNet,VSupCode,VStatus,VJMDOrdAppStatus;

     String SFile="";

     JLayeredPane DeskTop;
     StatusPanel  SPanel;
     int          iMillCode;
     String       SSupTable;

     Vector VCode,VName;
     FileWriter FW;

    Document document;
    PdfPTable table;
    // String SPdfFile= "/root/RDCOrder.pdf";
    String SPdfFile = common.getPrintPath()+"/WorkOrder.pdf";

     OrderAbsFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode,String SSupTable)
     {
         super("Abstract on Orders Placed During a Period");
         this.DeskTop   = DeskTop;
         this.SPanel    = SPanel;
         this.iMillCode = iMillCode;
         this.SSupTable = SSupTable;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     public void createComponents()
     {
         TStDate     = new DateField();
         TEnDate     = new DateField();
         BApply      = new JButton("Apply");
         BPrint      = new JButton("Print Marked Orders");
         BCreatePDF  = new JButton(" Create PDF");
         BCancel     = new JButton("Cancel");
         TFile       = new MyTextField(20);
         TNo         = new NextField(5); 
         TopPanel    = new JPanel();
         BottomPanel = new JPanel();
         JCOrder     = new JComboBox();
          JCGSTType   = new JComboBox();  

         TStDate.setTodayDate();
         TEnDate.setTodayDate();
         TFile.setText("WorkOrder.prn");
         BApply.setMnemonic('A');
         BPrint.setMnemonic('P');
         BCancel.setMnemonic('C');
         BPrint.setEnabled(false);
         TFile.setEditable(false);
     }

     public void setLayouts()
     {
        TopPanel.setLayout(new GridLayout(3,8));
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,790,500);
     }

     public void addComponents()
     {
          
         JCOrder.addItem("Order No");
         JCOrder.addItem("Supplierwise");

         JCGSTType  . addItem(" NEW (GST) ");
         JCGSTType  . addItem(" OLD (Non - GST)");  


         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel("TYPE"));
         TopPanel.add(JCGSTType);
         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(""));


         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(" "));
         TopPanel.add(new JLabel(" "));
         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(""));
         TopPanel.add(new JLabel(""));



         TopPanel.add(new JLabel(""));
         TopPanel.add(TNo);
         TopPanel.add(new JLabel("Sorted On"));
         TopPanel.add(JCOrder);
         TopPanel.add(TStDate);
         TopPanel.add(TEnDate);
         TopPanel.add(BApply);
         TopPanel.add(new JLabel(""));

         BottomPanel.add(TFile);
         BottomPanel.add(BCreatePDF); 
         BottomPanel.add(BPrint);
         BottomPanel.add(BCancel);

         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
         BApply.addActionListener(new ApplyList());
         BPrint.addActionListener(new PrintList());
         BCancel.addActionListener(new CancelList());
        BCreatePDF.addActionListener(new  PdfList());
     }
     public class CancelList implements ActionListener
     {
         public void actionPerformed(ActionEvent ae)
         {
               removeHelpFrame();
         }
     }
     public void removeHelpFrame()
     {
         try
         {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
         }
         catch(Exception ex)
         {
          System.out.println(ex);
          ex.printStackTrace();
         }

     }
     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(checkJMDAppPending())
               {
                    try
                    {
                         SFile = TFile.getText();
                         if((SFile.trim()).length()==0)
                              SFile = "WorkOrder.prn";
     
                         FW = new FileWriter(common.getPrintPath()+SFile);
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
                    for(int i=0;i<RowData.length;i++)
                    {
                         Boolean Bselected = (Boolean)RowData[i][0];
                         if(Bselected.booleanValue())
                         {
                              String SOrdNo     = (String)VOrdNo.elementAt(i);
                              String SOrdDate   = (String)VOrdDate.elementAt(i);
                              String SSupCode   = (String)VSupCode.elementAt(i);
                              String SSupName   = (String)VSupName.elementAt(i);
     
                              if(iMillCode==0)
                              {
                                   try
                                   {
                                        new WorkOrderPrint(FW,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
                                   }
                                   catch(Exception ex)
                                   {
                                        System.out.println("From OrderAbs "+ex);
                                        ex.printStackTrace();
                                   }
                              }
                              if(iMillCode==1)
                              {
                                   try
                                   {
                                        new DyeingWorkOrderPrint(FW,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
                                   }
                                   catch(Exception ex)
                                   {
                                        System.out.println("From OrderAbs "+ex);
                                        ex.printStackTrace();
                                   }
                              }
                              if(iMillCode==3)
                              {
                                   try
                                   {
                                        new MelangeWorkOrderPrint(FW,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
                                   }
                                   catch(Exception ex)
                                   {
                                        System.out.println("From OrderAbs "+ex);
                                        ex.printStackTrace();
                                   }
                              }
                              if(iMillCode==4)
                              {
                                   try
                                   {
                                        new KamadhenuWorkOrderPrint(FW,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
                                   }
                                   catch(Exception ex)
                                   {
                                        System.out.println("From OrderAbs "+ex);
                                        ex.printStackTrace();
                                   }
                              }
     
                         }
                    }
                    try
                    {
                         FW.close();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
     
                    }
                    showFileView(common.getPrintPath()+SFile);
               }
          }
     }

     public void showFileView(String SFileName)
     {
          JInternalFrame jf = new JInternalFrame(SFileName);
          Notepad        np = new Notepad(new File(SFileName),SPanel);
          jf.getContentPane().add("Center",np);
          jf.show();
          jf.setBounds(80,100,550,350);
          jf.setClosable(true);
          jf.setMaximizable(true);
          try
          {
               DeskTop.add(jf);
               jf.moveToFront();
               jf.setSelected(true);
               DeskTop.repaint();
          }
          catch(java.beans.PropertyVetoException ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    //getContentPane().remove(tabreport);
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
               try
               {
                        tabreport = new TabReport(RowData,ColumnData,ColumnType);
                        getContentPane().add(tabreport,BorderLayout.CENTER);
                        setSelected(true);
                        DeskTop.repaint();
                        DeskTop.updateUI();
                        BPrint.setEnabled(true);
                        TFile.setEditable(true);
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
          }
     }
     public void setDataIntoVector()
     {
           VOrdDate     = new Vector();
           VOrdNo       = new Vector();
           VSupName     = new Vector();
           VBasic       = new Vector();
           VDisc        = new Vector();
           VCenVat      = new Vector();
           VTax         = new Vector();
           VSur         = new Vector();
           VAdd         = new Vector();
           VLess        = new Vector();
           VNet         = new Vector();
           VSupCode     = new Vector();
           VStatus       = new Vector();
           VJMDOrdAppStatus = new Vector();

           SStDate = TStDate.TDay.getText()+"."+TStDate.TMonth.getText()+"."+TStDate.TYear.getText();
           SEnDate = TEnDate.TDay.getText()+"."+TEnDate.TMonth.getText()+"."+TEnDate.TYear.getText();
           String StDate  = TStDate.TYear.getText()+TStDate.TMonth.getText()+TStDate.TDay.getText();
           String EnDate  = TEnDate.TYear.getText()+TEnDate.TMonth.getText()+TEnDate.TDay.getText();

           try
           {
              if(theconnect==null)
              {
                   connect=ORAConnection.getORAConnection();
                   theconnect=connect.getConnection();
              }
              Statement stat  = theconnect.createStatement();
              String QString = getQString(StDate,EnDate);
              ResultSet res  = stat.executeQuery(QString);
              while (res.next())
              {
                       String str1  = res.getString(1);  
                       String str2  = res.getString(2);
                       String str3  = res.getString(3);
                       String str4  = res.getString(4);
                       String str5  = res.getString(5);
                       String str6  = res.getString(6);
                       String str7  = res.getString(7);
                       String str8  = res.getString(8);
                       String str9  = res.getString(9);
                       String str10 = res.getString(10);
                       String str11 = res.getString(11);
                       String str12 = res.getString(12);

                         int iSoStatus   = common.toInt(res.getString(13));
                         int iIaStatus   = common.toInt(res.getString(14));
                         int iJmdStatus  = common.toInt(res.getString(15));

                         String SStatus = getStatus(iJmdStatus,iIaStatus,iSoStatus);

                       VOrdNo.addElement(str1);
                       VOrdDate.addElement(common.parseDate(str2));
                       VSupName.addElement(str3);
                       VBasic.addElement(str4);
                       VDisc.addElement(str5);
                       VCenVat.addElement(str6);
                       VTax.addElement(str7);
                       VSur.addElement(str8);
                       VNet.addElement(str9);
                       VSupCode.addElement(str10);
                       VAdd.addElement(str11);
                       VLess.addElement(str12);
                       VStatus.addElement(SStatus);
                       VJMDOrdAppStatus.addElement(String.valueOf(iJmdStatus));
              }
              res.close();
              stat.close();
           }
           catch(Exception ex)
           {
               System.out.println(ex);
               ex.printStackTrace();
           }
     }
     public void setRowData()
     {
         RowData     = new Object[VOrdDate.size()][13];
         for(int i=0;i<VOrdDate.size();i++)
         {
               RowData[i][0] =  new Boolean(false);
               RowData[i][1]  = (String)VOrdNo.elementAt(i);
               RowData[i][2]  = (String)VOrdDate.elementAt(i);
               RowData[i][3]  = (String)VSupName.elementAt(i);
               RowData[i][4] =  (String)VBasic.elementAt(i);
               RowData[i][5] =  (String)VDisc.elementAt(i);
               RowData[i][6] =  (String)VCenVat.elementAt(i);
               RowData[i][7] =  (String)VTax.elementAt(i);
               RowData[i][8] =  (String)VSur.elementAt(i);
               RowData[i][9] =  (String)VAdd.elementAt(i);
               RowData[i][10] =  (String)VLess.elementAt(i);
               double dRNet = common.toDouble((String)VNet.elementAt(i))+common.toDouble((String)VAdd.elementAt(i))-common.toDouble((String)VLess.elementAt(i));
               RowData[i][11] = ""+ dRNet;
               RowData[i][12] =  (String)VStatus.elementAt(i);
        }  
     }
     public String getQString(String StDate,String EnDate)
     {
          int iNo = common.toInt(TNo.getText());

          DateField df = new DateField();
          df.setTodayDate();
          String SToday = df.TYear.getText()+df.TMonth.getText()+df.TDay.getText();
          String QString  = "";
     
          QString = " SELECT WorkOrder.OrderNo, WorkOrder.OrderDate, "+
                    " "+SSupTable+".Name, Sum(WorkOrder.qty*rate) AS Expr1, "+
                    " Sum(WorkOrder.Disc) as sumOfDisc, Sum(WorkOrder.Cenvat) as sumOfCenVat, "+
                    " Sum(WorkOrder.Tax) as sumOfTax, Sum(WorkOrder.Sur) as sumOfSur, "+
                    " (Sum(WorkOrder.qty*rate)+Sum(WorkOrder.Disc)+Sum(WorkOrder.Cenvat)+Sum(WorkOrder.Tax)+Sum(WorkOrder.Sur)), "+
                    " WorkOrder.Sup_Code,WorkOrder.Plus,WorkOrder.Less, "+
                    " WorkOrder.SOOrderApproval, WorkOrder.IAOrderApproval, WorkOrder.JMDOrderApproval "+
                    " FROM WorkOrder "+
                    " INNER JOIN "+SSupTable+" ON WorkOrder.Sup_Code="+SSupTable+".Ac_Code "+
                    " and WorkOrder.MillCode="+iMillCode;

             
            if(JCGSTType.getSelectedIndex() == 0)
                  {
                        QString = QString+" And WorkOrder.GSTSTATUS= 0";
                  }
                 else
                  {
                        QString = QString+" And WorkOrder.GSTSTATUS= 1";
                  }
  
                   QString = QString+"  GROUP BY WorkOrder.OrderNo,WorkOrder.OrderDate, "+SSupTable+".Name,WorkOrder.Sup_Code,WorkOrder.Plus,WorkOrder.Less, WorkOrder.SOOrderApproval, WorkOrder.IAOrderApproval, WorkOrder.JMDOrderApproval ";

          if(iNo > 0)
               QString = QString+" Having WorkOrder.OrderNo = "+iNo;
          else
               QString = QString+" Having WorkOrder.OrderDate >= '"+StDate+"' and WorkOrder.OrderDate <='"+EnDate+"'";


          if(JCOrder.getSelectedIndex() == 0)      
               QString = QString+" Order By WorkOrder.OrderNo,WorkOrder.OrderDate";
          if(JCOrder.getSelectedIndex() == 1)      
               QString = QString+" Order By "+SSupTable+".Name,WorkOrder.OrderDate";
System.out.println("QS===>"+QString);
          return QString;
     }

 //pdf List

   //pdf

   public class PdfList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(checkJMDAppPending())
               {
                    try
                    {
                         document = new Document(PageSize.A4);
                       document.setMargins(30,10,20,0); 
                   PdfWriter.getInstance(document, new FileOutputStream(SPdfFile));
                      document.open();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
                    for(int i=0;i<RowData.length;i++)
                    {
                         Boolean Bselected = (Boolean)RowData[i][0];
                         if(Bselected.booleanValue())
                         {
                              String SOrdNo     = (String)VOrdNo.elementAt(i);
                              String SOrdDate   = (String)VOrdDate.elementAt(i);
                              String SSupCode   = (String)VSupCode.elementAt(i);
                              String SSupName   = (String)VSupName.elementAt(i);
     
                              if(iMillCode==0)
                              {
                                   try
                                   {
                                       
                                       if(JCGSTType.getSelectedIndex() == 0){
                                        new WorkOrderGSTPdf(document,SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
                                        } else if(JCGSTType.getSelectedIndex() == 1){
                                        new WorkOrderPdfFile(document,SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
                                        }
                                   }
                                   catch(Exception ex)
                                   {
                                        System.out.println("From OrderAbs "+ex);
                                        ex.printStackTrace();
                                   }
                              }
                              if(iMillCode==1)
                              {
                                   try
                                   {
                                       
                                          if(JCGSTType.getSelectedIndex() == 0){
                                         new DyeingWorkOrderGSTPdf(document,SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable); 
                                         } else if(JCGSTType.getSelectedIndex() == 1){
                                         new DyeingWorkOrderPdfFile(document,SPdfFile,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
                                         } 
     
          
                                   }
                                   catch(Exception ex)
                                   {
                                        System.out.println("From OrderAbs "+ex);
                                        ex.printStackTrace();
                                   }
                              }
                             /* if(iMillCode==3)
                              {
                                   try
                                   {
                                        new MelangeWorkOrderPrint(document,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
                                   }
                                   catch(Exception ex)
                                   {
                                        System.out.println("From OrderAbs "+ex);
                                        ex.printStackTrace();
                                   }
                              }
                              if(iMillCode==4)
                              {
                                   try
                                   {
                                        new KamadhenuWorkOrderPrint(document,SOrdNo,SOrdDate,SSupCode,SSupName,iMillCode,SSupTable);
                                   }
                                   catch(Exception ex)
                                   {
                                        System.out.println("From OrderAbs "+ex);
                                        ex.printStackTrace();
                                   }
                              }*/
     
                         }
                    }
                    try
                    {
                         document.close();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
     
                    }
           
                            try{
                     File theFile   = new File(SPdfFile);
                     Desktop        . getDesktop() . open(theFile);
                       
     
                       
                    }catch(Exception ex){}
                   // showFileView(common.getPrintPath()+SFile);
               }
          }
     }

     private String getStatus(int iJmdStatus,int iIaStatus,int iSoStatus)
     {
        String SStatus="";

        if(iJmdStatus==0 && iIaStatus==0 && iSoStatus==0)
            SStatus =" Pending With S.O";

        if(iJmdStatus==0 && iIaStatus==0 && iSoStatus==1)
            SStatus =" Pending With IA";

        if(iJmdStatus==0 && iIaStatus==1 && iSoStatus==1)
            SStatus =" Pending With JMD";

        return SStatus;
     }

     private boolean checkJMDAppPending()
     {
          String SErrorMsg    = "";

          for(int i=0;i<RowData.length;i++)
          {
               Boolean BValue = (Boolean)RowData[i][0];

               if(BValue.booleanValue())
               {
                    String SOrderNo     = common.parseNull((String)VOrdNo.elementAt(i));
                    int iJMDAppNo       = common.toInt((String)VJMDOrdAppStatus.elementAt(i));

                    if(iJMDAppNo == 0)
                    {
                         SErrorMsg     += SOrderNo+", ";
                    }
               }
          }

          if(SErrorMsg.trim().length() > 0)
          {
               SErrorMsg = SErrorMsg.substring(0, SErrorMsg.length() - 2);

               SErrorMsg = "JMD Approval is Pending. So printing not allowed for the following order numbers\n\n"+SErrorMsg;

               JOptionPane.showMessageDialog(null, SErrorMsg, "Info", JOptionPane.ERROR_MESSAGE);
               return false;
          }

          return true;
     }
}
