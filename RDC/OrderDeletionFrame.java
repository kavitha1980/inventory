package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class OrderDeletionFrame extends JInternalFrame
{
          
     JPanel    TopPanel;
     JPanel    BottomPanel;

     JButton   BApply,BDelete;
     NextField TOrderNo;
    
     Object RowData[][];
     String ColumnData[] = {"Order No","Date","Supplier","Description","Dept","Group","Unit","Qty","Rate","Amount","Due Date"};
     String ColumnType[] = {"N","S","S","S","S","S","S","N","N","N","S"};

     JLayeredPane DeskTop;
     StatusPanel  SPanel;
     int          iUserCode;
     int          iMillCode;
     String       SSupTable;

     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;

     Vector VOrdDate,VOrdNo,VSupName,VOrdName,VOrdDeptName,VOrdCataName,VOrdUnitName,VOrdQty,VOrdRate,VOrdNet,VOrdDue,VId;

     TabReport tabreport;
     
     OrderDeletionFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable)
     {
          super("Work Order Deletion");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     public void createComponents()
     {
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          TOrderNo = new NextField();
          BApply   = new JButton("Apply");
          BDelete  = new JButton("Delete Work Order");

          BApply.setMnemonic('A');
          BDelete.setEnabled(false);
     }

     public void setLayouts()
     {    
          TopPanel.setLayout(new GridLayout(1,3));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("Work Order No"));
          TopPanel.add(TOrderNo);
          TopPanel.add(BApply);

          BottomPanel.add(BDelete);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
         BApply.addActionListener(new ApplyList());
         BDelete.addActionListener(new DeleteList());
     }
     public class DeleteList implements ActionListener
     {
         public void actionPerformed(ActionEvent ae)
         {
               BDelete.setEnabled(false);
               deleteRecords();
               removeHelpFrame();
         }
     }
     private void deleteRecords()
     {
          String QS="";

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               for(int i=0;i<RowData.length;i++)
               {
                    QS = "Update WorkOrder Set DM=9 Where Id="+(String)VId.elementAt(i);

                    try
                    {
                         stat.execute(QS);
                    }
                    catch(Exception ex)
                    {
                         JOptionPane.showMessageDialog(null,"Problem in Deletion","Attention",JOptionPane.INFORMATION_MESSAGE);
                         return;
                    }
               }
               try
               {
                    QS = "Insert Into DeletedWorkOrder Select WorkOrder.* From WorkOrder Where WorkOrder.DM=9";
                    stat.execute(QS);
               }
               catch(Exception ex)
               {
                         JOptionPane.showMessageDialog(null,"Problem in Deletion","Attention",JOptionPane.INFORMATION_MESSAGE);
                         ex.printStackTrace();
                         return;
               }
               try
               {
                    QS = "Delete From WorkOrder Where DM=9";
                    stat.execute(QS);
               }
               catch(Exception ex)
               {
                         JOptionPane.showMessageDialog(null,"Problem in Deletion","Attention",JOptionPane.INFORMATION_MESSAGE);
                         ex.printStackTrace();
                         return;
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void removeHelpFrame()
     {
         try
         {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
         }
         catch(Exception ex){}

     }
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setTabReport();
          }
     }
     public void setTabReport()
     {
          if(common.toInt(TOrderNo.getText())==0)
          {
               JOptionPane.showMessageDialog(null,"Invalid Work Order No","Error Message",JOptionPane.INFORMATION_MESSAGE);
               TOrderNo.requestFocus();
               BDelete.setEnabled(false);
               return;
          }

          setDataIntoVector();
          setRowData();
          try
          {
             getContentPane().remove(tabreport);
          }
          catch(Exception ex){}

          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             getContentPane().add(tabreport,BorderLayout.CENTER);
             setSelected(true);
             DeskTop.repaint();
             DeskTop.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }
          if(RowData.length>0)
          {
               BDelete.setEnabled(true);
          }
          else
          {
               BDelete.setEnabled(false);
          }
     }
     public void setDataIntoVector()
     {
           VOrdDate     = new Vector();
           VOrdNo       = new Vector();
           VSupName     = new Vector();
           VOrdName     = new Vector();
           VOrdDeptName = new Vector();
           VOrdCataName = new Vector();
           VOrdUnitName = new Vector();
           VOrdQty      = new Vector();
           VOrdRate     = new Vector();
           VOrdNet      = new Vector();
           VOrdDue      = new Vector();
           VId          = new Vector();

           String QString = "";

           try
           {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
              QString = getQString();

              ResultSet res  = stat.executeQuery(QString);
              while (res.next())
              {
                 double dInvQty = common.toDouble(res.getString(13));
                 if(dInvQty>0)
                 {
                    JOptionPane.showMessageDialog(null,"Grn Made Against this Order","Error Message",JOptionPane.INFORMATION_MESSAGE);
                    TOrderNo.setText("");
                    TOrderNo.requestFocus();
                    return;
                 }

                 VOrdNo.addElement(res.getString(1));
                 VOrdDate.addElement(common.parseDate(res.getString(2)));
                 VSupName.addElement(res.getString(3));
                 VOrdName.addElement(res.getString(4));
                 VOrdDeptName.addElement(common.parseNull(res.getString(5)));
                 VOrdCataName.addElement(common.parseNull(res.getString(6)));
                 VOrdUnitName.addElement(common.parseNull(res.getString(7)));
                 VOrdQty.addElement(res.getString(8));
                 VOrdRate.addElement(res.getString(9));;
                 VOrdNet.addElement(res.getString(10));
                 VOrdDue.addElement(common.parseDate(res.getString(11)));
                 VId.addElement(res.getString(12));
              }
              res.close();
              stat.close();
           }
           catch(Exception ex){System.out.println(ex);}
     }
     public void setRowData()
     {
         RowData     = new Object[VOrdDate.size()][ColumnData.length];
         for(int i=0;i<VOrdDate.size();i++)
         {
               RowData[i][0]  = (String)VOrdNo.elementAt(i);
               RowData[i][1]  = (String)VOrdDate.elementAt(i);
               RowData[i][2]  = (String)VSupName.elementAt(i);
               RowData[i][3]  = (String)VOrdName.elementAt(i);
               RowData[i][4]  = (String)VOrdDeptName.elementAt(i);
               RowData[i][5]  = (String)VOrdCataName.elementAt(i);
               RowData[i][6]  = (String)VOrdUnitName.elementAt(i);
               RowData[i][7]  = (String)VOrdQty.elementAt(i);
               RowData[i][8]  = (String)VOrdRate.elementAt(i);
               RowData[i][9]  = (String)VOrdNet.elementAt(i);
               RowData[i][10] = (String)VOrdDue.elementAt(i);
        }  
     }
     public String getQString()
     {
          String SOrderNo = TOrderNo.getText();

          String QString  = " SELECT WorkOrder.OrderNo, WorkOrder.OrderDate,"+
                            " "+SSupTable+".Name, WorkOrder.Descript,"+
                            " Dept.Dept_Name, Cata.Group_Name, Unit.Unit_Name,"+
                            " WorkOrder.Qty, WorkOrder.Rate, WorkOrder.Net,"+
                            " WorkOrder.DueDate, WorkOrder.Id, WorkOrder.InvQty "+
                            " FROM (((WorkOrder INNER JOIN "+SSupTable+" ON WorkOrder.Sup_Code="+SSupTable+".Ac_Code) "+
                            " INNER JOIN Dept ON WorkOrder.Dept_Code=Dept.Dept_Code) "+
                            " INNER JOIN Cata ON WorkOrder.Group_Code=Cata.Group_Code) "+
                            " INNER JOIN Unit ON WorkOrder.Unit_Code=Unit.Unit_Code "+
                            " Where WorkOrder.MillCode="+iMillCode+" And WorkOrder.Qty > 0 And WorkOrder.OrderNo="+SOrderNo;

          return QString;
     }
}
