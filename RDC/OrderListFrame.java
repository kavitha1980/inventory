package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class OrderListFrame extends JInternalFrame
{
          
     JPanel    TopPanel;
     JPanel    BottomPanel;

     String    SStDate,SEnDate;
     JComboBox JCOrder,JCFilter;
     JButton   BApply,BExit;
    
     DateField TStDate;
     DateField TEnDate;
    
     Object RowData[][];
     String ColumnData[] = {"Order No","Date","Supplier","Description","Dept","Group","Unit","Qty","Rate","Amount","Due Date","Status"};
     String ColumnType[] = {"N","S","S","S","S","S","S","N","N","N","S","S"};

     JLayeredPane DeskTop;
     StatusPanel  SPanel;
     int          iUserCode;
     int          iMillCode;
     int          iAuthCode;
     String       SSupTable;

     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;

     Vector VOrdDate,VOrdNo,VSupName,VOrdName,VOrdDeptName,VOrdCataName,VOrdUnitName,VOrdQty,VOrdRate,VOrdNet,VOrdDue,VStatus;

     Vector VSelectedName,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup;

     TabReport tabreport;
     
     OrderListFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,int iAuthCode,String SSupTable)
     {
          super("Work Orders Placed During a Period");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.iAuthCode = iAuthCode;
          this.SSupTable = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     public void createComponents()
     {
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          TStDate  = new DateField();
          TEnDate  = new DateField();
          BApply   = new JButton("Apply");
          BExit    = new JButton("Exit");
          JCOrder  = new JComboBox();
          JCFilter = new JComboBox();

          TStDate.setTodayDate();
          TEnDate.setTodayDate();
          BApply.setMnemonic('A');
          BExit.setMnemonic('E');
     }

     public void setLayouts()
     {    
          TopPanel.setLayout(new GridLayout(1,8));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
     }

     public void addComponents()
     {
          JCOrder.addItem("Order No");
          JCOrder.addItem("Materialwise");
          JCOrder.addItem("Supplierwise");
          JCOrder.addItem("Departmentwise");
          JCOrder.addItem("Groupwise");

          JCFilter.addItem("All");
          JCFilter.addItem("Over-Due");
          JCFilter.addItem("All Pendings");
          JCFilter.addItem("Before Over-Due");
          
          TopPanel.add(new JLabel("Sorted On"));
          TopPanel.add(JCOrder);

          TopPanel.add(new JLabel("List Only"));
          TopPanel.add(JCFilter);

          TopPanel.add(new JLabel("Period"));
          TopPanel.add(TStDate);
          TopPanel.add(TEnDate);
          TopPanel.add(BApply);

          BottomPanel.add(BExit);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
         BApply.addActionListener(new ApplyList(this));
         BExit.addActionListener(new ExitList());
     }
     public class ExitList implements ActionListener
     {
         public void actionPerformed(ActionEvent ae)
         {
               removeHelpFrame();
         }

     }
     public void removeHelpFrame()
     {
         try
         {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
         }
         catch(Exception ex)
         {
                System.out.println(ex);
                ex.printStackTrace();
         }

     }
     public class ApplyList implements ActionListener
     {
          OrderListFrame orderlistframe;

          public ApplyList(OrderListFrame orderlistframe)
          {
               this.orderlistframe = orderlistframe;
          }
          public void actionPerformed(ActionEvent ae)
          {
               setTabReport(orderlistframe);
          }
     }
     public void setTabReport(OrderListFrame orderlistframe)
     {
          setDataIntoVector();
          setRowData();
          try
          {
             getContentPane().remove(tabreport);
          }
          catch(Exception ex)
          {
          }
          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             getContentPane().add(tabreport,BorderLayout.CENTER);

             if(iAuthCode>1)
             {
                   tabreport.ReportTable.addKeyListener(new KeyList(orderlistframe));
             }

             setSelected(true);
             DeskTop.repaint();
             DeskTop.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println("Come in exc--:"+ex);
             ex.printStackTrace();
          }

     }
     public class KeyList extends KeyAdapter
     {
           OrderListFrame orderlistframe;

           public KeyList(OrderListFrame orderlistframe)
           {
                this.orderlistframe = orderlistframe;
           }
           public void keyPressed(KeyEvent ke)
           {
                if (ke.getKeyCode()==10)
                {
                     try
                     {
                        int i = tabreport.ReportTable.getSelectedRow();
                        String SOrderNo = (String)RowData[i][0];
                        DirectOrderFrame directorderframe = new DirectOrderFrame(DeskTop,SPanel,true,iUserCode,iMillCode,orderlistframe,SSupTable);
                        directorderframe.fillData(SOrderNo);
                        DeskTop.add(directorderframe);
                        directorderframe.show();
                        directorderframe.moveToFront();
                        DeskTop.repaint();
                        DeskTop.updateUI();
                     }
                     catch(Exception ex)
                     {
                        System.out.println(ex);
                          ex.printStackTrace();
                     }
                }
           } 
     }
     public void setDataIntoVector()
     {
           VOrdDate     = new Vector();
           VOrdNo       = new Vector();
           VSupName     = new Vector();
           VOrdName     = new Vector();
           VOrdDeptName = new Vector();
           VOrdCataName = new Vector();
           VOrdUnitName = new Vector();
           VOrdQty      = new Vector();
           VOrdRate     = new Vector();
           VOrdNet      = new Vector();
           VOrdDue      = new Vector();
           VStatus      = new Vector();

           SStDate = TStDate.toString();
           SEnDate = TEnDate.toString();
           String StDate  = TStDate.toNormal();
           String EnDate  = TEnDate.toNormal();

           String QString  = " Create table Temp1 as SELECT WorkOrder.OrderNo, WorkOrder.OrderDate, "+
                             " "+SSupTable+".Name, WorkOrder.Descript, WorkOrder.Qty, "+
                             " WorkOrder.Rate, WorkOrder.Net, WorkOrder.DueDate, "+
                             " WorkOrder.InvQty, Dept.Dept_Name, Cata.Group_Name, "+
                             " Unit.Unit_Name, WorkOrder.SlNo, WorkOrder.MillCode "+
                             " FROM (((WorkOrder INNER JOIN "+SSupTable+" ON WorkOrder.Sup_Code="+SSupTable+".Ac_Code) "+
                             " INNER JOIN Dept ON WorkOrder.Dept_Code=Dept.Dept_Code) "+
                             " INNER JOIN Cata ON WorkOrder.Group_Code=Cata.Group_Code) "+
                             " INNER JOIN Unit ON WorkOrder.Unit_Code=Unit.Unit_Code "+
                             " Where WorkOrder.EntryStatus=0 and WorkOrder.Qty > 0 and WorkOrder.MillCode="+iMillCode;
           try
           {
              if(theconnect==null)
              {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
              }
              Statement stat  = theconnect.createStatement();
              try
              {
                  stat.execute("Drop Table Temp1");
              }
              catch(Exception ex)
              {
                ex.printStackTrace();
              }

              stat.execute(QString);

              QString = getQString(StDate,EnDate);
              ResultSet res  = stat.executeQuery(QString);
              while (res.next())
              {
                 VOrdNo.addElement(res.getString(1));
                 VOrdDate.addElement(common.parseDate(res.getString(2)));
                 VSupName.addElement(res.getString(3));
                 VOrdName.addElement(res.getString(4));
                 VOrdDeptName.addElement(common.parseNull(res.getString(5)));
                 VOrdCataName.addElement(common.parseNull(res.getString(6)));
                 VOrdUnitName.addElement(common.parseNull(res.getString(7)));
                 VOrdQty.addElement(res.getString(8));
                 VOrdRate.addElement(res.getString(9));
                 VOrdNet.addElement(res.getString(10));
                 VOrdDue.addElement(common.parseDate(res.getString(11)));
                 VStatus.addElement(" ");
              }
              res.close();
              stat.close();
           }
           catch(Exception ex)
           {
                System.out.println(ex);
                ex.printStackTrace();
           }
     }
     public void setRowData()
     {
         RowData     = new Object[VOrdDate.size()][ColumnData.length];
         for(int i=0;i<VOrdDate.size();i++)
         {
               RowData[i][0]  = (String)VOrdNo.elementAt(i);
               RowData[i][1]  = (String)VOrdDate.elementAt(i);
               RowData[i][2]  = (String)VSupName.elementAt(i);
               RowData[i][3]  = (String)VOrdName.elementAt(i);
               RowData[i][4]  = (String)VOrdDeptName.elementAt(i);
               RowData[i][5]  = (String)VOrdCataName.elementAt(i);
               RowData[i][6]  = (String)VOrdUnitName.elementAt(i);
               RowData[i][7]  = (String)VOrdQty.elementAt(i);
               RowData[i][8]  = (String)VOrdRate.elementAt(i);
               RowData[i][9]  = (String)VOrdNet.elementAt(i);
               RowData[i][10] = (String)VOrdDue.elementAt(i);
               RowData[i][11] = (String)VStatus.elementAt(i);
        }  
     }
     public String getQString(String StDate,String EnDate)
     {
          DateField df = new DateField();
          df.setTodayDate();
          String SToday = df.toNormal();
          String QString  = "";

          QString  = " SELECT temp1.OrderNo, temp1.OrderDate, "+
                     " temp1.Name, temp1.Descript, "+
                     " temp1.Dept_Name, temp1.Group_Name, "+
                     " temp1.Unit_Name, temp1.Qty, temp1.Rate, "+
                     " temp1.Net, temp1.DueDate "+
                     " FROM temp1 "+
                     " Where temp1.MillCode="+iMillCode+
                     " And temp1.OrderDate >= '"+StDate+"' and temp1.OrderDate <='"+EnDate+"'";

          if(JCFilter.getSelectedIndex()==1)
               QString = QString+" and temp1.InvQty = 0 and temp1.DueDate <= '"+SToday+"'";

          if(JCFilter.getSelectedIndex()==2)
               QString = QString+" and temp1.InvQty = 0";

          if(JCFilter.getSelectedIndex()==3)
               QString = QString+" and temp1.InvQty > 0 and temp1.DueDate >= '"+SToday+"'";

          if(JCOrder.getSelectedIndex() == 0)      
               QString = QString+" Order By temp1.OrderNo,temp1.OrderDate";
          if(JCOrder.getSelectedIndex() == 1)      
               QString = QString+" Order By temp1.Name,temp1.OrderDate";
          if(JCOrder.getSelectedIndex() == 2)      
               QString = QString+" Order By temp1.Name,temp1.OrderDate";
          if(JCOrder.getSelectedIndex() == 3)      
               QString = QString+" Order By temp1.Dept_Name,temp1.OrderDate";
          if(JCOrder.getSelectedIndex() == 4)
               QString = QString+" Order By temp1.Group_Name,temp1.OrderDate";

          return QString;
     }
}
