package RDC;

import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class OrderPdfClass
{

   
     Vector VName,VQty,VRate;
     Vector VDiscPer,VCenVatPer,VTaxPer,VSurPer;
     Vector VBasic,VDisc,VCenVat,VTax,VSur,VNet,VOthers,VWODesc;
     Vector VDesc;

     String SPayTerm = "";
     String SDueDate = "";
     String SOthers  = "";
     String SReference = "";
     String SRemarks   = "";
     double dOthers  = 0;
     String SAddr1="",SAddr2="",SAddr3="";
     String SEMail="",SFaxNo="";
     double dTDisc=0,dTCenVat=0,dTTax=0,dTSur=0,dTNet=0;
     double dSumOthers = 0;

     FileWriter FW;
     String SOrdNo,SOrdDate,SSupCode,SSupName;
     int iMillCode;
     String SSupTable;

     String SToName="",SThroName="";
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;

     int Lctr      = 100;
     int Pctr      = 0;


     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

    
    private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
     private static Font mediumbold  = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);
     private static Font smallnormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);  

     private static Font tinyBold   = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
     private static Font tinyNormal = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

     	
     String SHead[]  = {"Description of Materials Serviced","UOM","Quantity","Rate","Discount","Cenvat","Tax","SurCharg ","Net"};
     String SHead2[] = {"and Nature of Work Performed ","","","RS","Rs","Rs","Rs","Rs","Rs","Rs"};
     int    iWidth[] = {10,10,3,6,9,8,7,8,8,8};

     Document document;
     PdfPTable table,table1;
     String SFile="";

     OrderPdfClass(Document document,PdfPTable table,String SFile,String SOrdNo,String SOrdDate,String SSupCode,String SSupName,int iMillCode,String SSupTable)
     {
          this.document   = document;
          this.table      = table;
          this.SFile      = SFile;
          this.SOrdNo     = SOrdNo;
          this.SOrdDate   = SOrdDate;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.SSupTable = SSupTable;
          this.iMillCode  = iMillCode;

          getAddr();
          setDataIntoVector();
          createPdfFile();
     }
     
     public void setDataIntoVector()
     {
          VName      = new Vector();
          VDesc      = new Vector();
          VQty       = new Vector();
          VRate      = new Vector();
          VDiscPer   = new Vector();
          VCenVatPer = new Vector();
          VTaxPer    = new Vector();
          VSurPer    = new Vector();
          VBasic     = new Vector();
          VDisc      = new Vector();
          VCenVat    = new Vector();
          VTax       = new Vector();
          VSur       = new Vector();
          VNet       = new Vector();
          VOthers    = new Vector();
          VWODesc    = new Vector();

           try
           {
              if(theconnect==null)
              {
                   connect=ORAConnection.getORAConnection();
                   theconnect=connect.getConnection();
              }
              Statement stat  = theconnect.createStatement();
              String QString = getQString();
              ResultSet res  = stat.executeQuery(QString);
              while (res.next())
              {
                 VName.addElement(res.getString(1));
                 VQty.addElement(res.getString(2));
                 VRate.addElement(res.getString(3));
                 VDiscPer.addElement(res.getString(4));
                 VDisc.addElement(res.getString(5));
                 VCenVatPer.addElement(res.getString(6));
                 VCenVat.addElement(res.getString(7));
                 VTaxPer.addElement(res.getString(8));
                 VTax.addElement(res.getString(9));
                 VSurPer.addElement(res.getString(10));
                 VSur.addElement(res.getString(11));
                 VNet.addElement(res.getString(12));

                 VOthers.addElement(res.getString(13));

                 SDueDate = common.parseDate(res.getString(14));
                 SToName  = common.parseNull(res.getString(15));
                 SThroName= common.parseNull(res.getString(16));
                 SPayTerm = common.parseNull(res.getString(17));
                 VDesc.addElement(" ");
                 SReference = common.parseNull(res.getString(18));
                 SRemarks   = common.parseNull(res.getString(19));
                 VWODesc.addElement(common.parseNull(res.getString(20)));
              }
              res.close();
              stat.close();
           }
           catch(Exception ex)
           {
               System.out.println(ex);
               ex.printStackTrace();
           }
     }
     public String getQString()
     {
          String QString  = "";
     
          QString = " SELECT WorkOrder.Descript,"+
                    " WorkOrder.Qty, WorkOrder.Rate, "+
                    " WorkOrder.DiscPer, WorkOrder.Disc, "+
                    " WorkOrder.CenVatPer, WorkOrder.CenVat, "+
                    " WorkOrder.TaxPer, WorkOrder.Tax, "+
                    " WorkOrder.SurPer,WorkOrder.Sur,"+
                    " WorkOrder.Net,WorkOrder.Misc,WorkOrder.DueDate, "+
                    " BookTo.ToName,BookThro.ThroName,WorkOrder.PayTerms, "+
                    " WorkOrder.Reference,WorkOrder.Remarks,WorkOrder.WODesc "+
                    " FROM WorkOrder "+
                    " Inner Join BookTo   On BookTo.ToCode = WorkOrder.ToCode "+
                    " And WorkOrder.OrderNo = "+SOrdNo+" and WorkOrder.MillCode="+iMillCode+
                    " Inner Join BookThro On BookThro.ThroCode = WorkOrder.ThroCode ";
          return QString;
     }
     
    public void getAddr()
     {
           String QS = "Select nvl("+SSupTable+".Addr1,'na'),nvl("+SSupTable+".Addr2,'na'),nvl("+SSupTable+".Addr3,'na'),"+SSupTable+".EMail,"+SSupTable+".Fax,Place.PlaceName "+
                       "From "+SSupTable+" LEFT Join Place On Place.PlaceCode = "+SSupTable+".City_Code "+
                       "Where "+SSupTable+".Ac_Code = '"+SSupCode+"'";

           try
           {
                 if(theconnect==null)
                 {
                      connect=ORAConnection.getORAConnection();
                      theconnect=connect.getConnection();
                 }
                 Statement stat  = theconnect.createStatement();
                 ResultSet res   = stat.executeQuery(QS);
                 while (res.next())
                 {
                        SAddr1 = res.getString(1);
                        SAddr2 = res.getString(2);
                        SAddr3 = common.parseNull(res.getString(3));
                        SEMail = common.parseNull(res.getString(4));
                        SFaxNo = common.parseNull(res.getString(5));
                 }
                 res.close();
                 stat.close();
           }
           catch(Exception ex)
           {
                System.out.println(ex);
                ex.printStackTrace();
           }
     }
     public boolean isStationary(String SItemCode)
     {
          String SStkGroupName     = "";
          String QS                = "";

          QS =    " select stockgroup.groupname,invitems.item_code from invitems"+
                  " inner join stockgroup on stockgroup.groupcode = invitems.stkgroupcode"+
                  " where invitems.item_code = '"+SItemCode+"'";
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnect    =  oraConnection.getConnection();               
               Statement      stat          = theConnect.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result.next())
               {
                    SStkGroupName  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }
          if(SStkGroupName.equals("STATIONARY"))
               return true;

          return false;
     }

     public void createPdfFile()
     {
          try
          {
              table = new PdfPTable(10);
               table.setWidths(iWidth);
               table.setWidthPercentage(100);

               addHead(document,table);
             
               addFoot(document,table,0);
          }
          catch (Exception e)
          {
               e.printStackTrace();
          }
     }

     private void addHead(Document document,PdfPTable table) throws BadElementException
     {
          try
          {
               if(Lctr < 42)
                    return;
               
               if(Pctr > 0)
                    addFoot(document,table,1);
             

               Pctr++;

               document.newPage();

               table.flushContent();

               table = new PdfPTable(10);
               table.setWidths(iWidth);
               table.setWidthPercentage(100);

             


               String SState      = "";
               String SEpcg       = "";
               String SOrderTitle = "";
     
                       


               PdfPCell c1;

               String Str1   = "AMARJOTHI SPINNING MILLS LIMITED";
               String Str2   = "PUDUSURIPALAYAM";
               String Str3   = "NAMBIYUR  -  638 458";
               String Str4   = "";
               String Str5   = "E-Mail: purchase@amarjothi.net,mill@amarjothi.net";
               String Str6   = "Phones: 04285 - 267201,267301, Fax - 04285-267565";
               String Str7   = "Our C.S.T. No. : 440691 dt. 24.09.1990  ";
               String Str8   = "T.N.G.S.T No. R.C. No. 2960864  dt. 01.04.1995 ";
               String Str9   = "To,";
               String Str10  = "M/s. "+SSupName;
               String Str11  = SAddr1;
               String Str12  = SAddr2;
               String Str13  = SAddr3;
               String Str14  = common.Pad("Fax No: "+SFaxNo,25)+common.Pad("EMail: "+SEMail,38);

               String Str1a  = "C.Ex. RC No : 04/93";
               String Str2a  = "PLA No    : 59/93";
               String Str3a  = "Division    : Erode";
               String Str4a  = SState;
               String Str6a  = "NO";
               String Str8a  = "Book To";
               String Str10a = "Book Thro";
               String Str12a = "Reference";
               String SRem   = "Remarks";

               
               String Str1b  = "ECC No  :   AAFCA 7082C XM 001";
               String Str2b  = "Range   :  Gobichettipalayam";
               String Str3b  = "Commissionerate  : Coimbatore";
               String Str4b  = SEpcg;
               String Str6b  = common.Pad("WO"+"-"+SOrdNo,27);
               String Str8b  = common.Pad(SToName,55);
               String Str10b = common.Pad(SThroName,55);
               String Str12b = common.Pad(SReference,55);

               String Str4c  = " WORK ORDER ";
               String Str6c  = "Date";
               String Str6d  = common.Pad(SOrdDate,10);
               String Str6e  = "Page";
               String Str6f  = common.Pad(""+Pctr,3);



               c1 = new PdfPCell(new Phrase(Str1,mediumbold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.TOP | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str1a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.TOP);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str1b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.TOP | Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str2,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str2a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(2);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str2b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str3,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str3a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(2);
               c1.setBorder(Rectangle.LEFT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str3b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.RIGHT);
               table.addCell(c1);



               c1 = new PdfPCell(new Phrase(Str4,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

            /*   c1 = new PdfPCell(new Phrase(Str4a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str4b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);*/

               c1 = new PdfPCell(new Phrase(Str4c,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setColspan(6);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str5,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str6,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);
               
               c1 = new PdfPCell(new Phrase(Str6c,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6d,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6e,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6f,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str7,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str8,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str8a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str8b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str9,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

     
               c1 = new PdfPCell(new Phrase(Str10,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str10a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str10b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str11,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);


               c1 = new PdfPCell(new Phrase(Str12,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str12a,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str12b,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setRowspan(2);
               c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str13,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               table.addCell(c1);

                c1 = new PdfPCell(new Phrase(Str14,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setColspan(4);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
               table.addCell(c1); 


               c1 = new PdfPCell(new Phrase(SRem,smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase("",smallBold));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
              c1.setColspan(5);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               table.addCell(c1);

              


 




            AddCellIntoTable("Description of Materials Serviced and Nature of Work Performed", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("Uom", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("Quantity", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("Rate", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("Discount", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("Cenvat", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("Tax", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("SurCharg", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("Net", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);


            AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("Rs", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("Rs", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("Rs", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("Rs", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("Rs", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("Rs", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);


               addBody(document,table);

               document.add(table);
             
               Lctr = 16;
          }    
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }


    private void addBody(Document document,PdfPTable table) throws BadElementException
     {
          String Strl="";

          for(int i=0;i<VName.size();i++)
          {
              
               String SName     = (String)VName.elementAt(i);
               String SWODesc   = (String)VWODesc.elementAt(i);
               String SQty      = common.getRound((String)VQty.elementAt(i),2);
               String SRate     = common.getRound((String)VRate.elementAt(i),2);
               String SDisc     = common.getRound((String)VDisc.elementAt(i),2);
               String SDiscPer  = common.getRound((String)VDiscPer.elementAt(i),2)+"%";
               String SCenVat   = common.getRound((String)VCenVat.elementAt(i),2);
               String SCenVatPer= common.getRound((String)VCenVatPer.elementAt(i),2)+"%";
               String STax      = common.getRound((String)VTax.elementAt(i),2);
               String STaxPer   = common.getRound((String)VTaxPer.elementAt(i),2)+"%";
               String SSur      = common.getRound((String)VSur.elementAt(i),2);
               String SSurPer   = common.getRound((String)VSurPer.elementAt(i),2)+"%";
               String SNet      = common.getRound((String)VNet.elementAt(i),2);
               dSumOthers          = dSumOthers + common.toDouble((String)VOthers.elementAt(i));


               if(common.toDouble(SQty)==0)
                  continue;

               SName    = SName.trim();
               SWODesc  = SWODesc.trim();
               dTDisc   = dTDisc+common.toDouble(SDisc);
               dTCenVat = dTCenVat+common.toDouble(SCenVat);
               dTTax    = dTTax   +common.toDouble(STax);
               dTSur    = dTSur   +common.toDouble(SSur);
               dTNet    = dTNet   +common.toDouble(SNet);
//               dOthers  = common.toDouble(SOthers);

               Vector vect = common.getLines(SName+" ~ "+SWODesc);

               try
               {
                    for(int j=0;j<vect.size();j++)
                    {
                          if(j==0)
                          {
                              
                             
               
                AddCellIntoTable((String)vect.elementAt(j), table, Element.ALIGN_LEFT, Element.ALIGN_RIGHT, 2, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 0, 0, 8, 0, smallnormal);
                AddCellIntoTable(SQty, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 0, 0, 8, 0, smallnormal); 
                AddCellIntoTable(SRate, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 0, 0,8, 0, smallnormal);
                AddCellIntoTable(SDisc, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 0, 0, 8, 0, smallnormal);
                AddCellIntoTable(SCenVat, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 0, 0, 8, 0, smallnormal);
                AddCellIntoTable(STax, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 0, 0, 8, 0, smallnormal); 
                AddCellIntoTable(SSur, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 0, 0, 8, 0, smallnormal);
                AddCellIntoTable(SNet, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 0, 0, 8, 0, smallnormal);



                               Lctr++;
                               continue;                                   
                          }
                          if(j==1)
                          {
                              

                           
                AddCellIntoTable((String)vect.elementAt(j), table, Element.ALIGN_LEFT, Element.ALIGN_RIGHT, 2,4, 0, 8, 0, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4,0, 8, 0, smallnormal); 
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable(SDiscPer, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable(SCenVatPer, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable(STaxPer, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal); 
                AddCellIntoTable(SSurPer, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                 
                             Lctr++;
                               continue;                                   
                          }                             
                          else
                          {


                           AddCellIntoTable((String)vect.elementAt(j), table, Element.ALIGN_LEFT, Element.ALIGN_RIGHT, 2, 4, 0,8, 0, smallnormal);
                          AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal); 
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal); 
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);

                             
                               Lctr=Lctr+1;
                          }
                    }

                    if(vect.size()==1)
                    {

                        AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(SDiscPer, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(SCenVatPer, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
                AddCellIntoTable(STaxPer, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(SSurPer, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
               AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
 
                        
                         Lctr = Lctr+1;
                    }                             

                  AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2, 4, 0, 8, 2, smallnormal);
                 AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 2, smallnormal); 
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT, 1, 4, 0, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 2, smallnormal); 
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT, 1, 4, 0, 8, 2, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 2, smallnormal);
                
                    Lctr = Lctr+1;
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
          }
     }

     

    /* private void addBody(Document document,PdfPTable table) throws BadElementException
     {
          try
          {
               int iCount=0;

               PdfPCell c1;

              for(int i=0;i<VName.size();i++)
          {
               setOrderHead();
               String SName     = (String)VName.elementAt(i);
               String SWODesc   = (String)VWODesc.elementAt(i);
               String SQty      = common.getRound((String)VQty.elementAt(i),2);
               String SRate     = common.getRound((String)VRate.elementAt(i),2);
               String SDisc     = common.getRound((String)VDisc.elementAt(i),2);
               String SDiscPer  = common.getRound((String)VDiscPer.elementAt(i),2)+"%";
               String SCenVat   = common.getRound((String)VCenVat.elementAt(i),2);
               String SCenVatPer= common.getRound((String)VCenVatPer.elementAt(i),2)+"%";
               String STax      = common.getRound((String)VTax.elementAt(i),2);
               String STaxPer   = common.getRound((String)VTaxPer.elementAt(i),2)+"%";
               String SSur      = common.getRound((String)VSur.elementAt(i),2);
               String SSurPer   = common.getRound((String)VSurPer.elementAt(i),2)+"%";
               String SNet      = common.getRound((String)VNet.elementAt(i),2);
               dSumOthers          = dSumOthers + common.toDouble((String)VOthers.elementAt(i));


               if(common.toDouble(SQty)==0)
                  continue;

               SName    = SName.trim();
               SWODesc  = SWODesc.trim();
               dTDisc   = dTDisc+common.toDouble(SDisc);
               dTCenVat = dTCenVat+common.toDouble(SCenVat);
               dTTax    = dTTax   +common.toDouble(STax);
               dTSur    = dTSur   +common.toDouble(SSur);
               dTNet    = dTNet   +common.toDouble(SNet);
//               dOthers  = common.toDouble(SOthers);

               Vector vect = common.getLines(SName+" ~ "+SWODesc);

                    dTNet    = dTNet    + common.toDouble(SNet);
                    
                  /*  try
                    {
                         c1 = new PdfPCell(new Phrase(common.Pad(SCode,9),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SBkName,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);
                    
                         c1 = new PdfPCell(new Phrase(common.Pad(SName,37),tinyNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.Pad(SUOM,5),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SQty,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SRate,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SDisc,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SCenVat,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(STax,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SNet,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         Lctr = Lctr+1;

                         Vector vect = common.getLines(SDesc+"` ");
                         
                         for(int j=0;j<vect.size();j++)
                         {
                              if(j==0)
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   c1 = new PdfPCell(new Phrase(common.Pad((String)vect.elementAt(j),37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   c1 = new PdfPCell(new Phrase(SDiscPer,smallNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   c1 = new PdfPCell(new Phrase(SCenVatPer,smallNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   c1 = new PdfPCell(new Phrase(STaxPer,smallNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   addEmptyCell(table,"Left","Right");

                                   Lctr = Lctr+1;
                              }
                              else
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   c1 = new PdfPCell(new Phrase(common.Pad((String)vect.elementAt(j),37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   Lctr = Lctr+1;
                              }
                         }
                         
                         if(SMake.length() > 0)
                         {
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              c1 = new PdfPCell(new Phrase(common.Pad(SMake,37),tinyNormal));
                              c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                              c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                              table.addCell(c1);

                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              Lctr = Lctr+1;
                         }                         
                         
                         if((SCatl.trim()).length() > 0)
                         {
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              c1 = new PdfPCell(new Phrase(common.Pad("Catl   : "+SCatl,37),tinyNormal));
                              c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                              c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                              table.addCell(c1);

                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              Lctr = Lctr+1;
                         }                         
                         
                         if((SDraw.trim()).length() > 0)
                         {
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              c1 = new PdfPCell(new Phrase(common.Pad("Drawing: "+SDraw,37),tinyNormal));
                              c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                              c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                              table.addCell(c1);

                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");
                              addEmptyCell(table,"Left","Right");

                              Lctr = Lctr+1;
                         }
     
                         String    SSPCoSets = "Colour : "+common.Pad(SPColour,12);
                                   SSPCoSets+= "Sets: "+SPSets;
               
                         String    SSPSis    = "Size   : "+common.Pad(SPSize,12);
                                   SSPSis   += "Side: "+SPSide;
     
                         String    SSPBooks  = "Book No: "+SBookFromNo+" To "+SBookToNo;
                         String    SSPSlip   = "Slip No: "+SSlipFromNo+" To "+SSlipToNo;
     
                         if(isStationary(SCode))
                         {
                              if((SPColour.trim()).length() > 0 || ((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0))
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
     
                                   c1 = new PdfPCell(new Phrase(common.Pad(SSPCoSets,37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);
     
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   Lctr++;
                              }
          
                              if(((SPColour.trim()).length() > 0) || ((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0))
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
     
                                   c1 = new PdfPCell(new Phrase(common.Pad(SSPSis,37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);
     
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   Lctr++;
                              }
     
                              if((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0)
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
     
                                   c1 = new PdfPCell(new Phrase(common.Pad(SSPSlip,37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);
     
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   Lctr++;
                              }
     
                              if((SSlipFromNo.trim()).length() > 0 && common.toInt((SSlipFromNo.trim()))!=0)
                              {
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
     
                                   c1 = new PdfPCell(new Phrase(common.Pad(SSPBooks,37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);
     
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                   Lctr++;
                              }
                         }
                    }
                    catch(Exception ex){}
               }
          }    
          catch(Exception ex)
          {
               System.out.println(ex);
          }


          try
               {
                    for(int j=0;j<vect.size();j++)
                    {
                          if(j==0)
                          {


                        c1 = new PdfPCell(new Phrase(common.Pad((String)vect.elementAt(j),37),tinyNormal));
                        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                        c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                        table.addCell(c1);

                         addEmptyCell(table,"Left","Right"); 

                         c1 = new PdfPCell(new Phrase(common.Pad(SQty,9),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SRate,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);
                    
                         c1 = new PdfPCell(new Phrase(common.Pad(SDisc,37),tinyNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.Pad(SCenVat,5),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(STax,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SRate,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SDisc,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SCenVat,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                        
                         c1 = new PdfPCell(new Phrase(STax,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);                         table.addCell(c1);

                           c1 = new PdfPCell(new Phrase(SSur,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);                         table.addCell(c1);

                          c1 = new PdfPCell(new Phrase(SNet,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);                         table.addCell(c1);

                               
          
                               continue;                                   
                          }
                          if(j==1)
                          {

                        c1 = new PdfPCell(new Phrase(common.Pad((String)vect.elementAt(j),37),tinyNormal));
                        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                        c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                        table.addCell(c1);
                             
                         c1 = new PdfPCell(new Phrase("",smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase("",smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase("",smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);
                    
                         c1 = new PdfPCell(new Phrase(common.Pad(SDiscPer,37),tinyNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.Pad(SCenVatPer,5),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(STaxPer,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SSurPer,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SDisc,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SCenVat,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                        
                         c1 = new PdfPCell(new Phrase(STax,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);                         table.addCell(c1);

                           c1 = new PdfPCell(new Phrase(SSur,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);                         table.addCell(c1);

                          c1 = new PdfPCell(new Phrase("",smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);                         table.addCell(c1);

                               continue;                                   
                          }                             
                          else
                          {


                             

                                   c1 = new PdfPCell(new Phrase(common.Pad((String)vect.elementAt(j),37),tinyNormal));
                                   c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                                   c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                                   table.addCell(c1);

                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                                    addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                           
                               Lctr=Lctr+1;
                          }
                    }

                    if(vect.size()==1)
                    {


                       addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");

                        c1 = new PdfPCell(new Phrase(common.Pad(SDiscPer,37),tinyNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(common.Pad(SCenVatPer,5),smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(STaxPer,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase(SSurPer,smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
                         table.addCell(c1);

                         c1 = new PdfPCell(new Phrase("",smallNormal));
                         c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);                         table.addCell(c1);

                                
                         Lctr = Lctr+1;
                    }                             


                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right");
                                   addEmptyCell(table,"Left","Right"); 
                                      
  
                   
                    Lctr = Lctr+1;
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
     }*/

     private void addFoot(Document document,PdfPTable table,int iSig) throws BadElementException
     {
          try
          {
               PdfPCell c1;

               String SDisc   = common.getRound(dTDisc,2);
               String SCenVat = common.getRound(dTCenVat,2);
               String STax    = common.getRound(dTTax,2);
               String SSur    = common.getRound(dTSur,2);
               String SNet    = common.getRound(dTNet,2);
               String SOthers = common.getRound(dOthers,2);
               String SGrand  = common.getRound(dTNet+dOthers,2);
               
               float f = 16.0f;


                  AddCellIntoTable("T O T A L ", table, Element.ALIGN_LEFT, Element.ALIGN_RIGHT, 2, 4, 0,8, 0, smallnormal);
                 AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal); 
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable(SDisc, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable(SCenVat, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal); 
                AddCellIntoTable(STax, table, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT, 1, 4, 0, 8, 0, smallnormal);
                AddCellIntoTable(SNet, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);

              /* addEmptyCell(table);
               addEmptyCell(table);

               c1 = new PdfPCell(new Phrase(" T O T A L ",smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setFixedHeight(f);
               table.addCell(c1);

               addEmptyCell(table);
               addEmptyCell(table);
               addEmptyCell(table);

               c1 = new PdfPCell(new Phrase(SDisc,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SCenVat,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(STax,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SNet,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               table.addCell(c1);

*/
               
                if(iSig==0)
               {
                    c1 = new PdfPCell(new Phrase(" Expected Date of Delivery",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                   // c1.setFixedHeight(f);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase("Terms of Payments",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase("Rounding Off/Others",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
     
     
                    c1 = new PdfPCell(new Phrase("Order Value",smallBold));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(4);
                    table.addCell(c1);
     
     
                    c1 = new PdfPCell(new Phrase(SDueDate,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setFixedHeight(f);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                    c1 = new PdfPCell(new Phrase(SPayTerm,smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                   c1 = new PdfPCell(new Phrase("",smallNormal));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(2);
                    table.addCell(c1);
     
                   
     
                    c1 = new PdfPCell(new Phrase(SGrand,mediumBold));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c1.setColspan(4);
                    table.addCell(c1);
               }


               String Str1  = " Note  :   1. Please mention our order No. and material code in all correspondence with us.";
               String Str2  = common.Space(14)+" 2. Please mention our TIN Number in your invoices.";
               String Str3  = common.Space(14)+" 3. Please send  invoices in triplicate. Original invoice to be sent direct to accounts manager.";
               String Str4  = common.Space(14)+" 4. Please send copy of your invoice along with consignment.";
               String Str5  = common.Space(14)+" 5. We reserve the right to increase / decrease the order qty. / cancel the order without assigning any reason";
               String Str6  = common.Space(14)+" 6. Any legal dispute shall be within the jurisdiction of Tirupur.";

               String Str7  = common.Space(14)+" 7. These materials should be supplied against EPCG Scheme  for which please send us your Proforma Invoice along with the catalogue immediately so that we can open the EPCG Licence.";
               String Str8  = common.Space(14)+" 8. Please quote the EPCG License No in your all Bills.";

               String Str9  = "For AMARJOTHI SPINNING MILLS LTD";
               String Str10 = common.Space(10)+"Prepared By"+common.Space(50)+"Checked By"+common.Space(50)+"I.A."+common.Space(50)+"Authorised Signatory";
               String Str11 = " Special Instruction :-";

               String Str12 = " Regd. Office - 'AMARJOTHI HOUSE' 157, Kumaran Road, Tirupur - 638 601. Phone : 0421- 2201980 to 2201984";
               String Str13 = " E-Mail       - amarin@giasmd01.vsnl.net.in, amarjothi@vsnl.com  ";

               String Str14 = "I N D I A 'S   F I N E S T  M E L A N G E  Y A R N  P R O D U C E R S";
               
            
	       String SGstNote1 = " GST Note : "; 
	       String SGstNote2 = common.Space(15)+" ITC shall be eligible only on the basis of Credit available in GSTN Portal. The Supplier shall be responsible for proper filing of GST Return in the  ";
	       String SGstNote3 = common.Space(15)+" GSTN  Portal.  If for any reason,  the GST as indicated in the Invoice is not reflected in the  GSTN  Portal,  the tax component  shall be debited to ";
	       String SGstNote4 = common.Space(15)+" your account with interest. ";



               c1 = new PdfPCell(new Phrase(Str1,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str2,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str3,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str4,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str5,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str6,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

             
              // addEmptySpanRow(table,"LEFT","RIGHT");

               c1 = new PdfPCell(new Phrase(SGstNote1,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SGstNote2,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SGstNote3,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(SGstNote4,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

             //  addEmptySpanRow(table,"LEFT","RIGHT");

               c1 = new PdfPCell(new Phrase(Str9,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

             //  addEmptySpanRow(table,"LEFT","RIGHT");
             //  addEmptySpanRow(table,"LEFT","RIGHT");
             // addEmptySpanRow(table,"LEFT","RIGHT");


		AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 10,4, 0, 8, 0, smallnormal);
		AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 10, 4, 0, 8, 0, smallnormal);
		AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 10, 4, 0, 8, 0, smallnormal);
		AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 10,4, 0, 8, 0, smallnormal);
		AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 10, 4, 0, 8, 0, smallnormal);
		AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 10, 4, 0, 8, 0, smallnormal);
		AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 10,4, 0, 8, 0, smallnormal);
		AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 10, 4, 0, 8, 0, smallnormal);
		AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 10, 4, 0, 8, 0, smallnormal);


               c1 = new PdfPCell(new Phrase(Str10,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
               c1.setColspan(10);
               table.addCell(c1);


               
               c1 = new PdfPCell(new Phrase(Str11,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(10);
               c1.setFixedHeight(f);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str12,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str13,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_LEFT);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
               c1.setColspan(10);
               table.addCell(c1);

               c1 = new PdfPCell(new Phrase(Str14,smallNormal));
               c1.setHorizontalAlignment(Element.ALIGN_CENTER);
               c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
               c1.setColspan(10);
               c1.setFixedHeight(f);
               table.addCell(c1);

               document.add(table);


               String Str15="";

               if(iSig==0)
                 {
                     String SDateTime = common.getServerDateTime2();
                    Str15 = "Report Taken on "+SDateTime+"";
                 }
               else{
                    Str15 = "(Continued on Next Page)";
                   }

               Paragraph paragraph = new Paragraph(Str15,smallNormal);
               document.add(paragraph);

          }    
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void addEmptyCell(PdfPTable table)
     {
          PdfPCell c1 = new PdfPCell();
          table.addCell(c1);
     }

     private void addEmptyCell(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
          table.addCell(c1);
     }

     private void addEmptyRow(PdfPTable table)
     {
          for(int i=0;i<SHead.length;i++)
          {
               PdfPCell c1 = new PdfPCell();
               table.addCell(c1);
          }
     }

     private void addEmptySpanRow(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
          c1.setColspan(SHead.length);
          table.addCell(c1);
     }

     private static void addEmptyLine(Paragraph paragraph, int number)
     {
          for (int i = 0; i < number; i++)
          {
               paragraph.add(new Paragraph(" "));
          }
     }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iRIGHTBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iRIGHTBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iRIGHTBorder, int iTopBorder, int iRightBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        //c1.setRowspan(iRowSpan);
        c1.setBorder(iRIGHTBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iRIGHTBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iRIGHTBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iRIGHTBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iRIGHTBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

}
