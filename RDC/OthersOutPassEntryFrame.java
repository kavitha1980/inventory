package RDC;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;
import jdbc.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class OthersOutPassEntryFrame extends JInternalFrame
{
     JPanel  TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopCenter;

     JTextField  TSupCode;
     TimeField   TOutTime;
     MyTextField TDCNo,TInvNo,TFormJJNo;
     DateField   DDCDate,DInvDate,DFormJJDate;
     JButton     BSupplier;
     JComboBox   JCBookNo;

     JButton BOk;

     Object RowData[][];

     String ColumnData[]={"Sl No","Description","Quantity","Purpose"};
     String ColumnType[]={"N"    ,"S"          ,"N"       ,"S"      };

     TabReport      tabreport;
     Vector         VOutPassRec;
     JLayeredPane   Layer;
     int iUserCode,iMillCode;
     String SYearCode;

     Common   common = new Common();
     ORAConnection connect;
     Connection theconnect;

     String SOutPassNo="";

     int iBookNo = 0;

     OthersOutPassEntryFrame(JLayeredPane Layer,int iMillCode,int iUserCode,String SYearCode)
     {
          this.Layer     = Layer;
          this.iMillCode = iMillCode;
          this.iUserCode = iUserCode;
          this.SYearCode = SYearCode;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel       = new JPanel(true);
          TopLeft        = new JPanel();
          TopRight       = new JPanel();
          TopCenter      = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();

          TOutTime       = new TimeField(true);
          TDCNo          = new MyTextField(20);
          TInvNo         = new MyTextField(20);
          TFormJJNo      = new MyTextField(20);
          DDCDate        = new DateField();
          DInvDate       = new DateField();
          DFormJJDate    = new DateField();
          TSupCode       = new JTextField();
          BSupplier      = new JButton("Select Party");

          JCBookNo       = new JComboBox();
          JCBookNo       . addItem("Book1");
          JCBookNo       . addItem("Book2");
          JCBookNo       . addItem("Book3");
          JCBookNo       . setEnabled(false);

          BOk            = new JButton("Save & Exit");

          VOutPassRec    = new Vector();

          BOk.setMnemonic('S');
     }
     public void setLayouts()
     {
          TopPanel.setLayout(new GridLayout(5,4,5,5));
          BottomPanel.setLayout(new FlowLayout());
          MiddlePanel.setLayout(new BorderLayout());
          TopPanel.setBorder(new TitledBorder("Info"));
          MiddlePanel.setBorder(new TitledBorder("OutPass List"));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
          setTitle("Others OutPass Entry");
     }
     public void addComponents()
     {
          TopPanel.add(new MyLabel("Party Name"));
          TopPanel.add(BSupplier);

          TopPanel.add(new MyLabel(""));
          TopPanel.add(new MyLabel(""));

          TopPanel.add(new Label("Out Pass Book"));
          TopPanel.add(JCBookNo);

          TopPanel.add(new MyLabel("Time Out"));
          TopPanel.add(TOutTime);

          TopPanel.add(new MyLabel("D.C. No"));
          TopPanel.add(TDCNo);

          TopPanel.add(new MyLabel("D.C. Date"));
          TopPanel.add(DDCDate);

          TopPanel.add(new MyLabel("Invoice No"));
          TopPanel.add(TInvNo);

          TopPanel.add(new MyLabel("Invoice Date"));
          TopPanel.add(DInvDate);

          TopPanel.add(new MyLabel("From JJ No"));
          TopPanel.add(TFormJJNo);

          TopPanel.add(new MyLabel("Form JJ Date"));
          TopPanel.add(DFormJJDate);

          BottomPanel.add(BOk);
          RowData = new Object[8][ColumnData.length];

          for(int i=0;i<8;i++)
          {
               for(int j=0;j<ColumnData.length;j++)
               {
                    RowData[i][j]="";
               }
               VOutPassRec.addElement(new OutPassRec(Layer,RowData,String.valueOf(i),this));
               RowData[i][0]=""+(i+1);
          }

          MiddlePanel.add("Center",tabreport=new TabReport(RowData,ColumnData,ColumnType));

          tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

          getContentPane() .add("North",TopPanel);
          getContentPane() .add("Center",MiddlePanel);
          getContentPane() .add("South",BottomPanel);
     }

     public void addListeners()
     {
          BSupplier .addActionListener(new PartySearch(Layer,TSupCode));
          tabreport .ReportTable.addKeyListener(new KeyList());
          BOk       .addActionListener(new ActList());
     }
     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(false);
               boolean bSig = false;
               try
               {
                     bSig = allOk();
               }
               catch(Exception ex)
               {
                     System.out.println(ex);
                     ex.printStackTrace();
               }
               if(bSig)
               {
                    SOutPassNo   = getNextOutPassNo();
     
                    insertOutPass();
                    insertOutPassDetails();
                    UpdateOutPassNo();
     
                    String SNum = "The OutPass Number is "+SOutPassNo;
                    JOptionPane.showMessageDialog(null,SNum,"Information",JOptionPane.INFORMATION_MESSAGE);
                    removeHelpFrame();
               }
               else
               {
                     BOk.setEnabled(true);
               }
          }
     }
     public boolean allOk()
     {
          String SSupCode     = TSupCode.getText();

          int iCount=0;

          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
          {
               String SDesc = ((String)tabreport.ReportTable.getValueAt(i,1)).trim();
 
               if(SDesc.length()>0)
               {
                    iCount++;
               }
          }
 
          if(iCount<=0)
          {
                JOptionPane.showMessageDialog(null,"All Rows Empty","Error",JOptionPane.ERROR_MESSAGE);
                tabreport.ReportTable.requestFocus();
                return false;
          }

          if(SSupCode.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Select Party","Error",JOptionPane.ERROR_MESSAGE);
               BSupplier.setEnabled(true);
               BSupplier.requestFocus();
               return false;
          }

          String SOutTime = common.parseNull(TOutTime.toString());

          if(SOutTime.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Out Time Must be filled ","Error",JOptionPane.ERROR_MESSAGE);
               TOutTime.THours.requestFocus();
               return false;
          }

          String SDCNo     = TDCNo.getText().trim();
          String SInvNo    = TInvNo.getText().trim();
          String SFormJJNo = TFormJJNo.getText().trim();

          if((SDCNo.length()==0) && (SInvNo.length()==0) && (SFormJJNo.length()==0))
          {
               JOptionPane.showMessageDialog(null,"DC or Inv or FormJJ No Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TDCNo.requestFocus();
               return false;
          }

          String SDCDate     = DDCDate.toNormal();
          String SInvDate    = DInvDate.toNormal();
          String SFormJJDate = DFormJJDate.toNormal();

          if((SDCNo.length()>0) && (SDCDate.length()!=8))
          {
               JOptionPane.showMessageDialog(null,"Invalid DC Date","Error",JOptionPane.ERROR_MESSAGE);
               DDCDate.TDay.requestFocus();
               return false;
          }

          if((SInvNo.length()>0) && (SInvDate.length()!=8))
          {
               JOptionPane.showMessageDialog(null,"Invalid Inv Date","Error",JOptionPane.ERROR_MESSAGE);
               DInvDate.TDay.requestFocus();
               return false;
          }

          if((SFormJJNo.length()>0) && (SFormJJDate.length()!=8))
          {
               JOptionPane.showMessageDialog(null,"Invalid FormJJ Date","Error",JOptionPane.ERROR_MESSAGE);
               DFormJJDate.TDay.requestFocus();
               return false;
          }

          return true;
     }

     private void insertOutPass()
     {
          try
          {
               String    QS = " Insert into OutPass (ID,OUTPASSNO,OUTPASSDATE,MATERIALTYPE,BOOKTYPE,MILLCODE,USERCODE,CREATIONDATE) values (";
                         QS   = QS+" OUTPASS_SEQ.nextval ,";
                         QS   = QS+SOutPassNo+",";
                         QS   = QS+"'"+common.getServerPureDate()+"',";
                         QS   = QS+"1"+",";
                         QS   = QS+iBookNo+",";
                         QS   = QS+iMillCode+",";
                         QS   = QS+iUserCode+",";
                         QS   = QS+"'"+common.getServerDate()+"')";

               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();
                              stat           . execute(QS);
                              stat           . close();
          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void insertOutPassDetails()
     {
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();

               for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
               {

                    String SDesc = ((String)tabreport.ReportTable.getValueAt(i,1)).trim();

                    if(SDesc.equals(""))
                         continue;

                    String QS = " Insert into OutPassDetails (ID,OUTPASSNO,PARTYCODE,MATERIALTYPE,BOOKTYPE,MILLCODE,DESCRIPT,QTY,PURPOSE,TIMEOUT,DCNO,DCDATE,INVNO,INVDATE,FORMJJNO,FORMJJDATE) values (";
                           QS = QS+" OUTPASSDETAILS_SEQ.nextval ,";
                           QS = QS+SOutPassNo+",";
                           QS = QS+"'"+TSupCode.getText()+"',";
                           QS = QS+"1"+",";
                           QS = QS+iBookNo+",";
                           QS = QS+iMillCode+",";
                           QS = QS+"'"+SDesc.toUpperCase()+"',";
                           QS = QS+"0"+(String)tabreport.ReportTable.getValueAt(i,2)+",";
                           QS = QS+"'"+((String)tabreport.ReportTable.getValueAt(i,3)).toUpperCase()+"',";
                           QS = QS+"'"+TOutTime.toString()+"',";
                           QS = QS+"'"+TDCNo.getText()+"',";
                           QS = QS+"0"+DDCDate.toNormal()+",";
                           QS = QS+"'"+TInvNo.getText()+"',";
                           QS = QS+"0"+DInvDate.toNormal()+",";
                           QS = QS+"'"+TFormJJNo.getText()+"',";
                           QS = QS+"0"+DFormJJDate.toNormal()+")";

                    stat.execute(QS);
               }
               stat.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public String getNextOutPassNo()
     {
          String    QS           = "";

          int       iOutPassNo   = 0;

                    iBookNo   = 0;
                    iBookNo   = JCBookNo.getSelectedIndex();
                    iBookNo   = iBookNo+1;      

          if(iBookNo==1)
               QS = "  Select maxno from Config"+iMillCode+""+SYearCode+" where id = 13";

          /*if(iBookNo==2)
               QS = "  Select maxno from config where id = 27";

          if(iBookNo==3)
               QS = "  Select maxno from config where id = 28";*/

          try
          {
               ORAConnection  oraConnection  = ORAConnection     . getORAConnection();
               Connection     theConnection  = oraConnection     . getConnection();   
               Statement      stat           = theConnection     . createStatement();
               ResultSet      result         = stat              . executeQuery(QS);

                         result         . next();
                         iOutPassNo     = result.getInt(1);

                         result         . close();
                         stat           . close();

                         iOutPassNo     = iOutPassNo+1;

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

          return String.valueOf(iOutPassNo);
     }

     public void UpdateOutPassNo()
     {
          String    QS1       = "";

                    iBookNo   = 0;
                    iBookNo   = JCBookNo.getSelectedIndex();
                    iBookNo   = iBookNo+1;      

          if(iBookNo==1)
               QS1 = " Update Config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1  where id = 13";

          /*if(iBookNo==2)
               QS1 = " Update config set MaxNo = MaxNo+1  where id = 27";

          if(iBookNo==3)
               QS1 = " Update config set MaxNo = MaxNo+1  where id = 28";*/

          try
          {
               ORAConnection  oraConnection  = ORAConnection     . getORAConnection();
               Connection     theConnection  = oraConnection     . getConnection();   
               Statement      stat           = theConnection     . createStatement();
                              stat           . executeUpdate(QS1);
                              stat           . close();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    OutPassRec outpassrec=(OutPassRec)VOutPassRec.elementAt(tabreport.ReportTable.getSelectedRow());
                    outpassrec.setActivation(true);
                    outpassrec.onThisFrame();
               }
          }
     }


}
