package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;

import jdbc.*;
import util.*;
import guiutil.*;

public class OutPassRec 
{
     JInternalFrame theFrame;

     JPanel         TopPanel,BottomPanel;

     MyTextField    TDesc,TPurpose;

     NextField      TQty;

     JButton        BOk;

     JLayeredPane   Layer;
     Object         RowData[][];
     String         Id;
     int            iActivation;
     OthersOutPassEntryFrame    othersoutpassentryframe;

     Common common = new Common();

     public OutPassRec(JLayeredPane Layer,Object[][] RowData,String Id,OthersOutPassEntryFrame othersoutpassentryframe)
     {
          this.Layer     = Layer;
          this.RowData   = RowData;
          this.Id        = Id;
          this.othersoutpassentryframe = othersoutpassentryframe;
          iActivation    = 0;

          createComponents();
     }
     public void setActivation(boolean bflag)
     {
          if(iActivation > 0)
               return;
          setLayouts();
          addComponents();
          addListeners();
          if(bflag)
               setPresets();
     }
     public void setPresets()
     {
          int i = common.toInt(Id);

          TDesc.setText((String)RowData[i][1]);
          TQty.setText((String)RowData[i][2]);
          TPurpose.setText((String)RowData[i][3]);
     }

     public void createComponents()
     {
          theFrame       = new JInternalFrame();
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();

          TDesc          = new MyTextField(60);
          TPurpose       = new MyTextField(60);
          TQty           = new NextField(12);
          BOk            = new JButton("Submit");
          BOk.setMnemonic('U');
     }

     public void setLayouts()
     {
          theFrame.setClosable(true);
          theFrame.setMaximizable(true);
          theFrame.setIconifiable(true);
          theFrame.setResizable(true);
          theFrame.setBounds(100,100,550,200);
          theFrame.setTitle("Details about the goods");
          theFrame.getContentPane().setLayout(new BorderLayout());
          TopPanel.setLayout(new GridLayout(6,2));
          BottomPanel.setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel("Description About the Goods"));
          TopPanel.add(TDesc);
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel("Quantity"));
          TopPanel.add(TQty);
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel("Purpose"));
          TopPanel.add(TPurpose);

          BottomPanel.add(BOk);

          theFrame.getContentPane().add(TopPanel,BorderLayout.NORTH);
          theFrame.getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          theFrame.setVisible(false);
     }

     public void addListeners()
     {
          BOk.addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(checkData())
               {
                    int i         = common.toInt(Id);
                    RowData[i][1] = TDesc.getText();
                    RowData[i][2] = TQty.getText();
                    RowData[i][3] = TPurpose.getText();
     
                    offThisFrame();
               }
          }
     }

     public void offThisFrame()
     {
          theFrame.setVisible(false);
          Layer.updateUI();
          othersoutpassentryframe.tabreport.ReportTable.requestFocus();
     }

     public void onThisFrame()
     {
          try
          {
               Layer.add(theFrame);
               iActivation++;
               theFrame.moveToFront();
               Layer.repaint();
               Layer.updateUI();
               theFrame.setVisible(true);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     public boolean checkData()
     {
          if((TDesc.getText()).equals(""))
          {
               JOptionPane.showMessageDialog(null,"Description Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TDesc.requestFocus();
               return false;
          }
          if((TQty.getText()).equals(""))
          {
               JOptionPane.showMessageDialog(null,"Quantity Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TQty.requestFocus();
               return false;
          }
          if((TQty.getText()).length()>12)
          {
               JOptionPane.showMessageDialog(null,"Invalid Quantity","Error",JOptionPane.ERROR_MESSAGE);
               TQty.requestFocus();
               return false;
          }
          if((TPurpose.getText()).equals(""))
          {
               JOptionPane.showMessageDialog(null,"Purpose Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TPurpose.requestFocus();
               return false;
          }
          return true;
     }
}
