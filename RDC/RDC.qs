 Select RDC.RDCDate,RDC.RDCNo,Supplier.Name,Descript, 
 RDC.Remarks,RDC.Thro,RDC.Qty,RDC.RecQty, 
 (RDC.Qty-RDC.RecQty),RDC.DueDate,RDC.Sup_Code,RawUser.UserName 
 From RDC 
 Inner Join Supplier.Ac_Code = RDC.Sup_Code 
 Inner Join RawUser on RDC.MemoAuthUserCode=RawUser.UserCode 
 Where RDC.RecQty < RDC.Qty 
 and RDC.MillCode=0 and Supplier.Ac_Code=?
 And RDC.AssetFlag=0 and DocType=0 
