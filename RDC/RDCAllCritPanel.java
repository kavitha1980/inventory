package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCAllCritPanel extends JPanel
{
    JPanel TopPanel;
    JPanel SupplierPanel;
    JPanel ListPanel;
    JPanel TypePanel;
    JPanel SortPanel;
    JPanel BasicPanel;
    JPanel ControlPanel;
    JPanel ApplyPanel;
    JPanel EmptyPanel;

    Common common = new Common();
    Vector VSup,VSupCode;
    MyComboBox JCSup,JCList,JCType;
    JTextField TSort;

    JRadioButton JRPeriod,JRNo;
    JRadioButton JRAllSup,JRSeleSup;
    JRadioButton JRAllList,JRSeleList;
    JRadioButton JRAllType,JRSeleType;
    JRadioButton JRDate,JROrdNo;

    JButton      BApply;
    JTextField   TStNo,TEnNo;
    DateField    TStDate,TEnDate;

    boolean bsig;
    ORAConnection connect;
    Connection theconnect;

    int iMillCode;
    String SSupTable;

    RDCAllCritPanel(int iMillCode,String SSupTable)
    {
        this.iMillCode = iMillCode;
        this.SSupTable = SSupTable;

        getUDGBS();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
    }

    public void createComponents()
    {
        TopPanel      = new JPanel();

        ListPanel     = new JPanel();
        TypePanel     = new JPanel();
        SortPanel     = new JPanel();
        ControlPanel  = new JPanel();
        ApplyPanel    = new JPanel();
        SupplierPanel = new JPanel();
        BasicPanel    = new JPanel();
        EmptyPanel    = new JPanel();

        JRPeriod = new JRadioButton("Periodical",true);
        JRNo     = new JRadioButton("RDC No");

        JRAllSup    = new JRadioButton("All",true);
        JRSeleSup   = new JRadioButton("Selected");
        JCSup       = new MyComboBox(VSup);
        JCSup.setEnabled(false);

        JRAllList   = new JRadioButton("All",true);
        JRSeleList  = new JRadioButton("Selected");
        JCList      = new MyComboBox();
        JCList.setEnabled(false);

        JRAllType   = new JRadioButton("All",true);
        JRSeleType  = new JRadioButton("Selected");
        JCType      = new MyComboBox();
        JCType.setEnabled(false);

        JRDate      = new JRadioButton("Date",true);
        JROrdNo     = new JRadioButton("Order No");
        bsig = true;

        TSort       = new JTextField();
        TStNo       = new JTextField();
        TEnNo       = new JTextField();

        TStDate       = new DateField();
        TEnDate       = new DateField();

        BApply      = new JButton("Apply");

        TStDate.setTodayDate();
        TEnDate.setTodayDate();

    }
    public void setLayouts()
    {
        setLayout(new GridLayout(1,1));
        TopPanel.setLayout(new GridLayout(2,4));

        ListPanel  .setLayout(new GridLayout(3,1));
        TypePanel  .setLayout(new GridLayout(3,1));
        SortPanel   .setLayout(new GridLayout(3,1));
        ControlPanel  .setLayout(new GridLayout(2,1));
        ApplyPanel  .setLayout(new GridLayout(3,1));
        SupplierPanel  .setLayout(new GridLayout(3,1));
        BasicPanel.setLayout(new GridLayout(2,1)); 

        SupplierPanel  .setBorder(new TitledBorder("Supplier"));
        ListPanel.setBorder(new TitledBorder("List Only"));
        TypePanel.setBorder(new TitledBorder("Rdc Type"));
        SortPanel .setBorder(new TitledBorder("Sort on"));
        BasicPanel .setBorder(new TitledBorder("Based on"));
        ControlPanel.setBorder(new TitledBorder("Control"));
        ApplyPanel.setBorder(new TitledBorder("Apply"));
        EmptyPanel.setBorder(new TitledBorder(""));
    }
    public void addComponents()
    {
        JCList.addItem("Returnable");
        JCList.addItem("Non-Returnable");

        JCType.addItem("Regular");
        JCType.addItem("Non-Regular");

        add(TopPanel);

        TopPanel.add(ListPanel);
        TopPanel.add(TypePanel);
        TopPanel.add(SupplierPanel);
        TopPanel.add(SortPanel);
        TopPanel.add(BasicPanel);
        TopPanel.add(ControlPanel);
        TopPanel.add(ApplyPanel);
        TopPanel.add(EmptyPanel);

        BasicPanel.add(JRPeriod);
        BasicPanel.add(JRNo);

        SupplierPanel.add(JRAllSup);
        SupplierPanel.add(JRSeleSup);
        SupplierPanel.add(JCSup);

        ListPanel.add(JRAllList);
        ListPanel.add(JRSeleList);
        ListPanel.add(JCList);

        TypePanel.add(JRAllType);
        TypePanel.add(JRSeleType);
        TypePanel.add(JCType);

        SortPanel.add("Center",TSort);

        addControlPanel();

        ApplyPanel.add(new MyLabel(" "));
        ApplyPanel.add(BApply);
        ApplyPanel.add(new MyLabel(" "));
    }
    public void addControlPanel()
    {
        ControlPanel.removeAll();
        if(bsig)
        {
            ControlPanel.add(TStDate);
            ControlPanel.add(TEnDate);
        }
        else
        {
            ControlPanel.add(TStNo);
            ControlPanel.add(TEnNo);
        }
        ControlPanel.updateUI();
    }
    public void addListeners()
    {
        JRAllSup.addActionListener(new SupList());
        JRSeleSup.addActionListener(new SupList());

        JRAllList.addActionListener(new SelectList());
        JRSeleList.addActionListener(new SelectList());

        JRAllType.addActionListener(new TypeList());
        JRSeleType.addActionListener(new TypeList());

        JRDate.addActionListener(new BaseList());
        JROrdNo.addActionListener(new BaseList());

        JRPeriod.addActionListener(new JRList());
        JRNo.addActionListener(new JRList());
    }
     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    ControlPanel.setBorder(new TitledBorder("Periodical"));
                    ControlPanel.removeAll();
                    ControlPanel.add(TStDate);
                    ControlPanel.add(TEnDate);
                    ControlPanel.updateUI();
                    JRNo.setSelected(false);
               }
               else
               {
                    ControlPanel.setBorder(new TitledBorder("Numbered"));
                    ControlPanel.removeAll();
                    ControlPanel.add(TStNo);
                    ControlPanel.add(TEnNo);
                    ControlPanel.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }

    public class SupList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllSup)
            {
                JRAllSup.setSelected(true);
                JRSeleSup.setSelected(false);
                JCSup.setEnabled(false);
            }
            if(ae.getSource()==JRSeleSup)
            {
                JRAllSup.setSelected(false);
                JRSeleSup.setSelected(true);
                JCSup.setEnabled(true);
            }
        }
    }
    public class SelectList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllList)
            {
                JRAllList.setSelected(true);
                JRSeleList.setSelected(false);
                JCList.setEnabled(false);
            }
            if(ae.getSource()==JRSeleList)
            {
                JRAllList.setSelected(false);
                JRSeleList.setSelected(true);
                JCList.setEnabled(true);
            }
        }
    }
    public class TypeList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllType)
            {
                JRAllType.setSelected(true);
                JRSeleType.setSelected(false);
                JCType.setEnabled(false);
            }
            if(ae.getSource()==JRSeleType)
            {
                JRAllType.setSelected(false);
                JRSeleType.setSelected(true);
                JCType.setEnabled(true);
            }
        }
    }

    public class BaseList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRDate)
            {
                bsig = true;
                JRDate.setSelected(true);
                JROrdNo.setSelected(false);
                addControlPanel();
            }
            if(ae.getSource()==JROrdNo)
            {
                bsig = false;
                JROrdNo.setSelected(true);
                JRDate.setSelected(false);
                addControlPanel();
            }
        }
    }

    public void getUDGBS()
    {
        VSup       = new Vector();
        VSupCode   = new Vector();

        try
        {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();

               ResultSet result3 = stat.executeQuery("Select Name,Ac_Code From "+SSupTable+" Order By Name");
               while(result3.next())
               {
                    VSup.addElement(result3.getString(1));
                    VSupCode.addElement(result3.getString(2));
               }
               result3.close();
               stat.close();
        }
        catch(Exception ex)
        {
            System.out.println("Unit,Dept & Supplier :"+ex);
        }
    }


}
