package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;



// pdf import
import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;



public class RDCDateRegister extends JInternalFrame
{
     JPanel     TopPanel;
     JPanel     BottomPanel;
     JPanel     MiddlePanel;

     String     SDate;
     JButton    BApply,BPrint,BCreatePdf;
     JTextField TFile;
    
     DateField TDate;

     MyComboBox JCAsset;

    
     Object RowData[][];
     String ColumnData[] = {"RDC Date","RDC No","Supplier","Description","Remarks","Thro","Qty Sent","Qty Recd","Qty Pending","Due Date","Delayed Days","RDC Status"}; 
     String ColumnType[] = {"S","S","S","S","S","S","N","N","N","S","N","S"}; 

     JLayeredPane DeskTop;
     StatusPanel SPanel;
     int iMillCode;
     String SSupTable,SMillName;
     JComboBox      JCFilter;
     TabReport tabreport;
     Common common = new Common();
     Vector VRDCDate,VRDCNo,VSupName,VDesc,VRemark,VThro,VDelayDays;
     Vector VRDCQty,VRecQty,VPending,VDueDate,VAcCode;

     Vector VORDCDate,VORDCNo,VOSupName,VODesc,VORemark,VOThro;
     Vector VORDCQty,VORecQty,VOPending,VODueDate,VOAcCode,VRDCStatus;

     int Lctr = 100,Pctr=0;
     FileWriter FW;
     String str6;
     boolean bBlank=true;
     ORAConnection connect;
     Connection theconnect;


   // pdf 
    
    Document document;
    PdfPTable table;
    int iTotalColumns = 12;
    int iWidth[] = {12, 10, 25, 25, 25, 10, 8, 8, 8, 12, 8,12};
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 11, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font bigNormal = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
    String SFile="";   
      String PDFFile = "";

     RDCDateRegister(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode,String SSupTable,String SMillName)
     {
         super("RDC Pending As On today - Datewise");
         this.DeskTop   = DeskTop;
         this.SPanel    = SPanel;
         this.iMillCode = iMillCode;
         this.SSupTable = SSupTable;
         this.SMillName = SMillName;

		PDFFile = common.getPrintPath()+"/RDCPending2.pdf";

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     private void createComponents()
     {
          TopPanel    = new JPanel(true);
          BottomPanel = new JPanel(true);
          MiddlePanel = new JPanel(true);

          TDate       = new DateField();
          JCAsset     = new MyComboBox();
          JCFilter    = new MyComboBox();   
          BApply      = new JButton("Apply");
          BPrint      = new JButton("Print");
          BCreatePdf   = new JButton("Create pdf"); 
          TFile       = new JTextField(15);

          TDate.setTodayDate();
          TFile.setText("RDCPend2.prn");
          TFile.setEditable(false);
          BPrint.setEnabled(false);

          BApply.setMnemonic('A');
          BPrint.setMnemonic('P');

          JCAsset.addItem("Non Asset");
          JCAsset.addItem("Asset");
          JCAsset.addItem("ALL");
          JCAsset.setSelectedItem("ALL");

     }
     private void setLayouts()
     {
         TopPanel.setLayout(new FlowLayout(0,0,0));
         MiddlePanel.setLayout(new BorderLayout());

         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,790,500);
     }
     private void addComponents()
     {
          JCFilter             . addItem("ALL");
          JCFilter             . addItem("BELOW 30");
          JCFilter             . addItem("ABOVE 30");
           
             
          TopPanel.add(new JLabel("As On"));
          TopPanel.add(TDate);

          TopPanel.add(new JLabel("Sorted By"));
          TopPanel.add(JCFilter);

          TopPanel.add(new JLabel("Asset"));
          TopPanel.add(JCAsset);

          TopPanel.add(BApply);

          BottomPanel.add(TFile);
          BottomPanel.add(BPrint);
          BottomPanel. add(BCreatePdf);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

     }
     private void addListeners()
     {
          BApply.addActionListener(new ActList());
          BPrint.addActionListener(new PrintList());
          BCreatePdf.addActionListener(new PrintList());
     }

     private class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource() == BCreatePdf)
               {
                        createPDFFile();
                     
                      try{
                File theFile   = new File(PDFFile);
                Desktop        . getDesktop() . open(theFile);
                  

                  
               }catch(Exception ex){}

                       
                             // saveData();
                               // printPDF();
                        
                  
               }

               if(iMillCode==0)
               {
                    setOtherVector();
               }
               setRDCPendingReport();
               removeHelpFrame();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     private void setRDCPendingReport()
     {
          if(VAcCode.size() == 0)
               return;

          Lctr = 100;Pctr=0;
          String SFile = TFile.getText();
          if((SFile.trim()).length()==0)
               SFile = "1.prn";

          int iPNo=0;
          bBlank=false;
          try
          {
               FW = new FileWriter(common.getPrintPath()+SFile);
               for(int i=0;i<VRDCDate.size();i++)
               {
                    setHead();
                    int iNo = common.toInt((String)VRDCNo.elementAt(i));
                    if(iNo != iPNo)
                    {
                         if(i>0 && Lctr !=7)
                         {
                              FW.write(str6+"\n");
                              Lctr++;
                         }
                         iPNo = iNo;
                         bBlank=false;
                    }
                    setBody(i);
                    bBlank=true;
               }

               if(iMillCode==0)
               {
                    if(VOAcCode.size()>0)
                    {
                         Lctr = 100;
               
                         int iOPNo=0;
                         bBlank=false;

                         for(int i=0;i<VORDCDate.size();i++)
                         {
                              setOtherHead();
                              int iONo = common.toInt((String)VORDCNo.elementAt(i));
                              if(iONo != iOPNo)
                              {
                                   if(i>0 && Lctr !=7)
                                   {
                                        FW.write(str6+"\n");
                                        Lctr++;
                                   }
                                   iOPNo = iONo;
                                   bBlank=false;
                              }
                              setOtherBody(i);
                              bBlank=true;
                         }
                    }
               }

               FW.write(str6+"\n");
               String SDateTime = common.getServerDateTime2();
               String SEnd = "Report Taken on "+SDateTime+"\n";
               FW.write(SEnd);
               FW.close();
          }
          catch(Exception ex){}
     }

     private void setHead() throws Exception
     {
          if (Lctr < 60)
               return;

          String str1 = "Company : "+SMillName;
          String str2 = "Document : RDC Pending List Register "+TDate.toString();
          String str4 = common.Pad("RDC",10)+common.Space(3)+common.Rad("RDC",10)+common.Space(3)+
                        common.Pad("Supplier",30)+common.Space(3)+ 
                        common.Pad("Material Description",40)+common.Space(3)+common.Pad("Instructions",15)+common.Space(3)+
                        common.Pad("Thro'",15)+common.Space(3)+ 
                        common.Rad("RDC",6)+common.Space(3)+common.Rad("Recd",6)+common.Space(3)+
                        common.Rad("Pending",6)+common.Space(3)+common.Pad("Due Date",10)+common.Space(3)+
                        common.Rad("Delayed",7)+common.Space(3)+common.Pad("RDC Status",30);

          String str5 = common.Pad("Date",10)+common.Space(3)+common.Rad("No",10)+common.Space(3)+
                        common.Space(30)+common.Space(3)+ 
                        common.Pad(" ",40)+common.Space(3)+common.Pad(" ",15)+common.Space(3)+
                        common.Space(15)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Rad("Qty",6)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Pad(" ",10)+common.Space(3)+
                        common.Rad("Days",7)+common.Space(3)+common.Pad("",30);
          str6 = common.Replicate("-",str5.length());

          if (Pctr > 0)
               FW.write(str6+"\n");

          Pctr++;

          String str3 = "Page No  : "+Pctr+" ";

          FW.write("P"+str1+"\n");
          FW.write(str2+"\n");
         // FW.write(str3+"M\n");
          FW.write(str3+"g\n");
          FW.write(str6+"\n");
          FW.write(str4+"\n");
          FW.write(str5+"\n");
          FW.write(str6+"\n");
          Lctr = 7;
          bBlank=false;
     }

     private void setBody(int i) throws Exception
     {
          String SDate    = common.parseDate((String)VRDCDate  .elementAt(i));
          String SNo      = (String)VRDCNo    .elementAt(i);
          String SDesc    = (String)VDesc     .elementAt(i);
          String SRemark  = (String)VRemark   .elementAt(i);
          String SQty     = common.getRound((String)VRDCQty   .elementAt(i),0);
          String SRecQty  = common.getRound((String)VRecQty   .elementAt(i),0);
          String SPending = common.getRound((String)VPending  .elementAt(i),0);
          String SDueDate = common.parseDate((String)VDueDate  .elementAt(i));
          String SDelay   = (String)RowData[i][10];               
          String SName    = (String)VSupName.elementAt(i);
          String SThro    = (String)VThro.elementAt(i);
          String str="";
          String SRDCStatus = (String)VRDCStatus.elementAt(i);
          if(!bBlank)
          {
               str = common.Pad(SDate,10)+common.Space(3)+common.Rad(SNo,10)+common.Space(3)+
                            common.Pad(SName,30)+common.Space(3)+  
                            common.Pad(SDesc,40)+common.Space(3)+common.Pad(SRemark,15)+common.Space(3)+
                            common.Pad(SThro,15)+common.Space(3)+  
                            common.Rad(SQty,6)+common.Space(3)+common.Rad(SRecQty,6)+common.Space(3)+
                            common.Rad(SPending,6)+common.Space(3)+common.Pad(SDueDate,10)+common.Space(3)+
                            common.Rad(SDelay,7)+common.Space(3)+common.Pad(SRDCStatus,30);
          }
          else
          {
               str = common.Pad("",10)+common.Space(3)+common.Rad("",10)+common.Space(3)+
                            common.Pad("",30)+common.Space(3)+  
                            common.Pad(SDesc,40)+common.Space(3)+common.Pad(SRemark,15)+common.Space(3)+
                            common.Pad(SThro,15)+common.Space(3)+  
                            common.Rad(SQty,6)+common.Space(3)+common.Rad(SRecQty,6)+common.Space(3)+
                            common.Rad(SPending,6)+common.Space(3)+common.Pad(SDueDate,10)+common.Space(3)+
                            common.Rad(SDelay,7)+common.Space(3)+common.Pad(SRDCStatus,30);
          }

          FW.write(str+"\n");
          Lctr++;
     }

     private void setOtherHead() throws Exception
     {
          if (Lctr < 60)
               return;

          String str1 = "Company : "+SMillName;
          String str2 = "Document : RDC Pending List Register (Other than Stores) "+TDate.toString();
          String str4 = common.Pad("RDC",10)+common.Space(3)+common.Rad("RDC",10)+common.Space(3)+
                        common.Pad("Supplier",30)+common.Space(3)+ 
                        common.Pad("Material Description",40)+common.Space(3)+common.Pad("Instructions",15)+common.Space(3)+
                        common.Pad("Thro'",15)+common.Space(3)+ 
                        common.Rad("RDC",6)+common.Space(3)+common.Rad("Recd",6)+common.Space(3)+
                        common.Rad("Pending",6)+common.Space(3)+common.Pad("Due Date",10)+common.Space(3)+
                        common.Rad("Delayed",7);

          String str5 = common.Pad("Date",10)+common.Space(3)+common.Rad("No",10)+common.Space(3)+
                        common.Space(30)+common.Space(3)+ 
                        common.Pad(" ",40)+common.Space(3)+common.Pad(" ",15)+common.Space(3)+
                        common.Space(15)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Rad("Qty",6)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Pad(" ",10)+common.Space(3)+
                        common.Rad("Days",7);
          str6 = common.Replicate("-",str5.length());

          if (Pctr > 0)
               FW.write(str6+"\n");

          Pctr++;

          String str3 = "Page No  : "+Pctr+" ";

          FW.write("P"+str1+"\n");
          FW.write(str2+"\n");
          // FW.write(str3+"M\n");
          FW.write(str3+"g\n");
          FW.write(str6+"\n");
          FW.write(str4+"\n");
          FW.write(str5+"\n");
          FW.write(str6+"\n");
          Lctr = 7;
          bBlank=false;
     }

     private void setOtherBody(int i) throws Exception
     {
          String SDate    = common.parseDate((String)VORDCDate  .elementAt(i));
          String SNo      = (String)VORDCNo    .elementAt(i);
          String SDesc    = (String)VODesc     .elementAt(i);
          String SRemark  = (String)VORemark   .elementAt(i);
          String SQty     = common.getRound((String)VORDCQty   .elementAt(i),0);
          String SRecQty  = common.getRound((String)VORecQty   .elementAt(i),0);
          String SPending = common.getRound((String)VOPending  .elementAt(i),0);
          String SDueDate = common.parseDate((String)VODueDate  .elementAt(i));

          String SName    = (String)VOSupName.elementAt(i);
          String SThro    = (String)VOThro.elementAt(i);

          String SDelay = "";
          try
          {
               SDelay  = common.getDateDiff(TDate.toString(),SDueDate);
          }
          catch(Exception ex)
          {
               SDelay = "";
          }

          String str="";
          if(!bBlank)
          {
               str = common.Pad(SDate,10)+common.Space(3)+common.Rad(SNo,10)+common.Space(3)+
                            common.Pad(SName,30)+common.Space(3)+  
                            common.Pad(SDesc,40)+common.Space(3)+common.Pad(SRemark,15)+common.Space(3)+
                            common.Pad(SThro,15)+common.Space(3)+  
                            common.Rad(SQty,6)+common.Space(3)+common.Rad(SRecQty,6)+common.Space(3)+
                            common.Rad(SPending,6)+common.Space(3)+common.Pad(SDueDate,10)+common.Space(3)+
                            common.Rad(SDelay,7);
          }
          else
          {
               str = common.Pad("",10)+common.Space(3)+common.Rad("",10)+common.Space(3)+
                            common.Pad("",30)+common.Space(3)+  
                            common.Pad(SDesc,40)+common.Space(3)+common.Pad(SRemark,15)+common.Space(3)+
                            common.Pad(SThro,15)+common.Space(3)+  
                            common.Rad(SQty,6)+common.Space(3)+common.Rad(SRecQty,6)+common.Space(3)+
                            common.Rad(SPending,6)+common.Space(3)+common.Pad(SDueDate,10)+common.Space(3)+
                            common.Rad(SDelay,7);
          }

          FW.write(str+"\n");
          Lctr++;
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               boolean bflag = setDataIntoVector();
               if(!bflag)
                    return;
               setVectorIntoRowData();
               try
               {
                    MiddlePanel.remove(tabreport);
                    MiddlePanel.updateUI();
               }
               catch(Exception ex){}
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               MiddlePanel.add("Center",tabreport);
               MiddlePanel.updateUI();
               BPrint.setEnabled(true);
               TFile.setEditable(true);
          }
     }
     private boolean setDataIntoVector()
     {
          boolean bflag = false;
          VRDCDate  = new Vector();
          VRDCNo    = new Vector();
          VSupName  = new Vector();
          VDesc     = new Vector();
          VRemark   = new Vector();
          VThro     = new Vector();
          VRDCQty   = new Vector();
          VRecQty   = new Vector();
          VPending  = new Vector();
          VDueDate  = new Vector();
          VAcCode   = new Vector();
          VRDCStatus = new Vector();
          String SDate = TDate.toNormal();
          VDelayDays  = new Vector();
          String QS = " Select RDC.RDCDate,RDC.RDCNo,"+SSupTable+".Name,Descript, "+
                      " RDC.Remarks,RDC.Thro,RDC.Qty,RDC.RecQty,RDC.Qty-RDC.RecQty,RDC.DueDate,RDC.Sup_Code,RDCStatus.Status "+
                      " From RDC Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = RDC.Sup_Code "+
                      " Left join RDCStatus on RDCStatus.RDCID = RDC.ID and RDCStatus.ActiveStatus=0"+
                      " Where RDC.RDCDate<='"+SDate+"' And RDC.RecQty < RDC.Qty "+
                      " And RDC.MillCode="+iMillCode+
                      " And RDC.AssetFlag=0  and DocType=0  ";


               if(!(JCAsset.getSelectedItem()).equals("ALL"))
                    QS = QS + " and RDC.GodownAsset="+JCAsset.getSelectedIndex();


               QS = QS + " Order By 1,2 ";

           try
           {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                     
                       String SDelayDays  = common.getDateDiff(TDate.toString(),common.parseDate(res.getString(1)));

                     if(JCFilter.getSelectedIndex()==1)
                          if(common.toInt(SDelayDays) >30)
                          continue;
                     if(JCFilter.getSelectedIndex()==2 )
                        if(  common.toInt(SDelayDays) <=30)
                         continue;



                    VRDCDate  .addElement(""+res.getString(1));
                    VRDCNo    .addElement(""+res.getString(2));
                    VSupName  .addElement(""+res.getString(3));
                    VDesc     .addElement(""+res.getString(4));
                    VRemark   .addElement(""+res.getString(5));
                    VThro     .addElement(""+res.getString(6));
                    VRDCQty   .addElement(""+res.getString(7));
                    VRecQty   .addElement(""+res.getString(8));
                    VPending  .addElement(""+res.getString(9));
                    VDueDate  .addElement(""+res.getString(10));
                    VAcCode   .addElement(""+res.getString(11));
                    VRDCStatus . addElement(""+common.parseNull(res.getString(12)));
                 //  String SDelayDays  = common.getDateDiff(TDate.toString(),common.parseDate(res.getString(1)));

                    VDelayDays.addElement(common.parseNull(SDelayDays));

                  

               }
               res.close();
               stat.close();
               bflag = true;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bflag = false;
          }
          return bflag;
     }
     private void setVectorIntoRowData()
     {
          RowData = new Object[VRDCNo.size()][12];
          String SDueDate="",SDelay="";
           String SDays="";
           
           

          for(int i=0;i<VRDCDate.size();i++)
          {

             
               //calc delay days
               
               SDays=(common.parseNull((String)VDelayDays  .elementAt(i)));
              
               RowData[i][0]  = common.parseDate(common.parseNull((String)VRDCDate  .elementAt(i)));
               RowData[i][1]  = common.parseNull((String)VRDCNo    .elementAt(i));
               RowData[i][2]  =common.parseNull( (String)VSupName  .elementAt(i));
               RowData[i][3]  =common.parseNull( (String)VDesc     .elementAt(i));
               RowData[i][4]  = common.parseNull((String)VRemark   .elementAt(i));
               RowData[i][5]  =common.parseNull( (String)VThro     .elementAt(i));
               RowData[i][6]  =common.parseNull( (String)VRDCQty   .elementAt(i));
               RowData[i][7]  =common.parseNull( (String)VRecQty   .elementAt(i));
               RowData[i][8]  =common.parseNull( (String)VPending  .elementAt(i));
               
               try
               {
                    SDueDate = common.parseDate((String)VDueDate  .elementAt(i));
               }
               catch(Exception ex)
               {
                    SDueDate = "";
               }
             
               RowData[i][9]  = SDueDate;
              
             
              // RowData[i][10] = common.toInt(SDays);
              
                RowData[i][10] = SDays;
              
             
               RowData[i][11] = (String)VRDCStatus.elementAt(i);
             
            
            
             
          }
     }

     private void setOtherVector()
     {
          VORDCDate  = new Vector();
          VORDCNo    = new Vector();
          VOSupName  = new Vector();
          VODesc     = new Vector();
          VORemark   = new Vector();
          VOThro     = new Vector();
          VORDCQty   = new Vector();
          VORecQty   = new Vector();
          VOPending  = new Vector();
          VODueDate  = new Vector();
          VOAcCode   = new Vector();

          String SDate = TDate.toNormal();

          String QS = " Select SamplesDC.DCDate,SamplesDC.DCNo,PartyMaster.PartyName,SamplesDC.Description, "+
                      " ''  as Remarks,SamplesDC.TruckNo,SamplesDC.Qty,0 as RecQty, "+
                      " SamplesDC.Qty as PendQty,SamplesDC.DCDate as DueDate,SamplesDC.PartyCode "+
                      " From SamplesDC "+
                      " Inner Join PartyMaster On PartyMaster.PartyCode = SamplesDC.PartyCode "+
                      " And SamplesDC.DCDate<='"+SDate+"'"+
                      " And SamplesDC.DCType=1 and SamplesDC.RetStatus=0 "+
                      " Order By 1,2 ";

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    VORDCDate  .addElement(""+res.getString(1));
                    VORDCNo    .addElement(""+res.getString(2));
                    VOSupName  .addElement(""+res.getString(3));
                    VODesc     .addElement(""+res.getString(4));
                    VORemark   .addElement(""+res.getString(5));
                    VOThro     .addElement(""+res.getString(6));
                    VORDCQty   .addElement(""+res.getString(7));
                    VORecQty   .addElement(""+res.getString(8));
                    VOPending  .addElement(""+res.getString(9));
                    VODueDate  .addElement(""+res.getString(10));
                    VOAcCode   .addElement(""+res.getString(11));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

private void createPDFFile() {
        try {
           // String PDFFile = "D://DepoShadePrint.pdf";
            document = new Document(PageSize.A4.rotate());
            PdfWriter.getInstance(document, new FileOutputStream(PDFFile));
            document.open();
            int iColumnCount = tabreport.ReportTable.getColumnCount();
            System.out.println("Inside CreatePDF -->" + iColumnCount);

            table = new PdfPTable(tabreport.ReportTable.getColumnCount());
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            table.setHeaderRows(1);
            table.getRowHeight(10);


            addHead(document, table);
            addBody(document, table);

          String SDateTime = common.getServerDateTime2();
          String SEnd = "Report Taken on "+SDateTime+" ";

           Paragraph paragraph;

            String Str1 = SEnd;
            //String Str2   = "Deduction Details";

            paragraph = new Paragraph(Str1, bigbold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            paragraph.setSpacingAfter(10);
            document.add(paragraph);
            document.close();
            JOptionPane.showMessageDialog(null, "PDF File Created in " + PDFFile, "Info", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addHead(Document document, PdfPTable table) throws BadElementException {
        try {
/*
            String SStDate = TopPanel.TStDate.toNormal();
          String SEnDate = TopPanel.TEnDate.toNormal();
            String List   = (String)TopPanel.JCList.getSelectedItem();  */        

            document.newPage();
            Paragraph paragraph;

            String Str1 = "Company : AMARJOTHI SPINNING MILLS LTD ";
            //String Str2   = "Deduction Details";

            paragraph = new Paragraph(Str1, bigbold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            paragraph.setSpacingAfter(10);
            document.add(paragraph);//Document : RDC List 
            
            String Str2 = "Document : RDC Pending List Register "+TDate.toString()+"";
            //String Str2   = "Deduction Details";

            paragraph = new Paragraph(Str2, bigbold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            paragraph.setSpacingAfter(10);
            document.add(paragraph);
            

	/*	AddCellIntoTable("AMARJOTHI SPINNING MILLS LTD", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 12, 0, 0, 0, 0, smallbold);
		AddCellIntoTable("Document : RDC List  ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 12, 0, 0, 0, 0, smallbold);  
		AddCellIntoTable("Period   : "+common.parseDate(SStDate)+"-"+common.parseDate(SEnDate), table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 12, 0, 0, 0, 0, smallbold);
		*/	

            PdfPCell c1;


            for (int i = 0; i < tabreport.ReportTable.getColumnCount(); i++) {
                c1 = new PdfPCell(new Phrase(tabreport.ReportTable.getColumnName(i), mediumbold));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                c1.setRowspan(1);
                table.addCell(c1);
            }
            Lctr = 16;
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    private void addBody(Document document, PdfPTable table) throws BadElementException {
        try {
            Paragraph paragraph;
            PdfPCell c1;
         

            for (int i = 0; i < tabreport.ReportTable.getRowCount(); i++) {

                for (int j = 0; j < tabreport.ReportTable.getColumnCount(); j++) {
                    c1 = new PdfPCell(new Phrase((String) tabreport.ReportTable.getValueAt(i, j), smallnormal));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                    table.addCell(c1);
                }
            }


            document.add(table);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("ex" + ex);
        }
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
}
