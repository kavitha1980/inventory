package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCDeletionFrame extends JInternalFrame
{
          
     JPanel    TopPanel;
     JPanel    BottomPanel;

     JButton   BApply,BDelete;
     NextField TRDCNo;
    
     Object RowData[][];
     String ColumnData[] = {"Doc Type","RDC Type","RDC No","Date","Supplier","Description","Purpose","Qty","Delete"};
     String ColumnType[] = {"S"       ,"S"       ,"N"     ,"S"   ,"S"       ,"S"          ,"S"      ,"N"  ,"B"     };

     JLayeredPane DeskTop;
     StatusPanel  SPanel;
     int          iUserCode;
     int          iMillCode;
     String       SSupTable;

     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;

     Vector VDocType,VRDCType,VRDCDate,VRDCNo,VSupName,VDesc,VPurpose,VQty,VId;

     TabReport tabreport;
     
     RDCDeletionFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable)
     {
          super("RDC Deletion");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     public void createComponents()
     {
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          TRDCNo   = new NextField();
          BApply   = new JButton("Apply");
          BDelete  = new JButton("Delete Marked RDC");

          BApply.setMnemonic('A');
          BDelete.setEnabled(false);
     }

     public void setLayouts()
     {    
          TopPanel.setLayout(new GridLayout(1,3));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("RDC No"));
          TopPanel.add(TRDCNo);
          TopPanel.add(BApply);

          BottomPanel.add(BDelete);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
         BApply.addActionListener(new ApplyList());
         BDelete.addActionListener(new DeleteList());
     }
     public class DeleteList implements ActionListener
     {
         public void actionPerformed(ActionEvent ae)
         {
               BDelete.setEnabled(false);
               deleteRecords();
               removeHelpFrame();
         }

     }
     private void deleteRecords()
     {
          String QS="";

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               String SSysName = common.getLocalHostName();
               String SCurrentDate=common.getCurrentDate();
               String SServerDateTime=common.getServerDateTime();
              
               for(int i=0;i<RowData.length;i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][8];

                    if(Bselected.booleanValue())
                    {
                         QS = "Update RDC Set DM=9 ,DELETEDDATE="+SCurrentDate+",DELETEDDATETIME = '"+SServerDateTime+"', DELETEDSYSNAME='"+SSysName+"' Where Id="+(String)VId.elementAt(i);
     
                         try
                         {
                              stat.execute(QS);
                         }
                         catch(Exception ex)
                         {
                              JOptionPane.showMessageDialog(null,"Problem in Deletion","Attention",JOptionPane.INFORMATION_MESSAGE);
                              return;
                         }
                    }
               }
               try
               {
                    QS = "Insert Into DeletedRDC Select RDC.* From RDC Where RDC.DM=9";
                    stat.execute(QS);
               }
               catch(Exception ex)
               {
                         JOptionPane.showMessageDialog(null,"Problem in Deletion","Attention",JOptionPane.INFORMATION_MESSAGE);
                         ex.printStackTrace();
                         return;
               }
               try
               {
                    QS = "Delete From RDC Where DM=9";
                    stat.execute(QS);
               }
               catch(Exception ex)
               {
                         JOptionPane.showMessageDialog(null,"Problem in Deletion","Attention",JOptionPane.INFORMATION_MESSAGE);
                         ex.printStackTrace();
                         return;
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void removeHelpFrame()
     {
         try
         {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
         }
         catch(Exception ex){}

     }
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setTabReport();
          }
     }
     public void setTabReport()
     {
          if(common.toInt(TRDCNo.getText())==0)
          {
               JOptionPane.showMessageDialog(null,"Invalid RDC No","Error Message",JOptionPane.INFORMATION_MESSAGE);
               TRDCNo.requestFocus();
               BDelete.setEnabled(false);
               return;
          }

          setDataIntoVector();
          setRowData();
          try
          {
             getContentPane().remove(tabreport);
          }
          catch(Exception ex){}

          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             getContentPane().add(tabreport,BorderLayout.CENTER);
             setSelected(true);
             DeskTop.repaint();
             DeskTop.updateUI();
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }
          if(RowData.length>0)
          {
               BDelete.setEnabled(true);
          }
          else
          {
               BDelete.setEnabled(false);
          }

     }
     public void setDataIntoVector()
     {
          VDocType     = new Vector();
          VRDCType     = new Vector();
          VRDCDate     = new Vector();
          VRDCNo       = new Vector();
          VSupName     = new Vector();
          VDesc        = new Vector();
          VPurpose     = new Vector();
          VQty         = new Vector();
          VId          = new Vector();

          String QString = "";
               
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               QString = getQString();

               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    int iAuth = common.toInt(res.getString(10));
                    if(iAuth>0)
                    {
                         JOptionPane.showMessageDialog(null,"Materials Already sent for this RDC","Error Message",JOptionPane.INFORMATION_MESSAGE);
                         TRDCNo.setText("");
                         TRDCNo.requestFocus();
                         return;
                    }

                    int iDocType = res.getInt(1);
                    int iRdcType = res.getInt(2);

                    if(iDocType==0)
                    {
                         VDocType.addElement("RDC");
                    }
                    else
                    {
                         VDocType.addElement("NRDC");
                    }

                    if(iRdcType==0)
                    {
                         VRDCType.addElement("Regular");
                    }
                    else
                    {
                         VRDCType.addElement("Non Regular");
                    }

                    VRDCNo.addElement(res.getString(3));
                    VRDCDate.addElement(common.parseDate(res.getString(4)));
                    VSupName.addElement(res.getString(5));
                    VDesc.addElement(common.parseNull(res.getString(6)));
                    VPurpose.addElement(common.parseNull(res.getString(7)));
                    VQty.addElement(res.getString(8));
                    VId.addElement(res.getString(9));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex){System.out.println(ex);}
     }
     public void setRowData()
     {
         RowData     = new Object[VRDCDate.size()][ColumnData.length];
         for(int i=0;i<VRDCDate.size();i++)
         {
               RowData[i][0]  = (String)VDocType.elementAt(i);
               RowData[i][1]  = (String)VRDCType.elementAt(i);
               RowData[i][2]  = (String)VRDCNo.elementAt(i);
               RowData[i][3]  = (String)VRDCDate.elementAt(i);
               RowData[i][4]  = (String)VSupName.elementAt(i);
               RowData[i][5]  = (String)VDesc.elementAt(i);
               RowData[i][6]  = (String)VPurpose.elementAt(i);
               RowData[i][7]  = (String)VQty.elementAt(i);
               RowData[i][8]  = new Boolean(false);
        }  
     }
     public String getQString()
     {
          String SRDCNo = TRDCNo.getText();

          String QString  = " SELECT RDC.DocType, RDC.RDCType, RDC.RDCNo, RDC.RDCDate,"+
                            " "+SSupTable+".Name, RDC.Descript, RDC.Remarks, RDC.Qty, "+
							// comment by ananthi 10.02.2021  
                           // " RDC.Id, RDC.GateAuth "+
						    " RDC.Id, RDC.GateStatus "+
                            " FROM RDC "+
                            " INNER JOIN "+SSupTable+" ON RDC.Sup_Code="+SSupTable+".Ac_Code "+
                            " Where RDC.RDCNo="+SRDCNo+
                            " And RDC.MillCode="+iMillCode;

          return QString;
     }
}
