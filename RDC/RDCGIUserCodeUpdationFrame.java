package RDC;

import util.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.Vector;
import java.sql.*;
import java.awt.event.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCGIUserCodeUpdationFrame extends JInternalFrame
{
     JPanel TopPanel,MiddlePanel,BottomPanel;
     String mac      = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
     String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

     Common common   = new Common();

     Vector VGINo,VRdcNo,VRdcSlNo,VMillCode,VId;

     JTextArea     TInfo;

     JButton   BApply;
     JLayeredPane layer;

     Connection theConnection=null;

     public RDCGIUserCodeUpdationFrame(JLayeredPane layer)
     {
          super("Utility for Updating RDC Gateinward UserCode");
          this.layer      = layer;

          updateLookAndFeel();
          setSize(500,500);
          
          TopPanel    = new JPanel(true);
          MiddlePanel = new JPanel(true);
          MiddlePanel.setLayout(new BorderLayout());

          TInfo       = new JTextArea(50,50);
          BApply      = new JButton("Apply");

          TopPanel.add(BApply);
          MiddlePanel.add(TInfo);

          BApply.addActionListener(new AppList());

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          setVisible(true);
          setClosable(true);
     }
      private class AppList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {

               BApply.setEnabled(false);
               try
               {
                    MiddlePanel.removeAll();
               }catch(Exception ex){System.out.println(ex);}
               MiddlePanel.add(TInfo);
               TInfo.append("Processing ....."+"\n");

               TInfo.append("Fetching Stock Data ....."+"\n");
               getRDCData();

               if(VRdcNo.size()>0)
               {
                    TInfo.append("Updation is going on ....."+"\n");
                    updateRDCData();
               }

               TInfo.append("Updation Over .....");
               removeFrame();
          }
      }

     private void getRDCData()
     {
          VGINo         = new Vector();
          VRdcNo        = new Vector();
          VRdcSlNo      = new Vector();
          VMillCode     = new Vector();
          VId           = new Vector();


          String QS = " Select GINo,RdcNo,RdcSlNo,MillCode,Id from GateInwardRDC Where GIDate>=20110401 "+
                      " Order by MillCode,RdcNo,Id ";

          try
          {

               if(theConnection==null)
               {
                    ORAConnection jdbc       = ORAConnection.getORAConnection();
                    theConnection            = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    VGINo.addElement(common.parseNull((String)theResult.getString(1)));
                    VRdcNo.addElement(common.parseNull((String)theResult.getString(2)));
                    VRdcSlNo.addElement(common.parseNull((String)theResult.getString(3)));
                    VMillCode.addElement(common.parseNull((String)theResult.getString(4)));
                    VId.addElement(common.parseNull((String)theResult.getString(5)));
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void updateRDCData()
     {
          try
          {
               if(theConnection==null)
               {
                    ORAConnection  jdbc           = ORAConnection.getORAConnection();
                    theConnection                 = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();

               for(int i=0;i<VRdcNo.size();i++)
               {
                    String SGINo     = (String)VGINo.elementAt(i);
                    String SRdcNo    = (String)VRdcNo.elementAt(i);
                    String SRdcSlNo  = (String)VRdcSlNo.elementAt(i);
                    String SMillCode = (String)VMillCode.elementAt(i);
                    String SId       = (String)VId.elementAt(i);

                    String SMemoUserCode = getMemoUserCode(stat,SRdcNo,SRdcSlNo,SMillCode);

                    String QS = " Update GateInwardRDC Set MemoAuthUserCode="+SMemoUserCode+
                                " Where GINo="+SGINo+
                                " and RdcNo="+SRdcNo+
                                " and RdcSlNo="+SRdcSlNo+
                                " and MillCode="+SMillCode+
                                " and Id="+SId;

                    stat.execute(QS);
               }
               stat.close();
               JOptionPane.showMessageDialog(null,VRdcNo.size()+" Rows updated ");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               JOptionPane.showMessageDialog(null,"Updation Error:"+ex);
               removeFrame();
          }

     }

     private String getMemoUserCode(Statement stat,String SRdcNo,String SRdcSlNo,String SMillCode)
     {

          String SMemoUserCode = "1";

          try
          {
               String QS = " Select MemoAuthUserCode from RDC "+
                           " Where RdcNo="+SRdcNo+
                           " and SlNo="+SRdcSlNo+
                           " and MillCode="+SMillCode;

               ResultSet theResult      = stat.executeQuery(QS);
               while(theResult.next())
               {
                    SMemoUserCode = common.parseNull((String)theResult.getString(1));
               }
               theResult.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

          if(common.toInt(SMemoUserCode)<=0)
          {
               SMemoUserCode = "1";
          }
          return SMemoUserCode;
     }

     private void removeFrame()
     {
          layer.remove(this);
          layer.repaint();
          layer.updateUI();
     }
     private void updateLookAndFeel()
     {
          try
          {
               UIManager.setLookAndFeel(windows);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }                    

     }
     
}



