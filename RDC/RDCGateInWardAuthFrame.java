import util.*;
import guiutil.*;
import jdbc.JDBCConnection1;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import javax.swing.border.*;
import java.util.Date;
 
public class RDCGateInWardAuthFrame extends JInternalFrame
{
	JPanel    				TopPanel,MiddlePanel,BottomPanel 
	JButton           		BSave,BExit;

	JLayeredPane      		theLayer;
	JTable            		theTable;
	RDCGateInWardAuthModel	theModel;
	Common            		common = new Common();
	Connection				theconnect;
	JLabel					LTitle;

	public RDCGateInWardAuthFrame(JLayeredPane theLayer,int iUserCode)
    {
    	try
        {
			this.theLayer = theLayer;
			this.iUserCode = iUserCode;

			createCompenents();
			setLayouts();
			addCompenents();
			addListeners();
			SetVector();
		}
        catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e);
		}
	}     
	public void createCompenents()
	{                 
		LTitle				= new JLabel();    
		TopPanel          	= new JPanel();
		MiddlePanel       	= new JPanel();
		BottomPanel			= new JPanel();

		theModel       		= new RDCGateInWardAuthModel();
		theTable       		= new JTable(theModel);

		BSave          		= new JButton("Authentication");
		BExit          		= new JButton("Exit");
		
	}
    private void setLayouts()
    {
		setTitle("RDC GateInWard Authentication");
		setMaximizable(true);
		setClosable(true);
		setResizable(true);
		setIconifiable(true);
		setSize(790,490);
		show();

		TopPanel.setBorder(new TitledBorder("Title"));
		MiddlePanel.setBorder(new TitledBorder("DataList"));
		BottomPanel.setBorder(new TitledBorder("Controls"));

		TopPanel.setLayout(new BorderLayout());
		MiddlePanel.setLayout(new BorderLayout());
		BottomPanel.setLayout(new FlowLayout());
	}
	private void addCompenents()
	{
		BSave.setMnemonic('S');
		BExit.setMnemonic('E');

		LTitle = new JLable("RDC GateInWard Authentication Screen");

		TopPanel.add(LTitle);
		MiddlePanel.add(new JScrollPane(theTable));

		BottomPanel.add(BSave);
		BottomPanel.add(BExit);

		getContentPane().add("North",TopPanel);
		getContentPane().add("Center",MiddlePanel);
		getContentPane().add("South",BottomPanel);
	}
    private void addListeners()
    {
    	BSave .addActionListener(new ActionList(this));
        BExit .addActionListener(new ActionList(this));
	}
	
	private class ActionList implements ActionListener
    {
		public void actionPerformed(ActionEvent ae)
        {
        
			if(ae.getSource()==BExit)
			{
				removeHelpFrame();
			}
			if(ae.getSource()==BSave)
			{
				UpdateRDCData();
				SetVector();
			}
		}
	}
    private void UpdateRDCData()
    {
		for(int i=0;i<theModel.getRows();i++)
		{
		}

	}
    private void SetVector()
	{
		try
		{       
			theModel.setNumRows(0);
			SetRDCDetails();
		
			for(int i=0;i<ARDCList.size();i++)
            {
            	HashMap theMap           = (HashMap)ARDCList.get(i);

                Vector theVect           = new Vector();

                theVect                  . addElement(String.valueOf(i+1));
                theVect                  . addElement(common.parseNull((String)theMap.get("RDCNO")));
				theVect                  . addElement(common.parseDate((String)theMap.get("RDCDATE")));
                theVect                  . addElement(common.parseNull((String)theMap.get("GINO")));
				theVect                  . addElement(common.parseDate((String)theMap.get("GIDATE")));
                theVect                  . addElement(common.parseNull((String)theMap.get("ITEMNAME")));
                theVect                  . addElement(common.parseNull((String)theMap.get("RECQTY")));
                theVect                  . addElement(new Boolean(false));

                theModel                 . addRow(theVect);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			System.out.println(ex);
		}

	}
	private void SetRDCDetails()
	{
		ARDCList = new ArrayList();

		try
		{
			if(theconnect==null)
			{
				connect=JDBCConnection1.getJDBCConnection1();
				theconnect=connect.getConnection();
			}
			Statement theStatement   =theconnect.createStatement();
			ResultSet res = theStatement.executeQuery(getRDCQS());
			while(res.next())
			{
				HashMap theMap = new HashMap();

				theMap.put("RDCNO",res.getString(1));
				theMap.put("RDCDATE",res.getString(2));
				theMap.put("GINO",res.getString(3));
				theMap.put("GIDATE",res.getString(4));
				theMap.put("RECQTY",res.getString(5));
				theMap.put("ITEMNAME",res.getString(6));
				theMap.put("ITEMCODE",res.getString(7));

				ARDCList.add(theMap);
			}
			res.close();
			theStatement.close();
		}               
		catch(Exception ex)
		{
			ex.printStackTrace();
			System.out.println(ex);
		}
	}
	private String getRDCQS()
	{
		String  QS = " Select RDC.RDCNo,RDC.RDCDate,GateInwardRDC.GiNo,GateInwardRDC.GiDate "+
		" ,InvItem.ItemName,GateInwardRDC.ItemCode "+
		" ,GateInwardRDC.RecQty "+
		" from RDC "+
		" Inner join GateInwardRDC on GateInwardRDC.RDCNo = RDC.RDCNo "+
		" and RDC.HSNMATCODE = GateInwardRDC.ItemCode "+
		" and GateInwardRDC.AuthStatus = 0 "+
		" Inner join InvItems on InvItems.ItemCode = GateInwardRDC.ItemCode ";

		System.out.println("QS :"+QS);

		return QS;
	}
   	private void removeHelpFrame()
	{
		if(JOptionPane.showConfirmDialog(null,"Do You Want To Exit","Exit",JOptionPane.YES_NO_OPTION)==0)
		{
			try
			{
				theLayer.remove(this);
				theLayer.repaint();
				theLayer.updateUI();
			}
			catch(Exception ex)
			{
				System.out.println(ex);
			}
		}
		else
			return;
	}
}

