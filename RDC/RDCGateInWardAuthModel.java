import util.*;
import guiutil.*;

import java.awt.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import java.sql.*;

public class RDCGateInwardAuthModel extends DefaultTableModel 
{

     String ColumnName[] = {"RDCNO","RDCDATE","GINO","GIDATE","ITEM NAME","REC QTY","SELECT"};
     String ColumnType[] = {"S"    ,"S"      ,"S"   ,"S"     ,"S"        ,"E"      ,"B"     };  

     public RDCGateInwardAuthModel()
     {
          setDataVector(getRowData(),ColumnName);
     }
     public Object[][] getRowData()
     {
          Object RowData[][] = new Object[0][ColumnName.length];
          return RowData;
     }

     public boolean isCellEditable(int row,int col)
     {
		if(ColumnType[col]=="B" || ColumnType[col]=="E")
               return true;
          return false;
     }
	
     public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }
     
     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
          }
          catch(Exception ex)
          {
               System.out.println("from Model : "+ex);
          }

     }
     public int getRows()
     {
          return super.dataVector.size();
     }

     public void appendEmptyRow()
     {
          Vector temp = new Vector();

          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");


          insertRow(getRows(),temp);
     }
     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }
  
     
}                       
