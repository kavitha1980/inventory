package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCInspectionFrame extends JInternalFrame
{
      JPanel     TopPanel;
      JPanel     MiddlePanel;
      JPanel     BottomPanel;
 
      String     SDate;
      JButton    BApply,BOk;
      DateField  TDate;

      JLayeredPane DeskTop;
      StatusPanel SPanel;
      int iMillCode;
      String SSupTable;

      Common common = new Common();

      Connection     theConnection = null;

      TabReport tabreport;

      Vector VSupName,VRDCNo,VDescript,VGINo,VGIDate,VRecQty,VId;

      String ColumnData[] = {"Supplier","RDC No","Descript","Recd Date","Recd Qty","Accepted Qty","Select"};
      String ColumnType[] = {"S"       ,"N"     ,"S"       ,"S"        ,"N"       ,"E"           ,"B"     };
      Object RowData[][];

      boolean bComflag = true;

      RDCInspectionFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode,String SSupTable)
      {
          super("RDC Recd and Pending For Inspection As On ");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;


          ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                          theConnection =  oraConnection.getConnection();

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
      }

      public void createComponents()
      {
          TopPanel    = new JPanel();
          MiddlePanel = new JPanel();
          BottomPanel = new JPanel();
 
          TDate    = new DateField();
          BApply   = new JButton("Apply");
          BOk      = new JButton("Save");
 
          TDate.setTodayDate();
          BOk.setEnabled(false);
      }

      public void setLayouts()
      {
          TopPanel.setLayout(new GridLayout(2,3));
          MiddlePanel.setLayout(new BorderLayout());
            
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
      }
                                                                        
      public void addComponents()
      {
          TopPanel.add(new JLabel("As On "));
          TopPanel.add(TDate);
          TopPanel.add(BApply);
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));

          BottomPanel.add(BOk);
 
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
      }
      public void addListeners()
      {
          BApply.addActionListener(new ApplyList());
          BOk.addActionListener(new ActList());
      }
      public class ApplyList implements ActionListener
      {
           public void actionPerformed(ActionEvent ae)
           {
                setDataIntoVector();
                setVectorIntoRowData();
                try
                {
                      MiddlePanel.remove(tabreport);
                }
                catch(Exception ex){}
 
                tabreport = new TabReport(RowData,ColumnData,ColumnType);
                tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
                MiddlePanel.add("Center",tabreport);
 
                if(RowData.length>0)
                {
                     BOk.setEnabled(true);
                     BApply.setEnabled(false);
                }
                else
                {
                     BOk.setEnabled(false);
                     BApply.setEnabled(true);
                }
 
                MiddlePanel.updateUI();
           }
      }

      public class ActList implements ActionListener
      {
           public void actionPerformed(ActionEvent ae)
           {
                BOk.setEnabled(false);
                updateInspection();
                getACommit();
                removeHelpFrame();
           }
      }

      public void removeHelpFrame()
      {
            try
            {
                  DeskTop.remove(this);
                  DeskTop.repaint();
                  DeskTop.updateUI();
            }
            catch(Exception ex) { }
      }

      private void setDataIntoVector()
      {
            VSupName       = new Vector();
            VRDCNo         = new Vector();
            VDescript      = new Vector();
            VGINo          = new Vector();
            VGIDate        = new Vector();
            VRecQty        = new Vector();
            VId            = new Vector();

            String QS  = "";

            QS  = " SELECT "+SSupTable+".NAME, GateInwardRDC.RDCNo, GateInwardRDC.Descript, GateInwardRDC.GINo, "+
                  " GateInwardRDC.GIDate, GateInwardRDC.RecQty, GateInwardRDC.Id "+
                  " FROM GateInwardRDC INNER JOIN "+SSupTable+" ON GateInwardRDC.Sup_Code = "+SSupTable+".AC_CODE "+
                  " And "+SSupTable+".RDCFlag=1 And GateInwardRDC.GIDate <= '"+TDate.toNormal()+"' And GateInwardRDC.Inspection=0 And GateInwardRDC.MillCode="+iMillCode+
                  " ORDER BY "+SSupTable+".NAME,GateInwardRDC.GIDate,GateInwardRDC.RDCNo,GateInwardRDC.Descript ";

            try
            {
                  Statement stat = theConnection.createStatement();
                  ResultSet theResult = stat.executeQuery(QS);
                  while(theResult.next())
                  {
                        VSupName       .addElement(theResult.getString(1));
                        VRDCNo         .addElement(theResult.getString(2));
                        VDescript      .addElement(theResult.getString(3));
                        VGINo          .addElement(theResult.getString(4));
                        VGIDate        .addElement(theResult.getString(5));
                        VRecQty        .addElement(theResult.getString(6));
                        VId            .addElement(theResult.getString(7));
                  }
                  theResult.close();
                  stat.close();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      private void setVectorIntoRowData()
      {
            RowData = new Object[VSupName.size()][ColumnData.length];
            for(int i=0;i<VSupName.size();i++)
            {
                  RowData[i][0] = (String)VSupName.elementAt(i);
                  RowData[i][1] = (String)VRDCNo.elementAt(i);
                  RowData[i][2] = (String)VDescript.elementAt(i);
                  RowData[i][3] = common.parseDate((String)VGIDate.elementAt(i));
                  RowData[i][4] = (String)VRecQty.elementAt(i);
                  RowData[i][5] = (String)VRecQty.elementAt(i);
                  RowData[i][6] = new Boolean(false);
            }
      }

      public void updateInspection()
      {
           try
           {                                                          
                Statement stat = theConnection.createStatement();
                
                for(int i=0;i<VId.size();i++)
                {
                     Boolean Bselected = (Boolean)RowData[i][6];
                     if(Bselected.booleanValue())
                     {
                          String SId    = (String)VId.elementAt(i);
                          String SRDCNo = (String)VRDCNo.elementAt(i);
                          String SGINo  = (String)VGINo.elementAt(i);

                          String QString = "Update GateInwardRDC Set ";
                          QString = QString+" Inspection  = 1,AccQty="+(String)RowData[i][5]+" ";
                          QString = QString+" Where Id = "+SId+" and RDCNo="+SRDCNo+" and GINo="+SGINo+" and MillCode="+iMillCode;
                     
                          if(theConnection   . getAutoCommit())
                               theConnection   . setAutoCommit(false);
 
                          stat.execute(QString);
                     }
                }
                stat.close();
           }
           catch(Exception e)
           {
                System.out.println(e);
                bComflag  = false;
           }
      }
 
      private void getACommit()
      {
           try
           {
                if(bComflag)
                {
                     theConnection  . commit();
                     JOptionPane    . showMessageDialog(null,"The Entered Data is Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                     System         . out.println("Commit");
                }
                else
                {
                     theConnection  . rollback();
                     JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                     System         . out.println("RollBack");
                }
                theConnection   . setAutoCommit(true);
           }catch(Exception ex)
           {
                ex.printStackTrace();
           }
      }


}
