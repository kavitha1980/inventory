package RDC;

import java.io.*;
import util.*;
import java.util.*;

public class RDCInstruction
{
     String SRdcNo,SRdcDate,SRefNo,SRefDate,SInwNo,SInwDate,SBillNo,SBillDate,SThro,SPerson,SName,SAddress1,SAddress2,SAddress3,SPlace,SGiNo,SGiDate,SDocType,SMBDNo,SUom,SMBDDate,SPhoneNum,SAsset;
     Vector VDescript,VQty,VRemarks,VUom,VRDCRate,VRDCValue,VHsnCode,VItemName,VItemCode;
     Common common = new Common();

     public RDCInstruction(String SRdcNo,String SRdcDate,String SRefNo,String SRefDate,String SInwNo,String SInwDate,String SBillNo,String SBillDate,String SThro,String SPerson,String SName,String SAddress1,String SAddress2,String SAddress3,String SPlace,String SGiNo,String SGiDate,String SDocType,String SMBDNo,String SUom,String SMBDDate,String SPhoneNum)
     {                   
          this.SRdcNo     = SRdcNo;
          this.SRdcDate   = SRdcDate;
          this.SRefNo     = SRefNo;
          this.SRefDate   = SRefDate;
          this.SInwNo     = SInwNo;
          this.SInwDate   = SInwDate;
          this.SBillNo    = SBillNo;
          this.SBillDate  = SBillDate;
          this.SThro      = SThro;
          this.SPerson    = SPerson;
          this.SName      = SName;
          this.SAddress1  = SAddress1;
          this.SAddress2  = SAddress2;
          this.SAddress3  = SAddress3;
          this.SPlace     = SPlace;
          this.SGiNo      = SGiNo;
          this.SGiDate    = SGiDate;
          this.SDocType   = SDocType;
          this.SMBDNo     = SMBDNo;
          this.SUom       = SUom;
          this.SMBDDate   = SMBDDate;
          this.SPhoneNum  = SPhoneNum;
		 

          VDescript       = new Vector();
          VQty            = new Vector();
          VRemarks        = new Vector();
          VUom            = new Vector();
          VRDCRate         = new Vector();
          VRDCValue        = new Vector(); 
          VHsnCode         = new Vector(); 
          VItemName         = new Vector();
          VItemCode       = new Vector();

     }  

 public RDCInstruction(String SRdcNo,String SRdcDate,String SRefNo,String SRefDate,String SInwNo,String SInwDate,String SBillNo,String SBillDate,String SThro,String SPerson,String SName,String SAddress1,String SAddress2,String SAddress3,String SPlace,String SGiNo,String SGiDate,String SDocType,String SMBDNo,String SUom,String SMBDDate,String SPhoneNum,String SAsset)
     {                   
          this.SRdcNo     = SRdcNo;
          this.SRdcDate   = SRdcDate;
          this.SRefNo     = SRefNo;
          this.SRefDate   = SRefDate;
          this.SInwNo     = SInwNo;
          this.SInwDate   = SInwDate;
          this.SBillNo    = SBillNo;
          this.SBillDate  = SBillDate;
          this.SThro      = SThro;
          this.SPerson    = SPerson;
          this.SName      = SName;
          this.SAddress1  = SAddress1;
          this.SAddress2  = SAddress2;
          this.SAddress3  = SAddress3;
          this.SPlace     = SPlace;
          this.SGiNo      = SGiNo;
          this.SGiDate    = SGiDate;
          this.SDocType   = SDocType;
          this.SMBDNo     = SMBDNo;
          this.SUom       = SUom;
          this.SMBDDate   = SMBDDate;
          this.SPhoneNum  = SPhoneNum;
		  this.SAsset	  = SAsset;

          VDescript       = new Vector();
          VQty            = new Vector();
          VRemarks        = new Vector();
          VUom            = new Vector();
          VRDCRate         = new Vector();
          VRDCValue        = new Vector(); 
          VHsnCode         = new Vector(); 
          VItemName         = new Vector();
          VItemCode       = new Vector();

     }  	 
     public void append(String SDescript,double dQty,String SRemarks,String SUom,double drdcrate,double drdcvalue,String SHsncode,String SItemName,String SItemCode)
     {
          VDescript  .addElement(SDescript);
          VQty       .addElement(String.valueOf(dQty));
          VRemarks   .addElement(SRemarks);
          VUom       .addElement(SUom);
          VRDCRate    .addElement(String.valueOf(drdcrate)); 
          VRDCValue    .addElement(String.valueOf(drdcvalue)); 
          VHsnCode   .addElement(SHsncode);
          VItemName  . addElement(SItemName);
          VItemCode  . addElement(SItemCode);
     }

   public void append(String SDescript,double dQty,String SRemarks,String SUom)
     {
          VDescript  .addElement(SDescript);
          VQty       .addElement(String.valueOf(dQty));
          VRemarks   .addElement(SRemarks);
          VUom       .addElement(SUom);
         
     }
}
