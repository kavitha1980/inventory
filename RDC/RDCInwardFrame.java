package RDC;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

import javax.comm.*;

import javax.print.*;
import javax.print.attribute.*;
import javax.print.attribute.standard.*;
import javax.print.event.*;


public class RDCInwardFrame extends JInternalFrame
{
     DateField                TDate;
     JTextField               TSupCode;
    // DateField                TInvDate,TDCDate;
	 AttDateField	          TInvDate, TDCDate;
     MyTextField              TInvNo,TDCNo;
     InwardModeModel          InwMode;

     DirectRDCMiddlePanel     MiddlePanel;
     JPanel                   TopPanel,BottomPanel;
     JButton                  BOk,BApply,BSupplier;
     
     JLayeredPane             DeskTop;
     StatusPanel              SPanel;
     int                      iMillCode,iUserCode,iAuthCode;
     String                   SYearCode;
     String                   SSupTable;

     JComboBox                JCInwType,JCSort;

     Vector                   VInwName,VInwNo;

     Connection               theMConnection = null;

     boolean                  bComflag       = true;
     Common                   common         = new Common();
	 
	 String       SPort = "COM1";

     int iEntryCount =0;
	 int iTickCount  =0;
	 int iSeleCount  =0;

     String SGateNo="",SGrnNo="";
	 
	 
	 int iPreviousStock = 0;
	 
	 //Vector VGiItem,VGiQty;
	 

     public RDCInwardFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode,int iAuthCode,int iUserCode,String SYearCode,String SSupTable)
     {
          this.DeskTop            = DeskTop;
          this.SPanel             = SPanel;
          this.iMillCode          = iMillCode;
          this.iAuthCode          = iAuthCode;
          this.iUserCode          = iUserCode;
          this.SYearCode          = SYearCode;
          this.SSupTable          = SSupTable;

          ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                         theMConnection = oraConnection.getConnection();
          
          setInwardData();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }

     public void createComponents()
     {
          BOk        = new JButton("Okay");
          BApply     = new JButton("Apply");
          BSupplier  = new JButton("Supplier");
          
          TDate       = new DateField();
          
          TSupCode    = new JTextField();

          /*TInvDate    = new DateField();
          TDCDate     = new DateField();*/

		  TInvDate    = new AttDateField(10);
          TDCDate     = new AttDateField(10);

          TInvNo      = new MyTextField(15);
          TDCNo       = new MyTextField(15);

          JCInwType   = new JComboBox(VInwName);
          InwMode     = new InwardModeModel(iUserCode,iAuthCode,iMillCode);
          JCSort      = new JComboBox();
          
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();
          
          TDate.setTodayDate();
          
          TDate		.setEditable(false);
          TSupCode	.setEditable(false);

          JCInwType.setSelectedItem("RDC");
          JCInwType.setEnabled(false);
     }

     public void setLayouts()
     {
          setTitle("Inward of Materials against RDC");
          
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,970,500);
          
          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(5,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());

          BSupplier.setBorder(new javax.swing.border.BevelBorder(0));
          BSupplier.setBackground(new Color(128,128,255));
          BSupplier.setForeground(Color.RED);

          TopPanel.setBorder(new TitledBorder(""));
     }

     public void addComponents()
     {
          try
          {
               JCSort.addItem("RDCNo");
               JCSort.addItem("Name");

               TopPanel.add(new JLabel("Inward Date"));
               TopPanel.add(TDate);
               
               TopPanel.add(new JLabel("Supplier"));
               TopPanel.add(BSupplier);
               
               TopPanel.add(new JLabel("Invoice No"));
               TopPanel.add(TInvNo);
     
               TopPanel.add(new JLabel("Invoice Date"));
               TopPanel.add(TInvDate);
     
               TopPanel.add(new JLabel("DC No"));
               TopPanel.add(TDCNo);
     
               TopPanel.add(new JLabel("DC Date"));
               TopPanel.add(TDCDate);

               TopPanel.add(new JLabel("Select Inward Type"));
               TopPanel.add(JCInwType);

               TopPanel.add(new JLabel("Sorting Pending Orders By"));
               TopPanel.add(JCSort);

               TopPanel.add(new JLabel("Select Inward Mode"));
               TopPanel.add(InwMode);

               TopPanel.add(new JLabel(""));
               TopPanel.add(BApply);
               
               BottomPanel.add(BOk);
               getContentPane().add(TopPanel,BorderLayout.NORTH);
          }
          catch(Exception e)
          {
               System.out.println(e);
          }
     }

     public void addListeners()
     {
          BApply.addActionListener(new ActList());
          BOk.addActionListener(new ActList());
          BSupplier.addActionListener(new SupplierSearch(DeskTop,TSupCode,SSupTable));

		  TInvDate.theDate.addFocusListener(new FocusList());
		  TDCDate.theDate.addFocusListener(new FocusList());
     }
                                                      
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
                    setMiddlePanel();
               }

               if(ae.getSource()==BOk)
               {
                    BOk.setEnabled(false);

                    if(validateRDC())
                    {
                         insertRDCCollectedData();
			             getACommit();
                    }
                    else
                    {
                         BOk.setEnabled(true);
                    }
               }
          }
     }

	private class FocusList extends FocusAdapter
    {

        public void focusLost(FocusEvent fe)
        {
            
			String SMsg = getValidDaysCount();
			if(SMsg.length() > 0)
			{
				JOptionPane.showMessageDialog(null,SMsg,"Dear user,",JOptionPane.ERROR_MESSAGE);
				BApply.setEnabled(false);
			}
			else	
			BApply.setEnabled(true);
        }
    }

	
	private String getValidDaysCount()
	{
		String SMsg="";
		try
		{
			String SPartyCode = TSupCode.getText();

			int iGRNDate 		= common.toInt(TDate.toNormal());
			int iDcDate 		= common.toInt(TDCDate.toNormal());
			int iInvoiceDate 	= common.toInt(TInvDate.toNormal());
			SMsg		 		= common.getInvoiceValidDays(iGRNDate,iDcDate,iInvoiceDate,SPartyCode);

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			System.out.println("ex:"+ex);
		}
		return SMsg;
	}

     private void setMiddlePanel()
     {
          String SSupCode = TSupCode.getText();
          String SSupName = BSupplier.getText();
          int iSortIndex  = JCSort.getSelectedIndex();

          if(SSupCode.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Please Select the Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
          }
          else
          {
               int iCount = checkPendingRDC(SSupCode);

               if(iCount<=0)
               {
                    JOptionPane.showMessageDialog(null,"There is no Pending Orders for this Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
               }
               else
               {
                    BSupplier.setEnabled(false);
                    BApply.setEnabled(false);
                    JCInwType.setEnabled(false);
                    JCSort.setEnabled(false);

                    //MiddlePanel = new DirectRDCMiddlePanel(DeskTop,iMillCode,SSupCode,SSupName,iSortIndex);
					
					MiddlePanel = new DirectRDCMiddlePanel(DeskTop,iMillCode,SSupCode,SSupName,iSortIndex);
					
					

                    getContentPane().add(MiddlePanel,BorderLayout.CENTER);
                    getContentPane().add(BottomPanel,BorderLayout.SOUTH);

                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
          }

     }

     private int checkPendingRDC(String SSupCode)
     {
          int iCount=0;

          try
          {
               String QS = " Select Count(*) from RDC "+
                           " Where Sup_Code = '"+SSupCode+"' "+
                           " And RecQty < Qty And DocType=0 "+
                           " And MillCode="+iMillCode;

               Statement       stat =  theMConnection.createStatement();
               
               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    iCount = res.getInt(1);    
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E1"+ex );
          }
          return iCount;
     }

     private boolean validateRDC()
     {
		  iEntryCount	=0;
		  iTickCount	=0;
		  iSeleCount	=0;
          
          String SInvNo       = common.parseNull(TInvNo.getText().trim());
          String SInvDate     = TInvDate.toNormal();
          String SDCNo        = common.parseNull(TDCNo.getText().trim());
          String SDCDate      = TDCDate.toNormal();
          String SInwMode     = common.parseNull(InwMode.getText().trim());
		  
		  
		  /*if(SInvNo.length()>0 ) {
			  
		  printLabelNew("A1255225","TEST","TEST BIN",5);
		  return false;
			  
		  } */
		  
		  
		  String SMsg = getValidDaysCount();
		  
			if(SMsg.length() > 0)
			{
				JOptionPane.showMessageDialog(null,SMsg,"Dear user,",JOptionPane.ERROR_MESSAGE);
				BApply.setEnabled(false);
				return false;
			}


		  

          if(!SDCNo.equals(""))
          {
               if(common.toInt(SDCDate) ==0)
               {
                    JOptionPane.showMessageDialog(null,"Enter Dc Date ","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(common.toInt(SDCDate)>0)
          {
               if(SDCNo.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Enter Dc No ","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(!SInvNo.equals(""))
          {
               if(common.toInt(SInvDate) ==0)
               {
                    JOptionPane.showMessageDialog(null,"Enter Invoice Date ","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(common.toInt(SInvDate)>0)
          {
               if(SInvNo.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Enter Invoice No ","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

          }
          if(SDCNo.equals("") && SInvNo.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Select DC No Or Invoice No","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
               return false;

          }
          if(SInwMode.equals("Mode"))
          {
               JOptionPane.showMessageDialog(null,"Mode Is Not Select","Problem in Save Data",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          if(isGateNotFilled())
          {
               JOptionPane.showMessageDialog(null,"Some Gate Entries are Not Filled","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          if(isRDCNotTicked())
          {
               JOptionPane.showMessageDialog(null,"Some RDC Entries are Not filled","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          if(isWONotFilled())
          {
               JOptionPane.showMessageDialog(null,"Quantity Mismatch","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          if(isNotValidWOQty())
          {
               JOptionPane.showMessageDialog(null,"Invalid Free and WorkOrder Qty","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          if(iEntryCount<=0)
          {
               JOptionPane.showMessageDialog(null,"No Entries Made","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
		  
		  if(!isScrapValid()) {
			  
               JOptionPane.showMessageDialog(null,"Scrap Qty Greater Than REceved Qty..","Error",JOptionPane.ERROR_MESSAGE);
               return false;
			  
		  }
		  
		  if(!isBinValid()) {
			  
               JOptionPane.showMessageDialog(null,"Bin Name Must Be Feed..","Error",JOptionPane.ERROR_MESSAGE);
               return false;
			  
		  }
		  
		  		  if(!isServiceorNonStockValid()){

               //JOptionPane.showMessageDialog(null,"Bin Name Must Be Feed..","Error",JOptionPane.ERROR_MESSAGE);
               return false;
		  
		  }
		  

		getTickCount();

		iSeleCount = MiddlePanel.MiddlePanel.dataModel.getRows();

		if(iTickCount>iSeleCount || iTickCount<iSeleCount)
          {
               JOptionPane.showMessageDialog(null,"Noof Selected and Entered Items Mismatch","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
         
          return true;
     }
     
     private boolean isGateNotFilled()
     {
          if(MiddlePanel.GatePanel.GateModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.GatePanel.GateModel.getRows();i++)
               {
                    String SDesc = (String)MiddlePanel.GatePanel.GateModel.getValueAt(i,0);
                    String SQty  = (String)MiddlePanel.GatePanel.GateModel.getValueAt(i,1);

                    if(SDesc.equals("") || SQty.equals("") || common.toDouble(SQty)<=0)
                         return true;

                    iEntryCount++;
               }
          }
          return false;
     }
     
     private boolean isRDCNotTicked()
     {
          if(MiddlePanel.RdcPanel.RdcModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.RdcPanel.RdcModel.getRows();i++)
               {
                    String SQty    = (String)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,5);
                    Boolean bValue = (Boolean)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,11);
                    
                    if(common.toDouble(SQty)>0 && !bValue.booleanValue())
                         return true;


                    if(common.toDouble(SQty)>0 && bValue.booleanValue())
                         iEntryCount++;
               }
          }
          return false;
     }

     private boolean isWONotFilled()
     {
          if(MiddlePanel.RdcPanel.RdcModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.RdcPanel.RdcModel.getRows();i++)
               {
                    double dRecQty  = common.toDouble((String)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,5));
                    double dFreeQty = common.toDouble((String)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,9));
                    double dWOQty   = common.toDouble((String)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,10));
                    double dQty     = dFreeQty+dWOQty;
                    Boolean bValue  = (Boolean)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,11);
                    
                    if(dRecQty>0 && bValue.booleanValue() && (dQty>dRecQty || dQty<dRecQty))
                         return true;

               }
          }
          return false;
     }
	 
     private boolean isScrapValid()
     {
          if(MiddlePanel.RdcPanel.RdcModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.RdcPanel.RdcModel.getRows();i++)
               {
                    double dRecQty  	= common.toDouble((String)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,5));
                    double dScrapQty   	= common.toDouble((String)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,12));
                    Boolean bValue  = (Boolean)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,11);
                    
                    if(dRecQty>0 && bValue.booleanValue() && (dRecQty<dScrapQty ))
                         return false;

               }
          }
          return true;
     }
	 
     private boolean isBinValid()
     {
          if(MiddlePanel.RdcPanel.RdcModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.RdcPanel.RdcModel.getRows();i++)
               {
                    String SBin   	= common.parseNull((String)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,16));
                    Boolean bValue  = (Boolean)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,11);

                    if(bValue) {

						if(SBin.length()==0){
							return false;	
						}
                         

                    }

                    

               }
          }
          return true;
     }
	 
     private boolean isServiceorNonStockValid()
     {
          if(MiddlePanel.RdcPanel.RdcModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.RdcPanel.RdcModel.getRows();i++)
               {
                    
					
                    Boolean bValue  = (Boolean)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,11);

                    if(bValue) {
					
						Boolean bService   = (Boolean)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,17);
						Boolean bNonStock  = (Boolean)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,18);
						Boolean bSkipStock = (Boolean)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,19);
						
						System.out.println("bService"+bService);
						System.out.println("bNonStock"+bNonStock);
						System.out.println("bSkipStock"+bSkipStock);
						
						if(!bService && !bNonStock && !bSkipStock ) {

							JOptionPane.showMessageDialog(null,"Select Check box Service Stock or Non Stock or Without Stock  ","Error",JOptionPane.ERROR_MESSAGE);
							return false;
						}
						
						int iSelectCount=0;

						if(bService) {
							iSelectCount++;
						}
						
						if(bNonStock){
							iSelectCount++;
						}
						
						if(bSkipStock) {

						  iSelectCount++;
						}
						
						if(iSelectCount>1) {
							JOptionPane.showMessageDialog(null,"Select Only One Check Box.. ","Error",JOptionPane.ERROR_MESSAGE);
							return false;
						}
					
						
					}
                    

               }
          }
          return true;
     }
	 
	 
	 

     private boolean isNotValidWOQty()
     {
          if(MiddlePanel.RdcPanel.RdcModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.RdcPanel.RdcModel.getRows();i++)
               {
                    double dRdcQty      = common.toDouble((String)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,3));
                    double dDirectWOQty = common.toDouble((String)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,6));
                    double dPrevWOQty   = common.toDouble((String)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,7));
                    double dPrevFreeQty = common.toDouble((String)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,8));
                    double dFreeQty     = common.toDouble((String)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,9));
                    Boolean bValue      = (Boolean)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,11);

                    double dQty         = common.toDouble(common.getRound(dRdcQty - (dDirectWOQty+dPrevWOQty+dPrevFreeQty),3));
                    System.out.println("arun"+dFreeQty);
                    System.out.println(dQty);
                    if(bValue.booleanValue() && dFreeQty>dQty)
                         return true;
               }
          }
          return false;
     }

     private void getTickCount()
     {
          if(MiddlePanel.RdcPanel.RdcModel.getRows()>0)
          {
               for(int i=0;i<MiddlePanel.RdcPanel.RdcModel.getRows();i++)
               {
                    String SQty    = (String)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,5);
                    Boolean bValue = (Boolean)MiddlePanel.RdcPanel.RdcModel.getValueAt(i,11);
                    
                    if(common.toDouble(SQty)>0 && bValue.booleanValue())
                         iTickCount++;
               }
          }
     }

     private void insertRDCCollectedData()
     {
		try
		{
		     if(MiddlePanel.MiddlePanel.dataModel.getRows()>0)
		     {
		          setGrnNo				();
			      setGateNo				();
		          updateRDCDetails		();
		          insertGateDetails		();
		          insertRDCGateDetails	();
				  
				  //insertscrapDetails	(); // add scrap item
				  convertStoreStock		();// add rdc item to store stock
		     }

		     if(MiddlePanel.GatePanel.GateModel.getRows()>0)
		     {
		          insertDirectGateDetails();
		     }
		}
		catch(Exception ex)
          {
               System.out.println("I1 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

     private void setGrnNo()
     {
          SGrnNo="";

          String QS ="";
		  
          try
          {
               QS = "  Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 10 for update of MaxNo noWait ";

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               Statement       stat =  theMConnection.createStatement();
               
               PreparedStatement thePrepare = theMConnection.prepareStatement("Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 10"); 

               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    SGrnNo = res.getString(1);    
               }
               res.close();

               thePrepare.setInt(1,common.toInt(SGrnNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println("E1"+ex );
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    setGrnNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
     }

     private void setGateNo()
     {
          SGateNo="";

          String QS ="";
          try
          {
               QS = " Select (maxno+1) from config"+iMillCode+""+SYearCode+" where id = 3 for update of MaxNo noWait ";

               if(theMConnection  . getAutoCommit())
                    theMConnection  . setAutoCommit(false);

               Statement       stat =  theMConnection.createStatement();
               
               PreparedStatement thePrepare = theMConnection.prepareStatement(" Update config"+iMillCode+""+SYearCode+" set MaxNo = ?  where Id = 3"); 

               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    SGateNo = res.getString(1);    
               }
               res.close();

               thePrepare.setInt(1,common.toInt(SGateNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println("E1"+ex );
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    setGateNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
     }

     public void updateRDCDetails()
     {
          try
          {

               Statement       stat =  theMConnection.createStatement();
               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();                 
               for(int i=0;i<RowData.length;i++)
               {
                    String    QS = "Update RDC Set ";
                              QS = QS+" RecQty=RecQty+"+(String)RowData[i][5];
                              QS = QS+" Where Id = "+(String)MiddlePanel.VSeleId.elementAt(i);

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }
	 
     public void insertscrapDetails()
     {
          String QString = " Insert Into ScrapRdc (Id,ItemCode,Entrydate,GiNo,RdcNo,ScrapQty) Values(";
          try
          {
			  
               Statement       stat =  theMConnection.createStatement();

               Object RdcData[][] = MiddlePanel.RdcPanel.getFromVector();
               
               String SDateTime = common.getServerDateTime();


               for(int i=0;i<RdcData.length;i++)
               {
                    Boolean bValue = (Boolean)RdcData[i][11];
     
                    if(!bValue.booleanValue())
                         continue;
					 
                    
					 

                    String  SItemCode  = ((String)RdcData[i][14]).trim();
                    String  SScrapQty  = (String)RdcData[i][12];
					String  SRdcNo     = (String)RdcData[i][1];

                    if(common.toDouble(SScrapQty)>0)
                    {
						
						String 	QS = "Insert Into ScrapRdc (Id,ItemCode,Entrydate,GiNo,RdcNo,ScrapQty) Values( ";
								QS = QS+"ScrapRdc_seq.nextval,";						
								QS = QS+"'"+SItemCode+"',";
								QS = QS+"sysdate,";
								QS = QS+"0"+SGateNo+",";
								QS = QS+"0"+SRdcNo+",";
                                QS = QS+"0"+SScrapQty+")";

						if(theMConnection  . getAutoCommit())
							 theMConnection  . setAutoCommit(false);

						stat.executeUpdate(QS);
						
                    }

               }

               stat.close();
			   
			   
          }
          catch(Exception ex)
          {
               System.out.println("Scrap Insert : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }
	 
	 
	 
	 
	 
	 
	 

     public void insertGateDetails()
     {
          String QString = " Insert Into GateInward (GINo,GIDate,InwNo,Sup_Code,InvNo,InvDate,DcNo,DcDate,ModeCode,Item_Name,SupQty,GateQty,UnmeasuredQty,UserCode,UserTime,BaseCode,MillCode,id,GrnNo,EntryStatus,WOStatus,Authentication,ItemCode,RdcItem,WithOutStatus) Values(";
          try
          {
			  
			   //VGiItem = new Vector();
			   //VGiQty  = new Vector();			  
			   
			  
               Statement       stat =  theMConnection.createStatement();
			   
			   


               Object RdcData[][] = MiddlePanel.RdcPanel.getFromVector();
               
               Vector VGIName = new Vector();
               Vector VGIQty  = new Vector();
               Vector VStatus = new Vector();
               Vector VItemCode = new Vector();
			   Vector VStockStatus = new Vector();
			   

               String SDateTime = common.getServerDateTime();

               String SEntryStatus = "1";

               for(int i=0;i<RdcData.length;i++)
               {
                    Boolean bValue = (Boolean)RdcData[i][11];
     
                    if(!bValue.booleanValue())
                         continue;

                    String  SItemName  = ((String)RdcData[i][0]).trim();
                    String  SFreeQty   = (String)RdcData[i][9];
                    String  SWOQty     = (String)RdcData[i][10];
					
                    String  SItemCode  = ((String)RdcData[i][14]).trim();
					
					Boolean bValue1 = (Boolean)RdcData[i][19];
					
					
					

                    if(common.toDouble(SFreeQty)>0)
                    {
                         VGIName.addElement(SItemName);
                         VGIQty	.addElement(SFreeQty);
                         VStatus.addElement("0");
						 VItemCode.addElement(SItemCode);
						 
						if(bValue1.booleanValue()) {
							VStockStatus.addElement("1");
						}else {
							VStockStatus.addElement("0");
						}
						 

					     //VGiItem = new Vector();
						 //VGiQty  = new Vector();			  
						 
                    }

                    if(common.toDouble(SWOQty)>0)
                    {
                         VGIName.addElement(SItemName);
                         VGIQty.addElement(SWOQty);
                         VStatus.addElement("1");
						 VItemCode.addElement(SItemCode);
						 
						if(bValue1.booleanValue()) {
							VStockStatus.addElement("1");
						}else {
							VStockStatus.addElement("0");
						}
						 
						 
                    }
               }

               String SInvNo       = common.parseNull(TInvNo.getText().trim());
               String SInvDate     = TInvDate.toNormal();
               String SDCNo        = common.parseNull(TDCNo.getText().trim());
               String SDCDate      = TDCDate.toNormal();

               String SSupCode = TSupCode.getText();
               String SDate    = TDate.toNormal();

               String SInwNo = (String)VInwNo.elementAt(JCInwType.getSelectedIndex());

               String SModeCode = InwMode.TCode.getText();

               for(int i=0;i<VGIName.size();i++)
               {
                    String  SItemName  = (String)VGIName.elementAt(i);
                    String  SQty       = (String)VGIQty.elementAt(i);
                    String  SWOStatus  = (String)VStatus.elementAt(i);

                    if(SItemName.length()>60)
                         SItemName = SItemName.substring(0,60);
					 
					 
                    
                    String    QS1 = QString;
                              QS1 = QS1+"0"+SGateNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"0"+SInwNo+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"'"+SInvNo+"',";
                              QS1 = QS1+"'"+SInvDate+"',";
                              QS1 = QS1+"'"+SDCNo+"',";
                              QS1 = QS1+"'"+SDCDate+"',";
                              QS1 = QS1+"0"+SModeCode+",";
                              QS1 = QS1+"'"+common.getNarration(SItemName)+"',";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+"0"+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+SDateTime+"',";
                              QS1 = QS1+"0"+iAuthCode+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"gateinward_seq.nextval"+",";
                              QS1 = QS1+"0"+"0"+",";
                              QS1 = QS1+"0"+SEntryStatus+",";
                              QS1 = QS1+"0"+SWOStatus+",";
                              //QS1 = QS1+"0"+"1"+")";
							  QS1 = QS1+"0"+"1"+",";
							  QS1 = QS1+"'"+(String)VItemCode.elementAt(i)+"',";
							  QS1 = QS1+"0"+"1,";
							  QS1 = QS1+"'"+(String)VStockStatus.elementAt(i)+"')";
							  
							  
							  

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.executeUpdate(QS1);
               }
               stat.close();
			   
			   
			   
			   
          }
          catch(Exception ex)
          {
               System.out.println("E3 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

     public void insertRDCGateDetails()
     {
          try
          {

               Statement       stat =  theMConnection.createStatement();

               Object RdcData[][] = MiddlePanel.RdcPanel.getFromVector();
               
               String SInvNo       = common.parseNull(TInvNo.getText().trim());
               String SInvDate     = TInvDate.toNormal();
               String SDCNo        = common.parseNull(TDCNo.getText().trim());
               String SDCDate      = TDCDate.toNormal();

               String SSupCode = TSupCode.getText();
               String SDate    = TDate.toNormal();

               String SDateTime = common.getServerDateTime();

               String SEntryStatus = "1";

               for(int i=0;i<RdcData.length;i++)
               {
                    Boolean bValue = (Boolean)RdcData[i][11];
     
                    if(!bValue.booleanValue())
                         continue;

                    Vector VQty     = new Vector();
                    Vector VOrderNo = new Vector();
                    Vector VStatus  = new Vector();

                    int iRDCNo       = common.toInt((String)RdcData[i][1]);
                    int iSlNo        = common.toInt(MiddlePanel.getRdcSLNo(i));
                    int iMemoUserCode= common.toInt(MiddlePanel.getMemoUserCode(i));

                    if(iMemoUserCode<=0)
                         iMemoUserCode = 1;

                    String SDescript = ((String)RdcData[i][0]).trim();


                    String  SFreeQty     = (String)RdcData[i][9];
                    String  SWOQty       = (String)RdcData[i][10];
                    String  SDirectWOQty = (String)RdcData[i][6];

                    if(common.toDouble(SFreeQty)>0)
                    {
                         VQty.addElement(SFreeQty);
                         VOrderNo.addElement("0");
                         VStatus.addElement("0");
                    }

                    if(common.toDouble(SWOQty)>0)
                    {
                         if(common.toDouble(SDirectWOQty)>0)
                         {
                              if(common.toDouble(SWOQty)>common.toDouble(SDirectWOQty))
                              {
                                   double dNewWOQty = common.toDouble(SWOQty) - common.toDouble(SDirectWOQty);

                                   VQty.addElement(String.valueOf(dNewWOQty));
                                   VOrderNo.addElement("0");
                                   VStatus.addElement("1");

                                   String SOrderNo = getOrderNo(iRDCNo,iSlNo,SDescript);

                                   if(SOrderNo.equals("0"))
                                   {
                                        JOptionPane.showMessageDialog(null,"Problem in Getting OrderNo - Contact EDP ","Information",JOptionPane.INFORMATION_MESSAGE);
                                        bComflag  = false;
                                   }
                                   else
                                   {
                                        VQty.addElement(SDirectWOQty);
                                        VOrderNo.addElement(SOrderNo);
                                        VStatus.addElement("1");
                                   }

                              }
                              else
                              {
                                   String SOrderNo = getOrderNo(iRDCNo,iSlNo,SDescript);

                                   if(SOrderNo.equals("0"))
                                   {
                                        JOptionPane.showMessageDialog(null,"Problem in Getting OrderNo - Contact EDP ","Information",JOptionPane.INFORMATION_MESSAGE);
                                        bComflag  = false;
                                   }
                                   else
                                   {
                                        VQty.addElement(SWOQty);
                                        VOrderNo.addElement(SOrderNo);
                                        VStatus.addElement("1");
                                   }
                              }
                         }
                         else
                         {
                              VQty.addElement(SWOQty);
                              VOrderNo.addElement("0");
                              VStatus.addElement("1");
                         }
                    }

                    for(int k=0;k<VOrderNo.size();k++)
                    {
                         String SWOStatus = (String)VStatus.elementAt(k);
                         String SQty      = (String)VQty.elementAt(k);
                         String SOrderNo  = (String)VOrderNo.elementAt(k);
     
                         String QS = "Insert Into GateInwardRDC (ID,GINo,GIDate,Sup_Code,InvNo,InvDate,DcNo,DcDate,Descript,SupQty,GateQty,UnmeasuredQty,UserCode,RdcNo,RdcSlNo,MemoAuthUserCode,RecQty,GrnNo,EntryStatus,WOStatus,WOQty,OrderNo,MillCode) Values(";
                         QS = QS+"GateInwardRDC_Seq.nextval,";
                         QS = QS+"0"+SGateNo+",";
                         QS = QS+"'"+SDate+"',";
                         QS = QS+"'"+SSupCode+"',";
                         QS = QS+"'"+SInvNo+"',";
                         QS = QS+"'"+SInvDate+"',";
                         QS = QS+"'"+SDCNo+"',";
                         QS = QS+"'"+SDCDate+"',";
                         QS = QS+"'"+common.getNarration(((String)RdcData[i][0]).trim())+"',";
                         QS = QS+"0"+SQty+",";
                         QS = QS+"0"+SQty+",";
                         QS = QS+"0"+"0"+",";
                         QS = QS+"0"+iUserCode+",";
                         QS = QS+"0"+iRDCNo+",";
                         QS = QS+"0"+iSlNo+",";
                         QS = QS+"0"+iMemoUserCode+",";
                         QS = QS+"0"+SQty+",";
                         QS = QS+"0"+SGrnNo+",";
                         QS = QS+"0"+SEntryStatus+",";
                         QS = QS+"0"+SWOStatus+",";
                         QS = QS+"0"+SQty+",";
                         QS = QS+"0"+SOrderNo+",";
                         QS = QS+"0"+iMillCode+")";
     
                         stat.execute(QS);

                         String QS1 = "";

                         if(SWOStatus.equals("1"))
                         {
                              if(common.toInt(SOrderNo)>0)
                              {
                                   QS1 = "Update RDC Set WOQty=WOQty+"+SQty+
                                         ",DirectWOQty=DirectWOQty-"+SQty+
                                         ",WOStatus="+SWOStatus+
                                         " Where trim(Descript)='"+SDescript+"' and RDCNo="+iRDCNo+" and SlNo="+iSlNo+" and MillCode="+iMillCode;
                              }
                              /*else
                              {
                                   QS1 = "Update RDC Set WOQty=WOQty+"+SQty+
                                         ",WOStatus="+SWOStatus+
                                         " Where Descript='"+SDescript+"' and RDCNo="+iRDCNo+" and SlNo="+iSlNo+" and MillCode="+iMillCode;
                              }*/
                         }
                         else
                         {
                              QS1 = "Update RDC Set FreeQty=FreeQty+"+SQty+
                                    ",WOStatus="+SWOStatus+
                                    " Where trim(Descript)='"+SDescript+"' and RDCNo="+iRDCNo+" and SlNo="+iSlNo+" and MillCode="+iMillCode;
                         }

                         if(QS1.length()>0)
                              stat.execute(QS1);
                    }
               }

               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

     private String getOrderNo(int iRDCNo,int iSlNo,String SDescript)
     {
          String SOrderNo="0";

          String QS = "Select Max(OrderNo) from WorkOrder Where Lower(Descript)=Lower('"+SDescript+"') and RDCNo="+iRDCNo+" and RDCSlNo="+iSlNo+" and MillCode="+iMillCode;

          try
          {
              Statement       stat =  theMConnection.createStatement();

              ResultSet result = stat.executeQuery(QS);
              while(result.next())
              {
                    SOrderNo = common.parseNull(result.getString(1));
              }
              result.close();
              stat.close();

              if(SOrderNo.equals(""))
                   SOrderNo="0";
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return SOrderNo;
     }


     public void insertDirectGateDetails()
     {
          String QString = " Insert Into GateInward (GINo,GIDate,InwNo,Sup_Code,InvNo,InvDate,DcNo,DcDate,ModeCode,Item_Name,SupQty,GateQty,UnmeasuredQty,UserCode,UserTime,BaseCode,MillCode,id,EntryStatus,Authentication) Values(";
          try
          {
               Statement       stat =  theMConnection.createStatement();

               Object GateData[][] = MiddlePanel.GatePanel.getFromVector();
               
               String SInvNo       = common.parseNull(TInvNo.getText().trim());
               String SInvDate     = TInvDate.toNormal();
               String SDCNo        = common.parseNull(TDCNo.getText().trim());
               String SDCDate      = TDCDate.toNormal();

               String SSupCode = TSupCode.getText();
               String SDate    = TDate.toNormal();

               String SInwNo = (String)VInwNo.elementAt(JCInwType.getSelectedIndex());

               String SModeCode = InwMode.TCode.getText();

               String SDateTime = common.getServerDateTime();

               String SEntryStatus = "1";

               for(int i=0;i<GateData.length;i++)
               {
                    String  SItemName  = (((String)GateData[i][0]).toUpperCase()).trim();
                    String  SQty       = (String)GateData[i][1];
                    
                    String    QS1 = QString;
                              QS1 = QS1+"0"+SGateNo+",";
                              QS1 = QS1+"'"+SDate+"',";
                              QS1 = QS1+"0"+SInwNo+",";
                              QS1 = QS1+"'"+SSupCode+"',";
                              QS1 = QS1+"'"+SInvNo+"',";
                              QS1 = QS1+"'"+SInvDate+"',";
                              QS1 = QS1+"'"+SDCNo+"',";
                              QS1 = QS1+"'"+SDCDate+"',";
                              QS1 = QS1+"0"+SModeCode+",";
                              QS1 = QS1+"'"+common.getNarration(SItemName)+"',";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+SQty+",";
                              QS1 = QS1+"0"+"0"+",";
                              QS1 = QS1+"0"+iUserCode+",";
                              QS1 = QS1+"'"+SDateTime+"',";
                              QS1 = QS1+"0"+iAuthCode+",";
                              QS1 = QS1+"0"+iMillCode+",";
                              QS1 = QS1+"gateinward_seq.nextval"+",";
                              QS1 = QS1+"0"+SEntryStatus+",";
                              QS1 = QS1+"0"+"1"+")";

                    if(theMConnection  . getAutoCommit())
                         theMConnection  . setAutoCommit(false);

                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("E2 : "+ex);
               ex.printStackTrace();
               bComflag  = false;
          }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theMConnection . commit();

                    JOptionPane    . showMessageDialog(null,"The Entered Data is Saved GI No - "+SGateNo,"Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("Commit");
               }
               else
               {
                    theMConnection . rollback();
                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theMConnection   . setAutoCommit(true);

               removeHelpFrame();

          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     private void setInwardData()
     {
          VInwName   = new Vector();
          VInwNo     = new Vector();

          String QS1 = "Select InwName,InwNo From InwType Where InwNo Not in (12) Order By 2";

          try
          {
              ORAConnection   oraConnection =  ORAConnection.getORAConnection();
              Connection      theConnection =  oraConnection.getConnection();
              Statement       stat =  theConnection.createStatement();

              ResultSet result1 = stat.executeQuery(QS1);
              while(result1.next())
              {
                    VInwName.addElement(result1.getString(1));
                    VInwNo.addElement(result1.getString(2));
              }
              result1.close();
              stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
	 
	 
	 
	 // Rdc Materials add into sotre stock  06.04.2020
	 
	 
	private void convertStoreStock() {
		
		try {
		
               Object RdcData[][] = MiddlePanel.RdcPanel.getFromVector();
               

               String SDateTime = common.getServerDateTime();

               String SEntryStatus = "1";

               for(int i=0;i<RdcData.length;i++)
               {
                    Boolean bValue = (Boolean)RdcData[i][11];
     
                    if(!bValue.booleanValue())
                         continue;
					 
					 Boolean bSkipStock = (Boolean)RdcData[i][19];
					 
                    if(bSkipStock.booleanValue())
						continue;
					 
					 
					System.out.println("comm into insert stock"); 
					 
					String sId           	= ""; 
					String SItemCode     	= common.parseNull((String)RdcData[i][14]);				
					
					String SItemName     	= common.parseNull((String)RdcData[i][13]);				
					
					
					 Boolean bServiceValue = (Boolean)RdcData[i][17];
					 Boolean bNonStockValue = (Boolean)RdcData[i][18];
					
					
					if(bServiceValue)	{
						
						
						int iHODCode  			= 10371;
						int iTransferStock  	= common.toInt((String)RdcData[i][5]);
						String SLocationName  	= "";

						int iCount 					= alredyInserted(SItemCode,iHODCode);
						
						int iRDCNo       = common.toInt((String)RdcData[i][1]);
						
						SLocationName	 = common.parseNull((String)RdcData[i][16]);				


						
						if(iCount>0){
								
							insertItemStockHistory(SItemCode,iHODCode);
							double dTotalStock = iPreviousStock + iTransferStock;

							updateItemStock(SItemCode,iHODCode,dTotalStock,sId);
							insertServiceStock(SItemCode,iTransferStock,iRDCNo,iHODCode,SLocationName);
							//updateStatus(SItemCode,sId,iHODCode);
							updateLocation(SItemCode,SLocationName);
							
							printLabelNew(SItemCode,SItemName,SLocationName,iTransferStock);
						}
						else
						{
							insertItemstock(SItemCode,iHODCode,iTransferStock,sId);
							insertServiceStock(SItemCode,iTransferStock,iRDCNo,iHODCode,SLocationName);
							//updateStatus(SItemCode,sId,iHODCode);
							updateLocation(SItemCode,SLocationName);
							printLabelNew(SItemCode,SItemName,SLocationName,iTransferStock);
						}
						
					}
					
					
					if(bNonStockValue)	{
						


						int iHODCode  			= 6125;
						int iTransferStock  	= common.toInt((String)RdcData[i][5]);
						String SLocationName  	= "";

						int iCount 					= alredyInserted(SItemCode,iHODCode);
						
						int iRDCNo       = common.toInt((String)RdcData[i][1]);
						
						SLocationName	 = common.parseNull((String)RdcData[i][16]);				

						
						if(iCount>0){
								
							//insertItemStockHistory(SItemCode,iHODCode);
							double dTotalStock = iPreviousStock + iTransferStock;
							
							insertItemStockHistory(SItemCode,iHODCode);

							updateItemStock(SItemCode,iHODCode,dTotalStock,sId);
							updateLocation1(SItemCode,SLocationName);
							insertNonStock(SItemCode,iTransferStock,iRDCNo,iHODCode,SLocationName);
							printLabelNew(SItemCode,SItemName,SLocationName,iTransferStock);
							
						}
						else
						{
							
							insertItemstock(SItemCode,iHODCode,iTransferStock,sId);
							insertNonStock(SItemCode,iTransferStock,iRDCNo,iHODCode,SLocationName);
							updateLocation1(SItemCode,SLocationName);
							printLabelNew(SItemCode,SItemName,SLocationName,iTransferStock);
						}

						
						
					}	
					
			   }
		}	   
		catch(Exception ex){
			ex.printStackTrace();
		}

	}
	 
	 

	private int alredyInserted(String SItemCode,int iHODCode)
	{
		int iCount 		= 0;
		iPreviousStock 	= 0;
		
		StringBuffer sb = new StringBuffer();
		
		sb.append(" Select Count(1),sum(stock) from itemstock ");
		sb.append(" where ItemCode ='"+SItemCode+"' and HODCode = "+iHODCode+" and MillCode = "+iMillCode );	
		
		try{
			
			ORAConnection   oraConnection =  ORAConnection.getORAConnection();
			Connection      theConnection =  oraConnection.getConnection();

			PreparedStatement ps     	= theConnection.prepareStatement(sb.toString());
			ResultSet	result1	 		= ps.executeQuery();
			
			while(result1.next())
			{ 
				iCount 			= result1.getInt(1);
				iPreviousStock 	= result1.getInt(2);		
			}
			result1	. close();
			ps    	. close();
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return iCount;
	}
	
	
			
	private void insertNonStock(String SItemCode,int iTransferStock,int iRDCNo,int iHODCode,String SLocationName) {
		
		try{



			
			
			/*String QS = " Insert into rdc_itemstock (Id,GiNo,RdcNo,ItemCode,Receipt_qty,entrydate) values ( "+
						" rdc_itemstock_seq.nextval,"+SGateNo+","+iRDCNo+",'"+SItemCode+"',"+iTransferStock+",sysdate ) ";*/
						
						
		StringBuffer sb = new StringBuffer();
		
			sb.append(" Select MEMOAUTHUSERCODE,Dept_Code from rdc ");
			sb.append(" where rdcno="+iRDCNo+" " );	
		
		    int iRdcUser=0;
			String SDeptCode="";
		
			
			ORAConnection   oraConnection =  ORAConnection.getORAConnection();
			Connection      theConnection =  oraConnection.getConnection();

			PreparedStatement ps1     	= theConnection.prepareStatement(sb.toString());
			ResultSet	result1	 		= ps1.executeQuery();
			
			while(result1.next())
			{ 
				iRdcUser 	= result1.getInt(1);
				SDeptCode	= result1.getString(2);
		    }
			result1.close();
			ps1.close();
			
			String SSysName 	= common.getLocalHostName();
			String SDate     	= common.pureDate(common.getServerDate()); 
						
			//String QS = " Insert into servicestocktransfer(Id,ItemCode,Stock,StockValue,HODCode,MillCode,IndentQty,TransferQty,ReservedQty,EntryUserCode,EntryDateTime,EntrySystemName,OwnerCode,EntryDate,DeptCode,LocName,TRANSFERSTATUSDATE,TRANSFERSTATUS,RDcitem ) "+
				//		" Values(servicestock_seq.nextVal,'"+SItemCode+"',"+iTransferStock+",0,"+iHODCode+","+iMillCode+",0,0,0,"+iRdcUser+",sysdate,'"+SSysName+"',"+iRdcUser+",'"+SDate+"','"+SDeptCode+"' ,'"+SLocationName+"',Sysdate,1 ,1 ) ";

			StringBuffer SBInsert = new StringBuffer()		;

			SBInsert.append(" Insert into Itemstock_transfer(MillCode,HODCode,ItemCode,Stock,StockValue,IndentQty,TransferQty,ReservedQty,EntryUserCode,EntryDateTime,Id,rdcitem,TRANSFERDATE,TRANFERSTATUS ) ");
			SBInsert.append(" Values("+iMillCode+","+iHODCode+",'"+SItemCode+"',"+iTransferStock+", ");
			SBInsert.append(" 0,0,0,0,"+iRdcUser+", SysDate,ItemStock_Transfer_seq.nextVal,1,sysdate,1) ");
						
						
			
			if(theMConnection  . getAutoCommit())
				 theMConnection  . setAutoCommit(false);
			 
			PreparedStatement ps = theMConnection.prepareStatement(SBInsert.toString());
			ps.executeQuery();	
			ps.close();
			
		}
		catch(Exception ex){
			ex.printStackTrace();
			bComflag  = false;
		}
	}
			
	
	
	
	private void insertServiceStock(String SItemCode,int iTransferStock,int iRDCNo,int iHODCode,String SLocationName) {
		
		try{



			
			
			/*String QS = " Insert into rdc_itemstock (Id,GiNo,RdcNo,ItemCode,Receipt_qty,entrydate) values ( "+
						" rdc_itemstock_seq.nextval,"+SGateNo+","+iRDCNo+",'"+SItemCode+"',"+iTransferStock+",sysdate ) ";*/
						
						
		StringBuffer sb = new StringBuffer();
		
			sb.append(" Select MEMOAUTHUSERCODE,Dept_Code from rdc ");
			sb.append(" where rdcno="+iRDCNo+" " );	
		
		    int iRdcUser=0;
			String SDeptCode="";
		
			
			ORAConnection   oraConnection =  ORAConnection.getORAConnection();
			Connection      theConnection =  oraConnection.getConnection();

			PreparedStatement ps1     	= theConnection.prepareStatement(sb.toString());
			ResultSet	result1	 		= ps1.executeQuery();
			
			while(result1.next())
			{ 
				iRdcUser 	= result1.getInt(1);
				SDeptCode	= result1.getString(2);
		    }
			result1.close();
			ps1.close();
			
			String SSysName 	= common.getLocalHostName();
			String SDate     	= common.pureDate(common.getServerDate()); 
						
			String QS = " Insert into servicestocktransfer(Id,ItemCode,Stock,StockValue,HODCode,MillCode,IndentQty,TransferQty,ReservedQty,EntryUserCode,EntryDateTime,EntrySystemName,OwnerCode,EntryDate,DeptCode,LocName,TRANSFERSTATUSDATE,TRANSFERSTATUS,RDcitem ) "+
						" Values(servicestock_seq.nextVal,'"+SItemCode+"',"+iTransferStock+",0,"+iHODCode+","+iMillCode+",0,0,0,"+iRdcUser+",sysdate,'"+SSysName+"',"+iRdcUser+",'"+SDate+"','"+SDeptCode+"' ,'"+SLocationName+"',Sysdate,1 ,1 ) ";
						
			
			if(theMConnection  . getAutoCommit())
				 theMConnection  . setAutoCommit(false);
			 
			PreparedStatement ps = theMConnection.prepareStatement(QS);
			ps.executeQuery();	
			ps.close();
			
		}
		catch(Exception ex){
			ex.printStackTrace();
			bComflag  = false;
		}
	}
	
	/*private void insertItemStockHistory (String SItemCode,int iHODCode)
	{
		try{
			
			String QS = "Insert into itemstockhistory (select MillCode, HODCode, ItemCode, Stock, StockValue,IndentQty,TransferQty,ReservedQty,ItemStockTransferId,Id,Sysdate, itemstockhistory_seq.nextVal from itemstock Where ItemCode = '"+SItemCode+"' and HODCode = "+iHODCode+" and MillCode = "+iMillCode+" ) ";
			
			
			if(theMConnection  . getAutoCommit())
				 theMConnection  . setAutoCommit(false);
			 
			PreparedStatement ps = theMConnection.prepareStatement(QS);
			ps.executeQuery();	
			ps.close();
			
		}
		catch(Exception ex){
			ex.printStackTrace();
			bComflag  = false;
		}
	}*/
	
	private void insertItemStockHistory (String SItemCode,int iHODCode)
	{
		try{
			
			if(theMConnection  . getAutoCommit())
				 theMConnection  . setAutoCommit(false);
			
			String QS15 = "Insert into ItemStock_History (select MillCode, HODCode, ItemCode, Stock, StockValue,IndentQty,TransferQty,ReservedQty,ItemStockTransferId,Id,Sysdate, ItemStock_History_Seq.nextVal from ItemStock Where ItemCode = '"+SItemCode+"' and HODCode = "+iHODCode+" and MillCode = "+iMillCode+" ) ";
			 
			PreparedStatement ps = theMConnection.prepareStatement(QS15);
			ps.executeQuery();	
			ps.close();
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	
	private void insertItemstock(String SItemCode,int iHODCode,int iTransferStock,String sId)
	{
		try{
			
			String QS = 	" Insert into itemstock(MillCode,HODCode,ItemCode,Stock,StockValue,IndentQty,TransferQty,ReservedQty,ItemStockTransferId,Id,EntryDateTime,rdc ) " +
							" Values( "+iMillCode+","+iHODCode+",'"+SItemCode+"','"+iTransferStock+"', "+
							" 0,0,0,0,'"+sId+"',itemstock_seq.nextVal,SysDate,1  )";
							
							
				System.out.println("insert item stock==>"+QS)			;

				
				PreparedStatement ps = theMConnection.prepareStatement(QS);
				ps.executeQuery();
				ps.close();
				
				if(theMConnection  . getAutoCommit())
					 theMConnection  . setAutoCommit(false);
				
		}
		catch(Exception ex){
			ex.printStackTrace();
			bComflag  = false;
		}
	}
	
	private void updateItemStock(String SItemCode,int iHODCode,double dTotalStock,String sId){

		try{

			//String QS = "Update itemstock set Stock = "+dTotalStock+", ITEMSTOCKTRANSFERID="+sId+",ENTRYDATETIME = SysDate Where ItemCode = '"+SItemCode+"' and HODCode = "+iHODCode+" and MillCode = "+iMillCode+" ";
			
                         String QS = "Update itemstock set Stock = "+dTotalStock+", ENTRYDATETIME = SysDate Where ItemCode = '"+SItemCode+"' and HODCode = "+iHODCode+" and MillCode = "+iMillCode+" ";


                        System.out.println("update QS"+QS)        ;
			 
				

			PreparedStatement ps1 = theMConnection.prepareStatement(QS);
			ps1.executeQuery();	
			ps1.close();
			
			if(theMConnection  . getAutoCommit())
				 theMConnection  . setAutoCommit(false);
			
		}
		catch(Exception ex){
			ex.printStackTrace();
			bComflag  = false;
		}
	}
	
	private void updateLocation(String SItemCode,String SLocation){

		try{

			String QS3 = "Update InvItems set ItemLocName = '"+SLocation+"' Where Item_Code = '"+SItemCode+"' ";

			PreparedStatement ps1 = theMConnection.prepareStatement(QS3);
			ps1.executeQuery();	
			ps1.close();
			
			if(theMConnection  . getAutoCommit())
				 theMConnection  . setAutoCommit(false);
			
			
		}
		catch(Exception ex){
			bComflag  = false;
			ex.printStackTrace();
		}
	}

	private void updateLocation1(String SItemCode,String SLocation){

		try{

			String QS3 = "Update InvItems set LocName = '"+SLocation+"' Where Item_Code = '"+SItemCode+"' ";

			PreparedStatement ps1 = theMConnection.prepareStatement(QS3);
			ps1.executeQuery();	
			ps1.close();
			
			if(theMConnection  . getAutoCommit())
				 theMConnection  . setAutoCommit(false);
			
			
		}
		catch(Exception ex){
			bComflag  = false;
			ex.printStackTrace();
		}
	}
	
	private void printLabelNew(String SItemCode,String SItemName,String SLocationName,int iTransferStock)
     {
          try
          {
			
			SerialPort serialPort    = getSerialPort();
			serialPort               . setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
			OutputStream output      = serialPort.getOutputStream();
			  
			System.out.println("commin final "+serialPort);
			
			String SItemCod   	= SItemCode;
			String SItemCodd   	= "CODE: "+SItemCode;				
						
			String SItemNamee 	= "ITEM: "+SItemName;
			String SLoc 		= "LOC:  "+SLocationName;
			
			
			String SCatl = "CATL: ";
			String SDraw = "DRAW: ";
			String SQty  = "QNTY: "+iTransferStock;

			String SLocDraw = SCatl  +"  "+  SDraw;
			String SLocQty = SLoc  +"                 "+  SQty;


			String str 		= "";
			String substr 	= "";
			String substr2 	= "";
				
			if(SItemNamee.length()>28) {
					
				substr = SItemNamee.substring(0, 28);
				substr2 = SItemNamee.substring(28,SItemNamee.length());
				
			}else {
				substr = SItemNamee;	
			}	

			String Str2 = "191100301600065"+substr+"\r";     //191100501400070    191100300800070
			String Str4 = "191100301400100"+substr2+"\r";     //191100501400070    191100300800070
			String Str3 = "191100301200065"+SItemCodd+"\r";
			String Str5 = "191100301000065"+SCatl+"\r";
			String Str10 = "191100300800065"+SDraw+"\r";
			String Str7 = "191100300600065"+SLoc+"\r";
			String Str11 = "191100300400065"+SQty+"\r";
			String Str8 = "1e6205000000185"+SItemCod+"\r";
			
			/*
			
			String Str2 = "191100301600020"+substr+"\r";     //191100501400070    191100300800070
			String Str4 = "191100301400055"+substr2+"\r";     //191100501400070    191100300800070
			String Str3 = "191100301200020"+SItemCodd+"\r";
			String Str5 = "191100301000020"+SCatl+"\r";
			String Str10 = "191100300800020"+SDraw+"\r";
			String Str7 = "191100300600020"+SLoc+"\r";
			String Str11 = "191100300400020"+SQty+"\r";
			String Str8 = "1e6205000000140"+SItemCod+"\r";
			*/

			output.write("n".getBytes());
			output.write("f285".getBytes());
			output.write("L".getBytes());
			output.write("H10".getBytes());
			output.write("D11".getBytes());

	 
			output.write(Str3.getBytes());
			output.write(Str4.getBytes());
			output.write(Str5.getBytes());
			output.write(Str7.getBytes());
			output.write(Str2.getBytes()); 
			output.write(Str8.getBytes()); 
			output.write(Str10.getBytes()); 
			output.write(Str11.getBytes());
			output.write("E\r".getBytes()); 
                  
							  
			System.out.println("commin all print final");
					
			serialPort.close();
 
		  }catch(Exception ex)	    {
			  ex.printStackTrace();
		  }
	 }	  

     private SerialPort getSerialPort() 
     {
          SerialPort serialPort         = null;
     
          try
          {
               Enumeration portList     = CommPortIdentifier.getPortIdentifiers();
     
               while(portList.hasMoreElements())
               {
                    CommPortIdentifier portId = (CommPortIdentifier)portList.nextElement();

                    if(portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort     = (SerialPort)portId.open("comapp", 2000);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               
          }
     
          return serialPort;
     }
	
	
	
	 
	 

}

