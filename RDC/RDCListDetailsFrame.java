package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;



// pdf import
import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;

public class RDCListDetailsFrame extends JInternalFrame
{

     RDCCritPanel   TopPanel;
     JPanel         BottomPanel;

     JComboBox      JCStatus;
     DateField      TStDate,TEnDate;
     JButton        BApply,BCreatePdf;
     JButton        BPrint;
     JTextField     TFile;

     TabReport      tabreport;
     Object         RowData[][];
          
     String ColumnData[] = {"Rdc No","Rdc Date","Sent Through","Supplier Name","Description of Goods","Send Qty","Received Qty","Balance Qty","DC No","DC Date","GI No","GI Date","Purpose","Delay Days"};
     String ColumnType[] = {"N"     ,"S"       ,"S"           ,"S"            ,"S"                   ,"N"       ,"N"           ,"N"          ,"S"    ,"S"      ,"N"    ,"S"      ,"S"      ,"N"         };
     int iColWidth[]     = {80      ,80        ,100           ,250            ,250                   ,80        ,100           ,100          ,100    ,100      ,100    ,100      ,100      ,100         };
     
     Common common   = new Common();
     ORAConnection connect;
     Connection theconnect;

     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     int            iMillCode;
     String         SSupTable,SMillName;

     Vector         VRDCNo;
     Vector         VRDCDate;
     Vector         VThro;
     Vector         VSupName;
     Vector         VDescript;
     Vector         VQty;
     Vector         VRemarks;
     Vector         VDCNo;
     Vector         VDCDate;
     Vector         VGINo;
     Vector         VGIDate;
     Vector         VDelay;
     Vector         VRecQty;
     Vector         VPending;

     String         SHead1,SHead2,SHead3;

     FileWriter     FW;
     int iLctr = 100,iPctr=0;

     
   // pdf 
    
    Document document;
    PdfPTable table;
    int iTotalColumns = 14;
     int iWidth[] = {10, 10, 10, 20, 22, 10, 8, 8, 8, 10, 8,10,29,15};
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 11, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font bigNormal = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
    String SFile="";   
      String PDFFile = common.getPrintPath()+"/RDCListDetails.pdf";

     RDCListDetailsFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode,String SSupTable,String SMillName)
     {
          super("RDC List");

          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SMillName = SMillName;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel    = new RDCCritPanel(SSupTable);
          BottomPanel = new JPanel();

          JCStatus    = new JComboBox();
          TStDate     = new DateField();
          TEnDate     = new DateField();
          TFile       = new JTextField();

          BApply      = new JButton("Apply");
          BPrint      = new JButton("Print");
          BCreatePdf   = new JButton("Create pdf"); 
          TStDate.setTodayDate();
          TEnDate.setTodayDate();

          TFile.setText("RDCListDetails.prn");
          TFile.setEditable(false);
          BPrint.setEnabled(false);

          BApply.setMnemonic('A');
          BPrint.setMnemonic('P');
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
     }

     public void addComponents()
     {
          JCStatus.addItem("All");
          JCStatus.addItem("Received");
          JCStatus.addItem("Not Received");

          BottomPanel.add(BPrint);
          BottomPanel.add(TFile);
          BottomPanel. add(BCreatePdf);

          getContentPane().add(TopPanel,BorderLayout.NORTH);

          setDataIntoVector();
          setRowData();

          try
          {
               getContentPane().remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.setPrefferedColumnWidth1(iColWidth);

               getContentPane().add(tabreport,BorderLayout.CENTER);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          TopPanel.BApply.addActionListener(new AppList());
          BPrint.addActionListener(new PrintList());
          BCreatePdf.addActionListener(new PrintList());
     }

     public class AppList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==TopPanel.BApply)
               {
                    setDataIntoVector();
                    setRowData();
          
                    try
                    {
                         getContentPane().remove(tabreport);
                    }
                    catch(Exception ex){}
                    try
                    {
                         tabreport = new TabReport(RowData,ColumnData,ColumnType);
                         tabreport.setPrefferedColumnWidth1(iColWidth);
          
                         getContentPane().add(tabreport,BorderLayout.CENTER);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                         BPrint.setEnabled(true);
                         TFile.setEditable(true);
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }     
          }
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {

               if(ae.getSource() == BCreatePdf)
               {
                        createPDFFile();
                     
                      try{
                File theFile   = new File(PDFFile);
                Desktop        . getDesktop() . open(theFile);
                  

                  
               }catch(Exception ex){}

                       
                             // saveData();
                               // printPDF();
                        
                  
               }
               if(ae.getSource()==BPrint)
               {
                    try
                    {
                         iLctr = 100;
                         iPctr=0;

                         String SFile = TFile.getText();
                         if((SFile.trim()).length()==0)
                              SFile = "RDCDetails.prn";

                         FW      = new FileWriter(common.getPrintPath()+SFile);
                         setBody();
                         FW.close();
                    }
                    catch(Exception ex)
                    {
                          System.out.println(ex);
                    }
                    removeHelpFrame();
               }     
          }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();

          }
          catch(Exception ex){}
     }

     public void setDataIntoVector()
     {

          String SStDate = TopPanel.TStDate.toNormal();
          String SEnDate = TopPanel.TEnDate.toNormal();
          String SSupplier="";
          int iList=0;
          String QS = " SELECT RDC.RDCNo, RDC.RDCDate, RDC.Thro, "+SSupTable+".Name, "+ 
                      " RDC.Descript, RDC.Qty, RDC.Remarks, GateInwardRDC.DCNo, "+
                      " GateInwardRDC.DCDate, GateInwardRDC.GINo, GateInwardRDC.GIDate, "+
                      " RDC.RecQty, RDC.Qty - RDC.RecQty AS Pending "+
                      " FROM (RDC INNER JOIN "+SSupTable+" ON "+SSupTable+".Ac_Code = RDC.Sup_Code) "+ 
                      " LEFT JOIN GateInwardRDC ON ((RDC.Descript = GateInwardRDC.Descript) "+
                      " AND (RDC.RDCNo = GateInwardRDC.RDCNo) And (RDC.MillCode = GateInwardRDC.MillCode) "+
                      " And RDC.DocType=0) "+
                      " Where RDC.MillCode="+iMillCode+" And RDC.DocType=0 ";

          if(TopPanel.JRPeriod.isSelected())
          {
               QS = QS +  " And RDC.RDCDate > '"+SStDate+"' and RDC.RDCDate < '"+SEnDate+"' ";
          }
          else
          {
               QS = QS +  " And RDC.RDCNo >="+TopPanel.TStNo.getText()+" and RDC.RDCNo <= "+TopPanel.TEnNo.getText() ;
          }
          if(TopPanel.JRSeleSup.isSelected())
          {
               SSupplier= ""+SSupTable+".Ac_Code= '"+(String)TopPanel.VSupCode.elementAt(TopPanel.JCSup.getSelectedIndex())+"'";
               QS = QS +" and "+SSupplier;
          }
          if(TopPanel.JRSeleList.isSelected())
          {
               iList   = TopPanel.JCList.getSelectedIndex();
               if(iList==0)
               {
                    QS = QS + " AND RDC.Qty - RDC.RecQty = 0 ";
               }
               if(iList==1)
               {
                    QS = QS + " AND RDC.Qty - RDC.RecQty > 0 "; 
               }
          }
          if(TopPanel.JCOrder.getSelectedIndex() == 0)
               QS = QS+" Order By RDC.RDCNo,RDC.RDCDate";
          if(TopPanel.JCOrder.getSelectedIndex() == 1)
               QS = QS+" Order By "+SSupTable+".Name,RDC.RDCDate";


          VRDCNo   = new Vector();
          VRDCDate = new Vector();
          VThro    = new Vector();
          VSupName = new Vector();
          VDescript= new Vector();
          VQty     = new Vector();
          VRemarks = new Vector();
          VDCNo    = new Vector();
          VDCDate  = new Vector();
          VGINo    = new Vector();
          VGIDate  = new Vector();
          VDelay   = new Vector();
          VRecQty  = new Vector();
          VPending = new Vector();

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res   = stat.executeQuery(QS);

               while (res.next())
               {
                    VRDCNo   .addElement(common.parseNull(res.getString(1)));

                    String SRDCDate = common.parseDate(res.getString(2));
                    VRDCDate .addElement(SRDCDate);

                    VThro    .addElement(common.parseNull(res.getString(3)));
                    VSupName .addElement(common.parseNull(res.getString(4)));
                    VDescript.addElement(common.parseNull(res.getString(5)));
                    VQty     .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(6))),3));
                    VRemarks .addElement(common.parseNull(res.getString(7)));
                    VDCNo    .addElement(common.parseNull(res.getString(8)));
                    VDCDate  .addElement(common.parseDate(res.getString(9)));
                    VGINo    .addElement(common.parseNull(res.getString(10)));

                    String SGIDate = common.parseDate(res.getString(11));
                    VGIDate  .addElement(SGIDate);

                    String SDelay = "";

                    if(SGIDate.equals(""))
                         SDelay  = "Not Received";
                    else
                         SDelay  = common.getDateDiff(SGIDate,SRDCDate);

                    VDelay   .addElement(SDelay);
                    VRecQty  .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(12))),3));
                    VPending .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(13))),3));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VGINo.size()][ColumnData.length];

          for(int i=0;i<VGINo.size();i++)
          {
               RowData[i][0]  = (String)VRDCNo    .elementAt(i);
               RowData[i][1]  = (String)VRDCDate  .elementAt(i);
               RowData[i][2]  = (String)VThro     .elementAt(i);
               RowData[i][3]  = (String)VSupName  .elementAt(i);
               RowData[i][4]  = (String)VDescript .elementAt(i);
               RowData[i][5]  = (String)VQty      .elementAt(i);
               RowData[i][6]  = (String)VRecQty   .elementAt(i);
               RowData[i][7]  = (String)VPending  .elementAt(i);
               RowData[i][8]  = (String)VDCNo     .elementAt(i);
               RowData[i][9]  = (String)VDCDate   .elementAt(i);
               RowData[i][10] = (String)VGINo     .elementAt(i);
               RowData[i][11] = (String)VGIDate   .elementAt(i);
               RowData[i][12] = (String)VRemarks  .elementAt(i);
               RowData[i][13] = (String)VDelay    .elementAt(i);
          }
     }

     private void setHead() throws Exception
     {
          SHead1 =  common.Pad("RDC No"                ,12)+common.Space(2)+
                    common.Pad("RDC Date"              ,12)+common.Space(2)+
                    common.Pad("Sent Through"          ,12)+common.Space(2)+
                    common.Pad("Supplier"              ,25)+common.Space(2)+
                    common.Pad("Material Description"  ,25)+common.Space(2)+
                    common.Rad("Send Qty"              ,12)+common.Space(2)+
                    common.Rad("Recd Qty"              ,12)+common.Space(2)+
                    common.Rad("Balance Qty"           ,12)+common.Space(2)+
                    common.Pad("DC No."                ,12)+common.Space(2)+
                    common.Pad("DC Date"               ,12)+common.Space(2)+
                    common.Pad("GI No"                 ,12)+common.Space(2)+
                    common.Pad("GI Date"               ,12)+common.Space(2)+
                    common.Pad("Purpose"               ,12)+common.Space(2)+
                    common.Rad("Delay Days"            ,12)+common.Space(2);

          SHead2 =  common.Rad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",25)+common.Space(2)+
                    common.Pad("",25)+common.Space(2)+
                    common.Rad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Rad("",12)+common.Space(2)+
                    common.Rad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",25)+common.Space(2);

          SHead3 = common.Replicate("-",SHead1.length());

          if(iLctr < 60)
               return ;
          if(iPctr > 0)
          {     
               FW.write(SHead3+"\n");
               FW.write("\n");
          }
          iPctr++;

          String SStDate = TopPanel.TStDate.toNormal();
          String SEnDate = TopPanel.TEnDate.toNormal();

          String str1 = "Company : "+SMillName;
          String str2 = "Document : Returnable Material Cum Receipt (Both Returnable & Service) ";
          String str3 = "Period   : "+common.parseDate(SStDate)+"-"+common.parseDate(SEnDate);
          String str4 = "Page     : "+iPctr+"";

          FW.write("P"+str1+"\n");
          FW.write(str2+"\n");
          FW.write(str3+"\n");
          FW.write(str4+"\n");
          FW.write(SHead3+"\n");
          FW.write(SHead1+"\n");
          FW.write(SHead2+"\n");
          FW.write(SHead3+"\n");

          iLctr = 8;
     }      

     private void setBody() throws Exception
     {
          for(int i=0;i<VRDCNo.size();i++)
          {
               setHead();

               RowData[i][0]  = (String)VRDCNo    .elementAt(i);
               RowData[i][1]  = (String)VRDCDate  .elementAt(i);
               RowData[i][2]  = (String)VThro     .elementAt(i);
               RowData[i][3]  = (String)VSupName  .elementAt(i);
               RowData[i][4]  = (String)VDescript .elementAt(i);
               RowData[i][5]  = (String)VQty      .elementAt(i);
               RowData[i][6]  = (String)VRecQty   .elementAt(i);
               RowData[i][7]  = (String)VPending  .elementAt(i);
               RowData[i][8]  = (String)VDCNo     .elementAt(i);
               RowData[i][9]  = (String)VDCDate   .elementAt(i);
               RowData[i][10] = (String)VGINo     .elementAt(i);
               RowData[i][11] = (String)VGIDate   .elementAt(i);
               RowData[i][12] = (String)VRemarks  .elementAt(i);
               RowData[i][13] = (String)VDelay    .elementAt(i);

               String str = common.Pad((String)VRDCNo       .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VRDCDate     .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VThro        .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VSupName     .elementAt(i),25)+common.Space(2)+
                            common.Pad((String)VDescript    .elementAt(i),25)+common.Space(2)+
                            common.Rad((String)VQty         .elementAt(i),12)+common.Space(2)+
                            common.Rad((String)VRecQty      .elementAt(i),12)+common.Space(2)+
                            common.Rad((String)VPending     .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VDCNo        .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VDCDate      .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VGINo        .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VGIDate      .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VRemarks     .elementAt(i),12)+common.Space(2)+
                            common.Rad((String)VDelay       .elementAt(i),12)+common.Space(2);

               FW.write(str+"\n");
               iLctr++;
          }
          FW.write(SHead3+"\n");
          String SDateTime = common.getServerDateTime2();
          String SEnd = "Report Taken on "+SDateTime+"\n";
          FW.write(SEnd);
     }
private void createPDFFile() {
        try {
           // String PDFFile = "D://DepoShadePrint.pdf";
            document = new Document(PageSize.A4.rotate());
            PdfWriter.getInstance(document, new FileOutputStream(PDFFile));
            document.open();
            int iColumnCount = tabreport.ReportTable.getColumnCount();
            System.out.println("Inside CreatePDF -->" + iColumnCount);

            table = new PdfPTable(tabreport.ReportTable.getColumnCount());
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            table.setHeaderRows(1);
            table.getRowHeight(10);


            addHead(document, table);
            addBody(document, table);

          String SDateTime = common.getServerDateTime2();
          String SEnd = "Report Taken on "+SDateTime+" ";

           Paragraph paragraph;

            String Str1 = SEnd;
            //String Str2   = "Deduction Details";

            paragraph = new Paragraph(Str1, bigbold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            paragraph.setSpacingAfter(10);
            document.add(paragraph);
            document.close();
            JOptionPane.showMessageDialog(null, "PDF File Created in " + PDFFile, "Info", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addHead(Document document, PdfPTable table) throws BadElementException {
        try {

            String SStDate  = TStDate.toNormal();
          String SEnDate  = TEnDate.toNormal();
           
            document.newPage();
            Paragraph paragraph;

            String Str1 = "Company : AMARJOTHI SPINNING MILLS LTD ";
            //String Str2   = "Deduction Details";

            paragraph = new Paragraph(Str1, bigbold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            paragraph.setSpacingAfter(10);
            document.add(paragraph);//Document : RDC List 
            
            String Str2 = "Document : Returnable Material Cum Receipt (Both Returnable & Service)";
            //String Str2   = "Deduction Details";

            paragraph = new Paragraph(Str2, bigbold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            paragraph.setSpacingAfter(10);
            document.add(paragraph);
            
           String Str3 = "Period   : "+common.parseDate(SStDate)+"-"+common.parseDate(SEnDate)+"";
            //String Str2   = "Deduction Details";

            paragraph = new Paragraph(Str3, bigbold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            paragraph.setSpacingAfter(10);
            document.add(paragraph);

	/*	AddCellIntoTable("AMARJOTHI SPINNING MILLS LTD", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 12, 0, 0, 0, 0, smallbold);
		AddCellIntoTable("Document : RDC List  ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 12, 0, 0, 0, 0, smallbold);  
		AddCellIntoTable("Period   : "+common.parseDate(SStDate)+"-"+common.parseDate(SEnDate), table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 12, 0, 0, 0, 0, smallbold);
		*/	

            PdfPCell c1;


            for (int i = 0; i < tabreport.ReportTable.getColumnCount(); i++) {
                c1 = new PdfPCell(new Phrase(tabreport.ReportTable.getColumnName(i), mediumbold));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                c1.setRowspan(1);
                table.addCell(c1);
            }
           // Lctr = 16;
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    private void addBody(Document document, PdfPTable table) throws BadElementException {
        try {
            Paragraph paragraph;
            PdfPCell c1;
         

            for (int i = 0; i < tabreport.ReportTable.getRowCount(); i++) {

                for (int j = 0; j < tabreport.ReportTable.getColumnCount(); j++) {
                     if(j==2 ){
                  c1 = new PdfPCell(new Phrase(common.Pad((String) tabreport.ReportTable.getValueAt(i, j),7), smallnormal));}
		   else  if(j==3 ){
                  c1 = new PdfPCell(new Phrase(common.Pad((String) tabreport.ReportTable.getValueAt(i, j),15), smallnormal));}
                  else   if(j==4 ){
                  c1 = new PdfPCell(new Phrase(common.Pad((String) tabreport.ReportTable.getValueAt(i, j),20), smallnormal));}
                  else   if(j==12 ){
                  c1 = new PdfPCell(new Phrase(common.Pad((String) tabreport.ReportTable.getValueAt(i, j),20), smallnormal));}
		   else
                    c1 = new PdfPCell(new Phrase((String) tabreport.ReportTable.getValueAt(i, j), smallnormal));
                     if(j==5 || j==6 || j==7){
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);}
                   else  c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
                    table.addCell(c1);
                }
            }


            document.add(table);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("ex" + ex);
        }
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
}
