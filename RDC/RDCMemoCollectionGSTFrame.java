package RDC;         
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;
import jdbc.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCMemoCollectionGSTFrame extends JInternalFrame
{
     JLayeredPane   Layer;
     Vector         VSeleMemoNo;
     String         SDocType;
     int iUserCode,iMillCode;
     RDCMemoPendingFrame rdcmemopendingframe;
     String SSupTable,SYearCode,SItemTable;
    String  SStateCode,SPartyTypeCode;
     JPanel  TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopCenter;
     JPanel  Top1Panel,Top2Panel;

     MyLabel     LMemoNo,LMemoDate,LHodName;
     JComboBox   JCType,JCDocType,JCAsset;
     JTextField  TSupCode;
     DateField   TDate,DRefDate,DInwardDate,DGiDate,DBillDate,DMbdDate;
     JButton     BSupplier;
     MyTextField TSlNo,TSentThro,TRefNo,TInwardNo,TGiNo,TBillNo,TPerson,TMbdNo;

     JButton BOk;

     Object RowData[][];

     String ColumnData[]={"Sl No","Item Code","Item Name"," HSN Code","Description","Purpose","Uom","Department","Unit","Due Date","Quantity","Rate"," Value"};
     String ColumnType[]={"N"    ,"S"    ,"S"," S " ,"N"     ,"S"      ,"S"  ,"S"         ,"S"   ,"S"       ,"N"       ,"E","N"  };

     //TabReport      tabreport;
     Vector         VRDCMemoRec;
     Vector         VDept,VUnit,VDeptCode,VUnitCode,VUom,VUomCode;


     String SRDCNo = "",SDate="";

     String SMemoNo="",SMemoDate="",SSupName="",SRSupCode="",SHodCode="",SHodName="",SVehicle="",SRepStatus="";

     Vector VMemoNo,VDesc,VPurpose,VRUom,VRDept,VRUnit,VDueDate,VQty,VSlNo,VMUserCode,VRepStatus,VRepNo,VRepCode,VItemName,VItemCode,VHsnCode;

     Common   common = new Common();
     ORAConnection connect;
     Connection theconnect;

     RDCOutwardTabReport      tabreport;

     boolean bComflag = true;

     RDCMemoCollectionGSTFrame(JLayeredPane Layer,Vector VSeleMemoNo,String SDocType,int iUserCode,int iMillCode,RDCMemoPendingFrame rdcmemopendingframe,String SSupTable,String SYearCode,String SItemTable)
     {
          this.Layer       = Layer;
          this.VSeleMemoNo = VSeleMemoNo;
          this.SDocType    = SDocType;
          this.iUserCode   = iUserCode;
          this.iMillCode   = iMillCode;
          this.rdcmemopendingframe = rdcmemopendingframe;
          this.SSupTable   = SSupTable;
          this.SYearCode   = SYearCode;
          this.SItemTable   =SItemTable;

		  try {

          setParamVectors();
          createComponents();
         
          setLayouts();
          addComponents();
          addListeners();

		  
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
		  
		  
     }

     public void createComponents()
     {
          TopPanel       = new JPanel(true);
          Top1Panel      = new JPanel();
          Top2Panel      = new JPanel();
          TopLeft        = new JPanel();
          TopRight       = new JPanel();
          TopCenter      = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();

          LMemoNo        = new MyLabel("");
          LMemoDate      = new MyLabel("");
          LHodName       = new MyLabel("");
          JCType         = new JComboBox();
          JCDocType      = new JComboBox();
          JCAsset        = new JComboBox();
          TSlNo          = new MyTextField(8);
          TSupCode       = new JTextField();
          TDate          = new DateField();
          DRefDate       = new DateField();
          DInwardDate    = new DateField();
          DGiDate        = new DateField();
          DBillDate      = new DateField();
          BSupplier      = new JButton("Select a Workshop/Supplier");
          TSentThro      = new MyTextField(20);
          TRefNo         = new MyTextField(20);
          TInwardNo      = new MyTextField(20);
          TGiNo          = new MyTextField(20);
          TBillNo        = new MyTextField(20);
          TPerson        = new MyTextField(20);
          TMbdNo         = new MyTextField(20);
          DMbdDate       = new DateField();

          BOk            = new JButton("Save & Exit");

          VRDCMemoRec    = new Vector();

         
        

          TDate.fromString1(SDate);

          JCType.addItem("Regular");
          JCType.addItem("Non-Regular");

          JCDocType.addItem("Returnable");
          JCDocType.addItem("Non-Returnable");

          JCAsset.addItem("No");
          JCAsset.addItem("Yes");
          JCAsset.setSelectedItem("No");


          if(iMillCode==0)
          {
               JCType.setEnabled(true);
          }
          else
          {
               JCType.setEnabled(false);
          }


          BOk.setMnemonic('S');
          int iDType = JCDocType.getSelectedIndex();
          int iRType = JCType.getSelectedIndex();
          setRDCNo(iDType,iRType);
          TSlNo.setEnabled(false);
     }
     public void setLayouts()
     {
          TopPanel.setLayout(new BorderLayout());
          Top1Panel.setLayout(new GridLayout(1,6));
          Top2Panel.setLayout(new GridLayout(1,3));
          TopLeft.setLayout(new GridLayout(6,2));
          TopLeft.setBorder(new TitledBorder("RDC Info"));
          TopRight.setLayout(new GridLayout(6,2));
          TopRight.setBorder(new TitledBorder("RDC Info"));
          TopCenter.setLayout(new GridLayout(6,2));
          TopCenter.setBorder(new TitledBorder("RDC Info"));
          BottomPanel.setLayout(new FlowLayout());
          MiddlePanel.setLayout(new BorderLayout());
          Top1Panel.setBorder(new TitledBorder("Memo Info"));
          MiddlePanel.setBorder(new TitledBorder("RDC List"));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,850,500);
          setTitle("RDC Against MEMO for Materials being sent out for Repairs and Maintenance");
     }
     public void addComponents()
     {
          TopLeft.add(new MyLabel("Document Type"));
          TopLeft.add(JCDocType);

          TopLeft.add(new MyLabel("RDC Type"));
          TopLeft.add(JCType);

          TopLeft.add(new MyLabel("Asset"));
          TopLeft.add(JCAsset);

          TopLeft.add(new MyLabel("RDC Sl No"));
          TopLeft.add(TSlNo);

          TopLeft.add(new MyLabel("RDC Date"));
          TopLeft.add(TDate);

          TopLeft.add(new MyLabel("Workshop/Lathe/Supplier"));
          TopLeft.add(BSupplier);

          TopCenter.add(new MyLabel("Vehicle to Sent Thro"));
          TopCenter.add(TSentThro);

          TopCenter.add(new MyLabel("Name of the person"));
          TopCenter.add(TPerson);

          TopCenter.add(new MyLabel("MBD Number"));
          TopCenter.add(TMbdNo);

          TopCenter.add(new MyLabel("MBD Date"));
          TopCenter.add(DMbdDate);

          TopCenter.add(new MyLabel("Ref.No"));
          TopCenter.add(TRefNo);

          TopCenter.add(new MyLabel("Ref.Date"));
          TopCenter.add(DRefDate);

          TopRight.add(new MyLabel("InwardNo"));
          TopRight.add(TInwardNo);

          TopRight.add(new MyLabel("InwardDate"));
          TopRight.add(DInwardDate);

          TopRight.add(new MyLabel("GI No"));
          TopRight.add(TGiNo);

          TopRight.add(new MyLabel("GI Date"));
          TopRight.add(DGiDate);

          TopRight.add(new MyLabel("Bill No"));
          TopRight.add(TBillNo);

          TopRight.add(new MyLabel("Bill Date"));
          TopRight.add(DBillDate);

          Top1Panel.add(new MyLabel("Memo No"));
          Top1Panel.add(LMemoNo);
          Top1Panel.add(new MyLabel("Memo Date"));
          Top1Panel.add(LMemoDate);
          Top1Panel.add(new MyLabel("User Department"));
          Top1Panel.add(LHodName);

          Top2Panel.add(TopLeft);
          Top2Panel.add(TopCenter);
          Top2Panel.add(TopRight);

          TopPanel.add("North",Top1Panel);
          TopPanel.add("Center",Top2Panel);

          BottomPanel.add(new MyLabel(" F2  ======> To Get Item Data"));
          BottomPanel.add(BOk);
          RowData = new Object[30][ColumnData.length];

          for(int i=0;i<30;i++)
          {
               for(int j=0;j<ColumnData.length;j++)
               {
                    RowData[i][j]="";
               }
               RowData[i][0]=""+(i+1);
               VRDCMemoRec.addElement(new RDCMemoRecGst(Layer,VUom,VDept,VUnit,VDeptCode,VUnitCode,RowData,String.valueOf(i),this,TDate.toNormal()));
             
          }
 
          MiddlePanel.add("Center",tabreport=new RDCOutwardTabReport(RowData,ColumnData,ColumnType));
          tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        
          getContentPane() .add("North",TopPanel);
          getContentPane() .add("Center",MiddlePanel);
          getContentPane() .add("South",BottomPanel);
     }

     public void addListeners()
     {
          BSupplier .addActionListener(new SupplierSearch(Layer,TSupCode,SSupTable));
          BOk       .addActionListener(new ActList());
          JCDocType .addItemListener(new ItemList());
          JCType    .addItemListener(new ItemList());
          tabreport .ReportTable.addKeyListener(new KeyList());
     }

     private class ItemList implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
               if(ie.getSource()==JCDocType)
               {
                    try
                    {
                         if(iMillCode==0)
                         {
                              if((JCDocType.getSelectedIndex())==1)
                              {
                                   JCType.setSelectedIndex(0);
                                   JCType.setEnabled(false);
                              }
                              else
                              {
                                   JCType.setEnabled(true);
                              }
                         }
                         else
                         {
                              JCType.setSelectedIndex(0);
                              JCType.setEnabled(false);
                         }
                    }
                    catch(Exception e)
                    {
                         System.out.println(e);
                         e.printStackTrace();
                    }
               }
               if(ie.getSource()==JCDocType || ie.getSource()==JCType)
               {
                    int iDocType = JCDocType.getSelectedIndex();
                    int iRdcType = JCType.getSelectedIndex();
                    setRDCNo(iDocType,iRdcType);
               }
          }

     }
     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(false);
               boolean bSig = false;
               try
               {
                     bSig = allOk();
               }
               catch(Exception ex)
               {
                     System.out.println(ex);
                     ex.printStackTrace();
               }
               if(bSig)
               {
                    try
                    {
                         int iDocType = JCDocType.getSelectedIndex();
                         int iRdcType = JCType.getSelectedIndex();
                         setRDCNo(iDocType,iRdcType);
                         insertRDC();
                         updateMemo();
                         updateOrderNo(iDocType,iRdcType);
                    }
                    catch(Exception Ex)
                    {
                         Ex.printStackTrace();
                         bComflag = false;
                    }

                    try
                    {
                         if(bComflag)
                         {
                              theconnect     . commit();
                              System         . out.println("Commit");
                              theconnect     . setAutoCommit(true);
                              removeHelpFrame();
                              rdcmemopendingframe . addComponents();
                         }
                         else
                         {
                              theconnect     . rollback();
                              System         . out.println("RollBack");
                              theconnect     . setAutoCommit(true);
                              JOptionPane.showMessageDialog(null,"Problem in Save","Error",JOptionPane.ERROR_MESSAGE);
                              BOk.setEnabled(true);
                         }
                    }catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
               }
               else
               {
                     BOk.setEnabled(true);
               }
          }
     }
     public boolean allOk()
     {
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               
               for(int i=0;i<VSeleMemoNo.size();i++)
               {
                    int iRDCStatus=0;

                    Statement stat   = theconnect.createStatement();

                    String SSeleMemoNo = (String)VSeleMemoNo.elementAt(i);

                    String QS = " Select Max(RDCStatus) from RDCMemo "+
                                " Where MemoNo="+SSeleMemoNo+" and MillCode="+iMillCode+" and IsDelete=0";
     
                    ResultSet res = stat.executeQuery(QS);
                    while(res.next())
                    {
                         iRDCStatus = res.getInt(1);
                    }
                    res.close();
                    stat.close();

                    if(iRDCStatus>0)
                    {
                         JOptionPane.showMessageDialog(null,"Memo No -"+SSeleMemoNo+" Already Processed","Error",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }
               }


               String SSupCode     = TSupCode.getText();
               String SRDCDate     = TDate.toString();
               String SCurDate     = common.getServerDate();
               int    iDateDiff    = common.toInt(common.getDateDiff(SRDCDate,SCurDate));
               int    iDueDate     = 0;
               int    iRDCDate     = common.toInt(TDate.toNormal());
               int    iMemoDate    = common.toInt(SMemoDate);
               int    iCount       = 0;
              
     
               if(iRDCDate<iMemoDate)
               {
                    JOptionPane.showMessageDialog(null,"Invalid Date","Error",JOptionPane.ERROR_MESSAGE);
                    TDate.TDay.requestFocus();
                    return false;
               }
     
               for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
               {
                    if(common.toDouble((String)tabreport.ReportTable.getValueAt(i,10))<=0)
                         continue;
      
                    iCount++;
      
                    iDueDate     = common.toInt(common.pureDate((String)tabreport.ReportTable.getValueAt(i,9)));
                    if(iDueDate!=iRDCDate)
                    {
                         if(iDueDate<=iRDCDate)
                         {
                               JOptionPane.showMessageDialog(null,"Invalid Date","Error",JOptionPane.ERROR_MESSAGE);
                               TDate.TDay.requestFocus();
                               return false;
                         }
                    }

                     String SItemCode=(String)tabreport.ReportTable.getValueAt(i,1);
                     String SItemName=(String)tabreport.ReportTable.getValueAt(i,2);
                     String SHsncode=(String)tabreport.ReportTable.getValueAt(i,3);

                     if(SItemCode.equals("") || SItemName.equals("") || SHsncode.equals(""))
                     {

                         JOptionPane.showMessageDialog(null,"ItemCode/ItemName/HSNCode is Empty","Error",JOptionPane.ERROR_MESSAGE);
                         return false;
                     }


                   
                   String SRate=(String)tabreport.ReportTable.getValueAt(i,11);

                   if(SRate.equals("")){
                      
                               JOptionPane.showMessageDialog(null,"Rate Field is Empty","Error",JOptionPane.ERROR_MESSAGE);
                               return false;
                   }
         

                  
               }
     
               if(iCount<=0)
               {
                    JOptionPane.showMessageDialog(null,"No Entries Made","Error",JOptionPane.ERROR_MESSAGE);
                    tabreport.ReportTable.requestFocus();
                    return false;
               }
     
               if((TSlNo.getText().trim()).length()==0)
               {
                    JOptionPane.showMessageDialog(null,"RDC Slip No Must be filled","Error",JOptionPane.ERROR_MESSAGE);
                    TSlNo.requestFocus();
                    return false;
               }
               if(SSupCode.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Invalid Supplier","Error",JOptionPane.ERROR_MESSAGE);
                    BSupplier.requestFocus();
                    return false;
               }
               if((TSentThro.getText().trim()).length()==0)
               {
                    JOptionPane.showMessageDialog(null,"Sent Thro Must be filled","Error",JOptionPane.ERROR_MESSAGE);
                    TSentThro.requestFocus();
                    return false;
               }

              
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               return false;
          }

          return true;
     }

     private void insertRDC()
     {

          String SItemCode   ="";
          String SItemName   ="";
          String SHSNCode    = "";
          String SDescription = "";
          String SRemarks     = "";
          String SUnitName    = "";
          String SUomName     = "";
          String SDeptName    = "";
          String SDueDate     = "";
          double dQty         = 0;
          int    iSlNo        = 0;
          double dValue       =0;
          double dRate        =0;
 
          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
          {
               dQty           = common.toDouble((String)tabreport.ReportTable.getValueAt(i,10));
 
               if(dQty<=0)
                    continue;
 
               iSlNo++;
 
               SItemCode           = (String)tabreport.ReportTable.getValueAt(i,1);
               SItemName           = (String)tabreport.ReportTable.getValueAt(i,2);
               SHSNCode           = (String)tabreport.ReportTable.getValueAt(i,3);
               SDescription        = common.getNarration((String)tabreport.ReportTable.getValueAt(i,4));
               SRemarks            = common.getNarration((String)tabreport.ReportTable.getValueAt(i,5));
               SUnitName           = (String)tabreport.ReportTable.getValueAt(i,8);
               int iUnitCode       =  getUnitCode(SUnitName);
               SUomName            = (String)tabreport.ReportTable.getValueAt(i,6);
               int iUomCode        =  getUomCode(SUomName);
               SDeptName           = (String)tabreport.ReportTable.getValueAt(i,7);
               int iDeptCode       =  getDeptCode(SDeptName);
               SDueDate            = common.pureDate((String)tabreport.ReportTable.getValueAt(i,9));
               int iMemoSlNo       = common.toInt((String)VSlNo.elementAt(i));
               String SMemoNo      = (String)VMemoNo.elementAt(i);
               int iMUserCode      = common.toInt((String)VMUserCode.elementAt(i));
               String SRepNoStatus = (String)VRepStatus.elementAt(i);
               String SRepNo       = (String)VRepNo.elementAt(i);
               String SRepCode     = (String)VRepCode.elementAt(i);


               dRate               = common.toDouble((String)tabreport.ReportTable.getValueAt(i,11));
               dValue              = common.toDouble((String)tabreport.ReportTable.getValueAt(i,12));
              
 
               insertData(SMemoNo,SDescription,SRemarks,iUnitCode,iUomCode,iDeptCode,SDueDate,dQty,iSlNo,iMemoSlNo,iMUserCode,SRepNoStatus,SRepNo,SRepCode,dRate,dValue,SItemCode,SItemName,SHSNCode);
          }
    }
    public void insertData(String SMemoNo,String SDescription,String SRemarks,int iUnitCode,int iUomCode,int iDeptCode,String SDueDate,double dQty,int iSlNo,int iMemoSlNo,int iMUserCode,String SRepNoStatus,String SRepNo,String SRepCode,double dRate,double dValue,String SItemCode,String SItemName,String SHSNCode)
    {
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               String Qry=" insert into RDC(ID,RDCType,RDCNo,RDCDate,Sup_Code,Descript,Dept_Code,Unit_Code,Remarks,Qty,DueDate,Thro,RecQty,ValId,SlNo,UserCode,ModiDate,MillCode,AssetFlag,Status,Flag,DocType,RefNo,RefDate,RefInwNo,RefInwDate,RefGiNo,RefGiDate,RefBillNo,RefBillDate,Person,UomCode,MBDNo,MBDDate,GodownAsset,MemoNo,HodCode,MemoSlNo,AuthStatus,MemoAuthUserCode,RepNoStatus,RepNo,Item_Code,RDCrate,RDCvalue,HSNMATCODE) values(RDC_seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
               PreparedStatement prepare=theconnect.prepareStatement(Qry);

               int iStatus = 0;

               if(JCDocType.getSelectedIndex()==0) {

                    iStatus = 1;
               }

               System.out.println("Doc Type Index-->"+JCDocType.getSelectedIndex());
               System.out.println("Status"+iStatus);

 
               prepare.setInt(1,JCType.getSelectedIndex());
               prepare.setInt(2,common.toInt((String)TSlNo.getText()));
               prepare.setString(3,TDate.toNormal());
               prepare.setString(4,TSupCode.getText());
               prepare.setString(5,SDescription.toUpperCase());
               prepare.setInt(6,iDeptCode);
               prepare.setInt(7,iUnitCode);
               prepare.setString(8,SRemarks.toUpperCase());
               prepare.setDouble(9,dQty);
               prepare.setString(10,SDueDate);
               prepare.setString(11,(TSentThro.getText()).toUpperCase());
               prepare.setDouble(12,0);
               prepare.setInt(13,0);
               prepare.setInt(14,iSlNo);
               prepare.setInt(15,iUserCode);
               prepare.setString(16,SDate);
               prepare.setInt(17,iMillCode);
               prepare.setInt(18,0);
               prepare.setInt(19,0);
               prepare.setInt(20,0);
               prepare.setInt(21,JCDocType.getSelectedIndex());
               prepare.setString(22,(TRefNo.getText()).toUpperCase());
               prepare.setString(23,DRefDate.toNormal());
               prepare.setInt(24,(common.toInt((String)TInwardNo.getText())));
               prepare.setString(25,DInwardDate.toNormal());
               prepare.setInt(26,(common.toInt((String)TGiNo.getText())));
               prepare.setString(27,DGiDate.toNormal());
               prepare.setString(28,(TBillNo.getText()).toUpperCase());
               prepare.setString(29,DBillDate.toNormal());
               prepare.setString(30,(TPerson.getText()).toUpperCase());
               prepare.setInt(31,iUomCode);
               prepare.setString(32,(TMbdNo.getText()).toUpperCase());
               prepare.setString(33,DMbdDate.toNormal());
               prepare.setInt(34,JCAsset.getSelectedIndex());
               prepare.setString(35,SMemoNo);
               prepare.setString(36,SHodCode);
               prepare.setInt(37,iMemoSlNo);
               prepare.setInt(38,iStatus);
               prepare.setInt(39,iMUserCode);
               prepare.setString(40,SRepNoStatus);
               prepare.setString(41,SRepNo);
               prepare.setString(42,SRepCode);
               prepare.setDouble(43,dRate);
               prepare.setDouble(44,dValue);
               prepare.setString(45,SItemCode);
         

 
               if(theconnect.getAutoCommit())
                         theconnect . setAutoCommit(false);
 
               prepare.executeUpdate();
               prepare.close();
          }
          catch(Exception e)
          {
               bComflag = false;
               System.out.println(e);
               e.printStackTrace();
          }
     }

     private void updateMemo()
     {
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();

               if(theconnect.getAutoCommit())
                         theconnect . setAutoCommit(false);
               

               for(int i=0;i<VSeleMemoNo.size();i++)
               {
                    String SSeleMemoNo = (String)VSeleMemoNo.elementAt(i);

                    String QS = " Update RDCMemo set RDCStatus=1,RDCNo="+common.toInt((String)TSlNo.getText())+
                                " Where MemoNo="+SSeleMemoNo+" and HodCode="+SHodCode+
                                " and DocType="+SDocType+" and MillCode="+iMillCode+" and IsDelete=0";
     
                    stat.executeUpdate(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void updateOrderNo(int iDocType,int iRdcType)
     {
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();

               if(theconnect.getAutoCommit())
                         theconnect . setAutoCommit(false);
               
               if(iDocType==0 && iRdcType==0)
               {
                    String QS1 = " Update Config"+iMillCode+""+SYearCode+" Set MaxNo=MaxNo+1 Where ID=7 ";
                    stat.executeUpdate(QS1);
               }
               else if(iDocType==0 && iRdcType==1)
               {
                    String QS2 = " Update Config"+iMillCode+""+SYearCode+" Set MaxNo=MaxNo+1 Where ID=8 ";
                    stat.executeUpdate(QS2);
               }
               else if(iDocType==1 && iRdcType==0)
               {
                    String QS3 = " Update Config"+iMillCode+""+SYearCode+" Set MaxNo=MaxNo+1 Where ID=9 ";
                    stat.executeUpdate(QS3);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setRDCNo(int iDocType,int iRdcType)
     {
          try
          {
               String QS = "";

               if(iDocType==0 && iRdcType==0)
               {
                    QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" Where Id=7";
               }
               else if(iDocType==0 && iRdcType==1)
               {
                    QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" Where Id=8";
               }
               else if(iDocType==1 && iRdcType==0)
               {
                    QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" Where Id=9";
               }

               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();
               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    SRDCNo = result.getString(1);
               }
               result.close();
               stat.close();

               int iRDCNo = common.toInt(SRDCNo)+1;
               SRDCNo     = String.valueOf(iRDCNo);
               TSlNo.setText(SRDCNo);
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    if(common.toDouble(SRepStatus)<=0)
                    {
                         int iRow            = tabreport.ReportTable.getSelectedRow();
                         String SDescription = (String)tabreport.ReportTable.getValueAt(iRow,4);
     
                         if(!SDescription.equals(""))
                         {
                              RDCMemoRecGst rdcmemorec=(RDCMemoRecGst)VRDCMemoRec.elementAt(tabreport.ReportTable.getSelectedRow());
                              rdcmemorec.setDueDate();
                              rdcmemorec.setActivation();
                              rdcmemorec.onThisFrame();
                         }
                    }
               }
           
            if(ke.getKeyCode()==KeyEvent.VK_F2) 
		    {
			try{ 
			
           		PartyCheck();


                        if(common.toInt(SStateCode) <= 0  &&   common.toInt(SPartyTypeCode) <= 0 ){

                         JOptionPane.showMessageDialog(null,"State code/Party Classification  not Updated","Information",JOptionPane.INFORMATION_MESSAGE);

                         }else{

                         

                         int iRow            = tabreport.ReportTable.getSelectedRow();
                         String SDescription = (String)tabreport.ReportTable.getValueAt(iRow,4);
     
                         if(!SDescription.equals(""))
                         {

				MaterialPicker materialpicker = new MaterialPicker(Layer,tabreport.ReportTable,iMillCode,SItemTable);

				Layer   . add(materialpicker);
				Layer   . repaint();
				materialpicker . setSelected(true);
				Layer   . updateUI();
				materialpicker . show();
				materialpicker . BrowList.requestFocus();
                         }


                       }
		          }catch(Exception ex){}

                       
			
		    }

 
          }
     }

     public void setParamVectors()
     {
          VDept     = new Vector();
          VUnit     = new Vector();
          VUom      = new Vector();

          VDeptCode = new Vector();
          VUnitCode = new Vector();
          VUomCode  = new Vector();

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res  = stat.executeQuery("Select UomName,UomCode From Uom Order By UomCode");
               while(res.next())
               {
                    VUom      .addElement(res.getString(1));
                    VUomCode  .addElement(res.getString(2));
               }
               res.close();
               res  = stat.executeQuery("Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name");
               while(res.next())
               {
                    VDept     .addElement(res.getString(1));
                    VDeptCode .addElement(res.getString(2));
               }
               res.close();
               res  = stat.executeQuery("Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name");
               while(res.next())
               {
                    VUnit     .addElement(res.getString(1));
                    VUnitCode .addElement(res.getString(2));
               }
               res.close();
               res  = stat.executeQuery("Select to_Char(Sysdate,'YYYYMMDD HH24:MI:SS') From Dual");
               while(res.next())
               {
                    SDate =res.getString(1);
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private int getUnitCode(String SUnitName)
     {
          int index= VUnit.indexOf(common.parseNull(SUnitName));

          if(index==-1)
              return 0;
          else
              return (common.toInt((String)VUnitCode.elementAt(index)));
     }
     private int getUomCode(String SUomName)
     {
          int index= VUom.indexOf(common.parseNull(SUomName));

          if(index==-1)
              return 0;
          else
              return (common.toInt((String)VUomCode.elementAt(index)));
     }
     private int getDeptCode(String SMakeName)
     {
          int index= VDept.indexOf(common.parseNull(SMakeName));

          if(index==-1)
              return 0;
          else
              return (common.toInt((String)VDeptCode.elementAt(index)));
     }

     public void fillData()
     {
          getData();
          setData();
     }

     public void getData()
     {
          VDesc      = new Vector();
          VPurpose   = new Vector();
          VRUom      = new Vector();
          VRDept     = new Vector();
          VRUnit     = new Vector();
          VDueDate   = new Vector();
          VQty       = new Vector();
          VSlNo      = new Vector();
          VMemoNo    = new Vector();
          VMUserCode = new Vector();
          VRepStatus = new Vector();
          VRepNo     = new Vector();
          VRepCode   = new Vector();
		  VItemName	 = new Vector();
		  VItemCode	 = new Vector();
		  
		  VHsnCode	 = new Vector();

          String Str = "";

          int iCount=0;

          for(int i=0;i<VSeleMemoNo.size();i++)
          {
               String SMemoNo = (String)VSeleMemoNo.elementAt(i);

               iCount++;

               if(iCount==1)
               {
                    Str = Str + ""+SMemoNo+"";
               }
               else
               {
                    Str = Str + ","+SMemoNo+"";
               }
          }


          String QS1 = " Select Max(RepNoStatus) from RDCMemo "+
                       " Where MemoNo in ( ";

                      QS1 = QS1 + Str + ")";



          String QS = " Select RDCMemo.MemoDate,"+SSupTable+".Name,Hod.HodName,RDCMemo.HodCode, "+
                      " RDCMemo.Descript,RDCMemo.Remarks,Uom.UomName,Dept.Dept_Name, "+
                      " Unit.Unit_Name,RDCMemo.DueDate,RDCMemo.Qty,RDCMemo.SlNo,RDCMemo.Sup_Code,"+
                      " RDCMemo.Thro,RDCMemo.MemoNo,RDCMemo.AuthUserCode, "+
                      " RDCMemo.RepNoStatus,RDCMemo.RepNo,RDCMemo.Item_Code "+
					  " ,"+SItemTable+".Item_Name,"+SItemTable+".Item_Code,"+SItemTable+".HSNCODE "+
                      " From RDCMemo "+
                      " Inner Join "+SSupTable+" on RDCMemo.Sup_Code = "+SSupTable+".Ac_Code "+
                      " Inner Join Hod on RDCMemo.HodCode = Hod.HodCode "+
                      " Inner Join Uom on RDCMemo.UomCode = Uom.UomCode "+
                      " Inner Join Dept on RDCMemo.Dept_Code = Dept.Dept_Code "+
                      " Inner Join Unit on RDCMemo.Unit_Code = Unit.Unit_Code "+
					  " left Join "+SItemTable+" on RDCMemo.Item_Code = "+SItemTable+".Item_Code "+
					  
                      " Where RDCMemo.MemoNo in ( ";


                      QS = QS + Str + ")"+

                      " And RDCMemo.DocType="+SDocType+
                      " And RDCMemo.IsDelete=0 And RDCMemo.MillCode="+iMillCode+
                      " And RDCMemo.RDCStatus=0 And RDCMemo.AuthStatus=1 "+
                      " Order by RDCMemo.MemoNo,RDCMemo.SlNo ";

 System.out.println("QS==>"+QS);

          try
          {
                if(theconnect==null)
                {
                     connect=ORAConnection.getORAConnection();
                     theconnect=connect.getConnection();
                }
                Statement stat   = theconnect.createStatement();

                ResultSet result = stat.executeQuery(QS1);
                while(result.next())
                {
                     SRepStatus = result.getString(1);
                }
                result.close();

                result = stat.executeQuery(QS);
                while(result.next())
                {
                     SMemoDate = result.getString(1);
                     SSupName  = result.getString(2);
                     SHodName  = result.getString(3);
                     SHodCode  = result.getString(4);
     System.out.println("name==>"+SSupName);
                     VDesc.addElement(result.getString(5));
                     VPurpose.addElement(result.getString(6));
                     VRUom.addElement(result.getString(7));
                     VRDept.addElement(result.getString(8));
                     VRUnit.addElement(result.getString(9));
                     VDueDate.addElement(result.getString(10));
                     VQty.addElement(result.getString(11));
                     VSlNo.addElement(result.getString(12));
     
                     SRSupCode = result.getString(13);
                     SVehicle  = result.getString(14);
                     VMemoNo.addElement(result.getString(15));
                     VMUserCode.addElement(result.getString(16));
                     VRepStatus.addElement(result.getString(17));
                     VRepNo.addElement(result.getString(18));
                     VRepCode.addElement(result.getString(19));
					 
					 VItemName.addElement(result.getString(20));
					 VItemCode.addElement(result.getString(21));
					 VHsnCode.addElement(result.getString(22));
                }
                result.close();
                stat.close();
          }
          catch(Exception ex)
          {
                  System.out.println(ex);           
          }
     }

     public void setData()
     {
          String Str="";

          int iCount=0;

          for(int i=0;i<VSeleMemoNo.size();i++)
          {
               String SMemoNo = (String)VSeleMemoNo.elementAt(i);

               iCount++;

               if(iCount==1)
               {
                    Str = Str + ""+SMemoNo+"";
               }
               else
               {
                    Str = Str + ","+SMemoNo+"";
               }
          }

          LMemoNo.setText(Str);
          LMemoDate.setText(common.parseDate(SMemoDate));
          LHodName.setText(SHodName);
          JCDocType.setSelectedIndex(common.toInt(SDocType));
          TSlNo.setText(SRDCNo);
          TSentThro.setText(SVehicle);
System.out.println("SRSupCode 1"+SRSupCode);
          BSupplier.setText(SSupName);

System.out.println("SRSupCode"+SRSupCode);
          TSupCode.setText(SRSupCode);


          for(int i=0;i<VDesc.size();i++)
          {
			  
			  
			  
	           RowData[i][1]=common.parseNull((String)VItemCode.elementAt(i));
               RowData[i][2]=common.parseNull((String)VItemName.elementAt(i));
               RowData[i][3]=common.parseNull((String)VHsnCode.elementAt(i));
               RowData[i][4] = common.parseNull((String)VDesc.elementAt(i));
               RowData[i][5] = common.parseNull((String)VPurpose.elementAt(i));
               RowData[i][6] = common.parseNull((String)VRUom.elementAt(i));
               RowData[i][7] = common.parseNull((String)VRDept.elementAt(i));
               RowData[i][8] = common.parseNull((String)VRUnit.elementAt(i));
               RowData[i][9] = common.parseDate((String)VDueDate.elementAt(i));
               RowData[i][10] = common.parseNull((String)VQty.elementAt(i));

               RowData[i][11] ="";
               RowData[i][12] ="";

          }          Layer.updateUI();

          JCDocType.setEnabled(false);
          TSlNo.setEditable(false);

          if(common.toDouble(SRepStatus)>0)
          {
               JCType.setSelectedIndex(0);
               JCType.setEnabled(false);
               JCAsset.setEnabled(false);
               BSupplier.setEnabled(false);
               TDate.setEditable(false);
          }
     }


    public void PartyCheck()
     {

         String SSupCode     = TSupCode.getText();
         String QS1=" select  statecode,GSTPARTYTYPECODE from partymaster where  partycode='"+SSupCode+"' ";

         SStateCode="";
         SPartyTypeCode="";

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               ResultSet      rs1            = stat.executeQuery(QS1);

System.out.println("QS1"+QS1);
             
               if(rs1.next())
               {
                   
                    SStateCode           = common.parseNull(String.valueOf(rs1.getInt(1)));
                    SPartyTypeCode       = common.parseNull(String.valueOf(rs1.getInt(2)));

   System.out.println("State==>"+SStateCode+"=="+SPartyTypeCode);
               }
               rs1                           . close();

               stat . close();
          }
          catch(Exception ex){System.out.println(ex);ex.printStackTrace();}
     }

 
}
