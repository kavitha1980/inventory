package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

import java.net.*;

public class RDCMemoPendingFrame extends JInternalFrame
{
     JComboBox      JCGSTType;
     JPanel         MiddlePanel,BottomPanel;
     JTabbedPane    JTP;
     TabReport      tabreport;
     JButton        BOk;
     JPanel         TopPanel;
     Object         RowData[][];
     String         ColumnData[] = {"WorkShop/Supplier","Hod","Memo Type","Memo Date","Memo No","Select"};
     String         ColumnType[] = {"S"                ,"S"  ,"S"        ,"S"        ,"S"      ,"B"     };
     
     JLayeredPane   DeskTop;
     
     Common common = new Common();
     
     int            iMillCode,iUserCode;
     String         SSupTable,SYearCode,SItemTable;

     Vector         VMemoDate,VMemoNo,VHodName,VMemoType,VHodCode,VMemoTypeCode,VSupName,VSupCode,VPStateCode,VPTypeCode;

     ORAConnection connect;
     Connection theconnect;

     String SSeleDocType   = "";
     Vector VSeleMemoNo;


     public RDCMemoPendingFrame(JLayeredPane DeskTop,int iUserCode,int iMillCode,String SSupTable,String SYearCode,String SItemTable)
     {
          super(" MEMOs Pending For RDC Conversion");
          this.DeskTop        = DeskTop;
          this.iUserCode      = iUserCode;
          this.iMillCode      = iMillCode;
          this.SSupTable      = SSupTable;
          this.SYearCode      = SYearCode;
          this.SItemTable     = SItemTable; 

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel     = new JPanel();
          MiddlePanel  = new JPanel();
          BottomPanel  = new JPanel();
          JTP          = new JTabbedPane();
          BOk          = new JButton("Pass RDC");
          JCGSTType   = new JComboBox(); 

     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,850,475);
         
          MiddlePanel.setLayout(new BorderLayout());
     }

     public void addComponents()
     {

         
      
          try
          {
               JTP.removeAll();
               MiddlePanel.removeAll();
               getContentPane().remove(MiddlePanel);
               getContentPane().remove(BottomPanel);
          }
          catch(Exception ex){}

          
          
          getContentPane().add("Center",MiddlePanel);
          MiddlePanel.add("Center",JTP);

          BottomPanel.add(BOk);

          setDataIntoVector();
          setRowData();

          try
          {
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

               JTP.addTab("Pending Memos",tabreport);

               if(RowData.length>0)
               {
                    getContentPane().add("South",BottomPanel);
               }

               setSelected(true);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void addListeners()
     {
          BOk.addActionListener(new ActList(this));
     }

     private class ActList implements ActionListener
     {
          RDCMemoPendingFrame rdcmemopendingframe;

          public ActList(RDCMemoPendingFrame rdcmemopendingframe)
          {
               this.rdcmemopendingframe = rdcmemopendingframe;
          }
          public void actionPerformed(ActionEvent ae)
          {
               if(isValidData())
               {

                  
                    
                  
                    RDCMemoCollectionGSTFrame rdcmemocollectionframe = new RDCMemoCollectionGSTFrame(DeskTop,VSeleMemoNo,SSeleDocType,iUserCode,iMillCode,rdcmemopendingframe,SSupTable,SYearCode,SItemTable);
                    rdcmemocollectionframe.fillData();
                    DeskTop.add(rdcmemocollectionframe);
                    rdcmemocollectionframe.show();
                    rdcmemocollectionframe.moveToFront();
                    DeskTop.repaint();
                    DeskTop.updateUI();
                    }
             
          }
     }

     private boolean isValidData()
     {
          int iCount   = 0;

          SSeleDocType = "";
          VSeleMemoNo  = new Vector();

          try
          {
               for(int i=0;i<RowData.length;i++)
               {
                    Boolean BSelected = (Boolean)RowData[i][5];
                    if(BSelected.booleanValue())
                    {
                         iCount++;
                    }
               }

               if(iCount==0)
               {
                    JOptionPane.showMessageDialog(null,"No Row Selected","Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }
               else
               {
                    int iRowCount=0;

                    String SDocType    = "";
                    String SPartyCode  = "";
                    String SHodCode    = "";

                    String SPDocType   = "";
                    String SPPartyCode = "";
                    String SPHodCode   = "";

                    iRowCount=0;

                    for(int i=0;i<RowData.length;i++)
                    {
                         Boolean BSelected = (Boolean)RowData[i][5];
                         if(BSelected.booleanValue())
                         {

				 String SSupType   = (String)VPTypeCode.elementAt(i);
				 String SStateCode = (String)VPStateCode.elementAt(i);
				 String SSupName   = (String)VSupName.elementAt(i);

				 if(SSupType.equals("") || SSupType.equals("0"))
				 {
		    	              JOptionPane.showMessageDialog(null,"Gst Classification Not Updated for Supplier-"+SSupName,"Information",JOptionPane.INFORMATION_MESSAGE);
				      return false;
				 }

				 if(SStateCode.equals("") || SStateCode.equals("0"))
				 {
				      JOptionPane.showMessageDialog(null,"State Data Not Updated for Supplier-"+SSupName,"Information",JOptionPane.INFORMATION_MESSAGE);
				      return false;
				 }

                              iRowCount++;

                              SDocType   = (String)VMemoTypeCode.elementAt(i);
                              SPartyCode = (String)VSupCode.elementAt(i);
                              SHodCode   = (String)VHodCode.elementAt(i);

                              if(iRowCount==1)
                              {
                                   SPDocType   = SDocType;
                                   SPPartyCode = SPartyCode;
                                   SPHodCode   = SHodCode;

                                   SSeleDocType   = SDocType;
                              }
                              else
                              {
                                   if((!SDocType.equals(SPDocType)) || (!SPartyCode.equals(SPPartyCode)) || (!SHodCode.equals(SPHodCode)))
                                   {
                                        JOptionPane.showMessageDialog(null,"Party or HOD or Memo Type Mismatch","Error",JOptionPane.ERROR_MESSAGE);
                                        return false;
                                   }
                              }

                              VSeleMemoNo.addElement((String)VMemoNo.elementAt(i));
                         }
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          return true;
     }

     public void setDataIntoVector()
     {
          VMemoDate      = new Vector();
          VMemoNo        = new Vector();
          VHodName       = new Vector();
          VMemoType      = new Vector();
          VHodCode       = new Vector();
          VMemoTypeCode  = new Vector();
          VSupName       = new Vector();
          VSupCode       = new Vector();
	  VPStateCode	 = new Vector();
	  VPTypeCode	 = new Vector();

          String QS = " Select distinct(RDCMemo.MemoNo),RDCMemo.MemoDate,Hod.HodName,"+
                      " decode(RDCMemo.DocType,0,'Returnable','Non-Returnable') as MemoTypeName,"+
                      " RDCMemo.HodCode,RDCMemo.DocType,"+SSupTable+".Name,RDCMemo.Sup_Code,"+
		      " decode(PartyMaster.StateCode,0,'0',decode(PartyMaster.CountryCode,'61',nvl(State.GstStateCode,0),'999')) as GstStateCode,nvl(PartyMaster.GstPartyTypeCode,0) as GstPartyTypeCode "+ //nvl(State.GstStateCode,0) as GstStateCode
		      " from RDCMemo "+
                      " Left Join Hod on RDCMemo.HodCode=Hod.HodCode "+
                      " Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = RDCMemo.Sup_Code "+
		      " Inner Join PartyMaster on "+SSupTable+".Ac_Code=PartyMaster.PartyCode "+
		      " Inner Join State on PartyMaster.StateCode=State.StateCode "+
                      " Where RDCMemo.RDCStatus=0 and RDCMemo.AuthStatus=1 "+
                      " and RDCMemo.MillCode="+iMillCode+" and RDCMemo.isDelete=0 "+
                      " Order by "+SSupTable+".Name,RDCMemo.MemoDate,RDCMemo.MemoNo ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               ResultSet res = stat.executeQuery(QS);
               
               while (res.next())
               {
                    VMemoNo        . addElement(res.getString(1));
                    VMemoDate      . addElement(common.parseDate(res.getString(2)));
                    VHodName       . addElement(res.getString(3));
                    VMemoType      . addElement(res.getString(4));
                    VHodCode       . addElement(res.getString(5));
                    VMemoTypeCode  . addElement(res.getString(6));
                    VSupName       . addElement(res.getString(7));
                    VSupCode       . addElement(res.getString(8));
		    VPStateCode    . addElement(res.getString(9));
		    VPTypeCode     . addElement(res.getString(10));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VMemoDate.size()][ColumnData.length];
          for(int i=0;i<VMemoDate.size();i++)
          {
               RowData[i][0]  = (String)VSupName  .elementAt(i);
               RowData[i][1]  = (String)VHodName  .elementAt(i);
               RowData[i][2]  = (String)VMemoType .elementAt(i);
               RowData[i][3]  = (String)VMemoDate .elementAt(i);
               RowData[i][4]  = (String)VMemoNo   .elementAt(i);
               RowData[i][5]  = new Boolean(false);
          }  
     }

}
