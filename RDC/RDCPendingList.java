package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

// pdf import
import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;

import com.itextpdf.text.Element;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;


public class RDCPendingList extends JInternalFrame
{
	JPanel     TopPanel;
	JPanel     BottomPanel;
	JPanel     MiddlePanel;

	String     SDate;
	MyComboBox JCDept,JCAsset,JCSupplier;
	JButton    BApply,BPrint,BCreatePdf;
	JTextField TFile;

	DateField TDate;

	Object RowData[][];
	String ColumnData[] = {"RDC Date","RDC No","Supplier","Description","Remarks","Thro","Qty Sent","Qty Recd","Qty Pending","Due Date","Delayed Days","RDC User","RDC Staus"}; 
	String ColumnType[] = {"S"       ,"S"     ,"S"       ,"S"          ,"S"      ,"S"   ,"N"       ,"N"       ,"N"          ,"S"       ,"N"           ,"S"       ,"S"}; 

	JLayeredPane DeskTop;
	StatusPanel SPanel;
	int iMillCode;
	String SSupTable,SMillName;

	TabReport tabreport;
	ORAConnection connect;
	Connection theconnect;
	Common common = new Common();

	Vector VRDCDate,VRDCNo,VSupName,VDesc,VRemark,VThro;
	Vector VRDCQty,VRecQty,VPending,VDueDate,VAcCode,VRdcUser,VRDCStatus;

	Vector VDept,VDeptCode,VSupplier,VSupplierCode;

	Vector VORDCDate,VORDCNo,VOSupName,VODesc,VORemark,VOThro;
	Vector VORDCQty,VORecQty,VOPending,VODueDate,VOAcCode,VORdcUser;

	int Lctr = 100,Pctr=0;
	FileWriter FW;
	  
    Document document;
    PdfPTable table;
    int iTotalColumns = 11;
    int iWidth[] = {10, 8, 20, 20, 6, 6, 6, 10, 10, 14, 10};
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 14, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 6, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font bigNormal = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
    String SFile="";   
    String PDFFile = "";

    RDCPendingList(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode,String SSupTable,String SMillName)
    {
		super("RDC Pending As On today-Supplierwise");
		this.DeskTop   = DeskTop;
		this.SPanel    = SPanel;
		this.iMillCode = iMillCode;
		this.SSupTable = SSupTable;
		this.SMillName = SMillName;

		PDFFile = common.getPrintPath()+"/NewRDCPend1.pdf";
		//System.out.println("common.getPrintPath()-->"+common.getPrintPath());

		setDept();
		createComponents();
		setLayouts();
		addComponents();
		addListeners();
	}

    private void createComponents()
    {
		TopPanel    = new JPanel(true);
		BottomPanel = new JPanel(true);
		MiddlePanel = new JPanel(true);

		TDate       = new DateField();
		JCDept      = new MyComboBox(VDept);
		JCAsset     = new MyComboBox();
		JCSupplier  = new MyComboBox(VSupplier);
		BApply      = new JButton("Apply");
		BPrint      = new JButton("Print");
		BCreatePdf   = new JButton("Create pdf");
		TFile       = new JTextField(15);

		TDate.setTodayDate();
		TFile.setText("RDCPend1.prn");
		TFile.setEditable(false);
		BPrint.setEnabled(false);

		BApply.setMnemonic('A');
		BPrint.setMnemonic('P');

		JCAsset.addItem("Non Asset");
		JCAsset.addItem("Asset");
		JCAsset.addItem("ALL");
		JCAsset.setSelectedItem("ALL");
	}

    private void setLayouts()
    {
		TopPanel.setLayout(new GridLayout(1,7,10,10));
		MiddlePanel.setLayout(new BorderLayout());

		setClosable(true);
		setMaximizable(true);
		setIconifiable(true);
		setResizable(true);
		setBounds(0,0,790,500);
    }
	private void addComponents()
    {
		TopPanel.add(new JLabel("As On"));
		TopPanel.add(TDate);
		TopPanel.add(new JLabel("Asset"));
		TopPanel.add(JCAsset);

		TopPanel.add(new JLabel("Dept"));
		TopPanel.add(JCDept);

		TopPanel.add(new JLabel("Supplier"));
		TopPanel.add(JCSupplier);

		TopPanel.add(BApply);

		BottomPanel.add(TFile);
		BottomPanel.add(BPrint);
		BottomPanel. add(BCreatePdf);

		getContentPane().add("North",TopPanel);
		getContentPane().add("Center",MiddlePanel);
		getContentPane().add("South",BottomPanel);
	}

    private void addListeners()
    {
    	BApply.addActionListener(new ActList());
        BPrint.addActionListener(new PrintList());
        BCreatePdf.addActionListener(new PrintList());
    }
	
	private class PrintList implements ActionListener
    {
    	public void actionPerformed(ActionEvent ae)
        {
        	if(ae.getSource() == BCreatePdf)
            {
            	
			}
			if(iMillCode==0)
			{
				setOtherVector();
			}
			setRDCPendingReport();
			createPDFFile();
			try
				{
					
	                File theFile   = new File(PDFFile);
	                Desktop        . getDesktop() . open(theFile);
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					System.out.println("ex:"+ex);
				}
			removeHelpFrame();
		}
	}

	public void removeHelpFrame()
	{
		try
		{
			DeskTop.remove(this);
			DeskTop.repaint();
			DeskTop.updateUI();
		}
		catch(Exception ex) { }
	}

	private void setRDCPendingReport()
    {
    	if(VAcCode.size() == 0)
        	return;

		Lctr = 100;Pctr=0;
		String PAcCode = (String)VAcCode.elementAt(0);
		String SFile    = TFile.getText();
		if((SFile.trim()).length()==0)
		SFile = "1.prn";

		try
        {
        	FW = new FileWriter(common.getPrintPath()+SFile);
            for(int i=0;i<VAcCode.size();i++)
            {
				setHead((String)VSupName.elementAt(i),(String)VThro.elementAt(i));
				String SAcCode = (String)VAcCode.elementAt(i);
				if(!SAcCode.equals(PAcCode))
				{
					setContBreak(0);
					PAcCode = SAcCode;
					setFirstLine(0,(String)VSupName.elementAt(i),(String)VThro.elementAt(i));
				}
				setBody(i);
            }

			if(iMillCode==0)
			{
				if(VOAcCode.size()>0)
				{
					Lctr = 100;
					String POAcCode = (String)VOAcCode.elementAt(0);

					for(int i=0;i<VOAcCode.size();i++)
					{
						setOtherHead((String)VOSupName.elementAt(i),(String)VOThro.elementAt(i));
						String SOAcCode = (String)VOAcCode.elementAt(i);
						if(!SOAcCode.equals(POAcCode))
						{
							setContBreak(1);
							POAcCode = SOAcCode;
							setFirstLine(0,(String)VOSupName.elementAt(i),(String)VOThro.elementAt(i));
						}
						setOtherBody(i);
					}
				}
			}
			setContBreak(0);
			String SDateTime = common.getServerDateTime2();
			String SEnd = "Report Taken on "+SDateTime+"\n";
			FW.write(SEnd);
			FW.close();
		}
      	catch(Exception ex){}
	}

	private void setHead(String SName,String SThro) throws Exception
    {
    	if (Lctr < 60)
        	return;

		if (Pctr > 0)
        {
        	setContBreak(0);
            FW.write("\n");
        }

		Pctr++;
		String str1 = "Company : "+SMillName;
		String str2 = "EDocument : Report on Returnable Material Pending List as On "+TDate.toString()+"  -  Dept : "+(String)JCDept.getSelectedItem()+"F ";
		String str3 = "Page No  : "+Pctr+" ";
		String str4 = common.Pad("RDC",10)+common.Space(3)+common.Rad("RDC",10)+common.Space(3)+
				    common.Pad("Material Description",40)+common.Space(3)+common.Pad("Instructions",15)+common.Space(3)+
				    common.Rad("RDC",6)+common.Space(3)+common.Rad("Recd",6)+common.Space(3)+
				    common.Rad("Pending",6)+common.Space(3)+common.Pad("Due Date",10)+common.Space(3)+
				    common.Rad("Delayed",7)+common.Space(3)+common.Pad("RDC User",15)+common.Space(3)+common.Pad("RDC Status",30);
		String str5 = common.Pad("Date",10)+common.Space(3)+common.Rad("No",10)+common.Space(3)+
				    common.Pad(" ",40)+common.Space(3)+common.Pad(" ",15)+common.Space(3)+
				    common.Rad("Qty",6)+common.Space(3)+common.Rad("Qty",6)+common.Space(3)+
				    common.Rad("Qty",6)+common.Space(3)+common.Pad(" ",10)+common.Space(3)+
				    common.Rad("Days",7)+common.Space(3)+common.Pad(" ",15)+common.Space(3)+common.Pad("",30);
		String str6 = common.Replicate("-",str5.length());

		FW.write("g"+str1+"\n");
		FW.write(str2+"\n");
		FW.write(str3+"\n");
		FW.write(str6+"\n");
		FW.write(str4+"\n");
		FW.write(str5+"\n");
		FW.write(str6+"\n");
		Lctr = 7;
		setFirstLine(1,SName,SThro);
	}

    private void setFirstLine(int i,String SName,String SThro) throws Exception 
    {
    	String str="";
        if(i==1)
        	str = "EName : "+SName+"F (Contd...)";               
		else
			str = "EName : "+SName+" Through : "+SThro+"F";

		FW.write(str+"\n");
		Lctr++;
	}

    private void setContBreak(int iValue) throws Exception
    {
    	String str5 = common.Pad("Date",10)+common.Space(3)+common.Rad("No",10)+common.Space(3)+
                        common.Pad(" ",40)+common.Space(3)+common.Pad(" ",15)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Rad("Qty",6)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Pad(" ",10)+common.Space(3)+
                        common.Rad("Days",7)+common.Space(3)+common.Pad(" ",15);
                     if(iValue==0)
                        str5 = str5+common.Space(3)+common.Pad(" ",30);
		String str6 = common.Replicate("-",str5.length());

        FW.write(str6+"\n");
        Lctr++;
	}

    private void setBody(int i) throws Exception
    {
		String SDate    = common.parseDate((String)VRDCDate  .elementAt(i));
		String SNo      = (String)VRDCNo    .elementAt(i);
		String SDesc    = (String)VDesc     .elementAt(i);
		String SRemark  = (String)VRemark   .elementAt(i);
		String SQty     = common.getRound((String)VRDCQty   .elementAt(i),0);
		String SRecQty  = common.getRound((String)VRecQty   .elementAt(i),0);
		String SPending = common.getRound((String)VPending  .elementAt(i),0);
		String SDueDate = common.parseDate((String)VDueDate  .elementAt(i));
		String SDelay   = (String)RowData[i][10];
		String SRdcUser = (String)VRdcUser.elementAt(i);
		String SRDCStatus =(String)VRDCStatus.elementAt(i);
		String str = common.Pad(SDate,10)+common.Space(3)+common.Rad(SNo,10)+common.Space(3)+
		common.Pad(SDesc,40)+common.Space(3)+common.Pad(SRemark,15)+common.Space(3)+
		common.Rad(SQty,6)+common.Space(3)+common.Rad(SRecQty,6)+common.Space(3)+
		common.Rad(SPending,6)+common.Space(3)+common.Pad(SDueDate,10)+common.Space(3)+
		common.Rad(SDelay,7)+common.Space(3)+common.Pad(SRdcUser,15)+common.Space(3)+common.Pad(SRDCStatus,30);

		FW.write(str+"\n");
		Lctr++;
	}

    private void setOtherHead(String SName,String SThro) throws Exception
    {
    	if (Lctr < 60)
        	return;

		if (Pctr > 0)
        {
        	setContBreak(0);
            FW.write("\n");
		}

		Pctr++;
		String str1 = "Company : "+SMillName;
		String str2 = "EDocument : Report on Returnable Material (Other than Stores) Pending List as On "+TDate.toString()+"F ";
		String str3 = "Page No  : "+Pctr+" ";

		String str4 = common.Pad("RDC",10)+common.Space(3)+common.Rad("RDC",10)+common.Space(3)+
		common.Pad("Material Description",40)+common.Space(3)+common.Pad("Instructions",15)+common.Space(3)+
		common.Rad("RDC",6)+common.Space(3)+common.Rad("Recd",6)+common.Space(3)+
		common.Rad("Pending",6)+common.Space(3)+common.Pad("Due Date",10)+common.Space(3)+
		common.Rad("Delayed",7)+common.Space(3)+common.Pad("RDC User",15);

		String str5 = common.Pad("Date",10)+common.Space(3)+common.Rad("No",10)+common.Space(3)+
		common.Pad(" ",40)+common.Space(3)+common.Pad(" ",15)+common.Space(3)+
		common.Rad("Qty",6)+common.Space(3)+common.Rad("Qty",6)+common.Space(3)+
		common.Rad("Qty",6)+common.Space(3)+common.Pad(" ",10)+common.Space(3)+
		common.Rad("Days",7)+common.Space(3)+common.Pad(" ",15);

		String str6 = common.Replicate("-",str5.length());

		FW.write("g"+str1+"\n");
		FW.write(str2+"\n");
		FW.write(str3+"\n");
		FW.write(str6+"\n");
		FW.write(str4+"\n");
		FW.write(str5+"\n");
		FW.write(str6+"\n");
		Lctr = 7;
		setFirstLine(1,SName,SThro);
	}

    private void setOtherBody(int i) throws Exception
    {
          String SDate    = common.parseDate((String)VORDCDate  .elementAt(i));
          String SNo      = (String)VORDCNo    .elementAt(i);
          String SDesc    = (String)VODesc     .elementAt(i);
          String SRemark  = (String)VORemark   .elementAt(i);
          String SQty     = common.getRound((String)VORDCQty   .elementAt(i),0);
          String SRecQty  = common.getRound((String)VORecQty   .elementAt(i),0);
          String SPending = common.getRound((String)VOPending  .elementAt(i),0);
          String SDueDate = common.parseDate((String)VODueDate  .elementAt(i));

          String SDelay = "";
          try
          {
               SDelay  = common.getDateDiff(TDate.toString(),SDueDate);
          }
          catch(Exception ex)
          {
               SDelay = "";
          }
          String SRdcUser = (String)VORdcUser.elementAt(i);

          String str = common.Pad(SDate,10)+common.Space(3)+common.Rad(SNo,10)+common.Space(3)+
                       common.Pad(SDesc,40)+common.Space(3)+common.Pad(SRemark,15)+common.Space(3)+
                       common.Rad(SQty,6)+common.Space(3)+common.Rad(SRecQty,6)+common.Space(3)+
                       common.Rad(SPending,6)+common.Space(3)+common.Pad(SDueDate,10)+common.Space(3)+
                       common.Rad(SDelay,7)+common.Space(3)+common.Pad(SRdcUser,15);

          FW.write(str+"\n");
          Lctr++;
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               boolean bflag = setDataIntoVector();
               if(!bflag)
                    return;
               setVectorIntoRowData();
               try
               {
                    MiddlePanel.remove(tabreport);
                    MiddlePanel.updateUI();
               }
               catch(Exception ex){}
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               MiddlePanel.add("Center",tabreport);
               MiddlePanel.updateUI();
               BPrint.setEnabled(true);
               TFile.setEditable(true);
          }
     }

     private boolean setDataIntoVector()
     {
          boolean bflag = false;
          VRDCDate  = new Vector();
          VRDCNo    = new Vector();
          VSupName  = new Vector();
          VDesc     = new Vector();
          VRemark   = new Vector();
          VThro     = new Vector();
          VRDCQty   = new Vector();
          VRecQty   = new Vector();
          VPending  = new Vector();
          VDueDate  = new Vector();
          VAcCode   = new Vector();
          VRdcUser  = new Vector();
          VRDCStatus = new Vector();
          String SDate = TDate.toNormal();


          String QS = " Select RDC.RDCDate,RDC.RDCNo,"+SSupTable+".Name,Descript, "+
                      " RDC.Remarks,RDC.Thro,RDC.Qty,RDC.RecQty, "+
                      " (RDC.Qty-RDC.RecQty),RDC.DueDate,RDC.Sup_Code,RawUser.UserName,RDCStatus.Status "+
                      " From RDC "+
                      " Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = RDC.Sup_Code "+
                      " Inner Join RawUser on RDC.MemoAuthUserCode=RawUser.UserCode "+
                      " Left Join RDCStatus on RDCStatus.RDCId = Rdc.Id and RdcStatus.ActiveStatus=0"+
                      " Where RDC.RDCDate<='"+SDate+"' And RDC.RecQty < RDC.Qty "+
                      " and RDC.MillCode="+iMillCode+
                      " And RDC.AssetFlag=0 and DocType=0 ";

               if(!(JCDept.getSelectedItem()).equals("ALL"))
                    QS = QS + " and RDC.Dept_code="+VDeptCode.elementAt(JCDept.getSelectedIndex());


				if(!(JCSupplier.getSelectedItem()).equals("ALL"))
                    QS = QS + " and RDC.Sup_Code = '"+VSupplierCode.elementAt(JCSupplier.getSelectedIndex())+"' ";

               if(!(JCAsset.getSelectedItem()).equals("ALL"))
                    QS = QS + " and RDC.GodownAsset="+JCAsset.getSelectedIndex();

                 QS = QS + " Order By 3,1 ";

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res  = stat.executeQuery(QS);
               while(res.next())
               {
                    VRDCDate  .addElement(""+res.getString(1));
                    VRDCNo    .addElement(""+res.getString(2));
                    VSupName  .addElement(""+res.getString(3));
                    VDesc     .addElement(""+res.getString(4));
                    VRemark   .addElement(""+res.getString(5));
                    VThro     .addElement(""+res.getString(6));
                    VRDCQty   .addElement(""+res.getString(7));
                    VRecQty   .addElement(""+res.getString(8));
                    VPending  .addElement(""+res.getString(9));
                    VDueDate  .addElement(""+res.getString(10));
                    VAcCode   .addElement(""+res.getString(11));
                    VRdcUser  .addElement(""+res.getString(12));
                    VRDCStatus.addElement(""+common.parseNull(res.getString(13)));
               }
               res.close();
               stat.close();
               bflag = true;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bflag = false;
          }
          return bflag;
     }

     private void setVectorIntoRowData()
     {

          RowData = new Object[VRDCNo.size()][13];
          String SDueDate="",SDelay="";

          for(int i=0;i<VRDCDate.size();i++)
          {
               RowData[i][0]  = common.parseDate((String)VRDCDate  .elementAt(i));
               RowData[i][1]  = (String)VRDCNo    .elementAt(i);
               RowData[i][2]  = (String)VSupName  .elementAt(i);
               RowData[i][3]  = (String)VDesc     .elementAt(i);
               RowData[i][4]  = (String)VRemark   .elementAt(i);
               RowData[i][5]  = (String)VThro     .elementAt(i);
               RowData[i][6]  = (String)VRDCQty   .elementAt(i);
               RowData[i][7]  = (String)VRecQty   .elementAt(i);
               RowData[i][8]  = (String)VPending  .elementAt(i);

               try
               {
                    SDueDate = common.parseDate((String)VDueDate  .elementAt(i));
               }
               catch(Exception ex)
               {
                    SDueDate = "";
               }

               try
               {
                    SDelay  = common.getDateDiff(TDate.toString(),common.parseDate((String)VRDCDate.elementAt(i)));
               }
               catch(Exception ex)
               {
                    SDelay = "";
               }

               RowData[i][9]  = SDueDate;
               RowData[i][10] = SDelay;
               RowData[i][11] = (String)VRdcUser  .elementAt(i);
               RowData[i][12] = (String)VRDCStatus.elementAt(i);
          }
     }

     private void setDept()
     {
          VDept          = new Vector();
          VDeptCode      = new Vector();
          
          VDept          .addElement("ALL");
          VDeptCode      .addElement("9999");

		  VSupplier          = new Vector();
          VSupplierCode      = new Vector();
          
          VSupplier          .addElement("ALL");
          VSupplierCode      .addElement("9999");
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               String QS1 = "Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               String QS2 = "Select Name,Ac_Code From Supplier Order By Name";

               ResultSet result1 = stat.executeQuery(QS1);
               while(result1.next())
               {
                    VDept     .addElement(result1.getString(1));
                    VDeptCode .addElement(result1.getString(2));
               }
               result1.close();

				ResultSet result2 = stat.executeQuery(QS2);
               while(result2.next())
               {
                    VSupplier     .addElement(result2.getString(1));
                    VSupplierCode .addElement(result2.getString(2));
               }
               result2.close();



               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept:"+ex);
               System.exit(0);
          }
     }

     private void setOtherVector()
     {
          VORDCDate  = new Vector();
          VORDCNo    = new Vector();
          VOSupName  = new Vector();
          VODesc     = new Vector();
          VORemark   = new Vector();
          VOThro     = new Vector();
          VORDCQty   = new Vector();
          VORecQty   = new Vector();
          VOPending  = new Vector();
          VODueDate  = new Vector();
          VOAcCode   = new Vector();
          VORdcUser  = new Vector();

          String SDate = TDate.toNormal();


          String QS = " Select SamplesDC.DCDate,SamplesDC.DCNo,PartyMaster.PartyName,SamplesDC.Description, "+
                      " ''  as Remarks,SamplesDC.TruckNo,SamplesDC.Qty,0 as RecQty, "+
                      " SamplesDC.Qty as PendQty,SamplesDC.DCDate as DueDate,SamplesDC.PartyCode "+
                      " From SamplesDC "+
                      " Inner Join PartyMaster On PartyMaster.PartyCode = SamplesDC.PartyCode "+
                      " And SamplesDC.DCDate<='"+SDate+"'"+
                      //" And SamplesDC.DCType=1 and SamplesDC.RetStatus=0 "+
					  " And SamplesDC.DCType=1 and SamplesDC.RetStatus=-1 "+
                      " Order By 3,1 ";

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat = theconnect.createStatement();
               ResultSet res  = stat.executeQuery(QS);
               while(res.next())
               {
                    VORDCDate  .addElement(""+res.getString(1));
                    VORDCNo    .addElement(""+res.getString(2));
                    VOSupName  .addElement(""+res.getString(3));
                    VODesc     .addElement(""+res.getString(4));
                    VORemark   .addElement(""+res.getString(5));
                    VOThro     .addElement(""+res.getString(6));
                    VORDCQty   .addElement(""+res.getString(7));
                    VORecQty   .addElement(""+res.getString(8));
                    VOPending  .addElement(""+res.getString(9));
                    VODueDate  .addElement(""+res.getString(10));
                    VOAcCode   .addElement(""+res.getString(11));
                    VORdcUser  .addElement("");
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

 private void createPDFFile() {
 

        
        if(VAcCode.size() == 0)
               return;

		
          Lctr = 100;Pctr=0;
          String PAcCode = (String)VAcCode.elementAt(0);
          String SFile    = TFile.getText();
          if((SFile.trim()).length()==0)
               SFile = "1.pdf";

          try
          {
              document = new Document(PageSize.A4.rotate());
              //PdfWriter.getInstance(document, new FileOutputStream(PDFFile));
			  PdfWriter writer =PdfWriter.getInstance(document, new FileOutputStream(PDFFile));
              document.open();
              //document .  newPage();  
              

				table  = new PdfPTable(11);
			table  . setWidths(iWidth);
			table  . setWidthPercentage(100);
                        table.setHeaderRows(3);


				HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            	writer.setPageEvent(event);  
        
                        Pctr++;
       AddCellIntoTable("Company : "+SMillName+" ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 0, 0, 0, 0, smallbold);
	AddCellIntoTable( "Document : Report on Returnable Material Pending List as On "+TDate.toString()+"  -  Dept : "+(String)JCDept.getSelectedItem()+"", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 0, 0, 0, 0, smallbold);           
      
// AddCellIntoTable( "Page No: "+ Pctr+" ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 0, 0, 0, 0, smallbold);
//AddCellIntoTable( "Page No: "+ document.getPageNumber()+" ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 0, 0, 0, 0, smallbold);                  
//AddCellIntoTable(new Phrase("Page No :" + document.getPageNumber()), table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 0, 0, 0, 0, smallbold);                  
 
  setHeadpdf();
          
               for(int i=0;i<VAcCode.size();i++)
               { 
            
                    //setHeadpdf((String)VSupName.elementAt(i),(String)VThro.elementAt(i)); 
        
                    String SAcCode = (String)VAcCode.elementAt(i);
                    if(!SAcCode.equals(PAcCode) || i==0)
                    {
                        // setContBreakpdf(0);
                         PAcCode = SAcCode;
                         setFirstLinepdf((String)VSupName.elementAt(i),(String)VThro.elementAt(i));
                    }
                    setBodypdf(i);
               }


               if(iMillCode==0)
               {
                    if(VOAcCode.size()>0)
                    {
                       //  Lctr = 100;
                         String POAcCode = (String)VOAcCode.elementAt(0);

                         for(int i=0;i<VOAcCode.size();i++)
                         {
                              setOtherHeadpdf((String)VOSupName.elementAt(i),(String)VOThro.elementAt(i));
                              String SOAcCode = (String)VOAcCode.elementAt(i);
                              if(!SOAcCode.equals(POAcCode))
                              {
                                  // setContBreakpdf(1);
                                   POAcCode = SOAcCode;
                                   setFirstLinepdf((String)VOSupName.elementAt(i),(String)VOThro.elementAt(i));
                              }
                              setOtherBodypdf(i);
                         }
                    }
               }

              // setContBreakpdf(0);
               String SDateTime = common.getServerDateTime2();
               String SEnd = "Report Taken on "+SDateTime+"\n";
               AddCellIntoTable(SEnd, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 0, 0, 0, 0, smallbold); 
                        
   
               document.add(table);
               document.close();
          }
          catch(Exception ex){ System.out.println("Exception ex"+ex);}
    }

  private void setHeadpdf() throws Exception
     {
          

         
          String str1 = "Company : "+SMillName;
          String str2 = "Document : Report on Returnable Material Pending List as On "+TDate.toString()+"  -  Dept : "+(String)JCDept.getSelectedItem()+"";
          String str3 = "Page No  :  ";
          String str4 = common.Pad("RDC Date",10)+common.Space(3)+common.Rad("RDC",10)+common.Space(3)+
                        common.Pad("Material Description",40)+common.Space(3)+common.Pad("Instructions",15)+common.Space(3)+
                        common.Rad("RDC",6)+common.Space(3)+common.Rad("Recd",6)+common.Space(3)+
                        common.Rad("Pending",6)+common.Space(3)+common.Pad("Due Date",10)+common.Space(3)+
                        common.Rad("Delayed",7)+common.Space(3)+common.Pad("RDC User",15)+common.Space(3)+common.Pad("RDC Status",30);
          String str5 = common.Pad("Date",10)+common.Space(3)+common.Rad("No",10)+common.Space(3)+
                        common.Pad(" ",40)+common.Space(3)+common.Pad(" ",15)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Rad("Qty",6)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Pad(" ",10)+common.Space(3)+
                        common.Rad("Days",7)+common.Space(3)+common.Pad(" ",15)+common.Space(3)+common.Pad("",30);

   
      

        AddCellIntoTable("RDC Date", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 2, 2, mediumbold);
	AddCellIntoTable("RDC No", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("Material Description ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
        AddCellIntoTable("Instructions", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
	AddCellIntoTable("RDC Qty", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("Recd Qty", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
       AddCellIntoTable("Pending Qty", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
       AddCellIntoTable("Due Date", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  
	AddCellIntoTable("Delayed Days", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
       AddCellIntoTable("RDC user", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
       AddCellIntoTable("RDC Status", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);  


        /*  FW.write("g"+str1+"\n");
          FW.write(str2+"\n");
          FW.write(str3+"\n");
          FW.write(str6+"\n");
          FW.write(str4+"\n");
          FW.write(str5+"\n");
          FW.write(str6+"\n");
          Lctr = 7;*/
          //setFirstLinepdf(1,SName,SThro); 
     }

 private void setFirstLinepdf(String SName,String SThro) throws Exception 
     {
          String str="";
         /* if(i==1)
               str = "Name : "+SName+" (Contd...)";               
          else*/
               str = "Name : "+SName+" Through : "+SThro+"";

          AddCellIntoTable(str, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 0, 0, 0, 0, smallbold); 
         
     }
 private void setBodypdf(int i) throws Exception
     {
          String SDate    = common.parseDate((String)VRDCDate  .elementAt(i));
          String SNo      = (String)VRDCNo    .elementAt(i);
          String SDesc    = (String)VDesc     .elementAt(i);
          String SRemark  = (String)VRemark   .elementAt(i);
          String SQty     = common.getRound((String)VRDCQty   .elementAt(i),0);
          String SRecQty  = common.getRound((String)VRecQty   .elementAt(i),0);
          String SPending = common.getRound((String)VPending  .elementAt(i),0);
          String SDueDate = common.parseDate((String)VDueDate  .elementAt(i));
          String SDelay   = (String)RowData[i][10];
          String SRdcUser = (String)VRdcUser.elementAt(i);
          String SRDCStatus =(String)VRDCStatus.elementAt(i);
          String str = common.Pad(SDate,10)+common.Space(3)+common.Rad(SNo,10)+common.Space(3)+
                       common.Pad(SDesc,40)+common.Space(3)+common.Pad(SRemark,15)+common.Space(3)+
                       common.Rad(SQty,6)+common.Space(3)+common.Rad(SRecQty,6)+common.Space(3)+
                       common.Rad(SPending,6)+common.Space(3)+common.Pad(SDueDate,10)+common.Space(3)+
                       common.Rad(SDelay,7)+common.Space(3)+common.Pad(SRdcUser,15)+common.Space(3)+common.Pad(SRDCStatus,30);
         

         AddCellIntoTable(SDate, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 2, 2, smallnormal);
	AddCellIntoTable(SNo, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
	AddCellIntoTable(SDesc, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
        AddCellIntoTable(SRemark, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
	AddCellIntoTable(SQty, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
	AddCellIntoTable(SRecQty, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
       AddCellIntoTable(SPending, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
       AddCellIntoTable(SDueDate, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
	AddCellIntoTable(SDelay, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
       AddCellIntoTable(SRdcUser, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
       AddCellIntoTable(SRDCStatus, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);  
 
         // AddCellIntoTable(str, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 4, 1, 8, 2, smallbold); 
        
     }
 private void setContBreakpdf(int iValue) throws Exception
     {
          String str5 = common.Pad("Date",10)+common.Space(3)+common.Rad("No",10)+common.Space(3)+
                        common.Pad(" ",40)+common.Space(3)+common.Pad(" ",15)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Rad("Qty",6)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Pad(" ",10)+common.Space(3)+
                        common.Rad("Days",7)+common.Space(3)+common.Pad(" ",15);
                     if(iValue==0)
                        str5 = str5+common.Space(3)+common.Pad(" ",30);
          String str6 = common.Replicate("-",str5.length());

           AddCellIntoTable(str6, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 4, 1, 8, 2, smallbold); 
         
     }

  private void setOtherHeadpdf(String SName,String SThro) throws Exception
     {
        

         
          String str1 = "Company : "+SMillName;
          String str2 = "Document : Report on Returnable Material (Other than Stores) Pending List as On "+TDate.toString()+" ";
         // String str3 = "Page No  : "+Pctr+" ";
          String str4 = common.Pad("RDC",10)+common.Space(3)+common.Rad("RDC",10)+common.Space(3)+
                        common.Pad("Material Description",40)+common.Space(3)+common.Pad("Instructions",15)+common.Space(3)+
                        common.Rad("RDC",6)+common.Space(3)+common.Rad("Recd",6)+common.Space(3)+
                        common.Rad("Pending",6)+common.Space(3)+common.Pad("Due Date",10)+common.Space(3)+
                        common.Rad("Delayed",7)+common.Space(3)+common.Pad("RDC User",15);
          String str5 = common.Pad("Date",10)+common.Space(3)+common.Rad("No",10)+common.Space(3)+
                        common.Pad(" ",40)+common.Space(3)+common.Pad(" ",15)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Rad("Qty",6)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Pad(" ",10)+common.Space(3)+
                        common.Rad("Days",7)+common.Space(3)+common.Pad(" ",15);
          String str6 = common.Replicate("-",str5.length());

     /*  AddCellIntoTable(str1, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 4, 1, 8, 2, smallbold);
	AddCellIntoTable(str2, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 4, 1, 8, 2, smallbold);  
	//AddCellIntoTable(str3, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 4, 1, 8, 2, smallbold); 
       // AddCellIntoTable(str6, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 4, 1, 8, 2, smallbold);
	AddCellIntoTable(str4, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 4, 1, 8, 2, smallbold);  
	AddCellIntoTable(str5, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 4, 1, 8, 2, smallbold); 
        AddCellIntoTable(str6, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,  11, 4, 1, 8, 2, smallbold); */
       AddCellIntoTable("RDC Date", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 2, 2, smallbold);
	AddCellIntoTable("RDC No", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallbold);  
	AddCellIntoTable("Material Description ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallbold); 
        AddCellIntoTable("Instructions", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallbold);
	AddCellIntoTable("RDC Qty", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallbold);  
	AddCellIntoTable("Recd Qty", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallbold); 
       AddCellIntoTable("Pending Qty", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallbold); 
       AddCellIntoTable("Due Date", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallbold);  
	AddCellIntoTable("Delayed Days", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallbold); 
       AddCellIntoTable("RDC user", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallbold); 
       AddCellIntoTable("RDC Status", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallbold);  


        
          setFirstLinepdf(SName,SThro);
     }

 private void setOtherBodypdf(int i) throws Exception
     {
          String SDate    = common.parseDate((String)VORDCDate  .elementAt(i));
          String SNo      = (String)VORDCNo    .elementAt(i);
          String SDesc    = (String)VODesc     .elementAt(i);
          String SRemark  = (String)VORemark   .elementAt(i);
          String SQty     = common.getRound((String)VORDCQty   .elementAt(i),0);
          String SRecQty  = common.getRound((String)VORecQty   .elementAt(i),0);
          String SPending = common.getRound((String)VOPending  .elementAt(i),0);
          String SDueDate = common.parseDate((String)VODueDate  .elementAt(i));

          String SDelay = "";
          try
          {
               SDelay  = common.getDateDiff(TDate.toString(),SDueDate);
          }
          catch(Exception ex)
          {
               SDelay = "";
          }
          String SRdcUser = (String)VORdcUser.elementAt(i);

          String str = common.Pad(SDate,10)+common.Space(3)+common.Rad(SNo,10)+common.Space(3)+
                       common.Pad(SDesc,40)+common.Space(3)+common.Pad(SRemark,15)+common.Space(3)+
                       common.Rad(SQty,6)+common.Space(3)+common.Rad(SRecQty,6)+common.Space(3)+
                       common.Rad(SPending,6)+common.Space(3)+common.Pad(SDueDate,10)+common.Space(3)+
                       common.Rad(SDelay,7)+common.Space(3)+common.Pad(SRdcUser,15);

         AddCellIntoTable(str, table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 11, 4, 1, 8, 2, smallbold); 
         
     }

	 public class HeaderFooterPageEvent extends PdfPageEventHelper {

   public void onStartPage(PdfWriter writer, Document document) {
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(""), 30, 800, 0);
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(""), 550, 800, 0);
    }

    public void onEndPage(PdfWriter writer, Document document) {
        //ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(""), 110, 30, 0);
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("Page No :" + document.getPageNumber()), 30, 5, 0);
    }

  }

   public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }


}
