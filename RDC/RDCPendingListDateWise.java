package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCPendingListDateWise extends JInternalFrame
{

     JPanel     TopPanel;
     JPanel     BottomPanel;
     JPanel     MiddlePanel;

     String     SDate;
     JButton    BApply,BPrint;
    
     DateField TDate,TEnDate;

     MyComboBox JCAsset;

    
     Object RowData[][];

     String ColumnData[] = {"RDC Date","RDC No","Sent Through","Supplier","Description Of Goods","Uom","Send Qty","PurPose","DC No","DC Date","GI No","GI Date","GRN No","GRN Date","Received Qty","Balance Qty","Balance Value","Due Date","Delayed Days","Remarks","RDC Status"};
     String ColumnType[] = {"S"       ,"S"     ,"S"           ,"S"       ,"S"                   ,"S"  ,"N"       ,"S"      ,"S"    ,"S"      ,"N"    ,"S"      ,"N"     ,"S"       ,"N"           ,"N"          ,"N"            ,"S"       ,"N"           ,"S"      ,"S"}; 

     JLayeredPane DeskTop;
     StatusPanel SPanel;
     int iMillCode;
     String SSupTable;

     TabReport tabreport;
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;

     Vector VRDCDate,VRDCNo,VThro,VSupName,VDesc,VUom;
     Vector VRDCQty,VPurpose,VDCNo,VDCDate,VGINo,VGIDate,VGRNNo,VGRNDate;
     Vector VRecQty,VPending,VPendingValue,VDueDate,VRemarks,VAcCode,VDelay,VRDCStatus;

     int Lctr = 100,Pctr=0;

     RDCPendingListDateWise(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode,String SSupTable)
     {
         super("RDC Pending");
         this.DeskTop   = DeskTop;
         this.SPanel    = SPanel;
         this.iMillCode = iMillCode;
         this.SSupTable = SSupTable;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     private void createComponents()
     {
          TopPanel    = new JPanel(true);
          BottomPanel = new JPanel(true);
          MiddlePanel = new JPanel(true);

          TDate       = new DateField();
          TEnDate     = new DateField();

          JCAsset     = new MyComboBox();

          BApply      = new JButton("Apply");
          BPrint      = new JButton("Print");

          TDate.setTodayDate();
          TEnDate.setTodayDate();

          BApply.setMnemonic('A');

          JCAsset.addItem("Non Asset");
          JCAsset.addItem("Asset");
          JCAsset.addItem("ALL");
          JCAsset.setSelectedItem("ALL");


     }
     private void setLayouts()
     {
         TopPanel.setLayout(new FlowLayout(0,0,0));
         MiddlePanel.setLayout(new BorderLayout());

         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,790,500);
     }
     private void addComponents()
     {
          TopPanel.add(new JLabel("From"));
          TopPanel.add(TDate);
          TopPanel.add(new JLabel("To"));
          TopPanel.add(TEnDate);

          TopPanel.add(new JLabel("Asset"));
          TopPanel.add(JCAsset);


          TopPanel.add(BApply);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

     }
     private void addListeners()
     {
          BApply.addActionListener(new ActList());
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               boolean bflag = setDataIntoVector();
               if(!bflag)
                    return;
               setVectorIntoRowData();
               try
               {
                    MiddlePanel.remove(tabreport);
                    MiddlePanel.updateUI();
               }
               catch(Exception ex){}
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               MiddlePanel.add("Center",tabreport);
               MiddlePanel.updateUI();
          }
     }

     private boolean setDataIntoVector()
     {
          boolean bflag = false;

          String SStDate = TDate.toNormal();
          String SEnDate = TEnDate.toNormal();

          VRDCDate  = new Vector();
          VRDCNo    = new Vector();
          VThro     = new Vector();
          VSupName  = new Vector();
          VDesc     = new Vector();
          VUom      = new Vector();
          VRDCQty   = new Vector();
          VPurpose  = new Vector();
          VDCNo     = new Vector();
          VDCDate   = new Vector();
          VGINo     = new Vector();
          VGIDate   = new Vector();
          VGRNNo    = new Vector();
          VGRNDate  = new Vector();
          VRecQty   = new Vector();
          VPending  = new Vector();
          VPendingValue= new Vector();
          VDueDate  = new Vector();
          VRemarks  = new Vector();
          VAcCode   = new Vector();
          VDelay    = new Vector();
          VRDCStatus = new Vector();

          String SDate = common.parseDate(common.getServerPureDate());

          String QS = " Select RDC.RDCDate,RDC.RDCNo,"+SSupTable+".Name,RDC.Descript, "+
                      " RDC.Remarks,RDC.Thro,RDC.Qty,RDC.RecQty,RDC.Qty - RDC.RecQty, "+
                      " RDC.DueDate,RDC.Sup_Code,RDCStatus.Status "+
                      " From RDC Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = RDC.Sup_Code "+
                      " Left Join RDCStatus on RDCStatus.RDCID = RDC.ID and RDCStatus.ActiveStatus=0"+
                      " Where RDC.RDCDate>='"+SStDate+"' and RDC.RDCDate<='"+SEnDate+"'"+
                      " and RDC.MillCode="+iMillCode+
                      " and RDC.RecQty < RDC.Qty And RDC.AssetFlag=0  and DocType=0  ";

               if(!(JCAsset.getSelectedItem()).equals("ALL"))
                    QS = QS + " and RDC.GodownAsset="+JCAsset.getSelectedIndex();

                    QS = QS + " Order By 3,1";

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    String SRDCDate = common.parseDate(res.getString(1));
                    String SDueDate = common.parseDate(res.getString(10));
                    String SDelay   = common.getDateDiff(SDate,SRDCDate);

                    VRDCDate  .addElement(SRDCDate);
                    VRDCNo    .addElement(""+res.getString(2));
                    VSupName  .addElement(""+res.getString(3));
                    VDesc     .addElement(""+res.getString(4));
                    VRemarks  .addElement(""+res.getString(5));
                    VThro     .addElement(""+res.getString(6));
                    VRDCQty   .addElement(""+res.getString(7));
                    VRecQty   .addElement(""+res.getString(8));
                    VPending  .addElement(""+res.getString(9));
                    VDueDate  .addElement(SDueDate);
                    VAcCode   .addElement(""+res.getString(11));
                    VUom      .addElement(" ");
                    VPurpose  .addElement(" ");
                    VDCNo     .addElement(" ");
                    VDCDate   .addElement(" ");
                    VGINo     .addElement(" ");
                    VGIDate   .addElement(" ");
                    VGRNNo    .addElement(" ");
                    VGRNDate  .addElement(" ");
                    VPendingValue.addElement(" ");
                    VDelay.addElement(SDelay);
                    VRDCStatus .addElement(""+common.parseNull(res.getString(12)));
               }
               res.close();
               stat.close();
               bflag = true;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bflag = false;
          }
          return bflag;
     }

     private void setVectorIntoRowData()
     {
          RowData = new Object[VRDCNo.size()][21];
          for(int i=0;i<VRDCDate.size();i++)
          {
               RowData[i][0]  = common.parseDate((String)VRDCDate  .elementAt(i));
               RowData[i][1]  = (String)VRDCNo    .elementAt(i);
               RowData[i][2]  = (String)VThro     .elementAt(i);
               RowData[i][3]  = (String)VSupName  .elementAt(i);
               RowData[i][4]  = (String)VDesc     .elementAt(i);
               RowData[i][5]  = (String)VUom      .elementAt(i);
               RowData[i][6]  = (String)VRDCQty   .elementAt(i);
               RowData[i][7]  = (String)VPurpose  .elementAt(i);
               RowData[i][8]  = (String)VDCNo     .elementAt(i);
               RowData[i][9]  = (String)VDCDate   .elementAt(i);
               RowData[i][10] = (String)VGINo     .elementAt(i);
               RowData[i][11] = (String)VGIDate   .elementAt(i);
               RowData[i][12] = (String)VGRNNo    .elementAt(i);
               RowData[i][13] = (String)VGRNDate  .elementAt(i);
               RowData[i][14] = (String)VRecQty   .elementAt(i);
               RowData[i][15] = (String)VPending  .elementAt(i);
               RowData[i][16] = (String)VPendingValue.elementAt(i);
               RowData[i][17] = (String)VDueDate  .elementAt(i);
               RowData[i][18] = (String)VDelay    .elementAt(i);
               RowData[i][19] = (String)VRemarks  .elementAt(i);
               RowData[i][20] = (String)VRDCStatus.elementAt(i);
          }
     }
     
}
