package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;



import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;

public final class RDCPrintClassGST
{
     FileWriter FW;
     Vector theVector;
     String SRdcNo="";
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;
     int iMillCode;
     String SSupTable;
     RDCInstruction rdcInstruction;
     int iLctr = 70,iPctr = 0;
     int iWidth[] ={20,20}  ;
     Document document;
     PdfPTable table, table1,table2,table3,table4;
     int iTotalColumns = 4;
     PdfPCell c1;
     int iWidth1[] ={30,20,10,12,6,12};
     int iWidth2[] ={5,10,50,30,10,10};  
     int iWidth3[] ={20,15,15,20,10,10};
     int iWidth4[] ={20,10,15};
     private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 14, Font.BOLD);
     private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
     private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 8, Font.NORMAL);
     private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
     private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
     String         SStateName,SStateCode,GSTId;
     String SPartyCode ;
   // String PDFFile = common.getPrintPathNew()+"/RDCList.pdf";
     public RDCPrintClassGST()
     {
 try{
       
                        

       }catch(Exception ex){}

     }
     public void printWithDate(FileWriter FW,String SRdcNo,int iMillCode,String SSupTable)
     {
          try
          {
               this.SRdcNo     = SRdcNo;
               this.FW         = FW;
               this.iMillCode  = iMillCode;
               this.SSupTable  = SSupTable;

              
               setDataintoVector();
               toPdfPrint();
               toPrint();
               
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

 public void printWithDatepdf(Document document,String SRdcNo,int iMillCode,String SSupTable)
     {
          
       
          try
          {
               this.document   = document;
               this.SRdcNo     = SRdcNo;
               this.iMillCode  = iMillCode;
               this.SSupTable  = SSupTable;


               setDataintoVector();
               toPdfPrint();
             
               
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

   
     private void toPrint()
     {
          toPrtHead();
         
          toPrtBody();
         
          toPrtFoot(0);
       
     }
     private void toPdfPrint()
     {
         
          toPdfHead();
        
          toPdfBody();
          
          toPdfFoot(0);
          // document .  newPage();  
     }

    private void toPdfHead()
    {


        try
          {

                    
               document .  newPage();  

               for(int i=0; i<theVector.size(); i++)
               {
                    rdcInstruction   = (RDCInstruction)theVector.elementAt(i);
                      getAddr(); 
System.out.println("Enter into this "+i);
                    if(iMillCode==0)
                    {
                       


			Paragraph paragraph;

   
                        paragraph = new Paragraph(" ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
                        paragraph . setSpacingAfter(30);
			document  . add(paragraph);
			paragraph = new Paragraph("AMARJOTHI SPINNING MILLS LIMITED ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			document  . add(paragraph);

			paragraph = new Paragraph("Pudusuripalayam, Nambiyur - 638 458. ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			document  . add(paragraph);  

			paragraph = new Paragraph("Gobi Taluk, Erode(Dt.). ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			document  . add(paragraph);  


                       paragraph = new Paragraph("TamilNadu 33",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			paragraph . setSpacingAfter(30);
			document  . add(paragraph);  

			table  = new PdfPTable(2);
			table  . setWidths(iWidth);
			table  . setWidthPercentage(100);

			AddCellIntoTable("GSTIN   : 33AAFCA7082C1Z0  ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 0, 0, smallbold);
			AddCellIntoTable("E-mail :mill@amarjothi.net  ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 1, 8, 0, smallbold);  

			AddCellIntoTable("ph  : 04285 - 267 201, 267 301 ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 0, 0, 2, smallbold);
			AddCellIntoTable("purchase@amarjothi.net  ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 8, 2, smallbold); 

			AddCellIntoTable(" ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);
                        AddCellIntoTable(" ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);
			
			document.add(table);

			if(rdcInstruction.SDocType.startsWith("RETU"))
			{
			paragraph = new Paragraph("DELIVERY CHALLAN - "+rdcInstruction.SDocType +" ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			paragraph . setSpacingAfter(30);
			document  . add(paragraph);  
			}
			else if(rdcInstruction.SDocType.startsWith("NON"))
			{
			paragraph = new Paragraph("DELIVERY CHALLAN - "+rdcInstruction.SDocType +"",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			paragraph . setSpacingAfter(30);
			document  . add(paragraph);  

			}
                    }
                       
                    if(iMillCode==1)
                    {
                        
			Paragraph paragraph;
			paragraph = new Paragraph("AMARJOTHI SPINNING MILLS (DYEING DIVISION)  ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			document  . add(paragraph);

			paragraph = new Paragraph("SIPCOT INDUSTRIAL AREA ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			document  . add(paragraph);  

			paragraph = new Paragraph("PERUNDURAI  -  638 052 ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			document  . add(paragraph);  
 
                       paragraph = new Paragraph("Erode Dt",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			document  . add(paragraph);


                        paragraph = new Paragraph("TamilNadu 33 ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			paragraph . setSpacingAfter(30);
			document  . add(paragraph);  

			table  = new PdfPTable(2);
			table  . setWidths(iWidth);
			table  . setWidthPercentage(100);

			AddCellIntoTable("GSTIN   : 33AAFCA7082C1Z0  ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 0, 0, smallbold);
			AddCellIntoTable("E-mail :mill@amarjothi.net  ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 1, 8, 0, smallbold);  

			AddCellIntoTable("ph  : 04285 - 267 201, 267 301 ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);
			AddCellIntoTable("purchase@amarjothi.net  ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold); 

			
			AddCellIntoTable("", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);
                        AddCellIntoTable(" ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold); 


			document.add(table);
			if(rdcInstruction.SDocType.startsWith("RETU"))
			{
			paragraph = new Paragraph("DELIVERY CHALLAN  - "+rdcInstruction.SDocType +" ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			paragraph . setSpacingAfter(30);
			document  . add(paragraph);  
			}
			else if(rdcInstruction.SDocType.startsWith("NON"))
			{
			paragraph = new Paragraph("DELIVERY CHALLAN -  "+rdcInstruction.SDocType +" ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			paragraph . setSpacingAfter(30);
			document  . add(paragraph);  
			}
                    }
                    if(iMillCode==3)
                    {
                        

			Paragraph paragraph;
			paragraph = new Paragraph("AMARJOTHI COLOUR MELANGE SPINNING MILLS LIMITED  ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			document  . add(paragraph);

			paragraph = new Paragraph("Pudusuripalayam, Nambiyur - 638 458. ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			document  . add(paragraph);  

			paragraph = new Paragraph("Gobi Taluk, Erode(Dt.). ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			paragraph . setSpacingAfter(30);
			document  . add(paragraph);  

			table  = new PdfPTable(2);
			table  . setWidths(iWidth);
			table  . setWidthPercentage(100);

			AddCellIntoTable("TIN : 33712404816 ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);
			AddCellIntoTable("CST    : 1031365  dt.05.05.2010   ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);  

			AddCellIntoTable("ph  :  04285 - 267 201, 267 301 ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);
			AddCellIntoTable(" E-mail : ajsmmill@yahoo.co.in ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold); 

			AddCellIntoTable("Fax : 04285 - 267 565 ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);
			AddCellIntoTable("purchase@amarjothi.net ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);

			document.add(table);

			if(rdcInstruction.SDocType.startsWith("RETU"))
			{
			paragraph = new Paragraph("DELIVERY CHALLAN  - "+rdcInstruction.SDocType +" ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			paragraph . setSpacingAfter(30);
			document  . add(paragraph);  
			}
			else if(rdcInstruction.SDocType.startsWith("NON"))
			{
			paragraph = new Paragraph("DELIVERY CHALLAN  - "+rdcInstruction.SDocType +"",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			paragraph . setSpacingAfter(30);
			document  . add(paragraph);  
			}
                    }

                    if(iMillCode==4)
                    {

			Paragraph paragraph;
			paragraph = new Paragraph("SRI KAMADHENU TEXTILES ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			document  . add(paragraph);

			paragraph = new Paragraph("Pudusuripalayam, Nambiyur - 638 458. ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			document  . add(paragraph);  

			paragraph = new Paragraph("Gobi Taluk, Erode(Dt.). ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			paragraph . setSpacingAfter(30);
			document  . add(paragraph);  

			table  = new PdfPTable(2);
			table  . setWidths(iWidth);
			table  . setWidthPercentage(100);

			AddCellIntoTable("TIN : 33612404386 ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);
			AddCellIntoTable("CST    :  972942 dt. 14.12.2008     ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);  

			AddCellIntoTable("ph  :  04285 - 267 201, 267 301 ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);
			AddCellIntoTable(" E-mail : ajsmmill@yahoo.co.in ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold); 

			AddCellIntoTable("Fax : 04285 - 267 565 ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);
			AddCellIntoTable("purchase@amarjothi.net ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 0, 0, 0, 0, smallbold);

			document.add(table);

			if(rdcInstruction.SDocType.startsWith("RETU"))
			{

			paragraph = new Paragraph("DELIVERY CHALLAN  - "+rdcInstruction.SDocType +"",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			paragraph . setSpacingAfter(30);
			document  . add(paragraph);  

			}
			else if(rdcInstruction.SDocType.startsWith("NON"))
			{
			paragraph = new Paragraph("DELIVERY CHALLAN  - "+rdcInstruction.SDocType +" ",bigbold);
			paragraph . setAlignment(Element.ALIGN_CENTER);
			paragraph . setSpacingAfter(30);
			document  . add(paragraph);  
			}
                    }

                   
                    
                    table1  = new PdfPTable(6);
		    table1  . setWidths(iWidth1);
		    table1  . setWidthPercentage(100);


                    AddCellIntoTable("Name of the consignee:", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 2, mediumbold);
		    AddCellIntoTable("RDC No.     ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
                    AddCellIntoTable(String.valueOf(common.Pad(rdcInstruction.SRdcNo,12) ), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, bigbold);
                    AddCellIntoTable("Date    ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
                    AddCellIntoTable(String.valueOf(common.parseDate(rdcInstruction.SRdcDate)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
 
                     AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 0, mediumbold);
		    AddCellIntoTable("Ref No.     ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,2); 
                    AddCellIntoTable(String.valueOf(common.Pad(rdcInstruction.SRefNo,12) ), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,2);
                    AddCellIntoTable("Date    ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,2);
                    AddCellIntoTable(String.valueOf(common.parseDate(rdcInstruction.SRefDate)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,2);
  
                     AddCellIntoTable(String.valueOf(common.Pad(rdcInstruction.SName,73)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 5, 4, 0, 8, 0, mediumbold);
		 

                  
                  
				AddCellIntoTable(String.valueOf(common.Pad(rdcInstruction.SAddress1,73)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 0, 8, 0, smallnormal);
				AddCellIntoTable("Inward No.  ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
				AddCellIntoTable(String.valueOf(common.Pad(rdcInstruction.SInwNo,12)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
				AddCellIntoTable("Date ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
				AddCellIntoTable(String.valueOf(common.Cad(common.parseDate(rdcInstruction.SInwDate),12)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);


				AddCellIntoTable(String.valueOf(common.Pad(rdcInstruction.SAddress2,73)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 0, 8, 0, smallnormal);
				AddCellIntoTable("Rej No.  ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
				AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
				AddCellIntoTable("Date", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
				AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);


				AddCellIntoTable(String.valueOf(common.Pad(rdcInstruction.SAddress3,73)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 0, 8, 0, smallnormal);
				AddCellIntoTable("GI No.  ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
				AddCellIntoTable(String.valueOf(common.Pad(rdcInstruction.SGiNo,12)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
				AddCellIntoTable("Date ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
				AddCellIntoTable(String.valueOf(common.Cad(common.parseDate(rdcInstruction.SGiDate),12)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);

				AddCellIntoTable(SStateName, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 0, 0, 0, smallnormal);
				AddCellIntoTable("Bill No  ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
				AddCellIntoTable(String.valueOf(common.Pad(rdcInstruction.SBillNo,12)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
				AddCellIntoTable("Date ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
				AddCellIntoTable(String.valueOf(common.Cad(common.parseDate(rdcInstruction.SBillDate),12)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);

				AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 0, 0, 0, smallnormal);
				AddCellIntoTable("Asset  ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal); 
				AddCellIntoTable(String.valueOf(common.Pad(rdcInstruction.SAsset,12)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
				AddCellIntoTable(" ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);
				AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal);


				AddCellIntoTable("GSTIN  : "+GSTId, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 0, 0, 0, smallnormal);
				AddCellIntoTable("MBD  No  ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,2); 
				AddCellIntoTable(String.valueOf(common.Pad(rdcInstruction.SMBDNo,12)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,2);
				AddCellIntoTable("Date ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,2);
				AddCellIntoTable(String.valueOf(common.Cad(common.parseDate(rdcInstruction.SMBDDate),12)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, smallnormal,2);


				AddCellIntoTable("Ph No :"+String.valueOf(common.Pad(rdcInstruction.SPhoneNum,73)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 2, smallnormal);
				AddCellIntoTable("State Code  - "+SStateCode, table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 


                    AddCellIntoTable("We are despatching the materials to your good office as per the details given below: ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 5, 4, 1, 8, 2, smallnormal);

                   document.add(table1);
                   table2  = new PdfPTable(6);
		   table2  . setWidths(iWidth2);
		   table2  . setWidthPercentage(100);

                 

                    AddCellIntoTable("S No", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
                  //  AddCellIntoTable("Code ", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
                   // AddCellIntoTable("Name ", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
                    AddCellIntoTable("HSN/SAC Code ", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		    AddCellIntoTable("  DESCRIPTION OF GOODS   ", table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
                    AddCellIntoTable("PURPOSE", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
                    AddCellIntoTable("QTY ", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
                  //  AddCellIntoTable("RATE", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
                    AddCellIntoTable("VALUE ", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
                   
                
       }
       iLctr=30;
                    
   }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

    }
 private void toPdfBody()
    {

      int iSlNo=1;
     double dRate=0;
     double dValue=0; 
          try
          {
               for(int i=0; i<theVector.size(); i++)
               {
                 
                    rdcInstruction   = (RDCInstruction)theVector.elementAt(i);

		    int iItemCount = 0;

                    for(int j=0; j<rdcInstruction.VDescript.size(); j++)
                    {
			 iItemCount++;

			/* if(iItemCount>15)
			 {
				foot
				newpage
				head
				iItemCount=0;
			 }*/
                         if(iItemCount>10)
			 {
                            
                           document.add(table2);
                           table2  = new PdfPTable(6); 
                           toPdfFoot(1);
                           document.newPage(); 
				//document.add(table); document.add(table1);document.add(table2);
                           toPdfHead(); 
                           iItemCount=0;
                           
                         }
                      
                          
                         String SDescript = (String)rdcInstruction.VDescript.elementAt(j);
                         String SRemarks  = (String)rdcInstruction.VRemarks.elementAt(j);
                        dRate+= common.toDouble((String)rdcInstruction.VRDCRate.elementAt(j));
                        dValue+= common.toDouble((String)rdcInstruction.VRDCValue.elementAt(j));


			AddCellIntoTable((String.valueOf(iSlNo)), table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 2,smallnormal);
                      //  AddCellIntoTable((String)rdcInstruction.VItemCode.elementAt(j), table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 20f,4, 1, 8, 2, smallnormal); 
			//AddCellIntoTable((String)rdcInstruction.VItemName.elementAt(j), table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 20f,4, 1, 8, 2, smallnormal); 
			AddCellIntoTable((String)rdcInstruction.VHsnCode.elementAt(j), table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 20f,4, 1, 8, 2, smallnormal); 
			AddCellIntoTable(SDescript, table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 20f,4, 1, 8, 2, smallnormal); 
			AddCellIntoTable(SRemarks, table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 30f,4, 1, 8, 2, smallnormal);
			AddCellIntoTable(( (((String)rdcInstruction.VQty.elementAt(j)))+"    "+((String)rdcInstruction.VUom.elementAt(j))), table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,20f, 4, 1, 8, 2, smallnormal);
			//AddCellIntoTable((String)rdcInstruction.VRDCRate.elementAt(j), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 20f,4, 1, 8, 2, smallnormal);
			AddCellIntoTable((String)rdcInstruction.VRDCValue.elementAt(j), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 20f,4, 1, 8, 2, smallnormal);

                     
                     

                         iSlNo++;

			


                    


                       
                        // iLctr++;
                    }


                    AddCellIntoTable("Total", table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 3, 20f,4, 1, 8, 2, smallnormal);
			AddCellIntoTable("", table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 20f,4, 1, 8, 2, smallnormal);
			AddCellIntoTable("", table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 20f,4, 1, 8, 2, smallnormal);
			//AddCellIntoTable("", table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 20f,4, 1, 8, 2, smallnormal);
			AddCellIntoTable(String.valueOf(dValue), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 20f,4, 1, 8, 2, smallnormal);

                  
                  document.add(table2);
                    

               }

                   
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }   
 
    }
 private void toPdfFoot(int iSig)
     {
        try{
System.out.println("Enter into foot");
    
                    table3  = new PdfPTable(6);
		    table3  . setWidths(iWidth3);
		    table3  . setWidthPercentage(100);


               
                    AddCellIntoTable("Send Through / vehicle no", table3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,30f, 4, 1, 8, 2, smallnormal);
		    AddCellIntoTable(String.valueOf(rdcInstruction.SThro), table3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 30f,4, 1, 8, 2, smallnormal); 
                    AddCellIntoTable("Name of the person", table3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,30f, 4, 1, 8, 2, smallnormal);
                    AddCellIntoTable(String.valueOf(rdcInstruction.SPerson), table3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,30f, 4, 1, 8, 2, smallnormal);
                    AddCellIntoTable("Time out", table3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,30f, 4, 1, 8, 2, smallnormal);
                    AddCellIntoTable("   ", table3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,30f, 4, 1, 8, 2, smallnormal);

              

                    document.add(table3);

                    table4  = new PdfPTable(3);
		    table4  . setWidths(iWidth4);
		    table4  . setWidthPercentage(100);

                    AddCellIntoTable("   ", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 50f,4, 1, 8, 2, mediumbold);
                    AddCellIntoTable("", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 50f,4, 1, 8, 2, mediumbold);
                    AddCellIntoTable("   ", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,50f, 4, 1, 8, 2, mediumbold);  


                    AddCellIntoTable("Prepared by   ", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
                    AddCellIntoTable("Checked by", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
                    AddCellIntoTable("Authorised by   ", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);

                    AddCellIntoTable("Instructions:   ", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 1, 8, 0, mediumbold);
                    AddCellIntoTable("", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 0, mediumbold);

                    AddCellIntoTable("1.Original & Duplicate to be sent to party and triplicate copy with    ", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 0, 8, 0, smallnormal);
                    AddCellIntoTable("", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);

                    AddCellIntoTable("Stores files. ", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 0, 8, 0, smallnormal); 
                    AddCellIntoTable("", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);

                    AddCellIntoTable("2.The party should return the Duplicate copy duly signed for   ", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 0, 8, 0, smallnormal);
                    AddCellIntoTable("", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);

                    AddCellIntoTable("acknowledgement.  ", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 0, 8, 0, smallnormal);
                    AddCellIntoTable("", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal); 


                    AddCellIntoTable("3.This is not for sale.It Will be returned after servicing   ", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 0, 8, 0, smallnormal);
                    AddCellIntoTable("", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 0, 8, 0, smallnormal);


                     AddCellIntoTable(" ", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 4, 0, 8, 2, smallnormal);
                    AddCellIntoTable(" Goods received by (Sign & Seal)", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
                    
                    if(iSig==0)
               {
                    AddCellIntoTable(" End of the report ", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 3, 0, 0, 0, 0, mediumbold);
                   
               }
               else
               {
                   AddCellIntoTable(" (Continued on Next Page) ", table4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 3, 0, 0, 0, 0, mediumbold);
                  
               }

                 document.add(table4);
              // document.newPage();
                 //document.close();
 }catch(Exception ex){}
     }
 
     private void toPrtHead()
     {
          try
          {
               if(iLctr < 44)
               {
                    return;
               }
               if(iPctr > 0)
               {
                    //FW.write("-------------------------------------------------------------------------------------------------------------------------|"+"\n");
                    //FW.write("                                                                                                       ...contd          "+"\n");
                    toPrtFoot(1);
               }
               iPctr++;

               for(int i=0; i<theVector.size(); i++)
               {
                    rdcInstruction   = (RDCInstruction)theVector.elementAt(i);

                    FW.write("g\n");
                    FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
                    FW.write("|                                                                                                                        |"+"\n");

                      
                      

                    if(iMillCode==0)
                    {
                         FW.write("|                            E AMARJOTHI SPINNING MILLS LIMITED F@g                                                                                                                         |"+"\n");
                         FW.write("|E"+common.Cad("Pudusuripalayam, Nambiyur - 638 458. ",120)+"F|"+"\n");
                         FW.write("|E"+common.Cad("Gobi Taluk, Erode(Dt.). ",120)+"F|"+"\n");
                         FW.write("|                                                                                                                        |"+"\n");
                         FW.write("|  ETIN : 33632960864F                                                               CST    : 440691 dt.24.9.1990          |"+"\n");
                         FW.write("|  Eph  : 04285 - 267 201, 267 301F                                                  E-mail : ajsmmill@yahoo.co.in         |"+"\n");
                         FW.write("|  EFax : 04285 - 267 565F                                                                    purchase@amarjothi.net       |"+"\n");
                         FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
             

                     
                         if(rdcInstruction.SDocType.startsWith("RETU"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                             

                         }
                         else if(rdcInstruction.SDocType.startsWith("NON"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                       
                          }
                    }
                       
                    if(iMillCode==1)
                    {
                         FW.write("|                   E AMARJOTHI SPINNING MILLS (DYEING DIVISION) F@g                                                                                                                         |"+"\n");
                         FW.write("|E"+common.Cad("SIPCOT INDUSTRIAL AREA",120)+"F|"+"\n");
                         FW.write("|E"+common.Cad("PERUNDURAI  -  638 052",120)+"F|"+"\n");
                         FW.write("|                                                                                                                        |"+"\n");
                         FW.write("|  ETIN : 33632960864F                                                              CST    : 440691 dt.24.09.1990          |"+"\n");
                         FW.write("|  Eph  : 04294 - 309339, 267 301F                                                  E-mail : ajsmmill@yahoo.co.in          |"+"\n");
                         FW.write("|  EFax : 04294 - 230392F                                                                    purchase@amarjothi.net        |"+"\n");
                         FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");

                         if(rdcInstruction.SDocType.startsWith("RETU"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                         else if(rdcInstruction.SDocType.startsWith("NON"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                    }
                    if(iMillCode==3)
                    {
                         FW.write("|            E   AMARJOTHI COLOUR MELANGE SPINNING MILLS LIMITED   F@g                                                                                                                         |"+"\n");
                         FW.write("|E"+common.Cad("Pudusuripalayam, Nambiyur - 638 458. ",120)+"F|"+"\n");
                         FW.write("|E"+common.Cad("Gobi Taluk, Erode(Dt.). ",120)+"F|"+"\n");
                         FW.write("|                                                                                                                        |"+"\n");
                         FW.write("|  ETIN : 33712404816F                                                               CST    : 1031365  dt.05.05.2010       |"+"\n");
                         FW.write("|  Eph  : 04285 - 267 201, 267 301F                                                  E-mail : ajsmmill@yahoo.co.in         |"+"\n");
                         FW.write("|  EFax : 04285 - 267 565F                                                                    purchase@amarjothi.net       |"+"\n");
                         FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");



                         if(rdcInstruction.SDocType.startsWith("RETU"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                         else if(rdcInstruction.SDocType.startsWith("NON"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                    }

                    if(iMillCode==4)
                    {
                         FW.write("|                                   E SRI KAMADHENU TEXTILES F@g                                                                                                                         |"+"\n");
                         FW.write("|E"+common.Cad("Pudusuripalayam, Nambiyur - 638 458. ",120)+"F|"+"\n");
                         FW.write("|E"+common.Cad("Gobi Taluk, Erode(Dt.). ",120)+"F|"+"\n");
                         FW.write("|                                                                                                                        |"+"\n");
                         FW.write("|  ETIN : 33612404386F                                                               CST    : 972942 dt. 14.12.2008        |"+"\n");
                         FW.write("|  Eph  : 04285 - 267 201, 267 301F                                                  E-mail : ajsmmill@yahoo.co.in         |"+"\n");
                         FW.write("|  EFax : 04285 - 267 565F                                                                    purchase@amarjothi.net       |"+"\n");
                         FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");

                       

                         if(rdcInstruction.SDocType.startsWith("RETU"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                         else if(rdcInstruction.SDocType.startsWith("NON"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                    }


                    FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
                    //FW.write("| EName of the consignee:F                                                   |ERDC No.   F|E"+common.Pad(rdcInstruction.SRdcNo,12)+"F| EDate   F|E"+common.Cad(common.parseDate(rdcInstruction.SRdcDate),12)+"F|"+"\n");
                    FW.write("| EName of the consignee:F                                                   |ERDC No.   F|E"+common.Pad(rdcInstruction.SRdcNo,12)+"F@g"+common.Space(100)+"| EDate   F|E"+common.Cad(common.parseDate(rdcInstruction.SRdcDate),12)+"F|"+"\n");
                    FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
                    FW.write("|                                                                          |Ref.No.   |"+common.Pad(rdcInstruction.SRefNo,12)+"| Date   |"+common.Cad(common.parseDate(rdcInstruction.SRefDate),12)+"|"+"\n");
                    FW.write("| "+common.Pad(rdcInstruction.SName,73)+"|---------------------------------------------|"+"\n");


                    if((rdcInstruction.SAddress1).equals("na"))
                    {
                         FW.write("| "+common.Pad("",73)+"|Inward No.|"+common.Pad(rdcInstruction.SInwNo,12)+"| Date   |"+common.Cad(common.parseDate(rdcInstruction.SInwDate),12)+"|"+"\n");
                    }
                    else
                    {
                         FW.write("| "+common.Pad(rdcInstruction.SAddress1,73)+"|Inward No.|"+common.Pad(rdcInstruction.SInwNo,12)+"| Date   |"+common.Cad(common.parseDate(rdcInstruction.SInwDate),12)+"|"+"\n");
                    }
                    if((rdcInstruction.SAddress2).equals("na"))
                    {
                         FW.write("| "+common.Pad("",73)+"|---------------------------------------------|"+"\n");
                    }
                    else
                    {
                         FW.write("| "+common.Pad(rdcInstruction.SAddress2,73)+"|---------------------------------------------|"+"\n");
                    }
                    FW.write("| "+common.Pad(rdcInstruction.SAddress3,73)+"|GI No.    |"+common.Pad(rdcInstruction.SGiNo,12)+"| Date   |"+common.Cad(common.parseDate(rdcInstruction.SGiDate),12)+"|"+"\n");


                    if((rdcInstruction.SPlace).equals("UNKNOWN"))
                    {
                         FW.write("| "+common.Pad("",73)+"|---------------------------------------------|"+"\n");
                    }
                    else
                    {
                         FW.write("| "+common.Pad(rdcInstruction.SPlace,73)+"|---------------------------------------------|"+"\n");
                    }
                    FW.write("|                                                                          |Bill No.  |"+common.Pad(rdcInstruction.SBillNo,12)+"| Date   |"+common.Cad(common.parseDate(rdcInstruction.SBillDate),12)+"|"+"\n");
                    FW.write("| "+common.Pad("",73)+"|---------------------------------------------|"+"\n");
                    FW.write("|                                                                          |MBD  No.  |"+common.Pad(rdcInstruction.SMBDNo,12)+"| Date   |"+common.Cad(common.parseDate(rdcInstruction.SMBDDate),12)+"|"+"\n");
                    FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
                    FW.write("| We are despatching the materials to your good office as per the details given below:                                   |"+"\n");
                    FW.write("|                                                                                                                        |"+"\n");
                    FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
                    FW.write("| SNO |                   DESCRIPTION OF GOODS             |      QTY      |                   PURPOSE                   |"+"\n");
                    FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
               }
               iLctr=30;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void toPrtBody()
     {
          int iSlNo=1;

          try
          {
               for(int i=0; i<theVector.size(); i++)
               {
                    rdcInstruction   = (RDCInstruction)theVector.elementAt(i);

                    for(int j=0; j<rdcInstruction.VDescript.size(); j++)
                    {
                         toPrtHead();

                         String SDescript = (String)rdcInstruction.VDescript.elementAt(j);
                         String SRemarks  = (String)rdcInstruction.VRemarks.elementAt(j);

                         if((SDescript.length()<=51) && (SRemarks.length()<=44))
                         {
                              FW.write("|"+common.Cad(String.valueOf(iSlNo),5)+"|"+
                                           common.Pad(common.Space(1)+SDescript,52)+"|"+
                                           common.Rad(common.getRound((String)rdcInstruction.VQty.elementAt(j),3)+
                                           common.Space(1),11)+common.Pad((String)rdcInstruction.VUom.elementAt(j),4)+"|"+
                                           common.Pad(common.Space(1)+SRemarks,45)+"|\n");

                              iLctr++;
                         }
                         else if((SDescript.length()>51) && (SRemarks.length()<=44))
                         {
                              String SDesc1 = SDescript.substring(0,50)+"-";
                              String SDesc2 = "-"+SDescript.substring(50,SDescript.length());

                              FW.write("|"+common.Cad(String.valueOf(iSlNo),5)+"|"+
                                           common.Pad(common.Space(1)+SDesc1,52)+"|"+
                                           common.Rad(common.getRound((String)rdcInstruction.VQty.elementAt(j),3)+
                                           common.Space(1),11)+common.Pad((String)rdcInstruction.VUom.elementAt(j),4)+"|"+
                                           common.Pad(common.Space(1)+(String)rdcInstruction.VRemarks.elementAt(j),45)+"|\n");

                              FW.write("|"+common.Pad(common.Space(5),5)+"|"+
                                           common.Rad(SDesc2,52)+"|"+
                                           common.Pad(common.Space(15),15)+"|"+
                                           common.Pad(common.Space(45),45)+"|\n");

                              iLctr=iLctr+2;
                         }
                         else if((SDescript.length()<=51) && (SRemarks.length()>44))
                         {

                              String SRem1 = SRemarks.substring(0,43)+"-";
                              String SRem2 = "-"+SRemarks.substring(43,SRemarks.length());

                              FW.write("|"+common.Cad(String.valueOf(iSlNo),5)+"|"+
                                           common.Pad(common.Space(1)+SDescript,52)+"|"+
                                           common.Rad(common.getRound((String)rdcInstruction.VQty.elementAt(j),3)+
                                           common.Space(1),11)+common.Pad((String)rdcInstruction.VUom.elementAt(j),4)+"|"+
                                           common.Pad(common.Space(1)+SRem1,45)+"|\n");

                              FW.write("|"+common.Pad(common.Space(5),5)+"|"+
                                           common.Pad(common.Space(52),52)+"|"+
                                           common.Pad(common.Space(15),15)+"|"+
                                           common.Rad(SRem2,45)+"|\n");

                              iLctr=iLctr+2;
                         }
                         else if((SDescript.length()>51) && (SRemarks.length()>44))
                         {

                              String SDesc1 = SDescript.substring(0,50)+"-";
                              String SDesc2 = "-"+SDescript.substring(50,SDescript.length());

                              String SRem1 = SRemarks.substring(0,43)+"-";
                              String SRem2 = "-"+SRemarks.substring(43,SRemarks.length());

                              FW.write("|"+common.Cad(String.valueOf(iSlNo),5)+"|"+
                                           common.Pad(common.Space(1)+SDesc1,52)+"|"+
                                           common.Rad(common.getRound((String)rdcInstruction.VQty.elementAt(j),3)+
                                           common.Space(1),11)+common.Pad((String)rdcInstruction.VUom.elementAt(j),4)+"|"+
                                           common.Pad(common.Space(1)+SRem1,45)+"|\n");

                              FW.write("|"+common.Pad(common.Space(5),5)+"|"+
                                           common.Rad(SDesc2,52)+"|"+
                                           common.Pad(common.Space(15),15)+"|"+
                                           common.Rad(SRem2,45)+"|\n");

                              iLctr=iLctr+2;
                         }


                         iSlNo++;

                         FW.write("|"+common.Space(5)+"|"+
                                      common.Space(52)+"|"+
                                      common.Space(15)+"|"+
                                      common.Space(45)+"|\n");
                         iLctr++;
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void toPrtFoot(int iSig)
     {
          try
          {

               FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
               FW.write("|SENT THROUGH   Vehicle No  |"+common.Pad(rdcInstruction.SThro,22)+"|Name of the|"+common.Pad(rdcInstruction.SPerson,33)+"| Time  |               |"+"\n");
               FW.write("|                           |                      |  person   |                                 | out   |               |"+"\n");
               FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
               FW.write("|                           |                                              |                                             |"+"\n");          
             //FW.write("|                           |                                              |                                             |"+"\n");          
               FW.write("|                           |                                              |                                             |"+"\n");
               FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
               FW.write("|          Prepared by      |                     Checked by               |               Authorised by                 |"+"\n");
               FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
               FW.write("|Instructions:                                                             |                                             |"+"\n");
               FW.write("|                                                                          |                                             |"+"\n");
               FW.write("|1.Original & Duplicate to be sent to party and triplicate copy with       |                                             |"+"\n");
               FW.write("|  Stores files.                                                           |                                             |"+"\n");
               FW.write("|2.The party should return the Duplicate copy duly signed for              |                                             |"+"\n");
               FW.write("|  acknowledgement.                                                        |---------------------------------------------|"+"\n");
               FW.write("|                                                                          |       Goods received by (Sign & Seal)       |"+"\n");
               FW.write("-------------------------------------------------------------------------------------------------------------------------|"+"\n");
               if(iSig==0)
               {
                    FW.write("< End Of Report >");
               }
               else
               {
                    FW.write("                                                                                         (Continued on Next Page)        "+"\n");
               }

               //FW.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void setDataintoVector()
     {
          theVector  = new Vector();

           try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement theStatement   = theconnect.createStatement();
               ResultSet theResult      = theStatement.executeQuery(getRDCQs());

               while(theResult.next())
                    organizeRDC(theResult);
               theResult.close();

               theStatement.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               System.out.println(ex);
          }
     }
     private String getRDCQs()
     {
          String QS = " select RdcNo,RdcDate,RefNo,RefDate,RefInwNo,RefInwDate,RefGiNo,RefGiDate,RefBillNo, "+
                      " RefBillDate,thro,Person,Name,nvl(Addr1,'na'),nvl(Addr2,'na'),Addr3, nvl(PlaceName,'UNKNOWN'),Descript,Qty,Remarks, "+
                      " decode(DocType,0,'RETURNABLE','NON RETURNABLE') as DocType,MBDNo,Uom.UomName,MBDDate,phone,rdcrate,rdcvalue,invitems.hsncode,Invitems.Item_name,InvItems.Item_code,supplier.AC_code "+
                      " ,RDC.GodownAsset From RDC "+
                      " Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = RDC.Sup_Code "+
                      " Left Join Place on Place.PlaceCode = "+SSupTable+".City_Code "+
                      " Inner Join Uom on RDC.UomCode = Uom.UomCode "+
                      " left join invitems on invitems.ITEM_CODE=rdc.HSNMATCODE "+
                      " Where RdcNo = "+SRdcNo+" and Rdc.MillCode="+iMillCode+"  ";// and RDC.SOAUthStaus=1
System.out.println("QS"+QS);  
          return QS;

     }
     private void organizeRDC(ResultSet theResult) throws Exception
     {
          RDCInstruction   rdcInstruction  = null;

          String SRdcNo     =    common.parseNull(theResult.getString(1));
          String SRdcDate   =    common.parseNull(theResult.getString(2));
          String SRefNo     =    common.parseNull(theResult.getString(3));
          String SRefDate   =    common.parseNull(theResult.getString(4));
          String SInwNo     =    common.parseNull(theResult.getString(5));
          String SInwDate   =    common.parseNull(theResult.getString(6));
          String SGiNo      =    common.parseNull(theResult.getString(7));
          String SGiDate    =    common.parseNull(theResult.getString(8));
          String SBillNo    =    common.parseNull(theResult.getString(9));
          String SBillDate  =    common.parseNull(theResult.getString(10));
          String SThro      =    common.parseNull(theResult.getString(11));
          String SPerson    =    common.parseNull(theResult.getString(12));
          String SName      =    common.parseNull(theResult.getString(13));
          String SAddress1  =    common.parseNull(theResult.getString(14));
          String SAddress2  =    common.parseNull(theResult.getString(15));
          String SAddress3  =    common.parseNull(theResult.getString(16));
          String SPlace     =    common.parseNull(theResult.getString(17));
          String SDescript  =    common.getNarration(common.parseNull(theResult.getString(18)));
          double dQty       =    theResult.getDouble(19);
          String SRemarks   =    common.parseNull(theResult.getString(20));
          String SDocType   =    common.parseNull(theResult.getString(21));
          String SMBDNo     =    common.parseNull(theResult.getString(22));
          String SUom       =    common.parseNull(theResult.getString(23));
          String SMBDDate   =    common.parseNull(theResult.getString(24));
          String SPhoneNum  =    common.parseNull(theResult.getString(25));
          double drdcrate   =    theResult.getDouble(26);
          double drdcvalue  =    theResult.getDouble(27);
          String SHsncode   =    common.parseNull(theResult.getString(28)); 
          String SItemName   =    common.parseNull(theResult.getString(29)); 
          String SItemCode  =    common.parseNull(theResult.getString(30)); 
          SPartyCode =   theResult.getString(31);
		  String SAssetVal  =    common.parseNull(theResult.getString(32));
		  String SAsset		= "";
		  if(SAssetVal.equals("0"))
		  {
			  SAsset = "No";
		  }
		  else
		  {
			 SAsset = "Yes";
		  }


          int iIndex = getIndexOf(SRdcNo);

          if(iIndex==-1)
          {
               rdcInstruction  = new RDCInstruction(SRdcNo,SRdcDate,SRefNo,SRefDate,SInwNo,SInwDate,SBillNo,SBillDate,SThro,SPerson,SName,SAddress1,SAddress2,SAddress3,SPlace,SGiNo,SGiDate,SDocType,SMBDNo,SUom,SMBDDate,SPhoneNum,SAsset);
               theVector.addElement(rdcInstruction);
               iIndex = theVector.size()-1;
          }
          rdcInstruction = (RDCInstruction)theVector.elementAt(iIndex);
          rdcInstruction.append(SDescript,dQty,SRemarks,SUom,drdcrate,drdcvalue,SHsncode,SItemName,SItemCode);
     }
     private int getIndexOf(String SRdcNo)
     {
          int iIndex =-1;
          for(int i=0; i<theVector.size(); i++)
          {
               RDCInstruction rdcInstruction = (RDCInstruction)theVector.elementAt(i);
               if((rdcInstruction.SRdcNo).equals(SRdcNo))
               {
                    iIndex =i;
                    break;
               }
          }
          return iIndex;
     }
public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }
 private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,Font docFont)
      {
          c1.setHorizontalAlignment(iHorizontal);
          c1.hasMinimumHeight();
          c1.setBorder(0);
          table.addCell(c1);
      }


//gst party

  public void getAddr()
     {

       

         
System.out.println("Enter into this");

         SStateName ="";
         SStateCode="";
         GSTId="";

        String QS1="";

         

          try
          {

                  QS1=" select distinct state.statename,state.GSTSTATECODE,case  when GSTPartyTypeCode ='1' then gstinid else scm.gstpartytype.name end from partymaster "+
	             " inner join state on state.STATECODE=partymaster.statecode "+
                     " inner join scm.gstpartytype on scm.gstpartytype.code=partymaster.GSTPARTYTYPECODE  "+
                     " Where partymaster.partycode='"+SPartyCode+"' ";

           
       

System.out.println("QS1======>"+QS1);
         

               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement theStatement   = theconnect.createStatement();
               ResultSet rs1      = theStatement.executeQuery(QS1);


             
               if(rs1.next())
               {
                    SStateName           = common.parseNull(rs1.getString(1));
                    SStateCode           = common.parseNull(rs1.getString(2));
                    GSTId                = common.parseNull(rs1.getString(3));

               }
               rs1                           . close();

               theStatement . close();
          }
          catch(Exception ex){System.out.println(ex);ex.printStackTrace();}
     }

    

}

