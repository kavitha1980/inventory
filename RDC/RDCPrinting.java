package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;


public class RDCPrinting extends JInternalFrame
{
      JLayeredPane Layer;

      JPanel         TopPanel,BottomPanel,MiddlePanel,TopLeft,TopRight;
      AddressField   TShort;
      MyComboBox     JCDept,JCMachine,JCServDept,JCUnit;
      JButton        BApply,BOk,BCancel,BPool,BCreatePDF;
      TabReport      theReport;
      JTabbedPane    thePane;
      DateField      fromDate,toDate;
      Vector         VRDCNo,VRDCDate,VSupplier,VGSTStatus;
      FileWriter     FW;

      Common common   = new Common();
      int iUserCode,iMillCode;
      String SSupTable;

      ORAConnection connect;
      Connection theconnect;


   Document document;
    PdfPTable table, table1;
    int iTotalColumns = 4;
    int iWidth[] = {5,10, 10, 15};
    
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 14, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 9, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    String PDFFile = common.getPrintPath()+"/RDCList.pdf";

      public RDCPrinting(JLayeredPane Layer,int iUserCode,int iMillCode,String SSupTable)
      {
            this.Layer     = Layer;
            this.iUserCode = iUserCode;
            this.iMillCode = iMillCode;
            this.SSupTable = SSupTable;

            createComponents();
            setLayouts();
            addComponents();
            addListeners();
      }
      public void createComponents()
      {
            try
            {
                 thePane     = new JTabbedPane();
                 fromDate    = new DateField();
                 toDate      = new DateField();

                 fromDate.setTodayDate();
                 toDate.setTodayDate();

                 TopPanel    = new JPanel(true);
                 TopLeft     = new JPanel(true);
                 TopRight    = new JPanel(true);
                 BottomPanel = new JPanel(true);
                 MiddlePanel = new JPanel(true);

                 BApply      = new JButton("Apply");
                 BOk         = new JButton("Print");
                 BCancel     = new JButton("Cancel");
                 BCreatePDF         = new JButton("CreatePDF"); 
            }catch(Exception ex)
            {
                ex.printStackTrace();
            }
      }
      public void setLayouts()
      {
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,450,500);
            setTitle("RDC Printing Moniter");

            TopPanel.setLayout(new GridLayout(1,2));
            TopLeft.setLayout(new GridLayout(2,2));
            TopLeft.setBorder(new TitledBorder("Date"));
            TopRight.setLayout(new GridLayout(1,1));
            TopRight.setBorder(new TitledBorder("Apply"));
            MiddlePanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());
            TopPanel.setBorder(new TitledBorder("Info"));
            //MiddlePanel.setBorder(new TitledBorder("RDC List"));
      }
      public void addComponents()
      {
            TopLeft.add(new MyLabel("FromDate"));
            TopLeft.add(fromDate);

            TopLeft.add(new MyLabel("ToDate"));
            TopLeft.add(toDate);

            TopRight.add(BApply);

            TopPanel.add(TopLeft);
            TopPanel.add(TopRight);

            BottomPanel.add(BOk);
            BottomPanel.add(BCancel);
            BottomPanel.add(BCreatePDF);

            getContentPane().add("North",TopPanel);
            getContentPane().add("Center",MiddlePanel);
            getContentPane().add("South",BottomPanel);
     }
     public void addListeners()
     {
          BOk         .addActionListener(new ActList());
          BCancel     .addActionListener(new ActList());
          BApply      .addActionListener(new ActList());
          BCreatePDF.addActionListener(new ActList());
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BCreatePDF)
               {
                    
                    createPDFFile();
                    
                    try{
                File theFile   = new File(PDFFile);
                Desktop        . getDesktop() . open(theFile);
                  

                  
               }catch(Exception ex){}
               }
               if(ae.getSource()==BOk)
               {
                    PrintStatus();
                   // createPDFFile();     
                    removeHelpFrame();
               }
               if(ae.getSource()==BApply)
               {
                    String SStDate = fromDate.toNormal();
                    String SEnDate = toDate.toNormal();
                   showList(SStDate,SEnDate);
                    setTabReport();

                   int iCount = getSoPending();

                    if(iCount>0)
                    {
                           JOptionPane.showMessageDialog(null," RDC SO Authentication was pending ");
                    }   
               }
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
               }
          }
     }
     private void setTabReport()
     {
          try
          {
               MiddlePanel.removeAll();
               thePane.removeAll();

               String ColumnName[]  = {"Sl.No","RDCNo","RDCDate","Supplier","Print" };
               String ColumnType[]  = {"N","N","S","S","B"};
               int    ColumnWidth[] = {50,60,100,100,100};

               if(VSupplier.size()==0)
                    return;

               Object RowData[][] = new Object[VSupplier.size()][ColumnName.length];
               for(int i=0;i<VSupplier.size();i++)
               {
                    RowData[i][0]   = String.valueOf(i+1);
                    RowData[i][1]   = common.parseNull((String)VRDCNo.elementAt(i));
                    RowData[i][2]   = common.parseDate((String)VRDCDate.elementAt(i));
                    RowData[i][3]   = common.parseNull((String)VSupplier.elementAt(i));
                    RowData[i][4]   = new Boolean(false);
               }
               theReport      = new TabReport(RowData,ColumnName,ColumnType);
               theReport.setPrefferedColumnWidth(ColumnWidth);
               MiddlePanel.add("Center",thePane);
               thePane.addTab("RDC Print List",theReport);
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }
     private void PrintStatus()
     {
          Boolean bFlag;
          try
          {
               FW  = new FileWriter(common.getPrintPath()+"RDCList.prn");


               for(int i=0;i<theReport.ReportTable.getRowCount();i++)
               {
                    bFlag  = (Boolean)theReport.ReportTable.getValueAt(i,4);

                    if(bFlag.booleanValue())
                    {
                         String SRdcNo    = (String)theReport.ReportTable.getValueAt(i,1);
                         printData(SRdcNo);
                    }
               }


               FW   .close();

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
private void createPDFFile() {
        try {
          
          

             Boolean bFlag; 

            try{
		document = new Document(PageSize.A4);
		PdfWriter.getInstance(document, new FileOutputStream(PDFFile));
		document.open();
              //  document .  newPage();  

       }catch(Exception ex){}
            for(int i=0;i<theReport.ReportTable.getRowCount();i++)
               {
                    bFlag  = (Boolean)theReport.ReportTable.getValueAt(i,4);

                    if(bFlag.booleanValue())
                    {
                         String SRdcNo    = (String)theReport.ReportTable.getValueAt(i,1);
			 String SGstStatus = (String)VGSTStatus.elementAt(i);
                     System.out.println("Status==>"+SGstStatus);
                         printDatapdf(SRdcNo,SGstStatus);
                    }
               }

 
              document.close();
            //JOptionPane.showMessageDialog(null, "PDF File Created in " + PDFFile, "Info", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) 
        {
            e.printStackTrace();
        }
    }
     private void printData(String SRdcNo)
     {
          try
          {
               RDCPrintClass rdcPrintClass = new RDCPrintClass();
               rdcPrintClass.printWithDate(FW,SRdcNo,iMillCode,SSupTable);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
  private void printDatapdf(String SRdcNo,String SGstStatus)
     {
          try
          {

                
            
           if(SGstStatus.equals("0"))
           {
                RDCPrintClassGST rdcPrintClass = new RDCPrintClassGST();
               rdcPrintClass.printWithDatepdf(document,SRdcNo,iMillCode,SSupTable);
           }else{
                RDCPrintClass rdcPrintClass = new RDCPrintClass();
               rdcPrintClass.printWithDatepdf(document,SRdcNo,iMillCode,SSupTable);
           }
        
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void showList(String SStDate,String SEnDate)
     {
          VRDCNo    = new Vector();
          VRDCDate  = new Vector();
          VSupplier = new Vector(); 
          VGSTStatus  = new Vector(); 

          String QS = " select distinct RDCNo,RDCDate,Name ,nvl(GSTSTATUS,0)"+
                      " From RDC "+
                      " Inner Join "+SSupTable+" On "+SSupTable+".AC_Code = RDC.Sup_Code "+
                      " WHere RDcDate>="+SStDate+" and RDCDate<="+SEnDate+" and MillCode="+iMillCode+" Order by 1 "; // and RDC.SOAuthStatus>0
		  System.out.println(QS);
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat       = theconnect.createStatement();
               ResultSet theResult  = stat.executeQuery(QS);

               while(theResult.next())
               {
                    VRDCNo    .addElement(theResult.getString(1));
                    VRDCDate  .addElement(theResult.getString(2));
                    VSupplier .addElement(theResult.getString(3));
                    VGSTStatus.addElement(theResult.getString(4));
               }
               theResult.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }

private int getSoPending()
 {
     int  icount=0;

      try{

          String QS=" select count(*) from  rdc where  soauthstatus=0 and millcode="+iMillCode+" ";
 
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               ResultSet res                = stat.executeQuery(QS);
               
               while (res.next())
               {
          	  icount=res.getInt(1);
                }
               res.close();
               stat.close();

         
	}catch(Exception ex)

        {
            System.out.println("Exception in count==>"+ex);
        } 
   return icount;


 }


   public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    } 


}
