package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;

import jdbc.*;
import util.*;
import guiutil.*;

public class RDCRec 
{
     JInternalFrame theFrame;
     MyTextField TRemarks;

     JPanel         TopPanel,BottomPanel;

     JTextField     TDesc;

     DateField      TDueDate;
     NextField      TQty;

     MyComboBox     JCDept;
     MyComboBox     JCUnit;
     MyComboBox     JCUom;

     JButton        BOk;

     JLayeredPane   Layer;
     Vector         VUom,VDept,VUnit,VUnitCode,VDeptCode;
     Object         RowData[][];
     String         Id,SDate;
     int            iActivation;
     RDCOutFrame    rdcoutframe;

     Common common = new Common();

     public RDCRec(JLayeredPane Layer,Vector VUom,Vector VDept,Vector VUnit,Vector VDeptCode,Vector VUnitCode,Object[][] RowData,String Id,RDCOutFrame rdcoutframe,String SDate)
     {
          this.Layer     = Layer;
          this.VUom      = VUom;
          this.VDept     = VDept;
          this.VUnit     = VUnit;
          this.RowData   = RowData;
          this.Id        = Id;
          this.VDeptCode = VDeptCode;
          this.VUnitCode = VUnitCode;
          iActivation    = 0;
          this.rdcoutframe = rdcoutframe;
          this.SDate     = SDate;

          createComponents();
     }
     public void setActivation(boolean bflag)
     {
          if(iActivation > 0)
               return;
          setLayouts();
          addComponents();
          addListeners();
          if(bflag)
               setPresets();
     }
     public void setPresets()
     {
          int i = common.toInt(Id);

          TDesc.setText((String)RowData[i][1]);
          TRemarks.setText((String)RowData[i][2]);
          JCUom.setSelectedItem((String)RowData[i][3]);
          JCDept.setSelectedItem((String)RowData[i][4]);
          JCUnit.setSelectedItem((String)RowData[i][5]);
          TDueDate.fromString((String)RowData[i][6]);
          TQty.setText((String)RowData[i][7]);

          setDueDate();
     }

     public void setDueDate()
     {
          if(rdcoutframe.JCDocType.getSelectedIndex()==1)
		{
			TDueDate.setTodayDate();
			TDueDate.setEditable(false);
		}
          else
          {
               TDueDate.setEditable(true);
          }
     }

     public void createComponents()
     {
          theFrame       = new JInternalFrame();
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();

          TDesc          = new JTextField();
          TRemarks       = new MyTextField(100);
          JCDept         = new MyComboBox(VDept);
          JCUnit         = new MyComboBox(VUnit);
          JCUom          = new MyComboBox(VUom);
          TDueDate       = new DateField();
          TQty           = new NextField();
          BOk            = new JButton("Submit");
          BOk.setMnemonic('U');
     }

     public void setLayouts()
     {
          theFrame.setClosable(true);
          theFrame.setMaximizable(true);
          theFrame.setIconifiable(true);
          theFrame.setResizable(true);
          theFrame.setBounds(0,0,550,300);
          theFrame.setTitle("Details about the goods despatched");
          theFrame.getContentPane().setLayout(new BorderLayout());
          TopPanel.setLayout(new GridLayout(7,2));
          BottomPanel.setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("Description About the Goods"));
          TopPanel.add(TDesc);

          TopPanel.add(new JLabel("Unit of Measurement"));
          TopPanel.add(JCUom);

          TopPanel.add(new JLabel("Department the Mac/Service related to"));
          TopPanel.add(JCDept);

          TopPanel.add(new JLabel("Processing Unit the Mac/Service related to"));
          TopPanel.add(JCUnit);

          TopPanel.add(new JLabel("Due Date of Receipt"));
          TopPanel.add(TDueDate);

          TopPanel.add(new JLabel("How many Quantities "));
          TopPanel.add(TQty);

          TopPanel.add(new JLabel("Purpose"));
          TopPanel.add(TRemarks);

          BottomPanel.add(BOk);

          theFrame.getContentPane().add(TopPanel,BorderLayout.NORTH);
          theFrame.getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          theFrame.setVisible(false);
     }

     public void addListeners()
     {
          BOk.addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(checkData())
               {
                    int i         = common.toInt(Id);
                    RowData[i][1] = TDesc.getText();
                    RowData[i][2] = TRemarks.getText();
                    RowData[i][3] = JCUom.getSelectedItem();
                    RowData[i][4] = JCDept.getSelectedItem();
                    RowData[i][5] = JCUnit.getSelectedItem();
                    RowData[i][6] = TDueDate.toString();
                    RowData[i][7] = TQty.getText();
     
                    offThisFrame();
               }
          }
     }

     public void offThisFrame()
     {
          theFrame.setVisible(false);
          Layer.updateUI();
          rdcoutframe.tabreport.ReportTable.requestFocus();
     }

     public void onThisFrame()
     {
          try
          {
               Layer.add(theFrame);
               iActivation++;
               theFrame.moveToFront();
               Layer.repaint();
               Layer.updateUI();
               theFrame.setVisible(true);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     public boolean checkData()
     {
          int iDueDate = common.toInt((String)TDueDate.toNormal());
          int iRdcDate = common.toInt(SDate);

          if((TDesc.getText()).equals(""))
          {
               JOptionPane.showMessageDialog(null,"Description Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TDesc.requestFocus();
               return false;
          }
          if(iDueDate<iRdcDate)
          {
               JOptionPane.showMessageDialog(null,"Due Date Grater than RDC Date","Error",JOptionPane.ERROR_MESSAGE);
               TDueDate.TDay.requestFocus();
               return false;
          }
          if((TQty.getText()).equals(""))
          {
               JOptionPane.showMessageDialog(null,"Quantities Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TQty.requestFocus();
               return false;
          }
          if((TRemarks.getText()).equals(""))
          {
               JOptionPane.showMessageDialog(null,"Purpose Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TRemarks.requestFocus();
               return false;
          }
          return true;
     }
}
