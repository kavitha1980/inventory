package RDC;

import java.awt.BorderLayout;
import java.awt.*;
import java.awt.event.*;
import java.net.InetAddress;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.border.*;
import java.util.HashMap;
import java.util.Vector;
import java.util.*;
import util.*;
import guiutil.*;
import java.awt.event.ItemListener;
import javax.swing.table.TableColumn;


import java.io.*;
import java.sql.*;
import java.net.*;
import oracle.jdbc.*;
import oracle.sql.*;
import Biometric.*;
import jdbc.*;



public class RDCSOAuthFrame extends JInternalFrame
{

     JPanel          TopPanel;
     JPanel          BottomPanel;
     JLabel                    LStatus;
     DateField      TStDate,TEnDate;
     JButton        BApply;
     JButton        BAuth,BCreatePdf,BPunch;
     JTextField     TFile;
     MyComboBox     JCType;
     TabReport      tabreport;
     Object         RowData[][];
          
     String ColumnData[] = {"Doc Type","Rdc Type","Rdc No","Rdc Date","Grn No","Rej No","Sent Through","Supplier Name","Description of Goods","Purpose","Send Qty","Click"};
     String ColumnType[] = {"S"       ,"S"       ,"N"     ,"S"       ,"N"     ,"N"     ,"S"           ,"S"            ,"S"                   ,"S"      ,"N"      ,"B"            };
     int iColWidth[]     = {80        ,80        ,80      ,80        ,80      ,80      ,100           ,250            ,250                   ,100      ,80        ,70            };
     
     Common common   = new Common();
     ORAConnection connect;
     Connection theconnect;

     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     int            iMillCode,iUserCode;
     String         SSupTable,SYearCode,SMillName;

     Vector         VDocType;
     Vector         VRDCType;
     Vector         VRDCNo;
     Vector         VRDCDate;
     Vector         VThro;
     Vector         VSupName;
     Vector         VDescript;
     Vector         VQty;
     Vector         VRemarks;
     Vector         VRecQty;
     Vector         VAuth;
     Vector         VGrnNo,VRejNo;
     Vector         VDocTypeCode,VRDCTypeCode;
     Vector         VNewRDCNo; 
     String         SHead1,SHead2,SHead3;

     int iList=-1;

     FileWriter     FW;
     int iLctr = 100,iPctr=0;
     int         iPunchStatus=0;
     JButton     BSend;
     JTextField  TSecurityNo;  
     String SFile="";   
     Verification form=null;
     ArrayList  theSOAuthList;
     Connection theConnection1=null;

     RDCSOAuthFrame(JLayeredPane DeskTop,int iUserCode,int iMillCode,String SSupTable,String SYearCode)
     {
          super("RDC List");

          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;
          this.SMillName = SMillName;
          //onLoad();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setTabReport();
     }
	 
	 
    RDCSOAuthFrame(JLayeredPane DeskTop,int iUserCode,int iMillCode,String SSupTable,String SYearCode,int iPunchStatus)
     {
          super("RDC List");

          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;
          this.SMillName = SMillName;
	  this.iPunchStatus=iPunchStatus;
	  System.out.println("iPunch==>"+iPunchStatus);
		   

         
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setTabReport();
     }

     public void createComponents()
     {
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          TStDate     = new DateField();
          TEnDate     = new DateField();
          TFile       = new JTextField();
          LStatus    = new JLabel("");
          JCType      = new MyComboBox();
          JCType      .addItem("All");
          JCType      .addItem("Returnable");
          JCType      .addItem("NonReturnable");
          BApply      = new JButton("Apply");
          BPunch      = new JButton("Punch");
        
          BAuth      = new JButton("Authentcicate");
          BSend          = new JButton("Send SecurityCode");
          TSecurityNo    = new JTextField(4);

          TSecurityNo.setEditable(false);
         
          TStDate.setTodayDate();
          TEnDate.setTodayDate();

          TFile.setText("RDCListDetails.prn");
          TFile.setEditable(false);
          BAuth.setEnabled(false);

          BApply.setMnemonic('A');
         
     }

     public void setLayouts()
     {
          TopPanel.setLayout(new GridLayout(1,4));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,850,500);
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("Type"));
          TopPanel.add(JCType);  
          TopPanel.add(new JLabel(""));
          TopPanel.add(BApply);
          BottomPanel.add(BAuth);
          BottomPanel.add(BSend);
          BottomPanel.add(TSecurityNo);
          BottomPanel.add(BPunch);
          BottomPanel  . add(LStatus);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply.addActionListener(new AppList());
          BAuth.addActionListener(new AppList());
          BSend     . addActionListener(new ActList());
          BPunch  . addActionListener(new AuthList());
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(!checkValidData())
               {
                    JOptionPane.showMessageDialog(null,"No Data Selected For Send SecurityCode");
                    return;
               } 
               setRandomNumber();
              // updateStatus();  

          }
     }

     public class AppList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
                    setTabReport();
               }  
               if(ae.getSource()==BAuth)
               {
                 if(isValid1() && booleanValidation())
                   {
                
                            updateStatus();
                            //SaveDetails();
                           

                        
                  } 
               }   
          }
     }

// AuthList


     public class AuthList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
                  if(ae.getSource()==BPunch)
		 {
                    System.out.println("iUserCode"+iUserCode);
                    form = new Verification(1987);
                    form.setVisible(true);
		    BAuth.setEnabled(true);
                  }

                     
                  
          }
     }
     public void updateStatus()
     {
          int iMrsNo=0;

          Connection theConnection = null;
          try
          {
            
                      

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][11];   
               

                    int iSecurityNo   = common.toInt(TSecurityNo.getText());
                    int iCheckNo      = 0;

                    if(Bselected.booleanValue())
                    {
                         if(iSecurityNo>0)
                         {
                             
     
                              Statement      stat          =  theConnection.createStatement();
                              ResultSet      theResult     =  stat.executeQuery("Select MaxNo from Configall where id=5 ");
     
                              if(theResult.next())
                                   iCheckNo = theResult.getInt(1);
    
                              theResult.close();
                         }
     
                         if(iSecurityNo!=iCheckNo || iSecurityNo==0)
                         {
                                   JOptionPane.showMessageDialog(null," Biometric Authentication Not Made ");
                         }      
						 else {
						           SaveDetails();
						  }
						  
                    }
           }
                   
          }
          catch(Exception ex)
          {
               try
               {
                    JOptionPane.showMessageDialog(null," Problem in Storing Contact EDP");
                    theConnection.rollback();
               }catch(Exception e){}
               System.out.println(ex);
          }
     }


// bsend validate
  
    private boolean checkValidData()
    {
          int iCount=0;

          for(int i=0;i<RowData.length;i++)
          {
               Boolean Bselected = (Boolean)RowData[i][11];   
          
               if(Bselected.booleanValue())
               {
                    iCount=iCount+1;
               }
          }
          if(iCount==0)
          return false;

          return true;
    }

 //random number

     private void setRandomNumber()
     {
          int iRandomNumber=0;

          TSecurityNo.setEditable(true);

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet res = stat.executeQuery("select Round(dbms_random.value(1000,9999)) from dual");



               if(res.next())
               {
                    iRandomNumber = res.getInt(1);
               }
               res.close();
               String SStatus = "STATUS279";
	       int iRawUserCode=1987;//2826 -Natraj sir//386 -Arun sir

               String Msg = " Security Code for RDCAuthentication "+iRandomNumber+"  ";

               String QS1 = " INSERT INTO TBL_ALERT_COLLECTION VALUES (scm.alertCollection_seq.nextval,'"+SStatus+"','"+Msg+"','"+iRawUserCode+"',0,0) ";
               stat.execute(QS1);
            
               for(int i=0;i<RowData.length;i++)
               {
                    String SRDCTransNo       = (String)RowData[i][3];

                    Boolean Bselected = (Boolean)RowData[i][11];   

                    if(Bselected.booleanValue())
                         stat.execute(" Update configall set Maxno="+iRandomNumber+" where id=5 ");

               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
    }

     public void setTabReport()
     {
          setDataIntoVector();
          setRowData();

          try
          {
               getContentPane().remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.setPrefferedColumnWidth1(iColWidth);

               getContentPane().add(tabreport,BorderLayout.CENTER);
               DeskTop.repaint();
               DeskTop.updateUI();
             
              
               //tabreport.ReportTable.addKeyListener(new KeyList());
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();

          }
          catch(Exception ex){}
     }



     public void setDataIntoVector()
     {

          String SSupplier="";
          iList=-1;
          int iType=0;

          String SType=(String)JCType.getSelectedItem();

          String QS = " SELECT RDC.DocType, RDC.RdcType, RDC.RDCNo, RDC.RDCDate, "+
                      " RDC.Thro, "+SSupTable+".Name, RDC.Descript, RDC.Remarks, "+
                      " RDC.Qty, RDC.RecQty, RDC.GateAuth,nvl(Grn.RejNo,'') as RejNo,nvl(Grn.GrnNo,'') as GrnNo "+
                      " FROM RDC INNER JOIN "+SSupTable+" ON "+SSupTable+".Ac_Code = RDC.Sup_Code "+
                      " Left Join Grn on RDC.GrnId = Grn.Id "+
                      " Where RDC.MillCode = "+iMillCode+" and rdc.skauthstatus=1 and soauthstatus=0   "; //and rdc.skauthstatus=1 and soauthstatus=0 
                  if(!SType.equals("ALL"))
                  {
		            if(SType.equals(("Returnable")))
		            {
		                  QS=QS+" and RDC.DocType=0 ";
		            }
		            if(SType.equals("NonReturnable"))
		            {
		                  QS=QS+" and RDC.DocType=1";
		            }
                  }

              QS=QS+"Order by  RDC.RDCDate,RDC.RDCNo";
       
          VDocType = new Vector();
          VRDCType = new Vector();
          VRDCNo   = new Vector();
          VRDCDate = new Vector();
          VThro    = new Vector();
          VSupName = new Vector();
          VDescript= new Vector();
          VQty     = new Vector();
          VRemarks = new Vector();
          VRecQty  = new Vector();
          VAuth    = new Vector();
          VRejNo   = new Vector();
          VGrnNo   = new Vector();
          VDocTypeCode = new Vector();
          VRDCTypeCode = new Vector();

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res   = stat.executeQuery(QS);

               while (res.next())
               {
                    int iDocType = res.getInt(1);
                    int iRdcType = res.getInt(2);

                    if(iDocType==0)
                    {
                         VDocType.addElement("RDC");
                    }
                    else
                    {
                         VDocType.addElement("NRDC");
                    }

                    if(iRdcType==0)
                    {
                         VRDCType.addElement("Regular");
                    }
                    else
                    {
                         VRDCType.addElement("Non Regular");
                    }

                    VRDCNo   .addElement(common.parseNull(res.getString(3)));

                    String SRDCDate = common.parseDate(res.getString(4));
                    VRDCDate .addElement(SRDCDate);

                    VThro    .addElement(common.parseNull(res.getString(5)));
                    VSupName .addElement(common.parseNull(res.getString(6)));
                    VDescript.addElement(common.parseNull(res.getString(7)));
                    VRemarks .addElement(common.parseNull(res.getString(8)));
                    VQty     .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(9))),3));
                    if(iList==1)
                    {
                         VRecQty  .addElement("");
                    }
                    else
                    {
                         VRecQty  .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(10))),3));
                    }
                    VAuth    .addElement(common.parseNull(res.getString(11)));
                    VRejNo   .addElement(common.parseNull(res.getString(12)));
                    VGrnNo   .addElement(common.parseNull(res.getString(13)));
                    VDocTypeCode.addElement(""+iDocType);
                    VRDCTypeCode.addElement(""+iRdcType);
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VRDCNo.size()][ColumnData.length];

          for(int i=0;i<VRDCNo.size();i++)
          {
               RowData[i][0]  = (String)VDocType  .elementAt(i);
               RowData[i][1]  = (String)VRDCType  .elementAt(i);
               RowData[i][2]  = (String)VRDCNo    .elementAt(i);
               RowData[i][3]  = (String)VRDCDate  .elementAt(i);
               RowData[i][4]  = (String)VGrnNo    .elementAt(i);
               RowData[i][5]  = (String)VRejNo    .elementAt(i);
               RowData[i][6]  = (String)VThro     .elementAt(i);
               RowData[i][7]  = (String)VSupName  .elementAt(i);
               RowData[i][8]  = (String)VDescript .elementAt(i);
               RowData[i][9]  = (String)VRemarks  .elementAt(i);
               RowData[i][10] = (String)VQty      .elementAt(i);
               RowData[i][11] = new Boolean(false);  
            
          }
     }


 private boolean isValid1()
       {
	 Boolean bflag;
         try
         {
             VNewRDCNo= new Vector();
             for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
	      {
                 
		  Boolean bValue           = (Boolean)tabreport.ReportTable.getValueAt(i, 11);
         	   if(bValue.booleanValue())
	           {
                     String SRDCNo  =((String) tabreport.ReportTable.getValueAt(i,2));

			int iNewIndex = common.indexOf(VNewRDCNo,SRDCNo);

			if(iNewIndex>=0)
				continue;

                       int iIndex = common.indexOf(VRDCNo,SRDCNo);    
                       
                       bflag = SetRDCNO(SRDCNo,iIndex);

                        if(bflag==true){
                           continue;
                         }
                         else{
                              JOptionPane.showMessageDialog(null, "This "+SRDCNo+" not fully selected.", "Information", JOptionPane.INFORMATION_MESSAGE);
                              return false; 
                              }

						
             
                   }
              }
    
           
         }
         catch(Exception ex)
         {
               System.out.println("Error in date"+ex);
         }
         return true;
       }


 private boolean SetRDCNO(String SRDCNo,int iIndex)
       {
         try
         {
      
             for(int i=iIndex;i<tabreport.ReportTable.getRowCount();i++)
	      {

		  String SRowRDCNo  =((String) tabreport.ReportTable.getValueAt(i,2));

                 if(!SRowRDCNo.equals(SRDCNo))
		 {
			VNewRDCNo.addElement(SRDCNo);
                       
			break;
		 }
                
                
		  Boolean bValue           = (Boolean)tabreport.ReportTable.getValueAt(i, 11);
		
		 	   if(bValue.booleanValue())
			   {
		                            
                            continue;
                            
                           
                           }
                           else 
                           {
                              return false;
                           }


              }
    
           
         }
         catch(Exception ex)
         {
               System.out.println("Error in setrdcno"+ex);
         }
               return true;
         
       }


	private void SaveDetails()
	{
		 Boolean bFlag; 
		  
                 String SSysName = common.getLocalHostName();
                 String SServerDateTime=common.getServerDateTime();

		 System.out.println("Inside Save");

		  for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
			       {

		                    
				    bFlag  = (Boolean)tabreport.ReportTable.getValueAt(i,11);

				   if(bFlag.booleanValue())
				    {
                                      String sRDCNo  = (String)tabreport.ReportTable.getValueAt(i,2);
                                      UpdateDetails(sRDCNo,SSysName,SServerDateTime);
                                    
				        
				    }
                               
			       }
    setTabReport();
 JOptionPane.showMessageDialog(null,"Data has been updated sucessfully","Dear User",JOptionPane.INFORMATION_MESSAGE);
   

	}


   private boolean booleanValidation()
     {
          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
          {
               Boolean bValue           = (Boolean)tabreport.ReportTable.getValueAt(i, 11);

               if(bValue.booleanValue())
               {

                    return true;
               }
          }
          JOptionPane.showMessageDialog(null, "No Items Selected", "Information", JOptionPane.INFORMATION_MESSAGE);
          return false;
     }

 private void UpdateDetails(String sRDCNo,String SSysName,String SServerDateTime)
    {   
      try{


         String QS=" update rdc set SOAUTHSTATUS=1 , SOAUTHUSERCODE=? , SOAUTHDATETIME='"+SServerDateTime+"' , SOAUTHSYSNAME='"+SSysName+"'  where rdcno="+sRDCNo+" and millcode=?  ";
     
                ORAConnection oraConnection     =   ORAConnection.getORAConnection();
		Connection connection           =   oraConnection.getConnection();
		        
           
              PreparedStatement ps      = connection.prepareStatement(QS);
	      ps                        . setInt(1,iUserCode);
              ps                        . setInt(2,iMillCode);
              ps                        . executeQuery();
	      ps			. close();
       
            
         }catch(Exception ex){
           System.out.println("Exception in Update"+ex);
          } 

   }
  
   /*  private void SaveDetails()
     {
       
          int iSecurityNo   = common.toInt(TSecurityNo.getText());
          int iCheckNo      = 0;
          String SSysName = common.getLocalHostName();
          String SServerDateTime=common.getServerDateTime();
          System.out.println("Inside Save");

        
          try
          {
               Connection theConnection=null;

               if(theConnection == null)
               {
                     ORAConnection oraConnection     =   ORAConnection.getORAConnection();
		     Connection connection           =   oraConnection.getConnection();
		
               }



               if(iSecurityNo>0)
               {
     
                    Statement      stat          =  theConnection.createStatement();
                    ResultSet      theResult     =  stat.executeQuery(" Select MaxNo from Configall where id=5 " );
     
                    if(theResult.next())
                         iCheckNo = theResult.getInt(1);
     
                    theResult.close();
               }
          } catch(Exception ex)
          {

          }



          if(iSecurityNo!=iCheckNo || iSecurityNo==0)
          {

               if(form==null && iPunchStatus==1)
               {
                    JOptionPane.showMessageDialog(null," Biometric Authentication Not Made ");
                    return;
               }
               
               if(form!=null && form.getVerified()==0)
               {
                    JOptionPane.showMessageDialog(null," Biometric Authentication Not Made");
                    return;
               }
          }

         
          try
          {
                 Boolean bFlag;
            for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
			       {

		                    
				    bFlag  = (Boolean)tabreport.ReportTable.getValueAt(i,11);

				   if(bFlag.booleanValue())
				    {
                                      String sRDCNo  = (String)tabreport.ReportTable.getValueAt(i,2);
                                      UpdateDetails(sRDCNo,SSysName,SServerDateTime);
                                      
				        
				    }
                               
			       }
               setTabReport();
               JOptionPane.showMessageDialog(null," RDC Was Authenticated Successfully");
              
              

          }

          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }*/

}
