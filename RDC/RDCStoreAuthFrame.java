package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;


// pdf import
import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;



public class RDCStoreAuthFrame extends JInternalFrame
{

     JPanel          TopPanel;
     JPanel          BottomPanel;

     DateField      TStDate,TEnDate;
     JButton        BApply;
     JButton        BAuth,BCreatePdf;
     JTextField     TFile;
     MyComboBox     JCType;
     TabReport      tabreport;
     Object         RowData[][];
          
     String ColumnData[] = {"Doc Type","Rdc Type","Rdc No","Rdc Date","Grn No","Rej No","Sent Through","Supplier Name","Description of Goods","Purpose","Send Qty","Click"};
     String ColumnType[] = {"S"       ,"S"       ,"N"     ,"S"       ,"N"     ,"N"     ,"S"           ,"S"            ,"S"                   ,"S"      ,"N"      ,"B"            };
     int iColWidth[]     = {80        ,80        ,80      ,80        ,80      ,80      ,100           ,250            ,250                   ,100      ,80        ,70            };
     
     Common common   = new Common();
     ORAConnection connect;
     Connection theconnect;

     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     int            iMillCode,iUserCode;
     String         SSupTable,SYearCode,SMillName;

     Vector         VDocType;
     Vector         VRDCType;
     Vector         VRDCNo;
     Vector         VRDCDate;
     Vector         VThro;
     Vector         VSupName;
     Vector         VDescript;
     Vector         VQty;
     Vector         VRemarks;
     Vector         VRecQty;
     Vector         VAuth;
     Vector         VGrnNo,VRejNo;
     Vector         VDocTypeCode,VRDCTypeCode;
     Vector         VNewRDCNo; 
     String         SHead1,SHead2,SHead3;

     int iList=-1;

     FileWriter     FW;
     int iLctr = 100,iPctr=0;

      // pdf 
    int Lctr = 100;
    int Pctr = 0;
    Document document;
    PdfPTable table;
    int iTotalColumns = 12;
    int iWidth[] = {8, 12, 12, 14, 5, 6, 15, 25, 25, 20, 12,10};
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 11, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 9, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font bigNormal = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
    String SFile="";   
      String PDFFile = common.getPrintPath()+"/RDCDetailsList.pdf";
     RDCStoreAuthFrame(JLayeredPane DeskTop,int iUserCode,int iMillCode,String SSupTable,String SYearCode)
     {
          super("RDC List");

          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;
          this.SMillName = SMillName;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setTabReport();
     }

     public void createComponents()
     {
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          TStDate     = new DateField();
          TEnDate     = new DateField();
          TFile       = new JTextField();

          JCType      = new MyComboBox();
          JCType      .addItem("All");
          JCType      .addItem("Returnable");
          JCType      .addItem("NonReturnable");
          BApply      = new JButton("Apply");
          BAuth      = new JButton("Authenticate");
         
          TStDate.setTodayDate();
          TEnDate.setTodayDate();

          TFile.setText("RDCListDetails.prn");
          TFile.setEditable(false);
        

          BApply.setMnemonic('A');
         
     }

     public void setLayouts()
     {
         TopPanel.setLayout(new GridLayout(1,4));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("Type"));
          TopPanel.add(JCType);  
          TopPanel.add(new JLabel(""));
          TopPanel.add(BApply);
          BottomPanel.add(BAuth);
         

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply.addActionListener(new AppList());
          BAuth.addActionListener(new AppList());

     }

     public class AppList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
                    setTabReport();
               }  
               if(ae.getSource()==BAuth)
               {
                 if(isValid1() && booleanValidation())
                   {
                
                     
                            SaveDetails();
                           

                        
                  } 
    setMessage();
   JOptionPane.showMessageDialog(null,"Data has been updated sucessfully","Dear User",JOptionPane.INFORMATION_MESSAGE);
               }   
          }
     }

     public void setTabReport()
     {
          setDataIntoVector();
          setRowData();

          try
          {
               getContentPane().remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.setPrefferedColumnWidth1(iColWidth);

               getContentPane().add(tabreport,BorderLayout.CENTER);
               DeskTop.repaint();
               DeskTop.updateUI();
             
              
               //tabreport.ReportTable.addKeyListener(new KeyList());
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();

          }
          catch(Exception ex){}
     }



     public void setDataIntoVector()
     {

          String SSupplier="";
          iList=-1;
          int iType=0;

          String SType=(String)JCType.getSelectedItem();

          String QS = " SELECT RDC.DocType, RDC.RdcType, RDC.RDCNo, RDC.RDCDate, "+
                      " RDC.Thro, "+SSupTable+".Name, RDC.Descript, RDC.Remarks, "+
                      " RDC.Qty, RDC.RecQty, RDC.GateAuth,nvl(Grn.RejNo,'') as RejNo,nvl(Grn.GrnNo,'') as GrnNo "+
                      " FROM RDC INNER JOIN "+SSupTable+" ON "+SSupTable+".Ac_Code = RDC.Sup_Code "+
                      " Left Join Grn on RDC.GrnId = Grn.Id "+
                      " Where RDC.MillCode = "+iMillCode+" and rdc.skauthstatus=0";
                  if(!SType.equals("ALL"))
                  {
		            if(SType.equals(("Returnable")))
		            {
		                  QS=QS+" and RDC.DocType=0 ";
		            }
		            if(SType.equals("NonReturnable"))
		            {
		                  QS=QS+" and RDC.DocType=1";
		            }
                  }

              QS=QS+"Order by  RDC.RDCDate,RDC.RDCNo";
       
          VDocType = new Vector();
          VRDCType = new Vector();
          VRDCNo   = new Vector();
          VRDCDate = new Vector();
          VThro    = new Vector();
          VSupName = new Vector();
          VDescript= new Vector();
          VQty     = new Vector();
          VRemarks = new Vector();
          VRecQty  = new Vector();
          VAuth    = new Vector();
          VRejNo   = new Vector();
          VGrnNo   = new Vector();
          VDocTypeCode = new Vector();
          VRDCTypeCode = new Vector();

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res   = stat.executeQuery(QS);

               while (res.next())
               {
                    int iDocType = res.getInt(1);
                    int iRdcType = res.getInt(2);

                    if(iDocType==0)
                    {
                         VDocType.addElement("RDC");
                    }
                    else
                    {
                         VDocType.addElement("NRDC");
                    }

                    if(iRdcType==0)
                    {
                         VRDCType.addElement("Regular");
                    }
                    else
                    {
                         VRDCType.addElement("Non Regular");
                    }

                    VRDCNo   .addElement(common.parseNull(res.getString(3)));

                    String SRDCDate = common.parseDate(res.getString(4));
                    VRDCDate .addElement(SRDCDate);

                    VThro    .addElement(common.parseNull(res.getString(5)));
                    VSupName .addElement(common.parseNull(res.getString(6)));
                    VDescript.addElement(common.parseNull(res.getString(7)));
                    VRemarks .addElement(common.parseNull(res.getString(8)));
                    VQty     .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(9))),3));
                    if(iList==1)
                    {
                         VRecQty  .addElement("");
                    }
                    else
                    {
                         VRecQty  .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(10))),3));
                    }
                    VAuth    .addElement(common.parseNull(res.getString(11)));
                    VRejNo   .addElement(common.parseNull(res.getString(12)));
                    VGrnNo   .addElement(common.parseNull(res.getString(13)));
                    VDocTypeCode.addElement(""+iDocType);
                    VRDCTypeCode.addElement(""+iRdcType);
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VRDCNo.size()][ColumnData.length];

          for(int i=0;i<VRDCNo.size();i++)
          {
               RowData[i][0]  = (String)VDocType  .elementAt(i);
               RowData[i][1]  = (String)VRDCType  .elementAt(i);
               RowData[i][2]  = (String)VRDCNo    .elementAt(i);
               RowData[i][3]  = (String)VRDCDate  .elementAt(i);
               RowData[i][4]  = (String)VGrnNo    .elementAt(i);
               RowData[i][5]  = (String)VRejNo    .elementAt(i);
               RowData[i][6]  = (String)VThro     .elementAt(i);
               RowData[i][7]  = (String)VSupName  .elementAt(i);
               RowData[i][8]  = (String)VDescript .elementAt(i);
               RowData[i][9]  = (String)VRemarks  .elementAt(i);
               RowData[i][10] = (String)VQty      .elementAt(i);
               RowData[i][11] = new Boolean(false);  
            
          }
     }


 private boolean isValid1()
       {
             Boolean bflag;
         try
         {
             VNewRDCNo= new Vector();
             for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
	      {
                 
		  Boolean bValue           = (Boolean)tabreport.ReportTable.getValueAt(i, 11);
         	   if(bValue.booleanValue())
	           {
                     String SRDCNo  =((String) tabreport.ReportTable.getValueAt(i,2));

                    
			int iNewIndex = common.indexOf(VNewRDCNo,SRDCNo);

			if(iNewIndex>=0)
				continue;

                       int iIndex = common.indexOf(VRDCNo,SRDCNo);    
                       
                        bflag = SetRDCNO(SRDCNo,iIndex);

                        if(bflag==true)
                        { 
                          continue;
                        }
                         else{
                              JOptionPane.showMessageDialog(null, "This "+SRDCNo+" not fully selected.", "Information", JOptionPane.INFORMATION_MESSAGE);
                              return false; 
                              }

             
                   }
              }
    
           
         }
         catch(Exception ex)
         {
               System.out.println("Error in date"+ex);
         }
         return true;
       }


 private boolean SetRDCNO(String SRDCNo,int iIndex)
       {
         try
         {
      
             for(int i=iIndex;i<tabreport.ReportTable.getRowCount();i++)
	      {

		  String SRowRDCNo  =((String) tabreport.ReportTable.getValueAt(i,2));

                 if(!SRowRDCNo.equals(SRDCNo))
		 {
			VNewRDCNo.addElement(SRDCNo);
                       
			break;
		 }
                
                
		  Boolean bValue           = (Boolean)tabreport.ReportTable.getValueAt(i, 11);
		
		 	   if(bValue.booleanValue())
			   {
		                            
                            continue;
                           
                           }
                           else 
                           {
                              return false;
                           }


              }
    
           
         }
         catch(Exception ex)
         {
               System.out.println("Error in setrdcno"+ex);
         }
               return true;
         
       }


	private void SaveDetails()
	{
		 Boolean bFlag; 

                 String SSysName = common.getLocalHostName();
                 String SServerDateTime=common.getServerDateTime();
		  
		  for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
			       {
				    bFlag  = (Boolean)tabreport.ReportTable.getValueAt(i,11);

				   if(bFlag.booleanValue())
				    {
                                
                                      String sRDCNo  = (String)tabreport.ReportTable.getValueAt(i,2);
                                    
                                      UpdateDetails(sRDCNo,SSysName,SServerDateTime);
                                     
				        
				    }
                              
                               
			       }
                     setTabReport();
           JOptionPane.showMessageDialog(null,"Data has been updated sucessfully","Dear User",JOptionPane.INFORMATION_MESSAGE);


	}


   private boolean booleanValidation()
     {
          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
          {
               Boolean bValue           = (Boolean)tabreport.ReportTable.getValueAt(i, 11);

               if(bValue.booleanValue())
               {

                    return true;
               }
          }
          JOptionPane.showMessageDialog(null, "No Items Selected", "Information", JOptionPane.INFORMATION_MESSAGE);
          return false; 
     }

  private void UpdateDetails(String sRDCNo,String SSysName,String SServerDateTime)
    {   
      try{


         String QS=" update rdc set SKAUTHSTATUS=1 , SKAUTHUSERCODE=? , SKAUTHDATETIME='"+SServerDateTime+"' , SKAUTHSYSNAME='"+SSysName+"'  Where rdcno="+sRDCNo+" and millcode=?  ";


        
               
                ORAConnection oraConnection     =   ORAConnection.getORAConnection();
		Connection connection           =   oraConnection.getConnection();
		        
           

              
 // System.out.println("QS==>"+QS);
              PreparedStatement ps      = connection.prepareStatement(QS);
	      ps                        . setInt(1,iUserCode);
              ps                        . setInt(2,iMillCode);
              ps                        . executeQuery();
	      ps			. close();
        
            
         }catch(Exception ex){
           System.out.println("Exception in Update"+ex);
          } 

   }

 private void setMessage()
     {
        

         

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet res = stat.executeQuery("select Round(dbms_random.value(1000,9999)) from dual");

            
               res.close();
               String SStatus = "STATUS279";
	       int iRawUserCode=1987;//2826 -Natraj sir//386 -Arun sir //1987

               String Msg = " RDC Approval was Pending  ";

               String QS1 = " INSERT INTO TBL_ALERT_COLLECTION VALUES (scm.alertCollection_seq.nextval,'"+SStatus+"','"+Msg+"','"+iRawUserCode+"',0,0) ";
               stat.execute(QS1);
            
             System.out.println("Message===>"+QS1); 
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
    }

}
