package RDC;

import util.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.Vector;
import java.sql.*;
import java.awt.event.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCUserCodeUpdationFrame extends JInternalFrame
{
     JPanel TopPanel,MiddlePanel,BottomPanel;
     String mac      = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
     String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

     Common common   = new Common();

     Vector VRdcNo,VRdcSlNo,VMemoNo,VMemoSlNo,VDocType,VMillCode,VId;

     JTextArea     TInfo;

     JButton   BApply;
     JLayeredPane layer;

     Connection theConnection=null;

     public RDCUserCodeUpdationFrame(JLayeredPane layer)
     {
          super("Utility for Updating RDC UserCode");
          this.layer      = layer;

          updateLookAndFeel();
          setSize(500,500);
          
          TopPanel    = new JPanel(true);
          MiddlePanel = new JPanel(true);
          MiddlePanel.setLayout(new BorderLayout());

          TInfo       = new JTextArea(50,50);
          BApply      = new JButton("Apply");

          TopPanel.add(BApply);
          MiddlePanel.add(TInfo);

          BApply.addActionListener(new AppList());

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          setVisible(true);
          setClosable(true);
     }
      private class AppList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {

               BApply.setEnabled(false);
               try
               {
                    MiddlePanel.removeAll();
               }catch(Exception ex){System.out.println(ex);}
               MiddlePanel.add(TInfo);
               TInfo.append("Processing ....."+"\n");

               TInfo.append("Fetching Stock Data ....."+"\n");
               getRDCData();

               if(VRdcNo.size()>0)
               {
                    TInfo.append("Updation is going on ....."+"\n");
                    updateRDCData();
               }

               TInfo.append("Updation Over .....");
               removeFrame();
          }
      }

     private void getRDCData()
     {
          VRdcNo        = new Vector();
          VRdcSlNo      = new Vector();
          VMemoNo       = new Vector();
          VMemoSlNo     = new Vector();
          VDocType      = new Vector();
          VMillCode     = new Vector();
          VId           = new Vector();


          String QS = " Select RdcNo,SlNo,MemoNo,MemoSlNo,DocType,MillCode,Id from RDC Where MemoNo>0 "+
                      " Order by MillCode,RdcNo,Id ";

          try
          {

               if(theConnection==null)
               {
                    ORAConnection jdbc       = ORAConnection.getORAConnection();
                    theConnection            = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    VRdcNo.addElement(common.parseNull((String)theResult.getString(1)));
                    VRdcSlNo.addElement(common.parseNull((String)theResult.getString(2)));
                    VMemoNo.addElement(common.parseNull((String)theResult.getString(3)));
                    VMemoSlNo.addElement(common.parseNull((String)theResult.getString(4)));
                    VDocType.addElement(common.parseNull((String)theResult.getString(5)));
                    VMillCode.addElement(common.parseNull((String)theResult.getString(6)));
                    VId.addElement(common.parseNull((String)theResult.getString(7)));
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void updateRDCData()
     {
          try
          {
               if(theConnection==null)
               {
                    ORAConnection  jdbc           = ORAConnection.getORAConnection();
                    theConnection                 = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();

               for(int i=0;i<VRdcNo.size();i++)
               {
                    String SRdcNo    = (String)VRdcNo.elementAt(i);
                    String SRdcSlNo  = (String)VRdcSlNo.elementAt(i);
                    String SMemoNo   = (String)VMemoNo.elementAt(i);
                    String SMemoSlNo = (String)VMemoSlNo.elementAt(i);
                    String SDocType  = (String)VDocType.elementAt(i);
                    String SMillCode = (String)VMillCode.elementAt(i);
                    String SId       = (String)VId.elementAt(i);

                    String SMemoUserCode = getMemoUserCode(stat,SRdcNo,SMemoNo,SMemoSlNo,SDocType,SMillCode);

                    String QS = " Update RDC Set MemoAuthUserCode="+SMemoUserCode+
                                " Where RdcNo="+SRdcNo+
                                " and SlNo="+SRdcSlNo+
                                " and MemoNo="+SMemoNo+
                                " and MemoSlNo="+SMemoSlNo+
                                " and DocType="+SDocType+
                                " and MillCode="+SMillCode+
                                " and Id="+SId;

                    stat.execute(QS);
               }
               stat.close();
               JOptionPane.showMessageDialog(null,VRdcNo.size()+" Rows updated ");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               JOptionPane.showMessageDialog(null,"Updation Error:"+ex);
               removeFrame();
          }

     }

     private String getMemoUserCode(Statement stat,String SRdcNo,String SMemoNo,String SMemoSlNo,String SDocType,String SMillCode)
     {

          String SMemoUserCode = "1";

          try
          {
               String QS = " Select AuthUserCode from RDCMemo "+
                           " Where RdcNo="+SRdcNo+
                           " and MemoNo="+SMemoNo+
                           " and SlNo="+SMemoSlNo+
                           " and DocType="+SDocType+
                           " and MillCode="+SMillCode;

               ResultSet theResult      = stat.executeQuery(QS);
               while(theResult.next())
               {
                    SMemoUserCode = common.parseNull((String)theResult.getString(1));
               }
               theResult.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

          if(common.toInt(SMemoUserCode)<=0)
          {
               SMemoUserCode = "1";
          }
          return SMemoUserCode;
     }

     private void removeFrame()
     {
          layer.remove(this);
          layer.repaint();
          layer.updateUI();
     }
     private void updateLookAndFeel()
     {
          try
          {
               UIManager.setLookAndFeel(windows);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }                    

     }
     
}



