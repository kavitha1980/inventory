package RDC;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import javax.swing.border.*;
import java.io.*;
import java.util.*;
import java.awt.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class ServiceAndMaterialPickerFrameGst extends JInternalFrame
{
     java.sql.Connection theConnection = null;
     JTextField     TMatCode,TMatName,THsnCode,TDescription;
     JTextField     TIndicator;
	 FractionNumberField FnRate,FnQty,FnDis; 	
     JButton        BOk,BCancel,BApply,BRefresh;
	 
	 JLabel         LBasic,LDiscount,LCGST,LSGST,LIGST,LCess,LNet;
	 NextField      TAdd,TLess;
     
     JList          BrowList;
     JScrollPane    BrowScroll;
     //JPanel         LeftPanel;
     //JPanel         LeftCenterPanel,LeftBottomPanel,TopPanel;
	 private   JPanel                        TopPanel, MiddlePanel, ControlPanel, InfoPanel, BottomPanel,SearchPanel,HsnTyepPanel;
	 private   JComboBox                     JCHSNType;
     
     JLayeredPane      Layer;
     JTable            ReportTable;

     Vector            VSName,VSCode;
     Vector            VSNameCode,VSHsnCode;
     MyArrayList       MaterialCodeList;

	 private   String sSupplierCode="",SSupTable="",sGstStateCode="",sGstPartyType="";
     double dCGST=0,dSGST=0,dIGST=0,dCess=0;

     String         str ="";
     
     int            iLastIndex= 0,iSelectedRow,iQtyChk;
     Common         common    = new Common();
     int iMillCode;
       protected      String Tables="";
     ServiceAndMaterialPickerFrameGst(JLayeredPane Layer , int iSelectedRow,String sSupplierCode ,String SSupTable,JTable ReportTable,JLabel LBasic,JLabel LDiscount,JLabel LCGST,JLabel LSGST,JLabel LIGST,JLabel LCess,NextField TAdd,NextField TLess,JLabel LNet,MyArrayList MaterialCodeList,int iMillCode)
     {
		this.Layer            	= Layer;
		this.iSelectedRow     	= iSelectedRow;
		this.SSupTable		  	= SSupTable;
		this.ReportTable 	  	= ReportTable;
		this.sSupplierCode    	= sSupplierCode;
		this.LBasic         	= LBasic;
		this.LDiscount      	= LDiscount;
		this.LCGST          	= LCGST;
		this.LSGST          	= LSGST;  
		this.LIGST          	= LIGST; 
		this.LCess          	= LCess; 
		this.TAdd           	= TAdd;
		this.TLess          	= TLess;
		this.LNet           	= LNet;
		this.MaterialCodeList	= MaterialCodeList;
        this.iMillCode			= iMillCode;
		  
			System.out.println("sSupplierCode-->"+sSupplierCode);
			System.out.println("iSelectedRow-->"+iSelectedRow);
		  
	 	  setDataIntoVector(1);
          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
		  setReportData();
     }

     public void createComponents()
     {
          BrowList            = new JList(VSNameCode);
          BrowScroll          = new JScrollPane();
          TMatCode            = new JTextField();
          TMatName            = new JTextField();
          THsnCode            = new JTextField();
          TIndicator          = new JTextField();
          TIndicator          . setEditable(false);

		  TDescription		  = new JTextField();
		  FnRate			  = new FractionNumberField();
		  FnQty				  = new FractionNumberField();
		  FnDis				  = new FractionNumberField();

		  JCHSNType      	  = new JComboBox();
		  JCHSNType			  . addItem("Material");
		  JCHSNType			  . addItem("Service");
		  JCHSNType			  . setSelectedItem("Service");

		  BApply              = new JButton("Apply");
          BOk                 = new JButton("Okay");
          BCancel             = new JButton("Cancel");
          BRefresh            = new JButton("Refresh");
           
		  TopPanel                 = new JPanel();
		  SearchPanel			   = new JPanel();
		  HsnTyepPanel			   = new JPanel();	
          MiddlePanel              = new JPanel();
          ControlPanel             = new JPanel();
          InfoPanel                = new JPanel();
          BottomPanel              = new JPanel();
	
          //LeftPanel           = new JPanel(true);
          //LeftCenterPanel     = new JPanel(true);
          //LeftBottomPanel     = new JPanel(true);
          
          TMatCode            . setEditable(false);
          TMatName            . setEditable(false);
          THsnCode            . setEditable(false);
     }

     public void setLayouts()
     {
          setBounds(80,50,650,500);
          setResizable(true);
          setClosable(true);
          setTitle("Service Selection");
		
		  TopPanel                 . setLayout(new GridLayout(1,2,5,5));
		  SearchPanel			   . setLayout(new GridLayout(1,1,5,5));
		  HsnTyepPanel			   . setLayout(new GridLayout(2,1,5,5));
          MiddlePanel              . setLayout(new BorderLayout());
          //ControlPanel             . setLayout(new FlowLayout());
		  ControlPanel             . setLayout(new GridLayout(5,2,5,5));
          InfoPanel                . setLayout(new GridLayout(5,2,5,5));
          BottomPanel              . setLayout(new GridLayout(1,2,5,5));

		  SearchPanel              . setBorder(new TitledBorder("Search Service / Item"));
		  HsnTyepPanel             . setBorder(new TitledBorder("Select Type"));
          MiddlePanel              . setBorder(new TitledBorder("Service / Item List"));
          ControlPanel             . setBorder(new TitledBorder("Controls"));
          InfoPanel                . setBorder(new TitledBorder("Info."));
          
		  /*getContentPane()    . setLayout(new GridLayout(1,1));
          LeftPanel           . setLayout(new BorderLayout());
          LeftCenterPanel     . setLayout(new BorderLayout());
          LeftBottomPanel     . setLayout(new GridLayout(4,2));
		  TopPanel            . setLayout(new BorderLayout());*/
     }

     public void addComponents()
     {
		
			SearchPanel			   . add(TIndicator);
		    HsnTyepPanel		   . add(JCHSNType);
		    HsnTyepPanel		   . add(BApply);

		   MiddlePanel             . add(BrowScroll);

		   //ControlPanel            . add(BOk);
           //ControlPanel            . add(BCancel);
			
		  ControlPanel			   . add(new JLabel("Material Code"));
		  ControlPanel             . add(TMatCode);

		  ControlPanel			   . add(new JLabel("Material Name"));
		  ControlPanel             . add(TMatName);

		  ControlPanel			   . add(new JLabel("Hsn Code"));
		  ControlPanel             . add(THsnCode);	


           ControlPanel			   . add(new JLabel("To Refresh"));
		  ControlPanel             . add(BRefresh);



		  ControlPanel            . add(BOk);
          ControlPanel            . add(BCancel);

		  InfoPanel                . add(new JLabel("Description"));
          InfoPanel                . add(TDescription);

		  InfoPanel                . add(new JLabel("Qty"));
          InfoPanel                . add(FnQty);
		 
  		  InfoPanel                . add(new JLabel("Rate"));
          InfoPanel                . add(FnRate);

		  InfoPanel                . add(new JLabel("Discount(%)"));
          InfoPanel                . add(FnDis);


          InfoPanel                . add(new JLabel(""));
          InfoPanel                . add(new JLabel(""));


		  BottomPanel              . add(ControlPanel);
          BottomPanel              . add(InfoPanel);

		  TopPanel                 . add(SearchPanel);
		  TopPanel                 . add(HsnTyepPanel);

		   getContentPane()  . add("North", TopPanel);
		   getContentPane()  . add("Center", MiddlePanel);
		   getContentPane()  . add("South", BottomPanel);

          /*getContentPane()    . add(LeftPanel);

		  TopPanel            . add(JCHSNType);
          
          LeftPanel           . add("Center",LeftCenterPanel);
          LeftPanel           . add("South",LeftBottomPanel);
          
		  getContentPane()    . add("North", TopPanel);
          LeftCenterPanel     . add("Center",BrowScroll);
          LeftCenterPanel     . add("South",TIndicator);
          LeftBottomPanel     . add(new JLabel("Material Name"));
          LeftBottomPanel     . add(TMatName);
          LeftBottomPanel     . add(new JLabel("Material Code"));
          LeftBottomPanel     . add(TMatCode);
          LeftBottomPanel     . add(new JLabel(""));
          LeftBottomPanel     . add(new JLabel(""));
          LeftBottomPanel     . add(BOk);
          LeftBottomPanel     . add(BCancel);*/
     }

     public void setPresets()
     {
          int i = ReportTable.getSelectedRow();
          String SNameCode="";
          String SCode   = (String)ReportTable.getModel().getValueAt(i,0);
          String SName   = (String)ReportTable.getModel().getValueAt(i,1);
          int iindex     = common.indexOf(VSCode,SCode);
          if(iindex > -1)
               SNameCode = (String)VSNameCode.elementAt(iindex);
          
          BrowList            . setAutoscrolls(true);
          BrowScroll          . getViewport().setView(BrowList);
          TMatCode            . setText(SCode);
          TMatName            . setText(SName);

          show();
          ensureIndexIsVisible(SNameCode);
          MiddlePanel.updateUI();
     }

     public void addListeners()
     {
          BrowList  .addKeyListener(new KeyList());
          BOk       .addActionListener(new ActList());
          BCancel   .addActionListener(new ActList());
                    addMouseListener(new MouseList());
		  BApply    . addActionListener(new ActList());
           BRefresh   .addActionListener(new ActList());     
     }

     public class MouseList extends MouseAdapter
     {
          public void mouseEntered(MouseEvent me)
          {
               //BrowList.ensureIndexIsVisible(iLastIndex);               
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
					     if(validations()){
								double dTGross=0,dTDisc=0,dTCGST=0,dTSGST=0,dTIGST=0,dTCess=0,dTNet=0,dTOthers=0;
								int iHsnType=0;
								//int       i = ReportTable.getSelectedRow();

								/*int index = BrowList.getSelectedIndex();
                    			String SMatName     = (String)VSName.elementAt(index);
                    			String SMatCode     = (String)VSCode.elementAt(index);
                    			String SMatNameCode = (String)VSNameCode.elementAt(index);
                    			String SHsnCode     = (String)VSHsnCode.elementAt(index);*/

								String SMatName     = TMatName.getText();
                    			String SMatCode     = TMatCode.getText();
                    			String SHsnCode     = THsnCode.getText();

								ReportTable     . setValueAt(common.parseNull(SHsnCode), iSelectedRow, 1);	//	Hsn
								ReportTable     . setValueAt(common.parseNull(SMatName), iSelectedRow, 0);	// Name
								ReportTable     . setValueAt(common.parseNull((String)TDescription.getText()), iSelectedRow, 2);	// Desc
							    ReportTable     . setValueAt(common.parseNull((String)FnQty.getText()), iSelectedRow, 4);	// Qty
								ReportTable     . setValueAt(common.parseNull((String)FnRate.getText()), iSelectedRow, 5); // Rate
								ReportTable     . setValueAt(common.parseNull((String)FnDis.getText()), iSelectedRow, 6);	// Discount
			
								getSupplierDetails();
								iHsnType = JCHSNType.getSelectedIndex();
								dCGST=0;
								dSGST=0;
								dIGST=0;
								dCess=0;
								if(!sGstPartyType.equals("1"))
								{
									dCGST=0;
									dSGST=0;
									dIGST=0;
									dCess=0;
								}
								else
								{
									int iRateCheck = getRateCheck(SHsnCode);
									System.out.println("iRateCheck-->"+iRateCheck);
								  	if(iRateCheck>0)
								    {

								        getGstRate(SHsnCode);
							        }
									else
						            {
									ReportTable     . setValueAt("", iSelectedRow, 1);	//	Hsn
									ReportTable     . setValueAt("", iSelectedRow, 0);	// Name
									ReportTable     . setValueAt("", iSelectedRow, 2);	// Desc
									ReportTable     . setValueAt("", iSelectedRow, 4);	// Qty
									ReportTable     . setValueAt("", iSelectedRow, 5);  // Rate
									ReportTable     . setValueAt("",iSelectedRow, 6);	// Discount
							        JOptionPane.showMessageDialog(null,"Gst Rate Not Updated for HsnCode-"+SHsnCode,"Information",JOptionPane.INFORMATION_MESSAGE);
		        					return;
								   }
								}
								double dQty = 0;
								/*if(iHsnType==0){
									dQty = common.toDouble(common.parseNull(FnQty.getText()));
								}
								else{
									dQty =1;
								}*/
								dQty = common.toDouble(common.parseNull(FnQty.getText()));

								if(dQty==0){
									dQty =1;
								}

								double dRate       = common.toDouble(common.parseNull((String)FnRate.getText()));
								double dDiscPer    = common.toDouble(common.parseNull((String)FnDis.getText()));

								double dGross   = dQty*dRate;
								double dDisc    = dGross*dDiscPer/100;
								double dBasic   = dGross - dDisc;
								double dCGSTVal = dBasic*dCGST/100;
								double dSGSTVal = dBasic*dSGST/100;
								double dIGSTVal = dBasic*dIGST/100;
								double dCessVal = dBasic*dCess/100;
								double dNet     = dBasic+dCGSTVal+dSGSTVal+dIGSTVal+dCessVal;

								ReportTable     . setValueAt(common.getRound(dCGST,2),iSelectedRow,7);
								ReportTable     . setValueAt(common.getRound(dSGST,2),iSelectedRow,8);
								ReportTable     . setValueAt(common.getRound(dIGST,2),iSelectedRow,9);
								ReportTable     . setValueAt(common.getRound(dCess,2),iSelectedRow,10);
								ReportTable     . setValueAt(common.getRound(dGross,2),iSelectedRow,11);
								ReportTable     . setValueAt(common.getRound(dDisc,2),iSelectedRow,12);
								ReportTable     . setValueAt(common.getRound(dCGSTVal,2),iSelectedRow,13);
								ReportTable     . setValueAt(common.getRound(dSGSTVal,2),iSelectedRow,14);
								ReportTable     . setValueAt(common.getRound(dIGSTVal,2),iSelectedRow,15);
								ReportTable     . setValueAt(common.getRound(dCessVal,2),iSelectedRow,16);
								ReportTable     . setValueAt(common.getRound(dNet,2),iSelectedRow,17);
								for(int j=0;j<ReportTable.getRowCount();j++){
									dTGross   = dTGross+common.toDouble((String)ReportTable.getValueAt(j,11));
									dTDisc    = dTDisc+common.toDouble((String)ReportTable.getValueAt(j,12));
									dTCGST    = dTCGST+common.toDouble((String)ReportTable.getValueAt(j,13));
									dTSGST    = dTSGST+common.toDouble((String)ReportTable.getValueAt(j,14));
									dTIGST    = dTIGST+common.toDouble((String)ReportTable.getValueAt(j,15));
									dTCess    = dTCess+common.toDouble((String)ReportTable.getValueAt(j,16));
									dTNet     = dTNet+common.toDouble((String)ReportTable.getValueAt(j,17));
								}
								dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
								LBasic.setText(common.getRound(dTGross,2));
								LDiscount.setText(common.getRound(dTDisc,2));
								LCGST.setText(common.getRound(dTCGST,2));
								LSGST.setText(common.getRound(dTSGST,2));
								LIGST.setText(common.getRound(dTIGST,2));
								LCess.setText(common.getRound(dTCess,2));
								LNet.setText(common.getRound(dTNet,2));

								//ReportTable.getModel().setValueAt(TMatCode.getText(),i,0);
								//ReportTable.getModel().setValueAt(TMatName.getText(),i,1);
								//ReportTable.getModel().setValueAt(THsnCode.getText(),i,2);	
						
								MaterialCodeList.add(iSelectedRow,SMatCode);

								removeHelpFrame();
								ReportTable.requestFocus();
						}
               }
				if(ae.getSource() == BApply)
			    {
					setDataIntoVector(JCHSNType.getSelectedIndex());
					BrowList.setListData(VSNameCode);
					setPresets();
				}
				if(ae.getSource() == BCancel)
               {
                    removeHelpFrame();
               }

                if(ae.getSource()==BRefresh)
              {

                 setDataIntoVector(JCHSNType.getSelectedIndex());
                    BrowList.setListData(VSNameCode);

              }
          }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==116)    // F5 is pressed
               {
                    setDataIntoVector(JCHSNType.getSelectedIndex());
                    BrowList.setListData(VSNameCode);
               }
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SMatName     = (String)VSName.elementAt(index);
                    String SMatCode     = (String)VSCode.elementAt(index);
                    String SMatNameCode = (String)VSNameCode.elementAt(index);
                    String SHsnCode     = common.parseNull((String)VSHsnCode.elementAt(index));
                    //addMatDet(SMatName,SMatCode,SMatNameCode,SHsnCode,index);
					if(SHsnCode.length() <= 0 || SHsnCode.equals(""))
					{
						JOptionPane.showMessageDialog(null, "HSN Code Not Updated ", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else{
						addMatDet(SMatName,SMatCode,SMatNameCode,SHsnCode,index);
					}
               }
          }
     }

     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<VSNameCode.size();index++)
          {
               String str1 = ((String)VSNameCode.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index);
                    break;
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

     public boolean addMatDet(String SMatName,String SMatCode,String SMatNameCode,String SHsnCode,int index)
     {
          TMatCode       . setText(SMatCode);
          TMatName       . setText(SMatName);
          THsnCode       . setText(SHsnCode);

          return true;    
     }
	 public String getMatCode(){
			return common.parseNull(TMatCode.getText());
	 }
	 private void setReportData(){
			String SMatCode="",SHsnCode="",SName="",SDiscription ="",SQty="",SRate="",SDisCount="";
			try{
				if(MaterialCodeList.size()>0){
			
					SName = common.parseNull((String)ReportTable.getValueAt(iSelectedRow, 0)).trim();
					if(SName.length() <= 0 || SName.equals("")){
						SMatCode="";
					}
					else
					{
						SMatCode     = (String)MaterialCodeList.get(iSelectedRow);
					}				
			    }
				else{
					SMatCode="";
				}
				SName = common.parseNull((String)ReportTable.getValueAt(iSelectedRow, 0));				
				SHsnCode = common.parseNull((String)ReportTable.getValueAt(iSelectedRow, 1));
				SDiscription = common.parseNull((String)ReportTable.getValueAt(iSelectedRow, 2));
				SQty = common.parseNull((String)ReportTable.getValueAt(iSelectedRow, 4));
				SRate = common.parseNull((String)ReportTable.getValueAt(iSelectedRow, 5));
				SDisCount = common.parseNull((String)ReportTable.getValueAt(iSelectedRow, 6));

			    TMatCode        . setText(SMatCode);
				TMatName        . setText(SName);
				THsnCode        . setText(SHsnCode);
				TDescription	. setText(SDiscription);
				FnRate			. setText(SRate);
				FnQty			. setText(SQty);
				FnDis			. setText(SDisCount);
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
	 }

     public boolean ensureIndexIsVisible(String SNameCode)
     {
          SNameCode = SNameCode.trim();
          int i=0;

          for(i=0;i<VSNameCode.size();i++)
          {
               String SCurName = (String)VSNameCode.elementAt(i);
               SCurName = SCurName.trim();
               if(SCurName.startsWith(SNameCode))
               break;
          }

          if(i==VSNameCode.size())
          {
               return false;
          }
          iLastIndex=i;
          BrowList.setSelectedIndex(i);
          BrowList.ensureIndexIsVisible(i);
          BrowList.requestFocus();
          BrowList.updateUI();
          return true;
     }
	private boolean validations()
    {
        String SError       = "";
        int iSlNo           = 1;
        String sDesc = common.parseNull(TDescription.getText()).trim();
		double dRate = common.toDouble(common.parseNull(FnRate.getText()).trim());
		double dQty  = common.toDouble(common.parseNull(FnQty.getText()).trim());
		double dDis  = common.toDouble(common.parseNull(FnDis.getText()).trim());

		String SHsnCode     = common.parseNull(THsnCode.getText().trim());
		String SMatCode     = common.parseNull(TMatCode.getText().trim());	

		//System.out.println("SMatCode-->"+SMatCode);
	
		if(SMatCode.length() <= 0 || SMatCode.equals(""))
		{
			SError   += (iSlNo++)+"). Select Service / Material Name .\n";
		}		
		if(SHsnCode.length() <= 0 || SHsnCode.equals(""))
		{
			SError   += (iSlNo++)+"). HsnCode Not Updated .\n";
		}

        if(sDesc.length() <= 0)
        {
            SError   += (iSlNo++)+"). Please Enter Description .\n";
        }
		if(dRate <= 0)
        {
            SError   += (iSlNo++)+"). Please Enter Rate .\n";
        }
		if(dQty <= 0 && iSelectedRow==0)
        {
            SError   += (iSlNo++)+"). Please Enter Qty .\n";
        }
        if(SError.length() > 0)
        {
            JOptionPane.showMessageDialog(null, SError, "Information", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

     public void setDataIntoVector(int itype)
     {
          VSName     = new Vector();
          VSCode     = new Vector();
          VSNameCode = new Vector();
          VSHsnCode  = new Vector();

           String SItemTable=getDyeTable("select ITEM_TABLE from mill where millcode="+iMillCode+"");

            String QString="";

         //String QString = " Select Item_Name,Item_Code,HsnCode From InvItems Where StkGroupCode='A01' Order By Item_Name";
		  
        if(iMillCode==0)
		{

		      QString = " Select Item_Name,Item_Code,HsnCode From "+SItemTable+" Where HsnType="+itype+" Order By Item_Name";	
		}
		else
		{
		      QString   = "  Select Item_Name,"+SItemTable+".Item_Code,HsnCode From "+SItemTable+"   "+
		                   " inner join InvItems On InvItems.Item_Code = "+SItemTable+".item_code   Where HsnType="+itype+"  Order By Item_Name ";
		}       
          
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement      stat           = theConnection.createStatement();
               ResultSet      res            = stat.executeQuery(QString);

               while(res.next())
               {
                    String    str1      = res.getString(1);
                    String    str2      = res.getString(2);
                    String    str3      = res.getString(3);
                              VSName    . addElement(str1);
                              VSCode    . addElement(str2);
                              VSNameCode. addElement(str1+" (Code : "+str2+")"+" (HSNCode : "+str3+")");
                              VSHsnCode . addElement(str3);
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
    public int getRateCheck(String SHsnCode)
     {
		int iCount=0;

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "Select Count(*) from HsnGstRate Where HsnCode='"+SHsnCode+"'";

               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    iCount = result.getInt(1);
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getRateCheck :"+ex);
        }
		return iCount;
    } 
	public void getGstRate(String SHsnCode)
     {
		dCGST=0;
		dSGST=0;
		dIGST=0;
		dCess=0;

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();

               String QS = "";

			   if(sGstStateCode.equals("33"))
			   {
				QS = " Select nvl(CGST,0) as CGST,nvl(SGST,0) as SGST,0 as IGST,nvl(Cess,0) as Cess "+
				     " from HsnGstRate Where HsnCode='"+SHsnCode+"'";
			   }
			   else
			   {
				QS = " Select 0 as CGST,0 as SGST,nvl(IGST,0) as IGST,nvl(Cess,0) as Cess "+
				     " from HsnGstRate Where HsnCode='"+SHsnCode+"'";
			   }

				//System.out.println("Qry-->"+QS);


               ResultSet result = theStatement.executeQuery(QS);
               while(result.next())
               {
                    dCGST = result.getDouble(1);
                    dSGST = result.getDouble(2);
                    dIGST = result.getDouble(3);
                    dCess = result.getDouble(4);
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getGstRate :"+ex);
        }
     }

	public void getSupplierDetails()
    {
		sGstStateCode="";
		sGstPartyType="";
		StringBuffer sb = new StringBuffer();

					 sb.append(" Select Supplier.Name,Supplier.Ac_Code,decode(PartyMaster.StateCode,0,'0',decode(PartyMaster.CountryCode,'61',nvl(State.GstStateCode,0),'999')) as GstStateCode, ");
					 sb.append(" nvl(PartyMaster.GstPartyTypeCode,0) as GstPartyTypeCode From Supplier ");
					 sb.append(" Inner Join PartyMaster on Supplier.Ac_Code=PartyMaster.PartyCode ");
					 sb.append(" Inner Join State on PartyMaster.StateCode=State.StateCode ");
					 sb.append(" where Supplier.Ac_Code='"+sSupplierCode+"' ");
					 sb.append(" Order By Name ");

					 //System.out.println("getSupplierDetails-->"+sb.toString());		

        try
        {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }

               Statement theStatement    = theConnection.createStatement();              
			   
               ResultSet result = theStatement.executeQuery(sb.toString());
               while(result.next())
               {
                    sGstStateCode = result.getString(3);
                    sGstPartyType = result.getString(4);
               }
               result.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("getSupplierDetails :"+ex);
        }
     }

    public String getDyeTable(String QueryString)
     {
          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();               
               Statement       theStatement  =  theConnection.createStatement();
               ResultSet theResult           = theStatement.executeQuery(QueryString);
               while(theResult.next())
               {
                    Tables = theResult.getString(1);
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);  
          }
          return Tables;
     }
}
