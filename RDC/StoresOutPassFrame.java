package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

class StoresOutPassFrame extends JInternalFrame
{
     Connection     theConnection  = null;
     Common         common         = new Common();

     JLayeredPane   Layer;
     JPanel         TopPanel,BottomPanel,MiddlePanel;

     JButton        BOk;
     JComboBox      JCBookNo;
     MyComboBox     JCRDCNo;
     MyTextField    TOutTime;

     Vector         VRDCNo;

     String         SOutPassNo= "";
     String         SOutTime  = "";

     int            iBookNo   = 0;
     int            iMillCode,iUserCode;
     String         SYearCode;
                    
     public StoresOutPassFrame(JLayeredPane Layer,int iMillCode,int iUserCode,String SYearCode)
     {
          super("Stores OutPass Entry Screen");
          this . Layer        = Layer;
          this . iMillCode    = iMillCode;
          this . iUserCode    = iUserCode;
          this . SYearCode    = SYearCode;

          setDataIntoVector();
          createComponents();
          setLayouts();
          addComponents();
          addListener();
     }

     public void createComponents()
     {
          JCRDCNo        = new MyComboBox(VRDCNo);

          JCBookNo       = new JComboBox();
          JCBookNo       . addItem("Book1");
          JCBookNo       . addItem("Book2");
          JCBookNo       . addItem("Book3");

          JCBookNo       . setEnabled(false);

          TOutTime       = new MyTextField(10);

          BOk            = new JButton("Save");

          if(VRDCNo.size()>0)
          {
               BOk.setEnabled(true);
          }
          else
          {
               BOk.setEnabled(false);
          }
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          MiddlePanel    = new JPanel();
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,350,400);
          setTitle("Stores OutPass Entry Screen");

          TopPanel       . setLayout(new GridLayout(3,2));
          BottomPanel    . setLayout(new BorderLayout());
     }

     public void addComponents()
     {
          TopPanel            . add(new Label("RDCNo"));
          TopPanel            . add(JCRDCNo);

          TopPanel            . add(new Label("Out Pass Book"));
          TopPanel            . add(JCBookNo);

          TopPanel            . add(new Label("Time Out"));
          TopPanel            . add(TOutTime);

          BottomPanel         . add(BOk);

          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(MiddlePanel,BorderLayout.CENTER);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListener()
     {
          BOk           . addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(isValidNew())
               {
                    BOk.setEnabled(false);

                    SOutPassNo = getNextOrderNo();
                    SOutTime   = TOutTime.getText();
     
                    insertinOutPass();
                    UpdateinRDC();
                    UpdateOrdNo();
     
                    String    SNum           = "The OutPass Number is "+SOutPassNo;
                              JOptionPane    . showMessageDialog(null,SNum,"Information",JOptionPane.INFORMATION_MESSAGE);
     
                    removeHelpFrame();
               }
               else
               {
                    BOk.setEnabled(true);
               }
          }
     }

     public boolean isValidNew()
     {
          /*if(TOutTime.getText().equals(""))
          {
               JOptionPane.showMessageDialog(null,"Time out must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TOutTime.requestFocus();
               return false;
          }*/
          return true;
     }
     private void setDataIntoVector()
     {
                    VRDCNo    = new Vector();
          String    QS        = " Select distinct RDCNo from rdc where outpassstatus = 0 and GateAuth=0 and millcode = "+iMillCode+" Order by 1";

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    VRDCNo    . addElement(result.getString(1));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void UpdateinRDC()
     {
          String SRdcNo = (String)JCRDCNo  . getSelectedItem(); 

          try
          {

               String QS = " Update RDC set OUTPASSSTATUS=1, OUTPASSNO="+SOutPassNo+","+
                           " OUTPASSDATE = '"+common.getServerPureDate()+"',"+
                           " OUTTIME = '"+SOutTime+"'"+
                           " where rdcno = "+SRdcNo+" and Millcode = "+iMillCode;

               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();
                              stat           . executeUpdate(QS);
                              stat           . close();
          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void insertinOutPass()
     {
          try
          {
               String    QS = " insert into OutPass (ID,OUTPASSNO,OUTPASSDATE,MATERIALTYPE,BOOKTYPE,MILLCODE,USERCODE,CREATIONDATE,OUTTIME) values (";
                         QS   = QS+" OUTPASS_SEQ.nextval ,";
                         QS   = QS+SOutPassNo+",";
                         QS   = QS+" '"+common.getServerPureDate()+"',";
                         QS   = QS+"0"+",";
                         QS   = QS+iBookNo+",";
                         QS   = QS+iMillCode+",";
                         QS   = QS+iUserCode+",";
                         QS   = QS+" '"+common.getServerDate()+"',";
                         QS   = QS+"'"+SOutTime+"')";
     
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();
                              stat           . execute(QS);
                              stat           . close();
          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public String getNextOrderNo()
     {
          String    QS           = "";

          int       iOutPassNo   = 0;

                    iBookNo   = 0;
                    iBookNo   = JCBookNo.getSelectedIndex();
                    iBookNo   = iBookNo+1;      

          if(iBookNo==1)
               QS = "  Select maxno from Config"+iMillCode+""+SYearCode+" where id = 13";

          /*if(iBookNo==2)
               QS = "  Select maxno from config where id = 27";

          if(iBookNo==3)
               QS = "  Select maxno from config where id = 28";*/

          try
          {
               ORAConnection  oraConnection  = ORAConnection     . getORAConnection();
               Connection     theConnection  = oraConnection     . getConnection();   
               Statement      stat           = theConnection     . createStatement();
               ResultSet      result         = stat              . executeQuery(QS);

                         result         . next();
                         iOutPassNo     = result.getInt(1);

                         result         . close();
                         stat           . close();

                         iOutPassNo     = iOutPassNo+1;

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

          return String.valueOf(iOutPassNo);
     }

     public void UpdateOrdNo()
     {
          String    QS1       = "";

                    iBookNo   = 0;
                    iBookNo   = JCBookNo.getSelectedIndex();
                    iBookNo   = iBookNo+1;      

          if(iBookNo==1)
               QS1 = " Update Config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1  where id = 13";

          /*if(iBookNo==2)
               QS1 = " Update config set MaxNo = MaxNo+1  where id = 27";

          if(iBookNo==3)
               QS1 = " Update config set MaxNo = MaxNo+1  where id = 28";*/

          try
          {
               ORAConnection  oraConnection  = ORAConnection     . getORAConnection();
               Connection     theConnection  = oraConnection     . getConnection();   
               Statement      stat           = theConnection     . createStatement();
                              stat           . executeUpdate(QS1);
                              stat           . close();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer   . remove(this);
               Layer   . repaint();
               Layer   . updateUI();
          }
          catch(Exception ex) { }
     }
}
