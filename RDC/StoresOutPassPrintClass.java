package RDC;

import java.util.*;
import java.lang.*;
import java.io.*;
import util.*;
import jdbc.*;
import java.sql.*;

class StoresOutPassPrintClass
{

     Common              common;
     Connection          theconnect;
     ORAConnection       connect;

     Vector              VDesc,VKgs,VPurpose;
     String              SRDCNo,SRecDate,SSupplierName,SAdd1,SAdd2,SAdd3;

     int                 iDocType;
     FileWriter FW;
     String SOutPassNo,SOutPassDate,SOutTime;
     int iMillCode;
     String SSupTable,SMillName;

     int iCount=0;
     int iVectSize=0;
     int iItemCount=0;

     public StoresOutPassPrintClass(FileWriter FW,String SOutPassNo,String SOutPassDate,String SOutTime,int iMillCode,String SSupTable,String SMillName)
     {
          this.FW           = FW;
          this.SOutPassNo   = SOutPassNo;
          this.SOutPassDate = SOutPassDate;
          this.SOutTime     = SOutTime;
          this.iMillCode    = iMillCode;
          this.SSupTable    = SSupTable;
          this.SMillName    = SMillName;

          common = new Common();
     }

     public void printWithData()
     {
          VDesc          = new Vector();
          VKgs           = new Vector();
          VPurpose       = new Vector();

          SRDCNo         = "";
          SRecDate       = "";
          SSupplierName  = "";
          SAdd1          = "";
          SAdd2          = "";
          SAdd3          = "";
          
          iDocType       = 0;

          setDataintoDataTypes(SOutPassNo,SOutPassDate,iMillCode);

          iVectSize = VDesc.size()-1;

          setHead();
          setBody();
          setFoot(true);
     }

     public void setHead()
     {
          try
          {

               FW   . write("M");

               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");

               if(iMillCode == 0)
               {
                    FW   . write("|                                                      E "+SMillName+"F@M                                                                                                                                                                            |\n");
                    FW   . write("|                                                              EPudusuripalayam, Nambiyur.F@M                                                                                                                                                                            |\n");
               }
               else
               if(iMillCode == 1)
               {
                    FW   . write("|                                             E"+SMillName+"F@M                                                                                                                                                                            |\n");
                    FW   . write("|                                                          EPERUNDURAI     -   638 052F@M                                                                                                                                                                            |\n");
               }
               else
               {
                    FW   . write("|                                                      E "+SMillName+"F@M                                                                                                                                                                            |\n");
                    FW   . write("|                                                              EPudusuripalayam, Nambiyur.F@M                                                                                                                                                                            |\n");
               }
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");

               if(iDocType==0)
                    FW   . write("|                                                                       ERETURNABLE F@M                                                                                                                                                                            |\n");

               if(iDocType==1)
                    FW   . write("|                                                                    ENON RETURNABLEF@M                                                                                                                                                                            |\n");

               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|                                                                           EOUT PASSF@M                                                                                                                                                                            |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|    S.No : E"+common.Pad(SOutPassNo,12)+"F@M"+common.Space(24)+"                                                                                                          Date : "+common.Pad(common.parseDate(SOutPassDate),10)+" | Time Out : "+common.Pad(SOutTime,10)+" |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|    EName of the ConsigneeF               |"+common.Pad(" "+SSupplierName,65)+"|\n");
               FW   . write("|                                        |"+common.Pad(" "+SAdd1,65)+"|\n");
               FW   . write("|                                        |"+common.Pad(" "+SAdd2,65)+"|\n");
               FW   . write("|                                        |"+common.Pad(" "+SAdd3,65)+"|\n");
               FW   . write("|                                        |                                                                                                                                  |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|    Departement                         |    Stores                                                                                                                        |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|ES.NoF|                           EDescriptionF                           |  EQty(No/Kg)F  |                                       EPurposeF                                       |\n");

          }
          catch(Exception ex)
          {
               System    . out.println(ex);
               ex        . printStackTrace();
          }

     }
     public void setBody()
     {
          try
          {
               String    SData          = "";

               for(int i=0;i<VDesc.size();i++)
               {
                    iCount++;

                    iItemCount++;

                    if(iItemCount>8)
                    {
                         setFoot(false);
                         setHead();
                         iItemCount=0;
                    }

                    if(i<=iVectSize)
                    {
                         SData     = "|"+common.Rad(String.valueOf(iCount)+" ",4)+"|"+common.Pad(" "+(String)VDesc. elementAt(i)+"",65)+"|"+common.Rad(common.getRound((String)VKgs . elementAt(i),3)+" ",14)+"| "+common.Pad((String)VPurpose.elementAt(i),84);
                    }
                    else
                    {
                         SData     = "|"+common.Rad(String.valueOf(iCount)+" ",4)+"|"+common.Space(65)+"|"+common.Space(14)+"|"+common.Space(85); 
                    }

                    if(iCount!=0)
                    {
                         FW        . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                         FW        . write(SData+"|\n");
                         SData     = "";
                    }
               }         
          }
          catch(Exception ex)
          {
               System    . out.println(ex);
               ex        . printStackTrace();
          }
     }
     public void setFoot(boolean bflag)
     {
          try
          {
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|    Documents Enclosed                                                                                                                                                     |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|  1  |  DC No.      / Date |                                               |  2  |  Inv.No.   / Date   |                                                                   |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|  3  |  Form JJ No. / Date |                                               |  4  |  Check Post Letter  |                                                                   |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");

               if(iDocType==0)
                    FW   . write("|  5  |  RDC No.   / Date   |"+common.Pad("  "+SRDCNo+" / "+common.parseDate(common.parseNull(SRecDate)),47)+"|  6  |  NRDC No.  / Date   |                                                                   |\n");
               if(iDocType==1)
                    FW   . write("|  5  |  RDC No.   / Date   |                                               |  6  |  NRDC No.  / Date   |"+common.Pad("  "+SRDCNo+" / "+common.parseDate(common.parseNull(SRecDate)),67)+"|\n");

               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|  7  |  Other doc. If Any  |                                               |     |                     |                                                                   |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|  Document are verified and found correct and permitted the movement of the above materials.                                                                               |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|                                        |                                                       |                                                                          |\n");
               FW   . write("|                                        |                                                       |                                                                          |\n");
               FW   . write("|              Prepared By               |                Signature of the Staff                 |                     Permitted By                                         |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|    The above materials are verified and hase been sent out.                                                                                                               |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|                                        |                                                       |                                                                          |\n");
               FW   . write("|                                        |                                                       |                                                                          |\n");
               FW   . write("|                                        |            Actual Time Out                            |         Sign of Security Officer                                         |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|    Instructions :      1. Only J.M.D / AO / JO (Accts) are alone permitted to give outpass.                                                                               |\n");
               FW   . write("|                        2. Goods shall not be permitted to move between 6.00 PM to 9.00 AM.                                                                                |\n");
               FW   . write("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               if(bflag)
               {
                    FW   . write("<End of Report>\n");
               }
               else
               {
                    FW   . write("Continued on Next Page...\n");
               }
          }
          catch(Exception ex)
          {
               System    . out.println(ex);
               ex        . printStackTrace();
          }
     }

     private void setDataintoDataTypes(String SOutPassNo,String SOutPassDate,int iMillCode)
     {
          String QS =    " select rdcno,rdcdate,descript,qty,Name,addr1,addr2,addr3,doctype,remarks "+
                         " From RDC "+
                         " Inner Join "+SSupTable+" On "+SSupTable+".AC_Code = RDC.Sup_Code "+
                         " WHere OutPassNo ="+SOutPassNo+" and OutPassDate>="+common.pureDate(SOutPassDate)+"  and MillCode="+iMillCode+" ";
          try
          {
               if(theconnect==null)
               {
                    connect   = ORAConnection.getORAConnection();
                    theconnect= connect.getConnection();
               }
               Statement stat       = theconnect.createStatement();
               ResultSet theResult  = stat.executeQuery(QS);

               while(theResult.next())
               {
                    SRDCNo         = common.parseNull((String)theResult.getString(1));
                    SRecDate       = common.parseNull((String)theResult.getString(2));

                    VDesc          . addElement(common.parseNull((String)theResult.getString(3)));
                    VKgs           . addElement(common.parseNull((String)theResult.getString(4)));

                    SSupplierName  = common.parseNull((String)theResult.getString(5));
                    SAdd1          = common.parseNull((String)theResult.getString(6));
                    SAdd2          = common.parseNull((String)theResult.getString(7));
                    SAdd3          = common.parseNull((String)theResult.getString(8));

                    iDocType       = theResult.getInt(9);
                    VPurpose       . addElement(common.parseNull((String)theResult.getString(10)));
               }
               theResult . close();
               stat      . close();
          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}
