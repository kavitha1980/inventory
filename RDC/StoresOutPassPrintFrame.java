package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class StoresOutPassPrintFrame extends JInternalFrame
{
     Connection         theconnect;
     FileWriter         FW;
     
     JLayeredPane       Layer;
     JPanel             TopPanel,BottomPanel,MiddlePanel,TopLeft,TopRight;
     JButton            BApply,BOk,BCancel,BPool;
     JTabbedPane        thePane;
     
     DateField          fromDate;
     TabReport          theReport;
     
     Common             common   = new Common();
     ORAConnection      connect;
     
     Vector             VOutPassNo,VOutPassDate,VSupplier,VRDCNo,VRDCDate,VOutTime;
     
     int                iUserCode,iMillCode;
     String             SSupTable,SMillName;
     
     public StoresOutPassPrintFrame(JLayeredPane Layer,int iUserCode,int iMillCode,String SSupTable,String SMillName)
     {
          this.Layer     = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SMillName = SMillName;
          
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          try
          {
               thePane        = new JTabbedPane();
               fromDate       = new DateField();

               fromDate       . setTodayDate();
               fromDate       . TDay    . setEnabled(false);
               fromDate       . TMonth  . setEnabled(false);
               fromDate       . TYear   . setEnabled(false);
               
               TopPanel       = new JPanel(true);
               TopLeft        = new JPanel(true);
               TopRight       = new JPanel(true);
               BottomPanel    = new JPanel(true);
               MiddlePanel    = new JPanel(true);
               
               BApply         = new JButton("Apply");
               BOk            = new JButton("Print");
               BCancel        = new JButton("Cancel");
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,625,500);
          setTitle("RDC Printing Moniter");
          
          TopPanel       . setLayout(new GridLayout(1,2));
          TopLeft        . setLayout(new GridLayout(1,2));
          TopLeft        . setBorder(new TitledBorder("Date"));
          TopRight       . setLayout(new GridLayout(1,1));
          TopRight       . setBorder(new TitledBorder("Apply"));
          MiddlePanel    . setLayout(new BorderLayout());
          BottomPanel    . setLayout(new FlowLayout());
          TopPanel       . setBorder(new TitledBorder("Info"));
     }

     public void addComponents()
     {
          TopLeft             . add(new MyLabel("As On"));
          TopLeft             . add(fromDate);
          
          TopRight            . add(BApply);
          
          TopPanel            . add(TopLeft);
          TopPanel            . add(TopRight);
          
          BottomPanel         . add(BOk);
          BottomPanel         . add(BCancel);
          
          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("Center",MiddlePanel);
          getContentPane()    . add("South",BottomPanel);
     }

     public void addListeners()
     {
          BOk       . addActionListener(new ActList());
          BCancel   . addActionListener(new ActList());
          BApply    . addActionListener(new ActList());
     }
     
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    PrintStatus();
                    removeHelpFrame();
               }
               if(ae.getSource()==BApply)
               {
                    String    SStDate   = fromDate. toNormal();
                                                    showList(SStDate);
                                                    setTabReport();
               }
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
               }
          }
     }

     private void setTabReport()
     {
          try
          {
               MiddlePanel    . removeAll();
               thePane        . removeAll();
               
               String ColumnName[]  = {"Sl.No","OutPass No","Out Pass Date","Supplier","RDC No","RDC Date","Print" };
               String ColumnType[]  = {"N","N","S","S","S","S","B"};
               int    ColumnWidth[] = {50,60,100,100,100,100,100};
               
               if(VSupplier.size()==0)
                    return;
               
               Object RowData[][] = new Object[VSupplier.size()][ColumnName.length];

               for(int i=0;i<VSupplier.size();i++)
               {
                    RowData[i][0]   = String.valueOf(i+1);
                    RowData[i][1]   = common.parseNull((String)VOutPassNo.elementAt(i));
                    RowData[i][2]   = common.parseNull(common.parseDate((String)VOutPassDate.elementAt(i)));
                    RowData[i][3]   = common.parseNull((String)VSupplier.elementAt(i));
                    RowData[i][4]   = common.parseNull((String)VRDCNo.elementAt(i));
                    RowData[i][5]   = common.parseNull(common.parseDate((String)VRDCDate.elementAt(i)));
                    RowData[i][6]   = new Boolean(false);
               }
               theReport      = new TabReport(RowData,ColumnName,ColumnType);
               theReport      . setPrefferedColumnWidth(ColumnWidth);
               MiddlePanel    . add("Center",thePane);
               thePane         .addTab("Stores Print List",theReport);
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }
     
     private void PrintStatus()
     {
          Boolean bFlag;
          try
          {
               String SFileName = common.getPrintPath()+"StoresOutPass.prn";

               java.lang.Process p = Runtime.getRuntime().exec("cmd.exe /c attrib -r "+SFileName);
               p.waitFor();

               File file = new File(SFileName);

               FW = new FileWriter(file);

               for(int i=0;i<theReport.ReportTable.getRowCount();i++)
               {
                    bFlag  = (Boolean)theReport.ReportTable.getValueAt(i,6);
                    
                    if(bFlag.booleanValue())
                    {
                         String    SOutPassNo    = (String)theReport.ReportTable.getValueAt(i,1);
                         String    SOutPassDate  = (String)theReport.ReportTable.getValueAt(i,2);
                         String    SOutTime      = (String)VOutTime.elementAt(i);
                         printData(SOutPassNo,SOutPassDate,SOutTime);
                    }
               }
               FW   .close();
               file.setReadOnly();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void printData(String SOutPassNo,String SOutPassDate,String SOutTime)
     {
          try
          {
               StoresOutPassPrintClass  storesOutPassPrintClass  = new StoresOutPassPrintClass(FW,SOutPassNo,SOutPassDate,SOutTime,iMillCode,SSupTable,SMillName);
                                        storesOutPassPrintClass  . printWithData();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void showList(String SStDate)
     {
          VOutPassNo     = new Vector();
          VOutPassDate   = new Vector();
          VSupplier      = new Vector();
          VRDCNo         = new Vector();
          VRDCDate       = new Vector();
          VOutTime       = new Vector();
     
          String QS =    " select distinct OutPassNo,OutPassDate,rdcno,rdcdate,Name,OutTime "+
                         " From RDC "+
                         " Inner Join "+SSupTable+" On "+SSupTable+".AC_Code = RDC.Sup_Code "+
                         " WHere OutPassDate>="+SStDate+"  and MillCode="+iMillCode+" Order by 1";

          try
          {
               if(theconnect==null)
               {
                    connect   = ORAConnection.getORAConnection();
                    theconnect= connect.getConnection();
               }
               Statement stat       = theconnect.createStatement();
               ResultSet theResult  = stat.executeQuery(QS);

               while(theResult.next())
               {
                    VOutPassNo     . addElement(theResult.getString(1));
                    VOutPassDate   . addElement(common.parseNull(theResult.getString(2)));
                    VRDCNo         . addElement(theResult.getString(3));
                    VRDCDate       . addElement(common.parseNull(theResult.getString(4)));
                    VSupplier      . addElement(theResult.getString(5));
                    VOutTime       . addElement(common.parseNull(theResult.getString(6)));
               }
               theResult.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
}
