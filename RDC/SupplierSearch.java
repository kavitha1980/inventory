package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class SupplierSearch implements ActionListener
{

     JLayeredPane   Layer;
     JTextField     TSupCode;
     String         SSupTable;
     JButton        BName;
     JButton        BOk;
     Vector         VSupName,VSupCode;
     JTextField     TIndicator;          
     JList          BrowList;
     JScrollPane    BrowScroll;
     JPanel         BottomPanel;
     JInternalFrame MediFrame;
     String str="";
     ActionEvent ae;
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;

     SupplierSearch(JLayeredPane Layer,JTextField TSupCode,String SSupTable)
     {
          this.Layer       = Layer;
          this.TSupCode    = TSupCode;
          this.SSupTable   = SSupTable;

          VSupName         = new Vector();
          VSupCode         = new Vector();

          setDataIntoVector();

          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator.setEditable(false);

          BrowList         = new JList(VSupName);

          BrowScroll    = new JScrollPane(BrowList);

          BottomPanel   = new JPanel(true);
          BottomPanel.setLayout(new GridLayout(1,2));
          MediFrame     = new JInternalFrame("Supplier Selector");
          MediFrame.show();
          MediFrame.setBounds(80,100,550,350);
          MediFrame.setClosable(true);
          MediFrame.setResizable(true);
          BrowList.addKeyListener(new KeyList());
          MediFrame.getContentPane().setLayout(new BorderLayout());
          MediFrame.getContentPane().add("South",BottomPanel);
          MediFrame.getContentPane().add("Center",BrowScroll);
          BottomPanel.add(TIndicator);
          BottomPanel.add(BOk);
          BOk.addActionListener(new ActList());
          
     }

     public void setDataIntoVector()
     {
          VSupName.removeAllElements();
          VSupCode.removeAllElements();
          
          String QString = "Select Name,Ac_Code From "+SSupTable+" Order By Name";
          
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat      = theconnect.createStatement();
               ResultSet theResult = stat.executeQuery(QString);
          
               while(theResult.next())
               {
                    VSupName.addElement(theResult.getString(1));
                    VSupCode.addElement(theResult.getString(2));
               }
               theResult.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent e)
          {
               int index = BrowList.getSelectedIndex();
               setGrnDet(index);
               str="";
               removeHelpFrame();
          }
     }

     public void actionPerformed(ActionEvent ae)
     {
          this.ae = ae;
          BName = (JButton)ae.getSource();

          removeHelpFrame();
          
          try
          {
               Layer.add(MediFrame);
               MediFrame.moveToFront();
               MediFrame.setSelected(true);
               MediFrame.show();
               BrowList.requestFocus();
               Layer.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
     }  

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='&') || (lastchar=='-') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==116)    // F5 is pressed
               {
                    setDataIntoVector();
                    BrowList.setListData(VSupName);
               }
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    setGrnDet(index);
                    str="";
                    removeHelpFrame();
               }
          }
          
     }

         public void setCursor()
         {
            TIndicator.setText(str);
            int index=0;
            for(index=0;index<VSupName.size();index++)
            {
                 String str1 = ((String)VSupName.elementAt(index)).toUpperCase();
                 if(str1.startsWith(str))
                 {
                      BrowList.setSelectedValue(str1,true);
                      BrowList.ensureIndexIsVisible(index+10);
                      break;
                 }
            }
         }

         public void removeHelpFrame()
         {
            try
            {
               Layer.remove(MediFrame);
               Layer.repaint();
               Layer.updateUI();
               ((JButton)ae.getSource()).requestFocus();
            }
            catch(Exception ex) { }
         }

         public boolean setGrnDet(int index)
         {
               BName.setText((String)VSupName.elementAt(index));
               TSupCode.setText((String)VSupCode.elementAt(index));
               return true;
         } 
}
