package RDC;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class ValueWOMiddlePanel extends JPanel 
{
     WOInvMiddlePanel MiddlePanel;

     Object RowData[][];
     Object IdData[];
     Object SlData[];

     //                           0           1           2           3           4             5           6      7         8           9          10          11          12          13            14          15           16           17          18         19        20       21      22         23
     String ColumnData[] = {"RDC Descript","RDC No","WO Descript","RDC Qty","Prev WO Qty","Pending Qty","WO Qty","Rate","Discount(%)","CenVat(%)","Tax(%)","Surcharge(%)","Basic","Discount (Rs)","CenVat(Rs)","Tax(Rs)","Surcharge(Rs)","Net (Rs)","Department","Group","Due Date","Unit","Remarks","LogBook No","DocId"};
     String[] ColumnType = new String[25];

     Common common = new Common();
     JLayeredPane DeskTop;
     int iMillCode;
     String SValue;

     public ValueWOMiddlePanel(JLayeredPane DeskTop,int iMillCode,String SValue)
     {
          this.DeskTop   = DeskTop;
          this.iMillCode = iMillCode;
          this.SValue    = SValue;

          setLayout(new BorderLayout());
          setRowData();
          createComponents();
     }

     public void createComponents()
     {
          setColType();
          MiddlePanel = new WOInvMiddlePanel(DeskTop,RowData,ColumnData,ColumnType,iMillCode);
          add(MiddlePanel,BorderLayout.CENTER);
          MiddlePanel.ReportTable.requestFocus();
     }

     public void setRowData()
     {
          RowData     = new Object[1][ColumnData.length];

          RowData[0][0]  = " ";
          RowData[0][1]  = " ";
          RowData[0][2]  = " ";
          RowData[0][3]  = " ";
          RowData[0][4]  = " ";
          RowData[0][5]  = " ";
          RowData[0][6]  = "1";
          RowData[0][7]  = SValue;
          RowData[0][8]  = " ";
          RowData[0][9]  = " ";
          RowData[0][10] = " ";
          RowData[0][11] = " ";
          RowData[0][12] = " ";
          RowData[0][13] = " ";
          RowData[0][14] = " ";
          RowData[0][15] = " ";
          RowData[0][16] = " ";
          RowData[0][17] = " ";
          RowData[0][18] = " ";
          RowData[0][19] = " ";
          RowData[0][20] = " ";
          RowData[0][21] = " ";
          RowData[0][22] = " ";
          RowData[0][23] = " ";
          RowData[0][24] = " ";
     }

     public void setColType()
     {
          ColumnType[0]  = "S";
          ColumnType[1]  = "S";
          ColumnType[2]  = "E";
          ColumnType[3]  = "N";
          ColumnType[4]  = "N";
          ColumnType[5]  = "N";
          ColumnType[6]  = "B";
          ColumnType[7]  = "B";
          ColumnType[8]  = "B";
          ColumnType[9]  = "B";
          ColumnType[10] = "B";
          ColumnType[11] = "B";
          ColumnType[12] = "N";
          ColumnType[13] = "N";
          ColumnType[14] = "N";
          ColumnType[15] = "N";
          ColumnType[16] = "N";
          ColumnType[17] = "N";
          ColumnType[18] = "B";
          ColumnType[19] = "B";
          ColumnType[20] = "B";
          ColumnType[21] = "B";
          ColumnType[22] = "S";
          ColumnType[23] = "S";
          RowData[0][24] = " ";
     }




}



