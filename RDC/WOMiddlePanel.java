package RDC;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WOMiddlePanel extends JPanel 
{
     WOInvMiddlePanel MiddlePanel;

     Object RowData[][];
     Object IdData[];
     Object SlData[];

     //                           0           1           2           3           4             5           6      7         8           9          10          11          12          13            14          15           16           17          18         19        20       21      22         23
     String ColumnData[] = {"RDC Descript","RDC No","WO Descript","RDC Qty","Prev WO Qty","Pending Qty","WO Qty","Rate","Discount(%)","CenVat(%)","Tax(%)","Surcharge(%)","Basic","Discount (Rs)","CenVat(Rs)","Tax(Rs)","Surcharge(Rs)","Net (Rs)","Department","Group","Due Date","Unit","Remarks","LogBook No","DocId"};
     String[] ColumnType = new String[25];

     Common common = new Common();
     JLayeredPane DeskTop;

     Vector VName,VRdcNo,VSlNo,VQty,VPrevQty,VPendQty,VWOQty,VRDept,VRGroup,VRUnit;
     int iMillCode;
     boolean bEditFlag = false;

     public WOMiddlePanel(JLayeredPane DeskTop,Vector VName,Vector VRdcNo,Vector VSlNo,Vector VQty,Vector VPrevQty,Vector VPendQty,Vector VWOQty,Vector VRDept,Vector VRGroup,Vector VRUnit,int iMillCode,boolean bEditFlag)
     {
          this.DeskTop       = DeskTop;
          this.VName         = VName;
          this.VRdcNo        = VRdcNo;
          this.VSlNo         = VSlNo;
          this.VQty          = VQty;
          this.VPrevQty      = VPrevQty;
          this.VPendQty      = VPendQty;
          this.VWOQty        = VWOQty;
          this.VRDept        = VRDept;
          this.VRGroup       = VRGroup;
          this.VRUnit        = VRUnit;
          this.iMillCode     = iMillCode;
          this.bEditFlag     = bEditFlag;

          setLayout(new BorderLayout());
          setRowData();
          createComponents();
     }
                                   
     public WOMiddlePanel(JLayeredPane DeskTop,int iMillCode)
     {
          this.DeskTop   = DeskTop;
          this.iMillCode = iMillCode;

          setLayout(new BorderLayout());
     }

     public void createComponents()
     {
          setColType();
          MiddlePanel = new WOInvMiddlePanel(DeskTop,RowData,ColumnData,ColumnType,iMillCode);
          add(MiddlePanel,BorderLayout.CENTER);
          MiddlePanel.ReportTable.requestFocus();
     }

     public void setRowData()
     {
          RowData     = new Object[VName.size()][ColumnData.length];
          SlData      = new Object[VSlNo.size()];

          for(int i=0;i<VName.size();i++)
          {
               SlData[i]      = common.parseNull((String)VSlNo.elementAt(i));
               RowData[i][0]  = common.parseNull((String)VName.elementAt(i));
               RowData[i][1]  = common.parseNull((String)VRdcNo.elementAt(i));
               RowData[i][2]  = " ";
               RowData[i][3]  = common.parseNull((String)VQty.elementAt(i));
               RowData[i][4]  = common.parseNull((String)VPrevQty.elementAt(i));
               RowData[i][5]  = common.parseNull((String)VPendQty.elementAt(i));
               RowData[i][6]  = common.parseNull((String)VWOQty.elementAt(i));
               RowData[i][7]  = " ";
               RowData[i][8]  = " ";
               RowData[i][9]  = " ";
               RowData[i][10] = " ";
               RowData[i][11] = " ";
               RowData[i][12] = " ";
               RowData[i][13] = " ";
               RowData[i][14] = " ";
               RowData[i][15] = " ";
               RowData[i][16] = " ";
               RowData[i][17] = " ";
               RowData[i][18] = common.parseNull((String)VRDept.elementAt(i));
               RowData[i][19] = common.parseNull((String)VRGroup.elementAt(i));
               RowData[i][20] = " ";
               RowData[i][21] = common.parseNull((String)VRUnit.elementAt(i));
               RowData[i][22] = " ";
               RowData[i][23] = " ";
               RowData[i][24] = " ";
          }
     }

     public void setColType()
     {
          ColumnType[0]  = "S";
          ColumnType[1]  = "S";
          ColumnType[2]  = "E";
          ColumnType[3]  = "N";
          ColumnType[4]  = "N";
          ColumnType[5]  = "N";
          ColumnType[7]  = "B";
          ColumnType[8]  = "B";
          ColumnType[9]  = "B";
          ColumnType[10] = "B";
          ColumnType[11] = "B";
          ColumnType[12] = "N";
          ColumnType[13] = "N";
          ColumnType[14] = "N";
          ColumnType[15] = "N";
          ColumnType[16] = "N";
          ColumnType[17] = "N";
          ColumnType[18] = "B";
          ColumnType[19] = "B";
          ColumnType[20] = "B";
          ColumnType[21] = "B";
          ColumnType[22] = "S";
          ColumnType[23] = "S";

          ColumnType[23] = "E";


          if(bEditFlag)
          {
               ColumnType[6]  = "B";
          }
          else
          {
               ColumnType[6]  = "N";
          }

     }




}



