package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WOPendingList implements ActionListener
{

     JLayeredPane     Layer;
     WOMiddlePanel    MiddlePanel;
     WorkOrderFrame   workOrderFrame;
     int              iMillCode;

     JList          BrowList,SelectedList;
     JScrollPane    BrowScroll,SelectedScroll;
     JTextField     TIndicator;
     JButton        BOk;
     JPanel         LeftPanel,RightPanel;
     JInternalFrame MaterialFrame;
     JPanel         MFMPanel,MFBPanel;

     Vector VPendCode,VPendName,VRdcNo,VPendDetails,VSlNo,VRdcQty;

     Vector VSelectedName;
     Vector VSelectedId;
     Vector VSelectedRDC;
     Vector VSelectedSlNo,VSelectedQty;

     JTextField TSupCode;     
     String str="";
     int iMFSig=0;
     ActionEvent ae;
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;

     WOPendingList(JLayeredPane Layer,WOMiddlePanel MiddlePanel,JTextField TSupCode,WorkOrderFrame workOrderFrame,int iMillCode)
     {
          this.Layer          = Layer;
          this.MiddlePanel    = MiddlePanel;
          this.TSupCode       = TSupCode;
          this.workOrderFrame = workOrderFrame;
          this.iMillCode      = iMillCode;
     }

     public void createComponents()
     {
          VPendCode     = new Vector();
          VPendName     = new Vector();
          VRdcNo        = new Vector();
          VPendDetails  = new Vector();
          VSlNo         = new Vector();
          VRdcQty       = new Vector();

          VSelectedName = new Vector();
          VSelectedId   = new Vector();
          VSelectedRDC  = new Vector();
          VSelectedSlNo = new Vector();
          VSelectedQty  = new Vector();

          BrowList      = new JList(getPendName());
          SelectedList  = new JList();
          BrowScroll    = new JScrollPane(BrowList);
          SelectedScroll= new JScrollPane(SelectedList);
          LeftPanel     = new JPanel(true);
          RightPanel    = new JPanel(true);
          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator.setEditable(false);
          MFMPanel      = new JPanel(true);
          MFBPanel      = new JPanel(true);

          MaterialFrame = new JInternalFrame("Materials Expected From this Supplier");

          MaterialFrame.show();
          MaterialFrame.setBounds(80,100,550,350);
          MaterialFrame.setClosable(true);
          MaterialFrame.setResizable(true);

          BrowList.addKeyListener(new KeyList());
          BrowList.requestFocus();
          BOk.setMnemonic('E');
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent e)
          {
               if(VSelectedName.size()==0)
               {
                    JOptionPane.showMessageDialog(null,"No Material is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                    workOrderFrame.BSupplier.setEnabled(false);
                    setMiddlePanel(true);
                    removeHelpFrame();
                    ((JButton)ae.getSource()).setEnabled(false);
                    str="";
                    workOrderFrame.MiddlePanel.requestFocus();
               }
               else
               {
                    BOk.setEnabled(false);
                    workOrderFrame.BSupplier.setEnabled(false);
                    setMiddlePanel(false);
                    removeHelpFrame();
                    ((JButton)ae.getSource()).setEnabled(false);
                    str="";
                    workOrderFrame.MiddlePanel.requestFocus();
               }
          }
     }

     public void actionPerformed(ActionEvent ae)
     {
          this.ae = ae;
          JButton source = (JButton)ae.getSource();
          createComponents();

          if(VPendName.size()==0)
          {
               JOptionPane.showMessageDialog(null,"No Materials are Pending","Information",JOptionPane.INFORMATION_MESSAGE);
               workOrderFrame.BSupplier.setEnabled(false);
               setMiddlePanel(true);
               removeHelpFrame();
               ((JButton)ae.getSource()).setEnabled(false);
               ((JButton)ae.getSource()).requestFocus();
               str="";
               return;
          }

          TIndicator.setText(str);
          BOk       .setEnabled(true);
          source    .setEnabled(false);

          if(iMFSig==0)
          {
               MFMPanel  .setLayout(new GridLayout(1,2));
               MFBPanel  .setLayout(new GridLayout(1,2));
               MFMPanel  .add(BrowScroll);
               MFMPanel  .add(SelectedScroll);
               MFBPanel  .add(TIndicator);
               MFBPanel  .add(BOk);
               BOk       .addActionListener(new ActList());

               MaterialFrame.getContentPane().add("Center",MFMPanel);
               MaterialFrame.getContentPane().add("South",MFBPanel);
               iMFSig=1;
          }
          removeHelpFrame();

          try
          {
               Layer.add(MaterialFrame);
               MaterialFrame  .moveToFront();
               MaterialFrame  .setSelected(true);
               MaterialFrame  .show();
               BrowList       .requestFocus();
               Layer.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);

               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }
          
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();

                    String SMatName = (String)VPendName.elementAt(index);
                    String SMatCode = (String)VPendCode.elementAt(index);
                    String SRDCNo   = (String)VRdcNo   .elementAt(index);
                    String SSlNo    = (String)VSlNo    .elementAt(index);
                    String SQty     = (String)VRdcQty  .elementAt(index);

                    addMatDet(SMatName,SMatCode,SRDCNo,SSlNo,SQty);
                    str="";
                    TIndicator.setText(str);
               }
          }
     }

     public void setCursor()
     {
          int index=0;
          TIndicator.setText(str);

          for(index=0;index<VPendName.size();index++)
          {
               String str1 = ((String)VPendName.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedValue(str1,true);   
                    break;
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(MaterialFrame);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public boolean addMatDet(String SMatName,String SMatCode,String SRdcNo,String SSlNo,String SQty)
     {
          int iIndex=VSelectedName.indexOf(SMatName);

          if (iIndex==-1)
          {
               VSelectedName .addElement(SMatName);
               VSelectedId   .addElement(SMatCode);
               VSelectedRDC  .addElement(SRdcNo);
               VSelectedSlNo .addElement(SSlNo);
               VSelectedQty  .addElement(SQty);
          }
          else
          {
               VSelectedName.removeElementAt(iIndex);
               VSelectedId  .removeElementAt(iIndex);
               VSelectedRDC .removeElementAt(iIndex);
               VSelectedSlNo.removeElementAt(iIndex);
               VSelectedQty .removeElementAt(iIndex);
          }

          SelectedList.setListData(VSelectedName);
          return true;
     }

     public void setMiddlePanel(boolean bEmpty)
     {
          workOrderFrame.setMiddlePanel(VSelectedName,VSelectedId,VSelectedRDC,bEmpty,VSelectedSlNo,VSelectedQty);
     }

     public Vector getPendName()
     {
          VPendName.removeAllElements();
          VPendCode.removeAllElements();
          VRdcNo   .removeAllElements();
          VSlNo    .removeAllElements();
          VRdcQty  .removeAllElements();

          VPendDetails.removeAllElements();

          String QS = " Select * from ( "+
                      " Select Descript,0,RDCNo as RNo,RDCSlNo,Sum(RecQty) as RecQty From GateInwardRDC "+
                      " Where OrderNo=0 and WOStatus=1 and MillCode="+iMillCode+
                      " and Sup_Code = '"+TSupCode.getText()+"' "+
                      " Group by Descript,RDCNo,RdcSlNo ) "+
                      " Order By Descript,RNo ";

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();
               ResultSet theResult      = stat.executeQuery(QS);

               while(theResult.next())
               {
                    VPendName.addElement(theResult.getString(1));
                    VPendCode.addElement(String.valueOf(theResult.getInt(2)));
                    VRdcNo   .addElement(common.parseNull(String.valueOf(theResult.getInt(3))));
                    VSlNo    .addElement(common.parseNull(String.valueOf(theResult.getInt(4))));
                    VRdcQty  .addElement(common.parseNull(String.valueOf(theResult.getInt(5))));
               }
               theResult.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

          for(int i=0;i<VPendName.size();i++)
          {
               VPendDetails.addElement((String)VPendName.elementAt(i)+" - "+(String)VRdcNo.elementAt(i));
          }
          return VPendDetails;
     }

}
