package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WORDCInvMiddlePanel extends JPanel
{
     JTable         ReportTable;
     InvTableModel  dataModel;
  
     JLabel         LBasic,LDiscount,LCenVat,LTax,LSur,LNet;
     NextField      TAdd,TLess;
     JPanel         GridPanel,BottomPanel,FigurePanel,ControlPanel;
  
     JButton        BAdd;
  
     Vector VDept,VDeptCode,VGroup,VGroupCode,VUnit,VUnitCode;
     JComboBox JCDept,JCGroup,JCUnit;
  
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;
  
     JLayeredPane   DeskTop;
     Object         RowData[][];
     String         ColumnData[],ColumnType[];
     int            iMillCode;
  
     // Constructor Method refferred from Order Collection Operations
     WORDCInvMiddlePanel(JLayeredPane DeskTop,Object RowData[][],String ColumnData[],String ColumnType[],int iMillCode)
     {
          this.DeskTop     = DeskTop;
          this.RowData     = RowData;
          this.ColumnData  = ColumnData;
          this.ColumnType  = ColumnType;
          this.iMillCode   = iMillCode;
 
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
 
          setReportTable();
     }

     public void createComponents()
     {
          LBasic           = new JLabel("0");
          LDiscount        = new JLabel("0");
          LCenVat          = new JLabel("0");
          LTax             = new JLabel("0");
          LSur             = new JLabel("0");
          TAdd             = new NextField();
          TLess            = new NextField();
          LNet             = new JLabel("0");
          GridPanel        = new JPanel(true);
          BottomPanel      = new JPanel();
          FigurePanel      = new JPanel();
          ControlPanel     = new JPanel();
          BAdd             = new JButton("Add New Record");
          BAdd.setMnemonic('N');
     }
          
     public void setLayouts()
     {
          GridPanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new BorderLayout());
          FigurePanel.setLayout(new GridLayout(2,8));
          ControlPanel.setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          FigurePanel.add(new JLabel("Basic"));
          FigurePanel.add(new JLabel("Discount"));
          FigurePanel.add(new JLabel("CenVat"));
          FigurePanel.add(new JLabel("Tax"));
          FigurePanel.add(new JLabel("Surcharge"));
          FigurePanel.add(new JLabel("Plus"));
          FigurePanel.add(new JLabel("Minus"));
          FigurePanel.add(new JLabel("Net"));
 
          FigurePanel.add(LBasic);
          FigurePanel.add(LDiscount);
          FigurePanel.add(LCenVat);
          FigurePanel.add(LTax);
          FigurePanel.add(LSur);
          FigurePanel.add(TAdd);
          FigurePanel.add(TLess);
          FigurePanel.add(LNet);
 
          ControlPanel.add(BAdd);
 
          BottomPanel.add("North",FigurePanel);
          BottomPanel.add("South",ControlPanel);
 
          getDeptGroupUnit();
 
          JCDept  = new JComboBox(VDept);
          JCGroup = new JComboBox(VGroup);
          JCUnit  = new JComboBox(VUnit);
     }

     public void addListeners()
     {
          BAdd   .addActionListener(new ActList());
          TAdd   .addKeyListener(new KeyList());
          TLess  .addKeyListener(new KeyList());
     }
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               calc();
          }
     }
     public void calc()
     {
          double dTNet = common.toDouble(LBasic.getText())-common.toDouble(LDiscount.getText())+common.toDouble(LCenVat.getText())+common.toDouble(LTax.getText())+common.toDouble(LSur.getText());
          dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
          LNet.setText(common.getRound(dTNet,2));
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BAdd)
               {
                    Vector VEmpty1 = new Vector();

                    for(int i=0;i<ColumnData.length;i++)
                         VEmpty1.addElement(" ");
                    dataModel.addRow(VEmpty1);
                    ReportTable.updateUI();
                    ReportTable.requestFocus();
               }
          }
     }
     public void setReportTable()
     {
          try
          {
               dataModel        = new InvTableModel(RowData,ColumnData,ColumnType,LBasic,LDiscount,LCenVat,LTax,LSur,TAdd,TLess,LNet);
               ReportTable      = new JTable(dataModel);

               ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
     
               DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
               cellRenderer.setHorizontalAlignment(JLabel.RIGHT);
     
               for (int col=0;col<ReportTable.getColumnCount();col++)
               {
                    if(ColumnType[col]=="N" || ColumnType[col]=="B" || ColumnType[col]=="E")
                         ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
               }
               //ReportTable.setShowGrid(false);
     
               TableColumn deptColumn  = ReportTable.getColumn("Department");
               TableColumn groupColumn = ReportTable.getColumn("Group");
               TableColumn unitColumn  = ReportTable.getColumn("Unit");
     
               deptColumn .setCellEditor(new DefaultCellEditor(JCDept));
               groupColumn.setCellEditor(new DefaultCellEditor(JCGroup));
               unitColumn .setCellEditor(new DefaultCellEditor(JCUnit));
     
               setLayout(new BorderLayout());
               GridPanel.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
               GridPanel.add(new JScrollPane(ReportTable),BorderLayout.CENTER);
     
               add(BottomPanel,BorderLayout.SOUTH);
               add(GridPanel,BorderLayout.CENTER);

		ReportTable    .  addKeyListener(new KeyList2());

               //ReportTable    .  addKeyListener(new KeyList1());
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public class KeyList2 extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
			setDocumentFrame();
	       }	
	  }
      }	


     public class KeyList1 extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {



               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    int i  = ReportTable.getSelectedRow();
                    int jj = ReportTable.getSelectedColumn();

                    if(jj==18)
                    {
                         OrderRemarks MP = new OrderRemarks(DeskTop,ReportTable);
                         try
                         {
                              DeskTop   . add(MP);
                              DeskTop   . repaint();
                              MP        . setSelected(true);
                              DeskTop   . updateUI();
                              MP        . show();
                         }
                         catch(java.beans.PropertyVetoException ex){}
                    }
                    else
                    if(jj==19)
                    {
                         LogBookFrame LF = new LogBookFrame(DeskTop,ReportTable,iMillCode);
                         try
                         {
                              DeskTop   . add(LF);
                              DeskTop   . repaint();
                              LF        . setSelected(true);
                              DeskTop   . updateUI();
                              LF        . show();
                         }
                         catch(java.beans.PropertyVetoException ex){}
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Only in Remarks and LogNo Column","Dear User",JOptionPane.INFORMATION_MESSAGE);
                    }
               }
          }
     }


     private void  setDocumentFrame() {

	       System.out.println("comm doc frame");
			
	       int iRow = ReportTable.getSelectedRow();
	       ColumnType[20]="S";	  	

               DocumentationEntryFrame docFrame = new DocumentationEntryFrame(DeskTop,ReportTable,dataModel,iRow,true,true);
               try
               {
                    DeskTop.add(docFrame);
                    DeskTop.repaint();
                    docFrame.setSelected(true);
                    DeskTop.updateUI();
                    docFrame.show();
               }
               catch(java.beans.PropertyVetoException ex){}
     }

     public Object[][] getFromVector()
     {
          return dataModel.getFromVector();     
     }
     public int getRows()
     {
          return dataModel.getRows();
     }

     public String getDeptCode(int i)                       // 14
     {
          Vector VCurVector = dataModel.getCurVector(i);
          String str = (String)VCurVector.elementAt(14);
          int iid = VDept.indexOf(str);
          return (iid==-1 ?"0":(String)VDeptCode.elementAt(iid));
     }
     public String getGroupCode(int i)                     // 15
     {
          Vector VCurVector = dataModel.getCurVector(i);
          String str = (String)VCurVector.elementAt(15);
          int iid = VGroup.indexOf(str);
          return (iid==-1?"0":(String)VGroupCode.elementAt(iid));
     }
     public String getUnitCode(int i)                     //  17
     {
          Vector VCurVector = dataModel.getCurVector(i);
          String str = (String)VCurVector.elementAt(17);
          int iid = VUnit.indexOf(str);
          return (iid==-1?"0":(String)VUnitCode.elementAt(iid));
     }

     public String getDueDate(int i,DateField TDate)      //  16
     {
          Vector VCurVector = dataModel.getCurVector(i);

          try
          {
               String str = (String)VCurVector.elementAt(16);
               if((str.trim()).length()==0)
                    return TDate.toNormal();
               else
                    return common.pureDate(str);
          }
          catch(Exception ex)
          {
               return " ";
          }
     }

     public void getDeptGroupUnit()
     {
               
          VDept      = new Vector();
          VDeptCode  = new Vector();

          VGroup     = new Vector();
          VGroupCode = new Vector();

          VUnit      = new Vector();
          VUnitCode  = new Vector();

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();

               String QS1 = "";
               String QS2 = "";
               String QS3 = "";

               QS1 = "Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               QS2 = "Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               QS3 = "Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";

               ResultSet result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VDept.addElement(result.getString(1));
                    VDeptCode.addElement(result.getString(2));
               }
               result.close();
               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VGroup.addElement(result.getString(1));
                    VGroupCode.addElement(result.getString(2));
               }
               result.close();
               result = stat.executeQuery(QS3);
               while(result.next())
               {
                    VUnit.addElement(result.getString(1));
                    VUnitCode.addElement(result.getString(2));
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept,Group & Unit :"+ex);
          }
     }

}

