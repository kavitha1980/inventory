package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WORDCInvMiddlePanelGst extends JPanel
{
     JTable         ReportTable;
     InvTableModelGst  dataModel;
	 WORDCInvMiddlePanelGst    theFrame;

     JLabel         LBasic,LDiscount,LCGST,LSGST,LIGST,LCess,LNet;
     NextField      TAdd,TLess;
     JTextField     TSupCode;	
     JPanel         GridPanel,BottomPanel,FigurePanel,ControlPanel;

     JButton        BAdd;

     Vector VDept,VDeptCode,VGroup,VGroupCode,VUnit,VUnitCode;
     JComboBox JCDept,JCGroup,JCUnit;
     MyArrayList<String>    MaterialCodeList;
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;

     JLayeredPane   DeskTop;
     Object         RowData[][];
     String         ColumnData[],ColumnType[];
     int            iMillCode;
	 String SSupTable="Supplier",sSupplierCode="";
  
     // Constructor Method refferred from Order Collection Operations
     WORDCInvMiddlePanelGst(JLayeredPane DeskTop,Object RowData[][],String ColumnData[],String ColumnType[],int iMillCode,String sSupplierCode)
     {
          this.DeskTop     = DeskTop;
          this.RowData     = RowData;
          this.ColumnData  = ColumnData;
          this.ColumnType  = ColumnType;
          this.iMillCode   = iMillCode;
	  this.sSupplierCode = sSupplierCode;
 	  MaterialCodeList = new MyArrayList<String>();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
 
          setReportTable();
     }

     public void createComponents()
     {
          TSupCode         = new JTextField();		
          LBasic           = new JLabel("0");
          LDiscount        = new JLabel("0");
          LCGST            = new JLabel("0");
          LSGST            = new JLabel("0");
          LIGST            = new JLabel("0");
		  LCess            = new JLabel("0");
          TAdd             = new NextField();
          TLess            = new NextField();
          LNet             = new JLabel("0");
          GridPanel        = new JPanel(true);
          BottomPanel      = new JPanel();
          FigurePanel      = new JPanel();
          ControlPanel     = new JPanel();
          BAdd             = new JButton("Add New Record");
          BAdd.setMnemonic('N');
     }
          
     public void setLayouts()
     {
          GridPanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new BorderLayout());
          FigurePanel.setLayout(new GridLayout(2,9));
          ControlPanel.setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          FigurePanel.add(new JLabel("Basic"));
          FigurePanel.add(new JLabel("Discount"));
          FigurePanel.add(new JLabel("CGST"));
          FigurePanel.add(new JLabel("SGST"));
          FigurePanel.add(new JLabel("IGST"));
		  FigurePanel.add(new JLabel("Cess"));
          FigurePanel.add(new JLabel("Plus"));
          FigurePanel.add(new JLabel("Minus"));
          FigurePanel.add(new JLabel("Net"));
 
          FigurePanel.add(LBasic);
          FigurePanel.add(LDiscount);
          FigurePanel.add(LCGST);
          FigurePanel.add(LSGST);
          FigurePanel.add(LIGST);
		  FigurePanel.add(LCess);
          FigurePanel.add(TAdd);
          FigurePanel.add(TLess);
          FigurePanel.add(LNet);
 
          ControlPanel.add(BAdd);
 
          BottomPanel.add("North",FigurePanel);
          BottomPanel.add("South",ControlPanel);
 
          getDeptGroupUnit();
 
          JCDept  = new JComboBox(VDept);
          JCGroup = new JComboBox(VGroup);
          JCUnit  = new JComboBox(VUnit);
     }

     public void addListeners()
     {
          BAdd   .addActionListener(new ActList());
          TAdd   .addKeyListener(new KeyList());
          TLess  .addKeyListener(new KeyList());
     }
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               calc();
          }
     }
     public void calc()
     {
          double dTNet = common.toDouble(LBasic.getText())-common.toDouble(LDiscount.getText())+common.toDouble(LCGST.getText())+common.toDouble(LSGST.getText())+common.toDouble(LIGST.getText())+common.toDouble(LCess.getText());
          dTNet = dTNet+common.toDouble(TAdd.getText())-common.toDouble(TLess.getText());
          LNet.setText(common.getRound(dTNet,2));
     }
     
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BAdd)
               {
                    Vector VEmpty1 = new Vector();

                    for(int i=0;i<ColumnData.length;i++)
                         VEmpty1.addElement(" ");
                    dataModel.addRow(VEmpty1);
                    ReportTable.updateUI();
                    ReportTable.requestFocus();
               }
          }
     }
     public void setReportTable()
     {
          try
          {
               dataModel        = new InvTableModelGst(RowData,ColumnData,ColumnType,LBasic,LDiscount,LCGST,LSGST,LIGST,LCess,TAdd,TLess,LNet);
               ReportTable      = new JTable(dataModel);

               ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
     
               DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
               cellRenderer.setHorizontalAlignment(JLabel.RIGHT);
     
               for (int col=0;col<ReportTable.getColumnCount();col++)
               {
                    if(ColumnType[col]=="N" || ColumnType[col]=="B" || ColumnType[col]=="E")
                         ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
               }
               //ReportTable.setShowGrid(false);
     
               TableColumn deptColumn  = ReportTable.getColumn("Department");
               TableColumn groupColumn = ReportTable.getColumn("Group");
               TableColumn unitColumn  = ReportTable.getColumn("Unit");
     
               deptColumn .setCellEditor(new DefaultCellEditor(JCDept));
               groupColumn.setCellEditor(new DefaultCellEditor(JCGroup));
               unitColumn .setCellEditor(new DefaultCellEditor(JCUnit));
     
               setLayout(new BorderLayout());
               GridPanel.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
               GridPanel.add(new JScrollPane(ReportTable),BorderLayout.CENTER);
     
               add(BottomPanel,BorderLayout.SOUTH);
               add(GridPanel,BorderLayout.CENTER);

			   ReportTable    .  addKeyListener(new KeyList2());

             //  ReportTable    .  addKeyListener(new KeyList1());    // Comment Removed 
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public class KeyList2 extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
	       int iCol = ReportTable.getSelectedColumn();
	       int iSelectedRow  = ReportTable.getSelectedRow();		   
               if(ke.getKeyCode()==KeyEvent.VK_F5)
               {
					/*int dialogButton = JOptionPane.YES_NO_OPTION;
					//int dialogResult = JOptionPane.showConfirmDialog (null, "Would You Like to add Quantity?","Warning",dialogButton);
					double dOty = common.toDouble(common.parseNull((String)ReportTable.getValueAt(iSelectedRow, 4)).trim());
					int dialogResult = JOptionPane.showConfirmDialog (null, "Would You Like to add Only Service?","Warning",dialogButton);
					if(dialogResult == JOptionPane.YES_OPTION){
  						iQtyChk = 1;
					}*/
					setServList(iSelectedRow);
	       	   }
			   if(ke.getKeyCode()==KeyEvent.VK_F3)
			   {
					setDocumentFrame();
			   }		
	  }
      }
	private void setServList(int iSelectedRow)
     {
           ServiceAndMaterialPickerFrameGst SP = new ServiceAndMaterialPickerFrameGst(DeskTop,iSelectedRow,sSupplierCode,SSupTable,ReportTable,LBasic,LDiscount,LCGST,LSGST,LIGST,LCess,TAdd,TLess,LNet,MaterialCodeList,iMillCode);
           try
           {
                DeskTop   . add(SP);
                DeskTop   . repaint();
                SP        . setSelected(true);
                DeskTop   . updateUI();
                SP        . show();
                SP        . BrowList.requestFocus();			    				
           }
           catch(java.beans.PropertyVetoException ex){}
     }

     public class KeyList1 extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    int i  = ReportTable.getSelectedRow();
                    int jj = ReportTable.getSelectedColumn();

                    if(jj==22)
                    {
                         WorkOrderWithoutRDCOrderRemarksGst MP = new WorkOrderWithoutRDCOrderRemarksGst(DeskTop,ReportTable);
                         try
                         {
                              DeskTop   . add(MP);
                              DeskTop   . repaint();
                              MP        . setSelected(true);
                              DeskTop   . updateUI();
                              MP        . show();
                         }
                         catch(java.beans.PropertyVetoException ex){}
                    }
                    else
                    if(jj==23)
                    {
                         WorkOrderWithoutRdcLogBookFrameGst LF = new WorkOrderWithoutRdcLogBookFrameGst(DeskTop,ReportTable,iMillCode);
                         try
                         {
                              DeskTop   . add(LF);
                              DeskTop   . repaint();
                              LF        . setSelected(true);
                              DeskTop   . updateUI();
                              LF        . show();
                         }
                         catch(java.beans.PropertyVetoException ex){}
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Only in Remarks and LogNo Column","Dear User",JOptionPane.INFORMATION_MESSAGE);
                    }
               }
          }
     }


     private void  setDocumentFrame() {

	       System.out.println("comm doc frame");
			
	       int iRow = ReportTable.getSelectedRow();
	       ColumnType[24]="E";	  	

               DocumentationEntryFrameGst docFrame = new DocumentationEntryFrameGst(DeskTop,ReportTable,dataModel,iRow,true,true);
               try
               {
                    DeskTop.add(docFrame);
                    DeskTop.repaint();
                    docFrame.setSelected(true);
                    DeskTop.updateUI();
                    docFrame.show();
               }
               catch(java.beans.PropertyVetoException ex){}
     }

     public Object[][] getFromVector()
     {
          return dataModel.getFromVector();     
     }
     public int getRows()
     {
          return dataModel.getRows();
     }

     public String getDeptCode(int i)                       // 14
     {
          Vector VCurVector = dataModel.getCurVector(i);
          String str = (String)VCurVector.elementAt(18);
          int iid = VDept.indexOf(str);
          return (iid==-1 ?"0":(String)VDeptCode.elementAt(iid));
     }
     public String getGroupCode(int i)                     // 15
     {
          Vector VCurVector = dataModel.getCurVector(i);
          String str = (String)VCurVector.elementAt(19);
          int iid = VGroup.indexOf(str);
          return (iid==-1?"0":(String)VGroupCode.elementAt(iid));
     }
     public String getUnitCode(int i)                     //  17
     {
          Vector VCurVector = dataModel.getCurVector(i);
          String str = (String)VCurVector.elementAt(21);
          int iid = VUnit.indexOf(str);
          return (iid==-1?"0":(String)VUnitCode.elementAt(iid));
     }

     public String getDueDate(int i,DateField TDate)      //  16
     {
          Vector VCurVector = dataModel.getCurVector(i);

          try
          {
               String str = (String)VCurVector.elementAt(20);
               if((str.trim()).length()==0)
                    return TDate.toNormal();
               else
                    return common.pureDate(str);
          }
          catch(Exception ex)
          {
               return " ";
          }
     }
	 /*public String getItemCode(int i){
		return (String)VItemCode.elementAt(i);
	 }*/

     public void getDeptGroupUnit()
     {
               
          VDept      = new Vector();
          VDeptCode  = new Vector();

          VGroup     = new Vector();
          VGroupCode = new Vector();

          VUnit      = new Vector();
          VUnitCode  = new Vector();

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();

               String QS1 = "";
               String QS2 = "";
               String QS3 = "";

               QS1 = "Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               QS2 = "Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               QS3 = "Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";

               ResultSet result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VDept.addElement(result.getString(1));
                    VDeptCode.addElement(result.getString(2));
               }
               result.close();
               result = stat.executeQuery(QS2);
               while(result.next())
               {
                    VGroup.addElement(result.getString(1));
                    VGroupCode.addElement(result.getString(2));
               }
               result.close();
               result = stat.executeQuery(QS3);
               while(result.next())
               {
                    VUnit.addElement(result.getString(1));
                    VUnitCode.addElement(result.getString(2));
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept,Group & Unit :"+ex);
          }
     }
}

