package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkGRNCollectionFrame1 extends JInternalFrame
{

     DateField      TInvDate,TDCDate;
     DateField      TDate;
     MyTextField    TInvNo,TDCNo;
     JTextField     TSupName;
     WholeNumberField TInvoice;
     WorkGRNMiddlePanel1 MiddlePanel;

     JPanel         TopPanel,BottomPanel;
     JButton        BOk,BCancel;
     MyLabel        LGrnNo;

     JLayeredPane   DeskTop;
     Vector         VCode,VName;
     StatusPanel    SPanel;
     String         SIndex;
     int            iIndex=0;
     JTable         ReportTable;
     String         SSupCode;
     int            iUserCode;
     int            iMillCode;
     WorkGRNFrame1   workgrnframe;
     String         SYearCode;

     Common common   = new Common();
     ORAConnection connect;
     Connection theconnect;

     boolean             bComflag = true;
     
     WorkGRNCollectionFrame1(JLayeredPane DeskTop,StatusPanel SPanel,String SIndex,JTable ReportTable,String SSupCode,int iUserCode,int iMillCode,WorkGRNFrame1 workgrnframe,String SYearCode)
     {
          this.DeskTop        = DeskTop;
          this.SPanel         = SPanel;
          this.SIndex         = SIndex;
          this.ReportTable    = ReportTable;
          this.SSupCode       = SSupCode;
          this.iUserCode      = iUserCode;
          this.iMillCode      = iMillCode;
          this.workgrnframe   = workgrnframe;
          this.SYearCode      = SYearCode;

          iIndex              = common.toInt(SIndex);
		
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }

     public void createComponents()
     {
          BOk         = new JButton("Save");
          BCancel     = new JButton("Abort");
                      
          TDate       = new DateField();
          TInvDate    = new DateField();
          TDCDate     = new DateField();
          TInvNo      = new MyTextField(15);
          TDCNo       = new MyTextField(15);
          TSupName    = new JTextField();
          TInvoice    = new WholeNumberField(2);
          LGrnNo      = new MyLabel();

          MiddlePanel = new WorkGRNMiddlePanel1(DeskTop,SSupCode,iMillCode,SYearCode);

          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          TDate     . setTodayDate();
          TSupName  . setEditable(false);
          TDate     . setEditable(false);
          BOk       . setMnemonic('S');
          BCancel   . setMnemonic('A');
     }

     public void setLayouts()
     {
          setTitle("Invoice Valuation of Materials recd against Work Order");

          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,790,500);

          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(4,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("GRN No"));
          TopPanel.add(LGrnNo);

          TopPanel.add(new JLabel("GRN Date"));
          TopPanel.add(TDate);

          TopPanel.add(new JLabel("Supplier"));
          TopPanel.add(TSupName);

          TopPanel.add(new JLabel("No. of Bills"));
          TopPanel.add(TInvoice);

          TopPanel.add(new JLabel("Invoice No"));
          TopPanel.add(TInvNo);

          TopPanel.add(new JLabel("Invoice Date"));
          TopPanel.add(TInvDate);

          TopPanel.add(new JLabel("DC No"));
          TopPanel.add(TDCNo);

          TopPanel.add(new JLabel("DC Date"));
          TopPanel.add(TDCDate);

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          TSupName.setText((String)ReportTable.getModel().getValueAt(iIndex,0));

          LGrnNo         . setText(getNextOrderNo());
          MiddlePanel    . createComponents();
     }

     public void addListeners()
     {
          BOk       . addActionListener(new ActList());
          BCancel   . addActionListener(new CancelList());
     }

     public class CancelList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               removeHelpFrame();
          }
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               /* changed on 14.08.2010 for additional validation purpose (during multi company addition changes)*/

               if(allOk())              
               {
                    BOk.setEnabled(false);
                    String    SGRNNo  = getNextOrderNo();
                    LGrnNo            . setText(SGRNNo);

                    insertGRNCollectedData(SGRNNo);
                    workgrnframe   . addComponents();
                    BOk            . setEnabled(true);
               }
          }
     }

     private void insertGRNCollectedData(String SGRNNo)
     {
          insertGRNDetails(SGRNNo);
          setOrderLink();
          updateGRNNo();
          setAutoCommitStatus();
          removeHelpFrame();
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public void insertGRNDetails(String SGRNNo)
     {

          int iSerlNo = 0;
          String QString = "Insert Into WorkGRN (ID,OrderNo,GrnNo,GrnDate,Sup_Code,GateInNo,GateInDate,InvNo,InvDate,DcNo,DcDate,Descript,InvQty,MillQty,Qty,Pending,OrderQty,InvRate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,InvAmount,InvNet,Plus,Less,Misc,Dept_Code,Group_Code,Unit_Code,InvSlNo,SlNo,ActualModVat,UserCode,NoOfBills,OrderSlNo,WODesc,EntryStatus,MemoAuthUserCode,MillCode) Values (";
          String QSL     = "Select Max(InvSlNo) From WorkGRN";
          int iInvSlNo = 0;
          int iSlNo = 0;

          try
          {
               if(theconnect==null)
               {
                     connect       = ORAConnection.getORAConnection();
                     theconnect    = connect.getConnection();
               }

               Statement stat      = theconnect.createStatement();
               ResultSet res       = stat.executeQuery(QSL);

               while(res.next())
               {
                    iInvSlNo = res.getInt(1);
               }
               res.close();

               iInvSlNo++;

               Object RowData[][]  = MiddlePanel.MiddlePanel.getFromVector();                 
               String SAdd         = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess        = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm          = common.toDouble(SAdd)-common.toDouble(SLess);
               double dActVat      = MiddlePanel.MiddlePanel.getModVat();
               double dVatBasic    = MiddlePanel.MiddlePanel.getVatBasic();
               double dBasic       = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio       = dpm/dBasic;
               double dVatRatio    = dActVat/dVatBasic;               

               for(int i=0;i<RowData.length;i++)
               {
                    iSerlNo = common.toInt(common.parseNull((String)MiddlePanel.SlData[i]));

                    double dDcQty     = common.toDouble((String)RowData[i][4]);
                    if(dDcQty == 0)
                         continue;

                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SOrderNo   = MiddlePanel.getOrderNo(i);
                    String SOrdQty    = MiddlePanel.getOrdQty(i);
                    String SGINo      = MiddlePanel.getGINo(i);
                    String SGIDate    = MiddlePanel.getGIDate(i);

                    String SMemoUserCode = MiddlePanel.getMemoUserCode(i);

                    if(common.toInt(SMemoUserCode)<=0)
                    {
                         SMemoUserCode = "1";
                    }

                    dBasic            = common.toDouble((String)RowData[i][11]);
                    double dCenVatPer = common.toDouble((String)RowData[i][8]);
                    String SCenVat    = "0";

                    if(dCenVatPer > 0)
                         SCenVat = common.getRound(dBasic*dVatRatio,3);

                    String SMisc      = common.getRound(dBasic*dRatio,3);

                    String SEntryStatus = "1";

                    iSlNo++;
                                             
                    String QS1 = QString;
                    QS1 = QS1+"WorkGrn_Seq.nextval,";
                    QS1 = QS1+"0"+SOrderNo+",";
                    QS1 = QS1+"0"+SGRNNo+",";
                    QS1 = QS1+"'"+TDate.toNormal()+"',";
                    QS1 = QS1+"'"+SSupCode+"',";
                    QS1 = QS1+"0"+SGINo+",";
                    QS1 = QS1+"'"+SGIDate+"',";
                    QS1 = QS1+"'"+TInvNo.getText()+"',";
                    QS1 = QS1+"'"+TInvDate.toNormal()+"',";
                    QS1 = QS1+"'"+TDCNo.getText()+"',";
                    QS1 = QS1+"'"+TDCDate.toNormal()+"',";
                    QS1 = QS1+"'"+(String)RowData[i][0]+"',";
                    QS1 = QS1+"0"+(String)RowData[i][4]+",";
                    QS1 = QS1+"0"+(String)RowData[i][5]+",";
                    QS1 = QS1+"0"+(String)RowData[i][5]+",";
                    QS1 = QS1+"0"+(String)RowData[i][3]+",";
                    QS1 = QS1+"0"+SOrdQty+",";
                    QS1 = QS1+"0"+(String)RowData[i][6]+",";
                    QS1 = QS1+"0"+(String)RowData[i][7]+",";
                    QS1 = QS1+"0"+(String)RowData[i][12]+",";
                    QS1 = QS1+"0"+(String)RowData[i][8]+",";
                    QS1 = QS1+"0"+(String)RowData[i][13]+",";
                    QS1 = QS1+"0"+(String)RowData[i][9]+",";
                    QS1 = QS1+"0"+(String)RowData[i][14]+",";
                    QS1 = QS1+"0"+(String)RowData[i][10]+",";
                    QS1 = QS1+"0"+(String)RowData[i][15]+",";
                    QS1 = QS1+"0"+(String)RowData[i][16]+",";
                    QS1 = QS1+"0"+(String)RowData[i][16]+",";
                    QS1 = QS1+"0"+SAdd+",";
                    QS1 = QS1+"0"+SLess+",";
                    QS1 = QS1+"0"+SMisc+",";
                    QS1 = QS1+"0"+SDeptCode+",";
                    QS1 = QS1+"0"+SGroupCode+",";
                    QS1 = QS1+"0"+SUnitCode+",";
                    QS1 = QS1+"0"+iInvSlNo+",";
                    QS1 = QS1+"0"+iSlNo+",";
                    QS1 = QS1+"0"+SCenVat+",";
                    QS1 = QS1+"0"+iUserCode+",";
                    QS1 = QS1+"0"+TInvoice.getText()+",";
                    QS1 = QS1+"0"+iSerlNo+",";
                    QS1 = QS1+"'"+(String)RowData[i][1]+"',";
                    QS1 = QS1+"0"+SEntryStatus+",";
                    QS1 = QS1+"0"+SMemoUserCode+",";
                    QS1 = QS1+"0"+iMillCode+")";
                         
                    if(theconnect.getAutoCommit())
                         theconnect     . setAutoCommit(false);

                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("@QS1 : "+ex);
          }
     }

     public void setOrderLink()
     {
          try
          {
               if(theconnect==null)
               {
                     connect=ORAConnection.getORAConnection();
                     theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
               for(int i=0;i<RowData.length;i++)
               {
                    double dDcQty     = common.toDouble((String)RowData[i][4]);
                    if(dDcQty == 0)
                         continue;
     
                    String    QS = "Update WorkOrder Set ";
                              QS = QS+" InvQty=InvQty+"+(String)RowData[i][4];
                              QS = QS+" Where Id = "+(String)MiddlePanel.VId.elementAt(i);

                    if(theconnect.getAutoCommit())
                         theconnect    . setAutoCommit(false);
     
                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }
     public void updateGRNNo()
     {
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();

               if(theconnect.getAutoCommit())
                    theconnect    . setAutoCommit(false);

               String QS1 = " Update Config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1  where Id = 12";
               stat.executeUpdate(QS1);
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public String getNextOrderNo()
     {
          String QS = "";

          QS = " Select (MaxNo+1) From Config"+iMillCode+""+SYearCode+" "+
               " Where ID=12 ";

          String SOrderNo = String.valueOf(common.toInt(common.getID(QS)));
          return SOrderNo;
     }

     private boolean allOk()
     {

          String SCurDate     = common.parseDate(common.getServerPureDate());
          String SInvoice     = common.parseNull(TInvoice.getText().trim());
          String SInvNo       = common.parseNull(TInvNo.getText().trim());
          String SInvDate     = TInvDate.toNormal();
          String SDCNo        = common.parseNull(TDCNo.getText().trim());
          String SDCDate      = TDCDate.toNormal();
          int    iInvDateDiff = common.toInt(common.getDateDiff(common.parseDate(SInvDate),SCurDate));
          int    iDCDateDiff  = common.toInt(common.getDateDiff(common.parseDate(SDCDate),SCurDate));

          if(common.toInt(SInvoice)==0)
          {
              JOptionPane.showMessageDialog(null,"No. of Bills Field is Empty","Error",JOptionPane.ERROR_MESSAGE);
              TInvoice.requestFocus();
              return false;
          }
          if(SDCNo.equals("") && SInvNo.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Select DC No Or Invoice No","Error",JOptionPane.ERROR_MESSAGE);
               TInvNo.requestFocus();
               return false;
          }
          if((SInvNo.length()==0) && (SInvDate.length()>0))
          {
              JOptionPane.showMessageDialog(null,"Invoice No Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
              TInvNo.requestFocus();
              return false;
          }
          if((SInvNo.length()>0) && ((SInvDate.length()<8) || (iInvDateDiff>0)))
          {
              JOptionPane.showMessageDialog(null,"Invoice Date is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
              TInvDate.TDay.requestFocus();
              return false;
          }
          if((SDCNo.length()==0) && (SDCDate.length()>0))
          {
              JOptionPane.showMessageDialog(null,"D.C. No Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
              TDCNo.requestFocus();
              return false;
          }
          if((SDCNo.length()>0) && ((SDCDate.length()<8) || (iDCDateDiff>0)))
          {
              JOptionPane.showMessageDialog(null,"D.C. Date is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
              TDCDate.TDay.requestFocus();
              return false;
          }

          Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<RowData.length;i++)
          {
               double dDcQty   = common.toDouble((String)RowData[i][4]);
               double dInvQty  = common.toDouble((String)RowData[i][5]);
               String sDesc    = (String)RowData[i][0];
		   String SRDCN    = MiddlePanel.getRDCNo(i);

               if(dDcQty != dInvQty)
               {
                    JOptionPane.showMessageDialog(null,"Quantity Mismatch","Error",JOptionPane.ERROR_MESSAGE);
                    MiddlePanel.requestFocus();
                    return false;
               }
               if(dDcQty>0)
               {
                    String SRDCNo = MiddlePanel.getRDCNo(i);
                    if(common.toInt(SRDCNo)>0)
                    {
                         String SGINo  = MiddlePanel.getGINo(i);
     
                         if(SGINo.equals("") || SGINo.equals("0"))
                         {
                              JOptionPane.showMessageDialog(null,"Goods Not Recd for this Order @Row-"+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                              MiddlePanel.requestFocus();
                              return false;
                         }
                    }
               }

          }

          double dTDCQty=0;
          for(int i=0;i<RowData.length;i++)
          {
               dTDCQty   = dTDCQty+common.toDouble((String)RowData[i][4]);
          }
          if(dTDCQty == 0)
          {
               JOptionPane.showMessageDialog(null,"Invalid GRN - No Entries Made","Error",JOptionPane.ERROR_MESSAGE);
               MiddlePanel.requestFocus();
               return false;
          }
System.out.println("CheckRDCAuthStatus");
          for(int i=0;i<RowData.length;i++)
          {
               String sDesc   	 = (String)RowData[i][0];
		   String SRDCNo    	 = MiddlePanel.getRDCNo(i);
	         int iRDCAuthStatus	 = getRDCAuthStatus(sDesc.trim(),SRDCNo);

System.out.println("CheckRDCAuthStatus Inside Loop-->"+SRDCNo+"--"+iRDCAuthStatus );

		   if(iRDCAuthStatus==0){
			System.out.println("Inside Else ----------->"+iRDCAuthStatus+"--"+SRDCNo);
			JOptionPane.showMessageDialog(null," This RDC No ("+SRDCNo+" )  is Not Authenticated","Error",JOptionPane.ERROR_MESSAGE);
	            return false;
		   }
	    }
          return true;

     }

     private void setAutoCommitStatus()
     {
          try
          {
               if(bComflag)
               {
                    theconnect     . commit();
                    System         . out.println("Commit");
                    theconnect     . setAutoCommit(true);
               }
               else
               {
                    theconnect     . rollback();
                    System         . out.println("RollBack");
                    theconnect     . setAutoCommit(true);
               }
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

private int getRDCAuthStatus(String sDescription, String sRDCNo){
int iRDCAuthStatus=-1;
try{
        StringBuffer sb     =new StringBuffer(); 
        
        sb.append("  Select RDCAuthStatus from RDC ");
	  sb.append("  Where RDCNo='"+ sRDCNo+"'  and Descript ='"+sDescription+"'");
        
        System.out.println("GetRDCAuthStatus Qry:"+sb.toString());
        if (theconnect==null)
              {
                 connect       = ORAConnection.getORAConnection();
                 theconnect    = connect.getConnection();
              }
        PreparedStatement ps   = theconnect.prepareStatement(sb.toString());
        ResultSet rst          = ps.executeQuery();
    
       while (rst.next())
         {
            iRDCAuthStatus    = rst.getInt(1);
         }
            rst.close();
            ps.close();
            ps=null;
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("getNoofEmp:"+e);
    }
    return iRDCAuthStatus;

}


}
