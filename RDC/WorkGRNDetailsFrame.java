package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkGRNDetailsFrame extends JInternalFrame
{
     Object RowData[][];
     String ColumnData[] = {"GRN No","Date","Supplier","Order No","G.I. No","G.I. Date","DC No","DC Date","Inv No","Inv Date","Description","Inv Qty","Recd Qty","Inv Rate","Amount","Accepted Qty","Dept","Group","Unit","Status"};
     String ColumnType[] = {"N"     ,"S"   ,"S"       ,"N"       ,"N"      ,"S"        ,"N"    ,"S"      ,"N"     ,"S"       ,"S"          ,"N"      ,"N"       ,"N"       ,"N"     ,"N"           ,"S"   ,"S"    ,"S"   ,"N"     };

     Vector VGrnNo,VGrnDate,VSupName,VOrdNo,VDcNo,VDcDate,VGno,VGDate,VINo,VIDate,VGrnName,VIQty,VRQty,VIRate,VIAmt,VAccQty,VGrnDeptName,VGrnCataName,VGrnUnitName,VStatus,VSPJNo,VId;

     TabReport tabreport;

     WorkGRNCritPanel   TopPanel;
     JPanel BottomPanel;
     JButton BPrint;
     JTextField TFileName;
     FileWriter FW;
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;
     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;
     String SUnit="";
     String SDept="";
     String SGroup="";
     String SSupplier="";
     String SStDate = "";
     String SEnDate = "" ;
     int iList;
     int len=0;
     int iPrintLine =0;
     int iUserCode,iMillCode;
     String SSupTable,SMillName;

     int ipagec=1;
     String unit="",dept="",group="",sup="",list="";

     WorkGRNDetailsFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable,String SMillName)
     {
         super("WorkGRN Details Frame");
         this.DeskTop   = DeskTop;
         this.SPanel    = SPanel;
         this.iUserCode = iUserCode;
         this.iMillCode = iMillCode;
         this.SSupTable = SSupTable;
         this.SMillName = SMillName;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }

     public void createComponents()
     {
         TopPanel    = new WorkGRNCritPanel(iMillCode,SSupTable);
         BottomPanel = new JPanel();
         BPrint      = new JButton("Print");
         TFileName   = new JTextField(25);
         TFileName.setText("WorkGRNListDetails.prn");
     }

     public void setLayouts()
     {
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,650,500);
     }

     public void addComponents()
     {
         BPrint.setEnabled(false);
         BottomPanel.add(BPrint);
         BottomPanel.add(TFileName);
         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);
         
     }
     public void addListeners()
     {
         TopPanel.BApply.addActionListener(new ApplyList());
         BPrint.addActionListener(new PrintList());
     }
     private void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.updateUI();
               DeskTop.repaint();
          }
          catch(Exception ex){}
     }
     public class PrintList implements  ActionListener
     {
       public void actionPerformed(ActionEvent ae)
       {
                 PrnFileWrite();
                 JOptionPane.showMessageDialog(null,"File is Successfully Saved","InformationMessage",JOptionPane.INFORMATION_MESSAGE);
                 BPrint.setEnabled(false);
       }
     }
     public void PrnFileWrite()
     {
           try
           {
             String SFile =TFileName.getText();

             if((SFile.trim()).length()==0)
                SFile = "1.prn";

             FW = new FileWriter(common.getPrintPath()+SFile);
             PrintMainHead();
             PrintHead();
             PrintData();

             String SStatus = (new java.util.Date()).toString();

             String str3 = common.Replicate("-",len)+"\n";
             str3        = str3+"Report Taken on :"+SStatus+"\n";
             prtStrl(str3);

             FW.close();
           }
          catch(Exception e)
          {
            System.out.println(e);
          }
    }
     private void PrintMainHead()
     {
          String s="";

          prtStrl(SMillName);
          prtStrl("WorkGRNListDetails From "  +common.parseDate(TopPanel.TStDate.toNormal())+  "  TO "  +common.parseDate(TopPanel.TEnDate.toNormal()));

               s="Report Taken By ";
          if(TopPanel.JRAllUnit.isSelected())
          {
               unit=" Unit : ALL ";
              
          }
          else
          {
               unit = "Unit : "+((String)TopPanel.JCUnit.getSelectedItem());
          }
                s  = s +unit;

          if(TopPanel.JRAllDept.isSelected())
          {
               dept=" Dept : ALL ";
          }
          else
          {
               dept = "Dept : "+((String)TopPanel.JCDept.getSelectedItem());
          }
               s  = s +" and "+ dept;

          if(TopPanel.JRAllGroup.isSelected())
          {
               group=" Group : ALL ";
          }
          else
          {
               group= "Group : "+((String)TopPanel.JCGroup.getSelectedItem());
          }
               s  = s +" and "+ group;

          if(TopPanel.JRAllSup.isSelected())
          {
               sup=" Supplier : ALL ";
          }
          else
          {
               sup= "Supplier : "+((String)TopPanel.JCSup.getSelectedItem());
          }
               s  = s +" and "+ sup;

          if(TopPanel.JRAllList.isSelected())
          {
               list="";
          }
          else
          {
               list= ((String)TopPanel.JCList.getSelectedItem());
               s  = s +" and "+ list;
          } 
               prtStrl(s);
               prtStrl("Page:"+ipagec);
     }
     private void PrintHead()
     {
      String   S0= common.Cad("SL No",5)+" ";
      String   S1= common.Pad("GRN No",8)+" ";
      String   S2= common.Cad("Date",10)+" ";
      String   S3= common.Pad("Supplier",25)+" ";
      String   S4= common.Pad("Order No",8)+" ";
      String   S5= common.Pad("G.I. No",8)+" ";
      String   S6= common.Cad("G.I. Date",10)+" ";
      String   S9= common.Pad("Inv No",10)+" ";
      String   S10=common.Cad("Inv Date",10)+" ";

      String   S11= common.Pad("Description",35)+" ";
      String   S12= common.Rad("Inv Qty",10)+" ";
      String   S13= common.Rad("Recd Qty",10)+" ";
      String   S14= common.Rad("Inv Rate",10)+" ";
      String   S15= common.Rad("Amount",10)+" ";
      String   S16= common.Rad("Accept Qty",10)+" ";
      String   S17= common.Cad("Dept",15)+" ";
      String   S18= common.Cad("Group",22)+" ";
      String   S19= common.Cad("Unit",10)+" ";

        String Mar=S0+S1+S2+S3+S4+S5+S6+S9+S10+S11+S12+S13+S14+S15+S16+S17+S18+S19;
        len=Mar.length();
        prtStrl(common.Replicate("-",len));
        prtStrl(Mar);
        prtStrl(common.Replicate("-",len));
     }
     private void PrintData()
     {
           for(int i=0; i<VGrnNo.size(); i++)
           {
             String Str0  =Integer.toString(i+1);
             String Str1  =(String)VGrnNo.elementAt(i);
             String Str2  =(String)VGrnDate.elementAt(i);
             String Str3  =(String)VSupName.elementAt(i);
             String Str4  =(String)VOrdNo.elementAt(i);
             String Str5  =(String)VGno.elementAt(i);
             String Str6  =(String)VGDate.elementAt(i);
             String Str7  =(String)VDcNo.elementAt(i);
             String Str8  =(String)VDcDate.elementAt(i);
             String Str9  =(String)VINo.elementAt(i);
             String Str10 = (String)VIDate.elementAt(i);

             String Str11  =(String)VGrnName.elementAt(i);
             String Str12  =(String)VIQty.elementAt(i);
             String Str13  =(String)VRQty.elementAt(i);
             String Str14  =(String)VIRate.elementAt(i);
             String Str15  =(String)VIAmt.elementAt(i);
             String Str16  =(String)VAccQty.elementAt(i);
             String Str17  =(String)VGrnDeptName.elementAt(i);
             String Str18  =(String)VGrnCataName.elementAt(i);
             String Str19  =(String)VGrnUnitName.elementAt(i);
             String Str20 = (String)VIDate.elementAt(i);

             String   S0 = common.Rad(Str0,5)+" ";
             String   S1 = common.Pad(Str1,8)+" ";
             String   S2 = common.Pad(Str2,10)+" ";
             String   S3 = common.Pad(Str3,25)+" ";
             String   S4 = common.Pad(Str4,8)+" ";
             String   S5 = common.Pad(Str5,8)+" ";
             String   S6 = common.Pad(Str6,10)+" ";
             String   S9 = common.Pad(Str9,10)+" ";
             String   S10= common.Pad(Str10,10)+" ";

             String   S11 = common.Pad(Str11,35)+" ";
             String   S12 = common.Rad(Str12,10)+" ";
             String   S13 = common.Rad(Str13,10)+" ";
             String   S14 = common.Rad(Str14,10)+" ";
             String   S15 = common.Rad(Str15,10)+" ";
             String   S16 = common.Rad(Str16,10)+" ";
             String   S17 = common.Pad(Str17,15)+" ";
             String   S18 = common.Pad(Str18,22)+" ";
             String   S19 = common.Pad(Str19,10)+" ";

              String Mar=S0+S1+S2+S3+S4+S5+S6+S9+S10+S11+S12+S13+S14+S15+S16+S17+S18+S19;
              len=Mar.length();
              prtStrl(Mar);
           }
     }
     private void prtStrl(String Strl)
     {
          iPrintLine=iPrintLine+1;
          try
          {
                   if(iPrintLine >=70)
                   {
                        ipagec= ipagec+1;
                        iPrintLine=1;
                        prtStrl(common.Replicate("-",len));
                        FW.write(""+"\n");
                        PrintMainHead();
                        prtStrl(common.Replicate("-",len));
                   }
                  FW.write(Strl+"\n");
          }
          catch(Exception e)
          {
              System.out.println(e);
          }
     } 
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               ipagec=1;
               iPrintLine=0;
               setDataIntoVector();
               setRowData();
               BPrint.setEnabled(true);
               try
               {
                  getContentPane().remove(tabreport);
               }
               catch(Exception ex){}

               try
               {
                  tabreport = new TabReport(RowData,ColumnData,ColumnType);
                  getContentPane().add(tabreport,BorderLayout.CENTER);
                  setSelected(true);
                  DeskTop.repaint();
                  DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
               }
          }
     }

     public void setDataIntoVector()
     {
           VGrnNo       = new Vector();
           VGrnDate     = new Vector();
           VSupName     = new Vector();
           VOrdNo       = new Vector();
           VGno         = new Vector();
           VGDate       = new Vector();
           VDcNo        = new Vector();
           VDcDate      = new Vector();
           VINo         = new Vector();
           VIDate       = new Vector();
           VGrnName     = new Vector();
           VIQty        = new Vector();
           VRQty        = new Vector();
           VIRate       = new Vector();
           VIAmt        = new Vector();
           VAccQty      = new Vector();
           VGrnDeptName = new Vector();
           VGrnCataName = new Vector();
           VGrnUnitName = new Vector();
           VStatus      = new Vector();
           VSPJNo       = new Vector();
           VId          = new Vector();

           SStDate = TopPanel.TStDate.TDay.getText()+"."+TopPanel.TStDate.TMonth.getText()+"."+TopPanel.TStDate.TYear.getText();
           SEnDate = TopPanel.TEnDate.TDay.getText()+"."+TopPanel.TEnDate.TMonth.getText()+"."+TopPanel.TEnDate.TYear.getText();
           String StDate  = TopPanel.TStDate.TYear.getText()+TopPanel.TStDate.TMonth.getText()+TopPanel.TStDate.TDay.getText();
           String EnDate  = TopPanel.TEnDate.TYear.getText()+TopPanel.TEnDate.TMonth.getText()+TopPanel.TEnDate.TDay.getText();

           String QString = getQString(StDate,EnDate);
           try
           {
              if(theconnect==null)
              {
                   connect=ORAConnection.getORAConnection();
                   theconnect=connect.getConnection();
              }
              Statement stat   = theconnect.createStatement();
              ResultSet res  = stat.executeQuery(QString);
              while (res.next())
              {
                     String str1  = res.getString(1);  
                     String str2  = res.getString(2);
                     String str4  = res.getString(3);
                     String str5  = res.getString(4);
                     String str6  = res.getString(5);
                     String str7  = res.getString(6);
                     String str8  = res.getString(7);
                     String str9  = res.getString(8);
                     String str10 = res.getString(9);
                     String str11 = res.getString(10);
                     String str13 = res.getString(11);
                     String str14 = res.getString(12);
                     String str15 = res.getString(13);
                     String str16 = res.getString(14);
                     String str17 = res.getString(15);
                     String str18 = res.getString(16);
                     String str19 = res.getString(17);
                     String str20 = res.getString(18);
                     String str21 = res.getString(19);
                     String str22 = res.getString(20);
                     String str23 = res.getString(21);
                                                    
                     VGrnNo       .addElement(str1);
                     VGrnDate     .addElement(common.parseDate(str2));
                     VSupName     .addElement(str4);
                     VOrdNo       .addElement(str5);
                     VGno         .addElement(str6);
                     VGDate       .addElement(common.parseDate(str7));
                     VDcNo        .addElement(common.parseNull(str8));
                     VDcDate      .addElement(common.parseDate(str9));
                     VINo         .addElement(common.parseNull(str10));
                     VIDate       .addElement(common.parseDate(str11));
                     VGrnName     .addElement(str13);
                     VIQty        .addElement(str14);
                     VRQty        .addElement(str15);
                     VIRate       .addElement(str16);
                     VIAmt        .addElement(str17);
                     VAccQty      .addElement(str18);
                     VGrnDeptName .addElement(common.parseNull(str19));
                     VGrnCataName .addElement(common.parseNull(str20));
                     VGrnUnitName .addElement(common.parseNull(str21));
                     VSPJNo       .addElement(str22);
                     VStatus      .addElement(" ");
                     VId          .addElement(str23);
              }
              res.close();
              stat.close();
           }
           catch(Exception ex)
           {
               try
               {
                    FileWriter FW = new FileWriter(common.getPrintPath()+"q.txt");
                    FW.write(QString);
                    FW.close();
               }
               catch(Exception e){}
               System.out.println(ex);
           }
     }
     public void setRowData()
     {
         RowData     = new Object[VGrnNo.size()][ColumnData.length];
         for(int i=0;i<VGrnNo.size();i++)
         {
               RowData[i][0]  = (String)VGrnNo.elementAt(i);
               RowData[i][1]  = (String)VGrnDate.elementAt(i);
               RowData[i][2]  = (String)VSupName.elementAt(i);
               RowData[i][3]  = (String)VOrdNo.elementAt(i);
               RowData[i][4]  = (String)VGno.elementAt(i);
               RowData[i][5]  = (String)VGDate.elementAt(i);
               RowData[i][6]  = (String)VDcNo.elementAt(i);
               RowData[i][7]  = (String)VDcDate.elementAt(i);
               RowData[i][8]  = (String)VINo.elementAt(i);
               RowData[i][9]  = (String)VIDate.elementAt(i);
               RowData[i][10] = (String)VGrnName.elementAt(i);
               RowData[i][11] = (String)VIQty.elementAt(i);
               RowData[i][12] = (String)VRQty.elementAt(i);
               RowData[i][13] = (String)VIRate.elementAt(i);
               RowData[i][14] = (String)VIAmt.elementAt(i);
               RowData[i][15] = (String)VAccQty.elementAt(i);
               RowData[i][16] = (String)VGrnDeptName.elementAt(i);
               RowData[i][17] = (String)VGrnCataName.elementAt(i);
               RowData[i][18] = (String)VGrnUnitName.elementAt(i);
               RowData[i][19] = (String)VStatus.elementAt(i);
        }
     }

     public String getQString(String StDate,String EnDate)
     {
          String QString  = "";

          if(TopPanel.JRPeriod.isSelected())
          {                 
               QString  = " SELECT WorkGRN.GrnNo, WorkGRN.GrnDate, "+
                          " "+SSupTable+".Name, WorkGRN.OrderNo, "+
                          " WorkGRN.GateInNo, WorkGRN.GateInDate, "+
                          " WorkGRN.DcNo, WorkGRN.DcDate, "+
                          " WorkGRN.InvNo, WorkGRN.InvDate, "+
                          " WorkGRN.Descript, WorkGRN.InvQty, "+
                          " WorkGRN.MillQty, WorkGRN.InvRate, "+
                          " WorkGRN.InvAmount, WorkGRN.Qty, "+
                          " Dept.Dept_Name, Cata.Group_Name, "+
                          " Unit.Unit_Name, WorkGRN.SPJNo, WorkGRN.Id "+
                          " FROM (((WorkGRN INNER JOIN "+SSupTable+" ON WorkGRN.Sup_Code = "+SSupTable+".Ac_Code) "+
                          " INNER JOIN Dept ON WorkGRN.Dept_Code = Dept.Dept_code) "+
                          " INNER JOIN Cata ON WorkGRN.Group_Code = Cata.Group_Code) "+
                          " INNER JOIN Unit ON WorkGRN.Unit_Code = Unit.Unit_Code "+
                          " Where WorkGRN.MillCode="+iMillCode+
                          " And WorkGRN.GrnDate >= '"+StDate+"' and WorkGRN.GrnDate <='"+EnDate+"'";
          }
          else
          {
               QString  = " SELECT WorkGRN.GrnNo, WorkGRN.GrnDate, "+
                          " "+SSupTable+".Name, WorkGRN.OrderNo, "+
                          " WorkGRN.GateInNo, WorkGRN.GateInDate, "+
                          " WorkGRN.DcNo, WorkGRN.DcDate, "+
                          " WorkGRN.InvNo, WorkGRN.InvDate, "+
                          " WorkGRN.Descript, WorkGRN.InvQty, "+
                          " WorkGRN.MillQty, WorkGRN.InvRate, "+
                          " WorkGRN.InvAmount, WorkGRN.Qty, "+
                          " Dept.Dept_Name, Cata.Group_Name, "+
                          " Unit.Unit_Name, WorkGRN.SPJNo, WorkGRN.Id "+
                          " FROM (((WorkGRN INNER JOIN "+SSupTable+" ON WorkGRN.Sup_Code = "+SSupTable+".Ac_Code) "+
                          " INNER JOIN Dept ON WorkGRN.Dept_Code = Dept.Dept_code) "+
                          " INNER JOIN Cata ON WorkGRN.Group_Code = Cata.Group_Code) "+
                          " INNER JOIN Unit ON WorkGRN.Unit_Code = Unit.Unit_Code "+
                          " Where WorkGRN.MillCode="+iMillCode+
                          " And WorkGRN.GRNNo >="+TopPanel.TStNo.getText()+" and WorkGRN.GrnNo <= "+TopPanel.TEnNo.getText();
          }
          if(TopPanel.JRSeleUnit.isSelected())
          {
               SUnit   = "Unit.Unit_Code = "+(String)TopPanel.VUnitCode.elementAt(TopPanel.JCUnit.getSelectedIndex());
               QString = QString +" and "+SUnit;
          }
          if(TopPanel.JRSeleDept.isSelected())
          {
               SDept   = "Dept.Dept_Code = "+(String)TopPanel.VDeptCode.elementAt(TopPanel.JCDept.getSelectedIndex());
               QString = QString +" and "+SDept;
          }
          if(TopPanel.JRSeleGroup.isSelected())
          {
               SGroup  = "Cata.Group_Code = "+(String)TopPanel.VGroupCode.elementAt(TopPanel.JCGroup.getSelectedIndex());
               QString = QString +" and "+SGroup;
        
          }
          if(TopPanel.JRSeleSup.isSelected())
          {
               SSupplier= ""+SSupTable+".Ac_Code= '"+(String)TopPanel.VSupCode.elementAt(TopPanel.JCSup.getSelectedIndex())+"'";
               QString = QString +" and "+SSupplier;
          }
          if(TopPanel.JRSeleList.isSelected())
          {
               iList   = TopPanel.JCList.getSelectedIndex();
               if(iList==0)
               {
                    QString = QString +" and  Len(trim(WorkGRN.InvNo)) > 0 ";
               }
               if(iList==1)
               {
                    QString = QString +" and  Len(trim(WorkGRN.InvNo)) = 0 ";
               }

          }

          if(TopPanel.JCOrder.getSelectedIndex() == 0)
               QString = QString+" Order By WorkGRN.GrnNo,WorkGRN.GrnDate";
          if(TopPanel.JCOrder.getSelectedIndex() == 1)
               QString = QString+" Order By WorkGRN.Descript,WorkGRN.GrnDate";
          if(TopPanel.JCOrder.getSelectedIndex() == 2)
               QString = QString+" Order By "+SSupTable+".Name,WorkGRN.GrnDate";
          if(TopPanel.JCOrder.getSelectedIndex() == 3)
               QString = QString+" Order By Dept.Dept_Name,WorkGRN.GrnDate";
          if(TopPanel.JCOrder.getSelectedIndex() == 4)
               QString = QString+" Order By Cata.Group_Name,WorkGRN.GrnDate";
          if(TopPanel.JCOrder.getSelectedIndex() == 5)
               QString = QString+" Order By Unit.Unit_Name,WorkGRN.GrnDate";

          return QString;
     }
}

