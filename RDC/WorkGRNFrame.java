package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkGRNFrame extends JInternalFrame
{
     
     JPanel         MiddlePanel;
     JTabbedPane    JTP;
     TabReport      tabreport1;

     Object RowData1[][];
     String ColumnData1[] = {"Supplier Name","Order No","Date"};
     String ColumnType1[] = {"S"            ,"N"       ,"S"   };

     Common common   = new Common();
     ORAConnection connect;
     Connection theconnect;

     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     int            iUserCode;
     int            iMillCode;
     String         SSupTable,SYearCode;

     Vector         VCode,VName;

     Vector VOrderNo1,VOrderDate1,VSupName1,VSupCode1;
     String SGINo,SGIDate;

     WorkGRNFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable,String SYearCode)
     {
          super("Pending Work Orders");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     public void createComponents()
     {
          MiddlePanel  = new JPanel();
          JTP          = new JTabbedPane();
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
          MiddlePanel.setLayout(new BorderLayout());
     }

     public void addComponents()
     {
          try
          {
               JTP.removeAll();
               MiddlePanel.removeAll();
               getContentPane().remove(MiddlePanel);
          }
          catch(Exception ex){}

          getContentPane().add("Center",MiddlePanel);
          MiddlePanel.add("Center",JTP);

          setDataIntoVector();
          setRowData();

          try
          {
               tabreport1 = new TabReport(RowData1,ColumnData1,ColumnType1);
               tabreport1.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
               tabreport1.ReportTable.addKeyListener(new KeyList1(this));

               JTP.addTab("Pending Work Orders",tabreport1);

               setSelected(true);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void addListeners()
     {

     }

     public void setDataIntoVector()
     {
          
          String QString1 = " SELECT Distinct WorkOrder.OrderNo,WorkOrder.OrderDate, "+
                            " "+SSupTable+".Name,WorkOrder.Sup_Code "+
                            " From WorkOrder Inner Join "+SSupTable+" on "+
                            " "+SSupTable+".Ac_Code = WorkOrder.Sup_Code "+
                            " And WorkOrder.MillCode="+iMillCode+
                            " And WorkOrder.EntryStatus>0 "+
                            " And WorkOrder.InvQty < WorkOrder.Qty "+
                            " Order By 3,2 ";

          VOrderNo1      = new Vector();
          VOrderDate1    = new Vector();
          VSupName1      = new Vector();
          VSupCode1      = new Vector();

          try
          {
               if(theconnect==null)
               {
                     connect=ORAConnection.getORAConnection();
                     theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res  = stat.executeQuery(QString1);

               while (res.next())
               {
                    VOrderNo1      .addElement(res.getString(1));
                    VOrderDate1    .addElement(common.parseDate(res.getString(2)));
                    VSupName1      .addElement(res.getString(3));
                    VSupCode1      .addElement(res.getString(4));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
          RowData1     = new Object[VOrderNo1.size()][ColumnData1.length];

          for(int i=0;i<VOrderNo1.size();i++)
          {
               RowData1[i][0]  = (String)VSupName1.elementAt(i);
               RowData1[i][1]  = (String)VOrderNo1.elementAt(i);
               RowData1[i][2]  = (String)VOrderDate1.elementAt(i);
          }
     }

     public class KeyList1 extends KeyAdapter
     {
          WorkGRNFrame workgrnframe;
          public KeyList1(WorkGRNFrame workgrnframe)
          {
               this.workgrnframe = workgrnframe;
          }
          public void keyPressed(KeyEvent ke)
          {
               if (ke.getKeyCode()==10)
               {
                    JTable theTable = (JTable)ke.getSource();
                    int i           = theTable.getSelectedRow();
                    String SIndex   = String.valueOf(i);
                    String SSupCode = (String)VSupCode1.elementAt(i);
                    String SOrderNo= (String)VOrderNo1.elementAt(i);

                    WorkGRNCollectionFrame workgrncollectionframe = new WorkGRNCollectionFrame(DeskTop,SPanel,SIndex,theTable,SSupCode,iUserCode,iMillCode,workgrnframe,SYearCode, SOrderNo );

                    DeskTop.add(workgrncollectionframe);

                    workgrncollectionframe.show();
                    workgrncollectionframe.moveToFront();

                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
          }
     }
}
