package RDC;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkGRNMiddlePanel extends JTabbedPane
{
         DirectGRNInvMiddlePanel MiddlePanel;
         Object RowData[][],RD[][];
         Object SlData[];

         String ColumnData[] = {"Description","WO Desc","Order No","Pending Qty","Inv/DC Qty","Recd Qty","Rate","Disc (%)","Cenvat (%)","Tax (%)","Surcharge (%)","Basic","Disc (Rs)","Vat (Rs)","Tax (Rs)","Surcharge (Rs)","Net (Rs)","Department","Group","Unit"};
         String ColumnType[] = {"S","S","S","N","B","B","N","N","N","N","N","N","N","N","N","N","N","B","B","B"};

         JLayeredPane DeskTop;
         String SSupCode="";
         int iMillCode;
         String SYearCode,SOrderNo;

         Vector VGName,VGGrnNo,VGOrderNo;
         Vector VGPendQty,VGRate,VGDiscPer,VGVatPer,VGSTPer,VGSurPer;
         Vector VGDept,VGDeptCode,VGGroup,VGGroupCode,VGUnit,VGUnitCode;
         Vector VId,VGOrdQty,VGOrdDate,VSlNo,VWODesc,VGRDCNo,VGRDCSlNo,VMemoUserCode;
         Vector VGGINo,VGGIDate;

         Common common   = new Common();
         ORAConnection connect;
         Connection theconnect;

         public WorkGRNMiddlePanel(JLayeredPane DeskTop,String SSupCode,int iMillCode,String SYearCode,String SOrderNo)
         {
              this.DeskTop   = DeskTop;
              this.SSupCode  = SSupCode;
              this.iMillCode = iMillCode;
              this.SYearCode = SYearCode;
              this.SOrderNo   = SOrderNo;
         }
         public void createComponents()
         {
            setVectorData();
            setRowData();
            try
            {
                  MiddlePanel = new DirectGRNInvMiddlePanel(DeskTop,RowData,ColumnData,ColumnType,iMillCode);
                  addTab("Materials Pending @ Work Order",MiddlePanel);
                  MiddlePanel.setBorder(new TitledBorder("Pending Work Orders"));
            }
            catch(Exception ex)
            {
                  System.out.println("GRNInvMiddlePanel: "+ex);
            }
         }
        public void setVectorData()
        {
            String QS1 = " SELECT WorkOrder.Descript,WorkOrder.OrderNo, "+
                         " WorkOrder.Qty-WorkOrder.InvQty AS Pending, "+
                         " WorkOrder.Rate, WorkOrder.DiscPer, "+
                         " WorkOrder.CenVatPer, WorkOrder.TaxPer, "+
                         " WorkOrder.SurPer, Dept.Dept_Name, "+
                         " WorkOrder.Dept_Code, Cata.Group_Name, "+
                         " WorkOrder.Group_Code, Unit.Unit_Name, "+
                         " WorkOrder.Unit_Code,WorkOrder.Id, "+
                         " WorkOrder.Qty,WorkOrder.OrderDate,WorkOrder.SlNo,WorkOrder.WODesc, "+
                         " WorkOrder.RDCNo,WorkOrder.RDCSlNo,WorkOrder.MemoAuthUserCode "+
                         " FROM WorkOrder INNER JOIN Dept ON WorkOrder.Dept_Code = Dept.Dept_code "+
                         " And WorkOrder.EntryStatus>0 and workorder.orderno='"+SOrderNo+"' And WorkOrder.MillCode="+iMillCode+
                         " and WorkOrder.Sup_Code = '"+SSupCode+"' And WorkOrder.InvQty < WorkOrder.Qty "+
                         " INNER JOIN Cata ON WorkOrder.Group_Code = Cata.Group_Code "+
                         " INNER JOIN Unit ON WorkOrder.Unit_Code = Unit.Unit_Code "+
                         " Order By 1,2";

            VGName        = new Vector(); 
            VGGrnNo       = new Vector();
            VGOrderNo     = new Vector();
            VGPendQty     = new Vector();
            VGRate        = new Vector();
            VGDiscPer     = new Vector();
            VGVatPer      = new Vector();
            VGSTPer       = new Vector();
            VGSurPer      = new Vector();
            VGDept        = new Vector();
            VGDeptCode    = new Vector(); 
            VGGroup       = new Vector();
            VGGroupCode   = new Vector(); 
            VGUnit        = new Vector();
            VGUnitCode    = new Vector();
            VGOrdQty      = new Vector();
            VGOrdDate     = new Vector();
            VId           = new Vector();
            VSlNo         = new Vector();
            VWODesc       = new Vector();
            VGRDCNo       = new Vector();
            VGRDCSlNo     = new Vector();
            VGGINo        = new Vector();
            VGGIDate      = new Vector();
            VMemoUserCode = new Vector();

            try
            {
                   if(theconnect==null)
                   {
                         connect=ORAConnection.getORAConnection();
                         theconnect=connect.getConnection();
                   }
                   Statement stat  = theconnect.createStatement();
                   ResultSet res1 = stat.executeQuery(QS1);
//			 System.out.println("WorkOrder Qry-->"+QS1);
                   while(res1.next())
                   {
                           VGName       .addElement(res1.getString(1)); 
                           VGGrnNo      .addElement("0");
                           VGOrderNo    .addElement(res1.getString(2));
                           VGPendQty    .addElement(res1.getString(3));
                           VGRate       .addElement(res1.getString(4));
                           VGDiscPer    .addElement(res1.getString(5));
                           VGVatPer     .addElement(res1.getString(6));
                           VGSTPer      .addElement(res1.getString(7));
                           VGSurPer     .addElement(res1.getString(8));
                           VGDept       .addElement(res1.getString(9));
                           VGDeptCode   .addElement(res1.getString(10)); 
                           VGGroup      .addElement(res1.getString(11));
                           VGGroupCode  .addElement(res1.getString(12)); 
                           VGUnit       .addElement(res1.getString(13));
                           VGUnitCode   .addElement(res1.getString(14));
                           VId          .addElement(res1.getString(15));
                           VGOrdQty     .addElement(res1.getString(16));
                           VGOrdDate    .addElement(res1.getString(17));
                           VSlNo        .addElement(res1.getString(18));
                           VWODesc      .addElement(common.parseNull(res1.getString(19)));
                           VGRDCNo      .addElement(common.parseNull(res1.getString(20)));
                           VGRDCSlNo    .addElement(common.parseNull(res1.getString(21)));
                           VMemoUserCode.addElement(common.parseNull(res1.getString(22)));
                           VGGINo       .addElement("");
                           VGGIDate     .addElement("");
                   }
                   res1.close();
                   stat.close();
                   setGRNNo();
                   setGateNo();
            }
            catch(Exception ex)
            {
                System.out.println(ex);
            }
        }
        public void setGRNNo()
        {
               String SGNo = "";

               String QS = "";

               QS = " Select (MaxNo+1) From Config"+iMillCode+""+SYearCode+" "+
                    " Where ID=12 ";

               SGNo   = common.getID(QS);
               SGNo   = String.valueOf(common.toInt(SGNo));
               for(int i=0;i<VGGrnNo.size();i++)
                    VGGrnNo.setElementAt(SGNo,i);    
        }

        public void setGateNo()
        {
             try
             {
                  if(theconnect==null)
                  {
                        connect=ORAConnection.getORAConnection();
                        theconnect=connect.getConnection();
                  }
                  Statement stat  = theconnect.createStatement();

                  for(int i=0;i<VGName.size();i++)
                  {
                       String SOrderNo = (String)VGOrderNo.elementAt(i);
                       String SRDCNo   = (String)VGRDCNo.elementAt(i);
                       String SRDCSlNo = (String)VGRDCSlNo.elementAt(i);

                       String QS = "";

                       if(common.toInt(SRDCNo)>0)
                       {
                            QS = " Select GINo,GIDate from GateInwardRDC "+
                                 " Where RDCNo="+SRDCNo+" and RDCSlNo="+SRDCSlNo+" and MillCode="+iMillCode;
                       }
                       else
                       {
                            QS = " Select GINo,GIDate from GateInwardRDC "+
                                 " Where Id = (Select Max(Id) from GateInwardRDC Where OrderNo="+SOrderNo+" and MillCode="+iMillCode+")";
                       }

                       ResultSet res = stat.executeQuery(QS);
                       while(res.next())
                       {
                            VGGINo.setElementAt(res.getString(1),i);
                            VGGIDate.setElementAt(res.getString(2),i); 
                       }
                       res.close();
                  }
                  stat.close();
             }
             catch(Exception ex)
             {
                  System.out.println(ex);
             }
        }

        public boolean setRowData()
        {
            RowData   = new Object[VGName.size()][ColumnData.length];
            SlData    = new Object[VSlNo.size()];

            for(int i=0;i<VGName.size();i++)
            {
                      SlData[i]      = common.parseNull((String)VSlNo.elementAt(i));
                      RowData[i][0]  = (String)VGName    .elementAt(i);
                      RowData[i][1]  = (String)VWODesc   .elementAt(i);
                      RowData[i][2]  = (String)VGOrderNo .elementAt(i)+"/"+common.parseDate((String)VGOrdDate.elementAt(i));
                      RowData[i][3]  = (String)VGPendQty .elementAt(i);
                      RowData[i][4]  = "";
                      RowData[i][5]  = "";
                      RowData[i][6]  = (String)VGRate    .elementAt(i);
                      RowData[i][7]  = (String)VGDiscPer .elementAt(i);
                      RowData[i][8]  = (String)VGVatPer  .elementAt(i);
                      RowData[i][9]  = (String)VGSTPer   .elementAt(i);
                      RowData[i][10] = (String)VGSurPer  .elementAt(i);
                      RowData[i][11] = "0";
                      RowData[i][12] = "0";
                      RowData[i][13] = "0";
                      RowData[i][14] = "0";
                      RowData[i][15] = "0";
                      RowData[i][16] = "0";
                      RowData[i][17] = (String)VGDept    .elementAt(i);
                      RowData[i][18] = (String)VGGroup   .elementAt(i);
                      RowData[i][19] = (String)VGUnit    .elementAt(i);
            }
            return true;
        }
        public String getOrderNo(int i)
        {
               return (String)VGOrderNo.elementAt(i);
        }
        public String getGRNNo(int i)
        {
               return (String)VGGrnNo.elementAt(i);
        }
        public String getOrdQty(int i)
        {
               return (String)VGOrdQty.elementAt(i);
        }
        public String getGINo(int i)
        {
               return (String)VGGINo.elementAt(i);
        }
        public String getGIDate(int i)
        {
               return (String)VGGIDate.elementAt(i);
        }
        public String getRDCNo(int i)
        {
               return (String)VGRDCNo.elementAt(i);
        }
        public String getMemoUserCode(int i)
        {
               return (String)VMemoUserCode.elementAt(i);
        }

        public String getDesc(int i)
        {
               return (String)VWODesc.elementAt(i);
        }

}
