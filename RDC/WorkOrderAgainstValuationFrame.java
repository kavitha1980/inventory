package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkOrderAgainstValuationFrame extends JInternalFrame
{
      JPanel     TopPanel;
      JPanel     MiddlePanel;
      JPanel     BottomPanel;
 
      JButton    BApply,BOk;
      MyComboBox JCSupplier;
      JLabel     LNet;

      JLayeredPane DeskTop;
      StatusPanel SPanel;
      int iMillCode,iUserCode;
      String SSupTable,SYearCode;

      Common common = new Common();

      Connection     theConnection = null;

      TabReport tabreport;

      Vector VSupName,VSupCode;
      Vector VRDCNo,VDescript,VGINo,VGIDate,VRecQty,VAccQty,VRate,VNetValue,VId;
      Vector VSeleId;

      String ColumnData[] = {"RDC No","Descript","Recd Date","Recd Qty","Accepted Qty","Rate","Value","Select"};
      String ColumnType[] = {"N"     ,"S"       ,"S"        ,"N"       ,"N"           ,"N"   ,"N"    ,"B"     };
      Object RowData[][];

      boolean bComflag = true;

      WorkOrderAgainstValuationFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable,String SYearCode)
      {
          super("WorkOrder Against RDC Valuation");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;


          ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                          theConnection =  oraConnection.getConnection();

          getSupplier();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
      }

      public void createComponents()
      {
          TopPanel    = new JPanel();
          MiddlePanel = new JPanel();
          BottomPanel = new JPanel();
 
          JCSupplier  = new MyComboBox(VSupName);
          LNet        = new JLabel("");
          LNet.setFont(new Font("monospaced", Font.BOLD, 14));
          

          BApply   = new JButton("Apply");
          BOk      = new JButton("Pass WorkOrder");
 
          BOk.setEnabled(false);
          if(VSupName.size()>0)
          {
               BApply.setEnabled(true);
          }
          else
          {
               BApply.setEnabled(false);
          }
      }

      public void setLayouts()
      {
          TopPanel.setLayout(new GridLayout(2,3));
          MiddlePanel.setLayout(new BorderLayout());
          //BottomPanel.setLayout(new GridLayout(1,3));
            
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
      }
                                                                        
      public void addComponents()
      {
          TopPanel.add(new JLabel(" Select Supplier "));
          TopPanel.add(JCSupplier);
          TopPanel.add(BApply);
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));

          BottomPanel.add(BOk);
          BottomPanel.add(new JLabel("     Total Value     "));
          BottomPanel.add(LNet);
 
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
      }
      public void addListeners()
      {
          BApply.addActionListener(new ApplyList());
          BOk.addActionListener(new ActList());
      }
      public class ApplyList implements ActionListener
      {
           public void actionPerformed(ActionEvent ae)
           {
                setDataIntoVector();
                setVectorIntoRowData();
                try
                {
                      MiddlePanel.remove(tabreport);
                }
                catch(Exception ex){}
 
                tabreport = new TabReport(RowData,ColumnData,ColumnType);
                tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
                MiddlePanel.add("Center",tabreport);
 
                if(RowData.length>0)
                {
                     BOk.setEnabled(true);
                     BApply.setEnabled(false);
                     JCSupplier.setEnabled(false);
                     setTotalValue();
                }
                else
                {
                     BOk.setEnabled(false);
                     BApply.setEnabled(true);
                     JCSupplier.setEnabled(true);
                }
 
                MiddlePanel.updateUI();

                tabreport.ReportTable.addKeyListener(new KeyList1());
                tabreport.ReportTable.addMouseListener(new MouseList1());
           }
      }

      public class ActList implements ActionListener
      {
           public void actionPerformed(ActionEvent ae)
           {
                if(validData())
                {
                     String SValue   = LNet.getText();
                     String SSupCode = (String)VSupCode.elementAt(JCSupplier.getSelectedIndex());
                     String SSupName = (String)VSupName.elementAt(JCSupplier.getSelectedIndex());
                     ValueWorkOrderFrame workorderframe = new ValueWorkOrderFrame(DeskTop,SPanel,false,iUserCode,iMillCode,SSupTable,SYearCode,SValue,SSupCode,SSupName,VSeleId);
                     //workorderframe.fillData(SOrderNo);
                     DeskTop.add(workorderframe);
                     workorderframe.show();
                     workorderframe.moveToFront();
                     removeHelpFrame();
                     DeskTop.repaint();
                     DeskTop.updateUI();
                }
           }
      }

      public void removeHelpFrame()
      {
            try
            {
                  DeskTop.remove(this);
                  DeskTop.repaint();
                  DeskTop.updateUI();
            }
            catch(Exception ex) { }
      }

      private class KeyList1 extends KeyAdapter
      {
          public void keyPressed(KeyEvent ke)
          {
             if(ke.getKeyCode()==KeyEvent.VK_SPACE)
             {
                 setTotalValue();
             }
          }
      }
 
      public class MouseList1 extends MouseAdapter
      {
           public void mouseReleased(MouseEvent me)
           {
                if(me.getClickCount()==1)
                {
                     setTotalValue();
                }
           }
      }

      private void setTotalValue()
      {
           double dTValue = 0;
           VSeleId = new Vector();

           for(int i=0;i<RowData.length;i++)
           {
                Boolean Bselected = (Boolean)RowData[i][7];
                if(Bselected.booleanValue())
                {
                     double dValue = common.toDouble((String)RowData[i][6]);

                     dTValue = dTValue + dValue;

                     VSeleId.addElement((String)VId.elementAt(i));
                }
           }
           LNet.setText(common.getRound(dTValue,2));
           LNet.setForeground(Color.red);
      }

      private void setDataIntoVector()
      {
            VRDCNo         = new Vector();
            VDescript      = new Vector();
            VGINo          = new Vector();
            VGIDate        = new Vector();
            VRecQty        = new Vector();
            VAccQty        = new Vector();
            VRate          = new Vector();
            VNetValue      = new Vector();
            VId            = new Vector();

            String QS  = "";

            String SSupCode = (String)VSupCode.elementAt(JCSupplier.getSelectedIndex());

            QS  = " SELECT GateInwardRDC.RDCNo, GateInwardRDC.Descript, GateInwardRDC.GINo, "+
                  " GateInwardRDC.GIDate, GateInwardRDC.RecQty, GateInwardRDC.AccQty, "+
                  " GateInwardRDC.Rate,GateInwardRDC.NetValue,GateInwardRDC.Id "+
                  " FROM GateInwardRDC "+
                  " Where GateInwardRDC.Sup_Code='"+SSupCode+"' And GateInwardRDC.Valuation=1 And GateInwardRDC.OrderStatus=0 "+
                  " And GateInwardRDC.AccQty>0 And GateInwardRDC.MillCode="+iMillCode+
                  " ORDER BY GateInwardRDC.GIDate,GateInwardRDC.RDCNo,GateInwardRDC.Descript ";

            try
            {
                  Statement stat = theConnection.createStatement();
                  ResultSet theResult = stat.executeQuery(QS);
                  while(theResult.next())
                  {
                        VRDCNo         .addElement(theResult.getString(1));
                        VDescript      .addElement(theResult.getString(2));
                        VGINo          .addElement(theResult.getString(3));
                        VGIDate        .addElement(theResult.getString(4));
                        VRecQty        .addElement(theResult.getString(5));
                        VAccQty        .addElement(theResult.getString(6));
                        VRate          .addElement(theResult.getString(7));
                        VNetValue      .addElement(theResult.getString(8));
                        VId            .addElement(theResult.getString(9));
                  }
                  theResult.close();
                  stat.close();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      private void setVectorIntoRowData()
      {
            RowData = new Object[VRDCNo.size()][ColumnData.length];
            for(int i=0;i<VRDCNo.size();i++)
            {
                  RowData[i][0] = (String)VRDCNo.elementAt(i);
                  RowData[i][1] = (String)VDescript.elementAt(i);
                  RowData[i][2] = common.parseDate((String)VGIDate.elementAt(i));
                  RowData[i][3] = (String)VRecQty.elementAt(i);
                  RowData[i][4] = (String)VAccQty.elementAt(i);
                  RowData[i][5] = (String)VRate.elementAt(i);
                  RowData[i][6] = (String)VNetValue.elementAt(i);

                  RowData[i][7] = new Boolean(false);
            }
      }

      private boolean validData()
      {
           int iCount=0;

           for(int i=0;i<RowData.length;i++)
           {
                Boolean Bselected = (Boolean)RowData[i][7];

                if(!Bselected.booleanValue())
                     continue;

                double dValue = common.toDouble((String)RowData[i][6]);

                if(dValue<=0)
                {
                     JOptionPane.showMessageDialog(null,"Value Not Entered for Row - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                     return false;
                }

                iCount++;
           }

           if(iCount<=0)
           {
                JOptionPane.showMessageDialog(null,"No Entries Made","Error",JOptionPane.ERROR_MESSAGE);
                return false;
           }
           return true;
      }

      private void getACommit()
      {
           try
           {
                if(bComflag)
                {
                     theConnection  . commit();
                     JOptionPane    . showMessageDialog(null,"The Entered Data is Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                     System         . out.println("Commit");
                }
                else
                {
                     theConnection  . rollback();
                     JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                     System         . out.println("RollBack");
                }
                theConnection   . setAutoCommit(true);
           }catch(Exception ex)
           {
                ex.printStackTrace();
           }
      }

      private void getSupplier()
      {
            VSupCode       = new Vector();
            VSupName       = new Vector();

            String QS  = "";

            QS  = " SELECT GateInwardRDC.Sup_Code,"+SSupTable+".NAME "+
                  " FROM GateInwardRDC INNER JOIN "+SSupTable+" ON GateInwardRDC.Sup_Code = "+SSupTable+".AC_CODE "+
                  " And "+SSupTable+".RDCFlag=1 And GateInwardRDC.Valuation=1 And GateInwardRDC.AccQty>0 "+
                  " And GateInwardRDC.OrderStatus=0 And GateInwardRDC.MillCode="+iMillCode+
                  " Group by GateInwardRDC.Sup_Code,"+SSupTable+".NAME ORDER BY "+SSupTable+".NAME ";

            try
            {
                  Statement stat = theConnection.createStatement();
                  ResultSet theResult = stat.executeQuery(QS);
                  while(theResult.next())
                  {
                        VSupCode       .addElement(theResult.getString(1));
                        VSupName       .addElement(theResult.getString(2));
                  }
                  theResult.close();
                  stat.close();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }



}
