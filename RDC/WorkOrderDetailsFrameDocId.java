package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class WorkOrderDetailsFrameDocId extends JInternalFrame
{
     WorkOrderCritPanel    TopPanel;
     JPanel    BottomPanel;

     String    SStDate,SEnDate;
     String    SStNo,SEndNo; 
     JComboBox JCOrder,JCFilter;
     JButton   BApply,BExit;
     JButton BPrint,BDoc;
     FileWriter FW;
     JTextField TFileName;

     DateField TStDate;
     DateField TEnDate;
    
     Object RowData[][];
     String ColumnData[] = {"Order No","Date","Supplier","Description","Dept","Group","Unit","Qty","Rate","Amount","Due Date","Status","DocId","Select"};
     String ColumnType[] = {"N","S","S","S","S","S","S","N","N","N","S","S","E","B"};

     JLayeredPane DeskTop;
     StatusPanel  SPanel;
     String str="";
     String SUnit="";
     String SDept="";
     String SGroup="";
     String SSupplier="";
     String s="";
     int          iUserCode;
     int          iMillCode;
     String       SSupTable,SMillName;

     int          iSelect; 
     int len=0;
     String ReportHead="";
     int iPrintLine=0;
     int ipagec=1;
  
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;
     Connection theConnect;

     Vector VOrdDate,VOrdNo,VSupName,VOrdName,VOrdDeptName,VOrdCataName,VOrdUnitName,VOrdQty,VOrdRate,VOrdNet,VOrdDue,VStatus;

     Vector VSelectedName,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup;

     TabReport tabreport;
	 WorkOrderDetailsFrameDocId workorderdetailsframe ;

     boolean             bflag;
     boolean             bComflag = true;
     
     WorkOrderDetailsFrameDocId(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable,String SMillName)
     {
          super("Work Orders List Details  During a Period");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SMillName = SMillName;

		  getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
	public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnect     =    oraConnection.getConnection();
     }
     public void createComponents()
     {
         TopPanel    	= new WorkOrderCritPanel(iMillCode,SSupTable);
         BottomPanel	= new JPanel();
         BExit    		= new JButton("Exit");
         BPrint      	= new JButton("Print");
		 BDoc           = new JButton("Update DocId");
         TFileName   	= new JTextField(25);
         TFileName		. setText("WorkOrderListDetails.prn");
     }

     public void setLayouts()
     {    
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
     }

     public void addComponents()
     {
          BPrint.setEnabled(false);
          BottomPanel.add(BExit);
          BottomPanel.add(BPrint);
		  BottomPanel.add(BDoc);
          BottomPanel.add(TFileName);

		  getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
         TopPanel	. BApply.addActionListener(new ApplyList(this));
         BExit		. addActionListener(new ExitList());
         BPrint		. addActionListener(new PrintList());
		 BDoc		. addActionListener(new DocUpdtList());
     }
     public class ExitList implements ActionListener
     {
         public void actionPerformed(ActionEvent ae)
         {
               removeHelpFrame();
         }

     }
     public void removeHelpFrame()
     {
         try
         {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
         }
         catch(Exception ex){}
     }
	 public class DocUpdtList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BDoc . setEnabled(false);

               try
               {
                    updateDocIdDetails();
               }
               catch(Exception Ex)
               {
                    Ex.printStackTrace();
                    bComflag = false;
               }

               try
               {
                    if(bComflag)
                    {
                         theConnect     . commit();
                         System         . out.println("Commit");
                         theConnect     . setAutoCommit(true);
                    }
                    else
                    {
                         theConnect     . rollback();
                         System         . out.println("RollBack");
                         theConnect     . setAutoCommit(true);
                     }
               }catch(Exception ex)
               {
                     ex.printStackTrace();
               }
               removeHelpFrame();
          }
     }
	 public void updateDocIdDetails()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);
               Statement stat           =  theConnect.createStatement();
                                        
				for(int i=0; i<VOrdNo.size(); i++)
           		{
             		String SOrderNo  = (String)RowData[i][0];
					String SID = (String)RowData[i][12];
					int iRow = tabreport.ReportTable.getSelectedRow();

					Boolean bValue           = (Boolean)RowData[i][13];
				
					if(bValue.booleanValue())
					{
						System.out.println("SOrderNo :"+SOrderNo);
						System.out.println("SID :"+SID);

						String    QS1 = "Update WorkOrder Set DocId = "+SID+" Where OrderNo = "+SOrderNo+" ";
	                    stat.executeUpdate(QS1);
					}
               	}
				stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("44"+ex);
               ex.printStackTrace();
          }
     }

     public class PrintList implements  ActionListener
     {
       public void actionPerformed(ActionEvent ae)
       {
         PrnFileWrite();
         JOptionPane.showMessageDialog(null,getInfo(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
       }
     }
     public void PrnFileWrite()
     {
           try
           {
             String SFile =TFileName.getText();

             if((SFile.trim()).length()==0)
                SFile = "1.prn";

             FW = new FileWriter(common.getPrintPath()+SFile);
             PrintMainHead();
             PrintHead();
             PrintData();
             prtStrl(common.Replicate("-",len));
             FW.close();
           }
          catch(Exception e)
          {
            System.out.println(e);
          }
    }

     private void PrintMainHead()
     {
             if(TopPanel.bsig==true)
             {
             prtStrl(SMillName);
             prtStrl("WorkOrderListDetails From" +common.Space(3)+common.parseDate(TopPanel.TStDate.toNormal()) +common.Space(3)+ "TO"+common.Space(3) +common.parseDate(TopPanel.TEnDate.toNormal()));
             prtStrl("M\n");
             prtStrl(ReportHead);
             prtStrl("Page:"+ipagec);
             prtStrl("\n");
             }
             else
             {
             prtStrl(SMillName);
             prtStrl("WorkOrderListDetails OrderNo From" +common.Space(3)+common.parseNull(TopPanel.TStNo.getText()) +common.Space(3)+ "TO"+common.Space(3) +common.parseNull(TopPanel.TEnNo.getText()));
             prtStrl(ReportHead);
             prtStrl("Page:"+ipagec);
             prtStrl("\n");
             }
     }
     private void PrintHead()
     {
        String   S1= common.Cad("Order No",15);
        String   S2= common.Cad("Date",15);
        String   S3= common.Pad("Supplier",35);
        String   S4= common.Pad("Description",50);
        String   S5= common.Cad("Dept",20);
        String   S6= common.Cad("Group",25);
        String   S7= common.Cad("Unit",15);
        String   S8= common.Rad("Qty",15);
        String   S9= common.Rad("Rate",15);
        String   S10=common.Rad( "Amount",15);
        String   S11=common.Cad( "DueDate",15);
        String   S12=common.Cad( "Status",15);

        String Mar=S1+S2+S3+S4+S5+S6+S7+S8+S9+S10+S11+S12;
        len=Mar.length();
        prtStrl(common.Replicate("-",len));
        prtStrl(Mar);
        prtStrl(common.Replicate("-",len));
     }
     private void PrintData()
     {
           for(int i=0; i<VOrdNo.size(); i++)
           {
             String Str1  =(String)VOrdNo.elementAt(i);
             String Str2  = (String)VOrdDate.elementAt(i);
             String Str3  =(String)VSupName.elementAt(i);
             String Str4  =(String)VOrdName.elementAt(i);
             String Str5  =(String)VOrdDeptName.elementAt(i);
             String Str6  =(String)VOrdCataName.elementAt(i);
             String Str7  =(String)VOrdUnitName.elementAt(i);
             String Str8  =common.getRound((String)VOrdQty.elementAt(i),3);
             String Str9  =common.getRound((String)VOrdRate.elementAt(i),2);
             String Str10 =common.getRound((String)VOrdNet.elementAt(i),2);
             String Str11 = (String)VOrdDue.elementAt(i);
             String Str12 = (String)VStatus.elementAt(i);

             String   S1 = common.Cad(Str1,15);
             String   S2 = common.Pad(Str2,15);
             String   S3 = common.Pad(Str3,35);
             String   S4 = common.Pad(Str4,50);
             String   S5 = common.Cad(Str5,20);
             String   S6 = common.Cad(Str6,25);
             String   S7 = common.Cad(Str7,15);
             String   S8 = common.Rad(Str8,15);
             String   S9 = common.Rad(Str9,15);
             String   S10= common.Rad(Str10,15);
             String   S11= common.Cad(Str11,15);
             String   S12= common.Cad(Str12,15);

              String Mar=S1+S2+S3+S4+S5+S6+S7+S8+S9+S10+S11+S12;
              prtStrl(Mar);
           }
     }

     private void prtStrl(String Strl)
     {
          iPrintLine=iPrintLine+1;
          try
          {
                   if(iPrintLine >=60)
                   {
                        ipagec= ipagec+1; 
                        iPrintLine=1;
                        prtStrl(common.Replicate("-",len));
                        FW.write(""+"\n");
                        PrintMainHead();
                        PrintHead();
                   }
                  FW.write(Strl+"\n");
          }
          catch(Exception e)
          {
              System.out.println(e);
          }
     }

    public class ApplyList implements ActionListener
     {
         WorkOrderDetailsFrameDocId workorderdetailsframe;

          public ApplyList(WorkOrderDetailsFrameDocId workorderdetailsframe)
          {
               this.workorderdetailsframe = workorderdetailsframe;
          }
          public void actionPerformed(ActionEvent ae)
          {
               setTabReport(workorderdetailsframe);
          }
			
     }
     public void setTabReport(WorkOrderDetailsFrameDocId workorderdetailsframe)
     {
          ReportHead="";
          ipagec =1;
          iPrintLine=0;
          setDataIntoVector();
          setRowData();
          BPrint.setEnabled(true);
          try
          {
             getContentPane().remove(tabreport);
          }
          catch(Exception ex){}

          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             getContentPane().add(tabreport,BorderLayout.CENTER);
			 setSelected(true);

			 tabreport.ReportTable.addKeyListener(new KeyList());
             
             DeskTop.repaint();
             DeskTop.updateUI();

			System.out.println("End Of setTabReport");
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }
     }
     public void setDataIntoVector()
     {
           VOrdDate     = new Vector();
           VOrdNo       = new Vector();
           VSupName     = new Vector();
           VOrdName     = new Vector();
           VOrdDeptName = new Vector();
           VOrdCataName = new Vector();
           VOrdUnitName = new Vector();
           VOrdQty      = new Vector();
           VOrdRate     = new Vector();
           VOrdNet      = new Vector();
           VOrdDue      = new Vector();
           VStatus      = new Vector();

           SStDate = TopPanel.TStDate.toString();
           SEnDate = TopPanel.TEnDate.toString();
           SStNo   =TopPanel.TStNo.getText();
           SEndNo  =TopPanel.TEnNo.getText();
           String StDate  = TopPanel.TStDate.toNormal();
           String EnDate  = TopPanel.TEnDate.toNormal();

           String QString  = " Create table Temp1 as SELECT WorkOrder.OrderNo, WorkOrder.OrderDate, "+
                             " "+SSupTable+".Name, WorkOrder.Descript, WorkOrder.Qty, "+
                             " WorkOrder.Rate, WorkOrder.Net, WorkOrder.DueDate, "+
                             " WorkOrder.InvQty, Dept.Dept_Name, Cata.Group_Name, "+
                             " Unit.Unit_Name, WorkOrder.SlNo, WorkOrder.MillCode "+
                             " FROM (((WorkOrder INNER JOIN "+SSupTable+" ON WorkOrder.Sup_Code="+SSupTable+".Ac_Code) "+
                             " INNER JOIN Dept ON WorkOrder.Dept_Code=Dept.Dept_Code) "+
                             " INNER JOIN Cata ON WorkOrder.Group_Code=Cata.Group_Code) "+
                             " INNER JOIN Unit ON WorkOrder.Unit_Code=Unit.Unit_Code "+
                             " Where WorkOrder.MillCode="+iMillCode+" And WorkOrder.Qty > 0 ";
           try
           {
              if(theconnect==null)
              {
                   connect=ORAConnection.getORAConnection();
                   theconnect=connect.getConnection();
              }
              Statement stat  = theconnect.createStatement();
              try
              {
                  stat.execute("Drop Table Temp1");
              }
              catch(Exception ex){}

              stat.execute(QString);

              QString = getQString(StDate,EnDate);
              ResultSet res  = stat.executeQuery(QString);
              while (res.next())
              {
                 VOrdNo.addElement(res.getString(1));
                 VOrdDate.addElement(common.parseDate(res.getString(2)));
                 VSupName.addElement(res.getString(3));
                 VOrdName.addElement(res.getString(4));
                 VOrdDeptName.addElement(common.parseNull(res.getString(5)));
                 VOrdCataName.addElement(common.parseNull(res.getString(6)));
                 VOrdUnitName.addElement(common.parseNull(res.getString(7)));
                 VOrdQty.addElement(res.getString(8));
                 VOrdRate.addElement(res.getString(9));;
                 VOrdNet.addElement(res.getString(10));
                 VOrdDue.addElement(common.parseDate(res.getString(11)));
                 VStatus.addElement(" ");
              }
              res.close();
              stat.close();
           }
           catch(Exception ex){System.out.println(ex);}
     }
     public void setRowData()
     {
         RowData     = new Object[VOrdDate.size()][ColumnData.length];
         for(int i=0;i<VOrdDate.size();i++)
         {
               RowData[i][0]  = (String)VOrdNo.elementAt(i);
               RowData[i][1]  = (String)VOrdDate.elementAt(i);
               RowData[i][2]  = (String)VSupName.elementAt(i);
               RowData[i][3]  = (String)VOrdName.elementAt(i);
               RowData[i][4]  = (String)VOrdDeptName.elementAt(i);
               RowData[i][5]  = (String)VOrdCataName.elementAt(i);
               RowData[i][6]  = (String)VOrdUnitName.elementAt(i);
               RowData[i][7]  = (String)VOrdQty.elementAt(i);
               RowData[i][8]  = (String)VOrdRate.elementAt(i);
               RowData[i][9]  = (String)VOrdNet.elementAt(i);
               RowData[i][10] = (String)VOrdDue.elementAt(i);
               RowData[i][11] = (String)VStatus.elementAt(i);
			   RowData[i][12] = "";
			   RowData[i][13] = new Boolean(false);
        }  
     }
     public String getQString(String StDate,String EnDate)
     {
          DateField df = new DateField();
          df.setTodayDate();
          String SToday = df.toNormal();
          String QString  = "";

          QString  = " SELECT temp1.OrderNo, temp1.OrderDate, "+
                     " temp1.Name, temp1.Descript, "+
                     " temp1.Dept_Name, temp1.Group_Name, "+
                     " temp1.Unit_Name, temp1.Qty, temp1.Rate, "+
                     " temp1.Net, temp1.DueDate "+
                     " FROM temp1 "+
                     " Where temp1.millcode="+iMillCode; 
         if(TopPanel.bsig==true)
         {
         QString =QString+ " and temp1.orderDate>='"+StDate+"' and temp1.orderDate<='"+EnDate+"'";
         }
         else
         {
         QString = QString+" and temp1.orderNo>= "+SStNo+" and temp1.orderNo<= "+SEndNo+"";
         }
          if(TopPanel.JRSeleUnit.isSelected())
          {
             SUnit= (String)TopPanel.JCUnit.getSelectedItem();
             QString   =QString+" and temp1.Unit_name='"+SUnit+"'";
             ReportHead=ReportHead+"Unit:"+SUnit+" ,";
          }
          if(TopPanel.JRSeleDept.isSelected())
          {
             SDept =(String)TopPanel.JCDept.getSelectedItem();
             QString =QString+" and temp1.Dept_name='"+SDept+"'";
             ReportHead=ReportHead+"Dept:"+SDept+" ,";
          }

          if(TopPanel.JRSeleGroup.isSelected())
          {
             SGroup  =(String)TopPanel.JCGroup.getSelectedItem();
             QString =QString+" and temp1.Group_name='"+SGroup+"'";
             ReportHead=ReportHead+"Group:"+SGroup+" ,";
          }
          if(TopPanel.JRSeleSup.isSelected())
          {
             SSupplier =(String)TopPanel.JCSup.getSelectedItem();
             QString =QString+" and temp1.Name='"+SSupplier+"'";
             ReportHead=ReportHead+"Sup:"+SSupplier+" ,";
          }
          if(TopPanel.JRSeleList.isSelected())
          {
                df = new DateField();
               df.setTodayDate();
                SToday = df.toNormal();
                iSelect = TopPanel.JCList.getSelectedIndex();

               if (iSelect==1)
               {
                     QString = QString+" and temp1.InvQty = 0 and temp1.DueDate <= '"+SToday+"'";
                     ReportHead=ReportHead+"List:OverDue";
               }
               if(iSelect==2)
               {
                   QString = QString+" and temp1.InvQty = 0";
                   ReportHead=ReportHead+"List:AllPendings";
               }
               if(iSelect==3)
               {
                  QString = QString+" and temp1.InvQty > 0 and temp1.DueDate >= '"+SToday+"'";
                  ReportHead=ReportHead+"List:Before Over Due";
               }
          }

          String SOrder=(TopPanel.TSort.getText()).trim();
          if(SOrder.length()>0)
                QString = QString+" Order By "+TopPanel.TSort.getText()+",11";
          else
                QString = QString+" Order By 1,2,3,4";
         
          return QString;
     }

     private String getInfo()
     {

          String str = "<html><body>"; 
          str = str+"<h4><font color='Red'> PRN File Successfully Created</font></h4>";
          str = str+"</body></html>";

        return str;
     }
	
	 public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if (ke.getKeyCode()==10)
               {
                    try
                    {
                         int iRow = tabreport.ReportTable.getSelectedRow();
						DocumentationEntryFrameNew docFrame = new DocumentationEntryFrameNew(DeskTop,tabreport,iRow,RowData);

						DeskTop.add(docFrame);
						DeskTop.repaint();
						docFrame.setSelected(true);
						DeskTop.updateUI();
						docFrame.show();
					}
					catch(Exception e)
					{
					}
				}
			}
	}

}
