package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import java.net.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkOrderFrame1 extends JInternalFrame
{
     
     JPanel           TopPanel,TopLeft,TopRight,BottomPanel;
     WOMiddlePanel    MiddlePanel;

     JButton          BOk,BCancel,BSupplier;
     JButton          BApply;

     NextField        TOrderNo;
     JTextField       TSupCode;
     MyTextField      TRef,TPayTerm;
     NextField        TAdvance;
     DateField        TDate;
     MyComboBox       JCTo,JCThro,JCForm;
     String           SOrderNo="";

     JLayeredPane     DeskTop;
     StatusPanel      SPanel;
     int              iUserCode;
     int              iMillCode;
     WorkOrderListFrame   orderlistframe;
     String           SSupTable,SYearCode;

     boolean          bflag;                 

     Common common    = new Common();
     ORAConnection connect;
     Connection theConnect = null;

     Vector           VTo,VThro,VToCode,VThroCode,VFormCode,VForm;

     Vector           VPendCode,VPendName,VRdcNo,VPendDetails,VSlNo,VRdcQty,VPrevQty,VPendQty,VWOQty;
     Vector           VRDept,VRGroup,VRUnit,VMemoUserCode;

     int       iMaxSlNo  = 0;
     int       iCount=0;

     boolean             bComflag = true;

     int iActualAmend=0;

     WorkOrderFrame1(JLayeredPane DeskTop,StatusPanel SPanel,boolean bflag,int iUserCode,int iMillCode,String SSupTable,String SYearCode)
     {
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.bflag     = bflag;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }
     WorkOrderFrame1(JLayeredPane DeskTop,StatusPanel SPanel,boolean bflag,int iUserCode,int iMillCode,WorkOrderListFrame orderlistframe,String SSupTable)
     {
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.bflag     = bflag;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.orderlistframe= orderlistframe;
          this.SSupTable = SSupTable;

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }

     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnect     =    oraConnection.getConnection();
     }

     public void createComponents()
     {
          TopPanel    = new JPanel();
          TopLeft     = new JPanel();
          TopRight    = new JPanel();
          BottomPanel = new JPanel();

          BOk         = (bflag?new JButton("Update"):new JButton("Save"));
          BCancel     = new JButton("Abort");

          BSupplier   = new JButton("Supplier");
          BApply      = new JButton("Apply");

          TOrderNo    = new NextField();
          TDate       = new DateField();
          TSupCode    = new JTextField();
          TAdvance    = new NextField(10);
          TPayTerm    = new MyTextField(40);
          TRef        = new MyTextField(30);

          getVTo();

          JCTo        = new MyComboBox(VTo);
          JCThro      = new MyComboBox(VThro);
          JCForm      = new MyComboBox(VForm);

          TDate.setTodayDate();
          
          TDate.setEditable(false);
          TOrderNo.setEditable(false);
          BSupplier.setMnemonic('U');
          BApply.setMnemonic('A');
          BCancel.setMnemonic('C');
          if(bflag)
          {
               BOk.setMnemonic('U');
          }
          else
          {
               BOk.setMnemonic('S');
          }
     }


     public void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(1,2));
          TopRight.setLayout(new GridLayout(5,2));

          if(bflag)
          {
               setTitle("Amendment Or Modification to Work Order");
               TopLeft   .setLayout(new GridLayout(4,2));
          }
          else
          {
               setTitle("Work Order Placement");
               TopLeft.setLayout(new GridLayout(5,2));
          }

          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,790,500);

          getContentPane()    .setLayout(new BorderLayout());
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          TopLeft   .add(new JLabel("Order No"));
          TopLeft   .add(TOrderNo);

          TopLeft   .add(new JLabel("Order Date"));
          TopLeft   .add(TDate);

          TopLeft   .add(new JLabel("Reference"));
          TopLeft   .add(TRef);

          TopLeft   .add(new JLabel("Supplier"));
          TopLeft   .add(BSupplier);

          if(!bflag)
          {
               TopLeft   .add(new JLabel("Materials"));
               TopLeft   .add(BApply);
          }

          TopRight  .add(new JLabel("Book To"));
          TopRight  .add(JCTo);

          TopRight  .add(new JLabel("Book Thro"));
          TopRight  .add(JCThro);

          TopRight  .add(new JLabel("Form No"));
          TopRight  .add(JCForm);

          TopRight  .add(new JLabel("Advance"));
          TopRight  .add(TAdvance);

          TopRight  .add(new JLabel("Payment Terms"));
          TopRight  .add(TPayTerm);

          TopPanel  .add(TopLeft);
          TopPanel  .add(TopRight);

          TopLeft   .setBorder(new TitledBorder("Order Id Block-I"));
          TopRight  .setBorder(new TitledBorder("Order Id Block-II"));

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          TOrderNo.setText(getNextOrderNo());  
     }

     public void addListeners()
     {
          BSupplier.addActionListener(new SupplierSearch(DeskTop,TSupCode,SSupTable));
          BApply.addActionListener(new ApplyList());

          if(bflag)
          {
               BOk       .addActionListener(new UpdtList());
          }
          else 
          {
               BOk       .addActionListener(new ActList());
          }
          BCancel   .addActionListener(new CanList());
     }
     public class CanList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               removeHelpFrame();
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setMiddlePanel();
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(common.toInt(TOrderNo.getText())>0)
               {
                    if(checkData())
                    {
                         iCount=0;
                         BOk.setEnabled(true);
                         insertOrderDetails();
                         if(iCount>0)
                             updateOrderNo();

                         getACommit();
                         removeHelpFrame();
                    }
               }
          }
     }

     public class UpdtList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(checkData())
               {
                    BOk.setEnabled(false);

	               int iSel = JOptionPane.showConfirmDialog(null,"Do U want to Update this Order. This will remove SO Approval"); 
	               if (iSel == JOptionPane.YES_OPTION)
	               {
	                    try
	                    {
						insertAmendDetails();
		                        updateOrderDetails();
	                    }
	                    catch(Exception Ex)
	                    {
	                         Ex.printStackTrace();
	                         bComflag = false;
	                    }

	                    try
	                    {
		                    getACommit();
		                    removeHelpFrame();
		                    orderlistframe.setTabReport(orderlistframe);
	                    }catch(Exception ex)
	                    {
	                         ex.printStackTrace();
	                    }
	               }
	               else
	               {
	                    BOk  . setEnabled(true);
	               }
               }
          }
     }

     public void insertAmendDetails()
     {
          try
          {
               iActualAmend=0;

               int iAmendNo = 0;

               String SOrderNo = ((String)TOrderNo.getText()).trim();

               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat           =  theConnect.createStatement();
                                        

               iActualAmend = getActualAmend(SOrderNo);

               if(iActualAmend>0)
               {
                    iAmendNo = getAmendNo(SOrderNo);
               }

               String SSysName  = InetAddress.getLocalHost().getHostName();
               String SDateTime = common.getServerDate();

               String QS1 = " Insert Into WorkOrderAmend (Id,NoofAmend,AmendUserCode,AmendSystemName,AmendDateTime,"+
"OrderId,OrderNo,OrderDate,Sup_Code,Reference,Advance,Descript,Qty,Rate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,"+
"SurPer,Sur,Net,Plus,Less,Misc,DueDate,PayTerms,InvQty,Dept_Code,Group_Code,Unit_Code,ToCode,ThroCode,Posted,"+
"AdvSlNo,AmendmentFlag,FormCode,Class_Code,SlNo,PYPost,WA,DM,RDCNo,UserCode,MillCode,ModiDate,Status,Flag,RdcSlNo,"+
"Remarks,LogBookNo,IntimationNo,WODesc,EntryStatus,MemoAuthUserCode,SOOrderApproval,SOOrderAppDate,SOOrderRejection,"+
"SOOrderRejDate,IAOrderApproval,IAOrderAppDate,IAOrderRejRemarks,IAOrderRejection,JMDOrderApproval,JMDOrderAppDate,"+
"WorkGroupNo,JMDOrderRejDate,JMDOrderHolding,JMDOrderHoldingDate,JMDOrderRejection,IAOrderRejDate) "+
" (Select WorkOrderAmend_Seq.nextVal,"+
""+iAmendNo+" as NoofAmend,"+
""+iUserCode+" as AmendUserCode,"+
"'"+SSysName+"' as AmendSystemName,"+
"'"+SDateTime+"' as AmendDateTime,"+
" Id as OrderId,OrderNo,OrderDate,Sup_Code,Reference,Advance,Descript,Qty,Rate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,"+
"SurPer,Sur,Net,Plus,Less,Misc,DueDate,PayTerms,InvQty,Dept_Code,Group_Code,Unit_Code,ToCode,ThroCode,Posted,"+
"AdvSlNo,AmendmentFlag,FormCode,Class_Code,SlNo,PYPost,WA,DM,RDCNo,UserCode,MillCode,ModiDate,Status,Flag,RdcSlNo,"+
"Remarks,LogBookNo,IntimationNo,WODesc,EntryStatus,MemoAuthUserCode,SOOrderApproval,SOOrderAppDate,SOOrderRejection,"+
"SOOrderRejDate,IAOrderApproval,IAOrderAppDate,IAOrderRejRemarks,IAOrderRejection,JMDOrderApproval,JMDOrderAppDate,"+
"WorkGroupNo,JMDOrderRejDate,JMDOrderHolding,JMDOrderHoldingDate,JMDOrderRejection,IAOrderRejDate "+
" From WorkOrder Where OrderNo="+SOrderNo+" and MillCode="+iMillCode+")";

               stat.executeUpdate(QS1);
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("4"+ex);
               ex.printStackTrace();
          }
     }

     public int getAmendNo(String SOrderNo)
     {
          int iAmendNo=0;

          String QS = " Select Max(NoofAmend) from WorkOrderAmend Where OrderNo="+SOrderNo+" and MillCode="+iMillCode;

          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         iAmendNo  = common.toInt((String)result.getString(1))+1;
                         result    . close();
                         stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
           
          return iAmendNo;
     }

     public int getActualAmend(String SOrderNo)
     {
          int iAmendNo=0;

          int iCount=0;

          String QS1 = " Select Count(*) from WorkOrder Where AmendmentFlag=1 and (IAOrderApproval=0 and JMDOrderApproval=0) and OrderNo="+SOrderNo+" and MillCode="+iMillCode;

          String QS2 = " Select Count(*) from WorkOrderAmend Where (IAOrderApproval=1 or JMDOrderApproval=1) and OrderNo="+SOrderNo+" and MillCode="+iMillCode;

          String QS3 = " Select Count(*) from WorkOrder Where (IAOrderApproval=1 or JMDOrderApproval=1) and OrderNo="+SOrderNo+" and MillCode="+iMillCode;

          try
          {
               Statement stat      = theConnect   . createStatement();
               ResultSet result    = stat         . executeQuery(QS1);
                         result    . next();
                         iCount    = result.getInt(1);
                         result    . close();

               if(iCount>0)
               {
                    iAmendNo = checkQtyAmend(SOrderNo,false);
                    //iAmendNo=1;
               }
               else
               {
                    result    = stat         . executeQuery(QS2);
                    result    . next();
                    iCount    = result.getInt(1);
                    result    . close();

                    if(iCount>0)
                    {
                         iAmendNo = checkQtyAmend(SOrderNo,true);
                         //iAmendNo=1;
                    }
                    else
                    {
                         result    = stat         . executeQuery(QS3);
                         result    . next();
                         iCount    = result.getInt(1);
                         result    . close();
     
                         if(iCount>0)
                         {
                              iAmendNo = checkQtyAmend(SOrderNo,false);
                              //iAmendNo=1;
                         }
                    }
               }

               stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
           
          return iAmendNo;
     }

     public int checkQtyAmend(String SOrderNo,boolean bAmendFlag)
     {
          int iAmendNo=0;
          int iOtherAmend=0;
          int iQtyAmend=0;

          try
          {
               Statement stat      = theConnect   . createStatement();


               Object FinalData[][]  = MiddlePanel.MiddlePanel.getFromVector();

               double dNewAdd           = common.toDouble(MiddlePanel.MiddlePanel.TAdd.getText());
               double dNewLess          = common.toDouble(MiddlePanel.MiddlePanel.TLess.getText());

               for(int i=0;i<MiddlePanel.IdData.length;i++)
               {
                    String SOldItemCode  = "";
                    double dOldQty       = 0;
                    double dOldRate      = 0;
                    double dOldDiscPer   = 0;
                    double dOldCenvatPer = 0;
                    double dOldTaxPer    = 0;
                    double dOldSurPer    = 0;
				double dOldAdd	      = 0;
				double dOldLess	 = 0;

                    String SNewItemCode  = (String)FinalData[i][0];
                    double dNewQty       = common.toDouble(((String)FinalData[i][6]).trim());
                    double dNewRate      = common.toDouble(((String)FinalData[i][7]).trim());
                    double dNewDiscPer   = common.toDouble(((String)FinalData[i][8]).trim());
                    double dNewCenvatPer = common.toDouble(((String)FinalData[i][9]).trim());
                    double dNewTaxPer    = common.toDouble(((String)FinalData[i][10]).trim());
                    double dNewSurPer    = common.toDouble(((String)FinalData[i][11]).trim());

                    String SId           = (String)MiddlePanel.IdData[i];

                    String QS1 = "";

                    if(bAmendFlag)
                    {
                         QS1 = " Select Descript,Qty,Rate,DiscPer,CenvatPer,TaxPer,SurPer,Plus,Less from WorkOrderAmend "+
                               " Where Id=(Select Min(Id) from WorkOrderAmend Where OrderId="+SId+
                               " And (IAOrderApproval=1 or JMDOrderApproval=1)) ";
                    }
                    else
                    {
                         QS1 = " Select Descript,Qty,Rate,DiscPer,CenvatPer,TaxPer,SurPer,Plus,Less from WorkOrder "+
                               " Where Id="+SId;
                    }

                    ResultSet result = stat.executeQuery(QS1);
                    while(result.next())
                    {
                         SOldItemCode  = common.parseNull((String)result.getString(1));
                         dOldQty       = common.toDouble(common.parseNull((String)result.getString(2)));
                         dOldRate      = common.toDouble(common.parseNull((String)result.getString(3)));
                         dOldDiscPer   = common.toDouble(common.parseNull((String)result.getString(4)));
                         dOldCenvatPer = common.toDouble(common.parseNull((String)result.getString(5)));
                         dOldTaxPer    = common.toDouble(common.parseNull((String)result.getString(6)));
                         dOldSurPer    = common.toDouble(common.parseNull((String)result.getString(7)));
					dOldAdd       = common.toDouble(common.parseNull((String)result.getString(8)));
					dOldLess      = common.toDouble(common.parseNull((String)result.getString(9)));
                    }
                    result.close();

                    if((SNewItemCode.equals(SOldItemCode)) && (dNewRate==dOldRate) && (dNewDiscPer==dOldDiscPer) && (dNewCenvatPer==dOldCenvatPer) && (dNewTaxPer==dOldTaxPer) && (dNewSurPer==dOldSurPer) && (dNewAdd==dOldAdd) && (dNewLess==dOldLess))
                    {
                         iOtherAmend = 0;
                    }
                    else
                    {
                         iOtherAmend = 1;
                         iAmendNo    = 1;
                         break;
                    }

                    if(dNewQty>dOldQty)
                    {
                         iQtyAmend = 1;
                         iAmendNo  = 1;
                         break;
                    }
                    else
                    {
                         if(dNewQty==dOldQty)
                         {
                              iQtyAmend = 0;
                         }
                         else
                         {
                              double dAllowQty = dOldQty - (dOldQty * 10 / 100);
     
                              if(dNewQty<dAllowQty)
                              {
                                   iQtyAmend = 1;
                                   iAmendNo  = 1;
                                   break;
                              }
                         }
                    }
               }

               stat.close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
           
          return iAmendNo;
     }

     public void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnect     . commit();
                    System         . out.println("Commit");
                    theConnect     . setAutoCommit(true);
               }
               else
               {
                    theConnect     . rollback();
                    JOptionPane.showMessageDialog(null,"Problem in Saving Data","Error",JOptionPane.ERROR_MESSAGE);
                    System         . out.println("RollBack");
                    theConnect     . setAutoCommit(true);
               }
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void insertOrderDetails()
     {

          String QString = "Insert Into WorkOrder (ID,OrderNo,OrderDate,Sup_Code,Reference,Advance,PayTerms,ToCode,ThroCode,FormCode,Descript,RDCNo,Qty,Rate,DiscPer,Disc,CenvatPer,CenVat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,Unit_Code,Dept_Code,Group_Code,DueDate,SlNo,UserCode,ModiDate,RDCSlNo,MillCode,LogBookNo,Remarks,WODesc,EntryStatus,DocId,MemoAuthUserCode) Values (";

          int iSlNo = 0;
          int iMemoUserCode = 1;

          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat      = theConnect.createStatement();

               TOrderNo.setText(getNextOrderNo());  

               if(bflag)
                    iMaxSlNo = getMaxSlNo(((String)TOrderNo.getText()).trim());
               else
                    iMaxSlNo = 0;


               int k = iMaxSlNo;

               Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
               String SAdd       = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess      = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm        = common.toDouble(SAdd)-common.toDouble(SLess);
               double dBasic     = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio     = dpm/dBasic;

               for(int i=0;i<FinalData.length;i++)
               {
                    try
                    {
                         if(!isFreshRecord(i))
                              continue;
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
                  
                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SDueDate   = MiddlePanel.MiddlePanel.getDueDate(i,TDate);

                    dBasic            = common.toDouble(((String)FinalData[i][12]).trim());

                    if(dBasic == 0)
                         continue;

                    String SMisc      = common.getRound(dBasic*dRatio,3);

                    if(MiddlePanel.SlData.length>i)
                    {
                         iSlNo = common.toInt(common.parseNull((String)MiddlePanel.SlData[i]));
                    }
                    else
                    {
                         iSlNo = 0;
                    }

                    String SRDCNo = ((String)FinalData[i][1]).trim();

                    String SEntryStatus="1";

                    String SRDCDesc = "";
                    String SWODesc  = "";

                    SRDCDesc = (((String)FinalData[i][0]).trim()).toUpperCase();
                    SWODesc  = (((String)FinalData[i][2]).trim()).toUpperCase();


                    if(SRDCDesc.equals(""))
                    {
                         SRDCDesc = SWODesc;
                         SWODesc  = "";
                    }

                    iMemoUserCode = common.toInt((String)VMemoUserCode.elementAt(i));

                    if(iMemoUserCode<=0)
                    {
                         iMemoUserCode = 1;
                    }

                    String QS1 = QString;
                    QS1 = QS1+"WorkOrder_Seq.nextval,";
                    QS1 = QS1+"0"+TOrderNo.getText()+",";
                    QS1 = QS1+"'"+TDate.toNormal()+"',";
                    QS1 = QS1+"'"+TSupCode.getText()+"',";
                    QS1 = QS1+"'"+(TRef.getText()).toUpperCase()+"',";    
                    QS1 = QS1+"0"+TAdvance.getText()+",";
                    QS1 = QS1+"'"+(TPayTerm.getText()).toUpperCase()+"',";
                    QS1 = QS1+"0"+(String)VToCode.elementAt(JCTo.getSelectedIndex())+",";
                    QS1 = QS1+"0"+(String)VThroCode.elementAt(JCThro.getSelectedIndex())+",";
                    QS1 = QS1+"0"+(String)VFormCode.elementAt(JCForm.getSelectedIndex())+",";
                    QS1 = QS1+"'"+common.getNarration(SRDCDesc)+"',";
                    QS1 = QS1+"0"+SRDCNo+",";
                    QS1 = QS1+"0"+((String)FinalData[i][6]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][7]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][8]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][13]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][9]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][14]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][10]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][15]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][11]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][16]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][17]).trim()+",";
                    QS1 = QS1+"0"+SAdd+",";
                    QS1 = QS1+"0"+SLess+",";
                    QS1 = QS1+"0"+SMisc+",";
                    QS1 = QS1+"0"+SUnitCode+",";
                    QS1 = QS1+"0"+SDeptCode+",";
                    QS1 = QS1+"0"+SGroupCode+",";
                    QS1 = QS1+"'"+SDueDate+"',";
                    QS1 = QS1+(k+1)+",";
                    QS1 = QS1+"0"+iUserCode+",";
                    QS1 = QS1+"'"+common.getServerDateTime2()+"',";
                    QS1 = QS1+iSlNo+",";
                    QS1 = QS1+iMillCode+",";
                    QS1 = QS1+"0"+((String)FinalData[i][23]).trim()+",";
                    QS1 = QS1+"'"+(((String)FinalData[i][22]).trim()).toUpperCase()+"',";
                    QS1 = QS1+"'"+common.getNarration(SWODesc)+"',";
                    QS1 = QS1+"0"+SEntryStatus+",";

                    QS1 = QS1+"0"+((String)FinalData[i][24]).trim()+",";

                    QS1 = QS1+"0"+iMemoUserCode+")";

                    stat.executeUpdate(QS1);

                    String QS2 = " Update RDC set WOStatus=1,WOQty=WOQty+("+((String)FinalData[i][6]).trim()+")"+
                                 " Where RdcNo="+SRDCNo+" and SlNo="+iSlNo+
                                 " and MillCode="+iMillCode+" and Descript='"+((String)FinalData[i][0]).trim()+"'";

                    stat.executeUpdate(QS2);

                    String QS3 = " Update GateInwardRDC set OrderNo="+TOrderNo.getText()+
                                 " Where OrderNo=0 and WOStatus=1 and RdcNo="+SRDCNo+" and RDCSlNo="+iSlNo+
                                 " and MillCode="+iMillCode+" and Descript='"+((String)FinalData[i][0]).trim()+"'";

                    stat.executeUpdate(QS3);
			System.out.println("Insert Qry-->"+QS2);

			System.out.println("Update Qry-->"+QS3);


                    k++;
                    iCount++;
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void updateOrderDetails()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat      = theConnect.createStatement();

               Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
               String SAdd       = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess      = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm        = common.toDouble(SAdd)-common.toDouble(SLess);
               double dBasic     = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio     = dpm/dBasic;

               for(int i=0;i<MiddlePanel.IdData.length;i++)
               {
                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SDueDate   = MiddlePanel.MiddlePanel.getDueDate(i,TDate);
                    dBasic            = common.toDouble(((String)FinalData[i][12]).trim());
                    String SMisc      = common.getRound(dBasic*dRatio,3);

                    String QS1 = "Update WorkOrder Set ";
                    QS1 = QS1+"OrderDate  = '"+TDate.toNormal()+"',";
                    QS1 = QS1+"Reference='"+(TRef.getText()).toUpperCase()+"',";
                    QS1 = QS1+"Advance=0"+TAdvance.getText()+",";
                    QS1 = QS1+"PayTerms='"+(TPayTerm.getText()).toUpperCase()+"',";
                    QS1 = QS1+"ToCode=0"+(String)VToCode.elementAt(JCTo.getSelectedIndex())+",";
                    QS1 = QS1+"ThroCode=0"+(String)VThroCode.elementAt(JCThro.getSelectedIndex())+",";
                    QS1 = QS1+"FormCode=0"+(String)VFormCode.elementAt(JCForm.getSelectedIndex())+",";
                    //QS1 = QS1+"Descript='"+(((String)FinalData[i][0]).trim()).toUpperCase()+"',";
                    //QS1 = QS1+"RDCNo=0"+((String)FinalData[i][1]).trim()+",";
                    QS1 = QS1+"WODesc='"+common.getNarration(((common.parseNull((String)FinalData[i][2])).trim()).toUpperCase())+"',";
                    //QS1 = QS1+"Qty=0"+((String)FinalData[i][6]).trim()+",";
                    QS1 = QS1+"Rate=0"+((String)FinalData[i][7]).trim()+",";
                    QS1 = QS1+"DiscPer=0"+((String)FinalData[i][8]).trim()+",";
                    QS1 = QS1+"Disc=0"+((String)FinalData[i][13]).trim()+",";
                    QS1 = QS1+"CenVatPer=0"+((String)FinalData[i][9]).trim()+",";
                    QS1 = QS1+"Cenvat=0"+((String)FinalData[i][14]).trim()+",";
                    QS1 = QS1+"TaxPer=0"+((String)FinalData[i][10]).trim()+",";
                    QS1 = QS1+"Tax=0"+((String)FinalData[i][15]).trim()+",";
                    QS1 = QS1+"SurPer=0"+((String)FinalData[i][11]).trim()+",";
                    QS1 = QS1+"Sur=0"+((String)FinalData[i][16]).trim()+",";
                    QS1 = QS1+"Net=0"+((String)FinalData[i][17]).trim()+",";
                    QS1 = QS1+"Plus=0"+SAdd+",";
                    QS1 = QS1+"Less=0"+SLess+",";
                    QS1 = QS1+"Misc=0"+SMisc+",";
                    QS1 = QS1+"Unit_Code=0"+SUnitCode+",";
                    QS1 = QS1+"Dept_Code=0"+SDeptCode+",";
                    QS1 = QS1+"Group_Code=0"+SGroupCode+",";
				QS1 = QS1+"AmendmentFlag=0"+iActualAmend+",";

                    if(iActualAmend>0)
                    {
                         QS1 = QS1+"SOOrderApproval = 0,";
                         QS1 = QS1+"SOOrderRejection = 0,";
                         QS1 = QS1+"SOOrderAppDate = '',";
                         QS1 = QS1+"IAOrderApproval = 0,";
                         QS1 = QS1+"IAOrderRejection = 0,";
                         QS1 = QS1+"IAOrderAppDate = '',";
                         QS1 = QS1+"JMDOrderApproval = 0,";
                         QS1 = QS1+"JMDOrderRejection = 0,";
                         QS1 = QS1+"JMDOrderAppDate = '',";
                    }

                    QS1 = QS1+"LogBookNo=0"+((String)FinalData[i][23]).trim()+",";
                    QS1 = QS1+"Remarks='"+(((String)FinalData[i][22]).trim()).toUpperCase()+"',";
                    QS1 = QS1+"DocId=0"+((String)FinalData[i][24]).trim()+",";
                    QS1 = QS1+"DueDate='"+SDueDate+"' Where OrderNo="+TOrderNo.getText()+" and ID = "+(String)MiddlePanel.IdData[i];

                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void updateOrderNo()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat      = theConnect.createStatement();

               String QS1 = " Update Config"+iMillCode+""+SYearCode+" Set MaxNo=MaxNo+1 Where ID=11 ";
               stat.executeUpdate(QS1);
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public boolean isFreshRecord(int i)
     {
          try
          {
               if(MiddlePanel.IdData==null)
                    return true;

               boolean bfresh = (i<MiddlePanel.IdData.length?false:true);
               return bfresh;
          }
          catch(Exception ex)
          {
               return true;
          }
     }

     public int getMaxSlNo(String SOrderNo)
     {
          int iMaxNo=0;

          String QS = " Select Max(SlNo) from WorkOrder Where OrderNo="+SOrderNo+" and MillCode="+iMillCode;

          iMaxNo = common.toInt(common.getID(QS));
           
          return iMaxNo;
     }

     public String getNextOrderNo()
     {
          if(bflag)
               return TOrderNo.getText();
               
          String QS = "";

          QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" "+
               " Where ID=11 ";

          String SOrderNo = String.valueOf(common.toInt(common.getID(QS))+1);
          return SOrderNo;
     }
     public void getVTo()
     {
          VTo       = new Vector();
          VToCode   = new Vector();
          VThro     = new Vector();
          VThroCode = new Vector();
          VFormCode = new Vector();
          VForm     = new Vector();

          try
          {
               Statement stat   = theConnect.createStatement();
               ResultSet res = stat.executeQuery("Select ToName,ToCode From BookTo Order By 1");
               while(res.next())
               {
                    VTo.addElement(res.getString(1));
                    VToCode.addElement(res.getString(2));
               }
               res.close();
               res = stat.executeQuery("Select ThroName,ThroCode From BookThro Order By 1");
               while(res.next())
               {
                    VThro.addElement(res.getString(1));
                    VThroCode.addElement(res.getString(2));
               }
               res.close();
               res = stat.executeQuery("Select FormNo,FormCode From STForm Order By 1");
               while(res.next())
               {
                    VForm.addElement(res.getString(1));
                    VFormCode.addElement(res.getString(2));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void setMiddlePanel()
     {
          String SSupCode = TSupCode.getText();

          if(SSupCode.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Please Select the Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
          }
          else
          {
               setDataVector();

               if(VPendName.size()<=0)
               {
                    JOptionPane.showMessageDialog(null,"There is no Pending for this Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
               }
               else
               {
                    BSupplier.setEnabled(false);
                    BApply.setEnabled(false);

                    MiddlePanel = new WOMiddlePanel(DeskTop,VPendName,VRdcNo,VSlNo,VRdcQty,VPrevQty,VPendQty,VWOQty,VRDept,VRGroup,VRUnit,iMillCode,false);

                    getContentPane().add(MiddlePanel,BorderLayout.CENTER);
                    DeskTop.updateUI();
               }
          }

     }

     public void setDataVector()
     {
          VPendName     = new Vector();
          VRdcNo        = new Vector();
          VSlNo         = new Vector();
          VRdcQty       = new Vector();
          VPrevQty      = new Vector();
          VPendQty      = new Vector();
          VWOQty        = new Vector();
          VRDept        = new Vector();
          VRUnit        = new Vector();
          VRGroup       = new Vector();
          VMemoUserCode = new Vector();

          String QS = " Select * from ( "+
                      " Select GateInwardRDC.Descript,GateInwardRDC.RDCNo as RNo,"+
                      " GateInwardRDC.RDCSlNo,RDC.Qty,sum(RDC.WOQty+RDC.DirectWOQty+RDC.FreeQty) as PrevWOQty,Sum(GateInwardRDC.WOQty) as WOQty,"+
                      " Dept.Dept_Name,Unit.Unit_Name,GateInwardRDC.MemoAuthUserCode From GateInwardRDC "+
                      " Inner Join RDC on (GateInwardRDC.RDCNo=RDC.RDCNO and GateInwardRDC.RDCSlNo=RDC.SlNo "+
                      " and GateInwardRDC.Descript=RDC.Descript and GateInwardRDC.MillCode=RDC.MillCode) "+
                      " And GateInwardRDC.OrderNo=0 and GateInwardRDC.WOStatus=1 and GateInwardRDC.MillCode="+iMillCode+
                      " and GateInwardRDC.Sup_Code = '"+TSupCode.getText()+"' "+ 
                      " Inner Join Dept on RDC.Dept_Code=Dept.Dept_Code "+
                      " Inner Join Unit on RDC.Unit_Code=Unit.Unit_Code "+
                      " Group by GateInwardRDC.Descript,GateInwardRDC.RDCNo,"+
                      " GateInwardRDC.RdcSlNo,RDC.Qty,Dept.Dept_Name,Unit.Unit_Name,GateInwardRDC.MemoAuthUserCode ) "+
                      " Order By Descript,RNo ";

//		System.out.println("Qry:"+QS);
          try
          {
               Statement stat      = theConnect.createStatement();
               ResultSet theResult = stat.executeQuery(QS);

               while(theResult.next())
               {
                    double dRDCQty  = theResult.getDouble(4);
                    double dPrevQty = theResult.getDouble(5);
                    double dPendQty = dRDCQty - dPrevQty;
                    double dWOQty   = theResult.getDouble(6);

                    VPendName    .addElement(theResult.getString(1));
                    VRdcNo       .addElement(common.parseNull(String.valueOf(theResult.getInt(2))));
                    VSlNo        .addElement(common.parseNull(String.valueOf(theResult.getInt(3))));
                    VRdcQty      .addElement(String.valueOf(dRDCQty));
                    VPrevQty     .addElement(String.valueOf(dPrevQty));
                    VPendQty     .addElement(String.valueOf(dPendQty));
                    VWOQty       .addElement(String.valueOf(dWOQty));
                    VRDept       .addElement(common.parseNull(theResult.getString(7)));
                    VRUnit       .addElement(common.parseNull(theResult.getString(8)));
                    VRGroup      .addElement("REPAIRS AND RENEWALS");
                    VMemoUserCode.addElement(common.parseNull(theResult.getString(9)));
               }
               theResult.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void fillData(String SOrderNo)
     {
          try
          {
               MiddlePanel = new WOMiddlePanel(DeskTop,iMillCode);
//               new WorkOrderResultSet(SOrderNo,TOrderNo,TDate,BSupplier,TSupCode,TRef,TAdvance,TPayTerm,JCTo,JCThro,JCForm,MiddlePanel,VToCode,VThroCode,VFormCode,this,iMillCode,SSupTable);
		    new WorkOrderResultSet1();
               getContentPane().add(MiddlePanel,BorderLayout.CENTER);
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println("6"+ex);
               ex.printStackTrace();
          }
     }

     public boolean checkData()
     {
          String SOrderDate = TDate.toNormal();
          String SSupCode   = TSupCode.getText();
	    int iRDCAuthStatus=0;
          int iRows=0;

          if(SOrderDate.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Order Date Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TDate.TDay.requestFocus();
               return false;
          }
          if(SSupCode.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Select Supplier","Error",JOptionPane.ERROR_MESSAGE);
               BSupplier.setEnabled(true);
               BSupplier.requestFocus();
               return false;
          }
          try
          {
               iRows    = MiddlePanel.MiddlePanel.getRows();
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"No Materials Selected","Error",JOptionPane.ERROR_MESSAGE);
               BApply.setEnabled(true);
               BApply.requestFocus();
               return false;
          }

          int iRowCount=0;

          Object FData[][] = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<FData.length;i++)
          {
               double dPendQty = common.toDouble(((String)FData[i][5]).trim());
               double dQty     = common.toDouble(((String)FData[i][6]).trim());
               double dRate    = common.toDouble(((String)FData[i][7]).trim());
               String SDesc1 = ((String)FData[i][0]).trim();
               String SDesc2 = (common.parseNull((String)FData[i][2])).trim();
		   String SRDCNo = (common.parseNull((String)FData[i][1])).trim();
	        iRDCAuthStatus	= getRDCAuthStatus(SDesc1.trim(),SRDCNo);

System.out.println("RDC authStatus-->"+iRDCAuthStatus+"i-->"+i+"--"+SRDCNo);	

               if(dQty<=0 && dRate<=0){
                    continue;
			}
               if(!bflag)
               {
                    if(dQty>dPendQty)
                    {
                         JOptionPane.showMessageDialog(null,"Invalid Qty @Row - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }
               }

               /*if(dQty>0 && dRate<=0)
               {
                    JOptionPane.showMessageDialog(null,"Invalid Rate @Row - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }*/

               if(dQty<=0 && dRate>0)
               {
                    JOptionPane.showMessageDialog(null,"Invalid Qty @Row - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               String SDueDate   = MiddlePanel.MiddlePanel.getDueDate(i,TDate);

               if(common.toInt(SDueDate)<common.toInt((String)TDate.toNormal()))
               {
                    JOptionPane.showMessageDialog(null,"Due Date Greater than Order Date","Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(SDesc1.equals("") && SDesc2.equals("") && dQty>0)
               {
                    JOptionPane.showMessageDialog(null,"Invalid Description @Row - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(bflag)
               {
                    if(i<MiddlePanel.IdData.length && dQty<=0)
                    {
                         JOptionPane.showMessageDialog(null,"Invalid Quantity @Row-"+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }

               }

    	       if(iRDCAuthStatus==0){
				System.out.println("false");
			System.out.println("Inside Else ----------->"+iRDCAuthStatus+"--"+SRDCNo);
			JOptionPane.showMessageDialog(null," This RDC No ("+SRDCNo+" )  is Not Authenticated","Error",JOptionPane.ERROR_MESSAGE);
	            return false;
		   }
               iRowCount++;
		}

	  
          if(iRowCount<=0)
          {
               JOptionPane.showMessageDialog(null,"No Rows Filled","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          return true;
     }



private int getRDCAuthStatus(String sDescription, String sRDCNo){
int iRDCAuthStatus=-1;
try{
        StringBuffer sb     =new StringBuffer(); 
        
        sb.append("  Select RDCAuthStatus from RDC ");
	  sb.append("  Where RDCNo='"+ sRDCNo+"'  and Descript ='"+sDescription+"'");
        
        System.out.println("GetRDCAuthStatus Qry:"+sb.toString());
        if (theConnect==null)
              {
                  ORAConnection jdbc  = ORAConnection.getORAConnection();
                  theConnect         = jdbc.getConnection();
              }
        PreparedStatement ps          = theConnect.prepareStatement(sb.toString());
        ResultSet rst                 = ps.executeQuery();
    
       while (rst.next())
         {
                    iRDCAuthStatus    = rst.getInt(1);
         }
            rst.close();
            ps.close();
            ps=null;
    }
    catch(Exception e){
            e.printStackTrace();
            System.out.println("getNoofEmp:"+e);
    }
    return iRDCAuthStatus;

}


}
