package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkOrderGstLogBookFrame extends JInternalFrame
{
     MyComboBox     JCLogNo;
     JLabel         LDesc;

     JButton        BOk,BCancel;
     JPanel         TopPanel,BottomPanel;

     Vector         VLogNo;
     
     JLayeredPane   Layer;
     JTable         ReportTable;
     int            iMillCode;

     ORAConnection2 connect;
     Connection theconnect;

     WorkOrderGstLogBookFrame(JLayeredPane Layer,JTable ReportTable,int iMillCode)
     {
          this.Layer          = Layer;
          this.ReportTable    = ReportTable;
          this.iMillCode      = iMillCode;

          setPendingData();
          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
     }
     public void createComponents()
     {
          JCLogNo             = new MyComboBox(VLogNo);
          LDesc               = new JLabel("");
          
          BOk                 = new JButton("Okay");
          BCancel             = new JButton("Cancel");
          
          TopPanel            = new JPanel(true);
          BottomPanel         = new JPanel(true);
     }
     public void setLayouts()
     {
          setBounds(0,0,350,250);
          setResizable(true);
          setClosable(true);
          setTitle("LogBook Link Frame");

          TopPanel.setLayout(new GridLayout(4,2));
          BottomPanel.setLayout(new FlowLayout());
     }
     public void addComponents()
     {
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));

          TopPanel.add(new MyLabel("Description"));
          TopPanel.add(LDesc);

          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));

          TopPanel.add(new MyLabel("LogBookNo"));
          TopPanel.add(JCLogNo);

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add("North",TopPanel);
          getContentPane().add("South",BottomPanel);
     }
     public void addListeners()
     {
          BOk       .addActionListener(new ActList());
          BCancel   .addActionListener(new ActList());
     }
     public void setPresets()
     {
          int i = ReportTable.getSelectedRow();
          String SName      = (String)ReportTable.getModel().getValueAt(i,0);
          String SLogBookNo = (String)ReportTable.getModel().getValueAt(i,23);
          
          LDesc.setText(SName);
          JCLogNo.setSelectedItem(SLogBookNo);
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    int i = ReportTable.getSelectedRow();
                    String SLogNo = (String)VLogNo.elementAt(JCLogNo.getSelectedIndex());
                    ReportTable.getModel().setValueAt(SLogNo,i,23);
               }
               removeHelpFrame();
               ReportTable.requestFocus();
          }
     }
     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

     public void setPendingData()
     {
          VLogNo    = new Vector();

          VLogNo.addElement("0");

          iMillCode = iMillCode+1;

          try
          {

               String QS = " Select LogBookNo from ( "+
                           " Select LogBookNo from LogBook Where IntimationFlag=0 and MillCode="+iMillCode+
                           " Union All "+
                           " Select LogBook.LogBookNo from LogBook "+
                           " Inner Join Intimation on (LogBook.IntimationNo=Intimation.IntimationNo and LogBook.MillCode=Intimation.MillCode) "+
                           " Where Intimation.LetterStatus=0 and LogBook.MillCode="+iMillCode+
                           " Union All "+
                           " Select LogBook.LogBookNo from LogBook "+
                           " Inner Join Preliminary on (Logbook.IntimationNo=Preliminary.IntimationNo and LogBook.MillCode=Preliminary.MillCode) "+
                           " Where Preliminary.OrderStatus=0 and LogBook.MillCode="+iMillCode+") order by 1 ";

               if(theconnect==null)
               {
                    connect=ORAConnection2.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    VLogNo.addElement(res.getString(1));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

}
