package RDC;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkOrderGstMiddlePanel extends JPanel 
{
     //WOInvMiddlePanel MiddlePanel;
     WorkOrderGstInvMiddlePanel MiddlePanel;
     Object RowData[][];
     Object IdData[];
     Object SlData[];
     String sSupplierCode="";
  

    //                           0           1           2           3           4             5           6      7         8           9          10          11          12          13            14          15           16           17          18         19        20       21      22         23
//     String ColumnData[] = {"Service","HsnCode","RDC Descript","RDC No","WO Descript","RDC Qty","Prev WO Qty","Pending Qty","WO Qty","Rate","Discount(%)","CenVat(%)","Tax(%)","Surcharge(%)","Basic","Discount (Rs)","CenVat(Rs)","Tax(Rs)","Surcharge(Rs)","Net (Rs)","Department","Group","Due Date","Unit","Remarks","LogBook No","DocId"};
//     String[] ColumnType = new String[27];

	 String ColumnData[] ={"Service","HsnCode","RDC Descript","RDC No","WO Descript","RDC Qty","Prev WO Qty","Pending Qty","WO Qty","Rate","Discount(%)","CGST(%)","SGST(%)","IGST(%)","Cess(%)","Basic","Discount (Rs)","CGST(Rs)","SGST(Rs)","IGST(Rs)","Cess(Rs)","Net (Rs)","Department","Group","Due Date","Unit","Remarks","LogBook No","DocId"};
     String[] ColumnType = new String[29]; //25

     Common common = new Common();
     JLayeredPane DeskTop;

     Vector VName,VRdcNo,VSlNo,VQty,VPrevQty,VPendQty,VWOQty,VRDept,VRGroup,VRUnit,VMemoUserCode;
     int iMillCode;
     boolean bEditFlag = false;
     public boolean bflag;	
     public WorkOrderGstMiddlePanel(JLayeredPane DeskTop,Vector VName,Vector VRdcNo,Vector VSlNo,Vector VQty,Vector VPrevQty,Vector VPendQty,Vector VWOQty,Vector VRDept,Vector VRGroup,Vector VRUnit,int iMillCode,boolean bEditFlag,String sSupplierCode,boolean bflag,Vector VMemoUserCode)
     {
          this.DeskTop       = DeskTop;
          this.VName         = VName;
          this.VRdcNo        = VRdcNo;
          this.VSlNo         = VSlNo;
          this.VQty          = VQty;
          this.VPrevQty      = VPrevQty;
          this.VPendQty      = VPendQty;
          this.VWOQty        = VWOQty;
          this.VRDept        = VRDept;
          this.VRGroup       = VRGroup;
          this.VRUnit        = VRUnit;
          this.iMillCode     = iMillCode;
          this.bEditFlag     = bEditFlag;
	  	  this.sSupplierCode = sSupplierCode;
	  	  this.bflag	     = bflag;
		  this.VMemoUserCode = VMemoUserCode;
     
          setLayout(new BorderLayout());
          setRowData();
          createComponents();
     }
                                   
     public WorkOrderGstMiddlePanel(JLayeredPane DeskTop,int iMillCode,boolean bflag,String sSupplierCode)
     {
          this.DeskTop   = DeskTop;
          this.iMillCode = iMillCode;
	  this.bflag     = bflag;
	  this.sSupplierCode = sSupplierCode;
		
	  //System.out.println("bflag-->1"+bflag);
	  //System.out.println("sSupplierCode-->1"+sSupplierCode);

          setLayout(new BorderLayout());
     }

     public void createComponents()
     {
          setColType();
          MiddlePanel = new WorkOrderGstInvMiddlePanel(DeskTop,RowData,ColumnData,ColumnType,iMillCode,sSupplierCode,bflag,VMemoUserCode);
          add(MiddlePanel,BorderLayout.CENTER);
          MiddlePanel.ReportTable.requestFocus();
     }

     public void setRowData()
     {
          RowData     = new Object[VName.size()][ColumnData.length];
          SlData      = new Object[VSlNo.size()];

          for(int i=0;i<VName.size();i++)
          {
               SlData[i]      = common.parseNull((String)VSlNo.elementAt(i));
		
			   RowData[i][0]  = " ";
               RowData[i][1]  = " ";	
               RowData[i][2]  = common.parseNull((String)VName.elementAt(i));
               RowData[i][3]  = common.parseNull((String)VRdcNo.elementAt(i));
               RowData[i][4]  = " ";
               RowData[i][5]  = common.parseNull((String)VQty.elementAt(i));
               RowData[i][6]  = common.parseNull((String)VPrevQty.elementAt(i));
               RowData[i][7]  = common.parseNull((String)VPendQty.elementAt(i));
               RowData[i][8]  = common.parseNull((String)VWOQty.elementAt(i));
               RowData[i][9]  = " ";
			   RowData[i][10]  = " ";
			   RowData[i][11]  = " ";
               RowData[i][12] = " ";
               RowData[i][13] = " ";
               RowData[i][14] = " ";
               RowData[i][15] = " ";
               RowData[i][16] = " ";
               RowData[i][17] = " ";
               RowData[i][18] = " ";
               RowData[i][19] = " ";
               RowData[i][20] = " ";
               RowData[i][21] = " ";
               RowData[i][22] = common.parseNull((String)VRDept.elementAt(i));
               RowData[i][23] = common.parseNull((String)VRGroup.elementAt(i));
               RowData[i][24] = " ";
               RowData[i][25] = common.parseNull((String)VRUnit.elementAt(i));
               RowData[i][26] = " ";
               RowData[i][27] = " ";
               RowData[i][28] = " ";
          }
     }

     public void setColType()
     {
		ColumnType[0]  = "S";
		ColumnType[1]  = "S";
		ColumnType[2]  = "S";
		ColumnType[3]  = "S";
		ColumnType[4]  = "E";
		ColumnType[5]  = "N";
		ColumnType[6]  = "N";
		ColumnType[7]  = "N";
		ColumnType[8]  = "N";
		ColumnType[9]  = "N";
		ColumnType[10] = "N";
		ColumnType[11] = "N";
		ColumnType[12] = "N";
		ColumnType[13] = "N";
		ColumnType[14] = "N";
		ColumnType[15] = "N";
		ColumnType[16] = "N";
		ColumnType[17] = "N";
		ColumnType[18] = "N";
		ColumnType[19] = "N";
		ColumnType[20] = "N";
		ColumnType[21] = "N";
		ColumnType[22] = "B";
		ColumnType[23] = "B";
		ColumnType[24] = "E";
		ColumnType[25] = "B";
		ColumnType[26] = "E";
		ColumnType[27] = "E";
		ColumnType[28] = "E";


          if(bEditFlag)
          {
               ColumnType[6]  = "B";
          }
          else
          {
               ColumnType[6]  = "N";
          }
     }
}



