package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkOrderGstResultSet
{
      String    SOrderNo="";
      String    SOrderDate="";

      String    SToCode    ="0";
      String    SThroCode  ="0";
      String    SFormCode  ="0";

      String    SSupName   ="";
      String    SSupCode   ="";
      String    SRef       ="";
      String    SAdvance   ="";
      String    SPayTerm   ="";
      String    SAdd       ="";
      String    SLess      ="";
      Vector    VOName,VOQty,VORate,VODiscPer,VOCgstPer,VOSgstPer,VOIgstPer,VOCessPer,VItmeName,VHsnCode;
      Vector    VODeptName,VOGroupName,VOUnitName,VODueDate,VORDCNo,VRemarks,VLogBookNo,VRdcSlNo,VWODesc,VDocId;
      Vector    VOId,VHsnMatCode;

      NextField  TOrderNo;
      DateField  TDate;
      JButton    BSupplier;
      JTextField TSupCode;
      JTextField TRef;
      JTextField TAdvance;
      JTextField TPayTerm;
      JComboBox  JCTo,JCThro,JCForm;
      WorkOrderGstMiddlePanel MiddlePanel;
      Vector VToCode,VThroCode,VFormCode;
	 // MyArrayList<String>    MaterialCodeList;
      WorkOrderFrameGst workorderframe;
      int iMillCode;
      String SSupTable;

      Vector VDept,VDeptCode,VUnit,VUnitCode,VGroup,VGroupCode;
      Common common = new Common();
      ORAConnection connect;
      Connection theconnect;

      WorkOrderGstResultSet(String SOrderNo,NextField TOrderNo,DateField TDate,JButton BSupplier,JTextField TSupCode,JTextField TRef,JTextField TAdvance,JTextField TPayTerm,JComboBox JCTo,JComboBox JCThro,JComboBox JCForm,WorkOrderGstMiddlePanel MiddlePanel,Vector VToCode,Vector VThroCode,Vector VFormCode,WorkOrderFrameGst workorderframe,int iMillCode,String SSupTable)
      {
            this.SOrderNo    = SOrderNo;
            this.TOrderNo    = TOrderNo;
            this.TDate       = TDate;
            this.BSupplier   = BSupplier;
            this.TSupCode    = TSupCode;
            this.TRef        = TRef;
            this.TAdvance    = TAdvance;
            this.TPayTerm    = TPayTerm;
            this.JCTo        = JCTo;
            this.JCThro      = JCThro;
            this.JCForm      = JCForm;
            this.MiddlePanel = MiddlePanel;
            this.VToCode     = VToCode;
            this.VThroCode   = VThroCode;
            this.VFormCode   = VFormCode;
            this.workorderframe = workorderframe;
            this.iMillCode   = iMillCode;
            this.SSupTable   = SSupTable;
	        //this.MaterialCodeList	= MaterialCodeList;
            
            VOName      = new Vector();
            VOQty       = new Vector();
            VORate      = new Vector();
            VODiscPer   = new Vector();
            VOCgstPer   = new Vector();
            VOSgstPer   = new Vector();
            VOIgstPer   = new Vector();
	    	VOCessPer   = new Vector();
            VODeptName  = new Vector();
            VOGroupName = new Vector();
            VOUnitName  = new Vector();
            VODueDate   = new Vector();
            VOId        = new Vector();
            VORDCNo     = new Vector();
            VRemarks    = new Vector();
            VLogBookNo  = new Vector();
            VRdcSlNo    = new Vector();
            VWODesc     = new Vector();

	    	VDocId      = new Vector();

	    	VItmeName	= new Vector();
	    	VHsnCode	= new Vector();
		    VHsnMatCode = new Vector();;

            getData();
            setData();
      }
      public void setData()
      {
            MiddlePanel.RowData     = new Object[VOName.size()][29];
            MiddlePanel.SlData      = new Object[VOName.size()];
            MiddlePanel.IdData      = new Object[VOName.size()];
		    double dTGross=0,dTDisc=0,dTCGST=0,dTSGST=0,dTIGST=0,dTCess=0,dTNet=0,dTOthers=0;
			String SMatCode = "";
            for(int i=0;i<VOName.size();i++)
            {
                MiddlePanel.SlData[i]      = (String)VRdcSlNo.elementAt(i);
                MiddlePanel.IdData[i]      = (String)VOId.elementAt(i);
			
				//SMatCode				  = (String)VRdcSlNo.elementAt(i);
				//MaterialCodeList.add(i,SMatCode);
		
			double dGross   = common.toDouble((String)VOQty.elementAt(i))*common.toDouble((String)VORate.elementAt(i));
			double dDisc    = dGross*common.toDouble((String)VODiscPer.elementAt(i))/100;
			double dBasic   = dGross - dDisc;
			double dCGSTVal = dBasic*common.toDouble((String)VOCgstPer.elementAt(i))/100;
			double dSGSTVal = dBasic*common.toDouble((String)VOSgstPer.elementAt(i))/100;
			double dIGSTVal = dBasic*common.toDouble((String)VOIgstPer.elementAt(i))/100;
			double dCessVal = dBasic*common.toDouble((String)VOCessPer.elementAt(i))/100;
			double dNet     = dBasic+dCGSTVal+dSGSTVal+dIGSTVal+dCessVal;
			

			MiddlePanel.RowData[i][0]   = (String)VItmeName.elementAt(i);
			MiddlePanel.RowData[i][1]   = (String)VHsnCode.elementAt(i);	
			MiddlePanel.RowData[i][2]   = (String)VOName.elementAt(i);
			MiddlePanel.RowData[i][3]   = (String)VORDCNo.elementAt(i);
			MiddlePanel.RowData[i][4]   = (String)VWODesc.elementAt(i);
			MiddlePanel.RowData[i][5]   = " ";
			MiddlePanel.RowData[i][6]   = " ";
			MiddlePanel.RowData[i][7]   = " ";
			MiddlePanel.RowData[i][8]   = (String)VOQty.elementAt(i);
			MiddlePanel.RowData[i][9]   = (String)VORate.elementAt(i);
			MiddlePanel.RowData[i][10]  = (String)VODiscPer.elementAt(i);
			MiddlePanel.RowData[i][11]  = (String)VOCgstPer.elementAt(i);
			MiddlePanel.RowData[i][12]  = (String)VOSgstPer.elementAt(i);
			MiddlePanel.RowData[i][13]  = (String)VOIgstPer.elementAt(i);
			MiddlePanel.RowData[i][14]  = (String)VOCessPer.elementAt(i);
			MiddlePanel.RowData[i][15]  = common.getRound(dGross,2); // Basic
			MiddlePanel.RowData[i][16]  = common.getRound(dDisc,2);  // disc
			MiddlePanel.RowData[i][17]  = common.getRound(dCGSTVal,2); 
			MiddlePanel.RowData[i][18]  = common.getRound(dSGSTVal,2);
			MiddlePanel.RowData[i][19]  = common.getRound(dIGSTVal,2);
			MiddlePanel.RowData[i][20]  = common.getRound(dCessVal,2);
			MiddlePanel.RowData[i][21]  = common.getRound(dNet,2);
			MiddlePanel.RowData[i][22]  = (String)VODeptName.elementAt(i);
			MiddlePanel.RowData[i][23]  = (String)VOGroupName.elementAt(i);
			MiddlePanel.RowData[i][24]  = common.parseDate((String)VODueDate.elementAt(i));
			MiddlePanel.RowData[i][25]  = (String)VOUnitName.elementAt(i);
			MiddlePanel.RowData[i][26]  = (String)VRemarks.elementAt(i);
			MiddlePanel.RowData[i][27]  = (String)VLogBookNo.elementAt(i);
			MiddlePanel.RowData[i][28]  = (String)VDocId.elementAt(i);

			dTGross   = dTGross+dGross;
			dTDisc    = dTDisc+dDisc;
			dTCGST    = dTCGST+dCGSTVal;
			dTSGST    = dTSGST+dSGSTVal;
			dTIGST    = dTIGST+dIGSTVal;
			dTCess    = dTCess+dCessVal;
			dTNet     = dTNet+dNet;

            }
		    
            MiddlePanel.createComponents();

            // Setting Common Data
            TOrderNo.setText(SOrderNo);
            TDate.TDay.setText(SOrderDate.substring(6,8));
            TDate.TMonth.setText(SOrderDate.substring(4,6));
            TDate.TYear.setText(SOrderDate.substring(0,4));
            BSupplier.setText(SSupName);
            TSupCode.setText(SSupCode);
            TRef.setText(SRef);
            TAdvance.setText(SAdvance);
            TPayTerm.setText(SPayTerm);
            JCTo.setSelectedIndex(VToCode.indexOf(SToCode));
            JCThro.setSelectedIndex(VThroCode.indexOf(SThroCode));
            JCForm.setSelectedIndex(VFormCode.indexOf(SFormCode));
            MiddlePanel.MiddlePanel.LBasic.setText(common.getRound(dTGross,2));
            MiddlePanel.MiddlePanel.LDiscount.setText(common.getRound(dTDisc,2));
            MiddlePanel.MiddlePanel.LCGST.setText(common.getRound(dTCGST,2));
            MiddlePanel.MiddlePanel.LSGST.setText(common.getRound(dTSGST,2));
            MiddlePanel.MiddlePanel.LIGST.setText(common.getRound(dTIGST,2));
            MiddlePanel.MiddlePanel.LCess.setText(common.getRound(dTCess,2));

            MiddlePanel.MiddlePanel.TAdd.setText(SAdd);
            MiddlePanel.MiddlePanel.TLess.setText(SLess);
			dTNet = dTNet+common.toDouble(MiddlePanel.MiddlePanel.TAdd.getText())-common.toDouble(MiddlePanel.MiddlePanel.TLess.getText());
            MiddlePanel.MiddlePanel.LNet.setText(common.getRound(dTNet,2));
            MiddlePanel.MiddlePanel.calc();
            TOrderNo.setEditable(false);
            BSupplier.setEnabled(false);
      }
      public void getData()
      {
          String QString = " SELECT WorkOrder.OrderDate, "+
                           " "+SSupTable+".Name, WorkOrder.Descript,  "+
                           " WorkOrder.Qty, WorkOrder.Rate, "+
                           " WorkOrder.DiscPer, WorkOrder.Cgst, "+
                           " WorkOrder.Sgst, Dept.Dept_Name, "+
                           " Cata.Group_Name, Unit.Unit_Name, "+
                           " WorkOrder.DueDate, WorkOrder.ID, "+
                           " WorkOrder.Sup_Code, WorkOrder.Igst, "+
                           " WorkOrder.Advance, WorkOrder.ToCode, "+
                           " WorkOrder.ThroCode, WorkOrder.Plus, "+
                           " WorkOrder.Less, WorkOrder.PayTerms, "+
                           " WorkOrder.FormCode,WorkOrder.Reference, "+
                           " WorkOrder.RDCNo,WorkOrder.Remarks,WorkOrder.LogBookNo,"+
                           " WorkOrder.RdcSlNo,WorkOrder.WODesc,WorkOrder.DocId,WorkOrder.Cess, "+
                           " InvItems.ITEM_NAME,InvItems.HSNCODE,WorkOrder.HSNMATCODE "+
                           " FROM WorkOrder INNER JOIN "+SSupTable+" ON WorkOrder.Sup_Code="+SSupTable+".Ac_Code "+
                           " And WorkOrder.OrderNo="+SOrderNo+" And WorkOrder.Qty > 0 "+
                           " And WorkOrder.EntryStatus>0 And WorkOrder.MillCode="+iMillCode+
                           " INNER JOIN Dept ON WorkOrder.Dept_Code=Dept.Dept_code "+
                           " INNER JOIN Cata ON WorkOrder.Group_Code=Cata.Group_Code "+
                           " INNER JOIN Unit ON WorkOrder.Unit_Code=Unit.Unit_Code "+
                           " LEFT  JOIN InvItems ON InvItems.Item_Code=WorkOrder.HSNMATCODE "+
                           " ORDER BY WorkOrder.ID";
          try
          {
                if(theconnect==null)
                {
                     connect=ORAConnection.getORAConnection();
                     theconnect=connect.getConnection();
                }
                Statement stat   = theconnect.createStatement();
                ResultSet result = stat.executeQuery(QString);
                while(result.next())
                {
                        SOrderDate=result.getString(1);
                        SSupName  =result.getString(2);
                        VOName.addElement(result.getString(3));
                        VOQty.addElement(result.getString(4));
                        VORate.addElement(result.getString(5));
                        VODiscPer.addElement(result.getString(6));
                        VOCgstPer.addElement(result.getString(7));
                        VOSgstPer.addElement(result.getString(8));
                        VODeptName.addElement(result.getString(9));
                        VOGroupName.addElement(result.getString(10));
                        VOUnitName.addElement(result.getString(11));
                        VODueDate.addElement(result.getString(12));
                        VOId.addElement(result.getString(13));
                        SSupCode    = result.getString(14);
                        VOIgstPer.addElement(result.getString(15));
                        SAdvance    = result.getString(16);
                        SToCode     = result.getString(17);
                        SThroCode   = result.getString(18);
                        SAdd        = result.getString(19);
                        SLess       = result.getString(20);
                        SPayTerm    = result.getString(21);
                        SFormCode   = result.getString(22);
                        SRef        = result.getString(23);
                        VORDCNo.addElement(result.getString(24));
                        VRemarks.addElement(common.parseNull(result.getString(25)));
                        VLogBookNo.addElement(common.parseNull(result.getString(26)));
                        VRdcSlNo.addElement(result.getString(27));
                        VWODesc.addElement(common.parseNull(result.getString(28)));
						VDocId.addElement(common.parseNull(result.getString(29)));
						VOCessPer.addElement(result.getString(30));
						VItmeName.addElement(common.parseNull(result.getString(31)));
						VHsnCode.addElement(common.parseNull(result.getString(32)));
						VHsnMatCode.addElement(common.parseNull(result.getString(33)));
                }
                result.close();
                stat.close();
          }
          catch(Exception ex)
          {
                  System.out.println(ex);           
          }
      }
}
