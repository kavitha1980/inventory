package RDC;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.JFileChooser;

import guiutil.*;
import util.*;


public class WorkOrderGstUserSelection extends JInternalFrame
{
     protected     JLayeredPane Layer;

     JPanel        TopPanel,MiddlePanel,BottomPanel;
     JButton       BApply,BExit;
     Common        common = new Common();

     JTable        theTable;

     Vector        theVector,VUserName,VUserCode;
     Vector        VPrevName,VPrevCode;

     UserModel     theModel;
     WorkOrderGstDocumentationEntryFrame theFrame;
     String                        findstr = "";

     public  WorkOrderGstUserSelection(JLayeredPane Layer,WorkOrderGstDocumentationEntryFrame theFrame)
     {
          this.Layer     = Layer;
          this.theFrame  = theFrame;

          setVector();
          setUserVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setTabReport();
     }

     private void createComponents()
     {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();

          theModel       = new UserModel();
          theTable       = new JTable(theModel);

          TableColumnModel TC = theTable.getColumnModel();

          for(int i=0;i<theModel.ColumnName.length;i++)
          {
               TC        . getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
          }
          BExit          = new JButton("Exit");
          BApply         = new JButton("Apply");    
     }

     private void setLayouts()
     {
          setTitle("User Selection Frame ");
          setClosable(true);                    
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(50,50,350,400);


          TopPanel.setLayout(new BorderLayout());
          TopPanel.setBorder(new TitledBorder("UserDetails"));

          BottomPanel.setLayout(new FlowLayout());
          show();
     }
     private void addComponents()
     {

          TopPanel.add(new JScrollPane(theTable));

          BottomPanel.add(BApply);
          BottomPanel.add(BExit);

          getContentPane().add("Center",TopPanel);
          getContentPane().add("South",BottomPanel);
          BApply.setMnemonic('S');
          BExit.setMnemonic('E');
     }

     public void addListeners()
     {
          BApply      . addActionListener(new ActList());
          BExit       . addActionListener(new ActList());
          theTable    . addKeyListener(new KeyList());
     }


     private void setTabReport()
     {
          try
          {
               theModel.setNumRows(0);
     
               for(int i=0;i<theVector.size();i++)
               {
                    HashMap   theMap    =(HashMap)theVector.elementAt(i);
     
                    Vector Vect    =    new Vector();
                    Vect           .    addElement(String.valueOf(i+1));
                    Vect           .    addElement(common.parseNull((String)theMap.get("USERNAME")));
                    Vect           .    addElement(new Boolean(false));
     
                    theModel       . appendRow(Vect);
               }
          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
     }

     private class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               JTable theTable1 = (JTable)ke.getSource();
               int iRow = theTable1.getSelectedRow();
               int iCol = theTable1.getSelectedColumn();

              char lastchar=ke.getKeyChar();
              lastchar=Character.toUpperCase(lastchar);
              try
              {
                   if(ke.getKeyCode()==8)
                   {
                        findstr=findstr.substring(0,(findstr.length()-1));
                        setCursor(theTable1,iCol);
                   }
                   else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar>='0' && lastchar <= '9'))
                   {
                        findstr=findstr+lastchar;
                        setCursor(theTable1,iCol);
                   }
              }
              catch(Exception ex){}
          }

     }

     private void setCursor(JTable theTable1,int iCol)
     {
          UserModel theModel = (UserModel)theTable1.getModel();
          int index = -1;

          for(int i=0;i<theModel.getRows();i++)
          {
               String str = (String)theModel.getValueAt(i,iCol);

               if(str.startsWith(findstr))
               {
                    index = i;
                    break;
               }
          }

          if(index == -1)
               return;

          theTable.setRowSelectionInterval(index,index);
          Rectangle rect = theTable.getCellRect(index,0,true);
          theTable.scrollRectToVisible(rect);
     }

     

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BExit)
               {
                   removeFrame();
               }
               if(ae.getSource()==BApply)               
               {
                    if(CheckAnyOneSelected())
                    {
                        InsertUser();
                        removeFrame();
                    }
               }
          }
     }


     public void InsertUser()
     {
          for(int i=0;i<theVector.size();i++)
          {
               Boolean bValue      = (Boolean)theModel.getValueAt(i,2);

               if(bValue.booleanValue())
               {
                    String SUserName    = (String)theModel.getValueAt(i,1);

                    setUserVectors();
               }
          }
     }
     private boolean CheckAnyOneSelected()
     {
          int iCount = 0;

          for(int i=0;i<theVector.size();i++)
          {
               Boolean bValue      = (Boolean)theModel.getValueAt(i,2);

               if(bValue.booleanValue())
               {
                    return true;               
               }
          }
          JOptionPane.showMessageDialog(null,"Please Select Any one Check Box for Print","Information",JOptionPane.ERROR_MESSAGE);
          return false;
     }



     private Vector setVector()
     {
          theVector      =    new Vector();

          String QS      =    " Select UserName,UserCode from RawUser Where DocUser=1 Order by 1 ";

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
               Statement stat      = theConnection.createStatement();
               ResultSet result1   = stat.executeQuery(QS);
               ResultSetMetaData rsmd = result1.getMetaData();

               while(result1.next())
               {
                    HashMap   theMap    =    new HashMap();

                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         theMap.put(rsmd.getColumnName(i+1),result1.getString(i+1));
                    }
                    theVector.addElement(theMap);
               }
                    result1        .close();
                    stat           .close();

          }


          catch(Exception ex)
          {ex.printStackTrace();
          }
          return theVector;
     }

     private void removeFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex){}
     }


     private void setUserVector()
     {
          VUserName                    = new Vector();
          VUserCode                    = new Vector();
          String QS =    " Select UserName,UserCode From RawUser Where DocUser=1  Order by 1 ";

          System.out.println(QS);

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
               Statement stat   = theConnection.createStatement();
               ResultSet rs     = stat.executeQuery(QS);
               while(rs.next())
               {
                    VUserName          . addElement(common.parseNull(rs.getString(1)));
                    VUserCode          . addElement(common.parseNull(rs.getString(2)));
               }
               rs                       . close();
               stat.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setPrevVector()
     {
          VPrevName                    = new Vector();
          VPrevCode                    = new Vector();

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","admin","admin");
               Statement stat   = theConnection.createStatement();
               ResultSet rs     = stat.executeQuery("  Select DocName,UserCode From DocUser ");
               while(rs.next())
               {
                    VPrevName          . addElement(common.parseNull(rs.getString(1)));
                    VPrevCode          . addElement(common.parseNull(rs.getString(2)));
               }
               rs                       . close();
               stat.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void setUserVectors()
     {
          Vector theVector1 = new Vector();

          for(int i=0;i<theVector.size();i++)
          {
               Boolean bValue      = (Boolean)theModel.getValueAt(i,2);

               if(bValue.booleanValue())
               {
                    String SUserName    = (String)theModel.getValueAt(i,1);

                    theVector1          . addElement(SUserName);
               }
          }

          theFrame.setUserVector(theVector1);
     }



     private String getPrevName(String SDoc)
     {
          int  iIndex    =    0;

          iIndex         =    VPrevName.indexOf(SDoc);

          if(iIndex!=-1)
               return    (String)VPrevName.elementAt(iIndex);
          return    "";
     }

     private String getPrevCode(String SCode)
     {
          int  iIndex    =    0;

          iIndex         =    VPrevCode.indexOf(SCode);

          if(iIndex!=-1)
               return    (String)VPrevCode.elementAt(iIndex);
          return    "";
     }





}
