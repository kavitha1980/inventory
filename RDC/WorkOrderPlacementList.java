package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkOrderPlacementList extends JInternalFrame
{
      JPanel     TopPanel;
      JPanel     MiddlePanel;
      JPanel     BottomPanel;
 
      String     SDate;
      JButton    BApply;
      DateField  TDate;

      JLayeredPane DeskTop;
      StatusPanel SPanel;
      int iMillCode;
      String SSupTable;

      Common common = new Common();
      ORAConnection connect;
      Connection theconnect;
      TabReport tabreport;

      Vector VSupName,VRDCNo,VDescript,VRecQty,VWOQty;

      String ColumnData[] = {"Supplier","RDC No","Descript","Recd Qty","Work Order Qty"};
      String ColumnType[] = {"S"       ,"N"     ,"S"       ,"N"       ,"N"             };
      Object RowData[][];


      WorkOrderPlacementList(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode,String SSupTable)
      {
          super("RDC Recd and Pending For WorkOrder Placement As On ");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
      }

      public void createComponents()
      {
          TopPanel    = new JPanel();
          MiddlePanel = new JPanel();
          BottomPanel = new JPanel();
 
          TDate    = new DateField();
          BApply   = new JButton("Apply");
 
          TDate.setTodayDate();
      }

      public void setLayouts()
      {
          TopPanel.setLayout(new GridLayout(2,3));
          MiddlePanel.setLayout(new BorderLayout());
            
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
      }
                                                                        
      public void addComponents()
      {
          TopPanel.add(new JLabel("As On "));
          TopPanel.add(TDate);
          TopPanel.add(BApply);
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));
 
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
      }
      public void addListeners()
      {
          BApply.addActionListener(new ApplyList());
      }
      public class ApplyList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setDataIntoVector();
                  setVectorIntoRowData();
                  try
                  {
                        MiddlePanel.remove(tabreport);
                  }
                  catch(Exception ex){}

                  tabreport = new TabReport(RowData,ColumnData,ColumnType);
                  tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
                  MiddlePanel.add("Center",tabreport);
                  MiddlePanel.updateUI();
            }
      }
      public void removeHelpFrame()
      {
            try
            {
                  DeskTop.remove(this);
                  DeskTop.repaint();
                  DeskTop.updateUI();
            }
            catch(Exception ex) { }
      }

      private void setDataIntoVector()
      {
            VSupName       = new Vector();
            VRDCNo         = new Vector();
            VDescript      = new Vector();
            VRecQty        = new Vector();
            VWOQty         = new Vector();

            String QS  = "";

            QS  = " SELECT "+SSupTable+".NAME, GateInwardRDC.RDCNo, GateInwardRDC.Descript, GateInwardRDC.RecQty, GateInwardRDC.WOQty "+
                  " FROM GateInwardRDC INNER JOIN "+SSupTable+" ON GateInwardRDC.Sup_Code = "+SSupTable+".AC_CODE "+
                  " And GateInwardRDC.GIDate <= '"+TDate.toNormal()+"' And GateInwardRDC.WOStatus=1 And GateInwardRDC.OrderNo=0 And GateInwardRDC.MillCode="+iMillCode+" ORDER BY "+SSupTable+".NAME,GateInwardRDC.RDCNo,GateInwardRDC.Descript ";

            try
            {
                  if(theconnect==null)
                  {
                       connect=ORAConnection.getORAConnection();
                       theconnect=connect.getConnection();
                  }
                  Statement stat  = theconnect.createStatement();
                  ResultSet theResult = stat.executeQuery(QS);
                  while(theResult.next())
                  {
                        VSupName       .addElement(theResult.getString(1));
                        VRDCNo         .addElement(theResult.getString(2));
                        VDescript      .addElement(theResult.getString(3));
                        VRecQty        .addElement(theResult.getString(4));
                        VWOQty         .addElement(theResult.getString(5));
                  }
                  theResult.close();
                  stat.close();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      private void setVectorIntoRowData()
      {
            RowData = new Object[VSupName.size()][ColumnData.length];
            for(int i=0;i<VSupName.size();i++)
            {
                  RowData[i][0] = (String)VSupName.elementAt(i);
                  RowData[i][1] = (String)VRDCNo.elementAt(i);
                  RowData[i][2] = (String)VDescript.elementAt(i);
                  RowData[i][3] = (String)VRecQty.elementAt(i);
                  RowData[i][4] = (String)VWOQty.elementAt(i);
            }
      }

}
