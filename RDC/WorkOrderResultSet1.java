package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkOrderResultSet1
{
      String    SOrderNo="";
      String    SOrderDate="";

      String    SToCode    ="0";
      String    SThroCode  ="0";
      String    SFormCode  ="0";

      String    SSupName   ="";
      String    SSupCode   ="";
      String    SRef       ="";
      String    SAdvance   ="";
      String    SPayTerm   ="";
      String    SAdd       ="";
      String    SLess      ="";
      Vector    VOName,VOQty,VORate,VODiscPer,VOCenVatPer,VOTaxPer,VOSurPer;
      Vector    VODeptName,VOGroupName,VOUnitName,VODueDate,VORDCNo,VRemarks,VLogBookNo,VRdcSlNo,VWODesc,VDocId;
      Vector    VOId;

      NextField  TOrderNo;
      DateField  TDate;
      JButton    BSupplier;
      JTextField TSupCode;
      JTextField TRef;
      JTextField TAdvance;
      JTextField TPayTerm;
      JComboBox  JCTo,JCThro,JCForm;
      WOMiddlePanel MiddlePanel;
      Vector VToCode,VThroCode,VFormCode;
      WorkOrderFrame workorderframe;
      int iMillCode;
      String SSupTable;

      Vector VDept,VDeptCode,VUnit,VUnitCode,VGroup,VGroupCode;
      Common common = new Common();
      ORAConnection connect;
      Connection theconnect;
	WorkOrderResultSet1(){

        }

      WorkOrderResultSet1(String SOrderNo,NextField TOrderNo,DateField TDate,JButton BSupplier,JTextField TSupCode,JTextField TRef,JTextField TAdvance,JTextField TPayTerm,JComboBox JCTo,JComboBox JCThro,JComboBox JCForm,WOMiddlePanel MiddlePanel,Vector VToCode,Vector VThroCode,Vector VFormCode,WorkOrderFrame workorderframe,int iMillCode,String SSupTable)
      {
            this.SOrderNo    = SOrderNo;
            this.TOrderNo    = TOrderNo;
            this.TDate       = TDate;
            this.BSupplier   = BSupplier;
            this.TSupCode    = TSupCode;
            this.TRef        = TRef;
            this.TAdvance    = TAdvance;
            this.TPayTerm    = TPayTerm;
            this.JCTo        = JCTo;
            this.JCThro      = JCThro;
            this.JCForm      = JCForm;
            this.MiddlePanel = MiddlePanel;
            this.VToCode     = VToCode;
            this.VThroCode   = VThroCode;
            this.VFormCode   = VFormCode;
            this.workorderframe = workorderframe;
            this.iMillCode   = iMillCode;
            this.SSupTable   = SSupTable;
            
            VOName      = new Vector();
            VOQty       = new Vector();
            VORate      = new Vector();
            VODiscPer   = new Vector();
            VOCenVatPer = new Vector();
            VOTaxPer    = new Vector();
            VOSurPer    = new Vector();
            VODeptName  = new Vector();
            VOGroupName = new Vector();
            VOUnitName  = new Vector();
            VODueDate   = new Vector();
            VOId        = new Vector();
            VORDCNo     = new Vector();
            VRemarks    = new Vector();
            VLogBookNo  = new Vector();
            VRdcSlNo    = new Vector();
            VWODesc     = new Vector();

	    VDocId      = new Vector();

            getData();
            setData();
      }
      public void setData()
      {
            MiddlePanel.RowData     = new Object[VOName.size()][25];
            MiddlePanel.SlData      = new Object[VOName.size()];
            MiddlePanel.IdData      = new Object[VOName.size()];

            for(int i=0;i<VOName.size();i++)
            {
                MiddlePanel.SlData[i]      = (String)VRdcSlNo.elementAt(i);
                MiddlePanel.IdData[i]      = (String)VOId.elementAt(i);
                MiddlePanel.RowData[i][0]  = (String)VOName.elementAt(i);
                MiddlePanel.RowData[i][1]  = (String)VORDCNo.elementAt(i);
                MiddlePanel.RowData[i][2]  = (String)VWODesc.elementAt(i);
                MiddlePanel.RowData[i][3]  = " ";
                MiddlePanel.RowData[i][4]  = " ";
                MiddlePanel.RowData[i][5]  = " ";
                MiddlePanel.RowData[i][6]  = (String)VOQty.elementAt(i);
                MiddlePanel.RowData[i][7]  = (String)VORate.elementAt(i);
                MiddlePanel.RowData[i][8]  = (String)VODiscPer.elementAt(i);
                MiddlePanel.RowData[i][9]  = (String)VOCenVatPer.elementAt(i);
                MiddlePanel.RowData[i][10] = (String)VOTaxPer.elementAt(i);
                MiddlePanel.RowData[i][11] = (String)VOSurPer.elementAt(i);
                MiddlePanel.RowData[i][12] = " ";
                MiddlePanel.RowData[i][13] = " ";
                MiddlePanel.RowData[i][14] = " ";
                MiddlePanel.RowData[i][15] = " ";
                MiddlePanel.RowData[i][16] = " ";
                MiddlePanel.RowData[i][17] = " ";
                MiddlePanel.RowData[i][18] = (String)VODeptName.elementAt(i);
                MiddlePanel.RowData[i][19] = (String)VOGroupName.elementAt(i);
                MiddlePanel.RowData[i][20] = common.parseDate((String)VODueDate.elementAt(i));
                MiddlePanel.RowData[i][21] = (String)VOUnitName.elementAt(i);
                MiddlePanel.RowData[i][22] = (String)VRemarks.elementAt(i);
                MiddlePanel.RowData[i][23] = (String)VLogBookNo.elementAt(i);
                MiddlePanel.RowData[i][24] = (String)VDocId.elementAt(i);
            }
            MiddlePanel.createComponents();

            // Setting Common Data
            TOrderNo.setText(SOrderNo);
            TDate.TDay.setText(SOrderDate.substring(6,8));
            TDate.TMonth.setText(SOrderDate.substring(4,6));
            TDate.TYear.setText(SOrderDate.substring(0,4));
            BSupplier.setText(SSupName);
            TSupCode.setText(SSupCode);
            TRef.setText(SRef);
            TAdvance.setText(SAdvance);
            TPayTerm.setText(SPayTerm);
            JCTo.setSelectedIndex(VToCode.indexOf(SToCode));
            JCThro.setSelectedIndex(VThroCode.indexOf(SThroCode));
            JCForm.setSelectedIndex(VFormCode.indexOf(SFormCode));
            MiddlePanel.MiddlePanel.TAdd.setText(SAdd);
            MiddlePanel.MiddlePanel.TLess.setText(SLess);
            MiddlePanel.MiddlePanel.calc();
            TOrderNo.setEditable(false);
            BSupplier.setEnabled(false);
      }
      public void getData()
      {
          String QString = " SELECT WorkOrder.OrderDate, "+
                           " "+SSupTable+".Name, WorkOrder.Descript,  "+
                           " WorkOrder.Qty, WorkOrder.Rate, "+
                           " WorkOrder.DiscPer, WorkOrder.CenVatPer, "+
                           " WorkOrder.TaxPer, Dept.Dept_Name, "+
                           " Cata.Group_Name, Unit.Unit_Name, "+
                           " WorkOrder.DueDate, WorkOrder.ID, "+
                           " WorkOrder.Sup_Code, WorkOrder.SurPer, "+
                           " WorkOrder.Advance, WorkOrder.ToCode, "+
                           " WorkOrder.ThroCode, WorkOrder.Plus, "+
                           " WorkOrder.Less, WorkOrder.PayTerms, "+
                           " WorkOrder.FormCode,WorkOrder.Reference, "+
                           " WorkOrder.RDCNo,WorkOrder.Remarks,WorkOrder.LogBookNo,"+
                           " WorkOrder.RdcSlNo,WorkOrder.WODesc,WorkOrder.DocId "+
                           " FROM WorkOrder INNER JOIN "+SSupTable+" ON WorkOrder.Sup_Code="+SSupTable+".Ac_Code "+
                           " And WorkOrder.OrderNo="+SOrderNo+" And WorkOrder.Qty > 0 "+
                           " And WorkOrder.EntryStatus>0 And WorkOrder.MillCode="+iMillCode+
                           " INNER JOIN Dept ON WorkOrder.Dept_Code=Dept.Dept_code "+
                           " INNER JOIN Cata ON WorkOrder.Group_Code=Cata.Group_Code "+
                           " INNER JOIN Unit ON WorkOrder.Unit_Code=Unit.Unit_Code "+
                           " ORDER BY WorkOrder.ID";
          try
          {
                if(theconnect==null)
                {
                     connect=ORAConnection.getORAConnection();
                     theconnect=connect.getConnection();
                }
                Statement stat   = theconnect.createStatement();
                ResultSet result = stat.executeQuery(QString);
                while(result.next())
                {
                        SOrderDate=result.getString(1);
                        SSupName  =result.getString(2);
                        VOName.addElement(result.getString(3));
                        VOQty.addElement(result.getString(4));
                        VORate.addElement(result.getString(5));
                        VODiscPer.addElement(result.getString(6));
                        VOCenVatPer.addElement(result.getString(7));
                        VOTaxPer.addElement(result.getString(8));
                        VODeptName.addElement(result.getString(9));
                        VOGroupName.addElement(result.getString(10));
                        VOUnitName.addElement(result.getString(11));
                        VODueDate.addElement(result.getString(12));
                        VOId.addElement(result.getString(13));
                        SSupCode    = result.getString(14);
                        VOSurPer.addElement(result.getString(15));
                        SAdvance    = result.getString(16);
                        SToCode     = result.getString(17);
                        SThroCode   = result.getString(18);
                        SAdd        = result.getString(19);
                        SLess       = result.getString(20);
                        SPayTerm    = result.getString(21);
                        SFormCode   = result.getString(22);
                        SRef        = result.getString(23);
                        VORDCNo.addElement(result.getString(24));
                        VRemarks.addElement(common.parseNull(result.getString(25)));
                        VLogBookNo.addElement(common.parseNull(result.getString(26)));
                        VRdcSlNo.addElement(result.getString(27));
                        VWODesc.addElement(common.parseNull(result.getString(28)));

			VDocId.addElement(common.parseNull(result.getString(29)));
                }
                result.close();
                stat.close();
          }
          catch(Exception ex)
          {
                  System.out.println(ex);           
          }
      }
}
