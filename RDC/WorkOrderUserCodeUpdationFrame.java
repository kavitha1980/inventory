package RDC;

import util.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.Vector;
import java.sql.*;
import java.awt.event.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkOrderUserCodeUpdationFrame extends JInternalFrame
{
     JPanel TopPanel,MiddlePanel,BottomPanel;
     String mac      = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
     String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

     Common common   = new Common();

     Vector VOrderNo,VOrderSlNo,VRdcNo,VRdcSlNo,VMillCode,VId;

     JTextArea     TInfo;

     JButton   BApply;
     JLayeredPane layer;

     Connection theConnection=null;

     public WorkOrderUserCodeUpdationFrame(JLayeredPane layer)
     {
          super("Utility for Updating WorkOrder UserCode");
          this.layer      = layer;

          updateLookAndFeel();
          setSize(500,500);
          
          TopPanel    = new JPanel(true);
          MiddlePanel = new JPanel(true);
          MiddlePanel.setLayout(new BorderLayout());

          TInfo       = new JTextArea(50,50);
          BApply      = new JButton("Apply");

          TopPanel.add(BApply);
          MiddlePanel.add(TInfo);

          BApply.addActionListener(new AppList());

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          setVisible(true);
          setClosable(true);
     }
      private class AppList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {

               BApply.setEnabled(false);
               try
               {
                    MiddlePanel.removeAll();
               }catch(Exception ex){System.out.println(ex);}
               MiddlePanel.add(TInfo);
               TInfo.append("Processing ....."+"\n");

               TInfo.append("Fetching Stock Data ....."+"\n");
               getRDCData();

               if(VRdcNo.size()>0)
               {
                    TInfo.append("Updation is going on ....."+"\n");
                    updateRDCData();
               }

               TInfo.append("Updation Over .....");
               removeFrame();
          }
      }

     private void getRDCData()
     {
          VOrderNo      = new Vector();
          VOrderSlNo    = new Vector();
          VRdcNo        = new Vector();
          VRdcSlNo      = new Vector();
          VMillCode     = new Vector();
          VId           = new Vector();


          String QS = " Select OrderNo,SlNo,RdcNo,RdcSlNo,MillCode,Id from WorkOrder Where OrderDate>=20110401 and RDCNo>0 "+
                      " Order by MillCode,OrderNo,Id ";

          try
          {

               if(theConnection==null)
               {
                    ORAConnection jdbc       = ORAConnection.getORAConnection();
                    theConnection            = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    VOrderNo.addElement(common.parseNull((String)theResult.getString(1)));
                    VOrderSlNo.addElement(common.parseNull((String)theResult.getString(2)));
                    VRdcNo.addElement(common.parseNull((String)theResult.getString(3)));
                    VRdcSlNo.addElement(common.parseNull((String)theResult.getString(4)));
                    VMillCode.addElement(common.parseNull((String)theResult.getString(5)));
                    VId.addElement(common.parseNull((String)theResult.getString(6)));
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void updateRDCData()
     {
          try
          {
               if(theConnection==null)
               {
                    ORAConnection  jdbc           = ORAConnection.getORAConnection();
                    theConnection                 = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();

               for(int i=0;i<VRdcNo.size();i++)
               {
                    String SOrderNo   = (String)VOrderNo.elementAt(i);
                    String SOrderSlNo = (String)VOrderSlNo.elementAt(i);
                    String SRdcNo     = (String)VRdcNo.elementAt(i);
                    String SRdcSlNo   = (String)VRdcSlNo.elementAt(i);
                    String SMillCode  = (String)VMillCode.elementAt(i);
                    String SId        = (String)VId.elementAt(i);

                    String SMemoUserCode = getMemoUserCode(stat,SRdcNo,SRdcSlNo,SMillCode);

                    String QS = " Update WorkOrder Set MemoAuthUserCode="+SMemoUserCode+
                                " Where OrderNo="+SOrderNo+
                                " and SlNo="+SOrderSlNo+
                                " and RdcNo="+SRdcNo+
                                " and RdcSlNo="+SRdcSlNo+
                                " and MillCode="+SMillCode+
                                " and Id="+SId;

                    stat.execute(QS);
               }
               stat.close();
               JOptionPane.showMessageDialog(null,VRdcNo.size()+" Rows updated ");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               JOptionPane.showMessageDialog(null,"Updation Error:"+ex);
               removeFrame();
          }

     }

     private String getMemoUserCode(Statement stat,String SRdcNo,String SRdcSlNo,String SMillCode)
     {

          String SMemoUserCode = "1";

          try
          {
               String QS = " Select MemoAuthUserCode from RDC "+
                           " Where DocType=0 and RdcNo="+SRdcNo+
                           " and SlNo="+SRdcSlNo+
                           " and MillCode="+SMillCode;

               ResultSet theResult      = stat.executeQuery(QS);
               while(theResult.next())
               {
                    SMemoUserCode = common.parseNull((String)theResult.getString(1));
               }
               theResult.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

          if(common.toInt(SMemoUserCode)<=0)
          {
               SMemoUserCode = "1";
          }
          return SMemoUserCode;
     }

     private void removeFrame()
     {
          layer.remove(this);
          layer.repaint();
          layer.updateUI();
     }
     private void updateLookAndFeel()
     {
          try
          {
               UIManager.setLookAndFeel(windows);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }                    

     }
     
}



