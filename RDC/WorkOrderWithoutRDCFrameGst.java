package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkOrderWithoutRDCFrameGst extends JInternalFrame
{
      Connection theConnect = null;
     
     JPanel           TopPanel,TopLeft,TopRight,BottomPanel;
     WorkOrderWithoutRDCPanelGst MiddlePanel;

     JButton          BOk,BCancel,BSupplier;

     NextField        TOrderNo;
     JTextField       TSupCode;
     MyTextField      TRef,TPayTerm;
     NextField        TAdvance;
     DateField        TDate;
     MyComboBox       JCTo,JCThro,JCForm;
     String           SOrderNo="";
	 String 		  SSupType ="",SStateCode="";
     JLayeredPane     DeskTop;
     StatusPanel      SPanel;
     int              iUserCode;
     int              iMillCode;
     String           SSupTable,SYearCode;

     Common common    = new Common();
     ORAConnection connect;
     Connection theconnect;

     Vector           VTo,VThro,VToCode,VThroCode,VFormCode,VForm;

     int       iMaxSlNo  = 0;
     int       iCount=0;

     WorkOrderWithoutRDCFrameGst(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable,String SYearCode)
     {
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();		   	
          show();
     }
     public void createComponents()
     {
          TopPanel    = new JPanel();
          TopLeft     = new JPanel();
          TopRight    = new JPanel();
          BottomPanel = new JPanel();

          BOk         = new JButton("Save");
          BCancel     = new JButton("Abort");

          BSupplier   = new JButton("Supplier");

          TOrderNo    = new NextField();
          TDate       = new DateField();
          TSupCode    = new JTextField();
          TAdvance    = new NextField(10);
          TPayTerm    = new MyTextField(40);
          TRef        = new MyTextField(30);

          getVTo();

          JCTo        = new MyComboBox(VTo);
          JCThro      = new MyComboBox(VThro);
          JCForm      = new MyComboBox(VForm);

          TDate.setTodayDate();

          TDate.setEditable(false);
          TOrderNo.setEditable(false);
          BSupplier.setMnemonic('U');
          BCancel.setMnemonic('A');
          BOk.setMnemonic('S');
     }

     public void setLayouts()
     {
          TopPanel.setLayout(new GridLayout(1,2));
          TopLeft.setLayout(new GridLayout(5,2));
          TopRight.setLayout(new GridLayout(5,2));

          setTitle("Work Order Without RDC Placement (GST)");

          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,790,500);

          getContentPane()    .setLayout(new BorderLayout());
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          TopLeft   .add(new JLabel("Order No"));
          TopLeft   .add(TOrderNo);

          TopLeft   .add(new JLabel("Order Date"));
          TopLeft   .add(TDate);

          TopLeft   .add(new JLabel("Reference"));
          TopLeft   .add(TRef);

          TopLeft   .add(new JLabel("Supplier"));
          TopLeft   .add(BSupplier);

          TopRight  .add(new JLabel("Book To"));
          TopRight  .add(JCTo);

          TopRight  .add(new JLabel("Book Thro"));
          TopRight  .add(JCThro);

          TopRight  .add(new JLabel("Form No"));
          TopRight  .add(JCForm);

          TopRight  .add(new JLabel("Advance"));
          TopRight  .add(TAdvance);

          TopRight  .add(new JLabel("Payment Terms"));
          TopRight  .add(TPayTerm);

          TopPanel  .add(TopLeft);
          TopPanel  .add(TopRight);

          TopLeft   .setBorder(new TitledBorder("Order Block-I"));
          TopRight  .setBorder(new TitledBorder("Order Block-II"));

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);
          BottomPanel.add(new JLabel("Press F5 Key to select service"));

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          TOrderNo.setText(getNextOrderNo());

          //setMiddlePanel();
     }

     public void addListeners()
     {
          BSupplier.addActionListener(new SupplierSearchGst(DeskTop,TSupCode,SSupTable,this));
          BOk.addActionListener(new ActList());
          BCancel.addActionListener(new CanList());
     }

     public class CanList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {				
               removeHelpFrame();
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(common.toInt(TOrderNo.getText())>0)
               {
                    if(checkData())
                    {
                         iCount=0;
                         BOk.setEnabled(true);
                         insertOrderDetails();
                         removeHelpFrame();
                         if(iCount>0)
                             updateOrderNo();
                    }
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void insertOrderDetails()
     {

          String QString = "Insert Into WorkOrder (ID,OrderNo,OrderDate,Sup_Code,Reference,Advance,PayTerms,ToCode,ThroCode,FormCode,Descript,WODesc,RDCNo,Qty,Rate,DiscPer,Disc,CGST,CGSTVAL,SGST,SGSTVAL,IGST,IGSTVAL,CESS,CESSVAL,Net,Plus,Less,Misc,Unit_Code,Dept_Code,Group_Code,DueDate,SlNo,UserCode,ModiDate,RDCSlNo,MillCode,LogBookNo,Remarks,EntryStatus,DocId,MemoAuthUserCode,HSNMATCODE) Values (";

          int iSlNo = 0;

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();
               TOrderNo.setText(getNextOrderNo());  

               iMaxSlNo = 0;

               int k = iMaxSlNo;

               Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
               String SAdd       = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess      = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm        = common.toDouble(SAdd)-common.toDouble(SLess);
               double dBasic     = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio     = dpm/dBasic;

               for(int i=0;i<FinalData.length;i++)
               {
                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SDueDate   = MiddlePanel.MiddlePanel.getDueDate(i,TDate);
		    		//String sMatCode   = MiddlePanel.MiddlePanel.getItemCode(i);
					

                    dBasic            = common.toDouble(((String)FinalData[i][11]).trim());
					double dqty 	  = common.toDouble(((String)FinalData[i][4]).trim());
                    if(dBasic == 0)
                         continue;

                    String SMisc      = common.getRound(dBasic*dRatio,3);

                    String SEntryStatus="2";

                    int iMemoUserCode = 1;

                    String SDesc = (((String)FinalData[i][2]).trim()).toUpperCase();
	            SDesc = common.getNarration(SDesc);

		    String sMatCode  = ((String)MiddlePanel.MiddlePanel.MaterialCodeList.get(i)).trim();

                    String QS1 = QString;
                    QS1 = QS1+"WorkOrder_Seq.nextval,";
                    QS1 = QS1+"0"+TOrderNo.getText()+",";
                    QS1 = QS1+"'"+TDate.toNormal()+"',";
                    QS1 = QS1+"'"+TSupCode.getText()+"',";
                    QS1 = QS1+"'"+(TRef.getText()).toUpperCase()+"',";    
                    QS1 = QS1+"0"+TAdvance.getText()+",";
                    QS1 = QS1+"'"+(TPayTerm.getText()).toUpperCase()+"',";
                    QS1 = QS1+"0"+(String)VToCode.elementAt(JCTo.getSelectedIndex())+",";
                    QS1 = QS1+"0"+(String)VThroCode.elementAt(JCThro.getSelectedIndex())+",";
                    QS1 = QS1+"0"+(String)VFormCode.elementAt(JCForm.getSelectedIndex())+",";
                    QS1 = QS1+"'"+SDesc+"',";
                    QS1 = QS1+"'"+SDesc+"',";
                    QS1 = QS1+"0"+((String)FinalData[i][3]).trim()+",";		//RDc No
					if(dqty>0){
                    		QS1 = QS1+"0"+((String)FinalData[i][4]).trim()+",";     //Qty
					}
					else{
						    QS1 = QS1+"0,";     //Qty
					}
                    QS1 = QS1+"0"+((String)FinalData[i][5]).trim()+",";		//Rate
                    QS1 = QS1+"0"+((String)FinalData[i][6]).trim()+",";		//DiscPer
                    QS1 = QS1+"0"+((String)FinalData[i][12]).trim()+",";	//Disc
                    QS1 = QS1+"0"+((String)FinalData[i][7]).trim()+",";		//CGST
                    QS1 = QS1+"0"+((String)FinalData[i][13]).trim()+",";	//CGSTVal
                    QS1 = QS1+"0"+((String)FinalData[i][8]).trim()+",";		//SGST
                    QS1 = QS1+"0"+((String)FinalData[i][14]).trim()+",";	//SGSTVal
                    QS1 = QS1+"0"+((String)FinalData[i][9]).trim()+",";		//IGST
                    QS1 = QS1+"0"+((String)FinalData[i][15]).trim()+",";	//IGSTVal
                    QS1 = QS1+"0"+((String)FinalData[i][10]).trim()+",";	//CESS
                    QS1 = QS1+"0"+((String)FinalData[i][16]).trim()+",";	//CESSVal
                    QS1 = QS1+"0"+((String)FinalData[i][17]).trim()+",";    	//Net
                    QS1 = QS1+"0"+SAdd+",";
                    QS1 = QS1+"0"+SLess+",";
                    QS1 = QS1+"0"+SMisc+",";
                    QS1 = QS1+"0"+SUnitCode+",";
                    QS1 = QS1+"0"+SDeptCode+",";
                    QS1 = QS1+"0"+SGroupCode+",";
                    QS1 = QS1+"'"+SDueDate+"',";
                    QS1 = QS1+(k+1)+",";
                    QS1 = QS1+"0"+iUserCode+",";
                    QS1 = QS1+"'"+common.getServerDateTime2()+"',";
                    QS1 = QS1+iSlNo+",";
                    QS1 = QS1+iMillCode+",";
                    QS1 = QS1+"0"+((String)FinalData[i][23]).trim()+",";
                    QS1 = QS1+"'"+(((String)FinalData[i][22]).trim()).toUpperCase()+"',";
                    QS1 = QS1+"0"+SEntryStatus+",";
                    QS1 = QS1+"0"+((String)FinalData[i][24]).trim()+",";
                    QS1 = QS1+"0"+iMemoUserCode+",";
					QS1 = QS1+"'"+sMatCode+"' )";


                    stat.executeUpdate(QS1);
                    k++;
                    iCount++;
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void updateOrderNo()
     {
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();

               String QS1 = " Update Config"+iMillCode+""+SYearCode+" Set MaxNo=MaxNo+1 Where ID=11 ";
               stat.executeUpdate(QS1);
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public String getNextOrderNo()
     {
          String QS = "";

          QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" "+
               " Where ID=11 ";

          String SOrderNo = String.valueOf(common.toInt(common.getID(QS))+1);
          return SOrderNo;
     }

     public void getVTo()
     {
          VTo       = new Vector();
          VToCode   = new Vector();
          VThro     = new Vector();
          VThroCode = new Vector();
          VFormCode = new Vector();
          VForm     = new Vector();

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();
               ResultSet res = stat.executeQuery("Select ToName,ToCode From BookTo Order By 1");
               while(res.next())
               {
                    VTo.addElement(res.getString(1));
                    VToCode.addElement(res.getString(2));
               }
               res.close();
               res = stat.executeQuery("Select ThroName,ThroCode From BookThro Order By 1");
               while(res.next())
               {
                    VThro.addElement(res.getString(1));
                    VThroCode.addElement(res.getString(2));
               }
               res.close();
               res = stat.executeQuery("Select FormNo,FormCode From STForm Order By 1");
               while(res.next())
               {
                    VForm.addElement(res.getString(1));
                    VFormCode.addElement(res.getString(2));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setMiddlePanel()
     {
          try
          {
				getPartyGst();
				if(SSupType.equals("") || SSupType.equals("0"))
				{
					JOptionPane.showMessageDialog(null,"Gst Classification Not Updated for Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
				}
				else if(SStateCode.equals("") || SStateCode.equals("0"))
				{
					JOptionPane.showMessageDialog(null,"State Data Not Updated for Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
				}
				else if(!TSupCode.getText().equals("")){
	               MiddlePanel = new WorkOrderWithoutRDCPanelGst(DeskTop,iMillCode,TSupCode.getText());
				   BSupplier.setEnabled(false);
    	           getContentPane().add(MiddlePanel,BorderLayout.CENTER);
	               DeskTop.updateUI();
				}
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }          

     public boolean checkData()
     {
          String SOrderDate = TDate.toNormal();
          String SSupCode   = TSupCode.getText();

          int iRows=0;

          if(SOrderDate.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Order Date Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TDate.TDay.requestFocus();
               return false;
          }
          if(SSupCode.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Select Supplier","Error",JOptionPane.ERROR_MESSAGE);
               BSupplier.setEnabled(true);
               BSupplier.requestFocus();
               return false;
          }
          try
          {
               iRows    = MiddlePanel.MiddlePanel.getRows();
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"No Materials Selected","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          int iRowCount=0;

          int iDescLength = getDescLength();

          Object FData[][] = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<FData.length;i++)
          {
               String SDueDate   = MiddlePanel.MiddlePanel.getDueDate(i,TDate);
			   String Service  = ((String)FData[i][0]).trim();
               String SHsnCode = ((String)FData[i][1]).trim();

               if(common.toInt(SDueDate)<common.toInt((String)TDate.toNormal()))
               {
                    JOptionPane.showMessageDialog(null,"Due Date Grater than Order Date","Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               String SDesc = ((String)FData[i][2]).trim();
               double dQty  = common.toDouble((String)FData[i][4]);
               double dRate = common.toDouble((String)FData[i][5]);

               if(SDesc.length()>iDescLength)
               {
                    JOptionPane.showMessageDialog(null,"Description Must be below "+iDescLength+" Characters (Current-"+SDesc.length()+")","Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }
			   if(SHsnCode.equals("") && dRate>0)
               {
                    JOptionPane.showMessageDialog(null,"Select HsnCode @Row - "+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }
			   if(i==0){
					if(SDesc.length()>0 && dQty>0 && dRate>0)
               		{
                    	iRowCount++;
               		}
			   }
			   if(i>=1){
					if(SDesc.length()>0 && dRate>0)
               		{
                    	iRowCount++;
               		}
			   }               
          }

          if(iRowCount<=0)
          {
               JOptionPane.showMessageDialog(null,"No Data Entered","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }
          return true;
     }

     public int getDescLength()
     {
          int iLength=0;

          String QS = " Select Data_Length from all_tab_columns Where table_name='WORKORDER' and column_name='DESCRIPT' ";

          iLength = common.toInt(common.getID(QS));
           
          return iLength;
     }
	private void getPartyGst(){
		SSupType = "";SStateCode="";
		try{
			StringBuffer sb     =new StringBuffer(); 
			sb.append(" select decode(PartyMaster.StateCode,0,'0',decode(PartyMaster.CountryCode,'61',nvl(State.GstStateCode,0),'999')) as GstStateCode,nvl(PartyMaster.GstPartyTypeCode,0) as GstPartyTypeCode from PartyMaster ");
			sb.append(" Inner Join State on PartyMaster.StateCode=State.StateCode ");
			sb.append(" where PartyMaster.partyCode='"+TSupCode.getText()+"' ");

	        if (theConnect==null)
            {
                  ORAConnection jdbc  = ORAConnection.getORAConnection();
                  theConnect         = jdbc.getConnection();
            }
    	    PreparedStatement ps          = theConnect.prepareStatement(sb.toString());
    	    ResultSet rst                 = ps.executeQuery();
    
    	   while (rst.next())
    	   {
				SStateCode    = rst.getString(1);
				SSupType	  = rst.getString(2);
           }
			rst.close();
			ps.close();
	    }
    	catch(Exception e){
            e.printStackTrace();
            System.out.println("getPartyGst:"+e);
    	}
	}
}
