package RDC;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkOrderWithoutRDCPanelGst extends JPanel 
{
     WORDCInvMiddlePanelGst MiddlePanel;
	 String sSupplierCode="";
     Object RowData[][];

     //			                 0           1       2     3         4            5           6          7           8          9              10         11          12            13           14         15      16        17      18         19    20    21   22  23  24  25
     String ColumnData[] = {"Service","HsnCode","Description","RDC No","Qty","Rate","Discount(%)","CGST(%)","SGST(%)","IGST(%)","Cess(%)","Basic","Discount (Rs)","CGST(Rs)","SGST(Rs)","IGST(Rs)","Cess(Rs)","Net (Rs)","Department","Group","Due Date","Unit","Remarks","LogBook No","DocId"};
     String ColumnType[] = {"S"      ,"S"      ,"E"          ,"S"     ,"S"  ,"S"   ,"S"          ,"N"      ,"N"      ,"N"      ,"N"      ,"N"    ,"N"            ,"N"       ,"N"       ,"N"       ,"N"       ,"N"       ,"B"         ,"B"    ,"E"       ,"B"   ,"E"       ,"E"        ,"E" };
                             
     Common common = new Common();
     JLayeredPane DeskTop;
     int iMillCode;

     public WorkOrderWithoutRDCPanelGst(JLayeredPane DeskTop,int iMillCode,String sSupplierCode)
     {
          this.DeskTop   = DeskTop;
          this.iMillCode = iMillCode;
		  this.sSupplierCode = sSupplierCode;

          setLayout(new BorderLayout());
          setRowData();
          createComponents();
     }
     public void createComponents()
     {
          MiddlePanel = new WORDCInvMiddlePanelGst(DeskTop,RowData,ColumnData,ColumnType,iMillCode,sSupplierCode);
          add(MiddlePanel,BorderLayout.CENTER);
          MiddlePanel.ReportTable.requestFocus();
     }

     public void setRowData()
     {
          RowData     = new Object[1][25];
          for(int i=0;i<1;i++)
          {
               RowData[i][0]  = " ";
               RowData[i][1]  = " ";
               RowData[i][2]  = " ";
               RowData[i][3]  = " ";
               RowData[i][4]  = " ";
               RowData[i][5]  = " ";
               RowData[i][6]  = " ";
               RowData[i][7]  = " ";
               RowData[i][8]  = " ";
               RowData[i][9]  = " ";
               RowData[i][10] = " ";
               RowData[i][11] = " ";
               RowData[i][12] = " ";
               RowData[i][13] = " ";
               RowData[i][14] = " ";
               RowData[i][15] = " ";
               RowData[i][16] = " ";
               RowData[i][17] = " ";
               RowData[i][18] = " ";
               RowData[i][19] = " ";
               RowData[i][20] = " ";
			   RowData[i][21] = " ";
			   RowData[i][22] = " ";
			   RowData[i][23] = " ";
			   RowData[i][24] = " ";
          }
     }
}
