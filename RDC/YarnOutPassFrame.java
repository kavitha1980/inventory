package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

class YarnOutPassFrame extends JInternalFrame
{
     Connection     theConnection  = null;
     Common         common         = new Common();

     JLayeredPane   Layer;
     JPanel         TopPanel,BottomPanel,MiddlePanel;

     JButton        BOk,BApply;
     JComboBox      JCBookNo;
     MyComboBox     JCType,JCOutPassNo;
     TimeField      TOutTime;

     Vector         VOutPassNo,VPartyCode,VPartyName,VInvNo,VInvDate,VFormJJNo,VFormJJDate,VBales,VQty,VTruckNo;

     Object RowData[][];

     String ColumnData[]={"Inv No/FormJJ No","Inv/FormJJ Date","Buyer Name","No of Bales","Weight"};
     String ColumnType[]={"N"               ,"S"              ,"S"         ,"N"          ,"N"     };

     TabReport      tabreport;

     String         SOutPassNo= "";
     String         SDespOutPassNo="";

     int            iBookNo   = 0;
     int            iMillCode,iUserCode;
     String         SYearCode;
                    
     public YarnOutPassFrame(JLayeredPane Layer,int iMillCode,int iUserCode,String SYearCode)
     {
          super("Yarn OutPass Entry Screen");
          this . Layer        = Layer;
          this . iMillCode    = iMillCode;
          this . iUserCode    = iUserCode;
          this . SYearCode    = SYearCode;

          createComponents();
          setLayouts();
          addComponents();
          addListener();
          getLoadData(0);
     }

     public void createComponents()
     {
          JCType         = new MyComboBox();
          JCOutPassNo    = new MyComboBox();

          JCBookNo       = new JComboBox();

          JCType         . addItem("Direct Invoice");
          JCType         . addItem("Stock Transfer");
          JCBookNo       . addItem("Book1");
          JCBookNo       . addItem("Book2");
          JCBookNo       . addItem("Book3");

          JCBookNo       . setEnabled(false);

          TOutTime       = new TimeField(true);

          BApply         = new JButton("Apply");
          BOk            = new JButton("Save");

          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          MiddlePanel    = new JPanel();

          VOutPassNo  = new Vector();
          BOk.setEnabled(false);
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,550,400);
          setTitle("Yarn OutPass Entry Screen");

          TopPanel       . setLayout(new GridLayout(5,2,10,10));
          BottomPanel    . setLayout(new FlowLayout());
          MiddlePanel    . setLayout(new BorderLayout());
          MiddlePanel.setBorder(new TitledBorder("Details For this OutPass"));
     }

     public void addComponents()
     {
          TopPanel            . add(new Label("Out Pass Book"));
          TopPanel            . add(JCBookNo);

          TopPanel            . add(new Label("Despatch Type"));
          TopPanel            . add(JCType);

          TopPanel            . add(new Label("Despatch OutPassNo"));
          TopPanel            . add(JCOutPassNo);

          TopPanel            . add(new Label("Time Out"));
          TopPanel            . add(TOutTime);

          TopPanel            . add(new Label(""));
          TopPanel            . add(BApply);

          BottomPanel         . add(BOk);

          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListener()
     {
          BApply        . addActionListener(new ActList());
          BOk           . addActionListener(new ActList());
          JCType        . addItemListener(new ItemList());
     }

     private class ItemList implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
               if(ie.getSource()==JCType)
               {
                    int iIndex = JCType.getSelectedIndex();
                    getLoadData(iIndex);
               }
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    if(isValidNew())
                    {
                         BOk.setEnabled(false);
     
                         SOutPassNo = getNextOutPassNo();
          
                         insertOutPass();
                         insertOutPassDetails();
                         UpdateInvoice();
                         UpdateOutPassNo();
          
                         String    SNum           = "The OutPass Number is "+SOutPassNo;
                                   JOptionPane    . showMessageDialog(null,SNum,"Information",JOptionPane.INFORMATION_MESSAGE);
          
                         removeHelpFrame();
                    }
                    else
                    {
                         BOk.setEnabled(true);
                    }
               }
               if(ae.getSource()==BApply)
               {
                    BApply.setEnabled(false);
                    JCType.setEnabled(false);
                    JCOutPassNo.setEnabled(false);
                    int iIndex = JCType.getSelectedIndex();
                    SDespOutPassNo = (String)JCOutPassNo.getSelectedItem();
                    setDataIntoVector(SDespOutPassNo,iIndex);
                    setTabReport(iIndex);

                    if(tabreport.ReportTable.getRowCount()>0)
                         BOk.setEnabled(true);
               }
          }
     }

     public void getLoadData(int iIndex)
     {
          try
          {
               JCOutPassNo.removeAllItems();

               VOutPassNo  .removeAllElements();

               String QS = "";

               if(iIndex==0)
               {
                    QS = " Select Distinct(OutPassNo) From Invoice "+
                         " Inner Join PartyMaster on Invoice.InvoicePartyCode=PartyMaster.PartyCode "+
                         " Where IssueNo is Null and PartyMaster.TransferParty=0 and InvoiceDate="+common.getServerPureDate()+
                         " and GateOutPassStatus=0 "+
                         " Order By 1 ";
               }
               else
               {
                    QS = " Select Distinct(OutPassNo) From StockInvoice "+
                         " Where InvoiceDate="+common.getServerPureDate()+
                         " and GateOutPassStatus=0 "+
                         " Order By 1 ";
  
               }

               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    VOutPassNo . addElement(result.getString(1));
               }
               result    . close();
               stat      . close();


               for(int i=0;i<VOutPassNo.size();i++)
               {
                    JCOutPassNo.addItem((String)VOutPassNo.elementAt(i));
               }
  
               if(VOutPassNo.size()>0)
               {
                    BApply.setEnabled(true);
               }
               else
               {
                    BApply.setEnabled(false);
               }

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setDataIntoVector(String SDespOutPassNo,int iIndex)
     {
          try
          {
               VPartyCode  = new Vector();
               VPartyName  = new Vector();
               VInvNo      = new Vector();
               VInvDate    = new Vector();
               VFormJJNo   = new Vector();
               VFormJJDate = new Vector();
               VBales      = new Vector();
               VQty        = new Vector();
               VTruckNo    = new Vector();

               String QS = "";

               if(iIndex==0)
               {
                    QS = " Select Invoice.InvoicePartyCode as PartyCode,PartyMaster.PartyName, "+
                         " Invoice.InvoiceNo,Invoice.InvoiceDate,'' as FormJJNo,'' as FormJJDate, "+
                         " Sum(Invoice.DespatchBags) as Bales,Sum(Invoice.DespatchWeight) as Qty,Invoice.TruckNo "+
                         " From Invoice "+
                         " Inner Join PartyMaster on Invoice.InvoicePartyCode=PartyMaster.PartyCode "+
                         " Where Invoice.OutPassNo="+SDespOutPassNo+" and Invoice.IssueNo is Null  and PartyMaster.TransferParty=0 "+
                         " Group by Invoice.InvoicePartyCode,PartyMaster.PartyName, "+
                         " Invoice.InvoiceNo,Invoice.InvoiceDate,Invoice.TruckNo "+
                         " Order By 1 ";
               }
               else
               {
                    QS = " Select StockInvoice.DespatchPartyCode as PartyCode,PartyMaster.PartyName, "+
                         " '' as InvoiceNo,'' as InvoiceDate,StockInvoice.FormXXNo as FormJJNo,StockInvoice.InvoiceDate as FormJJDate, "+
                         " Sum(StockInvoice.DespatchBags) as Bales,Sum(StockInvoice.DespatchWeight) as Qty,StockInvoice.TruckNo "+
                         " From StockInvoice "+
                         " Inner Join PartyMaster on StockInvoice.DespatchPartyCode=PartyMaster.PartyCode "+
                         " Where StockInvoice.OutPassNo="+SDespOutPassNo+
                         " Group by StockInvoice.DespatchPartyCode,PartyMaster.PartyName, "+
                         " StockInvoice.FormXXNo,StockInvoice.InvoiceDate,StockInvoice.TruckNo "+
                         " Order By 1 ";
               }

               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    VPartyCode . addElement(result.getString(1));
                    VPartyName . addElement(result.getString(2));
                    VInvNo     . addElement(result.getString(3));
                    VInvDate   . addElement(result.getString(4));
                    VFormJJNo  . addElement(result.getString(5));
                    VFormJJDate. addElement(result.getString(6));
                    VBales     . addElement(result.getString(7));
                    VQty       . addElement(result.getString(8));
                    VTruckNo   . addElement(result.getString(9));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setTabReport(int iIndex)
     {
          RowData = new Object[VInvNo.size()][ColumnData.length];

          for(int i=0;i<VInvNo.size();i++)
          {
               if(iIndex==0)
               {
                    RowData[i][0] = (String)VInvNo.elementAt(i);
                    RowData[i][1] = common.parseDate((String)VInvDate.elementAt(i));
                    RowData[i][2] = (String)VPartyName.elementAt(i);
                    RowData[i][3] = (String)VBales.elementAt(i);
                    RowData[i][4] = (String)VQty.elementAt(i);
               }
               else
               {
                    RowData[i][0] = (String)VFormJJNo.elementAt(i);
                    RowData[i][1] = common.parseDate((String)VFormJJDate.elementAt(i));
                    RowData[i][2] = (String)VPartyName.elementAt(i);
                    RowData[i][3] = (String)VBales.elementAt(i);
                    RowData[i][4] = (String)VQty.elementAt(i);
               }
          }

          MiddlePanel.add("Center",tabreport=new TabReport(RowData,ColumnData,ColumnType));

          tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          Layer.updateUI();
     }

     public boolean isValidNew()
     {
          String SOutTime = common.parseNull(TOutTime.toString());

          if(SOutTime.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Time out must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TOutTime.THours.requestFocus();
               return false;
          }
          return true;
     }

     private void insertOutPass()
     {
          try
          {
               String    QS = " Insert into OutPass (ID,OUTPASSNO,OUTPASSDATE,MATERIALTYPE,BOOKTYPE,MILLCODE,USERCODE,CREATIONDATE) values (";
                         QS   = QS+" OUTPASS_SEQ.nextval ,";
                         QS   = QS+SOutPassNo+",";
                         QS   = QS+"'"+common.getServerPureDate()+"',";
                         QS   = QS+"3"+",";            // 0 - For Stores; 1 - For Others; 2 - Waste; 3 - Yarn;
                         QS   = QS+iBookNo+",";
                         QS   = QS+iMillCode+",";
                         QS   = QS+iUserCode+",";
                         QS   = QS+"'"+common.getServerDate()+"')";

               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();
                              stat           . execute(QS);
                              stat           . close();
          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void insertOutPassDetails()
     {
          try
          {
               int iIndex = JCType.getSelectedIndex();

               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();

               for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
               {
                    String QS = "";

                    if(iIndex==0)
                    {
                         QS = " Insert into OutPassDetails (ID,OUTPASSNO,PARTYCODE,MATERIALTYPE,BOOKTYPE,MILLCODE,DESCRIPT,QTY,PURPOSE,TIMEOUT,INVNO,INVDATE,NOOFBALES,YARNDESPTYPE,YARNOUTPASSNO,TRUCKNO) values (";
                         QS = QS+" OUTPASSDETAILS_SEQ.nextval ,";
                         QS = QS+SOutPassNo+",";
                         QS = QS+"'"+(String)VPartyCode.elementAt(i)+"',";
                         QS = QS+"3"+",";
                         QS = QS+iBookNo+",";
                         QS = QS+iMillCode+",";
                         QS = QS+"'"+"YARN BAGS"+"',";
                         QS = QS+"0"+(String)tabreport.ReportTable.getValueAt(i,4)+",";
                         QS = QS+"'"+"FOR SALES"+"',";
                         QS = QS+"'"+TOutTime.toString()+"',";
                         QS = QS+"'"+(String)tabreport.ReportTable.getValueAt(i,0)+"',";
                         QS = QS+"0"+common.pureDate((String)tabreport.ReportTable.getValueAt(i,1))+",";
                         QS = QS+"0"+(String)tabreport.ReportTable.getValueAt(i,3)+",";
                         QS = QS+iIndex+",";
                         QS = QS+SDespOutPassNo+",";
                         QS = QS+"'"+(String)VTruckNo.elementAt(i)+"')";
                         
                    }
                    else
                    {
                         QS = " Insert into OutPassDetails (ID,OUTPASSNO,PARTYCODE,MATERIALTYPE,BOOKTYPE,MILLCODE,DESCRIPT,QTY,PURPOSE,TIMEOUT,FORMJJNO,FORMJJDATE,NOOFBALES,YARNDESPTYPE,YARNOUTPASSNO,TRUCKNO) values (";
                         QS = QS+" OUTPASSDETAILS_SEQ.nextval ,";
                         QS = QS+SOutPassNo+",";
                         QS = QS+"'"+(String)VPartyCode.elementAt(i)+"',";
                         QS = QS+"3"+",";
                         QS = QS+iBookNo+",";
                         QS = QS+iMillCode+",";
                         QS = QS+"'"+"YARN BAGS"+"',";
                         QS = QS+"0"+(String)tabreport.ReportTable.getValueAt(i,4)+",";
                         QS = QS+"'"+"FOR SALES"+"',";
                         QS = QS+"'"+TOutTime.toString()+"',";
                         QS = QS+"'"+(String)tabreport.ReportTable.getValueAt(i,0)+"',";
                         QS = QS+"0"+common.pureDate((String)tabreport.ReportTable.getValueAt(i,1))+",";
                         QS = QS+"0"+(String)tabreport.ReportTable.getValueAt(i,3)+",";
                         QS = QS+iIndex+",";
                         QS = QS+SDespOutPassNo+",";
                         QS = QS+"'"+(String)VTruckNo.elementAt(i)+"')";
                         
                    }
                    stat.execute(QS);

               }
               stat.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public String getNextOutPassNo()
     {
          String    QS           = "";

          int       iOutPassNo   = 0;

                    iBookNo   = 0;
                    iBookNo   = JCBookNo.getSelectedIndex();
                    iBookNo   = iBookNo+1;      

          if(iBookNo==1)
               QS = "  Select maxno from Config"+iMillCode+""+SYearCode+" where id = 13";

          /*if(iBookNo==2)
               QS = "  Select maxno from config where id = 27";

          if(iBookNo==3)
               QS = "  Select maxno from config where id = 28";*/

          try
          {
               ORAConnection  oraConnection  = ORAConnection     . getORAConnection();
               Connection     theConnection  = oraConnection     . getConnection();   
               Statement      stat           = theConnection     . createStatement();
               ResultSet      result         = stat              . executeQuery(QS);

                         result         . next();
                         iOutPassNo     = result.getInt(1);

                         result         . close();
                         stat           . close();

                         iOutPassNo     = iOutPassNo+1;

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

          return String.valueOf(iOutPassNo);
     }

     public void UpdateOutPassNo()
     {
          String    QS1       = "";

                    iBookNo   = 0;
                    iBookNo   = JCBookNo.getSelectedIndex();
                    iBookNo   = iBookNo+1;      

          if(iBookNo==1)
               QS1 = " Update Config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1  where id = 13";

          /*if(iBookNo==2)
               QS1 = " Update config set MaxNo = MaxNo+1  where id = 27";

          if(iBookNo==3)
               QS1 = " Update config set MaxNo = MaxNo+1  where id = 28";*/

          try
          {
               ORAConnection  oraConnection  = ORAConnection     . getORAConnection();
               Connection     theConnection  = oraConnection     . getConnection();   
               Statement      stat           = theConnection     . createStatement();
                              stat           . executeUpdate(QS1);
                              stat           . close();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void UpdateInvoice()
     {
          try
          {
               int iIndex = JCType.getSelectedIndex();

               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();

               String QS = "";

               if(iIndex==0)
               {
                    QS = " Update Invoice Set GateOutPassStatus=1, GateOutPassNo="+SOutPassNo+
                         " Where OutPassNo = "+SDespOutPassNo;
               }
               else
               {
                    QS = " Update StockInvoice Set GateOutPassStatus=1, GateOutPassNo="+SOutPassNo+
                         " Where OutPassNo = "+SDespOutPassNo;
               }
               stat.executeUpdate(QS);
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer   . remove(this);
               Layer   . repaint();
               Layer   . updateUI();
          }
          catch(Exception ex) { }
     }
}
