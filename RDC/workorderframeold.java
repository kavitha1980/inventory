package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class WorkOrderFrame extends JInternalFrame
{
     
     JPanel           TopPanel,TopLeft,TopRight,BottomPanel;
     WOMiddlePanel    MiddlePanel;

     JButton          BOk,BCancel,BSupplier;
     JButton          BMaterial;

     NextField        TOrderNo;
     JTextField       TSupCode;
     MyTextField      TRef,TPayTerm;
     NextField        TAdvance;
     DateField        TDate;
     MyComboBox       JCTo,JCThro,JCForm;
     String           SOrderNo="";

     JLayeredPane     DeskTop;
     StatusPanel      SPanel;
     int              iUserCode;
     int              iMillCode;
     WorkOrderListFrame   orderlistframe;
     String           SSupTable,SYearCode;

     boolean          bflag;                 

     Common common    = new Common();
     ORAConnection connect;
     Connection theConnect = null;

     Vector           VTo,VThro,VToCode,VThroCode,VFormCode,VForm;

     Vector           VId;
     Vector           VName,VSlNo;
     Vector           VRdcNo;

     int       iMaxSlNo  = 0;
     int       iCount=0;

     boolean             bComflag = true;

     WorkOrderFrame(JLayeredPane DeskTop,StatusPanel SPanel,boolean bflag,int iUserCode,int iMillCode,String SSupTable,String SYearCode)
     {
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.bflag     = bflag;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
          TDate.requestFocus();
     }
     WorkOrderFrame(JLayeredPane DeskTop,StatusPanel SPanel,boolean bflag,int iUserCode,int iMillCode,WorkOrderListFrame orderlistframe,String SSupTable)
     {
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.bflag     = bflag;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.orderlistframe= orderlistframe;
          this.SSupTable = SSupTable;

          getDBConnection();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
          TDate.requestFocus();
     }

     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnect     =    oraConnection.getConnection();
     }

     public void createComponents()
     {
          TopPanel    = new JPanel();
          TopLeft     = new JPanel();
          TopRight    = new JPanel();
          BottomPanel = new JPanel();

          BOk         = (bflag?new JButton("Update"):new JButton("Save"));
          BCancel     = new JButton("Abort");

          BSupplier   = new JButton("Supplier");
          BMaterial   = new JButton("Pending Materials");

          TOrderNo    = new NextField();
          TDate       = new DateField();
          TSupCode    = new JTextField();
          TAdvance    = new NextField(10);
          TPayTerm    = new MyTextField(40);
          TRef        = new MyTextField(30);

          getVTo();

          JCTo        = new MyComboBox(VTo);
          JCThro      = new MyComboBox(VThro);
          JCForm      = new MyComboBox(VForm);

          TOrderNo.setEditable(false);
          BSupplier.setMnemonic('U');
          BMaterial.setMnemonic('P');
          BCancel.setMnemonic('A');
          if(bflag)
          {
               BOk.setMnemonic('U');
          }
          else
          {
               BOk.setMnemonic('S');
          }
     }


     public void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(1,2));
          TopRight.setLayout(new GridLayout(5,2));

          if(bflag)
          {
               setTitle("Amendment Or Modification to Work Order");
               TopLeft   .setLayout(new GridLayout(4,2));
          }
          else
          {
               setTitle("Work Order Placement");
               TopLeft.setLayout(new GridLayout(5,2));
          }

          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,790,500);

          getContentPane()    .setLayout(new BorderLayout());
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          TopLeft   .add(new JLabel("Order No"));
          TopLeft   .add(TOrderNo);

          TopLeft   .add(new JLabel("Order Date"));
          TopLeft   .add(TDate);

          TopLeft   .add(new JLabel("Reference"));
          TopLeft   .add(TRef);

          TopLeft   .add(new JLabel("Supplier"));
          TopLeft   .add(BSupplier);

          if(!bflag)
          {
               TopLeft   .add(new JLabel("Materials"));
               TopLeft   .add(BMaterial);
          }

          TopRight  .add(new JLabel("Book To"));
          TopRight  .add(JCTo);

          TopRight  .add(new JLabel("Book Thro"));
          TopRight  .add(JCThro);

          TopRight  .add(new JLabel("Form No"));
          TopRight  .add(JCForm);

          TopRight  .add(new JLabel("Advance"));
          TopRight  .add(TAdvance);

          TopRight  .add(new JLabel("Payment Terms"));
          TopRight  .add(TPayTerm);

          TopPanel  .add(TopLeft);
          TopPanel  .add(TopRight);

          TopLeft   .setBorder(new TitledBorder("Order Id Block-I"));
          TopRight  .setBorder(new TitledBorder("Order Id Block-II"));

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          TOrderNo.setText(getNextOrderNo());  
     }

     public void addListeners()
     {
          BSupplier.addActionListener(new SupplierSearch(DeskTop,TSupCode,SSupTable));
          BMaterial.addActionListener(new WOPendingList(DeskTop,MiddlePanel,TSupCode,this,iMillCode));

          if(bflag)
          {
               BOk       .addActionListener(new UpdtList());
          }
          else 
          {
               BOk       .addActionListener(new ActList());
          }
          BCancel   .addActionListener(new CanList());
     }
     public class CanList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               removeHelpFrame();
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(common.toInt(TOrderNo.getText())>0)
               {
                    if(checkData())
                    {
                         iCount=0;
                         BOk.setEnabled(true);
                         insertOrderDetails();
                         if(iCount>0)
                             updateOrderNo();

                         getACommit();
                         removeHelpFrame();
                    }
               }
          }
     }

     public class UpdtList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(checkData())
               {
                    BOk.setEnabled(true);
                    BOk.setEnabled(false);
                    updateOrderDetails();
                    insertOrderDetails();
                    getACommit();
                    removeHelpFrame();
                    orderlistframe.setTabReport(orderlistframe);
               }
          }
     }

     public void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnect     . commit();
                    System         . out.println("Commit");
                    theConnect     . setAutoCommit(true);
               }
               else
               {
                    theConnect     . rollback();
                    JOptionPane.showMessageDialog(null,"Problem in Saving Data","Error",JOptionPane.ERROR_MESSAGE);
                    System         . out.println("RollBack");
                    theConnect     . setAutoCommit(true);
               }
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void insertOrderDetails()
     {

          String QString = "Insert Into WorkOrder (ID,OrderNo,OrderDate,Sup_Code,Reference,Advance,PayTerms,ToCode,ThroCode,FormCode,Descript,RDCNo,Qty,Rate,DiscPer,Disc,CenvatPer,CenVat,TaxPer,Tax,SurPer,Sur,Net,Plus,Less,Misc,Unit_Code,Dept_Code,Group_Code,DueDate,SlNo,UserCode,ModiDate,RDCSlNo,MillCode,LogBookNo,Remarks,WODesc,EntryStatus) Values (";

          int iSlNo = 0;

          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat      = theConnect.createStatement();

               TOrderNo.setText(getNextOrderNo());  

               if(bflag)
                    iMaxSlNo = getMaxSlNo(((String)TOrderNo.getText()).trim());
               else
                    iMaxSlNo = 0;


               int k = iMaxSlNo;

               Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
               String SAdd       = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess      = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm        = common.toDouble(SAdd)-common.toDouble(SLess);
               double dBasic     = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio     = dpm/dBasic;

               for(int i=0;i<FinalData.length;i++)
               {
                    try
                    {
                         if(!isFreshRecord(i))
                              continue;
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
                  
                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SDueDate   = MiddlePanel.MiddlePanel.getDueDate(i,TDate);

                    dBasic            = common.toDouble(((String)FinalData[i][9]).trim());

                    if(dBasic == 0)
                         continue;

                    String SMisc      = common.getRound(dBasic*dRatio,3);

                    if(MiddlePanel.SlData.length>i)
                    {
                         iSlNo = common.toInt(common.parseNull((String)MiddlePanel.SlData[i]));
                    }
                    else
                    {
                         iSlNo = 0;
                    }

                    String SRDCNo = ((String)FinalData[i][1]).trim();

                    String SEntryStatus="1";

                    String SRDCDesc = "";
                    String SWODesc  = "";

                    SRDCDesc = (((String)FinalData[i][0]).trim()).toUpperCase();
                    SWODesc  = (((String)FinalData[i][2]).trim()).toUpperCase();


                    if(SRDCDesc.equals(""))
                    {
                         SRDCDesc = SWODesc;
                         SWODesc  = "";
                    }

                    String QS1 = QString;
                    QS1 = QS1+"WorkOrder_Seq.nextval,";
                    QS1 = QS1+"0"+TOrderNo.getText()+",";
                    QS1 = QS1+"'"+TDate.toNormal()+"',";
                    QS1 = QS1+"'"+TSupCode.getText()+"',";
                    QS1 = QS1+"'"+(TRef.getText()).toUpperCase()+"',";    
                    QS1 = QS1+"0"+TAdvance.getText()+",";
                    QS1 = QS1+"'"+(TPayTerm.getText()).toUpperCase()+"',";
                    QS1 = QS1+"0"+(String)VToCode.elementAt(JCTo.getSelectedIndex())+",";
                    QS1 = QS1+"0"+(String)VThroCode.elementAt(JCThro.getSelectedIndex())+",";
                    QS1 = QS1+"0"+(String)VFormCode.elementAt(JCForm.getSelectedIndex())+",";
                    QS1 = QS1+"'"+common.getNarration(SRDCDesc)+"',";
                    QS1 = QS1+"0"+SRDCNo+",";
                    QS1 = QS1+"0"+((String)FinalData[i][3]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][4]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][5]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][10]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][6]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][11]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][7]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][12]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][8]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][13]).trim()+",";
                    QS1 = QS1+"0"+((String)FinalData[i][14]).trim()+",";
                    QS1 = QS1+"0"+SAdd+",";
                    QS1 = QS1+"0"+SLess+",";
                    QS1 = QS1+"0"+SMisc+",";
                    QS1 = QS1+"0"+SUnitCode+",";
                    QS1 = QS1+"0"+SDeptCode+",";
                    QS1 = QS1+"0"+SGroupCode+",";
                    QS1 = QS1+"'"+SDueDate+"',";
                    QS1 = QS1+(k+1)+",";
                    QS1 = QS1+"0"+iUserCode+",";
                    QS1 = QS1+"'"+common.getServerDateTime2()+"',";
                    QS1 = QS1+iSlNo+",";
                    QS1 = QS1+iMillCode+",";
                    QS1 = QS1+"0"+((String)FinalData[i][20]).trim()+",";
                    QS1 = QS1+"'"+(((String)FinalData[i][19]).trim()).toUpperCase()+"',";
                    QS1 = QS1+"'"+common.getNarration(SWODesc)+"',";
                    QS1 = QS1+"0"+SEntryStatus+")";

                    stat.executeUpdate(QS1);

                    if(common.toInt(SRDCNo)>0)
                    {
                         String QS2 = " Update GateInwardRDC set OrderNo="+TOrderNo.getText()+
                                      " Where OrderNo=0 and RdcNo="+SRDCNo+" and RDCSlNo="+iSlNo+
                                      " and MillCode="+iMillCode+" and Descript='"+((String)FinalData[i][0]).trim()+"'";

                         stat.executeUpdate(QS2);
                    }

                    k++;
                    iCount++;
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void updateOrderDetails()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat      = theConnect.createStatement();

               Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
               String SAdd       = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess      = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm        = common.toDouble(SAdd)-common.toDouble(SLess);
               double dBasic     = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio     = dpm/dBasic;

               for(int i=0;i<MiddlePanel.IdData.length;i++)
               {
                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SDueDate   = MiddlePanel.MiddlePanel.getDueDate(i,TDate);
                    dBasic            = common.toDouble(((String)FinalData[i][9]).trim());
                    String SMisc      = common.getRound(dBasic*dRatio,3);

                    String QS1 = "Update WorkOrder Set ";
                    QS1 = QS1+"OrderDate  = '"+TDate.toNormal()+"',";
                    QS1 = QS1+"Reference='"+(TRef.getText()).toUpperCase()+"',";
                    QS1 = QS1+"Advance=0"+TAdvance.getText()+",";
                    QS1 = QS1+"PayTerms='"+(TPayTerm.getText()).toUpperCase()+"',";
                    QS1 = QS1+"ToCode=0"+(String)VToCode.elementAt(JCTo.getSelectedIndex())+",";
                    QS1 = QS1+"ThroCode=0"+(String)VThroCode.elementAt(JCThro.getSelectedIndex())+",";
                    QS1 = QS1+"FormCode=0"+(String)VFormCode.elementAt(JCForm.getSelectedIndex())+",";
                    //QS1 = QS1+"Descript='"+(((String)FinalData[i][0]).trim()).toUpperCase()+"',";
                    //QS1 = QS1+"RDCNo=0"+((String)FinalData[i][1]).trim()+",";
                    QS1 = QS1+"WODesc='"+common.getNarration(((common.parseNull((String)FinalData[i][2])).trim()).toUpperCase())+"',";
                    QS1 = QS1+"Qty=0"+((String)FinalData[i][3]).trim()+",";
                    QS1 = QS1+"Rate=0"+((String)FinalData[i][4]).trim()+",";
                    QS1 = QS1+"DiscPer=0"+((String)FinalData[i][5]).trim()+",";
                    QS1 = QS1+"Disc=0"+((String)FinalData[i][10]).trim()+",";
                    QS1 = QS1+"CenVatPer=0"+((String)FinalData[i][6]).trim()+",";
                    QS1 = QS1+"Cenvat=0"+((String)FinalData[i][11]).trim()+",";
                    QS1 = QS1+"TaxPer=0"+((String)FinalData[i][7]).trim()+",";
                    QS1 = QS1+"Tax=0"+((String)FinalData[i][12]).trim()+",";
                    QS1 = QS1+"SurPer=0"+((String)FinalData[i][8]).trim()+",";
                    QS1 = QS1+"Sur=0"+((String)FinalData[i][13]).trim()+",";
                    QS1 = QS1+"Net=0"+((String)FinalData[i][14]).trim()+",";
                    QS1 = QS1+"Plus=0"+SAdd+",";
                    QS1 = QS1+"Less=0"+SLess+",";
                    QS1 = QS1+"Misc=0"+SMisc+",";
                    QS1 = QS1+"Unit_Code=0"+SUnitCode+",";
                    QS1 = QS1+"Dept_Code=0"+SDeptCode+",";
                    QS1 = QS1+"Group_Code=0"+SGroupCode+",";
                    QS1 = QS1+"LogBookNo=0"+((String)FinalData[i][20]).trim()+",";
                    QS1 = QS1+"Remarks='"+(((String)FinalData[i][19]).trim()).toUpperCase()+"',";
                    QS1 = QS1+"DueDate='"+SDueDate+"' Where OrderNo="+TOrderNo.getText()+" and ID = "+(String)MiddlePanel.IdData[i];

                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void updateOrderNo()
     {
          try
          {
               if(theConnect.getAutoCommit())
                         theConnect     . setAutoCommit(false);

               Statement stat      = theConnect.createStatement();

               String QS1 = " Update Config"+iMillCode+""+SYearCode+" Set MaxNo="+(common.toInt((String)TOrderNo.getText()))+" Where ID=11 ";
               stat.executeUpdate(QS1);
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public boolean isFreshRecord(int i)
     {
          try
          {
               if(MiddlePanel.IdData==null)
                    return true;

               boolean bfresh = (i<MiddlePanel.IdData.length?false:true);
               return bfresh;
          }
          catch(Exception ex)
          {
               return true;
          }
     }

     public int getMaxSlNo(String SOrderNo)
     {
          int iMaxNo=0;

          String QS = " Select Max(SlNo) from WorkOrder Where OrderNo="+SOrderNo+" and MillCode="+iMillCode;

          iMaxNo = common.toInt(common.getID(QS));
           
          return iMaxNo;
     }

     public String getNextOrderNo()
     {
          if(bflag)
               return TOrderNo.getText();
               
          String QS = "";

          QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" "+
               " Where ID=11 ";

          String SOrderNo = String.valueOf(common.toInt(common.getID(QS))+1);
          return SOrderNo;
     }
     public void getVTo()
     {
          VTo       = new Vector();
          VToCode   = new Vector();
          VThro     = new Vector();
          VThroCode = new Vector();
          VFormCode = new Vector();
          VForm     = new Vector();

          try
          {
               Statement stat   = theConnect.createStatement();
               ResultSet res = stat.executeQuery("Select ToName,ToCode From BookTo Order By 1");
               while(res.next())
               {
                    VTo.addElement(res.getString(1));
                    VToCode.addElement(res.getString(2));
               }
               res.close();
               res = stat.executeQuery("Select ThroName,ThroCode From BookThro Order By 1");
               while(res.next())
               {
                    VThro.addElement(res.getString(1));
                    VThroCode.addElement(res.getString(2));
               }
               res.close();
               res = stat.executeQuery("Select FormNo,FormCode From STForm Order By 1");
               while(res.next())
               {
                    VForm.addElement(res.getString(1));
                    VFormCode.addElement(res.getString(2));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setMiddlePanel(Vector VName,Vector VId,Vector VRdcNo,boolean bEmpty,Vector VSlNo,Vector VQty)
     {
          try
          {
               if(bEmpty)
                    MiddlePanel = new WOMiddlePanel(DeskTop,VName,VRdcNo,"Empty",VSlNo,VQty,iMillCode);
               else
                    MiddlePanel = new WOMiddlePanel(DeskTop,VName,VRdcNo,VSlNo,VQty,iMillCode);

               getContentPane().add(MiddlePanel,BorderLayout.CENTER);
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }          
     public void fillData(String SOrderNo)
     {
          try
          {
               MiddlePanel = new WOMiddlePanel(DeskTop,iMillCode);
               new WorkOrderResultSet(SOrderNo,TOrderNo,TDate,BSupplier,TSupCode,TRef,TAdvance,TPayTerm,JCTo,JCThro,JCForm,MiddlePanel,VToCode,VThroCode,VFormCode,this,iMillCode,SSupTable);
               getContentPane().add(MiddlePanel,BorderLayout.CENTER);
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println("6"+ex);
               ex.printStackTrace();
          }
     }

     public boolean checkData()
     {
          String SOrderDate = TDate.toNormal();
          String SSupCode   = TSupCode.getText();

          int iRows=0;

          if(SOrderDate.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Order Date Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TDate.TDay.requestFocus();
               return false;
          }
          if(SSupCode.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Select Supplier","Error",JOptionPane.ERROR_MESSAGE);
               BSupplier.setEnabled(true);
               BSupplier.requestFocus();
               return false;
          }
          try
          {
               iRows    = MiddlePanel.MiddlePanel.getRows();
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"No Materials Selected","Error",JOptionPane.ERROR_MESSAGE);
               BMaterial.setEnabled(true);
               BMaterial.requestFocus();
               return false;
          }

          Object FData[][] = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<FData.length;i++)
          {
               String SDueDate   = MiddlePanel.MiddlePanel.getDueDate(i,TDate);

               if(common.toInt(SDueDate)<common.toInt((String)TDate.toNormal()))
               {
                    JOptionPane.showMessageDialog(null,"Due Date Greater than Order Date","Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               double dQty   = common.toDouble(((String)FData[i][3]).trim());
               String SDesc1 = ((String)FData[i][0]).trim();
               String SDesc2 = (common.parseNull((String)FData[i][2])).trim();

               if(SDesc1.equals("") && SDesc2.equals("") && dQty>0)
               {
                    JOptionPane.showMessageDialog(null,"Invalid Description @Row - "+i+1,"Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               if(bflag)
               {
                    if(i<MiddlePanel.IdData.length && dQty<=0)
                    {
                         JOptionPane.showMessageDialog(null,"Invalid Quantity @Row-"+i+1,"Error",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }
               }
          }
          return true;
     }
}
