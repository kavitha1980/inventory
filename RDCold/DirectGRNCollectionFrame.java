package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class DirectGRNCollectionFrame extends JInternalFrame
{

     MyTextField    TGINo;
     DateField      TGIDate,TInvDate,TDCDate;
     DateField      TDate;
     MyTextField    TInvNo,TDCNo;
     JTextField     TSupName;
     WholeNumberField TInvoice;
     DirectGRNMiddlePanel MiddlePanel;

     JPanel         TopPanel,BottomPanel;
     JButton        BOk,BCancel;
     MyLabel        LGrnNo;

     JLayeredPane   DeskTop;
     Vector         VCode,VName;
     StatusPanel    SPanel;
     String         SIndex;
     int            iIndex=0;
     JTable         ReportTable;
     String         SSupCode;
     String         SGINo;
     String         SGIDate;
     int            iUserCode;
     int            iMillCode;
     DirectGRNFrame directgrnframe;
     String         SYearCode;

     Common common   = new Common();
     ORAConnection connect;
     Connection theconnect;

     boolean             bComflag = true;
     
     DirectGRNCollectionFrame(JLayeredPane DeskTop,StatusPanel SPanel,String SIndex,JTable ReportTable,String SSupCode,String SGINo,String SGIDate,int iUserCode,int iMillCode,DirectGRNFrame directgrnframe,String SYearCode)
     {
          this.DeskTop        = DeskTop;
          this.SPanel         = SPanel;
          this.SIndex         = SIndex;
          this.ReportTable    = ReportTable;
          this.SSupCode       = SSupCode;
          this.SGINo          = SGINo;
          this.SGIDate        = SGIDate;
          this.iUserCode      = iUserCode;
          this.iMillCode      = iMillCode;
          this.directgrnframe = directgrnframe;
          this.SYearCode      = SYearCode;

          iIndex              = common.toInt(SIndex);

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }

     public void createComponents()
     {
          BOk         = new JButton("Save");
          BCancel     = new JButton("Abort");
                      
          TDate       = new DateField();
          TGINo       = new MyTextField(8);
          TGIDate     = new DateField();
          TInvDate    = new DateField();
          TDCDate     = new DateField();
          TInvNo      = new MyTextField(15);
          TDCNo       = new MyTextField(15);
          TSupName    = new JTextField();
          TInvoice    = new WholeNumberField(2);
          LGrnNo      = new MyLabel();

          MiddlePanel = new DirectGRNMiddlePanel(DeskTop,SSupCode,iMillCode,SYearCode);

          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          TDate     . setTodayDate();
          TSupName  . setEditable(false);
          TDate     . setEditable(false);
          BOk       . setMnemonic('S');
          BCancel   . setMnemonic('A');
     }

     public void setLayouts()
     {
          setTitle("Invoice Valuation of Materials recd against Work Order");

          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,790,500);

          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(5,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("Gate Inward No"));
          TopPanel.add(TGINo);

          TopPanel.add(new JLabel("Gate Inward Date"));
          TopPanel.add(TGIDate);

          TopPanel.add(new JLabel("GRN No"));
          TopPanel.add(LGrnNo);

          TopPanel.add(new JLabel("GRN Date"));
          TopPanel.add(TDate);

          TopPanel.add(new JLabel("Supplier"));
          TopPanel.add(TSupName);

          TopPanel.add(new JLabel("No. of Bills"));
          TopPanel.add(TInvoice);

          TopPanel.add(new JLabel("Invoice No"));
          TopPanel.add(TInvNo);

          TopPanel.add(new JLabel("Invoice Date"));
          TopPanel.add(TInvDate);

          TopPanel.add(new JLabel("DC No"));
          TopPanel.add(TDCNo);

          TopPanel.add(new JLabel("DC Date"));
          TopPanel.add(TDCDate);

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          TSupName.setText((String)ReportTable.getModel().getValueAt(iIndex,0));

          TGIDate        . fromString(SGIDate);
          LGrnNo         . setText(getNextOrderNo());
          MiddlePanel    . createComponents();
     }

     public void addListeners()
     {
          BOk       . addActionListener(new ActList());
          BCancel   . addActionListener(new CancelList());
     }

     public class CancelList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               removeHelpFrame();
          }
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               /* changed on 14.08.2010 for additional validation purpose (during multi company addition changes)*/

               //if(checkData())
               if(allOk())              
               {
                    BOk.setEnabled(false);
                    String    SGRNNo  = getNextOrderNo();
                    LGrnNo         . setText(SGRNNo);

                    insertGRNCollectedData(SGRNNo);
                    directgrnframe . addComponents();
                    BOk            . setEnabled(true);
               }
          }
     }

     private void insertGRNCollectedData(String SGRNNo)
     {
          insertGRNDetails(SGRNNo);
          setOrderLink();
          updateOrderNo();
          setAutoCommitStatus();
          removeHelpFrame();
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
     public void insertGRNDetails(String SGRNNo)
     {

          int iSerlNo = 0;
          String QString = "Insert Into WorkGRN (ID,OrderNo,GrnNo,GrnDate,Sup_Code,GateInNo,GateInDate,InvNo,InvDate,DcNo,DcDate,Descript,InvQty,MillQty,Qty,Pending,OrderQty,InvRate,DiscPer,Disc,CenvatPer,Cenvat,TaxPer,Tax,SurPer,Sur,InvAmount,InvNet,Plus,Less,Misc,Dept_Code,Group_Code,Unit_Code,InvSlNo,ActualModVat,UserCode,NoOfBills,OrderSlNo,MillCode) Values (";
          String QSL     = "Select Max(InvSlNo) From WorkGRN";
          int iSlNo = 0;

          try
          {
               if(theconnect==null)
               {
                     connect       = ORAConnection.getORAConnection();
                     theconnect    = connect.getConnection();
               }

               Statement stat      = theconnect.createStatement();
               ResultSet res       = stat.executeQuery(QSL);

               while(res.next())
               {
                    iSlNo = res.getInt(1);
               }
               res.close();

               iSlNo++;

               Object RowData[][]  = MiddlePanel.MiddlePanel.getFromVector();                 
               String SAdd         = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess        = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm          = common.toDouble(SAdd)-common.toDouble(SLess);
               double dActVat      = MiddlePanel.MiddlePanel.getModVat();
               double dVatBasic    = MiddlePanel.MiddlePanel.getVatBasic();
               double dBasic       = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio       = dpm/dBasic;
               double dVatRatio    = dActVat/dVatBasic;               

               for(int i=0;i<RowData.length;i++)
               {
                    iSerlNo = common.toInt(common.parseNull((String)MiddlePanel.SlData[i]));

                    double dDcQty     = common.toDouble((String)RowData[i][4]);
                    if(dDcQty == 0)
                         continue;

                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    String SOrderNo   = MiddlePanel.getOrderNo(i);
                    String SGrnNos    = MiddlePanel.getGRNNo(i);
                    String SOrdQty    = MiddlePanel.getOrdQty(i);

                    dBasic            = common.toDouble((String)RowData[i][11]);
                    double dCenVatPer = common.toDouble((String)RowData[i][8]);
                    String SCenVat    = "0";

                    if(dCenVatPer > 0)
                         SCenVat = common.getRound(dBasic*dVatRatio,3);

                    String SMisc      = common.getRound(dBasic*dRatio,3);
                                             
                    String QS1 = QString;
                    QS1 = QS1+"WorkGrn_Seq.nextval,";
                    QS1 = QS1+"0"+SOrderNo+",";
                    QS1 = QS1+"0"+SGRNNo+",";
                    QS1 = QS1+"'"+TDate.toNormal()+"',";
                    QS1 = QS1+"'"+SSupCode+"',";
                    QS1 = QS1+"0"+TGINo.getText()+",";
                    QS1 = QS1+"'"+TGIDate.toNormal()+"',";
                    QS1 = QS1+"'"+TInvNo.getText()+"',";
                    QS1 = QS1+"'"+TInvDate.toNormal()+"',";
                    QS1 = QS1+"'"+TDCNo.getText()+"',";
                    QS1 = QS1+"'"+TDCDate.toNormal()+"',";
                    QS1 = QS1+"'"+(String)RowData[i][0]+"',";
                    QS1 = QS1+"0"+(String)RowData[i][4]+",";
                    QS1 = QS1+"0"+(String)RowData[i][5]+",";
                    QS1 = QS1+"0"+(String)RowData[i][5]+",";
                    QS1 = QS1+"0"+(String)RowData[i][3]+",";
                    QS1 = QS1+"0"+SOrdQty+",";
                    QS1 = QS1+"0"+(String)RowData[i][6]+",";
                    QS1 = QS1+"0"+(String)RowData[i][7]+",";
                    QS1 = QS1+"0"+(String)RowData[i][12]+",";
                    QS1 = QS1+"0"+(String)RowData[i][8]+",";
                    QS1 = QS1+"0"+(String)RowData[i][13]+",";
                    QS1 = QS1+"0"+(String)RowData[i][9]+",";
                    QS1 = QS1+"0"+(String)RowData[i][14]+",";
                    QS1 = QS1+"0"+(String)RowData[i][10]+",";
                    QS1 = QS1+"0"+(String)RowData[i][15]+",";
                    QS1 = QS1+"0"+(String)RowData[i][16]+",";
                    QS1 = QS1+"0"+(String)RowData[i][16]+",";
                    QS1 = QS1+"0"+SAdd+",";
                    QS1 = QS1+"0"+SLess+",";
                    QS1 = QS1+"0"+SMisc+",";
                    QS1 = QS1+"0"+SDeptCode+",";
                    QS1 = QS1+"0"+SGroupCode+",";
                    QS1 = QS1+"0"+SUnitCode+",";
                    QS1 = QS1+"0"+iSlNo+",";
                    QS1 = QS1+"0"+SCenVat+",";
                    QS1 = QS1+"0"+iUserCode+",";
                    QS1 = QS1+"0"+TInvoice.getText()+",";
                    QS1 = QS1+"0"+iSerlNo+",";
                    QS1 = QS1+"0"+iMillCode+")";
                         
                    if(theconnect.getAutoCommit())
                         theconnect     . setAutoCommit(false);

                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println("@QS1 : "+ex);
          }
     }

     public void setOrderLink()
     {
          try
          {
               if(theconnect==null)
               {
                     connect=ORAConnection.getORAConnection();
                     theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
               for(int i=0;i<RowData.length;i++)
               {
                    double dDcQty     = common.toDouble((String)RowData[i][4]);
                    if(dDcQty == 0)
                         continue;
     
                    String    QS = "Update WorkOrder Set ";
                              QS = QS+" InvQty=InvQty+"+(String)RowData[i][4];
                              QS = QS+" Where Id = "+(String)MiddlePanel.VId.elementAt(i);

                    if(theconnect.getAutoCommit())
                         theconnect    . setAutoCommit(false);
     
                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
          }
     }
     public String getNextOrderNo()
     {
          String QS = "";

          QS = " Select (MaxNo+1) From Config"+iMillCode+""+SYearCode+" "+
               " Where ID=12 ";

          String SOrderNo = String.valueOf(common.toInt(common.getID(QS)));
          return SOrderNo;
     }
     public void updateOrderNo()
     {
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();

               if(theconnect.getAutoCommit())
                    theconnect    . setAutoCommit(false);

               String QS1 = " Update Config"+iMillCode+""+SYearCode+" Set MaxNo="+(common.toInt((String)LGrnNo.getText()))+" Where ID=12 ";
               stat.executeUpdate(QS1);
               stat.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private boolean allOk()
     {

          String SCurDate     = common.parseDate(common.getServerPureDate());
          String SInvoice     = common.parseNull(TInvoice.getText().trim());
          String SGINo        = common.parseNull(TGINo.getText().trim());
          String SGIDate      = TGIDate.toNormal();
          String SInvNo       = common.parseNull(TInvNo.getText().trim());
          String SInvDate     = TInvDate.toNormal();
          String SDCNo        = common.parseNull(TDCNo.getText().trim());
          String SDCDate      = TDCDate.toNormal();
          int    iGIDateDiff  = common.toInt(common.getDateDiff(common.parseDate(SGIDate),SCurDate));
          int    iInvDateDiff = common.toInt(common.getDateDiff(common.parseDate(SInvDate),SCurDate));
          int    iDCDateDiff  = common.toInt(common.getDateDiff(common.parseDate(SDCDate),SCurDate));

          if(common.toInt(SInvoice)==0)
          {
              JOptionPane.showMessageDialog(null,"No. of Bills Field is Empty","Error",JOptionPane.ERROR_MESSAGE);
              TInvoice.requestFocus();
              return false;
          }
          if(common.toInt(SGINo)==0)
          {
              JOptionPane.showMessageDialog(null,"Invalid Gate Inward No","Error",JOptionPane.ERROR_MESSAGE);
              TGINo.requestFocus();
              return false;
          }
          if((SGINo.length()>0) && ((SGIDate.length()<8) || (iGIDateDiff>0)))
          {
              JOptionPane.showMessageDialog(null,"Gate Inward Date is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
              TGIDate.TDay.requestFocus();
              return false;
          }

          if((SInvNo.length()==0) && (SInvDate.length()>0))
          {
              JOptionPane.showMessageDialog(null,"Invoice No Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
              TInvNo.requestFocus();
              return false;
          }
          if((SInvNo.length()>0) && ((SInvDate.length()<8) || (iInvDateDiff>0)))
          {
              JOptionPane.showMessageDialog(null,"Invoice Date is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
              TInvDate.TDay.requestFocus();
              return false;
          }
          if((SDCNo.length()==0) && (SDCDate.length()>0))
          {
              JOptionPane.showMessageDialog(null,"D.C. No Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
              TDCNo.requestFocus();
              return false;
          }
          if((SDCNo.length()>0) && ((SDCDate.length()<8) || (iDCDateDiff>0)))
          {
              JOptionPane.showMessageDialog(null,"D.C. Date is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
              TDCDate.TDay.requestFocus();
              return false;
          }

          Object RowData[][]   = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<RowData.length;i++)
          {
               double dDcQty   = common.toDouble((String)RowData[i][4]);
               double dInvQty  = common.toDouble((String)RowData[i][5]);
               if(dDcQty != dInvQty)
               {
                    JOptionPane.showMessageDialog(null,"Quantity Mismatch","Error",JOptionPane.ERROR_MESSAGE);
                    MiddlePanel.requestFocus();
                    return false;
               }
          }

          double dTDCQty=0;
          for(int i=0;i<RowData.length;i++)
          {
               dTDCQty   = dTDCQty+common.toDouble((String)RowData[i][4]);
          }
          if(dTDCQty == 0)
          {
               JOptionPane.showMessageDialog(null,"Invalid GRN - All Zero","Error",JOptionPane.ERROR_MESSAGE);
               MiddlePanel.requestFocus();
               return false;
          }
          return true;

     }
     public boolean checkData()
     {
          if((TGINo.getText()).equals(""))
          {
               JOptionPane.showMessageDialog(null,"Gate Inward No Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TGINo.requestFocus();
               return false;
          }
          if((TGIDate.toNormal()).equals(""))
          {
               JOptionPane.showMessageDialog(null,"Gate Inward Date Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TGIDate.TDay.requestFocus();
               return false;
          }
          if((TInvoice.getText()).equals(""))
          {
               JOptionPane.showMessageDialog(null,"No.of Bills Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TInvoice.requestFocus();
               return false;
          }
          if((TInvNo.getText()).equals(""))
          {
               JOptionPane.showMessageDialog(null,"Invoice No Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TInvNo.requestFocus();
               return false;
          }
          if((TInvDate.toNormal()).equals(""))
          {
               JOptionPane.showMessageDialog(null,"Invoice Date Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TInvDate.requestFocus();
               return false;
          }
          return true;
     }

     private void setAutoCommitStatus()
     {
          try
          {
               if(bComflag)
               {
                    theconnect     . commit();
                    System         . out.println("Commit");
                    theconnect     . setAutoCommit(true);
               }
               else
               {
                    theconnect     . rollback();
                    System         . out.println("RollBack");
                    theconnect     . setAutoCommit(true);
               }
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
