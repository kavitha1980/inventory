package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class FinPendingList extends JInternalFrame
{
      JPanel     TopPanel;
      JPanel     MiddlePanel;
      JPanel     BottomPanel;
 
      String     SDate;
      JButton    BApply,BPrint;
      JTextField TFile;
      DateField  TDate;

      JLayeredPane DeskTop;
      StatusPanel SPanel;
      int iMillCode;
      String SSupTable,SMillName;

      Common common = new Common();
      ORAConnection connect;
      Connection theconnect;
      TabReport tabreport;

      Vector VAcCode,VAcName,VGrnNo,VGrnDate,VGateNo,VGateDate,VOrderNo;
      Vector VInvNo,VInvDate;
      Vector VMatName,VInvQty,VRecQty,VRejQty,VAcQty;
      Vector VInvValue,VOrdQty,VFinDelay,VGrnDelay;

      Vector VXOrderDate,VXOrderNo,VXIndex,VPaymentTerms;   

      String ColumnData[] = {"Supplier","Work Grn No","Work Grn Date","Work Order No","Order Date","Invoice No","Invoice Date","Material Name","Qty Accepted","Invoice Value","GRN Vs Finance","GI Vs GRN"};
      String ColumnType[] = {"S","S","S","S","S","S","S","S","N","V","N","N"};
      Object RowData[][];

      String SHead1 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String SHead2 = "|        WORK GRN         |          Gate           |        Work Order       |        Invoice          |                                          |                        Q U A N T I T Y                         |      Invoice |   Payment Terms     | Delayed Days  |";
      String SHead3 = "|-------------------------------------------------------------------------------------------------------|              Material Name               |----------------------------------------------------------------|        Value |                     |---------------|";
      String SHead4 = "|         No |       Date |         No |       Date |         No |       Date |         No |       Date |                                          |  As Per DC |   Received |   Rejected |   Accepted |    Ordered |           Rs |                     |  Grn | GI-GRN |";
      String SHead5 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String SHead6 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String SHead7 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String SHead8 = "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
      String SHead9 = "|            |            |            |            |            |            |            |            |                                          |            |            |            |            |            |              |                     |      |        |";

      int Lctr=100,Pctr=0;
      FileWriter FW;
      double dInvValue=0;

      FinPendingList(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode,String SSupTable,String SMillName)
      {
          super("Grn's Pending @ Finance As On ");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SMillName = SMillName;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
      }

      public void createComponents()
      {
          TopPanel    = new JPanel();
          MiddlePanel = new JPanel();
          BottomPanel = new JPanel();
 
          TDate    = new DateField();
          BApply   = new JButton("Apply");
          BPrint   = new JButton("Print");
          TFile    = new JTextField(15);
 
          TDate.setTodayDate();
          TFile.setText("Finance.prn");
      }

      public void setLayouts()
      {
          TopPanel.setLayout(new GridLayout(1,5));
          MiddlePanel.setLayout(new BorderLayout());
            
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
      }
                                                                        
      public void addComponents()
      {
          TopPanel.add(new JLabel("As On "));
          TopPanel.add(TDate);
          TopPanel.add(BApply);
 
          BottomPanel.add(BPrint);
          BottomPanel.add(TFile);
 
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
      }
      public void addListeners()
      {
          BApply.addActionListener(new ApplyList());
          BPrint.addActionListener(new PrintList());
      }
      public class ApplyList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setDataIntoVector();
                  setVectorIntoRowData();
                  try
                  {
                        MiddlePanel.remove(tabreport);
                  }
                  catch(Exception ex){}

                  tabreport = new TabReport(RowData,ColumnData,ColumnType);                        
                  MiddlePanel.add("Center",tabreport);
                  MiddlePanel.updateUI();
            }
      }
      public class PrintList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setPendingReport();
                  removeHelpFrame();
            }
      }
      public void removeHelpFrame()
      {
            try
            {
                  DeskTop.remove(this);
                  DeskTop.repaint();
                  DeskTop.updateUI();
            }
            catch(Exception ex) { }
      }

      private void setDataIntoVector()
      {
            VAcCode        = new Vector();
            VAcName        = new Vector();
            VGrnNo         = new Vector();
            VGrnDate       = new Vector();
            VGateNo        = new Vector();
            VGateDate      = new Vector();
            VOrderNo       = new Vector();
            VInvNo         = new Vector();
            VInvDate       = new Vector();
            VMatName       = new Vector();
            VInvQty        = new Vector();
            VRecQty        = new Vector();
            VRejQty        = new Vector();
            VAcQty         = new Vector();
            VInvValue      = new Vector();
            VOrdQty        = new Vector();

            VXOrderDate    = new Vector();
            VXOrderNo      = new Vector();
            VPaymentTerms  = new Vector();


            String QS  = "";
            String QS1 = "";

            QS  = " SELECT WorkGRN.Sup_Code, "+SSupTable+".NAME, WorkGRN.GrnNo, WorkGRN.GrnDate, WorkGRN.GateInNo, WorkGRN.GateInDate, WorkGRN.OrderNo, WorkGRN.InvNo, WorkGRN.InvDate, WorkGrn.Descript, WorkGRN.InvQty, WorkGRN.MillQty, WorkGRN.RejQty, WorkGRN.Qty, WorkGRN.InvNet,WorkGrn.Pending "+
                  " FROM WorkGRN INNER JOIN "+SSupTable+" ON WorkGRN.Sup_Code = "+SSupTable+".AC_CODE "+
                  " WHERE WorkGRN.GRNDate <= '"+TDate.toNormal()+"' And WorkGRN.SPJNo=0 And WorkGrn.Qty > 0 And WorkGrn.MillCode="+iMillCode+" ORDER BY "+SSupTable+".NAME,WorkGrn.GrnNo,WorkGRN.OrderNo,WorkGRN.GrnDate,WorkGrn.Descript ";

            QS1 = " Select WorkOrder.OrderDate,WorkGrn.OrderNo,WorkOrder.PayTerms From WorkOrder "+
                  " Inner Join WorkGRN On WorkOrder.OrderNo = WorkGrn.OrderNo And WorkGRN.MillCode=WorkOrder.MillCode "+
                  " Where WorkGRN.SPJNo=0  And WorkGrn.Qty > 0 And WorkGrn.MillCode="+iMillCode+" Order By 2,3 ";

            try
            {
                  if(theconnect==null)
                  {
                       connect=ORAConnection.getORAConnection();
                       theconnect=connect.getConnection();
                  }
                  Statement stat  = theconnect.createStatement();
                  ResultSet theResult = stat.executeQuery(QS);
                  while(theResult.next())
                  {
                        VAcCode        .addElement(theResult.getString(1));
                        VAcName        .addElement(theResult.getString(2));
                        VGrnNo         .addElement(theResult.getString(3));
                        VGrnDate       .addElement(theResult.getString(4));
                        VGateNo        .addElement(theResult.getString(5));
                        VGateDate      .addElement(theResult.getString(6));
                        VOrderNo       .addElement(theResult.getString(7));
                        VInvNo         .addElement(theResult.getString(8));
                        VInvDate       .addElement(theResult.getString(9));
                        VMatName       .addElement(theResult.getString(10));
                        VInvQty        .addElement(theResult.getString(11));
                        VRecQty        .addElement(theResult.getString(12));
                        VRejQty        .addElement(theResult.getString(13));
                        VAcQty         .addElement(theResult.getString(14));
                        VInvValue      .addElement(theResult.getString(15));
                        VOrdQty        .addElement(common.getRound(theResult.getString(16),2));
                  }
                  theResult.close();

                  ResultSet theResult1 = stat.executeQuery(QS1);
                  while(theResult1.next())
                  {
                      VXOrderDate  .addElement(theResult1.getString(1));    
                      VXOrderNo    .addElement(theResult1.getString(2));
                      VPaymentTerms.addElement(theResult1.getString(3));
                  }
                  theResult1.close();
                  stat.close();
                  setIndexTable();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      private void setVectorIntoRowData()
      {
            RowData = new Object[VAcCode.size()][ColumnData.length];
            for(int i=0;i<VAcCode.size();i++)
            {
                  String SFinDelay  = common.getDateDiff(TDate.toString(),common.parseDate((String)VGrnDate.elementAt(i)));
                  String SGateDelay = common.getDateDiff(common.parseDate((String)VGrnDate.elementAt(i)),common.parseDate((String)VGateDate.elementAt(i)));

                  RowData[i][0] = ""+(String)VAcName.elementAt(i);
                  RowData[i][1] = ""+(String)VGrnNo.elementAt(i);
                  RowData[i][2] = ""+common.parseDate((String)VGrnDate.elementAt(i));
                  RowData[i][3] = ""+(String)VOrderNo.elementAt(i);
                  RowData[i][4] = " ";
                  RowData[i][5] = ""+(String)VInvNo.elementAt(i);
                  RowData[i][6] = common.parseDate((String)VInvDate.elementAt(i));
                  RowData[i][7] = ""+(String)VMatName.elementAt(i);
                  RowData[i][8] = ""+(String)VAcQty.elementAt(i);
                  RowData[i][9] = ""+(String)VInvValue.elementAt(i);
                  RowData[i][10] =""+SFinDelay;
                  RowData[i][11] =""+SGateDelay;
            }
      }

     private void setPendingReport()
     {
          if(VAcCode.size() == 0)
               return;

          Lctr = 100;Pctr=0;

          String PAcCode   = (String)VAcCode.elementAt(0);
          String SPGrnNo   = "";
          String SFile     = TFile.getText();
          dInvValue = 0;

          if((SFile.trim()).length()==0)
             SFile = "1.prn";

          try
          {
               FW = new FileWriter(common.getPrintPath()+SFile);
               for(int i=0;i<VAcCode.size();i++)
               {
                    setHead((String)VAcName.elementAt(i));
                    String SAcCode = (String)VAcCode.elementAt(i);
                    String SGrnNo  = (String)VGrnNo.elementAt(i);
                    if(!SAcCode.equals(PAcCode))
                    {
                         setContBreak();
                         PAcCode = SAcCode;
                         setFirstLine(0,(String)VAcName.elementAt(i));
                         dInvValue = 0;
                    }
                    if(SPGrnNo.equals(SGrnNo))
                         setBody(i,false);
                    else
                    {
                         setBody(i,true);
                    }
                    SPGrnNo = SGrnNo;
               }
               setContBreak();
               FW.write(SHead8+"\n");
               FW.write("< End of Report >\n\n");
               FW.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void setHead(String SName) throws Exception
     {
          if (Lctr < 60)
               return;

          if (Pctr > 0)
               setPageBreak();

          Pctr++;
          String str1 = "Company  : "+SMillName;
          String str2 = "Document : Report on WorkGRN's Pending @ Finpro as On "+TDate.toString();
          String str3 = "Page No  : "+Pctr+"";
          FW.write("M"+str1+"\n");
          FW.write(str2+"\n");
          FW.write(str3+"\n");
          FW.write(SHead1+"\n");
          FW.write(SHead2+"\n");
          FW.write(SHead3+"\n");
          FW.write(SHead4+"\n");
          FW.write(SHead5+"\n");
          Lctr = 8;
          if(Pctr == 1)
            setFirstLine(0,SName);
     }

     private void setFirstLine(int i,String SName) throws Exception 
     {
          String str="";
          if(i==1)
               SName = SName+"(Contd...)";               

          
          String strx = "| "+common.Pad(SName,10+3+10+3+10+3+10+3+10+3+10+3)+
                        common.Rad(" ",10)+"   "+common.Pad(" ",10)+"   "+
                        common.Pad(" ",40)+"   "+
                        common.Rad(" ",10)+"   "+common.Rad(" ",10)+"   "+
                        common.Rad(" ",10)+"   "+common.Rad(" ",10)+"   "+
                        common.Rad(" ",10)+"   "+common.Rad(" ",12)+"   "+
                        common.Space(19)+" | "+
                        common.Rad(" ",4)+"   "+common.Rad(" ",6)+" |";

          FW.write(SHead6+"\n");
          FW.write(strx+"\n");
          FW.write(SHead7+"\n");
          Lctr = Lctr+3;
     }

     private void setContBreak() throws Exception
     {
          if(Lctr == 8)
            return;
          FW.write(SHead5+"\n");

          String str  = "| "+common.Rad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                        common.Rad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                        common.Rad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                        common.Rad(" ",10)+" | "+common.Pad(" ",10)+" | "+
                        common.Pad("T O T A L",40)+" | "+
                        common.Rad(" ",10)+" | "+common.Rad(" ",10)+" | "+
                        common.Rad(" ",10)+" | "+common.Rad(" ",10)+" | "+
                        common.Rad(" ",10)+" | "+common.Rad(common.getRound(dInvValue,2),12)+" | "+
                        common.Space(19)+" | "+
                        common.Rad(" ",4)+" | "+common.Rad(" ",6)+" |";

          FW.write(str+"\n");

          FW.write(SHead5+"\n");
          Lctr=Lctr+3;
     }
     private void setPageBreak() throws Exception
     {
          FW.write(SHead8+"\n");
     }
     private void setBody(int i,boolean bFlag) throws Exception
     {
          if(Lctr == 8)
          {
               setFirstLine(1,(String)VAcName.elementAt(i));
               bFlag = true;
          }
          if(bFlag && Lctr > 8)
          {
               FW.write(SHead9+"\n");
               Lctr++;
          }
          String SGrnNo = "",SGrnDate="",SGINo="",SGIDate="",SInvNo="",SInvDate="";
          if(bFlag)
          {
               SGrnNo     = (String)VGrnNo.elementAt(i);
               SGrnDate   = common.parseDate((String)VGrnDate.elementAt(i));
               SGINo      = (String)VGateNo.elementAt(i);
               SGIDate    = common.parseDate((String)VGateDate.elementAt(i));
               SInvNo     = (String)VInvNo.elementAt(i);
               SInvDate   = common.parseDate((String)VInvDate.elementAt(i));
          }
          String SOrderNo   = (String)VOrderNo.elementAt(i);
          String SOrderDate = common.parseDate(getOrdDate(i));
          String SPaymentTerms = getPaymentTerms(i);
          String SMatName   = (String)VMatName.elementAt(i);

          String SInvQty    = common.getRound((String)VInvQty.elementAt(i),3);
          String SRecQty    = common.getRound((String)VRecQty.elementAt(i),3);        
          String SRejQty    = common.getRound((String)VRejQty.elementAt(i),3);        
          String SAcQty     = common.getRound((String)VAcQty.elementAt(i),3);         
          String SOrdQty    = common.getRound((String)VOrdQty.elementAt(i),3);         

          String SInvValue  = common.getRound((String)VInvValue.elementAt(i),2);
          String SFinDelay  = common.getDateDiff(TDate.toString(),common.parseDate((String)VGrnDate.elementAt(i)));
          String SGateDelay = common.getDateDiff(common.parseDate((String)VGrnDate.elementAt(i)),common.parseDate((String)VGateDate.elementAt(i)));

          dInvValue         = dInvValue+common.toDouble(SInvValue);

          String str  = "| "+common.Rad(SGrnNo,10)+" | "+common.Pad(SGrnDate,10)+" | "+
                        common.Rad(SGINo,10)+" | "+common.Pad(SGIDate,10)+" | "+
                        common.Rad(SOrderNo,10)+" | "+common.Pad(SOrderDate,10)+" | "+
                        common.Rad(SInvNo,10)+" | "+common.Pad(SInvDate,10)+" | "+
                        common.Pad(SMatName,40)+" | "+
                        common.Rad(SInvQty,10)+" | "+common.Rad(SRecQty,10)+" | "+
                        common.Rad(SRejQty,10)+" | "+common.Rad(SAcQty,10)+" | "+
                        common.Rad(SOrdQty,10)+" | "+common.Rad(SInvValue,12)+" | "+
                        common.Pad(SPaymentTerms,19)+" | "+
                        common.Rad(SFinDelay,4)+" | "+common.Rad(SGateDelay,6)+" |";

          FW.write(str+"\n");
          Lctr++;
     }
     private void setIndexTable()
     {
          VXIndex = new Vector();
          for(int i=0;i<VXOrderNo.size();i++)
          {
               String str1 = (String)VXOrderNo.elementAt(i);
               VXIndex.addElement(str1);
          }
     }
     private int getOrderIndex(int i)
     {
          String str1  = (String)VOrderNo.elementAt(i);
          return VXIndex.indexOf(str1);
     }
     private String getOrdDate(int i)
     {
          int iIndex = getOrderIndex(i);
          if (iIndex == -1)
               return " ";
          return (String)VXOrderDate.elementAt(iIndex);
     }
     private String getPaymentTerms(int i)
     {
          int iIndex = getOrderIndex(i);
          if (iIndex == -1)
               return " ";
          return (String)VPaymentTerms.elementAt(iIndex);

     }
}
