package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;
import jdbc.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GIRdcMiddlePanel extends JPanel 
{
         JScrollPane TabScroll;
         InvMiddlePanel MiddlePanel;

         Object RowData[][];
         Object RDCData[];
         Object SlData[];
         String ColumnData[] = {"Name","DC/Inv Qty Mentioned By Supplier","Qty Measured @ Gate","Unmeasureable No of packs"};
         String ColumnType[] = {"S","E","E","E"};

         JLayeredPane Layer;

         Common common = new Common();
         public GIRdcMiddlePanel(JLayeredPane Layer)
         {
              this.Layer  = Layer;

              setLayout(new BorderLayout());
         }
         public void createComponents()
         {
               try
               {
                  MiddlePanel           = new InvMiddlePanel(Layer,RowData,ColumnData,ColumnType);
                  JScrollPane TabScroll = new JScrollPane(MiddlePanel);
                  add(TabScroll,BorderLayout.CENTER);
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
               }
         }
         public void setRowData(Vector VSelectedName,Vector VPendRDC,Vector VSlNo,Vector VID)
         {
            RowData     = new Object[VSelectedName.size()][ColumnData.length];
            RDCData     = new Object[VSelectedName.size()];
            SlData      = new Object[VSelectedName.size()];

            for(int i=0;i<VSelectedName.size();i++)
            {
                  RDCData[i]    = (String)VPendRDC.elementAt(i);
                  SlData[i]     = (String)VSlNo.elementAt(i);
                  RowData[i][0] = (String)VSelectedName.elementAt(i);
                  RowData[i][1] = "";
                  RowData[i][2] = "";
                  RowData[i][3] = "";
            }
         }
}
