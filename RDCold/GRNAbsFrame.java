package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GRNAbsFrame extends JInternalFrame
{
     String      SStDate,SEnDate;
     JComboBox   JCOrder,JCFilter;
     JButton     BApply,BPrint,BList;
     NextField   TNo;
     JPanel      TopPanel;
     JPanel      BottomPanel;
     MyTextField TFile;
    
     TabReport tabreport;
     DateField TStDate;
     DateField TEnDate;
    
     Object RowData[][];
     String ColumnData[] = {"Click To Print","GRN No","Date","Supplier","GI No","GI Date","Inv No","Inv Date","DC No","DC Date","Basic","Discount","CenVat","Tax","Surcharge","Plus","Minus","Net"};
     String ColumnType[] = {"B","S","S","S","N","S","S","S","S","S","N","N","N","N","N","N","N","N"};
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;

     Vector VGrnNo,VDate,VSupName,VGINo,VGIDate,VInvNo,VInvDate,VDCNo,VDCDate,VRecBasic,VAccBasic,VDisc,VCenVat,VTax,VSur,VAdd,VLess,VNet,VSign;
     Vector VSupCode;

     JLayeredPane DeskTop;
     StatusPanel SPanel;
     int iMillCode;
     String SSupTable,SMillName;

     FileWriter FW;

     GRNAbsFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode,String SSupTable,String SMillName)
     {
          super("Abstract on GRNs During a Period");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SMillName = SMillName;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     public void createComponents()
     {
          TStDate     = new DateField();
          TEnDate     = new DateField();
          BApply      = new JButton("Apply");
          BPrint      = new JButton("Print Marked GRNs");
          BList       = new JButton("Print Marked Lists");
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();
          JCOrder     = new JComboBox();
          JCFilter    = new JComboBox();
          TNo         = new NextField(5);
          TFile       = new MyTextField(20);

          TStDate.setTodayDate();
          TEnDate.setTodayDate();
          TFile.setText("WorkGrn.prn");
          BPrint.setEnabled(false);
          TFile.setEditable(false);

          BApply.setMnemonic('A');
          BPrint.setMnemonic('P');
     }

     public void setLayouts()
     {
          TopPanel.setLayout(new FlowLayout());
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
     }

     public void addComponents()
     {
          JCOrder.addItem("GRN No");
          JCOrder.addItem("Supplierwise");

          JCFilter.addItem("All");
          JCFilter.addItem("Invoice Received");
          JCFilter.addItem("Invoice Not Received");

          TopPanel.add(TNo);
          
          TopPanel.add(new JLabel("Sorted On"));
          TopPanel.add(JCOrder);

          TopPanel.add(new JLabel("Show Only"));
          TopPanel.add(JCFilter);
          
          TopPanel.add(TStDate);
          TopPanel.add(TEnDate);
          TopPanel.add(BApply);

          BottomPanel.add(BPrint);
          BottomPanel.add(TFile);

          BottomPanel         . add(BList);
          BList              . setEnabled(false);


          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply.addActionListener(new ApplyList());
          BPrint.addActionListener(new PrintList());
          BList.addActionListener(new List());

     }    
     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    String SFile = TFile.getText();
                    if((SFile.trim()).length()==0)
                         SFile = "WorkGrn.prn";

                    FW = new FileWriter(common.getPrintPath()+SFile);

                    for(int i=0;i<RowData.length;i++)
                    {
                         Boolean BValue = (Boolean)RowData[i][0];
                         if(BValue.booleanValue())
                         {
                                   String SGrnNo     = (String)VGrnNo.elementAt(i);
                                   String SGrnDate   = (String)VDate.elementAt(i);
                                   String SSupCode   = (String)VSupCode.elementAt(i);
                                   String SSupName   = (String)VSupName.elementAt(i);
                                   String SNet       = (String)VNet.elementAt(i);
                                   new GRNPrint(FW,SGrnNo,SGrnDate,SSupCode,SSupName,SNet,iMillCode,SMillName);
                         }
                    }
                    FW.close();
                    removeHelpFrame();
               }
               catch(Exception ex){};
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport = new TabReport(RowData,ColumnData,ColumnType);
                    tabreport.ReportTable.addKeyListener(new TabKeyList());
                    getContentPane().add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    DeskTop.repaint();
                    DeskTop.updateUI();
                    BPrint.setEnabled(true);
                    TFile.setEditable(true);
                    BList.setEnabled(true);

               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }
     public class List implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               printData();
          }
     }

     public class TabKeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_F2)
                    setStatus(true);
               if(ke.getKeyCode()==KeyEvent.VK_F3)
                    setStatus(false);
          }
     }

     public void setStatus(boolean bFlag)
     {
          for(int i=0;i<RowData.length;i++)
               RowData[i][0] = new Boolean(bFlag);

          tabreport.ReportTable.updateUI();
     }
     public void printData()
     {
            String STitle = "Work GRN Abstract Report Taken On: " +common.getServerDate(); 

            String SFile  = "WorkGrnPrintList.prn";
            new DocPrint(getPendingBody(),getPendingHead(),STitle,SFile,iMillCode,SMillName);
     }

     public Vector getPendingBody()
     {
        Vector vect = new Vector();

        int iCount=0;

          for(int i=0;i<RowData.length;i++)
          {
               Boolean BValue = (Boolean)RowData[i][0];

               if(!BValue.booleanValue())
                    continue;


              iCount++;

              String strl="";

              String SGRNNo       = (String)VGrnNo.elementAt(i);
              String SGRNDate     = common.parseDate((String)VDate.elementAt(i));
              String SSupplier    = common.parseNull((String)VSupName.elementAt(i));
              String SInvNo       = (String)VInvNo.elementAt(i);
              String SInvDate     = common.parseDate((String)VInvDate.elementAt(i));
              String SDCNo        = (String)VDCNo.elementAt(i);
              String SDCDate      = common.parseDate((String)VDCDate.elementAt(i));
//            String SRecBasic    = common.getRound(common.toDouble((String)VRecBasic.elementAt(i)),2);
//            String SDiscount    = (String)VDisc.elementAt(i);
//            String SCenVat      = (String)VCenVat.elementAt(i);
//            String STax         = (String)VTax.elementAt(i);
//            String SSurcharge   = (String)VSur.elementAt(i);
//            String SPlus        = (String)VAdd.elementAt(i);
//            String SMinus       = (String)VLess.elementAt(i);
              String SNet         = common.getRound(common.toDouble((String)VNet.elementAt(i)),2);

              strl = strl + common.Cad(String.valueOf(iCount),5)+common.Space(3);
              strl = strl + common.Cad(SGRNNo,9)+common.Space(3);
              strl = strl + common.Cad(SGRNDate,12)+common.Space(3);
              strl = strl + common.Cad(SSupplier,15)+common.Space(3);
              strl = strl + common.Cad(SInvNo,9)+common.Space(3);
              strl = strl + common.Cad(SInvDate,12)+common.Space(3);
              strl = strl + common.Cad(SDCNo,9)+common.Space(3);
              strl = strl + common.Cad(SDCDate,12)+common.Space(3);
//            strl = strl + common.Cad(SRecBasic,8)+common.Space(3);
//            strl = strl + common.Cad(SDiscount,9)+common.Space(3);
//            strl = strl + common.Cad(SCenVat,7)+common.Space(3);
//            strl = strl + common.Cad(STax,5)+common.Space(3);
//            strl = strl + common.Cad(SSurcharge,9)+common.Space(3);
//            strl = strl + common.Cad(SPlus,5)+common.Space(3);
//            strl = strl + common.Cad(SMinus,5)+common.Space(3);
              strl = strl + common.Rad(SNet,12)+common.Space(3);
              strl = strl + common.Cad("",10)+common.Space(3);
              

              strl = strl+"\n";
              vect.addElement(strl);
        }
        return vect;
     }

     public Vector getPendingHead()
     {
           Vector vect = new Vector();

           String strl1 = "";

              strl1 = strl1 + common.Cad("S.No",5)+common.Space(3);
              strl1 = strl1 + common.Cad("GRN No",9)+common.Space(3);
              strl1 = strl1 + common.Cad("GRN Date",12)+common.Space(3);
              strl1 = strl1 + common.Cad("Supplier",15)+common.Space(3);
              strl1 = strl1 + common.Cad("Inv No",9)+common.Space(3);
              strl1 = strl1 + common.Cad("Inv Date",12)+common.Space(3);
              strl1 = strl1 + common.Cad("DC No",9)+common.Space(3);
              strl1 = strl1 + common.Cad("DC Date",12)+common.Space(3);
//            strl1 = strl1 + common.Cad("Basic",8)+common.Space(3);
//            strl1 = strl1 + common.Cad("Discount",9)+common.Space(3);
//            strl1 = strl1 + common.Cad("CenVat",7)+common.Space(3);
//            strl1 = strl1 + common.Cad("Tax",5)+common.Space(3);
//            strl1 = strl1 + common.Cad("Surcharge",9)+common.Space(3);
//            strl1 = strl1 + common.Cad("Plus",5)+common.Space(3);
//            strl1 = strl1 + common.Cad("Minus",5)+common.Space(3);
              strl1 = strl1 + common.Cad("Net",12)+common.Space(3);
              strl1 = strl1 + common.Cad("Sign",10)+common.Space(3);

           strl1 = strl1+"\n";

           vect.addElement(strl1);

           return vect;
     } 

     public void setDataIntoVector()
     {
          VDate      = new Vector();
          VGrnNo     = new Vector();
          VSupName   = new Vector();
          VGINo      = new Vector();
          VGIDate    = new Vector();
          VInvNo     = new Vector();
          VInvDate   = new Vector();
          VDCNo      = new Vector();
          VDCDate    = new Vector();
          VRecBasic  = new Vector();
          VAccBasic  = new Vector();
          VDisc      = new Vector();
          VCenVat    = new Vector();
          VTax       = new Vector();
          VSur       = new Vector();
          VAdd       = new Vector();
          VLess      = new Vector();
          VNet       = new Vector();
          VSupCode   = new Vector();

          SStDate = TStDate.TDay.getText()+"."+TStDate.TMonth.getText()+"."+TStDate.TYear.getText();
          SEnDate = TEnDate.TDay.getText()+"."+TEnDate.TMonth.getText()+"."+TEnDate.TYear.getText();

          String StDate  = TStDate.TYear.getText()+TStDate.TMonth.getText()+TStDate.TDay.getText();
          String EnDate  = TEnDate.TYear.getText()+TEnDate.TMonth.getText()+TEnDate.TDay.getText();

          try
          {
               if(theconnect==null)
               {
                     connect=ORAConnection.getORAConnection();
                     theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               String QString = getQString(StDate,EnDate);
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    String str = res.getString(6);

                    if(JCFilter.getSelectedIndex()==1)
                    if(((str.trim()).length())==0)
                         continue;

                    if(JCFilter.getSelectedIndex()==2)
                    if(((str.trim()).length())>0)
                         continue;

                    VGrnNo    .addElement(common.parseNull(res.getString(1)));
                    VDate     .addElement(common.parseNull(common.parseDate(res.getString(2))));
                    VSupName  .addElement(common.parseNull(res.getString(3)));
                    VGINo     .addElement(common.parseNull(res.getString(4)));
                    VGIDate   .addElement(common.parseNull(res.getString(5)));
                    VInvNo    .addElement(common.parseNull(str));
                    VInvDate  .addElement(common.parseNull(res.getString(7)));
                    VDCNo     .addElement(common.parseNull(res.getString(8)));
                    VDCDate   .addElement(common.parseNull(res.getString(9)));
                    VRecBasic .addElement(common.parseNull(res.getString(10)));
                    VDisc     .addElement(common.parseNull(res.getString(11)));
                    VCenVat   .addElement(common.parseNull(res.getString(12)));
                    VTax      .addElement(common.parseNull(res.getString(13)));
                    VSur      .addElement(common.parseNull(res.getString(14)));
                    VNet      .addElement(common.parseNull(res.getString(15)));
                    VAdd      .addElement(common.parseNull(res.getString(16)));
                    VLess     .addElement(common.parseNull(res.getString(17)));
                    VAccBasic .addElement(common.parseNull(res.getString(18)));
                    VSupCode  .addElement(common.parseNull(res.getString(19)));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VGrnNo.size()][ColumnData.length];
          for(int i=0;i<VDate.size();i++)
          {
               double dAccBasic = common.toDouble((String)VAccBasic.elementAt(i));
               RowData[i][1]  = (String)VGrnNo.elementAt(i);
               RowData[i][2]  = (String)VDate.elementAt(i);
               RowData[i][3]  = (String)VSupName.elementAt(i);
               RowData[i][4]  = (String)VGINo.elementAt(i);
               RowData[i][5]  = common.parseDate((String)VGIDate.elementAt(i));
               RowData[i][6]  = (String)VInvNo.elementAt(i);
               RowData[i][7]  = common.parseDate((String)VInvDate.elementAt(i));
               RowData[i][8]  = (String)VDCNo.elementAt(i);
               RowData[i][9]  = (String)VDCDate.elementAt(i);
               if (dAccBasic == 0)
               {
                    RowData[i][10] =  (String)VRecBasic.elementAt(i);
               }
               else
               {
                    RowData[i][10] =  (String)VAccBasic.elementAt(i);
               }
               RowData[i][11] =  (String)VDisc.elementAt(i);
               RowData[i][12] =  (String)VCenVat.elementAt(i);
               RowData[i][13] =  (String)VTax.elementAt(i);
               RowData[i][14] =  (String)VSur.elementAt(i);
               RowData[i][15] =  (String)VAdd.elementAt(i);
               RowData[i][16] =  (String)VLess.elementAt(i);
               double dRNet = common.toDouble((String)VNet.elementAt(i))+common.toDouble((String)VAdd.elementAt(i))-common.toDouble((String)VLess.elementAt(i));
               RowData[i][17] = ""+dRNet;
               RowData[i][0] = new Boolean(false);
          }  
     }

     public String getQString(String StDate,String EnDate)
     {
          int iNo = common.toInt(TNo.getText());
          String QString  = "";
     
          QString = " SELECT WorkGRN.GRNNo,WorkGRN.GRNDate, "+
                    " "+SSupTable+".Name,WorkGRN.GateInNo,WorkGRN.GateInDate, "+
                    " WorkGRN.InvNo,WorkGRN.InvDate,WorkGRN.DCNo,WorkGRN.DCDate, "+
                    " sum(WorkGRN.MillQty*WorkGRN.InvRate),sum(WorkGRN.Disc), "+
                    " sum(WorkGRN.Cenvat),sum(WorkGRN.Tax),sum(WorkGRN.Sur), "+
                    " sum(WorkGRN.InvAmount),WorkGRN.Plus,WorkGRN.Less, "+
                    " sum(WorkGRN.Qty*WorkGRN.InvRate),WorkGRN.Sup_Code "+
                    " From WorkGRN Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = WorkGRN.Sup_Code "+
                    " And WorkGRN.MillCode="+iMillCode+
                    " Group By WorkGRN.GrnNo,WorkGRN.GRNDate,"+SSupTable+".Name, "+
                    " WorkGRN.GateInNo,WorkGRN.GateInDate,WorkGRN.InvNo,WorkGRN.InvDate, "+
                    " WorkGRN.DCNo,WorkGRN.DCDate,WorkGRN.Plus,WorkGRN.Less,WorkGRN.Sup_Code ";
                    
          if(iNo > 0)
               QString = QString+" Having WorkGRN.GRNNo = "+iNo;
          else
               QString = QString+" Having WorkGRN.GRNDate >= '"+StDate+"' and WorkGRN.GRNDate <='"+EnDate+"'";

          if(JCOrder.getSelectedIndex() == 0)      
               QString = QString+" Order By WorkGRN.GrnNo ";
          if(JCOrder.getSelectedIndex() == 1)      
               QString = QString+" Order By "+SSupTable+".Name,WorkGRN.GRNNo ";

          return QString;
     }

}
