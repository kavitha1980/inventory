package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GRNModiFrame extends JInternalFrame
{

     MyTextField    TGINo;
     DateField      TDate,TGIDate,TInvDate,TDCDate;
     MyTextField    TInvNo,TDCNo;
     JTextField     TGrnNo,TSupName;
     WholeNumberField TInvoice;
     GRNMiddlePanel MiddlePanel;
     JPanel         TopPanel,BottomPanel;
     JButton        BOk,BCancel;

     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel  SPanel;
     int iMillCode;
     GRNListFrame grnlistframe;
     String SSupTable;

     String SGrnNo,SGateNo,SPJNo;
     Common common         = new Common();
     ORAConnection connect;
     Connection theconnect;
     GRNRecords grnrecords;

     GRNModiFrame(JLayeredPane DeskTop,String SGrnNo,String SGateNo,String SPJNo,int iMillCode,GRNListFrame grnlistframe,String SSupTable)
     {
          this.DeskTop            = DeskTop;
          this.SGrnNo             = SGrnNo;
          this.SGateNo            = SGateNo;
          this.SPJNo              = SPJNo;
          this.iMillCode          = iMillCode;
          this.grnlistframe       = grnlistframe;
          this.SSupTable          = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setData();
          show();
     }
     public void createComponents()
     {
          BOk        = new JButton("Update");
          BCancel    = new JButton("Abort");

          TDate       = new DateField();
          TGrnNo      = new JTextField();
          TGINo       = new MyTextField(8);
          TGIDate     = new DateField();
          TInvDate    = new DateField();
          TDCDate     = new DateField();
          TInvNo      = new MyTextField(15);
          TDCNo       = new MyTextField(15);
          TSupName    = new JTextField();
          TInvoice    = new WholeNumberField(2);

          MiddlePanel = new GRNMiddlePanel(DeskTop,iMillCode);

          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          TGrnNo.setEditable(false);
          TSupName.setEditable(false);
          BOk.setMnemonic('U');
          BCancel.setMnemonic('A');

          if(common.toInt(SPJNo) > 0)
                BOk.setEnabled(false);
     }
     public void setLayouts()
     {
          setTitle("Modification of Invoice Valuation");

          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,790,500);

          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(5,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());
     }
     public void addComponents()
     {
          TopPanel.add(new JLabel("Gate Inward No"));
          TopPanel.add(TGINo);

          TopPanel.add(new JLabel("Gate Inward Date"));
          TopPanel.add(TGIDate);

          TopPanel.add(new JLabel("GRN No"));
          TopPanel.add(TGrnNo);

          TopPanel.add(new JLabel("GRN Date"));
          TopPanel.add(TDate);

          TopPanel.add(new JLabel("Supplier"));
          TopPanel.add(TSupName);

          TopPanel.add(new JLabel("No. of Bills"));
          TopPanel.add(TInvoice);

          TopPanel.add(new JLabel("Invoice No"));
          TopPanel.add(TInvNo);

          TopPanel.add(new JLabel("Invoice Date"));
          TopPanel.add(TInvDate);

          TopPanel.add(new JLabel("DC No"));
          TopPanel.add(TDCNo);

          TopPanel.add(new JLabel("DC Date"));
          TopPanel.add(TDCDate);

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
          BOk.addActionListener(new ActList());
          BCancel.addActionListener(new CancelList());
     }
     public class CancelList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
                removeHelpFrame();
          }

     }
     public class ActList implements ActionListener
     {
            public void actionPerformed(ActionEvent ae)
            {
                    BOk.setEnabled(false);
                    /*boolean bSig = false;
                    try
                    {
                         bSig = allOk();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
                    if(bSig)
                    {*/
                         updateGRNDetails();
                         removeHelpFrame();
                         grnlistframe.setTabReport(grnlistframe);
                    /*}
                    else
                    {
                         BOk.setEnabled(true);
                    }*/
            }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
     public void updateGRNDetails()
     {
          String QString = "Update WorkGRN Set ";
          try
          {
               if(theconnect==null)
               {
                     connect=ORAConnection.getORAConnection();
                     theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               String SSupCode    = MiddlePanel.SSupCode;
               Object RowData[][] = MiddlePanel.MiddlePanel.getFromVector();
               String SAdd        = MiddlePanel.MiddlePanel.TAdd.getText();
               String SLess       = MiddlePanel.MiddlePanel.TLess.getText();
               double dpm         = common.toDouble(SAdd)-common.toDouble(SLess);
               double dActVat     = MiddlePanel.MiddlePanel.getModVat();
               double dVatBasic   = MiddlePanel.MiddlePanel.getVatBasic();
               double dBasic      = common.toDouble(MiddlePanel.MiddlePanel.LBasic.getText());
               double dRatio      = dpm/dBasic;
               double dVatRatio   = dActVat/dVatBasic;               
               for(int i=0;i<RowData.length;i++)
               {
                    String SUnitCode  = MiddlePanel.MiddlePanel.getUnitCode(i);
                    String SDeptCode  = MiddlePanel.MiddlePanel.getDeptCode(i);
                    String SGroupCode = MiddlePanel.MiddlePanel.getGroupCode(i);
                    dBasic            = common.toDouble(((String)RowData[i][11]).trim());
                    String SMisc      = common.getRound(dBasic*dRatio,3);
                    double dCenVatPer = common.toDouble((String)RowData[i][8]);
                    String SCenVat    = "0";

                    if(dCenVatPer > 0)
                         SCenVat = common.getRound(dBasic*dVatRatio,3);

                    String QS1 = QString;
                    QS1 = QS1+"GrnDate='"+TDate.toNormal()+"',";
                    QS1 = QS1+"GateInNo='"+TGINo.getText()+"',";
                    QS1 = QS1+"GateInDate='"+TGIDate.toNormal()+"',";
                    QS1 = QS1+"InvNo='"+TInvNo.getText()+"',";
                    QS1 = QS1+"InvDate='"+TInvDate.toNormal()+"',";
                    QS1 = QS1+"DCNo='"+TDCNo.getText()+"',";
                    QS1 = QS1+"DCDate='"+TDCDate.toNormal()+"',";
                    QS1 = QS1+"InvRate=0"+(String)RowData[i][6]+",";
                    QS1 = QS1+"DiscPer=0"+(String)RowData[i][7]+",";
                    QS1 = QS1+"Disc=0"+(String)RowData[i][12]+",";
                    QS1 = QS1+"CenvatPer=0"+(String)RowData[i][8]+",";
                    QS1 = QS1+"Cenvat=0"+(String)RowData[i][13]+",";
                    QS1 = QS1+"TaxPer=0"+(String)RowData[i][9]+",";
                    QS1 = QS1+"Tax=0"+(String)RowData[i][14]+",";
                    QS1 = QS1+"SurPer=0"+(String)RowData[i][10]+",";
                    QS1 = QS1+"Sur=0"+(String)RowData[i][15]+",";
                    QS1 = QS1+"InvAmount=0"+(String)RowData[i][16]+",";
                    QS1 = QS1+"InvNet=0"+(String)RowData[i][16]+",";
                    QS1 = QS1+"Plus=0"+SAdd+",";
                    QS1 = QS1+"Less=0"+SLess+",";
                    QS1 = QS1+"Misc=0"+SMisc+",";
                    QS1 = QS1+"Dept_Code=0"+SDeptCode+",";
                    QS1 = QS1+"Group_Code=0"+SGroupCode+",";
                    QS1 = QS1+"Unit_Code=0"+SUnitCode+",";
                    QS1 = QS1+"ActualModVat=0"+SCenVat+",";
                    QS1 = QS1+"NoOfBills=0"+TInvoice.getText();
                    QS1 = QS1+" Where Id = "+(String)MiddlePanel.IdData[i];
                    stat.executeUpdate(QS1);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private boolean allOk()
     {

          String SCurDate     = common.parseDate(common.getServerPureDate());
          String SInvoice     = common.parseNull(TInvoice.getText().trim());
          String SGINo        = common.parseNull(TGINo.getText().trim());
          String SGIDate      = TGIDate.toNormal();
          String SInvNo       = common.parseNull(TInvNo.getText().trim());
          String SInvDate     = TInvDate.toNormal();
          String SDCNo        = common.parseNull(TDCNo.getText().trim());
          String SDCDate      = TDCDate.toNormal();
          int    iGIDateDiff  = common.toInt(common.getDateDiff(common.parseDate(SGIDate),SCurDate));
          int    iInvDateDiff = common.toInt(common.getDateDiff(common.parseDate(SInvDate),SCurDate));
          int    iDCDateDiff  = common.toInt(common.getDateDiff(common.parseDate(SDCDate),SCurDate));


          if(common.toInt(SInvoice)==0)
          {
              JOptionPane.showMessageDialog(null,"No. of Bills Field is Empty","Error",JOptionPane.ERROR_MESSAGE);
              TInvoice.requestFocus();
              return false;
          }

          if(common.toInt(SGINo)==0)
          {
              JOptionPane.showMessageDialog(null,"Invalid Gate Inward No","Error",JOptionPane.ERROR_MESSAGE);
              TGINo.requestFocus();
              return false;
          }
          if((SGINo.length()>0) && ((SGIDate.length()<8) || (iGIDateDiff>0)))
          {
              JOptionPane.showMessageDialog(null,"Gate Inward Date is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
              TGIDate.TDay.requestFocus();
              return false;
          }

          if((SInvNo.length()==0) && (SInvDate.length()>0))
          {
              JOptionPane.showMessageDialog(null,"Invoice No Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
              TInvNo.requestFocus();
              return false;
          }
          if((SInvNo.length()>0) && ((SInvDate.length()<8) || (iInvDateDiff>0)))
          {
              JOptionPane.showMessageDialog(null,"Invoice Date is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
              TInvDate.TDay.requestFocus();
              return false;
          }
          if((SDCNo.length()==0) && (SDCDate.length()>0))
          {
              JOptionPane.showMessageDialog(null,"D.C. No Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
              TDCNo.requestFocus();
              return false;
          }
          if((SDCNo.length()>0) && ((SDCDate.length()<8) || (iDCDateDiff>0)))
          {
              JOptionPane.showMessageDialog(null,"D.C. Date is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
              TDCDate.TDay.requestFocus();
              return false;
          }

          return true;

     }

     public void setData()
     {
          grnrecords = new GRNRecords(SGrnNo,iMillCode,SSupTable);
          TGrnNo.setText(SGrnNo);
          TGINo.setText(SGateNo);
          TGIDate.fromString(common.parseDate(grnrecords.SGIDate));
          TSupName.setText(grnrecords.SSupName);
          TInvNo.setText(grnrecords.SInvNo);
          TInvDate.fromString(common.parseDate(grnrecords.SInvDate));
          TDCNo.setText(grnrecords.SDCNo);
          TDCDate.fromString(common.parseDate(grnrecords.SDCDate));
          TDate.fromString(common.parseDate(grnrecords.SGDate));
          TInvoice.setText(grnrecords.SInvoice);

          MiddlePanel.setData(grnrecords);
          MiddlePanel.MiddlePanel.TAdd.setText(grnrecords.SAdd);
          MiddlePanel.MiddlePanel.TLess.setText(grnrecords.SLess);
          MiddlePanel.MiddlePanel.TModVat.setText(grnrecords.SModVat);
          MiddlePanel.MiddlePanel.calc();
     }
}
