package RDC;

import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GRNRecords
{
     String SGDate = "",SSupName = "",SGIDate = "";
     String SInvNo = "",SInvDate = "",SDCNo = "",SDCDate = "";
     String SAdd = "0",SLess = "0",SModVat="0",SInvoice="0";
     Vector VGOrdNo,VGOrdQty,VGPending,VGItemName,VGInvQty,VGRecdQty,VGAccQty;
     Vector VGInvRate,VGDiscPer,VGCenVatPer,VGTaxPer,VGSurPer;
     Vector VGDeptName,VGGroupName,VGUnitName;
     Vector VId,VPId;
     
     String SGrnNo;
     int iMillCode;
     String SSupTable;
     
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;

    GRNRecords(String SGrnNo,int iMillCode,String SSupTable)
    {
        this.SGrnNo    = SGrnNo;
        this.iMillCode = iMillCode;
        this.SSupTable = SSupTable;

        VGOrdNo     = new Vector();
        VGOrdQty    = new Vector();
        VGPending   = new Vector();
        VGItemName  = new Vector();
        VGInvQty    = new Vector();
        VGRecdQty   = new Vector();
        VGAccQty    = new Vector();
        VGInvRate   = new Vector();
        VGDiscPer   = new Vector();
        VGCenVatPer = new Vector();
        VGTaxPer    = new Vector();
        VGSurPer    = new Vector();
        VGDeptName  = new Vector();
        VGGroupName = new Vector();
        VGUnitName  = new Vector();
        VId         = new Vector();
        VPId        = new Vector();

        String QS = " SELECT WorkGRN.GRNDate, WorkGRN.GateInDate, "+
                    " WorkGRN.InvNo, WorkGRN.InvDate, WorkGRN.DcNo, "+
                    " WorkGRN.DcDate, WorkGRN.OrderNo, "+SSupTable+".Name, "+
                    " WorkGRN.Descript, "+
                    " WorkGRN.InvQty, WorkGRN.InvRate, WorkGRN.DiscPer, "+
                    " WorkGRN.CenvatPer, WorkGRN.TaxPer, WorkGRN.SurPer, "+
                    " Dept.Dept_Name, Cata.Group_Name, "+
                    " Unit.Unit_Name, WorkGRN.Id, "+
                    " WorkGRN.Plus,WorkGRN.Less,WorkGRN.OrderQty,"+
                    " WorkGRN.Pending,WorkGRN.MillQty,WorkOrder.Id,WorkGRN.Qty,WorkGRN.NoOfBills "+
                    " FROM ((((WorkGRN INNER JOIN "+SSupTable+" ON WorkGRN.Sup_Code="+SSupTable+".Ac_Code) "+
                    " INNER JOIN Dept ON WorkGRN.Dept_Code=Dept.Dept_code) "+
                    " INNER JOIN Cata ON WorkGRN.Group_Code=Cata.Group_Code) "+
                    " INNER JOIN Unit ON WorkGRN.Unit_Code=Unit.Unit_Code) "+
                    " Inner JOIN WorkOrder ON ((WorkGRN.OrderQty=WorkOrder.Qty) AND (WorkGRN.Descript=WorkOrder.Descript) "+
                    " AND (WorkGRN.OrderNo=WorkOrder.OrderNo) And (WorkGRN.MillCode=WorkOrder.MillCode)) "+
                    " Where WorkGRN.MillCode="+iMillCode+" And WorkGRN.GrnNo = "+SGrnNo;

        String QS1= " SELECT Sum(WorkGRN.ActualModVat) as ModVat "+
                    " FROM WorkGRN  "+
                    " Where WorkGRN.MillCode="+iMillCode+" And WorkGRN.GrnNo = "+SGrnNo;


        try
        {
            if(theconnect==null)
            {
                 connect=ORAConnection.getORAConnection();
                 theconnect=connect.getConnection();
            }
            Statement stat   = theconnect.createStatement();
            ResultSet res1 = stat.executeQuery(QS1);
            while (res1.next())
            {
                SModVat = res1.getString(1);
            }
            res1.close();

            ResultSet res  = stat.executeQuery(QS);
            while (res.next())
            {
                SGDate   = common.parseDate(res.getString(1));
                SGIDate  = common.parseDate(res.getString(2));
                SInvNo   = res.getString(3);
                SInvDate = common.parseDate(res.getString(4));
                SDCNo    = res.getString(5);
                SDCDate  = common.parseDate(res.getString(6));

                VGOrdNo     .addElement(res.getString(7));
                SSupName = res.getString(8);

                VGItemName  .addElement(res.getString(9));
                VGInvQty    .addElement(res.getString(10));
                VGInvRate   .addElement(res.getString(11));
                VGDiscPer   .addElement(res.getString(12));
                VGCenVatPer .addElement(res.getString(13));
                VGTaxPer    .addElement(res.getString(14));
                VGSurPer    .addElement(res.getString(15));
                VGDeptName  .addElement(res.getString(16));
                VGGroupName .addElement(res.getString(17));
                VGUnitName  .addElement(res.getString(18));
                VId         .addElement(res.getString(19));
                SAdd  = res.getString(20);
                SLess = res.getString(21);
                VGOrdQty    .addElement(res.getString(22));
                VGPending   .addElement(res.getString(23));
                VGRecdQty   .addElement(res.getString(24));
                VPId        .addElement(res.getString(25));
                VGAccQty    .addElement(res.getString(26));
                SInvoice = res.getString(27);
            }
            res.close();
            stat.close();
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
    }
}

