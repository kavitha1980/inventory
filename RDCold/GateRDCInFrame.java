package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;
import jdbc.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class GateRDCInFrame extends JInternalFrame
{
     JButton        BOk,BCancel,BPending,BSupplier;
     NextField      TGateNo;
     DateField      TGateDate,TInvDate,TDCDate;
     JTextField     TSupCode,TInvNo,TDCNo;

     GIRdcMiddlePanel    MiddlePanel;
     JPanel              TopPanel,BottomPanel;

     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     int iUserCode,iMillCode;
     String SSupTable;

     String         SGINo ="";

     Common common   = new Common();
     ORAConnection connect;
     Connection theconnect;
          
     GateRDCInFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable)
     {
          super("Gate Inward of RDC Materials");
          this.DeskTop            = DeskTop;
          this.SPanel             = SPanel;
          this.iUserCode          = iUserCode;
          this.iMillCode          = iMillCode;
          this.SSupTable          = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }

     public void createComponents()
     {
          BOk         = new JButton("Save");
          BCancel     = new JButton("Abort");
          BPending    = new JButton("Pending RDC Materials");
          BSupplier   = new JButton("Supplier");

          TGateDate   = new DateField();
          TInvDate    = new DateField();
          TDCDate     = new DateField();
          TGateNo     = new NextField();
          TInvNo      = new JTextField();
          TDCNo       = new JTextField();
          TSupCode    = new JTextField();
          MiddlePanel = new GIRdcMiddlePanel(DeskTop);
          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          TGateDate.setTodayDate();

          BPending.setMnemonic('P');
          BSupplier.setMnemonic('U');
          BOk.setMnemonic('S');
          BCancel.setMnemonic('A');
          TGateDate.TDay.setEnabled(false);
          TGateDate.TMonth.setEnabled(false);
          TGateDate.TYear.setEnabled(false);
     }
     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);

          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(5,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          TopPanel       .add(new JLabel("Supplier"));
          TopPanel       .add(BSupplier);

          TopPanel       .add(new JLabel(""));
          TopPanel       .add(new JLabel(""));

          TopPanel       .add(new JLabel("Gate Inward No"));
          TopPanel       .add(TGateNo);

          TopPanel       .add(new JLabel("Gate Inward Date"));
          TopPanel       .add(TGateDate);

          TopPanel       .add(new JLabel("Invoice No"));
          TopPanel       .add(TInvNo);

          TopPanel       .add(new JLabel("Invoice Date"));
          TopPanel       .add(TInvDate);

          TopPanel       .add(new JLabel("DC No"));
          TopPanel       .add(TDCNo);

          TopPanel       .add(new JLabel("DC Date"));
          TopPanel       .add(TDCDate);

          TopPanel       .add(new JLabel("Fetch Expected Materials"));
          TopPanel       .add(BPending);

          TopPanel       .add(new JLabel(""));
          TopPanel       .add(new JLabel(""));

          BottomPanel    .add(BOk);
          BottomPanel    .add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BOk            .addActionListener(new ActList());
          BCancel        .addActionListener(new ActList());
          BPending       .addActionListener(new PendingRdcList(DeskTop,MiddlePanel,TSupCode,iMillCode));
          BSupplier      .addActionListener(new SupplierSearch(DeskTop,TSupCode,SSupTable));
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    BOk.setEnabled(false);
                    boolean bSig = false;
                    try
                    {
                         bSig = allOk();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
                    if(bSig)
                    {
                         insertGIRdcDetails();
                         removeHelpFrame();
                    }
                    else
                    {
                         BOk.setEnabled(true);
                    }
               }     
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
               }     
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public void insertGIRdcDetails()
     {
          
          int iRDCNo = 0;
          int iSlNo  = 0;

          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement theStatement   = theconnect.createStatement();

               for(int i=0;i<FinalData.length;i++)
               {
                    iRDCNo = common.toInt((String)MiddlePanel.RDCData[i]);
                    iSlNo  = common.toInt((String)MiddlePanel.SlData[i]);

                    String QS = "Insert Into GateInwardRDC (ID,GINo,GIDate,Sup_Code,InvNo,InvDate,DcNo,DcDate,Descript,SupQty,GateQty,UnmeasuredQty,UserCode,RdcNo,RdcSlNo,MillCode) Values(";
                    QS = QS+"GateInwardRDC_Seq.nextval,";
                    QS = QS+"0"+TGateNo.getText()+",";
                    QS = QS+"'"+TGateDate.TYear.getText()+TGateDate.TMonth.getText()+TGateDate.TDay.getText()+"',";
                    QS = QS+"'"+TSupCode.getText()+"',";
                    QS = QS+"'"+TInvNo.getText()+"',";
                    QS = QS+"'"+TInvDate.TYear.getText()+TInvDate.TMonth.getText()+TInvDate.TDay.getText()+"',";
                    QS = QS+"'"+TDCNo.getText()+"',";
                    QS = QS+"'"+TDCDate.TYear.getText()+TDCDate.TMonth.getText()+TDCDate.TDay.getText()+"',";
                    QS = QS+"'"+common.getNarration((String)FinalData[i][0])+"',";
                    QS = QS+"0"+String.valueOf(common.toDouble(((String)FinalData[i][1]).trim()))+",";
                    QS = QS+"0"+String.valueOf(common.toDouble(((String)FinalData[i][2]).trim()))+",";
                    QS = QS+"0"+String.valueOf(common.toDouble(((String)FinalData[i][3]).trim()))+",";
                    QS = QS+"0"+iUserCode+",";
                    QS = QS+"0"+iRDCNo+",";
                    QS = QS+"0"+iSlNo+",";
                    QS = QS+"0"+iMillCode+")";
                    theStatement.execute(QS);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex.getMessage());
          }
     }

     public boolean allOk()
     {

          String SSupCode     = TSupCode.getText();
          String SCurDate     = common.getServerDate();
          String SDate        = TGateDate.toNormal();
          String SGateNo      = common.parseNull(TGateNo.getText().trim());
          String SInvNo       = common.parseNull(TInvNo.getText().trim());
          String SInvDate     = TInvDate.toNormal();
          String SDCNo        = common.parseNull(TDCNo.getText().trim());
          String SDCDate      = TDCDate.toNormal();
          int    iDateDiff    = common.toInt(common.getDateDiff(common.parseDate(SDate),SCurDate));
          int    iInvDateDiff = common.toInt(common.getDateDiff(common.parseDate(SInvDate),SCurDate));
          int    iDCDateDiff  = common.toInt(common.getDateDiff(common.parseDate(SDCDate),SCurDate));

          int iRows=0;

          if(SGateNo.length()==0)
          {
              JOptionPane.showMessageDialog(null,"Gate No is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
              TGateNo.requestFocus();
              return false;
          }

          int iCount = checkGateNo(SGateNo);

          if(iCount>0)
          {
              JOptionPane.showMessageDialog(null,"This Gate No Already Exists ","Error",JOptionPane.ERROR_MESSAGE);
              TGateNo.requestFocus();
              return false;
          }

          if(SInvNo.equals("") && SDCNo.equals(""))
          {
              JOptionPane.showMessageDialog(null,"InvoiceNo or DC No must be Filled ","Error",JOptionPane.ERROR_MESSAGE);
              TGateDate.TDay.requestFocus();
              return false;
          }
          if(SSupCode.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Select Supplier","Error",JOptionPane.ERROR_MESSAGE);
               BSupplier.setEnabled(true);
               BSupplier.requestFocus();
               return false;
          }
          if(!SInvNo.equals("") && SInvDate.equals(""))
          {
              JOptionPane.showMessageDialog(null,"Invoice Date is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
              TInvDate.TDay.requestFocus();
              return false;
          }
          if(SInvNo.equals("") && !SInvDate.equals(""))
          {
              JOptionPane.showMessageDialog(null,"Invoice No is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
              TInvNo.requestFocus();
              return false;
          }
          if(!SDCNo.equals("") && SDCDate.equals(""))
          {
              JOptionPane.showMessageDialog(null,"DC Date is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
              TDCDate.TDay.requestFocus();
              return false;
          }
          if(SDCNo.equals("") && !SDCDate.equals(""))
          {
              JOptionPane.showMessageDialog(null,"DC No is Invalid ","Error",JOptionPane.ERROR_MESSAGE);
              TDCNo.requestFocus();
              return false;
          }

          try
          {
               iRows    = MiddlePanel.MiddlePanel.getRows();
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"No Materials Selected","Error",JOptionPane.ERROR_MESSAGE);
               BPending.setEnabled(true);
               BPending.requestFocus();
               return false;
          }

          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<FinalData.length;i++)
          {
               String S1 = ((String)FinalData[i][1]).trim();
               String S2 = ((String)FinalData[i][2]).trim();
               String S3 = ((String)FinalData[i][3]).trim();

               String SValue1 = common.getRound(S1,3);
               String SValue2 = common.getRound(S2,3);
               String SValue3 = common.getRound(S3,3);

               if((S1.length()==0) || (S2.length()==0) || (S3.length()==0))
               {
                    JOptionPane.showMessageDialog(null,"Quantity Fields Must be Filled","Error",JOptionPane.ERROR_MESSAGE);
                    MiddlePanel.requestFocus();
                    return false;
               }
               try
               {
                    double d1=Double.parseDouble(S1);
                    double d2=Double.parseDouble(S2);
                    double d3=Double.parseDouble(S3);
               }
               catch(Exception ex)
               {
                    JOptionPane.showMessageDialog(null,"Invalid Number","Error",JOptionPane.ERROR_MESSAGE);
                    MiddlePanel.requestFocus();
                    return false;
               }
               if((SValue1.length()>10) || (SValue2.length()>10) || (SValue3.length()>10))
               {
                    JOptionPane.showMessageDialog(null,"Invalid Quantity","Error",JOptionPane.ERROR_MESSAGE);
                    MiddlePanel.requestFocus();
                    return false;
               }
          }
          return true;
     }
     public int checkGateNo(String SGateNo)
     {
          int iCount=0;

          try
          {
               String QS = " Select count(*) from GateInwardRDC where GINo="+SGateNo+" and MillCode="+iMillCode;

               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();
               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    iCount = result.getInt(1);
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
          return iCount;
     }
}
