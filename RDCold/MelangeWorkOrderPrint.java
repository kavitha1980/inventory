
package RDC;

import java.io.*;
import java.util.*;
import java.sql.*;
import util.*;
import jdbc.*;
import guiutil.*;

public class MelangeWorkOrderPrint
{
     Vector VName,VQty,VRate;
     Vector VDiscPer,VCenVatPer,VTaxPer,VSurPer;
     Vector VBasic,VDisc,VCenVat,VTax,VSur,VNet,VOthers;
     Vector VDesc;

     String SPayTerm = "";
     String SDueDate = "";
     String SOthers  = "";
     String SReference = "";
     String SRemarks   = "";
     double dOthers  = 0;
     String SAddr1="",SAddr2="",SAddr3="";
     String SEMail="",SFaxNo="";
     double dTDisc=0,dTCenVat=0,dTTax=0,dTSur=0,dTNet=0;
     double dSumOthers = 0;

     FileWriter FW;
     String SOrdNo,SOrdDate,SSupCode,SSupName;
     int iMillCode;
     String SSupTable;

     String SToName="",SThroName="";
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;

     int Lctr      = 100;
     int Pctr      = 0;

     MelangeWorkOrderPrint(FileWriter FW,String SOrdNo,String SOrdDate,String SSupCode,String SSupName,int iMillCode,String SSupTable)
     {
          this.FW         = FW;
          this.SOrdNo     = SOrdNo;
          this.SOrdDate   = SOrdDate;
          this.SSupCode   = SSupCode;
          this.SSupName   = SSupName;
          this.iMillCode  = iMillCode;
          this.SSupTable  = SSupTable;

          try
          {
                getAddr();
                setDataIntoVector();
                setOrderHead();
                setOrderBody();
                setOrderFoot(0);
          }
          catch(Exception ex)
          {
                System.out.println(ex);
                ex.printStackTrace();
          }
     }
   
     public void setOrderHead()
     {
          String Strl13 = "";
          String Strl14 = "";
          String Strl15 = "";

          if(Lctr < 42)
            return;

          if(Pctr > 0)
            setOrderFoot(1);

          Pctr++;

          String Strl01 = "|-------------------------------------------------------------------------------------------------------------------------|";
          String Strl02 = "|AMARJOTHI COLOUR MELANGE SPINNING MILLS PVT LIMITED| C.Ex. RC No :              ECC No          :                        |";
          String Strl03 = "|      PUDUSURIPALAYAM, NAMBIYUR - 638 458          | PLA No      :              Range           :                        |";
          String Strl04 = "|         GOBI(TK),ERODE(DT),TAMILNADU.             | Division    :              Commissionerate :                        |";
          String Strl05 = "|-------------------------------------------------------------------------------------------------------------------------|";
          String Strl06 = "| E-Mail : amarjoth@eth.net       TIN No.33712404816            |                            WORK ORDER                   |";
          String Strl07 = "| Phones : 04285 - 267201,267301, Fax - 04285-267565            |---------------------------------------------------------|";
          String Strl08 = "| Our C.S.T. No.1031365           dt.05.05.2010                 |    NO     | "+common.Pad("WO"+"-"+SOrdNo,12)+"| Date | "+common.Pad(SOrdDate,10)+" | Page | "+common.Pad(""+Pctr,2)+" |";
       // String Strl09 = "| T.N.G.S.T No. R.C. No.          dt.                           |---------------------------------------------------------|";
          String Strl09 = "|                                                               |---------------------------------------------------------|";
          String Strl10 = "|---------------------------------------------------------------| Book To   |"+common.Pad(SToName,45)+                   "|";
          String Strl11 = "|To,                                                            |---------------------------------------------------------|"; 
          String Strl12 = "|"+common.Pad("M/s "+SSupName,63)+                             "| Book Thro'|"+common.Pad(SThroName,45)+                 "|";



          if(SAddr1.equals("na"))
          {
               Strl13 = "|"+common.Pad("",63)+                                      "|---------------------------------------------------------|";
          }
          else
          {
               Strl13 = "|"+common.Pad(SAddr1,63)+                                      "|---------------------------------------------------------|";
          }
          if(SAddr2.equals("na"))
          {
               Strl14 = "|"+common.Pad("",63)+                                      "| Reference |"+common.Pad(SReference,45)+"|";
          }
          else
          {
               Strl14 = "|"+common.Pad(SAddr2,63)+                                      "| Reference |"+common.Pad(SReference,45)+"|";
          }
          if(SAddr3.equals("na"))
          {
               Strl15 = "|"+common.Pad("",63)+                                      "|---------------------------------------------------------|";
          }
          else
          {
               Strl15 = "|"+common.Pad(SAddr3,63)+                                      "|---------------------------------------------------------|";
          }

          String Strl15a= "|"+common.Pad("Fax No: "+SFaxNo,25)+common.Pad("EMail: "+SEMail,38)+"| Remarks   |"+common.Pad(SRemarks,45)+"|";

          String Strl16 = "|-------------------------------------------------------------------------------------------------------------------------|";
          String Strl17 = "| Description of Materials Serviced  | UOM | Quantity |     Rate | Discount |   CenVat |      Tax | Surcharg |        Net |";
          String Strl17a= "| and Nature of Work Performed       |     |          |       Rs |       Rs |       Rs |       Rs |       Rs |         Rs |";
          String Strl18 = "|-------------------------------------------------------------------------------------------------------------------------|";
                                       
          try
          {
               FW.write( "g"+Strl01+"\n");       
               FW.write( Strl02+"\n");
               FW.write( Strl03+"\n");
               FW.write( Strl04+"\n");
               FW.write( Strl05+"\n");       
               FW.write( Strl06+"\n");       
               FW.write( Strl07+"\n");       
               FW.write( Strl08+"\n");       
               FW.write( Strl09+"\n");       
               FW.write( Strl10+"\n");       
               FW.write( Strl11+"\n");       
               FW.write( Strl12+"\n");       
               FW.write( Strl13+"\n");       
               FW.write( Strl14+"\n");       
               FW.write( Strl15+"\n");
               FW.write( Strl15a+"\n");       
               FW.write( Strl16+"\n");       
               FW.write( Strl17+"\n");
               FW.write( Strl17a+"\n");       
               FW.write( Strl18+"\n");
               Lctr = 20;
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public void setOrderBody()
     {
          String Strl="";

          for(int i=0;i<VName.size();i++)
          {
               setOrderHead();
               String SName     = (String)VName.elementAt(i);
               String SQty      = common.getRound((String)VQty.elementAt(i),2);
               String SRate     = common.getRound((String)VRate.elementAt(i),2);
               String SDisc     = common.getRound((String)VDisc.elementAt(i),2);
               String SDiscPer  = common.getRound((String)VDiscPer.elementAt(i),2)+"%";
               String SCenVat   = common.getRound((String)VCenVat.elementAt(i),2);
               String SCenVatPer= common.getRound((String)VCenVatPer.elementAt(i),2)+"%";
               String STax      = common.getRound((String)VTax.elementAt(i),2);
               String STaxPer   = common.getRound((String)VTaxPer.elementAt(i),2)+"%";
               String SSur      = common.getRound((String)VSur.elementAt(i),2);
               String SSurPer   = common.getRound((String)VSurPer.elementAt(i),2)+"%";
               String SNet      = common.getRound((String)VNet.elementAt(i),2);
               dSumOthers          = dSumOthers + common.toDouble((String)VOthers.elementAt(i));

               if(common.toDouble(SQty)==0)
                  continue;

               SName    = SName.trim();
               dTDisc   = dTDisc+common.toDouble(SDisc);
               dTCenVat = dTCenVat+common.toDouble(SCenVat);
               dTTax    = dTTax   +common.toDouble(STax);
               dTSur    = dTSur   +common.toDouble(SSur);
               dTNet    = dTNet   +common.toDouble(SNet);
//               dOthers  = common.toDouble(SOthers);

               Vector vect = common.getLines(SName+"`");

               try
               {
                    for(int j=0;j<vect.size();j++)
                    {
                          if(j==0)
                          {
                               Strl =    "| "+common.Pad((String)vect.elementAt(j),34)+" | "+
                                            common.Pad("  ",3)+" | "+common.Rad(SQty,8)+" | "+
                                            common.Rad(SRate,8)+" | "+common.Rad(SDisc,8)+" | "+
                                            common.Rad(SCenVat,8)+" | "+common.Rad(STax,8)+" | "+
                                            common.Rad(SSur,8)+" | "+common.Rad(SNet,10)+" |";
                               FW.write(Strl+"\n");
                               Lctr++;
                               continue;                                   
                          }
                          if(j==1)
                          {
                               Strl =      "| "+common.Pad((String)vect.elementAt(j),34)+" | "+
                                            common.Space(3)+" | "+common.Space(8)+" | "+
                                            common.Space(8)+" | "+common.Rad(SDiscPer,8)+" | "+
                                            common.Rad(SCenVatPer,8)+" | "+common.Rad(STaxPer,8)+" | "+
                                            common.Rad(SSurPer,8)+" | "+common.Space(10)+" |";
                               FW.write(Strl+"\n");
                               Lctr++;
                               continue;                                   
                          }                             
                          else
                          {
                               Strl =     "| "+common.Pad((String)vect.elementAt(j),34)+" | "+
                                            common.Space(3)+" | "+common.Space(8)+" | "+
                                            common.Space(8)+" | "+common.Space(8)+" | "+
                                            common.Space(8)+" | "+common.Space(8)+" | "+
                                            common.Space(8)+" | "+common.Space(10)+" |";
                               FW.write(Strl+"\n");
                               Lctr=Lctr+1;
                          }
                    }

                    if(vect.size()==1)
                    {
                         Strl =      "| "+common.Pad("",34)+" | "+
                                      common.Space(3)+" | "+common.Space(8)+" | "+
                                      common.Space(8)+" | "+common.Rad(SDiscPer,8)+" | "+
                                      common.Rad(SCenVatPer,8)+" | "+common.Rad(STaxPer,8)+" | "+
                                      common.Rad(SSurPer,8)+" | "+common.Space(10)+" |";
                         FW.write(Strl+"\n");
                         Lctr = Lctr+1;
                    }                             

                    Strl =      "| "+common.Pad("",34)+" | "+
                                common.Space(3)+" | "+common.Space(8)+" | "+
                                common.Space(8)+" | "+common.Rad("",8)+" | "+
                                common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                common.Rad("",8)+" | "+common.Space(10)+" |";
                    FW.write(Strl+"\n");
                    Lctr = Lctr+1;
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
          }
     }

     public void setOrderFoot(int iSig)
     {
          String SDisc   = common.getRound(dTDisc,2);
          String SCenVat = common.getRound(dTCenVat,2);
          String STax    = common.getRound(dTTax,2);
          String SSur    = common.getRound(dTSur,2);
          String SNet    = common.getRound(dTNet,2);
          String SOthers = common.getRound(dSumOthers,2);
          String SGrand  = common.getRound(dTNet+dSumOthers,2);
          String Strl1 = "|-------------------------------------------------------------------------------------------------------------------------|";

          String Strl2 = "| "+common.Pad("T O T A L ",34)+" | "+
                              common.Space(3)+" | "+common.Space(8)+" | "+
                              common.Space(8)+" | "+common.Rad(SDisc,8)+" | "+
                              common.Rad(SCenVat,8)+" | "+common.Rad(STax,8)+" | "+
                              common.Rad(SSur,8)+" | "+common.Rad(SNet,10)+" |";
          String Strl3 = "|-------------------------------------------------------------------------------------------------------------------------|";
          String Strl3a= "|-------------------------------------------------------------------------------------------------------------------------|";
          String Strl3b= "| Expected Date of Delivery  |         Terms of Payments        | Rounding Off/Others  |           Order Value            |";
          String Strl3c= "|-------------------------------------------------------------------------------------------------------------------------|";

          String Strl3d= "| "+common.Pad(SDueDate,27)+"| "+common.Pad(SPayTerm,33)+"| "+
                              common.Rad(SOthers,19)+"  | "+
                              ""+common.Pad(SGrand,16)+"|";
          String Strl3e= "|-------------------------------------------------------------------------------------------------------------------------|";
          String Strl4 = "| Note : 1. EPlease mention our Work Order No in all future correspondence with us.F                                        |";
          String Strl4a= "|        2. EPlease mention our TIN Number in your invoices.F                                                               |";
          String Strl5 = "|        3. Please send  invoices in triplicate. Original invoice to be sent direct to accounts manager.                  |";
          String Strl6 = "|        4. Please send copy of your invoice along with consignment.                                                      |";
          String Strl7 = "|        5. We reserve the right to cancel the order without assigning any reason.                                        |";
          String Strl8 = "|        6. Any legal dispute shall be within the jurisdiction of Tirupur.                                                |";
          String Strl9 = "|                                                                    For AMARJOTHI COLOUR MELANGE SPINNING MILLS PVT LTD  |";
          String Strl10= "|                                                                                                                         |";
          String Strl11= "|                                                                                                                         |";
          String Strl12= "| Prepared By                                Checked By                                         Authorised Signatory      |";
          String Strl13= "|-------------------------------------------------------------------------------------------------------------------------|";
          String Strl14= "| Special Instruction :-                                                                                                  |";
          String Strl15= "|-------------------------------------------------------------------------------------------------------------------------|";
          String Strl16= "| Regd. Office - 'AMARJOTHI HOUSE' 157, Kumaran Road, Tirupur - 638 601. Phone : 0421- 2201980 to 2201984                 |";
          String Strl16a= "| E-Mail       - sales@amarjothi.com, ajsmmill@yahoo.com                                                                  |";
          String Strl17= "|-------------------------------------------------------------------------------------------------------------------------|";
          String Strl18= "|                           I N D I A 'S   F I N E S T  M E L A N G E  Y A R N  P R O D U C E R S                         |";
          String Strl19= "--------------------------------------------------------------------------------------------------------------------------|";
          String Strl20= "";
          if(iSig==0)
          {
             String SDateTime = common.getServerDateTime2();
             Strl20 = "Report Taken on "+SDateTime+"";
          }
          else
             Strl20= "(Continued on Next Page) ";
          try
          {
               FW.write( Strl1+"\n");       
               FW.write( Strl2+"\n");       
               FW.write( Strl3+"\n");
               if(iSig==0)
               {
                    FW.write( Strl3a+"\n");
                    FW.write( Strl3b+"\n");
                    FW.write( Strl3c+"\n");
                    FW.write( Strl3d+"\n");
                    FW.write( Strl3e+"\n");
               } 
               FW.write( Strl4+"\n");
               FW.write( Strl4a+"\n");       
               FW.write( Strl5+"\n");       
               FW.write( Strl6+"\n");       
               FW.write( Strl7+"\n");       
               FW.write( Strl8+"\n");       
               FW.write( Strl9+"\n");       
               FW.write( Strl10+"\n");       
               FW.write( Strl11+"\n");       
               FW.write( Strl12+"\n");       
               FW.write( Strl13+"\n");       
               FW.write( Strl14+"\n");       
               FW.write( Strl15+"\n");       
               FW.write( Strl16+"\n");
               FW.write( Strl16a+"\n");       
               FW.write( Strl17+"\n");       
               FW.write( Strl18+"\n");       
               FW.write( Strl19+"\n");       
               FW.write( Strl20+"\n");       
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
     public void setDataIntoVector()
     {
          VName      = new Vector();
          VDesc      = new Vector();
          VQty       = new Vector();
          VRate      = new Vector();
          VDiscPer   = new Vector();
          VCenVatPer = new Vector();
          VTaxPer    = new Vector();
          VSurPer    = new Vector();
          VBasic     = new Vector();
          VDisc      = new Vector();
          VCenVat    = new Vector();
          VTax       = new Vector();
          VSur       = new Vector();
          VNet       = new Vector();
          VOthers    = new Vector();

           try
           {
              if(theconnect==null)
              {
                   connect=ORAConnection.getORAConnection();
                   theconnect=connect.getConnection();
              }
              Statement stat  = theconnect.createStatement();
              String QString = getQString();
              ResultSet res  = stat.executeQuery(QString);
              while (res.next())
              {
                 VName.addElement(res.getString(1));
                 VQty.addElement(res.getString(2));
                 VRate.addElement(res.getString(3));
                 VDiscPer.addElement(res.getString(4));
                 VDisc.addElement(res.getString(5));
                 VCenVatPer.addElement(res.getString(6));
                 VCenVat.addElement(res.getString(7));
                 VTaxPer.addElement(res.getString(8));
                 VTax.addElement(res.getString(9));
                 VSurPer.addElement(res.getString(10));
                 VSur.addElement(res.getString(11));
                 VNet.addElement(res.getString(12));

                 VOthers.addElement(res.getString(13));

                 SDueDate = common.parseDate(res.getString(14));
                 SToName  = common.parseNull(res.getString(15));
                 SThroName= common.parseNull(res.getString(16));
                 SPayTerm = common.parseNull(res.getString(17));
                 VDesc.addElement(" ");
                 SReference = common.parseNull(res.getString(18));
                 SRemarks   = common.parseNull(res.getString(19));
              }
              res.close();
              stat.close();
           }
           catch(Exception ex)
           {
               System.out.println(ex);
               ex.printStackTrace();
           }
     }
     public String getQString()
     {
          String QString  = "";
     
          QString = " SELECT WorkOrder.Descript,"+
                    " WorkOrder.Qty, WorkOrder.Rate, "+
                    " WorkOrder.DiscPer, WorkOrder.Disc, "+
                    " WorkOrder.CenVatPer, WorkOrder.CenVat, "+
                    " WorkOrder.TaxPer, WorkOrder.Tax, "+
                    " WorkOrder.SurPer,WorkOrder.Sur,"+
                    " WorkOrder.Net,WorkOrder.Misc,WorkOrder.DueDate, "+
                    " BookTo.ToName,BookThro.ThroName,WorkOrder.PayTerms, "+
                    " WorkOrder.Reference,WorkOrder.Remarks "+
                    " FROM (WorkOrder "+
                    " Inner Join BookTo   On BookTo.ToCode = WorkOrder.ToCode) "+
                    " Inner Join BookThro On BookThro.ThroCode = WorkOrder.ThroCode "+
                    " Where WorkOrder.OrderNo = "+SOrdNo+" and WorkOrder.MillCode="+iMillCode;
          return QString;
     }
     public void getAddr()
     {
           String QS = "Select nvl("+SSupTable+".Addr1,'na'),nvl("+SSupTable+".Addr2,'na'),nvl("+SSupTable+".Addr3,'na'),"+SSupTable+".EMail,"+SSupTable+".Fax,Place.PlaceName "+
                       "From "+SSupTable+" LEFT Join Place On Place.PlaceCode = "+SSupTable+".City_Code "+
                       "Where "+SSupTable+".Ac_Code = '"+SSupCode+"'";

           try
           {
                 if(theconnect==null)
                 {
                      connect=ORAConnection.getORAConnection();
                      theconnect=connect.getConnection();
                 }
                 Statement stat  = theconnect.createStatement();
                 ResultSet res   = stat.executeQuery(QS);
                 while (res.next())
                 {
                        SAddr1 = res.getString(1);
                        SAddr2 = res.getString(2);
                        SAddr3 = common.parseNull(res.getString(3));
                        SEMail = common.parseNull(res.getString(4));
                        SFaxNo = common.parseNull(res.getString(5));
                 }
                 res.close();
                 stat.close();
           }
           catch(Exception ex)
           {
                System.out.println(ex);
                ex.printStackTrace();
           }
     }
}
