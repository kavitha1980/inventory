package RDC;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class OrderMiddlePanel extends JPanel 
{
     DOInvMiddlePanel MiddlePanel;

     Object RowData[][];
     Object IdData[];
     Object SlData[];

     //                          0           1       2     3         4            5           6          7           8          9              10         11          12            13           14         15      16        17      18         19
     String ColumnData[] = {"Description","RDC No","Qty","Rate","Discount(%)","CenVat(%)","Tax(%)","Surcharge(%)","Basic","Discount (Rs)","CenVat(Rs)","Tax(Rs)","Surcharge(Rs)","Net (Rs)","Department","Group","Due Date","Unit","Remarks","LogBook No"};
     String ColumnType[] = {"E"          ,"E"     ,"B"  ,"B"   ,"B"          ,"B"        ,"B"     ,"B"           ,"N"   ,"N"             ,"N"         ,"N"      ,"N"            ,"N"       ,"B"         ,"B"    ,"B"       ,"B"      ,"S"   ,"S"         };
                             
     Common common = new Common();
     JLayeredPane DeskTop;

     Vector VName,VRdcNo,VSlNo;
     String SEmpty;
     int iMillCode;

     public OrderMiddlePanel(JLayeredPane DeskTop,Vector VName,Vector VRdcNo,Vector VSlNo,int iMillCode)
     {
          this.DeskTop   = DeskTop;
          this.VName     = VName;
          this.VRdcNo    = VRdcNo;
          this.VSlNo     = VSlNo;
          this.iMillCode = iMillCode;

          setLayout(new BorderLayout());
          setRowData(VName,VRdcNo,VSlNo);
          createComponents();
     }
                                   
     public OrderMiddlePanel(JLayeredPane DeskTop,Vector VName,Vector VRdcNo,String SEmpty,Vector VSlNo,int iMillCode)
     {
          this.DeskTop   = DeskTop;
          this.VName     = VName;
          this.VRdcNo    = VRdcNo;
          this.SEmpty    = SEmpty;
          this.VSlNo     = VSlNo;
          this.iMillCode = iMillCode;

          setLayout(new BorderLayout());
          setRowData();
          createComponents();
     }
     public OrderMiddlePanel(JLayeredPane DeskTop,int iMillCode)
     {
          this.DeskTop   = DeskTop;
          this.iMillCode = iMillCode;

          setLayout(new BorderLayout());
     }

     public void createComponents()
     {
          MiddlePanel = new DOInvMiddlePanel(DeskTop,RowData,ColumnData,ColumnType,iMillCode);
          add(MiddlePanel,BorderLayout.CENTER);
          MiddlePanel.ReportTable.requestFocus();
     }

     public void setRowData(Vector VName,Vector VRdcNo,Vector VSlNo)
     {
          RowData     = new Object[VName.size()][20];
          SlData      = new Object[VSlNo.size()];

          for(int i=0;i<VName.size();i++)
          {
               SlData[i]      = common.parseNull((String)VSlNo.elementAt(i));
               RowData[i][0]  = common.parseNull((String)VName   .elementAt(i));
               RowData[i][1]  = common.parseNull((String)VRdcNo  .elementAt(i));
               RowData[i][2]  = " ";
               RowData[i][3]  = " ";
               RowData[i][4]  = " ";
               RowData[i][5]  = " ";
               RowData[i][6]  = " ";
               RowData[i][7]  = " ";
               RowData[i][8]  = " ";
               RowData[i][9]  = " ";
               RowData[i][10] = " ";
               RowData[i][11] = " ";
               RowData[i][12] = " ";
               RowData[i][13] = " ";
               RowData[i][14] = " ";
               RowData[i][15] = " ";
               RowData[i][16] = " ";
               RowData[i][17] = " ";
               RowData[i][18] = " ";
               RowData[i][19] = " ";
          }
     }

     public void setRowData()
     {
          RowData     = new Object[1][20];
          SlData      = new Object[1];
          for(int i=0;i<1;i++)
          {
               SlData[i]      = "0";
               RowData[i][0]  = " ";
               RowData[i][1]  = " ";
               RowData[i][2]  = " ";
               RowData[i][3]  = " ";
               RowData[i][4]  = " ";
               RowData[i][5]  = " ";
               RowData[i][6]  = " ";
               RowData[i][7]  = " ";
               RowData[i][8]  = " ";
               RowData[i][9]  = " ";
               RowData[i][10] = " ";
               RowData[i][11] = " ";
               RowData[i][12] = " ";
               RowData[i][13] = " ";
               RowData[i][14] = " ";
               RowData[i][15] = " ";
               RowData[i][16] = " ";
               RowData[i][17] = " ";
               RowData[i][18] = " ";
               RowData[i][19] = " ";
          }
     }


}



