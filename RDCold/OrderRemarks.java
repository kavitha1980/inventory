package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class OrderRemarks extends JInternalFrame
{
     JTextField     TItemCode;
     AddressField   TRemarks;

     JButton        BOk,BCancel;
     JPanel         TopPanel,BottomPanel;
     
     JLayeredPane   Layer;
     JTable         ReportTable;
     
     OrderRemarks(JLayeredPane Layer,JTable ReportTable)
     {
          this.Layer          = Layer;
          this.ReportTable    = ReportTable;

          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
     }
     public void createComponents()
     {
          TItemCode               = new JTextField();
          TRemarks                = new AddressField(100);
          
          BOk                 = new JButton("Okay");
          BCancel             = new JButton("Cancel");
          
          TopPanel            = new JPanel(true);
          BottomPanel         = new JPanel(true);
     }
     public void setLayouts()
     {
          setBounds(0,0,350,250);
          setResizable(true);
          setClosable(true);
          setTitle("Remarks Frame");

          TopPanel.setLayout(new GridLayout(4,2));
          BottomPanel.setLayout(new FlowLayout());
     }
     public void addComponents()
     {
          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));

          TopPanel.add(new MyLabel("Description"));
          TopPanel.add(TItemCode);

          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));

          TopPanel.add(new MyLabel("Remarks"));
          TopPanel.add(TRemarks);

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add("North",TopPanel);
          getContentPane().add("South",BottomPanel);
     }
     public void addListeners()
     {
          BOk       .addActionListener(new ActList());
          BCancel   .addActionListener(new ActList());
     }
     public void setPresets()
     {
          int i = ReportTable.getSelectedRow();
          String SName      = (String)ReportTable.getModel().getValueAt(i,0);
          String SRemarks   = (String)ReportTable.getModel().getValueAt(i,18);
          
          TItemCode            . setText(SName);
          TRemarks             . setText(SRemarks);
          TItemCode            . setEditable(false);
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    int       i = ReportTable.getSelectedRow();
                    ReportTable.getModel().setValueAt(TRemarks.getText(),i,18);
               }
               removeHelpFrame();
               ReportTable.requestFocus();
          }
     }
     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }
}
