package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

class OthersOutPassFrame extends JInternalFrame
{
     Connection     theConnection  = null;
     Common         common         = new Common();

     JLayeredPane   Layer;
     JPanel         TopPanel,BottomPanel,MiddlePanel;

     JButton        BOky;
     JComboBox      JCBookNo;

     String         SOutPassNo= "";

     int            iBookNo   = 0;
     int            iMillCode,iUserCode;
                    
     public OthersOutPassFrame(JLayeredPane Layer,int iMillCode,int iUserCode)
     {
          super("Others OutPass Entry Screen");
          this . Layer        = Layer;
          this . iMillCode    = iMillCode;
          this . iUserCode    = iUserCode;

          createComponents();
          setLayouts();
          addComponents();
          addListener();
     }

     public void createComponents()
     {
          JCBookNo       = new JComboBox();
          JCBookNo       . addItem("Book1");
          JCBookNo       . addItem("Book2");
          JCBookNo       . addItem("Book3");
          JCBookNo       . setEnabled(false);

          BOky           = new JButton("Save");

          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          MiddlePanel    = new JPanel();
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,350,400);
          setTitle("Others OutPass Entry Screen");

          TopPanel       . setLayout(new GridLayout(1,2));
          BottomPanel    . setLayout(new BorderLayout());
     }

     public void addComponents()
     {
          TopPanel            . add(new Label("Out Pass Book"));
          TopPanel            . add(JCBookNo);

          BottomPanel         . add(BOky);

          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(MiddlePanel,BorderLayout.CENTER);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListener()
     {
          BOky           . addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               SOutPassNo   = getNextOrderNo();

               insertinOutPass();
               UpdateOrdNo();

               String    SNum           = "The OutPass Number is "+SOutPassNo;
                         JOptionPane    . showMessageDialog(null,SNum,"Information",JOptionPane.INFORMATION_MESSAGE);
          }
     }
     private void insertinOutPass()
     {
          try
          {
               String    QS = " insert into OutPass (ID,OUTPASSNO,OUTPASSDATE,MATERIALTYPE,BOOKTYPE,MILLCODE,USERCODE,CREATIONDATE) values (";
                         QS   = QS+" OUTPASS_SEQ.nextval ,";
                         QS   = QS+SOutPassNo+",";
                         QS   = QS+" '"+common.getServerPureDate()+"',";
                         QS   = QS+"1"+",";
                         QS   = QS+iBookNo+",";
                         QS   = QS+iMillCode+",";
                         QS   = QS+iUserCode+",";
                         QS   = QS+" '"+common.getServerDate()+"')";

     
//               System.out.println(QS);

               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();
                              stat           . execute(QS);
                              stat           . close();
          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public String getNextOrderNo()
     {
          String    QS           = "";

          int       iOutPassNo   = 0;

                    iBookNo   = 0;
                    iBookNo   = JCBookNo.getSelectedIndex();
                    iBookNo   = iBookNo+1;      

          if(iMillCode==0)
          {
               if(iBookNo==1)
                    QS = "  Select maxno from config where id = 26";

               if(iBookNo==2)
                    QS = "  Select maxno from config where id = 27";

               if(iBookNo==3)
                    QS = "  Select maxno from config where id = 28";
          }
          else
          {
               if(iBookNo==1)
                    QS = "  Select maxno from config where id = 29";

               if(iBookNo==2)
                    QS = "  Select maxno from config where id = 30";

               if(iBookNo==3)
                    QS = "  Select maxno from config where id = 31";
          }

//          System.out.println(QS);

          try
          {
               ORAConnection  oraConnection  = ORAConnection     . getORAConnection();
               Connection     theConnection  = oraConnection     . getConnection();   
               Statement      stat           = theConnection     . createStatement();
               ResultSet      result         = stat              . executeQuery(QS);

                         result         . next();
                         iOutPassNo     = result.getInt(1);

                         result         . close();
                         stat           . close();

                         iOutPassNo     = iOutPassNo+1;

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

          return String.valueOf(iOutPassNo);
     }

     public void UpdateOrdNo()
     {
          String    QS1       = "";

                    iBookNo   = 0;
                    iBookNo   = JCBookNo.getSelectedIndex();
                    iBookNo   = iBookNo+1;      

          if(iMillCode==0)
          {
               if(iBookNo==1)
                    QS1 = " Update config set MaxNo = MaxNo+1  where id = 26";

               if(iBookNo==2)
                    QS1 = " Update config set MaxNo = MaxNo+1  where id = 27";

               if(iBookNo==3)
                    QS1 = " Update config set MaxNo = MaxNo+1  where id = 28";
          }
          else
          {
               if(iBookNo==1)
                    QS1 = " Update config set MaxNo = MaxNo+1  where id = 29";

               if(iBookNo==2)
                    QS1 = " Update config set MaxNo = MaxNo+1  where id = 30";

               if(iBookNo==3)
                    QS1 = " Update config set MaxNo = MaxNo+1  where id = 31";
          }

//          System.out.println(QS1);

          try
          {
               ORAConnection  oraConnection  = ORAConnection     . getORAConnection();
               Connection     theConnection  = oraConnection     . getConnection();   
               Statement      stat           = theConnection     . createStatement();
                              stat           . executeUpdate(QS1);
                              stat           . close();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

}
