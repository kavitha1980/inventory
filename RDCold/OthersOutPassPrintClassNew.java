package RDC;

import java.util.*;
import java.lang.*;
import java.io.*;
import util.*;
import jdbc.*;
import java.sql.*;

class OthersOutPassPrintClassNew
{

     Common              common;
     Connection          theconnect;
     ORAConnection       connect;

     Vector              VDesc,VKgs,VPurpose;
     String              SSupplierName,SAdd1,SAdd2,SAdd3;
     String              SOutTime,SDCNo,SDCDate,SInvNo,SInvDate,SFormJJNo,SFormJJDate;

     FileWriter FW;
     String SOutPassNo,SOutPassDate,SMatType,SBookType;
     int iMillCode;
     String SMillName;

     int iCount=0;
     int iVectSize=0;
     int iItemCount=0;

     public OthersOutPassPrintClassNew(FileWriter FW,String SOutPassNo,String SOutPassDate,String SMatType,String SBookType,int iMillCode,String SMillName)
     {
          this.FW           = FW;
          this.SOutPassNo   = SOutPassNo;
          this.SOutPassDate = SOutPassDate;
          this.SMatType     = SMatType;
          this.SBookType    = SBookType;
          this.iMillCode    = iMillCode;
          this.SMillName    = SMillName;

          common = new Common();
     }

     public void printWithData()
     {
          VDesc          = new Vector();
          VKgs           = new Vector();
          VPurpose       = new Vector();

          SSupplierName  = "";
          SAdd1          = "";
          SAdd2          = "";
          SAdd3          = "";
          SOutTime       = "";
          SDCNo          = "";
          SDCDate        = "";
          SInvNo         = "";
          SInvDate       = "";
          SFormJJNo      = "";
          SFormJJDate    = "";
          
          setDataintoDataTypes();

          iVectSize = VDesc.size()-1;

          setHead();
          setBody();
          setFoot(true);
     }

     public void setHead()
     {
          try
          {

               FW   . write("M");

               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");

               if(iMillCode == 0)
               {
                    FW   . write("|                                                      E "+SMillName+"F@M                                                                                                                                                                            |\n");
                    FW   . write("|                                                              EPudusuripalayam, Nambiyur.F@M                                                                                                                                                                            |\n");
               }
               else
               if(iMillCode == 1)
               {
                    FW   . write("|                                             E"+SMillName+"F@M                                                                                                                                                                            |\n");
                    FW   . write("|                                                          EPERUNDURAI     -   638 052F@M                                                                                                                                                                            |\n");
               }
               else
               {
                    FW   . write("|                                                      E "+SMillName+"F@M                                                                                                                                                                            |\n");
                    FW   . write("|                                                              EPudusuripalayam, Nambiyur.F@M                                                                                                                                                                            |\n");
               }

               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|                                                             ERETURNABLE / NON RETURNABLEF@M                                                                                                                                                                            |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|                                                                           EOUT PASSF@M                                                                                                                                                                            |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|    S.No : E"+common.Pad(SOutPassNo,12)+"F@M"+common.Space(24)+"                                                                                                          Date : "+common.Pad(common.parseDate(SOutPassDate),10)+" | Time Out : "+common.Pad(SOutTime,10)+" |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|    EName of the ConsigneeF               |"+common.Pad(" "+SSupplierName,65)+"|\n");
               FW   . write("|                                        |"+common.Pad(" "+SAdd1,65)+"|\n");
               FW   . write("|                                        |"+common.Pad(" "+SAdd2,65)+"|\n");
               FW   . write("|                                        |"+common.Pad(" "+SAdd3,65)+"|\n");
               FW   . write("|                                        |                                                                                                                                  |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|    Departement                         |    Stores / Yarn / Waste Cotton / Cotton / VST / PSF / Others                                                                    |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|ES.NoF|                           EDescriptionF                           |  EQty(No/Kg)F  |                                       EPurposeF                                       |\n");

          }
          catch(Exception ex)
          {
               System    . out.println(ex);
               ex        . printStackTrace();
          }

     }
     public void setBody()
     {
          try
          {
               String    SData          = "";

               for(int i=0;i<VDesc.size();i++)
               {
                    iCount++;

                    iItemCount++;

                    if(iItemCount>8)
                    {
                         setFoot(false);
                         setHead();
                         iItemCount=0;
                    }

                    if(i<=iVectSize)
                    {
                         SData     = "|"+common.Rad(String.valueOf(iCount)+" ",4)+"|"+common.Pad(" "+(String)VDesc. elementAt(i)+"",65)+"|"+common.Rad(common.getRound((String)VKgs.elementAt(i),3)+" ",14)+"| "+common.Pad((String)VPurpose.elementAt(i),84);
                    }
                    else
                    {
                         SData     = "|"+common.Rad(String.valueOf(iCount)+" ",4)+"|"+common.Space(65)+"|"+common.Space(14)+"|"+common.Space(85); 
                    }

                    if(iCount!=0)
                    {
                         FW        . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
                         FW        . write(SData+"|\n");
                         SData     = "";
                    }
               }         
          }
          catch(Exception ex)
          {
               System    . out.println(ex);
               ex        . printStackTrace();
          }
     }
     public void setFoot(boolean bflag)
     {
          try
          {
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|    Documents Enclosed                                                                                                                                                     |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");

               if((SDCNo.length()>0) && (SInvNo.length()>0))
               {
                    FW   . write("|  1  |  DC No.      / Date |"+common.Pad("  "+SDCNo+" / "+common.parseDate(common.parseNull(SDCDate)),47)+"|  6  |  Inv No.   / Date   |"+common.Pad("  "+SInvNo+" / "+common.parseDate(common.parseNull(SInvDate)),67)+"|\n");
               }
               else
               if((SDCNo.length()==0) && (SInvNo.length()>0))
               {
                    FW   . write("|  1  |  DC No.      / Date |"+common.Pad("  ",47)+"|  6  |  Inv No.   / Date   |"+common.Pad("  "+SInvNo+" / "+common.parseDate(common.parseNull(SInvDate)),67)+"|\n");
               }
               else
               if((SDCNo.length()>0) && (SInvNo.length()==0))
               {
                    FW   . write("|  1  |  DC No.      / Date |"+common.Pad("  "+SDCNo+" / "+common.parseDate(common.parseNull(SDCDate)),47)+"|  6  |  Inv No.   / Date   |"+common.Pad("  ",67)+"|\n");
               }
               else
               {
                    FW   . write("|  1  |  DC No.      / Date |"+common.Pad("  ",47)+"|  6  |  Inv No.   / Date   |"+common.Pad("  ",67)+"|\n");
               }


               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");


               if(SFormJJNo.length()>0)
               {
                    FW   . write("|  3  |  Form JJ No. / Date |"+common.Pad("  "+SFormJJNo+" / "+common.parseDate(common.parseNull(SFormJJDate)),47)+"|  4  |  Check Post Letter  |                                                                   |\n");
               }
               else
               {
                    FW   . write("|  3  |  Form JJ No. / Date |"+common.Pad("  ",47)+"|  4  |  Check Post Letter  |                                                                   |\n");
               }


               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|  5  |  RDC No.     / Date |                                               |  6  |  NRDC No.  / Date   |                                                                   |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|  7  |  Other doc. If Any  |                                               |     |                     |                                                                   |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|  Document are verified and found correct and permitted the movement of the above materials.                                                                               |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|                                        |                                                       |                                                                          |\n");
               FW   . write("|                                        |                                                       |                                                                          |\n");
               FW   . write("|              Prepared By               |                Signature of the Staff                 |                     Permitted By                                         |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|    The above materials are verified and hase been sent out.                                                                                                               |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|                                        |                                                       |                                                                          |\n");
               FW   . write("|                                        |                                                       |                                                                          |\n");
               FW   . write("|                                        |            Actual Time Out                            |         Sign of Security Officer                                         |\n");
               FW   . write("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               FW   . write("|    Instructions :      1. Only J.M.D / AO / JO (Accts) are alone permitted to give outpass.                                                                               |\n");
               FW   . write("|                        2. Goods shall not be permitted to move between 6.00 PM to 9.00 AM.                                                                                |\n");
               FW   . write("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
               if(bflag)
               {
                    FW   . write("<End of Report>\n");
               }
               else
               {
                    FW   . write("Continued on Next Page...\n");
               }
          }
          catch(Exception ex)
          {
               System    . out.println(ex);
               ex        . printStackTrace();
          }
     }

     private void setDataintoDataTypes()
     {
          String QS =    " Select Descript,Qty,Purpose,PartyName,Address1,Address2,Address3,TimeOut,DCNo,DCDate,InvNo,InvDate,FormJJNo,FormJJDate,Id From "+
                         " (Select Descript,Qty,Purpose,PartyName,Address1,Address2,Place.PlaceName as Address3,TimeOut,DCNo,DCDate,InvNo,InvDate,FormJJNo,FormJJDate,OutPassDetails.Id "+
                         " From OutPassDetails "+
                         " Inner Join PartyMaster On PartyMaster.PartyCode = OutPassDetails.PartyCode "+
                         " Inner Join Place on PartyMaster.CityCode=Place.PlaceCode "+
                         " Where OutPassNo ="+SOutPassNo+" and MaterialType="+SMatType+" and BookType="+SBookType+
                         " and MillCode="+iMillCode+" and PartyMaster.CustomerTypeCode<>2 "+
                         " Union All "+
                         " Select Descript,Qty,Purpose,PartyName,Address1,Address2,Address3,TimeOut,DCNo,DCDate,InvNo,InvDate,FormJJNo,FormJJDate,OutPassDetails.Id "+
                         " From OutPassDetails "+
                         " Inner Join PartyMaster On PartyMaster.PartyCode = OutPassDetails.PartyCode "+
                         " Where OutPassNo ="+SOutPassNo+" and MaterialType="+SMatType+" and BookType="+SBookType+
                         " and MillCode="+iMillCode+" and PartyMaster.CustomerTypeCode=2) "+
                         " Order by Id ";
          try
          {
               if(theconnect==null)
               {
                    connect   = ORAConnection.getORAConnection();
                    theconnect= connect.getConnection();
               }
               Statement stat       = theconnect.createStatement();
               ResultSet theResult  = stat.executeQuery(QS);

               while(theResult.next())
               {
                    VDesc          . addElement(common.parseNull((String)theResult.getString(1)));
                    VKgs           . addElement(common.parseNull((String)theResult.getString(2)));
                    VPurpose       . addElement(common.parseNull((String)theResult.getString(3)));

                    SSupplierName  = common.parseNull((String)theResult.getString(4));
                    SAdd1          = common.parseNull((String)theResult.getString(5));
                    SAdd2          = common.parseNull((String)theResult.getString(6));
                    SAdd3          = common.parseNull((String)theResult.getString(7));

                    SOutTime       = common.parseNull((String)theResult.getString(8));
                    SDCNo          = common.parseNull((String)theResult.getString(9));
                    SDCDate        = common.parseNull((String)theResult.getString(10));
                    SInvNo         = common.parseNull((String)theResult.getString(11));
                    SInvDate       = common.parseNull((String)theResult.getString(12));
                    SFormJJNo      = common.parseNull((String)theResult.getString(13));
                    SFormJJDate    = common.parseNull((String)theResult.getString(14));
               }
               theResult . close();
               stat      . close();
          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}
