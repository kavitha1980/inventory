package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.sql.*;
import java.util.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class RDC
{
     JMenuBar       theMenuBar;
     JFrame         theFrame;
     Container      Container;
     JDesktopPane   DeskTop;
     StatusPanel    SPanel;
     JPanel         MenuPanel;

     JMenu RDCOutMenu,WorkOrderMenu,RDCInMenu,GRNMenu,OutPassMenu;

     JMenuItem mRDCMemo,mRDCOut,mRDCOutList,mRDCAsset,mRDCPending,mRDCDatePending,mRDCDatePendingDateWise,mRDCPrint,mRDCDelete;
     JMenuItem mRDCGateIn,mRDCIn,mRDCAbs,mRDCDetails,mRDCSupDetails,mRDCListDetails;
     JMenuItem mWorkOrder,mWorkOrderList,mWorkOrderDetail,mWorkOrderAbs,mWorkOrderDelete;
     JMenuItem mGRN,mGRNList,mGRNListPending,mGRNListPending1,mGRNDetail,mGRNAbs;


     JMenuItem      mStoreOutPass,mWasteOutPass,mRdcForStores,mYarnOutPass,mOthersOutPass,mStoresOutPassPrint,mWasteOutPassPrint,mYarnOutPassPrint,mOthersOutPassPrint;

     boolean        bflag;
     Common         common;

     // Possible Look & Feels
     String mac      = "com.sun.java.swing.plaf.mac.MacLookAndFeel";
     String metal    = "javax.swing.plaf.metal.MetalLookAndFeel";
     String motif    = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
     String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

     // The current Look & Feel
     String currentLookAndFeel = windows;

     int    iUserCode,iAuthCode,iMillCode;
     String SFinYear,SStDate,SEnDate,SYearCode,SItemTable,SSupTable,SMillName;

     public RDC(int iUserCode,int iAuthCode,int iMillCode,String SFinYear,String SStDate,String SEnDate,String SYearCode,String SItemTable,String SSupTable,String SMillName) 
     {
          this.iUserCode  = iUserCode;
          this.iAuthCode  = iAuthCode;
          this.iMillCode  = iMillCode;
          this.SFinYear   = SFinYear;
          this.SStDate    = SStDate;
          this.SEnDate    = SEnDate;
          this.SYearCode  = SYearCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;

          updateLookAndFeel();
          CreateComponents();
          setLayouts();
          AddComponents();
          AddListeners();
          theFrame.setVisible(true);
     }

     public void CreateComponents()
     {
          theFrame        = new JFrame("RDC Monitoring System - "+SMillName+" - Year - "+SFinYear);

          Container       = theFrame.getContentPane();
          DeskTop         = new JDesktopPane();
          SPanel          = new StatusPanel();
          MenuPanel       = new JPanel();
          theMenuBar      = new JMenuBar();

          if(iMillCode==0)
          {
               Container . setBackground(new Color(250,200,200));
               theFrame  . setBackground(new Color(250,200,200));
               DeskTop   . setBackground(new Color(250,200,200));
               theMenuBar. setBackground(new Color(250,200,200));
               MenuPanel . setBackground(new Color(250,200,200));
          }
          if(iMillCode==1)
          {
               Container . setBackground(new Color(220,250,250));
               theFrame  . setBackground(new Color(220,250,250));
               DeskTop   . setBackground(new Color(220,250,250));
               theMenuBar. setBackground(new Color(220,250,250));
               MenuPanel . setBackground(new Color(220,250,250));
          }
          if(iMillCode==3)
          {
               Container . setBackground(new Color(200,250,200));
               theFrame  . setBackground(new Color(200,250,200));
               DeskTop   . setBackground(new Color(200,250,200));
               theMenuBar. setBackground(new Color(200,250,200));
               MenuPanel . setBackground(new Color(200,250,200));
          }
          if(iMillCode==4)
          {
               Container . setBackground(new Color(200,200,250));
               theFrame  . setBackground(new Color(200,200,250));
               DeskTop   . setBackground(new Color(200,200,250));
               theMenuBar. setBackground(new Color(200,200,250));
               MenuPanel . setBackground(new Color(200,200,250));
          }

          RDCOutMenu      = new JMenu("RDC Outwards");
          RDCInMenu       = new JMenu("RDC Inwards");
          WorkOrderMenu   = new JMenu("Work Orders");
          GRNMenu         = new JMenu("GRN");


          mRDCMemo        = new JMenuItem("RDC Against Memo");
          mRDCOut         = new JMenuItem("RDC Outward Slip");
          mRDCOutList     = new JMenuItem("RDC Outward List");
          //mRDCAsset       = new JMenuItem("RDC to Outside Asset Transfer");
          mRDCPending     = new JMenuItem("RDC Pending List - I");
          mRDCDatePending = new JMenuItem("RDC Pending List - II");
          mRDCDatePendingDateWise = new JMenuItem("RDC Pending List - III");
          mRDCPrint       = new JMenuItem("RDC Printing");
          mRDCDelete      = new JMenuItem("RDC Deletion");

          mRDCGateIn     = new JMenuItem("RDC Gate Entry");
          mRDCIn         = new JMenuItem("RDC Inward Slip");
          mRDCDetails    = new JMenuItem("Receipt Details"); 
          mRDCSupDetails = new JMenuItem("Receipt Details - SupplierWise"); 
          mRDCListDetails= new JMenuItem("Receipt List Details"); 

          mWorkOrder       = new JMenuItem("Work Order Placement");
          mWorkOrderList   = new JMenuItem("List of Work Orders");
          mWorkOrderDetail = new JMenuItem("List of Work Orders - Detailed");
          mWorkOrderAbs    = new JMenuItem("Work Order Abstract");
          mWorkOrderDelete = new JMenuItem("Work Order Deletion");

          mGRN           = new JMenuItem("GRN against Work Order");
          mGRNList       = new JMenuItem("List of GRNs");
          mGRNDetail     = new JMenuItem("List of GRNs - Detailed");
          mGRNAbs        = new JMenuItem("GRN Abstract");
          mGRNListPending= new JMenuItem("Pending @Finance");
          mGRNListPending1= new JMenuItem("List of GRNs - Pending");

          common         = new Common();
     }
     public void setLayouts()
     {
          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          theFrame  .setSize(screenSize);
          MenuPanel .setLayout(new BorderLayout());          
     }
     public void AddComponents()
     {
          Container      .add("North",MenuPanel);
          Container      .add("Center",DeskTop);
          Container      .add("South",SPanel);

          RDCOutMenu     .setMnemonic('O');
          RDCInMenu      .setMnemonic('I');
          WorkOrderMenu  .setMnemonic('W');
          GRNMenu        .setMnemonic('G');

          if((iUserCode!=11) && (iUserCode!=12))
          {
               theMenuBar     .add(RDCOutMenu);
               theMenuBar     .add(RDCInMenu);
               theMenuBar     .add(WorkOrderMenu);
               theMenuBar     .add(GRNMenu);
          }

          theMenuBar     .add(OutPassMenu    = new JMenu("Out Pass"));
          OutPassMenu    .setMnemonic('P');
          
          if(iAuthCode>0)
          {
               RDCOutMenu     .add(mRDCMemo);
               RDCOutMenu     .addSeparator();
               RDCOutMenu     .add(mRDCOut);
               //RDCOutMenu     .add(mRDCAsset);
               //RDCOutMenu     .addSeparator();
          }
          RDCOutMenu     .add(mRDCPrint);
          RDCOutMenu     .add(mRDCOutList);
          RDCOutMenu     .add(mRDCPending);
          RDCOutMenu     .add(mRDCDatePending);
          RDCOutMenu     .add(mRDCDatePendingDateWise);

          if(iAuthCode==3)
          {
               RDCOutMenu  .addSeparator();
               RDCOutMenu  .add(mRDCDelete);
          }

          if(iAuthCode>0)
          {
               RDCInMenu      .add(mRDCGateIn);
               RDCInMenu      .add(mRDCIn);
               RDCInMenu      .addSeparator();
          }
          RDCInMenu      .add(mRDCDetails);
          RDCInMenu      .add(mRDCSupDetails);
          RDCInMenu      .add(mRDCListDetails);

          if(iAuthCode>0)
          {
               WorkOrderMenu  .add(mWorkOrder);
               WorkOrderMenu  .addSeparator();
          }

          WorkOrderMenu  .add(mWorkOrderList);
          WorkOrderMenu  .add(mWorkOrderDetail);
          WorkOrderMenu  .add(mWorkOrderAbs);

          if(iAuthCode==3)
          {
               WorkOrderMenu  .add(mWorkOrderDelete);
          }

          if(iAuthCode>0)
          {
               GRNMenu        .add(mGRN);
               GRNMenu        .addSeparator();
          }

          GRNMenu        .add(mGRNList);
          GRNMenu        .add(mGRNDetail);
          GRNMenu        .add(mGRNAbs);
          GRNMenu        .add(mGRNListPending);
          GRNMenu        .add(mGRNListPending1);

          OutPassMenu    . add(mStoreOutPass       = new JMenuItem("Out Pass for Stores Items"));
          OutPassMenu    . add(mStoresOutPassPrint = new JMenuItem("Out Pass Print for Stores Items"));
          OutPassMenu    . add(mRdcForStores       = new JMenuItem("OutPassPendingList for Stores"));
          OutPassMenu    . addSeparator();
          OutPassMenu    . add(mOthersOutPass      = new JMenuItem("Out Pass for Others"));
          OutPassMenu    . add(mOthersOutPassPrint = new JMenuItem("Out Pass Print for Others Items"));

          if(iMillCode==0)
          {
               OutPassMenu    . addSeparator();
               OutPassMenu    . add(mWasteOutPass       = new JMenuItem("Out Pass for Waste"));
               OutPassMenu    . add(mWasteOutPassPrint  = new JMenuItem("Out Pass Print for Waste"));
               OutPassMenu    . addSeparator();
               OutPassMenu    . add(mYarnOutPass        = new JMenuItem("Out Pass for Yarn"));
               OutPassMenu    . add(mYarnOutPassPrint   = new JMenuItem("Out Pass Print for Yarn"));
          }

          MenuPanel      .add("West",theMenuBar);
     }
     public void AddListeners()
     {
          theFrame       .addWindowListener(new WinList());

          mRDCMemo       .addActionListener(new ActList());
          mRDCOut        .addActionListener(new ActList());
          mRDCOutList    .addActionListener(new ActList());
          //mRDCAsset      .addActionListener(new ActList());
          mRDCPending    .addActionListener(new ActList());
          mRDCDatePending.addActionListener(new ActList());
          mRDCDatePendingDateWise.addActionListener(new ActList());
          mRDCPrint.addActionListener(new ActList());
          mRDCDelete.addActionListener(new ActList());

          mRDCGateIn     .addActionListener(new ActList());
          mRDCIn         .addActionListener(new ActList());
          mRDCDetails    .addActionListener(new ActList());
          mRDCSupDetails .addActionListener(new ActList());
          mRDCListDetails.addActionListener(new ActList());

          mWorkOrder      .addActionListener(new ActList());
          mWorkOrderList  .addActionListener(new ActList());
          mWorkOrderDetail.addActionListener(new ActList());
          mWorkOrderAbs   .addActionListener(new ActList());
          mWorkOrderDelete.addActionListener(new ActList());

          mGRN           .addActionListener(new ActList());
          mGRNList       .addActionListener(new ActList());
          mGRNDetail     .addActionListener(new ActList());
          mGRNAbs        .addActionListener(new ActList());
          mGRNListPending.addActionListener(new ActList());
          mGRNListPending1.addActionListener(new ActList());

          mStoreOutPass       . addActionListener(new ActList());
          mStoresOutPassPrint . addActionListener(new ActList());
          mRdcForStores       . addActionListener(new ActList());
          mOthersOutPass      . addActionListener(new ActList());
          mOthersOutPassPrint . addActionListener(new ActList());

          if(iMillCode==0)
          {
               mWasteOutPass       . addActionListener(new ActList());
               mWasteOutPassPrint  . addActionListener(new ActList());
               mYarnOutPass        . addActionListener(new ActList());
               mYarnOutPassPrint   . addActionListener(new ActList());
          }
     }    
     public class WinList extends WindowAdapter
     {
          public void windowClosing(WindowEvent we)
          {
               System.exit(0);
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==mRDCMemo)
               {
                    RDCMemoPendingFrame rdcmemopendingframe = new RDCMemoPendingFrame(DeskTop,iUserCode,iMillCode,SSupTable,SYearCode);
                    try
                    {
                         DeskTop.add(rdcmemopendingframe);
                         DeskTop.repaint();
                         rdcmemopendingframe.setSelected(true);
                         DeskTop.updateUI();
                         rdcmemopendingframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mRDCOut)
               {
                    RDCOutFrame rdcoutframe = new RDCOutFrame(DeskTop,iUserCode,iMillCode,false,SSupTable,SYearCode);   // true - for Modification; false - for entry
                    try
                    {
                         DeskTop.add(rdcoutframe);
                         DeskTop.repaint();
                         rdcoutframe.setSelected(true);
                         DeskTop.updateUI();
                         rdcoutframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mRDCPrint)
               {
                    RDCPrinting rDCPrinting = new RDCPrinting(DeskTop,iUserCode,iMillCode,SSupTable);

                    try
                    {
                         DeskTop.add(rDCPrinting);
                         DeskTop.repaint();
                         rDCPrinting.setSelected(true);
                         DeskTop.updateUI();
                         rDCPrinting.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mRDCOutList)
               {
                    RDCListFrame rdclistframe = new RDCListFrame(DeskTop,SPanel,iUserCode,iMillCode,SSupTable,SYearCode,SMillName);
                    try
                    {
                         DeskTop.add(rdclistframe);
                         DeskTop.repaint();
                         rdclistframe.setSelected(true);
                         DeskTop.updateUI();
                         rdclistframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mRDCPending)
               {
                    RDCPendingList rdcpendinglist = new RDCPendingList(DeskTop,SPanel,iMillCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(rdcpendinglist);
                         DeskTop.repaint();
                         rdcpendinglist.setSelected(true);
                         DeskTop.updateUI();
                         rdcpendinglist.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mRDCDatePending)
               {
                    RDCDateRegister rdcdatependinglist = new RDCDateRegister(DeskTop,SPanel,iMillCode,SSupTable,SMillName);

                    try
                    {
                         DeskTop.add(rdcdatependinglist);
                         DeskTop.repaint();
                         rdcdatependinglist.setSelected(true);
                         DeskTop.updateUI();
                         rdcdatependinglist.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mRDCDatePendingDateWise)
               {
                    RDCPendingListDateWise rdcdatependinglistdatewise = new RDCPendingListDateWise(DeskTop,SPanel,iMillCode,SSupTable);

                    try
                    {
                         DeskTop.add(rdcdatependinglistdatewise);
                         DeskTop.repaint();
                         rdcdatependinglistdatewise.setSelected(true);
                         DeskTop.updateUI();
                         rdcdatependinglistdatewise.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mRDCDelete)
               {
                    RDCDeletionFrame rdcdeletionframe = new RDCDeletionFrame(DeskTop,SPanel,iUserCode,iMillCode,SSupTable);

                    try
                    {
                         DeskTop.add(rdcdeletionframe);
                         DeskTop.repaint();
                         rdcdeletionframe.setSelected(true);
                         DeskTop.updateUI();
                         rdcdeletionframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mRDCGateIn)
               {
                    GateRDCInFrame grdcinframe = new GateRDCInFrame(DeskTop,SPanel,iUserCode,iMillCode,SSupTable);
                    try
                    {
                         DeskTop.add(grdcinframe);
                         DeskTop.repaint();
                         grdcinframe.setSelected(true);
                         DeskTop.updateUI();
                         grdcinframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mRDCIn)
               {
                    RDCInFrame rdcinframe = new RDCInFrame(DeskTop,SPanel,iUserCode,iMillCode,SSupTable,SYearCode);
                    try
                    {
                         DeskTop.add(rdcinframe);
                         DeskTop.repaint();
                         rdcinframe.setSelected(true);
                         DeskTop.updateUI();
                         rdcinframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mRDCDetails)
               {
                    RDCDetailsFrame rdcDetailsFrame = new RDCDetailsFrame(DeskTop,SPanel,iMillCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(rdcDetailsFrame);
                         DeskTop.repaint();
                         rdcDetailsFrame.setSelected(true);
                         DeskTop.updateUI();
                         rdcDetailsFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mRDCSupDetails)
               {
                    RDCSupDetailsFrame rdcSupDetailsFrame = new RDCSupDetailsFrame(DeskTop,SPanel,iMillCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(rdcSupDetailsFrame);
                         DeskTop.repaint();
                         rdcSupDetailsFrame.setSelected(true);
                         DeskTop.updateUI();
                         rdcSupDetailsFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mRDCListDetails)
               {
                    RDCListDetailsFrame rdclistDetailsFrame = new RDCListDetailsFrame(DeskTop,SPanel,iMillCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(rdclistDetailsFrame);
                         DeskTop.repaint();
                         rdclistDetailsFrame.setSelected(true);
                         DeskTop.updateUI();
                         rdclistDetailsFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mWorkOrder)
               {
                    DirectOrderFrame orderframe = new DirectOrderFrame(DeskTop,SPanel,false,iUserCode,iMillCode,SSupTable,SYearCode);
                    try
                    {
                         DeskTop.add(orderframe);
                         DeskTop.repaint();
                         orderframe.setSelected(true);
                         DeskTop.updateUI();
                         orderframe.show();
                         orderframe.TDate.requestFocus();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mWorkOrderList)
               {
                    OrderListFrame orderlistframe = new OrderListFrame(DeskTop,SPanel,iUserCode,iMillCode,iAuthCode,SSupTable);
                    try
                    {
                         DeskTop.add(orderlistframe);
                         DeskTop.repaint();
                         orderlistframe.setSelected(true);
                         DeskTop.updateUI();
                         orderlistframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mWorkOrderDetail)
               {
                    WorkOrderDetailsFrame workorderdetailsframe = new WorkOrderDetailsFrame(DeskTop,SPanel,iUserCode,iMillCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(workorderdetailsframe);
                         DeskTop.repaint();
                         workorderdetailsframe.setSelected(true);
                         DeskTop.updateUI();
                         workorderdetailsframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mWorkOrderAbs)
               {
                    OrderAbsFrame orderabsframe = new OrderAbsFrame(DeskTop,SPanel,iMillCode,SSupTable);
                    
                    try
                    {
                         DeskTop.add(orderabsframe);
                         DeskTop.repaint();
                         orderabsframe.setSelected(true);
                         DeskTop.updateUI();
                         orderabsframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mWorkOrderDelete)
               {
                    OrderDeletionFrame orderdeletionframe = new OrderDeletionFrame(DeskTop,SPanel,iUserCode,iMillCode,SSupTable);
                    try
                    {
                         DeskTop.add(orderdeletionframe);
                         DeskTop.repaint();
                         orderdeletionframe.setSelected(true);
                         DeskTop.updateUI();
                         orderdeletionframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mGRN)
               {
                    DirectGRNFrame directgrnframe = new DirectGRNFrame(DeskTop,SPanel,iUserCode,iMillCode,SSupTable,SYearCode);
                    try
                    {
                         DeskTop.add(directgrnframe);
                         DeskTop.repaint();
                         directgrnframe.setSelected(true);
                         DeskTop.updateUI();
                         directgrnframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mGRNList)
               {
                    GRNListFrame grnlistframe = new GRNListFrame(DeskTop,SPanel,iUserCode,iMillCode,iAuthCode,SSupTable);
                    try
                    {
                         DeskTop.add(grnlistframe);
                         DeskTop.repaint();
                         grnlistframe.setSelected(true);
                         DeskTop.updateUI();
                         grnlistframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mGRNDetail)
               {
                    WorkGRNDetailsFrame workgrndetailsframe = new WorkGRNDetailsFrame(DeskTop,SPanel,iUserCode,iMillCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(workgrndetailsframe);
                         DeskTop.repaint();
                         workgrndetailsframe.setSelected(true);
                         DeskTop.updateUI();
                         workgrndetailsframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mGRNAbs)
               {
                    GRNAbsFrame grnabsframe = new GRNAbsFrame(DeskTop,SPanel,iMillCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(grnabsframe);
                         DeskTop.repaint();
                         grnabsframe.setSelected(true);
                         DeskTop.updateUI();
                         grnabsframe.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource()==mGRNListPending)
               {
                    FinPendingList finpendinglist = new FinPendingList(DeskTop,SPanel,iMillCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(finpendinglist);
                         DeskTop.repaint();
                         finpendinglist.setSelected(true);
                         DeskTop.updateUI();
                         finpendinglist.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource()==mGRNListPending1)
               {
                    GRNListFramePending grnlistframepending = new GRNListFramePending(DeskTop,SPanel,iUserCode,iMillCode,SSupTable);
                    try
                    {
                         DeskTop.add(grnlistframepending);
                         DeskTop.repaint();
                         grnlistframepending.setSelected(true);
                         DeskTop.updateUI();
                         grnlistframepending.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource() == mStoreOutPass)
               {
                    StoresOutPassFrame storesOutPassFrame = new StoresOutPassFrame(DeskTop,iMillCode,iUserCode,SYearCode);
                    try
                    {
                         DeskTop.add(storesOutPassFrame);
                         DeskTop.repaint();
                         storesOutPassFrame.setSelected(true);
                         DeskTop.updateUI();
                         storesOutPassFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource() == mStoresOutPassPrint)
               {
                    StoresOutPassPrintFrame storesOutPassPrintFrame = new  StoresOutPassPrintFrame(DeskTop,iUserCode,iMillCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(storesOutPassPrintFrame);
                         DeskTop.repaint();
                         storesOutPassPrintFrame.setSelected(true);
                         DeskTop.updateUI();
                         storesOutPassPrintFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource() == mRdcForStores)
               {
                    RDCPrintForStore RdcprintFrame = new RDCPrintForStore(DeskTop,iMillCode,iUserCode,SSupTable,SMillName);
                    try
                    {
                         DeskTop.add(RdcprintFrame);
                         DeskTop.repaint();
                         RdcprintFrame.setSelected(true);
                         DeskTop.updateUI();
                         RdcprintFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource() == mOthersOutPass)
               {
                    OthersOutPassEntryFrame othersOutPassFrame = new OthersOutPassEntryFrame(DeskTop,iMillCode,iUserCode,SYearCode);
                    try
                    {
                         DeskTop.add(othersOutPassFrame);
                         DeskTop.repaint();
                         othersOutPassFrame.setSelected(true);
                         DeskTop.updateUI();
                         othersOutPassFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource() == mOthersOutPassPrint)
               {
                    OthersOutPassPrintFrame othersOutPassPrintFrame = new  OthersOutPassPrintFrame(DeskTop,iUserCode,iMillCode,SMillName);
                    try
                    {
                         DeskTop.add(othersOutPassPrintFrame);
                         DeskTop.repaint();
                         othersOutPassPrintFrame.setSelected(true);
                         DeskTop.updateUI();
                         othersOutPassPrintFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource() == mWasteOutPass)
               {
                    WasteOutPassFrame wasteOutPassFrame = new WasteOutPassFrame(DeskTop,iMillCode,iUserCode,SYearCode);
                    try
                    {
                         DeskTop.add(wasteOutPassFrame);
                         DeskTop.repaint();
                         wasteOutPassFrame.setSelected(true);
                         DeskTop.updateUI();
                         wasteOutPassFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
               if(ae.getSource() == mWasteOutPassPrint)
               {
                    WasteOutPassPrintFrame wasteOutPassPrintFrame = new  WasteOutPassPrintFrame(DeskTop,iUserCode,iMillCode,SMillName);
                    try
                    {
                         DeskTop.add(wasteOutPassPrintFrame);
                         DeskTop.repaint();
                         wasteOutPassPrintFrame.setSelected(true);
                         DeskTop.updateUI();
                         wasteOutPassPrintFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource() == mYarnOutPass)
               {
                    YarnOutPassFrame yarnOutPassFrame = new YarnOutPassFrame(DeskTop,iMillCode,iUserCode,SYearCode);
                    try
                    {
                         DeskTop.add(yarnOutPassFrame);
                         DeskTop.repaint();
                         yarnOutPassFrame.setSelected(true);
                         DeskTop.updateUI();
                         yarnOutPassFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

               if(ae.getSource() == mYarnOutPassPrint)
               {
                    YarnOutPassPrintFrame yarnOutPassPrintFrame = new  YarnOutPassPrintFrame(DeskTop,iUserCode,iMillCode,SMillName);
                    try
                    {
                         DeskTop.add(yarnOutPassPrintFrame);
                         DeskTop.repaint();
                         yarnOutPassPrintFrame.setSelected(true);
                         DeskTop.updateUI();
                         yarnOutPassPrintFrame.show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }

          }
     }
     public void updateLookAndFeel()
     {
          try
          {
               UIManager.setLookAndFeel(currentLookAndFeel);
          }
          catch (Exception ex)
          { 
               System.out.println("Failed loading L&F: " + currentLookAndFeel);
               System.out.println(ex);
          }
     }

}
