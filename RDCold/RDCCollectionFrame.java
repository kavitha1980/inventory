package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCCollectionFrame extends JInternalFrame
{

     JTextField     TGINo;
     DateField      TDate,TGIDate,TInvDate,TDCDate;
     JTextField     TGrnNo,TInvNo,TDCNo,TSupName;
     RDCMiddlePanel MiddlePanel;
     JPanel         TopPanel,BottomPanel;
     JButton        BOk,BCancel;

     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     String         SIndex;
     int            iIndex=0;
     JTable         ReportTable;
     int            iUserCode;
     int            iMillCode;
     RDCInFrame     rdcinframe;
     String         SYearCode;

     Common common   = new Common();
     ORAConnection connect;
     Connection theconnect;
     
     RDCCollectionFrame(JLayeredPane DeskTop,StatusPanel SPanel,String SIndex,JTable ReportTable,int iUserCode,int iMillCode,RDCInFrame rdcinframe,String SYearCode)
     {
          this.DeskTop            = DeskTop;
          this.SPanel             = SPanel;
          this.SIndex             = SIndex;
          this.ReportTable        = ReportTable;
          this.iUserCode          = iUserCode;
          this.iMillCode          = iMillCode;
          this.rdcinframe         = rdcinframe;
          this.SYearCode          = SYearCode;
          
          iIndex = common.toInt(SIndex);

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }

     public void createComponents()
     {
          BOk         = new JButton("Save");
          BCancel     = new JButton("Abort");

          TDate       = new DateField();
          TGrnNo      = new JTextField();
          TGINo       = new JTextField();
          TGIDate     = new DateField();
          TInvDate    = new DateField();
          TDCDate     = new DateField();
          TInvNo      = new JTextField();
          TDCNo       = new JTextField();
          TSupName    = new JTextField();

          MiddlePanel = new RDCMiddlePanel(DeskTop);

          TopPanel    = new JPanel();
          BottomPanel = new JPanel();

          setGrnNo();

          TDate     .setTodayDate();

          TGrnNo    .setEditable(false);
          TGINo     .setEditable(false);
          TGIDate   .setEditable(false);
          TDate     .setEditable(false);
          TSupName  .setEditable(false);
          BOk.setMnemonic('S');
          BCancel.setMnemonic('A');
     }

     public void setLayouts()
     {
          setTitle("Invoice Valuation and Quantity Appropriation against pending RDC Outwards");

          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,790,500);

          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(5,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("Gate Inward No"));
          TopPanel.add(TGINo);

          TopPanel.add(new JLabel("Gate Inward Date"));
          TopPanel.add(TGIDate);

          TopPanel.add(new JLabel("GRN No"));
          TopPanel.add(TGrnNo);

          TopPanel.add(new JLabel("GRN Date"));
          TopPanel.add(TDate);

          TopPanel.add(new JLabel("Supplier"));
          TopPanel.add(TSupName);

          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));

          TopPanel.add(new JLabel("Invoice No"));
          TopPanel.add(TInvNo);

          TopPanel.add(new JLabel("Invoice Date"));
          TopPanel.add(TInvDate);

          TopPanel.add(new JLabel("DC No"));
          TopPanel.add(TDCNo);

          TopPanel.add(new JLabel("DC Date"));
          TopPanel.add(TDCDate);

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          TGINo     .setText((String)ReportTable.getModel().getValueAt(iIndex,0));
          TGIDate   .fromString((String)ReportTable.getModel().getValueAt(iIndex,1));
          TSupName  .setText((String)ReportTable.getModel().getValueAt(iIndex,2));
          TInvNo    .setText((String)ReportTable.getModel().getValueAt(iIndex,3));
          TInvDate  .fromString((String)ReportTable.getModel().getValueAt(iIndex,4));
          TDCNo     .setText((String)ReportTable.getModel().getValueAt(iIndex,5));
          TDCDate   .fromString((String)ReportTable.getModel().getValueAt(iIndex,6));

          MiddlePanel.createComponents(TGINo.getText(),iMillCode);
     }

     public void addListeners()
     {
          BOk.addActionListener(new ActList());
          BCancel.addActionListener(new CancelList());
     }

     public class CancelList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               removeHelpFrame();
          }

     }
     public void setGrnNo()
     {
          String SGrnNo = "";

          SGrnNo = String.valueOf(common.toInt(common.getID("Select MaxNo From Config"+iMillCode+""+SYearCode+" Where id=10"))+1);

          TGrnNo    .setText(SGrnNo);
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(false);
               setGrnNo();
               setRdcOutLink();
               updateGrnNo();
               removeHelpFrame();
               rdcinframe.addComponents();
               rdcinframe.tabreport.ReportTable.requestFocus();
          }
     }

     private void updateGrnNo()
     {
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();

               String QS = " Update Config"+iMillCode+""+SYearCode+" Set MaxNo="+(common.toInt((String)TGrnNo.getText()))+" Where ID=10 ";
               stat.executeUpdate(QS);
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     public void setRdcOutLink()
     {
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               for(int i=0;i<MiddlePanel.RowData.length;i++)
               {
                    String QS = " Update RDC Set ";
                    QS = QS+" RecQty=RecQty+"+((String)MiddlePanel.RowData[i][5]).trim();
                    QS = QS+" Where Id  = "+(String)MiddlePanel.VRId.elementAt(i);
                    QS = QS+" and SlNo  = "+(String)MiddlePanel.VRDCSlNo.elementAt(i);
                    QS = QS+" and RdcNo = "+(String)MiddlePanel.VRDCNo.elementAt(i);
                    QS = QS+" and MillCode = "+iMillCode;

                    stat.execute(QS);
               }
               
               for(int i=0;i<MiddlePanel.RowData.length;i++)
               {
                    String QS =" Update GateInwardRDC Set ";
                    QS = QS+" InvNo='"+TInvNo.getText()+"',";
                    QS = QS+" InvDate='"+TInvDate.toNormal()+"',";
                    QS = QS+" DcNo='"+TDCNo.getText()+"',";
                    QS = QS+" DcDate='"+TDCDate.toNormal()+"',";
                    QS = QS+" RecQty="+((String)MiddlePanel.RowData[i][5]).trim()+",";
                    QS = QS+" RdcNo ="+((String)MiddlePanel.RowData[i][0]).trim()+",";
                    QS = QS+" GrnNo ="+TGrnNo.getText();
                    QS = QS+" Where Id    = "+(String)MiddlePanel.VGId.elementAt(i);
                    QS = QS+" and RdcSlNo = "+(String)MiddlePanel.VGateSlNo.elementAt(i);
                    QS = QS+" and RdcNo   = "+(String)MiddlePanel.VGateRdcNo.elementAt(i);
                    QS = QS+" and MillCode = "+iMillCode;

                    stat.execute(QS);
               }
               stat.close();
          }
          catch(Exception ex)
          {
                System.out.println(ex);
          }
     }

}
