package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCCritPanel extends JPanel
{
    JPanel TopPanel;
    JPanel SupplierPanel;
    JPanel ListPanel;
    JPanel SortPanel;
    JPanel BasicPanel;
    JPanel ControlPanel;
    JPanel ApplyPanel;

    Common common = new Common();
    Vector VSup,VSupCode;
    MyComboBox JCSup,JCList;
    JComboBox JCOrder;

    JRadioButton JRPeriod,JRNo;
    JRadioButton JRAllSup,JRSeleSup;
    JRadioButton JRAllList,JRSeleList;

    JButton      BApply;
    JTextField   TStNo,TEnNo;
    DateField    TStDate,TEnDate;

    boolean bsig;
    ORAConnection connect;
    Connection theconnect;

    String SSupTable;

    RDCCritPanel(String SSupTable)
    {
        this.SSupTable = SSupTable;

        getSupplier();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
    }

    public void createComponents()
    {
        TopPanel      = new JPanel();

        JCOrder       = new JComboBox();

        ListPanel     = new JPanel();
        SortPanel     = new JPanel();
        ControlPanel  = new JPanel();
        ApplyPanel    = new JPanel();
        SupplierPanel = new JPanel();
        BasicPanel    = new JPanel();

        JRPeriod = new JRadioButton("Periodical",true);
        JRNo     = new JRadioButton("RDC No");

        JRAllSup    = new JRadioButton("All",true);
        JRSeleSup   = new JRadioButton("Selected");
        JCSup       = new MyComboBox(VSup);
        JCSup.setEnabled(false);

        JRAllList   = new JRadioButton("All",true);
        JRSeleList  = new JRadioButton("Selected");
        JCList      = new MyComboBox();
        JCList.setEnabled(false);

        bsig = true;

        TStNo       = new JTextField();
        TEnNo       = new JTextField();

        TStDate       = new DateField();
        TEnDate       = new DateField();

        BApply      = new JButton("Apply");

        TStDate.setTodayDate();
        TEnDate.setTodayDate();
    }
    public void setLayouts()
    {
        setLayout(new GridLayout(1,1));
        TopPanel.setLayout(new GridLayout(1,6));

        ListPanel  .setLayout(new GridLayout(3,1));
        SortPanel   .setLayout(new GridLayout(3,1));
        ControlPanel  .setLayout(new GridLayout(2,1));
        ApplyPanel  .setLayout(new GridLayout(3,1));
        SupplierPanel  .setLayout(new GridLayout(3,1));
        BasicPanel.setLayout(new GridLayout(2,1)); 

        SupplierPanel  .setBorder(new TitledBorder("Supplier"));
        ListPanel.setBorder(new TitledBorder("List Only"));
        SortPanel .setBorder(new TitledBorder("Sort on"));
        BasicPanel .setBorder(new TitledBorder("Based on"));
        ControlPanel.setBorder(new TitledBorder("Control"));
        ApplyPanel.setBorder(new TitledBorder("Apply"));
    }
    public void addComponents()
    {
        JCOrder.addItem("RDC No");
        JCOrder.addItem("Supplierwise");

        JCList.addItem("Invoice Received");
        JCList.addItem("Invoice Not Received");

        add(TopPanel);

        TopPanel.add(SupplierPanel);
        TopPanel.add(ListPanel);
        TopPanel.add(SortPanel);
        TopPanel.add(BasicPanel);
        TopPanel.add(ControlPanel);
        TopPanel.add(ApplyPanel);

        BasicPanel.add(JRPeriod);
        BasicPanel.add(JRNo);

        SupplierPanel.add(JRAllSup);
        SupplierPanel.add(JRSeleSup);
        SupplierPanel.add(JCSup);

        ListPanel.add(JRAllList);
        ListPanel.add(JRSeleList);
        ListPanel.add(JCList);

        SortPanel.add("Center",JCOrder);

        addControlPanel();

        ApplyPanel.add(new MyLabel(" "));
        ApplyPanel.add(BApply);
        ApplyPanel.add(new MyLabel(" "));
    }
    public void addControlPanel()
    {
        ControlPanel.removeAll();
        if(bsig)
        {
            ControlPanel.add(TStDate);
            ControlPanel.add(TEnDate);
        }
        else
        {
            ControlPanel.add(TStNo);
            ControlPanel.add(TEnNo);
        }
        ControlPanel.updateUI();
    }
    public void addListeners()
    {
        JRAllSup.addActionListener(new SupList());
        JRSeleSup.addActionListener(new SupList());

        JRAllList.addActionListener(new SelectList());
        JRSeleList.addActionListener(new SelectList());

        JRPeriod.addActionListener(new JRList());
        JRNo.addActionListener(new JRList());
    }
     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    ControlPanel.setBorder(new TitledBorder("Periodical"));
                    ControlPanel.removeAll();
                    ControlPanel.add(TStDate);
                    ControlPanel.add(TEnDate);
                    ControlPanel.updateUI();
                    JRNo.setSelected(false);
               }
               else
               {
                    ControlPanel.setBorder(new TitledBorder("Numbered"));
                    ControlPanel.removeAll();
                    ControlPanel.add(TStNo);
                    ControlPanel.add(TEnNo);
                    ControlPanel.updateUI();
                    JRPeriod.setSelected(false);
               }
          }
     }

    public class SupList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllSup)
            {
                JRAllSup.setSelected(true);
                JRSeleSup.setSelected(false);
                JCSup.setEnabled(false);
            }
            if(ae.getSource()==JRSeleSup)
            {
                JRAllSup.setSelected(false);
                JRSeleSup.setSelected(true);
                JCSup.setEnabled(true);
            }
        }
    }
    public class SelectList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllList)
            {
                JRAllList.setSelected(true);
                JRSeleList.setSelected(false);
                JCList.setEnabled(false);
            }
            if(ae.getSource()==JRSeleList)
            {
                JRAllList.setSelected(false);
                JRSeleList.setSelected(true);
                JCList.setEnabled(true);
            }
        }
    }

    public void getSupplier()
    {
        VSup       = new Vector();
        VSupCode   = new Vector();

        try
        {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();

               ResultSet result = stat.executeQuery("Select Name,Ac_Code From "+SSupTable+" Order By Name");
               while(result.next())
               {
                    VSup.addElement(result.getString(1));
                    VSupCode.addElement(result.getString(2));
               }
               result.close();
               stat.close();
        }
        catch(Exception ex)
        {
            System.out.println("Supplier :"+ex);
        }
    }

}
