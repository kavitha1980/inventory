package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCDateRegister extends JInternalFrame
{
     JPanel     TopPanel;
     JPanel     BottomPanel;
     JPanel     MiddlePanel;

     String     SDate;
     JButton    BApply,BPrint;
     JTextField TFile;
    
     DateField TDate;

     MyComboBox JCAsset;

    
     Object RowData[][];
     String ColumnData[] = {"RDC Date","RDC No","Supplier","Description","Remarks","Thro","Qty Sent","Qty Recd","Qty Pending","Due Date","Delayed Days"}; 
     String ColumnType[] = {"S","S","S","S","S","S","N","N","N","S","N"}; 

     JLayeredPane DeskTop;
     StatusPanel SPanel;
     int iMillCode;
     String SSupTable,SMillName;

     TabReport tabreport;
     Common common = new Common();
     Vector VRDCDate,VRDCNo,VSupName,VDesc,VRemark,VThro;
     Vector VRDCQty,VRecQty,VPending,VDueDate,VAcCode;

     int Lctr = 100,Pctr=0;
     FileWriter FW;
     String str6;
     boolean bBlank=true;
     ORAConnection connect;
     Connection theconnect;

     RDCDateRegister(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode,String SSupTable,String SMillName)
     {
         super("RDC Pending As On today - Datewise");
         this.DeskTop   = DeskTop;
         this.SPanel    = SPanel;
         this.iMillCode = iMillCode;
         this.SSupTable = SSupTable;
         this.SMillName = SMillName;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     private void createComponents()
     {
          TopPanel    = new JPanel(true);
          BottomPanel = new JPanel(true);
          MiddlePanel = new JPanel(true);

          TDate       = new DateField();
          JCAsset     = new MyComboBox();

          BApply      = new JButton("Apply");
          BPrint      = new JButton("Print");
          TFile       = new JTextField(15);

          TDate.setTodayDate();
          TFile.setText("RDCPend2.prn");
          TFile.setEditable(false);
          BPrint.setEnabled(false);

          BApply.setMnemonic('A');
          BPrint.setMnemonic('P');

          JCAsset.addItem("Non Asset");
          JCAsset.addItem("Asset");
          JCAsset.addItem("ALL");
          JCAsset.setSelectedItem("ALL");

     }
     private void setLayouts()
     {
         TopPanel.setLayout(new FlowLayout(0,0,0));
         MiddlePanel.setLayout(new BorderLayout());

         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,790,500);
     }
     private void addComponents()
     {
          TopPanel.add(new JLabel("As On"));
          TopPanel.add(TDate);

          TopPanel.add(new JLabel("Asset"));
          TopPanel.add(JCAsset);

          TopPanel.add(BApply);

          BottomPanel.add(TFile);
          BottomPanel.add(BPrint);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

     }
     private void addListeners()
     {
          BApply.addActionListener(new ActList());
          BPrint.addActionListener(new PrintList());
     }

     private class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setRDCPendingReport();
               removeHelpFrame();
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }

     private void setRDCPendingReport()
     {
          if(VAcCode.size() == 0)
               return;

          Lctr = 100;Pctr=0;
          String SFile = TFile.getText();
          if((SFile.trim()).length()==0)
               SFile = "1.prn";

          int iPNo=0;
          bBlank=false;
          try
          {
               FW = new FileWriter(common.getPrintPath()+SFile);
               for(int i=0;i<VRDCDate.size();i++)
               {
                    setHead();
                    int iNo = common.toInt((String)VRDCNo.elementAt(i));
                    if(iNo != iPNo)
                    {
                         if(i>0 && Lctr !=7)
                         {
                              FW.write(str6+"\n");
                              Lctr++;
                         }
                         iPNo = iNo;
                         bBlank=false;
                    }
                    setBody(i);
                    bBlank=true;
               }
               FW.write(str6+"\n");
               String SDateTime = common.getServerDateTime2();
               String SEnd = "Report Taken on "+SDateTime+"\n";
               FW.write(SEnd);
               FW.close();
          }
          catch(Exception ex){}
     }

     private void setHead() throws Exception
     {
          if (Lctr < 60)
               return;

          String str1 = "Company : "+SMillName;
          String str2 = "Document : RDC Pending List Register "+TDate.toString();
          String str4 = common.Pad("RDC",10)+common.Space(3)+common.Rad("RDC",10)+common.Space(3)+
                        common.Pad("Supplier",30)+common.Space(3)+ 
                        common.Pad("Material Description",40)+common.Space(3)+common.Pad("Instructions",15)+common.Space(3)+
                        common.Pad("Thro'",15)+common.Space(3)+ 
                        common.Rad("RDC",6)+common.Space(3)+common.Rad("Recd",6)+common.Space(3)+
                        common.Rad("Pending",6)+common.Space(3)+common.Pad("Due Date",10)+common.Space(3)+
                        common.Rad("Delayed",7);

          String str5 = common.Pad("Date",10)+common.Space(3)+common.Rad("No",10)+common.Space(3)+
                        common.Space(30)+common.Space(3)+ 
                        common.Pad(" ",40)+common.Space(3)+common.Pad(" ",15)+common.Space(3)+
                        common.Space(15)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Rad("Qty",6)+common.Space(3)+
                        common.Rad("Qty",6)+common.Space(3)+common.Pad(" ",10)+common.Space(3)+
                        common.Rad("Days",7);
          str6 = common.Replicate("-",str5.length());

          if (Pctr > 0)
               FW.write(str6+"\n");

          Pctr++;

          String str3 = "Page No  : "+Pctr+" ";

          FW.write("P"+str1+"\n");
          FW.write(str2+"\n");
          FW.write(str3+"g\n");
          FW.write(str6+"\n");
          FW.write(str4+"\n");
          FW.write(str5+"\n");
          FW.write(str6+"\n");
          Lctr = 7;
          bBlank=false;
     }

     private void setBody(int i) throws Exception
     {
          String SDate    = common.parseDate((String)VRDCDate  .elementAt(i));
          String SNo      = (String)VRDCNo    .elementAt(i);
          String SDesc    = (String)VDesc     .elementAt(i);
          String SRemark  = (String)VRemark   .elementAt(i);
          String SQty     = common.getRound((String)VRDCQty   .elementAt(i),0);
          String SRecQty  = common.getRound((String)VRecQty   .elementAt(i),0);
          String SPending = common.getRound((String)VPending  .elementAt(i),0);
          String SDueDate = common.parseDate((String)VDueDate  .elementAt(i));
          String SDelay   = (String)RowData[i][10];               
          String SName    = (String)VSupName.elementAt(i);
          String SThro    = (String)VThro.elementAt(i);
          String str="";
          if(!bBlank)
          {
               str = common.Pad(SDate,10)+common.Space(3)+common.Rad(SNo,10)+common.Space(3)+
                            common.Pad(SName,30)+common.Space(3)+  
                            common.Pad(SDesc,40)+common.Space(3)+common.Pad(SRemark,15)+common.Space(3)+
                            common.Pad(SThro,15)+common.Space(3)+  
                            common.Rad(SQty,6)+common.Space(3)+common.Rad(SRecQty,6)+common.Space(3)+
                            common.Rad(SPending,6)+common.Space(3)+common.Pad(SDueDate,10)+common.Space(3)+
                            common.Rad(SDelay,7);
          }
          else
          {
               str = common.Pad("",10)+common.Space(3)+common.Rad("",10)+common.Space(3)+
                            common.Pad("",30)+common.Space(3)+  
                            common.Pad(SDesc,40)+common.Space(3)+common.Pad(SRemark,15)+common.Space(3)+
                            common.Pad(SThro,15)+common.Space(3)+  
                            common.Rad(SQty,6)+common.Space(3)+common.Rad(SRecQty,6)+common.Space(3)+
                            common.Rad(SPending,6)+common.Space(3)+common.Pad(SDueDate,10)+common.Space(3)+
                            common.Rad(SDelay,7);
          }

          FW.write(str+"\n");
          Lctr++;
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               boolean bflag = setDataIntoVector();
               if(!bflag)
                    return;
               setVectorIntoRowData();
               try
               {
                    MiddlePanel.remove(tabreport);
                    MiddlePanel.updateUI();
               }
               catch(Exception ex){}
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               MiddlePanel.add("Center",tabreport);
               MiddlePanel.updateUI();
               BPrint.setEnabled(true);
               TFile.setEditable(true);
          }
     }
     private boolean setDataIntoVector()
     {
          boolean bflag = false;
          VRDCDate  = new Vector();
          VRDCNo    = new Vector();
          VSupName  = new Vector();
          VDesc     = new Vector();
          VRemark   = new Vector();
          VThro     = new Vector();
          VRDCQty   = new Vector();
          VRecQty   = new Vector();
          VPending  = new Vector();
          VDueDate  = new Vector();
          VAcCode   = new Vector();

          String SDate = TDate.toNormal();

          String QS = " Select RDC.RDCDate,RDC.RDCNo,"+SSupTable+".Name,Descript, "+
                      " RDC.Remarks,RDC.Thro,RDC.Qty,RDC.RecQty,RDC.Qty-RDC.RecQty,RDC.DueDate,RDC.Sup_Code "+
                      " From RDC Inner Join "+SSupTable+" On "+SSupTable+".Ac_Code = RDC.Sup_Code "+
                      " Where RDC.RDCDate<='"+SDate+"' And RDC.RecQty < RDC.Qty "+
                      " And RDC.MillCode="+iMillCode+
                      " And RDC.AssetFlag=0  and DocType=0  ";


               if(!(JCAsset.getSelectedItem()).equals("ALL"))
                    QS = QS + " and RDC.GodownAsset="+JCAsset.getSelectedIndex();


               QS = QS + " Order By 1,2 ";

           try
           {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    VRDCDate  .addElement(""+res.getString(1));
                    VRDCNo    .addElement(""+res.getString(2));
                    VSupName  .addElement(""+res.getString(3));
                    VDesc     .addElement(""+res.getString(4));
                    VRemark   .addElement(""+res.getString(5));
                    VThro     .addElement(""+res.getString(6));
                    VRDCQty   .addElement(""+res.getString(7));
                    VRecQty   .addElement(""+res.getString(8));
                    VPending  .addElement(""+res.getString(9));
                    VDueDate  .addElement(""+res.getString(10));
                    VAcCode   .addElement(""+res.getString(11));
               }
               res.close();
               stat.close();
               bflag = true;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bflag = false;
          }
          return bflag;
     }
     private void setVectorIntoRowData()
     {
          RowData = new Object[VRDCNo.size()][11];
          String SDueDate="",SDelay="";
          for(int i=0;i<VRDCDate.size();i++)
          {
               RowData[i][0]  = common.parseDate((String)VRDCDate  .elementAt(i));
               RowData[i][1]  = (String)VRDCNo    .elementAt(i);
               RowData[i][2]  = (String)VSupName  .elementAt(i);
               RowData[i][3]  = (String)VDesc     .elementAt(i);
               RowData[i][4]  = (String)VRemark   .elementAt(i);
               RowData[i][5]  = (String)VThro     .elementAt(i);
               RowData[i][6]  = (String)VRDCQty   .elementAt(i);
               RowData[i][7]  = (String)VRecQty   .elementAt(i);
               RowData[i][8]  = (String)VPending  .elementAt(i);
               try
               {
                    SDueDate = common.parseDate((String)VDueDate  .elementAt(i));
               }
               catch(Exception ex)
               {
                    SDueDate = "";
               }
               try
               {
                    SDelay  = common.getDateDiff(TDate.toString(),common.parseDate((String)VRDCDate.elementAt(i)));
               }
               catch(Exception ex)
               {
                    SDelay = "";
               }
               RowData[i][9]  = SDueDate;
               RowData[i][10] = SDelay;
          }
     }
}
