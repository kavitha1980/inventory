package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCInFrame extends JInternalFrame
{
     TabReport tabreport;
    
     Object RowData[][];
     String ColumnData[] = {"GI No","Date","Supplier Name","Invoice No","Invoice Date","DC No","DC Date"};
     String ColumnType[] = {"N"    ,"S"   ,"S"            ,"S"         ,"S"           ,"S"    ,"S"      };

     Common common   = new Common();
     ORAConnection connect;
     Connection theconnect;

     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     int iUserCode,iMillCode;
     String SSupTable,SYearCode;

     Vector         VGINo,VGIDate,VSupName,VDCNo,VDCDate,VInvNo,VInvDate;

     RDCInFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable,String SYearCode)
     {
          super("RDC Materials Received @ Gate But Not Accounted In Stores");
          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {

     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
     }

     public void addComponents()
     {
          setDataIntoVector();
          setRowData();

          try
          {
             getContentPane().remove(tabreport);
          }
          catch(Exception ex){}

          try
          {
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.ReportTable.addKeyListener(new KeyList(this));
               getContentPane().add(tabreport,BorderLayout.CENTER);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void addListeners()
     {

     }

     public void setDataIntoVector()
     {
          String QString = " SELECT GateInwardRDC.GINo,GateInwardRDC.GIDate,"+SSupTable+".Name,GateInwardRDC.DcNo,GateInwardRDC.DCDate,GateInwardRDC.InvNo,GateInwardRDC.InvDate "+
                           " From (GateInwardRDC Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = GateInwardRDC.Sup_Code) "+
                           " Group By GateInwardRDC.GINo,GateInwardRDC.GIDate,"+SSupTable+".Name,GateInwardRDC.DcNo,GateInwardRDC.DCDate,GateInwardRDC.InvNo,GateInwardRDC.InvDate,GateInwardRDC.MillCode,GateInwardRDC.GrnNo "+
                           " Having GateInwardRDC.GrnNo = 0 and GateInwardRDC.MillCode="+iMillCode;
                              
          VGINo    = new Vector();
          VGIDate  = new Vector();
          VSupName = new Vector();
          VDCNo    = new Vector();
          VDCDate  = new Vector();
          VInvNo   = new Vector();
          VInvDate = new Vector();
          
          try
          {
               
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res  = stat.executeQuery(QString);

               while (res.next())
               {
                    VGINo    .addElement( common.parseNull(res.getString(1)));
                    VGIDate  .addElement( common.parseNull(common.parseDate(res.getString(2))));
                    VSupName .addElement( common.parseNull(res.getString(3)));
                    VDCNo    .addElement( common.parseNull(res.getString(4)));
                    VDCDate  .addElement( common.parseNull(common.parseDate(res.getString(5))));
                    VInvNo   .addElement( common.parseNull(res.getString(6)));
                    VInvDate .addElement( common.parseNull(common.parseDate(res.getString(7))));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {

          RowData     = new Object[VGINo.size()][ColumnData.length];
          for(int i=0;i<VGINo.size();i++)
          {
               RowData[i][0]  = (String)VGINo     .elementAt(i);
               RowData[i][1]  = (String)VGIDate   .elementAt(i);
               RowData[i][2]  = (String)VSupName  .elementAt(i);
               RowData[i][3]  = (String)VInvNo    .elementAt(i);
               RowData[i][4]  = (String)VInvDate  .elementAt(i);
               RowData[i][5]  = (String)VDCNo     .elementAt(i);
               RowData[i][6]  = (String)VDCDate   .elementAt(i);
          }  
     }

     public class KeyList extends KeyAdapter
     {
          RDCInFrame rdcinframe;

          public KeyList(RDCInFrame rdcinframe)
          {
               this.rdcinframe = rdcinframe;
          }

          public void keyPressed(KeyEvent ke)
          {
               if (ke.getKeyCode()==10)
               {
                    int i = tabreport.ReportTable.getSelectedRow();
                    String SIndex = String.valueOf(i);
                    RDCCollectionFrame rdccollectionframe = new RDCCollectionFrame(DeskTop,SPanel,SIndex,tabreport.ReportTable,iUserCode,iMillCode,rdcinframe,SYearCode);
                    DeskTop.add(rdccollectionframe);
                    rdccollectionframe.show();
                    rdccollectionframe.moveToFront();
                    DeskTop.repaint();
                    DeskTop.updateUI();
               }
          } 
     }

}
