package RDC;

import java.io.*;
import util.*;
import java.util.*;

public class RDCInstruction
{
     String SRdcNo,SRdcDate,SRefNo,SRefDate,SInwNo,SInwDate,SBillNo,SBillDate,SThro,SPerson,SName,SAddress1,SAddress2,SAddress3,SPlace,SGiNo,SGiDate,SDocType,SMBDNo,SUom,SMBDDate;
     Vector VDescript,VQty,VRemarks;
     Common common = new Common();

     public RDCInstruction(String SRdcNo,String SRdcDate,String SRefNo,String SRefDate,String SInwNo,String SInwDate,String SBillNo,String SBillDate,String SThro,String SPerson,String SName,String SAddress1,String SAddress2,String SAddress3,String SPlace,String SGiNo,String SGiDate,String SDocType,String SMBDNo,String SUom,String SMBDDate)
     {                   
          this.SRdcNo     = SRdcNo;
          this.SRdcDate   = SRdcDate;
          this.SRefNo     = SRefNo;
          this.SRefDate   = SRefDate;
          this.SInwNo     = SInwNo;
          this.SInwDate   = SInwDate;
          this.SBillNo    = SBillNo;
          this.SBillDate  = SBillDate;
          this.SThro      = SThro;
          this.SPerson    = SPerson;
          this.SName      = SName;
          this.SAddress1  = SAddress1;
          this.SAddress2  = SAddress2;
          this.SAddress3  = SAddress3;
          this.SPlace     = SPlace;
          this.SGiNo      = SGiNo;
          this.SGiDate    = SGiDate;
          this.SDocType   = SDocType;
          this.SMBDNo     = SMBDNo;
          this.SUom       = SUom;
          this.SMBDDate   = SMBDDate;

          VDescript       = new Vector();
          VQty            = new Vector();
          VRemarks        = new Vector();
     }           
     public void append(String SDescript,double dQty,String SRemarks)
     {
          VDescript  .addElement(SDescript);
          VQty       .addElement(String.valueOf(dQty));
          VRemarks   .addElement(SRemarks);
     }
}
