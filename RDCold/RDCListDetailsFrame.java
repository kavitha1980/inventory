package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCListDetailsFrame extends JInternalFrame
{

     RDCCritPanel   TopPanel;
     JPanel         BottomPanel;

     JComboBox      JCStatus;
     DateField      TStDate,TEnDate;
     JButton        BApply;
     JButton        BPrint;
     JTextField     TFile;

     TabReport      tabreport;
     Object         RowData[][];
          
     String ColumnData[] = {"Rdc No","Rdc Date","Sent Through","Supplier Name","Description of Goods","Send Qty","Received Qty","Balance Qty","DC No","DC Date","GI No","GI Date","Purpose","Delay Days"};
     String ColumnType[] = {"N"     ,"S"       ,"S"           ,"S"            ,"S"                   ,"N"       ,"N"           ,"N"          ,"S"    ,"S"      ,"N"    ,"S"      ,"S"      ,"N"         };
     int iColWidth[]     = {80      ,80        ,100           ,250            ,250                   ,80        ,100           ,100          ,100    ,100      ,100    ,100      ,100      ,100         };
     
     Common common   = new Common();
     ORAConnection connect;
     Connection theconnect;

     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     int            iMillCode;
     String         SSupTable,SMillName;

     Vector         VRDCNo;
     Vector         VRDCDate;
     Vector         VThro;
     Vector         VSupName;
     Vector         VDescript;
     Vector         VQty;
     Vector         VRemarks;
     Vector         VDCNo;
     Vector         VDCDate;
     Vector         VGINo;
     Vector         VGIDate;
     Vector         VDelay;
     Vector         VRecQty;
     Vector         VPending;

     String         SHead1,SHead2,SHead3;

     FileWriter     FW;
     int iLctr = 100,iPctr=0;

     RDCListDetailsFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iMillCode,String SSupTable,String SMillName)
     {
          super("RDC List");

          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SMillName = SMillName;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel    = new RDCCritPanel(SSupTable);
          BottomPanel = new JPanel();

          JCStatus    = new JComboBox();
          TStDate     = new DateField();
          TEnDate     = new DateField();
          TFile       = new JTextField();

          BApply      = new JButton("Apply");
          BPrint      = new JButton("Print");

          TStDate.setTodayDate();
          TEnDate.setTodayDate();

          TFile.setText("RDCListDetails.prn");
          TFile.setEditable(false);
          BPrint.setEnabled(false);

          BApply.setMnemonic('A');
          BPrint.setMnemonic('P');
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
     }

     public void addComponents()
     {
          JCStatus.addItem("All");
          JCStatus.addItem("Received");
          JCStatus.addItem("Not Received");

          BottomPanel.add(BPrint);
          BottomPanel.add(TFile);

          getContentPane().add(TopPanel,BorderLayout.NORTH);

          setDataIntoVector();
          setRowData();

          try
          {
               getContentPane().remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.setPrefferedColumnWidth1(iColWidth);

               getContentPane().add(tabreport,BorderLayout.CENTER);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          TopPanel.BApply.addActionListener(new AppList());
          BPrint.addActionListener(new PrintList());
     }

     public class AppList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==TopPanel.BApply)
               {
                    setDataIntoVector();
                    setRowData();
          
                    try
                    {
                         getContentPane().remove(tabreport);
                    }
                    catch(Exception ex){}
                    try
                    {
                         tabreport = new TabReport(RowData,ColumnData,ColumnType);
                         tabreport.setPrefferedColumnWidth1(iColWidth);
          
                         getContentPane().add(tabreport,BorderLayout.CENTER);
                         DeskTop.repaint();
                         DeskTop.updateUI();
                         BPrint.setEnabled(true);
                         TFile.setEditable(true);
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }     
          }
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BPrint)
               {
                    try
                    {
                         iLctr = 100;
                         iPctr=0;

                         String SFile = TFile.getText();
                         if((SFile.trim()).length()==0)
                              SFile = "RDCDetails.prn";

                         FW      = new FileWriter(common.getPrintPath()+SFile);
                         setBody();
                         FW.close();
                    }
                    catch(Exception ex)
                    {
                          System.out.println(ex);
                    }
                    removeHelpFrame();
               }     
          }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();

          }
          catch(Exception ex){}
     }

     public void setDataIntoVector()
     {

          String SStDate = TopPanel.TStDate.toNormal();
          String SEnDate = TopPanel.TEnDate.toNormal();
          String SSupplier="";
          int iList=0;
          String QS = " SELECT RDC.RDCNo, RDC.RDCDate, RDC.Thro, "+SSupTable+".Name, "+ 
                      " RDC.Descript, RDC.Qty, RDC.Remarks, GateInwardRDC.DCNo, "+
                      " GateInwardRDC.DCDate, GateInwardRDC.GINo, GateInwardRDC.GIDate, "+
                      " RDC.RecQty, RDC.Qty - RDC.RecQty AS Pending "+
                      " FROM (RDC INNER JOIN "+SSupTable+" ON "+SSupTable+".Ac_Code = RDC.Sup_Code) "+ 
                      " LEFT JOIN GateInwardRDC ON ((RDC.Descript = GateInwardRDC.Descript) "+
                      " AND (RDC.RDCNo = GateInwardRDC.RDCNo) And (RDC.MillCode = GateInwardRDC.MillCode) "+
                      " And RDC.DocType=0) "+
                      " Where RDC.MillCode="+iMillCode+" And RDC.DocType=0 ";

          if(TopPanel.JRPeriod.isSelected())
          {
               QS = QS +  " And RDC.RDCDate > '"+SStDate+"' and RDC.RDCDate < '"+SEnDate+"' ";
          }
          else
          {
               QS = QS +  " And RDC.RDCNo >="+TopPanel.TStNo.getText()+" and RDC.RDCNo <= "+TopPanel.TEnNo.getText() ;
          }
          if(TopPanel.JRSeleSup.isSelected())
          {
               SSupplier= ""+SSupTable+".Ac_Code= '"+(String)TopPanel.VSupCode.elementAt(TopPanel.JCSup.getSelectedIndex())+"'";
               QS = QS +" and "+SSupplier;
          }
          if(TopPanel.JRSeleList.isSelected())
          {
               iList   = TopPanel.JCList.getSelectedIndex();
               if(iList==0)
               {
                    QS = QS + " AND RDC.Qty - RDC.RecQty = 0 ";
               }
               if(iList==1)
               {
                    QS = QS + " AND RDC.Qty - RDC.RecQty > 0 "; 
               }
          }
          if(TopPanel.JCOrder.getSelectedIndex() == 0)
               QS = QS+" Order By RDC.RDCNo,RDC.RDCDate";
          if(TopPanel.JCOrder.getSelectedIndex() == 1)
               QS = QS+" Order By "+SSupTable+".Name,RDC.RDCDate";


          VRDCNo   = new Vector();
          VRDCDate = new Vector();
          VThro    = new Vector();
          VSupName = new Vector();
          VDescript= new Vector();
          VQty     = new Vector();
          VRemarks = new Vector();
          VDCNo    = new Vector();
          VDCDate  = new Vector();
          VGINo    = new Vector();
          VGIDate  = new Vector();
          VDelay   = new Vector();
          VRecQty  = new Vector();
          VPending = new Vector();

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res   = stat.executeQuery(QS);

               while (res.next())
               {
                    VRDCNo   .addElement(common.parseNull(res.getString(1)));

                    String SRDCDate = common.parseDate(res.getString(2));
                    VRDCDate .addElement(SRDCDate);

                    VThro    .addElement(common.parseNull(res.getString(3)));
                    VSupName .addElement(common.parseNull(res.getString(4)));
                    VDescript.addElement(common.parseNull(res.getString(5)));
                    VQty     .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(6))),3));
                    VRemarks .addElement(common.parseNull(res.getString(7)));
                    VDCNo    .addElement(common.parseNull(res.getString(8)));
                    VDCDate  .addElement(common.parseDate(res.getString(9)));
                    VGINo    .addElement(common.parseNull(res.getString(10)));

                    String SGIDate = common.parseDate(res.getString(11));
                    VGIDate  .addElement(SGIDate);

                    String SDelay = "";

                    if(SGIDate.equals(""))
                         SDelay  = "Not Received";
                    else
                         SDelay  = common.getDateDiff(SGIDate,SRDCDate);

                    VDelay   .addElement(SDelay);
                    VRecQty  .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(12))),3));
                    VPending .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(13))),3));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VGINo.size()][ColumnData.length];

          for(int i=0;i<VGINo.size();i++)
          {
               RowData[i][0]  = (String)VRDCNo    .elementAt(i);
               RowData[i][1]  = (String)VRDCDate  .elementAt(i);
               RowData[i][2]  = (String)VThro     .elementAt(i);
               RowData[i][3]  = (String)VSupName  .elementAt(i);
               RowData[i][4]  = (String)VDescript .elementAt(i);
               RowData[i][5]  = (String)VQty      .elementAt(i);
               RowData[i][6]  = (String)VRecQty   .elementAt(i);
               RowData[i][7]  = (String)VPending  .elementAt(i);
               RowData[i][8]  = (String)VDCNo     .elementAt(i);
               RowData[i][9]  = (String)VDCDate   .elementAt(i);
               RowData[i][10] = (String)VGINo     .elementAt(i);
               RowData[i][11] = (String)VGIDate   .elementAt(i);
               RowData[i][12] = (String)VRemarks  .elementAt(i);
               RowData[i][13] = (String)VDelay    .elementAt(i);
          }
     }

     private void setHead() throws Exception
     {
          SHead1 =  common.Pad("RDC No"                ,12)+common.Space(2)+
                    common.Pad("RDC Date"              ,12)+common.Space(2)+
                    common.Pad("Sent Through"          ,12)+common.Space(2)+
                    common.Pad("Supplier"              ,25)+common.Space(2)+
                    common.Pad("Material Description"  ,25)+common.Space(2)+
                    common.Rad("Send Qty"              ,12)+common.Space(2)+
                    common.Rad("Recd Qty"              ,12)+common.Space(2)+
                    common.Rad("Balance Qty"           ,12)+common.Space(2)+
                    common.Pad("DC No."                ,12)+common.Space(2)+
                    common.Pad("DC Date"               ,12)+common.Space(2)+
                    common.Pad("GI No"                 ,12)+common.Space(2)+
                    common.Pad("GI Date"               ,12)+common.Space(2)+
                    common.Pad("Purpose"               ,12)+common.Space(2)+
                    common.Rad("Delay Days"            ,12)+common.Space(2);

          SHead2 =  common.Rad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",25)+common.Space(2)+
                    common.Pad("",25)+common.Space(2)+
                    common.Rad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Rad("",12)+common.Space(2)+
                    common.Rad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",25)+common.Space(2);

          SHead3 = common.Replicate("-",SHead1.length());

          if(iLctr < 60)
               return ;
          if(iPctr > 0)
          {     
               FW.write(SHead3+"\n");
               FW.write("\n");
          }
          iPctr++;

          String SStDate = TopPanel.TStDate.toNormal();
          String SEnDate = TopPanel.TEnDate.toNormal();

          String str1 = "Company : "+SMillName;
          String str2 = "Document : Returnable Material Cum Receipt (Both Returnable & Service) ";
          String str3 = "Period   : "+common.parseDate(SStDate)+"-"+common.parseDate(SEnDate);
          String str4 = "Page     : "+iPctr+"";

          FW.write("P"+str1+"\n");
          FW.write(str2+"\n");
          FW.write(str3+"\n");
          FW.write(str4+"\n");
          FW.write(SHead3+"\n");
          FW.write(SHead1+"\n");
          FW.write(SHead2+"\n");
          FW.write(SHead3+"\n");

          iLctr = 8;
     }      

     private void setBody() throws Exception
     {
          for(int i=0;i<VRDCNo.size();i++)
          {
               setHead();

               RowData[i][0]  = (String)VRDCNo    .elementAt(i);
               RowData[i][1]  = (String)VRDCDate  .elementAt(i);
               RowData[i][2]  = (String)VThro     .elementAt(i);
               RowData[i][3]  = (String)VSupName  .elementAt(i);
               RowData[i][4]  = (String)VDescript .elementAt(i);
               RowData[i][5]  = (String)VQty      .elementAt(i);
               RowData[i][6]  = (String)VRecQty   .elementAt(i);
               RowData[i][7]  = (String)VPending  .elementAt(i);
               RowData[i][8]  = (String)VDCNo     .elementAt(i);
               RowData[i][9]  = (String)VDCDate   .elementAt(i);
               RowData[i][10] = (String)VGINo     .elementAt(i);
               RowData[i][11] = (String)VGIDate   .elementAt(i);
               RowData[i][12] = (String)VRemarks  .elementAt(i);
               RowData[i][13] = (String)VDelay    .elementAt(i);

               String str = common.Pad((String)VRDCNo       .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VRDCDate     .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VThro        .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VSupName     .elementAt(i),25)+common.Space(2)+
                            common.Pad((String)VDescript    .elementAt(i),25)+common.Space(2)+
                            common.Rad((String)VQty         .elementAt(i),12)+common.Space(2)+
                            common.Rad((String)VRecQty      .elementAt(i),12)+common.Space(2)+
                            common.Rad((String)VPending     .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VDCNo        .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VDCDate      .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VGINo        .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VGIDate      .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VRemarks     .elementAt(i),12)+common.Space(2)+
                            common.Rad((String)VDelay       .elementAt(i),12)+common.Space(2);

               FW.write(str+"\n");
               iLctr++;
          }
          FW.write(SHead3+"\n");
          String SDateTime = common.getServerDateTime2();
          String SEnd = "Report Taken on "+SDateTime+"\n";
          FW.write(SEnd);
     }
}
