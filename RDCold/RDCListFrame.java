package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCListFrame extends JInternalFrame
{

     RDCAllCritPanel TopPanel;
     JPanel          BottomPanel;

     DateField      TStDate,TEnDate;
     JButton        BApply;
     JButton        BPrint;
     JTextField     TFile;

     TabReport      tabreport;
     Object         RowData[][];
          
     String ColumnData[] = {"Doc Type","Rdc Type","Rdc No","Rdc Date","Grn No","Rej No","Sent Through","Supplier Name","Description of Goods","Purpose","Send Qty","Received Qty"};
     String ColumnType[] = {"S"       ,"S"       ,"N"     ,"S"       ,"N"     ,"N"     ,"S"           ,"S"            ,"S"                   ,"S"      ,"N"       ,"N"           };
     int iColWidth[]     = {80        ,80        ,80      ,80        ,80      ,80      ,100           ,250            ,250                   ,100      ,80        ,80            };
     
     Common common   = new Common();
     ORAConnection connect;
     Connection theconnect;

     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     int            iMillCode,iUserCode;
     String         SSupTable,SYearCode,SMillName;

     Vector         VDocType;
     Vector         VRDCType;
     Vector         VRDCNo;
     Vector         VRDCDate;
     Vector         VThro;
     Vector         VSupName;
     Vector         VDescript;
     Vector         VQty;
     Vector         VRemarks;
     Vector         VRecQty;
     Vector         VAuth;
     Vector         VGrnNo,VRejNo;
     Vector         VDocTypeCode,VRDCTypeCode;

     String         SHead1,SHead2,SHead3;

     int iList=-1;

     FileWriter     FW;
     int iLctr = 100,iPctr=0;

     RDCListFrame(JLayeredPane DeskTop,StatusPanel SPanel,int iUserCode,int iMillCode,String SSupTable,String SYearCode,String SMillName)
     {
          super("RDC List");

          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;
          this.SMillName = SMillName;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setTabReport();
     }

     public void createComponents()
     {
          TopPanel    = new RDCAllCritPanel(iMillCode,SSupTable);
          BottomPanel = new JPanel();

          TStDate     = new DateField();
          TEnDate     = new DateField();
          TFile       = new JTextField();

          BApply      = new JButton("Apply");
          BPrint      = new JButton("Print");

          TStDate.setTodayDate();
          TEnDate.setTodayDate();

          TFile.setText("RDCListDetails.prn");
          TFile.setEditable(false);
          BPrint.setEnabled(false);

          BApply.setMnemonic('A');
          BPrint.setMnemonic('P');
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
     }

     public void addComponents()
     {
          BottomPanel.add(BPrint);
          BottomPanel.add(TFile);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          TopPanel.BApply.addActionListener(new AppList());
          BPrint.addActionListener(new PrintList());
     }

     public class AppList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==TopPanel.BApply)
               {
                    setTabReport();
               }     
          }
     }

     public void setTabReport()
     {
          setDataIntoVector();
          setRowData();

          try
          {
               getContentPane().remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               tabreport.setPrefferedColumnWidth1(iColWidth);

               getContentPane().add(tabreport,BorderLayout.CENTER);
               DeskTop.repaint();
               DeskTop.updateUI();
               BPrint.setEnabled(true);
               TFile.setEditable(true);
               tabreport.ReportTable.addKeyListener(new KeyList());
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BPrint)
               {
                    try
                    {
                         iLctr = 100;
                         iPctr=0;

                         String SFile = TFile.getText();
                         if((SFile.trim()).length()==0)
                              SFile = "RDCDetails.prn";

                         FW      = new FileWriter(common.getPrintPath()+SFile);
                         setBody();
                         FW.close();
                    }
                    catch(Exception ex)
                    {
                          System.out.println(ex);
                    }
                    removeHelpFrame();
               }     
          }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();

          }
          catch(Exception ex){}
     }

     public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    BPrint.setEnabled(false);

                    int i = tabreport.ReportTable.getSelectedRow();
                    int iAuth = common.toInt((String)VAuth.elementAt(i));

                    if(iAuth>0)
                    {
                         JOptionPane.showMessageDialog(null,"Material Already Sent","Error",JOptionPane.ERROR_MESSAGE);
                    }
                    else
                    {
                         String SRDCNo   = (String)VRDCNo.elementAt(i);
                         String SDocType = (String)VDocTypeCode.elementAt(i);
                         String SRDCType = (String)VRDCTypeCode.elementAt(i);

                         try
                         {
                              RDCOutFrame rdcoutframe = new RDCOutFrame(DeskTop,iUserCode,iMillCode,true,SSupTable,SYearCode);
                              rdcoutframe.fillData(SRDCNo,SDocType,SRDCType);
                              DeskTop.add(rdcoutframe);
                              DeskTop.repaint();
                              rdcoutframe.setSelected(true);
                              DeskTop.updateUI();
                              rdcoutframe.show();
                         }
                         catch(Exception ex)
                         {
                              System.out.println(ex);
                              ex.printStackTrace();
                         }
                    }
               }
          }
     }

     public void setDataIntoVector()
     {

          String SStDate = TopPanel.TStDate.toNormal();
          String SEnDate = TopPanel.TEnDate.toNormal();
          String SSupplier="";
          iList=-1;
          int iType=0;

          String QS = " SELECT RDC.DocType, RDC.RdcType, RDC.RDCNo, RDC.RDCDate, "+
                      " RDC.Thro, "+SSupTable+".Name, RDC.Descript, RDC.Remarks, "+
                      " RDC.Qty, RDC.RecQty, RDC.GateAuth,nvl(Grn.RejNo,'') as RejNo,nvl(Grn.GrnNo,'') as GrnNo "+
                      " FROM RDC INNER JOIN "+SSupTable+" ON "+SSupTable+".Ac_Code = RDC.Sup_Code "+
                      " Left Join Grn on RDC.GrnId = Grn.Id "+
                      " Where RDC.MillCode = "+iMillCode;

          if(TopPanel.JRPeriod.isSelected())
          {
               QS = QS +  " And RDC.RDCDate >= '"+SStDate+"' and RDC.RDCDate <= '"+SEnDate+"' ";
          }
          else
          {
               QS = QS +  " And RDC.RDCNo >="+TopPanel.TStNo.getText()+" and RDC.RDCNo <= "+TopPanel.TEnNo.getText() ;
          }
          if(TopPanel.JRSeleSup.isSelected())
          {
               SSupplier= ""+SSupTable+".Ac_Code= '"+(String)TopPanel.VSupCode.elementAt(TopPanel.JCSup.getSelectedIndex())+"'";
               QS = QS +" and "+SSupplier;
          }
          if(TopPanel.JRSeleList.isSelected())
          {
               iList   = TopPanel.JCList.getSelectedIndex();
               if(iList==0)
               {
                    QS = QS + " AND RDC.DocType = 0 ";
               }
               if(iList==1)
               {
                    QS = QS + " AND RDC.DocType = 1 "; 
               }
          }
          if(TopPanel.JRSeleType.isSelected())
          {
               iType   = TopPanel.JCType.getSelectedIndex();
               if(iType==0)
               {
                    QS = QS + " AND RDC.RdcType = 0 ";
               }
               if(iType==1)
               {
                    QS = QS + " AND RDC.RdcType = 1 "; 
               }
          }

          String SOrder=(TopPanel.TSort.getText()).trim();
          if(SOrder.length()>0)
                QS = QS+" Order By "+TopPanel.TSort.getText()+",4,3";
          else
                QS = QS+" Order By 4,3";

          VDocType = new Vector();
          VRDCType = new Vector();
          VRDCNo   = new Vector();
          VRDCDate = new Vector();
          VThro    = new Vector();
          VSupName = new Vector();
          VDescript= new Vector();
          VQty     = new Vector();
          VRemarks = new Vector();
          VRecQty  = new Vector();
          VAuth    = new Vector();
          VRejNo   = new Vector();
          VGrnNo   = new Vector();
          VDocTypeCode = new Vector();
          VRDCTypeCode = new Vector();

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res   = stat.executeQuery(QS);

               while (res.next())
               {
                    int iDocType = res.getInt(1);
                    int iRdcType = res.getInt(2);

                    if(iDocType==0)
                    {
                         VDocType.addElement("RDC");
                    }
                    else
                    {
                         VDocType.addElement("NRDC");
                    }

                    if(iRdcType==0)
                    {
                         VRDCType.addElement("Regular");
                    }
                    else
                    {
                         VRDCType.addElement("Non Regular");
                    }

                    VRDCNo   .addElement(common.parseNull(res.getString(3)));

                    String SRDCDate = common.parseDate(res.getString(4));
                    VRDCDate .addElement(SRDCDate);

                    VThro    .addElement(common.parseNull(res.getString(5)));
                    VSupName .addElement(common.parseNull(res.getString(6)));
                    VDescript.addElement(common.parseNull(res.getString(7)));
                    VRemarks .addElement(common.parseNull(res.getString(8)));
                    VQty     .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(9))),3));
                    if(iList==1)
                    {
                         VRecQty  .addElement("");
                    }
                    else
                    {
                         VRecQty  .addElement(common.getRound(common.toDouble(common.parseNull(res.getString(10))),3));
                    }
                    VAuth    .addElement(common.parseNull(res.getString(11)));
                    VRejNo   .addElement(common.parseNull(res.getString(12)));
                    VGrnNo   .addElement(common.parseNull(res.getString(13)));
                    VDocTypeCode.addElement(""+iDocType);
                    VRDCTypeCode.addElement(""+iRdcType);
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VRDCNo.size()][ColumnData.length];

          for(int i=0;i<VRDCNo.size();i++)
          {
               RowData[i][0]  = (String)VDocType  .elementAt(i);
               RowData[i][1]  = (String)VRDCType  .elementAt(i);
               RowData[i][2]  = (String)VRDCNo    .elementAt(i);
               RowData[i][3]  = (String)VRDCDate  .elementAt(i);
               RowData[i][4]  = (String)VGrnNo    .elementAt(i);
               RowData[i][5]  = (String)VRejNo    .elementAt(i);
               RowData[i][6]  = (String)VThro     .elementAt(i);
               RowData[i][7]  = (String)VSupName  .elementAt(i);
               RowData[i][8]  = (String)VDescript .elementAt(i);
               RowData[i][9]  = (String)VRemarks  .elementAt(i);
               RowData[i][10] = (String)VQty      .elementAt(i);
               RowData[i][11] = (String)VRecQty   .elementAt(i);
          }
     }

     private void setHead() throws Exception
     {

          if(iList==1)
          {
               SHead1 =  common.Pad("Doc Type"              ,10)+common.Space(2)+
                         common.Pad("RDC Type"              ,12)+common.Space(2)+
                         common.Pad("RDC No"                ,10)+common.Space(2)+
                         common.Pad("RDC Date"              ,12)+common.Space(2)+
                         common.Pad("GRN No"                ,10)+common.Space(2)+
                         common.Pad("Reje No"               ,10)+common.Space(2)+
                         common.Pad("Sent Through"          ,12)+common.Space(2)+
                         common.Pad("Supplier"              ,40)+common.Space(2)+
                         common.Pad("Material Description"  ,75)+common.Space(2)+
                         common.Pad("Purpose"               ,75)+common.Space(2)+
                         common.Pad("Send Qty"              ,12)+common.Space(2)+
                         common.Pad(""                      ,12)+common.Space(2);
          }
          else
          {
               SHead1 =  common.Pad("Doc Type"              ,10)+common.Space(2)+
                         common.Pad("RDC Type"              ,12)+common.Space(2)+
                         common.Pad("RDC No"                ,10)+common.Space(2)+
                         common.Pad("RDC Date"              ,12)+common.Space(2)+
                         common.Pad("GRN No"                ,10)+common.Space(2)+
                         common.Pad("Reje No"               ,10)+common.Space(2)+
                         common.Pad("Sent Through"          ,12)+common.Space(2)+
                         common.Pad("Supplier"              ,40)+common.Space(2)+
                         common.Pad("Material Description"  ,75)+common.Space(2)+
                         common.Pad("Purpose"               ,75)+common.Space(2)+
                         common.Pad("Send Qty"              ,12)+common.Space(2)+
                         common.Pad("Rec Qty"               ,12)+common.Space(2);
          }

          SHead2 =  common.Rad("",10)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",10)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",10)+common.Space(2)+
                    common.Pad("",10)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",40)+common.Space(2)+
                    common.Pad("",75)+common.Space(2)+
                    common.Rad("",75)+common.Space(2)+
                    common.Pad("",12)+common.Space(2)+
                    common.Pad("",12)+common.Space(2);

          SHead3 = common.Replicate("-",SHead1.length());

          if(iLctr < 60)
               return ;
          if(iPctr > 0)
          {     
               FW.write(SHead3+"\n");
               FW.write("\n");
          }
          iPctr++;

          String SStDate = TopPanel.TStDate.toNormal();
          String SEnDate = TopPanel.TEnDate.toNormal();


          String str1 = "Company : "+SMillName;
          String str2 = "";
          if(iList==0)
          {
               str2 = "Document : RDC List (Returnable) ";
          }
          else
          if(iList==1)
          {
               str2 = "Document : RDC List (Non-Returnable) ";
          }
          else
          {
               str2 = "Document : RDC List ";
          }

          String str3 = "Period   : "+common.parseDate(SStDate)+"-"+common.parseDate(SEnDate);
          String str4 = "Page     : "+iPctr+"g";

          FW.write(str1+"\n");
          FW.write(str2+"\n");
          FW.write(str3+"\n");
          FW.write(str4+"\n");
          FW.write(SHead3+"\n");
          FW.write(SHead1+"\n");
          FW.write(SHead2+"\n");
          FW.write(SHead3+"\n");

          iLctr = 8;
     }      

     private void setBody() throws Exception
     {
          for(int i=0;i<VRDCNo.size();i++)
          {
               setHead();

               String str = common.Pad((String)VDocType     .elementAt(i),10)+common.Space(2)+
                            common.Pad((String)VRDCType     .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VRDCNo       .elementAt(i),10)+common.Space(2)+
                            common.Pad((String)VRDCDate     .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VGrnNo       .elementAt(i),10)+common.Space(2)+
                            common.Pad((String)VRejNo       .elementAt(i),10)+common.Space(2)+
                            common.Pad((String)VThro        .elementAt(i),12)+common.Space(2)+
                            common.Pad((String)VSupName     .elementAt(i),40)+common.Space(2)+
                            common.Pad((String)VDescript    .elementAt(i),75)+common.Space(2)+
                            common.Pad((String)VRemarks     .elementAt(i),75)+common.Space(2)+
                            common.Rad((String)VQty         .elementAt(i),12)+common.Space(2)+
                            common.Rad((String)VRecQty      .elementAt(i),12)+common.Space(2);

               FW.write(str+"\n");
               iLctr++;
          }
          FW.write(SHead3+"\n");
          String SDateTime = common.getServerDateTime2();
          String SEnd = "Report Taken on "+SDateTime+"\n";
          FW.write(SEnd);
     }
}
