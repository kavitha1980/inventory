package RDC;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCMiddlePanel extends JPanel 
{
         Object RowData[][];
         String ColumnData[] = {"RDC No","RDC Date","Description","Pending Qty","Inv/DC Qty","Recd Qty"};
         String ColumnType[] = {"N"     ,"S"       ,"S"          ,"N"          ,"N"         ,"E"       };
            
         JLayeredPane DeskTop;

         Vector VIName,VRDCNo,VDate,VPending,VDCQty;
         Vector VGId,VRId,VRDCSlNo,VGateRdcNo,VGateSlNo;

         TabReport tabreport;
      
         String SSupCode="";

         Common common = new Common();
         ORAConnection connect;
         Connection theconnect;

         public RDCMiddlePanel(JLayeredPane DeskTop)
         {
              this.DeskTop  = DeskTop;

              setLayout(new BorderLayout());
         }
         public void createComponents(String SGINo,int iMillCode)
         {
                  setDataVector(SGINo,iMillCode);
                  setRowData();
                  tabreport = new TabReport(RowData,ColumnData,ColumnType);
                  add(tabreport,BorderLayout.CENTER);
         }

        public void setDataVector(String SGINo,int iMillCode)
        {
            String QS = " SELECT RDC.RDCDate, RDC.RDCNo, "+
                        " RDC.Descript, RDC.Qty-RDC.RecQty ,"+
                        " GateInwardRDC.SupQty, GateInwardRDC.Id, "+
                        " RDC.Id,RDC.slno,GateInwardRDC.RdcNo,GateInwardRDC.RdcSlNo "+
                        " FROM RDC INNER JOIN GateInwardRDC ON "+
                        " RDC.Sup_Code = GateInwardRDC.Sup_Code AND "+
                        " RDC.Descript = GateInwardRDC.Descript AND "+
                        " RDC.MillCode = GateInwardRDC.MillCode "+
                        " WHERE (RDC.Qty-RDC.RecQty)>0 and GateInwardRDC.GINo ="+SGINo+
                        " and GateInwardRDC.MillCode="+iMillCode+
                        " Order By 1,2,3";

            VDate     = new Vector();
            VRDCNo    = new Vector();
            VIName    = new Vector();
            VPending  = new Vector();
            VDCQty    = new Vector();
            VGId      = new Vector();
            VRId      = new Vector();
            VRDCSlNo  = new Vector();
            VGateRdcNo= new Vector();
            VGateSlNo = new Vector();

           try
           {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res  = stat.executeQuery(QS);
               while (res.next())
               {
                    VDate     .addElement(res.getString(1));
                    VRDCNo    .addElement(res.getString(2));
                    VIName    .addElement(res.getString(3));
                    VPending  .addElement(res.getString(4));
                    VDCQty    .addElement(res.getString(5));
                    VGId      .addElement(res.getString(6));
                    VRId      .addElement(res.getString(7));
                    VRDCSlNo  .addElement(res.getString(8));
                    VGateRdcNo.addElement(res.getString(9));
                    VGateSlNo .addElement(res.getString(10));
              }
              res.close();
              stat.close();
           }
           catch(Exception ex)
           {
                System.out.println(ex);
           }
        }
        public boolean setRowData()
        {
            RowData = new Object[VDate.size()][ColumnData.length];

            for(int i=0;i<VDate.size();i++)
            {
                RowData[i][0]  = (String)VRDCNo.elementAt(i);
                RowData[i][1]  = (String)VDate.elementAt(i);
                RowData[i][2]  = (String)VIName.elementAt(i);
                RowData[i][3]  = (String)VPending.elementAt(i);
                RowData[i][4]  = (String)VDCQty.elementAt(i);
                RowData[i][5]  = "0";
            }
            return true;
        }
}         
