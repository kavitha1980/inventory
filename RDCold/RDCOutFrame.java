package RDC;         
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;
import jdbc.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCOutFrame extends JInternalFrame
{
     JPanel  TopPanel,MiddlePanel,BottomPanel,TopLeft,TopRight,TopCenter;

     JComboBox   JCType,JCDocType,JCAsset;
     JTextField  TSupCode;
     DateField   TDate,DRefDate,DInwardDate,DGiDate,DBillDate,DMbdDate;
     JButton     BSupplier;
     MyTextField TSlNo,TSentThro,TRefNo,TInwardNo,TGiNo,TBillNo,TPerson,TMbdNo;

     JButton BOk;

     Object RowData[][];

     String ColumnData[]={"Sl No","Description","Purpose","Uom","Department","Unit","Due Date","Quantity"};
     String ColumnType[]={"N"    ,"S"          ,"S"      ,"S"  ,"S"         ,"S"   ,"S"       ,"N"       };

     TabReport      tabreport;
     Vector         VRDCRec;
     Vector         VDept,VUnit,VDeptCode,VUnitCode,VUom,VUomCode;
     JLayeredPane   Layer;
     int iUserCode,iMillCode;
     boolean bflag;
     String SSupTable,SYearCode;

     String SRDCNo = "",SDate="";
     int iRdc = 0;

     String SRDCDate="",SSupName="",SVehicle="",SPerson="",SMBDNo="";
     String SRefNo="",SRefDate="",SInwNo="",SInwDate="";
     String SGINo="",SGIDate="",SInvNo="",SInvDate="",SMBDDate;
     String SRSupCode="";

     Vector VDesc,VPurpose,VRUom,VRDept,VRUnit,VDueDate,VQty,VSlNo;

     Common   common = new Common();
     ORAConnection connect;
     Connection theconnect;

     RDCOutFrame(JLayeredPane Layer,int iUserCode,int iMillCode,boolean bflag,String SSupTable,String SYearCode)
     {
          this.Layer     = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          this.bflag     = bflag;       // true - for Modification; false - for entry
          this.SSupTable = SSupTable;
          this.SYearCode = SYearCode;

          setParamVectors();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TopPanel       = new JPanel(true);
          TopLeft        = new JPanel();
          TopRight       = new JPanel();
          TopCenter      = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();

          JCType         = new JComboBox();
          JCDocType      = new JComboBox();
          JCAsset        = new JComboBox();
          TSlNo          = new MyTextField(8);
          TSupCode       = new JTextField();
          TDate          = new DateField();
          DRefDate       = new DateField();
          DInwardDate    = new DateField();
          DGiDate        = new DateField();
          DBillDate      = new DateField();
          BSupplier      = new JButton("Select a Workshop/Supplier");
          TSentThro      = new MyTextField(20);
          TRefNo         = new MyTextField(20);
          TInwardNo      = new MyTextField(20);
          TGiNo          = new MyTextField(20);
          TBillNo        = new MyTextField(20);
          TPerson        = new MyTextField(20);
          TMbdNo         = new MyTextField(20);
          DMbdDate       = new DateField();

          BOk            = new JButton("Save & Exit");

          VRDCRec        = new Vector();

          TDate.fromString1(SDate);

          JCType.addItem("Regular");
          JCType.addItem("Non-Regular");

          JCDocType.addItem("Returnable");
          JCDocType.addItem("Non-Returnable");

          JCAsset.addItem("No");
          JCAsset.addItem("Yes");
          JCAsset.setSelectedItem("No");



          if(iMillCode==0)
          {
               JCType.setEnabled(true);
          }
          else
          {
               JCType.setEnabled(false);
          }


          BOk.setMnemonic('S');
          int iDType = JCDocType.getSelectedIndex();
          int iRType = JCType.getSelectedIndex();
          setRDCNo(iDType,iRType);
          TSlNo.setEnabled(false);
     }
     public void setLayouts()
     {
          TopPanel.setLayout(new GridLayout(1,3));
          TopLeft.setLayout(new GridLayout(6,2));
          TopLeft.setBorder(new TitledBorder("RDC Info"));
          TopRight.setLayout(new GridLayout(6,2));
          TopRight.setBorder(new TitledBorder("RDC Info"));
          TopCenter.setLayout(new GridLayout(6,2));
          TopCenter.setBorder(new TitledBorder("RDC Info"));
          BottomPanel.setLayout(new FlowLayout());
          MiddlePanel.setLayout(new BorderLayout());
          TopPanel.setBorder(new TitledBorder("Info"));
          MiddlePanel.setBorder(new TitledBorder("RDC List"));

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,790,500);
          setTitle("RDC for Machinery Spare Parts being sent out for Repairs and Maintenance");
     }
     public void addComponents()
     {
          TopLeft.add(new MyLabel("Document Type"));
          TopLeft.add(JCDocType);

          TopLeft.add(new MyLabel("RDC Type"));
          TopLeft.add(JCType);

          TopLeft.add(new MyLabel("Asset"));
          TopLeft.add(JCAsset);

          TopLeft.add(new MyLabel("RDC Sl No"));
          TopLeft.add(TSlNo);

          TopLeft.add(new MyLabel("RDC Date"));
          TopLeft.add(TDate);

          TopLeft.add(new MyLabel("Workshop/Lathe/Supplier"));
          TopLeft.add(BSupplier);

          TopCenter.add(new MyLabel("Vehicle to Sent Thro"));
          TopCenter.add(TSentThro);

          TopCenter.add(new MyLabel("Name of the person"));
          TopCenter.add(TPerson);

          TopCenter.add(new MyLabel("MBD Number"));
          TopCenter.add(TMbdNo);

          TopCenter.add(new MyLabel("MBD Date"));
          TopCenter.add(DMbdDate);

          TopCenter.add(new MyLabel("Ref.No"));
          TopCenter.add(TRefNo);

          TopCenter.add(new MyLabel("Ref.Date"));
          TopCenter.add(DRefDate);

          TopRight.add(new MyLabel("InwardNo"));
          TopRight.add(TInwardNo);

          TopRight.add(new MyLabel("InwardDate"));
          TopRight.add(DInwardDate);

          TopRight.add(new MyLabel("GI No"));
          TopRight.add(TGiNo);

          TopRight.add(new MyLabel("GI Date"));
          TopRight.add(DGiDate);

          TopRight.add(new MyLabel("Bill No"));
          TopRight.add(TBillNo);

          TopRight.add(new MyLabel("Bill Date"));
          TopRight.add(DBillDate);

          TopPanel.add(TopLeft);
          TopPanel.add(TopCenter);
          TopPanel.add(TopRight);

          BottomPanel.add(BOk);
          RowData = new Object[30][ColumnData.length];

          for(int i=0;i<30;i++)
          {
               for(int j=0;j<ColumnData.length;j++)
               {
                    RowData[i][j]="";
               }
               VRDCRec.addElement(new RDCRec(Layer,VUom,VDept,VUnit,VDeptCode,VUnitCode,RowData,String.valueOf(i),this,TDate.toNormal()));
               RowData[i][0]=""+(i+1);
          }

          MiddlePanel.add("Center",tabreport=new TabReport(RowData,ColumnData,ColumnType));

          getContentPane() .add("North",TopPanel);
          getContentPane() .add("Center",MiddlePanel);
          getContentPane() .add("South",BottomPanel);
     }

     public void addListeners()
     {
          BSupplier .addActionListener(new SupplierSearch(Layer,TSupCode,SSupTable));
          tabreport .ReportTable.addKeyListener(new KeyList());
          BOk       .addActionListener(new ActList());
          JCDocType .addItemListener(new ItemList());
          JCType    .addItemListener(new ItemList());
     }
     private class ItemList implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
               if(ie.getSource()==JCDocType)
               {
                    try
                    {
                         if(iMillCode==0)
                         {
                              if((JCDocType.getSelectedIndex())==1)
                              {
                                   JCType.setSelectedIndex(0);
                                   JCType.setEnabled(false);
                              }
                              else
                              {
                                   JCType.setEnabled(true);
                              }
                         }
                         else
                         {
                              JCType.setSelectedIndex(0);
                              JCType.setEnabled(false);
                         }
                    }
                    catch(Exception e)
                    {
                         System.out.println(e);
                         e.printStackTrace();
                    }
               }
               if(ie.getSource()==JCDocType || ie.getSource()==JCType)
               {
                    int iDocType = JCDocType.getSelectedIndex();
                    int iRdcType = JCType.getSelectedIndex();
                    setRDCNo(iDocType,iRdcType);
               }



          }


     }
     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk.setEnabled(false);
               boolean bSig = false;
               try
               {
                     bSig = allOk();
               }
               catch(Exception ex)
               {
                     System.out.println(ex);
                     ex.printStackTrace();
               }
               if(bSig)
               {
                    if(bflag)
                    {
                         int iDocType  = JCDocType.getSelectedIndex();
                         int iRdcType  = JCType.getSelectedIndex();
                         String SRDCNo = TSlNo.getText();
                         deleteOldRDC(SRDCNo,iDocType,iRdcType);
                         insertRDC();
                         removeHelpFrame();
                    }
                    else
                    {
                         int iDocType = JCDocType.getSelectedIndex();
                         int iRdcType = JCType.getSelectedIndex();
                         setRDCNo(iDocType,iRdcType);
                         insertRDC();
                         updateOrderNo(iDocType,iRdcType);
                         removeHelpFrame();
                    }
               }
               else
               {
                     BOk.setEnabled(true);
               }
          }
     }
     public boolean allOk()
     {
          String SSupCode     = TSupCode.getText();
          String SDate        = TDate.toString();
          String SCurDate     = common.getServerDate();
          int    iDateDiff    = common.toInt(common.getDateDiff(SDate,SCurDate));
          int    iDueDate     = 0;
          int    iRDCDate     = common.toInt(TDate.toNormal());

          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
          {
              iDueDate     = common.toInt(common.pureDate((String)tabreport.ReportTable.getValueAt(i,6)));
              if(iDueDate!=iRDCDate)
              {
                   if(common.toDouble((String)tabreport.ReportTable.getValueAt(i,7))==0)
                        continue;
                   if(iDueDate<=iRDCDate)
                   {
                         JOptionPane.showMessageDialog(null,"Invalid Date","Error",JOptionPane.ERROR_MESSAGE);
                         TDate.TDay.requestFocus();
                         return false;
                   }
              }
          }
          if((TSlNo.getText().trim()).length()==0)
          {
               JOptionPane.showMessageDialog(null,"RDC Slip No Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TSlNo.requestFocus();
               return false;
          }
          if(SSupCode.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Select Supplier","Error",JOptionPane.ERROR_MESSAGE);
               BSupplier.setEnabled(true);
               BSupplier.requestFocus();
               return false;
          }
          if((TSentThro.getText().trim()).length()==0)
          {
               JOptionPane.showMessageDialog(null,"Sent Thro Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TSentThro.requestFocus();
               return false;
          }

          return true;
     }

     private void deleteOldRDC(String SRDCNo,int iDocType,int iRdcType)
     {
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();

               String QS1 = " Insert into ModifiedRDC Select * from RDC "+
                            " Where RDCNo="+SRDCNo+" And RDCType="+iRdcType+
                            " And DocType="+iDocType+" And MillCode="+iMillCode;


               stat.execute(QS1);

               String QS2 = " Delete from RDC "+
                            " Where RDCNo="+SRDCNo+" And RDCType="+iRdcType+
                            " And DocType="+iDocType+" And MillCode="+iMillCode;

               stat.execute(QS2);
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void insertRDC()
     {
         String SDescription = "";
         String SRemarks     = "";
         String SUnitName    = "";
         String SUomName     = "";
         String SDeptName    = "";
         String SDueDate     = "";
         double dQty         = 0;
         int    iSlNo        = 0;

         for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
         {
              iSlNo          = common.toInt((String)tabreport.ReportTable.getValueAt(i,0));
              SDescription   = (String)tabreport.ReportTable.getValueAt(i,1);
              SRemarks       = (String)tabreport.ReportTable.getValueAt(i,2);
              SUnitName      = (String)tabreport.ReportTable.getValueAt(i,5);
              int iUnitCode  =  getUnitCode(SUnitName);
              SUomName       = (String)tabreport.ReportTable.getValueAt(i,3);
              int iUomCode   =  getUomCode(SUomName);
              SDeptName      = (String)tabreport.ReportTable.getValueAt(i,4);
              int iDeptCode  =  getDeptCode(SDeptName);
              SDueDate       = common.pureDate((String)tabreport.ReportTable.getValueAt(i,6));
              dQty           = common.toDouble((String)tabreport.ReportTable.getValueAt(i,7));

              if(dQty<=0)
                   continue;
              insertData(SDescription,SRemarks,iUnitCode,iUomCode,iDeptCode,SDueDate,dQty,iSlNo);
         }
    }
    public void insertData(String SDescription,String SRemarks,int iUnitCode,int iUomCode,int iDeptCode,String SDueDate,double dQty,int iSlNo)
    {
         try
         {
              if(theconnect==null)
              {
                   connect=ORAConnection.getORAConnection();
                   theconnect=connect.getConnection();
              }
              String Qry=" insert into RDC(ID,RDCType,RDCNo,RDCDate,Sup_Code,Descript,Dept_Code,Unit_Code,Remarks,Qty,DueDate,Thro,RecQty,ValId,SlNo,UserCode,ModiDate,MillCode,AssetFlag,Status,Flag,DocType,RefNo,RefDate,RefInwNo,RefInwDate,RefGiNo,RefGiDate,RefBillNo,RefBillDate,Person,UomCode,MBDNo,MBDDate,GodownAsset) values(RDC_seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
              PreparedStatement prepare=theconnect.prepareStatement(Qry);

              prepare.setInt(1,JCType.getSelectedIndex());
              prepare.setInt(2,common.toInt((String)TSlNo.getText()));
              prepare.setString(3,TDate.toNormal());
              prepare.setString(4,TSupCode.getText());
              prepare.setString(5,SDescription.toUpperCase());
              prepare.setInt(6,iDeptCode);
              prepare.setInt(7,iUnitCode);
              prepare.setString(8,SRemarks.toUpperCase());
              prepare.setDouble(9,dQty);
              prepare.setString(10,SDueDate);
              prepare.setString(11,(TSentThro.getText()).toUpperCase());
              prepare.setDouble(12,0);
              prepare.setInt(13,0);
              prepare.setInt(14,iSlNo);
              prepare.setInt(15,iUserCode);
              prepare.setString(16,SDate);
              prepare.setInt(17,iMillCode);
              prepare.setInt(18,0);
              prepare.setInt(19,0);
              prepare.setInt(20,0);
              prepare.setInt(21,JCDocType.getSelectedIndex());
              prepare.setString(22,(TRefNo.getText()).toUpperCase());
              prepare.setString(23,DRefDate.toNormal());
              prepare.setInt(24,(common.toInt((String)TInwardNo.getText())));
              prepare.setString(25,DInwardDate.toNormal());
              prepare.setInt(26,(common.toInt((String)TGiNo.getText())));
              prepare.setString(27,DGiDate.toNormal());
              prepare.setString(28,(TBillNo.getText()).toUpperCase());
              prepare.setString(29,DBillDate.toNormal());
              prepare.setString(30,(TPerson.getText()).toUpperCase());
              prepare.setInt(31,iUomCode);
              prepare.setString(32,(TMbdNo.getText()).toUpperCase());
              prepare.setString(33,DMbdDate.toNormal());
              prepare.setInt(34,JCAsset.getSelectedIndex());

              prepare.executeUpdate();
              prepare.close();
         }
         catch(Exception e)
         {
              System.out.println(e);
              e.printStackTrace();
         }
     }
     private void updateOrderNo(int iDocType,int iRdcType)
     {
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();

               if(iDocType==0 && iRdcType==0)
               {
                    String QS1 = " Update Config"+iMillCode+""+SYearCode+" Set MaxNo="+(common.toInt((String)TSlNo.getText()))+" Where ID=7 ";
                    stat.executeUpdate(QS1);
               }
               else if(iDocType==0 && iRdcType==1)
               {
                    String QS2 = " Update Config"+iMillCode+""+SYearCode+" Set MaxNo="+(common.toInt((String)TSlNo.getText()))+" Where ID=8 ";
                    stat.executeUpdate(QS2);
               }
               else if(iDocType==1 && iRdcType==0)
               {
                    String QS3 = " Update Config"+iMillCode+""+SYearCode+" Set MaxNo="+(common.toInt((String)TSlNo.getText()))+" Where ID=9 ";
                    stat.executeUpdate(QS3);
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    RDCRec rdcrec=(RDCRec)VRDCRec.elementAt(tabreport.ReportTable.getSelectedRow());
                    rdcrec.setActivation(bflag);
                    rdcrec.onThisFrame();
               }
          }
     }

     public void setRDCNo(int iDocType,int iRdcType)
     {
          try
          {
               String QS = "";

               if(iDocType==0 && iRdcType==0)
               {
                    QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" Where Id=7";
               }
               else if(iDocType==0 && iRdcType==1)
               {
                    QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" Where Id=8";
               }
               else if(iDocType==1 && iRdcType==0)
               {
                    QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" Where Id=9";
               }

               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat   = theconnect.createStatement();
               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    SRDCNo = result.getString(1);
               }
               result.close();
               stat.close();

               int iRDCNo = common.toInt(SRDCNo)+1;
               SRDCNo     = String.valueOf(iRDCNo);
               TSlNo.setText(SRDCNo);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setParamVectors()
     {
          VDept     = new Vector();
          VUnit     = new Vector();
          VUom      = new Vector();

          VDeptCode = new Vector();
          VUnitCode = new Vector();
          VUomCode  = new Vector();

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res  = stat.executeQuery("Select UomName,UomCode From Uom Order By UomCode");
               while(res.next())
               {
                    VUom      .addElement(res.getString(1));
                    VUomCode  .addElement(res.getString(2));
               }
               res.close();
               res  = stat.executeQuery("Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name");
               while(res.next())
               {
                    VDept     .addElement(res.getString(1));
                    VDeptCode .addElement(res.getString(2));
               }
               res.close();
               res  = stat.executeQuery("Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name");
               while(res.next())
               {
                    VUnit     .addElement(res.getString(1));
                    VUnitCode .addElement(res.getString(2));
               }
               res.close();
               res  = stat.executeQuery("Select to_Char(Sysdate,'YYYYMMDD HH24:MI:SS') From Dual");
               while(res.next())
               {
                    SDate =res.getString(1);
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private int getUnitCode(String SUnitName)
     {
          int index= VUnit.indexOf(common.parseNull(SUnitName));

          if(index==-1)
              return 0;
          else
              return (common.toInt((String)VUnitCode.elementAt(index)));
     }
     private int getUomCode(String SUomName)
     {
          int index= VUom.indexOf(common.parseNull(SUomName));

          if(index==-1)
              return 0;
          else
              return (common.toInt((String)VUomCode.elementAt(index)));
     }
     private int getDeptCode(String SMakeName)
     {
          int index= VDept.indexOf(common.parseNull(SMakeName));

          if(index==-1)
              return 0;
          else
              return (common.toInt((String)VDeptCode.elementAt(index)));
     }

     public void fillData(String SRDCNo,String SDocType,String SRDCType)
     {
          getData(SRDCNo,SDocType,SRDCType);
          setData(SRDCNo,SDocType,SRDCType);
     }

     public void getData(String SRDCNo,String SDocType,String SRDCType)
     {
          VDesc    = new Vector();
          VPurpose = new Vector();
          VRUom    = new Vector();
          VRDept   = new Vector();
          VRUnit   = new Vector();
          VDueDate = new Vector();
          VQty     = new Vector();
          VSlNo    = new Vector();

          String QS = " Select RDC.RDCDate,"+SSupTable+".Name,RDC.Thro,RDC.Person, "+
                      " RDC.MBDNo,RDC.RefNo,RDC.RefDate,RDC.RefInwNo,RDC.RefInwDate, "+
                      " RDC.RefGINo,RDC.RefGIDate,RDC.RefBillNo,RDC.RefBillDate, "+
                      " RDC.Descript,RDC.Remarks,Uom.UomName,Dept.Dept_Name, "+
                      " Unit.Unit_Name,RDC.DueDate,RDC.Qty,RDC.SlNo,RDC.Sup_Code,RDC.MBDDate "+
                      " From RDC "+
                      " Inner Join "+SSupTable+" on RDC.Sup_Code = "+SSupTable+".Ac_Code "+
                      " Inner Join Uom on RDC.UomCode = Uom.UomCode "+
                      " Inner Join Dept on RDC.Dept_Code = Dept.Dept_Code "+
                      " Inner Join Unit on RDC.Unit_Code = Unit.Unit_Code "+
                      " Where RDC.RDCNo="+SRDCNo+" And RDC.DocType="+SDocType+
                      " And RDC.RDCType="+SRDCType+" And RDC.MillCode="+iMillCode+
                      " Order by RDC.SlNo ";

          try
          {
                if(theconnect==null)
                {
                     connect=ORAConnection.getORAConnection();
                     theconnect=connect.getConnection();
                }
                Statement stat   = theconnect.createStatement();
                ResultSet result = stat.executeQuery(QS);
                while(result.next())
                {
                        SRDCDate = result.getString(1);
                        SSupName = result.getString(2);
                        SVehicle = result.getString(3);
                        SPerson  = result.getString(4);
                        SMBDNo   = result.getString(5);
                        SRefNo   = result.getString(6);
                        SRefDate = result.getString(7);
                        SInwNo   = result.getString(8);
                        SInwDate = result.getString(9);
                        SGINo    = result.getString(10);
                        SGIDate  = result.getString(11);
                        SInvNo   = result.getString(12);
                        SInvDate = result.getString(13);

                        VDesc.addElement(result.getString(14));
                        VPurpose.addElement(result.getString(15));
                        VRUom.addElement(result.getString(16));
                        VRDept.addElement(result.getString(17));
                        VRUnit.addElement(result.getString(18));
                        VDueDate.addElement(result.getString(19));
                        VQty.addElement(result.getString(20));
                        VSlNo.addElement(result.getString(21));

                        SRSupCode = result.getString(22);
                        SMBDDate  = result.getString(23);
                }
                result.close();
                stat.close();
          }
          catch(Exception ex)
          {
                  System.out.println(ex);           
          }
     }

     public void setData(String SRDCNo,String SDocType,String SRDCType)
     {
          JCDocType.setSelectedIndex(common.toInt(SDocType));
          JCType.setSelectedIndex(common.toInt(SRDCType));
          TSlNo.setText(SRDCNo);
          TDate.fromString1(SRDCDate);
          BSupplier.setText(SSupName);
          TSupCode.setText(SRSupCode);
          TSentThro.setText(SVehicle);
          TPerson.setText(SPerson);
          TMbdNo.setText(SMBDNo);
          DMbdDate.fromString1(SMBDDate);
          TRefNo.setText(SRefNo);
          DRefDate.fromString1(SRefDate);
          TInwardNo.setText(SInwNo);
          DInwardDate.fromString1(SInwDate);
          TGiNo.setText(SGINo);
          DGiDate.fromString1(SGIDate);
          TBillNo.setText(SInvNo);
          DBillDate.fromString1(SInvDate);

          for(int i=0;i<VDesc.size();i++)
          {
               RowData[i][1] = (String)VDesc.elementAt(i);
               RowData[i][2] = (String)VPurpose.elementAt(i);
               RowData[i][3] = (String)VRUom.elementAt(i);
               RowData[i][4] = (String)VRDept.elementAt(i);
               RowData[i][5] = (String)VRUnit.elementAt(i);
               RowData[i][6] = common.parseDate((String)VDueDate.elementAt(i));
               RowData[i][7] = (String)VQty.elementAt(i);
          }
          Layer.updateUI();

          JCDocType.setEnabled(false);
          JCType.setEnabled(false);
          TSlNo.setEditable(false);
     }

}
