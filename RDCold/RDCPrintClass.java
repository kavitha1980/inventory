package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public final class RDCPrintClass
{
     FileWriter FW;
     Vector theVector;
     String SRdcNo="";
     Common common = new Common();
     ORAConnection connect;
     Connection theconnect;
     int iMillCode;
     String SSupTable;
     RDCInstruction rdcInstruction;
     int iLctr = 70,iPctr = 0;

     public RDCPrintClass()
     {
     }
     public void printWithDate(FileWriter FW,String SRdcNo,int iMillCode,String SSupTable)
     {
          try
          {
               this.SRdcNo     = SRdcNo;
               this.FW         = FW;
               this.iMillCode  = iMillCode;
               this.SSupTable  = SSupTable;

               setDataintoVector();
               toPrint();
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }

     private void toPrint()
     {
          toPrtHead();
          toPrtBody();
          toPrtFoot(0);
     }
     private void toPrtHead()
     {
          try
          {
               if(iLctr < 44)
               {
                    return;
               }
               if(iPctr > 0)
               {
                    //FW.write("-------------------------------------------------------------------------------------------------------------------------|"+"\n");
                    //FW.write("                                                                                                       ...contd          "+"\n");
                    toPrtFoot(1);
               }
               iPctr++;

               for(int i=0; i<theVector.size(); i++)
               {
                    rdcInstruction   = (RDCInstruction)theVector.elementAt(i);

                    FW.write("g\n");
                    FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
                    FW.write("|                                                                                                                        |"+"\n");

                    if(iMillCode==0)
                    {
                         FW.write("|                            E AMARJOTHI SPINNING MILLS LIMITED F@g                                                                                                                         |"+"\n");
                         FW.write("|E"+common.Cad("Pudusuripalayam, Nambiyur - 638 458. ",120)+"F|"+"\n");
                         FW.write("|E"+common.Cad("Gobi Taluk, Erode(Dt.). ",120)+"F|"+"\n");
                         FW.write("|                                                                                                                        |"+"\n");
                         FW.write("|  ETIN : 33632960864F                                                               CST    : 440691 dt.24.9.1990          |"+"\n");
                         FW.write("|  Eph  : 04285 - 267 201, 267 301F                                                  E-mail : ajsmmill@yahoo.co.in         |"+"\n");
                         FW.write("|  EFax : 04285 - 267 565F                                                                    purchase@amarjothi.net       |"+"\n");
                         FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");

                         if(rdcInstruction.SDocType.startsWith("RETU"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                         else if(rdcInstruction.SDocType.startsWith("NON"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                    }
                    if(iMillCode==1)
                    {
                         FW.write("|                   E AMARJOTHI SPINNING MILLS (DYEING DIVISION) F@g                                                                                                                         |"+"\n");
                         FW.write("|E"+common.Cad("SIPCOT INDUSTRIAL AREA",120)+"F|"+"\n");
                         FW.write("|E"+common.Cad("PERUNDURAI  -  638 052",120)+"F|"+"\n");
                         FW.write("|                                                                                                                        |"+"\n");
                         FW.write("|  ETIN : 33632960864F                                                              CST    : 440691 dt.24.09.1990          |"+"\n");
                         FW.write("|  Eph  : 04294 - 309339, 267 301F                                                  E-mail : ajsmmill@yahoo.co.in          |"+"\n");
                         FW.write("|  EFax : 04294 - 230392F                                                                    purchase@amarjothi.net        |"+"\n");
                         FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");

                         if(rdcInstruction.SDocType.startsWith("RETU"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                         else if(rdcInstruction.SDocType.startsWith("NON"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                    }
                    if(iMillCode==3)
                    {
                         FW.write("|            E AMARJOTHI COLOUR MELANGE SPINNING MILLS PVT LIMITED F@g                                                                                                                         |"+"\n");
                         FW.write("|E"+common.Cad("Pudusuripalayam, Nambiyur - 638 458. ",120)+"F|"+"\n");
                         FW.write("|E"+common.Cad("Gobi Taluk, Erode(Dt.). ",120)+"F|"+"\n");
                         FW.write("|                                                                                                                        |"+"\n");
                         FW.write("|  ETIN : 33712404816F                                                               CST    : 1031365  dt.05.05.2010       |"+"\n");
                         FW.write("|  Eph  : 04285 - 267 201, 267 301F                                                  E-mail : ajsmmill@yahoo.co.in         |"+"\n");
                         FW.write("|  EFax : 04285 - 267 565F                                                                    purchase@amarjothi.net       |"+"\n");
                         FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");

                         if(rdcInstruction.SDocType.startsWith("RETU"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                         else if(rdcInstruction.SDocType.startsWith("NON"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                    }

                    if(iMillCode==4)
                    {
                         FW.write("|                                   E SRI KAMADHENU TEXTILES F@g                                                                                                                         |"+"\n");
                         FW.write("|E"+common.Cad("Pudusuripalayam, Nambiyur - 638 458. ",120)+"F|"+"\n");
                         FW.write("|E"+common.Cad("Gobi Taluk, Erode(Dt.). ",120)+"F|"+"\n");
                         FW.write("|                                                                                                                        |"+"\n");
                         FW.write("|  ETIN : 33612404386F                                                               CST    : 972942 dt. 14.12.2008        |"+"\n");
                         FW.write("|  Eph  : 04285 - 267 201, 267 301F                                                  E-mail : ajsmmill@yahoo.co.in         |"+"\n");
                         FW.write("|  EFax : 04285 - 267 565F                                                                    purchase@amarjothi.net       |"+"\n");
                         FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");

                         if(rdcInstruction.SDocType.startsWith("RETU"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                         else if(rdcInstruction.SDocType.startsWith("NON"))
                         {
                              FW.write("|                            E DELIVERY CHALLAN - "+rdcInstruction.SDocType+" F@g                                                                                                                         |"+"\n");
                         }
                    }


                    FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
                    FW.write("| EName of the consignee:F                                                   |ERDC No.   F|E"+common.Pad(rdcInstruction.SRdcNo,12)+"F| EDate   F|E"+common.Cad(common.parseDate(rdcInstruction.SRdcDate),12)+"F|"+"\n");
                    FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
                    FW.write("|                                                                          |Ref.No.   |"+common.Pad(rdcInstruction.SRefNo,12)+"| Date   |"+common.Cad(common.parseDate(rdcInstruction.SRefDate),12)+"|"+"\n");
                    FW.write("| "+common.Pad(rdcInstruction.SName,73)+"|---------------------------------------------|"+"\n");




                    if((rdcInstruction.SAddress1).equals("na"))
                    {
                         FW.write("| "+common.Pad("",73)+"|Inward No.|"+common.Pad(rdcInstruction.SInwNo,12)+"| Date   |"+common.Cad(common.parseDate(rdcInstruction.SInwDate),12)+"|"+"\n");
                    }
                    else
                    {
                         FW.write("| "+common.Pad(rdcInstruction.SAddress1,73)+"|Inward No.|"+common.Pad(rdcInstruction.SInwNo,12)+"| Date   |"+common.Cad(common.parseDate(rdcInstruction.SInwDate),12)+"|"+"\n");
                    }
                    if((rdcInstruction.SAddress2).equals("na"))
                    {
                         FW.write("| "+common.Pad("",73)+"|---------------------------------------------|"+"\n");
                    }
                    else
                    {
                         FW.write("| "+common.Pad(rdcInstruction.SAddress2,73)+"|---------------------------------------------|"+"\n");
                    }
                    FW.write("| "+common.Pad(rdcInstruction.SAddress3,73)+"|GI No.    |"+common.Pad(rdcInstruction.SGiNo,12)+"| Date   |"+common.Cad(common.parseDate(rdcInstruction.SGiDate),12)+"|"+"\n");


                    if((rdcInstruction.SPlace).equals("UNKNOWN"))
                    {
                         FW.write("| "+common.Pad("",73)+"|---------------------------------------------|"+"\n");
                    }
                    else
                    {
                         FW.write("| "+common.Pad(rdcInstruction.SPlace,73)+"|---------------------------------------------|"+"\n");
                    }
                    FW.write("|                                                                          |Bill No.  |"+common.Pad(rdcInstruction.SBillNo,12)+"| Date   |"+common.Cad(common.parseDate(rdcInstruction.SBillDate),12)+"|"+"\n");
                    FW.write("| "+common.Pad("",73)+"|---------------------------------------------|"+"\n");
                    FW.write("|                                                                          |MBD  No.  |"+common.Pad(rdcInstruction.SMBDNo,12)+"| Date   |"+common.Cad(common.parseDate(rdcInstruction.SMBDDate),12)+"|"+"\n");
                    FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
                    FW.write("| We are despatching the materials to your good office as per the details given below:                                   |"+"\n");
                    FW.write("|                                                                                                                        |"+"\n");
                    FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
                    FW.write("| SNO |                   DESCRIPTION OF GOODS             |      QTY      |                   PURPOSE                   |"+"\n");
                    FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
               }
               iLctr=30;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void toPrtBody()
     {
          int iSlNo=1;

          try
          {
               for(int i=0; i<theVector.size(); i++)
               {
                    rdcInstruction   = (RDCInstruction)theVector.elementAt(i);

                    for(int j=0; j<rdcInstruction.VDescript.size(); j++)
                    {
                         toPrtHead();

                         String SDescript = (String)rdcInstruction.VDescript.elementAt(j);
                         String SRemarks  = (String)rdcInstruction.VRemarks.elementAt(j);

                         if((SDescript.length()<=51) && (SRemarks.length()<=44))
                         {
                              FW.write("|"+common.Cad(String.valueOf(iSlNo),5)+"|"+
                                           common.Pad(common.Space(1)+SDescript,52)+"|"+
                                           common.Rad(common.getRound((String)rdcInstruction.VQty.elementAt(j),3)+
                                           common.Space(1),11)+common.Pad((String)rdcInstruction.SUom,4)+"|"+
                                           common.Pad(common.Space(1)+SRemarks,45)+"|\n");

                              iLctr++;
                         }
                         else if((SDescript.length()>51) && (SRemarks.length()<=44))
                         {
                              String SDesc1 = SDescript.substring(0,50)+"-";
                              String SDesc2 = "-"+SDescript.substring(50,SDescript.length());

                              FW.write("|"+common.Cad(String.valueOf(iSlNo),5)+"|"+
                                           common.Pad(common.Space(1)+SDesc1,52)+"|"+
                                           common.Rad(common.getRound((String)rdcInstruction.VQty.elementAt(j),3)+
                                           common.Space(1),11)+common.Pad((String)rdcInstruction.SUom,4)+"|"+
                                           common.Pad(common.Space(1)+(String)rdcInstruction.VRemarks.elementAt(j),45)+"|\n");

                              FW.write("|"+common.Pad(common.Space(5),5)+"|"+
                                           common.Rad(SDesc2,52)+"|"+
                                           common.Pad(common.Space(15),15)+"|"+
                                           common.Pad(common.Space(45),45)+"|\n");

                              iLctr=iLctr+2;
                         }
                         else if((SDescript.length()<=51) && (SRemarks.length()>44))
                         {

                              String SRem1 = SRemarks.substring(0,43)+"-";
                              String SRem2 = "-"+SRemarks.substring(43,SRemarks.length());

                              FW.write("|"+common.Cad(String.valueOf(iSlNo),5)+"|"+
                                           common.Pad(common.Space(1)+SDescript,52)+"|"+
                                           common.Rad(common.getRound((String)rdcInstruction.VQty.elementAt(j),3)+
                                           common.Space(1),11)+common.Pad((String)rdcInstruction.SUom,4)+"|"+
                                           common.Pad(common.Space(1)+SRem1,45)+"|\n");

                              FW.write("|"+common.Pad(common.Space(5),5)+"|"+
                                           common.Pad(common.Space(52),52)+"|"+
                                           common.Pad(common.Space(15),15)+"|"+
                                           common.Rad(SRem2,45)+"|\n");

                              iLctr=iLctr+2;
                         }
                         else if((SDescript.length()>51) && (SRemarks.length()>44))
                         {

                              String SDesc1 = SDescript.substring(0,50)+"-";
                              String SDesc2 = "-"+SDescript.substring(50,SDescript.length());

                              String SRem1 = SRemarks.substring(0,43)+"-";
                              String SRem2 = "-"+SRemarks.substring(43,SRemarks.length());

                              FW.write("|"+common.Cad(String.valueOf(iSlNo),5)+"|"+
                                           common.Pad(common.Space(1)+SDesc1,52)+"|"+
                                           common.Rad(common.getRound((String)rdcInstruction.VQty.elementAt(j),3)+
                                           common.Space(1),11)+common.Pad((String)rdcInstruction.SUom,4)+"|"+
                                           common.Pad(common.Space(1)+SRem1,45)+"|\n");

                              FW.write("|"+common.Pad(common.Space(5),5)+"|"+
                                           common.Rad(SDesc2,52)+"|"+
                                           common.Pad(common.Space(15),15)+"|"+
                                           common.Rad(SRem2,45)+"|\n");

                              iLctr=iLctr+2;
                         }


                         iSlNo++;

                         FW.write("|"+common.Space(5)+"|"+
                                      common.Space(52)+"|"+
                                      common.Space(15)+"|"+
                                      common.Space(45)+"|\n");
                         iLctr++;
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void toPrtFoot(int iSig)
     {
          try
          {

               FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
               FW.write("|SENT THROUGH   Vehicle No  |"+common.Pad(rdcInstruction.SThro,22)+"|Name of the|"+common.Pad(rdcInstruction.SPerson,33)+"| Time  |               |"+"\n");
               FW.write("|                           |                      |  person   |                                 | out   |               |"+"\n");
               FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
               FW.write("|                           |                                              |                                             |"+"\n");          
             //FW.write("|                           |                                              |                                             |"+"\n");          
               FW.write("|                           |                                              |                                             |"+"\n");
               FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
               FW.write("|          Prepared by      |                     Checked by               |               Authorised by                 |"+"\n");
               FW.write("|------------------------------------------------------------------------------------------------------------------------|"+"\n");
               FW.write("|Instructions:                                                             |                                             |"+"\n");
               FW.write("|                                                                          |                                             |"+"\n");
               FW.write("|1.Original & Duplicate to be sent to party and triplicate copy with       |                                             |"+"\n");
               FW.write("|  Stores files.                                                           |                                             |"+"\n");
               FW.write("|2.The party should return the Duplicate copy duly signed for              |                                             |"+"\n");
               FW.write("|  acknowledgement.                                                        |---------------------------------------------|"+"\n");
               FW.write("|                                                                          |       Goods received by (Sign & Seal)       |"+"\n");
               FW.write("-------------------------------------------------------------------------------------------------------------------------|"+"\n");
               if(iSig==0)
               {
                    FW.write("< End Of Report >");
               }
               else
               {
                    FW.write("                                                                                         (Continued on Next Page)        "+"\n");
               }

               //FW.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void setDataintoVector()
     {
          theVector  = new Vector();

          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement theStatement   = theconnect.createStatement();
               ResultSet theResult      = theStatement.executeQuery(getRDCQs());

               while(theResult.next())
                    organizeRDC(theResult);
               theResult.close();

               theStatement.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               System.out.println(ex);
          }
     }
     private String getRDCQs()
     {
          String QS = " select RdcNo,RdcDate,RefNo,RefDate,RefInwNo,RefInwDate,RefGiNo,RefGiDate,RefBillNo, "+
                      " RefBillDate,thro,Person,Name,nvl(Addr1,'na'),nvl(Addr2,'na'),Addr3, nvl(PlaceName,'UNKNOWN'),Descript,Qty,Remarks, "+
                      " decode(DocType,0,'RETURNABLE','NON RETURNABLE') as DocType,MBDNo,Uom.UomName,MBDDate "+
                      " From RDC "+
                      " Inner Join "+SSupTable+" on "+SSupTable+".Ac_Code = RDC.Sup_Code "+
                      " Left Join Place on Place.PlaceCode = "+SSupTable+".City_Code "+
                      " Inner Join Uom on RDC.UomCode = Uom.UomCode "+
                      " Where RdcNo = "+SRdcNo+" and Rdc.MillCode="+iMillCode+" ";

          return QS;
     }
     private void organizeRDC(ResultSet theResult) throws Exception
     {
          RDCInstruction   rdcInstruction  = null;

          String SRdcNo     =    common.parseNull(theResult.getString(1));
          String SRdcDate   =    common.parseNull(theResult.getString(2));
          String SRefNo     =    common.parseNull(theResult.getString(3));
          String SRefDate   =    common.parseNull(theResult.getString(4));
          String SInwNo     =    common.parseNull(theResult.getString(5));
          String SInwDate   =    common.parseNull(theResult.getString(6));
          String SGiNo      =    common.parseNull(theResult.getString(7));
          String SGiDate    =    common.parseNull(theResult.getString(8));
          String SBillNo    =    common.parseNull(theResult.getString(9));
          String SBillDate  =    common.parseNull(theResult.getString(10));
          String SThro      =    common.parseNull(theResult.getString(11));
          String SPerson    =    common.parseNull(theResult.getString(12));
          String SName      =    common.parseNull(theResult.getString(13));
          String SAddress1  =    common.parseNull(theResult.getString(14));
          String SAddress2  =    common.parseNull(theResult.getString(15));
          String SAddress3  =    common.parseNull(theResult.getString(16));
          String SPlace     =    common.parseNull(theResult.getString(17));
          String SDescript  =    common.getNarration(common.parseNull(theResult.getString(18)));
          double dQty       =    theResult.getDouble(19);
          String SRemarks   =    common.parseNull(theResult.getString(20));
          String SDocType   =    common.parseNull(theResult.getString(21));
          String SMBDNo     =    common.parseNull(theResult.getString(22));
          String SUom       =    common.parseNull(theResult.getString(23));
          String SMBDDate   =    common.parseNull(theResult.getString(24));


          int iIndex = getIndexOf(SRdcNo);

          if(iIndex==-1)
          {
               rdcInstruction  = new RDCInstruction(SRdcNo,SRdcDate,SRefNo,SRefDate,SInwNo,SInwDate,SBillNo,SBillDate,SThro,SPerson,SName,SAddress1,SAddress2,SAddress3,SPlace,SGiNo,SGiDate,SDocType,SMBDNo,SUom,SMBDDate);
               theVector.addElement(rdcInstruction);
               iIndex = theVector.size()-1;
          }
          rdcInstruction = (RDCInstruction)theVector.elementAt(iIndex);
          rdcInstruction.append(SDescript,dQty,SRemarks);
     }
     private int getIndexOf(String SRdcNo)
     {
          int iIndex =-1;
          for(int i=0; i<theVector.size(); i++)
          {
               RDCInstruction rdcInstruction = (RDCInstruction)theVector.elementAt(i);
               if((rdcInstruction.SRdcNo).equals(SRdcNo))
               {
                    iIndex =i;
                    break;
               }
          }
          return iIndex;
     }
}

