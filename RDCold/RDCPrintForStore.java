package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.FlowLayout;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCPrintForStore extends JInternalFrame
{
     JPanel     TopPanel;
     JPanel     BottomPanel;
     JPanel     MiddlePanel;

     String     SDate;
     JButton    BApply,BPrint;
    
     DateField TDate;
    
     Object RowData[][];
     
     String ColumnData[] = {"Doc Type","RDC Type","RDC No","RDC Date","Sent Through","Supplier Name","Description of Goods","Purpose","Quantity","Delay"}; 
     String ColumnType[] = {"S","S","N","S","S","S","S","S","N","N"}; 
     String SNo,DocType,RdcType,RdcNo,RdcDate,RdcName,Senthr,SuppName,Descrip,Purpos,SendQty, SRDCDate,SDateDiff;

     JLayeredPane DeskTop;
     StatusPanel SPanel;
     int iMillCode,iUserCode,iLineCount=1,iPageNo=1;
     String SSupTable,SMillName;

     TabReport tabreport;
     Common common = new Common();
     Vector VDocNo,VRdcNo,VRdcName,VSentThr,VSupplier;
     Vector VDescrip,VPurpose,VSendQty,VRdcDate;

     int Lctr = 100,Pctr=0;
     FileWriter FW;
     String str6;
     boolean bBlank=true;
     ORAConnection connect;
     Connection theconnect;

     RDCPrintForStore(JLayeredPane DeskTop,int iMillCode,int iUserCode,String SSupTable,String SMillName)
     {
         super("OutpassPendingList(Stores)");
         this.DeskTop   = DeskTop;
         this.SPanel    = SPanel;
         this.iMillCode = iMillCode;
         this.iUserCode = iUserCode;
         this.SSupTable = SSupTable;
         this.SMillName = SMillName;

         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     private void createComponents()
     {
          TopPanel    = new JPanel(true);
          BottomPanel = new JPanel(true);
          MiddlePanel = new JPanel(true);

          TDate       = new DateField();
          BApply      = new JButton("Apply");
          BPrint      = new JButton("Print");

          TDate.setTodayDate();
          TDate.setEditable(false);
          BPrint.setEnabled(false);

          BApply.setMnemonic('A');
          BPrint.setMnemonic('P');
     }
     private void setLayouts()
     {
         TopPanel.setLayout(new FlowLayout(5));
         MiddlePanel.setLayout(new BorderLayout());

         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,790,500);
     }
     private void addComponents()
     {
          TopPanel.add(new JLabel("As On"));
          TopPanel.add(TDate);
          TopPanel.add(BApply);

          BottomPanel.add(BPrint);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

     }
     private void addListeners()
     {
          BApply.addActionListener(new ActList());
          BPrint.addActionListener(new PrintList());
     }

     private class PrintList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setRDCPendingReport();
               removeHelpFrame();
               
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
     
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               
               boolean bflag = setDataIntoVector();
               if(!bflag)
                    return;
               setVectorIntoRowData();
               try
               {
                    MiddlePanel.remove(tabreport);
                    MiddlePanel.updateUI();
               }
               catch(Exception ex){}

               tabreport = new TabReport(RowData,ColumnData,ColumnType);
               MiddlePanel.add("Center",tabreport);
               MiddlePanel.updateUI();
               BPrint.setEnabled(true);
          }
          
     }
     private boolean setDataIntoVector()
     {
          boolean bflag = false;

          VDocNo   = new Vector();
          VRdcNo   = new Vector();
          VRdcName = new Vector();
          VRdcDate = new Vector();
          VSentThr = new Vector();
          VSupplier= new Vector();
          VDescrip = new Vector();
          VPurpose = new Vector();
          VSendQty = new Vector();
          
          String SDate = TDate.toNormal();
          
          String QS=" SELECT RDC.DocType,RDC.RDCNo,RDC.RdcType,RDC.RDCDate,RDC.Thro, "+SSupTable+".Name, RDC.Descript, RDC.Remarks,RDC.Qty "+ 
                    " FROM RDC "+
                    " INNER JOIN "+SSupTable+" ON "+SSupTable+".Ac_Code = RDC.Sup_Code "+
                    " Left Join Grn on RDC.GrnId = Grn.Id"+ 
                    " Where RDC.MillCode ="+iMillCode+
                    " and rdc.outpassstatus=0 and rdc.gateauth=0 and RDC.RDCDate<="+SDate ;
           try
           {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();
               ResultSet res = stat.executeQuery(QS);
               while(res.next())
               {
                    int iDocType = res.getInt(1);
                    
                    if(iDocType==0)
                    {
                         VDocNo.addElement("RDC");
                    }
                    else
                    {
                         VDocNo.addElement("NRDC");
                    }

                    VRdcName  . addElement(res.getString(2));
                    int iRdcType = res.getInt(3);
                    if(iRdcType==0)
                    {
                         VRdcNo.addElement("Regular");
                    }
                    else
                    {
                         VRdcNo.addElement("Non Regular");
                    }

                    SRDCDate  = common.parseDate(res.getString(4));
                    VRdcDate  . addElement(SRDCDate);
                    VSentThr  . addElement(res.getString(5)); 
                    VSupplier . addElement(res.getString(6));
                    VDescrip  . addElement(res.getString(7));
                    VPurpose  . addElement(res.getString(8));
                    VSendQty  . addElement(res.getString(9));
               }
               res.close();
               stat.close();
               bflag = true;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
               bflag = false;
          }
          return bflag;
     }
     private void setRDCPendingReport()
     {
          int iLinecount =0;
          int iPageNo    =1;
          int iSlNo      =1;

          try
          {
               FW=new FileWriter(common.getPrintPath()+"RdcPrintForStore.prn");
               FW.write(toHeadPrn());

               for(int i=0;i<VSendQty.size();i++)
               {
                    int  iSNo  =i+1;    
                    SNo        =common.Pad(String.valueOf(iSNo),3);
                    DocType    =common.Pad(VDocNo.elementAt(i).toString(),10);
                    RdcType    =common.Pad(VRdcNo.elementAt(i).toString(),15);
                    RdcName    =common.Pad(VRdcName.elementAt(i).toString(),10);
                    RdcDate    =common.Pad(SRDCDate,10);
                    Senthr     =common.Pad(VSentThr.elementAt(i).toString(),20);
                    SuppName   =common.Pad(VSupplier.elementAt(i).toString(),20);
                    Descrip    =common.Pad(VDescrip.elementAt(i).toString(),15);
                    Purpos     =common.Pad(VPurpose.elementAt(i).toString(),15);
                    SendQty    =common.Rad(VSendQty.elementAt(i).toString(),10);
                    
   
                    if(iLineCount < 60)                                                                                          
                    {
                            iLineCount = 0;
    
                            FW.write("|"+SNo+"|"+DocType+"|"+RdcType+"|"+RdcName+"|"+RdcDate+"|"+Senthr+"|"+SuppName+"|"+Descrip+"|"+Purpos+"|"+SendQty+"|"+SDateDiff+"|\n");
                            FW.write("|"+common.Space(3)+"|"+common.Space(10)+"|"+common.Space(15)+"|"+common.Space(10)+"|"+common.Space(10)+"|"+common.Space(20)+"|"+common.Space(20)+"|"+common.Space(15)+"|"+common.Space(15)+"|"+common.Space(10)+"|"+common.Space(5)+"|\n");
                            iLineCount += 2;
                            iSNo= iSNo+1;
                    }
                    else
                    {
                            FW.write("|"+SNo+"|"+DocType+"|"+RdcType+"|"+RdcNo+"|"+RdcDate+"|"+Senthr+"|"+SuppName+"|"+Descrip+"|"+Purpos+"|"+SendQty+"|"+SDateDiff+"|\n");
                            FW.write("|"+common.Space(3)+"|"+common.Space(10)+"|"+common.Space(15)+"|"+common.Space(10)+"|"+common.Space(10)+"|"+common.Space(20)+"|"+common.Space(20)+"|"+common.Space(15)+"|"+common.Space(15)+"|"+common.Space(10)+"|"+common.Space(5)+"|\n");
                            iSNo=iSNo+1;
                    }
               }
               FW.write(toFootPrn());
               FW.close();

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

     }
     private String toHeadPrn()
     {    StringBuffer SB = new StringBuffer();
          SB.append("P\n");
          SB.append("E "+SMillName+"F\n");
          SB.append("E  Outpass Pending List (For Stores)  As On:"+TDate.toString()+"F\n");
          SB.append("EPage No : "+(iPageNo++)+"\n");
          SB.append("|-----------------------------------------------------------------------------------------------------------------------------------------------| \n");
          SB.append("|Sl.|Doc Type  | RDC Type      |  RDC No  |  RDC Date|    Sent Through    | SupplierName       |Description    |Purpose        |    Qty   |Delay| \n");
          SB.append("|No.|          |               |          |          |                    |                    |               |               |          |     |  \n");
          SB.append("|-----------------------------------------------------------------------------------------------------------------------------------------------|F\n");
          SB.append("");                                                                                                 
          
          return SB.toString();                                   
     }
     private String toFootPrn()
     {                                                                                                                                                                                                     
          StringBuffer SB = new StringBuffer();

          SB.append("------------------------------------------------------------------------------------------------------------------------------------------------| \n");
          SB.append("\n");

          return SB.toString();
     }

     private void setVectorIntoRowData()
     {
          RowData = new Object[VRdcNo.size()][10];
          
          for(int i=0;i<VRdcDate.size();i++)
          {
                    String temp    =   common.parseDate((String)VRdcDate.elementAt(i));
                    String temp1   =   common.parseDate(TDate.toNormal());
                    SDateDiff= common.Rad(common.getDateDiff(temp1,temp),5);

                    RowData[i][0]  = (String)VDocNo    .elementAt(i);
                    RowData[i][1]  = (String)VRdcNo    .elementAt(i);
                    RowData[i][2]  = (String)VRdcName  .elementAt(i);
                    RowData[i][3]  = (String)VRdcDate  .elementAt(i);
                    RowData[i][4]  = (String)VSentThr  .elementAt(i);
                    RowData[i][5]  = (String)VSupplier .elementAt(i);
                    RowData[i][6]  = (String)VDescrip  .elementAt(i);
                    RowData[i][7]  = (String)VPurpose  .elementAt(i);
                    RowData[i][8]  = (String)VSendQty  .elementAt(i);
                    RowData[i][9]  = SDateDiff          ;
          }
     }
}
