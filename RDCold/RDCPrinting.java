package RDC;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class RDCPrinting extends JInternalFrame
{
      JLayeredPane Layer;

      JPanel         TopPanel,BottomPanel,MiddlePanel,TopLeft,TopRight;
      AddressField   TShort;
      MyComboBox     JCDept,JCMachine,JCServDept,JCUnit;
      JButton        BApply,BOk,BCancel,BPool;
      TabReport      theReport;
      JTabbedPane    thePane;
      DateField      fromDate,toDate;
      Vector         VRDCNo,VRDCDate,VSupplier;
      FileWriter     FW;

      Common common   = new Common();
      int iUserCode,iMillCode;
      String SSupTable;

      ORAConnection connect;
      Connection theconnect;

      public RDCPrinting(JLayeredPane Layer,int iUserCode,int iMillCode,String SSupTable)
      {
            this.Layer     = Layer;
            this.iUserCode = iUserCode;
            this.iMillCode = iMillCode;
            this.SSupTable = SSupTable;

            createComponents();
            setLayouts();
            addComponents();
            addListeners();
      }
      public void createComponents()
      {
            try
            {
                 thePane     = new JTabbedPane();
                 fromDate    = new DateField();
                 toDate      = new DateField();

                 fromDate.setTodayDate();
                 toDate.setTodayDate();

                 TopPanel    = new JPanel(true);
                 TopLeft     = new JPanel(true);
                 TopRight    = new JPanel(true);
                 BottomPanel = new JPanel(true);
                 MiddlePanel = new JPanel(true);

                 BApply      = new JButton("Apply");
                 BOk         = new JButton("Print");
                 BCancel     = new JButton("Cancel");
            }catch(Exception ex)
            {
                ex.printStackTrace();
            }
      }
      public void setLayouts()
      {
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,450,500);
            setTitle("RDC Printing Moniter");

            TopPanel.setLayout(new GridLayout(1,2));
            TopLeft.setLayout(new GridLayout(2,2));
            TopLeft.setBorder(new TitledBorder("Date"));
            TopRight.setLayout(new GridLayout(1,1));
            TopRight.setBorder(new TitledBorder("Apply"));
            MiddlePanel.setLayout(new BorderLayout());
            BottomPanel.setLayout(new FlowLayout());
            TopPanel.setBorder(new TitledBorder("Info"));
            //MiddlePanel.setBorder(new TitledBorder("RDC List"));
      }
      public void addComponents()
      {
            TopLeft.add(new MyLabel("FromDate"));
            TopLeft.add(fromDate);

            TopLeft.add(new MyLabel("ToDate"));
            TopLeft.add(toDate);

            TopRight.add(BApply);

            TopPanel.add(TopLeft);
            TopPanel.add(TopRight);

            BottomPanel.add(BOk);
            BottomPanel.add(BCancel);

            getContentPane().add("North",TopPanel);
            getContentPane().add("Center",MiddlePanel);
            getContentPane().add("South",BottomPanel);
     }
     public void addListeners()
     {
          BOk         .addActionListener(new ActList());
          BCancel     .addActionListener(new ActList());
          BApply      .addActionListener(new ActList());
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    PrintStatus();
                    removeHelpFrame();
               }
               if(ae.getSource()==BApply)
               {
                    String SStDate = fromDate.toNormal();
                    String SEnDate = toDate.toNormal();
                    showList(SStDate,SEnDate);
                    setTabReport();
               }
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
               }
          }
     }
     private void setTabReport()
     {
          try
          {
               MiddlePanel.removeAll();
               thePane.removeAll();

               String ColumnName[]  = {"Sl.No","RDCNo","RDCDate","Supplier","Print" };
               String ColumnType[]  = {"N","N","S","S","B"};
               int    ColumnWidth[] = {50,60,100,100,100};

               if(VSupplier.size()==0)
                    return;

               Object RowData[][] = new Object[VSupplier.size()][ColumnName.length];
               for(int i=0;i<VSupplier.size();i++)
               {
                    RowData[i][0]   = String.valueOf(i+1);
                    RowData[i][1]   = common.parseNull((String)VRDCNo.elementAt(i));
                    RowData[i][2]   = common.parseDate((String)VRDCDate.elementAt(i));
                    RowData[i][3]   = common.parseNull((String)VSupplier.elementAt(i));
                    RowData[i][4]   = new Boolean(false);
               }
               theReport      = new TabReport(RowData,ColumnName,ColumnType);
               theReport.setPrefferedColumnWidth(ColumnWidth);
               MiddlePanel.add("Center",thePane);
               thePane.addTab("RDC Print List",theReport);
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
     }
     private void PrintStatus()
     {
          Boolean bFlag;
          try
          {
               FW  = new FileWriter(common.getPrintPath()+"RDCList.prn");
               for(int i=0;i<theReport.ReportTable.getRowCount();i++)
               {
                    bFlag  = (Boolean)theReport.ReportTable.getValueAt(i,4);

                    if(bFlag.booleanValue())
                    {
                         String SRdcNo    = (String)theReport.ReportTable.getValueAt(i,1);
                         printData(SRdcNo);
                    }
               }
               FW   .close();

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void printData(String SRdcNo)
     {
          try
          {
               RDCPrintClass rdcPrintClass = new RDCPrintClass();
               rdcPrintClass.printWithDate(FW,SRdcNo,iMillCode,SSupTable);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void showList(String SStDate,String SEnDate)
     {
          VRDCNo    = new Vector();
          VRDCDate  = new Vector();
          VSupplier = new Vector(); 

          String QS = " select distinct RDCNo,RDCDate,Name "+
                      " From RDC "+
                      " Inner Join "+SSupTable+" On "+SSupTable+".AC_Code = RDC.Sup_Code "+
                      " WHere RDcDate>="+SStDate+" and RDCDate<="+SEnDate+" and MillCode="+iMillCode+" Order by 1 ";
          try
          {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat       = theconnect.createStatement();
               ResultSet theResult  = stat.executeQuery(QS);

               while(theResult.next())
               {
                    VRDCNo    .addElement(theResult.getString(1));
                    VRDCDate  .addElement(theResult.getString(2));
                    VSupplier .addElement(theResult.getString(3));
               }
               theResult.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }

}
