package RDC;

import javax.swing.*;
import java.awt.*;
import util.*;
import jdbc.*;

public class Status extends JPanel
{
     JLabel LLabel,MLabel,RLabel;
     JPanel MPanel;
     public Status()
     {
          LLabel = new JLabel(" ",JLabel.LEFT);
          RLabel = new JLabel("Developed By FIT for Amarjothi Spinning Mills Ltd",new ImageIcon("cupanim.gif"),JLabel.LEFT);
          MLabel = new JLabel(" ",JLabel.LEFT);
          MPanel = new JPanel(true);

          setLayout(new GridLayout(1,4,1,1));

          add(LLabel);
          add(MPanel);
          add(MLabel);
          add(RLabel);
     }
}
