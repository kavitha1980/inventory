package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

class WasteOutPassFrame extends JInternalFrame
{
     Connection     theConnection  = null;
     Common         common         = new Common();

     JLayeredPane   Layer;
     JPanel         TopPanel,BottomPanel,MiddlePanel;

     JButton        BOk;
     JComboBox      JCBookNo;
     TimeField      TOutTime;

     Vector         VInvNo,VInvDate,VPartyCode,VPartyName,VBales,VQty,VId;

     Object RowData[][];

     String ColumnData[]={"Inv No","Inv Date","Buyer Name","No of Bales","Weight","Click"};
     String ColumnType[]={"N"     ,"S"       ,"S"         ,"N"          ,"N"     ,"B"    };

     TabReport      tabreport;

     String         SOutPassNo= "";
     String         SOutTime  = "";

     int            iBookNo   = 0;
     int            iMillCode,iUserCode;
     String         SYearCode;
                    
     public WasteOutPassFrame(JLayeredPane Layer,int iMillCode,int iUserCode,String SYearCode)
     {
          super("Waste OutPass Entry Screen");
          this . Layer        = Layer;
          this . iMillCode    = iMillCode;
          this . iUserCode    = iUserCode;
          this . SYearCode    = SYearCode;

          setDataIntoVector();
          createComponents();
          setLayouts();
          addComponents();
          addListener();
          setTabReport();
     }

     public void createComponents()
     {
          JCBookNo       = new JComboBox();
          JCBookNo       . addItem("Book1");
          JCBookNo       . addItem("Book2");
          JCBookNo       . addItem("Book3");

          JCBookNo       . setEnabled(false);

          TOutTime       = new TimeField(true);

          BOk            = new JButton("Save");

          if(VInvNo.size()>0)
          {
               BOk.setEnabled(true);
          }
          else
          {
               BOk.setEnabled(false);
          }
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          MiddlePanel    = new JPanel();
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,750,400);
          setTitle("Waste OutPass Entry Screen");

          TopPanel       . setLayout(new GridLayout(2,2,10,10));
          BottomPanel    . setLayout(new FlowLayout());
          MiddlePanel    . setLayout(new BorderLayout());
          MiddlePanel.setBorder(new TitledBorder("Pending List"));
     }

     public void addComponents()
     {
          TopPanel            . add(new Label("Out Pass Book"));
          TopPanel            . add(JCBookNo);

          TopPanel            . add(new Label("Time Out"));
          TopPanel            . add(TOutTime);

          BottomPanel         . add(BOk);

          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(MiddlePanel,BorderLayout.CENTER);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListener()
     {
          BOk           . addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(isValidNew())
               {
                    if(!isValidParty())
                    {
                         JOptionPane.showMessageDialog(null,"Party Mismatch","Information",JOptionPane.INFORMATION_MESSAGE);
                    }
                    else
                    {
                         BOk.setEnabled(false);
     
                         SOutPassNo = getNextOutPassNo();
          
                         insertOutPass();
                         insertOutPassDetails();
                         UpdateWaste();
                         UpdateOutPassNo();
          
                         String SNum    = "The OutPass Number is "+SOutPassNo;
                         JOptionPane    . showMessageDialog(null,SNum,"Information",JOptionPane.INFORMATION_MESSAGE);
                         
                         removeHelpFrame();
                    }
               }
               else
               {
                    BOk.setEnabled(true);
               }
          }
     }

     public boolean isValidNew()
     {
          String SOutTime = common.parseNull(TOutTime.toString());

          if(SOutTime.equals(""))
          {
               JOptionPane.showMessageDialog(null,"Time out must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TOutTime.THours.requestFocus();
               return false;
          }

          int iCount=0;

          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
          {
               Boolean Bselected = (Boolean)RowData[i][5];
               if(Bselected.booleanValue())
               {
                    iCount++;
               }
          }
 
          if(iCount<=0)
          {
                JOptionPane.showMessageDialog(null,"No Rows Selected","Error",JOptionPane.ERROR_MESSAGE);
                tabreport.ReportTable.requestFocus();
                return false;
          }

          return true;
     }

     private boolean isValidParty()
     {
          boolean flag=true;

          String SPartyCode="";
          String SPPartyCode="";
          int iRowCount=0;

          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
          {
               try
               {
                    Boolean BSelected = (Boolean)tabreport.ReportTable.getValueAt(i,5);
                    if(BSelected.booleanValue())
                    {
                         iRowCount++;

                         SPartyCode = (String)VPartyCode.elementAt(i);

                         if(iRowCount==1)
                         {
                              SPPartyCode = SPartyCode;
                         }
                         else
                         {
                              if(!SPartyCode.equals(SPPartyCode))
                              {
                                   return false;
                              }
                         }
                    }
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
          return flag;
     }


     private void setDataIntoVector()
     {
          VInvNo     = new Vector();
          VInvDate   = new Vector();
          VPartyCode = new Vector();
          VPartyName = new Vector();
          VBales     = new Vector();
          VQty       = new Vector();
          VId        = new Vector();

          String QS = " Select IssueNo,IssueDate,BuyerCode,PartyName,NoofBales,Sum(TotalWeight),IssueToSales.Id "+
                      " From IssueToSales "+
                      " Inner Join PartyMaster on to_char(IssueToSales.BuyerCode)=PartyMaster.PartyCode "+
                      " Where NoofBales>0 and OutPassStatus = 0 and IssueDate="+common.getServerPureDate()+
                      " Group by IssueNo,IssueDate,BuyerCode,PartyName,NoofBales,IssueToSales.Id "+
                      " Order by 4,1";

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();
               
               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    VInvNo    . addElement(result.getString(1));
                    VInvDate  . addElement(result.getString(2));
                    VPartyCode. addElement(result.getString(3));
                    VPartyName. addElement(result.getString(4));
                    VBales    . addElement(result.getString(5));
                    VQty      . addElement(result.getString(6));
                    VId       . addElement(result.getString(7));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     public void setTabReport()
     {
          RowData = new Object[VInvNo.size()][ColumnData.length];

          for(int i=0;i<VInvNo.size();i++)
          {
               RowData[i][0] = (String)VInvNo.elementAt(i);
               RowData[i][1] = common.parseDate((String)VInvDate.elementAt(i));
               RowData[i][2] = (String)VPartyName.elementAt(i);
               RowData[i][3] = (String)VBales.elementAt(i);
               RowData[i][4] = (String)VQty.elementAt(i);
               RowData[i][5] = new Boolean(false);
          }

          MiddlePanel.add("Center",tabreport=new TabReport(RowData,ColumnData,ColumnType));

          tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
     }

     private void insertOutPass()
     {
          try
          {
               String    QS = " Insert into OutPass (ID,OUTPASSNO,OUTPASSDATE,MATERIALTYPE,BOOKTYPE,MILLCODE,USERCODE,CREATIONDATE) values (";
                         QS   = QS+" OUTPASS_SEQ.nextval ,";
                         QS   = QS+SOutPassNo+",";
                         QS   = QS+"'"+common.getServerPureDate()+"',";
                         QS   = QS+"2"+",";            // 0 - For Stores; 1 - For Others; 2 - Waste
                         QS   = QS+iBookNo+",";
                         QS   = QS+iMillCode+",";
                         QS   = QS+iUserCode+",";
                         QS   = QS+"'"+common.getServerDate()+"')";

               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();
                              stat           . execute(QS);
                              stat           . close();
          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void insertOutPassDetails()
     {
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();

               for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
               {
                    Boolean BSelected = (Boolean)tabreport.ReportTable.getValueAt(i,5);
                    if(BSelected.booleanValue())
                    {
                         String QS = " Insert into OutPassDetails (ID,OUTPASSNO,PARTYCODE,MATERIALTYPE,BOOKTYPE,MILLCODE,DESCRIPT,QTY,PURPOSE,TIMEOUT,INVNO,INVDATE,NOOFBALES) values (";
                                QS = QS+" OUTPASSDETAILS_SEQ.nextval ,";
                                QS = QS+SOutPassNo+",";
                                QS = QS+"'"+(String)VPartyCode.elementAt(i)+"',";
                                QS = QS+"2"+",";
                                QS = QS+iBookNo+",";
                                QS = QS+iMillCode+",";
                                QS = QS+"'"+"WASTE"+"',";
                                QS = QS+"0"+(String)tabreport.ReportTable.getValueAt(i,4)+",";
                                QS = QS+"'"+"FOR SALES"+"',";
                                QS = QS+"'"+TOutTime.toString()+"',";
                                QS = QS+"'"+(String)tabreport.ReportTable.getValueAt(i,0)+"',";
                                QS = QS+"0"+common.pureDate((String)tabreport.ReportTable.getValueAt(i,1))+",";
                                QS = QS+"0"+(String)tabreport.ReportTable.getValueAt(i,3)+")";
     
                         stat.execute(QS);
                    }
               }
               stat.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public String getNextOutPassNo()
     {
          String    QS           = "";

          int       iOutPassNo   = 0;

                    iBookNo   = 0;
                    iBookNo   = JCBookNo.getSelectedIndex();
                    iBookNo   = iBookNo+1;      

          if(iBookNo==1)
               QS = "  Select maxno from Config"+iMillCode+""+SYearCode+" where id = 13";

          /*if(iBookNo==2)
               QS = "  Select maxno from config where id = 27";

          if(iBookNo==3)
               QS = "  Select maxno from config where id = 28";*/

          try
          {
               ORAConnection  oraConnection  = ORAConnection     . getORAConnection();
               Connection     theConnection  = oraConnection     . getConnection();   
               Statement      stat           = theConnection     . createStatement();
               ResultSet      result         = stat              . executeQuery(QS);

                         result         . next();
                         iOutPassNo     = result.getInt(1);

                         result         . close();
                         stat           . close();

                         iOutPassNo     = iOutPassNo+1;

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

          return String.valueOf(iOutPassNo);
     }

     public void UpdateOutPassNo()
     {
          String    QS1       = "";

                    iBookNo   = 0;
                    iBookNo   = JCBookNo.getSelectedIndex();
                    iBookNo   = iBookNo+1;      

          if(iBookNo==1)
               QS1 = " Update Config"+iMillCode+""+SYearCode+" set MaxNo = MaxNo+1  where id = 13";

          /*if(iBookNo==2)
               QS1 = " Update config set MaxNo = MaxNo+1  where id = 27";

          if(iBookNo==3)
               QS1 = " Update config set MaxNo = MaxNo+1  where id = 28";*/

          try
          {
               ORAConnection  oraConnection  = ORAConnection     . getORAConnection();
               Connection     theConnection  = oraConnection     . getConnection();   
               Statement      stat           = theConnection     . createStatement();
                              stat           . executeUpdate(QS1);
                              stat           . close();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void UpdateWaste()
     {
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();   
               Statement      stat           =  theConnection.createStatement();

               for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
               {
                    Boolean BSelected = (Boolean)tabreport.ReportTable.getValueAt(i,5);
                    if(BSelected.booleanValue())
                    {
                         String SId = (String)VId.elementAt(i);

                         String QS = " Update IssueToSales set OutPassStatus=1, OutPassNo="+SOutPassNo+
                                     " Where Id = "+SId;

                         stat.executeUpdate(QS);
                    }
               }
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer   . remove(this);
               Layer   . repaint();
               Layer   . updateUI();
          }
          catch(Exception ex) { }
     }
}
