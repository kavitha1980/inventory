package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class WorkOrderCritPanel extends JPanel
{
    JPanel TopPanel,BottomPanel;
    JPanel UnitPanel;
    JPanel DeptPanel;
    JPanel GroupPanel;
    JPanel SupplierPanel;
    JPanel ListPanel;
    JPanel SortPanel;
    JPanel BasePanel;
    JPanel ControlPanel;
    JPanel ApplyPanel;

    Common common = new Common();
    ORAConnection connect;
    Connection theconnect;

    Vector VUnit,VUnitCode,VDept,VDeptCode,VGroup,VGroupCode,VSup,VSupCode;
    MyComboBox JCUnit,JCDept,JCGroup,JCSup,JCList;

    JTextField TSort;

    JRadioButton JRAllUnit,JRSeleUnit;
    JRadioButton JRAllDept,JRSeleDept;
    JRadioButton JRAllGroup,JRSeleGroup;
    JRadioButton JRAllSup,JRSeleSup;
    JRadioButton JRAllList,JRSeleList;
    JRadioButton JRDate,JROrdNo;

    JButton      BApply;
    JTextField   TStNo,TEnNo;
    DateField    TStDate,TEnDate;

    boolean bsig;

    int iMillCode;
    String SSupTable;

    WorkOrderCritPanel(int iMillCode,String SSupTable)
    {
        this.iMillCode = iMillCode;
        this.SSupTable = SSupTable;

        getUDGBS();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
    }

    public void createComponents()
    {
        TopPanel      = new JPanel();
        BottomPanel   = new JPanel();
        UnitPanel     = new JPanel();
        DeptPanel     = new JPanel();
        GroupPanel    = new JPanel();
        SupplierPanel = new JPanel();
        ListPanel     = new JPanel();
        SortPanel     = new JPanel();
        BasePanel     = new JPanel();
        ControlPanel  = new JPanel();
        ApplyPanel    = new JPanel();

        JRAllUnit   = new JRadioButton("All",true);
        JRSeleUnit  = new JRadioButton("Selected");
        JCUnit      = new MyComboBox(VUnit);
        JCUnit.setEnabled(false);

        JRAllDept   = new JRadioButton("All",true);
        JRSeleDept  = new JRadioButton("Selected");
        JCDept      = new MyComboBox(VDept);
        JCDept.setEnabled(false);

        JRAllGroup  = new JRadioButton("All",true);
        JRSeleGroup = new JRadioButton("Selected");
        JCGroup     = new MyComboBox(VGroup);
        JCGroup.setEnabled(false);

        JRAllSup    = new JRadioButton("All",true);
        JRSeleSup   = new JRadioButton("Selected");
        JCSup       = new MyComboBox(VSup);
        JCSup.setEnabled(false);

        JRAllList   = new JRadioButton("All",true);
        JRSeleList  = new JRadioButton("Selected");
        JCList      = new MyComboBox();
        JCList.setEnabled(false);

        JRDate      = new JRadioButton("Date",true);
        JROrdNo     = new JRadioButton("Order No");
        bsig = true;

        TSort       = new JTextField();
        TStNo       = new JTextField();
        TEnNo       = new JTextField();

        TStDate       = new DateField();
        TEnDate       = new DateField();

        BApply      = new JButton("Apply");

        TStDate.setTodayDate();
        TEnDate.setTodayDate();
    }
    public void setLayouts()
    {
        setLayout(new GridLayout(2,1));

        TopPanel.setLayout(new GridLayout(1,5));

        BottomPanel.setLayout(new GridLayout(1,5));
      
        UnitPanel   .setLayout(new GridLayout(3,1));
        DeptPanel   .setLayout(new GridLayout(3,1));   
        GroupPanel  .setLayout(new GridLayout(3,1));
        SupplierPanel  .setLayout(new GridLayout(3,1));
        ListPanel  .setLayout(new GridLayout(3,1));
        SortPanel   .setLayout(new GridLayout(2,1));
        BasePanel   .setLayout(new GridLayout(2,1));
        ControlPanel  .setLayout(new GridLayout(2,1));
        ApplyPanel  .setLayout(new GridLayout(3,1));

        UnitPanel .setBorder(new TitledBorder("Processing Units"));
        DeptPanel .setBorder(new TitledBorder("Department"));
        GroupPanel.setBorder(new TitledBorder("Group"));
        SupplierPanel.setBorder(new TitledBorder("Supplier"));
        ListPanel.setBorder(new TitledBorder("List Only"));
        SortPanel .setBorder(new TitledBorder("Sort on"));
        BasePanel .setBorder(new TitledBorder("Based on"));
        ControlPanel.setBorder(new TitledBorder("Control"));
        ApplyPanel.setBorder(new TitledBorder("Apply"));
    }
    public void addComponents()
    {
        JCList.addItem("Over-Due");
        JCList.addItem("All Pendings");
        JCList.addItem("Before Over-Due");

        add(TopPanel);
        add(BottomPanel);

        TopPanel.add(UnitPanel);
        TopPanel.add(DeptPanel);
        TopPanel.add(GroupPanel);
        TopPanel.add(SupplierPanel);
        BottomPanel.add(ListPanel);
        BottomPanel.add(SortPanel);
        BottomPanel.add(BasePanel);
        BottomPanel.add(ControlPanel);
        BottomPanel.add(ApplyPanel);

        UnitPanel.add(JRAllUnit);  
        UnitPanel.add(JRSeleUnit); 
        UnitPanel.add(JCUnit);     

        DeptPanel.add(JRAllDept);
        DeptPanel.add(JRSeleDept);
        DeptPanel.add(JCDept);    

        GroupPanel.add(JRAllGroup);
        GroupPanel.add(JRSeleGroup);
        GroupPanel.add(JCGroup);

        SupplierPanel.add(JRAllSup);
        SupplierPanel.add(JRSeleSup);
        SupplierPanel.add(JCSup);

        ListPanel.add(JRAllList);
        ListPanel.add(JRSeleList);
        ListPanel.add(JCList);

        SortPanel.add(TSort);
        SortPanel.add(new JLabel("eg 1,2,3"));

        BasePanel.add(JRDate);
        BasePanel.add(JROrdNo);

        addControlPanel();

        ApplyPanel.add(new JLabel(" "));
        ApplyPanel.add(BApply);
        ApplyPanel.add(new JLabel(" "));
    }
    public void addControlPanel()
    {
        ControlPanel.removeAll();
        if(bsig)
        {
            ControlPanel.add(TStDate);
            ControlPanel.add(TEnDate);
        }
        else
        {
            ControlPanel.add(TStNo);
            ControlPanel.add(TEnNo);
        }
        ControlPanel.updateUI();
    }
    public void addListeners()
    {
        JRAllUnit.addActionListener(new UnitList());
        JRSeleUnit.addActionListener(new UnitList());

        JRAllDept.addActionListener(new DeptList());
        JRSeleDept.addActionListener(new DeptList());

        JRAllGroup.addActionListener(new GroupList());
        JRSeleGroup.addActionListener(new GroupList());

        JRAllSup.addActionListener(new SupList());
        JRSeleSup.addActionListener(new SupList());

        JRAllList.addActionListener(new SelectList());
        JRSeleList.addActionListener(new SelectList());

        JRDate.addActionListener(new BaseList());
        JROrdNo.addActionListener(new BaseList());
    }
    public class UnitList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllUnit)
            {
                JRAllUnit.setSelected(true);
                JRSeleUnit.setSelected(false);
                JCUnit.setEnabled(false);
            }
            if(ae.getSource()==JRSeleUnit)
            {
                JRAllUnit.setSelected(false);
                JRSeleUnit.setSelected(true);
                JCUnit.setEnabled(true);
            }
        }
    }

    public class DeptList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllDept)
            {
                JRAllDept.setSelected(true);
                JRSeleDept.setSelected(false);
                JCDept.setEnabled(false);
            }
            if(ae.getSource()==JRSeleDept)
            {
                JRAllDept.setSelected(false);
                JRSeleDept.setSelected(true);
                JCDept.setEnabled(true);
            }
        }
    }
    public class GroupList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllGroup)
            {
                JRAllGroup.setSelected(true);
                JRSeleGroup.setSelected(false);
                JCGroup.setEnabled(false);
            }
            if(ae.getSource()==JRSeleGroup)
            {
                JRAllGroup.setSelected(false);
                JRSeleGroup.setSelected(true);
                JCGroup.setEnabled(true);
            }
        }
    }
    public class SupList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllSup)
            {
                JRAllSup.setSelected(true);
                JRSeleSup.setSelected(false);
                JCSup.setEnabled(false);
            }
            if(ae.getSource()==JRSeleSup)
            {
                JRAllSup.setSelected(false);
                JRSeleSup.setSelected(true);
                JCSup.setEnabled(true);
            }
        }
    }
    public class SelectList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllList)
            {
                JRAllList.setSelected(true);
                JRSeleList.setSelected(false);
                JCList.setEnabled(false);
            }
            if(ae.getSource()==JRSeleList)
            {
                JRAllList.setSelected(false);
                JRSeleList.setSelected(true);
                JCList.setEnabled(true);
            }
        }
    }
    public class BaseList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRDate)
            {
                bsig = true;
                JRDate.setSelected(true);
                JROrdNo.setSelected(false);
                addControlPanel();
            }
            if(ae.getSource()==JROrdNo)
            {
                bsig = false;
                JROrdNo.setSelected(true);
                JRDate.setSelected(false);
                addControlPanel();
            }
        }
    }
    public String getSelectedUnit()
    {
       return (String)JCUnit.getSelectedItem();
    }

    public String getSelectedDept()
    {
       return (String)JCDept.getSelectedItem();
    }

    public void getUDGBS()
    {
        VUnit      = new Vector();
        VUnitCode  = new Vector();

        VDept      = new Vector();
        VDeptCode  = new Vector();

        VGroup     = new Vector();
        VGroupCode = new Vector();

        VSup       = new Vector();
        VSupCode   = new Vector();

        try
        {
               if(theconnect==null)
               {
                    connect=ORAConnection.getORAConnection();
                    theconnect=connect.getConnection();
               }
               Statement stat  = theconnect.createStatement();

               String QS1 = "";
               String QS2 = "";
               String QS3 = "";

               QS1 = "Select Dept_Name,Dept_Code From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name";
               QS2 = "Select Group_Name,Group_Code From Cata Where MillCode=2 or MillCode="+iMillCode+" Order By 1";
               QS3 = "Select Unit_Name,Unit_Code From Unit Where MillCode=2 or MillCode="+iMillCode+" Order By Unit_Name";

               ResultSet result1 = stat.executeQuery(QS1);
               while(result1.next())
               {
                    VDept.addElement(result1.getString(1));
                    VDeptCode.addElement(result1.getString(2));
               }
               result1.close();

               ResultSet result2 = stat.executeQuery(QS2);
               while(result2.next())
               {
                    VGroup.addElement(result2.getString(1));
                    VGroupCode.addElement(result2.getString(2));
               }
               result2.close();

               ResultSet result3 = stat.executeQuery(QS3);
               while(result3.next())
               {
                    VUnit.addElement(result3.getString(1));
                    VUnitCode.addElement(result3.getString(2));
               }
               result3.close();

               ResultSet result4 = stat.executeQuery("Select Name,Ac_Code From "+SSupTable+" Order By Name");
               while(result4.next())
               {
                    VSup.addElement(result4.getString(1));
                    VSupCode.addElement(result4.getString(2));
               }
               result4.close();
               stat.close();
        }
        catch(Exception ex)
        {
            System.out.println("Unit,Dept,Group & Supplier :"+ex);
        }
    }

}
