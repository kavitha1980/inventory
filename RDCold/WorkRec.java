package RDC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;

import jdbc.*;
import util.*;
import guiutil.*;

public class WorkRec 
{
     JInternalFrame theFrame;

     JPanel         TopPanel,BottomPanel;

     JTextField     TDesc;
     JTextField     TRemarks;
     JTextField     TMacCode;

     DateField      TDueDate;
     NextField      TQty;

     MyComboBox     JCDept;
     MyComboBox     JCUnit;

     JButton        BMac;
     JButton        BOk;

     JLayeredPane   Layer;
     Vector         VMac,VDept,VUnit,VUnitCode,VDeptCode;
     Object         RowData[][];
     String         Id,SDate;
     int            iActivation;
     RDCOutFrame    rdcoutframe;

     Common common = new Common();

     public WorkRec(JLayeredPane Layer,Object[][] RowData,String Id,RDCOutFrame rdcoutframe)
     {
          this.Layer     = Layer;
          this.RowData   = RowData;
          this.Id        = Id;
          iActivation    = 0;
          this.rdcoutframe = rdcoutframe;
          this.SDate     = SDate;

          createComponents();
     }
     public void setActivation()
     {
          if(iActivation > 0)
               return;
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          theFrame       = new JInternalFrame();
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();

          TRemarks       = new JTextField();
          BOk            = new JButton("Submit");
          BOk.setMnemonic('U');
     }

     public void setLayouts()
     {
          theFrame.setClosable(true);
          theFrame.setMaximizable(true);
          theFrame.setIconifiable(true);
          theFrame.setResizable(true);
          theFrame.setBounds(0,0,550,300);
          theFrame.setTitle("Details about the goods despatched");
          theFrame.getContentPane().setLayout(new BorderLayout());
          TopPanel.setLayout(new GridLayout(7,2));
          BottomPanel.setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          TopPanel.add(new JLabel("Purpose"));
          TopPanel.add(TRemarks);

          BottomPanel.add(BOk);

          theFrame.getContentPane().add(TopPanel,BorderLayout.NORTH);
          theFrame.getContentPane().add(BottomPanel,BorderLayout.SOUTH);

          theFrame.setVisible(false);
     }

     public void addListeners()
     {
          BOk.addActionListener(new ActList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               int i         = common.toInt(Id);
               RowData[i][18] = TRemarks.getText();

               offThisFrame();
          }
     }

     public void offThisFrame()
     {
          theFrame.setVisible(false);
          Layer.updateUI();
          rdcoutframe.tabreport.ReportTable.requestFocus();
     }

     public void onThisFrame()
     {
          try
          {
               Layer.add(theFrame);
               iActivation++;
               theFrame.moveToFront();
               Layer.repaint();
               Layer.updateUI();
               theFrame.setVisible(true);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}
