package ReverseCharge;

import java.sql.*;
import java.util.*;

import util.*;

public class RCMAvailedClass
{
	private int iActiveStatus;
	private ArrayList AHSNCode;
	private Common common;
	private String SErrorMsg;
	
	public RCMAvailedClass() {
		common = null;
		common = new Common();
	}
	
	public void setData() {
		try {
			iActiveStatus = 0;
			SErrorMsg = "";
			
			AHSNCode = null;
			AHSNCode = new ArrayList();
			AHSNCode . clear();
			
			// getConnection..
               Class . forName("oracle.jdbc.OracleDriver");
               Connection theConnection	= java.sql.DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "alpa", "alpa");
			PreparedStatement thePS = null;
			ResultSet rs = null;
			
			// get Active Status..
			thePS = theConnection.prepareStatement("Select ActiveStatus from RCMAvailedEnabled");
			rs = thePS.executeQuery();
			
			if(rs.next()) {
				iActiveStatus = common.toInt(rs.getString(1));
			}
			
			rs.close();
			thePS.close();
			
			// get HSN Code Data..
			thePS = theConnection.prepareStatement("Select HSNCode from RCMAvailedHSNCode where to_Char(SysDate, 'YYYYMMDD') between EffectiveFrom and EffectiveTo ");
			rs = thePS.executeQuery();
			
			if(rs.next()) {
				AHSNCode . add(common.parseNull(rs.getString(1)));
			}
			
			rs.close();
			thePS.close();

			// close Connection..
			theConnection . close();
			theConnection = null;
		}catch(Exception ex) {
			SErrorMsg = ex.getMessage();
			ex.printStackTrace();
		}
	}	
	
	public boolean checkRCMAvailedHSN(String SHSNCode) {
		if(iActiveStatus == 0) {
			return true;
		}
		
		if(AHSNCode.contains(SHSNCode)) {
			return true;
		}
		
		return false;
	}
	
	public String getErrorMsg() {
		return SErrorMsg;
	}
}