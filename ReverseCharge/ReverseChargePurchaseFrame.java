package ReverseCharge;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.*;
import jdbc.*;
import util.*;
import guiutil.*;


public class ReverseChargePurchaseFrame extends JInternalFrame 
{

    JPanel                  pnlTop, pnlMiddle, pnlBottom, pnlDate,pnlInsert,pnlPrint,pnlPrintTop,pnlPrintData,pnlPrintBot;
    FinDateField            txtFromDate, txtToDate;
    NextField               txtGiNo;
    JTextField              txtRoundOff;
    JButton                 btnSave, btnApply, btnExit,btnPrint;
    JTable                  theTable = null,thePrintTable=null;
    JLabel                  lblGiNo, lblGiDate, lblSupplier, lblGSTStateCode,lblGSTId,lblGSTPartyType,lblTotalInvoice,lblNetAmount;
    JTabbedPane             tabReport;
    ReverseChargePurchaseModel       theModel = null;
    ReverseChargePurchasePrintModel  thePrintModel=null;
    String                  SFromDate, SToDate, SDString,SPartyCode="",SGSTInNo="",SReceiptVoucherNo="",SReceiptVoucherDate="";
    ArrayList               ADataList = null,APrintDataList =   null,ATaxDetailsList = null,theList = null;
    Common                  common = null;
    Vector                  VPartyCode, VPartyName, VItemCode, VItemName,VItemHSNCode,VClaimStatus,VGstTypeCode,VHsnType,VPartyStateCode,VItemDetails;
    Vector                  VHSNCode, VGSTRate, VCGSTPer, VSGSTPer, VIGSTPer,VGSTPartyTypeCode,VGSTPartyType,VTaxCatCode;
    int                     iGiNo;
    String                  SFileLocation = "";
    int                     iUserCode = 0 ,iDivisionCode,iMaxLimit=0,iMillCode;
    String                  SYearCode = "";
    JLayeredPane            DeskTop;
    // set Model Index   
        int SNo=0,GateInwItem=1,ItemCode=2,ItemName=3,HSNCode=4,Rate=5,Qty=6,Value=7,DiscPer=8,Discount=9,BillValue=10,CGRatePer=11,CGValue=12,SGRatePer=13,SGValue=14,IGRatePer=15,IGValue=16,TotalValue=17,Select=18;
    // set Print Model Index
        int PSNo=0,PGINo=1,PItemName=2 ,PHSNCode=3,PRate=4,PQty=5,PValue=6,PCGRatePer=7,PCGValue=8,PSGRatePer=9,PSGValue=10,PIGRatePer=11,PIGRateValue=12,PTotalValue=13,PVoucherCode=14,PSelect=15;
    public ReverseChargePurchaseFrame(JLayeredPane DeskTop,int iUserCode,int iDivisionCode,String SYearCode,int iMillCode) 
    {
		try
        {
            this.DeskTop    = DeskTop;
            this.iUserCode  = iUserCode;
            this.iDivisionCode = iDivisionCode;
            this.SYearCode     = SYearCode;
            this.iMillCode     = iMillCode;
        }
		catch(Exception e)
        {
            e.printStackTrace();
        }
        common   = new Common();
        SDString = "-Select-";
        SFileLocation   = common.getPrintPath()+"TaxInvoice.Pdf";
        setHSNVector();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
    }

    private void createComponents() 
    {
        pnlTop      = new JPanel();
        pnlMiddle   = new JPanel();
        pnlBottom   = new JPanel();
        pnlDate     = new JPanel();
        pnlInsert   = new JPanel();
        pnlPrint    = new JPanel();
        pnlPrintTop = new JPanel();
        pnlPrintData= new JPanel();
        pnlPrintBot = new JPanel();
        
        txtFromDate = new FinDateField();
        txtToDate   = new FinDateField();

        txtGiNo     = new NextField();
        txtRoundOff = new JTextField();

        btnApply    = new JButton("Apply");
        btnSave     = new JButton("Save");
        btnExit     = new JButton("Exit");
        btnPrint    = new JButton("Print Voucher");

        lblGiNo     = new JLabel("");
        lblGiDate   = new JLabel("");
        lblSupplier = new JLabel("");
        lblGSTPartyType = new JLabel("");
        lblGSTStateCode = new JLabel("");
        lblGSTId        = new JLabel("");
        lblTotalInvoice = new JLabel("");
        lblNetAmount    = new JLabel("");
        
        tabReport   = new JTabbedPane();
        tabReport   . add("Cash Purchase",pnlInsert);
        tabReport   . add("Cash Purchase Print",pnlPrint);
        theModel    = new ReverseChargePurchaseModel(lblTotalInvoice,lblNetAmount);
        theTable    = new JTable();

        thePrintModel   = new ReverseChargePurchasePrintModel();
        thePrintTable   = new JTable(thePrintModel);
        
        
        theTable.setColumnModel(new GroupableTableColumnModel());
        theTable.setTableHeader(new GroupableTableHeader((GroupableTableColumnModel) theTable.getColumnModel()));
        theTable.setModel(theModel);

        // Setup Column Groups
        GroupableTableColumnModel cm = (GroupableTableColumnModel) theTable.getColumnModel();
        ColumnGroup g_CGST = new ColumnGroup("CGST");
        g_CGST.add(cm.getColumn(CGRatePer));
        g_CGST.add(cm.getColumn(CGValue));
        cm.addColumnGroup(g_CGST);

        ColumnGroup g_SGST = new ColumnGroup("SGST");
        g_SGST.add(cm.getColumn(SGRatePer));
        g_SGST.add(cm.getColumn(SGValue));
        cm.addColumnGroup(g_SGST);

        ColumnGroup g_IGST = new ColumnGroup("IGST");
        g_IGST.add(cm.getColumn(IGRatePer));
        g_IGST.add(cm.getColumn(IGValue));
        cm.addColumnGroup(g_IGST);

        theTable.setCellSelectionEnabled(true);
    }

    private void setLayouts() 
    {
        pnlTop      .   setBorder(new TitledBorder("Filter"));
        pnlMiddle   .   setBorder(new TitledBorder("Data"));
        pnlBottom   .   setBorder(new TitledBorder("Controls"));
        
        pnlPrintTop .   setBorder(new TitledBorder("Filter"));
        pnlPrintData.   setBorder(new TitledBorder("Data"));
        pnlPrintBot .   setBorder(new TitledBorder("Controls"));        

        pnlTop      .   setLayout(new GridLayout(3, 4, 3, 3));
        pnlMiddle   .   setLayout(new BorderLayout());
        pnlBottom   .   setLayout(new GridLayout(2, 6));
        pnlInsert   .   setLayout(new BorderLayout());
        pnlPrint    .   setLayout(new BorderLayout());
        pnlPrintTop .   setLayout(new FlowLayout(FlowLayout.CENTER));
        pnlPrintData.   setLayout(new BorderLayout());
        pnlPrintBot .   setLayout(new FlowLayout(FlowLayout.CENTER));

        this.setTitle("Cash Purchase Frame");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this.setMaximizable(true);
        this.setClosable(true);
    }

    private void addComponents() 
    {
        pnlTop.add(new JLabel("GI. No."));
        pnlTop.add(txtGiNo);
        pnlTop.add(new JLabel("GI. Date"));
        pnlTop.add(lblGiDate);
        
        pnlTop.add(new JLabel("Supplier"));
        pnlTop.add(lblSupplier);
        pnlTop.add(new JLabel("GST StateCode"));
        pnlTop.add(lblGSTStateCode);

        pnlTop.add(new JLabel("GST ID"));
        pnlTop.add(lblGSTId);
        pnlTop.add(new JLabel("Party Type"));
        pnlTop.add(lblGSTPartyType);
        
        pnlMiddle.add(new JScrollPane(theTable));

        pnlBottom.add(btnSave);
        pnlBottom.add(btnExit);
        pnlBottom.add(new JLabel(" Total  "));
        pnlBottom.add(lblTotalInvoice);
        pnlBottom.add(new JLabel("  RoundOff "));
        pnlBottom.add(txtRoundOff);

        pnlBottom.add(new JLabel(""));
//        pnlBottom.add(new JLabel("ItemSelection ===> Press Insert Key In ItemName"));
        pnlBottom.add(new JLabel("ItemSelection =="));
        pnlBottom.add(new JLabel("=> Double  Click"));
        pnlBottom.add(new JLabel("In ItemName  "));
        pnlBottom.add(new JLabel("  Net "));
        pnlBottom.add(lblNetAmount);


        pnlInsert.add(pnlTop, BorderLayout.NORTH);
        pnlInsert.add(pnlMiddle, BorderLayout.CENTER);
        pnlInsert.add(pnlBottom, BorderLayout.SOUTH);
        
        
        pnlPrintTop .   add(new JLabel("VOUCHER PRINT SCREEN"));
        
        pnlPrintData.   add(new JScrollPane(thePrintTable));
        
        pnlPrintBot .   add(btnPrint);
        
        pnlPrint    .   add(pnlPrintTop,BorderLayout.NORTH);
        pnlPrint    .   add(pnlPrintData,BorderLayout.CENTER);
        pnlPrint    .   add(pnlPrintBot,BorderLayout.SOUTH);

        this     . add(tabReport);
         
        theTable.getColumn("ItemCode").setPreferredWidth(120);
        theTable.getColumn("ItemName").setPreferredWidth(250);
        theTable.getColumn("GateInwItem").setPreferredWidth(250);

    }
    private void addListeners() 
    {
     //   txtGiNo     .   addFocusListener(new FocusList());
        txtGiNo      .   addKeyListener(new KeyList());
        btnApply     .   addActionListener(new ActList());
        btnSave      .   addActionListener(new ActList());
        btnExit      .   addActionListener(new ActList());
        btnPrint     .   addActionListener(new ActList());
//        theTable    .   addKeyListener(new TabKeyList());
        theTable     .   addMouseListener(new MouseList());
        tabReport    .   addChangeListener(new ChangeList());
        txtRoundOff  .   addFocusListener(new FocusList());
    }

/*    private class FocusList extends FocusAdapter 
    {
        public void focusLost(FocusEvent fe) 
        {
            if (fe.getSource() == txtGiNo) 
            {
                iGiNo = common.toInt(txtGiNo.getText());
                setTableData();
                SPartyCode      =    getPartyCode(lblSupplier.getText());
                if(!validAddress(SPartyCode))
                {
                   JOptionPane.showMessageDialog(null,"Not a Valid Party Address\n Plz Update the Party Address ");
                   txtGiNo	.	setText("");
				   clearData();
                   theModel	.	setNumRows(0);
                    
			    }		
            }
        }
    }*/

    private class ActList implements ActionListener 
    {
         public void actionPerformed(ActionEvent ae) 
         {
            if (ae.getSource() == btnApply) 
            {
                iGiNo = common.toInt(txtGiNo.getText());
                setTableData();
            }
            if (ae.getSource() == btnSave) 
            {
                SPartyCode      =    getPartyCode(lblSupplier.getText());
//                if(validAddress(SPartyCode))
  //              {
		            if(isItemsSelected())
		            {
		                int iSaveVal = insertData();
		                if (iSaveVal > 0) 
		                {
		                    JOptionPane.showMessageDialog(null, "Data Saved Sucessfully");
		                } 
		                else 
		                {
		                    JOptionPane.showMessageDialog(null, "Data Not Saved");
		                }
		                tabReport       .   setSelectedIndex(1);
		                setPrintTableData();
		                setTableData();
		            }
              /*  }
                else
                {
                   JOptionPane.showMessageDialog(null,"Not a Valid Party Address\n Plz Update the Party Address ");
                }  */

            }
            if (ae.getSource() == btnExit) 
            {
                dispose();
            }
            if (ae.getSource() == btnPrint) 
            {
                printVoucher();
            }            

        }
    }
    private class FocusList extends FocusAdapter
    {
        public void focusLost(FocusEvent fe)
        {
            if(fe.getSource() == txtRoundOff)
            {
                double dTotal      = common.toDouble(lblTotalInvoice.getText());
                double dRoundOff   = common.toDouble(txtRoundOff.getText());
                lblNetAmount       . setText(common.getRound(dTotal+(dRoundOff),2));
                
            }
        }
    }

    /*private class TabKeyList extends KeyAdapter 
    {
        public void keyReleased(KeyEvent ke) 
        {
            int iRow = theTable.getSelectedRow();
            int iCol = theTable.getSelectedColumn();
            if (ke.getKeyCode() == KeyEvent.VK_INSERT) 
            {
				setHSNVector();
                if (iCol == ItemName) 
                {
                     InvItemSearch itemsearch = new InvItemSearch(theModel,iRow,iCol,VItemCode,VItemName,VItemHSNCode);
                     itemsearch      .   setVisible(true);
                }
            }
        }
    }
*/
    private class MouseList extends MouseAdapter
    {
       public void mouseClicked(MouseEvent me)
       {
            int iRow = theTable.getSelectedRow();
            int iCol = theTable.getSelectedColumn();
             if(me.getClickCount()==2)
             {
                if (iCol == ItemName) 
                {
                     InvItemSearch itemsearch = new InvItemSearch(theModel,iRow,iCol,VItemCode,VItemName,VItemHSNCode,VItemDetails);
                     itemsearch      .   setVisible(true);
                }
             }
       }
    }

    private class ChangeList implements ChangeListener
    {
        public void stateChanged(ChangeEvent ce)
        {
            if(tabReport.getSelectedIndex()==1)
            {   
                iGiNo           =   common.toInt(txtGiNo.getText());
              //  getPartyCode(lblSupplier.getText());
                setPrintTableData();
            }
        }
    }
    private  class KeyList extends KeyAdapter
    {
        public void keyReleased(KeyEvent ke)
        {
             if(ke.getKeyCode() == KeyEvent.VK_ENTER)
             {
                iGiNo = common.toInt(txtGiNo.getText());
                setTableData();
				if (ADataList.size() > 0) 
				{
		            SPartyCode      =    getPartyCode(lblSupplier.getText());
		            if(!validAddress(SPartyCode))
		            {
		               JOptionPane.showMessageDialog(null,"Not a Valid Party Address\n Plz Update the Party Address ");
		               txtGiNo	.	setText("");
					   clearData();
		               theModel	.	setNumRows(0);
					}		
               }
             }
        }
    }
    private void setTableData() 
    {
        theModel.setNumRows(0);
        setVector();
        if (ADataList.size() == 0) 
        {
            clearData();
            return;
        }
        HashMap theHMap = (HashMap) ADataList.get(0);
        lblGiDate       .   setText(common.parseDate((String) theHMap.get("GiDate")));
        lblSupplier     .   setText((String) theHMap.get("PartyName"));
        lblGSTPartyType .   setText(getPartyType((String) theHMap.get("GSTPartyTypeCode")));
        lblGSTStateCode .   setText((String) theHMap.get("GSTStateCode"));
        lblGSTId        .   setText((String)theHMap.get("GSIInId"));
        theModel.iGSTStateCode = common.toInt(String.valueOf(lblGSTStateCode.getText()));
        
        for (int i = 0; i < ADataList.size(); i++) 
        {
            Vector theVect = new Vector();

            HashMap theMap = (HashMap) ADataList.get(i);
            theVect.addElement(String.valueOf(i + 1));
            theVect.addElement((String) theMap.get("ItemName"));
            theVect.addElement(SDString); // InvItems  ItemCode
            theVect.addElement(SDString); // InvItems  ItemName
            theVect.addElement("");   // For HSNCode
            theVect.addElement("");   // For Rate
            theVect.addElement(common.toDouble((String) theMap.get("GateQty")));
            theVect.addElement(common.toDouble((String) theMap.get("Value")));
            theVect.addElement(""); // For DiscPer
            theVect.addElement(common.toDouble("0.0"));
            theVect.addElement(common.toDouble("0.0"));
            theVect.addElement(common.toDouble("0.0"));
            theVect.addElement(common.toDouble("0.0"));
            theVect.addElement(common.toDouble("0.0"));
            theVect.addElement(common.toDouble("0.0"));
            theVect.addElement(common.toDouble("0.0"));
            theVect.addElement(common.toDouble("0.0"));
            theVect.addElement(common.toDouble("0.0"));
            theVect.addElement(new Boolean(true));
            theModel.appendRow(theVect);
        }
    }
    private void setPrintTableData()
    {
        thePrintModel   .   setNumRows(0);
        setPrintVector();
        for(int i=0;i<APrintDataList.size();i++)
        {
            HashMap theMap =    (HashMap)APrintDataList.get(i);
            
            Vector theVect =    new Vector();
            theVect        .    addElement(String.valueOf(i+1));
            theVect        .    addElement((String)theMap.get("GINO"));
            theVect        .    addElement((String)theMap.get("DESCRIPTION"));
            theVect        .    addElement((String)theMap.get("HSNCODE"));
            theVect        .    addElement((String)theMap.get("RATE"));
            theVect        .    addElement((String)theMap.get("SUPQTY"));
            theVect        .    addElement((String)theMap.get("TAXVALUE"));
            theVect        .    addElement((String)theMap.get("CGST_PER"));
            theVect        .    addElement((String)theMap.get("CGST_VAL"));
            theVect        .    addElement((String)theMap.get("SGST_PER"));
            theVect        .    addElement((String)theMap.get("SGST_VAL"));            
            theVect        .    addElement((String)theMap.get("IGST_PER"));
            theVect        .    addElement((String)theMap.get("IGST_VAL"));            
            theVect        .    addElement((String)theMap.get("TOTALVALUE"));  
            theVect        .    addElement((String)theMap.get("VOUCHERCODE"));            
            theVect        .    addElement(new Boolean(true));
            thePrintModel  .    appendRow(theVect);
        }
    }
    private void printVoucher()
    {
       if(isPrintAll())
       { 
				new TaxInvoicePdf(SPartyCode,SReceiptVoucherNo,SReceiptVoucherDate,APrintDataList);
		        JOptionPane . showMessageDialog(null,"Voucher File Created in "+SFileLocation);
       }
       thePrintModel    .   setNumRows(0);
       tabReport        .   setSelectedIndex(0);
       
    }
    private int insertData() 
    {
        int iCount = 0;
        ATaxDetailsList = new ArrayList();
        theList = new ArrayList();
        
        Vector VCGST = new Vector();
        Vector VSGST = new Vector();
        Vector VIGST = new Vector();
        
        int iGSTPartyTypeCode 	= getPartyTypeCode(lblGSTPartyType.getText());
        int iSave			= 0;

        if(iGSTPartyTypeCode==0 || iGSTPartyTypeCode==2)
        {
           JOptionPane.showMessageDialog(null,lblGSTPartyType.getText()+" Party ");
           return iSave;
        }

	   int iRCMStatus = 0;
		RCMAvailedClass theRCMAvailedClass = null;
		theRCMAvailedClass = new RCMAvailedClass();
		
		theRCMAvailedClass . setData();
		
		if(theRCMAvailedClass.getErrorMsg().trim().length() > 0) {
            	JOptionPane.showMessageDialog(null, "Error while retrieving RCM Availed HSN Codes.. \n Error Detail : "+theRCMAvailedClass.getErrorMsg().trim(), "Information", JOptionPane.ERROR_MESSAGE);
			return iSave;	
		}
	   
         String SVoucherType = "";
         if(iGSTPartyTypeCode	==	3)
  	     {
             SVoucherType = "RCVOCNO";
		   iRCMStatus = 1;
         }
         else if(iGSTPartyTypeCode	==	1)
         {
             SVoucherType = "RPVOCNO";
         }

        String SInvoiceQS   = "";
        SInvoiceQS			= getInvoiceInsertQS(iGSTPartyTypeCode);
        
//         System.out.println("QS-->"+sb.toString());
        
        JDBCConnection1 connect    = JDBCConnection1.getJDBCConnection();
        Connection theConnection   = connect.getConnection();


        ORAConnection oraconnect      = ORAConnection.getORAConnection();
        Connection theConnect         = oraconnect.getConnection();

        try 
        {
            
            if(theConnection.getAutoCommit())
               theConnection.setAutoCommit(false);
            String SMaxId 		=  "";
            int    iMaxNumber	=  1;

//			PreparedStatement theState     = theConnection.prepareStatement("Select to_char(sysdate,'yyyy')||'-'|| (MaxNUmber+1),(MaxNumber+1)  From NoConfig Where Divisioncode="+iDivisionCode+" and YearCode="+SYearCode+" and VocType='RCVOCNO'  for update of MaxNumber noWait"); 
			PreparedStatement theState     = theConnection.prepareStatement("Select to_char(sysdate,'yyyy')||'-'|| (MaxNUmber+1),(MaxNumber+1)  From NoConfig Where Divisioncode=1 and YearCode="+SYearCode+" and VocType='"+SVoucherType+"'  for update of MaxNumber noWait"); 

            ResultSet         theResult    = theState.executeQuery();
            while(theResult.next())
            {
               if(iGSTPartyTypeCode	==	3)
         	   {
                 SMaxId  	= theResult.getString(1);
               }
               else if(iGSTPartyTypeCode	==	1)
               {
                 SMaxId  	= theResult.getString(2);
               }
               iMaxNumber	= theResult.getInt(2);	
            }

            theResult     . close();
            theState  . close();                            
     
            PreparedStatement theStatement = theConnection.prepareStatement(SInvoiceQS);
            for (int i = 0; i < theModel.getRowCount(); i++) 
            {
                boolean bSelect = ((Boolean) theModel.getValueAt(i, Select)).booleanValue();
                if (!bSelect) 
                {
                    continue;
                }
                else
                  iCount++;
                
                String SGiNo        = txtGiNo.getText();
                String SGiDate      = common.pureDate(lblGiDate.getText());
                String SSupCode     = getPartyCode(lblSupplier.getText());
                String SGateInwItem = (String) theModel.getValueAt(i, GateInwItem);
                String SItemCode    = (String) theModel.getValueAt(i, ItemCode);
                String SItemName    = (String) theModel.getValueAt(i, ItemName);
                String SInvNo       = getInvoiceNo(SGiNo);
                String SInvDate     = getInvoiceDate(SGiNo);
                String SGRNNo 		= "0";
                String SGRNDate     = "0";
                String SHsnCode     = common.parseNull((String) theModel.getValueAt(i, HSNCode));
                double dRate        = common.toDouble(String.valueOf(theModel.getValueAt(i, Rate)));
			
			if(iRCMStatus == 1) {
				if(theRCMAvailedClass.checkRCMAvailedHSN(SHsnCode)) {
					// No Action...
				}else {
					continue;
				}
			}
			
                if (SItemCode.equals(SDString) || SItemName.equals(SDString)) 
                {
                    JOptionPane.showMessageDialog(null, "Not a Valid InvItem for " + SGateInwItem);
                    break;
                }
                if(SHsnCode.length()<=0)
                {
                    JOptionPane.showMessageDialog(null, "Plz Update HSNCode for Selected Item " + SGateInwItem);
                    break;
                }
                if(dRate<=0)
                {
                    JOptionPane.showMessageDialog(null, "Rate Must be Entered for "  + SGateInwItem);
                    break;
                }
              

                double dQty         = common.toDouble(String.valueOf(theModel.getValueAt(i, Qty)));
                double dValue       = common.toDouble(String.valueOf(theModel.getValueAt(i, Value)));
                double dCGSTPer     = common.toDouble(String.valueOf(theModel.getValueAt(i, CGRatePer)));
                double dCGSTValue   = common.toDouble(String.valueOf(theModel.getValueAt(i, CGValue)));
                double dSGSTPer     = common.toDouble(String.valueOf(theModel.getValueAt(i, SGRatePer)));
                double dSGSTValue   = common.toDouble(String.valueOf(theModel.getValueAt(i, SGValue)));
                double dIGSTPer     = common.toDouble(String.valueOf(theModel.getValueAt(i, IGRatePer)));
                double dIGSTValue   = common.toDouble(String.valueOf(theModel.getValueAt(i, IGValue)));
                double dTotalValue  = common.toDouble(String.valueOf(theModel.getValueAt(i, TotalValue)));
               // String SPayVocCode  = common.getCurrentDate().substring(0,4)+"-"+(iMaxId<10?"0"+String.valueOf(iMaxId):String.valueOf(iMaxId)); //"CP"+
                double dDiscPer     = common.toDouble(String.valueOf(theModel.getValueAt(i, DiscPer)));
                double dDiscValue   = common.toDouble(String.valueOf(theModel.getValueAt(i, Discount)));
                double dRoundOff    = common.toDouble(txtRoundOff.getText());
                double dNetAmount   = common.toDouble(lblNetAmount.getText());
                int    iClaimStatus = getClaimStatus(SItemCode);
                int    iTaxTypeCode = getTaxCatCode(SHsnCode);
                int    iHsnType     = getHsnType(SItemCode);
                int iImportTypeCode = getImportTypeCode(iHsnType);
                int    iGstTypeCode = getGstTypeCode(SItemCode);

                if(!getValidation(SItemName,SHsnCode,iTaxTypeCode,iClaimStatus,iHsnType,iImportTypeCode,iGstTypeCode))
                {
					break;
                }
                if(dValue<iMaxLimit)
                {
                    JOptionPane.showMessageDialog(null, "Invoice Amount is Less than MaxLimit("+iMaxLimit+") for   " + SGateInwItem);
                    break;
				}                                
                HashMap theMap = new HashMap();
                
                theMap.put("CGSTPer",String.valueOf(dCGSTPer));
                theMap.put("CGSTVal",String.valueOf(dCGSTValue));
                if(!VCGST.contains(dCGSTPer))
                    VCGST.addElement(dCGSTPer);
                    
                theMap.put("SGSTPer",String.valueOf(dSGSTPer));
                theMap.put("SGSTVal",String.valueOf(dSGSTValue));
                if(!VSGST.contains(dSGSTPer))
                    VSGST.addElement(dSGSTPer);
                
                theMap.put("IGSTPer",String.valueOf(dIGSTPer));
                theMap.put("IGSTVal",String.valueOf(dIGSTValue));
                if(!VIGST.contains(dIGSTPer))
                    VIGST.addElement(dIGSTPer);
                
                ATaxDetailsList.add(theMap);
                
                theStatement.setString(1, SMaxId);
                theStatement.setString(2, SSupCode);
                theStatement.setInt(3,iDivisionCode );
                theStatement.setString(4, SYearCode);
                theStatement.setString(5, SGiNo);
                theStatement.setString(6, SGRNNo);
                theStatement.setString(7, SGRNDate);
                theStatement.setString(8, SItemCode);
                theStatement.setString(9, SHsnCode);
                theStatement.setDouble(10,dQty);
                theStatement.setDouble(11,dRate);
                theStatement.setDouble(12,dValue);
                theStatement.setDouble(13, dCGSTPer);
                theStatement.setDouble(14, dCGSTValue);
                theStatement.setDouble(15, dSGSTPer);
                theStatement.setDouble(16, dSGSTValue);
                theStatement.setDouble(17, dIGSTPer);
                theStatement.setDouble(18, dIGSTValue);
                theStatement.setInt(19, iUserCode);
                theStatement.setString(20, common.getLocalHostName());
                System.out.println(" LH " +common.getLocalHostName());

             //   if(iGSTPartyTypeCode==3)
               // {
	                theStatement.setDouble(21, dTotalValue);
	                theStatement.setString(22, SInvNo);
	                theStatement.setString(23, SInvDate);
               // }
                theStatement.setDouble(24, dDiscPer);
                theStatement.setDouble(25, dDiscValue);
                if(iSave==0)
                {
                  theStatement.setDouble(26, dRoundOff);
                  theStatement.setDouble(27, dNetAmount);
                }
                else
                {
                  theStatement.setDouble(26, 0);  
                  theStatement.setDouble(27, 0);  
                }
                theStatement  .setInt(28,iClaimStatus);
                theStatement  .setInt(29,iTaxTypeCode);
                theStatement  .setInt(30,iImportTypeCode);
                theStatement  .setInt(31,iGstTypeCode);

                System.out.println(" AND " +dDiscPer+","+dDiscValue+","+dRoundOff+","+dNetAmount+","+iClaimStatus+","+iTaxTypeCode+","+iImportTypeCode+","+iGstTypeCode);

                theStatement.executeUpdate();

                iSave += 1;

            }
            if(iCount==iSave)
            {      
		        String STaxQS   = "";
		        STaxQS   		= getTaxDetailsInsertQS(iGSTPartyTypeCode);

		        java.util.HashMap  themap = new java.util.HashMap();
		        java.util.ArrayList  AList = new java.util.ArrayList();
		         
		        AList = getCGSTValue(VCGST, iRCMStatus, theRCMAvailedClass);
		                 
		         for(int j=0;j<AList.size();j++)
		         {
		             HashMap theMap = (HashMap)AList.get(j);
		                            
		             String CGSTAcCode = (String)theMap.get("GSTCode");
		             double dPer       = common.toDouble((String)theMap.get("GSTPer"));
		             double dValue     = common.toDouble((String)theMap.get("GSTValue"));
		             if(dValue<=0)
		               break;     
		             theStatement = theConnection.prepareStatement(STaxQS);
		        
		            theStatement.setString(1, SMaxId);
		            theStatement.setString(2, SYearCode);
		            theStatement.setInt(3,iDivisionCode );
		            theStatement.setString(4, CGSTAcCode);
		            theStatement.setDouble(5, dPer);
		            theStatement.setDouble(6,dValue );
		            theStatement.setString(7, common.getLocalHostName());
		            
		            theStatement.executeUpdate();
		             
		         }
		         
		         AList = new java.util.ArrayList();
		         
		        AList = getSGSTValue(VSGST, iRCMStatus, theRCMAvailedClass);
		         
		         for(int j=0;j<AList.size();j++)
		         {
		             HashMap theMap = (HashMap)AList.get(j);
		            
		             String SGSTAcCode = (String)theMap.get("GSTCode");
		             double dPer       = common.toDouble((String)theMap.get("GSTPer"));
		             double dValue     = common.toDouble((String)theMap.get("GSTValue"));
		             if(dValue<=0)
		               break;     
		             
		              theStatement = theConnection.prepareStatement(STaxQS);
		        
		            theStatement.setString(1, SMaxId);
		            theStatement.setString(2, SYearCode);
		            theStatement.setInt(3,iDivisionCode );
		            theStatement.setString(4, SGSTAcCode);
		            theStatement.setDouble(5, dPer);
		            theStatement.setDouble(6,dValue );
		            theStatement.setString(7, common.getLocalHostName());
		            
		            theStatement.executeUpdate();
		             
		         }
		         
		          AList = new java.util.ArrayList();
		         
		        AList = getIGSTValue(VIGST, iRCMStatus, theRCMAvailedClass);
		         
		         for(int j=0;j<AList.size();j++)
		         {
		             HashMap theMap = (HashMap)AList.get(j);
		            
		             String IGSTAcCode = (String)theMap.get("GSTCode");
		             double dPer       = common.toDouble((String)theMap.get("GSTPer"));
		             double dValue     = common.toDouble((String)theMap.get("GSTValue"));
		             if(dValue<=0)
		               break;     
		             
		              theStatement = theConnection.prepareStatement(STaxQS);
		        
		            theStatement.setString(1, SMaxId);
		            theStatement.setString(2, SYearCode);
		            theStatement.setInt(3,iDivisionCode );
		            theStatement.setString(4, IGSTAcCode);
		            theStatement.setDouble(5, dPer);
		            theStatement.setDouble(6,dValue );
		            theStatement.setString(7, common.getLocalHostName());
		            
		            theStatement.executeUpdate();
		             
		         }
		        
		        theStatement    .   close();
		        
//		        theStatement    =   theConnection.prepareStatement(" Update NoConfig set MaxNumber="+iMaxNumber+" where Divisioncode="+iDivisionCode+" and YearCode="+SYearCode+" and VocType='RCVOCNO'  ");
		        theStatement    =   theConnection.prepareStatement(" Update NoConfig set MaxNumber="+iMaxNumber+" where Divisioncode=1 and YearCode="+SYearCode+" and VocType='"+SVoucherType+"'  ");
		        theStatement    .   executeUpdate();

		        theStatement    .   close();


		        theStatement    =   theConnect.prepareStatement(" Update GateInward set CashReceiptStatus=1,InwNo=3 where GiNo="+txtGiNo.getText());
		        theStatement    .   executeUpdate();

		        theStatement    .   close();

		        theConnection . commit();
		        theConnection . setAutoCommit(true);
            }

        } 
        catch (Exception ex) 
        {
            System.out.println(" insertdata "+ex);
            ex.printStackTrace();
            String SException = String.valueOf(ex);
            if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
            {
                 JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                 try
                 {
                 }catch(Exception Ex)
                 {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                 }
                 insertData();
            }
            else
            {
				    try
				    {
				        theConnection   .   rollback();
				    }
				    catch(Exception e)
				    {
				        System.out.println(" rollback "+ex);
				        e.printStackTrace();
				    }
            }
        }
        return iSave;
    }
    private String getInvoiceInsertQS(int iGSTPartyTypeCode)
    {
         String SCurrentDate 	= common.getCurrentDate();
         StringBuffer sb	=	new StringBuffer();

         if(iGSTPartyTypeCode	==	3)
  	     {
		     sb.append(" Insert into ReverseCharge_Invoice(ID,ReceiptVocNo,EntryDate,SupCode,DivisionCode,YearCode,GiNo,GRNNo,GRNDate,ItemCode,");
		     sb.append(" HsnCode,InvQty,InvRate, ");
		     sb.append(" InvAmount,CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,UserCode,CreationDate,ComputerName,TotalValue,InvNo,InvDate,DiscPer,Discount,RoundOff,NetAmount,");
             sb.append(" ClaimStatus,TaxTypeCode,ImportTypeCode,GstTypeCode) ");
		     sb.append(" Values(ReverseCharge_Invoice_Seq.nextVal,?,sysdate,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,Sysdate,?,?,?,?,?,?,?,?,?,?,?,?)");
  
//		     sb.append(" Values(ReverseCharge_Invoice_Seq.nextVal,?,to_Date("+SCurrentDate+",'yyyymmdd'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,Sysdate,?,?,?,?,?,?,?,?,?,?,?,?)");
         }
         else
         {
		     sb.append(" Insert into RegisteredParty_Invoice(ID,ReceiptVocNo,EntryDate,SupCode,DivisionCode,YearCode,GiNo,GRNNo,GRNDate,ItemCode,");
		     sb.append(" HsnCode,InvQty,InvRate, ");
		     sb.append(" InvAmount,CGST,CGSTVal,SGST,SGSTVal,IGST,IGSTVal,UserCode,CreationDate,ComputerName,TotalValue,InvNo,InvDate,DiscPer,Discount,RoundOff,NetAmount,");
             sb.append(" ClaimStatus,TaxTypeCode,ImportTypeCode,GstTypeCode) ");
		     sb.append(" Values(RegisteredParty_Invoice_Seq.nextVal,?,sysdate,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,Sysdate,?,?,?,?,?,?,?,?,?,?,?,?)");
//		     sb.append(" Values(RegisteredParty_Invoice_Seq.nextVal,?,to_Date("+SCurrentDate+",'yyyymmdd'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,Sysdate,?,?,?,?,?,?,?,?,?,?,?,?)");
         }
         return sb.toString();
	}
    private String getTaxDetailsInsertQS(int iGSTPartyTypeCode)
	{
         String SCurrentDate 	= common.getCurrentDate();
         StringBuffer SB	=	new StringBuffer();
         if(iGSTPartyTypeCode==3)
		 {
	         SB.append(" insert into ReverseCharge_TaxDetails (ID,ReceiptVocNo,YearCode,DivisionCode,GSTAcCode,GSTPer,GSTVal,CreationDate,ComputerName) ");
    	     SB.append(" Values(ReverseCharge_TaxDetails_Seq.nextVal,?,?,?,?,?,?,Sysdate,?)");
		 }
         else
         {
	         SB.append(" insert into RegisteredParty_TaxDetails (ID,ReceiptVocNo,YearCode,DivisionCode,GSTAcCode,GSTPer,GSTVal,CreationDate,ComputerName) ");
    	     SB.append(" Values(RegisteredParty_TaxDetails_Seq.nextVal,?,?,?,?,?,?,Sysdate,?)");
         } 
         return SB.toString();

	}
    public ArrayList getCGSTValue(Vector VCGST, int iRCMStatus, RCMAvailedClass theRCMAvailedClass)
    {
        ArrayList AList = new ArrayList();
        double dValue = 0;
        
        for(int V=0;V<VCGST.size();V++)
        {
            HashMap theMap = new HashMap();
            double dCGSTPer = common.toDouble(String.valueOf(VCGST.elementAt(V)));
            
            for (int i = 0; i < theModel.getRowCount(); i++) 
            {
                boolean bSelect = ((Boolean) theModel.getValueAt(i, Select)).booleanValue();
			 
                if(bSelect) 
                {
				String SHsnCode     = common.parseNull((String) theModel.getValueAt(i, HSNCode));
				
				if(iRCMStatus == 1) {
					if(theRCMAvailedClass.checkRCMAvailedHSN(SHsnCode)) {
						// No Action...
					}else {
						continue;
					}
				}
				 
                    double dCGSTPer1     = common.toDouble(String.valueOf(theModel.getValueAt(i, CGRatePer)));
                    double dCGSTValue1   = common.toDouble(String.valueOf(theModel.getValueAt(i, CGValue)));
                    
                    if(dCGSTPer==dCGSTPer1)
                    {
                        dValue += dCGSTValue1;
                    }
                }
            }
            theMap.put("GSTCode","A_5994");
            theMap.put("GSTPer",String.valueOf(dCGSTPer));
            theMap.put("GSTValue",String.valueOf(dValue));
            
            AList.add(theMap);
        }
        return AList;
        
    }
    
     public ArrayList getSGSTValue(Vector VSGST, int iRCMStatus, RCMAvailedClass theRCMAvailedClass)
    {
        double dValue = 0;
        ArrayList AList = new ArrayList();
        
        for(int V=0;V<VSGST.size();V++)
        {
            HashMap theMap = new HashMap();
            double dSGSTPer = common.toDouble(String.valueOf(VSGST.elementAt(V)));
            
            for (int i = 0; i < theModel.getRowCount(); i++) 
            {
                boolean bSelect = ((Boolean) theModel.getValueAt(i, Select)).booleanValue();
                if(bSelect) 
                {
				String SHsnCode     = common.parseNull((String) theModel.getValueAt(i, HSNCode));
				
				if(iRCMStatus == 1) {
					if(theRCMAvailedClass.checkRCMAvailedHSN(SHsnCode)) {
						// No Action...
					}else {
						continue;
					}
				}
				 
                    double dSGSTPer1     = common.toDouble(String.valueOf(theModel.getValueAt(i, SGRatePer)));
                    double dSGSTValue1   = common.toDouble(String.valueOf(theModel.getValueAt(i, SGValue)));
                    
                    if(dSGSTPer==dSGSTPer1)
                    {
                        dValue += dSGSTValue1;
                    }
                }
            }
            theMap.put("GSTCode","A_5995");
            theMap.put("GSTPer",String.valueOf(dSGSTPer));
            theMap.put("GSTValue",String.valueOf(dValue));
             AList.add(theMap);
        }
        
        
        return AList;
        
    }
     
    public ArrayList getIGSTValue(Vector VIGST, int iRCMStatus, RCMAvailedClass theRCMAvailedClass)
    {
        ArrayList AList = new ArrayList();
        double dValue = 0;
        
        for(int V=0;V<VIGST.size();V++)
        {
            HashMap theMap = new HashMap();
            double dIGSTPer = common.toDouble(String.valueOf(VIGST.elementAt(V)));
            
            for (int i = 0; i < theModel.getRowCount(); i++) 
            {
                boolean bSelect = ((Boolean) theModel.getValueAt(i, Select)).booleanValue();
                if(bSelect) 
                {
				String SHsnCode     = common.parseNull((String) theModel.getValueAt(i, HSNCode));
				
				if(iRCMStatus == 1) {
					if(theRCMAvailedClass.checkRCMAvailedHSN(SHsnCode)) {
						// No Action...
					}else {
						continue;
					}
				}
				 
                    double dIGSTPer1     = common.toDouble(String.valueOf(theModel.getValueAt(i, IGRatePer)));
                    double dIGSTValue1   = common.toDouble(String.valueOf(theModel.getValueAt(i, IGValue)));
                    
                    if(dIGSTPer==dIGSTPer1)
                    {
                        dValue += dIGSTValue1;
                    }
                }
            }
            theMap.put("GSTCode","A_5996");
            theMap.put("GSTPer",String.valueOf(dIGSTPer));
            theMap.put("GSTValue",String.valueOf(dValue));
            AList.add(theMap);
        }
        
        
        return AList;
        
    }
    private boolean getValidation(String SItemName,String SHsnCode,int iTaxTypeCode,int iClaimStatus,int iHsnType,int iImportTypeCode,int iGstTypeCode)  
    {
         if(iTaxTypeCode<0)
         {
              JOptionPane . showMessageDialog(null,"Plz Update Tax Category (Tax/Zero Tax/Tax Exempted) for HSNCode "+SHsnCode+" ");
              return false;
         }
         if(iClaimStatus<=0)
         {
              JOptionPane . showMessageDialog(null,"Plz Update Claim status Of Item (Claim/NotClaim)  "+SItemName+" ");
              return false;
         }

         if(iHsnType<0)
         {
              JOptionPane . showMessageDialog(null,"Plz Update Hsn Type of the Item(Item/Service)  "+SItemName+" ");
              return false;
         }

         if(iImportTypeCode<0)
         {
              JOptionPane . showMessageDialog(null,"Plz Update Import Type Of Item   "+SItemName+" ");
              return false;
         }

         if(iGstTypeCode<0)
         {
              JOptionPane . showMessageDialog(null,"Plz Update Gst Type Of Item(GST/NonGst/GstExempted)  "+SItemName+" ");
              return false;
         }
         return true;

    }
    private void setVector() 
    {

        ADataList = new ArrayList();
        StringBuffer sb = new StringBuffer();

        sb.append(" select GateInward.GiNo,GateInward.GiDate,PartyMaster.PartyName,GateInward.InvNo,");
        sb.append(" GateInward.InvDate,SupQty,GateQty,GateInward.Rate,GateInward.Item_Code,GateInward.Item_Name,");
        sb.append(" State.GSTStateCode,GateInward.Sup_Code,PartyMaster.GSTInId,PartyMaster.GSTPartyTypeCode,State.StateCode from GateInward ");
        sb.append(" inner join PartyMaster on PartyMaster.PartyCode	= GateInward.Sup_Code");
        sb.append(" Inner join State on State.StateCode	=	PartyMaster.StateCode");
//        sb.append(" where GateInward.GrnNo=0 and GateInward.GrnNumber=0 and GateInward.InwNo=3");
        sb.append(" where GateInward.GrnNo=0 and GateInward.GrnNumber=0");
        sb.append(" and GateInward.MillCode ="+iMillCode);
        sb.append(" and GateInward.GiNo=" + iGiNo);
        sb.append(" and GateInward.CashReceiptStatus=0");
        sb.append(" and GateInward.ModiStatus=0");
        sb.append(" and GateInward.Authentication=1");
        sb.append(" order by GateInward.GiDate desc");

           System.out.println(" SETVECT "+sb.toString());
        try 
        {
            ORAConnection connect = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();

            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();

            while (theResult.next()) 
            {
                String SGiNo        = theResult.getString(1);
                String SGiDate      = theResult.getString(2);
                String SPartyName   = theResult.getString(3);
                String SInvNo       = theResult.getString(4);
                String SInvDate     = theResult.getString(5);
                double dSupQty      = theResult.getDouble(6);
                double dGateQty     = theResult.getDouble(7);
                double dRate        = theResult.getDouble(8);
                String SItemCode    = theResult.getString(9);
                String SItemName    = theResult.getString(10);
                String SGSTStateCode = theResult.getString(11);
               //  String SGSTStateCode = "33";
                String SSupCode      = theResult.getString(12);
                String SGSTInId      = theResult.getString(13);
                String SGSTPartyTypeCode = theResult.getString(14);
                String SStateCode        = theResult.getString(15);

                double dValue = dRate * dGateQty;

                HashMap theMap = new HashMap();
                theMap.put("GiNo", SGiNo);
                theMap.put("GiDate",SGiDate);
                theMap.put("PartyName",SPartyName);
                theMap.put("InvNo", SInvNo);
                theMap.put("InvDate", SInvDate);
                theMap.put("SupQty", String.valueOf(dSupQty));
                theMap.put("GateQty", String.valueOf(dGateQty));
                theMap.put("Rate", String.valueOf(dRate));
                theMap.put("Value", common.getRound(dValue, 2));
                theMap.put("ItemCode", SItemCode);
                theMap.put("ItemName", SItemName);
                theMap.put("GSTStateCode", SGSTStateCode);
                theMap.put("PartyCode", SSupCode);
                theMap.put("GSTInId", SGSTInId);
                theMap.put("GSTPartyTypeCode", SGSTPartyTypeCode);
                theMap.put("StateCode",SStateCode);
                ADataList.add(theMap);

            }
            theResult.close();
            theStatement.close();
        } 
        catch (Exception ex) 
        {
            System.out.println("    setVector " + ex);
        }
    }
    private void setPrintVector()
    {
        APrintDataList  =   new ArrayList();
        StringBuffer    sb  = new StringBuffer();
        sb.append(" select ReverseCharge_Invoice.GiNo,ReverseCharge_Invoice.SupCode,ReverseCharge_Invoice.ItemCode,");
        sb.append(" InvItems.Item_Name,ReverseCharge_Invoice.HSNCode,ReverseCharge_Invoice.InvRate,ReverseCharge_Invoice.InvQty,");
        sb.append(" ReverseCharge_Invoice.InvAmount,ReverseCharge_Invoice.CGST,ReverseCharge_Invoice.CGSTVal,ReverseCharge_Invoice.SGST,");
        sb.append(" ReverseCharge_Invoice.SGSTVal,");
        sb.append(" ReverseCharge_Invoice.IGST,ReverseCharge_Invoice.IGSTVal,ReverseCharge_Invoice.ReceiptVocNo,");
        sb.append(" to_char(ReverseCharge_Invoice.EntryDate,'dd-mm-yyyy') as ReceiptDate,ReverseCharge_Invoice.InvNo,ReverseCharge_Invoice.InvDate from ReverseCharge_Invoice ");
        sb.append(" Inner join InvItems on InvItems.Item_Code = ReverseCharge_Invoice.ItemCode ");
        sb.append(" where ReverseCharge_Invoice.GiNo="+iGiNo);
        sb.append(" and ReverseCharge_Invoice.DivisionCode="+iDivisionCode);
        sb.append(" and ReverseCharge_Invoice.YearCode="+SYearCode);

      //  System.out.println(" setPrintVector "+sb.toString());
        try
        {
            JDBCConnection1 connect    = JDBCConnection1.getJDBCConnection();
            Connection theConnection = connect.getConnection();

            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();

            while (theResult.next()) 
            {
                String SGiNo        = theResult.getString(1);
//                String SGiDate      = theResult.getString(2);
                String SSupCode     = theResult.getString(2);
                String SItemCode    = theResult.getString(3);
                String SItemName    = theResult.getString(4);                
                String SHSNCode     = common.parseNull(theResult.getString(5));                  
                double dRate        = theResult.getDouble(6);
                double dSupQty      = theResult.getDouble(7);
                double dPurValue    = theResult.getDouble(8);                
                double dCGSTPer     = theResult.getDouble(9);                
                double dCGSTValue   = theResult.getDouble(10);                
                double dSGSTPer     = theResult.getDouble(11);                
                double dSGSTValue   = theResult.getDouble(12);                
                double dIGSTPer     = theResult.getDouble(13);                
                double dIGSTValue   = theResult.getDouble(14);                                
                String SVoucherCode = theResult.getString(15);
                String SVoucherDate = theResult.getString(16);  
                String SInvNo       = theResult.getString(17);  
                String SInvDate     = theResult.getString(18);  
                double dTotalValue  = dPurValue+dCGSTValue+dSGSTValue+dIGSTValue;
                
                HashMap theMap      =   new HashMap();
                theMap  .   put("GINO",SGiNo);
//                theMap  .   put("GIDATE",SGiDate);
                theMap  .   put("SUPCODE",SSupCode);
                theMap  .   put("ITEMCODE",SItemCode);
                theMap  .   put("DESCRIPTION",SItemName);
                theMap  .   put("DISCOUNT","0");
                theMap  .   put("HSNCODE",SHSNCode);                
                theMap  .   put("RATE",String.valueOf(dRate));
                theMap  .   put("SUPQTY",String.valueOf(dSupQty));
                theMap  .   put("TAXVALUE",String.valueOf(dPurValue));
                theMap  .   put("CGST_PER",String.valueOf(dCGSTPer));
                theMap  .   put("CGST_VAL",String.valueOf(dCGSTValue));
                theMap  .   put("SGST_PER",String.valueOf(dSGSTPer));
                theMap  .   put("SGST_VAL",String.valueOf(dSGSTValue));                
                theMap  .   put("IGST_PER",String.valueOf(dIGSTPer));
                theMap  .   put("IGST_VAL",String.valueOf(dIGSTValue));                                
                theMap  .   put("TOTALVALUE",String.valueOf(dTotalValue));                                
                theMap  .   put("VOUCHERCODE",SVoucherCode);
                theMap  .   put("VOUCHERDATE",SVoucherDate);
                theMap  .   put("INVNO",SInvNo);
                theMap  .   put("INVDATE",SInvDate);
                APrintDataList.add(theMap);

                SPartyCode = SSupCode;
                SReceiptVoucherNo 	= SVoucherCode;
                SReceiptVoucherDate = SVoucherDate;
            }
            theResult.close();
            theStatement.close();            
            
        }
        catch(Exception ex)
        {
            System.out.println(" setPrint Vector "+ex);
        }
    }
    private void setHSNVector() 
    {
        iMaxLimit= 0;
        VHSNCode = new Vector();
        VGSTRate = new Vector();
        VCGSTPer = new Vector();
        VSGSTPer = new Vector();
        VIGSTPer = new Vector();
        VTaxCatCode = new Vector();
        
        VItemCode= new Vector();
        VItemName= new Vector();
        VItemHSNCode = new Vector();
        VClaimStatus = new Vector();
        VGstTypeCode = new Vector();
        VHsnType     = new Vector();
		VItemDetails = new Vector();       
        
        VPartyCode   = new Vector();
        VPartyName   = new Vector();
        VPartyStateCode   = new Vector();

        VGSTPartyTypeCode = new Vector();
        VGSTPartyType     = new Vector();
        try 
        {
            ORAConnection connect    = ORAConnection.getORAConnection();
            Connection theConnection = connect.getConnection();

            PreparedStatement theStatement = theConnection.prepareStatement("Select HSNCode,GSTRate,CGST,SGST,IGST,TaxCatCode From HSNGSTRate");
            ResultSet theResult = theStatement.executeQuery();
            while (theResult.next()) 
            {
                VHSNCode    .   addElement(theResult.getString(1));
                VGSTRate    .   addElement(theResult.getString(2));
                VCGSTPer    .   addElement(theResult.getString(3));
                VSGSTPer    .   addElement(theResult.getString(4));
                VIGSTPer    .   addElement(theResult.getString(5));
                VTaxCatCode .   addElement(theResult.getString(6));
            }
            theResult       .   close();
            theStatement    .   close();

            
            theStatement    = theConnection.prepareStatement("Select Item_Code,rpad(Item_Name,70),HSNCode,ClaimStatus,GstTypeCode,HsnType From InvItems Order By Item_Name");
            theResult       = theStatement.executeQuery();
            while(theResult.next())
            {
					String SItemCode	=	theResult.getString(1);
                    String SItemName    =   theResult.getString(2);
                    String SItemHsn     =   common.parseNull(theResult.getString(3));

                    VItemCode   .   addElement(SItemCode);
                    VItemName   .   addElement(SItemName);
                    VItemHSNCode    .   addElement(SItemHsn); 
                    VClaimStatus    .   addElement(common.parseNull(theResult.getString(4))); 
                    VGstTypeCode    .   addElement(common.parseNull(theResult.getString(5))); 
                    VHsnType        .   addElement(common.parseNull(theResult.getString(6))); 
					VItemDetails    .   addElement(SItemName+"  "+SItemCode+"  "+SItemHsn); 

            }
            theResult       .   close();            
            theStatement    .   close();
            
            theStatement    = theConnection.prepareStatement("Select PartyCode,PartyName,StateCode From PartyMaster Order By PartyName");
            theResult       = theStatement.executeQuery();
            while(theResult.next())
            {
                    VPartyCode   .   addElement(theResult.getString(1));
                    VPartyName   .   addElement(theResult.getString(2));
                    VPartyStateCode   .   addElement(theResult.getString(3));
            }
            theResult       .   close();            
            theStatement    .   close();            

            ORAConnection3 sconnect    = ORAConnection3.getORAConnection();
            Connection theConnect      = sconnect.getConnection();

			StringBuffer sb			   = new StringBuffer();
			sb.append("	Select Code,Name From GSTPartyType Order By Code");
/*			case when Code=0 then 'REVERSECHARGE_INVOICE' else 'REGISTEREDPARTY_INVOICE' end as TabName1,");
			sb.append("	case when Code=0 then 'REVERSECHARGE_TAXDETAILS' else 'REGISTEREDPARTY_TAXDETAILS' end as TabName2,");
			sb.append("	case when Code=0 then 'REVERSECHARGE_INVOICE_SEQ' else 'REGISTEREDPARTY_INVOICE_SEQ' end as SeqName1,");
			sb.append("	case when Code=0 then 'REVERSECHARGE_TAXDETAILS_SEQ' else 'REGISTEREDPARTY_TAXDETAILS_SEQ' end as SeqName2");
			sb.append("	 From GSTPartyType Order By Code");*/

            PreparedStatement thestate = theConnect.prepareStatement(sb.toString());
            ResultSet rst  	   		   = thestate.executeQuery();
            while(rst.next())
            {
                    VGSTPartyTypeCode   .   addElement(rst.getString(1));
                    VGSTPartyType       .   addElement(rst.getString(2));
            }
            rst       .   close();            
            thestate  .   close();   

            JDBCConnection1 alpaconnect     =   JDBCConnection1 . getJDBCConnection();
            Connection alpaConnection       =   alpaconnect     . getConnection();
         
            PreparedStatement stat = alpaConnection.prepareStatement("Select Max_Limit from ReverseCharge");
            ResultSet res  	   	   = stat.executeQuery();
            while(res.next())
            {
                iMaxLimit	=	res.getInt(1);
            }
            res	.	close();
            stat.   close();
        } 
        catch (Exception ex) 
        {
            System.out.println(" setHSNVector " + ex);
        }
    }

    private String getPartyCode(String SPartyName) 
    {
        String SCode = "";
        int iIndex = VPartyName.indexOf(SPartyName);
        SCode = (String) VPartyCode.elementAt(iIndex);
        return SCode;
    }
    private String getPartyType(String SPartyTypeCode) 
    {
        String SType = "";
        int iIndex = VGSTPartyTypeCode.indexOf(SPartyTypeCode);
        SType = (String) VGSTPartyType.elementAt(iIndex);
        return SType;
    }
    private int getPartyTypeCode(String SPartyType) 
    {
        String SCode = "";
        int iIndex = VGSTPartyType.indexOf(SPartyType);
        SCode = (String) VGSTPartyTypeCode.elementAt(iIndex);
        return common.toInt(SCode);
    }
    private String getPartyStateCode(String SPartyCode) 
    {
        String SCode = "";
        int iIndex = VPartyCode.indexOf(SPartyCode);
        SCode = (String) VPartyStateCode.elementAt(iIndex);
        return SCode;
    }

    private String getInvoiceNo(String SGiNo)
    {
         String SInvNo = "";
         for(int i=0;i<ADataList.size();i++)
         {
             HashMap theMap = (HashMap)ADataList.get(i);
             SInvNo         = (String)theMap.get("InvNo");
         }
         return SInvNo;
    }
    private String getInvoiceDate(String SGiNo)
    {
         String SInvDate = "";
         for(int i=0;i<ADataList.size();i++)
         {
             HashMap theMap = (HashMap)ADataList.get(i);
             SInvDate       = (String)theMap.get("InvDate");
         }
         return SInvDate;
       
    }
    private void clearData() 
    {
        lblGiDate.setText("");
        lblGSTStateCode.setText("");
        lblSupplier.setText("");
    }
    private boolean isItemsSelected()
    {
        for(int i=0;i<theTable.getRowCount();i++)
        {
                boolean bSelect = ((Boolean) theModel.getValueAt(i, Select)).booleanValue();
                if (bSelect) 
                {
                    //JOptionPane.showMessageDialog(null,"Plz Select All Items In the List");
                    return true;
                }
        }
        return false;
    }
    private boolean isPrintAll()
    {
        for(int i=0;i<thePrintTable.getRowCount();i++)
        {
                boolean bSelect = ((Boolean) thePrintModel.getValueAt(i, PSelect)).booleanValue();
                if (!bSelect) 
                {
                    JOptionPane.showMessageDialog(null,"Plz Select All Items For Print");
                    return false;
                }
        }
        return true;
    }    
    private boolean validAddress(String SPartyCode)
    {
        String SQs  = " ";
               SQs  = " SELECT PARTYCODE, PARTYNAME, ADDRESS1, ADDRESS2, ADDRESS3, COUNTRY.COUNTRYNAME, STATE.STATENAME||'-'||STATE.GSTSTATECODE  FROM SCM.PARTYMASTER ";
               SQs += " INNER JOIN SCM.STATE ON STATE.STATECODE = PARTYMASTER.STATECODE ";
               SQs += " INNER JOIN SCM.COUNTRY ON COUNTRY.COUNTRYCODE = STATE.COUNTRYCODE ";
               SQs += " WHERE PARTYCODE='"+SPartyCode+"' ";
//             System.out.println(" PQS "+SQs); 
        
            String SPartyName = ""; 
            String SAddress1  = "";
            String SAddress2  = "";
            String SAddress3  = "";
            String SCountry   = "";
            String SState     = "";

        try
		{    
            ORAConnection3 connect 	 = ORAConnection3.getORAConnection();
            Connection theConnection = connect.getConnection();
            java.sql.PreparedStatement pst = theConnection.prepareStatement(SQs);
            java.sql.ResultSet rst = pst.executeQuery();
            
            if(rst.next())
            {
                SPartyName = rst.getString(2); 
                SAddress1  = rst.getString(3);
                SAddress2  = rst.getString(4);
                SAddress3  = rst.getString(5);
                SCountry   = rst.getString(6);
                SState     = rst.getString(7);
            }
            rst.close();
            pst.close();
            
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        } 
//             System.out.println(" PQS "+SQs);
        if((SAddress1.trim()).equalsIgnoreCase("na")  || (SAddress2.trim()).equalsIgnoreCase("na"))
           return false;
        else
           return true;
    }

    private int getImportTypeCode(int iHsnType)
    {
           String SPartyCode        = getPartyCode(lblSupplier.getText());
           String SPartyStateCode   = getPartyStateCode(SPartyCode);
		   StringBuffer  sb 		= new StringBuffer();
		   int	iImportTypeCode 	= -1;	
           int iCountryCode     	= 0;
           sb.append("  Select CountryCode from State Where StateCode='"+SPartyStateCode+"'");
        
               try
               {
                   ORAConnection   oraConnection =  ORAConnection.getORAConnection();
                   Connection      theConnection =  oraConnection.getConnection();
                   Statement       theStatement  =  theConnection.createStatement();

                    ResultSet res = theStatement.executeQuery(sb.toString());
                    while(res.next())
                    {
                       iCountryCode = res.getInt(1);
                    }
                    res            . close();
                    theStatement   . close();
                    if(iCountryCode==61)
                    {
                      iImportTypeCode  = 0;
                    }    
                    else
                    {
                      if(iHsnType==0) // Service
                      {
                          iImportTypeCode = 2;
                      }
                      else if(iHsnType==1)
                      {
                          iImportTypeCode = 1;
                      }
                    }       
                     

               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
           return iImportTypeCode;
   }
   private int getClaimStatus(String SItemCode)
   {
       int iClaimStatus = -1;
       int iIndex      = VItemCode.indexOf(SItemCode);
       if(iIndex>=0)
       {
          iClaimStatus   = common.toInt((String)VClaimStatus.elementAt(iIndex));
       }
      
       return iClaimStatus;   
   }
   private int getGstTypeCode(String SItemCode)
   {
       int iGstTypeCode = -1;
       int iIndex       = VItemCode.indexOf(SItemCode);
       if(iIndex>=0)
       {
          iGstTypeCode   = common.toInt((String)VGstTypeCode.elementAt(iIndex));
       }
      
       return iGstTypeCode;   

   }
   private int getHsnType(String SItemCode) 
   {
       int iHsnType = -1;
       int iIndex       = VItemCode.indexOf(SItemCode);
       if(iIndex>=0)
       {
          iHsnType   = common.toInt((String)VHsnType.elementAt(iIndex));
       }
      
       return iHsnType;   

   }
   private int getTaxCatCode(String SHsnCode)
   {
        int iTaxCatCode = -1;
        int iIndex      = VHSNCode.indexOf(SHsnCode);
        if(iIndex>=0)
        {
          iTaxCatCode   = common.toInt((String)VTaxCatCode.elementAt(iIndex));
        }
        return iTaxCatCode;
   }
}
