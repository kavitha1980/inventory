package ReverseCharge;

import java.awt.*;              
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.plaf.basic.*;
import java.sql.*;
import java.util.*;
import jdbc.ORAConnection;
import util.*;


public class ReverseChargePurchaseModel extends DefaultTableModel
{
       
     String ColumnName[] = {"SNo","GateInwItem","ItemCode"   ,"ItemName" ,"HSN Code","Rate"  ,"Qty" ,"Value","DiscPer","DiscValue","BillValue","Rate%","Value","Rate%","Value","Rate%","Value","Total Value","SELECT"};
     String ColumnType[] = {"S"  ,"S"          ,    "S"      , "S"       , "S"      ,"E"     ,  "N" ,"N"    ,"E" , "N" , "N","N"    ,"N"    ,"N"   ,"N"     ,"N"    ,"N"    ,"N"           ,"B"    };
     int  iColumnWidth[] = {3    ,120          , 90          , 120       , 15       , 8      ,   7  ,12     ,10   ,10  ,12  ,10     ,10     ,10    ,10      ,10     ,10     ,12            ,8       };    
     
     Common common  =   null;
     
     Vector              			VHSNCode,VGSTRate,VCGSTPer,VSGSTPer,VIGSTPer;
     int                 			iGSTStateCode = 0;
     ReverseChargePurchaseModel   	theModel   =   null;
     JLabel                         lblTotalInvoice,lblNetAmount;

     // set Model Index   
     int SNo=0,GateInwItem=1,ItemCode=2,ItemName=3,HSNCode=4,Rate=5,Qty=6,Value=7,DiscPer=8,Discount=9,BillValue=10,CGRatePer=11,CGValue=12,SGRatePer=13,SGValue=14,IGRatePer=15,IGValue=16,TotalValue=17,Select=18;
     public ReverseChargePurchaseModel(JLabel lblTotalInvoice,JLabel lblNetAmount)
     {
          common =  new Common();
          this . lblTotalInvoice = lblTotalInvoice;
          this . lblNetAmount    = lblNetAmount;
          setHSNVector();
          setDataVector(getRowData(),ColumnName);
          this . theModel       = this;
     }
     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";

          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
               if(column   ==  Rate)
               {
                   setGSTValues(row);
               }
               if(column   ==  ItemName)
               {
                   setHSNDetails(row);
               }
               if(column   ==  DiscPer)
               {
                    setDiscGSTValues(row);
               }  
               setTotalValue();
          }

          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
               ex.printStackTrace();
          }
     }     
     
    public void setGSTValues(int iRow)
    {
        
        double dCGSTValue   =   0.0;
        double dSGSTValue   =   0.0;
        double dIGSTValue   =   0.0;
        double dTotalValue  =   0.0;
        
        double dRate = common.toDouble(String.valueOf(getValueAt(iRow,Rate)));
        double dQty  = common.toDouble(String.valueOf(getValueAt(iRow,Qty)));
        double dValue= dRate*dQty;
        double dCGSTPer =   common.toDouble(String.valueOf(getValueAt(iRow,CGRatePer)));
        double dSGSTPer =   common.toDouble(String.valueOf(getValueAt(iRow,SGRatePer)));
        double dIGSTPer =   common.toDouble(String.valueOf(getValueAt(iRow,IGRatePer)));
        if(iGSTStateCode==33)
        {
            dCGSTValue = dValue*dCGSTPer/100;
            dSGSTValue = dValue*dSGSTPer/100;
        }
        else
        {
             dIGSTValue = dValue*dIGSTPer/100;
        }
        if(iGSTStateCode==33)
        {
            dTotalValue = dValue+dCGSTValue+dSGSTValue;
        }
        else
        {
            dTotalValue = dValue+dIGSTValue;
        }
        
        setValueAt(common.toDouble(common.getRound(dValue,1)),iRow,Value) ;
        setValueAt(common.toDouble(common.getRound(dCGSTValue,1)),iRow,CGValue) ;
        setValueAt(common.toDouble(common.getRound(dSGSTValue,1)),iRow,SGValue) ;
        setValueAt(common.toDouble(common.getRound(dIGSTValue,1)),iRow,IGValue) ;
        setValueAt(common.toDouble(common.getRound(dTotalValue,1)),iRow,TotalValue) ;        
        
    }
    public void setDiscGSTValues(int iRow)
    {
        double dCGSTValue   =   0.0;
        double dSGSTValue   =   0.0;
        double dIGSTValue   =   0.0;
        double dTotalValue  =   0.0;

        double dRate = common.toDouble(String.valueOf(getValueAt(iRow,Rate)));
        double dQty  = common.toDouble(String.valueOf(getValueAt(iRow,Qty)));
        double dValue= dRate*dQty;
        double dDiscPer     =   common.toDouble(String.valueOf(getValueAt(iRow,DiscPer)));
        double dDiscValue   =   dValue*dDiscPer/100;
        double dBillValue   =   dValue-dDiscValue;
        double dCGSTPer     =   common.toDouble(String.valueOf(getValueAt(iRow,CGRatePer)));
        double dSGSTPer     =   common.toDouble(String.valueOf(getValueAt(iRow,SGRatePer)));
        double dIGSTPer     =   common.toDouble(String.valueOf(getValueAt(iRow,IGRatePer)));
        if(iGSTStateCode==33)
        {
            dCGSTValue = dBillValue*dCGSTPer/100;
            dSGSTValue = dBillValue*dSGSTPer/100;
            dIGSTPer   = 0;
            dIGSTValue = 0;
        }
        else
        {
            dIGSTValue = dBillValue*dIGSTPer/100;
        }
        if(iGSTStateCode==33)
        {
            dTotalValue = dBillValue+dCGSTValue+dSGSTValue;
        }
        else
        {
            dTotalValue = dBillValue+dIGSTValue;
        }

        setValueAt(common.toDouble(common.getRound(dValue,1)),iRow,Value) ;
        setValueAt(common.toDouble(common.getRound(dDiscValue,1)),iRow,Discount) ;
        setValueAt(common.toDouble(common.getRound(dBillValue,1)),iRow,BillValue) ;
        setValueAt(common.toDouble(common.getRound(dCGSTValue,1)),iRow,CGValue) ;
        setValueAt(common.toDouble(common.getRound(dSGSTValue,1)),iRow,SGValue) ;
        setValueAt(common.toDouble(common.getRound(dIGSTValue,1)),iRow,IGValue) ;
        setValueAt(common.toDouble(common.getRound(dTotalValue,1)),iRow,TotalValue) ;        

    }        

    private void setHSNDetails(int iRow)
    {
        int iHSNIndex   = -1;
        String SHSNCode = String.valueOf(getValueAt(iRow,HSNCode));

        iHSNIndex       = VHSNCode.indexOf(SHSNCode);

        if(iHSNIndex==-1)
            return;
        
        double dValue        =   common.toDouble(String.valueOf(getValueAt(iRow,Value)));
        double dCGSTPer      =   common.toDouble(String.valueOf(VCGSTPer.elementAt(iHSNIndex)));
        double dCGSTValue    =   dValue*dCGSTPer/100;
        double dSGSTPer      =   common.toDouble(String.valueOf(VSGSTPer.elementAt(iHSNIndex)));
        double dSGSTValue    =   dValue*dSGSTPer/100;
        double dIGSTPer      =   common.toDouble(String.valueOf(VIGSTPer.elementAt(iHSNIndex)));
        double dIGSTValue    =   dValue*dIGSTPer/100;
        double dTotalValue   =   dValue+dCGSTValue+dSGSTValue+dIGSTValue;

        setValueAt(common.toDouble(common.getRound(dCGSTPer,1)),iRow,CGRatePer) ;
        setValueAt(common.toDouble(common.getRound(dCGSTValue,1)),iRow,CGValue) ;
        setValueAt(common.toDouble(common.getRound(dSGSTPer,1)),iRow,SGRatePer) ;
        setValueAt(common.toDouble(common.getRound(dSGSTValue,1)),iRow,SGValue) ;
        setValueAt(common.toDouble(common.getRound(dIGSTPer,1)),iRow,IGRatePer) ;
        setValueAt(common.toDouble(common.getRound(dIGSTValue,1)),iRow,IGValue) ;
        setValueAt(common.toDouble(common.getRound(dTotalValue,1)),iRow,TotalValue) ;

    }
    private void setTotalValue()
    {
        double dTotal = 0.0;
        for(int i=0;i<getRowCount();i++)
        {
           boolean bSelect = ((Boolean)getValueAt(i,Select)).booleanValue();
           if(bSelect)
           {
            dTotal += common.toDouble(String.valueOf(getValueAt(i,TotalValue)));
           }
        }
        lblTotalInvoice . setText(common.getRound(dTotal,2));
        lblNetAmount    . setText(common.getRound(dTotal,2));
    }

    private void setHSNVector()
    {
        VHSNCode =  new Vector();
        VGSTRate =  new Vector();
        VCGSTPer =  new Vector();
        VSGSTPer =  new Vector();
        VIGSTPer =  new Vector();
        try
        {
            ORAConnection connect   =   ORAConnection.getORAConnection();
            Connection theConnection=   connect.getConnection();
            
            PreparedStatement theStatement  =   theConnection.prepareStatement("Select HSNCode,GSTRate,CGST,SGST,IGST From HSNGSTRate");
            ResultSet           theResult   =   theStatement.executeQuery();
            while(theResult.next())
            {
                VHSNCode    .   addElement(theResult.getString(1));
                VGSTRate    .   addElement(theResult.getString(2));
                VCGSTPer    .   addElement(theResult.getString(3));
                VSGSTPer    .   addElement(theResult.getString(4));                
                VIGSTPer    .   addElement(theResult.getString(5));                
            }
            theResult   .   close();
            theStatement.   close();
        }
        catch(Exception ex)
        {
            System.out.println(" setHSNVector "+ex);
        }
    }
}
