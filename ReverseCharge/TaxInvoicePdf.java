package ReverseCharge;

/*import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;*/
import java.util.*;
import java.sql.*;
import util.*;
import guiutil.*;
import java.net.InetAddress;
import jdbc.*;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import java.io.FileOutputStream;
/**
 *
 * @author root
 */
public class TaxInvoicePdf {

    Document    document = null;
    
    private PdfWriter   pdfWriter = null;
    private PdfPCell    pdfPCell  = null;
    private PdfPTable   pdfPTable = null;
    
    java.sql.Connection theScmConnection = null;
    
    String SPartyName = "", SAddress1 = "", SAddress2 = "", SAddress3 = "", SCountry = "", SState = "",SStateCode="";
    String SPartyCode = "",SReceiptVoucherNo="",SReceiptVoucherDate="",SGSTPartyType="",SGSTPartyTypeCode="";
    Common common;
    ArrayList theList  =   null;
    float[] fWidth;
    public TaxInvoicePdf() {
        common = new Common();
        generateInvoice();
    }
    public TaxInvoicePdf(String SPartyCode,String SReceiptVoucherNo,String SReceiptVocDate,String SGSTPartyType,ArrayList thePrintList) {
        common = new Common();
        this . SPartyCode   		=   SPartyCode;
        this . SReceiptVoucherNo	=   SReceiptVoucherNo;
        this . SReceiptVoucherDate	=   SReceiptVocDate;
        this . SGSTPartyType   	    =   SGSTPartyType;
        this . theList      		=   thePrintList;

        fWidth = new float[]{10f,50f,20f,15f,20f,10f,10f,10f,10f,10f,10f};
        generateInvoice();
    }
    public TaxInvoicePdf(String SPartyCode,String SReceiptVoucherNo,String SReceiptVocDate,ArrayList thePrintList) {
        common = new Common();
        this . SPartyCode   		=   SPartyCode;
        this . SReceiptVoucherNo	=   SReceiptVoucherNo;
        this . SReceiptVoucherDate	=   SReceiptVocDate;
        this . theList      		=   thePrintList;

        fWidth = new float[]{10f,50f,20f,15f,20f,10f,10f,10f,10f,10f,10f};
        generateInvoice();
    }
    
    private void generateInvoice(){
    
        try{
			getAddress(SPartyCode);
            createFile();
            pdfHead();
            pdfBody();
            
                    
        }catch(Exception ex){
            ex.printStackTrace();
        }
        finally{
            document.close();
            pdfWriter.close();            
        }
    }
    
    private void createFile(){
        
        try{
            document = new Document(PageSize.A4, 36, 36, 90, 36);
            pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(common.getPrintPath()+"TaxInvoice.pdf"));
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    
    private void addHeader(PdfWriter writer){
        PdfPTable header = new PdfPTable(4);
        try {
            // set defaults
            //header.setWidths(new int[]{2, 24});
            header.setTotalWidth(527);
            //header.setWidthPercentage(100f);
            header.setLockedWidth(true);
            header.getDefaultCell().setFixedHeight(40);
            header.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);
            header.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
            header.setHorizontalAlignment(Element.ALIGN_MIDDLE);
             
			AddCellTable("RECEIPT VOUCHER", header, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,1, 0, 0, 0, 8, 0, new Font(Font.FontFamily.HELVETICA, 10,Font.BOLD),BaseColor.LIGHT_GRAY);
			AddCellTable("VOUCHER NO", header, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,1, 0, 4, 0, 8,0, new Font(Font.FontFamily.HELVETICA, 10,Font.BOLD),BaseColor.LIGHT_GRAY);
			AddCellTable("VOUCHER DATE", header, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,1, 0, 4,0, 0, 0, new Font(Font.FontFamily.HELVETICA, 10,Font.BOLD),BaseColor.LIGHT_GRAY);


			AddCellTable("", header, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,1, 0, 0, 0, 8, 0, new Font(Font.FontFamily.HELVETICA, 10,Font.BOLD),BaseColor.LIGHT_GRAY);
			AddCellTable(SReceiptVoucherNo, header, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,1, 0, 4, 0, 8,0, new Font(Font.FontFamily.HELVETICA, 10,Font.BOLD),BaseColor.LIGHT_GRAY);
			AddCellTable(SReceiptVoucherDate, header, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,1, 0, 4, 0, 0, 0, new Font(Font.FontFamily.HELVETICA, 10,Font.BOLD),BaseColor.LIGHT_GRAY);
			
			
            // write content
            header.writeSelectedRows(0, -1, 34, 803, writer.getDirectContent());
            
        } catch(Exception de) {
            throw new ExceptionConverter(de);
        }
    }
    
    private void pdfHead(){
    
        try{
            // HeaderFooterPageEvent event = new HeaderFooterPageEvent();
	        //writer.setPageEvent(event);
        
            document.open();
            addHeader(pdfWriter);
            
            pdfPTable = new PdfPTable(2);
            pdfPTable .setWidthPercentage(100f);
            
            AddCellTable("Amarjothi Spinning Mills", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD));
            AddCellTable("Gobi Main Road,", pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            
            document.add(pdfPTable);

            pdfPTable = new PdfPTable(2);
            pdfPTable .setWidthPercentage(100f);
            
            AddCellTable("GSTIN  : 33AAFCA7082C1Z0", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD));
            AddCellTable("Pudhusuripalayam,", pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            
            document.add(pdfPTable);
            
            pdfPTable = new PdfPTable(1);
            pdfPTable .setWidthPercentage(100f);
            AddCellTable("Nambiyur 638458,", pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            
            document.add(pdfPTable);
            
            pdfPTable = new PdfPTable(1);
            pdfPTable .setWidthPercentage(100f);

            AddCellTable(" Gobi(TK),", pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            AddCellTable(" Erode(Dt),", pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));            
            AddCellTable(" TamilNadu-"+SStateCode+",India. ", pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            document.add(pdfPTable);
            
            document.add(new LineSeparator(0.5f, 100, null, 0, 0));
            
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
    
    }
    
    
    private void pdfBody(){
    
        try{

            //getAddress(SPartyCode);
            document.add(Chunk.NEWLINE );
            document.add(Chunk.NEWLINE );
            
            pdfPTable = new PdfPTable(1);
            pdfPTable .setWidthPercentage(100f);
            
            AddCellTable("Billing Details", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLD));
            AddCellTable(SPartyName+",", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            
            AddCellTable(SAddress1+",", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            
            if(!SAddress2.equalsIgnoreCase("na")){
            AddCellTable(SAddress2+",", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            }
            if(!SAddress3.equalsIgnoreCase("na")){
            AddCellTable(SAddress3+",", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            }
            AddCellTable(SState, pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            AddCellTable(SCountry+".", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            AddCellTable("GSTINNO. : "+SGSTPartyType, pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8));            
            document.add(pdfPTable);
            document.add(new Paragraph(""));
            document.add(Chunk.NEWLINE );
            document.add(Chunk.NEWLINE );
        
            pdfPTable = new PdfPTable(6);
            pdfPTable .setWidthPercentage(100f);

            AddCellTable("Bill No", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLD));
            AddCellTable("Bill Date", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLD));
            AddCellTable("Receipt Date", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLD));
            AddCellTable("Payment Terms", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLD));
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLD));
            


            java.util.HashMap hMap = (java.util.HashMap)theList.get(0);
            
            AddCellTable((String)hMap.get("INVNO"), pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            AddCellTable(common.parseDate((String)hMap.get("INVDATE")), pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            AddCellTable(common.parseDate(common.getServerPureDate()), pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            AddCellTable("On Receipt", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 1, 0, 4, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            
            
            document.add(pdfPTable);
            document.add(new Paragraph(""));
            //document.add(Chunk.NEWLINE );
            
            pdfPTable = new PdfPTable(11);
            pdfPTable . setWidths(fWidth);
            pdfPTable . setWidthPercentage(100f);

            
            AddCellTable("SI No", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("Description", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("HSN", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("Discount", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("Taxable Value", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("CGST", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("SGST", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("IGST", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            
            
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            
            AddCellTable("Rate", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("Amt.", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            
            AddCellTable("Rate", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("Amt.", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            
            AddCellTable("Rate", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("Amt.", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));

            double dTotInvoiceVal = 0;
            double dTotTaxVal     = 0;
            double dTotVal = 0;
            double dTotCGstTaxVal = 0;
            double dTotSGstTaxVal = 0;
            double dTotIGstTaxVal = 0;

            
            
            
            for(int i=0;i<theList.size();i++) {
                java.util.HashMap hashMap = (java.util.HashMap)theList.get(i);
                
                dTotVal        += common.toDouble((String)hashMap.get("TAXVALUE"));
                dTotCGstTaxVal += common.toDouble((String)hashMap.get("CGST_VAL"));
                dTotSGstTaxVal += common.toDouble((String)hashMap.get("SGST_VAL"));
                dTotIGstTaxVal += common.toDouble((String)hashMap.get("IGST_VAL"));
                
                AddCellTable(String.valueOf(i+1), pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
                AddCellTable(((String)hashMap.get("DESCRIPTION")), pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
                AddCellTable(((String)hashMap.get("HSNCODE")), pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
                AddCellTable(((String)hashMap.get("DISCOUNT")), pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
                AddCellTable(((String)hashMap.get("TAXVALUE")), pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));

                AddCellTable(((String)hashMap.get("CGST_PER")), pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
                AddCellTable(((String)hashMap.get("CGST_VAL")), pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));

                AddCellTable(((String)hashMap.get("SGST_PER")), pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
                AddCellTable(((String)hashMap.get("SGST_VAL")), pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));

                AddCellTable(((String)hashMap.get("IGST_PER")), pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
                AddCellTable(((String)hashMap.get("IGST_VAL")), pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            }
            
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            AddCellTable("TOTAL", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            AddCellTable(common.getRound(dTotVal,2), pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            AddCellTable(common.getRound(dTotCGstTaxVal,2), pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            AddCellTable(common.getRound(dTotSGstTaxVal,2), pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            AddCellTable(common.getRound(dTotIGstTaxVal,2), pdfPTable, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 1, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8));
            
            document.add(pdfPTable);
            
            dTotInvoiceVal = dTotVal+dTotCGstTaxVal+dTotSGstTaxVal+dTotIGstTaxVal;
            dTotTaxVal     = dTotCGstTaxVal+dTotSGstTaxVal+dTotIGstTaxVal;
            document.add(Chunk.NEWLINE );
            
            pdfPTable = new PdfPTable(2);
            pdfPTable .setWidthPercentage(100f);
            
            
            AddCellTable("Total invoice value(in figure)", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            AddCellTable(common.getRound(dTotInvoiceVal, 2), pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            
            AddCellTable("Total invoice value(in words)", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            AddCellTable(common.getRupee(String.valueOf(dTotInvoiceVal)), pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            
            AddCellTable("Amount of Tax subject to Reverse Charges", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            AddCellTable(common.getRound(dTotTaxVal, 2), pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            
            AddCellTable("Whether the tax is payable on reverse charge basis ", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));
            AddCellTable("Yes", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 0, 0, 0, 0, new Font(Font.FontFamily.TIMES_ROMAN, 10));

            document.add(pdfPTable);
    

            pdfPTable = new PdfPTable(4);
            pdfPTable . setSpacingBefore(20f);
            pdfPTable . setWidthPercentage(100f);

/*            AddCellTable("Prepared by", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_TOP, 1, 1, 50f, 4, 1, 8, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("Received by", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_TOP, 1, 1, 50f, 4, 1, 8, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("Checked by", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_TOP, 1, 1, 50f, 4, 1, 8, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));*/

            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_TOP, 1, 1, 50f, 4, 1, 8, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_TOP, 1, 1, 50f, 4, 1, 8, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_TOP, 1, 1, 50f, 4, 1, 8, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("For Amarjothi Spinning Mills Ltd.,", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_TOP, 1, 1, 50f, 4, 1, 8, 0, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));



/*            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 0, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 0, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 0, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));*/

            AddCellTable("Prepared By", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 0, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("Received By", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 0, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("Checked By", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 0, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            AddCellTable("Authorised Signatory", pdfPTable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1, 1, 0, 4, 0, 8, 2, new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD));
            document.add(pdfPTable);

        }catch(Exception ex){
            ex.printStackTrace();
        }
            
    }
    
    
    private void AddCellTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iRowSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Paragraph(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }
    private void AddCellTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iRowSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont,BaseColor bgcolor) {
        PdfPCell c1 = new PdfPCell(new Paragraph(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
//        c1.setBorderColor(BaseColor.LIGHT_GRAY);
        c1.setBackgroundColor(BaseColor.LIGHT_GRAY);

        table.addCell(c1);
    }
    
    
    
    public static void main(String args[]){
        
        new TaxInvoicePdf();
    }
 
    
    private void getAddress(String SPartyCode){
        String SQs  = " ";
               SQs  = " SELECT PARTYCODE, PARTYNAME, ADDRESS1, ADDRESS2, ADDRESS3, COUNTRY.COUNTRYNAME, STATE.STATENAME||'-'||STATE.GSTSTATECODE,";
               SQs += " STATE.GSTSTATECODE,   ";
               SQs += " PARTYMASTER.GSTINID,PARTYMASTER.GSTPARTYTYPECODE,GSTPARTYTYPE.NAME FROM SCM.PARTYMASTER";
               SQs += " INNER JOIN SCM.STATE ON STATE.STATECODE = PARTYMASTER.STATECODE ";
               SQs += " INNER JOIN SCM.COUNTRY ON COUNTRY.COUNTRYCODE = STATE.COUNTRYCODE ";
               SQs += " INNER JOIN SCM.GSTPARTYTYPE ON GSTPARTYTYPE.CODE = PARTYMASTER.GSTPARTYTYPECODE";
               SQs += " WHERE PARTYCODE='"+SPartyCode+"' ";
               System.out.println(" getADDR "+SQs);
        
        try{    
            if(theScmConnection==null){
                ORAConnection3 connect = ORAConnection3.getORAConnection();
                theScmConnection = connect.getConnection();
            }
            java.sql.PreparedStatement pst = theScmConnection.prepareStatement(SQs);
            java.sql.ResultSet rst = pst.executeQuery();
            
            SPartyName = ""; 
            SAddress1  = "";
            SAddress2  = "";
            SAddress3  = "";
            SCountry   = "";
            SState     = "";
            SStateCode = "";
            SGSTPartyTypeCode = "";
            SGSTPartyType     = "";
            
            if(rst.next()){
                SPartyName = rst.getString(2); 
                SAddress1  = rst.getString(3);
                SAddress2  = rst.getString(4);
                SAddress3  = rst.getString(5);
                SCountry   = rst.getString(6);
                SState     = rst.getString(7);
                SStateCode = rst.getString(8);
	            SGSTPartyTypeCode = rst.getString(10);
	            SGSTPartyType     = rst.getString(11);

            }
            rst.close();
            pst.close();
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
    }
    
}
