package Sales;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class HsnCodePicker extends JInternalFrame
{
     JTextField     THsnCode,THsnRate,THsnCess;
     JTextField     TIndicator;
     JButton        BUpdate,BCancel;
     
     JList          BrowList;
     JScrollPane    BrowScroll;
     JPanel         LeftPanel;
     JPanel         LeftTopPanel,LeftCenterPanel,LeftBottomPanel;

     JComboBox	    JCList;
     
     JLayeredPane      Layer;
     JTable            ReportTable;

     Vector            VSHsnCode,VSHsnRate,VSHsnData,VSHsnCess;

     String         str ="";
     
     int            iLastIndex= 0;
     Common         common    = new Common();

     HsnCodePicker(JLayeredPane Layer,JTable ReportTable)
     {
          this.Layer          = Layer;
          this.ReportTable    = ReportTable;

	  setDataIntoVector(0);
          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
     }

     public void createComponents()
     {
          BrowList            = new JList(VSHsnData);
          BrowScroll          = new JScrollPane();
          THsnCode            = new JTextField();
          THsnRate            = new JTextField();
          THsnCess            = new JTextField();
          TIndicator          = new JTextField();
          TIndicator          . setEditable(false);

	  JCList	      = new JComboBox();
          
          BUpdate             = new JButton("Update");
          BCancel             = new JButton("Cancel");
          
          LeftPanel           = new JPanel(true);
          LeftTopPanel        = new JPanel(true);
          LeftCenterPanel     = new JPanel(true);
          LeftBottomPanel     = new JPanel(true);
          
          THsnCode            . setEditable(false);
          THsnRate            . setEditable(false);
     }

     public void setLayouts()
     {
          setBounds(80,50,650,500);
          setResizable(true);
          setClosable(true);
          setTitle("HsnCode Selection");
          getContentPane()    . setLayout(new GridLayout(1,1));
          LeftPanel           . setLayout(new BorderLayout());
          LeftTopPanel        . setLayout(new GridLayout(1,3));
          LeftCenterPanel     . setLayout(new BorderLayout());
          LeftBottomPanel     . setLayout(new GridLayout(4,2));
     }

     public void addComponents()
     {
	  JCList.addItem("All");
	  JCList.addItem("StockGroup");
	  JCList.addItem("Material");

          getContentPane()    . add(LeftPanel);
          
          LeftPanel           . add("North",LeftTopPanel);
          LeftPanel           . add("Center",LeftCenterPanel);
          LeftPanel           . add("South",LeftBottomPanel);
          
          LeftTopPanel        . add(new JLabel(""));
          LeftTopPanel        . add(JCList);
          LeftTopPanel        . add(new JLabel(""));
          LeftCenterPanel     . add("Center",BrowScroll);
          LeftCenterPanel     . add("South",TIndicator);
          LeftBottomPanel     . add(new JLabel("Hsn Code"));
          LeftBottomPanel     . add(THsnCode);
          LeftBottomPanel     . add(new JLabel("Gst Rate %"));
          LeftBottomPanel     . add(THsnRate);
          LeftBottomPanel     . add(BUpdate);
          LeftBottomPanel     . add(BCancel);
     }

     public void setPresets()
     {
          BrowList            . setAutoscrolls(true);
          BrowScroll          . getViewport().setView(BrowList);

          show();
          //ensureIndexIsVisible(SNameCode);
          LeftCenterPanel.updateUI();
     }

     public void addListeners()
     {
          BrowList  .addKeyListener(new KeyList());
          BUpdate   .addActionListener(new ActList());
          BCancel   .addActionListener(new ActList());
	  JCList.addItemListener(new ItemList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BUpdate)
               {
                         int       i = ReportTable.getSelectedRow();

                         String SItemCode = (String)ReportTable.getModel().getValueAt(i,1);
     
                         String SNewHsnCode  = THsnCode.getText();

	                 updateItemMaster(SItemCode,SNewHsnCode);
     
                         ReportTable.getModel().setValueAt(SNewHsnCode,i,3);

						removeHelpFrame();
						ReportTable.requestFocus();
               }
          }
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='/') || (lastchar=='*') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==116)    // F5 is pressed
               {
		            int iSeleIndex = JCList.getSelectedIndex();
                    setDataIntoVector(iSeleIndex);
                    BrowList.setListData(VSHsnData);
               }
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SHsnCode     = (String)VSHsnCode.elementAt(index);
                    String SHsnRate     = (String)VSHsnRate.elementAt(index);
                    String SHsnCess     = (String)VSHsnCess.elementAt(index);
                    addMatDet(SHsnCode,SHsnRate,SHsnCess,index);
                    str="";
               }
          }
     }

     public class ItemList implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
	       int iSeleIndex = JCList.getSelectedIndex();
               setDataIntoVector(iSeleIndex);
               BrowList.setListData(VSHsnData);
          }
     }

     public void setCursor()
     {
          TIndicator.setText(str);            
          int index=0;
          for(index=0;index<VSHsnCode.size();index++)
          {
               String str1 = ((String)VSHsnData.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedIndex(index);
                    BrowList.ensureIndexIsVisible(index);
                    break;
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

     public boolean addMatDet(String SHsnCode,String SHsnRate,String SHsnCess,int index)
     {
          THsnCode       . setText(SHsnCode);
          THsnRate       . setText(SHsnRate);
          THsnCess       . setText(SHsnCess);

          return true;    
     }

     public void setDataIntoVector(int iSeleIndex)
     {
          VSHsnCode  = new Vector();
          VSHsnRate  = new Vector();
          VSHsnData  = new Vector();
          VSHsnCess  = new Vector();

	  String QS = "";

	  if(iSeleIndex==0)
	  {
	       QS = " Select HsnCode,GstRate,Cess from ( "+
		    " Select InvItems.HsnCode,HsnGstRate.GstRate,HsnGstRate.Cess from InvItems "+
		    " Inner Join HsnGstRate on InvItems.HsnCode = HsnGstRate.HsnCode and InvItems.HsnType=0 "+
		    " Group by InvItems.HsnCode,HsnGstRate.GstRate,HsnGstRate.Cess "+
		    " Union All "+
	       	    " Select MultiHsn.HsnCode,HsnGstRate.GstRate,HsnGstRate.Cess from MultiHsn "+
		    " Inner Join InvItems on MultiHsn.Item_Code = InvItems.Item_Code and InvItems.HsnType=0 "+
		    " Inner Join HsnGstRate on MultiHsn.HsnCode = HsnGstRate.HsnCode "+
		    " Group by MultiHsn.HsnCode,HsnGstRate.GstRate,HsnGstRate.Cess) "+
		    " Group by HsnCode,GstRate,Cess "+
		    " Order by 1,2 ";
	  }
	  /*else if(iSeleIndex==1)
	  {
	       QS = " Select HsnCode,GstRate,Cess from ( "+
	            " Select InvItems.HsnCode,HsnGstRate.GstRate,HsnGstRate.Cess from InvItems "+
		    " Inner Join HsnGstRate on InvItems.HsnCode = HsnGstRate.HsnCode and InvItems.HsnType=0 "+
		    " and InvItems.StkGroupCode=(Select StkGroupCode from InvItems Where Item_Code='"+SItemCode+"') "+
		    " Group by InvItems.HsnCode,HsnGstRate.GstRate,HsnGstRate.Cess "+
		    " Union All "+
	       	    " Select MultiHsn.HsnCode,HsnGstRate.GstRate,HsnGstRate.Cess from MultiHsn "+
		    " Inner Join InvItems on MultiHsn.Item_Code = InvItems.Item_Code and InvItems.HsnType=0 "+
		    " and InvItems.StkGroupCode=(Select StkGroupCode from InvItems Where Item_Code='"+SItemCode+"') "+
		    " Inner Join HsnGstRate on MultiHsn.HsnCode = HsnGstRate.HsnCode "+
		    " Group by MultiHsn.HsnCode,HsnGstRate.GstRate,HsnGstRate.Cess) "+
		    " Group by HsnCode,GstRate,Cess "+
		    " Order by 1,2 ";
	  }
	  else
	  {
	       QS = " Select HsnCode,GstRate,Cess from ( "+
	            " Select InvItems.HsnCode,HsnGstRate.GstRate,HsnGstRate.Cess from InvItems "+
		    " Inner Join HsnGstRate on InvItems.HsnCode = HsnGstRate.HsnCode and InvItems.HsnType=0 "+
		    " and InvItems.Item_Code='"+SItemCode+"'"+
		    " Group by InvItems.HsnCode,HsnGstRate.GstRate,HsnGstRate.Cess "+
		    " Union All "+
	       	    " Select MultiHsn.HsnCode,HsnGstRate.GstRate,HsnGstRate.Cess from MultiHsn "+
		    " Inner Join InvItems on MultiHsn.Item_Code = InvItems.Item_Code and InvItems.HsnType=0 "+
		    " and MultiHsn.Item_Code='"+SItemCode+"'"+
		    " Inner Join HsnGstRate on MultiHsn.HsnCode = HsnGstRate.HsnCode "+
		    " Group by MultiHsn.HsnCode,HsnGstRate.GstRate,HsnGstRate.Cess) "+
		    " Group by HsnCode,GstRate,Cess "+
		    " Order by 1,2 ";
	  }*/
	
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
               ResultSet      res            = stat.executeQuery(QS);

               while(res.next())
               {
                    String str1      = common.parseNull(res.getString(1));
                    String str2      = common.parseNull(res.getString(2));
                    String str3      = common.parseNull(res.getString(3));
                    VSHsnCode.addElement(str1);
                    VSHsnRate.addElement(str2);
                    VSHsnData.addElement(str1+" ~ GstRate - "+str2+"%");
                    VSHsnCess.addElement(str3);
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void updateItemMaster(String SItemCode,String SNewHsnCode)
     {
          String QString = " Update InvItems Set HsnCode='"+SNewHsnCode+"' Where Item_Code='"+SItemCode+"'";
          
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
               stat.executeQuery(QString);
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

}
