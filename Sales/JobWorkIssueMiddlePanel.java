package Sales;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class JobWorkIssueMiddlePanel extends JPanel
{
     Object              RowData[][];
     String              ColumnData[] = {"SNo","Code","Name","HSN Code","UOM","Stock","Quantity","Rate","CGST(%)","SGST(%)","IGST(%)","CESS(%)","Basic(RS)","CGST(Rs)","SGST(Rs)","IGST(Rs)","CESS(Rs)","Value"};
     String              ColumnType[] = {"N"  ,"S"   ,"S"   ,"S"       ,"S"  ,"N"    ,"N"       ,"E"   ,"E"      ,"E"      ,"E"      ,"E"      ,"N"        ,"N"       ,"N"       ,"N"       ,"N"       ,"N"    };
     
     Object              RateData[];

     JLayeredPane        Layer;
     Vector              VCode,VName,VUom,VNameCode,VStock,VStockRate,VHsnCode;
     PTMaterialSearch    MS;
     PTMaterialsSearch   SM;
     PTTabReport         tabreport;
     Common              common = new Common();
     int                 iMillCode;
     String              SItemTable,SSupTable,SAuthUserCode;
     JTextField          TSupCode;
     MyTextField         TOthers;
     JLabel LNet;

     Vector[] VMUserQty,VMUserCode,VMUserRate;

     JobWorkIssueMiddlePanel(JLayeredPane Layer,int iMillCode,String SItemTable,String SSupTable,String SAuthUserCode,JTextField TSupCode,JLabel LNet,MyTextField TOthers)
     {
          this.Layer         = Layer;
          this.iMillCode     = iMillCode;
          this.SItemTable    = SItemTable;
          this.SSupTable     = SSupTable;
          this.SAuthUserCode = SAuthUserCode;
          this.TSupCode      = TSupCode;
          this.LNet           =LNet; 
          this.TOthers        = TOthers; 
     }
     
     public void createComponents(int irows)
     {
          RowData        = new Object[irows][ColumnData.length];
          RateData       = new Object[irows];

          VMUserQty      = new Vector[irows];
          VMUserCode     = new Vector[irows];
          VMUserRate     = new Vector[irows];

          for(int i=0;i<irows;i++)
          {
               RowData[i][0] = String.valueOf(i+1);
               RowData[i][1] = " ";
               RowData[i][2] = " ";
               RowData[i][3] = " ";
               RowData[i][4] = " ";
               RowData[i][5] = " ";
               RowData[i][6] = " ";
               RowData[i][7] = " ";
               RowData[i][8] = " ";
               RowData[i][9] = " ";  
               RowData[i][10] = " ";
               RowData[i][11] = " ";
               RowData[i][12] = " ";
               RowData[i][13] = " ";
               RowData[i][14] = " ";
               RowData[i][15] = " ";
               RowData[i][16] = "";
               RowData[i][17] = "0";

               RateData[i] = "0";

	       VMUserQty[i]  = new Vector();
	       VMUserCode[i] = new Vector();
	       VMUserRate[i] = new Vector();
          }
          try
          {
               setLayout(new BorderLayout());
               tabreport   = new PTTabReport(RowData,ColumnData,ColumnType,iMillCode,SItemTable,SSupTable,SAuthUserCode,LNet,TOthers);
               add(tabreport,BorderLayout.CENTER);
               updateUI();
               addListeners();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     
     public void addListeners()
     {
          tabreport.ReportTable.addKeyListener(new KeyList());
     }
     
     private class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    String str = (TSupCode.getText()).trim();
                    if(str.length()==0)
                    {
                         JOptionPane.showMessageDialog(null,"Please Select Party ","Information",JOptionPane.INFORMATION_MESSAGE);
                    }
                    else
                    {
                         showMaterialSelectionFrame(1);
                    }
               }

               if(ke.getKeyCode()==KeyEvent.VK_F2) 
	       {
	            int i =tabreport. ReportTable.getSelectedRow();
		    int iCol = tabreport.ReportTable.getSelectedColumn();
              
		    if(iCol==3)
		    {
			 String SMatCode = (String)tabreport.RowData[i][1];
			 String SHsnCode = (String)tabreport.RowData[i][3];
			 if(SHsnCode.equals("") && !SMatCode.equals(""))
			 {
		              setServList();
			 }
		    }
               }

               if(ke.getKeyCode()==KeyEvent.VK_F3) 
	       {
	            int i = tabreport. ReportTable.getSelectedRow();
		    int iCol = tabreport.ReportTable.getSelectedColumn();
              
		    if(iCol==6)
		    {
			 String SMCode  = (String)tabreport.RowData[i][1];
			 String SMName  = (String)tabreport.RowData[i][2];
			 String SStkQty = (String)tabreport.RowData[i][5];
		         setUserList(i,SMCode,SMName,SStkQty);
		    }
               }
          }
     }

     private void setServList()
     {
           HsnCodePicker SP = new HsnCodePicker(Layer,tabreport.ReportTable);
           try
           {
                Layer   . add(SP);
                Layer   . repaint();
                SP        . setSelected(true);
                Layer   . updateUI();
                SP        . show();
                SP        . BrowList.requestFocus();
           }
           catch(java.beans.PropertyVetoException ex){}
     }

     private void setUserList(int iRow,String SMCode,String SMName,String SStkQty)
     {
           UserwiseMaterialFrame UF = new UserwiseMaterialFrame(Layer,iMillCode,this,iRow,SMCode,SMName,SStkQty);
           try
           {
                Layer   . add(UF);
                Layer   . repaint();
                UF      . setSelected(true);
                Layer   . updateUI();
                UF      . show();
           }
           catch(java.beans.PropertyVetoException ex){}
     }
     
     public void showMaterialSelectionFrame(int iClicked)
     {
          try
          {
               Layer.remove(MS);
               Layer.remove(SM);
               Layer.updateUI();
          }
          catch(Exception ex){}
          
          try
          {
               String SSupCode = (TSupCode.getText()).trim();

               if(iClicked==0)
               {
                    SM   = new PTMaterialsSearch(Layer,this,iMillCode,SItemTable,SSupTable,SAuthUserCode,SSupCode,VCode,VName,VUom,VNameCode,VStock,VStockRate,VHsnCode);
                    SM   .setSelected(true);
                    Layer.add(SM);
                    Layer.repaint();
                    SM   .moveToFront();
                    Layer.updateUI();
               }
               else
               {
                    MS   = new PTMaterialSearch(Layer,this,tabreport.ReportTable.getSelectedRow(),iMillCode,SItemTable,SSupTable,SAuthUserCode,SSupCode,VCode,VName,VUom,VNameCode,VStock,VStockRate,VHsnCode);
                    MS   .setSelected(true);
                    Layer.add(MS);
                    Layer.repaint();
                    MS   .moveToFront();
                    Layer.updateUI();
               }
          }
          catch(Exception ex){}
     }

     public void setDataIntoVector()
     {
          VName      = new Vector();
          VCode      = new Vector();
          VUom       = new Vector();
          VNameCode  = new Vector();
          VStock     = new Vector();
          VStockRate = new Vector();
	  VHsnCode   = new Vector();

          String QS = " Select InvItems.Item_Name,InvItems.Item_Code,Uom.UoMName,sum(ItemStock.Stock),ItemStock.StockValue,InvItems.HsnCode from InvItems "+
                      " inner join ItemStock on (InvItems.Item_Code=ItemStock.ItemCode and ItemStock.MillCode="+iMillCode+") "+ 
                      " inner join UOM on UOM.UOMCode = InvItems.UOMCode "+
                      " Group by InvItems.Item_Name,InvItems.Item_Code,Uom.UoMName,ItemStock.StockValue,InvItems.HsnCode "+
                      " having sum(ItemStock.Stock)>0 "+
                      " Order By InvItems.Item_Name";

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();   
               Statement      stat           = theConnection.createStatement();

               ResultSet result1 = stat.executeQuery(QS);

               while(result1.next())
               {
                    VName     .addElement(common.parseNull(result1.getString(1)));
                    VCode     .addElement(common.parseNull(result1.getString(2)));
                    VUom      .addElement(common.parseNull(result1.getString(3)));
                    VNameCode .addElement(common.parseNull(result1.getString(1))+" (Code : "+common.parseNull(result1.getString(2))+")");
                    VStock    .addElement(common.parseNull(result1.getString(4)));
                    VStockRate.addElement(common.parseNull(result1.getString(5)));
                    VHsnCode.addElement(common.parseNull(result1.getString(6)));
               }
               result1.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     
}
