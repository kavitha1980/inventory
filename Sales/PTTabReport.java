package Sales;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class PTTabReport extends JPanel
{
     JTable         ReportTable;
     Object         RowData[][];
     String         ColumnData[],ColumnType[];
     int            iMillCode;
     String         SItemTable,SSupTable;
     String         SAuthUserCode;
     JLabel LNet;
     MyTextField TOthers;  
     JPanel         thePanel;
     Common common = new Common();
     AbstractTableModel dataModel;
	double dGrandNet;
	

     PTTabReport(Object RowData[][],String ColumnData[],String ColumnType[],int iMillCode,String SItemTable,String SSupTable,String SAuthUserCode,JLabel LNet,MyTextField TOthers)
     {
          this.RowData        = RowData;
          this.ColumnData     = ColumnData;
          this.ColumnType     = ColumnType;
          this.iMillCode      = iMillCode;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;
          this.SAuthUserCode  = SAuthUserCode;
          this.LNet          = LNet;
          this.TOthers       =TOthers;
          thePanel            = new JPanel();

          setReportTable();
          setBorder(new TitledBorder("List Of Items"));
     }
     
     public void setReportTable()
     {
          dataModel = new AbstractTableModel()
          {
               public int getColumnCount(){return ColumnData.length;}
               public int getRowCount(){return RowData.length;}
               public Object getValueAt(int row,int col){return RowData[row][col];}
               public String getColumnName(int col){ return ColumnData[col];}
               public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }
               public boolean isCellEditable(int row,int col)
               {
                    if(ColumnType[col]=="B" || ColumnType[col]=="E")
                         return true;
                    return false;
               }
          
               public void setValueAt(Object element,int row,int col)
               {
					//if(col>11)
					//	return;

                    if(col == 6)
                    {
		                     if(isValidQty((String)element)) 
		                     {
		                          RowData[row][col]=element; 
		                          setTotal();
		                          double dQty    = Double.parseDouble((String)element);
		                          String SCode   = (String)ReportTable.getValueAt(row,1);
		                          double dStock  = getStock(SCode);

								System.out.println(dQty+":"+SCode+":"+dStock);

		                          dStock = dStock - dQty;

								System.out.println("After:"+SCode+":"+dStock);
		                               
		                          ReportTable.setValueAt(common.getRound(dStock,3),row,5);
		                     }
		                     else
		                          JOptionPane.showMessageDialog(null,"Invaid Quantity","Error Message",JOptionPane.INFORMATION_MESSAGE);
                    }
				    else
                    if(col==7 || col== 8 || col==9 || col ==10 || col==11)
                    {
                        RowData[row][col]=element;
                    	setTotal();
					}
				    else
				    {
                         RowData[row][col]=element;
					}

               }
          };
             
          ReportTable  = new JTable(dataModel);
          ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
          DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
          cellRenderer.setHorizontalAlignment(JLabel.RIGHT);
        
          for (int col=0;col<ReportTable.getColumnCount();col++)
          {
               if(ColumnType[col]=="N" || ColumnType[col]=="E") 
               ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
          }
         
          setQtyHeader();
          formatColumns();
          setLayout(new BorderLayout());
          thePanel.setLayout(new BorderLayout());
          thePanel.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
          thePanel.add(new JScrollPane(ReportTable),BorderLayout.CENTER);
          add("Center",thePanel);
           TOthers.addKeyListener(new KeyList());
               
     }
       public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               calc();
          }
     }
      public void calc()
     {

			double dTNet ;
			double dOthers = common.toDouble((TOthers.getText()).trim());


			dTNet = dGrandNet+dOthers;

			LNet.setText(common.getRound(dTNet,2));  
                                       
     }
     private void setQtyHeader()
     {
          DefaultTableCellRenderer cellRenderer   = new DefaultTableCellRenderer();
                                   cellRenderer   .setHorizontalAlignment(JLabel.CENTER);
                                   cellRenderer   .setBackground(new Color(100,200,250));
                                   cellRenderer   .setIcon(new ImageIcon("prodbig.gif"));
                                   cellRenderer   .setToolTipText("Quantity");
                                   (ReportTable   .getTableHeader()).setBorder(new BevelBorder(BevelBorder.RAISED));
                                   cellRenderer   .setBorder(new SoftBevelBorder(0));
                                   (ReportTable   .getColumn("Quantity")).setHeaderRenderer(cellRenderer);
     }
     
     private boolean isValidQty(String str)
     {
          boolean bFlag=true;
          try
          {
               double dvalue=Double.parseDouble(str);
               bFlag = true;
          }
          catch(Exception ex)
          {
               bFlag=false;
          }
          return bFlag;
     }
     
     private void formatColumns()
     {
          TableColumn snoColumn   =  ReportTable.getColumn(ReportTable.getColumnName(0));
          TableColumn codeColumn  =  ReportTable.getColumn(ReportTable.getColumnName(1));
          TableColumn nameColumn  =  ReportTable.getColumn(ReportTable.getColumnName(2));
          TableColumn hsncodeColumn  =  ReportTable.getColumn(ReportTable.getColumnName(3)); 
          TableColumn uomColumn   =  ReportTable.getColumn(ReportTable.getColumnName(4));
          TableColumn stockColumn =  ReportTable.getColumn(ReportTable.getColumnName(5));
          TableColumn qtyColumn   =  ReportTable.getColumn(ReportTable.getColumnName(6));
          TableColumn rateColumn  =  ReportTable.getColumn(ReportTable.getColumnName(7));
         // TableColumn valueColumn =  ReportTable.getColumn(ReportTable.getColumnName(8));
          
          snoColumn      .setPreferredWidth(30);
          codeColumn     .setPreferredWidth(75);
          nameColumn     .setPreferredWidth(200);
          uomColumn      .setPreferredWidth(50);
          stockColumn    .setPreferredWidth(75);
          qtyColumn      .setPreferredWidth(80);
          rateColumn     .setPreferredWidth(70);
         // valueColumn    .setPreferredWidth(70);
     }
     
     private void setTotal()
     {
          double dValue=0;
          dGrandNet=0;
         
          for(int i=0;i<RowData.length;i++)
		  {
				double dQty   = common.toDouble((String)ReportTable.getValueAt(i,6));    
				double dRate  = common.toDouble((String)ReportTable.getValueAt(i,7));

				double dBasic   =dQty*dRate;
				//Tax Value

				ReportTable.setValueAt(common.getRound(dBasic,2),i,12); 

				double dCGST      = common.toDouble((String)ReportTable.getValueAt(i,8));    
				double dSGST      = common.toDouble((String)ReportTable.getValueAt(i,9));    
				double dIGST      = common.toDouble((String)ReportTable.getValueAt(i,10));    
				double dCess      = common.toDouble((String)ReportTable.getValueAt(i,11));    


				double dCGSTVal = dBasic*dCGST/100;
				double dSGSTVal = dBasic*dSGST/100;
				double dIGSTVal = dBasic*dIGST/100;
				double dCessVal = dBasic*dCess/100;
				double dNet     = dBasic+dCGSTVal+dSGSTVal+dIGSTVal+dCessVal;

				ReportTable.setValueAt(common.getRound(dCGSTVal,2),i,13);
				ReportTable.setValueAt(common.getRound(dSGSTVal,2),i,14);
				ReportTable.setValueAt(common.getRound(dIGSTVal,2),i,15);
				ReportTable.setValueAt(common.getRound(dCessVal,2),i,16);
				ReportTable.setValueAt(common.getRound(dNet,2),i,17);

			
				dGrandNet+=dNet;
          
          }
 
          
         //  double dOthers = common.toDouble((TOthers.getText()).trim());
           
            LNet.setText(common.getRound(dGrandNet,2));
              
     }

     private double getStock(String SCode)
     {
          double dQty=0;

          try
          {
               String QS= " Select Sum(Stock) from ItemStock where ItemCode='"+SCode+"' and MillCode="+iMillCode+"";

               ORAConnection   oraConnection = ORAConnection.getORAConnection();
               Connection      theConnection = oraConnection.getConnection();               
               Statement theStatement        = theConnection.createStatement();
               ResultSet theResult           = theStatement.executeQuery(QS);
     
               if(theResult.next())
                    dQty = theResult.getDouble(1);

			theResult.close();
			theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);   
          }
          return dQty;
     }

}
