package Sales;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;


public class SalesFrame extends JInternalFrame
{
     DateField                TDCDate;
     MyTextField              TDCNo,TSentThro,TPurpose,TOthers;
     JTextField               TSupCode;
     MyComboBox               JCUser;
     JButton                  BApply,BSupplier,BOk,BCancel,BMaterial;
     JPanel                   TopPanel,BottomPanel,BottomPanel1;
     JPanel                   Top1Panel,Top2Panel;
     JLabel                    LNet;
     JobWorkIssueMiddlePanel  MiddlePanel;

     Common                   common = new Common();
     
     JLayeredPane             Layer;
     Vector                   VName,VCode,VNameCode;
     Vector                   VUom,VUomCode;
     Vector                   VIUserCode,VIUserName;
     StatusPanel              SPanel;
     int                      iUserCode,iMillCode;
     String                   SItemTable,SSupTable,SYearCode,SMillName,SStDate;

     String                   SIssueNo  = "";
     Connection               theConnection  = null;
     Connection               theDConnection = null;
     Connection               theConnection1 = null;
     boolean                  bComflag  = true;

     String SAuthUserCode="";

     int               iClicked=0;
     int  iDivCode;
     int iInvoiceNo=0;
     int iRdcNo=0;
     int iVocNo=0;
     int iIndentNo=0;
     int iIssueNo=0;
    
     String SSeleSupCode,SSeleSupType,SSeleStateCode;
     int iStateCheck = 0;
     int iTypeCheck  = 0;
 
     String SPreFix ="";

     
     public SalesFrame(JLayeredPane Layer,Vector VCode,Vector VName,Vector VNameCode,StatusPanel SPanel,int iUserCode,int iMillCode,String SItemTable,String SSupTable,String SYearCode,String SMillName,String SStDate)
     {
          this.Layer      = Layer;
          this.VCode      = VCode;
          this.VName      = VName;
          this.VNameCode  = VNameCode;
          this.SPanel     = SPanel;
          this.iUserCode  = iUserCode;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SYearCode  = SYearCode;
          this.SMillName  = SMillName;
          this.SStDate    = SStDate;
         
          
          setOraConnection();
          getUsers();
          setParamVectors();
          createComponents();
          setLayouts();

          addComponents();
          addListeners();
          
     }

     public void setOraConnection()
     {
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                              theConnection  =  oraConnection.getConnection();
                              theConnection  .  setAutoCommit(false);

               if(iMillCode==1)
               {
                    DORAConnection DoraConnection = DORAConnection.getORAConnection();
                                   theDConnection = DoraConnection.getConnection();
               }

               JDBCConnection1  jdbcConnection1  =  JDBCConnection1.getJDBCConnection();
                                theConnection1   =  jdbcConnection1.getConnection();
                                theConnection1  .  setAutoCommit(false);

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void createComponents()
     {
          
          BSupplier        = new JButton("Select Party");
          BOk              = new JButton("Okay");
          BCancel          = new JButton("Cancel");
          BMaterial        = new JButton("Select Materials");
          
          TDCDate          = new DateField();
          TDCNo            = new MyTextField(8);
          TSentThro        = new MyTextField(20);
          TPurpose         = new MyTextField(100);
          TOthers          = new MyTextField(25);
         
          TSupCode         = new JTextField();
          JCUser           = new MyComboBox(VIUserName);
          LNet             = new JLabel("0");
          TopPanel         = new JPanel();
          BottomPanel      = new JPanel();
          BottomPanel1      = new JPanel();
          Top1Panel        = new JPanel();
          Top2Panel        = new JPanel();

          TDCDate   .setTodayDate();
          BOk       .setEnabled(false);
     }
     
     public void setLayouts()
     {
          setTitle("Transfer to Party");
          setMaximizable(true);
          setClosable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,675,495);
          
          getContentPane()    .setLayout(new BorderLayout());

          TopPanel            .setLayout(new BorderLayout());
          Top1Panel           .setLayout(new GridLayout(6,2,10,10));
          BottomPanel1         .setLayout(new FlowLayout());
          BottomPanel         .setLayout(new GridLayout(2,4,10,10));
          TopPanel            .setBorder(new TitledBorder("Control Block"));
     }
     
     public void addComponents()
     {

		SAuthUserCode="";
		SAuthUserCode  = (String)VIUserCode.elementAt(JCUser.getSelectedIndex());
		MiddlePanel    = new JobWorkIssueMiddlePanel(Layer,iMillCode,SItemTable,SSupTable,SAuthUserCode,TSupCode,LNet,TOthers);
          Top1Panel         .add(new JLabel("  Name of the Party"));
          Top1Panel         .add(BSupplier);

          Top1Panel         .add(new MyLabel("DC No"));
          Top1Panel         .add(TDCNo);

          Top1Panel         .add(new JLabel("DC Date"));
          Top1Panel         .add(TDCDate);
          
          Top1Panel         .add(new MyLabel("Vehicle No"));
          Top1Panel         .add(TSentThro);

          Top1Panel         .add(new MyLabel("Purpose"));
          Top1Panel         .add(TPurpose);

          Top1Panel         .add(new JLabel("  Select Materials"));
          Top1Panel         .add(BMaterial);

          BottomPanel      .add(new JLabel("F2 - For HSNCode"));
          BottomPanel      .add(new JLabel("F3 in Qty for Sales Qty"));
          BottomPanel      .add(new JLabel("Others"));
          BottomPanel      .add(TOthers);

          BottomPanel      .add(new JLabel(""));
          BottomPanel      .add(BOk);
          BottomPanel      .add(new JLabel("Net"));
          BottomPanel      .add(LNet);

          TopPanel         .add("Center",Top1Panel);

          getContentPane() . add("North",TopPanel);
          getContentPane() . add("Center",MiddlePanel);
         
          getContentPane() . add("South",BottomPanel);
     }

     public void addListeners()
     {
          BSupplier.addActionListener(new SupplierSearch(Layer,TSupCode,SSupTable));
          BMaterial.addActionListener(new OnClick());
          BOk.addActionListener(new ActList());
     }

     private class OnClick implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               
               String str = (TSupCode.getText()).trim();

               if(str.length()==0)
               {
                    JOptionPane.showMessageDialog(null,"Please Select Party ","Information",JOptionPane.INFORMATION_MESSAGE);
               }
               else
               {
                    MiddlePanel.setDataIntoVector();
                    if(MiddlePanel.VCode.size()>0)
                    {
                         BSupplier.setEnabled(false);
                         BMaterial.setEnabled(false);
                         MiddlePanel.showMaterialSelectionFrame(iClicked);
                         BOk.setEnabled(true);
                    }
                    else
                    {
                         BSupplier.setEnabled(true);
                         BMaterial.setEnabled(true);
                         BOk.setEnabled(false);
                         JOptionPane    .showMessageDialog(null,"No Material Available","Information",JOptionPane.INFORMATION_MESSAGE);
                    }
               }
           
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    if(setData())
                    {
                         getInvNo();
                         getVocNo();
                         getIndenNo();
                         getIssueNo();
                         insertTransferDetails();
                         insertIssueDetails();
                         getACommit();
                         removeHelpFrame();
                    }
                    else
                    {
                         BOk.setEnabled(true);
                    }
               }
          }
     }

   
     public boolean setData()
     {
          if(!isValidData())
               return false;
          
          if(!isStockValid())
               return false;
          
          return true;
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer     .remove(this);
               Layer     .repaint();
               Layer     .updateUI();
          }
          catch(Exception ex) { }
     }

     private void getInvNo()
     {
          iInvoiceNo=0;
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               
               String QS = " Select MaxNo from invoiceconfig where millcode="+iMillCode+" for update of MaxNo nowait";

               PreparedStatement thePrepare    = theConnection.prepareStatement(QS);

               ResultSet theResult             = thePrepare.executeQuery();
               if(theResult.next())
                  iInvoiceNo = theResult.getInt(1)+1;

               theResult.close();
               thePrepare.close();

               thePrepare = theConnection.prepareStatement(" Update invoiceconfig set MaxNo=? where millcode="+iMillCode+" " );
               thePrepare.setInt(1,iInvoiceNo);
System.out.println("InvoiceConFig==>"+QS);
               thePrepare.executeUpdate();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);

               String SException = String.valueOf(ex);

               System.out.println("Indent indentNo: "+ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getInvNo();
                    bComflag = true;
               }
               else
               {
                    bComflag = false;
               }
          }
     }

   //alpa connection

  private void getVocNo()
     {
          iVocNo=0;
          try
          {
               if(theConnection1  . getAutoCommit())
                    theConnection1  . setAutoCommit(false);
               
               String QS = " Select MAXNUMBER from noconfig  where VOCTYPE='SJVOCNO' and DIVISIONCODE="+iDivCode+" and yearcode="+SYearCode+"  for update of MAXNUMBER nowait";

               PreparedStatement thePrepare    = theConnection1.prepareStatement(QS);

               ResultSet theResult             = thePrepare.executeQuery();
               if(theResult.next())
                  iVocNo = theResult.getInt(1)+1;

               theResult.close();
               thePrepare.close();

               thePrepare = theConnection1.prepareStatement(" Update noconfig set MAXNUMBER=? where VOCTYPE='SJVOCNO' and DIVISIONCODE="+iDivCode+" and yearcode="+SYearCode+" ");
               thePrepare.setInt(1,iVocNo);
System.out.println("NoCofig====>"+QS);
               thePrepare.executeUpdate();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);

               String SException = String.valueOf(ex);

               System.out.println("Indent indentNo: "+ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getVocNo();
                    bComflag = true;
               }
               else
               {
                    bComflag = false;
               }
          }
     }

     private void getIndenNo()
     {
          iIndentNo=0;
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               
               String QS = " Select IndentNo from IndentNoConfig for update of indentNo nowait";

               PreparedStatement thePrepare    = theConnection.prepareStatement(QS);

               ResultSet theResult             = thePrepare.executeQuery();
               if(theResult.next())
                  iIndentNo = theResult.getInt(1)+1;

               theResult.close();
               thePrepare.close();

               thePrepare = theConnection.prepareStatement(" Update IndentNoConfig set indentNo=?");
               thePrepare.setInt(1,iIndentNo);
               thePrepare.executeUpdate();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);

               String SException = String.valueOf(ex);

               System.out.println("Indent indentNo: "+ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getIndenNo();
                    bComflag = true;
               }
               else
               {
                    bComflag = false;
               }
          }
     }

     private void getIssueNo()
     {
          iIssueNo=0;
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);
               
               String QS = " Select MaxNo From Config"+iMillCode+""+SYearCode+" Where Id = 5 for update of MaxNo nowait";

               PreparedStatement thePrepare    = theConnection.prepareStatement(QS);

               ResultSet theResult             = thePrepare.executeQuery();
               if(theResult.next())
                  iIssueNo = theResult.getInt(1)+1;

               theResult.close();
               thePrepare.close();

               thePrepare = theConnection.prepareStatement(" Update Config"+iMillCode+""+SYearCode+" set MaxNo=? Where Id=5 ");
               thePrepare.setInt(1,iIssueNo);
               thePrepare.executeUpdate();
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);

               String SException = String.valueOf(ex);

               System.out.println("IssueNo: "+ex);
     
               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getIssueNo();
                    bComflag = true;
               }
               else
               {
                    bComflag = false;
               }
          }
     }

     private void insertTransferDetails()
     {
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);

               if(theConnection1  . getAutoCommit())
                    theConnection1  . setAutoCommit(false);
               
               String SInsQS1 = " Insert Into STORESINVOICE(Id,INVNO,INVDATE,DCNO,DCDATE,ITEMCODE,HSNCODE,QTY,RATE,BASIC,CGST,SGST,IGST,CESS,CGSTVAL,SGSTVAL,IGSTVAL,CESSVAL,OTHERS,NET,ENTRYDATETIME,VEHICLENO,MILLCODE,PARTYCODE) "+
                                " Values (STORESINVOICE_Seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


              String SSalesJournal1= "  Insert into SalesJournal ( VocNo, VocDate, VocTYpe, AcCode, Narr, Debit, Credit, InvNo, InvDate, DivisionCode, YearCode, Weight, NoofBags    )    "+              
                                    "  values  ( ?,?,?,?,?,?,?,?,?,?,?,?,?    ) ";  

              
              String SSalesJournal2= "  Insert into SalesJournal ( VocNo, VocDate, VocTYpe, AcCode, Narr, Debit, Credit, InvNo, InvDate, DivisionCode, YearCode, Weight, NoofBags    )    "+              
                                    "  values  ( ?,?,?,?,?,?,?,?,?,?,?,?,?    ) "; 

              String SSalesJournal3= "  Insert into SalesJournal ( VocNo, VocDate, VocTYpe, AcCode, Narr, Debit, Credit, InvNo, InvDate, DivisionCode, YearCode, Weight, NoofBags    )    "+              
                                    "  values  ( ?,?,?,?,?,?,?,?,?,?,?,?,?    ) ";  

              String SSalesJournal4= "  Insert into SalesJournal ( VocNo, VocDate, VocTYpe, AcCode, Narr, Debit, Credit, InvNo, InvDate, DivisionCode, YearCode, Weight, NoofBags    )    "+              
                                    "  values  ( ?,?,?,?,?,?,?,?,?,?,?,?,?    ) ";  

              
              String SSalesJournal5= "  Insert into SalesJournal ( VocNo, VocDate, VocTYpe, AcCode, Narr, Debit, Credit, InvNo, InvDate, DivisionCode, YearCode, Weight, NoofBags    )    "+              
                                    "  values  ( ?,?,?,?,?,?,?,?,?,?,?,?,?    ) "; 

              String SSalesJournal6= "  Insert into SalesJournal ( VocNo, VocDate, VocTYpe, AcCode, Narr, Debit, Credit, InvNo, InvDate, DivisionCode, YearCode, Weight, NoofBags    )    "+              
                                    "  values  ( ?,?,?,?,?,?,?,?,?,?,?,?,?    ) ";     
            
              String SSalesJournal7= "  Insert into SalesJournal ( VocNo, VocDate, VocTYpe, AcCode, Narr, Debit, Credit, InvNo, InvDate, DivisionCode, YearCode, Weight, NoofBags    )    "+              
                                    "  values  ( ?,?,?,?,?,?,?,?,?,?,?,?,?    ) ";                         
                

               int iSlNo=0;

			 double	dTotalBasic=0;
			 double	dTotalSGST=0;
			 double	dTotalCGST=0;
			 double	dTotalIGST=0;
			 double	dTotalQty=0;
            
               String SInvoiceNo="";
               String SSupCode   = (TSupCode.getText()).trim();
               String SDCNo      = common.parseNull(TDCNo.getText().trim());
	       String SDCDate    = TDCDate.toNormal();
               String SOthers    = (TOthers.getText()).trim();
               String SSentThro  = (common.parseNull(TSentThro.getText().trim())).toUpperCase();
               String SPurpose   = (common.parseNull(TPurpose.getText().trim())).toUpperCase();

               String SSalesCode ="";

               if(iMillCode==0)
                SSalesCode ="A_1216";
               else
                SSalesCode ="A_216";

               String  SInvoiceDate=common.getServerDate().substring(0,8);

                 if(iMillCode==0){
                    SInvoiceNo= (SPreFix+iInvoiceNo);
                 }else{
                    SInvoiceNo= (SPreFix+iInvoiceNo);
                    
                 }

				PreparedStatement thePrepare2 = theConnection1.prepareStatement(SSalesJournal1);
				PreparedStatement thePrepare3 = theConnection1.prepareStatement(SSalesJournal2);
				PreparedStatement thePrepare4 = theConnection1.prepareStatement(SSalesJournal3);
				PreparedStatement thePrepare5 = theConnection1.prepareStatement(SSalesJournal4);
				PreparedStatement thePrepare6 = theConnection1.prepareStatement(SSalesJournal5);
				PreparedStatement thePrepare7 = theConnection1.prepareStatement(SSalesJournal6);
				PreparedStatement thePrepare8 = theConnection1.prepareStatement(SSalesJournal7);


               for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
               {
                    iSlNo++;

                    PreparedStatement thePrepare1 = theConnection.prepareStatement(SInsQS1);

                    String SItemCode   = ((String)MiddlePanel.tabreport.RowData[i][1]).trim();
                    String SItemName   = ((String)MiddlePanel.tabreport.RowData[i][2]).trim();
                    String SHsnCode    = ((String)MiddlePanel.tabreport.RowData[i][3]).trim();
                    String SUomName    = ((String)MiddlePanel.tabreport.RowData[i][4]).trim();
                    int iUomCode       =  getUomCode(SUomName);
                    String SStockRate  = (String)MiddlePanel.RateData[i];
                    double dQty        = common.toDouble((String)MiddlePanel.tabreport.RowData[i][6]);
                    double dRate        = common.toDouble((String)MiddlePanel.tabreport.RowData[i][7]);
                    double dStockValue = common.toDouble(common.getRound(dQty * common.toDouble(SStockRate),2));
                    double dBasic      = common.toDouble((((String)MiddlePanel.tabreport.RowData[i][12]).trim()));
                    double dCGST       = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][8]).trim());
                    double dSGST       = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][9]).trim());
                    double dIGST       = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][10]).trim());
                    double dCESS       = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][11]).trim());
                    double dCGSTVal    = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][13]).trim());
                    double dSGSTVal    = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][14]).trim());
                    double dIGSTVal    = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][15]).trim());
                    double dCESSVal    = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][16]).trim());
                    double dValue      = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][17]).trim());

                    double dNet = common.toDouble((LNet.getText()).trim());
                    String SLastRate   = SStockRate;
                    double dBasicValue = dStockValue;

                    if(SItemCode.length() == 0)
                         continue;

                    thePrepare1.setString(1,SInvoiceNo);
                    thePrepare1.setString(2,SInvoiceDate);
                    thePrepare1.setString(3,SDCNo);
                    thePrepare1.setString(4,SDCDate);
                    thePrepare1.setString(5,SItemCode);
                    thePrepare1.setString(6,SHsnCode);
                    thePrepare1.setDouble(7,dQty);
                    thePrepare1.setDouble(8,dRate);
                    thePrepare1.setDouble(9,dBasic);
                    thePrepare1.setDouble(10,dCGST);
                    thePrepare1.setDouble(11,dSGST);
                    thePrepare1.setDouble(12,dIGST);
                    thePrepare1.setDouble(13,dCESS);
                    thePrepare1.setDouble(14,dCGSTVal);
                    thePrepare1.setDouble(15,dSGSTVal);
                    thePrepare1.setDouble(16,dIGSTVal);
                    thePrepare1.setDouble(17,dCESSVal);
                    thePrepare1.setString(18,SOthers);
                    thePrepare1.setDouble(19,dValue);
                    thePrepare1.setString(20,SInvoiceDate);
                    thePrepare1.setString(21,SSentThro);
                    thePrepare1.setInt(22,iMillCode);
                    thePrepare1.setString(23,SSupCode);
                  
                   thePrepare1.executeUpdate();
                    thePrepare1.close();

					dTotalBasic+=dBasic;
					dTotalSGST+=dSGSTVal;
					dTotalCGST+=dCGSTVal;
					dTotalIGST+=dIGSTVal;
					dTotalQty+=dQty;
          
               }

                    double dTotalValue = common.toDouble((LNet.getText()).trim());
               
                    thePrepare2.setInt(1,iVocNo);
                    thePrepare2.setString(2,SInvoiceDate);
                    thePrepare2.setString(3,"SJV");
                    thePrepare2.setString(4,SSupCode);
                    thePrepare2.setString(5,SPurpose);
                    thePrepare2.setDouble(6,dTotalValue);
                    thePrepare2.setString(7,"0");
                    thePrepare2.setString(8,SInvoiceNo);
                    thePrepare2.setString(9,SInvoiceDate);
                    thePrepare2.setInt(10,iDivCode);
                    thePrepare2.setString(11,SYearCode);
                    thePrepare2.setDouble(12,dTotalQty);
                    thePrepare2.setString(13,"0");
  

                    thePrepare2.executeUpdate();

                    thePrepare2.close();

   
                    thePrepare3.setInt(1,iVocNo);
                    thePrepare3.setString(2,SInvoiceDate);
                    thePrepare3.setString(3,"SJV");
                    thePrepare3.setString(4,SSalesCode);
                    thePrepare3.setString(5,SPurpose);
                    thePrepare3.setString(6,"0");
                    thePrepare3.setDouble(7,dTotalBasic);
                    thePrepare3.setString(8,SInvoiceNo);
                    thePrepare3.setString(9,SInvoiceDate);
                    thePrepare3.setInt(10,iDivCode);
                    thePrepare3.setString(11,SYearCode);
                    thePrepare3.setDouble(12,dTotalQty);
                    thePrepare3.setString(13,"0");
  

                   thePrepare3.executeUpdate();
                    thePrepare3.close();

                   //SGST
                   if(dTotalSGST>0)
                   {

                    thePrepare4.setInt(1,iVocNo);
                    thePrepare4.setString(2,SInvoiceDate);
                    thePrepare4.setString(3,"SJV");
                    thePrepare4.setString(4,"A_5995");
                    thePrepare4.setString(5,SPurpose);
                    thePrepare4.setString(6,"0");
                    thePrepare4.setDouble(7,dTotalSGST);
                    thePrepare4.setString(8,SInvoiceNo);
                    thePrepare4.setString(9,SInvoiceDate);
                    thePrepare4.setInt(10,iDivCode);
                    thePrepare4.setString(11,SYearCode);
                    thePrepare4.setDouble(12,dTotalQty);
                    thePrepare4.setString(13,"0");
  

                   thePrepare4.executeUpdate();
                    thePrepare4.close();

                  }
                  //CGST

                 if(dTotalCGST>0)
                   {

                    thePrepare5.setInt(1,iVocNo);
                    thePrepare5.setString(2,SInvoiceDate);
                    thePrepare5.setString(3,"SJV");
                    thePrepare5.setString(4,"A_5994");
                    thePrepare5.setString(5,SPurpose);
                    thePrepare5.setString(6,"0");
                    thePrepare5.setDouble(7,dTotalCGST);
                    thePrepare5.setString(8,SInvoiceNo);
                    thePrepare5.setString(9,SInvoiceDate);
                    thePrepare5.setInt(10,iDivCode);
                    thePrepare5.setString(11,SYearCode);
                    thePrepare5.setDouble(12,dTotalQty);
                    thePrepare5.setString(13,"0");
  

                    thePrepare5.executeUpdate();
                    thePrepare5.close();

                  }

                   //IGST

                 if(dTotalIGST>0)
                   {

                    thePrepare6.setInt(1,iVocNo);
                    thePrepare6.setString(2,SInvoiceDate);
                    thePrepare6.setString(3,"SJV");
                    thePrepare6.setString(4,"A_5996");
                    thePrepare6.setString(5,SPurpose);
                    thePrepare6.setString(6,"0");
                    thePrepare6.setDouble(7,dTotalIGST);
                    thePrepare6.setString(8,SInvoiceNo);
                    thePrepare6.setString(9,SInvoiceDate);
                    thePrepare6.setInt(10,iDivCode);
                    thePrepare6.setString(11,SYearCode);
                    thePrepare6.setDouble(12,dTotalQty);
                    thePrepare6.setString(13,"0");
  

                    thePrepare6.executeUpdate();
                    thePrepare6.close();

                  }
                  // TOthers >0
                  if(common.toDouble(SOthers) > 0)
                  { 
                    thePrepare7.setInt(1,iVocNo);
                    thePrepare7.setString(2,SInvoiceDate);
                    thePrepare7.setString(3,"SJV");
                    thePrepare7.setString(4,"A_9");
                    thePrepare7.setString(5,SPurpose);
                    thePrepare7.setString(6,"0");
                    thePrepare7.setString(7,SOthers);
                    thePrepare7.setString(8,SInvoiceNo);
                    thePrepare7.setString(9,SInvoiceDate);
                    thePrepare7.setInt(10,iDivCode);
                    thePrepare7.setString(11,SYearCode);
                    thePrepare7.setDouble(12,dTotalQty);
                    thePrepare7.setString(13,"0");
  

                    thePrepare7.executeUpdate();
                    thePrepare7.close();

                  }

                 //TOthers <0

                 if(common.toDouble(SOthers) < 0)
                  { 
                    thePrepare8.setInt(1,iVocNo);
                    thePrepare8.setString(2,SInvoiceDate);
                    thePrepare8.setString(3,"SJV");
                    thePrepare8.setString(4,"A_9");
                    thePrepare8.setString(5,SPurpose);
                    thePrepare8.setString(6,"0");
                    thePrepare8.setDouble(7,(common.toDouble(SOthers)*-1));
                    thePrepare8.setString(8,SInvoiceNo);
                    thePrepare8.setString(9,SInvoiceDate);
                    thePrepare8.setInt(10,iDivCode);
                    thePrepare8.setString(11,SYearCode);
                    thePrepare8.setDouble(12,dTotalQty);
                    thePrepare8.setString(13,"0");
  

                    thePrepare8.executeUpdate();
                    thePrepare8.close();

                  }  
           
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag = false;
          }
     }

     private void insertIssueDetails()
     {
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);

              /* String SInsQS2 = " Insert Into RDC (Id,RdcType,RdcNo,RdcDate,Sup_Code,Descript,Dept_Code,Unit_Code,Remarks,Qty,DueDate,Thro,SlNo,UserCode,ModiDate,MillCode,AssetFlag,DocType,UomCode,GodownAsset,MemoNo,MemoSlNo,MemoAuthUserCode) "+
                                " Values (Rdc_Seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";*/


                  String SIssueQuery = " Insert Into Issue(Id,IssueNo,IssueDate,Code,Qty,IssRate,HodCode,UIRefNo,Unit_Code,Dept_Code,Group_Code,UserCode,MillCode,Authentication,SlNo,CreationDate,IssueValue,IndentType,MrsAuthUserCode,IndentUserCode,PartyStatus,PartyCode,RDCNo) "+
                                " Values (Issue_Seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


               int iSlNo=0;

               int iIndentType   = 5;
               String SSupCode   = (TSupCode.getText()).trim();
               String SDCNo      = common.parseNull(TDCNo.getText().trim());

               String SUnitCode  = "0";
               String SDeptCode  = "76";
               String SGroupCode = "195";

               for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
               {
                    String SItemCode   = ((String)MiddlePanel.tabreport.RowData[i][1]).trim();

                    if(SItemCode.length() == 0)
                         continue;

                    double dQty        = common.toDouble((String)MiddlePanel.tabreport.RowData[i][6]);

		    if(dQty<=0)
		         continue;


            for(int k=0;k<MiddlePanel.VMUserCode[i].size();k++)
		    {

		            iSlNo++;

		            PreparedStatement theIssPrepare = theConnection.prepareStatement(SIssueQuery);

                    String SAuthUserCode = (String)MiddlePanel.VMUserCode[i].elementAt(k);
		            String SStockRate    = (String)MiddlePanel.VMUserRate[i].elementAt(k);
		            double dUserQty      = common.toDouble((String)MiddlePanel.VMUserQty[i].elementAt(k));
		            double dStockValue   = common.toDouble(common.getRound(dUserQty * common.toDouble(SStockRate),2));


		            theIssPrepare.setInt(1,iIssueNo);
		            theIssPrepare.setString(2,common.getServerDate().substring(0,8));
		            theIssPrepare.setString(3,SItemCode);
		            theIssPrepare.setDouble(4,dUserQty);
		            theIssPrepare.setString(5,SStockRate);
		            theIssPrepare.setInt(6,0);
		            theIssPrepare.setInt(7,iIndentNo);
		            theIssPrepare.setString(8,SUnitCode);
		            theIssPrepare.setString(9,SDeptCode);
		            theIssPrepare.setString(10,SGroupCode);
		            theIssPrepare.setInt(11,iUserCode);
		            theIssPrepare.setInt(12,iMillCode);
                    theIssPrepare.setInt(13,1);
                    theIssPrepare.setInt(14,iSlNo);
                    theIssPrepare.setString(15,common.getServerDate());
                    theIssPrepare.setString(16,common.getRound(dStockValue,2));
                    theIssPrepare.setInt(17,iIndentType);
                    theIssPrepare.setString(18,SAuthUserCode);
                    theIssPrepare.setString(19,SAuthUserCode);
                    theIssPrepare.setInt(20,1);
                    theIssPrepare.setString(21,SSupCode);
                    theIssPrepare.setString(22,SDCNo);
                    theIssPrepare.executeUpdate();
		            theIssPrepare.close();


                    System.out.println(dUserQty+","+SAuthUserCode+","+iMillCode);

                    /*PreparedStatement theUpItemStk = theConnection.prepareStatement(" Update ItemStock set Stock=Stock-? where HodCode=? and MillCode=?");
                   

                    theUpItemStk.setDouble(1,dUserQty);
                    theUpItemStk.setString(2,SAuthUserCode);
                    theUpItemStk.setInt(3,iMillCode);

                    theUpItemStk.executeUpdate();

                    theUpItemStk.close();*/

		    }
               }

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               bComflag = false;
          }
     }

     private void getACommit()
     {
          try
          {

                  // inventory  and alpaconn

               if(bComflag)
               {
                    theConnection  . commit();
                     theConnection1  . commit();

                    if(iMillCode==1)
                         theDConnection . commit();

                    JOptionPane    . showMessageDialog(null,"Data Saved Sucessfully");
                    System         . out.println("Commit");
               }
               else
               {
                    theConnection  . rollback();
                    theConnection1  . rollback();

                    if(iMillCode==1)
                         theDConnection . rollback();

                    JOptionPane    . showMessageDialog(null,"Sorry,The Entered Data is Not Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                    System         . out.println("RollBack");
               }
               theConnection   . setAutoCommit(true);
               theConnection1   . setAutoCommit(true); 

            

               if(iMillCode==1)
                    theDConnection . setAutoCommit(true);

          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private int getUomCode(String SUomName)
     {
          int index= VUom.indexOf(common.parseNull(SUomName));

          if(index==-1)
              return 0;
          else
              return (common.toInt((String)VUomCode.elementAt(index)));
     }

     private boolean isValidData()
     {
          try
          {
               String SDCNo        = common.parseNull(TDCNo.getText().trim());
               String SSentThro    = common.parseNull(TSentThro.getText().trim());
               String SPurpose     = common.parseNull(TPurpose.getText().trim());

               if(SDCNo.length()==0)
               {
                    JOptionPane.showMessageDialog(null,"DCNo Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
                    TDCNo.requestFocus();
                    return false;
               }

               if(SSentThro.length()==0)
               {
                    JOptionPane.showMessageDialog(null,"Sent Thro Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
                    TSentThro.requestFocus();
                    return false;
               }

               if(SPurpose.length()==0)
               {
                    JOptionPane.showMessageDialog(null,"Purpose Field is empty ","Error",JOptionPane.ERROR_MESSAGE);
                    TPurpose.requestFocus();
                    return false;
               }

               if((TDCDate.toNormal()).length()!=8)
               {
                    JOptionPane.showMessageDialog(null,"DC Date Field is Invalid","Invalid Field",JOptionPane.INFORMATION_MESSAGE);
                    return false;
               }

                   String SSupCode = (TSupCode.getText()).trim();
                 getSupData(SSupCode);
           for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
          {
               String SCode     = ((String)MiddlePanel.tabreport.RowData[i][1]).trim();
               String SHsnCode     = ((String)MiddlePanel.tabreport.RowData[i][3]).trim();
              
             
               double dQty         = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][6]).trim());
               double dRate        = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][7]).trim());
               
               double dCGST    = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][8]).trim());
               double dSGST    = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][9]).trim());
               double dIGST    = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][10]).trim());
 
            
              if(!SCode.equals(""))
              {
               if(SHsnCode.equals(""))
               {
                    JOptionPane.showMessageDialog(null,"HSNCode for Item No "+(i+1)+" Not Updated","Error",JOptionPane.ERROR_MESSAGE);
	                    return false;

               }
               if(dQty==0)
               {
                    JOptionPane.showMessageDialog(null,"Quantity for Item No "+(i+1)+" Not Updated","Error",JOptionPane.ERROR_MESSAGE);
	                    return false;

               }
	     
	            if(dCGST<=0 && dSGST<=0 && dIGST<=0)
	            {
                    JOptionPane.showMessageDialog(null,"Tax Data for Item No "+(i+1)+" Not Updated","Error",JOptionPane.ERROR_MESSAGE);
	                    return false;
	            }

	            if(dCGST<dSGST || dCGST>dSGST)
	            {
                       JOptionPane.showMessageDialog(null,"CGST and SGST Tax Data for Item No "+(i+1)+" Wrongly Updated ","Error",JOptionPane.ERROR_MESSAGE);
	                   return false;
	            }

	          
              }
                 
               
	           /* if(SSeleStateCode.equals("33"))
	            {
		         if(dIGST>0)
		         {
                  JOptionPane.showMessageDialog(null,"IGST Tax Data not applicable for this Party for Item No "+(i+1)+" ","Error",JOptionPane.ERROR_MESSAGE);
			     return false;
		         }
	            }
	            else
	            {
		         if(dCGST>0 || dSGST>0)
		         {
                    JOptionPane.showMessageDialog(null,"CGST/SGST Tax Data not applicable for this Party for Item No "+(i+1)+" ","Error",JOptionPane.ERROR_MESSAGE);
			      return false;
		         }
	            }*/

          }
     
          }
          catch(Exception ex)
          {
               System.out.println(" isValid in SalesFrame ->"+ex);
          }
          return true;
     }

     private boolean isStockValid()
     {
          boolean bflag = true;
          
          for(int i=0;i<MiddlePanel.tabreport.RowData.length;i++)
          {
               String Smsg="";
               String SCode     = ((String)MiddlePanel.tabreport.RowData[i][1]).trim();
               double dStock    = common.toDouble(((String)MiddlePanel.tabreport.RowData[i][5]).trim());
               
               if(SCode.length() == 0)
                    continue;
               
               if(dStock<0)
                    Smsg = Smsg + " Negative Stock for the Date,"; 
               if(Smsg.length()>0)
               {
                    Smsg = Smsg.substring(0,(Smsg.length()-1));
                    Smsg = Smsg + " in S.No - " + (i+1);
                    JOptionPane.showMessageDialog(null,Smsg,"Invalid Quantity",JOptionPane.INFORMATION_MESSAGE);
                    bflag=false;
               }
          }
          return bflag;
     }

     public void setParamVectors()
     {
          VUom      = new Vector();
          VUomCode  = new Vector();
          iDivCode=0;
          SPreFix="";  
          try
          {
               Statement stat = theConnection.createStatement();
               ResultSet res  = stat.executeQuery("Select UomName,UomCode From Uom Order By UomCode");
               while(res.next())
               {
                    VUom      .addElement(res.getString(1));
                    VUomCode  .addElement(res.getString(2));
               }
               res.close();
               res  = stat.executeQuery("select DivCode from mill where millcode="+iMillCode+"");
               while(res.next())
               {
                    iDivCode     = (res.getInt(1));
                    
               }
               res.close();


               res  = stat.executeQuery("select prefix from invoiceconfig where millcode="+iMillCode+"");
               while(res.next())
               {
                    SPreFix     = (res.getString(1));
                    
               }
               res.close();
                
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void getUsers()
     {
          ResultSet result    =    null;
          Statement stat      =    null;

          VIUserCode          = new Vector();
          VIUserName          = new Vector();

          int iDivisionCode=0;

          if(iMillCode==0)
          {
               iDivisionCode = 1;
          }
          else
          if(iMillCode==1)
          {
               iDivisionCode = 2;
          }
          else
          if(iMillCode==3)
          {
               iDivisionCode = 15;
          }
          else
          if(iMillCode==4)
          {
               iDivisionCode = 14;
          }

          try
          {
               stat =  theConnection.createStatement();
               String QS1 = "";
               
               QS1 = " Select UserCode,UserName from RawUser Where UserCode in (Select Distinct(AuthUserCode) from MrsUserAuthentication Where AuthUserCode>0 and authusercode<>6125) and (DivisionCode=6 or DivisionCode="+iDivisionCode+") Order by UserName ";

               result = stat.executeQuery(QS1);
               while(result.next())
               {
                    VIUserCode. addElement(result.getString(1));
                    VIUserName. addElement(result.getString(2));
               }
               result    . close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("  getUsers SalesFrame :"+ex);
          }
     }

      public void getSupData(String SSupCode)
      {
	       SSeleSupType   = "";
	       SSeleStateCode = "";

               String QString = " Select decode(PartyMaster.StateCode,0,'0',decode(PartyMaster.CountryCode,'61',nvl(State.GstStateCode,0),'999')) as GstStateCode, "+
			        " nvl(PartyMaster.GstPartyTypeCode,0) as GstPartyTypeCode From "+SSupTable+
				" Inner Join PartyMaster on "+SSupTable+".Ac_Code=PartyMaster.PartyCode and "+SSupTable+".Ac_Code='"+SSupCode+"'"+
				" Inner Join State on PartyMaster.StateCode=State.StateCode";
               try
               {
                    Statement theStatement    = theConnection.createStatement();

                    ResultSet res = theStatement.executeQuery(QString);
                    while(res.next())
                    {
			            SSeleStateCode = common.parseNull(res.getString(1));
                        SSeleSupType   = common.parseNull(res.getString(2));
                    }
                    res            . close();
                    theStatement   . close();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
      }


 

}
