package Sales;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

//import com.amarjothi.oracledb.Common;
import java.util.*;
import java.io.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

public class SalesInvoicePdfPrint extends JInternalFrame
{
	JLayeredPane            	Layer;	
	private Common              common;
	JPanel                   	TopPanel,MiddlePanel,BottomPanel;
	JButton      				BApply,BPrint,BExit;
	DateField     				TFromDate,TToDate;
	private       				JTable  theTable;
    private       				SalesInvoicePdfPrintModel    theModel;	
	String         				SFromDate,SToDate;
	ArrayList                   AList;
	
	private Document            document,document1;
	private PdfPTable           table;

	private int                 iWidth[]            = {20,50,38,30,35,35,20,35,20,35,20,35,20,35};
	private Font                bigbold             = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
	private Font                mediumbigbold       = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
	private Font                mediumbold          = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
	private Font                mediumboldunderline = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD|Font.UNDERLINE);
	private Font                mediumnormal        = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);

	private String SFileWritePath="";

	int iWidth1[] = {15,30,20,20,20,30};
    private Font                smallbold           = FontFactory.getFont("TIMES_ROMAN",7,Font.BOLD);
    private static Font smallnormal=FontFactory.getFont("TIMES_ROMAN",7,Font.NORMAL);

	String SCess1,SCessPer1;
	   
    public SalesInvoicePdfPrint(JLayeredPane Layer)
    {
		this.Layer      = Layer;
		common          = new Common();
       
		createComponents();
        setLayouts();
        addComponents();
		addListeners();
	}
	private void createComponents()
    {
        TopPanel      = new JPanel();
        MiddlePanel   = new JPanel();
        BottomPanel   = new JPanel();
                  
        TFromDate     = new DateField();
        TToDate       = new DateField();
            
        BExit         = new JButton("Exit");
        BApply        = new JButton("Apply");
		BPrint        = new JButton("Print");
                
        theModel      = new SalesInvoicePdfPrintModel();
        theTable      = new JTable(theModel);
                 
        for(int i=0;i<theModel.ColumnName.length;i++)
        {
           (theTable.getColumnModel()) . getColumn(i) . setPreferredWidth(theModel.iColumnWidth[i]);
        }
    }
    private void setLayouts()
    {
    
         TopPanel      . setLayout(new GridLayout(1,5));
         MiddlePanel   . setLayout(new BorderLayout());
         BottomPanel   . setLayout(new FlowLayout(FlowLayout.CENTER));
          
         TopPanel      . setBorder(new TitledBorder(" Filter "));
         MiddlePanel   . setBorder(new TitledBorder(" Details "));
         BottomPanel   . setBorder(new TitledBorder(" Controls "));
         
         this . setTitle("Sales Invocie Print Details");
         this . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
         this . setSize(900,600);
    }
    private void addComponents()
    {
		TopPanel . add(new JLabel(" FROM DATE "))  ;
        TopPanel . add(TFromDate);
        TopPanel . add(new JLabel(" TO DATE "))  ;
        TopPanel . add(TToDate);
        TopPanel . add(BApply);
        
        MiddlePanel.add(new JScrollPane(theTable));

		BottomPanel . add(BExit);
		BottomPanel . add(BPrint);

        this . add(TopPanel,BorderLayout.NORTH);
        this . add(MiddlePanel,BorderLayout.CENTER);
        this . add(BottomPanel,BorderLayout.SOUTH);
    }
    private void addListeners()
    {
        BApply . addActionListener(new ActList());
    	BExit  . addActionListener(new ActList());
		BPrint . addActionListener(new PrintList());
        
    }
	private class ActList implements ActionListener
    {
		public void actionPerformed(ActionEvent ae)   
		{
			SFromDate     = common.pureDate(TFromDate.toString());
			SToDate       = common.pureDate(TToDate.toString());
			if(ae.getSource()==BApply)
			{
				theModel.setNumRows(0);
				setTableDetails();
			}
		
			if(ae.getSource()==BExit)
			{
			   dispose();
			}
		}
    }
	public class PrintList implements ActionListener
    {
    	public void actionPerformed(ActionEvent ae)
        {
    		try
			{
				FileWriter FW = new FileWriter("d:\\Invoice.prn");
				    
          		for(int i=0;i<theModel.getRowCount();i++)
                {
                	Boolean BSig = (Boolean)theModel.getValueAt(i,19);
                                   
                    if(!BSig.booleanValue())
                    	continue;

					String SInvNo   = (String)theModel.getValueAt(i,1);
                    String SInvDate = (String)theModel.getValueAt(i,2);
     					
                    try
                    {
						initPdf();
                        
						for(int l=0;l<=3;l++)
						{
							String SHeading = "";
							if(l==0)
								SHeading = "ORIGINAL FOR BUYER"	;
							if(l==1)
								SHeading = "TRIPLICATE FOR ASSESSEE"	;
							if(l==2)
								SHeading = "DUPLICATE FOR TRANSPORTER"	;
							if(l==3)
								SHeading = "QUADRUPLICATE FOR HEADOFFICE COPY"	;
					
							CreatePDF(SInvNo,SInvDate,SHeading);
						}
						document.close();
						
					}
                    catch(Exception e)
                    {
                    	System.out.println(e);
                    }
				}
				FW.close();
			}
            catch(Exception ex)
            {
            	System.out.println(ex);
            }

		}
	}
	private void setTableDetails()
    {
        theModel                           . setNumRows(0);
		SetVector();
		int iSlNo                          = 1;
          
		double dTotalAmt = 0;
			
		for(int i=0;i<AList.size();i++)
		{
			HashMap theMap = (HashMap)AList.get(i);
            
            ArrayList theList             = null;
            theList                       = new ArrayList();
            theList                       . clear();
            theList                       . add(String.valueOf(iSlNo++));
            theList                       . add((String)theMap.get("INVNO"));
            theList                       . add(common.parseDate((String)theMap.get("INVDATE")));
            theList                       . add(String.valueOf(theMap.get("DCNO")));
            theList                       . add(common.parseDate((String)theMap.get("DCDATE")));
            theList                       . add(String.valueOf(theMap.get("PARTYNAME")));
            theList                       . add(String.valueOf(theMap.get("HSNCODE")));
            theList                       . add(String.valueOf(theMap.get("QTY")));
            theList                       . add(String.valueOf(theMap.get("RATE")));
            theList                       . add(String.valueOf(theMap.get("BASIC")));
            theList                       . add(String.valueOf(theMap.get("CGST")));
            theList                       . add(String.valueOf(theMap.get("SGST")));
            theList                       . add(String.valueOf(theMap.get("IGST")));
            theList                       . add(String.valueOf(theMap.get("CESS")));
            theList                       . add(String.valueOf(theMap.get("CGSTSVAL")));
            theList                       . add(String.valueOf(theMap.get("SGSTVAL")));
            theList                       . add(String.valueOf(theMap.get("IGSTVAL")));
            theList                       . add(String.valueOf(theMap.get("OTHERS")));
            theList                       . add(String.valueOf(theMap.get("NET")));
            theList                       . add(new java.lang.Boolean(false));
   
	    theModel                      . appendRow(new Vector(theList));
        }
        
      /* ArrayList theList1             = null;
        theList1                       = new ArrayList();
        theList1                       . clear();
        theList1                       . add("Total");
      
        theList1                       . add("");
        theList1                       . add(dTotalAmt);
        theList1                       . add(null);
                  
        theModel                      . appendRow(new Vector(theList1));*/
    }
    private void SetVector()
    {
       
        SFromDate     = common.pureDate(TFromDate.toString());
        SToDate       = common.pureDate(TToDate.toString());
        
		PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuffer sb = new StringBuffer();
        AList = new ArrayList();
        AList.clear();
        
        try
        {
			ORAConnection connect         =    ORAConnection.getORAConnection();
            Connection     theConnection   =    connect.getConnection();
			pst = theConnection.prepareStatement(getQS());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd = rst.getMetaData();
            while (rst.next()) 
			{
				HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) 
				{
					themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
				}
                AList.add(themap);
                
            }
System.out.println("AList"+AList.size());
			          
           rst.close();
           pst.close();
          
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
			System.out.println(e);
        }
    }
	 private String getQS()
    {
        
        String QS = " Select InvNO,InvDate,DCNo,DCDate,StoresInvoice.ITEMCODE,InvItems.HsnCode,Qty,RATE,Basic,CGST,SGST,IGST,Cess,CgstVal,SgstVal,IgstVal,CessVal "+
					" ,StoresInvoice.Others,Net,PartyName "+
					" ,PartyMaster.ADDRESS1,PartyMaster.ADDRESS2,PartyMaster.ADDRESS3,PartyMaster.TNGSTNo ,PartyMaster.GSTINID"+
					" ,State.STATENAME,State.GSTSTATECODE,StoresInvoice.VehicleNo,Item_Name "+
					" from StoresInvoice "+
					" Inner join PArtyMaster on PartyMaster.PARTYCODE = StoresInvoice.PARTYCODE "+
					" Inner join State on State.STATECODE = PartyMaster.STATECODE "+
					" Inner join InvItems on InvItems.Item_Code = StoresInvoice.ITEMCODE "+
					" where InvDate>="+SFromDate+" and InvDate<="+SToDate+" ";
        System.out.println("QS:"+QS);
        return QS;
    }
	
private void initPdf() 
	{
    	try 
		{
			document  = new Document(PageSize.A4);
			String Newfile="Invoice.pdf";
			String OSName = System.getProperty("os.name");
            if(OSName.startsWith("Lin"))
			{
                SFileWritePath = "/root/"+Newfile;
            }
            else
			{
                SFileWritePath = "D:\\"+Newfile;
            }
            PdfWriter . getInstance(document, new FileOutputStream(SFileWritePath));
            document  . open();
		}
		catch(Exception ex) 
		{
			ex.printStackTrace();
        }
	}
	private void CreatePDF(String SInvNo,String SInvDate,String SHeading) 
	{
		try 
		{
			//initPdf();

			document.newPage();

		    Paragraph P1    = new Paragraph("", mediumbold);
			P1.add(SHeading);
			P1.setAlignment(Element.ALIGN_RIGHT);
		    P1.setSpacingAfter(10);
		    
		    document.add(P1);

			// set Pdf Table..
			table               = new PdfPTable(14);
			table               . setWidths(iWidth);
			table               . setWidthPercentage(100f);

            double dQty  =0;
            double dAmt  =0;

            int iCount =0;
            double dTotAmt  =0;
            double dVatAmt  =0,dTotCess=0,dTotCgst=0,dTotSgst=0,dTotIgst=0;
			double dPackingCGSTPer = 0.0,dPackingCGST = 0.0 ,dLoadingCGSTPer = 0.0,dLoadingCGST = 0.0;
			double dPackingSGSTPer = 0.0,dPackingSGST = 0.0 ,dLoadingSGSTPer = 0.0,dLoadingSGST = 0.0;
			double dPackingIGSTPer = 0.0,dPackingIGST = 0.0 ,dLoadingIGSTPer = 0.0,dLoadingIGST = 0.0;
            double dPackingCessPer = 0.0,dPackingCess = 0.0 ,dLoadingCessPer = 0.0,dLoadingCess = 0.0;
			double dPackingTotal = 0.0,dLoadingTotal = 0.0 ,dCessTotal = 0.0 ;
			double dTotalRate = 0.0,dTotalValue=0,dTotalCgstValue=0,dTotalSgstValue=0,dTotalIgstValue=0,dTotalCessValue=0,dTotalQty = 0.0;
			double dCessCGSTPer = 0.0,dCessCGST = 0.0,dCessSGSTPer =0.0,dCessSGST =0.0,dCessIGSTPer =0.0,dCessIGST =0.0,dCess_CessPer =0.0,dCess_Cess = 0.0;
		
            double dVat     =0;  
			String SCommodity       = "", SBlend = "", SInvoiceHead = "", SInvoiceNoHead = "";
			String SInvoiceDate ="",STruckNo="",SPermitNo = "";
			double dPackCharge = 0.0,dLoadingCharge = 0.0;
			double dRoundOff = 0.0 , dNetValue = 0.0 ;

			java.util.HashMap theMap = new java.util.HashMap();
			
			for(int i=0;i<AList.size();i++)
			{
				theMap = (java.util.HashMap) AList.get(i);

				String SInvNo1       = common.parseNull((String)theMap.get("INVNO"));
				if(SInvNo1.equals(SInvNo))
				{

				//if(i==0)
				//{
					String SGSTNo       = common.parseNull((String)theMap.get("GSTINID"));
					setPdfHead("INVOICE",SGSTNo);

					String SInvoiceNo   = SInvNo;
					String SPartyName   = common.parseNull((String)theMap.get("PARTYNAME"));
                    String SInvAdd1     = common.parseNull((String)theMap.get("ADDRESS1"));
                    String SInvAdd2     = common.parseNull((String)theMap.get("ADDRESS2"));
                    String SInvAdd3     = common.parseNull((String)theMap.get("ADDRESS3"));
                    String STINNo       = common.parseNull((String)theMap.get("TNGSTNO"));
                    SInvoiceDate 		= SInvDate;

                    String STarif       = "";
                    String SInsNo       = "";
                    String SDespAdd1    = common.parseNull((String)theMap.get("ADDRESS1")).trim();
                    String SDespAdd2    = common.parseNull((String)theMap.get("ADDRESS2")).trim();
                    String SDespAdd3    = common.parseNull((String)theMap.get("ADDRESS3")).trim();

					STruckNo     = common.parseNull((String)theMap.get("VEHICLENO")).trim();
//					SPermitNo    = common.parseNull((String)theMap.get("PERMITNO")).trim();

					SPermitNo    = "";
					
                    String SCSTNo       = common.parseNull((String)theMap.get("CSTNO"));
                    String SStateName   = common.parseNull((String)theMap.get("STATENAME"));
                    String SStateCode   = common.parseNull((String)theMap.get("GSTSTATECODE"));
                   // String SPlace       = common.parseNull((String)theMap.get("PLACE"));
					String SPlace       = "";
                    String SHsnCode   = common.parseNull((String)theMap.get("HSNCODE"));
                    String SDCNo   = common.parseNull((String)theMap.get("DCNO"));
                    String SDCDate   = common.parseDate((String)theMap.get("DCDATE"));

					setPartyInvDetails(SPartyName,SInvAdd1,SInvAdd2,SInvAdd3,SInvoiceNo,SInvoiceDate,SDespAdd1,SDespAdd2,SDespAdd3,"Invoice No.",SGSTNo,SStateName,SStateCode,SPlace,SHsnCode,SDCNo,SDCDate);
				//}
			
				String SCGST         = common.parseNull(common.getRound(common.toDouble((String)theMap.get("CGST")),2));
				String SCGSTPer      = common.parseNull(common.getRound(common.toDouble((String)theMap.get("CGSTVAL")),2));
				String SSGST         = common.parseNull(common.getRound(common.toDouble((String)theMap.get("SGST")),2));
				String SSGSTPer      = common.parseNull(common.getRound(common.toDouble((String)theMap.get("SGSTVAL")),2));
				String SIGST         = common.parseNull(common.getRound(common.toDouble((String)theMap.get("IGST")),2));
				String SIGSTPer      = common.parseNull(common.getRound(common.toDouble((String)theMap.get("IGSTVAL")),2));
				String SCess         = common.parseNull(common.getRound(common.toDouble((String)theMap.get("CESS")),2));
				String SCessPer      = common.parseNull(common.getRound(common.toDouble((String)theMap.get("CESSVAL")),2));	
				       SCess1        = common.parseNull(common.getRound(common.toDouble((String)theMap.get("CESS1")),2));
				       SCessPer1     = common.parseNull(common.getRound(common.toDouble((String)theMap.get("CESSPER1")),2));	
				String SHSN          = common.parseNull((String)theMap.get("HSNCODE"));
			  //STruckNo             = common.parseNull(common.getRound(common.toDouble((String)theMap.get("TRUCKNO")),2)).trim();
				SInvoiceDate         = SInvDate;

				String SOrganic     = "";
				String SDesc		= common.parseNull((String)theMap.get("ITEM_NAME"));	
				String SConfirmRate = String.valueOf(common.toDouble((String)theMap.get("RATE")));
				String SNoOfBags    = "1";//common.parseNull((String)theMap.get("NOOFBALES"));
				String SDespWeight  = String.valueOf(common.toDouble((String)theMap.get("QTY")));
				String SAssValue    = "";
				String STotalValue  = common.getRound((common.toDouble((String)theMap.get("QTY")) * common.toDouble((String)theMap.get("RATE"))),2);

				dTotAmt  			+= common.toDouble(STotalValue);
				dTotCess 			+= common.toDouble((String)theMap.get("CESSVAL"));
				dTotCgst 			+= common.toDouble((String)theMap.get("CGSTVAL"));
				dTotSgst 			+= common.toDouble((String)theMap.get("SGSTVAL"));
				dTotIgst 			+= common.toDouble((String)theMap.get("IGSTVAL"));
				
				dTotalQty 			= dTotalQty + common.toDouble((String)theMap.get("QTY"));	
				dTotalValue 		= dTotalValue + (common.toDouble((String)theMap.get("QTY")) * common.toDouble((String)theMap.get("RATE"))) ;
				dTotalCgstValue 	= dTotalCgstValue + common.toDouble(common.getRound(common.toDouble((String)theMap.get("CGSTVAL")),2)) ;
				dTotalSgstValue 	= dTotalSgstValue + common.toDouble(common.getRound(common.toDouble((String)theMap.get("SGSTVAL")),2)) ;
				dTotalIgstValue 	= dTotalIgstValue + common.toDouble(common.getRound(common.toDouble((String)theMap.get("IGSTVAL")),2)) ;
				dTotalCessValue 	= dTotalCessValue + common.toDouble(common.getRound(common.toDouble((String)theMap.get("CESSVAL")),2)) ;

				dCessCGSTPer  		= common.toDouble((String)theMap.get("CESSCGSTPER"));
				dCessCGST     		= common.toDouble((String)theMap.get("CESSCGST"));					
				dCessSGSTPer  		= common.toDouble((String)theMap.get("CESSSGSTPER"));
				dCessSGST     		= common.toDouble((String)theMap.get("CESSSGST"));		
				dCessIGSTPer  		= common.toDouble((String)theMap.get("CESSIGSTPER"));
				dCessIGST     		= common.toDouble((String)theMap.get("CESSIGST"));	
				dCess_CessPer		= common.toDouble((String)theMap.get("CESS_CESSPER"));
				dCess_Cess    		= common.toDouble((String)theMap.get("CESS_CESS"));	
				dCessTotal    		= (dCessCGST + dCessSGST + dCessIGST + dCess_Cess + common.toDouble(SCess1));

				double dTaxableTotal = common.toDouble(STotalValue) + common.toDouble(common.getRound(common.toDouble((String)theMap.get("CESS")),2)) + common.toDouble(common.getRound(common.toDouble((String)theMap.get("CGST")),2)) +common.toDouble(common.getRound(common.toDouble((String)theMap.get("SGST")),2))+common.toDouble(common.getRound(common.toDouble((String)theMap.get("IGST")),2));

				setGoodsDescription(SOrganic,SDesc,SConfirmRate,SNoOfBags,SDespWeight,SAssValue,STotalValue,"",i,SCGST,SSGST,SIGST,SCess,SCGSTPer,SSGSTPer,SIGSTPer,SCessPer,SHSN,common.getRound(dTaxableTotal, 2));
							
        	 
            
			double dTaxableTotal1= dTotAmt + dTotCess + dTotCgst +dTotSgst+dTotIgst;
			String STaxableTotal= common.getRound(dTaxableTotal1, 2);

			double dGTotal         = dTotAmt + dTotCess + dTotCgst +dTotSgst+dTotIgst;
		    String SGrandTotal  = common.getRound(dGTotal, 2);

			//dRoundOff = common.toDouble(common.getRound(dGTotal,0));

			double dDiff = dRoundOff-dGTotal;

			setTaxDetails(String.valueOf(dTotAmt), "", common.getRound(dTotCess, 2), "", String.valueOf(dTotCgst), "", String.valueOf(dTotSgst), "", common.getRound(dTotIgst,2), "", STaxableTotal, "", "", "","", SGrandTotal,common.getRound(dGTotal,2),common.getRound(dRoundOff,2),common.getRound(dNetValue,2),
dPackCharge,dLoadingCharge,dPackingCGSTPer,dPackingCGST,dPackingSGSTPer,dPackingSGST,dPackingIGSTPer,dPackingIGST,dPackingCessPer,dPackingCess
,dLoadingCGSTPer,dLoadingCGST,dLoadingSGSTPer,dLoadingSGST,dLoadingIGSTPer,dLoadingIGST,dLoadingCessPer,dLoadingCess
,dPackingTotal,dLoadingTotal,dTotalQty,dTotalValue,dTotalCgstValue,dTotalSgstValue,dTotalIgstValue,dTotalCessValue,dDiff,SCess1,SCessPer1
,dCessCGSTPer,dCessCGST,dCessSGSTPer,dCessSGST,dCessIGSTPer,dCessIGST,dCess_CessPer,dCess_Cess,dCessTotal);
		//	setPODetails("", "");
			String SPreTime     = SInvoiceDate+" "+common.parseNull((String)theMap.get("INVOICETIME"))+" Hours";
            String SRemTime     = SInvoiceDate+" "+common.parseNull((String)theMap.get("INVOICETIME"))+" Hours";
			setBagAndTimeDetails("", SPreTime, SRemTime, STruckNo,SPermitNo);
			setTermsAndFinalPart();
			document . add(table);
			//closePdf();
            //drawFoot1(dQty,dTotAmt,dVatAmt,dVat);
            dQty         =0;
            dTotAmt      =0;
			dVatAmt      =0;
            dVat         =0;
			}
			}
		} 
		catch(Exception ex) 
		{
			ex.printStackTrace();
		}
	}
	
	private void setPdfHead(String SInvoiceHead,String SGSTNo) 
	{
		try 
		{
			float fHeight   = 0;
            
            Paragraph P1    = new Paragraph("", mediumbold);
            P1.add("AMARJOTHI SPINNING MILLS LIMITED\n");
            P1.add("Gobi Main Road,\n");
            P1.add("Puthusuripalayam, NAMBIYUR - 638 458,\n");
            P1.add("Gobi(TK), Erode (Dt.), ");
			P1.add("Tamilnadu (33), India.\n\n");

            P1.add("Phone : 04285 - 267301, ");
            P1.add("999442387564,9489204503\n\n");
            P1.add("E-mail : mill@amarjothi.net\n");
            P1.add("GST IN. : 33AAFCA7082C1Z0 \n\n");

			drawPdfParagraph(P1, 0, 4, "L", "ALL", fHeight);

            P1  =   new Paragraph("", mediumnormal);
            P1.add("Regd. Office : \n\"AMARJOTHI HOUSE\"\n");
            P1.add("157, Kumaran Road,\n Tirupur - 641 601\n\n");
            P1.add("Phone : 91-0421-4311622,\n");
			P1.add("4311600\n\n");
            P1.add("E-mail : info@amarjothi.net\n");
//          P1.add("Email : sales@amarjothi.net\ninfo@amarjothi.net\nexport@amarjothi.net\n");

            drawPdfParagraph(P1, 0, 4, "L", "ALL", fHeight);

            P1  =   new Paragraph("", mediumnormal);
            P1.add("C.Ex.RC.No. : AAFCA 7082C XM 001\n");
            P1.add("ECC No. : AAFCA 7082C XM 001\n");
            P1.add("PLA No. : 59 / 93\n\n");
            P1.add("Range : 47, KVP Plaza, \nCutcherry Road, Gobi-638 452\n\n");
            P1.add("Division : Division - II, \n81-Sathy Road, Erode-638 004\n\n");
            P1.add("Commissionerate :Anai Road, Salem\n");

            drawPdfParagraph(P1, 0, 4, "L", "ALL", fHeight);

            drawPdfText("\n\n\n\n"+SInvoiceHead+"\n\n\n", 0, 2, "C", bigbold, "ALL", fHeight);

	 //     drawPdfText("For removal of Excisable goods from Factory or warehouse on payment of Duty (Rule 11 of C.E. Rules, 2002)", 0, 4, "C", mediumnormal, "ALL", fHeight);

    	}
		catch(Exception ex) 
		{
			ex.printStackTrace();
        }
    }

	private void setPartyInvDetails(String SPartyName,String SInvAdd1,String SInvAdd2,String SInvAdd3,String SInvoiceNo,String SInvoiceDate,String SDespAdd1,
									String SDespAdd2,String SDespAdd3,String SInvoiceNoHead,String SGSTNo,String SStateName,String SStateCode,String SPlace,String SHsnCode,String SDCNo,String SDCDate)
	{
    	try 
		{
            float fHeight   = 0;
			
			String SYear = common.getCurrentDate();            

			SYear =  SYear.substring(0,4);	
            
            drawPdfText("Consignee : ", 0, 4, "L", mediumnormal, "LRT", fHeight);
            drawPdfText(SInvoiceNoHead+" : ", 0, 4, "L", mediumnormal, "LRT", fHeight);
			drawPdfText("Invoice Date : ", 0, 6, "L", mediumnormal, "LRT", fHeight);
//			drawPdfText("HSN CODE : ", 0, 3, "L", mediumnormal, "LRT", fHeight);
            
            drawPdfText(SPartyName, 0, 4, "L", mediumbold, "LR", fHeight);
            drawPdfText(SInvoiceNo, 0, 4, "L", mediumbigbold, "LRB", fHeight); //"["+SYear+"] - "+
            drawPdfText(SInvoiceDate, 0, 6, "L", mediumbold, "LRB", fHeight);
//			drawPdfText(SHsnCode, 0, 2, "L", mediumbigbold, "LRB", fHeight); 
            
            drawPdfText(SInvAdd1, 0, 4, "L", mediumbold, "LR", fHeight);
            drawPdfText("Party GSTIN  :", 0, 4, "L", mediumnormal, "LR", fHeight);
            drawPdfText("DC NO & DC Date  :", 0, 6, "L", mediumnormal, "LR", fHeight);
            
            drawPdfText(SInvAdd2, 0, 4, "L", mediumbold, "LR", fHeight);
			drawPdfText(SGSTNo, 0, 4, "L", mediumbold, "LRB", fHeight);
			drawPdfText(SDCNo+" & "+SDCDate, 0, 6, "L", mediumbold, "LRB", fHeight);
            
            drawPdfText(SInvAdd3, 0, 4, "L", mediumbold, "LR", fHeight);
            drawPdfText("", 0, 10, "L", mediumnormal, "LR", fHeight);

			drawPdfText("", 0, 4, "L", mediumbold, "LR", fHeight);//SPlace
            drawPdfText("", 0, 10, "L", mediumnormal, "LR", fHeight);

			drawPdfText(SStateName, 0, 3, "L", mediumbold, "LRB", fHeight);
			drawPdfText(SStateCode, 0, 1, "L", mediumbold, "All", fHeight);
			//drawPdfText("", 0, 1, "L", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 10, "L", mediumnormal, "LRB", fHeight);
                     
           /* drawPdfText("Delivery At : ", 0, 16, "L", mediumnormal, "LR", fHeight);
                  
            drawPdfText(SDespAdd1, 0, 16, "L", mediumbold, "LR", fHeight);
                   
            drawPdfText(SDespAdd2, 0, 16, "L", mediumbold, "LR", fHeight);
           
            drawPdfText(SDespAdd3, 0, 16, "L", mediumbold, "LR", fHeight);
		    drawPdfText("", 0, 16, "L", mediumbold, "LR", fHeight);//SPlace

		    drawPdfText(SStateName, 0, 4, "L", mediumbold, "LRB", fHeight);
			drawPdfText(SStateCode, 0, 1, "L", mediumbold, "All", fHeight);
			drawPdfText("", 0, 11, "L", mediumbold, "LRB", fHeight);*/

		//	drawPdfText(SStateCode, 0, 16, "L", mediumbold, "LRB", fHeight);

		}
		catch(Exception ex) 
		{
            ex.printStackTrace();
        }
	}
	private void setGoodsDescription(String SOrganic, String SDesc, String SConfirmRate, String SNoOfBales,
            String SDespWeight, String SAssValue, String STotalValue, String SDepoAdvNo,int i,String SCGST,String SSGST,String SIGST,String SCess,String SCGSTPer,String SSGSTPer,String SIGSTPer,String SCessPer,String SHSN,String STotal) 
	{
		try 
		{
			float fHeight   = 0;
            
           // if(i==0){
			
			drawPdfText("Sl. No.", 2, 0, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("DESCRIPTION", 2, 0, "C", mediumnormal, "ALL", fHeight);
			drawPdfText("HSN CODE", 2, 0, "C", mediumnormal, "ALL", fHeight);		
            drawPdfText("Qty in Kgs.", 2, 0, "C", mediumnormal, "ALL", fHeight);
			drawPdfText("Rate\n Per Kg", 2, 0, "C", mediumnormal, "ALL", fHeight);
			drawPdfText("TOT ASSES VALUE", 2, 0, "C", mediumnormal, "ALL", fHeight);		
			drawPdfText("CGST", 0, 2, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("SGST", 0, 2, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("IGST", 0, 2, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("Cess", 0, 2, "C", mediumnormal, "ALL", fHeight);
			drawPdfText("%", 0, 0, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("Val", 0, 0, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("%", 0, 0, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("Val", 0, 0, "C", mediumnormal, "ALL", fHeight);
			drawPdfText("%", 0, 0, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("Val", 0, 0, "C", mediumnormal, "ALL", fHeight);
			drawPdfText("%", 0, 0, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("Val", 0, 0, "C", mediumnormal, "ALL", fHeight);


			//}

                        
            drawPdfText(String.valueOf(i+1)+".", 0, 0, "C", mediumbold, "LR", fHeight);
            drawPdfText(SDesc, 0, 0, "L", mediumbold, "LR", fHeight);
            drawPdfText(SHSN, 0, 0, "C", mediumbold, "LR", fHeight);
            drawPdfText(SDespWeight, 0, 0, "R", mediumbold, "LR", fHeight);
         	drawPdfText(SConfirmRate, 0, 0, "R", mediumbold, "LR", fHeight);
         	drawPdfText(STotalValue, 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(SCGST, 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(SCGSTPer, 0, 0, "R", mediumbold, "LR", fHeight);
            drawPdfText(SSGST, 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(SSGSTPer, 0, 0, "R", mediumbold, "LR", fHeight);
            drawPdfText(SIGST, 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(SIGSTPer, 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(SCess, 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(SCessPer, 0, 0, "R", mediumbold, "LR", fHeight);            
			
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);    
			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
		    drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
	private void setTaxDetails(String STotalValue, String SCessPer, String sCess,
            String SCGstPer, String SCGstValue, String SsGstPer, String SsGstValue,
            String SIGstPer, String SIGstValue, String SOthers,
            String STaxableTotal, String STaxPer, String STaxValue, String SFreightValue,
            String SRoundOff1, String SGrandTotal,String SGTotal,String SRoundOff,String SNetValue,double dPackCharge,double dLoading
			,double dPackingCGSTPer,double dPackingCGST,double dPackingSGSTPer,double dPackingSGST,double dPackingIGSTPer,double dPackingIGST
			,double dPackingCessPer,double dPackingCess,double dLoadingCGSTPer,double dLoadingCGST,double dLoadingSGSTPer,double dLoadingSGST
			,double dLoadingIGSTPer,double dLoadingIGST,double dLoadingCessPer,double dLoadingCess,double dPackingTotal,double dLoadingTotal
			,double dTotalQty,double dTotalValue,double dTotalCgstValue,double dTotalSgstValue,double dTotalIgstValue,double dTotalCessValue,double dDiff
			,String SCess1,String SCessPer1
			,double dCessCGSTPer,double dCessCGST,double dCessSGSTPer,double dCessSGST,double dCessIGSTPer,double dCessIGST,double dCess_CessPer,double dCess_Cess,double dCessTotal) {
        try {
            float fHeight   = 0;
            

		


			double dTotal = (dTotalValue + dPackCharge+dLoading) + common.toDouble(SCess1) +
							(dTotalCgstValue + (dLoadingCGST) + (dPackingCGST) + dCessCGST) +
							(dTotalSgstValue + (dLoadingSGST) + (dPackingSGST) + dCessSGST) +
							(dTotalIgstValue + (dLoadingIGST) + (dPackingIGST) + dCessIGST) +
							(dTotalCessValue + (dLoadingCess) + (dPackingCess) + dCess_Cess) ;
		
			drawPdfText("", 0, 0, "C", mediumbold, "LR", fHeight);
            drawPdfText("Total", 0, 2, "L", mediumbold, "LR", fHeight);
         //   drawPdfText("", 0, 0, "C", mediumbold, "LR", fHeight);
			drawPdfText(common.getRound(dTotalQty,2), 0, 0, "C", mediumbold, "LR", fHeight);
            drawPdfText("", 0, 0, "R", mediumbold, "LR", fHeight);
         	drawPdfText(common.getRound((dTotalValue+((dPackCharge)+(dLoading)+common.toDouble(SCess1))),2), 0, 0, "R", mediumbold, "LR", fHeight);
         	drawPdfText("", 0, 0, "R", mediumbold, "LR", fHeight);
 			drawPdfText(common.getRound((dTotalCgstValue+((dLoadingCGST) + (dPackingCGST) +(dCessCGST) )),2), 0, 0, "R", mediumbold, "LR", fHeight);
            drawPdfText("", 0, 0, "R", mediumbold, "LR", fHeight);
 			drawPdfText(common.getRound((dTotalSgstValue+((dLoadingSGST) + (dPackingSGST) + (dCessSGST) )),2), 0, 0, "R", mediumbold, "LR", fHeight);
            drawPdfText("", 0, 0, "R", mediumbold, "LR", fHeight);
 			drawPdfText(common.getRound((dTotalIgstValue+((dLoadingIGST) + (dPackingIGST) + (dCessIGST) )),2), 0, 0, "R", mediumbold, "LR", fHeight);
            drawPdfText("", 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(common.getRound((dTotalCessValue+dLoadingCess+ dPackingCess + dCess_Cess),2), 0, 0, "R", mediumbold, "LR", fHeight);
//            drawPdfText(common.getRound(dTotal,2), 0, 0, "R", mediumbold, "LR", fHeight);

			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);    
			drawPdfText("", 0, 2, "C", mediumbold, "LRB", fHeight);                                 
		    drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
           
            drawPdfText("", 0, 5, "L", mediumnormal, "L", fHeight);
            drawPdfText("Rounded Off", 0, 3, "L", mediumnormal, "NO", fHeight);
            drawPdfText("", 0, 2, "L", mediumbold, "NO", fHeight);
            drawPdfText(SRoundOff, 0, 4, "R", mediumbold, "LR", fHeight); // common.getRound(dDiff,2)
            
            drawPdfText("", 0, 5, "L", mediumnormal, "LB", fHeight);
            drawPdfText("GRAND TOTAL", 0, 3, "L", mediumnormal, "B", fHeight);
            drawPdfText("", 0, 2, "L", mediumbold, "B", fHeight);
            drawPdfText(SGrandTotal, 0, 4, "R", mediumbigbold, "ALL", fHeight);
            
            drawPdfText("Rupees : ", 0, 14, "L", mediumnormal, "LR", fHeight);
            drawPdfText(common.getRupee(SGrandTotal), 0, 14, "L", mediumbigbold, "LRB", fHeight);

			//drawPdfText("", 0, 16, "L", mediumnormal, "LR", fHeight);
            //drawPdfText("GST Payable On Reverse Charge  : No ", 0, 16, "L", mediumbigbold, "LRB", fHeight);

        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
	private void setPODetails(String SPOInfo, String SInsurance) {
        try {
            float fHeight   = 0;
            
            drawPdfText("PO Details : ", 0, 3, "L", mediumnormal, "LR", fHeight);
            drawPdfText("Insurance Details : ", 0, 4, "L", mediumnormal, "LR", fHeight);
            
            drawPdfText(SPOInfo, 0, 3, "L", mediumbold, "LRB", fHeight);
            drawPdfText(SInsurance, 0, 4, "L", mediumbold, "LRB", fHeight);
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
	private void setBagAndTimeDetails(String SBagDetails, String SPreTime, String SRemTime, String STruckNo,String SPermitNo) {
        try {
            float fHeight   = 0;
            
            drawPdfText("Preparation Time / Date : ", 0, 4, "L", mediumnormal, "LR", fHeight);
            drawPdfText("Removal Time / Date : ", 0, 6, "L", mediumnormal, "LR", fHeight);
//            drawPdfText("Permit No : ", 0, 4, "L", mediumnormal, "LR", fHeight);		
            drawPdfText("Mode Of Transport : ", 0, 4, "L", mediumnormal, "LR", fHeight);
            
            drawPdfText(SPreTime, 0, 4, "L", mediumbold, "LRB", fHeight);
            drawPdfText(SRemTime, 0, 6, "L", mediumbold, "LRB", fHeight);
//            drawPdfText(SPermitNo, 0, 4, "L", mediumbold, "LRB", fHeight);
            drawPdfText(STruckNo, 0, 4, "L", mediumbold, "LRB", fHeight);
            

        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
	private void setTermsAndFinalPart() {
        try {
            float fHeight   = 0;
            
            drawPdfText("TERMS & CONDITIONS", 0, 14, "L", mediumboldunderline, "LR", fHeight);
            
            drawPdfText("1. Cash payment shall be made only in Head Office. Branches are empowered to receive only crossed Cheques / Drafts, Persons are not appointed to collect Cash.\n", 0, 14, "L", mediumnormal, "LR", fHeight);
            drawPdfText("2. All Transaction are subject to TIRUPUR juristiction only..\n", 0, 14, "L", mediumnormal, "LR", fHeight);
            
            drawPdfText("\n", 0, 4, "L", mediumnormal, "LR", fHeight);
            drawPdfText("Certified that the particulars given above are true and correct and the amount", 0, 10, "L", mediumnormal, "LRT", fHeight);
            
            drawPdfText("\n", 0, 4, "L", mediumnormal, "LR", fHeight);
            drawPdfText("indicated represents the price actually charged and that there is no flow of", 0, 10, "L", mediumnormal, "LR", fHeight);
            
            drawPdfText("E. & O.E..\n", 0, 4, "L", mediumnormal, "LRB", fHeight);
            drawPdfText("additional consideration directly or indirectly from the buyer.", 0, 10, "L", mediumnormal, "LRB", fHeight);
            
            
            drawPdfText("Received Goods", 0, 4, "L", mediumbold, "LR", fHeight);
            drawPdfText("Prepared By", 0, 2, "C", mediumnormal, "LR", fHeight);
            drawPdfText("Checked By", 0, 2, "C", mediumnormal, "LR", fHeight);
            drawPdfText("For AMARJOTHI SPINNING MILLS LTD.", 0, 6, "C", mediumbold, "LR", fHeight);
            
            drawPdfText("\n\n\n\n\n", 0, 4, "L", mediumnormal, "LR", fHeight);
            drawPdfText("", 0, 2, "C", mediumnormal, "LR", fHeight);
            drawPdfText("", 0, 2, "C", mediumnormal, "LR", fHeight);
            drawPdfText("", 0, 6, "C", mediumbold, "LR", fHeight);
            
            drawPdfText("Party's Signature with seal.", 0, 4, "R", mediumnormal, "LRB", fHeight);
            drawPdfText("", 0, 2, "C", mediumnormal, "LRB", fHeight);
            drawPdfText("", 0, 2, "C", mediumnormal, "LRB", fHeight);
            drawPdfText("Authorised Signatory.", 0, 6, "C", mediumnormal, "LRB", fHeight);
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
	private void drawPdfText(String SText, int iRowSpan, int iColSpan, String SAlign,Font theFont, String SBorderType, float fHeight) {
        try {
            PdfPCell c1 = new PdfPCell(new Phrase(SText, theFont));
            
            if(SAlign.equals("L")) {
                c1      . setHorizontalAlignment(Element.ALIGN_LEFT);
            }
            if(SAlign.equals("C")) {
                c1      . setHorizontalAlignment(Element.ALIGN_CENTER);
            }
            if(SAlign.equals("R")) {
                c1      . setHorizontalAlignment(Element.ALIGN_RIGHT);
            }

            if(iRowSpan > 0) {
                c1      . setRowspan(iRowSpan);
            }
            
            if(iColSpan > 0) {
                c1      . setColspan(iColSpan);
            }

            if(SBorderType.equals("ALL")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
            }
            if(SBorderType.equals("LR") || SBorderType.equals("RL")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            }
            if(SBorderType.equals("LRT") || SBorderType.equals("RLT")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
            }
            if(SBorderType.equals("LRB") || SBorderType.equals("RLB")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            }
            if(SBorderType.equals("LB")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
            }
            if(SBorderType.equals("L")) {
                c1.setBorder(Rectangle.LEFT);
            }
            if(SBorderType.equals("B")) {
                c1.setBorder(Rectangle.BOTTOM);
            }
            if(SBorderType.equals("NO")) {
                c1.setBorder(Rectangle.NO_BORDER);
            }

            if(fHeight == 0) {
                c1      . hasMinimumHeight();
            }else {
                c1      . setFixedHeight(12f);
            }
            
            table       . addCell(c1);
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void drawPdfParagraph(Paragraph thePara, int iRowSpan, int iColSpan, String SAlign, String SBorderType, float fHeight) {
        try {
            PdfPCell c1 = new PdfPCell(thePara);
            
            if(SAlign.equals("L")) {
                c1      . setHorizontalAlignment(Element.ALIGN_LEFT);
            }
            if(SAlign.equals("C")) {
                c1      . setHorizontalAlignment(Element.ALIGN_CENTER);
            }
            if(SAlign.equals("R")) {
                c1      . setHorizontalAlignment(Element.ALIGN_RIGHT);
            }

            if(iRowSpan > 0) {
                c1      . setRowspan(iRowSpan);
            }
            
            if(iColSpan > 0) {
                c1      . setColspan(iColSpan);
            }

            if(SBorderType.equals("ALL")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
            }
            if(SBorderType.equals("LR") || SBorderType.equals("RL")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            }
            if(SBorderType.equals("LRT") || SBorderType.equals("RLT")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
            }
            if(SBorderType.equals("LRB") || SBorderType.equals("RLB")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            }
            if(SBorderType.equals("LB")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
            }
            if(SBorderType.equals("L")) {
                c1.setBorder(Rectangle.LEFT);
            }
            if(SBorderType.equals("B")) {
                c1.setBorder(Rectangle.BOTTOM);
            }
            if(SBorderType.equals("NO")) {
                c1.setBorder(Rectangle.NO_BORDER);
            }

            if(fHeight == 0) {
                c1      . hasMinimumHeight();
            }else {
                c1      . setFixedHeight(12f);
            }
            
            table       . addCell(c1);
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
	private void closePdf() {
        try {
            document . close();
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }   
}
