package Sales;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

//import com.amarjothi.oracledb.Common;
import java.util.*;
import java.io.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

public class SalesInvoicePdfPrint extends JInternalFrame
{
	JLayeredPane            	Layer;	
	private Common              common;
	JPanel                   	TopPanel,MiddlePanel,BottomPanel;
	JButton      				BApply,BPrint,BExit;
	DateField     				TFromDate,TToDate;
	private       				JTable  theTable;
    private       				SalesInvoicePdfPrintModel    theModel;	
	String         				SFromDate,SToDate;
	ArrayList                   AList;
	
	private Document            document,document1;
	private PdfPTable           table;

	private int                 iWidth[]            = {20,50,38,25,30,35,35,20,35,20,35,20,35,20,35};
	private Font                bigbold             = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
	private Font                mediumbigbold       = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
	private Font                mediumbold          = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
	private Font                mediumboldunderline = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD|Font.UNDERLINE);
	private Font                mediumnormal        = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);

	private String SFileWritePath="";

	int iWidth1[] = {15,30,20,20,20,30};
    private Font                smallbold           = FontFactory.getFont("TIMES_ROMAN",7,Font.BOLD);
    private static Font smallnormal=FontFactory.getFont("TIMES_ROMAN",7,Font.NORMAL);

	String SCess1,SCessPer1;
	   
    public SalesInvoicePdfPrint(JLayeredPane Layer)
    {
		this.Layer      = Layer;
		common          = new Common();
       
		createComponents();
        setLayouts();
        addComponents();
		addListeners();
	}
	private void createComponents()
    {
        TopPanel      = new JPanel();
        MiddlePanel   = new JPanel();
        BottomPanel   = new JPanel();
                  
        TFromDate     = new DateField();
        TToDate       = new DateField();
            
        BExit         = new JButton("Exit");
        BApply        = new JButton("Apply");
		BPrint        = new JButton("Print");
                
        theModel      = new SalesInvoicePdfPrintModel();
        theTable      = new JTable(theModel);
                 
        for(int i=0;i<theModel.ColumnName.length;i++)
        {
           (theTable.getColumnModel()) . getColumn(i) . setPreferredWidth(theModel.iColumnWidth[i]);
        }
    }
    private void setLayouts()
    {
    
         TopPanel      . setLayout(new GridLayout(1,5));
         MiddlePanel   . setLayout(new BorderLayout());
         BottomPanel   . setLayout(new FlowLayout(FlowLayout.CENTER));
          
         TopPanel      . setBorder(new TitledBorder(" Filter "));
         MiddlePanel   . setBorder(new TitledBorder(" Details "));
         BottomPanel   . setBorder(new TitledBorder(" Controls "));
         
         this . setTitle("Sales Invocie Print Details");
         this . setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
         this . setSize(1100,600);
    }
    private void addComponents()
    {
		TopPanel . add(new JLabel(" FROM DATE "))  ;
        TopPanel . add(TFromDate);
        TopPanel . add(new JLabel(" TO DATE "))  ;
        TopPanel . add(TToDate);
        TopPanel . add(BApply);
        
        MiddlePanel.add(new JScrollPane(theTable));

		BottomPanel . add(BExit);
		BottomPanel . add(BPrint);

        this . add(TopPanel,BorderLayout.NORTH);
        this . add(MiddlePanel,BorderLayout.CENTER);
        this . add(BottomPanel,BorderLayout.SOUTH);
    }
    private void addListeners()
    {
        BApply . addActionListener(new ActList());
    	BExit  . addActionListener(new ActList());
		BPrint . addActionListener(new PrintList());
        
    }
	private class ActList implements ActionListener
    {
		public void actionPerformed(ActionEvent ae)   
		{
			SFromDate     = common.pureDate(TFromDate.toString());
			SToDate       = common.pureDate(TToDate.toString());
			if(ae.getSource()==BApply)
			{
				theModel.setNumRows(0);
				setTableDetails();
			}
		
			if(ae.getSource()==BExit)
			{
			   dispose();
			}
		}
    }
	public class PrintList implements ActionListener
    {
    	public void actionPerformed(ActionEvent ae)
        {
    		try
			{
				FileWriter FW = new FileWriter("d:\\Invoice.prn");
				    
          		initPdf();
                        
				for(int l=0;l<=3;l++)
				{
					String SHeading = "";
					if(l==0)
						SHeading = "ORIGINAL FOR BUYER"	;
					if(l==1)
						SHeading = "TRIPLICATE FOR ASSESSEE"	;
					if(l==2)
						SHeading = "DUPLICATE FOR TRANSPORTER"	;
					if(l==3)
						SHeading = "QUADRUPLICATE FOR HEADOFFICE COPY"	;
					
					//CreatePDF(SInvNo,SInvDate,SHeading);
					CreatePDF(SHeading);
				}
				document.close();
				JOptionPane . showMessageDialog(null,"Print Genrated in Ur D: Drive/Root ","Information",JOptionPane.INFORMATION_MESSAGE);
			}
            catch(Exception ex)
            {
            	System.out.println(ex);
            }

		}
	}
	private void setTableDetails()
    {
        theModel                           . setNumRows(0);
		SetVector();
		int iSlNo                          = 1;
          
		double dTotalAmt = 0;
			
		for(int i=0;i<AList.size();i++)
		{
			HashMap theMap = (HashMap)AList.get(i);
            
            ArrayList theList             = null;
            theList                       = new ArrayList();
            theList                       . clear();
            theList                       . add(String.valueOf(iSlNo++));
            theList                       . add((String)theMap.get("INVNO"));
            theList                       . add(common.parseDate((String)theMap.get("INVDATE")));
            theList                       . add(String.valueOf(theMap.get("DCNO")));
            theList                       . add(common.parseDate((String)theMap.get("DCDATE")));
            theList                       . add(String.valueOf(theMap.get("PARTYNAME")));
            theList                       . add(String.valueOf(theMap.get("HSNCODE")));
            theList                       . add(String.valueOf(theMap.get("QTY")));
            theList                       . add(String.valueOf(theMap.get("RATE")));
            theList                       . add(String.valueOf(theMap.get("BASIC")));
            theList                       . add(String.valueOf(theMap.get("CGST")));
            theList                       . add(String.valueOf(theMap.get("SGST")));
            theList                       . add(String.valueOf(theMap.get("IGST")));
            theList                       . add(String.valueOf(theMap.get("CESS")));
            theList                       . add(String.valueOf(theMap.get("CGSTVAL")));
            theList                       . add(String.valueOf(theMap.get("SGSTVAL")));
            theList                       . add(String.valueOf(theMap.get("IGSTVAL")));
            theList                       . add(common.parseNull(String.valueOf(theMap.get("OTHERS"))));
            theList                       . add(String.valueOf(theMap.get("NET")));
            theList                       . add(new java.lang.Boolean(false));
   
	    theModel                      . appendRow(new Vector(theList));
        }
        
      /* ArrayList theList1             = null;
        theList1                       = new ArrayList();
        theList1                       . clear();
        theList1                       . add("Total");
      
        theList1                       . add("");
        theList1                       . add(dTotalAmt);
        theList1                       . add(null);
                  
        theModel                      . appendRow(new Vector(theList1));*/
    }
    private void SetVector()
    {
       
        SFromDate     = common.pureDate(TFromDate.toString());
        SToDate       = common.pureDate(TToDate.toString());
        
		PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuffer sb = new StringBuffer();
        AList = new ArrayList();
        AList.clear();
        
        try
        {
			ORAConnection connect         =    ORAConnection.getORAConnection();
            Connection     theConnection   =    connect.getConnection();
			pst = theConnection.prepareStatement(getQS());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd = rst.getMetaData();
            while (rst.next()) 
			{
				HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) 
				{
					themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
				}
                AList.add(themap);
                
            }
System.out.println("AList"+AList.size());
			          
           rst.close();
           pst.close();
          
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
			System.out.println(e);
        }
    }
	 private String getQS()
    {
        
        String QS = " Select InvNO,InvDate,DCNo,DCDate,StoresInvoice.ITEMCODE,InvItems.HsnCode,Qty,RATE,Basic,CGST,SGST,IGST,Cess,CgstVal,SgstVal,IgstVal,CessVal "+
					" ,StoresInvoice.Others,Net,PartyName "+
					" ,PartyMaster.ADDRESS1,PartyMaster.ADDRESS2,PartyMaster.ADDRESS3,PartyMaster.TNGSTNo ,PartyMaster.GSTINID"+
					" ,State.STATENAME,State.GSTSTATECODE,StoresInvoice.VehicleNo,Item_Name,UOM.UomName,StoresInvoice.MILLCODE,StoresInvoice.ID "+
					" from StoresInvoice "+
					" Inner join PArtyMaster on PartyMaster.PARTYCODE = StoresInvoice.PARTYCODE "+
					" Inner join State on State.STATECODE = PartyMaster.STATECODE "+
					" Inner join InvItems on InvItems.Item_Code = StoresInvoice.ITEMCODE "+
					" Inner join UOM on UOM.UOMCODE = InvItems.UOMCODE "+
					" where InvDate>="+SFromDate+" and InvDate<="+SToDate+"  Order by ID";
        System.out.println("QS:"+QS);
        return QS;
    }
	
	private void initPdf() 
	{
    	try 
		{
			document  = new Document(PageSize.A4);
			String Newfile="Invoice.pdf";
			String OSName = System.getProperty("os.name");
            if(OSName.startsWith("Lin"))
			{
                SFileWritePath = "/root/"+Newfile;
            }
            else
			{
                SFileWritePath = "D:\\"+Newfile;
            }
            PdfWriter . getInstance(document, new FileOutputStream(SFileWritePath));
            document  . open();
		}
		catch(Exception ex) 
		{
			ex.printStackTrace();
        }
	}
	//private void CreatePDF(String SInvNo,String SInvDate,String SHeading)
	private void CreatePDF(String SHeading)  
	{
		try 
		{
	
			document.newPage();

		    Paragraph P1    = new Paragraph("", mediumbold);
			P1.add(SHeading);
			P1.setAlignment(Element.ALIGN_RIGHT);
		    P1.setSpacingAfter(10);
		    
		    document.add(P1);

			// set Pdf Table..
			table               = new PdfPTable(15);
			table               . setWidths(iWidth);
			table               . setWidthPercentage(100f);

            double dQty  =0;
            double dAmt  =0;

            int iCount =0;
            double dTotAmt  =0;
            double dVatAmt  =0,dTotCess=0,dTotCgst=0,dTotSgst=0,dTotIgst=0;
			
			double dCessTotal = 0.0 ;
			double dTotalRate = 0.0,dTotalValue=0,dTotalCgstValue=0,dTotalSgstValue=0,dTotalIgstValue=0,dTotalCessValue=0,dTotalQty = 0.0;
			double dCessCGSTPer = 0.0,dCessCGST = 0.0,dCessSGSTPer =0.0,dCessSGST =0.0,dCessIGSTPer =0.0,dCessIGST =0.0,dCess_CessPer =0.0,dCess_Cess = 0.0;
		
            double dVat     =0;  
			String SCommodity       = "", SBlend = "", SInvoiceHead = "", SInvoiceNoHead = "";
			String SInvoiceDate ="",STruckNo="",SPermitNo = "";
			
			double dRoundOff = 0.0 , dNetValue = 0.0 ;

			java.util.HashMap theMap  = new java.util.HashMap();
			java.util.HashMap theMap1 = new java.util.HashMap();
			java.util.HashMap theMap2 = new java.util.HashMap();

			String SNextDCNo = "",SPreviousDCNo = "";
			int iSlNo = 0;
System.out.println("theModel.getRowCount() : "+theModel.getRowCount());
			String SMillCode = "",SOthers ="";
			for( int i=0;i<theModel.getRowCount();i++) 
			{
				java.lang.Boolean bValue      = (java.lang.Boolean)theModel.getValueAt(i,19);

              	if(bValue.booleanValue()) 
				{
					theMap = (java.util.HashMap) AList.get(i);
					String SInvoiceNo1   = common.parseNull((String)theMap.get("INVNO"));
						 
					if(i==(theModel.getRowCount()-1))
					{
						SNextDCNo = "";				
					}
					else
					{
						theMap1 = (java.util.HashMap) AList.get(i+1);
						SNextDCNo = common.parseNull((String)theMap1.get("INVNO"));
					}
					if(i>0)
					{
						theMap2 = (java.util.HashMap) AList.get(i-1);
						SPreviousDCNo = common.parseNull((String)theMap2.get("INVNO"));
					}
					if(i>=0)
				  	{
						if(!SPreviousDCNo.equals(SInvoiceNo1) || i==0)
				  		{System.out.println("i Value :"+i);
												System.out.println("SPreviousDCNo :"+SPreviousDCNo);
									System.out.println("SInvoiceNo1 :"+SInvoiceNo1);

							String SGSTNo       = common.parseNull((String)theMap.get("GSTINID"));
							       SMillCode    = common.parseNull((String)theMap.get("MILLCODE"));

							setPdfHead("INVOICE",SGSTNo,SMillCode);

							String SInvoiceNo   = common.parseNull((String)theMap.get("INVNO"));
							String SPartyName   = common.parseNull((String)theMap.get("PARTYNAME"));
				            String SInvAdd1     = common.parseNull((String)theMap.get("ADDRESS1"));
				            String SInvAdd2     = common.parseNull((String)theMap.get("ADDRESS2"));
				            String SInvAdd3     = common.parseNull((String)theMap.get("ADDRESS3"));
				            String STINNo       = common.parseNull((String)theMap.get("TNGSTNO"));
				            SInvoiceDate 		= common.parseDate((String)theMap.get("INVDATE"));

				            String STarif       = "";
				            String SInsNo       = "";
				            String SDespAdd1    = common.parseNull((String)theMap.get("ADDRESS1")).trim();
				            String SDespAdd2    = common.parseNull((String)theMap.get("ADDRESS2")).trim();
				            String SDespAdd3    = common.parseNull((String)theMap.get("ADDRESS3")).trim();

							STruckNo     = common.parseNull((String)theMap.get("VEHICLENO")).trim();
							SPermitNo    = "";
					
				            String SCSTNo       = common.parseNull((String)theMap.get("CSTNO"));
				            String SStateName   = common.parseNull((String)theMap.get("STATENAME"));
				            String SStateCode   = common.parseNull((String)theMap.get("GSTSTATECODE"));
							String SPlace       = "";
				            String SHsnCode   = common.parseNull((String)theMap.get("HSNCODE"));
				            String SDCNo   = common.parseNull((String)theMap.get("DCNO"));
				            String SDCDate   = common.parseDate((String)theMap.get("DCDATE"));

							setPartyInvDetails(SPartyName,SInvAdd1,SInvAdd2,SInvAdd3,SInvoiceNo,SInvoiceDate,SDespAdd1,SDespAdd2,SDespAdd3,"Invoice No.",SGSTNo,SStateName,SStateCode,SPlace,SHsnCode,SDCNo,SDCDate);
						}
					}
			
				String SCGST         = common.parseNull(common.getRound(common.toDouble((String)theMap.get("CGST")),2));
				String SCGSTPer      = common.parseNull(common.getRound(common.toDouble((String)theMap.get("CGSTVAL")),2));
				String SSGST         = common.parseNull(common.getRound(common.toDouble((String)theMap.get("SGST")),2));
				String SSGSTPer      = common.parseNull(common.getRound(common.toDouble((String)theMap.get("SGSTVAL")),2));
				String SIGST         = common.parseNull(common.getRound(common.toDouble((String)theMap.get("IGST")),2));
				String SIGSTPer      = common.parseNull(common.getRound(common.toDouble((String)theMap.get("IGSTVAL")),2));
				String SCess         = common.parseNull(common.getRound(common.toDouble((String)theMap.get("CESS")),2));
				String SCessPer      = common.parseNull(common.getRound(common.toDouble((String)theMap.get("CESSVAL")),2));	
				      
				String SHSN          = common.parseNull((String)theMap.get("HSNCODE"));
				String SUom          = common.parseNull((String)theMap.get("UOMNAME"));
			    STruckNo             = common.parseNull((String)theMap.get("VEHICLENO"));
				SInvoiceDate         = common.parseDate((String)theMap.get("INVDATE"));
				String SInvoiceNo    = common.parseNull((String)theMap.get("INVNO"));
				SOthers              = common.parseNull((String)theMap.get("OTHERS"));

				String SDesc		= common.parseNull((String)theMap.get("ITEM_NAME"));	
				String SConfirmRate = String.valueOf(common.toDouble((String)theMap.get("RATE")));
				String SDespWeight  = String.valueOf(common.toDouble((String)theMap.get("QTY")));
				String STotalValue  = common.getRound((common.toDouble((String)theMap.get("QTY")) * common.toDouble((String)theMap.get("RATE"))),2);

				dTotAmt  			+= common.toDouble(STotalValue);
				dTotCess 			+= common.toDouble((String)theMap.get("CESSVAL"));
				dTotCgst 			+= common.toDouble((String)theMap.get("CGSTVAL"));
				dTotSgst 			+= common.toDouble((String)theMap.get("SGSTVAL"));
				dTotIgst 			+= common.toDouble((String)theMap.get("IGSTVAL"));
				
				dTotalQty 			= dTotalQty + common.toDouble((String)theMap.get("QTY"));	
				dTotalValue 		= dTotalValue + (common.toDouble((String)theMap.get("QTY")) * common.toDouble((String)theMap.get("RATE"))) ;
				dTotalCgstValue 	= dTotalCgstValue + common.toDouble(common.getRound(common.toDouble((String)theMap.get("CGSTVAL")),2)) ;
				dTotalSgstValue 	= dTotalSgstValue + common.toDouble(common.getRound(common.toDouble((String)theMap.get("SGSTVAL")),2)) ;
				dTotalIgstValue 	= dTotalIgstValue + common.toDouble(common.getRound(common.toDouble((String)theMap.get("IGSTVAL")),2)) ;
				dTotalCessValue 	= dTotalCessValue + common.toDouble(common.getRound(common.toDouble((String)theMap.get("CESSVAL")),2)) ;

				
				
				iSlNo = iSlNo + 1;
				setGoodsDescription(SDesc,SConfirmRate,SDespWeight,STotalValue,"",i,SCGST,SSGST,SIGST,SCess,SCGSTPer,SSGSTPer,SIGSTPer,SCessPer,SHSN,iSlNo, SPreviousDCNo,SInvoiceNo,SUom,SOthers);//,common.getRound(dTaxableTotal, 2)
				}
			}						        	             


			double dGTotal         = dTotAmt + dTotCess + dTotCgst +dTotSgst+dTotIgst;
					dGTotal = dGTotal + common.toDouble(SOthers);
		    String SGrandTotal  = common.getRound(dGTotal, 2);
			

			dRoundOff = common.toDouble(common.getRound(dGTotal,0));

			double dDiff = dRoundOff-dGTotal;

			setTaxDetails(String.valueOf(dTotAmt), "", common.getRound(dTotCess, 2), "", String.valueOf(dTotCgst), "", String.valueOf(dTotSgst), "", common.getRound(dTotIgst,2), SOthers,  "", "", "","", SGrandTotal,common.getRound(dGTotal,2),common.getRound(dRoundOff,2),common.getRound(dNetValue,2),
dTotalQty,dTotalValue,dTotalCgstValue,dTotalSgstValue,dTotalIgstValue,dTotalCessValue,dDiff);
		//	setPODetails("", "");
			String SPreTime     = SInvoiceDate+" "+common.parseNull((String)theMap.get("INVOICETIME"))+" Hours";
            String SRemTime     = SInvoiceDate+" "+common.parseNull((String)theMap.get("INVOICETIME"))+" Hours";
			setBagAndTimeDetails("", SPreTime, SRemTime, STruckNo,SPermitNo);
			setTermsAndFinalPart(SMillCode);
			document . add(table);
			//closePdf();
            //drawFoot1(dQty,dTotAmt,dVatAmt,dVat);
            dQty         =0;
            dTotAmt      =0;
			dVatAmt      =0;
            dVat         =0;
		//	}
		//	}
		} 
		catch(Exception ex) 
		{
			ex.printStackTrace();
		}
	}
	
	private void setPdfHead(String SInvoiceHead,String SGSTNo,String SMillCode) 
	{
		try 
		{
			float fHeight   = 0;
			Paragraph P1    = new Paragraph("", mediumbold);
			if(SMillCode.equals("0"))
            {
		        
		        P1.add("AMARJOTHI SPINNING MILLS LIMITED\n");
		        P1.add("Gobi Main Road,\n");
		        P1.add("Puthusuripalayam, NAMBIYUR - 638 458,\n");
		        P1.add("Gobi(TK), Erode (Dt.), ");
				P1.add("Tamilnadu (33), India.\n\n");

		        P1.add("Phone : 04285 - 267301, ");
		        P1.add("999442387564,9489204503\n\n");
		        P1.add("E-mail : mill@amarjothi.net\n");
		        P1.add("GST IN. : 33AAFCA7082C1Z0 \n\n");
			}
			else
			{
				//Paragraph P1    = new Paragraph("", mediumbold);
                P1.add("AMARJOTHI SPINNING MILLS LTD (DYEING DIVISION)\n");
                P1.add("PLOT NO.E7 TO E9,G11 TO G13,\n");
                P1.add("SIPCOT INDUSTRIAL GROWTH CENTER,\n");
                P1.add("PERUNDURAI 638052.\n");
				P1.add("Tamilnadu (33), India.\n\n");
                
                P1.add("Phone : 91 4294 234093\n");
			    P1.add("E-mail : mill@amarjothi.net\n");

				P1.add("GST IN. : 33AAFCA7082C1Z0 ");
			}
			drawPdfParagraph(P1, 0, 5, "L", "ALL", fHeight);

            P1  =   new Paragraph("", mediumnormal);
            P1.add("Regd. Office : \n\"AMARJOTHI HOUSE\"\n");
            P1.add("157, Kumaran Road,\n Tirupur - 641 601\n\n");
            P1.add("Phone : 91-0421-4311622,\n");
			P1.add("4311600\n\n");
            P1.add("E-mail : info@amarjothi.net\n");
//          P1.add("Email : sales@amarjothi.net\ninfo@amarjothi.net\nexport@amarjothi.net\n");

            drawPdfParagraph(P1, 0, 4, "L", "ALL", fHeight);

            P1  =   new Paragraph("", mediumnormal);
            P1.add("C.Ex.RC.No. : AAFCA 7082C XM 001\n");
            P1.add("ECC No. : AAFCA 7082C XM 001\n");
            P1.add("PLA No. : 59 / 93\n\n");
            P1.add("Range : 47, KVP Plaza, \nCutcherry Road, Gobi-638 452\n\n");
            P1.add("Division : Division - II, \n81-Sathy Road, Erode-638 004\n\n");
            P1.add("Commissionerate :Anai Road, Salem\n");

            drawPdfParagraph(P1, 0, 4, "L", "ALL", fHeight);

            drawPdfText("\n\n\n\n"+SInvoiceHead+"\n\n\n", 0, 2, "C", bigbold, "ALL", fHeight);

	 //     drawPdfText("For removal of Excisable goods from Factory or warehouse on payment of Duty (Rule 11 of C.E. Rules, 2002)", 0, 4, "C", mediumnormal, "ALL", fHeight);

    	}
		catch(Exception ex) 
		{
			ex.printStackTrace();
        }
    }

	private void setPartyInvDetails(String SPartyName,String SInvAdd1,String SInvAdd2,String SInvAdd3,String SInvoiceNo,String SInvoiceDate,String SDespAdd1,
									String SDespAdd2,String SDespAdd3,String SInvoiceNoHead,String SGSTNo,String SStateName,String SStateCode,String SPlace,String SHsnCode,String SDCNo,String SDCDate)
	{
    	try 
		{
            float fHeight   = 0;
			
			String SYear = common.getCurrentDate();            

			SYear =  SYear.substring(0,4);	
            
            drawPdfText("Consignee : ", 0, 5, "L", mediumnormal, "LRT", fHeight);
            drawPdfText(SInvoiceNoHead+" : ", 0, 4, "L", mediumnormal, "LRT", fHeight);
			drawPdfText("Invoice Date : ", 0, 6, "L", mediumnormal, "LRT", fHeight);
//			drawPdfText("HSN CODE : ", 0, 3, "L", mediumnormal, "LRT", fHeight);
            
            drawPdfText(SPartyName, 0, 5, "L", mediumbold, "LR", fHeight);
            drawPdfText(SInvoiceNo, 0, 4, "L", mediumbigbold, "LRB", fHeight); //"["+SYear+"] - "+
            drawPdfText(SInvoiceDate, 0, 6, "L", mediumbold, "LRB", fHeight);
//			drawPdfText(SHsnCode, 0, 2, "L", mediumbigbold, "LRB", fHeight); 
            
            drawPdfText(SInvAdd1, 0, 5, "L", mediumbold, "LR", fHeight);
            drawPdfText("Party GSTIN  :", 0, 4, "L", mediumnormal, "LR", fHeight);
            drawPdfText("DC NO & DC Date  :", 0, 6, "L", mediumnormal, "LR", fHeight);
            
            drawPdfText(SInvAdd2, 0, 5, "L", mediumbold, "LR", fHeight);
			drawPdfText(SGSTNo, 0, 4, "L", mediumbold, "LRB", fHeight);
			drawPdfText(SDCNo+" & "+SDCDate, 0, 6, "L", mediumbold, "LRB", fHeight);
            
            drawPdfText(SInvAdd3, 0, 5, "L", mediumbold, "LR", fHeight);
            drawPdfText("", 0, 10, "L", mediumnormal, "LR", fHeight);

			drawPdfText("", 0, 5, "L", mediumbold, "LR", fHeight);//SPlace
            drawPdfText("", 0, 10, "L", mediumnormal, "LR", fHeight);

			drawPdfText(SStateName, 0, 4, "L", mediumbold, "LRB", fHeight);
			drawPdfText(SStateCode, 0, 1, "L", mediumbold, "All", fHeight);
			//drawPdfText("", 0, 1, "L", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 10, "L", mediumnormal, "LRB", fHeight);
                     
           /* drawPdfText("Delivery At : ", 0, 16, "L", mediumnormal, "LR", fHeight);
                  
            drawPdfText(SDespAdd1, 0, 16, "L", mediumbold, "LR", fHeight);
                   
            drawPdfText(SDespAdd2, 0, 16, "L", mediumbold, "LR", fHeight);
           
            drawPdfText(SDespAdd3, 0, 16, "L", mediumbold, "LR", fHeight);
		    drawPdfText("", 0, 16, "L", mediumbold, "LR", fHeight);//SPlace

		    drawPdfText(SStateName, 0, 4, "L", mediumbold, "LRB", fHeight);
			drawPdfText(SStateCode, 0, 1, "L", mediumbold, "All", fHeight);
			drawPdfText("", 0, 11, "L", mediumbold, "LRB", fHeight);*/

		//	drawPdfText(SStateCode, 0, 16, "L", mediumbold, "LRB", fHeight);

		}
		catch(Exception ex) 
		{
            ex.printStackTrace();
        }
	}
	private void setGoodsDescription(String SDesc, String SConfirmRate,
            String SDespWeight,  String STotalValue, String SDepoAdvNo,int i,String SCGST,String SSGST,String SIGST,String SCess,String SCGSTPer,String SSGSTPer,String SIGSTPer,String SCessPer,String SHSN,int iSlNo,String SPreviousDCNo,String SInvoiceNo,String SUom,String SOthers) //,String STotal
	{
		try 
		{
			float fHeight   = 0;
            
           if(!SPreviousDCNo.equals(SInvoiceNo) || i==0)
			{
			
			drawPdfText("Sl. No.", 2, 0, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("DESCRIPTION", 2, 0, "C", mediumnormal, "ALL", fHeight);
			drawPdfText("HSN CODE", 2, 0, "C", mediumnormal, "ALL", fHeight);
			drawPdfText("UOM", 2, 0, "C", mediumnormal, "ALL", fHeight);				
            drawPdfText("Qty ", 2, 0, "C", mediumnormal, "ALL", fHeight);
			drawPdfText("Rate", 2, 0, "C", mediumnormal, "ALL", fHeight);
			drawPdfText("TOT ASSES VALUE", 2, 0, "C", mediumnormal, "ALL", fHeight);		
			drawPdfText("CGST", 0, 2, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("SGST", 0, 2, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("IGST", 0, 2, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("Cess", 0, 2, "C", mediumnormal, "ALL", fHeight);
			drawPdfText("%", 0, 0, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("Val", 0, 0, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("%", 0, 0, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("Val", 0, 0, "C", mediumnormal, "ALL", fHeight);
			drawPdfText("%", 0, 0, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("Val", 0, 0, "C", mediumnormal, "ALL", fHeight);
			drawPdfText("%", 0, 0, "C", mediumnormal, "ALL", fHeight);
            drawPdfText("Val", 0, 0, "C", mediumnormal, "ALL", fHeight);

			}

                        
            drawPdfText(String.valueOf(iSlNo)+".", 0, 0, "C", mediumbold, "LR", fHeight);
            drawPdfText(SDesc, 0, 0, "L", mediumbold, "LR", fHeight);
            drawPdfText(SHSN, 0, 0, "C", mediumbold, "LR", fHeight);
            drawPdfText(SUom, 0, 0, "C", mediumbold, "LR", fHeight);
            drawPdfText(SDespWeight, 0, 0, "R", mediumbold, "LR", fHeight);
         	drawPdfText(SConfirmRate, 0, 0, "R", mediumbold, "LR", fHeight);
         	drawPdfText(STotalValue, 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(SCGST, 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(SCGSTPer, 0, 0, "R", mediumbold, "LR", fHeight);
            drawPdfText(SSGST, 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(SSGSTPer, 0, 0, "R", mediumbold, "LR", fHeight);
            drawPdfText(SIGST, 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(SIGSTPer, 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(SCess, 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(SCessPer, 0, 0, "R", mediumbold, "LR", fHeight);            
			
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);    
			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
		    drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
	private void setTaxDetails(String STotalValue, String SCessPer, String sCess,
            String SCGstPer, String SCGstValue, String SsGstPer, String SsGstValue,
            String SIGstPer, String SIGstValue, String SOthers,
            String STaxPer, String STaxValue, String SFreightValue,
            String SRoundOff1, String SGrandTotal,String SGTotal,String SRoundOff,String SNetValue
			,double dTotalQty,double dTotalValue,double dTotalCgstValue,double dTotalSgstValue,double dTotalIgstValue,double dTotalCessValue,double dDiff
			) {
        try {
            float fHeight   = 0;
            	
			drawPdfText("", 0, 0, "C", mediumbold, "LR", fHeight);
            drawPdfText("Total", 0, 3, "L", mediumbold, "LR", fHeight);
         //   drawPdfText("", 0, 0, "C", mediumbold, "LR", fHeight);
			drawPdfText(common.getRound(dTotalQty,2), 0, 0, "C", mediumbold, "LR", fHeight);
            drawPdfText("", 0, 0, "R", mediumbold, "LR", fHeight);
         	drawPdfText(common.getRound(dTotalValue,2), 0, 0, "R", mediumbold, "LR", fHeight);
         	drawPdfText("", 0, 0, "R", mediumbold, "LR", fHeight);
 			drawPdfText(common.getRound(dTotalCgstValue,2), 0, 0, "R", mediumbold, "LR", fHeight);
            drawPdfText("", 0, 0, "R", mediumbold, "LR", fHeight);
 			drawPdfText(common.getRound(dTotalSgstValue,2), 0, 0, "R", mediumbold, "LR", fHeight);
            drawPdfText("", 0, 0, "R", mediumbold, "LR", fHeight);
 			drawPdfText(common.getRound(dTotalIgstValue,2), 0, 0, "R", mediumbold, "LR", fHeight);
            drawPdfText("", 0, 0, "R", mediumbold, "LR", fHeight);
			drawPdfText(common.getRound(dTotalCessValue,2), 0, 0, "R", mediumbold, "LR", fHeight);
//            drawPdfText(common.getRound(dTotal,2), 0, 0, "R", mediumbold, "LR", fHeight);

			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);    
			drawPdfText("", 0, 3, "C", mediumbold, "LRB", fHeight);                                 
		    drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
			drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
            drawPdfText("", 0, 0, "C", mediumbold, "LRB", fHeight);
           
            drawPdfText("", 0, 6, "L", mediumnormal, "L", fHeight);
            drawPdfText("Rounded Off", 0, 3, "L", mediumnormal, "NO", fHeight);
            drawPdfText("", 0, 2, "L", mediumbold, "NO", fHeight);
            drawPdfText(SOthers, 0, 4, "R", mediumbold, "LR", fHeight); // common.getRound(dDiff,2)
            
            drawPdfText("", 0, 6, "L", mediumnormal, "LB", fHeight);
            drawPdfText("GRAND TOTAL", 0, 3, "L", mediumnormal, "B", fHeight);
            drawPdfText("", 0, 2, "L", mediumbold, "B", fHeight);
            drawPdfText(common.getRound(SGrandTotal,0), 0, 4, "R", mediumbigbold, "ALL", fHeight);
            
            drawPdfText("Rupees : ", 0, 15, "L", mediumnormal, "LR", fHeight);
            drawPdfText(common.getRupee(common.getRound(SGrandTotal,0)), 0, 15, "L", mediumbigbold, "LRB", fHeight);

			//drawPdfText("", 0, 16, "L", mediumnormal, "LR", fHeight);
            //drawPdfText("GST Payable On Reverse Charge  : No ", 0, 16, "L", mediumbigbold, "LRB", fHeight);

        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
	private void setPODetails(String SPOInfo, String SInsurance) {
        try {
            float fHeight   = 0;
            
            drawPdfText("PO Details : ", 0, 3, "L", mediumnormal, "LR", fHeight);
            drawPdfText("Insurance Details : ", 0, 4, "L", mediumnormal, "LR", fHeight);
            
            drawPdfText(SPOInfo, 0, 3, "L", mediumbold, "LRB", fHeight);
            drawPdfText(SInsurance, 0, 4, "L", mediumbold, "LRB", fHeight);
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
	private void setBagAndTimeDetails(String SBagDetails, String SPreTime, String SRemTime, String STruckNo,String SPermitNo) {
        try {
            float fHeight   = 0;
            
            drawPdfText("Preparation Time / Date : ", 0, 5, "L", mediumnormal, "LR", fHeight);
            drawPdfText("Removal Time / Date : ", 0, 6, "L", mediumnormal, "LR", fHeight);
//            drawPdfText("Permit No : ", 0, 4, "L", mediumnormal, "LR", fHeight);		
            drawPdfText("Mode Of Transport : ", 0, 4, "L", mediumnormal, "LR", fHeight);
            
            drawPdfText(SPreTime, 0, 5, "L", mediumbold, "LRB", fHeight);
            drawPdfText(SRemTime, 0, 6, "L", mediumbold, "LRB", fHeight);
//            drawPdfText(SPermitNo, 0, 4, "L", mediumbold, "LRB", fHeight);
            drawPdfText(STruckNo, 0, 4, "L", mediumbold, "LRB", fHeight);
            

        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
	private void setTermsAndFinalPart(String SMillCode) {
        try {
            float fHeight   = 0;
            
            drawPdfText("TERMS & CONDITIONS", 0, 15, "L", mediumboldunderline, "LR", fHeight);
            
            drawPdfText("1. Cash payment shall be made only in Head Office. Branches are empowered to receive only crossed Cheques / Drafts, Persons are not appointed to collect Cash.\n", 0, 15, "L", mediumnormal, "LR", fHeight);
            drawPdfText("2. All Transaction are subject to TIRUPUR juristiction only..\n", 0, 15, "L", mediumnormal, "LR", fHeight);
            
            drawPdfText("\n", 0, 5, "L", mediumnormal, "LR", fHeight);
            drawPdfText("Certified that the particulars given above are true and correct and the amount", 0, 10, "L", mediumnormal, "LRT", fHeight);
            
            drawPdfText("\n", 0, 5, "L", mediumnormal, "LR", fHeight);
            drawPdfText("indicated represents the price actually charged and that there is no flow of", 0, 10, "L", mediumnormal, "LR", fHeight);
            
            drawPdfText("E. & O.E..\n", 0, 5, "L", mediumnormal, "LRB", fHeight);
            drawPdfText("additional consideration directly or indirectly from the buyer.", 0, 10, "L", mediumnormal, "LRB", fHeight);
            
            
            drawPdfText("Received Goods", 0, 5, "L", mediumbold, "LR", fHeight);
            drawPdfText("Prepared By", 0, 2, "C", mediumnormal, "LR", fHeight);
            drawPdfText("Checked By", 0, 2, "C", mediumnormal, "LR", fHeight);
			if(SMillCode.equals("1"))
			drawPdfText("For AMARJOTHI SPINNING MILLS LTD(DYEING DIVISION).", 0, 6, "C", mediumbold, "LR", fHeight);
			else
            drawPdfText("For AMARJOTHI SPINNING MILLS LTD.", 0, 6, "C", mediumbold, "LR", fHeight);
            
            drawPdfText("\n\n\n\n\n", 0, 5, "L", mediumnormal, "LR", fHeight);
            drawPdfText("", 0, 2, "C", mediumnormal, "LR", fHeight);
            drawPdfText("", 0, 2, "C", mediumnormal, "LR", fHeight);
            drawPdfText("", 0, 6, "C", mediumbold, "LR", fHeight);
            
            drawPdfText("Party's Signature with seal.", 0, 5, "R", mediumnormal, "LRB", fHeight);
            drawPdfText("", 0, 2, "C", mediumnormal, "LRB", fHeight);
            drawPdfText("", 0, 2, "C", mediumnormal, "LRB", fHeight);
            drawPdfText("Authorised Signatory.", 0, 6, "C", mediumnormal, "LRB", fHeight);
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
	private void drawPdfText(String SText, int iRowSpan, int iColSpan, String SAlign,Font theFont, String SBorderType, float fHeight) {
        try {
            PdfPCell c1 = new PdfPCell(new Phrase(SText, theFont));
            
            if(SAlign.equals("L")) {
                c1      . setHorizontalAlignment(Element.ALIGN_LEFT);
            }
            if(SAlign.equals("C")) {
                c1      . setHorizontalAlignment(Element.ALIGN_CENTER);
            }
            if(SAlign.equals("R")) {
                c1      . setHorizontalAlignment(Element.ALIGN_RIGHT);
            }

            if(iRowSpan > 0) {
                c1      . setRowspan(iRowSpan);
            }
            
            if(iColSpan > 0) {
                c1      . setColspan(iColSpan);
            }

            if(SBorderType.equals("ALL")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
            }
            if(SBorderType.equals("LR") || SBorderType.equals("RL")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            }
            if(SBorderType.equals("LRT") || SBorderType.equals("RLT")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
            }
            if(SBorderType.equals("LRB") || SBorderType.equals("RLB")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            }
            if(SBorderType.equals("LB")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
            }
            if(SBorderType.equals("L")) {
                c1.setBorder(Rectangle.LEFT);
            }
            if(SBorderType.equals("B")) {
                c1.setBorder(Rectangle.BOTTOM);
            }
            if(SBorderType.equals("NO")) {
                c1.setBorder(Rectangle.NO_BORDER);
            }

            if(fHeight == 0) {
                c1      . hasMinimumHeight();
            }else {
                c1      . setFixedHeight(12f);
            }
            
            table       . addCell(c1);
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void drawPdfParagraph(Paragraph thePara, int iRowSpan, int iColSpan, String SAlign, String SBorderType, float fHeight) {
        try {
            PdfPCell c1 = new PdfPCell(thePara);
            
            if(SAlign.equals("L")) {
                c1      . setHorizontalAlignment(Element.ALIGN_LEFT);
            }
            if(SAlign.equals("C")) {
                c1      . setHorizontalAlignment(Element.ALIGN_CENTER);
            }
            if(SAlign.equals("R")) {
                c1      . setHorizontalAlignment(Element.ALIGN_RIGHT);
            }

            if(iRowSpan > 0) {
                c1      . setRowspan(iRowSpan);
            }
            
            if(iColSpan > 0) {
                c1      . setColspan(iColSpan);
            }

            if(SBorderType.equals("ALL")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
            }
            if(SBorderType.equals("LR") || SBorderType.equals("RL")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            }
            if(SBorderType.equals("LRT") || SBorderType.equals("RLT")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
            }
            if(SBorderType.equals("LRB") || SBorderType.equals("RLB")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            }
            if(SBorderType.equals("LB")) {
                c1.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
            }
            if(SBorderType.equals("L")) {
                c1.setBorder(Rectangle.LEFT);
            }
            if(SBorderType.equals("B")) {
                c1.setBorder(Rectangle.BOTTOM);
            }
            if(SBorderType.equals("NO")) {
                c1.setBorder(Rectangle.NO_BORDER);
            }

            if(fHeight == 0) {
                c1      . hasMinimumHeight();
            }else {
                c1      . setFixedHeight(12f);
            }
            
            table       . addCell(c1);
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
	private void closePdf() {
        try {
            document . close();
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }   
}
