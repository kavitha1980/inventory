package Sales;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class UserwiseMaterialFrame extends JInternalFrame
{
     JLayeredPane   Layer;
     int            iMillCode,iRow;
     JobWorkIssueMiddlePanel SalesPanel;
     String SMCode,SMName,SStkQty;

     TabReport      tabreport;

     JButton        BOk;
     JLabel         LMaterial,LMatCode,LStkQty,LSalesQty;

     Vector VUCode,VUName,VUQty,VURate;

     JPanel         TopPanel,MiddlePanel,BottomPanel;

     Common common = new Common();

     Object RowData[][];
     String ColumnData[] = {"User Name","User Qty","SalesQty"};
     String ColumnType[] = {"S"        ,"N"       ,"E"       };

     Connection theConnection = null;

     UserwiseMaterialFrame(JLayeredPane Layer,int iMillCode,JobWorkIssueMiddlePanel SalesPanel,int iRow,String SMCode,String SMName,String SStkQty)
     {
          this.Layer      = Layer;
          this.iMillCode  = iMillCode;
          this.SalesPanel = SalesPanel;
          this.iRow       = iRow;
          this.SMCode     = SMCode;
          this.SMName     = SMName;
          this.SStkQty    = SStkQty;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          LMaterial      = new JLabel("");
          LMatCode       = new JLabel("");
          LStkQty        = new JLabel("");
          LSalesQty      = new JLabel("");

          BOk            = new JButton("Okay");
          TopPanel       = new JPanel(true);
          MiddlePanel    = new JPanel(true);
          BottomPanel    = new JPanel(true);
     }

     private void setLayouts()
     {
          TopPanel  .setLayout(new GridLayout(5,2,20,20));
          MiddlePanel.setLayout(new BorderLayout());

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,700,490);
     }

     private void addComponents()
     {
          TopPanel       .add(new JLabel("Selected Material Code"));
          TopPanel       .add(LMatCode);
          TopPanel       .add(new JLabel("Selected Material Name"));
          TopPanel       .add(LMaterial);
          TopPanel       .add(new JLabel("Stock Qty"));
          TopPanel       .add(LStkQty);
          TopPanel       .add(new JLabel("Sales Qty"));
          TopPanel       .add(LSalesQty);
          TopPanel       .add(new JLabel(""));
          TopPanel       .add(new JLabel(""));

	  BottomPanel    .add(BOk);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

          setPresets();
     }

     private void setPresets()
     {
          LMaterial .setText(SMName);
          LMatCode  .setText(SMCode);  
	  LStkQty   .setText(SStkQty);
          LSalesQty .setText("");

          setTabReport();
     }

     private void addListeners()
     {
         BOk.addActionListener(new ActList());
     }
               
     public void setTabReport()
     {
	  setDataVector();
          setRowData();
          try
          {
             MiddlePanel.remove(tabreport);
          }
          catch(Exception ex){}
          try
          {
             tabreport = new TabReport(RowData,ColumnData,ColumnType);
             MiddlePanel.add("Center",tabreport);
             tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
             setSelected(true);
             BOk.setEnabled(true);
             Layer.repaint();
             Layer.updateUI();
             tabreport.ReportTable.addKeyListener(new KeyList());
          }
          catch(Exception ex)
          {
             System.out.println(ex);
          }
     }

     private class KeyList extends KeyAdapter
     {
         public void keyPressed(KeyEvent ke)
         {
            setSalesQty();
         }
         public void keyReleased(KeyEvent ke)
         {
            setSalesQty();
         }
         public void keyTyped(KeyEvent ke)
         {
            setSalesQty();
         }
     }

     private void setSalesQty()
     {
         double dQty = 0;

         for(int i=0;i<RowData.length;i++)
         {
              dQty = dQty + common.toDouble((String)RowData[i][2]);
         }

         LSalesQty.setText(common.getRound(dQty,3));
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validData())
               {
                  saveData();
               }
               else
               {
                  BOk.setEnabled(true);
               }
          }
     }

     private boolean validData()
     {
         for(int i=0;i<RowData.length;i++)
         {
              double dStkQty   = common.toDouble((String)RowData[i][1]);
              double dSalesQty = common.toDouble((String)RowData[i][2]);

	      if(dSalesQty<0 || dSalesQty>dStkQty)
              {                   JOptionPane.showMessageDialog(null,"Invalid Sales Qty @Row-"+String.valueOf(i+1),"Error",JOptionPane.ERROR_MESSAGE);                   return false;              }	 }

	 setSalesQty();

	 if(common.toDouble(LSalesQty.getText())<=0)
         {              JOptionPane.showMessageDialog(null,"Sales Qty Can't be Zero","Error",JOptionPane.ERROR_MESSAGE);              return false;         }
         return true;
     }

     private void saveData()
     {
         BOk.setEnabled(false);

         for(int i=0;i<RowData.length;i++)
         {
              double dQty        = common.toDouble((String)RowData[i][2]);

	      if(dQty<=0)
	           continue;


	      SalesPanel.VMUserQty[iRow].addElement(common.getRound(dQty,3));
	      SalesPanel.VMUserCode[iRow].addElement((String)VUCode.elementAt(i));
	      SalesPanel.VMUserRate[iRow].addElement((String)VURate.elementAt(i));
         }

         SalesPanel.tabreport.ReportTable.setValueAt(LSalesQty.getText(),iRow,6);
         removeHelpFrame();
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }

     public void setDataVector()
     {
          VUName      = new Vector();
          VUCode      = new Vector();
          VUQty       = new Vector();
          VURate      = new Vector();	  

          String QS = " Select ItemStock.HodCode,RawUser.UserName,ItemStock.StockValue,sum(ItemStock.Stock) from ItemStock "+
                      " Inner Join RawUser on ItemStock.HodCode=RawUser.UserCode "+
                      " And ItemStock.ItemCode='"+SMCode+"' and ItemStock.MillCode="+iMillCode+
                      " Group by ItemStock.HodCode,RawUser.UserName,ItemStock.StockValue "+
                      " having sum(ItemStock.Stock)>0 "+
                      " Order By 2";

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();   
               Statement      stat           = theConnection.createStatement();

               ResultSet result1 = stat.executeQuery(QS);

               while(result1.next())
               {
                    VUCode    .addElement(common.parseNull(result1.getString(1)));
                    VUName    .addElement(common.parseNull(result1.getString(2)));
                    VURate    .addElement(common.parseNull(result1.getString(3)));
                    VUQty     .addElement(common.parseNull(result1.getString(4)));
               }
               result1.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData()
     {
         RowData     = new Object[VUCode.size()][ColumnData.length];
         for(int i=0;i<VUCode.size();i++)
         {
               RowData[i][0]  = (String)VUName.elementAt(i);
               RowData[i][1]  = (String)VUQty.elementAt(i);
               RowData[i][2]  = "";
         }  
     } 

}
