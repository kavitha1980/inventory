// Used By Order,GRN  and other operations
// The Operation Varies with the state of bSig.
// Initially it is set to true once called from Non-Order activities,
// the same shall be set to false.

package enquiry;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class EnqInvMiddlePanel extends JPanel
{
     JTable              ReportTable;
     EnqInvTableModel    dataModel;
     Common              common = new Common();;
     
     JLayeredPane        DeskTop;
     Object              RowData[][];
     String              ColumnData[],ColumnType[];
     Vector              VSelectedMrsSlNo;
     int                 iMillCode;
     
     JPanel              GridPanel,BottomPanel,ControlPanel;
     JPanel              GridTop,GridBottom;
     JButton             BAdd;
     
     Vector              VDesc,VMake,VDraw,VCatl;
     Vector              VCode,VName;
     
     String              SCatlog   = "",SDrawing  = "",SDescrip  = "",SMakeing  = "";
     
     
     // Constructor method referred in Direct Enquiry Frame Operations
     public EnqInvMiddlePanel(JLayeredPane DeskTop, Object RowData[][],String ColumnData[],String ColumnType[],Vector VSelectedMrsSlNo,int iMillCode)
     {
          this.DeskTop             = DeskTop;
          this.RowData             = RowData;
          this.ColumnData          = ColumnData;
          this.ColumnType          = ColumnType;
          this.VSelectedMrsSlNo    = VSelectedMrsSlNo;
          this.iMillCode           = iMillCode;
          
          createComponents();
          setLayouts();
          addComponents();
          setReportTable();
     }
     
     public void createComponents()
     {
          GridPanel      = new JPanel(true);
          GridBottom     = new JPanel(true);
          BottomPanel    = new JPanel();
          ControlPanel   = new JPanel();
          
          VDesc          = new Vector();
          VMake          = new Vector();
          VDraw          = new Vector();
          VCatl          = new Vector();
          
          for(int i=0;i<RowData.length;i++)
          {
               int       iMrsNo    = 0,iSlNo = 0;
               String    SCode     = ((String)RowData[i][0]).trim();

                         iMrsNo    = common.toInt(((String)RowData[i][2]).trim());

               setCDM(SCode);

               if(iMrsNo!=0)
               {
                    iSlNo     = common.toInt((String)VSelectedMrsSlNo.elementAt(i));
                    setDM(SCode,iSlNo,iMrsNo);

                    VDesc     . addElement(SDescrip);
                    VMake     . addElement(SMakeing);
               }
               else
               {
                    VDesc     . addElement("");
                    VMake     . addElement("");
               }

               VDraw     . addElement(SDrawing);
               VCatl     . addElement(SCatlog);
          }
     }
     
     public void setLayouts()
     {
          GridPanel      . setLayout(new GridLayout(1,1));
          BottomPanel    . setLayout(new BorderLayout());
          ControlPanel   . setLayout(new FlowLayout());
          GridBottom     . setLayout(new BorderLayout()); 
     }
     
     public void addComponents()
     {
          BottomPanel    . add("South",ControlPanel);
     }
     
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BAdd)
               {
                    Vector VEmpty1 = new Vector();
                    for(int i=0;i<ColumnData.length;i++)
                    {
                         String SCode = ((String)RowData[i][0]).trim();
                         setCDM(SCode);

                         VEmpty1   . addElement(" ");
                         VDesc     . addElement(" ");
                         VMake     . addElement(" ");
                         VDraw     . addElement(SDrawing);
                         VCatl     . addElement(SCatlog);
                    }

                    dataModel      . addRow(VEmpty1);
                    ReportTable    . updateUI();
               }
          }
     }
     
     public void setReportTable()
     {
          try
          {
               dataModel      = new EnqInvTableModel(RowData,ColumnData,ColumnType);   
               ReportTable    = new JTable(dataModel);
               ReportTable    . setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
               
               DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
               cellRenderer   . setHorizontalAlignment(JLabel.RIGHT);
               
               for (int col=0;col<ReportTable.getColumnCount();col++)
               {
                    if(ColumnType[col]=="N" || ColumnType[col]=="B")
                         ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
               }
               
               ReportTable.setShowGrid(false);
               
                                setLayout(new BorderLayout());
               GridBottom     . add(ReportTable.getTableHeader(),BorderLayout.NORTH);
               GridBottom     . add(new JScrollPane(ReportTable),BorderLayout.CENTER);
               
               GridPanel      . add(GridBottom);
               
               add(BottomPanel,BorderLayout.SOUTH);
               add(GridPanel,BorderLayout.CENTER);
               
               ReportTable    . addMouseListener(new MouseList());
               ReportTable    . addKeyListener(new DescList());
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
     
     public Object[][] getFromVector()
     {
          return dataModel.getFromVector(); 
     }
     
     public String getDesc(int i)
     {
          try
          {
               return (String)VDesc.elementAt(i);
          }
          catch(Exception ex){}
          return "";
     }
     
     public String getCatl(int i)
     {
          try
          {
               return (String)VCatl.elementAt(i);
          }
          catch(Exception ex){}
          return "";
     }
     
     public String getDraw(int i)
     {
          try
          {
               return (String)VDraw.elementAt(i);
          }
          catch(Exception ex){}
          return "";
     }
     
     public String getMake(int i)
     {
          try
          {
               return (String)VMake.elementAt(i);
          }
          catch(Exception ex){}
          return "";
     }
     
     public String getQty(int i)   // 3
     {
          Vector VCurVector   = dataModel.getCurVector(i);
          String str          = ((String)VCurVector.elementAt(3)).trim();
          
          return str;
     }
     
     public String getDueDate(int i)  //  4
     {
          DateField2 DueDate    = new DateField2();

          Vector VCurVector = dataModel.getCurVector(i);
          try
          {
               String str = (String)VCurVector.elementAt(4);
               DueDate.TYear  .setText(str.substring(0,2));
               DueDate.TMonth .setText(str.substring(3,5));
               DueDate.TDay   .setText(str.substring(6,10));

               if((str.trim()).length()==0)
                    return DueDate.toNormal();
               else
                    return common.pureDate(str);
          }
          catch(Exception ex)
          {
               return " ";
          }
     }
     
     public class MouseList extends MouseAdapter
     {
          public void mouseClicked(MouseEvent me)
          {
               if (me.getClickCount()==2)
               {
                    try
                    {
                         DescMouse DM = new DescMouse(DeskTop,VDesc,VMake,VDraw,VCatl,ReportTable,dataModel,VSelectedMrsSlNo);
                         DeskTop   . add(DM);
                         DeskTop   . repaint();
                         DM        . setSelected(true);
                         DeskTop   . updateUI();
                         DM        . show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
          }
     }
     
     private void setDescMouse()
     {
          DescMouse DM = new DescMouse(DeskTop,VDesc,VMake,VDraw,VCatl,ReportTable,dataModel,VSelectedMrsSlNo);
          try
          {
               DeskTop   . add(DM);
               DeskTop   . repaint();
               DM        . setSelected(true);
               DeskTop   . updateUI();
               DM        . show();
          }
          catch(java.beans.PropertyVetoException ex){}
     }
     
     public class DescList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
                    setDescMouse();
          }
     }

     public void setCDM(String SMatCode)
     {
          ResultSet result = null;
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               result = stat.executeQuery("Select Catl,Draw From InvItems Where Item_Code='"+SMatCode+"'");
          
               while(result.next())
               {
                    SCatlog   = common.parseNull(result.getString(1));
                    SDrawing  = common.parseNull(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("@ setCDM");
               System.out.println(ex);
          }
     }

     public void setDM(String SMatCode,int iSlNo,int iMrsNo)
     {
          ResultSet result = null;
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               
               result = stat.executeQuery("Select remarks,Make From Mrs Where Item_Code='"+SMatCode+"' and Mrs.SlNo ="+iSlNo+" and Mrs.MrsNo = "+iMrsNo+" and Mrs.MillCode="+iMillCode);

               while(result.next())
               {
                    SDescrip = common.parseNull(result.getString(1));
                    SMakeing = common.parseNull(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("@ setDM");
               System.out.println(ex);
          }
     }
}
