package enquiry;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class EnqInvTableModel extends DefaultTableModel
{
     Object         RowData[][],ColumnNames[],ColumnType[];
     NextField      TAdd,TLess;
     Common         common = new Common();
     
     public EnqInvTableModel(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType)
     {
          super(RowData,ColumnNames);
          this.RowData     = RowData;
          this.ColumnNames = ColumnNames;
          this.ColumnType  = ColumnType;
     }
     
     public Class getColumnClass(int col)
     {
          return getValueAt(0,col).getClass();
     }
     
     public boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="B" || ColumnType[col]=="E")
               return true;
          return false;
     }
     
     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               if(column==4)
               {
                    String    str       = ((String)aValue).trim();
                              aValue    = common.getDate(str,0,1);
               }
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
          }
          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }
     
     public Object[][] getFromVector()
     {
          Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
          for(int i=0;i<super.dataVector.size();i++)
          {
               Vector curVector = (Vector)super.dataVector.elementAt(i);
               for(int j=0;j<curVector.size();j++)
                    FinalData[i][j] = (String)curVector.elementAt(j);
          }
          return FinalData;
     }
     
     public int getRows()
     {
          return super.dataVector.size();
     }
     
     public Vector getCurVector(int i)
     {
          return (Vector)super.dataVector.elementAt(i);
     }
}
