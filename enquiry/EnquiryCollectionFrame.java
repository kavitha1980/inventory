package enquiry;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class EnquiryCollectionFrame extends JInternalFrame
{
     JButton             BOk,BCancel,BSuppliers;
     JTextArea           ASuppliers;
     JPanel              TopPanel,TopNorthPanel,TopSouthPanel,BottomPanel;
     JLayeredPane        DeskTop;
     
     Vector              VSelectedSupCode;
     Vector              VSupCode,VSupName;
     Vector              VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedMrsSlNo;
     Vector              VGSeleCode,VGSeleMrsNo,VGSeleMrsSlNo;
     
     NextField           TEnquiryNo;
     DateField2          TDate;
     StatusPanel         SPanel;
     Common              common;

     Connection          theConnection     = null;

     EnquiryMiddlePanel  MiddlePanel;

     String              SPColour  = "",SPSet     = "",SPSize    = "",SPSide    = "";
     
     int                 iMillCode,iUserCode;
     String              SItemTable,SSupTable;

     boolean             bComflag = true;
     
     EnquiryCollectionFrame(JLayeredPane DeskTop,Vector VSelectedCode,Vector VSelectedName,Vector VSelectedMRS,Vector VSelectedQty,Vector VSelectedMrsSlNo,StatusPanel SPanel,int iMillCode,int iUserCode,String SItemTable,String SSupTable,Vector VGSeleCode,Vector VGSeleMrsNo,Vector VGSeleMrsSlNo)
     {
          super("Enquiry Placement Against Selected MRS Materials");
          try
          {
               this.DeskTop             = DeskTop;
               this.VSelectedCode       = VSelectedCode;
               this.VSelectedName       = VSelectedName;
               this.VSelectedMRS        = VSelectedMRS;
               this.VSelectedQty        = VSelectedQty;
               this.VSelectedMrsSlNo    = VSelectedMrsSlNo;
               this.SPanel              = SPanel;
               this.iMillCode           = iMillCode;
               this.iUserCode           = iUserCode;
               this.SItemTable          = SItemTable;
               this.SSupTable           = SSupTable;
			this.VGSeleCode	     = VGSeleCode;
			this.VGSeleMrsNo		= VGSeleMrsNo;
			this.VGSeleMrsSlNo		= VGSeleMrsSlNo;

               common                   = new Common();
               
               getDBConnection();
               createComponents();
               setSuppliers();
               setLayouts();
               addComponents();
               addListeners();
               getNextEnquiryNo();
               show();
          }catch(Exception Ex)
          {
               Ex.printStackTrace();
          }
     }
     
     public void getDBConnection()
     {
               ORAConnection  oraConnection  =    ORAConnection.getORAConnection();
                              theConnection  =    oraConnection.getConnection();
     }

     public void createComponents()
     {
          BOk                 = new JButton("Okay");
          BCancel             = new JButton("Abort");
          BSuppliers          = new JButton("Supplier Selector");
          ASuppliers          = new JTextArea(2,40);
          
          VSelectedSupCode    = new Vector();
          VSupCode            = new Vector();
          VSupName            = new Vector();
          
          TEnquiryNo          = new NextField();
          TDate               = new DateField2();
          MiddlePanel         = new EnquiryMiddlePanel(DeskTop,VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedMrsSlNo,iMillCode);
          
          TopPanel            = new JPanel();
          TopNorthPanel       = new JPanel();  
          TopSouthPanel       = new JPanel();
          BottomPanel         = new JPanel();
          
          TDate.setTodayDate();

          TEnquiryNo.setEditable(false);
          ASuppliers.setEditable(false);
          TDate.setEditable(false);
     }
     
     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          
          getContentPane()    . setLayout(new BorderLayout());
          TopPanel            . setLayout(new BorderLayout());
          TopNorthPanel       . setLayout(new GridLayout(3,2,5,5));
          TopSouthPanel       . setLayout(new GridLayout(1,2,5,5));
          BottomPanel         . setLayout(new FlowLayout());
     }
     
     public void addComponents()
     {
          TopNorthPanel  . add(new JLabel("Enquiry No"));
          TopNorthPanel  . add(TEnquiryNo);
          
          TopNorthPanel  . add(new JLabel("Enquiry Date"));
          TopNorthPanel  . add(TDate);
          
          TopNorthPanel  . add(new JLabel("Select Suppliers"));
          TopNorthPanel  . add(BSuppliers);
          
          TopSouthPanel  . add(new JLabel("List of Suppliers"));
          TopSouthPanel  . add(ASuppliers);
          
          TopPanel       . add("North",TopNorthPanel);
          TopPanel       . add("South",TopSouthPanel);
          BottomPanel    . add(BOk);
          BottomPanel    . add(BCancel);
          
          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     
     public void addListeners()
     {
          BSuppliers     . addActionListener(new SuppliersSearch(DeskTop,VSelectedSupCode,ASuppliers,VSupCode,VSupName,SSupTable));
          BOk            . addActionListener(new ActList());
          BCancel        . addActionListener(new ExitList());
     }
     
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(MiddlePanel.RowData==null)
               {
                    JOptionPane.showMessageDialog(null,"Please Select the materials or supplier ","Error",JOptionPane.ERROR_MESSAGE);
               }
               else
               {
                    if(VSelectedSupCode.size()>0)
                    {
                         if(isCheck())
                         {
                              try
                              {
                                   BOk.setEnabled(false);
                                   insertEnquiryDetails();
                                   setMrsLink();

                                   try
                                   {
                                        if(bComflag)
                                        {
                                             theConnection  . commit();
                                             System         . out.println("Commit");
                                             theConnection  . setAutoCommit(true);
                                        }
                                        else
                                        {
                                             theConnection  . rollback();
                                             System         . out.println("RollBack");
                                             theConnection  . setAutoCommit(true);
                                        }
                                   }catch(Exception ex)
                                   {
                                        ex.printStackTrace();
                                   }

                                   removeHelpFrame();
                              }catch(Exception ex)
                              {
                                   ex.printStackTrace();
                              }
                         }
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Choose the Supplier Name","Error",JOptionPane.ERROR_MESSAGE);
                    }
               }
          }
     }

     public String getNextEnquiryNo()
     {
          String SEnqNo = "";
          String QS     = "";

          QS = " Select (maxno+1) From ConfigAll where Id=3";

          try
          {
               Statement stat      = theConnection. createStatement();
               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         SEnqNo    = common.parseNull((String)result.getString(1));
                         result    . close();
                         stat      . close();

          }catch(Exception Ex)
          {
               System.out.println(Ex);
               Ex.printStackTrace();
          }
          TEnquiryNo.setText(SEnqNo.trim());
           
          return TEnquiryNo.getText();
     }

     public String getInsertEnquiryNo()
     {
          String SEnqNo = "";
          String QS     = "";

          QS = " Select (maxno+1) From ConfigAll where Id=3 for update of MaxNo noWait";

          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection. setAutoCommit(false);

               Statement stat      = theConnection. createStatement();

               PreparedStatement thePrepare = theConnection.prepareStatement(" Update ConfigAll set MaxNo = ?  where Id = 3");

               ResultSet result    = stat         . executeQuery(QS);
                         result    . next();
                         SEnqNo    = common.parseNull((String)result.getString(1));
                         result    . close();

               thePrepare.setInt(1,common.toInt(SEnqNo));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();

          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    getInsertEnquiryNo();
                    bComflag  = true;
               }
               else
               {
                    bComflag  = false;
               }
          }
          TEnquiryNo.setText(SEnqNo.trim());
           
          return TEnquiryNo.getText();
     }

     public class ExitList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    removeHelpFrame();
               }catch(Exception ex)
               {
                    ex.printStackTrace();
               }
          }
     }
     
     public void insertEnquiryDetails()
     {
          try
          {
               Statement stat = theConnection.createStatement();
               
               TEnquiryNo  . setText(getInsertEnquiryNo());

               for(int i=0;i<VSelectedSupCode.size();i++)
               {
                    int iSSlNo = 1;
                    String SSupCode = (String)VSelectedSupCode.elementAt(i);
                    for(int j=0;j<MiddlePanel.RowData.length;j++)
                    {
                         String QString      = "";

                         String SDueDate     = MiddlePanel    . MiddlePanel. getDueDate(j);
                         String SQty         = MiddlePanel    . MiddlePanel. getQty(j);
                         String SDesc        = common.getNarration(MiddlePanel    . MiddlePanel. getDesc(j));
                         String SCatl        = MiddlePanel    . MiddlePanel. getCatl(j);
                         String SDraw        = MiddlePanel    . MiddlePanel. getDraw(j);
                         String SMake        = MiddlePanel    . MiddlePanel. getMake(j);
                         String MSSlNo       = (String)VSelectedMrsSlNo. get(j);
                         String SSlNo        = String         . valueOf(iSSlNo+j);
                         
                         if(!isStationary(((String)MiddlePanel.RowData[j][0]).trim()))
                         {
                              QString = "Insert Into Enquiry(ID,EnqNo,EnqDate,Sup_Code,MRS_No,Item_Code,Qty,DueDate,Descr,Catl,Drawing,Make,CREATIONDATE,USERCODE,MRSSLNO,SLNO,MillCode) Values (";
                              QString = QString+"0"+getNextVal("ENQ_SEQ")+",";
                              QString = QString+"0"+TEnquiryNo.getText()+",";
                              QString = QString+"'"+TDate.toNormal()+"',";    
                              QString = QString+"'"+SSupCode+"',";
                              QString = QString+"0"+common.toInt(((String)MiddlePanel.RowData[j][2]).trim())+",";
                              QString = QString+"'"+((String)MiddlePanel.RowData[j][0]).trim()+"',";
                              QString = QString+"0"+SQty+",";
                              QString = QString+"0"+SDueDate+",";
                              QString = QString+"'"+SDesc+"',";
                              QString = QString+"'"+SCatl+"',";
                              QString = QString+"'"+SDraw+"',";
                              QString = QString+"'"+SMake+"',";
                              QString = QString+"'"+common.getServerDate()+"',";
                              QString = QString+iUserCode+",";
                              QString = QString+"0"+MSSlNo+",";
                              QString = QString+SSlNo+",";
                              QString = QString+"0"+iMillCode+")";
                         }
                         else
                         {
                              QString = "Insert Into Enquiry(ID,EnqNo,EnqDate,Sup_Code,MRS_No,Item_Code,Qty,DueDate,Descr,Catl,Drawing,Make,CREATIONDATE,USERCODE,MRSSLNO,SLNO,MillCode,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE) Values (";
                              QString = QString+"0"+getNextVal("ENQ_SEQ")+",";
                              QString = QString+"0"+TEnquiryNo.getText()+",";
                              QString = QString+"'"+TDate.toNormal()+"',";    
                              QString = QString+"'"+SSupCode+"',";
                              QString = QString+"0"+common.toInt(((String)MiddlePanel.RowData[j][2]).trim())+",";
                              QString = QString+"'"+((String)MiddlePanel.RowData[j][0]).trim()+"',";
                              QString = QString+"0"+SQty+",";
                              QString = QString+"0"+SDueDate+",";
                              QString = QString+"'"+SDesc+"',";
                              QString = QString+"'"+SCatl+"',";
                              QString = QString+"'"+SDraw+"',";
                              QString = QString+"'"+SMake+"',";
                              QString = QString+"'"+common.getServerDate()+"',";
                              QString = QString+iUserCode+",";
                              QString = QString+"0"+MSSlNo+",";
                              QString = QString+SSlNo+",";
                              QString = QString+"0"+iMillCode+",";
                              QString = QString+"'"+SPColour+"',";
                              QString = QString+"'"+SPSet+"',";
                              QString = QString+"'"+SPSize+"',";
                              QString = QString+"'"+SPSide+"')";
                         }

                         if(theConnection.getAutoCommit())
                              theConnection    . setAutoCommit(false);

                         stat.execute(QString);
                    }
               }
               stat . close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     
     public void setMrsLink()
     {
          try
          {
               Statement      stat           =  theConnection.createStatement();


               for(int i=0;i<VGSeleMrsNo.size();i++)
               {
                    String    QS1 = "Update MRS set ";
                              QS1 = QS1+" EnquiryNo   ="+TEnquiryNo.getText();
                              QS1 = QS1+" Where MrsNo ="+(String)VGSeleMrsNo.elementAt(i)+" and ";
                              QS1 = QS1+" SlNo = "+(String)VGSeleMrsSlNo.elementAt(i)+" and ";
                              QS1 = QS1+" MillCode = "+iMillCode;

                    if(theConnection.getAutoCommit())
                         theConnection    . setAutoCommit(false);

                    stat.executeUpdate(QS1);
               }
               stat . close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
     
     public void setSuppliers()
     {
          ResultSet result    = null;
          VSupCode            = new Vector();
          VSupName            = new Vector();
          
          try
          {
               Statement      stat           =  theConnection.createStatement();
               result         =  stat.executeQuery("Select Ac_Code,Name From "+SSupTable+" Order By Name");
               
               while(result.next())
               {
                    VSupCode.addElement(result.getString(1));
                    VSupName.addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     
     private int getNextVal(String SSequence) throws Exception
     {
          int iValue = -1;
          String QS = "Select "+SSequence+".NextVal from dual";
          try
          {
               Statement      stat          =  theConnection.createStatement();
               
               ResultSet result = stat.executeQuery(QS);
               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("getNextVal :"+SSequence+ex);
          }
          return iValue;
     }
     
     private boolean isCheck()
     {
          String    SEnqDate = TDate.TYear.getText()+TDate.TMonth.getText()+TDate.TDay.getText();
          SEnqDate = SEnqDate.trim();

          if(SEnqDate.length()!=8)
          {
               JOptionPane.showMessageDialog(null,"Please enter the Enquiry Date Correctly","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          for(int j=0;j<MiddlePanel.RowData.length;j++)
          {
               String SDueDate   = MiddlePanel.MiddlePanel.getDueDate(j);
               String SQty       = MiddlePanel.MiddlePanel.getQty(j);
               
               double dQty       = common.toDouble(SQty);
               int iDiff         = common.toInt(common.getDateDiff(common.parseDate(SDueDate),common.parseDate(SEnqDate)));
               try
               {
                    if(dQty<=0)
                    {
                         JOptionPane.showMessageDialog(null,"Qty must be Greater Than Zero ","Error",JOptionPane.ERROR_MESSAGE);
                         
                         return false;
                    }
          
                    if(iDiff<0 || SDueDate.length()!=8)
                    {
                         JOptionPane.showMessageDialog(null,"Due Date is Empty or Check the DateFormat(dd.mm.yyyy) or Enter DueDate is greater than EnquiryDate","Error",JOptionPane.ERROR_MESSAGE);
                         
                         return false;
                    }
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
          return true;
     }

     private boolean isStationary(String SMatCode)
     {
          boolean   bFlag          = true;
          String    SStkGroupCode  = "";
          String    QS             = "";
          try
          {
               Statement      stat          =  theConnection     . createStatement();
          
               if(iMillCode==0)
               {
                    QS =    " select Invitems.stkgroupcode,INVITEMS.PAPERCOLOR,INVITEMS.PAPERSETS,INVITEMS.PAPERSIZE,INVITEMS.PAPERSIDE from invitems"+
                            " where invitems.item_code = '"+SMatCode+"'";
               }
               else
               {
                    QS =    " select "+SItemTable+".stkgroupcode,INVITEMS.PAPERCOLOR,INVITEMS.PAPERSETS,INVITEMS.PAPERSIZE,INVITEMS.PAPERSIDE from "+SItemTable+""+
                            " inner join invitems on invitems.item_code = "+SItemTable+".item_code"+
                            " where "+SItemTable+".item_code = '"+SMatCode+"'";
               }
               ResultSet result = stat.executeQuery(QS);

               while(result    . next())
               {
                    SStkGroupCode  = common.parseNull((String)result.getString(1));
                    SPColour       = common.parseNull((String)result.getString(2));
                    SPSet          = common.parseNull((String)result.getString(3));
                    SPSize         = common.parseNull((String)result.getString(4));
                    SPSide         = common.parseNull((String)result.getString(5));
               }
               result    . close();
               stat      . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }

          if(SStkGroupCode.equals("B01"))
          {
               SPColour       = SPColour. toUpperCase();
               SPSet          = SPSet   . toUpperCase();
               SPSize         = SPSize  . toUpperCase();
               SPSide         = SPSide  . toUpperCase();

               return bFlag;
          }
          return false;
     }
}
