package enquiry;

import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class EnquiryFormDesign
{
     Vector         VCode,VName,VUOM,VQty;
     Vector         VDesc,VMake,VCatl,VDraw,VPColor,VPSet,VPSize,VPSide;
     String         SAddr1  = "",SAddr2    = "",SAddr3    = "",SEMail = "";

     FileWriter     FW;
     
     String         SEnqNo,SEnqDate,SSupCode,SSupName,SApproved;
     Common         common = new Common();
     
     int            Lctr      = 100;
     int            Pctr      = 0;
     int            iMillCode, iMailSenderStatus;
     String         SSupTable="";
     
     public EnquiryFormDesign(String SEnqNo,String SEnqDate,String SSupCode,int iMillCode,String SEMail,String SApproved,String SSupTable)
     {
          this.SEnqNo     = SEnqNo;
          this.SEnqDate   = SEnqDate;
          this.SSupCode   = SSupCode;
          this.iMillCode  = iMillCode;
          this.SEMail     = SEMail;
          this.SApproved  = SApproved;
          this.SSupTable  = SSupTable;

          setDataIntoVector();
          getHTML();
     }

     public void getHTML()
     {
          try
          {
		   int imailSend    = -1;
               Vector VOrdDetails    = getEnquiryDetails(SEnqNo,SSupCode);

               FileWriter FTxt = new FileWriter(common.getMailPath()+SEnqNo+"_"+SSupCode+".html");
                          FTxt . write(" <html><body><PRE> ");

                          String str1=" <html><body><div align='justified'>Dear Sir/madam, <br>"+
                                      "      You have received an enquiry from Amarjothi Spinning Mills.,Nambiyur."+
                                      " <font color=red><h3>Use below links (Click any one of below links)</h3></font> and update your price details immediately.This link will expired "+
                                      " within 2 days. <br><br></div> ";

                          String str2=" Link-1 : <a href='http://www.melangeonline.net:8070/StoreEnquiry/EnquiryForm.jsp?TEnqNo="+SEnqNo+"&SupCode="+SSupCode+"&Type=0'><h1>Click Here</h1></a>\n\n\n<br><br><br>"+
                                      " Link-2 (Use, if Link-1 not works) : <a href='http://www.amarjothi.in:8070/StoreEnquiry/EnquiryForm.jsp?TEnqNo="+SEnqNo+"&SupCode="+SSupCode+"&Type=0'><h1>Click Here</h1></a>\n\n\n<br><br><br>";

                                 str2  += "<div align='justified'> ";
                                 str2  += " Note : <Br>";
                                 str2  += " 1. Diffentiate your price for cash & credit purchase. <Br>";
                                 str2  += " 2. Please send Samples (if any) along with your Quotation. <Br>";
                                 str2  += " 3. The quotation shall reach us, within 7 Days from the date of this Enquiry. <Br>";
                                 str2  += " 4. Please enclose price list and catalogue if any along with your quotation. <Br>";
                                 str2  += " 5. Instruments to avail concessional rate of tax (Form XVII etc.,) <Br>";
                                 str2  += " 6. Mention P & F Charges and Freight,if any, seperatly. <Br>";
                                 str2  += " 7. Fill your price in this enquiry and send back the same. <Br>";

                                 str2  += "</div> ";   

                                 str2  += "<br><h1><font color=blue>Enquiry No : "+SEnqNo+"</font></h1><br><br><legend><font color=green><b>Item Details</b></font></legend><br><table width='100%' border='0' cellpadding='1' cellspacing='1' class='td-black-txt'>";
                                 str2  += "<tr bgcolor='lightslategray' style='font-family: Comic sans Ms; font-size:14; font-weight:bold;'>";
                                 str2  += "<td>MatCode</td>";
                                 str2  += "<td>Description Make - Cataloge - Drawing Number</td>";
                                 str2  += "<td>UOM</td>";
                                 str2  += "<td>Quantity</td>";
                                 str2  += "</tr>";

                            HashMap tOrdObj = null;

                            for(int i=0;i<VOrdDetails.size();i++)
                            {
                                 tOrdObj = ((HashMap)VOrdDetails.elementAt(i));

                                 if(i%2==0)
                                 {
                                   
                                         str2 += "<tr bgcolor='#eaeaea' style='font-family: Comic sans Ms; font-size:12;'>";
                                         str2 += "<td><font color=blue><b>"+(String)tOrdObj.get("ITEM_CODE")+"</b></font></td>";
                                         str2 += "<td>"+(String)tOrdObj.get("ITEM_NAME")+"</td>";
                                         str2 += "<td>"+(String)tOrdObj.get("UOMNAME")+"</td>";
                                         str2 += "<td>"+(String)tOrdObj.get("QTY")+"</td>";
                                         str2 += "</tr>";
                                 }
                                 else
                                 {
                                         str2 += "<tr bgcolor='#B9D3EE' style='font-family: Comic sans Ms; font-size:12;'>";
                                         str2 += "<td><font color=blue><b>"+(String)tOrdObj.get("ITEM_CODE")+"</b></font></td>";
                                         str2 += "<td>"+(String)tOrdObj.get("ITEM_NAME")+"</td>";
                                         str2 += "<td>"+(String)tOrdObj.get("UOMNAME")+"</td>";
                                         str2 += "<td>"+(String)tOrdObj.get("QTY")+"</td>";
                                         str2 += "</tr>";
                                 }
                            }

                                 str2 += "</table>";

                                 str2 += " <br><br>Thanks & Regards \n\n<br><br>";
                                 str2 += " SudhalaiMuthu. \n<br>";
                                 str2 += " Stores Officer.<br><br><br> ";



                                   str2 +=" <div align='justified'>\n\nDisclaimer : \n<br>";
                                   str2 +=" Computer generated statment.\n<br>";
                                   str2 +=" This message contains confidential information and is intended only for the individual named.";
                                   str2 +=" Please notify the sender immediately by e-mail if you have received this e-mail by mistake";
                                   str2 +=" E-mail transmission cannot be guaranteed to be secure or error-free as information could";
                                   str2 +=" be  corrupted, lost, destroyed, arrive late or incomplete. The sender therefore does not";
                                   str2 +=" accept liability for any errors or omissions in the contents of this message, If verification";
                                   str2 +=" is required please request a hard-copy version to our collection department (crm@amarjothi.net)";
                                   str2 +=" ,accounts department (purchase@amarjothi.net) or contact your amarjothi collection representative";


                                   String SStatusId="Enq : "+SEnqNo+"";
                                   String SCollId="";
                                   str2 += " <br><hr> ";
                                   str2 += " You have received this email because you subscribed to email alert notifications from Amarjothi. If you do not wish to receive email alerts from Amarjothi,";
                                   str2 += " <a href='http://www.melangeonline.net:8070/StoreEnquiry/MailUnSubscribe.jsp?EMail="+SEMail+"&SupCode="+SSupCode+"&StatusId="+SStatusId+"&CollId="+SCollId+" '><font color=blue>Unsubscribe</font></a> ";


                                   

                          String str3= " </div></body></html> ";

                          FTxt . write(str1+str2+str3);
                          FTxt . close();
  

                          String sCc=SEMail;
                          String sToMail=SEMail;


                         String sSubject="Amarjothi Enquiry Form";
                         String SSupCodeNew = getSupCode(SSupCode);


                         String sText = str1+str2+str3;

                         String   sAttachment= common.getMailPath()+SEnqNo+"_"+SSupCode+".html";

                         int iIndex = SEMail.indexOf("@");

                         String sAttachment2="";
                         String SFromMailId="purchase@amarjothi.net";

//                         sToMail = sToMail + ".readnotify.com";

                         if(iIndex != -1)
                         {
                              if(iMailSenderStatus == 0)
                              {
                                   imailSend = HTMLMailSender.createMail(sCc,  sToMail,  sSubject,  sText,  sAttachment2,SFromMailId);
                              }
                              else
                              {
                                   imailSend = SMTPHTMLMailSender.createMail(sCc,  sToMail,  sSubject,  sText,  sAttachment2,SFromMailId);
                              }

                              if(imailSend==0)
                                   updateMail(SEnqNo,SSupCode);
                              else
                                   return; 
                         }

                         String SMessage = " "+sToMail+" ";
                         insertSMS(SSupCode,SMessage);

          }
          catch(Exception e)
          {
               System.out.println(e);
          }
     }

     public void setDataIntoVector()
     {
          VCode      = new Vector();
          VName      = new Vector();
          VUOM       = new Vector();
          VQty       = new Vector();
          VDesc      = new Vector();  
          VCatl      = new Vector();
          VDraw      = new Vector();
          VMake      = new Vector();
          VPColor    = new Vector();
          VPSet      = new Vector();
          VPSize     = new Vector();
          VPSide     = new Vector();

          iMailSenderStatus   = 0;
          
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(getQString());
               while (res.next())
               {
                    String    str1      = res.getString(1);  
                    String    str2      = res.getString(2);
                    String    str3      = res.getString(3);
                    String    str4      = res.getString(4);
                                   
                              VCode     . addElement(str1);
                              VName     . addElement(str2);
                              VQty      . addElement(str3);
                              VUOM      . addElement(str4);
                              VDesc     . addElement(common.parseNull(res.getString(5)));
                              VCatl     . addElement(common.parseNull(res.getString(6)));      
                              VDraw     . addElement(common.parseNull(res.getString(7)));      
                              VMake     . addElement(common.parseNull(res.getString(8)));
                              VPColor   . addElement(common.parseNull(res.getString(9)));
                              VPSet     . addElement(common.parseNull(res.getString(10)));
                              VPSize    . addElement(common.parseNull(res.getString(11)));
                              VPSide    . addElement(common.parseNull(res.getString(12)));
               }
               res  . close();

               String QS      = "Select Status from SCM.MailSenderStatus ";

               ResultSet rs   = stat.executeQuery(QS);
               if(rs.next())
               {
                    iMailSenderStatus   = common.toInt(rs.getString(1));
               }
               rs.close();

               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public String getQString()
     {
          String QString  = "";
          
          QString = " SELECT Enquiry.Item_Code, InvItems.Item_Name, Enquiry.Qty, "+
                    " UOM.UoMName,Enquiry.Descr,InvItems.Catl,InvItems.Draw,Enquiry.Make,"+
                    " InvItems.PAPERCOLOR,PaperSet.SETNAME,InvItems.PAPERSIZE,PaperSide.SIDENAME,Enquiry.EnqNo "+
                    " FROM Enquiry INNER JOIN InvItems ON InvItems.Item_Code = Enquiry.Item_Code "+
                    " inner join paperset on paperset.setcode = invitems.papersets inner join paperside on paperside.sidecode = invitems.paperside"+
                    " Inner join UOM on Uom.uomcode = InvItems.uomcode Where Enquiry.EnqNo = "+SEnqNo+" And Enquiry.Sup_Code = '"+SSupCode+"'";
          return QString;
     }

      public String getQString(String SEnqNo,String SSupCode)
     {
          String QString  = "";
          QString = " SELECT Enquiry.Item_Code,  concat(concat(concat(InvItems.Item_Name,concat(' - ',Enquiry.Descr)),concat(' - ',Enquiry.MAKE)) ,concat(concat(' - ',InvItems.Catl),concat(' - ',InvItems.DRAW))) as Item_Name, Enquiry.Qty, "+
                    " UOM.UoMName,Enquiry.Descr,InvItems.Catl,InvItems.Draw,Enquiry.Make,"+
                    " InvItems.PAPERCOLOR,PaperSet.SETNAME,InvItems.PAPERSIZE,PaperSide.SIDENAME,Enquiry.EnqNo,Enquiry.Id, "+
                    " nvl(DiscPer,0) as DiscPer,nvl(DiscVal,0)  as DiscVal,"+
                    " nvl(CenvatPer,0) as CenvatPer,nvl(CenvatVal,0)  as CenvatVal,"+
                    " nvl(TaxPer,0) as TaxPer,nvl(TaxVal,0)  as TaxVal,"+
                    " nvl(CstPer,0) as CstPer,nvl(CstVal,0)  as CstVal,"+
                    " nvl(SurPer,0) as SurPer,nvl(SurVal,0)  as SurVal,"+
                    " nvl(Rate,0) as rate,nvl(NetRate,0)  as NetRate,nvl(TotalValue,0) as TotalValue, "+
                    " Remarks,FreightDesc,InsuranceDesc,LandingCostDesc,PackingCostDesc,OthersDesc,CreditDays,ExpDeliveryDate,ExpDays,Approved "+
                    " FROM Enquiry INNER JOIN InvItems ON InvItems.Item_Code = Enquiry.Item_Code "+
                    " inner join paperset on paperset.setcode = invitems.papersets inner join paperside on paperside.sidecode = invitems.paperside"+
                    " Inner join UOM on Uom.uomcode = InvItems.uomcode Where Enquiry.EnqNo = "+SEnqNo+" And Enquiry.Sup_Code = '"+SSupCode+"' and Approved=0 "; //and MailStatus=0
                   

          System.out.println("QS : "+QString);
          return QString;
     }

     public Vector getEnquiryDetails(String SEnqNo,String SSupCode)
     {
          Vector theVector = new Vector();

        try {



               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
                PreparedStatement pstmt = theConnection.prepareStatement(getQString(SEnqNo,SSupCode));
                ResultSet result = pstmt.executeQuery();
               ResultSetMetaData rsmd    = result.getMetaData();

               while(result.next())
               {
                    HashMap row = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }
                    theVector.addElement(row);
               }
               result.close();
               pstmt.close();
          }
          catch(Exception ex)
          {
               System.out.println("Enquiry Details"+ex);
          }
          return theVector;
     }


     public void getAddr()
     {
          String QS =    "Select "+SSupTable+".Addr1,"+SSupTable+".Addr2,"+SSupTable+".Addr3,place.placename,"+SSupTable+".Name "+
                         "From "+SSupTable+" LEFT Join place On place.placecode = "+SSupTable+".City_Code "+
                         "Where "+SSupTable+".Ac_Code = '"+SSupCode+"'";
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(QS);

               while (res.next())
               {
                    SAddr1   = res.getString(1);
                    SAddr2   = res.getString(2);
                    SAddr3   = common.parseNull(res.getString(3));
                    SSupName = res.getString(5);
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public Vector getEnquiryDetails()
     {
          Vector theVector = new Vector();
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnect    =  oraConnection.getConnection();               
               Statement      theStatement  = theConnect.createStatement();
               ResultSet result          = theStatement.executeQuery(getQString());
               ResultSetMetaData rsmd    = result.getMetaData();
               
               while(result.next())
               {
                    HashMap row = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }                         
                    theVector.addElement(row);
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("Auto Invoice Details"+ex);
          }
          return theVector;
     }
     private void updateMail(String SEnqNo,String SSupCode)
     {
            try
            {
                    String QS = " Update Enquiry set MailStatus=1 Where EnqNo='"+SEnqNo+"' and Sup_Code='"+SSupCode+"' ";
        
                    ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
                    Connection     theConnection  =  oraConnection.getConnection();               
                    Statement      stat           =  theConnection.createStatement();
                                   stat.execute(QS);
            }
            catch(Exception e)
            {
                System.out.println(e);
            }

     }
     private void insertSMS(String SCustomerCode,String SMessage)
     {
            try
            {
                    String QS = " insert into Tbl_Alert_Collection (CollectionId,StatusId,AlertMessage,CustomerCode,Status,SMSType) Values(alertCollection_seq.nextval,'STATUS10','"+SMessage+"','"+SCustomerCode+"',0,1) ";
        
                    System.out.println(QS);
                    ORAConnection3  oraConnection  =  ORAConnection3.getORAConnection();
                    Connection     theConnection  =  oraConnection.getConnection();               
                    Statement      stat           =  theConnection.createStatement();
                                   stat.execute(QS);
            }
            catch(Exception e)
            {
                System.out.println(e);
            }

     }
     private String getSupCode(String SCode)
     {
          String Str="";

          Str =  SCode.replace(" ","%20");

          return Str;
     }


}

