package enquiry;

import javax.swing.*;
import javax.swing.table.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class EnquiryFrame extends JInternalFrame
{
     String         SStDate,SEnDate;
     JComboBox      JCOrder,JCFilter,JCFilter1;
     JButton        BApply,BEnquiry,BRateChart,BAutoOrderConversion;
     JPanel         TopPanel;
     JPanel         BottomPanel;
     
     TabReport      tabreport;
     DateField      TStDate;
     DateField      TEnDate;
     
     Object         RowData[][];

     String         ColumnData[] = {"Date","MRS No","Code","Name","Qty","Unit","Department","Group","Status","Click For Enquiry/RateChart","Mrs Type","OrderConversion","RCSupName","RCDate","Rate","PrevSup","PrevOrderDate","PrevRate"};
     String         ColumnType[] = {"S","N","S","S","N","S","S","S","S","B","S","B","S","S","S","S","S","S"};
     
     Common         common = new Common();

     Vector         VDate,VMrsNo,VRCode,VRName,VQty,VDept,VGroup,VStatus,VUnit,VDue,VMrsSlNo,VMrsType,VRCDate,VRCRate,VRCSupplier,VOrderStatus;
     Vector         VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedUnit,VSelectedDept,VSelectedGroup,VSelectedMrsSlNo;
     Vector VGSeleCode,VGSeleMrsNo,VGSeleMrsSlNo;
     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     Vector         VCode,VName;
     int            iMillCode,iUserCode;
     String         SItemTable,SSupTable;
     RateChartDialog       ratechart;

     public EnquiryFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iMillCode,int iUserCode,String SItemTable,String SSupTable)
     {
          super("Enquiry Against MRSs Placed During a Period");
          this.DeskTop        = DeskTop;
          this.SPanel         = SPanel;
          this.VCode          = VCode;
          this.VName          = VName;
          this.iMillCode      = iMillCode;
          this.iUserCode      = iUserCode;
          this.SItemTable     = SItemTable;
          this.SSupTable      = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TStDate        = new DateField();
          TEnDate        = new DateField();
          BApply         = new JButton("Apply");
          TopPanel       = new JPanel();
          JCOrder        = new JComboBox();
          JCFilter       = new JComboBox();
          JCFilter1      = new JComboBox();
          BottomPanel    = new JPanel();
          BEnquiry       = new JButton("Place Enquiry");
          BRateChart     = new JButton("Rate Chart");
          BAutoOrderConversion= new JButton("Auto Order Conversion");
          
          TStDate        . setTodayDate();
          TEnDate        . setTodayDate();
     }

     public void setLayouts()
     {
          TopPanel.setLayout(new GridLayout(2,5));
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          JCOrder             . addItem("Materialwise");
          JCOrder             . addItem("Groupwise");
          JCOrder             . addItem("Departmentwise");
          JCOrder             . addItem("MRS No");
          JCOrder             . addItem("SupplierWise");
     
          JCFilter            . addItem("All");
          JCFilter            . addItem("Over-Due");
     
          JCFilter1           . addItem("All");
          JCFilter1           . addItem("Regular");
          JCFilter1           . addItem("Monthly");
          JCFilter1           . addItem("Yearly");

          TopPanel            . add(new JLabel("Sorted On"));
          TopPanel            . add(JCOrder);
          TopPanel            . add(new JLabel("Period"));
          TopPanel            . add(TStDate);
          TopPanel            . add(TEnDate);
          TopPanel            . add(new JLabel("List Only"));
          TopPanel            . add(JCFilter);
          TopPanel            . add(new JLabel("Mrs Type"));
          TopPanel            . add(JCFilter1);
          TopPanel            . add(BApply);
     
          BottomPanel         . add(BEnquiry);
          BottomPanel         . add(BRateChart);
          BottomPanel         . add(BAutoOrderConversion);

          BEnquiry            . setEnabled(false);
          


          getContentPane()    . add(TopPanel,BorderLayout.NORTH);
          getContentPane()    . add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply              . addActionListener(new ApplyList());
          BEnquiry            . addActionListener(new ActList());
          BRateChart          . addActionListener(new ActList());
          BAutoOrderConversion. addActionListener(new ActList());
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport       = new TabReport(RowData,ColumnData,ColumnType);
                    getContentPane(). add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    DeskTop   . repaint();
                    DeskTop   . updateUI();
                    BEnquiry  . setEnabled(true);
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setSelectedRecords();
               try
               {
                    if(ae.getSource()==BEnquiry)
                    {
                         EnquiryCollectionFrame enquirycollectionframe = new EnquiryCollectionFrame(DeskTop,VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedMrsSlNo,SPanel,iMillCode,iUserCode,SItemTable,SSupTable,VGSeleCode,VGSeleMrsNo,VGSeleMrsSlNo);
                         DeskTop                  . add(enquirycollectionframe);
                         DeskTop                  . repaint();
                         enquirycollectionframe   . setSelected(true);
                         DeskTop                  . updateUI();
                         removeHelpFrame();
                         enquirycollectionframe   . show();
                    }

                    if(ae.getSource()==BRateChart)
                    {
                         try
                         {
                              ratechart = new RateChartDialog(DeskTop,1,tabreport);
                              DeskTop.add(ratechart);
                              DeskTop.repaint();
                              ratechart .setSelected(true);
                              DeskTop.updateUI();
                              ratechart.BSave.addActionListener(new ActList2());
                              ratechart.show();

                         }
                         catch(Exception ex)
                         {
                              System.out.println(ex);
                              ex.printStackTrace();
                         }
                    }

                    if(ae.getSource()==BAutoOrderConversion)
                    {
                         updateMrs();
                    }
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
                    ex.printStackTrace();
               }
          }
     }

     public void setDataIntoVector()
     {
          VDate          = new Vector();
          VMrsNo         = new Vector();
          VRCode         = new Vector();
          VRName         = new Vector();
          VQty           = new Vector();
          VDept          = new Vector();
          VGroup         = new Vector();
          VStatus        = new Vector();
          VDue           = new Vector();
          VUnit          = new Vector();
          VMrsSlNo       = new Vector();
		VMrsType		= new Vector();
          VRCDate        = new Vector();
          VRCRate        = new Vector();
          VRCSupplier    = new Vector();
          VOrderStatus   = new Vector();
          
          SStDate        = TStDate.toString();
          SEnDate        = TEnDate.toString();
          String StDate  = TStDate.toNormal();
          String EnDate  = TEnDate.toNormal();


          String QSTemp   = " Create table TempPendMrsOrder1 as("+
                        " Select max(OrderDate) as OrderDate,Item_Code,OrderStatusCode from("+
                        " select  max(OrderDate) as OrderDate,PurchaseOrder.Item_Code,1 as OrderStatusCode"+
                        " From PurchaseOrder "+
                        " Inner join Mrs on Mrs.Item_Code=PurchaseOrder.Item_Code"+
                        " and Mrs.OrderNo=0 and Mrs.AutoOrderConversion=0"+
                        " Left Join ComparsionEnquiry on ComparsionEnquiry.Item_code=PurchaseOrder.Item_Code"+
                        " and ComparsionEnquiry.Type=3 and ComparsionEnquiry.JmdOrderApproval=1"+
                        " where ComparsionEnquiry.Item_Code is null"+
                        " Group by Purchaseorder.Item_Code"+
/*                      " Union all"+
                        " select  max(OrderDate) as OrderDate,PYOrder.Item_Code,1 as OrderStatusCode"+
                        " From PYOrder "+
                        " Inner join Mrs on Mrs.Item_Code=PYOrder.Item_Code"+
                        " and Mrs.OrderNo=0 and Mrs.AutoOrderConversion=0"+
                        " Left Join ComparsionEnquiry on ComparsionEnquiry.Item_code=PYOrder.Item_Code"+
                        " and ComparsionEnquiry.Type=3 and ComparsionEnquiry.JmdOrderApproval=1"+
                        " where ComparsionEnquiry.Item_Code is null"+
                        " Group by PYOrder.Item_Code "+*/
                        " Union all"+
                        " select  to_Char(max(ComparsionDate)) as OrderDate,ComparsionEnquiry.Item_Code,3 as OrderStatusCode"+
                        " From  ComparsionEnquiry "+
                        " Inner join Mrs on Mrs.Item_Code=ComparsionEnquiry.Item_Code"+
                        " and Mrs.OrderNo=0 and Mrs.AutoOrderConversion=0"+
                        " where ComparsionEnquiry.Type=3 and (ComparsionEnquiry.JmdOrderApproval=1 or ComparsionEnquiry.RateChartEntryStatus=1 )"+     
                        " Group by ComparsionEnquiry.Item_Code) group by Item_Code,OrderStatusCode) ";


          String QSTemp1 ="   create table TempPendMrsOrder11 as("+
                   "  select max(Id) as Id,OrderDate,Item_Code,OrderStatusCOde from("+
/*                 "  select max(PYOrder.ID) as Id,TempPendMrsOrder1.OrderDate as OrderDate,TempPendMrsOrder1.Item_Code as Item_code,TempPendMrsOrder1.OrderStatusCode from PYOrder"+
                   "  Inner join TempPendMrsOrder1 on TempPendMrsOrder1.OrderDate=PYOrder.OrderDate"+
                   "  and TempPendMrsOrder1.Item_Code=PYOrder.Item_code and TempPendMrsOrder1.OrderStatusCode=1"+
                   "  group by TempPendMrsOrder1.Orderdate,TempPendMrsOrder1.Item_Code,TempPendMrsOrder1.OrderStatusCode"+
                   "  union All"+*/
                   "  select Max(Purchaseorder.Id) as Id,TempPendMrsOrder1.OrderDate,TempPendMrsOrder1.Item_Code as Item_Code,TempPendMrsOrder1.OrderStatusCode from PurchaseOrder"+
                   "  Inner join TempPendMrsOrder1 on  TempPendMrsOrder1.OrderDate=PurchaseOrder.OrderDate"+
                   "  and TempPendMrsOrder1.Item_Code=PurchaseOrder.Item_code  and TempPendMrsOrder1.OrderStatusCode=1"+
                   "  group by TempPendMrsOrder1.Orderdate,TempPendMrsOrder1.Item_Code,TempPendMrsOrder1.OrderStatusCode "+
                   "  union All"+
                   "  select Max(ComparsionEnquiry.ComparsionId) as Id,TempPendMRsOrder1.OrderDate,TempPendMrsOrder1.Item_Code as Item_Code,TempPendMrsOrder1.OrderStatusCode from ComparsionEnquiry"+
                   "  Inner join TempPendMrsOrder1 on  TempPendMRsOrder1.OrderDate=ComparsionEnquiry.ComparsionDate"+
                   "  and TempPendMrsOrder1.Item_Code=ComparsionEnquiry.Item_code  and TempPendMrsOrder1.OrderStatusCode=2"+
                   "  group by TempPendMrsOrder1.Orderdate,TempPendMRsOrder1.Item_Code,TempPendMrsOrder1.OrderStatusCode) group by OrderDate,Item_Code,OrderStatusCode) ";


          String QSTemp2= " create table Temp1PendMRSOrder as ("+
                     " Select OrderDate,Item_Code,"+
                     " Name,Rate,DiscPer, "+
                     " CenVatPer,TaxPer,SurPer, "+
                     " NetRate,OrderSource,OrderStatus,OrderStatusCode,decode(OrderStatusCode,2,getSupplier(Item_Code),'') as PrevDet"+
                     " from ( "+
/*                    " Select to_number(PYOrder.OrderDate) as OrderDate,PYOrder.Item_Code, "+
                     " Supplier.Name,PYOrder.Rate,PYOrder.DiscPer, "+
                     " PYOrder.CenVatPer,PYOrder.TaxPer,PYOrder.SurPer, "+
                     " PYOrder.Net/PYOrder.Qty as NetRate,'PrevOrder' as OrderSource,1 as OrderStatus,TempPendMRsOrder11.OrderStatusCode "+
                     " From PYOrder Inner Join Supplier On Supplier.Ac_Code = PyOrder.Sup_Code "+
                     " Inner join TempPendMrsOrder11 on TempPendMrsOrder11.OrderDate= PYOrder.Orderdate"+
                     " and TempPendMrsOrder11.Item_Code=PYOrder.Item_Code and TempPendMrsOrder11.Id=PYOrder.Id and TempPendMrsOrder11.OrderStatusCode=1 "+
                     " Where PYOrder.Qty > 0 And PYOrder.Net > 0 "+
                     " Union All "+ */
                     " Select to_number(PurchaseOrder.OrderDate) as OrderDate,PurchaseOrder.Item_Code, "+
                     " Supplier.Name,PurchaseOrder.Rate,PurchaseOrder.DiscPer, "+
                     " PurchaseOrder.CenVatPer,PurchaseOrder.TaxPer,PurchaseOrder.SurPer, " +
                     " PurchaseOrder.Net/PurchaseOrder.Qty as NetRate,'PrevOrder' as OrderSource,1 as OrderStatus ,TempPendMrsOrder11.OrderStatusCode"+
                     " From PurchaseOrder Inner Join Supplier On Supplier.Ac_Code = PurchaseOrder.Sup_Code "+
                     " Inner join TempPendMrsOrder11 on TempPendMrsOrder11.OrderDate= PurchaseOrder.Orderdate"+
                     " and TempPendMrsOrder11.Item_Code=PurchaseOrder.Item_Code and TempPendMrsOrder11.Id=PurchaseOrder.Id and TempPendMrsOrder11.OrderStatusCode=1 "+
                     " Where PurchaseOrder.Qty > 0 And PurchaseOrder.Net > 0 "+
                     " Union All"+
                     " select max(ComparsionDate) as OrderDate,ComparsionEnquiry.Item_Code,"+
                     " SUpplier.Name,ComparsionEnquiry.Rate,ComparsionEnquiry.DiscPer,"+
                     " ComparsionEnquiry.CenVatPer,ComparsionEnquiry.TaxPer,ComparsionEnquiry.SurPer,TotalValue as NetRate,'RateChart' as OrderSource,decode(jmdOrderApproval,0,3,2) as OrderStatus,TempPendMrsOrder11.OrderStatusCode"+ //
                     " from ComparsionEnquiry "+
                     " Inner join Supplier on Supplier.AC_Code=ComparsionEnquiry.Sup_Code"+
                     " Inner join TempPendMRSOrder11 on TempPendMRSOrder11.OrderDate= ComparsionEnquiry.Comparsiondate"+
                     " and TempPendMRSOrder11.Item_Code=ComparsionEnquiry.Item_Code and TempPendMRSOrder11.Id=ComparsionEnquiry.ComparsionId and TempPendMrsOrder11.OrderStatusCode=2 "+
                     " where "+
                     " ComparsionEnquiry.Type=3 "+
                     " and (ComparsionEnquiry.JmdOrderApproval=1 or (ComparsionEnquiry.JmdOrderApproval=0 and RateChartEntryStatus=1)) "+
                     " group by ComparsionEnquiry.Item_Code, "+
                     " SUpplier.Name,ComparsionEnquiry.Rate,ComparsionEnquiry.DiscPer, "+
                     " ComparsionEnquiry.CenVatPer,ComparsionEnquiry.TaxPer,ComparsionEnquiry.SurPer,TotalValue,decode(jmdOrderApproval,0,3,2),TempPendMRsOrder11.OrderStatusCode))";


          String QString = getQString(StDate,EnDate);
          
          try
          {
               ORAConnection  oraConnection  =  ORAConnection    . getORAConnection();
               Connection     theConnection  =  oraConnection    . getConnection();               
               Statement      stat           =  theConnection    . createStatement();

               try{stat.execute("Drop Table TempPendMRSOrder1");}catch(Exception ex){}
               try{stat.execute("Drop Table TempPendMRSOrder11");}catch(Exception ex){}
               try{stat.execute("Drop Table Temp1PendMRSOrder");}catch(Exception ex){}



               stat.execute(QSTemp);
               stat.execute(QSTemp1);
               stat.execute(QSTemp2);


               stat.execute(QString);
               
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    String str1    = res     . getString(1);
                    String str2    = res     . getString(2);
                    String str3    = res     . getString(3);
                    String str4    = res     . getString(4);
                    String str5    = res     . getString(5);
                    String str6    = res     . getString(6);
                    String str7    = res     . getString(7);
                    String str8    = res     . getString(8);
                    String str9    = res     . getString(9);
                    str2           = common  . parseDate(str2);
                    str8           = common  . parseDate(str8);

                    String str10    = res     . getString(13);
                    String str11    = res     . getString(14);
                    String str12    = res     . getString(15);

                    int iStatusCode = res     . getInt(16);


				String SMrsType="";

				String SYearPlan  = common.parseNull((String)res.getString(11));
				String SMonthPlan = common.parseNull((String)res.getString(12));

				if(SYearPlan.equals("1"))
				{
					SMrsType = "Yearly";
				}
				else
				if(SMonthPlan.equals("0"))
				{
					SMrsType = "Regular";
				}
				else
				{
					SMrsType = "Monthly";
				}

                    VMrsNo         . addElement(str1);
                    VDate          . addElement(str2);
                    VRCode         . addElement(str3);
                    VRName         . addElement(str4);
                    VQty           . addElement(str5);
                    VDept          . addElement(str6);
                    VGroup         . addElement(str7);
                    VDue           . addElement(str8);
                    VUnit          . addElement(str9);
                    VMrsSlNo       . addElement(common.parseNull((String)res.getString(10)));
                    VStatus        . addElement(" ");
				VMrsType		. addElement(SMrsType);
                    VRCDate        . addElement(str10);
                    VRCRate        . addElement(str11);
                    VRCSupplier    . addElement(str12);

                    VOrderStatus   . addElement(String.valueOf(iStatusCode));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System    . out.println(ex);
               ex        . printStackTrace();
          }
     }

     public void setRowData()
     {
          RowData     = new Object[VDate.size()][18];
          for(int i=0;i<VDate.size();i++)
          {
               RowData[i][0]  = (String)VDate      . elementAt(i);
               RowData[i][1]  = (String)VMrsNo     . elementAt(i);
               RowData[i][2]  = (String)VRCode     . elementAt(i);
               RowData[i][3]  = (String)VRName     . elementAt(i);
               RowData[i][4]  = (String)VQty       . elementAt(i);
               RowData[i][5]  = (String)VUnit      . elementAt(i);
               RowData[i][6]  = (String)VDept      . elementAt(i);
               RowData[i][7]  = (String)VGroup     . elementAt(i);
               RowData[i][8]  = (String)VStatus    . elementAt(i);
//               RowData[i][9]  = (String)VDue       . elementAt(i);
               RowData[i][9] = new Boolean(false);
               RowData[i][10] = (String)VMrsType   . elementAt(i);
               RowData[i][11] = new Boolean(false);
               if(common.toInt((String)VOrderStatus.elementAt(i))==2)
               {
                    RowData[i][12] = common.parseNull((String)VRCSupplier  .elementAt(i));
                    RowData[i][13] = common.parseDate((String)VRCDate      .elementAt(i));
                    RowData[i][14] = common.parseDate((String)VRCRate      .elementAt(i));

                    RowData[i][15] = "";
                    RowData[i][16] = "";
                    RowData[i][17] = "";

               }
               else
               {

                    RowData[i][12] = "";
                    RowData[i][13] = "";
                    RowData[i][14] = "";

                    RowData[i][15] = common.parseNull((String)VRCSupplier  .elementAt(i));
                    RowData[i][16] = common.parseDate((String)VRCDate      .elementAt(i));
                    RowData[i][17] = common.parseDate((String)VRCRate      .elementAt(i));



               }

          }  
     }

     public String getQString(String StDate,String EnDate)
     {
          DateField df   = new DateField();
                    df   . setTodayDate();
          String SToday  = df.toNormal();
          String QString = "";

          QString = " SELECT MRS.MrsNo, MRS.MrsDate, MRS.Item_Code, InvItems.Item_Name ,sum(MRS.Qty), Dept.Dept_Name , Cata.Group_Name , 0 as DueDate, Unit.Unit_Name,Mrs.SlNo ,Mrs.YearlyPlanningStatus,0 as PlanMonth, "+ // MRS.DueDate 
                    " Temp1PendMrsOrder.OrderDate,Temp1PendMrsOrder.Rate ,Temp1PendMrsOrder.Name,Temp1PendMrsOrder.OrderStatusCode "+
                    " FROM (((MRS INNER JOIN InvItems ON MRS.Item_Code=InvItems.Item_Code) INNER JOIN Dept ON MRS.Dept_Code=Dept.Dept_code) INNER JOIN Cata ON MRS.Group_Code=Cata.Group_Code) INNER JOIN Unit ON MRS.Unit_Code=Unit.Unit_Code "+
                    " Left join Temp1PendMrsOrder on Temp1PendMrsOrder.Item_Code=Mrs.Item_Code"+
                    " Where (Mrs.MrsFlag=0 or Mrs.DeptMrsAuth=1) and Mrs.MrsDate >= '"+StDate+"' and Mrs.MrsDate <='"+EnDate+"' and Mrs.EnquiryNo = 0 and Mrs.OrderNo=0 and Mrs.MillCode="+iMillCode;

          if(JCFilter.getSelectedIndex()==1)
               QString = QString+" and Mrs.DueDate <= '"+SToday+"'";
          
          if(JCFilter1.getSelectedIndex()==1)
               QString = QString+" and ((Mrs.YearlyPlanningStatus=0 or Mrs.YearlyPlanningStatus is null)  and (Mrs.PlanMonth=0 or Mrs.PlanMonth is null) ) ";

          if(JCFilter1.getSelectedIndex()==2)
               QString = QString+" and Mrs.YearlyPlanningStatus=0 and Mrs.PlanMonth>0 ";

          if(JCFilter1.getSelectedIndex()==3)
               QString = QString+" and Mrs.YearlyPlanningStatus=1 and Mrs.PlanMonth=0 ";

          QString = QString+" Group by MRS.MrsNo, MRS.MrsDate, MRS.Item_Code, InvItems.Item_Name , Dept.Dept_Name , Cata.Group_Name , Unit.Unit_Name,Mrs.YearlyPlanningStatus,Mrs.SlNo, "+ // MRS.DueDate Mrs.SlNo
                    " Temp1PendMrsOrder.OrderDate ,Temp1PendMrsOrder.Rate,Temp1PendMrsOrder.Name,Temp1PendMrsOrder.OrderStatusCode ";



          if(JCOrder.getSelectedIndex() == 0)      
               QString = QString+" Order By InvItems.Item_Name,Mrs.MrsDate";
          if(JCOrder.getSelectedIndex() == 1)
               QString = QString+" Order By Cata.Group_Name,Mrs.MrsDate";
          if(JCOrder.getSelectedIndex() == 2)      
               QString = QString+" Order By Dept.Dept_Name,Mrs.MrsDate";
          if(JCOrder.getSelectedIndex() == 3)      
               QString = QString+" Order By Mrs.MrsNo,Mrs.MrsDate";
          if(JCOrder.getSelectedIndex() == 4)      
               QString = QString+" Order By Temp1PendMrsOrder.Name,Mrs.MrsNo,Mrs.MrsDate";

          return QString;
     }

     public void setSelectedRecords()
     {
          VSelectedCode     = new Vector();
          VSelectedName     = new Vector();
          VSelectedMRS      = new Vector();
          VSelectedQty      = new Vector();
          VSelectedUnit     = new Vector();
          VSelectedDept     = new Vector();
          VSelectedGroup    = new Vector();
          VSelectedMrsSlNo  = new Vector();

		VGSeleCode        = new Vector();
		VGSeleMrsNo       = new Vector();
		VGSeleMrsSlNo     = new Vector();
          
          for(int i=0;i<RowData.length;i++)
          {
               Boolean Bselected = (Boolean)RowData[i][9];   
               if(Bselected.booleanValue())
               {
				String SItemCode = (String)RowData[i][2];

				int iIndex = common.indexOf(VGSeleCode,SItemCode);

				if(iIndex>=0)
				{
					double dQty     = common.toDouble((String)RowData[i][4]);
					double dPrevQty = common.toDouble((String)VSelectedQty.elementAt(iIndex));
					double dNewQty  = dQty + dPrevQty;

					VSelectedQty.setElementAt(common.getRound(dNewQty,3),iIndex);
				}
				else
				{
	                    VSelectedCode      . addElement((String)RowData[i][2]);
	                    VSelectedName      . addElement((String)RowData[i][3]);

	                    //VSelectedMRS       . addElement((String)RowData[i][1]);

					VSelectedMRS       . addElement("");
	                    VSelectedQty       . addElement((String)RowData[i][4]);
	                    VSelectedUnit      . addElement((String)RowData[i][5]);
	                    VSelectedDept      . addElement((String)RowData[i][6]);
	                    VSelectedGroup     . addElement((String)RowData[i][7]);
	                    //VSelectedMrsSlNo   . addElement((String)VMrsSlNo.get(i));
					VSelectedMrsSlNo   . addElement("");

					VGSeleCode.addElement(SItemCode);
				}
				VGSeleMrsNo.addElement((String)RowData[i][1]);
				VGSeleMrsSlNo.addElement((String)VMrsSlNo.get(i));
               }   
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop   . remove(this);
               DeskTop   . repaint();
               DeskTop   . updateUI();
          }
               catch(Exception ex) { }
     }
     private class ActList2 implements ActionListener
     {
          public ActList2()
          {
          }
          public void actionPerformed(ActionEvent ae)
          {

               ratechart.BSave.setEnabled(false);


                    Connection theConnection = null;

                    if(!isValidData())
                    {
                       ratechart.BSave.setEnabled(true);

                         return;
                    }

                    try
                    {
                         Class.forName("oracle.jdbc.OracleDriver");
                         theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
     
                         for(int i=0; i<ratechart.theModel.getRows(); i++)
                         {
                              Boolean bFlag = (Boolean)ratechart.theModel.getValueAt(i,16);
     
                              double dNet   = common.toDouble((String)ratechart.theModel.getValueAt(i,14));

                              if(bFlag && dNet>0)
                              {
     
                                        theConnection.setAutoCommit(false);
     
                                        String SItemCode    =   (String)ratechart.theModel.getValueAt(i,0);
                                        String SItemName    =   (String)ratechart.theModel.getValueAt(i,1);
                                        String SSupName     =   (String)ratechart.theModel.getValueAt(i,2);
                                        String SQty         = "1";
                                        String SRate        =   (String)ratechart.theModel.getValueAt(i,3);
     
                                        String SDiscPer     =   (String)ratechart.theModel.getValueAt(i,4);
                                        String SDisc        =   (String)ratechart.theModel.getValueAt(i,5);
                                        String SCenVatPer   =   (String)ratechart.theModel.getValueAt(i,6);
                                        String SCenvat      =   (String)ratechart.theModel.getValueAt(i,7);
                                        String STaxPer      =   (String)ratechart.theModel.getValueAt(i,8);
                                        String STax         =   (String)ratechart.theModel.getValueAt(i,9);
                                        String SCstPer      =   (String)ratechart.theModel.getValueAt(i,10);
                                        String SCst         =   (String)ratechart.theModel.getValueAt(i,11);
                                  
                                        String SSurPer      =   (String)ratechart.theModel.getValueAt(i,12);
                                        String SSur         =   (String)ratechart.theModel.getValueAt(i,13);

                                        String SNet         =    (String)ratechart.theModel.getValueAt(i,14);
                                        String SDocId       =    (String)ratechart.theModel.getValueAt(i,15);
     
     
                                        PreparedStatement thePrepare = theConnection.prepareStatement(getQS());

                                        thePrepare.setString(1,"0");
                                        thePrepare.setString(2,"0");
                                        thePrepare.setString(3,"0");
                                        thePrepare.setString(4,SSupName);
                                        thePrepare.setString(5,SItemCode);
                                        thePrepare.setString(6,SQty);
                                        thePrepare.setString(7,SRate);
                                        thePrepare.setString(8,SDiscPer);
                                        thePrepare.setString(9,SCenVatPer);
                                        thePrepare.setString(10,STaxPer);
                                        thePrepare.setString(11,SSurPer);
                                        thePrepare.setString(12,SNet);
                                        thePrepare.setInt(13,iUserCode); 
                                        thePrepare.setString(14,SCstPer);
                                        thePrepare.setString(15,SCst);
                                        thePrepare.setString(16,SDisc);
                                        thePrepare.setString(17,SCenvat);
                                        thePrepare.setString(18,STax);
                                        thePrepare.setString(19,SSur);
                                        thePrepare.setString(20,"0");
                                        thePrepare.setString(21,SNet);
                                        thePrepare.setString(22,"0");      // ComprsionNo
                                        thePrepare.setInt(23,iUserCode);   // UserCode
                                        thePrepare.setString(24,common.getLocalHostName()); // UserCode
                                        thePrepare.setString(25,"3");
                                        thePrepare.setString(26,SDocId);

                                        thePrepare.executeUpdate();
                                        thePrepare.close();

                                        theConnection.commit();
                              }
                         }
                         theConnection.close();

                    }
                    catch(Exception ex)
                    {
                         try
                         {
                              ex.printStackTrace();
                              ratechart.BSave.setEnabled(true);

                              JOptionPane.showMessageDialog(null," Problem in Storing Data");
                              theConnection.rollback();
                              return;
                         }
                         catch(Exception e)
                         {
                              
                         }
                    }
                    JOptionPane.showMessageDialog(null," Direct Rate Chart Stored Successfully");
                    ratechart.BSave.setEnabled(true);
                    ratechart.removeFrame();
                    setDataIntoVector();
                    setRowData();
                    try
                    {
                         getContentPane().remove(tabreport);
                    }
                    catch(Exception ex){}
                    try
                    {
                         tabreport       = new TabReport(RowData,ColumnData,ColumnType);
                         getContentPane(). add(tabreport,BorderLayout.CENTER);
                         setSelected(true);
                         DeskTop   . repaint();
                         DeskTop   . updateUI();
                         BEnquiry  . setEnabled(true);
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    } 
          }
     }

     private String getQS()
     {
          StringBuffer   sb = new StringBuffer();

           sb.append(" insert into ComparsionEnquiry " );
           sb.append(" (ID,ENQNO,ENQDATE,SUP_CODE,ITEM_CODE,QTY,RATE,DISCPER,CENVATPER, ");
           sb.append(" TAXPER,SURPER,NETRATE, ");
           sb.append("  USERCODE,CREATIONDATE,");
           sb.append("  CSTPER,CSTVAL,DISCVAL,CENVATVAL,TAXVAL, ");
           sb.append("  SURVAL,REMARKS, ");
           sb.append("  TOTALVALUE, ");
           sb.append("  COMPARSIONID,COMPARSIONNO,COMPARSIONDATE, ");
           sb.append("  COMPARISONENTRYUSERCODE, COMPARISONENTRYDATETIME, COMPARISONENTRYIP,Type,DocId,RateChartEntryStatus,RateChartNo) Values ( ");
           sb.append("  ?,?,?,getSupCode(?),?,?,?,?,?,?,?,?,?,to_Char(Sysdate,'YYYYMMDD'),?,?,?,?,?,?,?,?,");
           sb.append("  ComparsionEnquiry_seq.nextval,?, to_Char(Sysdate,'YYYYMMDD'), ");
           sb.append("  ?, to_Char(SysDate, 'DD.MM.YYYY HH24:MI:SS'), ?,?,?,1,RateChartNo_Seq.nextval ) ");
        
          return sb.toString();
     }
     private boolean isValidData()
     {
          Boolean bValue=true;

          for(int i=0; i<ratechart.theModel.getRows(); i++)
          {
               Boolean bFlag = (Boolean)ratechart.theModel.getValueAt(i,16);
     
               double dNet    = common.toDouble((String)ratechart.theModel.getValueAt(i,14));
               String SSupName= (String)ratechart.theModel.getValueAt(i,2);
               String SDocId  = (String)ratechart.theModel.getValueAt(i,15);

               if(bFlag)
               {
                    if(dNet<=0 )
                    {
                         JOptionPane.showMessageDialog(null," NetValue is Not Valid in Row"+(i+1));
                         bValue=false;
                         break;
                    }
                    if(SSupName.length()<=0 )
                    {
                         JOptionPane.showMessageDialog(null," Invalid Supplier Name in Row"+(i+1));
                         bValue=false;
                         break;
                    }
                    if(common.toInt(SDocId)<=0 )
                    {
                         JOptionPane.showMessageDialog(null," Invalid DocId in Row"+(i+1));
                         bValue=false;
                         break;
                    }
               }
          }
          return bValue;
     }
     private void updateMrs()
     {

          try
          {
               ORAConnection  oraConnection  =  ORAConnection    . getORAConnection();
               Connection     theConnection  =  oraConnection    . getConnection();               
               PreparedStatement      thePrepare     =  theConnection    . prepareStatement(getUpdateQS());

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][11];
                    if(Bselected.booleanValue())
                    {

                         int iOrderSource = common.toInt((String)VOrderStatus.elementAt(i));

                         String SItemCode = (String)RowData[i][2];
                         String SMrsNo    = (String)RowData[i][1];

                         thePrepare.setInt(1,1);
                         thePrepare.setInt(2,iOrderSource);
                         thePrepare.setString(3,SMrsNo);
                         thePrepare.setString(4,SItemCode);

                         thePrepare.executeUpdate();
                    }
               }
               thePrepare.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private String getUpdateQS()
     {
          String QS =" Update Mrs set AutoOrderConversion=?,OrderSourceCode=? where MrsNo=? and Item_Code=? ";

          return QS;
     }

}
