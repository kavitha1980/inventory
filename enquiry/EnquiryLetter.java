package enquiry;

import java.io.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class EnquiryLetter
{
     Vector         VCode,VName,VUOM,VQty;
     Vector         VDesc,VMake,VCatl,VDraw,VPColor,VPSet,VPSize,VPSide;
     String         SAddr1    = "",SAddr2    = "",SAddr3    = "";
     FileWriter     FW;

     String         SEnqNo,SEnqDate,SSupCode,SSupName;
     Common         common = new Common();

     int            Lctr      = 100;
     int            Pctr      = 0;
     boolean        bHeadSig  = true;
     int            iMillCode;
     String         SItemTable,SSupTable,SMillName;
     
     EnquiryLetter(FileWriter FW,String SEnqNo,String SEnqDate,String SSupCode,int iMillCode,String SItemTable,String SSupTable,String SMillName)
     {
          this.FW         = FW;
          this.SEnqNo     = SEnqNo;
          this.SEnqDate   = SEnqDate;
          this.SSupCode   = SSupCode;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;

          getAddr();
          setDataIntoVector();
          setEnqHead();
          setEnqBody();
          setEnqFoot();
     }
     
     public void setEnqHead()
     {
          if(Lctr < 60)
               return;

          Pctr++;
          bHeadSig = true;
          String Strl1  = "No : "+common.Pad(SEnqNo,12)+common.Rad(SEnqDate,80-12);
          String Strl2  = common.Pad(SSupName,63);
          String Strl3  = common.Pad(SAddr1,63);
          String Strl4  = common.Pad(SAddr2,63);
          String Strl5  = common.Pad(SAddr3,63);
          String Strl6  = "Dear Sir, ";
          String Strl7  = "We give below our requirements for your reference.";
          String Strl8  = "|-----------------------------------------------------------------------------------------------------|";
          String Strl9  = "| Mat Code  | Specification of Goods         | Item Make       | Item Model | Drawing No |   Quantity |";
          String Strl10 = "|-----------------------------------------------------------------------------------------------------|";
          String Strl11 = "|           |                                |                 |            |            |            |";
          String Strl12 = "------------------------------------------------------------------------------------------------------|";
          
          try
          {
               if(Pctr <= 1)
               {
                    FW.write(Strl1+"\n\n\n\n");       
                    FW.write(Strl2+"\n");       
                    FW.write(Strl3+"\n");       
                    FW.write(Strl4+"\n");       
                    FW.write(Strl5+"\n\n\n");       
                    FW.write(Strl6+"\n\n");       
                    FW.write(Strl7+"\n\n");
                    Lctr = 15;
               }
               else
               {
                    FW.write(Strl12+"\n");
                    FW.write(common.Rad(String.valueOf(Pctr),80)+"\n\n");
                    Lctr = 2;
               }
               FW.write(Strl8+"\n");       
               FW.write(Strl9+"\n");       
               FW.write(Strl10+"\n");       
               Lctr = Lctr+3;
          }
          catch(Exception ex){}
     }
     
     public void setEnqBody()
     {
          String SPColor   = "";
          String SPSet     = "";
          String SPSize    = "";
          String SPSide    = "";

          String Strl11 = "|           |                                |                 |            |            |            |";
          String Strl="";
          for(int i=0;i<VCode.size();i++)
          {
               setEnqHead();
                         bHeadSig  = false;
               String    SCode     = (String)VCode.elementAt(i);
               String    SUOM      = (String)VUOM.elementAt(i);
               String    SName     = (String)VName.elementAt(i);
               String    SQty      = common.getRound((String)VQty.elementAt(i),2);
               String    SDesc     = ((String)VDesc.elementAt(i)).trim();
               String    SCatl     = ((String)VCatl.elementAt(i)).trim();
               String    SDraw     = ((String)VDraw.elementAt(i)).trim();
               String    SMake     = ((String)VMake.elementAt(i)).trim();

                         SPColor   = ((String)VPColor.elementAt(i)).trim();
                         SPSet     = ((String)VPSet.elementAt(i)).trim();
                         SPSize    = ((String)VPSize.elementAt(i)).trim();
                         SPSide    = ((String)VPSide.elementAt(i)).trim();

               String    xtr       = SName+" "+SDesc;
               Vector    vectDesc  = common.getLines(xtr);

               for(int j=0;j<vectDesc.size();j++)
               {
                    try
                    {
                         if(j==0)
                         {
                              Strl = "| "+common.Pad(SCode,9)+" | "+common.Pad((String)vectDesc.elementAt(j),30)+" | "+
                              common.Pad(SMake,15)+" | "+common.Pad(SCatl,10)+" | "+
                              common.Pad(SDraw,10)+" | "+
                              common.Rad(SQty,10)+" |";
                         }
                         else
                         {
                              Strl = "| "+common.Pad(" ",9)+" | "+common.Pad((String)vectDesc.elementAt(j),30)+" | "+
                              common.Pad("",15)+" | "+common.Pad("",10)+" | "+
                              common.Pad("",10)+" | "+
                              common.Rad("",10)+" |";
                         }
                         FW.write(Strl+"\n");
                         Lctr = Lctr+1;
                    }
                    catch(Exception ex){}
               }
               try
               {
                    String SColor ="";
                    String SSize  ="";

                    if(SPColor.length() > 0)
                         SColor    = "Color: "+common.Pad(SPColor,11);

                    if(SPSet.length() > 0)
                         SColor    += " Set : "+SPSet;

                    if(SPSize.length() > 0)
                         SSize     = "Size : "+common.Pad(SPSize,11);

                    if(SPSide.length() > 0)
                         SSize     += " Side: "+SPSide;

                    if(isStationary(SCode))
                    {
                         Strl =    "| "+common.Pad(" ",9)+" | "+common.Pad(SColor,30)+" | "+
                                   common.Pad("",15)+" | "+common.Pad("",10)+" | "+
                                   common.Pad("",10)+" | "+
                                   common.Rad("",10)+" |";
                         FW.write(Strl+"\n");
     
                         Strl =    "| "+common.Pad(" ",9)+" | "+common.Pad(SSize,30)+" | "+
                                   common.Pad("",15)+" | "+common.Pad("",10)+" | "+
                                   common.Pad("",10)+" | "+
                                   common.Rad("",10)+" |";
     
                         FW.write(Strl+"\n");
                         Lctr=Lctr+2;
                    }
               }
               catch(Exception ex){}

               try
               {
                    FW.write(Strl11+"\n");
               }
               catch(Exception ex){}
          }
          try
          {
               if(!bHeadSig)
               {
                    Strl = "------------------------------------------------------------------------------------------------------|";
                    FW.write(Strl+"\n");
               }
          }
          catch(Exception ex){}
     }

     public void setEnqFoot()
     {
          Vector vect = new Vector();

          vect.addElement("We request you to kindly quote your price along with the deails");
          vect.addElement("given below :");
          vect.addElement("");
          vect.addElement("01) Price List with catalogue if any.");
          vect.addElement("02) % of Discount offered on the list price.");
          vect.addElement("03) % of Excise duty and availability of transporters copy.");
          vect.addElement("04) Nature of sale & % of sales tax.");
          vect.addElement("05) Whether Form XVII can be issued for availing the concessional");
          vect.addElement("    rate of tax.");
          vect.addElement("06) Packing and Forwarding charges, if any.");
          vect.addElement("07) Freight, if any.");
          vect.addElement("08) Delivery Period.");
          vect.addElement("09) Credit Period.");
          vect.addElement("10) Differentiate your price of");
          vect.addElement("    a) Cash Purchase");
          vect.addElement("    b) Credit Purchase (30 days credit period)");
          vect.addElement("");
          vect.addElement("Please consider the following points before submitting your");
          vect.addElement("quotation to us.");
          vect.addElement("");
          vect.addElement("a) Your quoted price shall be taken as final.");
          vect.addElement("b) Send the samples along with quotation.");
          vect.addElement("c) The enquiry reference No. shall be given in your quotation.");
          vect.addElement("d) The quotation should be reached at our mills");
          vect.addElement("   on or before "+common.getDate(SEnqDate,10,1));

          try
          {
               for(int i=0;i<vect.size();i++)
               {
                    setSmallFoot();
                    FW.write((String)vect.elementAt(i)+"\n\n");
                    Lctr=Lctr+2;
               }
               setSmallFoot();
               
               FW.write("Thanking you,\n");
               FW.write("For E"+SMillName+",\n\n\n");
               FW.write("(R.JAICHANDER)\n");
               FW.write("Joint Managing DirectorF\n\n");
          }
          catch(Exception ex){}
     }

     private void setSmallFoot()
     {
          if(Lctr < 55)
               return;
          try
          {
               Pctr++;
               FW.write("");
               FW.write(common.Rad(String.valueOf(Pctr),80)+"\n\n");
               Lctr = 2;
          }
          catch(Exception ex){}
     }

     public void setDataIntoVector()
     {
          VCode      = new Vector();
          VName      = new Vector();
          VUOM       = new Vector();
          VQty       = new Vector();
          VDesc      = new Vector();  
          VCatl      = new Vector();
          VDraw      = new Vector();
          VMake      = new Vector();
          VPColor    = new Vector();
          VPSet      = new Vector();
          VPSize     = new Vector();
          VPSide     = new Vector();

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               ResultSet res  = stat.executeQuery(getQString());
               
               while (res.next())
               {
                    String str1  = res.getString(1);  
                    String str2  = res.getString(2);
                    String str3  = res.getString(3);
                    String str4  = res.getString(4);

                    VCode     . addElement(str1);
                    VName     . addElement(str2);
                    VQty      . addElement(str3);
                    VUOM      . addElement(str4);
                    VDesc     . addElement(common.parseNull(res.getString(5)));
                    VCatl     . addElement(common.parseNull(res.getString(6)));      
                    VDraw     . addElement(common.parseNull(res.getString(7)));      
                    VMake     . addElement(common.parseNull(res.getString(8)));
                    VPColor   . addElement(common.parseNull(res.getString(9)));
                    VPSet     . addElement(common.parseNull(res.getString(10)));
                    VPSize    . addElement(common.parseNull(res.getString(11)));
                    VPSide    . addElement(common.parseNull(res.getString(12)));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public String getQString()
     {
          String QString  = "";
          try
          {
               QString = " SELECT Enquiry.Item_Code, InvItems.Item_Name, Enquiry.Qty, "+
                         " UOM.UoMName,Enquiry.Descr,InvItems.Catl,InvItems.Draw,Enquiry.Make,"+
                         " InvItems.PAPERCOLOR,PaperSet.SETNAME,InvItems.PAPERSIZE,PaperSide.SIDENAME"+
                         " FROM Enquiry INNER JOIN InvItems ON InvItems.Item_Code = Enquiry.Item_Code "+
                         " inner join paperset on paperset.setcode = invitems.papersets inner join paperside on paperside.sidecode = invitems.paperside"+
                         " INNER JOIN UOM on Uom.UomCode = InvItems.UomCode Where Enquiry.EnqNo = "+SEnqNo+" And Enquiry.Sup_Code = '"+SSupCode+"'";
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
          return QString;
     }

     public void getAddr()
     {
          try
          {
               String QS =    "Select "+SSupTable+".Addr1,"+SSupTable+".Addr2,"+SSupTable+".Addr3,place.placename,"+SSupTable+".Name "+
                              "From "+SSupTable+" LEFT Join place On place.placecode = "+SSupTable+".city_code "+
                              "Where "+SSupTable+".Ac_Code = '"+SSupCode+"'";

               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               
               ResultSet res  = stat.executeQuery(QS);
               
               while (res.next())
               {
                    SAddr1   = res.getString(1);
                    SAddr2   = res.getString(2);
                    SAddr3   = common.parseNull(res.getString(3));
                    SSupName = res.getString(5);
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public boolean isStationary(String SItemCode)
     {
          String SStkGroupCode     = "";
          String QS                = "";

          if(iMillCode==0)
          {
               QS =    " select stkgroupcode,invitems.item_code from invitems"+
                       " where invitems.item_code = '"+SItemCode+"'";
          }
          else
          {
               QS =    " select stkgroupcode,"+SItemTable+".item_code from "+SItemTable+""+
                       " where "+SItemTable+".item_code = '"+SItemCode+"'";
          }
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnect    =  oraConnection.getConnection();               
               Statement      stat          = theConnect.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result.next())
               {
                    SStkGroupCode  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }
          if(SStkGroupCode.equals("B01"))
               return true;

          return false;
     }
}
