package enquiry;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;


public class EnquiryListForm extends JInternalFrame
{
     String         SStDate,SEnDate;
     JComboBox      JCOrder,JCFilter;
     JButton        BApply,BDelete,BPDF;
     JPanel         TopPanel,BottomPanel;
     JPanel         DatePanel,SortPanel,BasisPanel,FilterPanel,ApplyPanel,MailPanel;    
     TabReport      tabreport;
     DateField2     TStDate;
     DateField2     TEnDate;
     NextField      TStNo,TEnNo;
     
     JRadioButton   JRPeriod,JRNo;
     JRadioButton   RWithMail,RWithOutMail,RAllMail;
     
     Object         RowData[][];
     String         ColumnData[] = {"Enq No","Date","Supplier","Qty","Department","Mail","EMail-Id","ContPerson","MobileNo"};
     String         ColumnType[] = {"N","S","S","N","S","B","S","S","S"};
     Common         common = new Common();
     Vector         VDate,VEnqNo,VMrsNo,VRCode,VRName,VSupName,VQty,VDept,VGroup,VStatus,VUnit,VDue,VId,VEMail,VSupCode,VApproved,VContractPerson,VMobileNo;
     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     Vector         VCode,VName;
     int            iAuthCode;
     int            iMillCode;
     String         SItemTable,SSupTable,SMillName;
     Vector         VSupContPerson,VSupMobileNo,VSupSupCode;

	//pdf
 
	Document document;
	PdfPCell c1;
	int iTotalColumns=8;
	int[] iWidth={8,10,30,10,15,30,15,15};
	private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
	private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
	private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
	private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 7, Font.BOLD);
	private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 4, Font.BOLD);
	String              SFileName, SFileOpenPath, SFileWritePath; 
	PdfPTable table;
    
    public EnquiryListForm(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iAuthCode,int iMillCode,String SItemTable,String SSupTable,String SMillName)
     {

          super("Enquiries Placed During a Period");

          this.DeskTop   = DeskTop;
          this.SPanel    = SPanel;
          this.VCode     = VCode;
          this.VName     = VName;
          this.iAuthCode = iAuthCode;
          this.iMillCode = iMillCode;


          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;


		  SFileName      	= common.getPrintPath()+"EnquiryList.pdf";
          createComponents();
          setLayouts();
          addComponents();
          addListeners();

     }

     public void createComponents()
     {
          TStDate        = new DateField2();
          TEnDate        = new DateField2();
          TStNo          = new NextField(0);
          TEnNo          = new NextField(0);
          BApply         = new JButton("Apply");
          BDelete        = new JButton("Mail Enquiries");
          BPDF	         = new JButton("Generate PDF");
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel(); 
          BasisPanel     = new JPanel(); 
          DatePanel      = new JPanel();
          SortPanel      = new JPanel();
          FilterPanel    = new JPanel();
          ApplyPanel     = new JPanel();
          MailPanel      = new JPanel();
          
          JCOrder        = new JComboBox();
          JCFilter       = new JComboBox();

          RAllMail       = new JRadioButton("All",true);
          RWithMail      = new JRadioButton("Sending Mail List",false);
          RWithOutMail   = new JRadioButton("Pending Mail List",false);

          
          JRPeriod       = new JRadioButton("Periodical",true);
          JRNo           = new JRadioButton("Enquiry No",false);

          TStDate        . setTodayDate();
          TEnDate        . setTodayDate();
     }    
     
     public void setLayouts()
     {
          TopPanel       . setLayout(new GridLayout(1,6));
          BasisPanel     . setLayout(new GridLayout(2,1));
          DatePanel      . setLayout(new GridLayout(2,1));
//          SortPanel      . setLayout(new BorderLayout());
//          FilterPanel    . setLayout(new BorderLayout());
          SortPanel      . setLayout(new GridLayout(2,1));
          FilterPanel    . setLayout(new GridLayout(2,1));
          ApplyPanel     . setLayout(new BorderLayout());
          MailPanel      . setLayout(new GridLayout(3,1));

          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          JCOrder   . addItem("Enq No");
          JCOrder   . addItem("Supplierwise");
          JCOrder   . addItem("Departmentwise");
          
          JCFilter  . addItem("All");
          JCFilter  . addItem("Over-Due");
          JCFilter  . addItem("Quotation Not Received");
          
          TopPanel  . add(SortPanel);
//          TopPanel  . add(FilterPanel);
          TopPanel  . add(BasisPanel);
          TopPanel  . add(DatePanel);
          TopPanel  . add(ApplyPanel);
          TopPanel  . add(MailPanel);
          
//          if(iAuthCode==3)
//          {
               BottomPanel.add(BDelete);
//          }
     
		  BottomPanel.add(BPDF);

          SortPanel      . add(new JLabel("Sort"));
          SortPanel      . add(JCOrder);
          SortPanel      . add(new JLabel("Filter"));
          SortPanel      . add(JCFilter);
//          FilterPanel    . add(new JLabel(""));
//          FilterPanel    . add("Center",JCFilter);
          DatePanel      . add(TStDate);
          DatePanel      . add(TEnDate);
          ApplyPanel     . add("Center",BApply);
          BasisPanel     . add(JRPeriod);
          BasisPanel     . add(JRNo);

          MailPanel      . add(RWithMail);
          MailPanel      . add(RWithOutMail);
          MailPanel      . add(RAllMail);

          SortPanel      . setBorder(new TitledBorder("Sorting & Filter"));
          FilterPanel    . setBorder(new TitledBorder("Filter"));
          BasisPanel     . setBorder(new TitledBorder("Basis"));
          DatePanel      . setBorder(new TitledBorder("Period"));
          ApplyPanel     . setBorder(new TitledBorder("Control"));
          MailPanel      . setBorder(new TitledBorder("Select"));
     
          getContentPane(). add(TopPanel,BorderLayout.NORTH);
          getContentPane(). add(BottomPanel,BorderLayout.SOUTH);

          BDelete.setEnabled(false);
     }

     public void addListeners()
     {
          BApply         . addActionListener(new ApplyList());
          JRPeriod       . addActionListener(new JRList());
          JRNo           . addActionListener(new JRList());
          BDelete        . addActionListener(new MailList());
          BPDF           . addActionListener(new PrintList());

          RWithMail       . addActionListener(new JRList1());
          RWithOutMail    . addActionListener(new JRList1());
          RAllMail        . addActionListener(new JRList1());

     }
	public class PrintList implements ActionListener
    {
    	public void actionPerformed(ActionEvent ae)
        {
			String STitle = "";

			SStDate = TStDate.TDay.getText()+"."+TStDate.TMonth.getText()+"."+TStDate.TYear.getText();
			SEnDate = TEnDate.TDay.getText()+"."+TEnDate.TMonth.getText()+"."+TEnDate.TYear.getText();

			if(JRPeriod.isSelected())
			{
				STitle = " Enquiry List From :"+SStDate+" To :"+SEnDate+" ";
			}
			else
			{
				STitle = " Enquiry From Number :"+TStNo+" Enquiry To Number:"+TEnNo+" ";
			}
        	CreatePDF(STitle);
        }
    }
     private class JRList1 implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==RAllMail)
               {
                    RAllMail       . setSelected(true);
                    RWithMail      . setSelected(false);
                    RWithOutMail   . setSelected(false);
                    BDelete.setEnabled(false);
               }
               if(ae.getSource()==RWithMail)
               {
                    RWithMail      . setSelected(true);
                    RAllMail       . setSelected(false);
                    RWithOutMail   . setSelected(false);
                    BDelete.setEnabled(true);
               }
               if(ae.getSource()==RWithOutMail)
               {
                    RWithOutMail   . setSelected(true);
                    RAllMail       . setSelected(false);
                    RWithMail      . setSelected(false);
                    BDelete.setEnabled(true);
               }
          }
     }

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    DatePanel . setBorder(new TitledBorder("Periodical"));
                    DatePanel . removeAll();
                    DatePanel . add(TStDate);
                    DatePanel . add(TEnDate);
                    DatePanel . updateUI();
                    JRNo      . setSelected(false);
               }
               else
               {
                    DatePanel . setBorder(new TitledBorder("Numbered"));
                    DatePanel . removeAll();
                    DatePanel . add(TStNo);
                    DatePanel . add(TEnNo);
                    DatePanel . updateUI();
                    JRPeriod  . setSelected(false);
               }
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               getMobileNoAndContactPerson();
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
                    tabreport           . setBorder(new TitledBorder("Enquiry List"));
                    getContentPane()    . add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    DeskTop             . repaint();
                    DeskTop             . updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public void setDataIntoVector()
     {
          VDate       = new Vector();
          VEnqNo      = new Vector();
          VMrsNo      = new Vector();
          VRCode      = new Vector();
          VRName      = new Vector();
          VSupName    = new Vector();
          VQty        = new Vector();
          VDept       = new Vector();
          VGroup      = new Vector();
          VStatus     = new Vector();
          VDue        = new Vector();
          VUnit       = new Vector();
          VId         = new Vector();
          VEMail      = new Vector();
          VSupCode    = new Vector();
          VApproved   = new Vector();
          VContractPerson = new Vector();
          VMobileNo       = new Vector();

                    SStDate = TStDate.TDay.getText()+"."+TStDate.TMonth.getText()+"."+TStDate.TYear.getText();
                    SEnDate = TEnDate.TDay.getText()+"."+TEnDate.TMonth.getText()+"."+TEnDate.TYear.getText();
          String    StDate  = TStDate.TYear.getText()+TStDate.TMonth.getText()+TStDate.TDay.getText();
          String    EnDate  = TEnDate.TYear.getText()+TEnDate.TMonth.getText()+TEnDate.TDay.getText();
          String    QString = getQString(StDate,EnDate);
          
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
          
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    String str1    = res.getString(1);  
                    String str2    = res.getString(2);
                    String str3    = res.getString(3);
                    String str4    = res.getString(4);
                    String str5    = res.getString(5);
                    String str6    = res.getString(6);
                    String str7    = res.getString(7);
                    String str8    = res.getString(8);
                    str2           = common.parseDate(str2);
                    str8           = common.parseDate(str8);
                    
                    
                    VEnqNo    . addElement(str1);
                    VDate     . addElement(str2);
                    VSupName  . addElement(str3);
                    VQty      . addElement(str4);
                    VUnit     . addElement(str5);
                    VDept     . addElement(str6);
                    VGroup    . addElement(str7);
                    VDue      . addElement(str8);
                    VStatus   . addElement(" ");
                    VEMail    . addElement(res.getString(9));
                    VSupCode  . addElement(res.getString(10));
                    VApproved . addElement(res.getString(11));

                    VContractPerson . addElement(getContractPerson(res.getString(10)));
                    VMobileNo       . addElement(getMobileNo(res.getString(10)));

               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VDate.size()][ColumnData.length];
          for(int i=0;i<VDate.size();i++)
          {
               RowData[i][0]  = common.parseNull((String)VEnqNo    . elementAt(i));
               RowData[i][1]  = common.parseNull((String)VDate     . elementAt(i));
               RowData[i][2]  = common.parseNull((String)VSupName  . elementAt(i));
               RowData[i][3]  = common.parseNull((String)VQty      . elementAt(i));
               RowData[i][4]  = common.parseNull((String)VDept     . elementAt(i));
               RowData[i][5]  = new Boolean(false);
               RowData[i][6] =  common.parseNull((String)VEMail   . elementAt(i));
               RowData[i][7] =  common.parseNull((String)VContractPerson. elementAt(i));
               RowData[i][8] =  common.parseNull((String)VMobileNo      . elementAt(i));

          }  
     }

     public String getQString(String StDate,String EnDate)
     {
          DateField2 df = new DateField2();
          df.setTodayDate();
          String SToday = df.TYear.getText()+df.TMonth.getText()+df.TDay.getText();
          String QString = "";
     
          if(JRPeriod.isSelected())
          {
               QString =      " SELECT Enquiry.EnqNo, Enquiry.EnqDate, "+SSupTable+".Name, Sum(Enquiry.Qty), '' as Unit_Name,'' as  Dept_Name, '' as GroupName, '' as DueDate,EMail,Enquiry.Sup_Code,Approved "+
                              " FROM ((((Enquiry INNER JOIN InvItems ON Enquiry.Item_Code = InvItems.Item_Code) INNER JOIN "+SSupTable+" ON Enquiry.Sup_Code = "+SSupTable+".Ac_Code) Left JOIN Unit ON Enquiry.Unit_Code = Unit.Unit_Code) "+
                              " Left JOIN Dept ON Enquiry.Dept_Code = Dept.Dept_code) Left JOIN Cata ON Enquiry.Group_Code = Cata.Group_Code "+
                              " Where Enquiry.EnqDate >= '"+StDate+"' and Enquiry.EnqDate <='"+EnDate+"'"+
                              " and Enquiry.MillCode="+iMillCode+"  ";     // and AutoMail=0
          }
          else
          {
               QString =      " SELECT Enquiry.EnqNo, Enquiry.EnqDate,  "+SSupTable+".Name, Sum(Enquiry.Qty), '' as Unit_Name, '' as Dept_Name, '' as GroupName, '' as DueDate,EMail,Enquiry.Sup_Code,Approved "+
                              " FROM ((((Enquiry INNER JOIN InvItems ON Enquiry.Item_Code = InvItems.Item_Code) INNER JOIN "+SSupTable+" ON Enquiry.Sup_Code = "+SSupTable+".Ac_Code) Left JOIN Unit ON Enquiry.Unit_Code = Unit.Unit_Code) "+
                              " Left JOIN Dept ON Enquiry.Dept_Code = Dept.Dept_code) Left JOIN Cata ON Enquiry.Group_Code = Cata.Group_Code "+
                              " Where Enquiry.EnqNo >="+TStNo.getText()+" and Enquiry.EnqNo <= "+TEnNo.getText()+
                              " and Enquiry.MillCode="+iMillCode+"   ";    //and AutoMail=0 

          }


          if(RWithOutMail.isSelected())
                 QString = QString+" and MailStatus=0 ";
          if(RWithMail.isSelected())
                 QString = QString+" and MailStatus=1 ";

          if(JCFilter.getSelectedIndex()==1)
               QString = QString+" and Enquiry.DueDate <= '"+SToday+"'";
          if(JCFilter.getSelectedIndex()==2)
               QString = QString+" and Enquiry.Quotation = 0";
          
               QString = QString+" Group By Enquiry.EnqNo, Enquiry.EnqDate, "+SSupTable+".Name,  EMail,Enquiry.Sup_Code,Approved ";


          if(JCOrder.getSelectedIndex() == 0)      
               QString = QString+" Order By Enquiry.EnqNo,Enquiry.EnqDate,"+SSupTable+".Name";
          if(JCOrder.getSelectedIndex() == 1)
               QString = QString+" Order By "+SSupTable+".Name,Enquiry.EnqNo,Enquiry.EnqDate";
          if(JCOrder.getSelectedIndex() == 2)      
               QString = QString+" Order By Enquiry.EnqNo,"+SSupTable+".Name";

          return QString;
     }


     private class MailList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               MailEnqs();


               getMobileNoAndContactPerson();
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
                    tabreport           . setBorder(new TitledBorder("Enquiry List"));
                    getContentPane()    . add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    DeskTop             . repaint();
                    DeskTop             . updateUI();
               }
               catch(Exception e)
               {
                    System.out.println(e);
               }



//               removeHelpFrame();
          }
     }

     private void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.updateUI();
               DeskTop.repaint();
          }
          catch(Exception ex){}
     }

     private void MailEnqs()
     {
          try
          {
               for(int i=0;i<RowData.length;i++)
               {
                    Boolean bValue = (Boolean)RowData[i][5];
                    if(!bValue.booleanValue())
                         continue;

                    String SEnqNo   = (String)VEnqNo   . elementAt(i);
                    String SEnqDate = (String)VDate    . elementAt(i);
                    String SMail    = (String)VEMail   . elementAt(i);
                    String SSupCode = (String)VSupCode . elementAt(i);
                    String SApproved= (String)VApproved. elementAt(i);
                                 toForm(SEnqNo,SEnqDate,SMail,SSupCode,SApproved);
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private void toForm(String SEnqNo,String SEnqDate,String SMail,String SSupCode,String SApproved)
     {
               new EnquiryFormDesign(SEnqNo,SEnqDate,SSupCode,iMillCode,SMail,SApproved,SSupTable);
     }

     public void getMobileNoAndContactPerson()
     {
          VSupContPerson = new Vector();
          VSupMobileNo   = new Vector();
          VSupSupCode    = new Vector();


          String QS = " Select ContPerson1,PurMobileNo,PartyCode from PartyMaster ";

          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(QS);

               while (res.next())
               {
                    VSupContPerson.addElement(common.parseNull(res.getString(1)));
                    VSupMobileNo.addElement(common.parseNull(res.getString(2)));
                    VSupSupCode.addElement(common.parseNull(res.getString(3)));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }
     public String getContractPerson(String SSupCode)
     {
        int iIndex=-1;
        iIndex = VSupSupCode.indexOf(SSupCode);
        if(iIndex!=-1)
                return common.parseNull((String)VSupContPerson.elementAt(iIndex));
        else
                return "";

     }
     public String getMobileNo(String SSupCode)
     {
        int iIndex=-1;
        iIndex = VSupSupCode.indexOf(SSupCode);
        if(iIndex!=-1)
                return common.parseNull((String)VSupMobileNo.elementAt(iIndex));
        else
                return "";

     }
	private void CreatePDF(String STitle) 
	{
		try 
		{
            document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(SFileName));
            document.setPageSize(PageSize.A4.rotate());
            document.open();

            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            table.setHeaderRows(5);

			SetPDFHead(STitle);
          
        } 
		catch (Exception ex) 
		{
            ex.printStackTrace();
			System.out.println(ex);
        }
	}
	private void SetPDFHead(String STitle)
	{
		AddCellIntoTable("AMARJOTHI SPINNING MILLS LTD" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,iTotalColumns,0,0,0,0,bigbold);
		AddCellIntoTable(STitle,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,iTotalColumns,0,0,0,0,bigbold);

		AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,iTotalColumns,0,0,0,0,bigbold);

		AddCellIntoTable("Enquiry No",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Enquiry Date",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Supplier Name",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
		AddCellIntoTable("Qty",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Department",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("EMail-ID",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold); 
		AddCellIntoTable("Contact Person",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);
		AddCellIntoTable("Mobile No",table,Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 4, 1, 8, 2, mediumbold);

 		for(int i=0;i<VDate.size();i++)
        {
			AddCellIntoTable(common.parseNull((String)VEnqNo.elementAt(i)),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(common.parseDate((String)VDate.elementAt(i)),table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(common.parseNull((String)VSupName.elementAt(i)),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(common.parseNull((String)VQty.elementAt(i)),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(common.parseNull((String)VDept.elementAt(i)),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(common.parseNull((String)VEMail.elementAt(i)),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(common.parseNull((String)VContractPerson.elementAt(i)),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(common.parseNull((String)VMobileNo.elementAt(i)),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
		}
		try
		{
			document.add(table);
			document.close();
		}catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e);
		}

		JOptionPane.showMessageDialog(null, "PDF File Created in "+SFileName,"Info",JOptionPane.INFORMATION_MESSAGE);
		try
		{
			File pdfFile = new File(SFileName);
						
			if (Desktop.isDesktopSupported()) // For Both Windows and Linux
			{
				Desktop.getDesktop().open(pdfFile);
			} 
			else 
			{
				System.out.println("Awt Desktop is not supported!");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e);
		}
	}
		public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
 
   
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {


        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));

      
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

}
