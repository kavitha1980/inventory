package enquiry;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class EnquiryListFrame extends JInternalFrame
{
     String         SStDate,SEnDate;
     JComboBox      JCOrder,JCFilter;
     JButton        BApply,BDelete;
     JPanel         TopPanel,BottomPanel;
     JPanel         DatePanel,SortPanel,BasisPanel,FilterPanel,ApplyPanel;    
     TabReport      tabreport;
     DateField2     TStDate;
     DateField2     TEnDate;
     NextField      TStNo,TEnNo;
     
     JRadioButton   JRPeriod,JRNo;
     
     Object         RowData[][];
     String         ColumnData[] = {"Enq No","Date","Mrs No","Code","Name","Supplier","Qty","Status","Due Date","Delete Mark"};
     String         ColumnType[] = {"N","S","N","S","S","S","N","S","S","B"};
     Common         common = new Common();
     Vector         VDate,VEnqNo,VMrsNo,VRCode,VRName,VSupName,VSupCode,VQty,VStatus,VDue,VId;
     Vector         VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedDueDate,VSelectedMrsSlNo,VSelectedSlNo;
     Vector         VPrevSupCode,VPrevSupName;
     JLayeredPane   DeskTop;
     StatusPanel    SPanel;
     Vector         VCode,VName;
     int            iUserCode,iAuthCode;
     int            iMillCode;
     String         SItemTable,SSupTable,SMillName;
     
     public EnquiryListFrame(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel,int iUserCode,int iAuthCode,int iMillCode,String SItemTable,String SSupTable,String SMillName)
     {
          super("Enquiries Placed During a Period");
          this.DeskTop    = DeskTop;
          this.SPanel     = SPanel;
          this.VCode      = VCode;
          this.VName      = VName;
          this.iUserCode  = iUserCode;
          this.iAuthCode  = iAuthCode;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;
     
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TStDate        = new DateField2();
          TEnDate        = new DateField2();
          TStNo          = new NextField(0);
          TEnNo          = new NextField(0);
          BApply         = new JButton("Apply");
          BDelete        = new JButton("Delete Marked Enquiries");
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel(); 
          BasisPanel     = new JPanel(); 
          DatePanel      = new JPanel();
          SortPanel      = new JPanel();
          FilterPanel    = new JPanel();
          ApplyPanel     = new JPanel();
          
          JCOrder        = new JComboBox();
          JCFilter       = new JComboBox();
          
          JRPeriod       = new JRadioButton("Periodical",true);
          JRNo           = new JRadioButton("Enquiry No",false);
          
          TStDate        . setTodayDate();
          TEnDate        . setTodayDate();
     }    
     
     public void setLayouts()
     {
          TopPanel       . setLayout(new GridLayout(1,5));
          BasisPanel     . setLayout(new GridLayout(2,1));
          DatePanel      . setLayout(new GridLayout(2,1));
          SortPanel      . setLayout(new BorderLayout());
          FilterPanel    . setLayout(new BorderLayout());
          ApplyPanel     . setLayout(new BorderLayout());
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
          setResizable(true);
          setBounds(0,0,650,500);
     }
     
     public void addComponents()
     {
          JCOrder   . addItem("Materialwise");
          JCOrder   . addItem("MRS No");
          JCOrder   . addItem("Enq No");
          JCOrder   . addItem("Supplierwise");
          
          JCFilter  . addItem("All");
          JCFilter  . addItem("Over-Due");
          JCFilter  . addItem("Quotation Not Received");
          
          TopPanel  . add(SortPanel);
          TopPanel  . add(FilterPanel);
          TopPanel  . add(BasisPanel);
          TopPanel  . add(DatePanel);
          TopPanel  . add(ApplyPanel);
          
          if(iAuthCode==3)
          {
               //BottomPanel.add(BDelete);
          }
     
          SortPanel      . add("Center",JCOrder);
          FilterPanel    . add("Center",JCFilter);
          DatePanel      . add(TStDate);
          DatePanel      . add(TEnDate);
          ApplyPanel     . add("Center",BApply);
          BasisPanel     . add(JRPeriod);
          BasisPanel     . add(JRNo);
     
          SortPanel      . setBorder(new TitledBorder("Sorting"));
          FilterPanel    . setBorder(new TitledBorder("Filter"));
          BasisPanel     . setBorder(new TitledBorder("Basis"));
          DatePanel      . setBorder(new TitledBorder("Period"));
          ApplyPanel     . setBorder(new TitledBorder("Control"));
     
          getContentPane(). add(TopPanel,BorderLayout.NORTH);
          getContentPane(). add(BottomPanel,BorderLayout.SOUTH);
     }

     public void addListeners()
     {
          BApply         . addActionListener(new ApplyList());
          JRPeriod       . addActionListener(new JRList());
          JRNo           . addActionListener(new JRList());
          BDelete        . addActionListener(new DeleteList());
     }

     private class JRList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRPeriod)
               {
                    DatePanel . setBorder(new TitledBorder("Periodical"));
                    DatePanel . removeAll();
                    DatePanel . add(TStDate);
                    DatePanel . add(TEnDate);
                    DatePanel . updateUI();
                    JRNo      . setSelected(false);
               }
               else
               {
                    DatePanel . setBorder(new TitledBorder("Numbered"));
                    DatePanel . removeAll();
                    DatePanel . add(TStNo);
                    DatePanel . add(TEnNo);
                    DatePanel . updateUI();
                    JRPeriod  . setSelected(false);
               }
          }
     }

     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                    getContentPane().remove(tabreport);
               }
               catch(Exception ex){}
               try
               {
                    tabreport           = new TabReport(RowData,ColumnData,ColumnType);
                    tabreport           . setBorder(new TitledBorder("<Insert> For Enquiry  <Home> For EnquiryLetter   <F3> for Supplier Addition "));
                    getContentPane()    . add(tabreport,BorderLayout.CENTER);
                    tabreport           . ReportTable.addKeyListener(new KeyList());
                    setSelected(true);
                    DeskTop             . repaint();
                    DeskTop             . updateUI();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public void setDataIntoVector()
     {
          VDate       = new Vector();
          VEnqNo      = new Vector();
          VMrsNo      = new Vector();
          VRCode      = new Vector();
          VRName      = new Vector();
          VSupName    = new Vector();
          VQty        = new Vector();
          VStatus     = new Vector();
          VDue        = new Vector();
          VId         = new Vector();
          VSupCode    = new Vector();
          
                    SStDate = TStDate.TDay.getText()+"."+TStDate.TMonth.getText()+"."+TStDate.TYear.getText();
                    SEnDate = TEnDate.TDay.getText()+"."+TEnDate.TMonth.getText()+"."+TEnDate.TYear.getText();
          String    StDate  = TStDate.TYear.getText()+TStDate.TMonth.getText()+TStDate.TDay.getText();
          String    EnDate  = TEnDate.TYear.getText()+TEnDate.TMonth.getText()+TEnDate.TDay.getText();
          String    QString = getQString(StDate,EnDate);
          
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
          
               ResultSet res  = stat.executeQuery(QString);
               while (res.next())
               {
                    String str1    = res.getString(1);  
                    String str2    = res.getString(2);
                    String str3    = res.getString(3);
                    String str4    = res.getString(4);
                    String str5    = res.getString(5);
                    String str6    = res.getString(6);
                    String str7    = res.getString(7);
                    String str8    = res.getString(8);
                    String str9    = res.getString(9);
                    String SId     = res.getString(10);

                    str2           = common.parseDate(str2);
                    str8           = common.parseDate(str8);

                    VEnqNo    . addElement(str1);
                    VDate     . addElement(str2);
                    VMrsNo    . addElement(str3);
                    VRCode    . addElement(str4);
                    VRName    . addElement(str5);
                    VSupName  . addElement(str6);
                    VQty      . addElement(str7);
                    VDue      . addElement(str8);
                    VSupCode  . addElement(str9);
                    VStatus   . addElement(" ");
                    VId       . addElement(SId);
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public void setRowData()
     {
          RowData     = new Object[VDate.size()][ColumnData.length];
          for(int i=0;i<VDate.size();i++)
          {
               RowData[i][0]  = (String)VEnqNo    . elementAt(i);
               RowData[i][1]  = (String)VDate     . elementAt(i);
               RowData[i][2]  = (String)VMrsNo    . elementAt(i);
               RowData[i][3]  = (String)VRCode    . elementAt(i);
               RowData[i][4]  = (String)VRName    . elementAt(i);
               RowData[i][5]  = (String)VSupName  . elementAt(i);
               RowData[i][6]  = (String)VQty      . elementAt(i);
               RowData[i][7]  = (String)VStatus   . elementAt(i);
               RowData[i][8]  = (String)VDue      . elementAt(i);
               RowData[i][9]  = new Boolean(false);
          }
     }

     public String getQString(String StDate,String EnDate)
     {
          DateField2 df = new DateField2();
          df.setTodayDate();
          String SToday = df.TYear.getText()+df.TMonth.getText()+df.TDay.getText();
          String QString = "";
     
          if(JRPeriod.isSelected())
          {
               QString =      " SELECT Enquiry.EnqNo, Enquiry.EnqDate, Enquiry.MRS_No, Enquiry.Item_Code, InvItems.Item_Name, "+SSupTable+".Name, Enquiry.Qty, Enquiry.DueDate,Enquiry.Sup_Code,Enquiry.Id "+
                              " FROM (Enquiry INNER JOIN InvItems ON Enquiry.Item_Code = InvItems.Item_Code) INNER JOIN "+SSupTable+" ON Enquiry.Sup_Code = "+SSupTable+".Ac_Code "+
                              " Where Enquiry.EnqDate >= '"+StDate+"' and Enquiry.EnqDate <='"+EnDate+"'"+
                              " and Enquiry.MillCode="+iMillCode;
          }
          else
          {
               QString =      " SELECT Enquiry.EnqNo, Enquiry.EnqDate, Enquiry.MRS_No, Enquiry.Item_Code, InvItems.Item_Name, "+SSupTable+".Name, Enquiry.Qty, Enquiry.DueDate,Enquiry.Sup_Code,Enquiry.Id "+
                              " FROM (Enquiry INNER JOIN InvItems ON Enquiry.Item_Code = InvItems.Item_Code) INNER JOIN "+SSupTable+" ON Enquiry.Sup_Code = "+SSupTable+".Ac_Code "+
                              " Where Enquiry.EnqNo >="+TStNo.getText()+" and Enquiry.EnqNo <= "+TEnNo.getText()+
                              " and Enquiry.MillCode="+iMillCode;
          }
          
          if(JCFilter.getSelectedIndex()==1)
               QString = QString+" and Enquiry.DueDate <= '"+SToday+"'";
          if(JCFilter.getSelectedIndex()==2)
               QString = QString+" and Enquiry.Quotation = 0";
          
          
          if(JCOrder.getSelectedIndex() == 0)      
               QString = QString+" Order By InvItems.Item_Name,Enquiry.EnqDate";
          if(JCOrder.getSelectedIndex() == 1)      
               QString = QString+" Order By Enquiry.Mrs_No,Enquiry.EnqDate,InvItems.Item_Name,"+SSupTable+".Name";
          if(JCOrder.getSelectedIndex() == 2)      
               QString = QString+" Order By Enquiry.EnqNo,Enquiry.EnqDate,InvItems.Item_Name,"+SSupTable+".Name";
          if(JCOrder.getSelectedIndex() == 3)      
               QString = QString+" Order By "+SSupTable+".Name,Enquiry.EnqNo,Enquiry.EnqDate,InvItems.Item_Name";
          
          return QString;
     }

     private class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    int index       = tabreport        . ReportTable  . getSelectedRow();
                    String SEnqNo   = (String)VEnqNo   . elementAt(index);
                    String SEnqDate = (String)VDate    . elementAt(index);
                                      toPrint(SEnqNo,SEnqDate);
               }
               if(ke.getKeyCode()==KeyEvent.VK_HOME)
               {
                    int index       = tabreport.ReportTable.getSelectedRow();
                    String SEnqNo   = (String)VEnqNo.elementAt(index);
                    String SEnqDate = (String)VDate.elementAt(index);
                                      toLetterPrint(SEnqNo,SEnqDate);
               }
               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    int index       = tabreport.ReportTable.getSelectedRow();
                    String SSupCode = (String)VSupCode.elementAt(index);
                    String SEnqNo   = (String)VEnqNo.elementAt(index);
                    String SEnqDate = (String)VDate.elementAt(index);

                    setSelectedRecords(SSupCode,SEnqNo);
                    try
                    {
                         SupplierAdditionFrame supplieradditionframe = new SupplierAdditionFrame(DeskTop,SEnqNo,SEnqDate,VPrevSupCode,VPrevSupName,VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedDueDate,VSelectedMrsSlNo,VSelectedSlNo,SPanel,iMillCode,iUserCode,SItemTable,SSupTable);
                         DeskTop                  . add(supplieradditionframe);
                         DeskTop                  . repaint();
                         supplieradditionframe    . setSelected(true);
                         DeskTop                  . updateUI();
                         removeHelpFrame();
                         supplieradditionframe    . show();
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                         ex.printStackTrace();
                    }
               }

          }
     }

     private class DeleteList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
//               deleteEnqs();
               removeHelpFrame();
          }
     }

     private void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.updateUI();
               DeskTop.repaint();
          }
          catch(Exception ex){}
     }

     private void deleteEnqs()
     {
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();
               for(int i=0;i<RowData.length;i++)
               {
                    Boolean bValue = (Boolean)RowData[i][9];
                    if(!bValue.booleanValue())
                         continue;

                    String SId     = (String)VId.elementAt(i); 
                    String QS      = "Delete From Enquiry Where Id = "+SId;
                    stat . execute(QS);
               }
               stat . close();
               JOptionPane.showMessageDialog(null,"Record Successfully Deleted","Error Message",JOptionPane.INFORMATION_MESSAGE);               
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"Problem In Deletion","Error Message",JOptionPane.INFORMATION_MESSAGE);               
          }
     }

     private void toPrint(String SEnqNo,String SEnqDate)
     {
          Vector VEnqSupCode = new Vector();

          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
               ResultSet      result         = stat.executeQuery("Select DISTINCT sup_code from Enquiry Where EnqNo = "+SEnqNo);

               while(result.next())
               {
                    VEnqSupCode.addElement(result.getString(1));                         
               }
               result    . close();
               stat      . close();

               FileWriter FW = new FileWriter(common.getPrintPath()+"enq.prn");

               for(int i=0;i<VEnqSupCode.size();i++)
                    new EnquiryPrint(FW,SEnqNo,SEnqDate,(String)VEnqSupCode.elementAt(i),iMillCode,SItemTable,SSupTable,SMillName);
               FW.close();
				
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void toLetterPrint(String SEnqNo,String SEnqDate)
     {
          Vector VEnqSupCode = new Vector();

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
               ResultSet      result         = stat.executeQuery("Select DISTINCT sup_code from Enquiry Where EnqNo = "+SEnqNo);

               while(result.next())
               {
                    VEnqSupCode.addElement(result.getString(1));                         
               }
               result    . close();
               stat      . close();

               FileWriter FW = new FileWriter(common.getPrintPath()+"enqlet.prn");

               for(int i=0;i<VEnqSupCode.size();i++)
                    new EnquiryLetter(FW,SEnqNo,SEnqDate,(String)VEnqSupCode.elementAt(i),iMillCode,SItemTable,SSupTable,SMillName);
               FW.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setSelectedRecords(String SSupCode,String SEnqNo)
     {
          VSelectedCode     = new Vector();
          VSelectedName     = new Vector();
          VSelectedMRS      = new Vector();
          VSelectedQty      = new Vector();
          VSelectedDueDate  = new Vector();
          VSelectedMrsSlNo  = new Vector();
          VSelectedSlNo     = new Vector();

          VPrevSupCode      = new Vector();
          VPrevSupName      = new Vector();

          String QS1 = " Select Distinct(Enquiry.Sup_Code),"+SSupTable+".Name "+
                       " From Enquiry INNER JOIN "+SSupTable+" ON Enquiry.Sup_Code = "+SSupTable+".Ac_Code "+
                       " and Enquiry.EnqNo="+SEnqNo+" and MillCode="+iMillCode;

          String QS2 = " SELECT Enquiry.Item_Code, InvItems.Item_Name,Enquiry.MRS_No, Enquiry.Qty, Enquiry.DueDate,Enquiry.MrsSlNo,Enquiry.SlNo "+
                       " FROM Enquiry INNER JOIN InvItems ON Enquiry.Item_Code = InvItems.Item_Code "+
                       " and Enquiry.EnqNo="+SEnqNo+" and Enquiry.Sup_Code='"+SSupCode+"'"+
                       " and Enquiry.MillCode="+iMillCode;

          try
          {
               ORAConnection  oraConnection  =  ORAConnection    . getORAConnection();
               Connection     theConnection  =  oraConnection    . getConnection();               
               Statement      stat           =  theConnection    . createStatement();
               
               ResultSet res  = stat.executeQuery(QS1);
               while (res.next())
               {
                    VPrevSupCode.addElement(common.parseNull(res.getString(1)));
                    VPrevSupName.addElement(common.parseNull(res.getString(2)));
               }
               res  . close();

               res  = stat.executeQuery(QS2);
               while (res.next())
               {
                    VSelectedCode   .addElement(common.parseNull(res.getString(1)));
                    VSelectedName   .addElement(common.parseNull(res.getString(2)));
                    VSelectedMRS    .addElement(common.parseNull(res.getString(3)));
                    VSelectedQty    .addElement(common.parseNull(res.getString(4)));
                    VSelectedDueDate.addElement(common.parseNull(res.getString(5)));
                    VSelectedMrsSlNo.addElement(common.parseNull(res.getString(6)));
                    VSelectedSlNo   .addElement(common.parseNull(res.getString(7)));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System    . out.println(ex);
               ex        . printStackTrace();
          }
     }

}
