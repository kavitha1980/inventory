package enquiry;

import javax.swing.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class EnquiryMiddlePanel extends JPanel 
{
     JScrollPane         TabScroll;
     EnqInvMiddlePanel   MiddlePanel;
     Object              RowData[][];

     String              ColumnData[] = {"Code","Name","MRS No","Qty","Due Date"};
     String[] ColumnType = new String[5];

     JLayeredPane        Layer;
     Vector              VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedMrsSlNo;
     Vector              VSelectedDueDate;
     int                 iMillCode;
     Common              common = new Common();


     boolean bFlag = false;
     public EnquiryMiddlePanel(JLayeredPane Layer,Vector VSelectedCode,Vector VSelectedName,Vector VSelectedMRS,Vector VSelectedQty,Vector VSelectedMrsSlNo,int iMillCode)
     {
          this.Layer               = Layer;
          this.VSelectedCode       = VSelectedCode;
          this.VSelectedName       = VSelectedName;
          this.VSelectedMRS        = VSelectedMRS;
          this.VSelectedQty        = VSelectedQty;
          this.VSelectedMrsSlNo    = VSelectedMrsSlNo;
          this.iMillCode           = iMillCode;

          setLayout(new BorderLayout());
          setRowData(VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty);
          createComponents();
     }

     public EnquiryMiddlePanel(JLayeredPane Layer,Vector VSelectedCode,Vector VSelectedName,Vector VSelectedMRS,Vector VSelectedQty,Vector VSelectedDueDate,Vector VSelectedMrsSlNo,int iMillCode,boolean bFlag)
     {
          this.Layer               = Layer;
          this.VSelectedCode       = VSelectedCode;
          this.VSelectedName       = VSelectedName;
          this.VSelectedMRS        = VSelectedMRS;
          this.VSelectedQty        = VSelectedQty;
          this.VSelectedDueDate    = VSelectedDueDate;
          this.VSelectedMrsSlNo    = VSelectedMrsSlNo;
          this.iMillCode           = iMillCode;
          this.bFlag               = bFlag;

          setLayout(new BorderLayout());
          setRowData(VSelectedCode,VSelectedName,VSelectedMRS,VSelectedQty,VSelectedDueDate);
          createComponents();
     }


     public EnquiryMiddlePanel(JLayeredPane Layer,Vector VSelectedMrsSlNo,int iMillCode)
     {
          this.Layer               = Layer;
          this.iMillCode           = iMillCode;
          this.VSelectedMrsSlNo    = VSelectedMrsSlNo;
     
          setLayout(new BorderLayout());
     }

     public void createComponents()
     {
          try
          {
               setColType();
               MiddlePanel           = new EnqInvMiddlePanel(Layer,RowData,ColumnData,ColumnType,VSelectedMrsSlNo,iMillCode);
               add(MiddlePanel,BorderLayout.CENTER);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void setRowData(Vector VSelectedCode,Vector VSelectedName,Vector VSelectedMRS,Vector VSelectedQty)
     {
          RowData     = new Object[VSelectedCode.size()][ColumnData.length];
          for(int i=0;i<VSelectedCode.size();i++)
          {
               RowData[i][0] = (String)VSelectedCode   . elementAt(i);
               RowData[i][1] = (String)VSelectedName   . elementAt(i);
               RowData[i][2] = (String)VSelectedMRS    . elementAt(i);                 
               RowData[i][3] = (String)VSelectedQty    . elementAt(i);
               RowData[i][4] = common.getDate(common.parseDate(common.getServerPureDate()),1,0);

          }
     }

     public void setRowData(Vector VSelectedCode,Vector VSelectedName,Vector VSelectedMRS,Vector VSelectedQty,Vector VSelectedDueDate)
     {
          String SCurDate = common.getServerPureDate();

          RowData     = new Object[VSelectedCode.size()][ColumnData.length];
          for(int i=0;i<VSelectedCode.size();i++)
          {
               String SDueDate = (String)VSelectedDueDate.elementAt(i);
               int iCurDiff    = common.toInt(common.getDateDiff(common.parseDate(SDueDate),common.parseDate(SCurDate)));

               RowData[i][0] = (String)VSelectedCode   . elementAt(i);
               RowData[i][1] = (String)VSelectedName   . elementAt(i);
               RowData[i][2] = (String)VSelectedMRS    . elementAt(i);                 
               RowData[i][3] = (String)VSelectedQty    . elementAt(i);
               if(iCurDiff<0)
               {
                    RowData[i][4] = " ";
               }
               else
               {
                    RowData[i][4] = common.parseDate((String)VSelectedDueDate.elementAt(i));
               }
          }
     }

     public void setRowData(Vector VSelectedCode,Vector VSelectedName)
     {
          RowData     = new Object[VSelectedCode.size()][ColumnData.length];
          for(int i=0;i<VSelectedCode.size();i++)
          {
               RowData[i][0] = (String)VSelectedCode.elementAt(i);
               RowData[i][1] = (String)VSelectedName.elementAt(i);
               RowData[i][2] = " ";
               RowData[i][3] = " ";
               RowData[i][4] = " ";
          }
     }

     public void setColType()
     {
          ColumnType[0]  = "S";
          ColumnType[1]  = "S";
          ColumnType[2]  = "N";
          ColumnType[4]  = "B";

          if(bFlag)
          {
               ColumnType[3]  = "N";
          }
          else
          {
               ColumnType[3]  = "B";
          }
     }

}         
