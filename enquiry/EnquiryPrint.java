package enquiry;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import javax.swing.*;

import util.*;
import guiutil.*;
import jdbc.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import java.io.FileOutputStream;

public class EnquiryPrint
{
     Vector         VCode,VName,VUOM,VQty;
     Vector         VDesc,VMake,VCatl,VDraw,VPColor,VPSet,VPSize,VPSide;
     String         SAddr1  = "",SAddr2    = "",SAddr3    = "";
     FileWriter     FW;
     
     String         SEnqNo,SEnqDate,SSupCode,SSupName;
     Common         common = new Common();
     
     int            Lctr      = 100;
     int            Pctr      = 0;
     int            iMillCode;
     String         SItemTable,SSupTable,SMillName;

	Document document;
	PdfPCell c1;
	int iTotalColumns=10;
	int[] iWidth={11,30,6,10,10,10,10,10,10,10};
	private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
	private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
	private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 8, Font.NORMAL);
	private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
	private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 4, Font.BOLD);
	String              SFileName, SFileOpenPath, SFileWritePath; 
	PdfPTable table;
     
     EnquiryPrint(FileWriter FW,String SEnqNo,String SEnqDate,String SSupCode,int iMillCode,String SItemTable,String SSupTable,String SMillName)
     {
          this.FW         = FW;
          this.SEnqNo     = SEnqNo;
          this.SEnqDate   = SEnqDate;
          this.SSupCode   = SSupCode;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.SMillName  = SMillName;

		  getAddr();

  		  SFileName      	= common.getPrintPath()+"EnquiryList"+SSupName+".pdf";
//        SFileName      	= "/root/EnquiryList.pdf";

          setDataIntoVector();
          setEnqHead();
          setEnqBody();
          setEnqFoot(0);
		  CreatePDF();
		  try
		  {
			  File theFile   = new File(SFileName);
			  Desktop        . getDesktop() . open(theFile);
		  }
		  catch(Exception ex){}	
     }
	 public void setEnqHead()
     {
          if(Lctr < 43)
               return;
          
          if(Pctr > 0)
               setEnqFoot(1);
          
          Pctr++;

          String Strl01 = "";
          String Strl02 = "";
          String Strl03 = "";
          String Strl04 = "";
          String Strl05 = "";
          String Strl06 = "";
          String Strl07 = "";
          String Strl08 = "";
          String Strl09 = "";
          String Strl10 = "";


          if(iMillCode==0)
          {
               Strl01 = "g|-------------------------------------------------------------------------------------------------------------------------------------|";
               Strl02 = "|         EAMARJOTHI SPINNING MILLS LIMITEDF                      | C.Ex. RC No : 04/93        ECC No          : AADFA 6924A XM 001     |";
               Strl03 = "|                 EPUDUSURIPALAYAMF                               | PLA No      : 59/93        Range           : Gobichettipalayam.     |";
               Strl04 = "|               ENAMBIYUR  -  638 458F                            | Division    : Erode.       Commissionerate : Coimbatore.            |";
               Strl05 = "|                                                               |---------------------------------------------------------------------|";
               Strl06 = "| E-Mail : purchase@amarjothi.net  TIN No. 33632960864          |                          EENQUIRY LETTERF                             |";
               Strl07 = "| Phones : 04285 - 267201,267301,  Fax - 04285-267565           |---------------------------------------------------------------------|";
               Strl08 = "| Our C.S.T. No. 440691 dt. 24.09.1990                          |    NO     | "+common.Pad(SEnqNo,12)+"| Date | "+common.Pad(SEnqDate,12)+" | Page | "+common.Pad(""+Pctr,12)+" |";
               Strl09 = "| T.N.G.S.T No. R.C. No. 2960864  dt. 01.04.1995                |---------------------------------------------------------------------|";
               Strl10 = "|---------------------------------------------------------------|                                                                     |";
          }
          if(iMillCode==1)
          {
               Strl01 = "g|-------------------------------------------------------------------------------------------------------------------------------------|";
               Strl02 = "|         EAMARJOTHI SPINNING MILLS (DYEING DIVISION)F            | C.Ex. RC No : 04/93        ECC No          : AADFA 6924A XM 001     |";
               Strl03 = "|                 ESIPCOT INDUSTRIAL AREAF                        | PLA No      : 59/93        Range           : Gobichettipalayam.     |";
               Strl04 = "|               EPERUNDURAI  -  638 052F                          | Division    : Erode.       Commissionerate : Coimbatore.            |";
               Strl05 = "|                                                               |---------------------------------------------------------------------|";
               Strl06 = "| E-Mail : purchase@amarjothi.net  TIN No. 33632960864          |                          EENQUIRY LETTERF                             |";
               Strl07 = "| Phones : 04294 - 309339          Fax - 04294 - 230392         |---------------------------------------------------------------------|";
               Strl08 = "| Our C.S.T. No. 440691 dt. 24.09.1990                          |    NO     | "+common.Pad(SEnqNo,12)+"| Date | "+common.Pad(SEnqDate,12)+" | Page | "+common.Pad(""+Pctr,12)+" |";
               Strl09 = "| T.N.G.S.T No. R.C. No. 2960864  dt. 01.04.1995                |---------------------------------------------------------------------|";
               Strl10 = "|---------------------------------------------------------------|                                                                     |";
          }

          if(iMillCode==3)
          {
               Strl01 = "g|-------------------------------------------------------------------------------------------------------------------------------------|";
               Strl02 = "|         EAMARJOTHI COLOUR MELANGE SPINNING MILLS LIMITEDF       | C.Ex. RC No :              ECC No          :                        |";
               Strl03 = "|                     EPUDUSURIPALAYAMF                           | PLA No      :              Range           :                        |";
               Strl04 = "|                   ENAMBIYUR  -  638 458F                        | Division    :              Commissionerate :                        |";
               Strl05 = "|               GOBI(TK),ERODE(DT),TAMILNADU.                   |---------------------------------------------------------------------|";
               Strl06 = "| E-Mail : purchase@amarjothi.net  TIN No. 33712404816          |                          EENQUIRY LETTERF                             |";
               Strl07 = "| Phones : 04285 - 267201,267301,  Fax - 04285-267565           |---------------------------------------------------------------------|";
               Strl08 = "| Our C.S.T. No.1031365            dt.05.05.2010                |    NO     | "+common.Pad(SEnqNo,12)+"| Date | "+common.Pad(SEnqDate,12)+" | Page | "+common.Pad(""+Pctr,12)+" |";
             //Strl09 = "| T.N.G.S.T No. R.C. No.           dt.                          |---------------------------------------------------------------------|";
               Strl09 = "|                                                               |---------------------------------------------------------------------|";
               Strl10 = "|---------------------------------------------------------------|                                                                     |";
          }

          if(iMillCode==4)
          {
               Strl01 = "g|-------------------------------------------------------------------------------------------------------------------------------------|";
               Strl02 = "|              ESRI KAMADHENU TEXTILESF                           | C.Ex. RC No :              ECC No          :                        |";
               Strl03 = "|                 EPUDUSURIPALAYAMF                               | PLA No      :              Range           :                        |";
               Strl04 = "|               ENAMBIYUR  -  638 458F                            | Division    :              Commissionerate :                        |";
               Strl05 = "|            GOBI(TK),ERODE(DT),TAMILNADU.                      |---------------------------------------------------------------------|";
               Strl06 = "| E-Mail : purchase@amarjothi.net  TIN No.33612404386           |                          EENQUIRY LETTERF                             |";
             //Strl06 = "| E-Mail : purchase@amarjothi.net                               |                          EENQUIRY LETTERF                             |";
               Strl07 = "| Phones : 04285 - 267201,267301,  Fax - 04285-267565           |---------------------------------------------------------------------|";
               Strl08 = "| Our C.S.T. No. 972942            dt.14.12.2008                |    NO     | "+common.Pad(SEnqNo,12)+"| Date | "+common.Pad(SEnqDate,12)+" | Page | "+common.Pad(""+Pctr,12)+" |";
             //Strl09 = "| T.N.G.S.T No. R.C. No.          dt.                           |---------------------------------------------------------------------|";
             //Strl08 = "|                                                               |    NO     | "+common.Pad(SEnqNo,12)+"| Date | "+common.Pad(SEnqDate,12)+" | Page | "+common.Pad(""+Pctr,12)+" |";
               Strl09 = "|                                                               |---------------------------------------------------------------------|";
               Strl10 = "|---------------------------------------------------------------|                                                                     |";
          }


          String Strl11 = "|To,                                                            |                                                                     |";
          String Strl12 = "|"+common.Pad("M/s "+SSupName,63)+                             "|                                                                     |";
          String Strl13 = "|"+common.Pad(SAddr1,63)+                                      "|---------------------------------------------------------------------|";
          String Strl14 = "|"+common.Pad(SAddr2,63)+                                      "|                                                                     |";
          String Strl15 = "|"+common.Pad(SAddr3,63)+                                      "|                                                                     |";
          String Strl16 = "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl17 = "| Mat Code  | Description                        | UOM | Quantity |     Rate | Discount |   CenVat |      Tax | Surcharg |        Net |";
          String Strl17A= "|           | Make - Catalogue - Drawing Number  |     |          |       Rs | Rs & %   |  Rs & %  | Rs & %   | Rs & %   |    Rs & %  |";
          String Strl18 = "|-------------------------------------------------------------------------------------------------------------------------------------|";
          
          try
          {
               FW.write( Strl01+"\n");       
               FW.write( Strl02+"\n");       
               FW.write( Strl03+"\n");       
               FW.write( Strl04+"\n");       
               FW.write( Strl05+"\n");       
               FW.write( Strl06+"\n");       
               FW.write( Strl07+"\n");       
               FW.write( Strl08+"\n");       
               FW.write( Strl09+"\n");       
               FW.write( Strl10+"\n");       
               FW.write( Strl11+"\n");       
               FW.write( Strl12+"\n");       
               FW.write( Strl13+"\n");       
               FW.write( Strl14+"\n");       
               FW.write( Strl15+"\n");       
               FW.write( Strl16+"\n");       
               FW.write( Strl17+"\n");
               FW.write( Strl17A+"\n");       
               FW.write( Strl18+"\n");
               Lctr = 19;
          }
          catch(Exception ex){}
     }
     
     public void setEnqBody()
     {
          String Strl="";
          String SPColor   = "";
          String SPSet     = "";
          String SPSize    = "";
          String SPSide    = "";


          for(int i=0;i<VCode.size();i++)
          {
               setEnqHead();
               String SCode     = (String)VCode.elementAt(i);
               String SUOM      = (String)VUOM.elementAt(i);
               String SName     = (String)VName.elementAt(i);
               String SQty      = common.getRound((String)VQty.elementAt(i),2);
               String SDesc     = ((String)VDesc.elementAt(i)).trim();
               String SCatl     = ((String)VCatl.elementAt(i)).trim();
               String SDraw     = ((String)VDraw.elementAt(i)).trim();
               String SMake     = ((String)VMake.elementAt(i)).trim();

                      SPColor   = ((String)VPColor.elementAt(i)).trim();
                      SPSet     = ((String)VPSet.elementAt(i)).trim();
                      SPSize    = ((String)VPSize.elementAt(i)).trim();
                      SPSide    = ((String)VPSide.elementAt(i)).trim();

               try
               {
                    Strl =    "| "+common.Pad(SCode,9)+" | "+"E"+common.Pad(SName,34)+"F"+" | "+
                              common.Pad(SUOM,3)+" | "+common.Rad(SQty,8)+" | "+
                              common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                              common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                              common.Rad("",8)+" | "+common.Rad("",10)+" |";

                    FW.write(Strl+"\n");
                    Lctr = Lctr+1;
                    
                    //SDesc = SDesc+"-"+SMake+" "+SCatl+" "+SDraw;
                    String xtr = "";
                    if(SDesc.length() > 0)
                         xtr=xtr+SDesc+"-";
                    if(SMake.length() > 0)
                         xtr=xtr+SMake+"-";
                    if(SCatl.length() > 0)
                         xtr=xtr+SCatl+"-";
                    if(SDraw.length() > 0)
                         xtr=xtr+SDraw;

                    Vector vect = common.getLines(xtr);

                    for(int j=0;j<vect.size();j++)
                    {
                         Strl =    "| "+common.Space(9)+" | "+common.Pad((String)vect.elementAt(j),34)+" | "+
                                   common.Space(3)+" | "+common.Space(8)+" | "+
                                   common.Space(8)+" | "+common.Rad("",8)+" | "+
                                   common.Rad("",8)+" | "+common.Rad("",8)+" | "+
                                   common.Rad("",8)+" | "+common.Space(10)+" |";
                         FW.write(Strl+"\n");
                         Lctr = Lctr+1;
                    }
                    String SColor ="";

                    String SSize  ="";

                    if(SPColor.length() > 0)
                         SColor    = "Color: "+common.Pad(SPColor,13);

                    if(SPSet.length() > 0)
                         SColor    += " Set : "+SPSet;

                    if(SPSize.length() > 0)
                         SSize     = "Size : "+common.Pad(SPSize,13);

                    if(SPSide.length() > 0)
                         SSize     += " Side: "+SPSide;
                    
                    if(isStationary(SCode))
                    {
                         Strl =    "| "+common.Space(9)+" | "+common.Pad(SColor,34)+" | "+
                                   common.Space(3)+" | "+common.Space(8)+" | "+
                                   common.Space(8)+" | "+common.Space(8)+" | "+
                                   common.Space(8)+" | "+common.Space(8)+" | "+
                                   common.Space(8)+" | "+common.Space(10)+" |";
     
                         FW.write(Strl+"\n");
     
                         Strl =    "| "+common.Space(9)+" | "+common.Pad(SSize,34)+" | "+
                                   common.Space(3)+" | "+common.Space(8)+" | "+
                                   common.Space(8)+" | "+common.Space(8)+" | "+
                                   common.Space(8)+" | "+common.Space(8)+" | "+
                                   common.Space(8)+" | "+common.Space(10)+" |";
                         FW.write(Strl+"\n");
                         Lctr=Lctr+2;
                    }
                    Strl =    "| "+common.Space(9)+" | "+common.Space(34)+" | "+
                              common.Space(3)+" | "+common.Space(8)+" | "+
                              common.Space(8)+" | "+common.Space(8)+" | "+
                              common.Space(8)+" | "+common.Space(8)+" | "+
                              common.Space(8)+" | "+common.Space(10)+" |";
                    FW.write(Strl+"\n");
                    
                    Lctr=Lctr+1;
               }
               catch(Exception ex){}
          }
     }
     
     public void setEnqFoot(int iSig)
     {
          String Strl1 = "|-------------------------------------------------------------------------------------------------------------------------------------|";
          
          String Strl2 = "| "+common.Space(9)+" | "+common.Pad("T O T A L ",34)+" | "+
          common.Space(3)+" | "+common.Space(8)+" | "+
          common.Space(8)+" | "+common.Rad("",8)+" | "+
          common.Rad("",8)+" | "+common.Rad("",8)+" | "+
          common.Rad("",8)+" | "+common.Rad("",10)+" |";
          String Strl3 = "|-------------------------------------------------------------------------------------------------------------------------------------|";
          
          String Strl3a= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl3b= "| Expected Date of Delivery  |         Terms of Payments        | Rounding Off/Freight and Others  |           Order Value            |";
          String Strl3c= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl3d= "| "+common.Pad("",27)+"| "+common.Pad("",33)+"| "+
          common.Rad("",33)+"| "+
          common.Pad("",33)+"|";
//          ""+common.Pad("",16)+"|";
          String Strl3e= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl4 = "| Care : 1. EPlease mention our Enquiry No at your Quotation and Any Correspondence with us.F                                           |";
          String Strl5 = "|        2. Please send Samples (if any) along with your Quotation.                                                                   |";
          String Strl6 = "|        3. The quotation shall reach us, within 7 Days from the date of this Enquiry.                                                |";
          String Strl7 = "|        4. Please enclose price list and catalogue if any along with your quotation.                                                 |";
          String Strl8 = "|                                                                                                                                     |";
          String Strl9 = "| Note : 1. Diffentiate your price for cash & credit purchase.                                                                        |";
          String Strl10= "|        2. Instruments to avail concessional rate of tax (Form XVII etc.,)                                                           |";
          String Strl11= "|        3. Mention P & F Charges and Freight,if any, seperatly.                                                                      |";
          //String Strl12= "|        4. Fill your price in this enquiry and send back the same.               For AMARJOTHI COLOUR MELANGE SPINNING MILLS LTD     |";
          String Strl12= "|        4. Fill your price in this enquiry and send back the same.               "+common.Rad("For "+SMillName+"    ",52)+"|";
          String Strl13= "|                                                                                                                                     |";
          String Strl14= "|                                                                                                                                     |";
          String Strl15= "| Prepared By                                Checked By                                                       Authorised Signatory    |";
          String Strl16= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl17= "| Special Instruction :-                                                                                                              |";
          String Strl18= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl19= "| Regd. Office - 'AMARJOTHI HOUSE' 157, Kumaran Road, Tirupur - 638 601. Phone : 0421- 2201980 to 2201984                             |";
          String Strl20 ="| E-Mail       -  sales@amarjothi.com, ajsmmill@yahoo.com                                                                             |";
          String Strl21= "|-------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl22= "|                           I N D I A 'S   F I N E S T  M E L A N G E  Y A R N  P R O D U C E R S                                     |";
          String Strl23= "--------------------------------------------------------------------------------------------------------------------------------------|";
          String Strl24="";

          if(iSig==0)
               Strl24= "< End Of Report >";
          else
               Strl24= "(Continued on Next Page) ";

          try
          {
               FW.write( Strl1+"\n");       
               FW.write( Strl2+"\n");       
               FW.write( Strl3+"\n");
               if(iSig==0)
               {
                    FW.write( Strl3a+"\n");
                    FW.write( Strl3b+"\n");
                    FW.write( Strl3c+"\n");
                    FW.write( Strl3d+"\n");
                    FW.write( Strl3e+"\n");
               } 
               FW.write( Strl4+"\n");       
               FW.write( Strl5+"\n");       
               FW.write( Strl6+"\n");       
               FW.write( Strl7+"\n");       
               FW.write( Strl8+"\n");       
               FW.write( Strl9+"\n");       
               FW.write( Strl10+"\n");       
               FW.write( Strl11+"\n");       
               FW.write( Strl12+"\n");       
               FW.write( Strl13+"\n");       
               FW.write( Strl14+"\n");       
               FW.write( Strl15+"\n");       
               FW.write( Strl16+"\n");
               FW.write( Strl17+"\n");       
               FW.write( Strl18+"\n");       
               FW.write( Strl19+"\n");       
               FW.write( Strl20+"\n");       
               FW.write( Strl21+"\n");
               FW.write( Strl22+"\n");       
               FW.write( Strl23+"\n");
               FW.write( Strl24+"\n");       
          }
          catch(Exception ex){}
     }

     public void setDataIntoVector()
     {
          VCode      = new Vector();
          VName      = new Vector();
          VUOM       = new Vector();
          VQty       = new Vector();
          VDesc      = new Vector();  
          VCatl      = new Vector();
          VDraw      = new Vector();
          VMake      = new Vector();
          VPColor    = new Vector();
          VPSet      = new Vector();
          VPSize     = new Vector();
          VPSide     = new Vector();
          
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(getQString());
               while (res.next())
               {
                    String    str1      = res.getString(1);  
                    String    str2      = res.getString(2);
                    String    str3      = res.getString(3);
                    String    str4      = res.getString(4);
                                   
                              VCode     . addElement(str1);
                              VName     . addElement(str2);
                              VQty      . addElement(str3);
                              VUOM      . addElement(str4);
                              VDesc     . addElement(common.parseNull(res.getString(5)));
                              VCatl     . addElement(common.parseNull(res.getString(6)));      
                              VDraw     . addElement(common.parseNull(res.getString(7)));      
                              VMake     . addElement(common.parseNull(res.getString(8)));
                              VPColor   . addElement(common.parseNull(res.getString(9)));
                              VPSet     . addElement(common.parseNull(res.getString(10)));
                              VPSize    . addElement(common.parseNull(res.getString(11)));
                              VPSide    . addElement(common.parseNull(res.getString(12)));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public String getQString()
     {
          String QString  = "";
          
          QString = " SELECT Enquiry.Item_Code, InvItems.Item_Name, Enquiry.Qty, "+
                    " UOM.UoMName,Enquiry.Descr,InvItems.Catl,InvItems.Draw,Enquiry.Make,"+
                    " InvItems.PAPERCOLOR,PaperSet.SETNAME,InvItems.PAPERSIZE,PaperSide.SIDENAME"+
                    " FROM Enquiry INNER JOIN InvItems ON InvItems.Item_Code = Enquiry.Item_Code "+
                    " inner join paperset on paperset.setcode = invitems.papersets inner join paperside on paperside.sidecode = invitems.paperside"+
                    " Inner join UOM on Uom.uomcode = InvItems.uomcode Where Enquiry.EnqNo = "+SEnqNo+" And Enquiry.Sup_Code = '"+SSupCode+"'";
          return QString;
     }

     public void getAddr()
     {
          String QS =    "Select "+SSupTable+".Addr1,"+SSupTable+".Addr2,"+SSupTable+".Addr3,place.placename,"+SSupTable+".Name "+
                         "From "+SSupTable+" LEFT Join place On place.placecode = "+SSupTable+".City_Code "+
                         "Where "+SSupTable+".Ac_Code = '"+SSupCode+"'";
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      stat          =  theConnection.createStatement();

               ResultSet res  = stat.executeQuery(QS);

               while (res.next())
               {
                    SAddr1   = res.getString(1);
                    SAddr2   = res.getString(2);
                    SAddr3   = common.parseNull(res.getString(3));
                    SSupName = res.getString(5);
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex){System.out.println(ex);}
     }

     public boolean isStationary(String SItemCode)
     {
          String SStkGroupCode     = "";
          String QS                = "";

          if(iMillCode==0)
          {
               QS =    " select stkgroupcode,invitems.item_code from invitems"+
                       " where invitems.item_code = '"+SItemCode+"'";
          }
          else
          {
               QS =    " select stkgroupcode,"+SItemTable+".item_code from "+SItemTable+""+
                       " where "+SItemTable+".item_code = '"+SItemCode+"'";
          }
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnect    =  oraConnection.getConnection();               
               Statement      stat          = theConnect.createStatement();
               ResultSet      result        = stat.executeQuery(QS);
               while(result.next())
               {
                    SStkGroupCode  = common.parseNull((String)result.getString(1));
               }
               result         . close();
               stat           . close();
          }catch(Exception ex)
          {
               System    . out     . println(ex);
               ex        . printStackTrace();
          }
          if(SStkGroupCode.equals("B01"))
               return true;           

          return false;
     }
	private void CreatePDF() 
	{
		try 
		{
        	document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(SFileName));
            document.setPageSize(PageSize.A4);
            document.open();

            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);

			SetPDFHead();
			
			document.add(table);
			document.close();

			//JOptionPane.showMessageDialog(null, "PDF File Created in "+SFileName,"Info",JOptionPane.INFORMATION_MESSAGE);

			/*try
			{
				Process p = Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler  "+SFileName);
				p.waitFor(); 
			}
			catch(Exception ex)
			{
				System.out.println(" initPDF "+ex);
			}*/
          
        } 
		catch (Exception ex) 
		{
            ex.printStackTrace();
			System.out.println(ex);
        }
	}
//L,T,R,B
	private void SetPDFHead()
	{
		if(iMillCode==0)
        {
			AddCellIntoTable("AMARJOTHI SPINNING MILLS LTD" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,1,8,0,mediumbold);
			AddCellIntoTable("C.Ex. RC No :  04/93        ECC No                :  AADFA 6924A XM 001",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,1,8,0,smallnormal);

			AddCellIntoTable("PUDUSURIPALAYAM" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,0,8,0,mediumbold);
			AddCellIntoTable("PLA No         :  59/93        Range                   :   Gobichettipalayam.",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("NAMBIYUR  -  638 458" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,0,8,0,mediumbold);
			AddCellIntoTable("Division         :  Erode.       Commissionerate :  Coimbatore. ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,10f,4,0,8,0,mediumbold);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,10f,4,0,8,2,smallnormal);

			AddCellIntoTable("E-Mail : purchase@amarjothi.net  ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("ENQUIRY LETTER",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallbold);

			AddCellIntoTable("Phones :9489204502,9489204503,9489204522,9489204528.",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("GSTIN ID : 33AAFCA7082C1Z0 ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("No",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(SEnqNo,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("Date",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(SEnqDate,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("Page",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("1",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);

			//AddCellIntoTable("T.N.G.S.T No. R.C. No. 2960864  dt. 01.04.1995",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,2,smallnormal);
		//	AddCellIntoTable("",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("To," ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,1,8,0,smallnormal);
			AddCellIntoTable("" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("M/s "+SSupName ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable(SAddr1 ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,1,8,0,smallnormal);

			AddCellIntoTable(SAddr2 ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable(SAddr3,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,2,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,2,smallnormal);
		}
		if(iMillCode==1)
        {
			AddCellIntoTable("AMARJOTHI SPINNING MILLS (DYEING DIVISION)" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,1,8,0,mediumbold);
			AddCellIntoTable("C.Ex. RC No :  04/93        ECC No                :  AADFA 6924A XM 001",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,1,8,0,smallnormal);

			AddCellIntoTable("SIPCOT INDUSTRIAL AREA",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,0,8,0,mediumbold);
			AddCellIntoTable("PLA No         :  59/93        Range                   :   Gobichettipalayam.",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("PERUNDURAI  -  638 052" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,0,8,0,mediumbold);
			AddCellIntoTable("Division         :  Erode.       Commissionerate :  Coimbatore. ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,10f,4,0,8,0,mediumbold);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,10f,4,0,8,2,smallnormal);

			AddCellIntoTable("E-Mail : purchase@amarjothi.net  ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("ENQUIRY LETTER",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallbold);

			AddCellIntoTable("Phones :9489204502,9489204503,9489204522,9489204528.",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("GSTIN ID : 33AAFCA7082C1Z0 ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("No",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(SEnqNo,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("Date",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(SEnqDate,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("Page",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("1",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);

			AddCellIntoTable("T.N.G.S.T No. R.C. No. 2960864  dt. 01.04.1995",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,2,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("To," ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,1,8,0,smallnormal);
			AddCellIntoTable("" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("M/s "+SSupName ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable(SAddr1 ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable(SAddr2 ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,1,8,0,smallnormal);
			AddCellIntoTable("" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable(SAddr3,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,2,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,2,smallnormal);		
		}
		if(iMillCode==3)
        {
			AddCellIntoTable("AMARJOTHI COLOUR MELANGE SPINNING MILLS LIMITED" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,1,8,0,mediumbold);
			AddCellIntoTable("C.Ex. RC No  :             ECC No           : ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,1,8,0,smallnormal);

			AddCellIntoTable("PUDUSURIPALAYAM",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,0,8,0,mediumbold);
			AddCellIntoTable("PLA No          :              Range              : ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("NAMBIYUR  -  638 458" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,0,8,0,mediumbold);
			AddCellIntoTable("Division          :        Commissionerate   : . ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("GOBI(TK),ERODE(DT),TAMILNADU. " ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,0,8,0,mediumbold);
			AddCellIntoTable("" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,10f,4,0,8,0,mediumbold);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,10f,4,0,8,2,smallnormal);

			AddCellIntoTable("E-Mail : purchase@amarjothi.net  ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("ENQUIRY LETTER",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallbold);

			AddCellIntoTable("Phones :9489204502,9489204503,9489204522,9489204528.",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,2,smallnormal);

			AddCellIntoTable("GSTIN ID : 33AAFCA7082C1Z0",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("No",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(SEnqNo,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("Date",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(SEnqDate,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("Page",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("1",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);

			AddCellIntoTable("T.N.G.S.T No. R.C. No. 2960864  dt. 01.04.1995",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,2,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("To," ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,1,8,0,smallnormal);
			AddCellIntoTable("" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("M/s "+SSupName ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable(SAddr1 ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,2,smallnormal);

			AddCellIntoTable(SAddr2 ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable(SAddr3,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,2,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,2,smallnormal);		
		}
		if(iMillCode==4)
        {
			AddCellIntoTable("SRI KAMADHENU TEXTILES" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,1,8,0,mediumbold);
			AddCellIntoTable("C.Ex. RC No  :             ECC No           : ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,1,8,0,smallnormal);

			AddCellIntoTable("PUDUSURIPALAYAM",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,0,8,0,mediumbold);
			AddCellIntoTable("PLA No          :              Range              : ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("NAMBIYUR  -  638 458" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,0,8,0,mediumbold);
			AddCellIntoTable("Division          :        Commissionerate   : . ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("GOBI(TK),ERODE(DT),TAMILNADU. ",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,4,0,8,0,mediumbold);
			AddCellIntoTable("" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,4,10f,4,0,8,0,mediumbold);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,10f,4,0,8,2,smallnormal);

			AddCellIntoTable("E-Mail : purchase@amarjothi.net  ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("ENQUIRY LETTER",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallbold);

			AddCellIntoTable("Phones :9489204502,9489204503,9489204522,9489204528.",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("GSTIN ID:33AAFCA7082C1Z0  ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,2,smallnormal);
			AddCellIntoTable("No",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(SEnqNo,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("Date",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(SEnqDate,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("Page",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("1",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);

			AddCellIntoTable("To," ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,1,8,0,smallnormal);
			AddCellIntoTable("" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable("M/s "+SSupName ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable(SAddr1 ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable(SAddr2 ,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,0,smallnormal);
			AddCellIntoTable("" ,table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,0,smallnormal);

			AddCellIntoTable(SAddr3,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,4,4,0,8,2,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,4,0,8,2,smallnormal);		
		}

	//For Body PDF

		AddCellIntoTable("Mat Code",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,smallbold);
		AddCellIntoTable("Description Make - Catalogue - Drawing No",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,smallbold);
		AddCellIntoTable("UOM",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,smallbold);
		AddCellIntoTable("Quantity",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,smallbold);
		AddCellIntoTable("Rate Rs",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,smallbold);
		AddCellIntoTable("Discount Rs & %",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,smallbold);
		AddCellIntoTable("CenVat Rs & %",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,smallbold);
		AddCellIntoTable("Tax Rs & %",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,smallbold);
		AddCellIntoTable("SurCharge Rs & %",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,smallbold);
		AddCellIntoTable("Net Rs & %",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,smallbold);

		for(int i=0;i<VCode.size();i++)
        {
			String SCode     = (String)VCode.elementAt(i);
			String SUOM      = (String)VUOM.elementAt(i);
			String SName     = (String)VName.elementAt(i);
			String SQty      = common.getRound((String)VQty.elementAt(i),2);
			String SDesc     = ((String)VDesc.elementAt(i)).trim();
			String SCatl     = ((String)VCatl.elementAt(i)).trim();
			String SDraw     = ((String)VDraw.elementAt(i)).trim();
			String SMake     = ((String)VMake.elementAt(i)).trim();

			String xtr = "";
            if(SDesc.length() > 0)
                 xtr=xtr+SDesc+"-";
            if(SMake.length() > 0)
                 xtr=xtr+SMake+"-";
            if(SCatl.length() > 0)
                 xtr=xtr+SCatl+"-";
            if(SDraw.length() > 0)
             xtr=xtr+SDraw;

			AddCellIntoTable(common.parseNull((String)VCode.elementAt(i)),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(common.parseNull((String)VName.elementAt(i))+"  "+xtr,table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(common.parseNull((String)VUOM.elementAt(i)),table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable(common.getRound((String)VQty.elementAt(i),2),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);
			AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,4,1,8,2,smallnormal);

		}
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,20f,4,1,8,2,smallbold);
		AddCellIntoTable("Total",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,20f,4,1,8,2,smallbold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,20f,4,1,8,2,smallbold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,20f,4,1,8,2,smallbold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,20f,4,1,8,2,smallbold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,20f,4,1,8,2,smallbold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,20f,4,1,8,2,smallbold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,20f,4,1,8,2,smallbold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,20f,4,1,8,2,smallbold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,20f,4,1,8,2,smallbold);

		AddCellIntoTable("Expected Date Of Delivery",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,2,20f,4,1,8,2,smallnormal);
		AddCellIntoTable("Terms Of Payment",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,3,20f,4,1,8,2,smallnormal);
		AddCellIntoTable("Rounding Off/Freight And Others",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,3,20f,4,1,8,2,smallnormal);
		AddCellIntoTable("Order Value",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,2,20f,4,1,8,2,smallnormal);

		AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,3,20f,4,1,8,2,smallnormal);
		AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,2,20f,4,1,8,2,smallnormal);
		AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,2,20f,4,1,8,2,smallnormal);
		AddCellIntoTable("",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,3,20f,4,1,8,2,smallnormal);

		AddCellIntoTable("Care  :  1.Please Mention Our Enquiry No at Your Quotation and Any Correspondence With us.",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,10,4,0,8,0,smallbold);
		AddCellIntoTable("             2.Please send Samples (if Any) along with Your Quotation.",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,10,4,0,8,0,smallnormal);
		AddCellIntoTable("             3.The Quotation Shall reach us,within 7 Days From the Date of This Enquiry",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,10,4,0,8,0,smallnormal);
		AddCellIntoTable("             4.Please Enclose price list and catalogue if any along with your quotation.",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,10,4,0,8,0,smallnormal);

		AddCellIntoTable("",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,10,10f,4,0,8,0,smallnormal);

		AddCellIntoTable("Note  :  1.Differentiate Your Price for Cash & Credit Purchase. ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,10,4,0,8,0,smallnormal);
		AddCellIntoTable("             2.Instruments to avail Concessional rate of tax(Form XVII etc.,",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,10,4,0,8,0,smallnormal);
		AddCellIntoTable("	             3.Mention P & F Charges and Freight, if any,Seperatly ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,10,4,0,8,0,smallnormal);
		AddCellIntoTable("             4.Fill Your Price in this Enquiry and send back the same ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,6,4,0,0,0,smallnormal);
		AddCellIntoTable("For Amarjothi Spinning Mills LTD ",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,4,0,0,8,0,smallnormal);

		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,10,20f,4,0,8,0,smallnormal);

		AddCellIntoTable("Prepare By",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,3,4,0,0,2,smallnormal);
		AddCellIntoTable("Checked By ",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,3,0,0,0,2,smallnormal);
		AddCellIntoTable("Authorised Signatory",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,4,0,0,8,2,smallnormal);

		AddCellIntoTable("Special Instruction :-",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,10,25f,4,1,8,2,smallnormal);

		AddCellIntoTable("Regd.Office - 'AMARJOTHI HOUSE' 157,Kumaran Road,Tirupur-638 601.Phone : 0421-2201960 to 2201984",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,10,15f,4,0,8,0,smallnormal);
		AddCellIntoTable("E-Mail         - sales@amarjothi.com,ajsmmill@yahoo.com",table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,10,15f,4,0,8,2,smallnormal);

		AddCellIntoTable("I N D I A 'S   F I N E S T   M E L A N G E  Y A R N  P R O D U C E R S",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,10,20f,4,1,8,2,smallnormal);

		

		
	}
		public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
 
   
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {


        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));

      
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }
}



