/******************************************************************************************************************************
 *
 ******************************************************************************************************************************
 *  File Name             :  TestJava
 *  Project Name          :  Amarjothi
 *  Customer Name	  :  Amarjothi
 *  Author		  :  Tirupathi Rao
 *  Version & Date        :  v1.0 &  May 29, 2009
 *  File Description      :
 *  Modification History  :
 *----------------------------------------------------------------------------------------------------------------------------
 *    Version		   Date			  Description			                  Changed By
 *----------------------------------------------------------------------------------------------------------------------------
 *
 ******************************************************************************************************************************/
/***********************************
Files to be imported
 ************************************/
//package com.amarjothi.alert;

package enquiry;

import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.*;
//import javax.sql.DataSource;

/******************************************************************************
This class has the method which deals with MailSending  functionality

@author 	Tirupathi Rao.P

This class contains the following methods.

@see		1.createMail()

@since      1.0
 ******************************************************************************/
public class HTMLMailSender {

//    static ResourceBundle rb = ResourceBundle.getBundle("ApplicationProperties");
//    static String sFromMail = rb.getString("MAIL_USER");
//    static String sFromMailPwd = rb.getString("MAIL_PWD");
//    static String sMailHost = rb.getString("MAIL_HOST");
//    static String sMailPort = rb.getString("MAIL_PORT");

                                               /*
    static String sFromMail ="collect@amarjothi.net";// rb.getString("MAIL_USER");
    static String sFromMailPwd ="master";// rb.getString("MAIL_PWD");
    static String sMailHost ="smtp.bizmail.yahoo.com";// rb.getString("MAIL_HOST");
    static String sMailPort ="465";// rb.getString("MAIL_PORT");
                                               */
//    static String sFromMail ="sales@amarjothi.net";// rb.getString("MAIL_USER");

    static String sFromMail ="purchase@amarjothi.net";// rb.getString("MAIL_USER");
    static String sFromMailPwd ="master";// rb.getString("MAIL_PWD");
    static String sMailHost ="192.168.1.104";// rb.getString("MAIL_HOST");
    static String sMailPort ="25";// rb.getString("MAIL_PORT");

//    static String sMailHost ="smtp.bizmail.yahoo.com";// rb.getString("MAIL_HOST");
//    static String sMailPort ="465";// rb.getString("MAIL_PORT");

    public static int createMail(String sCc, String sToMail, String sSubject, String sText, String sAttachment,String SFromMailId) {

        System.out.println("sFromMail:" + sFromMail);
        System.out.println("sFromMailPwd:" + sFromMailPwd);
        System.out.println("sMailHost:" + sMailHost);
        System.out.println("sMailPort:" + sMailPort);

        sFromMail = SFromMailId;

        Transport tMail = null;
        Session sMail = null;
        MimeMessage msg = null;

        String transportProtocol = "smtp";

//        String mailsend[] = null;
	    int imailSend =0;

        try {

            sMail = getSession();//Make session
          
            tMail = sMail.getTransport(transportProtocol);//Make transport

            msg = new MimeMessage(sMail);
            msg.setHeader("X-Mailer", "Amarjothi internal mail server");
            msg.setFrom(new InternetAddress(sFromMail));
            msg.setSubject(sSubject);

            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setText("");

//MohanJr
            MimeBodyPart textpart = new MimeBodyPart();   
            textpart.setContent(sText,"text/html");



            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

//MohanJr
            multipart.addBodyPart(textpart);

//anil
            messageBodyPart = new MimeBodyPart();
//        String filename = "D:\\file.txt";
//        String filename = "D:\\filetest.txt";
        String filename = sAttachment;
        System.out.println("Attach : "+sAttachment);
        DataSource source = new FileDataSource(filename);

        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(filename);

//        multipart.addBodyPart(messageBodyPart);

//anil
            msg.setContent(multipart);
            msg.setSentDate(new Date());


            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(sToMail));

            //msg.setRecipient(Message.RecipientType.CC, new InternetAddress(sCc));
            tMail.connect();
            tMail.sendMessage(msg, msg.getRecipients(Message.RecipientType.TO));
            //	tMail.sendMessage(msg, msg.getRecipients(Message.RecipientType.CC));
            tMail.close();


        } catch (Exception eError) {

            eError.printStackTrace();
		imailSend   = -1;
            JOptionPane.showMessageDialog(null,"Error in Mail Sending","Information",JOptionPane.INFORMATION_MESSAGE);                                                

        } finally {

            try {
                if (tMail != null) {
                    tMail.close();
                }
            } catch (MessagingException eError) {
                eError.printStackTrace();
		    imailSend   = -1;
                JOptionPane.showMessageDialog(null,"Error in Mail Sending","Information",JOptionPane.INFORMATION_MESSAGE);                                                

            }
        }
        return imailSend;
    }

    public static Session getSession() {
        Authenticator authenticator = new Authenticator();

        Properties properties = new Properties();
        properties.setProperty("mail.smtp.submitter", authenticator.getPasswordAuthentication().getUserName());
        properties.setProperty("mail.smtp.auth", "true");

        properties.setProperty("mail.smtp.host", sMailHost);
        properties.setProperty("mail.smtp.port", sMailPort);
        properties.setProperty("mail.user", sFromMail);
        properties.setProperty("mail.password", sFromMailPwd);

//          properties.setProperty("mail.smtps.auth", "true");
//          properties.setProperty("mail.smtp.starttls.enable", "true");
//          properties.setProperty("mail.smtp.socketFactory.port", sMailPort);
//          properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//          properties.setProperty("mail.smtp.socketFactory.fallback", "false");

        return Session.getInstance(properties, authenticator);
    }

    private static class Authenticator extends javax.mail.Authenticator {

        private PasswordAuthentication authentication;

        public Authenticator() {
            String username = sFromMail;
            String password = sFromMailPwd;
            authentication = new PasswordAuthentication(username, password);
        }

        protected PasswordAuthentication getPasswordAuthentication() {
            return authentication;
        }
    }
}

